.class public final Ldor;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private bEq:Lfdt;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mAudioState:Ldbh;

.field private final mLocationOracle:Lfdr;

.field private final mLocationSettings:Lcob;

.field private final mSearchConfig:Lcjs;

.field private final mServiceState:Ldcu;


# direct methods
.method public constructor <init>(Lfdr;Lcob;Lcjs;Ldbh;Ldcu;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Ldor;->mLocationOracle:Lfdr;

    .line 35
    iput-object p2, p0, Ldor;->mLocationSettings:Lcob;

    .line 36
    iput-object p3, p0, Ldor;->mSearchConfig:Lcjs;

    .line 37
    iput-object p4, p0, Ldor;->mAudioState:Ldbh;

    .line 38
    iput-object p5, p0, Ldor;->mServiceState:Ldcu;

    .line 39
    return-void
.end method


# virtual methods
.method public final a(Lddb;)V
    .locals 6

    .prologue
    .line 42
    invoke-virtual {p1}, Lddb;->aaD()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lddb;->aaE()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 43
    :cond_0
    iget-object v0, p0, Ldor;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->amT()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldor;->mAudioState:Ldbh;

    invoke-virtual {v0}, Ldbh;->Wg()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 47
    :cond_1
    iget-object v0, p0, Ldor;->bEq:Lfdt;

    if-nez v0, :cond_2

    iget-object v0, p0, Ldor;->mLocationSettings:Lcob;

    invoke-interface {v0}, Lcob;->QL()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldor;->mLocationOracle:Lfdr;

    const-string v1, "search"

    invoke-interface {v0, v1}, Lfdr;->lK(Ljava/lang/String;)Lfdt;

    move-result-object v0

    iput-object v0, p0, Ldor;->bEq:Lfdt;

    iget-object v0, p0, Ldor;->bEq:Lfdt;

    invoke-interface {v0}, Lfdt;->acquire()V

    iget-object v0, p0, Ldor;->mLocationOracle:Lfdr;

    iget-object v1, p0, Ldor;->mSearchConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->Lv()I

    move-result v1

    int-to-long v2, v1

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-interface {v0, v2, v3}, Lfdr;->bk(J)V

    .line 52
    :cond_2
    :goto_0
    return-void

    .line 49
    :cond_3
    iget-object v0, p0, Ldor;->bEq:Lfdt;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldor;->bEq:Lfdt;

    invoke-interface {v0}, Lfdt;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Ldor;->bEq:Lfdt;

    goto :goto_0
.end method
