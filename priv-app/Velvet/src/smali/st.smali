.class public final Lst;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private synthetic AO:Lwo;

.field private synthetic AP:I

.field private synthetic AQ:Lcom/android/launcher3/CellLayout;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/CellLayout;Lwo;I)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lst;->AQ:Lcom/android/launcher3/CellLayout;

    iput-object p2, p0, Lst;->AO:Lwo;

    iput p3, p0, Lst;->AP:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    .line 251
    iget-object v0, p0, Lst;->AO:Lwo;

    iget-object v0, v0, Lwo;->Jx:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 255
    if-nez v0, :cond_0

    .line 257
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 269
    :goto_0
    return-void

    .line 266
    :cond_0
    iget-object v0, p0, Lst;->AQ:Lcom/android/launcher3/CellLayout;

    invoke-static {v0}, Lcom/android/launcher3/CellLayout;->a(Lcom/android/launcher3/CellLayout;)[F

    move-result-object v1

    iget v2, p0, Lst;->AP:I

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v1, v2

    .line 267
    iget-object v0, p0, Lst;->AQ:Lcom/android/launcher3/CellLayout;

    iget-object v1, p0, Lst;->AQ:Lcom/android/launcher3/CellLayout;

    invoke-static {v1}, Lcom/android/launcher3/CellLayout;->b(Lcom/android/launcher3/CellLayout;)[Landroid/graphics/Rect;

    move-result-object v1

    iget v2, p0, Lst;->AP:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CellLayout;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_0
.end method
