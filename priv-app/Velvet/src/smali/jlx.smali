.class public final Ljlx;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field private alA:Ljava/lang/String;

.field private esY:I

.field private esZ:Ljkg;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 14391
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 14392
    iput v1, p0, Ljlx;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljlx;->afh:Ljava/lang/String;

    iput v1, p0, Ljlx;->esY:I

    const-string v0, ""

    iput-object v0, p0, Ljlx;->alA:Ljava/lang/String;

    iput-object v2, p0, Ljlx;->esZ:Ljkg;

    iput-object v2, p0, Ljlx;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljlx;->eCz:I

    .line 14393
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 14306
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljlx;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlx;->afh:Ljava/lang/String;

    iget v0, p0, Ljlx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlx;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljlx;->esY:I

    iget v0, p0, Ljlx;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlx;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlx;->alA:Ljava/lang/String;

    iget v0, p0, Ljlx;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljlx;->aez:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljlx;->esZ:Ljkg;

    if-nez v0, :cond_1

    new-instance v0, Ljkg;

    invoke-direct {v0}, Ljkg;-><init>()V

    iput-object v0, p0, Ljlx;->esZ:Ljkg;

    :cond_1
    iget-object v0, p0, Ljlx;->esZ:Ljkg;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 14409
    iget v0, p0, Ljlx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 14410
    const/4 v0, 0x1

    iget-object v1, p0, Ljlx;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 14412
    :cond_0
    iget v0, p0, Ljlx;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 14413
    const/4 v0, 0x2

    iget v1, p0, Ljlx;->esY:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 14415
    :cond_1
    iget v0, p0, Ljlx;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 14416
    const/4 v0, 0x3

    iget-object v1, p0, Ljlx;->alA:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 14418
    :cond_2
    iget-object v0, p0, Ljlx;->esZ:Ljkg;

    if-eqz v0, :cond_3

    .line 14419
    const/4 v0, 0x4

    iget-object v1, p0, Ljlx;->esZ:Ljkg;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 14421
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 14422
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 14426
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 14427
    iget v1, p0, Ljlx;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 14428
    const/4 v1, 0x1

    iget-object v2, p0, Ljlx;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14431
    :cond_0
    iget v1, p0, Ljlx;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 14432
    const/4 v1, 0x2

    iget v2, p0, Ljlx;->esY:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14435
    :cond_1
    iget v1, p0, Ljlx;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 14436
    const/4 v1, 0x3

    iget-object v2, p0, Ljlx;->alA:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14439
    :cond_2
    iget-object v1, p0, Ljlx;->esZ:Ljkg;

    if-eqz v1, :cond_3

    .line 14440
    const/4 v1, 0x4

    iget-object v2, p0, Ljlx;->esZ:Ljkg;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14443
    :cond_3
    return v0
.end method
