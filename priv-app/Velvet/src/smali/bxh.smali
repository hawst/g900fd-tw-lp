.class public Lbxh;
.super Lbyk;
.source "PG"

# interfaces
.implements Lbyj;


# instance fields
.field aNA:Lhym;

.field private aNB:Landroid/os/PowerManager$WakeLock;

.field private aNC:Landroid/os/PowerManager$WakeLock;

.field private aND:Z

.field private aNE:Landroid/view/View;

.field private aNF:Ldtr;

.field private aNG:[Ljava/lang/String;

.field private aNH:Z

.field private final aNI:Ljava/lang/Runnable;

.field private aNJ:Z

.field private aNK:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aNL:J

.field aNM:J

.field aNN:Z

.field aNO:Z

.field aNP:Z

.field aNQ:I

.field aNR:Z

.field aNS:Z

.field aNT:Z

.field aNU:Z

.field aNV:Z

.field private final aNW:Landroid/content/BroadcastReceiver;

.field private final aNX:Landroid/content/BroadcastReceiver;

.field private final aNY:Ljava/lang/Runnable;

.field private aNs:Lbxr;

.field private aNt:Landroid/view/View;

.field aNu:Landroid/app/Dialog;

.field aNv:Z

.field aNw:Z

.field aNx:Landroid/widget/TextView;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field aNy:Landroid/widget/TextView;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aNz:Landroid/widget/ImageView;

.field private final anm:Lecp;

.field private final mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

.field private mHandler:Landroid/os/Handler;

.field mIntentStarter:Lelp;

.field private mPrefs:Landroid/content/SharedPreferences;

.field mScreenStateHelper:Ldmh;

.field mSpeechLevelSource:Lequ;

.field mState:Lbyi;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 299
    const-string v0, "HandsFreeOverlay"

    invoke-direct {p0, v0}, Lbyk;-><init>(Ljava/lang/String;)V

    .line 88
    iput-boolean v1, p0, Lbxh;->aNv:Z

    .line 89
    iput-boolean v1, p0, Lbxh;->aNw:Z

    .line 150
    new-instance v0, Lbxq;

    invoke-direct {v0, p0}, Lbxq;-><init>(Lbxh;)V

    iput-object v0, p0, Lbxh;->anm:Lecp;

    .line 157
    new-instance v0, Lcom/google/android/search/shared/service/ClientConfig;

    const-wide/32 v2, 0xc000

    const-string v1, "e100-overlay"

    const-string v4, "android-search-app"

    invoke-static {v1, v4}, Lcom/google/android/shared/search/SearchBoxStats;->aA(Ljava/lang/String;Ljava/lang/String;)Lehj;

    move-result-object v1

    invoke-virtual {v1}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/search/shared/service/ClientConfig;-><init>(JLcom/google/android/shared/search/SearchBoxStats;)V

    iput-object v0, p0, Lbxh;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    .line 166
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lbxh;->mHandler:Landroid/os/Handler;

    .line 179
    new-instance v0, Lbxi;

    invoke-direct {v0, p0}, Lbxi;-><init>(Lbxh;)V

    iput-object v0, p0, Lbxh;->aNI:Ljava/lang/Runnable;

    .line 241
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbxh;->aNT:Z

    .line 248
    new-instance v0, Lbxj;

    invoke-direct {v0, p0}, Lbxj;-><init>(Lbxh;)V

    iput-object v0, p0, Lbxh;->aNW:Landroid/content/BroadcastReceiver;

    .line 275
    new-instance v0, Lbxk;

    invoke-direct {v0, p0}, Lbxk;-><init>(Lbxh;)V

    iput-object v0, p0, Lbxh;->aNX:Landroid/content/BroadcastReceiver;

    .line 286
    new-instance v0, Lbxl;

    invoke-direct {v0, p0}, Lbxl;-><init>(Lbxh;)V

    iput-object v0, p0, Lbxh;->aNY:Ljava/lang/Runnable;

    .line 300
    return-void
.end method

.method static synthetic a(Lbxh;)Ldtr;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lbxh;->aNF:Ldtr;

    return-object v0
.end method

.method static synthetic a(Lbxh;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 72
    invoke-static {p1}, Lbxh;->ar(Landroid/view/View;)V

    return-void
.end method

.method private a(Lejt;)V
    .locals 5

    .prologue
    .line 387
    const v0, 0x7f110168

    invoke-virtual {p0, v0}, Lbxh;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lejs;

    invoke-virtual {p0}, Lbxh;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00d1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lbxh;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d00d4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v1, v2, v3, p1}, Lejs;-><init>(IILejt;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 391
    return-void
.end method

.method private static ar(Landroid/view/View;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 961
    if-nez p0, :cond_0

    .line 967
    :goto_0
    return-void

    .line 962
    :cond_0
    const/high16 v0, 0x44160000    # 600.0f

    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result v1

    sub-float v1, v3, v1

    mul-float/2addr v0, v1

    float-to-long v0, v0

    .line 963
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method private static as(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 981
    if-nez p0, :cond_0

    .line 986
    :goto_0
    return-void

    .line 982
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result v1

    const/high16 v2, 0x44160000    # 600.0f

    mul-float/2addr v1, v2

    float-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method private n(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 425
    const-string v0, "android.speech.action.WEB_SEARCH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 427
    invoke-virtual {p0}, Lbxh;->Bo()V

    .line 429
    :cond_0
    const-string v0, "start-reason"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 430
    const-string v0, "start-reason"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbxh;->aNK:Ljava/lang/String;

    .line 431
    const-string v0, "start-time"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lbxh;->aNL:J

    .line 433
    :cond_1
    return-void
.end method


# virtual methods
.method protected final Bm()Lcom/google/android/search/shared/service/ClientConfig;
    .locals 1

    .prologue
    .line 701
    iget-object v0, p0, Lbxh;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    return-object v0
.end method

.method protected final Bn()Lecm;
    .locals 1

    .prologue
    .line 706
    iget-object v0, p0, Lbxh;->anm:Lecp;

    return-object v0
.end method

.method public final Bo()V
    .locals 4

    .prologue
    .line 854
    iget-object v0, p0, Lbxh;->aOO:Lecq;

    invoke-virtual {v0}, Lecq;->cancel()V

    .line 856
    new-instance v0, Ldlu;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ldlu;-><init>(Z)V

    .line 857
    new-instance v1, Ldmh;

    invoke-direct {v1, p0}, Ldmh;-><init>(Landroid/content/Context;)V

    .line 858
    const/4 v2, 0x3

    iget-object v3, p0, Lbxh;->aNA:Lhym;

    invoke-virtual {v1}, Ldmh;->adq()I

    move-result v1

    invoke-virtual {v0, v2, v3, v1}, Ldlu;->a(ILhym;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 861
    iget-object v0, p0, Lbxh;->aOO:Lecq;

    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apn()Lcom/google/android/shared/search/Query;

    move-result-object v1

    iget-object v2, p0, Lbxh;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {v2}, Lcom/google/android/search/shared/service/ClientConfig;->anv()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lecq;->y(Lcom/google/android/shared/search/Query;)V

    .line 869
    :goto_0
    return-void

    .line 865
    :cond_0
    iget-object v0, p0, Lbxh;->aOO:Lecq;

    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    const v2, 0x7f0a094f

    invoke-virtual {p0, v2}, Lbxh;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/shared/search/Query;->az(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    iget-object v2, p0, Lbxh;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {v2}, Lcom/google/android/search/shared/service/ClientConfig;->anv()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lecq;->y(Lcom/google/android/shared/search/Query;)V

    goto :goto_0
.end method

.method public final Bp()V
    .locals 1

    .prologue
    .line 873
    iget-object v0, p0, Lbxh;->aOO:Lecq;

    invoke-virtual {v0}, Lecq;->stopListening()V

    .line 874
    return-void
.end method

.method public final Bq()V
    .locals 1

    .prologue
    .line 879
    iget-object v0, p0, Lbxh;->aOO:Lecq;

    invoke-virtual {v0}, Lecq;->Bq()V

    .line 880
    return-void
.end method

.method public final Br()V
    .locals 1

    .prologue
    .line 941
    iget-object v0, p0, Lbxh;->aNC:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 942
    return-void
.end method

.method public final Bs()V
    .locals 2

    .prologue
    .line 949
    iget-boolean v0, p0, Lbxh;->aND:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbxh;->aNB:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 951
    iget-object v0, p0, Lbxh;->aNB:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 952
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbxh;->aND:Z

    .line 954
    :cond_0
    iget-object v0, p0, Lbxh;->aNC:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 955
    iget-object v0, p0, Lbxh;->aNC:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 957
    :cond_1
    iget-object v0, p0, Lbxh;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lbxh;->aNI:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 958
    return-void
.end method

.method public final ad(II)V
    .locals 6

    .prologue
    const/4 v1, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 889
    if-eq p2, v1, :cond_0

    .line 890
    iget-object v0, p0, Lbxh;->aNE:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 893
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 936
    :goto_0
    return-void

    .line 895
    :pswitch_0
    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-ne p1, v0, :cond_2

    .line 897
    :cond_1
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iget-object v1, p0, Lbxh;->aNG:[Ljava/lang/String;

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 898
    iget-object v1, p0, Lbxh;->aNx:Landroid/widget/TextView;

    iget-object v2, p0, Lbxh;->aNG:[Ljava/lang/String;

    aget-object v0, v2, v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 900
    :cond_2
    iget-object v0, p0, Lbxh;->aNx:Landroid/widget/TextView;

    invoke-static {v0}, Lbxh;->ar(Landroid/view/View;)V

    .line 901
    iget-object v0, p0, Lbxh;->aNt:Landroid/view/View;

    invoke-static {v0}, Lbxh;->ar(Landroid/view/View;)V

    .line 902
    iget-object v0, p0, Lbxh;->aNz:Landroid/widget/ImageView;

    invoke-static {v0}, Lbxh;->ar(Landroid/view/View;)V

    .line 903
    iget-object v0, p0, Lbxh;->aNz:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lbxh;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020174

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 905
    iget-object v0, p0, Lbxh;->aNF:Ldtr;

    invoke-virtual {v0, v4}, Ldtr;->eg(Z)V

    goto :goto_0

    .line 908
    :pswitch_1
    if-ne p1, v5, :cond_3

    .line 909
    iget-object v0, p0, Lbxh;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lbxh;->aNI:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 911
    :cond_3
    iget-object v0, p0, Lbxh;->aNx:Landroid/widget/TextView;

    invoke-static {v0}, Lbxh;->as(Landroid/view/View;)V

    .line 912
    iget-object v0, p0, Lbxh;->aNt:Landroid/view/View;

    invoke-static {v0}, Lbxh;->as(Landroid/view/View;)V

    .line 913
    iget-object v0, p0, Lbxh;->aNz:Landroid/widget/ImageView;

    invoke-static {v0}, Lbxh;->ar(Landroid/view/View;)V

    .line 914
    iget-object v0, p0, Lbxh;->aNz:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lbxh;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020171

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 916
    iget-object v0, p0, Lbxh;->aNF:Ldtr;

    invoke-virtual {v0, v5}, Ldtr;->eg(Z)V

    goto :goto_0

    .line 919
    :pswitch_2
    iget-object v0, p0, Lbxh;->aNz:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lbxh;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020173

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 923
    :pswitch_3
    iget-object v0, p0, Lbxh;->aNE:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lbxh;->aNE:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    iget-object v0, p0, Lbxh;->aNE:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotation(F)V

    iget-object v0, p0, Lbxh;->aNE:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const v1, 0x4728c000    # 43200.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->rotation(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 924
    iget-object v0, p0, Lbxh;->aNz:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lbxh;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020172

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 926
    iget-object v0, p0, Lbxh;->aNF:Ldtr;

    invoke-virtual {v0, v4}, Ldtr;->eg(Z)V

    goto/16 :goto_0

    .line 929
    :pswitch_4
    iget-object v0, p0, Lbxh;->aNx:Landroid/widget/TextView;

    invoke-static {v0}, Lbxh;->as(Landroid/view/View;)V

    .line 930
    iget-object v0, p0, Lbxh;->aNz:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lbxh;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020220

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 932
    iget-object v0, p0, Lbxh;->aNz:Landroid/widget/ImageView;

    invoke-static {v0}, Lbxh;->ar(Landroid/view/View;)V

    .line 933
    iget-object v0, p0, Lbxh;->aNF:Ldtr;

    invoke-virtual {v0, v4}, Ldtr;->eg(Z)V

    goto/16 :goto_0

    .line 893
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final aq(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 819
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 820
    const v3, 0x7f110176

    if-ne v2, v3, :cond_2

    .line 822
    iget-object v2, p0, Lbxh;->aNA:Lhym;

    invoke-virtual {v2}, Lhym;->aTO()Z

    move-result v2

    .line 823
    iget-object v3, p0, Lbxh;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "preferBluetoothSpeaker"

    if-nez v2, :cond_1

    :goto_0
    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 847
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 823
    goto :goto_0

    .line 824
    :cond_2
    const v3, 0x7f110179

    if-ne v2, v3, :cond_4

    .line 826
    iget-object v2, p0, Lbxh;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "hands_free_auto_driving_car_mode_enabled"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 827
    iget-object v3, p0, Lbxh;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "hands_free_auto_driving_car_mode_enabled"

    if-nez v2, :cond_3

    :goto_2
    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 829
    invoke-static {}, Lhgn;->aOF()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbxh;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_1

    :cond_3
    move v0, v1

    .line 827
    goto :goto_2

    .line 830
    :cond_4
    const v3, 0x7f11017c

    if-ne v2, v3, :cond_6

    .line 832
    iget-object v2, p0, Lbxh;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "hands_free_read_notifications_enabled"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 833
    iget-object v3, p0, Lbxh;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "hands_free_read_notifications_enabled"

    if-nez v2, :cond_5

    :goto_3
    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 835
    invoke-static {}, Lhgn;->aOF()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbxh;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_1

    :cond_5
    move v0, v1

    .line 833
    goto :goto_3

    .line 836
    :cond_6
    const v0, 0x7f11017f

    if-ne v2, v0, :cond_0

    .line 839
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 840
    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/search/core/preferences/HandsFreeSettingsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 842
    invoke-virtual {p0, v0}, Lbxh;->startActivity(Landroid/content/Intent;)V

    .line 845
    invoke-virtual {p0}, Lbxh;->finish()V

    goto :goto_1
.end method

.method public nextSong(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 394
    iget-object v0, p0, Lbxh;->aNs:Lbxr;

    const/16 v1, 0x57

    invoke-virtual {v0, v1}, Lbxr;->ep(I)V

    .line 395
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 639
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbxh;->aNM:J

    .line 640
    invoke-virtual {p0}, Lbxh;->finish()V

    .line 641
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const v2, 0x7f110170

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 305
    invoke-super {p0, p1}, Lbyk;->onCreate(Landroid/os/Bundle;)V

    .line 307
    const v0, 0x7f040063

    invoke-virtual {p0, v0}, Lbxh;->setContentView(I)V

    .line 309
    new-instance v0, Lequ;

    invoke-direct {v0}, Lequ;-><init>()V

    iput-object v0, p0, Lbxh;->mSpeechLevelSource:Lequ;

    .line 310
    new-instance v0, Lelp;

    invoke-direct {v0, p0, v7}, Lelp;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lbxh;->mIntentStarter:Lelp;

    .line 311
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    iput-object v0, p0, Lbxh;->aNA:Lhym;

    .line 312
    new-instance v0, Ldmh;

    invoke-direct {v0, p0}, Ldmh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbxh;->mScreenStateHelper:Ldmh;

    .line 313
    iget-object v0, p0, Lbxh;->aNA:Lhym;

    invoke-virtual {v0}, Lhym;->aUi()Z

    move-result v0

    iput-boolean v0, p0, Lbxh;->aNU:Z

    .line 314
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJs()Lchr;

    move-result-object v0

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    iput-object v0, p0, Lbxh;->mPrefs:Landroid/content/SharedPreferences;

    .line 316
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbxh;->aNM:J

    .line 317
    iput v7, p0, Lbxh;->aNQ:I

    .line 318
    const v0, 0x7f11016c

    invoke-virtual {p0, v0}, Lbxh;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbxh;->aNt:Landroid/view/View;

    .line 319
    const v0, 0x7f11016a

    invoke-virtual {p0, v0}, Lbxh;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbxh;->aNx:Landroid/widget/TextView;

    .line 320
    const v0, 0x7f11016b

    invoke-virtual {p0, v0}, Lbxh;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbxh;->aNy:Landroid/widget/TextView;

    .line 321
    const v0, 0x7f110172

    invoke-virtual {p0, v0}, Lbxh;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbxh;->aNz:Landroid/widget/ImageView;

    .line 323
    new-instance v0, Ldsv;

    invoke-virtual {p0}, Lbxh;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0d00d2

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lbxh;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d00d3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p0}, Lbxh;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0097

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v0, v1, v3, v4}, Ldsv;-><init>(III)V

    .line 327
    iget-object v1, p0, Lbxh;->aNz:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 328
    new-instance v1, Ldtr;

    iget-object v3, p0, Lbxh;->mSpeechLevelSource:Lequ;

    invoke-static {v3}, Leef;->a(Lequ;)Ldtb;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Ldtr;-><init>(Ldsv;Ldtb;)V

    iput-object v1, p0, Lbxh;->aNF:Ldtr;

    .line 331
    const v0, 0x7f110171

    invoke-virtual {p0, v0}, Lbxh;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbxh;->aNE:Landroid/view/View;

    .line 333
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lbxh;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 334
    const v1, 0x10000006

    const-string v3, "HandsFreeOverlay"

    invoke-virtual {v0, v1, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lbxh;->aNB:Landroid/os/PowerManager$WakeLock;

    .line 336
    const/16 v1, 0xa

    const-string v3, "HandsFreeOverlay"

    invoke-virtual {v0, v1, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lbxh;->aNC:Landroid/os/PowerManager$WakeLock;

    .line 338
    invoke-virtual {p0}, Lbxh;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbxh;->aNG:[Ljava/lang/String;

    .line 340
    new-instance v0, Lbyi;

    invoke-direct {v0}, Lbyi;-><init>()V

    iput-object v0, p0, Lbxh;->mState:Lbyi;

    .line 342
    const v0, 0x7f050001

    const v1, 0x7f050002

    invoke-virtual {p0, v0, v1}, Lbxh;->overridePendingTransition(II)V

    .line 344
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbxh;->setVolumeControlStream(I)V

    .line 347
    if-eqz p1, :cond_0

    .line 348
    const-string v0, "start-reason"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbxh;->aNK:Ljava/lang/String;

    .line 349
    const-string v0, "start-time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lbxh;->aNL:J

    .line 352
    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "state-restored"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 353
    :cond_1
    invoke-virtual {p0}, Lbxh;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lbxh;->n(Landroid/content/Intent;)V

    .line 356
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_5

    .line 357
    new-instance v0, Lbxr;

    iget-object v3, p0, Lbxh;->mState:Lbyi;

    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lbxh;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/media/AudioManager;

    iget-object v5, p0, Lbxh;->aNt:Landroid/view/View;

    iget-object v6, p0, Lbxh;->aNx:Landroid/widget/TextView;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lbxr;-><init>(Landroid/app/Activity;ILbyi;Landroid/media/AudioManager;Landroid/view/View;Landroid/view/View;)V

    iput-object v0, p0, Lbxh;->aNs:Lbxr;

    .line 360
    iget-object v0, p0, Lbxh;->aNs:Lbxr;

    iget-object v0, v0, Lbxr;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 361
    iget-object v0, p0, Lbxh;->aNt:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 362
    iget-object v0, p0, Lbxh;->aNx:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 368
    :goto_0
    invoke-virtual {p0}, Lbxh;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 369
    iget-object v0, p0, Lbxh;->aNs:Lbxr;

    iget-object v0, v0, Lbxr;->aOd:Lejt;

    invoke-direct {p0, v0}, Lbxh;->a(Lejt;)V

    .line 370
    iget-object v0, p0, Lbxh;->aNs:Lbxr;

    const v1, 0x7f110174

    const v3, 0x7f110175

    invoke-virtual {v0, v1, v2, v3}, Lbxr;->o(III)V

    .line 384
    :goto_1
    return-void

    .line 364
    :cond_3
    iget-object v0, p0, Lbxh;->aNt:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 365
    iget-object v0, p0, Lbxh;->aNx:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 372
    :cond_4
    iget-object v0, p0, Lbxh;->aNt:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 373
    iget-object v0, p0, Lbxh;->aNs:Lbxr;

    iget-object v0, v0, Lbxr;->aOc:Lejt;

    invoke-direct {p0, v0}, Lbxh;->a(Lejt;)V

    .line 374
    iget-object v0, p0, Lbxh;->aNs:Lbxr;

    const v1, 0x7f11016d

    const v2, 0x7f11016e

    const v3, 0x7f11016f

    invoke-virtual {v0, v1, v2, v3}, Lbxr;->o(III)V

    goto :goto_1

    .line 377
    :cond_5
    new-instance v0, Lbxm;

    invoke-direct {v0, p0}, Lbxm;-><init>(Lbxh;)V

    invoke-direct {p0, v0}, Lbxh;->a(Lejt;)V

    goto :goto_1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 407
    invoke-super {p0, p1}, Lbyk;->onNewIntent(Landroid/content/Intent;)V

    .line 408
    invoke-direct {p0, p1}, Lbxh;->n(Landroid/content/Intent;)V

    .line 409
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 541
    invoke-super {p0}, Lbyk;->onPause()V

    .line 542
    iget-object v0, p0, Lbxh;->aNB:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543
    iget-object v0, p0, Lbxh;->aNB:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 545
    :cond_0
    iget-object v0, p0, Lbxh;->aNC:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 546
    iget-object v0, p0, Lbxh;->aNC:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 548
    :cond_1
    iget-boolean v0, p0, Lbxh;->aNv:Z

    if-eqz v0, :cond_2

    .line 549
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbxh;->aNw:Z

    .line 550
    iget-object v0, p0, Lbxh;->aNu:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 552
    :cond_2
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 510
    invoke-super {p0, p1}, Lbyk;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 512
    const-string v0, "menu-is-open"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lbxh;->aNv:Z

    .line 513
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 517
    invoke-super {p0}, Lbyk;->onResume()V

    .line 518
    iget-boolean v0, p0, Lbxh;->aNH:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbxh;->mScreenStateHelper:Ldmh;

    invoke-virtual {v0}, Ldmh;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    .line 519
    :goto_0
    invoke-virtual {p0}, Lbxh;->getIntent()Landroid/content/Intent;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lbxh;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "started-via-query"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v2

    .line 521
    :goto_1
    if-eqz v0, :cond_4

    if-eqz v3, :cond_4

    :goto_2
    iput-boolean v2, p0, Lbxh;->aND:Z

    .line 529
    invoke-virtual {p0}, Lbxh;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v2, 0x80000

    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    .line 532
    iget-object v0, p0, Lbxh;->aNB:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 533
    iput-boolean v1, p0, Lbxh;->aNw:Z

    .line 534
    iget-boolean v0, p0, Lbxh;->aNv:Z

    if-eqz v0, :cond_1

    .line 535
    invoke-virtual {p0}, Lbxh;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbxh;->showMenu(Landroid/view/View;)V

    .line 537
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 518
    goto :goto_0

    :cond_3
    move v3, v1

    .line 519
    goto :goto_1

    :cond_4
    move v2, v1

    .line 521
    goto :goto_2
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 413
    invoke-super {p0, p1}, Lbyk;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 414
    const-string v0, "menu-is-open"

    iget-boolean v1, p0, Lbxh;->aNv:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 415
    const-string v0, "start-reason"

    iget-object v1, p0, Lbxh;->aNK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    const-string v0, "start-time"

    iget-wide v2, p0, Lbxh;->aNL:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 418
    const-string v0, "state-restored"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 419
    return-void
.end method

.method public final onServiceConnected()V
    .locals 2

    .prologue
    .line 687
    invoke-super {p0}, Lbyk;->onServiceConnected()V

    .line 689
    iget-object v0, p0, Lbxh;->mState:Lbyi;

    invoke-virtual {v0, p0}, Lbyi;->a(Lbyj;)V

    .line 690
    iget-object v0, p0, Lbxh;->aOO:Lecq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lecq;->bq(Z)V

    .line 691
    return-void
.end method

.method protected onStart()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 437
    invoke-super {p0}, Lbyk;->onStart()V

    .line 443
    iget-object v1, p0, Lbxh;->aNW:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.android.apps.gmm.NAVIGATION_STATE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v2}, Lbxh;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 447
    invoke-static {p0}, Lcn;->a(Landroid/content/Context;)Lcn;

    move-result-object v1

    iget-object v2, p0, Lbxh;->aNX:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "exit-hands-free-overlay"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcn;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 450
    iget-object v1, p0, Lbxh;->aNK:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 452
    const-wide/32 v2, 0xea60

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lbxh;->aNL:J

    sub-long/2addr v4, v6

    sub-long/2addr v2, v4

    .line 459
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 461
    iget-object v1, p0, Lbxh;->aNx:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 462
    iget-object v1, p0, Lbxh;->aNy:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 463
    iget-object v1, p0, Lbxh;->aNK:Ljava/lang/String;

    const-string v4, "in-car-bluetooth"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 464
    iget-object v1, p0, Lbxh;->aNy:Landroid/widget/TextView;

    const v4, 0x7f0a09c7

    invoke-virtual {p0, v4}, Lbxh;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 472
    :cond_0
    :goto_0
    iget-object v1, p0, Lbxh;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lbxh;->aNY:Ljava/lang/Runnable;

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 477
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.search.core.action.HANDS_FREE_OVERLAY_STARTED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 478
    const-class v2, Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 479
    invoke-virtual {p0, v1}, Lbxh;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 482
    invoke-virtual {p0}, Lbxh;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/high16 v2, 0x80000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 484
    iget-object v1, p0, Lbxh;->mScreenStateHelper:Ldmh;

    invoke-virtual {v1}, Ldmh;->isScreenOn()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    iput-boolean v0, p0, Lbxh;->aNH:Z

    .line 488
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lbxh;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 489
    const v1, 0x30000006

    const-string v2, "HandsFreeOverlay"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 492
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 493
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 494
    return-void

    .line 466
    :cond_3
    iget-object v1, p0, Lbxh;->aNK:Ljava/lang/String;

    const-string v4, "driving-charging"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 467
    iget-object v1, p0, Lbxh;->aNy:Landroid/widget/TextView;

    const v4, 0x7f0a09c6

    invoke-virtual {p0, v4}, Lbxh;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 556
    invoke-super {p0}, Lbyk;->onStop()V

    .line 570
    invoke-virtual {p0}, Lbxh;->hasWindowFocus()Z

    move-result v0

    .line 571
    iget-boolean v1, p0, Lbxh;->aNJ:Z

    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    move v5, v4

    .line 589
    :goto_0
    if-eqz v5, :cond_0

    .line 590
    iput-boolean v3, p0, Lbxh;->aNJ:Z

    .line 593
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v6, p0, Lbxh;->aNM:J

    sub-long/2addr v0, v6

    iget-boolean v2, p0, Lbxh;->aNN:Z

    const-wide/16 v6, 0x1770

    cmp-long v0, v0, v6

    if-lez v0, :cond_4

    move v0, v4

    :goto_1
    if-eqz v0, :cond_5

    move v2, v3

    .line 596
    :goto_2
    if-ne v2, v4, :cond_2

    .line 597
    iget v0, p0, Lbxh;->aNQ:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbxh;->aNQ:I

    .line 599
    new-instance v6, Landroid/content/Intent;

    const-string v0, "com.google.android.search.core.action.OVERLAY_REVIVE"

    invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 600
    const-class v0, Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v6, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 601
    const-wide/16 v0, 0x1f4

    .line 609
    iget-boolean v7, p0, Lbxh;->aNN:Z

    if-eqz v7, :cond_1

    iget v7, p0, Lbxh;->aNQ:I

    if-ne v7, v4, :cond_1

    .line 611
    const-wide/16 v0, 0x7d0

    .line 613
    :cond_1
    iget-object v4, p0, Lbxh;->mHandler:Landroid/os/Handler;

    new-instance v7, Lbxn;

    invoke-direct {v7, p0, v6}, Lbxn;-><init>(Lbxh;Landroid/content/Intent;)V

    invoke-virtual {v4, v7, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 622
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.search.core.action.HANDS_FREE_OVERLAY_STOPPED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 623
    const-class v1, Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 624
    const-string v1, "com.google.android.search.core.extra.IS_LOSING_WINDOW_FOCUS"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 625
    const-string v1, "com.google.android.search.core.extra.HANDS_FREE_OVERLAY_ACTION_EXECUTION"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 627
    const-string v1, "com.google.android.search.core.extra.IS_CHANGING_CONFIGURATIONS"

    invoke-virtual {p0}, Lbxh;->isChangingConfigurations()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 629
    invoke-virtual {p0, v0}, Lbxh;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 631
    iget-object v0, p0, Lbxh;->aNW:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lbxh;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 632
    iput-boolean v3, p0, Lbxh;->aNR:Z

    .line 633
    return-void

    :cond_3
    move v5, v3

    .line 571
    goto :goto_0

    :cond_4
    move v0, v3

    .line 593
    goto :goto_1

    :cond_5
    iget-boolean v0, p0, Lbxh;->aNO:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x2

    move v2, v0

    goto :goto_2

    :cond_6
    iget-boolean v0, p0, Lbxh;->aNP:Z

    if-eqz v0, :cond_7

    const/4 v0, 0x3

    move v2, v0

    goto :goto_2

    :cond_7
    move v2, v4

    goto :goto_2
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 498
    invoke-super {p0, p1}, Lbyk;->onWindowFocusChanged(Z)V

    .line 505
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbxh;->aNJ:Z

    .line 506
    :cond_0
    return-void
.end method

.method public playPauseSong(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 402
    iget-object v0, p0, Lbxh;->aNs:Lbxr;

    const/16 v1, 0x55

    invoke-virtual {v0, v1}, Lbxr;->ep(I)V

    .line 403
    return-void
.end method

.method public previousSong(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lbxh;->aNs:Lbxr;

    const/16 v1, 0x58

    invoke-virtual {v0, v1}, Lbxr;->ep(I)V

    .line 399
    return-void
.end method

.method public showMenu(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 710
    iput-boolean v7, p0, Lbxh;->aNv:Z

    .line 711
    new-instance v0, Lbxo;

    const v1, 0x7f090151

    invoke-direct {v0, p0, p0, v1}, Lbxo;-><init>(Lbxh;Landroid/content/Context;I)V

    iput-object v0, p0, Lbxh;->aNu:Landroid/app/Dialog;

    .line 720
    iget-object v0, p0, Lbxh;->aNu:Landroid/app/Dialog;

    const v1, 0x7f040064

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    .line 722
    iget-object v0, p0, Lbxh;->aNu:Landroid/app/Dialog;

    const v1, 0x7f11017a

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 723
    iget-object v1, p0, Lbxh;->aNu:Landroid/app/Dialog;

    const v2, 0x7f11017d

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 725
    iget-object v2, p0, Lbxh;->aNu:Landroid/app/Dialog;

    const v3, 0x7f110177

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 727
    iget-object v3, p0, Lbxh;->aNu:Landroid/app/Dialog;

    const v4, 0x7f11017b

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 728
    iget-object v4, p0, Lbxh;->aNu:Landroid/app/Dialog;

    const v5, 0x7f11017e

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 730
    new-instance v5, Lbxp;

    invoke-direct {v5, p0}, Lbxp;-><init>(Lbxh;)V

    .line 740
    iget-object v6, p0, Lbxh;->aNu:Landroid/app/Dialog;

    invoke-virtual {v6, v7}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 741
    iget-object v6, p0, Lbxh;->aNu:Landroid/app/Dialog;

    const v7, 0x7f110179

    invoke-virtual {v6, v7}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 743
    iget-object v6, p0, Lbxh;->aNu:Landroid/app/Dialog;

    const v7, 0x7f11017c

    invoke-virtual {v6, v7}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 745
    iget-object v6, p0, Lbxh;->aNu:Landroid/app/Dialog;

    const v7, 0x7f11017f

    invoke-virtual {v6, v7}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 748
    iget-object v6, p0, Lbxh;->mPrefs:Landroid/content/SharedPreferences;

    const-string v7, "hands_free_auto_driving_car_mode_enabled"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 749
    const v6, 0x7f020153

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 750
    const v0, 0x7f0a06e1

    invoke-virtual {p0, v0}, Lbxh;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 755
    :goto_0
    iget-object v0, p0, Lbxh;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "hands_free_read_notifications_enabled"

    invoke-interface {v0, v3, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 756
    const v0, 0x7f020159

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 757
    const v0, 0x7f0a06e3

    invoke-virtual {p0, v0}, Lbxh;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 767
    :goto_1
    iget-object v0, p0, Lbxh;->aNu:Landroid/app/Dialog;

    const v1, 0x7f110176

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 776
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-lt v1, v3, :cond_4

    iget-object v1, p0, Lbxh;->aNA:Lhym;

    invoke-virtual {v1}, Lhym;->aUe()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lbxh;->aNA:Lhym;

    invoke-virtual {v1}, Lhym;->aTN()Z

    move-result v1

    if-nez v1, :cond_4

    .line 779
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 780
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 781
    iget-object v0, p0, Lbxh;->aNu:Landroid/app/Dialog;

    const v1, 0x7f110178

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 784
    iget-object v1, p0, Lbxh;->aNA:Lhym;

    invoke-virtual {v1}, Lhym;->aTO()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 786
    const v1, 0x7f020150

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 787
    const v1, 0x7f0a06e6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 797
    :goto_2
    iget-object v0, p0, Lbxh;->aNu:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 798
    const/16 v1, 0x30

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 799
    invoke-virtual {p0}, Lbxh;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 800
    invoke-virtual {p0}, Lbxh;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {p0}, Lbxh;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00d8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 804
    :cond_0
    invoke-virtual {p0}, Lbxh;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {p0}, Lbxh;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00d6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 808
    iget-object v0, p0, Lbxh;->aNu:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 809
    return-void

    .line 752
    :cond_1
    const v6, 0x7f020154

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 753
    const v0, 0x7f0a06e2

    invoke-virtual {p0, v0}, Lbxh;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 759
    :cond_2
    const v0, 0x7f02015a

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 760
    const v0, 0x7f0a06e4

    invoke-virtual {p0, v0}, Lbxh;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 789
    :cond_3
    const v1, 0x7f020151

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 790
    const v1, 0x7f0a06e5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 793
    :cond_4
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method public final tY()V
    .locals 2

    .prologue
    .line 695
    invoke-super {p0}, Lbyk;->tY()V

    .line 696
    iget-object v0, p0, Lbxh;->mState:Lbyi;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbyi;->a(Lbyj;)V

    .line 697
    return-void
.end method
