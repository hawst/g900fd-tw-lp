.class public final Lfrs;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bLI:Ljava/lang/String;

.field bPf:Ljava/lang/String;

.field public cBf:Z

.field public cBg:Z

.field public cBh:Z

.field cBi:Ljava/lang/String;

.field cBj:Ljava/lang/String;

.field private cBk:Ljcn;

.field cBl:Lapp;

.field ceJ:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mEntry:Lizj;

.field private mPhotoWithAttributionDecorator:Lgbd;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lizj;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lfrs;->mContext:Landroid/content/Context;

    .line 40
    iput-object p2, p0, Lfrs;->bLI:Ljava/lang/String;

    .line 41
    iput-object p3, p0, Lfrs;->mEntry:Lizj;

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Ljcn;Lgbd;)Lfrs;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lfrs;->cBk:Ljcn;

    .line 72
    iput-object p2, p0, Lfrs;->mPhotoWithAttributionDecorator:Lgbd;

    .line 73
    return-object p0
.end method

.method public final aDh()Lanh;
    .locals 4

    .prologue
    const/16 v0, 0x12

    const/4 v1, 0x0

    .line 93
    invoke-virtual {p0}, Lfrs;->aDi()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 94
    const/16 v0, 0x34

    invoke-virtual {p0, v0}, Lfrs;->jz(I)Lanh;

    move-result-object v0

    .line 102
    :goto_0
    return-object v0

    .line 97
    :cond_0
    iget-object v2, p0, Lfrs;->cBk:Ljcn;

    if-eqz v2, :cond_4

    .line 98
    iget-object v2, p0, Lfrs;->cBk:Ljcn;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljcn;->bgH()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Ljcn;->bgI()Z

    move-result v3

    if-nez v3, :cond_3

    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    const/16 v0, 0x21

    :cond_2
    invoke-virtual {p0, v0}, Lfrs;->jz(I)Lanh;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-virtual {v2}, Ljcn;->getHeight()I

    move-result v3

    invoke-virtual {v2}, Ljcn;->getWidth()I

    move-result v2

    if-lt v3, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    .line 102
    :cond_4
    invoke-virtual {p0, v0}, Lfrs;->jz(I)Lanh;

    move-result-object v0

    goto :goto_0
.end method

.method public final aDi()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 109
    iget-boolean v2, p0, Lfrs;->cBg:Z

    if-eqz v2, :cond_1

    .line 138
    :cond_0
    :goto_0
    return v0

    .line 112
    :cond_1
    iget-boolean v2, p0, Lfrs;->cBf:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lfrs;->cBh:Z

    if-nez v2, :cond_2

    move v0, v1

    .line 113
    goto :goto_0

    .line 117
    :cond_2
    iget-object v2, p0, Lfrs;->cBk:Ljcn;

    if-eqz v2, :cond_0

    .line 121
    iget-object v2, p0, Lfrs;->cBk:Ljcn;

    invoke-virtual {v2}, Ljcn;->bgH()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lfrs;->cBk:Ljcn;

    invoke-virtual {v2}, Ljcn;->bgI()Z

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    move v0, v1

    .line 122
    goto :goto_0

    .line 126
    :cond_4
    iget-object v2, p0, Lfrs;->mContext:Landroid/content/Context;

    invoke-static {v2}, Leot;->au(Landroid/content/Context;)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x3f000000    # 0.5f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 127
    iget-object v3, p0, Lfrs;->cBk:Ljcn;

    invoke-virtual {v3}, Ljcn;->getWidth()I

    move-result v3

    .line 128
    iget-object v4, p0, Lfrs;->cBk:Ljcn;

    invoke-virtual {v4}, Ljcn;->getHeight()I

    move-result v4

    .line 129
    if-le v3, v4, :cond_5

    .line 130
    if-gt v3, v2, :cond_0

    move v0, v1

    .line 131
    goto :goto_0

    .line 134
    :cond_5
    if-gt v4, v2, :cond_0

    move v0, v1

    .line 135
    goto :goto_0
.end method

.method public final jA(I)Z
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lfrs;->cBk:Ljcn;

    if-nez v0, :cond_0

    .line 195
    const/4 v0, 0x0

    .line 201
    :goto_0
    return v0

    .line 197
    :cond_0
    const/16 v0, 0x34

    if-ne p1, v0, :cond_1

    .line 199
    iget-boolean v0, p0, Lfrs;->cBh:Z

    goto :goto_0

    .line 201
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final jz(I)Lanh;
    .locals 6

    .prologue
    .line 146
    new-instance v0, Lanc;

    invoke-direct {v0}, Lanc;-><init>()V

    .line 147
    iget-object v1, p0, Lfrs;->bLI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lanc;->Z(Ljava/lang/String;)Lanc;

    .line 148
    iget-boolean v1, p0, Lfrs;->cBf:Z

    invoke-virtual {v0, v1}, Lanc;->aO(Z)Lanc;

    .line 149
    iget-object v1, p0, Lfrs;->cBi:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 150
    iget-object v1, p0, Lfrs;->cBi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lanc;->ac(Ljava/lang/String;)Lanc;

    .line 152
    :cond_0
    iget-object v1, p0, Lfrs;->cBj:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 153
    iget-object v1, p0, Lfrs;->cBj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lanc;->ab(Ljava/lang/String;)Lanc;

    .line 156
    :cond_1
    iget-object v1, p0, Lfrs;->cBk:Ljcn;

    if-eqz v1, :cond_6

    .line 158
    iget-object v1, p0, Lfrs;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v2, p0, Lfrs;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lfrs;->cBk:Ljcn;

    const v4, 0x7f0d0172

    const v5, 0x7f0d016d

    invoke-virtual {v1, v2, v3, v4, v5}, Lgbd;->b(Landroid/content/Context;Ljcn;II)Laoi;

    move-result-object v1

    .line 161
    invoke-virtual {v1}, Laoi;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lanc;->ad(Ljava/lang/String;)Lanc;

    .line 162
    iget-object v1, p0, Lfrs;->cBk:Ljcn;

    invoke-virtual {v1}, Ljcn;->bgH()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 163
    iget-object v1, p0, Lfrs;->cBk:Ljcn;

    invoke-virtual {v1}, Ljcn;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lanc;->ci(I)Lanc;

    .line 165
    :cond_2
    iget-object v1, p0, Lfrs;->cBk:Ljcn;

    invoke-virtual {v1}, Ljcn;->bgI()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 166
    iget-object v1, p0, Lfrs;->cBk:Ljcn;

    invoke-virtual {v1}, Ljcn;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Lanc;->cj(I)Lanc;

    .line 173
    :cond_3
    :goto_0
    iget-object v1, p0, Lfrs;->cBl:Lapp;

    if-eqz v1, :cond_4

    .line 174
    iget-object v1, p0, Lfrs;->cBl:Lapp;

    iput-object v1, v0, Lanc;->agd:Lapp;

    .line 177
    :cond_4
    iget-object v1, p0, Lfrs;->ceJ:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 178
    iget-object v1, p0, Lfrs;->ceJ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lanc;->aa(Ljava/lang/String;)Lanc;

    .line 181
    :cond_5
    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    .line 182
    iget-object v2, p0, Lfrs;->mEntry:Lizj;

    iput-object v2, v1, Lanh;->ahu:Lizj;

    .line 183
    iput-object v0, v1, Lanh;->aho:Lanc;

    .line 184
    invoke-virtual {v1, p1}, Lanh;->cm(I)Lanh;

    .line 185
    invoke-virtual {p0, p1}, Lfrs;->jA(I)Z

    move-result v0

    .line 186
    invoke-virtual {v1, v0}, Lanh;->aW(Z)Lanh;

    .line 187
    iget-object v2, v1, Lanh;->aho:Lanc;

    invoke-virtual {v2, v0}, Lanc;->aP(Z)Lanc;

    .line 189
    return-object v1

    .line 168
    :cond_6
    iget-object v1, p0, Lfrs;->bPf:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 169
    iget-object v1, p0, Lfrs;->bPf:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lanc;->ad(Ljava/lang/String;)Lanc;

    .line 170
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lanc;->aQ(Z)Lanc;

    goto :goto_0
.end method
