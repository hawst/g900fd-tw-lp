.class public final Ljuv;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eFM:[Ljuv;


# instance fields
.field private eFN:[F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1590
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1591
    sget-object v0, Ljsu;->eCC:[F

    iput-object v0, p0, Ljuv;->eFN:[F

    const/4 v0, 0x0

    iput-object v0, p0, Ljuv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljuv;->eCz:I

    .line 1592
    return-void
.end method

.method public static buQ()[Ljuv;
    .locals 2

    .prologue
    .line 1576
    sget-object v0, Ljuv;->eFM:[Ljuv;

    if-nez v0, :cond_1

    .line 1577
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 1579
    :try_start_0
    sget-object v0, Ljuv;->eFM:[Ljuv;

    if-nez v0, :cond_0

    .line 1580
    const/4 v0, 0x0

    new-array v0, v0, [Ljuv;

    sput-object v0, Ljuv;->eFM:[Ljuv;

    .line 1582
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1584
    :cond_1
    sget-object v0, Ljuv;->eFM:[Ljuv;

    return-object v0

    .line 1582
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1570
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljuv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xd

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljuv;->eFN:[F

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [F

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljuv;->eFN:[F

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljuv;->eFN:[F

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Ljuv;->eFN:[F

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v2

    div-int/lit8 v3, v0, 0x4

    iget-object v0, p0, Ljuv;->eFN:[F

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v3, v0

    new-array v3, v3, [F

    if-eqz v0, :cond_4

    iget-object v4, p0, Ljuv;->eFN:[F

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v4, v3

    if-ge v0, v4, :cond_6

    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljuv;->eFN:[F

    array-length v0, v0

    goto :goto_3

    :cond_6
    iput-object v3, p0, Ljuv;->eFN:[F

    invoke-virtual {p1, v2}, Ljsi;->rW(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_2
        0xd -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 1604
    iget-object v0, p0, Ljuv;->eFN:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljuv;->eFN:[F

    array-length v0, v0

    if-lez v0, :cond_0

    .line 1605
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljuv;->eFN:[F

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1606
    const/4 v1, 0x1

    iget-object v2, p0, Ljuv;->eFN:[F

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Ljsj;->a(IF)V

    .line 1605
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1609
    :cond_0
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1610
    return-void
.end method

.method protected final lF()I
    .locals 2

    .prologue
    .line 1614
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1615
    iget-object v1, p0, Ljuv;->eFN:[F

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljuv;->eFN:[F

    array-length v1, v1

    if-lez v1, :cond_0

    .line 1616
    iget-object v1, p0, Ljuv;->eFN:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 1617
    add-int/2addr v0, v1

    .line 1618
    iget-object v1, p0, Ljuv;->eFN:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1620
    :cond_0
    return v0
.end method
