.class public final Lbtm;
.super Lbts;
.source "PG"


# static fields
.field private static aJw:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbhi;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lbts;-><init>(Landroid/content/Context;Lbhi;Z)V

    .line 62
    return-void
.end method

.method public static Z(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 83
    sget-object v0, Lbtm;->aJw:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020009

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lbtl;->m(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lbtm;->aJw:Landroid/graphics/Bitmap;

    :cond_0
    sget-object v0, Lbtm;->aJw:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic a(Lbtm;Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;Lbtt;Landroid/graphics/Bitmap;I)V
    .locals 6

    .prologue
    .line 25
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-super/range {v0 .. v5}, Lbts;->a(Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;Lbtt;Landroid/graphics/Bitmap;I)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 71
    new-instance v0, Lbtn;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lbtn;-><init>(Lbtm;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v1, p0, Lbts;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lbtm;->Z(Landroid/content/Context;)Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lbtm;->a(Lbtt;)V

    .line 73
    return-void
.end method
