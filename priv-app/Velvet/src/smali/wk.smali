.class final Lwk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public Jl:Lahz;

.field public xr:Landroid/content/ComponentName;


# direct methods
.method constructor <init>(Landroid/content/ComponentName;Lahz;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lwk;->xr:Landroid/content/ComponentName;

    .line 80
    iput-object p2, p0, Lwk;->Jl:Lahz;

    .line 81
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 90
    check-cast p1, Lwk;

    .line 91
    iget-object v0, p1, Lwk;->xr:Landroid/content/ComponentName;

    iget-object v1, p0, Lwk;->xr:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lwk;->Jl:Lahz;

    iget-object v1, p0, Lwk;->Jl:Lahz;

    invoke-virtual {v0, v1}, Lahz;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lwk;->xr:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->hashCode()I

    move-result v0

    iget-object v1, p0, Lwk;->Jl:Lahz;

    invoke-virtual {v1}, Lahz;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
