.class public final Lcsd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lchm;
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mSearchSettings:Lcke;

.field private final mSettings:Lhym;


# direct methods
.method public constructor <init>(Lcke;Lhym;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcsd;->mSearchSettings:Lcke;

    .line 52
    iput-object p2, p0, Lcsd;->mSettings:Lhym;

    .line 53
    iput-object p3, p0, Lcsd;->mContext:Landroid/content/Context;

    .line 54
    return-void
.end method

.method private aa(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    .line 58
    invoke-static {}, Lenu;->auQ()V

    .line 60
    iget-object v0, p0, Lcsd;->mContext:Landroid/content/Context;

    const-string v1, "download"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    .line 64
    iget-object v1, p0, Lcsd;->mSearchSettings:Lcke;

    invoke-interface {v1}, Lcke;->OD()Lcsj;

    move-result-object v1

    .line 66
    if-eqz v1, :cond_0

    .line 68
    new-array v2, v3, [J

    invoke-virtual {v1}, Lcsj;->SF()J

    move-result-wide v4

    aput-wide v4, v2, v6

    invoke-virtual {v0, v2}, Landroid/app/DownloadManager;->remove([J)I

    .line 69
    const/16 v1, 0x121

    invoke-static {v1}, Lege;->ht(I)V

    .line 75
    :cond_0
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Landroid/app/DownloadManager$Request;

    invoke-direct {v2, v1}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v2, v6}, Landroid/app/DownloadManager$Request;->setVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Request;

    const/4 v1, 0x2

    invoke-virtual {v2, v1}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    invoke-virtual {v2, v3}, Landroid/app/DownloadManager$Request;->setAllowedOverMetered(Z)Landroid/app/DownloadManager$Request;

    invoke-virtual {v2, v3}, Landroid/app/DownloadManager$Request;->setAllowedOverRoaming(Z)Landroid/app/DownloadManager$Request;

    invoke-virtual {v0, v2}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v0

    .line 76
    const/16 v2, 0x11f

    invoke-static {v2}, Lege;->ht(I)V

    .line 78
    iget-object v2, p0, Lcsd;->mSearchSettings:Lcke;

    invoke-interface {v2, v0, v1, p1, p2}, Lcke;->a(JLjava/lang/String;Ljava/lang/String;)V

    .line 79
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljjf;Z)V
    .locals 2

    .prologue
    .line 94
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcsd;->c(Ljjf;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 97
    if-nez v0, :cond_1

    .line 104
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 101
    :cond_1
    :try_start_1
    invoke-virtual {p0, v0}, Lcsd;->iH(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    iget-object v1, p0, Lcsd;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcsd;->aa(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c(Ljjf;)Ljava/lang/String;
    .locals 8
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 133
    iget-object v3, p1, Ljjf;->eoH:[Ljjg;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    .line 134
    invoke-virtual {v0}, Ljjg;->getId()I

    move-result v5

    const/16 v6, 0xc0

    if-ne v5, v6, :cond_2

    .line 135
    invoke-virtual {v0}, Ljjg;->TV()Ljava/lang/String;

    move-result-object v0

    const-string v5, ","

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 137
    array-length v0, v5

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1

    move v0, v1

    .line 138
    :goto_1
    array-length v6, v5

    if-ge v0, v6, :cond_2

    .line 139
    aget-object v6, v5, v0

    iget-object v7, p0, Lcsd;->mSettings:Lhym;

    invoke-virtual {v7}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 140
    add-int/lit8 v0, v0, 0x1

    aget-object v0, v5, v0

    .line 148
    :goto_2
    return-object v0

    .line 138
    :cond_0
    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 144
    :cond_1
    const-string v0, "HotwordConfigController"

    const-string v5, "Invalid input: hotword_models_locations"

    new-array v6, v1, [Ljava/lang/Object;

    const/4 v7, 0x5

    invoke-static {v7, v0, v5, v6}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 133
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 148
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final synthetic call()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcsd;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->Oj()Ljjf;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcsd;->c(Ljjf;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcsd;->iH(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcsd;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcsd;->aa(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final iH(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcsd;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    .line 114
    iget-object v1, p0, Lcsd;->mSearchSettings:Lcke;

    invoke-interface {v1, v0}, Lcke;->hd(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    const/4 v0, 0x0

    .line 122
    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
