.class public final Lchr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final aXk:Lcyg;

.field private aXl:Z

.field aXm:Ljava/util/ArrayList;

.field public aXn:I

.field public final dK:Ljava/lang/Object;

.field private final mAsyncServices:Lema;

.field private final mContext:Landroid/content/Context;

.field public final mGelStartupPrefs:Ldku;

.field public mMainPrefs:Lcyg;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lema;)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lchr;->dK:Ljava/lang/Object;

    .line 69
    iput-object p1, p0, Lchr;->mContext:Landroid/content/Context;

    .line 70
    iput-object p2, p0, Lchr;->mAsyncServices:Lema;

    .line 74
    const-string v0, "StartupSettings"

    invoke-direct {p0, v0}, Lchr;->gq(Ljava/lang/String;)Lcyg;

    move-result-object v0

    iput-object v0, p0, Lchr;->aXk:Lcyg;

    .line 75
    new-instance v0, Ldku;

    invoke-direct {v0, p1}, Ldku;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lchr;->mGelStartupPrefs:Ldku;

    .line 76
    return-void
.end method

.method private Ku()V
    .locals 3

    .prologue
    .line 243
    iget-object v0, p0, Lchr;->mMainPrefs:Lcyg;

    if-nez v0, :cond_2

    .line 245
    const-string v0, "SearchSettings"

    invoke-direct {p0, v0}, Lchr;->gq(Ljava/lang/String;)Lcyg;

    move-result-object v0

    iput-object v0, p0, Lchr;->mMainPrefs:Lcyg;

    .line 247
    iget-object v0, p0, Lchr;->aXm:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 248
    iget-object v0, p0, Lchr;->aXm:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 249
    iget-object v2, p0, Lchr;->mMainPrefs:Lcyg;

    invoke-interface {v2, v0}, Lcyg;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    goto :goto_0

    .line 251
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lchr;->aXm:Ljava/util/ArrayList;

    .line 254
    :cond_1
    iget v0, p0, Lchr;->aXn:I

    if-eqz v0, :cond_2

    .line 255
    iget-object v0, p0, Lchr;->mMainPrefs:Lcyg;

    invoke-interface {v0}, Lcyg;->EI()V

    .line 258
    :cond_2
    return-void
.end method

.method private Kv()V
    .locals 6

    .prologue
    const/16 v5, 0xa

    .line 279
    iget-boolean v0, p0, Lchr;->aXl:Z

    if-nez v0, :cond_1

    .line 286
    iget-object v0, p0, Lchr;->aXk:Lcyg;

    const-string v1, "settings_version"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Lcyg;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 291
    if-ge v4, v5, :cond_0

    .line 293
    invoke-direct {p0}, Lchr;->Ku()V

    .line 294
    iget-object v0, p0, Lchr;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lchr;->aXk:Lcyg;

    iget-object v2, p0, Lchr;->mMainPrefs:Lcyg;

    const-string v3, "settings_version"

    invoke-static/range {v0 .. v5}, Lchs;->a(Landroid/content/Context;Lcyg;Lcyg;Ljava/lang/String;II)V

    .line 298
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lchr;->aXl:Z

    .line 300
    :cond_1
    return-void
.end method

.method public static a(Landroid/preference/PreferenceManager;)V
    .locals 1

    .prologue
    .line 183
    const-string v0, "SearchSettings"

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 184
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/preference/PreferenceManager;->setSharedPreferencesMode(I)V

    .line 185
    return-void
.end method

.method private gq(Ljava/lang/String;)Lcyg;
    .locals 4

    .prologue
    .line 261
    iget-object v0, p0, Lchr;->mContext:Landroid/content/Context;

    const-string v1, "shared_prefs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 262
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".bin"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lchr;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->aus()Leqo;

    move-result-object v0

    invoke-static {v1, v0}, Lcyi;->a(Ljava/io/File;Ljava/util/concurrent/Executor;)Lcyi;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final Ks()Lcyg;
    .locals 2

    .prologue
    .line 84
    iget-object v1, p0, Lchr;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 85
    :try_start_0
    iget-boolean v0, p0, Lchr;->aXl:Z

    if-nez v0, :cond_0

    .line 86
    invoke-direct {p0}, Lchr;->Kv()V

    .line 88
    :cond_0
    iget-object v0, p0, Lchr;->aXk:Lcyg;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Kt()Lcyg;
    .locals 2

    .prologue
    .line 158
    iget-object v1, p0, Lchr;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 159
    :try_start_0
    iget-object v0, p0, Lchr;->mMainPrefs:Lcyg;

    if-nez v0, :cond_1

    .line 160
    iget-boolean v0, p0, Lchr;->aXl:Z

    if-nez v0, :cond_0

    .line 161
    invoke-direct {p0}, Lchr;->Kv()V

    .line 163
    :cond_0
    invoke-direct {p0}, Lchr;->Ku()V

    .line 165
    :cond_1
    iget-object v0, p0, Lchr;->mMainPrefs:Lcyg;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
