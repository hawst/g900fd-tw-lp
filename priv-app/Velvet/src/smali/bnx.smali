.class final Lbnx;
.super Lbit;

# interfaces
.implements Lbss;


# instance fields
.field private final aAw:Lcom/google/android/gms/common/api/Status;

.field private final aGv:Landroid/os/ParcelFileDescriptor;


# direct methods
.method public constructor <init>(Lbnv;Lbhh;Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbit;-><init>(Lbir;Ljava/lang/Object;)V

    iput-object p3, p0, Lbnx;->aAw:Lcom/google/android/gms/common/api/Status;

    iput-object p4, p0, Lbnx;->aGv:Landroid/os/ParcelFileDescriptor;

    return-void
.end method


# virtual methods
.method protected final synthetic an(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lbhh;

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Lbhh;->al(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;
    .locals 1

    iget-object v0, p0, Lbnx;->aGv:Landroid/os/ParcelFileDescriptor;

    return-object v0
.end method

.method public final release()V
    .locals 1

    iget-object v0, p0, Lbnx;->aGv:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbnx;->aGv:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final wZ()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lbnx;->aAw:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method protected final yU()V
    .locals 0

    invoke-virtual {p0}, Lbnx;->release()V

    return-void
.end method
