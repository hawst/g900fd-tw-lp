.class public final Lhvf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lchm;


# instance fields
.field private final RO:Ljava/lang/String;

.field private final drO:Lhvh;

.field private final drP:Ljava/util/Map;

.field private final drQ:I

.field private drR:Ljava/lang/Boolean;

.field private drS:Z

.field private final mExecutor:Ljava/util/concurrent/Executor;

.field private mSettings:Lhym;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/util/concurrent/Executor;Lhym;Ljava/util/Map;Lhvh;)V
    .locals 0

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    iput-object p2, p0, Lhvf;->RO:Ljava/lang/String;

    .line 134
    iput p3, p0, Lhvf;->drQ:I

    .line 135
    iput-object p4, p0, Lhvf;->mExecutor:Ljava/util/concurrent/Executor;

    .line 136
    iput-object p5, p0, Lhvf;->mSettings:Lhym;

    .line 137
    iput-object p6, p0, Lhvf;->drP:Ljava/util/Map;

    .line 138
    iput-object p7, p0, Lhvf;->drO:Lhvh;

    .line 139
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lhym;I)V
    .locals 8

    .prologue
    .line 113
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3}, Lhym;->getHotwordConfigMap()Ljava/util/Map;

    move-result-object v6

    const-string v0, "input_method"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    new-instance v7, Lhvh;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v7, v0, v1}, Lhvh;-><init>(Landroid/view/inputmethod/InputMethodManager;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move v3, p4

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v7}, Lhvf;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/util/concurrent/Executor;Lhym;Ljava/util/Map;Lhvh;)V

    .line 115
    return-void
.end method


# virtual methods
.method public final a(Lhym;)V
    .locals 4

    .prologue
    .line 289
    invoke-virtual {p1}, Lhym;->aUu()Z

    move-result v0

    if-nez v0, :cond_0

    .line 292
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lhym;->gP(Z)V

    .line 293
    iget-object v0, p0, Lhvf;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhvf;->k(Ljze;)V

    .line 296
    iget-object v0, p0, Lhvf;->drO:Lhvh;

    invoke-virtual {v0}, Lhvh;->aSA()Ljava/util/List;

    move-result-object v0

    .line 298
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 300
    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 302
    invoke-virtual {p1}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v1

    .line 303
    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 304
    invoke-virtual {p1}, Lhym;->aTI()Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 305
    invoke-static {v0}, Lgnq;->an(Ljava/util/List;)V

    .line 310
    iget-object v2, p0, Lhvf;->drP:Ljava/util/Map;

    invoke-static {v1, v0, v2}, Liaw;->b(Ljava/lang/String;Ljava/util/List;Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    .line 314
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    iget v3, p0, Lhvf;->drQ:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 315
    const/4 v3, 0x0

    invoke-interface {v0, v3, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 319
    invoke-virtual {p1}, Lhym;->aTH()Z

    move-result v2

    .line 320
    invoke-virtual {p1, v1, v0, v2}, Lhym;->b(Ljava/lang/String;Ljava/util/List;Z)V

    .line 324
    :cond_0
    return-void
.end method

.method public final a(Ljjf;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 341
    iget-object v2, p1, Ljjf;->eoH:[Ljjg;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 342
    invoke-virtual {v4}, Ljjg;->getId()I

    move-result v5

    const/16 v6, 0x20

    if-ne v5, v6, :cond_3

    .line 343
    invoke-virtual {v4}, Ljjg;->TO()Z

    move-result v0

    .line 350
    iget-object v2, p0, Lhvf;->drR:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhvf;->drR:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eq v2, v0, :cond_1

    .line 352
    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lhvf;->drR:Ljava/lang/Boolean;

    .line 353
    if-eqz v0, :cond_2

    .line 355
    iget-object v0, p0, Lhvf;->mSettings:Lhym;

    invoke-virtual {p0, v0}, Lhvf;->a(Lhym;)V

    .line 356
    invoke-virtual {p0}, Lhvf;->aSz()V

    .line 367
    :cond_1
    :goto_1
    return-void

    .line 360
    :cond_2
    iget-object v0, p0, Lhvf;->mSettings:Lhym;

    invoke-virtual {v0, v1}, Lhym;->gP(Z)V

    .line 361
    iget-object v0, p0, Lhvf;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lhvf;->a(Ljze;Z)V

    goto :goto_1

    .line 341
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a(Ljze;Z)V
    .locals 3

    .prologue
    .line 159
    monitor-enter p0

    .line 160
    :try_start_0
    iget-boolean v0, p0, Lhvf;->drS:Z

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 161
    monitor-exit p0

    .line 173
    :goto_0
    return-void

    .line 163
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhvf;->drS:Z

    .line 164
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 165
    new-instance v0, Lhvg;

    const-string v1, "Update Voice IME subtypes"

    const/4 v2, 0x0

    new-array v2, v2, [I

    invoke-direct {v0, p0, v1, v2, p1}, Lhvg;-><init>(Lhvf;Ljava/lang/String;[ILjze;)V

    .line 172
    iget-object v1, p0, Lhvf;->mExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final aSz()V
    .locals 12

    .prologue
    .line 219
    :try_start_0
    iget-object v0, p0, Lhvf;->drO:Lhvh;

    iget-object v0, v0, Lhvh;->xe:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->getInputMethodList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    .line 220
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lhvf;->RO:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 223
    iget-object v8, p0, Lhvf;->drO:Lhvh;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v9

    const/4 v0, 0x1

    new-array v10, v0, [Landroid/view/inputmethod/InputMethodSubtype;

    const/4 v11, 0x0

    new-instance v0, Landroid/view/inputmethod/InputMethodSubtype;

    const v1, 0x7f0a08e7

    const/4 v2, 0x0

    const-string v3, ""

    const-string v4, "voice"

    const-string v5, ""

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Landroid/view/inputmethod/InputMethodSubtype;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    aput-object v0, v10, v11

    invoke-virtual {v8, v9, v10}, Lhvh;->setAdditionalInputMethodSubtypes(Ljava/lang/String;[Landroid/view/inputmethod/InputMethodSubtype;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 231
    :cond_1
    :goto_0
    return-void

    .line 228
    :catch_0
    move-exception v0

    .line 229
    const-string v1, "VoiceImeSubtypeUpdater"

    const-string v2, "Error while removing subtypes: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final k(Ljze;)V
    .locals 12

    .prologue
    .line 187
    :try_start_0
    invoke-static {p1}, Lgnq;->e(Ljze;)Ljava/util/ArrayList;

    move-result-object v1

    .line 190
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v9

    .line 192
    invoke-static {p1}, Lgnq;->f(Ljze;)Ljava/util/ArrayList;

    move-result-object v10

    .line 194
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljzh;

    move-object v4, v0

    .line 195
    invoke-virtual {v4}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 197
    const-string v5, "voice"

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v2, 0x78

    invoke-direct {v6, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "UntranslatableReplacementStringInSubtypeName="

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljzh;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez v1, :cond_0

    const-string v1, ",requireNetworkConnectivity"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v1, ",bcp47="

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Landroid/view/inputmethod/InputMethodSubtype;

    const v2, 0x7f0a08ff

    const/4 v3, 0x0

    invoke-virtual {v4}, Ljzh;->bwR()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Landroid/view/inputmethod/InputMethodSubtype;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 207
    :catch_0
    move-exception v1

    .line 208
    const-string v2, "VoiceImeSubtypeUpdater"

    const-string v3, "Error updating IME subtype: "

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 210
    :cond_1
    :goto_1
    return-void

    .line 200
    :cond_2
    :try_start_1
    iget-object v1, p0, Lhvf;->drO:Lhvh;

    iget-object v1, v1, Lhvh;->xe:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->getInputMethodList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodInfo;

    .line 201
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lhvf;->RO:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 202
    iget-object v2, p0, Lhvf;->drO:Lhvh;

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Landroid/view/inputmethod/InputMethodSubtype;

    invoke-interface {v9, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/view/inputmethod/InputMethodSubtype;

    invoke-virtual {v2, v3, v1}, Lhvh;->setAdditionalInputMethodSubtypes(Ljava/lang/String;[Landroid/view/inputmethod/InputMethodSubtype;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
