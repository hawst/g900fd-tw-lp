.class public Lidh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldvn;


# instance fields
.field protected final mContactLabelConverter:Ldyv;

.field private final mGsaConfigFlags:Lchk;

.field private final mLoginHelper:Lcrh;

.field private final mSearchSettings:Lcke;


# direct methods
.method public constructor <init>(Lchk;Lcke;Lcrh;Ldyv;)V
    .locals 0
    .param p1    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcke;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lcrh;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Ldyv;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lidh;->mGsaConfigFlags:Lchk;

    .line 67
    iput-object p2, p0, Lidh;->mSearchSettings:Lcke;

    .line 68
    iput-object p3, p0, Lidh;->mLoginHelper:Lcrh;

    .line 69
    iput-object p4, p0, Lidh;->mContactLabelConverter:Ldyv;

    .line 70
    return-void
.end method

.method public static a(Lchk;Lcke;Lcrh;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 417
    invoke-virtual {p0}, Lchk;->FI()I

    move-result v3

    .line 418
    invoke-interface {p1}, Lcke;->CS()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p2}, Lcrh;->Su()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v0

    .line 422
    :goto_0
    if-eqz v2, :cond_2

    if-lez v3, :cond_2

    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 418
    goto :goto_0

    :cond_2
    move v0, v1

    .line 422
    goto :goto_1
.end method


# virtual methods
.method public synthetic a(Lcom/google/android/search/shared/actions/AddEventAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lidh;->d(Lcom/google/android/search/shared/actions/AddEventAction;)Ljkt;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/AddRelationshipAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method public synthetic a(Lcom/google/android/search/shared/actions/AgendaAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lidh;->b(Lcom/google/android/search/shared/actions/AgendaAction;)Ljkt;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/ButtonAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method public synthetic a(Lcom/google/android/search/shared/actions/ContactOptInAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lidh;->b(Lcom/google/android/search/shared/actions/ContactOptInAction;)Ljkt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/EmailAction;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 54
    new-instance v0, Ljlh;

    invoke-direct {v0}, Ljlh;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/EmailAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    new-array v2, v2, [Ljoq;

    const/4 v3, 0x0

    iget-object v4, p0, Lidh;->mContactLabelConverter:Ldyv;

    invoke-virtual {p0}, Lidh;->aUT()Z

    move-result v5

    invoke-virtual {v1, v4, v5}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->a(Ldyv;Z)Ljoq;

    move-result-object v1

    aput-object v1, v2, v3

    iput-object v2, v0, Ljlh;->ert:[Ljoq;

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/EmailAction;->getSubject()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/EmailAction;->getBody()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0, v1}, Ljlh;->wE(Ljava/lang/String;)Ljlh;

    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0, v2}, Ljlh;->wF(Ljava/lang/String;)Ljlh;

    :cond_2
    new-instance v1, Ljkt;

    invoke-direct {v1}, Ljkt;-><init>()V

    sget-object v2, Ljlh;->ers:Ljsm;

    invoke-virtual {v1, v2, v0}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    return-object v1
.end method

.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/HelpAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/LocalResultsAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/MediaControlAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/MessageSearchAction;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 54
    new-instance v0, Ljkt;

    invoke-direct {v0}, Ljkt;-><init>()V

    sget-object v1, Ljmb;->eti:Ljsm;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/MessageSearchAction;->ahF()Ljmb;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    return-object v0
.end method

.method public synthetic a(Lcom/google/android/search/shared/actions/OpenUrlAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lidh;->b(Lcom/google/android/search/shared/actions/OpenUrlAction;)Ljkt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/PhoneCallAction;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 54
    new-instance v0, Ljmg;

    invoke-direct {v0}, Ljmg;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PhoneCallAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lidh;->mContactLabelConverter:Ldyv;

    invoke-virtual {p0}, Lidh;->aUT()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->a(Ldyv;Z)Ljoq;

    move-result-object v1

    iput-object v1, v0, Ljmg;->etx:Ljoq;

    :cond_0
    new-instance v1, Ljkt;

    invoke-direct {v1}, Ljkt;-><init>()V

    sget-object v2, Ljmg;->etv:Ljsm;

    invoke-virtual {v1, v2, v0}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    return-object v1
.end method

.method public synthetic a(Lcom/google/android/search/shared/actions/PlayMediaAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lidh;->c(Lcom/google/android/search/shared/actions/PlayMediaAction;)Ljkt;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/PuntAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/ReadNotificationAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/RemoveRelationshipAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SelfNoteAction;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 54
    new-instance v0, Ljmp;

    invoke-direct {v0}, Ljmp;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SelfNoteAction;->aik()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljmp;->xh(Ljava/lang/String;)Ljmp;

    new-instance v1, Ljkt;

    invoke-direct {v1}, Ljkt;-><init>()V

    sget-object v2, Ljmp;->eui:Ljsm;

    invoke-virtual {v1, v2, v0}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    return-object v1
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SetAlarmAction;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 54
    new-instance v0, Ljmq;

    invoke-direct {v0}, Ljmq;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SetAlarmAction;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljmq;->xi(Ljava/lang/String;)Ljmq;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SetAlarmAction;->sZ()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljkh;

    invoke-direct {v1}, Ljkh;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SetAlarmAction;->getHour()I

    move-result v2

    invoke-virtual {v1, v2}, Ljkh;->qt(I)Ljkh;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SetAlarmAction;->getMinute()I

    move-result v2

    invoke-virtual {v1, v2}, Ljkh;->qu(I)Ljkh;

    move-result-object v1

    iput-object v1, v0, Ljmq;->erP:Ljkh;

    :cond_0
    new-instance v1, Ljkt;

    invoke-direct {v1}, Ljkt;-><init>()V

    sget-object v2, Ljmq;->eul:Ljsm;

    invoke-virtual {v1, v2, v0}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    return-object v1
.end method

.method public synthetic a(Lcom/google/android/search/shared/actions/SetReminderAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lidh;->d(Lcom/google/android/search/shared/actions/SetReminderAction;)Ljkt;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/google/android/search/shared/actions/SetTimerAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lidh;->c(Lcom/google/android/search/shared/actions/SetTimerAction;)Ljkt;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/google/android/search/shared/actions/ShowContactInformationAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lidh;->b(Lcom/google/android/search/shared/actions/ShowContactInformationAction;)Ljkt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/SmsAction;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 54
    new-instance v0, Ljmo;

    invoke-direct {v0}, Ljmo;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SmsAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    new-array v2, v2, [Ljoq;

    const/4 v3, 0x0

    iget-object v4, p0, Lidh;->mContactLabelConverter:Ldyv;

    invoke-virtual {p0}, Lidh;->aUT()Z

    move-result v5

    invoke-virtual {v1, v4, v5}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->a(Ldyv;Z)Ljoq;

    move-result-object v1

    aput-object v1, v2, v3

    iput-object v2, v0, Ljmo;->euf:[Ljoq;

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SmsAction;->aiL()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SmsAction;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljmo;->xg(Ljava/lang/String;)Ljmo;

    :cond_1
    new-instance v1, Ljkt;

    invoke-direct {v1}, Ljkt;-><init>()V

    sget-object v2, Ljmo;->eud:Ljsm;

    invoke-virtual {v1, v2, v0}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    return-object v1
.end method

.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/SocialUpdateAction;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/ModularAction;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 54
    new-instance v0, Ljkt;

    invoke-direct {v0}, Ljkt;-><init>()V

    sget-object v1, Ljrc;->ezW:Ljsm;

    invoke-virtual {p0}, Lidh;->aUT()Z

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3, v4, v4}, Lcom/google/android/search/shared/actions/modular/ModularAction;->b(ZZZZ)Ljrc;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    new-instance v1, Ljku;

    invoke-direct {v1}, Ljku;-><init>()V

    iput-object v1, v0, Ljkt;->eqn:Ljku;

    iget-object v1, v0, Ljkt;->eqn:Ljku;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->agc()I

    move-result v2

    invoke-virtual {v1, v2}, Ljku;->qx(I)Ljku;

    return-object v0
.end method

.method public final a(Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;Ljku;Ljlf;Ljlm;)Ljkt;
    .locals 4
    .param p2    # Lcom/google/android/search/shared/actions/utils/CardDecision;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljku;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljlf;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljlm;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 97
    invoke-interface {p1, p0}, Lcom/google/android/search/shared/actions/VoiceAction;->a(Ldvn;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljkt;

    .line 101
    if-nez v0, :cond_0

    .line 102
    const/4 v0, 0x0

    .line 168
    :goto_0
    return-object v0

    .line 104
    :cond_0
    if-eqz p3, :cond_5

    .line 105
    iget-object v1, v0, Ljkt;->eqn:Ljku;

    if-nez v1, :cond_1

    new-instance v1, Ljku;

    invoke-direct {v1}, Ljku;-><init>()V

    iput-object v1, v0, Ljkt;->eqn:Ljku;

    :cond_1
    iget-object v1, v0, Ljkt;->eqn:Ljku;

    .line 106
    invoke-virtual {p3}, Ljku;->boz()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 107
    invoke-virtual {p3}, Ljku;->boy()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljku;->as([B)Ljku;

    .line 109
    :cond_2
    invoke-virtual {p3}, Ljku;->bbg()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 110
    invoke-virtual {p3}, Ljku;->oY()I

    move-result v2

    invoke-virtual {v1, v2}, Ljku;->qx(I)Ljku;

    .line 112
    :cond_3
    invoke-virtual {p3}, Ljku;->box()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 113
    invoke-virtual {p3}, Ljku;->bow()I

    move-result v2

    invoke-virtual {v1, v2}, Ljku;->qy(I)Ljku;

    .line 115
    :cond_4
    invoke-virtual {p3}, Ljku;->boB()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 116
    invoke-virtual {p3}, Ljku;->aIW()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljku;->ws(Ljava/lang/String;)Ljku;

    .line 119
    :cond_5
    if-eqz p4, :cond_6

    .line 120
    sget-object v1, Ljlf;->erl:Ljsm;

    invoke-virtual {v0, v1, p4}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 122
    :cond_6
    if-eqz p5, :cond_7

    .line 123
    iput-object p5, v0, Ljkt;->eqo:Ljlm;

    .line 126
    :cond_7
    if-eqz p2, :cond_8

    .line 128
    iget-object v1, v0, Ljkt;->eqo:Ljlm;

    if-eqz v1, :cond_9

    .line 129
    iget-object v1, v0, Ljkt;->eqo:Ljlm;

    .line 135
    :goto_1
    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alk()I

    move-result v2

    .line 136
    if-eqz v2, :cond_a

    .line 137
    invoke-virtual {v1, v2}, Ljlm;->qH(I)Ljlm;

    .line 142
    :goto_2
    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alm()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 143
    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->all()I

    move-result v2

    invoke-virtual {v1, v2}, Ljlm;->qI(I)Ljlm;

    .line 157
    :goto_3
    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->aln()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljlm;->iz(Z)Ljlm;

    .line 158
    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alg()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 159
    invoke-virtual {v1, v3}, Ljlm;->ix(Z)Ljlm;

    .line 166
    :cond_8
    :goto_4
    invoke-virtual {v0, v3}, Ljkt;->is(Z)Ljkt;

    goto :goto_0

    .line 131
    :cond_9
    new-instance v1, Ljlm;

    invoke-direct {v1}, Ljlm;-><init>()V

    .line 132
    iput-object v1, v0, Ljkt;->eqo:Ljlm;

    goto :goto_1

    .line 139
    :cond_a
    invoke-virtual {v1}, Ljlm;->bpj()Ljlm;

    goto :goto_2

    .line 145
    :cond_b
    invoke-virtual {v1}, Ljlm;->bpk()Ljlm;

    goto :goto_3

    .line 161
    :cond_c
    invoke-virtual {v1}, Ljlm;->bpc()Ljlm;

    goto :goto_4
.end method

.method public aUT()Z
    .locals 3

    .prologue
    .line 406
    iget-object v0, p0, Lidh;->mGsaConfigFlags:Lchk;

    iget-object v1, p0, Lidh;->mSearchSettings:Lcke;

    iget-object v2, p0, Lidh;->mLoginHelper:Lcrh;

    invoke-static {v0, v1, v2}, Lidh;->a(Lchk;Lcke;Lcrh;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic b(Lcom/google/android/search/shared/actions/errors/SearchError;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Lcom/google/android/search/shared/actions/AgendaAction;)Ljkt;
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Lcom/google/android/search/shared/actions/ContactOptInAction;)Ljkt;
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Lcom/google/android/search/shared/actions/OpenUrlAction;)Ljkt;
    .locals 1

    .prologue
    .line 283
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Lcom/google/android/search/shared/actions/ShowContactInformationAction;)Ljkt;
    .locals 1

    .prologue
    .line 369
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(Lcom/google/android/search/shared/actions/PlayMediaAction;)Ljkt;
    .locals 1

    .prologue
    .line 327
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(Lcom/google/android/search/shared/actions/SetTimerAction;)Ljkt;
    .locals 1

    .prologue
    .line 355
    const/4 v0, 0x0

    return-object v0
.end method

.method public d(Lcom/google/android/search/shared/actions/AddEventAction;)Ljkt;
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x0

    return-object v0
.end method

.method public d(Lcom/google/android/search/shared/actions/SetReminderAction;)Ljkt;
    .locals 3

    .prologue
    .line 360
    new-instance v0, Ljkt;

    invoke-direct {v0}, Ljkt;-><init>()V

    .line 361
    sget-object v1, Ljkw;->eqw:Ljsm;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiC()Ljkw;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 363
    return-object v0
.end method
