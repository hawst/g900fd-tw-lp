.class public Ladh;
.super Lwq;
.source "PG"


# instance fields
.field Jr:Landroid/content/Intent$ShortcutIconResource;

.field ST:Z

.field SU:Z

.field public SV:Z

.field private SW:I

.field SX:Landroid/content/Intent;

.field private Sb:Landroid/graphics/Bitmap;

.field public flags:I

.field public intent:Landroid/content/Intent;

.field status:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 120
    invoke-direct {p0}, Lwq;-><init>()V

    .line 94
    iput-boolean v0, p0, Ladh;->SV:Z

    .line 111
    iput v0, p0, Ladh;->flags:I

    .line 121
    const/4 v0, 0x1

    iput v0, p0, Ladh;->Jz:I

    .line 122
    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/Bitmap;Lahz;)V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Ladh;-><init>()V

    .line 131
    iput-object p1, p0, Ladh;->intent:Landroid/content/Intent;

    .line 132
    iput-object p2, p0, Ladh;->title:Ljava/lang/CharSequence;

    .line 133
    iput-object p3, p0, Ladh;->Jk:Ljava/lang/CharSequence;

    .line 134
    iput-object p4, p0, Ladh;->Sb:Landroid/graphics/Bitmap;

    .line 135
    iput-object p5, p0, Ladh;->Jl:Lahz;

    .line 136
    return-void
.end method

.method public constructor <init>(Lrr;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 157
    invoke-direct {p0, p1}, Lwq;-><init>(Lwq;)V

    .line 94
    iput-boolean v2, p0, Ladh;->SV:Z

    .line 111
    iput v2, p0, Ladh;->flags:I

    .line 158
    iget-object v0, p1, Lrr;->title:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ladh;->title:Ljava/lang/CharSequence;

    .line 159
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p1, Lrr;->intent:Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Ladh;->intent:Landroid/content/Intent;

    .line 160
    iput-boolean v2, p0, Ladh;->ST:Z

    .line 161
    iget v0, p1, Lrr;->flags:I

    iput v0, p0, Ladh;->flags:I

    .line 162
    iget-wide v0, p1, Lrr;->firstInstallTime:J

    .line 163
    return-void
.end method


# virtual methods
.method final a(Landroid/content/Context;Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 183
    invoke-super {p0, p1, p2}, Lwq;->a(Landroid/content/Context;Landroid/content/ContentValues;)V

    .line 185
    iget-object v0, p0, Ladh;->title:Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ladh;->title:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 186
    :goto_0
    const-string v2, "title"

    invoke-virtual {p2, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Ladh;->SX:Landroid/content/Intent;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ladh;->SX:Landroid/content/Intent;

    invoke-virtual {v0, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v1

    .line 190
    :cond_0
    :goto_1
    const-string v0, "intent"

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget-boolean v0, p0, Ladh;->ST:Z

    if-eqz v0, :cond_4

    .line 193
    const-string v0, "iconType"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 195
    iget-object v0, p0, Ladh;->Sb:Landroid/graphics/Bitmap;

    invoke-static {p2, v0}, Ladh;->a(Landroid/content/ContentValues;Landroid/graphics/Bitmap;)V

    .line 209
    :cond_1
    :goto_2
    return-void

    :cond_2
    move-object v0, v1

    .line 185
    goto :goto_0

    .line 188
    :cond_3
    iget-object v0, p0, Ladh;->intent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ladh;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 197
    :cond_4
    iget-boolean v0, p0, Ladh;->SU:Z

    if-nez v0, :cond_5

    .line 198
    iget-object v0, p0, Ladh;->Sb:Landroid/graphics/Bitmap;

    invoke-static {p2, v0}, Ladh;->a(Landroid/content/ContentValues;Landroid/graphics/Bitmap;)V

    .line 200
    :cond_5
    const-string v0, "iconType"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 202
    iget-object v0, p0, Ladh;->Jr:Landroid/content/Intent$ShortcutIconResource;

    if-eqz v0, :cond_1

    .line 203
    const-string v0, "iconPackage"

    iget-object v1, p0, Ladh;->Jr:Landroid/content/Intent$ShortcutIconResource;

    iget-object v1, v1, Landroid/content/Intent$ShortcutIconResource;->packageName:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const-string v0, "iconResource"

    iget-object v1, p0, Ladh;->Jr:Landroid/content/Intent$ShortcutIconResource;

    iget-object v1, v1, Landroid/content/Intent$ShortcutIconResource;->resourceName:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final a(Lwi;)V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Ladh;->SX:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ladh;->SX:Landroid/content/Intent;

    :goto_0
    iget-object v1, p0, Ladh;->Jl:Lahz;

    invoke-virtual {p1, v0, v1}, Lwi;->a(Landroid/content/Intent;Lahz;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Ladh;->Sb:Landroid/graphics/Bitmap;

    .line 178
    iget-object v0, p0, Ladh;->Sb:Landroid/graphics/Bitmap;

    iget-object v1, p0, Ladh;->Jl:Lahz;

    invoke-virtual {p1, v0, v1}, Lwi;->a(Landroid/graphics/Bitmap;Lahz;)Z

    move-result v0

    iput-boolean v0, p0, Ladh;->SU:Z

    .line 179
    return-void

    .line 177
    :cond_0
    iget-object v0, p0, Ladh;->intent:Landroid/content/Intent;

    goto :goto_0
.end method

.method public final b(Lwi;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Ladh;->Sb:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 171
    invoke-virtual {p0, p1}, Ladh;->a(Lwi;)V

    .line 173
    :cond_0
    iget-object v0, p0, Ladh;->Sb:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final bH(I)Z
    .locals 1

    .prologue
    .line 233
    iget v0, p0, Ladh;->status:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bI(I)V
    .locals 1

    .prologue
    .line 246
    iput p1, p0, Ladh;->SW:I

    .line 247
    iget v0, p0, Ladh;->status:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ladh;->status:I

    .line 248
    return-void
.end method

.method public final getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Ladh;->intent:Landroid/content/Intent;

    return-object v0
.end method

.method public final h(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Ladh;->Sb:Landroid/graphics/Bitmap;

    .line 167
    return-void
.end method

.method public final kf()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Ladh;->SX:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ladh;->SX:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ladh;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    goto :goto_0
.end method

.method public final kg()Z
    .locals 1

    .prologue
    .line 238
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Ladh;->bH(I)Z

    move-result v0

    return v0
.end method

.method public final kh()I
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Ladh;->SW:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 213
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ShortcutInfo(title="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ladh;->title:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "intent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ladh;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ladh;->id:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ladh;->Jz:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " container="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ladh;->JA:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ladh;->Bd:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cellX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ladh;->Bb:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cellY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ladh;->Bc:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " spanX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ladh;->AY:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " spanY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ladh;->AZ:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dropPos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ladh;->JE:[I

    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " user="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ladh;->Jl:Lahz;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
