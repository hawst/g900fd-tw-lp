.class public Lgjh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgjd;


# instance fields
.field private aTa:Ljava/util/concurrent/Future;

.field private anR:Z

.field private final cLI:Lgjq;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cMj:Ljava/util/concurrent/ExecutorService;

.field private final cMk:Lgiq;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cMl:Ljava/util/HashMap;

.field private cMm:Lgju;

.field private final mGreco3DataManager:Lgix;


# direct methods
.method public constructor <init>(Lgix;Lgjq;Lgiq;)V
    .locals 2
    .param p2    # Lgjq;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lgiq;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lgjh;->mGreco3DataManager:Lgix;

    .line 65
    iput-object p2, p0, Lgjh;->cLI:Lgjq;

    .line 66
    iput-object p3, p0, Lgjh;->cMk:Lgiq;

    .line 70
    const-string v0, "Greco3Thread"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lemv;->q(Ljava/lang/String;Z)Lemw;

    move-result-object v0

    iput-object v0, p0, Lgjh;->cMj:Ljava/util/concurrent/ExecutorService;

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lgjh;->cMl:Ljava/util/HashMap;

    .line 73
    return-void
.end method

.method static a(Ljzp;)Ljux;
    .locals 2

    .prologue
    .line 407
    new-instance v0, Ljux;

    invoke-direct {v0}, Ljux;-><init>()V

    invoke-virtual {p0}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljux;->zb(Ljava/lang/String;)Ljux;

    move-result-object v0

    invoke-virtual {p0}, Ljzp;->getVersion()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljux;->zc(Ljava/lang/String;)Ljux;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized b(Ljava/lang/String;Lgjo;Lgjl;)Lgjk;
    .locals 4
    .param p3    # Lgjl;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 305
    monitor-enter p0

    :try_start_0
    sget-object v3, Lgjo;->cMP:Lgjo;

    if-ne p2, v3, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    move v3, v0

    :goto_0
    invoke-static {v3}, Lifv;->gX(Z)V

    .line 306
    iget-object v3, p0, Lgjh;->aTa:Ljava/util/concurrent/Future;

    if-nez v3, :cond_2

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 308
    iget-object v0, p0, Lgjh;->cMl:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjk;

    .line 309
    if-eqz v0, :cond_4

    .line 310
    invoke-virtual {v0, p1, p3, p2}, Lgjk;->a(Ljava/lang/String;Lgjl;Lgjo;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_3

    .line 331
    :goto_2
    monitor-exit p0

    return-object v0

    :cond_1
    move v3, v2

    .line 305
    goto :goto_0

    :cond_2
    move v0, v2

    .line 306
    goto :goto_1

    .line 315
    :cond_3
    :try_start_1
    iget-object v0, v0, Lgjk;->cMv:Lgjw;

    invoke-virtual {v0}, Lgjw;->delete()V

    .line 316
    iget-object v0, p0, Lgjh;->cMl:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    :cond_4
    invoke-direct {p0, p1, p2, p3}, Lgjh;->c(Ljava/lang/String;Lgjo;Lgjl;)Lgjk;

    move-result-object v0

    .line 321
    if-nez v0, :cond_5

    invoke-virtual {p2}, Lgjo;->aGy()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 322
    const-string v0, "en-US"

    const/4 v2, 0x0

    invoke-direct {p0, v0, p2, v2}, Lgjh;->c(Ljava/lang/String;Lgjo;Lgjl;)Lgjk;

    move-result-object v0

    .line 325
    :cond_5
    if-eqz v0, :cond_6

    .line 326
    iget-object v1, p0, Lgjh;->cMl:Ljava/util/HashMap;

    invoke-virtual {v1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 305
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_6
    move-object v0, v1

    .line 331
    goto :goto_2
.end method

.method private c(Ljava/lang/String;Lgjo;Lgjl;)Lgjk;
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 336
    iget-object v0, p0, Lgjh;->mGreco3DataManager:Lgix;

    invoke-virtual {v0, p1}, Lgix;->mM(Ljava/lang/String;)Lgkd;

    move-result-object v3

    .line 338
    if-nez v3, :cond_1

    .line 390
    :cond_0
    :goto_0
    return-object v2

    .line 343
    :cond_1
    invoke-virtual {v3, p2}, Lgkd;->d(Lgjo;)Ljava/lang/String;

    move-result-object v4

    .line 344
    if-eqz v4, :cond_0

    .line 350
    invoke-virtual {v3}, Lgkd;->aGB()Ljava/lang/String;

    move-result-object v1

    .line 351
    if-nez v1, :cond_2

    .line 352
    const-string v0, "Greco3EngineManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "No data for locale: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 357
    :cond_2
    sget-object v0, Lgjo;->cMP:Lgjo;

    if-ne p2, v0, :cond_5

    .line 359
    if-eqz p3, :cond_4

    iget-object v0, p0, Lgjh;->cLI:Lgjq;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgjh;->cLI:Lgjq;

    invoke-virtual {v0, p3}, Lgjq;->a(Lgjl;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, p3, v0}, Lgkd;->c(Lgjl;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 360
    :goto_1
    if-nez v0, :cond_3

    sget-object v5, Lgjo;->cMP:Lgjo;

    if-eq p2, v5, :cond_0

    .line 368
    :cond_3
    :goto_2
    new-instance v5, Lerc;

    invoke-direct {v5}, Lerc;-><init>()V

    .line 369
    invoke-virtual {v5}, Lerc;->avy()Lerc;

    .line 372
    if-eqz v0, :cond_6

    .line 373
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    aput-object v1, v6, v8

    aput-object v0, v6, v7

    .line 379
    :goto_3
    const-string v0, "Greco3EngineManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, "create_rm: m="

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ",l="

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v7, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v7}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 380
    invoke-static {v4, v6}, Lgjw;->f(Ljava/lang/String;[Ljava/lang/String;)Lgjw;

    move-result-object v1

    .line 382
    if-nez v1, :cond_7

    .line 383
    const-string v0, "Greco3EngineManager"

    const-string v1, "Error loading resources."

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    move-object v0, v2

    .line 359
    goto :goto_1

    :cond_5
    move-object v0, v2

    .line 365
    goto :goto_2

    .line 375
    :cond_6
    new-array v6, v7, [Ljava/lang/String;

    aput-object v1, v6, v8

    goto :goto_3

    .line 388
    :cond_7
    const-string v0, "Greco3EngineManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "Brought up new g3 instance :"

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " for: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "in: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v5}, Lerc;->avz()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " ms"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v0, v2, v4}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 390
    new-instance v0, Lgjk;

    invoke-virtual {v3, p2}, Lgkd;->d(Lgjo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lgkd;->aGC()Ljzp;

    move-result-object v7

    move-object v3, p1

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v7}, Lgjk;-><init>(Lgjw;Ljava/lang/String;Ljava/lang/String;Lgjl;Lgjo;[Ljava/lang/String;Ljzp;)V

    move-object v2, v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lgjo;Lgjl;)Lgjk;
    .locals 1
    .param p3    # Lgjl;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 117
    invoke-direct {p0, p1, p2, p3}, Lgjh;->b(Ljava/lang/String;Lgjo;Lgjl;)Lgjk;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lgju;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 176
    iget-object v0, p0, Lgjh;->aTa:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 177
    iget-object v0, p0, Lgjh;->cMm:Lgju;

    if-ne p1, v0, :cond_1

    :goto_1
    invoke-static {v1}, Lifv;->gY(Z)V

    .line 179
    invoke-virtual {p1}, Lgju;->cancel()I

    .line 188
    :try_start_0
    iget-object v0, p0, Lgjh;->aTa:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 200
    :goto_2
    iget-object v0, p0, Lgjh;->cMm:Lgju;

    invoke-virtual {v0}, Lgju;->delete()V

    .line 201
    iput-object v5, p0, Lgjh;->aTa:Ljava/util/concurrent/Future;

    .line 202
    iput-object v5, p0, Lgjh;->cMm:Lgju;

    .line 203
    :goto_3
    return-void

    :cond_0
    move v0, v2

    .line 176
    goto :goto_0

    :cond_1
    move v1, v2

    .line 177
    goto :goto_1

    .line 190
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 191
    const-string v0, "Greco3EngineManager"

    const-string v1, "Interrupted waiting for recognition to complete."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_3

    .line 193
    :catch_1
    move-exception v0

    .line 194
    const-string v1, "Greco3EngineManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception while running recognition: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public final a(Lgju;Ljava/io/InputStream;Lgiu;Ljwb;Ljava/lang/String;Lgjx;Ljzp;)V
    .locals 8
    .param p6    # Lgjx;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 134
    iget-object v0, p0, Lgjh;->aTa:Ljava/util/concurrent/Future;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 136
    invoke-virtual {p1, p2}, Lgju;->setAudioReader(Ljava/io/InputStream;)I

    .line 137
    invoke-virtual {p4}, Ljwb;->bvi()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1, v0}, Lgju;->kn(I)V

    .line 138
    invoke-virtual {p1, p3}, Lgju;->a(Lgiu;)V

    .line 140
    iget-object v7, p0, Lgjh;->cMj:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lgji;

    move-object v1, p0

    move-object v2, p6

    move-object v3, p5

    move-object v4, p1

    move-object v5, p4

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lgji;-><init>(Lgjh;Lgjx;Ljava/lang/String;Lgju;Ljwb;Ljzp;)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lgjh;->aTa:Ljava/util/concurrent/Future;

    .line 167
    iput-object p1, p0, Lgjh;->cMm:Lgju;

    .line 168
    return-void

    .line 134
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final declared-synchronized a(Ljava/io/File;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 248
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lgjh;->cMl:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjk;

    iget-object v4, v0, Lgjk;->cMy:[Ljava/lang/String;

    array-length v5, v4

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v6, v4, v0

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_5

    .line 254
    if-eqz p2, :cond_8

    .line 255
    iget-object v0, p0, Lgjh;->cMm:Lgju;

    if-eqz v0, :cond_1

    const-string v0, "Greco3EngineManager"

    const-string v2, "Terminating active recognition for shutdown."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    iget-object v0, p0, Lgjh;->cMm:Lgju;

    invoke-virtual {p0, v0}, Lgjh;->a(Lgju;)V

    :cond_1
    iget-object v0, p0, Lgjh;->cMl:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjk;

    iget-object v0, v0, Lgjk;->cMv:Lgjw;

    invoke-virtual {v0}, Lgjw;->delete()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 248
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 255
    :cond_4
    :try_start_1
    iget-object v0, p0, Lgjh;->cMl:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 262
    :cond_5
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_7

    array-length v3, v2

    move v0, v1

    :goto_3
    if-ge v0, v3, :cond_7

    aget-object v1, v2, v0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v4

    if-nez v4, :cond_6

    const-string v4, "Greco3EngineManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error deleting resource file: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v1, v5}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "Greco3EngineManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error deleting directory: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 263
    :cond_8
    monitor-exit p0

    return-void
.end method

.method public final a(Ljava/io/File;ZLjava/lang/Runnable;)V
    .locals 8

    .prologue
    .line 226
    monitor-enter p0

    .line 228
    :try_start_0
    iget-boolean v0, p0, Lgjh;->anR:Z

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 230
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    :goto_0
    return-void

    .line 232
    :cond_0
    monitor-exit p0

    .line 235
    iget-object v7, p0, Lgjh;->cMj:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lgjj;

    const-string v2, "Delete resource"

    const/4 v1, 0x0

    new-array v3, v1, [I

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lgjj;-><init>(Lgjh;Ljava/lang/String;[ILjava/io/File;ZLjava/lang/Runnable;)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 232
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final aGu()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 80
    monitor-enter p0

    .line 81
    :try_start_0
    iget-boolean v0, p0, Lgjh;->anR:Z

    if-eqz v0, :cond_0

    .line 82
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 104
    :goto_0
    return-void

    .line 84
    :cond_0
    monitor-exit p0

    .line 87
    iget-object v0, p0, Lgjh;->mGreco3DataManager:Lgix;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lgix;->fK(Z)V

    .line 94
    iget-object v0, p0, Lgjh;->cMk:Lgiq;

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lgjh;->cMk:Lgiq;

    iget-object v1, p0, Lgjh;->mGreco3DataManager:Lgix;

    invoke-virtual {v1}, Lgix;->aGl()Ligi;

    move-result-object v1

    iget-object v2, p0, Lgjh;->mGreco3DataManager:Lgix;

    invoke-interface {v0, v1, v2}, Lgiq;->a(Ligi;Lgix;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lgjh;->mGreco3DataManager:Lgix;

    invoke-virtual {v0, v3}, Lgix;->fK(Z)V

    .line 102
    :cond_1
    monitor-enter p0

    .line 103
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lgjh;->anR:Z

    .line 104
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 84
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
