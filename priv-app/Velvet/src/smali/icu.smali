.class public final Licu;
.super Licq;
.source "PG"


# instance fields
.field private dyt:I


# direct methods
.method public constructor <init>(Lcom/google/android/search/shared/actions/PlayMediaAction;Libo;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Licq;-><init>(Lcom/google/android/search/shared/actions/PlayMediaAction;Libo;)V

    .line 52
    return-void
.end method

.method private static aUQ()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 277
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.MEDIA_PLAY_FROM_SEARCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 278
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 279
    return-object v0
.end method

.method private ao(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 199
    iget-object v2, p0, Licu;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v2

    iget-object v2, v2, Ljmh;->etA:Ljml;

    .line 200
    if-nez v2, :cond_0

    .line 201
    iput v0, p0, Licu;->dyt:I

    .line 237
    :goto_0
    return v0

    .line 204
    :cond_0
    invoke-virtual {v2}, Ljml;->bqc()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 205
    const-string v0, "android.intent.extra.focus"

    const-string v2, "vnd.android.cursor.item/*"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 206
    iput v1, p0, Licu;->dyt:I

    :goto_1
    move v0, v1

    .line 237
    goto :goto_0

    .line 207
    :cond_1
    invoke-virtual {v2}, Ljml;->bqk()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 208
    const-string v0, "android.intent.extra.focus"

    const-string v2, "vnd.android.cursor.item/playlist"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 210
    const/4 v0, 0x7

    iput v0, p0, Licu;->dyt:I

    goto :goto_1

    .line 211
    :cond_2
    invoke-virtual {v2}, Ljml;->bqj()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 212
    const-string v0, "android.intent.extra.focus"

    const-string v2, "vnd.android.cursor.item/radio"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 214
    const/4 v0, 0x6

    iput v0, p0, Licu;->dyt:I

    goto :goto_1

    .line 215
    :cond_3
    invoke-virtual {v2}, Ljml;->bqa()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 216
    const-string v0, "android.intent.extra.focus"

    const-string v2, "vnd.android.cursor.item/audio"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 218
    const/4 v0, 0x5

    iput v0, p0, Licu;->dyt:I

    goto :goto_1

    .line 219
    :cond_4
    invoke-virtual {v2}, Ljml;->bpY()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 220
    const-string v0, "android.intent.extra.focus"

    const-string v2, "vnd.android.cursor.item/album"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 222
    const/4 v0, 0x4

    iput v0, p0, Licu;->dyt:I

    goto :goto_1

    .line 223
    :cond_5
    invoke-virtual {v2}, Ljml;->aVk()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 224
    const-string v0, "android.intent.extra.focus"

    const-string v2, "vnd.android.cursor.item/artist"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 226
    const/4 v0, 0x3

    iput v0, p0, Licu;->dyt:I

    goto :goto_1

    .line 227
    :cond_6
    invoke-virtual {v2}, Ljml;->bqb()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 228
    const-string v0, "android.intent.extra.focus"

    const-string v2, "vnd.android.cursor.item/genre"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 230
    const/4 v0, 0x2

    iput v0, p0, Licu;->dyt:I

    goto :goto_1

    .line 232
    :cond_7
    const-string v2, "android.intent.extra.focus"

    const-string v3, "vnd.android.cursor.item/*"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 234
    iput v0, p0, Licu;->dyt:I

    goto :goto_1
.end method

.method private ap(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 245
    iget-object v0, p0, Licu;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v0

    iget-object v0, v0, Ljmh;->etA:Ljml;

    .line 246
    iget v1, p0, Licu;->dyt:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 268
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    invoke-virtual {v0}, Ljml;->bqi()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 251
    const-string v1, "android.intent.extra.playlist"

    invoke-virtual {v0}, Ljml;->bqh()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 253
    :cond_2
    invoke-virtual {v0}, Ljml;->bqg()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 254
    const-string v1, "android.intent.extra.radio_channel"

    invoke-virtual {v0}, Ljml;->bqf()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 256
    :cond_3
    invoke-virtual {v0}, Ljml;->bqa()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 257
    const-string v1, "android.intent.extra.title"

    invoke-virtual {v0}, Ljml;->bpZ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 259
    :cond_4
    invoke-virtual {v0}, Ljml;->bpY()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 260
    const-string v1, "android.intent.extra.album"

    invoke-virtual {v0}, Ljml;->aVp()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 262
    :cond_5
    invoke-virtual {v0}, Ljml;->aVk()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 263
    const-string v1, "android.intent.extra.artist"

    invoke-virtual {v0}, Ljml;->aVj()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 265
    :cond_6
    invoke-virtual {v0}, Ljml;->bqb()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 266
    const-string v1, "android.intent.extra.genre"

    invoke-virtual {v0}, Ljml;->te()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public final aUO()Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 112
    iget-object v0, p0, Licu;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v0

    .line 114
    invoke-static {}, Licu;->aUQ()Landroid/content/Intent;

    move-result-object v1

    .line 115
    const-string v2, "com.google.android.music"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    invoke-direct {p0, v1}, Licu;->ao(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 118
    invoke-direct {p0, v1}, Licu;->ap(Landroid/content/Intent;)V

    .line 121
    :cond_0
    iget v2, p0, Licu;->dyt:I

    if-ne v2, v4, :cond_3

    .line 122
    const-string v2, "query"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 124
    const-string v2, "queryComplete"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    :goto_0
    iget v2, p0, Licu;->dyt:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    iget v2, p0, Licu;->dyt:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_1

    iget v2, p0, Licu;->dyt:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_5

    .line 135
    :cond_1
    invoke-virtual {v0}, Ljmh;->bpT()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 136
    new-array v2, v4, [Ljava/lang/String;

    .line 137
    invoke-virtual {v0}, Ljmh;->bpS()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    .line 138
    const-string v0, "android.intent.extra.inventory_identifier"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    :cond_2
    :goto_1
    return-object v1

    .line 125
    :cond_3
    invoke-virtual {v0}, Ljmh;->bpH()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 126
    const-string v2, "query"

    invoke-virtual {v0}, Ljmh;->bpG()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 128
    const-string v2, "queryComplete"

    invoke-virtual {v0}, Ljmh;->bpF()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 130
    :cond_4
    const-string v2, "query"

    invoke-virtual {v0}, Ljmh;->bpF()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 140
    :cond_5
    iget v2, p0, Licu;->dyt:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 141
    iget-object v2, v0, Ljmh;->etA:Ljml;

    invoke-virtual {v2}, Ljml;->bqe()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 142
    new-array v2, v4, [Ljava/lang/String;

    .line 143
    iget-object v0, v0, Ljmh;->etA:Ljml;

    invoke-virtual {v0}, Ljml;->bqd()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    .line 144
    const-string v0, "android.intent.extra.inventory_identifier"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method public final aUP()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 159
    iget-object v0, p0, Licu;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v0

    .line 161
    invoke-static {}, Licu;->aUQ()Landroid/content/Intent;

    move-result-object v1

    .line 162
    const-string v2, "com.google.android.youtube"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 164
    invoke-direct {p0, v1}, Licu;->ao(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 165
    invoke-direct {p0, v1}, Licu;->ap(Landroid/content/Intent;)V

    .line 168
    :cond_0
    iget v2, p0, Licu;->dyt:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 169
    const-string v2, "query"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 174
    :goto_0
    iget-object v2, v0, Ljmh;->etA:Ljml;

    if-eqz v2, :cond_1

    iget-object v2, v0, Ljmh;->etA:Ljml;

    iget-object v2, v2, Ljml;->eua:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, v0, Ljmh;->etA:Ljml;

    iget-object v2, v2, Ljml;->eua:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 176
    const-string v2, "android.intent.extra.inventory_identifier"

    iget-object v0, v0, Ljmh;->etA:Ljml;

    iget-object v0, v0, Ljml;->eua:[Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 180
    :cond_1
    return-object v1

    .line 171
    :cond_2
    const-string v2, "query"

    invoke-virtual {v0}, Ljmh;->bpF()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected final c(Ljmh;)I
    .locals 1

    .prologue
    .line 195
    invoke-virtual {p1}, Ljmh;->bpO()I

    move-result v0

    return v0
.end method

.method public final oX(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 60
    invoke-static {}, Licu;->aUQ()Landroid/content/Intent;

    move-result-object v0

    .line 62
    invoke-direct {p0, v0}, Licu;->ao(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    invoke-direct {p0, v0}, Licu;->ap(Landroid/content/Intent;)V

    .line 66
    :cond_0
    iget v1, p0, Licu;->dyt:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 67
    const-string v1, "query"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    :goto_0
    return-object v0

    .line 69
    :cond_1
    const-string v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final oY(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Licu;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v0

    invoke-virtual {v0}, Ljmh;->bpN()Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    const/4 v0, 0x0

    .line 81
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Licu;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v1

    invoke-virtual {v1}, Ljmh;->bpM()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public final oZ(Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    return-object v0
.end method

.method public final pc(Ljava/lang/String;)Lijj;
    .locals 3

    .prologue
    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 89
    iget-object v1, p0, Licu;->mAppSelectionHelper:Libo;

    invoke-virtual {p0}, Licu;->aUO()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Libo;->an(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 90
    iget-object v1, p0, Licu;->mAppSelectionHelper:Libo;

    invoke-virtual {p0}, Licu;->aUP()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Libo;->an(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 91
    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    return-object v0
.end method
