.class public abstract Lfus;
.super Lfuz;
.source "PG"

# interfaces
.implements Lfty;


# instance fields
.field protected final cCl:Lftx;

.field protected final mStringEvaluator:Lgbr;


# direct methods
.method protected constructor <init>(Lizj;Lftz;Lemp;Lgbr;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 45
    iput-object p4, p0, Lfus;->mStringEvaluator:Lgbr;

    .line 46
    new-instance v0, Lftx;

    invoke-direct {v0, p0}, Lftx;-><init>(Lfty;)V

    iput-object v0, p0, Lfus;->cCl:Lftx;

    .line 47
    return-void
.end method

.method protected constructor <init>(Lizq;Lftz;Lemp;Lgbr;)V
    .locals 6

    .prologue
    .line 55
    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lfus;-><init>(Lizq;Lftz;Lemp;Lgbr;I)V

    .line 57
    return-void
.end method

.method protected constructor <init>(Lizq;Lftz;Lemp;Lgbr;I)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizq;Lftz;Lemp;)V

    .line 68
    iput-object p4, p0, Lfus;->mStringEvaluator:Lgbr;

    .line 69
    iget-object v0, p1, Lizq;->dUZ:Lizj;

    invoke-virtual {p0, v0}, Lfus;->A(Lizj;)Ljau;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljau;->beC()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    invoke-virtual {v0}, Ljau;->aZu()I

    move-result p5

    .line 73
    :cond_0
    new-instance v0, Lftx;

    invoke-direct {v0, p0, p5}, Lftx;-><init>(Lfty;I)V

    iput-object v0, p0, Lfus;->cCl:Lftx;

    .line 74
    return-void
.end method


# virtual methods
.method public A(Lizj;)Ljau;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 79
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/content/Context;Lfmt;)Lang;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lfus;->cCl:Lftx;

    invoke-virtual {v0, p1}, Lftx;->aS(Landroid/content/Context;)Lang;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lftn;)V
    .locals 0

    .prologue
    .line 130
    return-void
.end method

.method public aDq()Z
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x0

    return v0
.end method

.method public aDv()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 121
    const/4 v0, 0x0

    return-object v0
.end method

.method public aT(Landroid/content/Context;)Lanh;
    .locals 3

    .prologue
    .line 139
    invoke-virtual {p0, p1}, Lfus;->aV(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 140
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    const/4 v0, 0x0

    .line 153
    :goto_0
    return-object v0

    .line 143
    :cond_0
    new-instance v1, Lftn;

    invoke-direct {v1, v0}, Lftn;-><init>(Ljava/lang/String;)V

    .line 144
    invoke-virtual {p0, p1}, Lfus;->aX(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 145
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 146
    iput-object v0, v1, Lftn;->ceJ:Ljava/lang/String;

    .line 148
    :cond_1
    invoke-virtual {p0}, Lfus;->aDv()Ljava/lang/String;

    move-result-object v0

    .line 149
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 150
    iput-object v0, v1, Lftn;->cBi:Ljava/lang/String;

    .line 152
    :cond_2
    invoke-virtual {p0, v1}, Lfus;->a(Lftn;)V

    .line 153
    invoke-virtual {v1}, Lftn;->aDh()Lanh;

    move-result-object v0

    goto :goto_0
.end method

.method public aU(Landroid/content/Context;)Lanh;
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    return-object v0
.end method

.method public aV(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 87
    iget-object v1, p0, Lfuz;->mEntryTreeNode:Lizq;

    .line 88
    if-eqz v1, :cond_0

    iget-object v2, v1, Lizq;->dUZ:Lizj;

    if-nez v2, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-object v0

    .line 91
    :cond_1
    iget-object v1, v1, Lizq;->dUZ:Lizj;

    invoke-virtual {p0, v1}, Lfus;->A(Lizj;)Ljau;

    move-result-object v1

    .line 92
    if-eqz v1, :cond_0

    iget-object v2, v1, Ljau;->dQX:Ljhe;

    if-eqz v2, :cond_0

    .line 93
    iget-object v0, p0, Lfus;->mStringEvaluator:Lgbr;

    iget-object v1, v1, Ljau;->dQX:Ljhe;

    invoke-virtual {v0, p1, v1}, Lgbr;->b(Landroid/content/Context;Ljhe;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public aX(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 103
    iget-object v1, p0, Lfuz;->mEntryTreeNode:Lizq;

    .line 104
    if-eqz v1, :cond_0

    iget-object v2, v1, Lizq;->dUZ:Lizj;

    if-nez v2, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-object v0

    .line 107
    :cond_1
    iget-object v1, v1, Lizq;->dUZ:Lizj;

    invoke-virtual {p0, v1}, Lfus;->A(Lizj;)Ljau;

    move-result-object v1

    .line 108
    if-eqz v1, :cond_0

    iget-object v2, v1, Ljau;->dXq:Ljhe;

    if-eqz v2, :cond_0

    .line 109
    iget-object v0, p0, Lfus;->mStringEvaluator:Lgbr;

    iget-object v1, v1, Ljau;->dXq:Ljhe;

    invoke-virtual {v0, p1, v1}, Lgbr;->b(Landroid/content/Context;Ljhe;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Landroid/content/Context;Ljava/util/List;)Lanh;
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 158
    iget-object v1, p0, Lfus;->cCl:Lftx;

    new-instance v2, Lanp;

    invoke-direct {v2}, Lanp;-><init>()V

    iget-object v0, v2, Lanp;->aii:[Lanh;

    invoke-static {v0, p2}, Leqh;->a([Ljava/lang/Object;Ljava/util/Collection;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v2, Lanp;->aii:[Lanh;

    new-instance v0, Lani;

    invoke-direct {v0}, Lani;-><init>()V

    invoke-virtual {v0, v4}, Lani;->cp(I)Lani;

    iput-object v2, v0, Lani;->ahG:Lanp;

    const/16 v2, 0xb6

    invoke-virtual {v0, v2}, Lani;->cq(I)Lani;

    new-instance v2, Lanb;

    invoke-direct {v2}, Lanb;-><init>()V

    iput-object v2, v0, Lani;->ahC:Lanb;

    iget-object v2, v0, Lani;->ahC:Lanb;

    const v3, 0x7f020137

    invoke-virtual {v2, v3}, Lanb;->ch(I)Lanb;

    new-instance v2, Laoj;

    invoke-direct {v2}, Laoj;-><init>()V

    const v3, 0x7f0a037f

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v3, Lanh;

    invoke-direct {v3}, Lanh;-><init>()V

    invoke-virtual {v3, v4}, Lanh;->cm(I)Lanh;

    iput-object v2, v3, Lanh;->agz:Laoj;

    iput-object v0, v3, Lanh;->ahs:Lani;

    iget-object v0, v1, Lftx;->mProvider:Lfty;

    invoke-interface {v0}, Lfty;->aAv()Lizq;

    move-result-object v0

    iget-object v0, v0, Lizq;->dUZ:Lizj;

    iput-object v0, v3, Lanh;->ahu:Lizj;

    return-object v3
.end method

.method public b(Landroid/content/Context;Lizj;)[Lanh;
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x0

    return-object v0
.end method
