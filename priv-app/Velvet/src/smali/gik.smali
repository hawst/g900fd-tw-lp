.class public final Lgik;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgij;


# instance fields
.field private final cLg:Lghs;

.field private final cLh:Lghl;

.field private final cLi:Lghg;

.field private final cLj:Lgnh;

.field private final cLk:Lgig;

.field private final mGenericTokenSupplier:Lgih;

.field private final mHandsFreeBiasingSupplier:Lgii;


# direct methods
.method public constructor <init>(Lgih;Lghs;Lghl;Lghg;Lgnh;Lgig;Lgii;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lgik;->mGenericTokenSupplier:Lgih;

    .line 43
    iput-object p2, p0, Lgik;->cLg:Lghs;

    .line 44
    iput-object p3, p0, Lgik;->cLh:Lghl;

    .line 45
    iput-object p4, p0, Lgik;->cLi:Lghg;

    .line 46
    iput-object p5, p0, Lgik;->cLj:Lgnh;

    .line 47
    iput-object p6, p0, Lgik;->cLk:Lgig;

    .line 48
    iput-object p7, p0, Lgik;->mHandsFreeBiasingSupplier:Lgii;

    .line 49
    return-void
.end method


# virtual methods
.method public final aFU()Ligi;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lgik;->cLg:Lghs;

    return-object v0
.end method

.method public final aFV()Ligi;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lgik;->cLh:Lghl;

    return-object v0
.end method

.method public final aFW()Ligi;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lgik;->cLi:Lghg;

    return-object v0
.end method

.method public final aFX()Ligi;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lgik;->mGenericTokenSupplier:Lgih;

    return-object v0
.end method

.method public final aFY()Ligi;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lgik;->mHandsFreeBiasingSupplier:Lgii;

    return-object v0
.end method

.method public final aFZ()Ligi;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lgik;->cLj:Lgnh;

    return-object v0
.end method

.method public final aGa()Ligi;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lgik;->cLk:Lgig;

    return-object v0
.end method
