.class final Lfth;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/text/Html$TagHandler;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    return-void
.end method

.method private static a(ZLandroid/text/Editable;Ljava/lang/Object;F)V
    .locals 5

    .prologue
    .line 211
    if-eqz p0, :cond_1

    .line 212
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const/16 v1, 0x11

    invoke-interface {p1, p2, v0, v0, v1}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    new-instance v1, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v1, p3}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    const/4 v3, 0x0

    invoke-interface {p1}, Landroid/text/Spanned;->length()I

    move-result v4

    invoke-interface {p1, v3, v4, v0}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    array-length v3, v0

    if-nez v3, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-interface {p1, v0}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v3

    invoke-interface {p1, v0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    if-eq v3, v2, :cond_0

    const/16 v0, 0x21

    invoke-interface {p1, v1, v3, v2, v0}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_2
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    aget-object v0, v0, v3

    goto :goto_1
.end method


# virtual methods
.method public final handleTag(ZLjava/lang/String;Landroid/text/Editable;Lorg/xml/sax/XMLReader;)V
    .locals 2

    .prologue
    .line 200
    const-string v0, "large"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    new-instance v0, Lfti;

    invoke-direct {v0}, Lfti;-><init>()V

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {p1, p3, v0, v1}, Lfth;->a(ZLandroid/text/Editable;Ljava/lang/Object;F)V

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    const-string v0, "xlarge"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 203
    new-instance v0, Lftj;

    invoke-direct {v0}, Lftj;-><init>()V

    const/high16 v1, 0x40400000    # 3.0f

    invoke-static {p1, p3, v0, v1}, Lfth;->a(ZLandroid/text/Editable;Ljava/lang/Object;F)V

    goto :goto_0

    .line 204
    :cond_2
    const-string v0, "xxlarge"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    new-instance v0, Lftk;

    invoke-direct {v0}, Lftk;-><init>()V

    const/high16 v1, 0x40800000    # 4.0f

    invoke-static {p1, p3, v0, v1}, Lfth;->a(ZLandroid/text/Editable;Ljava/lang/Object;F)V

    goto :goto_0
.end method
