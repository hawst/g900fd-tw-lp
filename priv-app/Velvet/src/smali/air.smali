.class public Lair;
.super Landroid/widget/FrameLayout;
.source "PG"


# static fields
.field private static final ZI:Z

.field private static final ZJ:Z


# instance fields
.field private ZK:Landroid/opengl/GLSurfaceView;

.field private ZL:Z

.field private ZM:Landroid/view/Choreographer$FrameCallback;

.field protected ZN:Laiu;

.field private ZO:Ljava/lang/Runnable;

.field protected dK:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x10

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lair;->ZI:Z

    .line 54
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, Lair;->ZJ:Z

    return-void

    :cond_0
    move v0, v2

    .line 52
    goto :goto_0

    :cond_1
    move v1, v2

    .line 54
    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lair;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 91
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    iput-boolean v2, p0, Lair;->ZL:Z

    .line 74
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lair;->dK:Ljava/lang/Object;

    .line 138
    new-instance v0, Lais;

    invoke-direct {v0, p0}, Lais;-><init>(Lair;)V

    iput-object v0, p0, Lair;->ZO:Ljava/lang/Runnable;

    .line 256
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 92
    sget-boolean v0, Lair;->ZI:Z

    if-nez v0, :cond_0

    .line 113
    :goto_0
    return-void

    .line 96
    :cond_0
    new-instance v0, Laiu;

    invoke-direct {v0}, Laiu;-><init>()V

    iput-object v0, p0, Lair;->ZN:Laiu;

    .line 97
    iget-object v0, p0, Lair;->ZN:Laiu;

    new-instance v1, Laim;

    invoke-direct {v1, p0}, Laim;-><init>(Landroid/view/View;)V

    iput-object v1, v0, Laiu;->ZS:Laim;

    .line 104
    new-instance v0, Landroid/opengl/GLSurfaceView;

    invoke-direct {v0, p1}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lair;->ZK:Landroid/opengl/GLSurfaceView;

    .line 105
    iget-object v0, p0, Lair;->ZK:Landroid/opengl/GLSurfaceView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/opengl/GLSurfaceView;->setEGLContextClientVersion(I)V

    .line 106
    iget-object v0, p0, Lair;->ZK:Landroid/opengl/GLSurfaceView;

    new-instance v1, Laiv;

    invoke-direct {v1, p0, v2}, Laiv;-><init>(Lair;B)V

    invoke-virtual {v0, v1}, Landroid/opengl/GLSurfaceView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 107
    iget-object v0, p0, Lair;->ZK:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0, v2}, Landroid/opengl/GLSurfaceView;->setRenderMode(I)V

    .line 108
    iget-object v0, p0, Lair;->ZK:Landroid/opengl/GLSurfaceView;

    .line 110
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lair;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method static synthetic a(Lair;)Landroid/opengl/GLSurfaceView;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lair;->ZK:Landroid/opengl/GLSurfaceView;

    return-object v0
.end method

.method private a(Laiu;)V
    .locals 3

    .prologue
    .line 193
    if-eqz p1, :cond_0

    iget-object v0, p1, Laiu;->ZQ:Laiq;

    if-eqz v0, :cond_0

    iget v0, p1, Laiu;->Ix:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lair;->getWidth()I

    move-result v0

    if-nez v0, :cond_1

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    invoke-virtual {p0}, Lair;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p1, Laiu;->ZQ:Laiq;

    invoke-interface {v1}, Laiq;->gi()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-virtual {p0}, Lair;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p1, Laiu;->ZQ:Laiq;

    invoke-interface {v2}, Laiq;->gj()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p1, Laiu;->Ix:F

    goto :goto_0
.end method

.method static synthetic a(Lair;Z)Z
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lair;->ZL:Z

    return v0
.end method


# virtual methods
.method public a(Laiq;Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 165
    sget-boolean v1, Lair;->ZI:Z

    if-nez v1, :cond_0

    .line 178
    :goto_0
    return-void

    .line 168
    :cond_0
    iget-object v2, p0, Lair;->dK:Ljava/lang/Object;

    monitor-enter v2

    .line 169
    :try_start_0
    iget-object v1, p0, Lair;->ZN:Laiu;

    iput-object p1, v1, Laiu;->ZQ:Laiq;

    .line 170
    iget-object v1, p0, Lair;->ZN:Laiu;

    iput-object p2, v1, Laiu;->ZR:Ljava/lang/Runnable;

    .line 171
    iget-object v3, p0, Lair;->ZN:Laiu;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Laiq;->gi()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    :goto_1
    iput v1, v3, Laiu;->centerX:I

    .line 172
    iget-object v3, p0, Lair;->ZN:Laiu;

    if-eqz p1, :cond_3

    invoke-interface {p1}, Laiq;->gj()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    :goto_2
    iput v1, v3, Laiu;->centerY:I

    .line 173
    iget-object v1, p0, Lair;->ZN:Laiu;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Laiq;->getRotation()I

    move-result v0

    :cond_1
    iput v0, v1, Laiu;->rotation:I

    .line 174
    iget-object v0, p0, Lair;->ZN:Laiu;

    const/4 v1, 0x0

    iput v1, v0, Laiu;->Ix:F

    .line 175
    iget-object v0, p0, Lair;->ZN:Laiu;

    invoke-direct {p0, v0}, Lair;->a(Laiu;)V

    .line 176
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    invoke-virtual {p0}, Lair;->invalidate()V

    goto :goto_0

    :cond_2
    move v1, v0

    .line 171
    goto :goto_1

    :cond_3
    move v1, v0

    .line 172
    goto :goto_2

    .line 176
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final destroy()V
    .locals 2

    .prologue
    .line 128
    sget-boolean v0, Lair;->ZI:Z

    if-nez v0, :cond_0

    .line 136
    :goto_0
    return-void

    .line 134
    :cond_0
    iget-object v0, p0, Lair;->ZK:Landroid/opengl/GLSurfaceView;

    iget-object v1, p0, Lair;->ZO:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/opengl/GLSurfaceView;->queueEvent(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 204
    sget-boolean v0, Lair;->ZI:Z

    if-nez v0, :cond_0

    .line 211
    :goto_0
    return-void

    .line 210
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public invalidate()V
    .locals 2

    .prologue
    .line 224
    sget-boolean v0, Lair;->ZI:Z

    if-nez v0, :cond_1

    .line 237
    :cond_0
    :goto_0
    return-void

    .line 231
    :cond_1
    sget-boolean v0, Lair;->ZJ:Z

    if-eqz v0, :cond_3

    .line 232
    iget-boolean v0, p0, Lair;->ZL:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lair;->ZL:Z

    iget-object v0, p0, Lair;->ZM:Landroid/view/Choreographer$FrameCallback;

    if-nez v0, :cond_2

    new-instance v0, Lait;

    invoke-direct {v0, p0}, Lait;-><init>(Lair;)V

    iput-object v0, p0, Lair;->ZM:Landroid/view/Choreographer$FrameCallback;

    :cond_2
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v0

    iget-object v1, p0, Lair;->ZM:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    goto :goto_0

    .line 234
    :cond_3
    iget-object v0, p0, Lair;->ZK:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 183
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 184
    sget-boolean v0, Lair;->ZI:Z

    if-nez v0, :cond_0

    .line 189
    :goto_0
    return-void

    .line 187
    :cond_0
    iget-object v1, p0, Lair;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 188
    :try_start_0
    iget-object v0, p0, Lair;->ZN:Laiu;

    invoke-direct {p0, v0}, Lair;->a(Laiu;)V

    .line 189
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public setTranslationX(F)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 216
    sget-boolean v0, Lair;->ZI:Z

    if-nez v0, :cond_0

    .line 220
    :goto_0
    return-void

    .line 219
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setTranslationX(F)V

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 1

    .prologue
    .line 117
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Lair;->ZK:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0, p1}, Landroid/opengl/GLSurfaceView;->setVisibility(I)V

    .line 125
    return-void
.end method
