.class public final Lieg;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dAe:[Lieg;


# instance fields
.field private aez:I

.field private aji:Ljava/lang/String;

.field private dAf:J

.field private dAg:Ljava/lang/String;

.field public dzt:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 3396
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3397
    const/4 v0, 0x0

    iput v0, p0, Lieg;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lieg;->aji:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Lieg;->dzt:[Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lieg;->dAf:J

    const-string v0, ""

    iput-object v0, p0, Lieg;->dAg:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lieg;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lieg;->eCz:I

    .line 3398
    return-void
.end method

.method public static aVB()[Lieg;
    .locals 2

    .prologue
    .line 3317
    sget-object v0, Lieg;->dAe:[Lieg;

    if-nez v0, :cond_1

    .line 3318
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 3320
    :try_start_0
    sget-object v0, Lieg;->dAe:[Lieg;

    if-nez v0, :cond_0

    .line 3321
    const/4 v0, 0x0

    new-array v0, v0, [Lieg;

    sput-object v0, Lieg;->dAe:[Lieg;

    .line 3323
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3325
    :cond_1
    sget-object v0, Lieg;->dAe:[Lieg;

    return-object v0

    .line 3323
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3311
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lieg;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lieg;->aji:Ljava/lang/String;

    iget v0, p0, Lieg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lieg;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lieg;->dzt:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lieg;->dzt:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lieg;->dzt:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lieg;->dzt:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lieg;->dAf:J

    iget v0, p0, Lieg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lieg;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lieg;->dAg:Ljava/lang/String;

    iget v0, p0, Lieg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lieg;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 3414
    iget v0, p0, Lieg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3415
    const/4 v0, 0x1

    iget-object v1, p0, Lieg;->aji:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3417
    :cond_0
    iget-object v0, p0, Lieg;->dzt:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lieg;->dzt:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 3418
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lieg;->dzt:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 3419
    iget-object v1, p0, Lieg;->dzt:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 3420
    if-eqz v1, :cond_1

    .line 3421
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3418
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3425
    :cond_2
    iget v0, p0, Lieg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 3426
    const/4 v0, 0x3

    iget-wide v2, p0, Lieg;->dAf:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 3428
    :cond_3
    iget v0, p0, Lieg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 3429
    const/4 v0, 0x4

    iget-object v1, p0, Lieg;->dAg:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3431
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3432
    return-void
.end method

.method public final aVC()J
    .locals 2

    .prologue
    .line 3358
    iget-wide v0, p0, Lieg;->dAf:J

    return-wide v0
.end method

.method public final bO(J)Lieg;
    .locals 2

    .prologue
    .line 3361
    const-wide/32 v0, 0xf1b30

    iput-wide v0, p0, Lieg;->dAf:J

    .line 3362
    iget v0, p0, Lieg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lieg;->aez:I

    .line 3363
    return-object p0
.end method

.method public final getCurrencyCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3377
    iget-object v0, p0, Lieg;->dAg:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 3436
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3437
    iget v2, p0, Lieg;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 3438
    const/4 v2, 0x1

    iget-object v3, p0, Lieg;->aji:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3441
    :cond_0
    iget-object v2, p0, Lieg;->dzt:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lieg;->dzt:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    move v3, v1

    .line 3444
    :goto_0
    iget-object v4, p0, Lieg;->dzt:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 3445
    iget-object v4, p0, Lieg;->dzt:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 3446
    if-eqz v4, :cond_1

    .line 3447
    add-int/lit8 v3, v3, 0x1

    .line 3448
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 3444
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3452
    :cond_2
    add-int/2addr v0, v2

    .line 3453
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 3455
    :cond_3
    iget v1, p0, Lieg;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_4

    .line 3456
    const/4 v1, 0x3

    iget-wide v2, p0, Lieg;->dAf:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3459
    :cond_4
    iget v1, p0, Lieg;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    .line 3460
    const/4 v1, 0x4

    iget-object v2, p0, Lieg;->dAg:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3463
    :cond_5
    return v0
.end method

.method public final pl(Ljava/lang/String;)Lieg;
    .locals 1

    .prologue
    .line 3380
    if-nez p1, :cond_0

    .line 3381
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3383
    :cond_0
    iput-object p1, p0, Lieg;->dAg:Ljava/lang/String;

    .line 3384
    iget v0, p0, Lieg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lieg;->aez:I

    .line 3385
    return-object p0
.end method
