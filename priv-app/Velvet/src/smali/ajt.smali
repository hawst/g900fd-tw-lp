.class public Lajt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private synthetic abd:Lcom/android/recurrencepicker/RecurrencePickerDialog;

.field private abs:I

.field private abt:I

.field private abu:I


# direct methods
.method public constructor <init>(Lcom/android/recurrencepicker/RecurrencePickerDialog;III)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lajt;->abd:Lcom/android/recurrencepicker/RecurrencePickerDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248
    iput p2, p0, Lajt;->abs:I

    .line 249
    iput p4, p0, Lajt;->abt:I

    .line 250
    iput p3, p0, Lajt;->abu:I

    .line 251
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 256
    const/4 v2, 0x0

    .line 259
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 264
    :goto_0
    iget v3, p0, Lajt;->abs:I

    if-ge v0, v3, :cond_1

    .line 265
    iget v0, p0, Lajt;->abs:I

    .line 273
    :goto_1
    if-eqz v1, :cond_0

    .line 274
    invoke-interface {p1}, Landroid/text/Editable;->clear()V

    .line 275
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 278
    :cond_0
    iget-object v1, p0, Lajt;->abd:Lcom/android/recurrencepicker/RecurrencePickerDialog;

    invoke-static {v1}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->a(Lcom/android/recurrencepicker/RecurrencePickerDialog;)V

    .line 279
    invoke-virtual {p0, v0}, Lajt;->bX(I)V

    .line 280
    return-void

    .line 261
    :catch_0
    move-exception v0

    iget v0, p0, Lajt;->abu:I

    goto :goto_0

    .line 267
    :cond_1
    iget v3, p0, Lajt;->abt:I

    if-le v0, v3, :cond_2

    .line 269
    iget v0, p0, Lajt;->abt:I

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method bX(I)V
    .locals 0

    .prologue
    .line 284
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 288
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 292
    return-void
.end method
