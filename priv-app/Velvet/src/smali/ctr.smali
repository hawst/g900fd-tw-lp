.class public interface abstract Lctr;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final bik:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 70
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "lookup_key"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "icon_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "given_names"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "score"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "emails"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "nickname"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "note"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "organization"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "phone_numbers"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "postal_address"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "phonetic_name"

    aput-object v2, v0, v1

    sput-object v0, Lctr;->bik:[Ljava/lang/String;

    return-void
.end method
