.class public final Ljcu;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eaX:[Ljcu;


# instance fields
.field private aez:I

.field private afb:Ljava/lang/String;

.field public dME:Lixn;

.field public dQU:Liym;

.field private eaY:Ljct;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 21563
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 21564
    const/4 v0, 0x0

    iput v0, p0, Ljcu;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljcu;->afb:Ljava/lang/String;

    iput-object v1, p0, Ljcu;->dQU:Liym;

    iput-object v1, p0, Ljcu;->dME:Lixn;

    iput-object v1, p0, Ljcu;->eaY:Ljct;

    iput-object v1, p0, Ljcu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljcu;->eCz:I

    .line 21565
    return-void
.end method

.method public static bgP()[Ljcu;
    .locals 2

    .prologue
    .line 21519
    sget-object v0, Ljcu;->eaX:[Ljcu;

    if-nez v0, :cond_1

    .line 21520
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 21522
    :try_start_0
    sget-object v0, Ljcu;->eaX:[Ljcu;

    if-nez v0, :cond_0

    .line 21523
    const/4 v0, 0x0

    new-array v0, v0, [Ljcu;

    sput-object v0, Ljcu;->eaX:[Ljcu;

    .line 21525
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 21527
    :cond_1
    sget-object v0, Ljcu;->eaX:[Ljcu;

    return-object v0

    .line 21525
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 21513
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljcu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcu;->afb:Ljava/lang/String;

    iget v0, p0, Ljcu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcu;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljcu;->dQU:Liym;

    if-nez v0, :cond_1

    new-instance v0, Liym;

    invoke-direct {v0}, Liym;-><init>()V

    iput-object v0, p0, Ljcu;->dQU:Liym;

    :cond_1
    iget-object v0, p0, Ljcu;->dQU:Liym;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljcu;->dME:Lixn;

    if-nez v0, :cond_2

    new-instance v0, Lixn;

    invoke-direct {v0}, Lixn;-><init>()V

    iput-object v0, p0, Ljcu;->dME:Lixn;

    :cond_2
    iget-object v0, p0, Ljcu;->dME:Lixn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljcu;->eaY:Ljct;

    if-nez v0, :cond_3

    new-instance v0, Ljct;

    invoke-direct {v0}, Ljct;-><init>()V

    iput-object v0, p0, Ljcu;->eaY:Ljct;

    :cond_3
    iget-object v0, p0, Ljcu;->eaY:Ljct;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 21581
    iget v0, p0, Ljcu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 21582
    const/4 v0, 0x1

    iget-object v1, p0, Ljcu;->afb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 21584
    :cond_0
    iget-object v0, p0, Ljcu;->dQU:Liym;

    if-eqz v0, :cond_1

    .line 21585
    const/4 v0, 0x2

    iget-object v1, p0, Ljcu;->dQU:Liym;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 21587
    :cond_1
    iget-object v0, p0, Ljcu;->dME:Lixn;

    if-eqz v0, :cond_2

    .line 21588
    const/4 v0, 0x3

    iget-object v1, p0, Ljcu;->dME:Lixn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 21590
    :cond_2
    iget-object v0, p0, Ljcu;->eaY:Ljct;

    if-eqz v0, :cond_3

    .line 21591
    const/4 v0, 0x4

    iget-object v1, p0, Ljcu;->eaY:Ljct;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 21593
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 21594
    return-void
.end method

.method public final baV()Z
    .locals 1

    .prologue
    .line 21546
    iget v0, p0, Ljcu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21535
    iget-object v0, p0, Ljcu;->afb:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 21598
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 21599
    iget v1, p0, Ljcu;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 21600
    const/4 v1, 0x1

    iget-object v2, p0, Ljcu;->afb:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21603
    :cond_0
    iget-object v1, p0, Ljcu;->dQU:Liym;

    if-eqz v1, :cond_1

    .line 21604
    const/4 v1, 0x2

    iget-object v2, p0, Ljcu;->dQU:Liym;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21607
    :cond_1
    iget-object v1, p0, Ljcu;->dME:Lixn;

    if-eqz v1, :cond_2

    .line 21608
    const/4 v1, 0x3

    iget-object v2, p0, Ljcu;->dME:Lixn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21611
    :cond_2
    iget-object v1, p0, Ljcu;->eaY:Ljct;

    if-eqz v1, :cond_3

    .line 21612
    const/4 v1, 0x4

    iget-object v2, p0, Ljcu;->eaY:Ljct;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21615
    :cond_3
    return v0
.end method

.method public final tp(Ljava/lang/String;)Ljcu;
    .locals 1

    .prologue
    .line 21538
    if-nez p1, :cond_0

    .line 21539
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 21541
    :cond_0
    iput-object p1, p0, Ljcu;->afb:Ljava/lang/String;

    .line 21542
    iget v0, p0, Ljcu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcu;->aez:I

    .line 21543
    return-object p0
.end method
