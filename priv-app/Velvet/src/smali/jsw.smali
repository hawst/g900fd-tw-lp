.class public final Ljsw;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eCH:[Ljsw;


# instance fields
.field private aez:I

.field private eCI:J

.field private eCJ:I

.field private eCK:I

.field private eCL:Z

.field private eCM:I

.field private eCN:I

.field private eCO:Z

.field private eCP:Z

.field private eCQ:Z

.field private eCR:I

.field private eCS:J

.field private eCT:J

.field private eCU:I

.field private eCV:I

.field public eCW:[Ljsy;

.field private eCX:[Ljsx;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 946
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 947
    iput v0, p0, Ljsw;->aez:I

    iput-wide v2, p0, Ljsw;->eCI:J

    iput v0, p0, Ljsw;->eCJ:I

    iput v0, p0, Ljsw;->eCK:I

    iput-boolean v0, p0, Ljsw;->eCL:Z

    iput v0, p0, Ljsw;->eCM:I

    iput v0, p0, Ljsw;->eCN:I

    iput-boolean v0, p0, Ljsw;->eCO:Z

    iput-boolean v0, p0, Ljsw;->eCP:Z

    iput-boolean v0, p0, Ljsw;->eCQ:Z

    iput v0, p0, Ljsw;->eCR:I

    iput-wide v2, p0, Ljsw;->eCS:J

    iput-wide v2, p0, Ljsw;->eCT:J

    iput v0, p0, Ljsw;->eCU:I

    iput v0, p0, Ljsw;->eCV:I

    invoke-static {}, Ljsy;->bum()[Ljsy;

    move-result-object v0

    iput-object v0, p0, Ljsw;->eCW:[Ljsy;

    invoke-static {}, Ljsx;->bul()[Ljsx;

    move-result-object v0

    iput-object v0, p0, Ljsw;->eCX:[Ljsx;

    const/4 v0, 0x0

    iput-object v0, p0, Ljsw;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljsw;->eCz:I

    .line 948
    return-void
.end method

.method public static bug()[Ljsw;
    .locals 2

    .prologue
    .line 661
    sget-object v0, Ljsw;->eCH:[Ljsw;

    if-nez v0, :cond_1

    .line 662
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 664
    :try_start_0
    sget-object v0, Ljsw;->eCH:[Ljsw;

    if-nez v0, :cond_0

    .line 665
    const/4 v0, 0x0

    new-array v0, v0, [Ljsw;

    sput-object v0, Ljsw;->eCH:[Ljsw;

    .line 667
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 669
    :cond_1
    sget-object v0, Ljsw;->eCH:[Ljsw;

    return-object v0

    .line 667
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljsw;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljsw;->eCI:J

    iget v0, p0, Ljsw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljsw;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljsw;->eCJ:I

    iget v0, p0, Ljsw;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljsw;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljsw;->eCK:I

    iget v0, p0, Ljsw;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljsw;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljsw;->eCM:I

    iget v0, p0, Ljsw;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljsw;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljsw;->eCN:I

    iget v0, p0, Ljsw;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljsw;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljsw;->eCR:I

    iget v0, p0, Ljsw;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljsw;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljsw;->eCS:J

    iget v0, p0, Ljsw;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljsw;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljsw;->eCV:I

    iget v0, p0, Ljsw;->aez:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Ljsw;->aez:I

    goto :goto_0

    :sswitch_9
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljsw;->eCW:[Ljsy;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljsy;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljsw;->eCW:[Ljsy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljsy;

    invoke-direct {v3}, Ljsy;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljsw;->eCW:[Ljsy;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljsy;

    invoke-direct {v3}, Ljsy;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljsw;->eCW:[Ljsy;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_0

    :pswitch_1
    iput v0, p0, Ljsw;->eCU:I

    iget v0, p0, Ljsw;->aez:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Ljsw;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljsw;->eCL:Z

    iget v0, p0, Ljsw;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljsw;->aez:I

    goto/16 :goto_0

    :sswitch_c
    const/16 v0, 0x62

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljsw;->eCX:[Ljsx;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljsx;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljsw;->eCX:[Ljsx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Ljsx;

    invoke-direct {v3}, Ljsx;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljsw;->eCX:[Ljsx;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Ljsx;

    invoke-direct {v3}, Ljsx;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljsw;->eCX:[Ljsx;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljsw;->eCT:J

    iget v0, p0, Ljsw;->aez:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Ljsw;->aez:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljsw;->eCO:Z

    iget v0, p0, Ljsw;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljsw;->aez:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljsw;->eCP:Z

    iget v0, p0, Ljsw;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljsw;->aez:I

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljsw;->eCQ:Z

    iget v0, p0, Ljsw;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljsw;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 976
    iget v0, p0, Ljsw;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 977
    const/4 v0, 0x1

    iget-wide v2, p0, Ljsw;->eCI:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 979
    :cond_0
    iget v0, p0, Ljsw;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 980
    const/4 v0, 0x2

    iget v2, p0, Ljsw;->eCJ:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 982
    :cond_1
    iget v0, p0, Ljsw;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 983
    const/4 v0, 0x3

    iget v2, p0, Ljsw;->eCK:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 985
    :cond_2
    iget v0, p0, Ljsw;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_3

    .line 986
    const/4 v0, 0x4

    iget v2, p0, Ljsw;->eCM:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 988
    :cond_3
    iget v0, p0, Ljsw;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_4

    .line 989
    const/4 v0, 0x5

    iget v2, p0, Ljsw;->eCN:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 991
    :cond_4
    iget v0, p0, Ljsw;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_5

    .line 992
    const/4 v0, 0x6

    iget v2, p0, Ljsw;->eCR:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 994
    :cond_5
    iget v0, p0, Ljsw;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_6

    .line 995
    const/4 v0, 0x7

    iget-wide v2, p0, Ljsw;->eCS:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 997
    :cond_6
    iget v0, p0, Ljsw;->aez:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_7

    .line 998
    const/16 v0, 0x8

    iget v2, p0, Ljsw;->eCV:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 1000
    :cond_7
    iget-object v0, p0, Ljsw;->eCW:[Ljsy;

    if-eqz v0, :cond_9

    iget-object v0, p0, Ljsw;->eCW:[Ljsy;

    array-length v0, v0

    if-lez v0, :cond_9

    move v0, v1

    .line 1001
    :goto_0
    iget-object v2, p0, Ljsw;->eCW:[Ljsy;

    array-length v2, v2

    if-ge v0, v2, :cond_9

    .line 1002
    iget-object v2, p0, Ljsw;->eCW:[Ljsy;

    aget-object v2, v2, v0

    .line 1003
    if-eqz v2, :cond_8

    .line 1004
    const/16 v3, 0x9

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 1001
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1008
    :cond_9
    iget v0, p0, Ljsw;->aez:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_a

    .line 1009
    const/16 v0, 0xa

    iget v2, p0, Ljsw;->eCU:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 1011
    :cond_a
    iget v0, p0, Ljsw;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_b

    .line 1012
    const/16 v0, 0xb

    iget-boolean v2, p0, Ljsw;->eCL:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 1014
    :cond_b
    iget-object v0, p0, Ljsw;->eCX:[Ljsx;

    if-eqz v0, :cond_d

    iget-object v0, p0, Ljsw;->eCX:[Ljsx;

    array-length v0, v0

    if-lez v0, :cond_d

    .line 1015
    :goto_1
    iget-object v0, p0, Ljsw;->eCX:[Ljsx;

    array-length v0, v0

    if-ge v1, v0, :cond_d

    .line 1016
    iget-object v0, p0, Ljsw;->eCX:[Ljsx;

    aget-object v0, v0, v1

    .line 1017
    if-eqz v0, :cond_c

    .line 1018
    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 1015
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1022
    :cond_d
    iget v0, p0, Ljsw;->aez:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_e

    .line 1023
    const/16 v0, 0xd

    iget-wide v2, p0, Ljsw;->eCT:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 1025
    :cond_e
    iget v0, p0, Ljsw;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_f

    .line 1026
    const/16 v0, 0xe

    iget-boolean v1, p0, Ljsw;->eCO:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 1028
    :cond_f
    iget v0, p0, Ljsw;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_10

    .line 1029
    const/16 v0, 0xf

    iget-boolean v1, p0, Ljsw;->eCP:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 1031
    :cond_10
    iget v0, p0, Ljsw;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_11

    .line 1032
    const/16 v0, 0x10

    iget-boolean v1, p0, Ljsw;->eCQ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 1034
    :cond_11
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1035
    return-void
.end method

.method public final buh()I
    .locals 1

    .prologue
    .line 715
    iget v0, p0, Ljsw;->eCK:I

    return v0
.end method

.method public final bui()I
    .locals 1

    .prologue
    .line 772
    iget v0, p0, Ljsw;->eCN:I

    return v0
.end method

.method public final buj()J
    .locals 2

    .prologue
    .line 867
    iget-wide v0, p0, Ljsw;->eCS:J

    return-wide v0
.end method

.method public final buk()I
    .locals 1

    .prologue
    .line 905
    iget v0, p0, Ljsw;->eCU:I

    return v0
.end method

.method public final dL(J)Ljsw;
    .locals 1

    .prologue
    .line 870
    iput-wide p1, p0, Ljsw;->eCS:J

    .line 871
    iget v0, p0, Ljsw;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljsw;->aez:I

    .line 872
    return-object p0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1039
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1040
    iget v2, p0, Ljsw;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 1041
    const/4 v2, 0x1

    iget-wide v4, p0, Ljsw;->eCI:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 1044
    :cond_0
    iget v2, p0, Ljsw;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 1045
    const/4 v2, 0x2

    iget v3, p0, Ljsw;->eCJ:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1048
    :cond_1
    iget v2, p0, Ljsw;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    .line 1049
    const/4 v2, 0x3

    iget v3, p0, Ljsw;->eCK:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1052
    :cond_2
    iget v2, p0, Ljsw;->aez:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_3

    .line 1053
    const/4 v2, 0x4

    iget v3, p0, Ljsw;->eCM:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1056
    :cond_3
    iget v2, p0, Ljsw;->aez:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_4

    .line 1057
    const/4 v2, 0x5

    iget v3, p0, Ljsw;->eCN:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1060
    :cond_4
    iget v2, p0, Ljsw;->aez:I

    and-int/lit16 v2, v2, 0x200

    if-eqz v2, :cond_5

    .line 1061
    const/4 v2, 0x6

    iget v3, p0, Ljsw;->eCR:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1064
    :cond_5
    iget v2, p0, Ljsw;->aez:I

    and-int/lit16 v2, v2, 0x400

    if-eqz v2, :cond_6

    .line 1065
    const/4 v2, 0x7

    iget-wide v4, p0, Ljsw;->eCS:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 1068
    :cond_6
    iget v2, p0, Ljsw;->aez:I

    and-int/lit16 v2, v2, 0x2000

    if-eqz v2, :cond_7

    .line 1069
    const/16 v2, 0x8

    iget v3, p0, Ljsw;->eCV:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1072
    :cond_7
    iget-object v2, p0, Ljsw;->eCW:[Ljsy;

    if-eqz v2, :cond_a

    iget-object v2, p0, Ljsw;->eCW:[Ljsy;

    array-length v2, v2

    if-lez v2, :cond_a

    move v2, v0

    move v0, v1

    .line 1073
    :goto_0
    iget-object v3, p0, Ljsw;->eCW:[Ljsy;

    array-length v3, v3

    if-ge v0, v3, :cond_9

    .line 1074
    iget-object v3, p0, Ljsw;->eCW:[Ljsy;

    aget-object v3, v3, v0

    .line 1075
    if-eqz v3, :cond_8

    .line 1076
    const/16 v4, 0x9

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1073
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_9
    move v0, v2

    .line 1081
    :cond_a
    iget v2, p0, Ljsw;->aez:I

    and-int/lit16 v2, v2, 0x1000

    if-eqz v2, :cond_b

    .line 1082
    const/16 v2, 0xa

    iget v3, p0, Ljsw;->eCU:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1085
    :cond_b
    iget v2, p0, Ljsw;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_c

    .line 1086
    const/16 v2, 0xb

    iget-boolean v3, p0, Ljsw;->eCL:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1089
    :cond_c
    iget-object v2, p0, Ljsw;->eCX:[Ljsx;

    if-eqz v2, :cond_e

    iget-object v2, p0, Ljsw;->eCX:[Ljsx;

    array-length v2, v2

    if-lez v2, :cond_e

    .line 1090
    :goto_1
    iget-object v2, p0, Ljsw;->eCX:[Ljsx;

    array-length v2, v2

    if-ge v1, v2, :cond_e

    .line 1091
    iget-object v2, p0, Ljsw;->eCX:[Ljsx;

    aget-object v2, v2, v1

    .line 1092
    if-eqz v2, :cond_d

    .line 1093
    const/16 v3, 0xc

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1090
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1098
    :cond_e
    iget v1, p0, Ljsw;->aez:I

    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_f

    .line 1099
    const/16 v1, 0xd

    iget-wide v2, p0, Ljsw;->eCT:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1102
    :cond_f
    iget v1, p0, Ljsw;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_10

    .line 1103
    const/16 v1, 0xe

    iget-boolean v2, p0, Ljsw;->eCO:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1106
    :cond_10
    iget v1, p0, Ljsw;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_11

    .line 1107
    const/16 v1, 0xf

    iget-boolean v2, p0, Ljsw;->eCP:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1110
    :cond_11
    iget v1, p0, Ljsw;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_12

    .line 1111
    const/16 v1, 0x10

    iget-boolean v2, p0, Ljsw;->eCQ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1114
    :cond_12
    return v0
.end method

.method public final sm(I)Ljsw;
    .locals 1

    .prologue
    .line 718
    iput p1, p0, Ljsw;->eCK:I

    .line 719
    iget v0, p0, Ljsw;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljsw;->aez:I

    .line 720
    return-object p0
.end method

.method public final sn(I)Ljsw;
    .locals 1

    .prologue
    .line 775
    iput p1, p0, Ljsw;->eCN:I

    .line 776
    iget v0, p0, Ljsw;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljsw;->aez:I

    .line 777
    return-object p0
.end method

.method public final so(I)Ljsw;
    .locals 1

    .prologue
    .line 908
    iput p1, p0, Ljsw;->eCU:I

    .line 909
    iget v0, p0, Ljsw;->aez:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Ljsw;->aez:I

    .line 910
    return-object p0
.end method
