.class public Lgju;
.super Lcom/google/speech/recognizer/AbstractRecognizer;
.source "PG"


# static fields
.field private static cMX:Z


# instance fields
.field private final bhu:I

.field private cMT:I

.field private final cMY:Lgjv;

.field private cal:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    sput-boolean v0, Lgju;->cMX:Z

    return-void
.end method

.method private constructor <init>(II)V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/speech/recognizer/AbstractRecognizer;-><init>()V

    .line 83
    new-instance v0, Lgjv;

    invoke-direct {v0}, Lgjv;-><init>()V

    iput-object v0, p0, Lgju;->cMY:Lgjv;

    .line 84
    iput p1, p0, Lgju;->cMT:I

    .line 85
    iput p2, p0, Lgju;->bhu:I

    .line 86
    iget-object v0, p0, Lgju;->cMY:Lgjv;

    invoke-virtual {p0, v0}, Lgju;->addCallback(Ljvj;)I

    .line 87
    return-void
.end method

.method public static a(Lgjk;II)Lgju;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 48
    new-instance v1, Lgju;

    invoke-direct {v1, p1, p2}, Lgju;-><init>(II)V

    .line 50
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lgjk;->cMw:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 53
    invoke-static {v2}, Lgjo;->k(Ljava/io/File;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 55
    iget-object v2, p0, Lgjk;->cMw:Ljava/lang/String;

    iget-object v3, p0, Lgjk;->cMv:Lgjw;

    invoke-virtual {v1, v2, v3}, Lgju;->initFromFile(Ljava/lang/String;Lcom/google/speech/recognizer/ResourceManager;)I

    move-result v2

    .line 68
    :goto_0
    if-nez v2, :cond_3

    move-object v0, v1

    .line 73
    :goto_1
    return-object v0

    .line 59
    :cond_0
    invoke-static {v2}, Lgju;->l(Ljava/io/File;)[B

    move-result-object v2

    .line 60
    if-eqz v2, :cond_1

    array-length v3, v2

    if-nez v3, :cond_2

    .line 61
    :cond_1
    const-string v1, "Greco3Recognizer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error reading g3 config file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lgjk;->cMw:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 65
    :cond_2
    iget-object v3, p0, Lgjk;->cMv:Lgjw;

    invoke-virtual {v1, v2, v3}, Lgju;->initFromProto([BLcom/google/speech/recognizer/ResourceManager;)I

    move-result v2

    goto :goto_0

    .line 72
    :cond_3
    const-string v1, "Greco3Recognizer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to bring up g3, Status code: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static declared-synchronized aGA()V
    .locals 2

    .prologue
    .line 31
    const-class v1, Lgju;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lgju;->cMX:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 44
    :goto_0
    monitor-exit v1

    return-void

    .line 36
    :cond_0
    :try_start_1
    const-string v0, "google_recognizer_jni_l"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 42
    :goto_1
    :try_start_2
    invoke-static {}, Lgju;->nativeInit()V

    .line 43
    const/4 v0, 0x1

    sput-boolean v0, Lgju;->cMX:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 39
    :catch_0
    move-exception v0

    :try_start_3
    const-string v0, "google_recognizer_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private static l(Ljava/io/File;)[B
    .locals 1

    .prologue
    .line 135
    :try_start_0
    invoke-static {p0}, Leoo;->e(Ljava/io/File;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 137
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lgiu;)V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lgju;->cMY:Lgjv;

    iput-object p1, v0, Lgjv;->cMZ:Lgiu;

    .line 95
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lgju;->cal:J

    .line 96
    return-void
.end method

.method public cancel()I
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lgju;->cMY:Lgjv;

    const/4 v1, 0x0

    iput-object v1, v0, Lgjv;->cMZ:Lgiu;

    .line 127
    invoke-super {p0}, Lcom/google/speech/recognizer/AbstractRecognizer;->cancel()I

    move-result v0

    return v0
.end method

.method public final kn(I)V
    .locals 0

    .prologue
    .line 90
    iput p1, p0, Lgju;->cMT:I

    .line 91
    return-void
.end method

.method protected read([B)I
    .locals 6

    .prologue
    .line 102
    :try_start_0
    invoke-super {p0, p1}, Lcom/google/speech/recognizer/AbstractRecognizer;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 108
    if-lez v0, :cond_0

    .line 109
    iget-wide v2, p0, Lgju;->cal:J

    mul-int/lit16 v1, v0, 0x3e8

    iget v4, p0, Lgju;->bhu:I

    iget v5, p0, Lgju;->cMT:I

    mul-int/2addr v4, v5

    div-int/2addr v1, v4

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lgju;->cal:J

    .line 116
    iget-wide v2, p0, Lgju;->cal:J

    const-wide/16 v4, 0xc8

    rem-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 117
    iget-object v1, p0, Lgju;->cMY:Lgjv;

    iget-wide v2, p0, Lgju;->cal:J

    iget-object v4, v1, Lgjv;->cMZ:Lgiu;

    if-eqz v4, :cond_0

    iget-object v1, v1, Lgjv;->cMZ:Lgiu;

    invoke-interface {v1, v2, v3}, Lgiu;->bA(J)V

    .line 121
    :cond_0
    return v0

    .line 103
    :catch_0
    move-exception v0

    .line 104
    iget-object v1, p0, Lgju;->cMY:Lgjv;

    new-instance v2, Lehx;

    const v3, 0x70006

    invoke-direct {v2, v0, v3}, Lehx;-><init>(Ljava/lang/Throwable;I)V

    iget-object v3, v1, Lgjv;->cMZ:Lgiu;

    if-eqz v3, :cond_1

    iget-object v1, v1, Lgjv;->cMZ:Lgiu;

    invoke-interface {v1, v2}, Lgiu;->l(Leiq;)V

    .line 105
    :cond_1
    throw v0
.end method
