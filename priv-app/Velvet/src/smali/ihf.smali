.class Lihf;
.super Ljava/util/AbstractMap;
.source "PG"


# instance fields
.field final synthetic dCh:Lihb;

.field final transient dCi:Ljava/util/Map;

.field private transient dCj:Ljava/util/Set;


# direct methods
.method constructor <init>(Lihb;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 1173
    iput-object p1, p0, Lihf;->dCh:Lihb;

    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 1174
    iput-object p2, p0, Lihf;->dCi:Ljava/util/Map;

    .line 1175
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 1236
    iget-object v0, p0, Lihf;->dCi:Ljava/util/Map;

    iget-object v1, p0, Lihf;->dCh:Lihb;

    invoke-static {v1}, Lihb;->a(Lihb;)Ljava/util/Map;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 1237
    iget-object v0, p0, Lihf;->dCh:Lihb;

    invoke-virtual {v0}, Lihb;->clear()V

    .line 1242
    :goto_0
    return-void

    .line 1240
    :cond_0
    new-instance v0, Lihh;

    invoke-direct {v0, p0}, Lihh;-><init>(Lihf;)V

    invoke-static {v0}, Likr;->f(Ljava/util/Iterator;)V

    goto :goto_0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1187
    iget-object v0, p0, Lihf;->dCi:Ljava/util/Map;

    invoke-static {v0, p1}, Lior;->b(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1180
    iget-object v0, p0, Lihf;->dCj:Ljava/util/Set;

    .line 1181
    if-nez v0, :cond_0

    new-instance v0, Lihg;

    invoke-direct {v0, p0}, Lihg;-><init>(Lihf;)V

    iput-object v0, p0, Lihf;->dCj:Ljava/util/Set;

    :cond_0
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1223
    if-eq p0, p1, :cond_0

    iget-object v0, p0, Lihf;->dCi:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1166
    iget-object v0, p0, Lihf;->dCi:Ljava/util/Map;

    invoke-static {v0, p1}, Lior;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lihf;->dCh:Lihb;

    invoke-static {v1, p1, v0}, Lihb;->a(Lihb;Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1227
    iget-object v0, p0, Lihf;->dCi:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    return v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1201
    iget-object v0, p0, Lihf;->dCh:Lihb;

    invoke-virtual {v0}, Lihb;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1166
    iget-object v0, p0, Lihf;->dCi:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lihf;->dCh:Lihb;

    invoke-virtual {v1}, Lihb;->aWm()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    iget-object v2, p0, Lihf;->dCh:Lihb;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-static {v2, v3}, Lihb;->b(Lihb;I)I

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    move-object v0, v1

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 1206
    iget-object v0, p0, Lihf;->dCi:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1231
    iget-object v0, p0, Lihf;->dCi:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
