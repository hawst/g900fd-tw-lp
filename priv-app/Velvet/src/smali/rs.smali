.class public final Lrs;
.super Landroid/widget/FrameLayout;
.source "PG"


# static fields
.field private static iA:Landroid/graphics/Rect;


# instance fields
.field private hX:I

.field private hY:I

.field private xA:Z

.field private xB:Z

.field private xC:Z

.field private xD:I

.field private xE:I

.field private xF:I

.field private xG:I

.field private xH:I

.field private xI:I

.field private xJ:I

.field private xK:I

.field private xL:I

.field private xM:I

.field private xN:I

.field private xO:I

.field private xP:I

.field private xQ:I

.field private xR:I

.field private xS:I

.field private xT:I

.field private xU:I

.field private xV:I

.field private xW:[I

.field private xX:[I

.field private xY:[I

.field private xZ:Lcom/android/launcher3/Launcher;

.field private xs:Lyx;

.field private xt:Lcom/android/launcher3/CellLayout;

.field private xu:Lcom/android/launcher3/DragLayer;

.field private xv:Landroid/widget/ImageView;

.field private xw:Landroid/widget/ImageView;

.field private xx:Landroid/widget/ImageView;

.field private xy:Landroid/widget/ImageView;

.field private xz:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lrs;->iA:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lyx;Lcom/android/launcher3/CellLayout;Lcom/android/launcher3/DragLayer;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, -0x2

    .line 77
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 53
    iput v3, p0, Lrs;->xU:I

    .line 54
    iput v3, p0, Lrs;->xV:I

    .line 56
    new-array v0, v4, [I

    iput-object v0, p0, Lrs;->xW:[I

    .line 57
    new-array v0, v4, [I

    iput-object v0, p0, Lrs;->xX:[I

    .line 58
    new-array v0, v4, [I

    iput-object v0, p0, Lrs;->xY:[I

    move-object v0, p1

    .line 60
    check-cast v0, Lcom/android/launcher3/Launcher;

    iput-object v0, p0, Lrs;->xZ:Lcom/android/launcher3/Launcher;

    .line 79
    iput-object p3, p0, Lrs;->xt:Lcom/android/launcher3/CellLayout;

    .line 80
    iput-object p2, p0, Lrs;->xs:Lyx;

    .line 81
    invoke-virtual {p2}, Lyx;->getAppWidgetInfo()Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v0

    iget v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->resizeMode:I

    iput v0, p0, Lrs;->xL:I

    .line 82
    iput-object p4, p0, Lrs;->xu:Lcom/android/launcher3/DragLayer;

    .line 84
    invoke-virtual {p2}, Lyx;->getAppWidgetInfo()Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lrs;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v1, v0}, Lcom/android/launcher3/Launcher;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I

    move-result-object v0

    .line 86
    aget v1, v0, v3

    iput v1, p0, Lrs;->xO:I

    .line 87
    aget v0, v0, v6

    iput v0, p0, Lrs;->xP:I

    .line 89
    const v0, 0x7f020318

    invoke-virtual {p0, v0}, Lrs;->setBackgroundResource(I)V

    .line 90
    invoke-virtual {p0, v3, v3, v3, v3}, Lrs;->setPadding(IIII)V

    .line 93
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lrs;->xv:Landroid/widget/ImageView;

    .line 94
    iget-object v0, p0, Lrs;->xv:Landroid/widget/ImageView;

    const v1, 0x7f02031a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 95
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x13

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 97
    iget-object v1, p0, Lrs;->xv:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v0}, Lrs;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 99
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lrs;->xw:Landroid/widget/ImageView;

    .line 100
    iget-object v0, p0, Lrs;->xw:Landroid/widget/ImageView;

    const v1, 0x7f02031b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 101
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x15

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 103
    iget-object v1, p0, Lrs;->xw:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v0}, Lrs;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 105
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lrs;->xx:Landroid/widget/ImageView;

    .line 106
    iget-object v0, p0, Lrs;->xx:Landroid/widget/ImageView;

    const v1, 0x7f02031c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 107
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x31

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 109
    iget-object v1, p0, Lrs;->xx:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v0}, Lrs;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 111
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lrs;->xy:Landroid/widget/ImageView;

    .line 112
    iget-object v0, p0, Lrs;->xy:Landroid/widget/ImageView;

    const v1, 0x7f020319

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 113
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x51

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 115
    iget-object v1, p0, Lrs;->xy:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v0}, Lrs;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 117
    invoke-virtual {p2}, Lyx;->getAppWidgetInfo()Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/appwidget/AppWidgetHostView;->getDefaultPaddingForWidget(Landroid/content/Context;Landroid/content/ComponentName;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    .line 119
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iput v1, p0, Lrs;->xD:I

    .line 120
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iput v1, p0, Lrs;->xF:I

    .line 121
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iput v1, p0, Lrs;->xE:I

    .line 122
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iput v0, p0, Lrs;->xG:I

    .line 124
    iget v0, p0, Lrs;->xL:I

    if-ne v0, v6, :cond_1

    .line 125
    iget-object v0, p0, Lrs;->xx:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lrs;->xy:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 132
    :cond_0
    :goto_0
    iget-object v0, p0, Lrs;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 133
    const/high16 v1, 0x41c00000    # 24.0f

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lrs;->xS:I

    .line 134
    iget v0, p0, Lrs;->xS:I

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lrs;->xT:I

    .line 139
    iget-object v0, p0, Lrs;->xt:Lcom/android/launcher3/CellLayout;

    iget-object v1, p0, Lrs;->xs:Lyx;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CellLayout;->K(Landroid/view/View;)V

    .line 140
    return-void

    .line 127
    :cond_1
    iget v0, p0, Lrs;->xL:I

    if-ne v0, v4, :cond_0

    .line 128
    iget-object v0, p0, Lrs;->xv:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lrs;->xw:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a(Lcom/android/launcher3/Launcher;IILandroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 8

    .prologue
    .line 345
    if-nez p3, :cond_0

    .line 346
    new-instance p3, Landroid/graphics/Rect;

    invoke-direct {p3}, Landroid/graphics/Rect;-><init>()V

    .line 348
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/android/launcher3/Workspace;->a(Lcom/android/launcher3/Launcher;I)Landroid/graphics/Rect;

    move-result-object v0

    .line 349
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/android/launcher3/Workspace;->a(Lcom/android/launcher3/Launcher;I)Landroid/graphics/Rect;

    move-result-object v1

    .line 350
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 353
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 354
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 355
    iget v5, v0, Landroid/graphics/Rect;->right:I

    .line 356
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 357
    mul-int/2addr v3, p1

    add-int/lit8 v6, p1, -0x1

    mul-int/2addr v5, v6

    add-int/2addr v3, v5

    int-to-float v3, v3

    div-float/2addr v3, v2

    float-to-int v3, v3

    .line 358
    mul-int/2addr v4, p2

    add-int/lit8 v5, p2, -0x1

    mul-int/2addr v0, v5

    add-int/2addr v0, v4

    int-to-float v0, v0

    div-float/2addr v0, v2

    float-to-int v0, v0

    .line 361
    iget v4, v1, Landroid/graphics/Rect;->left:I

    .line 362
    iget v5, v1, Landroid/graphics/Rect;->top:I

    .line 363
    iget v6, v1, Landroid/graphics/Rect;->right:I

    .line 364
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    .line 365
    mul-int/2addr v4, p1

    add-int/lit8 v7, p1, -0x1

    mul-int/2addr v6, v7

    add-int/2addr v4, v6

    int-to-float v4, v4

    div-float/2addr v4, v2

    float-to-int v4, v4

    .line 366
    mul-int/2addr v5, p2

    add-int/lit8 v6, p2, -0x1

    mul-int/2addr v1, v6

    add-int/2addr v1, v5

    int-to-float v1, v1

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 367
    invoke-virtual {p3, v4, v0, v3, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 368
    return-object p3
.end method

.method public static a(Landroid/appwidget/AppWidgetHostView;Lcom/android/launcher3/Launcher;II)V
    .locals 6

    .prologue
    .line 339
    sget-object v0, Lrs;->iA:Landroid/graphics/Rect;

    invoke-static {p1, p2, p3, v0}, Lrs;->a(Lcom/android/launcher3/Launcher;IILandroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 340
    const/4 v1, 0x0

    sget-object v0, Lrs;->iA:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    sget-object v0, Lrs;->iA:Landroid/graphics/Rect;

    iget v3, v0, Landroid/graphics/Rect;->top:I

    sget-object v0, Lrs;->iA:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->right:I

    sget-object v0, Lrs;->iA:Landroid/graphics/Rect;

    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/appwidget/AppWidgetHostView;->updateAppWidgetSize(Landroid/os/Bundle;IIII)V

    .line 342
    return-void
.end method

.method private v(Z)V
    .locals 14

    .prologue
    .line 224
    iget-object v0, p0, Lrs;->xt:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eV()I

    move-result v0

    iget-object v1, p0, Lrs;->xt:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher3/CellLayout;->eX()I

    move-result v1

    add-int/2addr v0, v1

    .line 225
    iget-object v1, p0, Lrs;->xt:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher3/CellLayout;->eW()I

    move-result v1

    iget-object v2, p0, Lrs;->xt:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v2}, Lcom/android/launcher3/CellLayout;->eY()I

    move-result v2

    add-int/2addr v1, v2

    .line 227
    iget v2, p0, Lrs;->hX:I

    iget v3, p0, Lrs;->xQ:I

    add-int/2addr v2, v3

    .line 228
    iget v3, p0, Lrs;->hY:I

    iget v4, p0, Lrs;->xR:I

    add-int/2addr v3, v4

    .line 230
    const/high16 v4, 0x3f800000    # 1.0f

    int-to-float v2, v2

    mul-float/2addr v2, v4

    int-to-float v0, v0

    div-float v0, v2, v0

    iget v2, p0, Lrs;->xM:I

    int-to-float v2, v2

    sub-float v2, v0, v2

    .line 231
    const/high16 v0, 0x3f800000    # 1.0f

    int-to-float v3, v3

    mul-float/2addr v0, v3

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lrs;->xN:I

    int-to-float v1, v1

    sub-float v3, v0, v1

    .line 233
    const/4 v0, 0x0

    .line 234
    const/4 v7, 0x0

    .line 235
    const/4 v4, 0x0

    .line 236
    const/4 v6, 0x0

    .line 238
    iget-object v1, p0, Lrs;->xt:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher3/CellLayout;->eT()I

    move-result v9

    .line 239
    iget-object v1, p0, Lrs;->xt:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher3/CellLayout;->eU()I

    move-result v10

    .line 241
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v5, 0x3f28f5c3    # 0.66f

    cmpl-float v1, v1, v5

    if-lez v1, :cond_14

    .line 242
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v0

    move v1, v0

    .line 244
    :goto_0
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v2, 0x3f28f5c3    # 0.66f

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 245
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 248
    :cond_0
    if-nez p1, :cond_2

    if-nez v1, :cond_2

    if-nez v7, :cond_2

    .line 334
    :cond_1
    :goto_1
    return-void

    .line 251
    :cond_2
    iget-object v0, p0, Lrs;->xs:Lyx;

    invoke-virtual {v0}, Lyx;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 253
    iget v11, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    .line 254
    iget v12, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    .line 255
    iget-boolean v0, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Bp:Z

    if-eqz v0, :cond_9

    iget v0, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Bn:I

    .line 256
    :goto_2
    iget-boolean v2, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Bp:Z

    if-eqz v2, :cond_a

    iget v2, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Bo:I

    .line 258
    :goto_3
    const/4 v3, 0x0

    .line 259
    const/4 v5, 0x0

    .line 263
    iget-boolean v13, p0, Lrs;->xz:Z

    if-eqz v13, :cond_b

    .line 264
    neg-int v3, v0

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 265
    iget v4, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    iget v9, p0, Lrs;->xO:I

    sub-int/2addr v4, v9

    invoke-static {v4, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 266
    mul-int/lit8 v1, v1, -0x1

    .line 267
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 268
    iget v4, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    iget v9, p0, Lrs;->xO:I

    sub-int/2addr v4, v9

    neg-int v4, v4

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 269
    neg-int v1, v4

    move v9, v1

    move v1, v3

    move v3, v4

    .line 277
    :goto_4
    iget-boolean v4, p0, Lrs;->xB:Z

    if-eqz v4, :cond_c

    .line 278
    neg-int v4, v2

    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 279
    iget v5, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    iget v6, p0, Lrs;->xP:I

    sub-int/2addr v5, v6

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 280
    mul-int/lit8 v4, v7, -0x1

    .line 281
    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 282
    iget v6, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    iget v7, p0, Lrs;->xP:I

    sub-int/2addr v6, v7

    neg-int v6, v6

    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 283
    neg-int v4, v6

    move v10, v4

    move v4, v5

    move v5, v6

    .line 290
    :goto_5
    iget-object v6, p0, Lrs;->xW:[I

    const/4 v7, 0x0

    const/4 v13, 0x0

    aput v13, v6, v7

    .line 291
    iget-object v6, p0, Lrs;->xW:[I

    const/4 v7, 0x1

    const/4 v13, 0x0

    aput v13, v6, v7

    .line 293
    iget-boolean v6, p0, Lrs;->xz:Z

    if-nez v6, :cond_3

    iget-boolean v6, p0, Lrs;->xA:Z

    if-eqz v6, :cond_11

    .line 294
    :cond_3
    add-int/2addr v3, v11

    .line 295
    add-int/2addr v1, v0

    .line 296
    if-eqz v9, :cond_4

    .line 297
    iget-object v6, p0, Lrs;->xW:[I

    const/4 v7, 0x0

    iget-boolean v0, p0, Lrs;->xz:Z

    if-eqz v0, :cond_d

    const/4 v0, -0x1

    :goto_6
    aput v0, v6, v7

    .line 301
    :cond_4
    :goto_7
    iget-boolean v0, p0, Lrs;->xB:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lrs;->xC:Z

    if-eqz v0, :cond_10

    .line 302
    :cond_5
    add-int/2addr v5, v12

    .line 303
    add-int/2addr v2, v4

    .line 304
    if-eqz v10, :cond_6

    .line 305
    iget-object v4, p0, Lrs;->xW:[I

    const/4 v6, 0x1

    iget-boolean v0, p0, Lrs;->xB:Z

    if-eqz v0, :cond_e

    const/4 v0, -0x1

    :goto_8
    aput v0, v4, v6

    :cond_6
    move v4, v5

    .line 309
    :goto_9
    if-nez p1, :cond_7

    if-nez v10, :cond_7

    if-eqz v9, :cond_1

    .line 313
    :cond_7
    if-eqz p1, :cond_f

    .line 314
    iget-object v0, p0, Lrs;->xW:[I

    const/4 v5, 0x0

    iget-object v6, p0, Lrs;->xX:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    aput v6, v0, v5

    .line 315
    iget-object v0, p0, Lrs;->xW:[I

    const/4 v5, 0x1

    iget-object v6, p0, Lrs;->xX:[I

    const/4 v7, 0x1

    aget v6, v6, v7

    aput v6, v0, v5

    .line 321
    :goto_a
    iget-object v0, p0, Lrs;->xt:Lcom/android/launcher3/CellLayout;

    iget-object v5, p0, Lrs;->xs:Lyx;

    iget-object v6, p0, Lrs;->xW:[I

    move v7, p1

    invoke-virtual/range {v0 .. v7}, Lcom/android/launcher3/CellLayout;->a(IIIILandroid/view/View;[IZ)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 323
    iput v1, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Bn:I

    .line 324
    iput v2, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Bo:I

    .line 325
    iput v3, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    .line 326
    iput v4, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    .line 327
    iget v0, p0, Lrs;->xN:I

    add-int/2addr v0, v10

    iput v0, p0, Lrs;->xN:I

    .line 328
    iget v0, p0, Lrs;->xM:I

    add-int/2addr v0, v9

    iput v0, p0, Lrs;->xM:I

    .line 329
    if-nez p1, :cond_8

    .line 330
    iget-object v0, p0, Lrs;->xs:Lyx;

    iget-object v1, p0, Lrs;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v0, v1, v3, v4}, Lrs;->a(Landroid/appwidget/AppWidgetHostView;Lcom/android/launcher3/Launcher;II)V

    .line 333
    :cond_8
    iget-object v0, p0, Lrs;->xs:Lyx;

    invoke-virtual {v0}, Lyx;->requestLayout()V

    goto/16 :goto_1

    .line 255
    :cond_9
    iget v0, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    goto/16 :goto_2

    .line 256
    :cond_a
    iget v2, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    goto/16 :goto_3

    .line 271
    :cond_b
    iget-boolean v13, p0, Lrs;->xA:Z

    if-eqz v13, :cond_13

    .line 272
    add-int v3, v0, v11

    sub-int v3, v9, v3

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 273
    iget v3, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    iget v9, p0, Lrs;->xO:I

    sub-int/2addr v3, v9

    neg-int v3, v3

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v9, v1

    move v3, v1

    move v1, v4

    .line 274
    goto/16 :goto_4

    .line 284
    :cond_c
    iget-boolean v4, p0, Lrs;->xC:Z

    if-eqz v4, :cond_12

    .line 285
    add-int v4, v2, v12

    sub-int v4, v10, v4

    invoke-static {v4, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 286
    iget v5, v8, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    iget v7, p0, Lrs;->xP:I

    sub-int/2addr v5, v7

    neg-int v5, v5

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    move v10, v4

    move v5, v4

    move v4, v6

    .line 287
    goto/16 :goto_5

    .line 297
    :cond_d
    const/4 v0, 0x1

    goto/16 :goto_6

    .line 305
    :cond_e
    const/4 v0, 0x1

    goto/16 :goto_8

    .line 317
    :cond_f
    iget-object v0, p0, Lrs;->xX:[I

    const/4 v5, 0x0

    iget-object v6, p0, Lrs;->xW:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    aput v6, v0, v5

    .line 318
    iget-object v0, p0, Lrs;->xX:[I

    const/4 v5, 0x1

    iget-object v6, p0, Lrs;->xW:[I

    const/4 v7, 0x1

    aget v6, v6, v7

    aput v6, v0, v5

    goto/16 :goto_a

    :cond_10
    move v4, v12

    goto/16 :goto_9

    :cond_11
    move v1, v0

    move v3, v11

    goto/16 :goto_7

    :cond_12
    move v10, v5

    move v4, v6

    move v5, v7

    goto/16 :goto_5

    :cond_13
    move v9, v3

    move v3, v1

    move v1, v4

    goto/16 :goto_4

    :cond_14
    move v1, v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final dY()V
    .locals 1

    .prologue
    .line 376
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lrs;->v(Z)V

    .line 377
    invoke-virtual {p0}, Lrs;->requestLayout()V

    .line 378
    return-void
.end method

.method public final dZ()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 381
    iget-object v0, p0, Lrs;->xt:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eV()I

    move-result v0

    iget-object v1, p0, Lrs;->xt:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher3/CellLayout;->eX()I

    move-result v1

    add-int/2addr v0, v1

    .line 382
    iget-object v1, p0, Lrs;->xt:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher3/CellLayout;->eW()I

    move-result v1

    iget-object v2, p0, Lrs;->xt:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v2}, Lcom/android/launcher3/CellLayout;->eY()I

    move-result v2

    add-int/2addr v1, v2

    .line 384
    iget v2, p0, Lrs;->xM:I

    mul-int/2addr v0, v2

    iput v0, p0, Lrs;->xQ:I

    .line 385
    iget v0, p0, Lrs;->xN:I

    mul-int/2addr v0, v1

    iput v0, p0, Lrs;->xR:I

    .line 386
    iput v3, p0, Lrs;->hX:I

    .line 387
    iput v3, p0, Lrs;->hY:I

    .line 389
    new-instance v0, Lrt;

    invoke-direct {v0, p0}, Lrt;-><init>(Lrs;)V

    invoke-virtual {p0, v0}, Lrs;->post(Ljava/lang/Runnable;)Z

    .line 395
    return-void
.end method

.method public final w(Z)V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 398
    invoke-virtual {p0}, Lrs;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/DragLayer$LayoutParams;

    .line 399
    iget-object v1, p0, Lrs;->xs:Lyx;

    invoke-virtual {v1}, Lyx;->getWidth()I

    move-result v1

    iget v2, p0, Lrs;->xS:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iget v2, p0, Lrs;->xD:I

    sub-int/2addr v1, v2

    iget v2, p0, Lrs;->xE:I

    sub-int/2addr v1, v2

    .line 401
    iget-object v2, p0, Lrs;->xs:Lyx;

    invoke-virtual {v2}, Lyx;->getHeight()I

    move-result v2

    iget v3, p0, Lrs;->xS:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget v3, p0, Lrs;->xF:I

    sub-int/2addr v2, v3

    iget v3, p0, Lrs;->xG:I

    sub-int/2addr v2, v3

    .line 404
    iget-object v3, p0, Lrs;->xY:[I

    iget-object v4, p0, Lrs;->xs:Lyx;

    invoke-virtual {v4}, Lyx;->getLeft()I

    move-result v4

    aput v4, v3, v8

    .line 405
    iget-object v3, p0, Lrs;->xY:[I

    iget-object v4, p0, Lrs;->xs:Lyx;

    invoke-virtual {v4}, Lyx;->getTop()I

    move-result v4

    aput v4, v3, v9

    .line 406
    iget-object v3, p0, Lrs;->xu:Lcom/android/launcher3/DragLayer;

    iget-object v4, p0, Lrs;->xt:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v4}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v4

    iget-object v5, p0, Lrs;->xY:[I

    invoke-virtual {v3, v4, v5}, Lcom/android/launcher3/DragLayer;->b(Landroid/view/View;[I)F

    .line 408
    iget-object v3, p0, Lrs;->xY:[I

    aget v3, v3, v8

    iget v4, p0, Lrs;->xS:I

    sub-int/2addr v3, v4

    iget v4, p0, Lrs;->xD:I

    add-int/2addr v3, v4

    .line 409
    iget-object v4, p0, Lrs;->xY:[I

    aget v4, v4, v9

    iget v5, p0, Lrs;->xS:I

    sub-int/2addr v4, v5

    iget v5, p0, Lrs;->xF:I

    add-int/2addr v4, v5

    .line 414
    if-gez v4, :cond_0

    .line 416
    neg-int v5, v4

    iput v5, p0, Lrs;->xU:I

    .line 420
    :goto_0
    add-int v5, v4, v2

    iget-object v6, p0, Lrs;->xu:Lcom/android/launcher3/DragLayer;

    invoke-virtual {v6}, Lcom/android/launcher3/DragLayer;->getHeight()I

    move-result v6

    if-le v5, v6, :cond_1

    .line 422
    add-int v5, v4, v2

    iget-object v6, p0, Lrs;->xu:Lcom/android/launcher3/DragLayer;

    invoke-virtual {v6}, Lcom/android/launcher3/DragLayer;->getHeight()I

    move-result v6

    sub-int/2addr v5, v6

    neg-int v5, v5

    iput v5, p0, Lrs;->xV:I

    .line 427
    :goto_1
    if-nez p1, :cond_2

    .line 428
    iput v1, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->width:I

    .line 429
    iput v2, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->height:I

    .line 430
    iput v3, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->x:I

    .line 431
    iput v4, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->y:I

    .line 432
    iget-object v0, p0, Lrs;->xv:Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 433
    iget-object v0, p0, Lrs;->xw:Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 434
    iget-object v0, p0, Lrs;->xx:Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 435
    iget-object v0, p0, Lrs;->xy:Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 436
    invoke-virtual {p0}, Lrs;->requestLayout()V

    .line 466
    :goto_2
    return-void

    .line 418
    :cond_0
    iput v8, p0, Lrs;->xU:I

    goto :goto_0

    .line 424
    :cond_1
    iput v8, p0, Lrs;->xV:I

    goto :goto_1

    .line 438
    :cond_2
    const-string v5, "width"

    new-array v6, v10, [I

    iget v7, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->width:I

    aput v7, v6, v8

    aput v1, v6, v9

    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 439
    const-string v5, "height"

    new-array v6, v10, [I

    iget v7, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->height:I

    aput v7, v6, v8

    aput v2, v6, v9

    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 441
    const-string v5, "x"

    new-array v6, v10, [I

    iget v7, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->x:I

    aput v7, v6, v8

    aput v3, v6, v9

    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    .line 442
    const-string v5, "y"

    new-array v6, v10, [I

    iget v7, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->y:I

    aput v7, v6, v8

    aput v4, v6, v9

    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    .line 443
    const/4 v5, 0x4

    new-array v5, v5, [Landroid/animation/PropertyValuesHolder;

    aput-object v1, v5, v8

    aput-object v2, v5, v9

    aput-object v3, v5, v10

    aput-object v4, v5, v12

    invoke-static {v0, p0, v5}, Lyr;->a(Ljava/lang/Object;Landroid/view/View;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 445
    iget-object v1, p0, Lrs;->xv:Landroid/widget/ImageView;

    const-string v2, "alpha"

    new-array v3, v9, [F

    aput v11, v3, v8

    invoke-static {v1, v2, v3}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 446
    iget-object v2, p0, Lrs;->xw:Landroid/widget/ImageView;

    const-string v3, "alpha"

    new-array v4, v9, [F

    aput v11, v4, v8

    invoke-static {v2, v3, v4}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 447
    iget-object v3, p0, Lrs;->xx:Landroid/widget/ImageView;

    const-string v4, "alpha"

    new-array v5, v9, [F

    aput v11, v5, v8

    invoke-static {v3, v4, v5}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 448
    iget-object v4, p0, Lrs;->xy:Landroid/widget/ImageView;

    const-string v5, "alpha"

    new-array v6, v9, [F

    aput v11, v6, v8

    invoke-static {v4, v5, v6}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 449
    new-instance v5, Lru;

    invoke-direct {v5, p0}, Lru;-><init>(Lrs;)V

    invoke-virtual {v0, v5}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 454
    invoke-static {}, Lyr;->ii()Landroid/animation/AnimatorSet;

    move-result-object v5

    .line 455
    iget v6, p0, Lrs;->xL:I

    if-ne v6, v10, :cond_3

    .line 456
    new-array v1, v12, [Landroid/animation/Animator;

    aput-object v0, v1, v8

    aput-object v3, v1, v9

    aput-object v4, v1, v10

    invoke-virtual {v5, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 463
    :goto_3
    const-wide/16 v0, 0x96

    invoke-virtual {v5, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 464
    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->start()V

    goto/16 :goto_2

    .line 457
    :cond_3
    iget v6, p0, Lrs;->xL:I

    if-ne v6, v9, :cond_4

    .line 458
    new-array v3, v12, [Landroid/animation/Animator;

    aput-object v0, v3, v8

    aput-object v1, v3, v9

    aput-object v2, v3, v10

    invoke-virtual {v5, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto :goto_3

    .line 460
    :cond_4
    const/4 v6, 0x5

    new-array v6, v6, [Landroid/animation/Animator;

    aput-object v0, v6, v8

    aput-object v1, v6, v9

    aput-object v2, v6, v10

    aput-object v3, v6, v12

    const/4 v0, 0x4

    aput-object v4, v6, v0

    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto :goto_3
.end method

.method public final x(II)Z
    .locals 8

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 143
    iget v0, p0, Lrs;->xL:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    move v0, v1

    .line 144
    :goto_0
    iget v3, p0, Lrs;->xL:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_4

    move v3, v1

    .line 146
    :goto_1
    iget v4, p0, Lrs;->xT:I

    if-ge p1, v4, :cond_5

    if-eqz v0, :cond_5

    move v4, v1

    :goto_2
    iput-boolean v4, p0, Lrs;->xz:Z

    .line 147
    invoke-virtual {p0}, Lrs;->getWidth()I

    move-result v4

    iget v7, p0, Lrs;->xT:I

    sub-int/2addr v4, v7

    if-le p1, v4, :cond_6

    if-eqz v0, :cond_6

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lrs;->xA:Z

    .line 148
    iget v0, p0, Lrs;->xT:I

    iget v4, p0, Lrs;->xU:I

    add-int/2addr v0, v4

    if-ge p2, v0, :cond_7

    if-eqz v3, :cond_7

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lrs;->xB:Z

    .line 149
    invoke-virtual {p0}, Lrs;->getHeight()I

    move-result v0

    iget v4, p0, Lrs;->xT:I

    sub-int/2addr v0, v4

    iget v4, p0, Lrs;->xV:I

    add-int/2addr v0, v4

    if-le p2, v0, :cond_8

    if-eqz v3, :cond_8

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lrs;->xC:Z

    .line 152
    iget-boolean v0, p0, Lrs;->xz:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lrs;->xA:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lrs;->xB:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lrs;->xC:Z

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    .line 155
    :cond_1
    invoke-virtual {p0}, Lrs;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lrs;->xH:I

    .line 156
    invoke-virtual {p0}, Lrs;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lrs;->xI:I

    .line 157
    invoke-virtual {p0}, Lrs;->getLeft()I

    move-result v0

    iput v0, p0, Lrs;->xJ:I

    .line 158
    invoke-virtual {p0}, Lrs;->getTop()I

    move-result v0

    iput v0, p0, Lrs;->xK:I

    .line 160
    if-eqz v2, :cond_2

    .line 161
    iget-object v1, p0, Lrs;->xv:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lrs;->xz:Z

    if-eqz v0, :cond_9

    move v0, v5

    :goto_6
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 162
    iget-object v1, p0, Lrs;->xw:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lrs;->xA:Z

    if-eqz v0, :cond_a

    move v0, v5

    :goto_7
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 163
    iget-object v1, p0, Lrs;->xx:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lrs;->xB:Z

    if-eqz v0, :cond_b

    move v0, v5

    :goto_8
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 164
    iget-object v0, p0, Lrs;->xy:Landroid/widget/ImageView;

    iget-boolean v1, p0, Lrs;->xC:Z

    if-eqz v1, :cond_c

    :goto_9
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 166
    :cond_2
    return v2

    :cond_3
    move v0, v2

    .line 143
    goto/16 :goto_0

    :cond_4
    move v3, v2

    .line 144
    goto/16 :goto_1

    :cond_5
    move v4, v2

    .line 146
    goto/16 :goto_2

    :cond_6
    move v0, v2

    .line 147
    goto :goto_3

    :cond_7
    move v0, v2

    .line 148
    goto :goto_4

    :cond_8
    move v0, v2

    .line 149
    goto :goto_5

    :cond_9
    move v0, v6

    .line 161
    goto :goto_6

    :cond_a
    move v0, v6

    .line 162
    goto :goto_7

    :cond_b
    move v0, v6

    .line 163
    goto :goto_8

    :cond_c
    move v5, v6

    .line 164
    goto :goto_9
.end method

.method public final y(II)V
    .locals 3

    .prologue
    .line 192
    iget-boolean v0, p0, Lrs;->xz:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lrs;->xJ:I

    neg-int v0, v0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lrs;->hX:I

    iget v0, p0, Lrs;->xH:I

    iget v1, p0, Lrs;->xT:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget v1, p0, Lrs;->hX:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lrs;->hX:I

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lrs;->xB:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lrs;->xK:I

    neg-int v0, v0

    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lrs;->hY:I

    iget v0, p0, Lrs;->xI:I

    iget v1, p0, Lrs;->xT:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget v1, p0, Lrs;->hY:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lrs;->hY:I

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lrs;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/DragLayer$LayoutParams;

    iget-boolean v1, p0, Lrs;->xz:Z

    if-eqz v1, :cond_6

    iget v1, p0, Lrs;->xJ:I

    iget v2, p0, Lrs;->hX:I

    add-int/2addr v1, v2

    iput v1, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->x:I

    iget v1, p0, Lrs;->xH:I

    iget v2, p0, Lrs;->hX:I

    sub-int/2addr v1, v2

    iput v1, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->width:I

    :cond_2
    :goto_2
    iget-boolean v1, p0, Lrs;->xB:Z

    if-eqz v1, :cond_7

    iget v1, p0, Lrs;->xK:I

    iget v2, p0, Lrs;->hY:I

    add-int/2addr v1, v2

    iput v1, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->y:I

    iget v1, p0, Lrs;->xI:I

    iget v2, p0, Lrs;->hY:I

    sub-int/2addr v1, v2

    iput v1, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->height:I

    :cond_3
    :goto_3
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lrs;->v(Z)V

    invoke-virtual {p0}, Lrs;->requestLayout()V

    .line 193
    return-void

    .line 192
    :cond_4
    iget-boolean v0, p0, Lrs;->xA:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lrs;->xu:Lcom/android/launcher3/DragLayer;

    invoke-virtual {v0}, Lcom/android/launcher3/DragLayer;->getWidth()I

    move-result v0

    iget v1, p0, Lrs;->xJ:I

    iget v2, p0, Lrs;->xH:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lrs;->hX:I

    iget v0, p0, Lrs;->xH:I

    neg-int v0, v0

    iget v1, p0, Lrs;->xT:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lrs;->hX:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lrs;->hX:I

    goto :goto_0

    :cond_5
    iget-boolean v0, p0, Lrs;->xC:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lrs;->xu:Lcom/android/launcher3/DragLayer;

    invoke-virtual {v0}, Lcom/android/launcher3/DragLayer;->getHeight()I

    move-result v0

    iget v1, p0, Lrs;->xK:I

    iget v2, p0, Lrs;->xI:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lrs;->hY:I

    iget v0, p0, Lrs;->xI:I

    neg-int v0, v0

    iget v1, p0, Lrs;->xT:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lrs;->hY:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lrs;->hY:I

    goto :goto_1

    :cond_6
    iget-boolean v1, p0, Lrs;->xA:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lrs;->xH:I

    iget v2, p0, Lrs;->hX:I

    add-int/2addr v1, v2

    iput v1, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->width:I

    goto :goto_2

    :cond_7
    iget-boolean v1, p0, Lrs;->xC:Z

    if-eqz v1, :cond_3

    iget v1, p0, Lrs;->xI:I

    iget v2, p0, Lrs;->hY:I

    add-int/2addr v1, v2

    iput v1, v0, Lcom/android/launcher3/DragLayer$LayoutParams;->height:I

    goto :goto_3
.end method
