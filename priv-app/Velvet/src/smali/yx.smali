.class public Lyx;
.super Landroid/appwidget/AppWidgetHostView;
.source "PG"

# interfaces
.implements Lug;


# instance fields
.field GS:Landroid/view/LayoutInflater;

.field private Mo:I

.field private mContext:Landroid/content/Context;

.field private xu:Lcom/android/launcher3/DragLayer;

.field private zA:Ltf;

.field private zt:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1}, Landroid/appwidget/AppWidgetHostView;-><init>(Landroid/content/Context;)V

    .line 46
    iput-object p1, p0, Lyx;->mContext:Landroid/content/Context;

    .line 47
    new-instance v0, Ltf;

    invoke-direct {v0, p0}, Ltf;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lyx;->zA:Ltf;

    .line 48
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lyx;->GS:Landroid/view/LayoutInflater;

    .line 49
    check-cast p1, Lcom/android/launcher3/Launcher;

    invoke-virtual {p1}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v0

    iput-object v0, p0, Lyx;->xu:Lcom/android/launcher3/DragLayer;

    .line 50
    return-void
.end method


# virtual methods
.method public cancelLongPress()V
    .locals 1

    .prologue
    .line 135
    invoke-super {p0}, Landroid/appwidget/AppWidgetHostView;->cancelLongPress()V

    .line 136
    iget-object v0, p0, Lyx;->zA:Ltf;

    invoke-virtual {v0}, Ltf;->cancelLongPress()V

    .line 137
    return-void
.end method

.method public final fT()V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lyx;->zA:Ltf;

    iget-boolean v0, v0, Ltf;->BV:Z

    if-nez v0, :cond_0

    .line 144
    iget-object v0, p0, Lyx;->zA:Ltf;

    invoke-virtual {v0}, Ltf;->cancelLongPress()V

    .line 146
    :cond_0
    return-void
.end method

.method public getDescendantFocusability()I
    .locals 1

    .prologue
    .line 150
    const/high16 v0, 0x60000

    return v0
.end method

.method protected getErrorView()Landroid/view/View;
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Lyx;->GS:Landroid/view/LayoutInflater;

    const v1, 0x7f040021

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public in()Z
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lyx;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 67
    iget v1, p0, Lyx;->Mo:I

    if-eq v1, v0, :cond_0

    .line 68
    const/4 v0, 0x1

    .line 70
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 129
    invoke-super {p0}, Landroid/appwidget/AppWidgetHostView;->onAttachedToWindow()V

    .line 130
    invoke-virtual {p0}, Lyx;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lyx;->zt:F

    .line 131
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 76
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 77
    iget-object v0, p0, Lyx;->zA:Ltf;

    invoke-virtual {v0}, Ltf;->cancelLongPress()V

    .line 81
    :cond_0
    iget-object v0, p0, Lyx;->zA:Ltf;

    iget-boolean v0, v0, Ltf;->BV:Z

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lyx;->zA:Ltf;

    invoke-virtual {v0}, Ltf;->cancelLongPress()V

    .line 83
    const/4 v0, 0x1

    .line 107
    :goto_0
    return v0

    .line 88
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 107
    :cond_2
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 90
    :pswitch_0
    iget-object v0, p0, Lyx;->zA:Ltf;

    invoke-virtual {v0}, Ltf;->fo()V

    .line 91
    iget-object v0, p0, Lyx;->xu:Lcom/android/launcher3/DragLayer;

    invoke-virtual {v0, p0}, Lcom/android/launcher3/DragLayer;->a(Lug;)V

    goto :goto_1

    .line 97
    :pswitch_1
    iget-object v0, p0, Lyx;->zA:Ltf;

    invoke-virtual {v0}, Ltf;->cancelLongPress()V

    goto :goto_1

    .line 100
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v2, p0, Lyx;->zt:F

    invoke-static {p0, v0, v1, v2}, Ladp;->a(Landroid/view/View;FFF)Z

    move-result v0

    if-nez v0, :cond_2

    .line 101
    iget-object v0, p0, Lyx;->zA:Ltf;

    invoke-virtual {v0}, Ltf;->cancelLongPress()V

    goto :goto_1

    .line 88
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 113
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 124
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 116
    :pswitch_0
    iget-object v0, p0, Lyx;->zA:Ltf;

    invoke-virtual {v0}, Ltf;->cancelLongPress()V

    goto :goto_0

    .line 119
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v2, p0, Lyx;->zt:F

    invoke-static {p0, v0, v1, v2}, Ladp;->a(Landroid/view/View;FFF)Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lyx;->zA:Ltf;

    invoke-virtual {v0}, Ltf;->cancelLongPress()V

    goto :goto_0

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public updateAppWidget(Landroid/widget/RemoteViews;)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lyx;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lyx;->Mo:I

    .line 61
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetHostView;->updateAppWidget(Landroid/widget/RemoteViews;)V

    .line 62
    return-void
.end method
