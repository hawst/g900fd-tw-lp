.class public final Lckx;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bak:Landroid/app/ActivityManager;

.field private final bal:Lcha;

.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/pm/PackageManager;Landroid/app/ActivityManager;Lcha;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManager;

    iput-object v0, p0, Lckx;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 57
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lckx;->bak:Landroid/app/ActivityManager;

    .line 58
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcha;

    iput-object v0, p0, Lckx;->bal:Lcha;

    .line 59
    return-void
.end method

.method private static a(Lcom/google/android/gms/appdatasearch/UsageInfo;)Lgnu;
    .locals 7
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 240
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->xt()Lcom/google/android/gms/appdatasearch/DocumentContents;

    move-result-object v0

    .line 242
    if-eqz v0, :cond_2

    .line 243
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/DocumentContents;->wV()[Lcom/google/android/gms/appdatasearch/DocumentSection;

    move-result-object v0

    move-object v2, v0

    .line 245
    :goto_0
    if-eqz v2, :cond_1

    .line 246
    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 247
    invoke-virtual {v4}, Lcom/google/android/gms/appdatasearch/DocumentSection;->wY()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v5

    if-eqz v5, :cond_0

    const-string v5, "SsbContext"

    invoke-virtual {v4}, Lcom/google/android/gms/appdatasearch/DocumentSection;->wY()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v6

    iget-object v6, v6, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, v4, Lcom/google/android/gms/appdatasearch/DocumentSection;->ava:[B

    if-eqz v5, :cond_0

    iget-object v5, v4, Lcom/google/android/gms/appdatasearch/DocumentSection;->ava:[B

    array-length v5, v5

    if-lez v5, :cond_0

    .line 252
    :try_start_0
    new-instance v0, Lgnu;

    invoke-direct {v0}, Lgnu;-><init>()V

    .line 253
    iget-object v2, v4, Lcom/google/android/gms/appdatasearch/DocumentSection;->ava:[B

    invoke-static {v0, v2}, Ljsr;->c(Ljsr;[B)Ljsr;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    .line 262
    :goto_2
    return-object v0

    .line 255
    :catch_0
    move-exception v0

    .line 256
    const-string v2, "AssistContextHelper"

    const-string v3, "Could not parse SsbContext bytes."

    invoke-static {v2, v3, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 257
    goto :goto_2

    .line 246
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 262
    goto :goto_2

    :cond_2
    move-object v2, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;)Lijj;
    .locals 10
    .param p0    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 166
    new-instance v4, Lijk;

    invoke-direct {v4}, Lijk;-><init>()V

    .line 167
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/UsageInfo;

    .line 168
    invoke-static {v0}, Lckx;->a(Lcom/google/android/gms/appdatasearch/UsageInfo;)Lgnu;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, v1, Lgnu;->cQy:Ljjz;

    .line 169
    :goto_1
    if-nez v1, :cond_1

    .line 170
    new-instance v1, Ljjz;

    invoke-direct {v1}, Ljjz;-><init>()V

    .line 173
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->xt()Lcom/google/android/gms/appdatasearch/DocumentContents;

    move-result-object v2

    if-nez v2, :cond_5

    move-object v2, v3

    .line 174
    :goto_2
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_8

    :cond_2
    const/4 v2, 0x1

    :goto_3
    if-eqz v2, :cond_0

    .line 180
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->xs()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-virtual {v1, v6, v7}, Ljjz;->dy(J)Ljjz;

    .line 183
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->xr()Lcom/google/android/gms/appdatasearch/DocumentId;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/DocumentId;->getUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljjz;->wp(Ljava/lang/String;)Ljjz;

    .line 185
    iget-object v2, v1, Ljjz;->epG:Ljjo;

    if-nez v2, :cond_3

    .line 186
    new-instance v2, Ljjo;

    invoke-direct {v2}, Ljjo;-><init>()V

    iput-object v2, v1, Ljjz;->epG:Ljjo;

    .line 189
    :cond_3
    iget-object v2, v1, Ljjz;->epG:Ljjo;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->xr()Lcom/google/android/gms/appdatasearch/DocumentId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/DocumentId;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljjo;->wl(Ljava/lang/String;)Ljjo;

    .line 192
    invoke-virtual {v4, v1}, Lijk;->bt(Ljava/lang/Object;)Lijk;

    goto :goto_0

    :cond_4
    move-object v1, v3

    .line 168
    goto :goto_1

    .line 173
    :cond_5
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->xt()Lcom/google/android/gms/appdatasearch/DocumentContents;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/appdatasearch/DocumentContents;->account:Landroid/accounts/Account;

    if-nez v2, :cond_6

    move-object v2, v3

    goto :goto_2

    :cond_6
    iget-object v6, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v7, "com.google"

    if-eq v6, v7, :cond_7

    move-object v2, v3

    goto :goto_2

    :cond_7
    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_2

    .line 174
    :cond_8
    invoke-static {p0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    goto :goto_3

    .line 195
    :cond_9
    iget-object v0, v4, Lijk;->IA:Ljava/util/ArrayList;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Landroid/os/Bundle;J)Ljjz;
    .locals 6
    .param p0    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 91
    if-nez p0, :cond_0

    move-object v0, v1

    .line 130
    :goto_0
    return-object v0

    .line 98
    :cond_0
    :try_start_0
    new-instance v0, Ljjo;

    invoke-direct {v0}, Ljjo;-><init>()V

    .line 101
    invoke-virtual {v0, p0}, Ljjo;->wl(Ljava/lang/String;)Ljjo;

    .line 103
    new-instance v2, Ljjz;

    invoke-direct {v2}, Ljjz;-><init>()V

    .line 105
    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, p2

    invoke-virtual {v2, v4, v5}, Ljjz;->dy(J)Ljjz;

    .line 106
    iput-object v0, v2, Ljjz;->epG:Ljjo;

    .line 108
    if-eqz p1, :cond_4

    .line 109
    const-string v3, "com.google.search.assist.SIMULATED_QUERY"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 110
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 112
    invoke-virtual {v0, v3}, Ljjo;->wm(Ljava/lang/String;)Ljjo;

    .line 115
    :cond_1
    const-string v0, "com.google.search.assist.URI"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    .line 116
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 118
    invoke-virtual {v2, v0}, Ljjz;->wp(Ljava/lang/String;)Ljjz;

    .line 121
    :cond_2
    const-string v0, "com.google.search.assist.DETAILS_PAGE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_3

    array-length v3, v0

    if-nez v3, :cond_7

    :cond_3
    move-object v0, v1

    .line 122
    :goto_2
    if-eqz v0, :cond_4

    .line 123
    iput-object v0, v2, Ljjz;->epF:Ljjy;

    :cond_4
    move-object v0, v2

    .line 127
    goto :goto_0

    .line 115
    :cond_5
    instance-of v3, v0, Landroid/net/Uri;

    if-eqz v3, :cond_6

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_1

    .line 121
    :cond_7
    new-instance v3, Ljjy;

    invoke-direct {v3}, Ljjy;-><init>()V

    invoke-static {v3, v0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Ljjy;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 128
    :catch_0
    move-exception v0

    .line 129
    const-string v2, "AssistContextHelper"

    const-string v3, "Could not read proto"

    invoke-static {v2, v3, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 130
    goto :goto_0
.end method

.method public static a(Ljjz;)Z
    .locals 4

    .prologue
    .line 143
    new-instance v0, Ljjz;

    invoke-direct {v0}, Ljjz;-><init>()V

    .line 144
    iget-object v1, p0, Ljjz;->epG:Ljjo;

    if-eqz v1, :cond_2

    .line 145
    new-instance v1, Ljjo;

    invoke-direct {v1}, Ljjo;-><init>()V

    .line 146
    iget-object v2, p0, Ljjz;->epG:Ljjo;

    invoke-virtual {v2}, Ljjo;->qA()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 147
    iget-object v2, p0, Ljjz;->epG:Ljjo;

    invoke-virtual {v2}, Ljjo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljjo;->wl(Ljava/lang/String;)Ljjo;

    .line 150
    :cond_0
    iget-object v2, p0, Ljjz;->epG:Ljjo;

    invoke-virtual {v2}, Ljjo;->bnW()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 151
    iget-object v2, p0, Ljjz;->epG:Ljjo;

    invoke-virtual {v2}, Ljjo;->bnU()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljjo;->dv(J)Ljjo;

    .line 154
    :cond_1
    iput-object v1, v0, Ljjz;->epG:Ljjo;

    .line 157
    :cond_2
    invoke-virtual {p0}, Ljjz;->bog()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 158
    invoke-virtual {p0}, Ljjz;->getTimestamp()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljjz;->dy(J)Ljjz;

    .line 161
    :cond_3
    invoke-static {p0, v0}, Leqh;->c(Ljsr;Ljsr;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final Pe()Ljava/lang/String;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 319
    iget-object v0, p0, Lckx;->bak:Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    .line 323
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 324
    iget v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v4, 0x64

    if-ne v3, v4, :cond_0

    .line 325
    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    if-nez v0, :cond_2

    move-object v0, v1

    .line 326
    :cond_1
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 333
    :goto_1
    return-object v0

    .line 325
    :cond_2
    const/16 v3, 0x3a

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-ltz v3, :cond_1

    if-nez v3, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {v0, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 332
    :cond_4
    const-string v0, "AssistContextHelper"

    const-string v2, "Couldn\'t find top package name"

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x4

    invoke-static {v4, v0, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 333
    goto :goto_1
.end method

.method public final a(Lgnu;)Ljjm;
    .locals 1
    .param p1    # Lgnu;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 62
    if-eqz p1, :cond_0

    iget-object v0, p1, Lgnu;->cQy:Ljjz;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lgnu;->cQy:Ljjz;

    iget-object v0, v0, Ljjz;->epG:Ljjo;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lgnu;->cQy:Ljjz;

    iget-object v0, v0, Ljjz;->epG:Ljjo;

    invoke-virtual {v0}, Ljjo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 65
    :cond_0
    const/4 v0, 0x0

    .line 68
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p1, Lgnu;->cQy:Ljjz;

    iget-object v0, v0, Ljjz;->epG:Ljjo;

    invoke-virtual {v0}, Ljjo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lckx;->hk(Ljava/lang/String;)Ljjm;

    move-result-object v0

    goto :goto_0
.end method

.method public final hk(Ljava/lang/String;)Ljjm;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 74
    :try_start_0
    iget-object v0, p0, Lckx;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v1, 0x40

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 77
    iget-object v0, p0, Lckx;->bal:Lcha;

    invoke-virtual {v0, v1}, Lcha;->a(Landroid/content/pm/PackageInfo;)Z

    move-result v2

    .line 78
    new-instance v0, Ljjm;

    invoke-direct {v0}, Ljjm;-><init>()V

    .line 79
    invoke-virtual {v0, p1}, Ljjm;->wk(Ljava/lang/String;)Ljjm;

    .line 80
    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    int-to-long v4, v1

    invoke-virtual {v0, v4, v5}, Ljjm;->du(J)Ljjm;

    .line 81
    invoke-virtual {v0, v2}, Ljjm;->io(Z)Ljjm;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :goto_0
    return-object v0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    const-string v1, "AssistContextHelper"

    const-string v2, "Could not find package"

    invoke-static {v1, v2, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 85
    const/4 v0, 0x0

    goto :goto_0
.end method
