.class public final Lcve;
.super Lcyc;
.source "PG"


# instance fields
.field private bkR:Landroid/preference/SwitchPreference;

.field private final mGsaConfigFlags:Lchk;

.field private final mSearchSettings:Lcke;


# direct methods
.method public constructor <init>(Lchk;Lcke;Lcrh;)V
    .locals 0
    .param p1    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcke;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lcrh;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 31
    invoke-direct {p0, p2}, Lcyc;-><init>(Lcke;)V

    .line 32
    iput-object p1, p0, Lcve;->mGsaConfigFlags:Lchk;

    .line 33
    iput-object p2, p0, Lcve;->mSearchSettings:Lcke;

    .line 34
    return-void
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 2

    .prologue
    .line 39
    check-cast p1, Landroid/preference/SwitchPreference;

    iput-object p1, p0, Lcve;->bkR:Landroid/preference/SwitchPreference;

    .line 40
    iget-object v0, p0, Lcve;->bkR:Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcve;->mSearchSettings:Lcke;

    invoke-interface {v1}, Lcke;->CS()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 41
    return-void
.end method

.method public final d(Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcve;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->FI()I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onResume()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcve;->bkR:Landroid/preference/SwitchPreference;

    if-nez v0, :cond_0

    .line 66
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v0, p0, Lcve;->bkR:Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcve;->mSearchSettings:Lcke;

    invoke-interface {v1}, Lcke;->CS()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcyc;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 46
    return-void
.end method
