.class public final Lfww;
.super Lfuz;
.source "PG"


# instance fields
.field final mActivityHelper:Lfzw;

.field private final mStringEvaluator:Lgbr;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lgbr;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 75
    iput-object p4, p0, Lfww;->mStringEvaluator:Lgbr;

    .line 76
    iget-object v0, p2, Lftz;->mActivityHelper:Lfzw;

    iput-object v0, p0, Lfww;->mActivityHelper:Lfzw;

    .line 77
    return-void
.end method

.method private a(Landroid/content/Context;Lgca;[Liyg;)I
    .locals 5
    .param p2    # Lgca;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const v4, 0x7f0b00ba

    const v3, 0x7f0b00b7

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 587
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lgca;->aEz()I

    move-result v1

    if-eq v1, v2, :cond_3

    .line 590
    :cond_0
    if-eqz p2, :cond_2

    invoke-virtual {p2, p1}, Lgca;->br(Landroid/content/Context;)I

    move-result v0

    .line 612
    :cond_1
    :goto_0
    return v0

    .line 590
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0

    .line 593
    :cond_3
    invoke-virtual {p2}, Lgca;->aEG()Z

    move-result v1

    if-eqz v1, :cond_5

    array-length v1, p3

    if-ne v1, v2, :cond_5

    .line 597
    iget-object v1, p2, Lgca;->mRoute:Liyg;

    iget-object v1, v1, Liyg;->dPL:Liyh;

    iget-object v1, v1, Liyh;->dQe:[Liyi;

    array-length v1, v1

    if-le v1, v2, :cond_4

    .line 598
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0

    .line 600
    :cond_4
    iget-object v1, p2, Lgca;->mRoute:Liyg;

    iget-object v1, v1, Liyg;->dPL:Liyh;

    iget-object v1, v1, Liyh;->dQe:[Liyi;

    aget-object v0, v1, v0

    invoke-virtual {v0}, Liyi;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 606
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00b8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0

    .line 603
    :pswitch_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0

    .line 609
    :cond_5
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Lgca;->p(Landroid/location/Location;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 610
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0

    .line 600
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(Landroid/content/Context;Lani;Lgca;Ljal;Ljak;Lfzb;)Lanh;
    .locals 8
    .param p3    # Lgca;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lfzb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 390
    new-instance v6, Lanh;

    invoke-direct {v6}, Lanh;-><init>()V

    .line 391
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Lanh;->cm(I)Lanh;

    .line 392
    iget-object v0, p4, Ljal;->dMF:[Liyg;

    invoke-direct {p0, p1, p3, v0}, Lfww;->a(Landroid/content/Context;Lgca;[Liyg;)I

    move-result v0

    .line 393
    if-eqz v0, :cond_0

    .line 394
    invoke-virtual {v6, v0}, Lanh;->cn(I)Lanh;

    .line 397
    :cond_0
    iput-object p2, v6, Lanh;->ahs:Lani;

    .line 398
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iput-object v0, v6, Lanh;->ahu:Lizj;

    .line 400
    new-instance v7, Lany;

    invoke-direct {v7}, Lany;-><init>()V

    .line 401
    iput-object v7, v6, Lanh;->agw:Lany;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    .line 402
    invoke-virtual/range {v0 .. v5}, Lfww;->a(Landroid/content/Context;Lgca;Ljal;Ljak;Lfzb;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lany;->bh(Ljava/lang/String;)Lany;

    .line 405
    if-eqz p3, :cond_2

    .line 406
    invoke-static {p1, p5}, Lfww;->b(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p3, p6, v0}, Lfww;->a(Landroid/content/Context;Lgca;Lfzb;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 408
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 409
    invoke-virtual {v7, v0}, Lany;->bi(Ljava/lang/String;)Lany;

    .line 412
    :cond_1
    invoke-virtual {p3}, Lgca;->aEH()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 413
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Lanh;->aV(Z)Lanh;

    .line 417
    :cond_2
    return-object v6
.end method

.method private a(Landroid/content/Context;Lfmt;Lani;Ljal;)Lanh;
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 453
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 454
    invoke-virtual {v0, v3}, Lanh;->cm(I)Lanh;

    .line 455
    iput-object p3, v0, Lanh;->ahs:Lani;

    .line 456
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iput-object v1, v0, Lanh;->ahu:Lizj;

    .line 457
    new-instance v1, Laor;

    invoke-direct {v1}, Laor;-><init>()V

    .line 458
    iput-object v1, v0, Lanh;->agx:Laor;

    .line 459
    invoke-interface {p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBP()Landroid/location/Location;

    move-result-object v2

    invoke-static {v2}, Lgay;->o(Landroid/location/Location;)Ljbp;

    move-result-object v2

    iput-object v2, v1, Laor;->ajY:Ljbp;

    .line 461
    iput-object p4, v1, Laor;->ajZ:Ljal;

    .line 462
    invoke-virtual {v1, v3}, Laor;->bf(Z)Laor;

    .line 463
    const v2, 0x7f0a015d

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p4, Ljal;->dWL:Ljak;

    invoke-static {p1, v5}, Lfww;->b(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Laor;->cm(Ljava/lang/String;)Laor;

    .line 465
    return-object v0
.end method

.method private a(Landroid/content/Context;Lgca;[Liyg;Lfzb;Lfmt;)Lanh;
    .locals 3
    .param p2    # Lgca;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 424
    if-eqz p2, :cond_0

    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    invoke-virtual {p4, v1, p5}, Lfzb;->a(Lizj;Lfmt;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 448
    :cond_0
    :goto_0
    return-object v0

    .line 428
    :cond_1
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    invoke-virtual {p4, p1, v1, p5}, Lfzb;->a(Landroid/content/Context;Lizj;Lfmt;)Ljava/lang/String;

    move-result-object v1

    .line 429
    if-eqz v1, :cond_0

    .line 433
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 434
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lanh;->cm(I)Lanh;

    .line 435
    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    iput-object v2, v0, Lanh;->ahu:Lizj;

    .line 436
    invoke-direct {p0, p1, p2, p3}, Lfww;->a(Landroid/content/Context;Lgca;[Liyg;)I

    move-result v2

    .line 437
    if-eqz v2, :cond_2

    .line 438
    invoke-virtual {v0, v2}, Lanh;->cn(I)Lanh;

    .line 440
    :cond_2
    new-instance v2, Laoj;

    invoke-direct {v2}, Laoj;-><init>()V

    iput-object v2, v0, Lanh;->agz:Laoj;

    .line 441
    iget-object v2, v0, Lanh;->agz:Laoj;

    invoke-virtual {v2, v1}, Laoj;->bS(Ljava/lang/String;)Laoj;

    .line 443
    new-instance v1, Lfsa;

    const/16 v2, 0xb1

    invoke-direct {v1, v2}, Lfsa;-><init>(I)V

    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    invoke-virtual {p4, v2, p5}, Lfzb;->c(Lizj;Lfmt;)I

    move-result v2

    invoke-virtual {v1, v2}, Lfsa;->jB(I)Lfsa;

    move-result-object v1

    invoke-virtual {p4, p1}, Lfzb;->bm(Landroid/content/Context;)Laoo;

    move-result-object v2

    invoke-virtual {v1, v2}, Lfsa;->a(Laoo;)Lani;

    move-result-object v1

    iput-object v1, v0, Lanh;->ahs:Lani;

    .line 446
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lanh;->aV(Z)Lanh;

    goto :goto_0
.end method

.method private static a(Ljbp;Liyg;)Lani;
    .locals 5
    .param p1    # Liyg;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 472
    if-nez p0, :cond_0

    move-object v0, v1

    .line 490
    :goto_0
    return-object v0

    .line 474
    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lgaz;->a(Ljbp;Liyg;Z)Ljava/lang/String;

    move-result-object v2

    .line 475
    const v0, 0x7f0200bb

    .line 476
    if-eqz p1, :cond_1

    .line 477
    invoke-virtual {p1}, Liyg;->aEz()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 490
    :cond_1
    :goto_1
    new-instance v3, Lfsa;

    const/4 v4, 0x3

    invoke-direct {v3, v4}, Lfsa;-><init>(I)V

    invoke-virtual {v3, v0}, Lfsa;->jB(I)Lfsa;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    goto :goto_0

    .line 479
    :pswitch_0
    const v0, 0x7f02011b

    .line 480
    goto :goto_1

    .line 482
    :pswitch_1
    const v0, 0x7f020119

    .line 483
    goto :goto_1

    .line 485
    :pswitch_2
    const v0, 0x7f02011c

    goto :goto_1

    .line 477
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/content/Context;ILiwk;)Lfrz;
    .locals 1
    .param p3    # Liwk;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 795
    if-nez p3, :cond_0

    .line 796
    const/4 v0, 0x0

    .line 798
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lfxb;

    invoke-direct {v0, p0, p1, p2, p3}, Lfxb;-><init>(Lfww;Landroid/content/Context;ILiwk;)V

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Lgca;Lfzb;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p2    # Lfzb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 529
    if-eqz p2, :cond_0

    .line 530
    invoke-virtual {p2, p0, p3}, Lfzb;->l(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 532
    if-eqz v0, :cond_0

    .line 548
    :goto_0
    return-object v0

    .line 536
    :cond_0
    invoke-virtual {p1}, Lgca;->aEz()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    .line 548
    goto :goto_0

    .line 540
    :pswitch_0
    invoke-virtual {p1, p0}, Lgca;->bv(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 541
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0

    .line 536
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Lgca;Z)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 560
    .line 561
    if-eqz p1, :cond_1

    .line 562
    invoke-virtual {p1}, Lgca;->aEH()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 563
    invoke-virtual {p1}, Lgca;->aEC()Ljava/lang/Integer;

    move-result-object v1

    .line 565
    :goto_0
    if-nez v1, :cond_0

    .line 566
    invoke-virtual {p1}, Lgca;->aEB()Ljava/lang/Integer;

    move-result-object v1

    .line 568
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_1

    .line 569
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lesi;->c(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v0

    .line 572
    :cond_1
    return-object v0

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method private aDN()Z
    .locals 1

    .prologue
    .line 661
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    invoke-static {v0}, Lgbf;->E(Lizj;)Ljal;

    move-result-object v0

    .line 662
    if-eqz v0, :cond_0

    iget-object v0, v0, Ljal;->dWL:Ljak;

    invoke-virtual {v0}, Ljak;->bef()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final aDO()Liwk;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 889
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    .line 890
    iget-object v2, v0, Lizj;->dUo:[Liwk;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 891
    invoke-virtual {v0}, Liwk;->getType()I

    move-result v4

    const/4 v5, 0x5

    if-eq v4, v5, :cond_0

    invoke-virtual {v0}, Liwk;->getType()I

    move-result v4

    const/16 v5, 0x11

    if-eq v4, v5, :cond_0

    invoke-virtual {v0}, Liwk;->getType()I

    move-result v4

    const/16 v5, 0x12

    if-ne v4, v5, :cond_1

    .line 897
    :cond_0
    :goto_1
    return-object v0

    .line 890
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 897
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static b(Landroid/content/Context;Ljak;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 523
    invoke-static {p0, p1}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(Lfmt;)Lfrz;
    .locals 1

    .prologue
    .line 780
    new-instance v0, Lfxa;

    invoke-direct {v0, p0, p1}, Lfxa;-><init>(Lfww;Lfmt;)V

    return-object v0
.end method

.method private f(Landroid/content/Context;Lfmt;)Lfrz;
    .locals 1

    .prologue
    .line 768
    new-instance v0, Lfwz;

    invoke-direct {v0, p0, p1, p2}, Lfwz;-><init>(Lfww;Landroid/content/Context;Lfmt;)V

    return-object v0
.end method

.method private static k(Landroid/content/Context;I)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 751
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 752
    const v1, 0x7f0a0333

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 753
    const v2, 0x7f0a0335

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 754
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " <a href=\""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\"><u>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</u></a>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 17

    .prologue
    .line 83
    move-object/from16 v0, p0

    iget-object v3, v0, Lfuz;->mEntry:Lizj;

    .line 84
    invoke-static {v3}, Lgbf;->E(Lizj;)Ljal;

    move-result-object v7

    .line 85
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 86
    iget-object v3, v7, Ljal;->dMF:[Liyg;

    array-length v3, v3

    if-lez v3, :cond_6

    iget-object v3, v7, Ljal;->dMF:[Liyg;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Liyg;->aEz()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_6

    .line 89
    iget-object v8, v7, Ljal;->dWL:Ljak;

    iget-object v3, v7, Ljal;->dMF:[Liyg;

    array-length v3, v3

    if-lez v3, :cond_1

    new-instance v6, Lgca;

    iget-object v3, v7, Ljal;->dMF:[Liyg;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lfuz;->mClock:Lemp;

    invoke-direct {v6, v3, v4}, Lgca;-><init>(Liyg;Lemp;)V

    :goto_0
    invoke-interface/range {p2 .. p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBO()Landroid/location/Location;

    move-result-object v10

    if-eqz v6, :cond_2

    invoke-virtual {v6, v10}, Lgca;->p(Landroid/location/Location;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v9, Lfzb;

    move-object/from16 v0, p0

    iget-object v3, v0, Lfuz;->mClock:Lemp;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfww;->mStringEvaluator:Lgbr;

    invoke-direct {v9, v3, v6, v4}, Lfzb;-><init>(Lemp;Lgca;Lgbr;)V

    :goto_1
    if-nez v6, :cond_3

    const/4 v3, 0x0

    move-object v14, v3

    :goto_2
    iget-object v3, v8, Ljak;->aeB:Ljbp;

    invoke-static {v3, v14}, Lfww;->a(Ljbp;Liyg;)Lani;

    move-result-object v5

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v9}, Lfww;->a(Landroid/content/Context;Lani;Lgca;Ljal;Ljak;Lfzb;)Lanh;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v9, :cond_4

    invoke-virtual {v9, v10}, Lfzb;->m(Landroid/location/Location;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v13, v7, Ljal;->dMF:[Liyg;

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object v12, v6

    move-object v14, v9

    move-object/from16 v15, p2

    invoke-direct/range {v10 .. v15}, Lfww;->a(Landroid/content/Context;Lgca;[Liyg;Lfzb;Lfmt;)Lanh;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lfww;->a(Ljava/util/List;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lfuz;->mEntry:Lizj;

    move-object/from16 v0, p1

    invoke-static {v0, v8}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v8, Ljak;->aeB:Ljbp;

    move-object/from16 v0, p1

    invoke-virtual {v9, v0, v3, v4, v5}, Lfzb;->a(Landroid/content/Context;Lizj;Ljava/lang/String;Ljbp;)Lanh;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    :cond_0
    :goto_3
    invoke-interface/range {p2 .. p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v3

    invoke-static {v3}, Lfph;->l(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lfph;

    move-result-object v3

    invoke-direct/range {p0 .. p0}, Lfww;->aDN()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-virtual {v3}, Lfph;->TC()Z

    move-result v3

    if-eqz v3, :cond_d

    new-instance v4, Laoj;

    invoke-direct {v4}, Laoj;-><init>()V

    const v3, 0x7f0a0362

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v3, Lanh;

    invoke-direct {v3}, Lanh;-><init>()V

    const-string v5, "com.google.android.googlequicksearchbox.TRAFFIC_CARD_SETTINGS"

    invoke-static {v5}, Lgaw;->ml(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    new-instance v6, Lfsa;

    const/16 v8, 0x14

    invoke-direct {v6, v8}, Lfsa;-><init>(I)V

    const v8, 0x7f0201b3

    invoke-virtual {v6, v8}, Lfsa;->jB(I)Lfsa;

    move-result-object v6

    invoke-virtual {v6, v5}, Lfsa;->L(Landroid/content/Intent;)Lani;

    move-result-object v5

    iput-object v5, v3, Lanh;->ahs:Lani;

    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Lanh;->cm(I)Lanh;

    iput-object v4, v3, Lanh;->agz:Laoj;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfuz;->mEntry:Lizj;

    iput-object v4, v3, Lanh;->ahu:Lizj;

    .line 95
    :goto_4
    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lfww;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 97
    new-instance v4, Lang;

    invoke-direct {v4}, Lang;-><init>()V

    .line 98
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lanh;

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lanh;

    iput-object v3, v4, Lang;->ags:[Lanh;

    .line 99
    iget-object v3, v7, Ljal;->dMF:[Liyg;

    array-length v3, v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_e

    iget-object v3, v7, Ljal;->dMF:[Liyg;

    const/4 v5, 0x0

    aget-object v3, v3, v5

    invoke-virtual {v3}, Liyg;->aEz()I

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_e

    const/4 v3, 0x0

    :goto_5
    invoke-virtual {v4, v3}, Lang;->aS(Z)Lang;

    .line 100
    return-object v4

    .line 89
    :cond_1
    const/4 v6, 0x0

    goto/16 :goto_0

    :cond_2
    const/4 v9, 0x0

    goto/16 :goto_1

    :cond_3
    iget-object v3, v6, Lgca;->mRoute:Liyg;

    move-object v14, v3

    goto/16 :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget-object v9, v0, Lfuz;->mEntry:Lizj;

    iget-object v10, v8, Ljak;->aeB:Ljbp;

    iget-object v4, v7, Ljal;->dMF:[Liyg;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v11

    const/4 v12, 0x1

    move-object/from16 v0, p0

    iget-object v13, v0, Lfww;->mStringEvaluator:Lgbr;

    move-object/from16 v8, p1

    invoke-static/range {v8 .. v13}, Lgbx;->a(Landroid/content/Context;Lizj;Ljbp;Ljava/lang/Iterable;ZLgbr;)Lanh;

    move-result-object v4

    invoke-virtual {v3}, Lanh;->hasBackgroundColor()Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, v7, Ljal;->dMF:[Liyg;

    array-length v3, v3

    const/4 v6, 0x1

    if-ne v3, v6, :cond_5

    iget-object v3, v4, Lanh;->agX:Lapj;

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lapj;->bk(Z)Lapj;

    :cond_5
    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, v7, Ljal;->dMF:[Liyg;

    array-length v3, v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    if-eqz v14, :cond_0

    iget-object v3, v14, Liyg;->dPL:Liyh;

    if-eqz v3, :cond_0

    iget-object v3, v14, Liyg;->dPL:Liyh;

    iget-object v3, v3, Liyh;->dQc:[Ljbp;

    array-length v3, v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v5, v7}, Lfww;->a(Landroid/content/Context;Lfmt;Lani;Ljal;)Lanh;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 91
    :cond_6
    iget-object v8, v7, Ljal;->dWL:Ljak;

    iget-object v3, v7, Ljal;->dMF:[Liyg;

    array-length v3, v3

    if-lez v3, :cond_a

    new-instance v6, Lgca;

    iget-object v3, v7, Ljal;->dMF:[Liyg;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lfuz;->mClock:Lemp;

    invoke-direct {v6, v3, v4}, Lgca;-><init>(Liyg;Lemp;)V

    :goto_6
    iget-object v3, v7, Ljal;->dMF:[Liyg;

    array-length v3, v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_b

    new-instance v3, Lgca;

    iget-object v4, v7, Ljal;->dMF:[Liyg;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lfuz;->mClock:Lemp;

    invoke-direct {v3, v4, v5}, Lgca;-><init>(Liyg;Lemp;)V

    move-object v11, v3

    :goto_7
    if-nez v6, :cond_c

    const/4 v3, 0x0

    move-object v10, v3

    :goto_8
    iget-object v3, v8, Ljak;->aeB:Ljbp;

    invoke-static {v3, v10}, Lfww;->a(Ljbp;Liyg;)Lani;

    move-result-object v5

    const/4 v9, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v9}, Lfww;->a(Landroid/content/Context;Lani;Lgca;Ljal;Ljak;Lfzb;)Lanh;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v5, v7}, Lfww;->a(Landroid/content/Context;Lfmt;Lani;Ljal;)Lanh;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v10, :cond_8

    iget-object v3, v10, Liyg;->dPO:[Ljhn;

    array-length v3, v3

    if-lez v3, :cond_8

    new-instance v3, Lanh;

    invoke-direct {v3}, Lanh;-><init>()V

    const/16 v4, 0x13

    invoke-virtual {v3, v4}, Lanh;->cm(I)Lanh;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfuz;->mEntry:Lizj;

    iput-object v4, v3, Lanh;->ahu:Lizj;

    new-instance v4, Lapa;

    invoke-direct {v4}, Lapa;-><init>()V

    iput-object v4, v3, Lanh;->agP:Lapa;

    iget-object v4, v3, Lanh;->agP:Lapa;

    move-object/from16 v0, p0

    iget-object v5, v0, Lfuz;->mCardContainer:Lfmt;

    invoke-interface {v5}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5, v10}, Lfyw;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Liyg;)[Laoz;

    move-result-object v5

    iput-object v5, v4, Lapa;->akz:[Laoz;

    invoke-virtual {v10}, Liyg;->bbK()Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, v3, Lanh;->agP:Lapa;

    invoke-virtual {v10}, Liyg;->bbJ()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lapa;->cF(Ljava/lang/String;)Lapa;

    :cond_7
    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    if-eqz v11, :cond_0

    iget-object v3, v8, Ljak;->aeB:Ljbp;

    new-instance v4, Lanh;

    invoke-direct {v4}, Lanh;-><init>()V

    const/16 v5, 0xc

    invoke-virtual {v4, v5}, Lanh;->cm(I)Lanh;

    move-object/from16 v0, p0

    iget-object v5, v0, Lfuz;->mEntry:Lizj;

    iput-object v5, v4, Lanh;->ahu:Lizj;

    new-instance v5, Laon;

    invoke-direct {v5}, Laon;-><init>()V

    iput-object v5, v4, Lanh;->agA:Laon;

    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v11, v5}, Lfww;->a(Landroid/content/Context;Lgca;Z)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Lgca;->br(Landroid/content/Context;)I

    move-result v6

    const-string v8, "<font color=\'%s\'>%s</font>"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v6}, Lgca;->jW(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v9, v10

    const/4 v6, 0x1

    aput-object v5, v9, v6

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v4, Lanh;->agA:Laon;

    const v8, 0x7f0a0184

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Laon;->ca(Ljava/lang/String;)Laon;

    const/4 v5, 0x0

    invoke-virtual {v3}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v11, v5, v6}, Lfww;->a(Landroid/content/Context;Lgca;Lfzb;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_9

    iget-object v6, v4, Lanh;->agA:Laon;

    invoke-virtual {v6, v5}, Laon;->cb(Ljava/lang/String;)Laon;

    :cond_9
    iget-object v5, v11, Lgca;->mRoute:Liyg;

    invoke-static {v3, v5}, Lfww;->a(Ljbp;Liyg;)Lani;

    move-result-object v3

    iput-object v3, v4, Lanh;->ahs:Lani;

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :cond_a
    const/4 v6, 0x0

    goto/16 :goto_6

    :cond_b
    const/4 v3, 0x0

    move-object v11, v3

    goto/16 :goto_7

    :cond_c
    iget-object v3, v6, Lgca;->mRoute:Liyg;

    move-object v10, v3

    goto/16 :goto_8

    .line 94
    :cond_d
    const/4 v3, 0x0

    goto/16 :goto_4

    .line 99
    :cond_e
    const/4 v3, 0x1

    goto/16 :goto_5
.end method

.method protected final a(Landroid/content/Context;Lanh;I)Lanh;
    .locals 12

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 288
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    .line 289
    invoke-static {v1}, Lgbf;->E(Lizj;)Ljal;

    move-result-object v9

    .line 290
    iget-object v10, v9, Ljal;->dWL:Ljak;

    .line 291
    iget-object v1, p0, Lfuz;->mCardContainer:Lfmt;

    invoke-interface {v1}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBO()Landroid/location/Location;

    move-result-object v1

    .line 293
    iget-object v2, v9, Ljal;->dMF:[Liyg;

    array-length v2, v2

    if-lez v2, :cond_3

    new-instance v2, Lgca;

    iget-object v3, v9, Ljal;->dMF:[Liyg;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget-object v4, p0, Lfuz;->mClock:Lemp;

    invoke-direct {v2, v3, v4}, Lgca;-><init>(Liyg;Lemp;)V

    .line 296
    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v2, v1}, Lgca;->p(Landroid/location/Location;)Z

    move-result v3

    if-nez v3, :cond_4

    :cond_0
    move-object v4, v0

    .line 300
    :goto_1
    invoke-virtual {p2}, Lanh;->getType()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 339
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lfuz;->a(Landroid/content/Context;Lanh;I)Lanh;

    move-result-object v0

    :cond_2
    :goto_2
    return-object v0

    :cond_3
    move-object v2, v0

    .line 293
    goto :goto_0

    .line 296
    :cond_4
    new-instance v4, Lfzb;

    iget-object v3, p0, Lfuz;->mClock:Lemp;

    iget-object v5, p0, Lfww;->mStringEvaluator:Lgbr;

    invoke-direct {v4, v3, v2, v5}, Lfzb;-><init>(Lemp;Lgca;Lgbr;)V

    goto :goto_1

    .line 303
    :sswitch_0
    if-eqz v4, :cond_1

    invoke-virtual {v4, v1}, Lfzb;->m(Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 305
    iget-object v3, v9, Ljal;->dMF:[Liyg;

    iget-object v5, p0, Lfuz;->mCardContainer:Lfmt;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lfww;->a(Landroid/content/Context;Lgca;[Liyg;Lfzb;Lfmt;)Lanh;

    move-result-object v0

    goto :goto_2

    .line 312
    :sswitch_1
    if-nez v2, :cond_5

    .line 314
    :goto_3
    iget-object v1, v10, Ljak;->aeB:Ljbp;

    invoke-static {v1, v0}, Lfww;->a(Ljbp;Liyg;)Lani;

    move-result-object v7

    move-object v5, p0

    move-object v6, p1

    move-object v8, v2

    move-object v11, v4

    .line 315
    invoke-direct/range {v5 .. v11}, Lfww;->a(Landroid/content/Context;Lani;Lgca;Ljal;Ljak;Lfzb;)Lanh;

    move-result-object v0

    goto :goto_2

    .line 312
    :cond_5
    iget-object v0, v2, Lgca;->mRoute:Liyg;

    goto :goto_3

    .line 321
    :sswitch_2
    if-eqz v4, :cond_6

    invoke-virtual {v4, v1}, Lfzb;->m(Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 323
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    invoke-static {p1, v10}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v10, Ljak;->aeB:Ljbp;

    invoke-virtual {v4, p1, v0, v1, v2}, Lfzb;->a(Landroid/content/Context;Lizj;Ljava/lang/String;Ljbp;)Lanh;

    move-result-object v0

    goto :goto_2

    .line 326
    :cond_6
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iget-object v2, v10, Ljak;->aeB:Ljbp;

    iget-object v0, v9, Ljal;->dMF:[Liyg;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iget-object v5, p0, Lfww;->mStringEvaluator:Lgbr;

    move-object v0, p1

    move v4, v6

    invoke-static/range {v0 .. v5}, Lgbx;->a(Landroid/content/Context;Lizj;Ljbp;Ljava/lang/Iterable;ZLgbr;)Lanh;

    move-result-object v0

    .line 329
    iget-object v1, p2, Lanh;->agX:Lapj;

    if-eqz v1, :cond_2

    iget-object v1, p2, Lanh;->agX:Lapj;

    invoke-virtual {v1}, Lapj;->sQ()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 331
    iget-object v1, v0, Lanh;->agX:Lapj;

    invoke-virtual {v1, v6}, Lapj;->bk(Z)Lapj;

    goto :goto_2

    .line 300
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x3 -> :sswitch_0
        0x1e -> :sswitch_2
        0x2d -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Landroid/app/Activity;Lfmt;)Lfry;
    .locals 15

    .prologue
    .line 626
    invoke-interface/range {p2 .. p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v1

    invoke-static {v1}, Lfph;->l(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lfph;

    move-result-object v2

    .line 629
    invoke-virtual {p0}, Lfww;->aDM()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 630
    invoke-direct {p0}, Lfww;->aDO()Liwk;

    move-result-object v1

    invoke-virtual {v1}, Liwk;->getType()I

    move-result v1

    const/16 v2, 0x11

    if-eq v1, v2, :cond_0

    const/16 v2, 0x12

    if-ne v1, v2, :cond_5

    :cond_0
    const/16 v2, 0x11

    if-ne v1, v2, :cond_2

    const v1, 0x7f0a0316

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v1, 0x7f0a0317

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_0
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iget-object v5, v1, Lizj;->dUo:[Liwk;

    array-length v6, v5

    const/4 v1, 0x0

    move v4, v1

    :goto_1
    if-ge v4, v6, :cond_4

    aget-object v1, v5, v4

    invoke-virtual {v1}, Liwk;->getType()I

    move-result v7

    const/16 v8, 0x11

    if-eq v7, v8, :cond_1

    invoke-virtual {v1}, Liwk;->getType()I

    move-result v7

    const/16 v8, 0x12

    if-ne v7, v8, :cond_3

    :cond_1
    move-object v6, v1

    :goto_2
    new-instance v1, Lfry;

    const v4, 0x7f0a031a

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x3d

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v5, v6}, Lfww;->a(Landroid/content/Context;ILiwk;)Lfrz;

    move-result-object v5

    const/16 v7, 0x3f

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v7, v6}, Lfww;->a(Landroid/content/Context;ILiwk;)Lfrz;

    move-result-object v6

    iget-object v8, p0, Lfuz;->mEntry:Lizj;

    const/16 v9, 0x3d

    const/16 v10, 0x3f

    move-object/from16 v7, p2

    invoke-direct/range {v1 .. v10}, Lfry;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lfrz;Lfrz;Lfmt;Lizj;II)V

    const/4 v2, 0x0

    iput-boolean v2, v1, Lfry;->cBv:Z

    .line 644
    :goto_3
    return-object v1

    .line 630
    :cond_2
    const v1, 0x7f0a0318

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v1, 0x7f0a0319

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    move-object v6, v1

    goto :goto_2

    :cond_5
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    const/16 v2, 0xe

    const/4 v3, 0x0

    new-array v3, v3, [I

    invoke-static {v1, v2, v3}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v6

    invoke-direct {p0}, Lfww;->aDO()Liwk;

    move-result-object v5

    new-instance v11, Lfry;

    const v1, 0x7f0a0314

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v1, 0x7f0a083c

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v12

    const v1, 0x7f0a083b

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/16 v1, 0x3e

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v1, v6}, Lfww;->a(Landroid/content/Context;ILiwk;)Lfrz;

    move-result-object v14

    if-eqz v5, :cond_6

    if-nez v6, :cond_7

    :cond_6
    const/4 v6, 0x0

    :goto_4
    iget-object v8, p0, Lfuz;->mEntry:Lizj;

    const/16 v9, 0xe

    const/16 v10, 0x40

    move-object v1, v11

    move-object v2, v7

    move-object v3, v12

    move-object v4, v13

    move-object v5, v14

    move-object/from16 v7, p2

    invoke-direct/range {v1 .. v10}, Lfry;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lfrz;Lfrz;Lfmt;Lizj;II)V

    const/4 v1, 0x0

    iput-boolean v1, v11, Lfry;->cBv:Z

    move-object v1, v11

    goto :goto_3

    :cond_7
    new-instance v1, Lfxc;

    const/16 v4, 0x40

    move-object v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v1 .. v6}, Lfxc;-><init>(Lfww;Landroid/content/Context;ILiwk;Liwk;)V

    move-object v6, v1

    goto :goto_4

    .line 631
    :cond_8
    invoke-direct {p0}, Lfww;->aDN()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {v2}, Lfph;->TC()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v2}, Lfph;->aCD()Z

    move-result v1

    if-nez v1, :cond_a

    :cond_9
    const/4 v1, 0x1

    :goto_5
    if-eqz v1, :cond_d

    .line 632
    invoke-virtual {v2}, Lfph;->TC()Z

    move-result v1

    .line 633
    invoke-virtual {v2}, Lfph;->aCD()Z

    move-result v3

    .line 634
    invoke-virtual {v2}, Lfph;->aCE()Z

    move-result v2

    .line 636
    if-nez v2, :cond_b

    if-nez v1, :cond_b

    if-nez v3, :cond_b

    .line 637
    new-instance v1, Lfry;

    const v2, 0x7f0a030d

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lfww;->k(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    const v3, 0x7f0a0310

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a0311

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lfwy;

    move-object/from16 v0, p2

    invoke-direct {v5, p0, v0}, Lfwy;-><init>(Lfww;Lfmt;)V

    invoke-direct/range {p0 .. p2}, Lfww;->f(Landroid/content/Context;Lfmt;)Lfrz;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lfry;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lfrz;Lfrz;)V

    goto/16 :goto_3

    .line 631
    :cond_a
    const/4 v1, 0x0

    goto :goto_5

    .line 638
    :cond_b
    if-eqz v1, :cond_c

    if-nez v3, :cond_c

    .line 639
    new-instance v1, Lfry;

    const v2, 0x7f0a030f

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a0312

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a0313

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lfww;->c(Lfmt;)Lfrz;

    move-result-object v5

    invoke-direct/range {p0 .. p2}, Lfww;->f(Landroid/content/Context;Lfmt;)Lfrz;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lfry;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lfrz;Lfrz;)V

    goto/16 :goto_3

    .line 640
    :cond_c
    if-nez v2, :cond_d

    if-nez v1, :cond_d

    if-eqz v3, :cond_d

    .line 641
    new-instance v1, Lfry;

    const v2, 0x7f0a030e

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lfww;->k(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v2

    const v3, 0x7f0a0310

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a0311

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lfww;->c(Lfmt;)Lfrz;

    move-result-object v5

    invoke-direct/range {p0 .. p2}, Lfww;->f(Landroid/content/Context;Lfmt;)Lfrz;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lfry;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lfrz;Lfrz;)V

    goto/16 :goto_3

    .line 644
    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_3
.end method

.method public final a(Landroid/content/Context;Lgca;Ljal;Ljak;Lfzb;)Ljava/lang/String;
    .locals 5
    .param p5    # Lfzb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 498
    if-eqz p2, :cond_4

    .line 499
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lgca;->aEG()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p3, Ljal;->dMF:[Liyg;

    array-length v0, v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 500
    invoke-virtual {p2, p1}, Lgca;->by(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 516
    :cond_0
    :goto_1
    return-object v0

    :cond_1
    move v0, v2

    .line 499
    goto :goto_0

    .line 502
    :cond_2
    if-eqz p5, :cond_3

    .line 503
    iget-object v0, p0, Lfuz;->mCardContainer:Lfmt;

    invoke-virtual {p5, p1, v0, v2}, Lfzb;->b(Landroid/content/Context;Lfmt;Z)Ljava/lang/String;

    move-result-object v0

    .line 505
    if-nez v0, :cond_0

    .line 509
    :cond_3
    invoke-static {p1, p2, v1}, Lfww;->a(Landroid/content/Context;Lgca;Z)Ljava/lang/String;

    move-result-object v0

    .line 510
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 511
    const v3, 0x7f0a0183

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v2

    invoke-static {p1, p4}, Lfww;->b(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 516
    :cond_4
    invoke-static {p1, p4}, Lfww;->b(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final aAx()Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 105
    .line 107
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v5, v0, Lizj;->dUo:[Liwk;

    array-length v6, v5

    move v4, v3

    move v0, v3

    move v2, v3

    :goto_0
    if-ge v4, v6, :cond_2

    aget-object v7, v5, v4

    .line 108
    invoke-virtual {v7}, Liwk;->getType()I

    move-result v8

    const/16 v9, 0x10

    if-ne v8, v9, :cond_1

    move v2, v1

    .line 107
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 110
    :cond_1
    invoke-virtual {v7}, Liwk;->getType()I

    move-result v7

    const/16 v8, 0xe

    if-ne v7, v8, :cond_0

    move v0, v1

    .line 111
    goto :goto_1

    .line 118
    :cond_2
    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    :goto_2
    return v1

    :cond_3
    move v1, v3

    goto :goto_2
.end method

.method public final aDM()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 648
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    const/16 v2, 0x10

    new-array v3, v0, [I

    invoke-static {v1, v2, v3}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final b(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lftp;
    .locals 11
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 126
    const v0, 0x7f040043

    const/4 v1, 0x0

    invoke-virtual {p3, v0, p4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 127
    const v0, 0x7f11011e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 128
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    invoke-static {v1}, Lgbf;->E(Lizj;)Ljal;

    move-result-object v1

    .line 129
    iget-object v2, v1, Ljal;->dWL:Ljak;

    .line 130
    if-eqz v2, :cond_0

    iget-object v4, v2, Ljak;->aeB:Ljbp;

    if-eqz v4, :cond_0

    .line 131
    iget-object v4, v2, Ljak;->aeB:Ljbp;

    .line 132
    new-instance v5, Ljcq;

    invoke-direct {v5}, Ljcq;-><init>()V

    .line 133
    iput-object v4, v5, Ljcq;->aeB:Ljbp;

    .line 134
    const/16 v6, 0x16

    invoke-virtual {v5, v6}, Ljcq;->oJ(I)Ljcq;

    .line 135
    new-instance v5, Lfox;

    invoke-direct {v5}, Lfox;-><init>()V

    .line 136
    iput-object v1, v5, Lfox;->mFrequentPlaceEntry:Ljal;

    .line 137
    invoke-interface {p2}, Lfmt;->aAD()Lfml;

    move-result-object v1

    iget-object v1, v1, Lfml;->cwa:Lgbo;

    invoke-virtual {v5}, Lfox;->aCh()Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;

    move-result-object v5

    invoke-virtual {v1, v5, v0}, Lgbo;->a(Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;Landroid/widget/ImageView;)V

    .line 140
    new-instance v0, Lfwx;

    invoke-direct {v0, p0, p1, v4}, Lfwx;-><init>(Lfww;Landroid/content/Context;Ljbp;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    :cond_0
    const v0, 0x7f11011f

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 150
    if-eqz v2, :cond_2

    .line 151
    iget-object v1, v2, Ljak;->clz:Ljcu;

    if-eqz v1, :cond_1

    iget-object v1, v2, Ljak;->clz:Ljcu;

    iget-object v1, v1, Ljcu;->dME:Lixn;

    if-eqz v1, :cond_1

    iget-object v1, v2, Ljak;->clz:Ljcu;

    iget-object v1, v1, Ljcu;->dME:Lixn;

    invoke-virtual {v1}, Lixn;->oK()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v2, Ljak;->clz:Ljcu;

    iget-object v1, v1, Ljcu;->dME:Lixn;

    invoke-virtual {v1}, Lixn;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 157
    new-instance v0, Lftp;

    const v1, 0x7f0a0314

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0315

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v4, 0x7f0a083c

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v6, 0x7f0a083b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lfuz;->mEntry:Lizj;

    const/16 v7, 0xe

    const/4 v8, 0x0

    new-array v8, v8, [I

    invoke-static {v6, v7, v8}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v7

    if-nez v7, :cond_3

    const/4 v6, 0x0

    :goto_1
    invoke-direct {p0}, Lfww;->aDO()Liwk;

    move-result-object v8

    if-nez v8, :cond_4

    const/4 v7, 0x0

    :goto_2
    invoke-direct/range {v0 .. v7}, Lftp;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lftq;Lftq;)V

    .line 165
    return-object v0

    .line 151
    :cond_1
    invoke-static {p1, v2}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 153
    :cond_2
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 157
    :cond_3
    new-instance v6, Lfxd;

    invoke-direct {v6, p0, p1, v7}, Lfxd;-><init>(Lfww;Landroid/content/Context;Liwk;)V

    goto :goto_1

    :cond_4
    iget-object v7, p0, Lfuz;->mEntry:Lizj;

    const/16 v9, 0xe

    const/4 v10, 0x0

    new-array v10, v10, [I

    invoke-static {v7, v9, v10}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v9

    if-nez v9, :cond_5

    const/4 v7, 0x0

    goto :goto_2

    :cond_5
    new-instance v7, Lfxe;

    invoke-direct {v7, p0, p1, v8, v9}, Lfxe;-><init>(Lfww;Landroid/content/Context;Liwk;Liwk;)V

    goto :goto_2
.end method

.method final l(Landroid/content/Context;I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 828
    invoke-static {}, Lfzv;->aDW()Landroid/content/Intent;

    move-result-object v0

    .line 829
    const-string v1, "action_type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 830
    const-string v1, "entry"

    iget-object v2, p0, Lfww;->mEntry:Lizj;

    invoke-static {v0, v1, v2}, Leqh;->a(Landroid/content/Intent;Ljava/lang/String;Ljsr;)V

    .line 831
    return-object v0
.end method
