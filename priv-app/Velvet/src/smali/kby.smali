.class public final Lkby;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final eQX:Lkap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lkbz;

    invoke-direct {v0}, Lkbz;-><init>()V

    sput-object v0, Lkby;->eQX:Lkap;

    return-void
.end method

.method private static a(Lkcq;Ljava/lang/String;)Ljava/util/List;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 131
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v6, v4

    .line 132
    :goto_0
    invoke-virtual {p0}, Lkcq;->length()I

    move-result v0

    if-ge v6, v0, :cond_1

    .line 133
    invoke-virtual {p0, v6}, Lkcq;->tL(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {p0, v6}, Lkcq;->getValue(I)Ljava/lang/String;

    move-result-object v0

    move v1, v4

    .line 138
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 140
    const-string v2, " "

    invoke-static {v0, v1, v2}, Lkbw;->b(Ljava/lang/String;ILjava/lang/String;)I

    move-result v2

    .line 142
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 143
    invoke-static {v0, v2}, Lkbw;->R(Ljava/lang/String;I)I

    move-result v2

    .line 149
    const/4 v1, 0x1

    const-string v3, "realm=\""

    const/4 v5, 0x7

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    add-int/lit8 v1, v2, 0x7

    .line 155
    const-string v2, "\""

    invoke-static {v0, v1, v2}, Lkbw;->b(Ljava/lang/String;ILjava/lang/String;)I

    move-result v2

    .line 156
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 157
    add-int/lit8 v1, v2, 0x1

    .line 158
    const-string v2, ","

    invoke-static {v0, v1, v2}, Lkbw;->b(Ljava/lang/String;ILjava/lang/String;)I

    move-result v1

    .line 159
    add-int/lit8 v1, v1, 0x1

    .line 160
    invoke-static {v0, v1}, Lkbw;->R(Ljava/lang/String;I)I

    move-result v1

    .line 161
    new-instance v2, Lkaq;

    invoke-direct {v2, v8, v3}, Lkaq;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 132
    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 164
    :cond_1
    return-object v7
.end method

.method public static a(Lkap;ILkcq;Lkcq;Ljava/net/Proxy;Ljava/net/URL;)Z
    .locals 5

    .prologue
    const/16 v4, 0x197

    const/4 v2, 0x0

    .line 96
    const/16 v0, 0x191

    if-ne p1, v0, :cond_0

    .line 97
    const-string v1, "WWW-Authenticate"

    .line 98
    const-string v0, "Authorization"

    .line 105
    :goto_0
    invoke-static {p2, v1}, Lkby;->a(Lkcq;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 106
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v2

    .line 117
    :goto_1
    return v0

    .line 99
    :cond_0
    if-ne p1, v4, :cond_1

    .line 100
    const-string v1, "Proxy-Authenticate"

    .line 101
    const-string v0, "Proxy-Authorization"

    goto :goto_0

    .line 103
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 109
    :cond_2
    invoke-virtual {p2}, Lkcq;->getResponseCode()I

    move-result v3

    if-ne v3, v4, :cond_3

    invoke-interface {p0, p4, p5, v1}, Lkap;->b(Ljava/net/Proxy;Ljava/net/URL;Ljava/util/List;)Lkar;

    move-result-object v1

    .line 112
    :goto_2
    if-nez v1, :cond_4

    move v0, v2

    .line 113
    goto :goto_1

    .line 109
    :cond_3
    invoke-interface {p0, p4, p5, v1}, Lkap;->a(Ljava/net/Proxy;Ljava/net/URL;Ljava/util/List;)Lkar;

    move-result-object v1

    goto :goto_2

    .line 116
    :cond_4
    iget-object v1, v1, Lkar;->ePF:Ljava/lang/String;

    invoke-virtual {p3, v0, v1}, Lkcq;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const/4 v0, 0x1

    goto :goto_1
.end method
