.class public final Ljxx;
.super Ljsl;
.source "PG"


# instance fields
.field public eLr:[Ljyc;

.field public eLs:Ljyb;

.field public eLt:Ljxy;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 363
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 364
    invoke-static {}, Ljyc;->bwb()[Ljyc;

    move-result-object v0

    iput-object v0, p0, Ljxx;->eLr:[Ljyc;

    iput-object v1, p0, Ljxx;->eLs:Ljyb;

    iput-object v1, p0, Ljxx;->eLt:Ljxy;

    iput-object v1, p0, Ljxx;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljxx;->eCz:I

    .line 365
    return-void
.end method

.method public static aC([B)Ljxx;
    .locals 1

    .prologue
    .line 474
    new-instance v0, Ljxx;

    invoke-direct {v0}, Ljxx;-><init>()V

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Ljxx;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 337
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljxx;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljxx;->eLr:[Ljyc;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljyc;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljxx;->eLr:[Ljyc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljyc;

    invoke-direct {v3}, Ljyc;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljxx;->eLr:[Ljyc;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljyc;

    invoke-direct {v3}, Ljyc;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljxx;->eLr:[Ljyc;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljxx;->eLs:Ljyb;

    if-nez v0, :cond_4

    new-instance v0, Ljyb;

    invoke-direct {v0}, Ljyb;-><init>()V

    iput-object v0, p0, Ljxx;->eLs:Ljyb;

    :cond_4
    iget-object v0, p0, Ljxx;->eLs:Ljyb;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljxx;->eLt:Ljxy;

    if-nez v0, :cond_5

    new-instance v0, Ljxy;

    invoke-direct {v0}, Ljxy;-><init>()V

    iput-object v0, p0, Ljxx;->eLt:Ljxy;

    :cond_5
    iget-object v0, p0, Ljxx;->eLt:Ljxy;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 379
    iget-object v0, p0, Ljxx;->eLr:[Ljyc;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljxx;->eLr:[Ljyc;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 380
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljxx;->eLr:[Ljyc;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 381
    iget-object v1, p0, Ljxx;->eLr:[Ljyc;

    aget-object v1, v1, v0

    .line 382
    if-eqz v1, :cond_0

    .line 383
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 380
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 387
    :cond_1
    iget-object v0, p0, Ljxx;->eLs:Ljyb;

    if-eqz v0, :cond_2

    .line 388
    const/4 v0, 0x2

    iget-object v1, p0, Ljxx;->eLs:Ljyb;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 390
    :cond_2
    iget-object v0, p0, Ljxx;->eLt:Ljxy;

    if-eqz v0, :cond_3

    .line 391
    const/4 v0, 0x3

    iget-object v1, p0, Ljxx;->eLt:Ljxy;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 393
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 394
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 398
    invoke-super {p0}, Ljsl;->lF()I

    move-result v1

    .line 399
    iget-object v0, p0, Ljxx;->eLr:[Ljyc;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljxx;->eLr:[Ljyc;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 400
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ljxx;->eLr:[Ljyc;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 401
    iget-object v2, p0, Ljxx;->eLr:[Ljyc;

    aget-object v2, v2, v0

    .line 402
    if-eqz v2, :cond_0

    .line 403
    const/4 v3, 0x1

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 400
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 408
    :cond_1
    iget-object v0, p0, Ljxx;->eLs:Ljyb;

    if-eqz v0, :cond_2

    .line 409
    const/4 v0, 0x2

    iget-object v2, p0, Ljxx;->eLs:Ljyb;

    invoke-static {v0, v2}, Ljsj;->c(ILjsr;)I

    move-result v0

    add-int/2addr v1, v0

    .line 412
    :cond_2
    iget-object v0, p0, Ljxx;->eLt:Ljxy;

    if-eqz v0, :cond_3

    .line 413
    const/4 v0, 0x3

    iget-object v2, p0, Ljxx;->eLt:Ljxy;

    invoke-static {v0, v2}, Ljsj;->c(ILjsr;)I

    move-result v0

    add-int/2addr v1, v0

    .line 416
    :cond_3
    return v1
.end method
