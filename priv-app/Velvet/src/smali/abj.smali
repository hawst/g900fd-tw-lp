.class public final Labj;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static OL:F

.field private static final OM:[F

.field private static final ON:[F

.field private static OR:F

.field private static OS:F


# instance fields
.field private OA:I

.field private OB:I

.field private OC:F

.field private OD:F

.field private OE:F

.field private OF:Z

.field private OG:Landroid/animation/TimeInterpolator;

.field private OH:Z

.field private OI:F

.field private OJ:I

.field private OK:F

.field private OO:F

.field private final OP:F

.field private OQ:F

.field private Oq:I

.field private Or:I

.field private Os:I

.field private Ot:I

.field private Ou:I

.field private Ov:I

.field private Ow:I

.field private Ox:I

.field private Oy:I

.field private Oz:I

.field private hV:J

.field private sG:F


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const v13, 0x3e333333    # 0.175f

    const/4 v4, 0x0

    const-wide v14, 0x3ee4f8b588e368f1L    # 1.0E-5

    const/16 v12, 0x64

    const/high16 v1, 0x3f800000    # 1.0f

    .line 66
    const-wide v2, 0x3fe8f5c28f5c28f6L    # 0.78

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    const-wide v6, 0x3feccccccccccccdL    # 0.9

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    div-double/2addr v2, v6

    double-to-float v0, v2

    sput v0, Labj;->OL:F

    .line 74
    const/16 v0, 0x65

    new-array v0, v0, [F

    sput-object v0, Labj;->OM:[F

    .line 75
    const/16 v0, 0x65

    new-array v0, v0, [F

    sput-object v0, Labj;->ON:[F

    .line 86
    const/4 v0, 0x0

    move v5, v0

    move v2, v4

    :goto_0
    if-ge v5, v12, :cond_4

    .line 87
    int-to-float v0, v5

    const/high16 v3, 0x42c80000    # 100.0f

    div-float v6, v0, v3

    move v0, v1

    move v3, v2

    .line 92
    :goto_1
    sub-float v2, v0, v3

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v2, v7

    add-float/2addr v2, v3

    .line 93
    const/high16 v7, 0x40400000    # 3.0f

    mul-float/2addr v7, v2

    sub-float v8, v1, v2

    mul-float/2addr v7, v8

    .line 94
    sub-float v8, v1, v2

    mul-float/2addr v8, v13

    const v9, 0x3eb33334    # 0.35000002f

    mul-float/2addr v9, v2

    add-float/2addr v8, v9

    mul-float/2addr v8, v7

    mul-float v9, v2, v2

    mul-float/2addr v9, v2

    add-float/2addr v8, v9

    .line 95
    sub-float v9, v8, v6

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    float-to-double v10, v9

    cmpg-double v9, v10, v14

    if-ltz v9, :cond_1

    .line 96
    cmpl-float v7, v8, v6

    if-lez v7, :cond_0

    move v0, v2

    goto :goto_1

    :cond_0
    move v3, v2

    .line 97
    goto :goto_1

    .line 99
    :cond_1
    sget-object v0, Labj;->OM:[F

    sub-float v8, v1, v2

    const/high16 v9, 0x3f000000    # 0.5f

    mul-float/2addr v8, v9

    add-float/2addr v8, v2

    mul-float/2addr v7, v8

    mul-float v8, v2, v2

    mul-float/2addr v2, v8

    add-float/2addr v2, v7

    aput v2, v0, v5

    move v0, v1

    .line 104
    :goto_2
    sub-float v2, v0, v4

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v2, v7

    add-float/2addr v2, v4

    .line 105
    const/high16 v7, 0x40400000    # 3.0f

    mul-float/2addr v7, v2

    sub-float v8, v1, v2

    mul-float/2addr v7, v8

    .line 106
    sub-float v8, v1, v2

    const/high16 v9, 0x3f000000    # 0.5f

    mul-float/2addr v8, v9

    add-float/2addr v8, v2

    mul-float/2addr v8, v7

    mul-float v9, v2, v2

    mul-float/2addr v9, v2

    add-float/2addr v8, v9

    .line 107
    sub-float v9, v8, v6

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    float-to-double v10, v9

    cmpg-double v9, v10, v14

    if-ltz v9, :cond_3

    .line 108
    cmpl-float v7, v8, v6

    if-lez v7, :cond_2

    move v0, v2

    goto :goto_2

    :cond_2
    move v4, v2

    .line 109
    goto :goto_2

    .line 111
    :cond_3
    sget-object v0, Labj;->ON:[F

    sub-float v6, v1, v2

    mul-float/2addr v6, v13

    const v8, 0x3eb33334    # 0.35000002f

    mul-float/2addr v8, v2

    add-float/2addr v6, v8

    mul-float/2addr v6, v7

    mul-float v7, v2, v2

    mul-float/2addr v2, v7

    add-float/2addr v2, v6

    aput v2, v0, v5

    .line 86
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v2, v3

    goto/16 :goto_0

    .line 113
    :cond_4
    sget-object v0, Labj;->OM:[F

    sget-object v2, Labj;->ON:[F

    aput v1, v2, v12

    aput v1, v0, v12

    .line 116
    const/high16 v0, 0x41000000    # 8.0f

    sput v0, Labj;->OR:F

    .line 118
    sput v1, Labj;->OS:F

    .line 119
    invoke-static {v1}, Labj;->p(F)F

    move-result v0

    div-float v0, v1, v0

    sput v0, Labj;->OS:F

    .line 121
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Labj;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    .line 135
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
    .locals 3

    .prologue
    .line 143
    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, v1, v0}, Labj;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V

    .line 145
    return-void

    .line 143
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V
    .locals 2

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v0

    iput v0, p0, Labj;->OK:F

    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Labj;->OF:Z

    .line 154
    iput-object p2, p0, Labj;->OG:Landroid/animation/TimeInterpolator;

    .line 155
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x43200000    # 160.0f

    mul-float/2addr v0, v1

    iput v0, p0, Labj;->OP:F

    .line 156
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v0

    invoke-direct {p0, v0}, Labj;->n(F)F

    move-result v0

    iput v0, p0, Labj;->OO:F

    .line 157
    iput-boolean p3, p0, Labj;->OH:Z

    .line 159
    const v0, 0x3f570a3d    # 0.84f

    invoke-direct {p0, v0}, Labj;->n(F)F

    move-result v0

    iput v0, p0, Labj;->OQ:F

    .line 160
    return-void
.end method

.method private n(F)F
    .locals 2

    .prologue
    .line 175
    const v0, 0x43c10b3d

    iget v1, p0, Labj;->OP:F

    mul-float/2addr v0, v1

    mul-float/2addr v0, p1

    return v0
.end method

.method private o(F)D
    .locals 3

    .prologue
    .line 460
    const v0, 0x3eb33333    # 0.35f

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    mul-float/2addr v0, v1

    iget v1, p0, Labj;->OK:F

    iget v2, p0, Labj;->OQ:F

    mul-float/2addr v1, v2

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    return-wide v0
.end method

.method private static p(F)F
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 477
    sget v0, Labj;->OR:F

    mul-float/2addr v0, p0

    .line 478
    cmpg-float v1, v0, v4

    if-gez v1, :cond_0

    .line 479
    neg-float v1, v0

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    double-to-float v1, v2

    sub-float v1, v4, v1

    sub-float/2addr v0, v1

    .line 485
    :goto_0
    sget v1, Labj;->OS:F

    mul-float/2addr v0, v1

    .line 486
    return v0

    .line 481
    :cond_0
    sub-float v0, v4, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    double-to-float v0, v0

    sub-float v0, v4, v0

    .line 483
    const v1, 0x3ebc5ab2

    const v2, 0x3f21d2a7

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final abortAnimation()V
    .locals 1

    .prologue
    .line 497
    iget v0, p0, Labj;->Ot:I

    iput v0, p0, Labj;->Oz:I

    .line 498
    iget v0, p0, Labj;->Ou:I

    iput v0, p0, Labj;->OA:I

    .line 499
    const/4 v0, 0x1

    iput-boolean v0, p0, Labj;->OF:Z

    .line 500
    return-void
.end method

.method public final computeScrollOffset()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/high16 v6, 0x42c80000    # 100.0f

    .line 279
    iget-boolean v0, p0, Labj;->OF:Z

    if-eqz v0, :cond_0

    .line 280
    const/4 v0, 0x0

    .line 336
    :goto_0
    return v0

    .line 283
    :cond_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iget-wide v4, p0, Labj;->hV:J

    sub-long/2addr v0, v4

    long-to-int v0, v0

    .line 285
    iget v1, p0, Labj;->OB:I

    if-ge v0, v1, :cond_4

    .line 286
    iget v1, p0, Labj;->Oq:I

    packed-switch v1, :pswitch_data_0

    :cond_1
    :goto_1
    move v0, v2

    .line 336
    goto :goto_0

    .line 288
    :pswitch_0
    int-to-float v0, v0

    iget v1, p0, Labj;->OC:F

    mul-float/2addr v0, v1

    .line 290
    iget-object v1, p0, Labj;->OG:Landroid/animation/TimeInterpolator;

    if-nez v1, :cond_2

    .line 291
    invoke-static {v0}, Labj;->p(F)F

    move-result v0

    .line 295
    :goto_2
    iget v1, p0, Labj;->Or:I

    iget v3, p0, Labj;->OD:F

    mul-float/2addr v3, v0

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    add-int/2addr v1, v3

    iput v1, p0, Labj;->Oz:I

    .line 296
    iget v1, p0, Labj;->Os:I

    iget v3, p0, Labj;->OE:F

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-int/2addr v0, v1

    iput v0, p0, Labj;->OA:I

    goto :goto_1

    .line 293
    :cond_2
    iget-object v1, p0, Labj;->OG:Landroid/animation/TimeInterpolator;

    invoke-interface {v1, v0}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    goto :goto_2

    .line 299
    :pswitch_1
    int-to-float v0, v0

    iget v1, p0, Labj;->OB:I

    int-to-float v1, v1

    div-float v3, v0, v1

    .line 300
    mul-float v0, v6, v3

    float-to-int v4, v0

    .line 301
    const/high16 v1, 0x3f800000    # 1.0f

    .line 302
    const/4 v0, 0x0

    .line 303
    const/16 v5, 0x64

    if-ge v4, v5, :cond_3

    .line 304
    int-to-float v0, v4

    div-float v1, v0, v6

    .line 305
    add-int/lit8 v0, v4, 0x1

    int-to-float v0, v0

    div-float/2addr v0, v6

    .line 306
    sget-object v5, Labj;->OM:[F

    aget v5, v5, v4

    .line 307
    sget-object v6, Labj;->OM:[F

    add-int/lit8 v4, v4, 0x1

    aget v4, v6, v4

    .line 308
    sub-float/2addr v4, v5

    sub-float/2addr v0, v1

    div-float v0, v4, v0

    .line 309
    sub-float v1, v3, v1

    mul-float/2addr v1, v0

    add-float/2addr v1, v5

    .line 312
    :cond_3
    iget v3, p0, Labj;->OJ:I

    int-to-float v3, v3

    mul-float/2addr v0, v3

    iget v3, p0, Labj;->OB:I

    int-to-float v3, v3

    div-float/2addr v0, v3

    const/high16 v3, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v3

    iput v0, p0, Labj;->OI:F

    .line 314
    iget v0, p0, Labj;->Or:I

    iget v3, p0, Labj;->Ot:I

    iget v4, p0, Labj;->Or:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v1

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    add-int/2addr v0, v3

    iput v0, p0, Labj;->Oz:I

    .line 316
    iget v0, p0, Labj;->Oz:I

    iget v3, p0, Labj;->Ow:I

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Labj;->Oz:I

    .line 317
    iget v0, p0, Labj;->Oz:I

    iget v3, p0, Labj;->Ov:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Labj;->Oz:I

    .line 319
    iget v0, p0, Labj;->Os:I

    iget v3, p0, Labj;->Ou:I

    iget v4, p0, Labj;->Os:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Labj;->OA:I

    .line 321
    iget v0, p0, Labj;->OA:I

    iget v1, p0, Labj;->Oy:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Labj;->OA:I

    .line 322
    iget v0, p0, Labj;->OA:I

    iget v1, p0, Labj;->Ox:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Labj;->OA:I

    .line 324
    iget v0, p0, Labj;->Oz:I

    iget v1, p0, Labj;->Ot:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Labj;->OA:I

    iget v1, p0, Labj;->Ou:I

    if-ne v0, v1, :cond_1

    .line 325
    iput-boolean v2, p0, Labj;->OF:Z

    goto/16 :goto_1

    .line 332
    :cond_4
    iget v0, p0, Labj;->Ot:I

    iput v0, p0, Labj;->Oz:I

    .line 333
    iget v0, p0, Labj;->Ou:I

    iput v0, p0, Labj;->OA:I

    .line 334
    iput-boolean v2, p0, Labj;->OF:Z

    goto/16 :goto_1

    .line 286
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final fling(IIIIIIII)V
    .locals 14

    .prologue
    .line 407
    iget-boolean v2, p0, Labj;->OH:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Labj;->OF:Z

    if-nez v2, :cond_0

    .line 408
    iget v2, p0, Labj;->Oq:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    iget v2, p0, Labj;->OI:F

    .line 410
    :goto_0
    iget v3, p0, Labj;->Ot:I

    iget v4, p0, Labj;->Or:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    .line 411
    iget v4, p0, Labj;->Ou:I

    iget v5, p0, Labj;->Os:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    .line 412
    mul-float v5, v3, v3

    mul-float v6, v4, v4

    add-float/2addr v5, v6

    invoke-static {v5}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v5

    .line 414
    div-float/2addr v3, v5

    .line 415
    div-float/2addr v4, v5

    .line 417
    mul-float/2addr v3, v2

    .line 418
    mul-float/2addr v2, v4

    .line 419
    move/from16 v0, p3

    int-to-float v4, v0

    invoke-static {v4}, Ljava/lang/Math;->signum(F)F

    move-result v4

    invoke-static {v3}, Ljava/lang/Math;->signum(F)F

    move-result v5

    cmpl-float v4, v4, v5

    if-nez v4, :cond_0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Math;->signum(F)F

    move-result v4

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v5

    cmpl-float v4, v4, v5

    if-nez v4, :cond_0

    .line 421
    move/from16 v0, p3

    int-to-float v4, v0

    add-float/2addr v3, v4

    float-to-int v0, v3

    move/from16 p3, v0

    .line 422
    const/4 v3, 0x0

    add-float/2addr v2, v3

    float-to-int v0, v2

    move/from16 p4, v0

    .line 426
    :cond_0
    const/4 v2, 0x1

    iput v2, p0, Labj;->Oq:I

    .line 427
    const/4 v2, 0x0

    iput-boolean v2, p0, Labj;->OF:Z

    .line 429
    mul-int v2, p3, p3

    mul-int v3, p4, p4

    add-int/2addr v2, v3

    int-to-float v2, v2

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v4

    .line 431
    iput v4, p0, Labj;->sG:F

    .line 432
    invoke-direct {p0, v4}, Labj;->o(F)D

    move-result-wide v2

    sget v5, Labj;->OL:F

    float-to-double v6, v5

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v6, v8

    const-wide v8, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    mul-double/2addr v2, v8

    double-to-int v2, v2

    iput v2, p0, Labj;->OB:I

    .line 433
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Labj;->hV:J

    .line 434
    iput p1, p0, Labj;->Or:I

    .line 435
    move/from16 v0, p2

    iput v0, p0, Labj;->Os:I

    .line 437
    const/4 v2, 0x0

    cmpl-float v2, v4, v2

    if-nez v2, :cond_2

    const/high16 v2, 0x3f800000    # 1.0f

    move v3, v2

    .line 438
    :goto_1
    const/4 v2, 0x0

    cmpl-float v2, v4, v2

    if-nez v2, :cond_3

    const/high16 v2, 0x3f800000    # 1.0f

    .line 440
    :goto_2
    invoke-direct {p0, v4}, Labj;->o(F)D

    move-result-wide v6

    sget v5, Labj;->OL:F

    float-to-double v8, v5

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v8, v10

    iget v5, p0, Labj;->OK:F

    iget v10, p0, Labj;->OQ:F

    mul-float/2addr v5, v10

    float-to-double v10, v5

    sget v5, Labj;->OL:F

    float-to-double v12, v5

    div-double v8, v12, v8

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    mul-double/2addr v6, v10

    .line 441
    invoke-static {v4}, Ljava/lang/Math;->signum(F)F

    move-result v4

    float-to-double v4, v4

    mul-double/2addr v4, v6

    double-to-int v4, v4

    iput v4, p0, Labj;->OJ:I

    .line 443
    const/high16 v4, -0x80000000

    iput v4, p0, Labj;->Ov:I

    .line 444
    const v4, 0x7fffffff

    iput v4, p0, Labj;->Ow:I

    .line 445
    const/4 v4, 0x0

    iput v4, p0, Labj;->Ox:I

    .line 446
    const/4 v4, 0x0

    iput v4, p0, Labj;->Oy:I

    .line 448
    float-to-double v4, v3

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v3, v4

    add-int/2addr v3, p1

    iput v3, p0, Labj;->Ot:I

    .line 450
    iget v3, p0, Labj;->Ot:I

    iget v4, p0, Labj;->Ow:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Labj;->Ot:I

    .line 451
    iget v3, p0, Labj;->Ot:I

    iget v4, p0, Labj;->Ov:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, p0, Labj;->Ot:I

    .line 453
    float-to-double v2, v2

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    add-int v2, v2, p2

    iput v2, p0, Labj;->Ou:I

    .line 455
    iget v2, p0, Labj;->Ou:I

    iget v3, p0, Labj;->Oy:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Labj;->Ou:I

    .line 456
    iget v2, p0, Labj;->Ou:I

    iget v3, p0, Labj;->Ox:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Labj;->Ou:I

    .line 457
    return-void

    .line 408
    :cond_1
    iget v2, p0, Labj;->sG:F

    iget v3, p0, Labj;->OO:F

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Labj;->hV:J

    sub-long/2addr v4, v6

    long-to-int v4, v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    const/high16 v4, 0x44fa0000    # 2000.0f

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    goto/16 :goto_0

    .line 437
    :cond_2
    move/from16 v0, p3

    int-to-float v2, v0

    div-float/2addr v2, v4

    move v3, v2

    goto/16 :goto_1

    .line 438
    :cond_3
    move/from16 v0, p4

    int-to-float v2, v0

    div-float/2addr v2, v4

    goto/16 :goto_2
.end method

.method public final forceFinished(Z)V
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x1

    iput-boolean v0, p0, Labj;->OF:Z

    .line 198
    return-void
.end method

.method public final getCurrX()I
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Labj;->Oz:I

    return v0
.end method

.method public final getCurrY()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Labj;->OA:I

    return v0
.end method

.method public final getFinalX()I
    .locals 1

    .prologue
    .line 262
    iget v0, p0, Labj;->Ot:I

    return v0
.end method

.method public final isFinished()Z
    .locals 1

    .prologue
    .line 188
    iget-boolean v0, p0, Labj;->OF:Z

    return v0
.end method

.method public final setFinalX(I)V
    .locals 2

    .prologue
    .line 534
    iput p1, p0, Labj;->Ot:I

    .line 535
    iget v0, p0, Labj;->Ot:I

    iget v1, p0, Labj;->Or:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Labj;->OD:F

    .line 536
    const/4 v0, 0x0

    iput-boolean v0, p0, Labj;->OF:Z

    .line 537
    return-void
.end method

.method public final setInterpolator(Landroid/animation/TimeInterpolator;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Labj;->OG:Landroid/animation/TimeInterpolator;

    .line 128
    return-void
.end method

.method public final startScroll(IIIII)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 372
    iput v2, p0, Labj;->Oq:I

    .line 373
    iput-boolean v2, p0, Labj;->OF:Z

    .line 374
    iput p5, p0, Labj;->OB:I

    .line 375
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Labj;->hV:J

    .line 376
    iput p1, p0, Labj;->Or:I

    .line 377
    iput v2, p0, Labj;->Os:I

    .line 378
    add-int v0, p1, p3

    iput v0, p0, Labj;->Ot:I

    .line 379
    iput v2, p0, Labj;->Ou:I

    .line 380
    int-to-float v0, p3

    iput v0, p0, Labj;->OD:F

    .line 381
    const/4 v0, 0x0

    iput v0, p0, Labj;->OE:F

    .line 382
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Labj;->OB:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Labj;->OC:F

    .line 383
    return-void
.end method
