.class public abstract Lgbo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public Kl:Z

.field private final blv:Lerp;

.field final cFf:Ljava/util/Map;

.field final cFg:Ljava/util/List;

.field private final cwO:Ljava/util/concurrent/Executor;

.field final dK:Ljava/lang/Object;

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lerp;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lgbo;->dK:Ljava/lang/Object;

    .line 44
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lgbo;->cFf:Ljava/util/Map;

    .line 47
    const/4 v0, 0x1

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lgbo;->cFg:Ljava/util/List;

    .line 54
    iput-object p1, p0, Lgbo;->mResources:Landroid/content/res/Resources;

    .line 55
    iput-object p2, p0, Lgbo;->blv:Lerp;

    .line 56
    iput-object p3, p0, Lgbo;->cwO:Ljava/util/concurrent/Executor;

    .line 57
    return-void
.end method

.method private static af(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 166
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 167
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbp;

    .line 169
    invoke-virtual {v0}, Lgbp;->isValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 173
    :cond_1
    return-void
.end method


# virtual methods
.method protected abstract a(Lgbn;)Landroid/graphics/Bitmap;
    .param p1    # Lgbn;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public final a(Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;Landroid/widget/ImageView;)V
    .locals 4

    .prologue
    .line 75
    invoke-static {p1}, Lgbn;->c(Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;)Lgbn;

    move-result-object v1

    iget-object v2, p0, Lgbo;->dK:Ljava/lang/Object;

    monitor-enter v2

    if-eqz v1, :cond_2

    :try_start_0
    iget-object v0, p0, Lgbo;->cFf:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v3, p0, Lgbo;->cFf:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    new-instance v3, Lgbp;

    invoke-direct {v3, p2}, Lgbp;-><init>(Landroid/widget/ImageView;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v0, p0, Lgbo;->Kl:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lgbo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lgbq;

    iget-object v2, p0, Lgbo;->blv:Lerp;

    invoke-interface {v2}, Lerp;->Ct()Ljava/util/concurrent/Executor;

    move-result-object v2

    iget-object v3, p0, Lgbo;->cwO:Ljava/util/concurrent/Executor;

    invoke-direct {v0, p0, v2, v3, v1}, Lgbq;-><init>(Lgbo;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lgbn;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lgbq;->a([Ljava/lang/Object;)Lenp;

    .line 76
    :cond_1
    return-void

    .line 75
    :cond_2
    :try_start_1
    iget-object v0, p0, Lgbo;->cFg:Ljava/util/List;

    new-instance v3, Lgbp;

    invoke-direct {v3, p2}, Lgbp;-><init>(Landroid/widget/ImageView;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method protected final b(Lgbn;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1    # Lgbn;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 221
    invoke-static {}, Lenu;->auQ()V

    .line 222
    invoke-virtual {p0, p1}, Lgbo;->a(Lgbn;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 223
    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lgbo;->mResources:Landroid/content/res/Resources;

    invoke-direct {v0, v2, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected isConnected()Z
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x1

    return v0
.end method

.method public final resume()V
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbo;->Kl:Z

    .line 121
    invoke-virtual {p0}, Lgbo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    invoke-virtual {p0}, Lgbo;->uK()V

    .line 124
    :cond_0
    return-void
.end method

.method public final uK()V
    .locals 6

    .prologue
    .line 137
    iget-object v1, p0, Lgbo;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 138
    :try_start_0
    iget-object v0, p0, Lgbo;->cFg:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    iget-object v0, p0, Lgbo;->cFg:Ljava/util/List;

    invoke-static {v0}, Lgbo;->af(Ljava/util/List;)V

    .line 142
    :cond_0
    iget-object v0, p0, Lgbo;->cFf:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 143
    iget-object v0, p0, Lgbo;->cFf:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 145
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 146
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 147
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 148
    invoke-static {v0}, Lgbo;->af(Ljava/util/List;)V

    .line 149
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 155
    :cond_2
    :try_start_1
    iget-object v0, p0, Lgbo;->cFg:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 156
    new-instance v0, Lgbq;

    iget-object v2, p0, Lgbo;->blv:Lerp;

    invoke-interface {v2}, Lerp;->Ct()Ljava/util/concurrent/Executor;

    move-result-object v2

    iget-object v3, p0, Lgbo;->cwO:Ljava/util/concurrent/Executor;

    const/4 v4, 0x0

    invoke-direct {v0, p0, v2, v3, v4}, Lgbq;-><init>(Lgbo;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lgbn;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lgbq;->a([Ljava/lang/Object;)Lenp;

    .line 159
    :cond_3
    iget-object v0, p0, Lgbo;->cFf:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbn;

    .line 160
    new-instance v3, Lgbq;

    iget-object v4, p0, Lgbo;->blv:Lerp;

    invoke-interface {v4}, Lerp;->Ct()Ljava/util/concurrent/Executor;

    move-result-object v4

    iget-object v5, p0, Lgbo;->cwO:Ljava/util/concurrent/Executor;

    invoke-direct {v3, p0, v4, v5, v0}, Lgbq;-><init>(Lgbo;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lgbn;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v3, v0}, Lgbq;->a([Ljava/lang/Object;)Lenp;

    goto :goto_1

    .line 162
    :cond_4
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
