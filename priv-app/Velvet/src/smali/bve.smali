.class public final Lbve;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;Landroid/os/Parcel;)V
    .locals 4

    const/4 v3, 0x0

    const/16 v0, 0x4f45

    invoke-static {p1, v0}, Lbju;->n(Landroid/os/Parcel;I)I

    move-result v0

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;->auB:I

    invoke-static {p1, v1, v2}, Lbju;->c(Landroid/os/Parcel;II)V

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;->packageName:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Lbju;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;->auC:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Lbju;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;->aLr:J

    invoke-static {p1, v1, v2, v3}, Lbju;->a(Landroid/os/Parcel;IJ)V

    invoke-static {p1, v0}, Lbju;->o(Landroid/os/Parcel;I)V

    return-void
.end method

.method public static ae(Landroid/os/Parcel;)Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;
    .locals 8

    const/4 v3, 0x0

    invoke-static {p0}, Lbjs;->x(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v2, v3

    :goto_0
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v6

    if-ge v6, v0, :cond_0

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v6

    const v7, 0xffff

    and-int/2addr v7, v6

    sparse-switch v7, :sswitch_data_0

    invoke-static {p0, v6}, Lbjs;->b(Landroid/os/Parcel;I)V

    goto :goto_0

    :sswitch_0
    invoke-static {p0, v6}, Lbjs;->d(Landroid/os/Parcel;I)I

    move-result v1

    goto :goto_0

    :sswitch_1
    invoke-static {p0, v6}, Lbjs;->h(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :sswitch_2
    invoke-static {p0, v6}, Lbjs;->h(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :sswitch_3
    invoke-static {p0, v6}, Lbjs;->e(Landroid/os/Parcel;I)J

    move-result-wide v4

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v6

    if-eq v6, v0, :cond_1

    new-instance v1, Lbjt;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Overread allowed size end="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p0}, Lbjt;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v1

    :cond_1
    new-instance v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;-><init>(ILjava/lang/String;Ljava/lang/String;J)V

    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x3e8 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lbve;->ae(Landroid/os/Parcel;)Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;

    return-object v0
.end method
