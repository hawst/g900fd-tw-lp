.class public final Ljew;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile egh:[Ljew;


# instance fields
.field private aez:I

.field private ajB:Ljava/lang/String;

.field private dRq:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 13648
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 13649
    const/4 v0, 0x0

    iput v0, p0, Ljew;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljew;->ajB:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljew;->dRq:J

    const/4 v0, 0x0

    iput-object v0, p0, Ljew;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljew;->eCz:I

    .line 13650
    return-void
.end method

.method public static bjg()[Ljew;
    .locals 2

    .prologue
    .line 13594
    sget-object v0, Ljew;->egh:[Ljew;

    if-nez v0, :cond_1

    .line 13595
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 13597
    :try_start_0
    sget-object v0, Ljew;->egh:[Ljew;

    if-nez v0, :cond_0

    .line 13598
    const/4 v0, 0x0

    new-array v0, v0, [Ljew;

    sput-object v0, Ljew;->egh:[Ljew;

    .line 13600
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13602
    :cond_1
    sget-object v0, Ljew;->egh:[Ljew;

    return-object v0

    .line 13600
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 13588
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljew;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljew;->ajB:Ljava/lang/String;

    iget v0, p0, Ljew;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljew;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljew;->dRq:J

    iget v0, p0, Ljew;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljew;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 13664
    iget v0, p0, Ljew;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 13665
    const/4 v0, 0x1

    iget-object v1, p0, Ljew;->ajB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 13667
    :cond_0
    iget v0, p0, Ljew;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 13668
    const/4 v0, 0x2

    iget-wide v2, p0, Ljew;->dRq:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 13670
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 13671
    return-void
.end method

.method public final getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13610
    iget-object v0, p0, Ljew;->ajB:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 13675
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 13676
    iget v1, p0, Ljew;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 13677
    const/4 v1, 0x1

    iget-object v2, p0, Ljew;->ajB:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13680
    :cond_0
    iget v1, p0, Ljew;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 13681
    const/4 v1, 0x2

    iget-wide v2, p0, Ljew;->dRq:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 13684
    :cond_1
    return v0
.end method

.method public final uk(Ljava/lang/String;)Ljew;
    .locals 1

    .prologue
    .line 13613
    if-nez p1, :cond_0

    .line 13614
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13616
    :cond_0
    iput-object p1, p0, Ljew;->ajB:Ljava/lang/String;

    .line 13617
    iget v0, p0, Ljew;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljew;->aez:I

    .line 13618
    return-object p0
.end method
