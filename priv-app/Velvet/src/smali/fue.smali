.class public final Lfue;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 25
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 8

    .prologue
    const v7, 0x7f11007f

    const/4 v6, 0x0

    .line 34
    iget-object v1, p0, Lfro;->mView:Landroid/view/View;

    .line 35
    iget-object v0, p0, Lfro;->cBc:Lanh;

    .line 36
    iget-object v2, v0, Lanh;->agH:Laoc;

    .line 38
    const v0, 0x7f1101a9

    invoke-virtual {v2}, Laoc;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v3}, Lfue;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 39
    const v0, 0x7f1101aa

    invoke-virtual {v2}, Laoc;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v3}, Lfue;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 40
    const v0, 0x7f110080

    invoke-virtual {v2}, Laoc;->ol()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v3}, Lfue;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 43
    const/4 v0, 0x0

    .line 44
    invoke-virtual {v2}, Laoc;->pA()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 45
    iget-object v0, p0, Lfue;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Laoc;->qn()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00b9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-static {v0, v3, v4}, Lgab;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 48
    :cond_0
    const-string v3, "  "

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/CharSequence;

    invoke-virtual {v2}, Laoc;->qm()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 50
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 51
    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 55
    :cond_1
    iget-object v0, v2, Laoc;->ahN:Laok;

    if-eqz v0, :cond_2

    .line 56
    iget-object v0, p0, Lfue;->mContext:Landroid/content/Context;

    iget-object v3, v2, Laoc;->ahN:Laok;

    invoke-static {v0, v3}, Lfxt;->a(Landroid/content/Context;Laok;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 58
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 59
    const v3, 0x7f1102f0

    invoke-static {v1, v3, v0}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 63
    :cond_2
    invoke-virtual {v2}, Laoc;->ok()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 64
    const v0, 0x7f1100ad

    invoke-virtual {v2}, Laoc;->oj()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v0, v2}, Lfue;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 66
    :cond_3
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 29
    const v0, 0x7f040116

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
