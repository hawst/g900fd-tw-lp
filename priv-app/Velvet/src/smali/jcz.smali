.class public final Ljcz;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ebe:Z

.field private ebf:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 20557
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 20558
    const/4 v0, 0x0

    iput v0, p0, Ljcz;->aez:I

    iput-boolean v1, p0, Ljcz;->ebe:Z

    iput-boolean v1, p0, Ljcz;->ebf:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljcz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljcz;->eCz:I

    .line 20559
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 20500
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljcz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljcz;->ebe:Z

    iget v0, p0, Ljcz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcz;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljcz;->ebf:Z

    iget v0, p0, Ljcz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljcz;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x18 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 20573
    iget v0, p0, Ljcz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 20574
    const/4 v0, 0x1

    iget-boolean v1, p0, Ljcz;->ebe:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 20576
    :cond_0
    iget v0, p0, Ljcz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 20577
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljcz;->ebf:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 20579
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 20580
    return-void
.end method

.method public final hF(Z)Ljcz;
    .locals 1

    .prologue
    .line 20544
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljcz;->ebf:Z

    .line 20545
    iget v0, p0, Ljcz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljcz;->aez:I

    .line 20546
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 20584
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 20585
    iget v1, p0, Ljcz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 20586
    const/4 v1, 0x1

    iget-boolean v2, p0, Ljcz;->ebe:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 20589
    :cond_0
    iget v1, p0, Ljcz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 20590
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljcz;->ebf:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 20593
    :cond_1
    return v0
.end method
