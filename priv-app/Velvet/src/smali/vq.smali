.class public final Lvq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private synthetic Id:Lcom/android/launcher3/FolderIcon;

.field private synthetic If:Z

.field private synthetic Ig:F

.field private synthetic Ih:Lvx;

.field private synthetic Ii:F


# direct methods
.method public constructor <init>(Lcom/android/launcher3/FolderIcon;ZFLvx;F)V
    .locals 0

    .prologue
    .line 652
    iput-object p1, p0, Lvq;->Id:Lcom/android/launcher3/FolderIcon;

    iput-boolean p2, p0, Lvq;->If:Z

    iput p3, p0, Lvq;->Ig:F

    iput-object p4, p0, Lvq;->Ih:Lvx;

    iput p5, p0, Lvq;->Ii:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 654
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 655
    iget-boolean v1, p0, Lvq;->If:Z

    if-eqz v1, :cond_0

    .line 656
    sub-float v0, v5, v0

    .line 657
    iget-object v1, p0, Lvq;->Id:Lcom/android/launcher3/FolderIcon;

    invoke-static {v1}, Lcom/android/launcher3/FolderIcon;->b(Lcom/android/launcher3/FolderIcon;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 660
    :cond_0
    iget-object v1, p0, Lvq;->Id:Lcom/android/launcher3/FolderIcon;

    invoke-static {v1}, Lcom/android/launcher3/FolderIcon;->g(Lcom/android/launcher3/FolderIcon;)Lvx;

    move-result-object v1

    iget v2, p0, Lvq;->Ig:F

    iget-object v3, p0, Lvq;->Ih:Lvx;

    iget v3, v3, Lvx;->Iv:F

    iget v4, p0, Lvq;->Ig:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    iput v2, v1, Lvx;->Iv:F

    .line 661
    iget-object v1, p0, Lvq;->Id:Lcom/android/launcher3/FolderIcon;

    invoke-static {v1}, Lcom/android/launcher3/FolderIcon;->g(Lcom/android/launcher3/FolderIcon;)Lvx;

    move-result-object v1

    iget v2, p0, Lvq;->Ii:F

    iget-object v3, p0, Lvq;->Ih:Lvx;

    iget v3, v3, Lvx;->Iw:F

    iget v4, p0, Lvq;->Ii:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    iput v2, v1, Lvx;->Iw:F

    .line 662
    iget-object v1, p0, Lvq;->Id:Lcom/android/launcher3/FolderIcon;

    invoke-static {v1}, Lcom/android/launcher3/FolderIcon;->g(Lcom/android/launcher3/FolderIcon;)Lvx;

    move-result-object v1

    iget-object v2, p0, Lvq;->Ih:Lvx;

    iget v2, v2, Lvx;->Ix:F

    sub-float/2addr v2, v5

    mul-float/2addr v0, v2

    add-float/2addr v0, v5

    iput v0, v1, Lvx;->Ix:F

    .line 663
    iget-object v0, p0, Lvq;->Id:Lcom/android/launcher3/FolderIcon;

    invoke-virtual {v0}, Lcom/android/launcher3/FolderIcon;->invalidate()V

    .line 664
    return-void
.end method
