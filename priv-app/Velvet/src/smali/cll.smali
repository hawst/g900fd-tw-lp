.class public final Lcll;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a([Lidz;)Lidz;
    .locals 4

    .prologue
    .line 54
    array-length v2, p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p0, v1

    .line 55
    iget-object v3, v0, Lidz;->dzv:Liee;

    if-eqz v3, :cond_0

    .line 57
    iget-object v1, v0, Lidz;->dzv:Liee;

    invoke-static {v1}, Lcll;->b(Liee;)V

    .line 61
    :goto_1
    return-object v0

    .line 54
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 61
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Liee;)Lief;
    .locals 6

    .prologue
    .line 98
    iget-object v2, p0, Liee;->dzQ:[Lief;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 99
    invoke-virtual {v0}, Lief;->aVw()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 103
    :goto_1
    return-object v0

    .line 98
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 103
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(Lief;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 208
    iget-object v3, p0, Lief;->dAb:[Lieg;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 209
    iget-object v6, v5, Lieg;->dzt:[Ljava/lang/String;

    array-length v7, v6

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_1

    aget-object v8, v6, v0

    .line 210
    if-eqz p1, :cond_0

    invoke-virtual {p1, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 211
    invoke-virtual {v5}, Lieg;->getCurrencyCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    .line 212
    const-string v2, "%s%.2f"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/Currency;->getSymbol()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    const/4 v0, 0x1

    invoke-virtual {v5}, Lieg;->aVC()J

    move-result-wide v4

    long-to-double v4, v4

    const-wide v6, 0x412e848000000000L    # 1000000.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 217
    :goto_2
    return-object v0

    .line 209
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 208
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 217
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private static a(Ljava/lang/String;CC)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 250
    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    if-ltz v4, :cond_3

    .line 257
    add-int/lit8 v0, v4, 0x1

    move v1, v2

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v0, v5, :cond_5

    .line 258
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, p1, :cond_1

    .line 259
    add-int/lit8 v1, v1, 0x1

    .line 261
    :cond_1
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, p2, :cond_2

    .line 262
    add-int/lit8 v1, v1, -0x1

    .line 265
    :cond_2
    if-nez v1, :cond_4

    .line 269
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    move v0, v2

    .line 280
    :goto_1
    if-nez v0, :cond_0

    .line 281
    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 285
    :cond_3
    return-object p0

    .line 257
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v3

    goto :goto_1
.end method

.method public static a(Lidz;ZLjava/lang/String;)Ljmh;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 121
    iget-object v1, p0, Lidz;->dzv:Liee;

    .line 122
    new-instance v0, Ljml;

    invoke-direct {v0}, Ljml;-><init>()V

    invoke-virtual {v1}, Liee;->aVp()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljml;->wZ(Ljava/lang/String;)Ljml;

    move-result-object v0

    invoke-virtual {v1}, Liee;->aVj()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljml;->wY(Ljava/lang/String;)Ljml;

    move-result-object v0

    invoke-virtual {v1}, Liee;->aVn()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljml;->xa(Ljava/lang/String;)Ljml;

    move-result-object v0

    invoke-virtual {v1}, Liee;->aVq()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljml;->iD(Z)Ljml;

    move-result-object v2

    .line 127
    new-instance v0, Ljmh;

    invoke-direct {v0}, Ljmh;-><init>()V

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Ljmh;->qM(I)Ljmh;

    move-result-object v0

    invoke-virtual {v1}, Liee;->aVn()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljmh;->wQ(Ljava/lang/String;)Ljmh;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljmh;->iC(Z)Ljmh;

    move-result-object v3

    invoke-static {v1}, Lcll;->a(Liee;)Lief;

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "https://play.google.com/store/music/album?id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lief;->aVy()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&tid=song-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lief;->aVx()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "&utm_source=sound_search_gsa"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljmh;->wS(Ljava/lang/String;)Ljmh;

    move-result-object v3

    .line 132
    iput-object v2, v3, Ljmh;->etA:Ljml;

    .line 133
    invoke-virtual {v1}, Liee;->aVs()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v1}, Liee;->aVr()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {v1}, Liee;->aVr()Ljava/lang/String;

    move-result-object v0

    .line 134
    :goto_1
    if-eqz v0, :cond_0

    .line 135
    invoke-virtual {v3, v0}, Ljmh;->wT(Ljava/lang/String;)Ljmh;

    .line 138
    :cond_0
    invoke-static {v1}, Lcll;->a(Liee;)Lief;

    move-result-object v0

    .line 139
    if-eqz v0, :cond_2

    .line 140
    invoke-static {v0, p2}, Lcll;->a(Lief;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 141
    if-eqz v1, :cond_1

    .line 142
    new-array v2, v6, [Ljmm;

    const/4 v4, 0x0

    new-instance v5, Ljmm;

    invoke-direct {v5}, Ljmm;-><init>()V

    invoke-virtual {v5, v1}, Ljmm;->xf(Ljava/lang/String;)Ljmm;

    move-result-object v1

    aput-object v1, v2, v4

    iput-object v2, v3, Ljmh;->etJ:[Ljmm;

    .line 144
    :cond_1
    invoke-virtual {v0}, Lief;->aVA()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 145
    invoke-virtual {v0}, Lief;->aVz()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljmh;->wU(Ljava/lang/String;)Ljmh;

    .line 148
    :cond_2
    return-object v3

    .line 127
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Liee;->aVm()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v1}, Liee;->aVl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "https://play.google.com/store/music/artist?id="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Liee;->aVl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&utm_source=sound_search_gsa"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_4
    const-string v4, "https://play.google.com/store/search?q="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Liee;->aVj()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&c=music&utm_source=sound_search_gsa"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 133
    :cond_5
    if-eqz p1, :cond_6

    invoke-virtual {v1}, Liee;->aVu()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v1}, Liee;->aVt()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {v1}, Liee;->aVt()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method public static b([Lidz;)Lidz;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 66
    array-length v2, p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p0, v1

    .line 67
    iget-object v3, v0, Lidz;->dzw:Lieh;

    if-eqz v3, :cond_0

    .line 71
    :goto_1
    return-object v0

    .line 66
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 71
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(Liee;)V
    .locals 1

    .prologue
    .line 239
    invoke-virtual {p0}, Liee;->aVo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    invoke-virtual {p0}, Liee;->aVn()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcll;->hm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Liee;->pj(Ljava/lang/String;)Liee;

    .line 242
    :cond_0
    invoke-virtual {p0}, Liee;->aVk()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    invoke-virtual {p0}, Liee;->aVj()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcll;->hm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Liee;->pi(Ljava/lang/String;)Liee;

    .line 245
    :cond_1
    return-void
.end method

.method public static c([Lidz;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 76
    invoke-static {p0}, Lcll;->a([Lidz;)Lidz;

    move-result-object v0

    .line 77
    if-eqz v0, :cond_0

    .line 78
    iget-object v0, v0, Lidz;->dzv:Liee;

    .line 79
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Liee;->aVj()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Liee;->aVn()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static hm(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 289
    const/16 v0, 0x28

    const/16 v1, 0x29

    invoke-static {p0, v0, v1}, Lcll;->a(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    .line 290
    const/16 v1, 0x5b

    const/16 v2, 0x5d

    invoke-static {v0, v1, v2}, Lcll;->a(Ljava/lang/String;CC)Ljava/lang/String;

    move-result-object v0

    .line 291
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
