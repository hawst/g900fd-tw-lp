.class public final Ljky;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eqQ:[Ljky;


# instance fields
.field private aez:I

.field private ajm:Ljava/lang/String;

.field private dOO:Ljava/lang/String;

.field private dRI:Ljava/lang/String;

.field private eqR:Ljava/lang/String;

.field private eqS:[Ljkz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13666
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 13667
    const/4 v0, 0x0

    iput v0, p0, Ljky;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljky;->dOO:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljky;->eqR:Ljava/lang/String;

    invoke-static {}, Ljkz;->boI()[Ljkz;

    move-result-object v0

    iput-object v0, p0, Ljky;->eqS:[Ljkz;

    const-string v0, ""

    iput-object v0, p0, Ljky;->ajm:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljky;->dRI:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljky;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljky;->eCz:I

    .line 13668
    return-void
.end method

.method public static boH()[Ljky;
    .locals 2

    .prologue
    .line 13562
    sget-object v0, Ljky;->eqQ:[Ljky;

    if-nez v0, :cond_1

    .line 13563
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 13565
    :try_start_0
    sget-object v0, Ljky;->eqQ:[Ljky;

    if-nez v0, :cond_0

    .line 13566
    const/4 v0, 0x0

    new-array v0, v0, [Ljky;

    sput-object v0, Ljky;->eqQ:[Ljky;

    .line 13568
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13570
    :cond_1
    sget-object v0, Ljky;->eqQ:[Ljky;

    return-object v0

    .line 13568
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 13264
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljky;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljky;->dOO:Ljava/lang/String;

    iget v0, p0, Ljky;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljky;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljky;->eqR:Ljava/lang/String;

    iget v0, p0, Ljky;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljky;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljky;->eqS:[Ljkz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljkz;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljky;->eqS:[Ljkz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljkz;

    invoke-direct {v3}, Ljkz;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljky;->eqS:[Ljkz;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljkz;

    invoke-direct {v3}, Ljkz;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljky;->eqS:[Ljkz;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljky;->ajm:Ljava/lang/String;

    iget v0, p0, Ljky;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljky;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljky;->dRI:Ljava/lang/String;

    iget v0, p0, Ljky;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljky;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 13685
    iget v0, p0, Ljky;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 13686
    const/4 v0, 0x1

    iget-object v1, p0, Ljky;->dOO:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 13688
    :cond_0
    iget v0, p0, Ljky;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 13689
    const/4 v0, 0x2

    iget-object v1, p0, Ljky;->eqR:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 13691
    :cond_1
    iget-object v0, p0, Ljky;->eqS:[Ljkz;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljky;->eqS:[Ljkz;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 13692
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljky;->eqS:[Ljkz;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 13693
    iget-object v1, p0, Ljky;->eqS:[Ljkz;

    aget-object v1, v1, v0

    .line 13694
    if-eqz v1, :cond_2

    .line 13695
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 13692
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 13699
    :cond_3
    iget v0, p0, Ljky;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 13700
    const/4 v0, 0x4

    iget-object v1, p0, Ljky;->ajm:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 13702
    :cond_4
    iget v0, p0, Ljky;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 13703
    const/4 v0, 0x5

    iget-object v1, p0, Ljky;->dRI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 13705
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 13706
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 13710
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 13711
    iget v1, p0, Ljky;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 13712
    const/4 v1, 0x1

    iget-object v2, p0, Ljky;->dOO:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13715
    :cond_0
    iget v1, p0, Ljky;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 13716
    const/4 v1, 0x2

    iget-object v2, p0, Ljky;->eqR:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13719
    :cond_1
    iget-object v1, p0, Ljky;->eqS:[Ljkz;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljky;->eqS:[Ljkz;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 13720
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljky;->eqS:[Ljkz;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 13721
    iget-object v2, p0, Ljky;->eqS:[Ljkz;

    aget-object v2, v2, v0

    .line 13722
    if-eqz v2, :cond_2

    .line 13723
    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 13720
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 13728
    :cond_4
    iget v1, p0, Ljky;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    .line 13729
    const/4 v1, 0x4

    iget-object v2, p0, Ljky;->ajm:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13732
    :cond_5
    iget v1, p0, Ljky;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_6

    .line 13733
    const/4 v1, 0x5

    iget-object v2, p0, Ljky;->dRI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13736
    :cond_6
    return v0
.end method
