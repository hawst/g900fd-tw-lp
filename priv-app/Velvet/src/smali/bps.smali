.class public abstract Lbps;
.super Lbir;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbhk;Lbhl;)V
    .locals 6

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lbir;-><init>(Landroid/content/Context;Landroid/os/Looper;Lbhk;Lbhl;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final Aa()Landroid/os/IInterface;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lbps;->yT()Landroid/os/IInterface;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1}, Landroid/os/RemoteException;-><init>()V

    invoke-virtual {v1, v0}, Landroid/os/RemoteException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v1
.end method

.method protected final yQ()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.icing.INDEX_SERVICE"

    return-object v0
.end method
