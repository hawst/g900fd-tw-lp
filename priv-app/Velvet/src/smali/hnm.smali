.class public final Lhnm;
.super Landroid/graphics/drawable/Drawable;
.source "PG"


# instance fields
.field private final dlQ:Landroid/graphics/RectF;

.field private final ob:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 40
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lhnm;->ob:Landroid/graphics/Paint;

    .line 41
    iget-object v0, p0, Lhnm;->ob:Landroid/graphics/Paint;

    const v1, 0x7f0b00a6

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 42
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lhnm;->dlQ:Landroid/graphics/RectF;

    .line 43
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/high16 v6, 0x45fa0000    # 8000.0f

    .line 47
    invoke-virtual {p0}, Lhnm;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 48
    invoke-virtual {p0}, Lhnm;->getLevel()I

    move-result v1

    int-to-float v1, v1

    .line 49
    iget-object v2, p0, Lhnm;->dlQ:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 50
    iget-object v2, p0, Lhnm;->dlQ:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    .line 51
    iget-object v2, p0, Lhnm;->dlQ:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v1

    const v5, 0x461c4000    # 10000.0f

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 52
    cmpl-float v2, v1, v6

    if-lez v2, :cond_0

    .line 53
    iget-object v2, p0, Lhnm;->dlQ:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v1, v6

    mul-float/2addr v1, v4

    const/high16 v4, 0x44fa0000    # 2000.0f

    div-float/2addr v1, v4

    add-float/2addr v1, v3

    iput v1, v2, Landroid/graphics/RectF;->left:F

    .line 59
    :goto_0
    iget-object v1, p0, Lhnm;->dlQ:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    iget-object v3, p0, Lhnm;->ob:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 60
    return-void

    .line 56
    :cond_0
    iget-object v1, p0, Lhnm;->dlQ:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iput v2, v1, Landroid/graphics/RectF;->left:F

    goto :goto_0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, -0x2

    return v0
.end method

.method protected final onLevelChange(I)Z
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lhnm;->invalidateSelf()V

    .line 65
    const/4 v0, 0x1

    return v0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lhnm;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 71
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lhnm;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 76
    return-void
.end method
