.class public final Lhav;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

.field public dbl:I

.field public dbm:Lhbq;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/ui/InAppWebPageActivity;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lhav;->dbl:I

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lhav;->dbm:Lhbq;

    .line 41
    iput-object p1, p0, Lhav;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Lhbq;)V
    .locals 1

    .prologue
    .line 50
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    iget-object v0, p0, Lhav;->dbm:Lhbq;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 52
    iput-object p1, p0, Lhav;->dbm:Lhbq;

    .line 53
    return-void

    .line 51
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aMW()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 66
    iget v0, p0, Lhav;->dbl:I

    packed-switch v0, :pswitch_data_0

    .line 80
    :goto_0
    invoke-virtual {p0}, Lhav;->aNb()V

    .line 81
    const/4 v0, 0x0

    iput v0, p0, Lhav;->dbl:I

    .line 82
    return-void

    .line 68
    :pswitch_0
    iget-object v0, p0, Lhav;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->aNO()V

    goto :goto_0

    .line 71
    :pswitch_1
    iget-object v0, p0, Lhav;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->aNP()V

    goto :goto_0

    .line 74
    :pswitch_2
    iget-object v0, p0, Lhav;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, p0, Lhav;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    iget-object v1, p0, Lhav;->dbm:Lhbq;

    iget-object v1, v1, Lhbq;->dbQ:Lhfp;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->b(Lhfp;)V

    .line 76
    iput-object v2, p0, Lhav;->dbm:Lhbq;

    goto :goto_0

    .line 66
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final aMX()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 89
    iget-object v0, p0, Lhav;->dbm:Lhbq;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 90
    iget v0, p0, Lhav;->dbl:I

    if-nez v0, :cond_2

    .line 91
    iget-object v0, p0, Lhav;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    iget-object v2, p0, Lhav;->dbm:Lhbq;

    iget-object v2, v2, Lhbq;->bLI:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v0, p0, Lhav;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    iget-object v2, p0, Lhav;->dbm:Lhbq;

    iget-object v2, v2, Lhbq;->dbQ:Lhfp;

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->a(Lhfp;)V

    .line 93
    iput v1, p0, Lhav;->dbl:I

    .line 100
    :cond_0
    :goto_1
    return-void

    .line 89
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 94
    :cond_2
    iget v0, p0, Lhav;->dbl:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 95
    iget-object v0, p0, Lhav;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->aNO()V

    .line 96
    iget-object v0, p0, Lhav;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    iget-object v2, p0, Lhav;->dbm:Lhbq;

    iget-object v2, v2, Lhbq;->bLI:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v0, p0, Lhav;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    iget-object v2, p0, Lhav;->dbm:Lhbq;

    iget-object v2, v2, Lhbq;->dbQ:Lhfp;

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->a(Lhfp;)V

    .line 98
    iput v1, p0, Lhav;->dbl:I

    goto :goto_1
.end method

.method public final aMY()V
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lhav;->dbl:I

    if-nez v0, :cond_0

    .line 128
    iget-object v0, p0, Lhav;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->aMY()V

    .line 129
    const/4 v0, 0x2

    iput v0, p0, Lhav;->dbl:I

    .line 131
    :cond_0
    return-void
.end method

.method public final aMZ()V
    .locals 2

    .prologue
    .line 146
    iget v0, p0, Lhav;->dbl:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 147
    iget-object v0, p0, Lhav;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    iget-object v1, p0, Lhav;->dbm:Lhbq;

    iget-object v1, v1, Lhbq;->bLI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 149
    :cond_0
    return-void
.end method

.method public final aNa()Z
    .locals 2

    .prologue
    .line 169
    iget v0, p0, Lhav;->dbl:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 170
    iget-object v0, p0, Lhav;->dbm:Lhbq;

    invoke-virtual {v0}, Lhbq;->Xb()Z

    move-result v0

    .line 172
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aNb()V
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lhav;->dbm:Lhbq;

    if-eqz v0, :cond_0

    iget v0, p0, Lhav;->dbl:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 210
    iget-object v0, p0, Lhav;->dbm:Lhbq;

    invoke-virtual {v0}, Lhbq;->destroy()V

    .line 212
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lhav;->dbm:Lhbq;

    .line 213
    return-void
.end method

.method public final ex(I)V
    .locals 2

    .prologue
    .line 108
    iget v0, p0, Lhav;->dbl:I

    if-nez v0, :cond_1

    .line 109
    iget-object v0, p0, Lhav;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ex(I)V

    .line 119
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lhav;->aNb()V

    .line 120
    const/4 v0, 0x3

    iput v0, p0, Lhav;->dbl:I

    .line 121
    return-void

    .line 110
    :cond_1
    iget v0, p0, Lhav;->dbl:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 111
    iget-object v0, p0, Lhav;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->aNO()V

    .line 112
    iget-object v0, p0, Lhav;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ex(I)V

    goto :goto_0

    .line 113
    :cond_2
    iget v0, p0, Lhav;->dbl:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 114
    iget-object v0, p0, Lhav;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    iget-object v1, p0, Lhav;->dbm:Lhbq;

    iget-object v1, v1, Lhbq;->dbQ:Lhfp;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->b(Lhfp;)V

    .line 115
    iget-object v0, p0, Lhav;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ex(I)V

    .line 116
    iget-object v0, p0, Lhav;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final g(Landroid/view/Menu;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 157
    invoke-interface {p1, v1}, Landroid/view/Menu;->removeGroup(I)V

    .line 158
    iget v0, p0, Lhav;->dbl:I

    if-ne v0, v1, :cond_0

    .line 159
    iget-object v0, p0, Lhav;->dbm:Lhbq;

    invoke-virtual {v0, p1, v1}, Lhbq;->a(Landroid/view/Menu;I)V

    .line 161
    :cond_0
    return-void
.end method
