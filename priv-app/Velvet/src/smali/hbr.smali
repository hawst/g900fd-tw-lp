.class final Lhbr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final aUU:I

.field public final dbY:Ljava/lang/String;

.field public final dbZ:Landroid/net/Uri;

.field public final dca:Z

.field public final dcb:Landroid/content/Intent;

.field final synthetic dcc:Lhbq;


# direct methods
.method public constructor <init>(Lhbq;Ljava/lang/String;ILandroid/net/Uri;Z)V
    .locals 1

    .prologue
    .line 252
    iput-object p1, p0, Lhbr;->dcc:Lhbq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253
    iput-object p2, p0, Lhbr;->dbY:Ljava/lang/String;

    .line 254
    iput p3, p0, Lhbr;->aUU:I

    .line 255
    iput-object p4, p0, Lhbr;->dbZ:Landroid/net/Uri;

    .line 256
    iput-boolean p5, p0, Lhbr;->dca:Z

    .line 257
    iget-object v0, p1, Lhbq;->mUrlHelper:Lcpn;

    invoke-virtual {v0, p4}, Lcpn;->p(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lhbr;->dcb:Landroid/content/Intent;

    .line 258
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 282
    instance-of v1, p1, Lhbr;

    if-eqz v1, :cond_0

    .line 283
    check-cast p1, Lhbr;

    .line 284
    iget-object v1, p0, Lhbr;->dcc:Lhbq;

    iget-object v2, p1, Lhbr;->dcc:Lhbq;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lhbr;->dbY:Ljava/lang/String;

    iget-object v2, p1, Lhbr;->dbY:Ljava/lang/String;

    invoke-static {v1, v2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lhbr;->aUU:I

    iget v2, p1, Lhbr;->aUU:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lhbr;->dbZ:Landroid/net/Uri;

    iget-object v2, p1, Lhbr;->dbZ:Landroid/net/Uri;

    invoke-static {v1, v2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lhbr;->dca:Z

    iget-boolean v2, p1, Lhbr;->dca:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 290
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 295
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lhbr;->dcc:Lhbq;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lhbr;->dbY:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lhbr;->aUU:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lhbr;->dbZ:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lhbr;->dca:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
