.class public final Lfst;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 24
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 35
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    .line 36
    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->agG:Lans;

    .line 37
    iget-object v2, v1, Lans;->aiw:Lanj;

    .line 38
    invoke-virtual {v2}, Lanj;->pb()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 39
    const v3, 0x7f110082

    invoke-virtual {v2}, Lanj;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 42
    :cond_0
    invoke-virtual {v2}, Lanj;->pd()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 43
    const v3, 0x7f1102ee

    invoke-virtual {v2}, Lanj;->pc()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 46
    :cond_1
    invoke-virtual {v2}, Lanj;->pe()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 47
    const v3, 0x7f1102f0

    invoke-virtual {v2}, Lanj;->on()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 50
    :cond_2
    invoke-virtual {v1}, Lans;->pM()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 51
    const v3, 0x7f1102f2

    invoke-virtual {v1}, Lans;->pL()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 54
    :cond_3
    invoke-virtual {v2}, Lanj;->pf()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 55
    const v3, 0x7f1100b0

    invoke-virtual {v2}, Lanj;->oo()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 58
    :cond_4
    iget-object v2, v2, Lanj;->ahM:Laoi;

    invoke-virtual {v1}, Lans;->pN()Z

    move-result v1

    invoke-virtual {p0, v0, v2, v1}, Lfst;->a(Landroid/view/View;Laoi;Z)V

    .line 62
    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->ahs:Lani;

    if-nez v1, :cond_5

    .line 63
    const v1, 0x7f1102f1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 64
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 65
    invoke-static {v0, v5, v5, v5, v5}, Leot;->a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)V

    .line 66
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 68
    :cond_5
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 28
    const v0, 0x7f04010b

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
