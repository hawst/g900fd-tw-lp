.class public final Laep;
.super Laeu;
.source "PG"


# instance fields
.field private Uw:I

.field private mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;ILandroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 231
    invoke-direct {p0}, Laeu;-><init>()V

    .line 232
    iput-object p1, p0, Laep;->mResources:Landroid/content/res/Resources;

    .line 233
    iput p2, p0, Laep;->Uw:I

    .line 234
    iput-object p3, p0, Laep;->UB:Landroid/graphics/drawable/Drawable;

    .line 235
    return-void
.end method


# virtual methods
.method public final a(Ladz;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 238
    new-instance v0, Laih;

    iget-object v1, p0, Laep;->mResources:Landroid/content/res/Resources;

    iget v2, p0, Laep;->Uw:I

    const/16 v3, 0x400

    invoke-direct {v0, v1, v2, v3}, Laih;-><init>(Landroid/content/res/Resources;II)V

    .line 241
    invoke-virtual {v0}, Laih;->lO()Z

    .line 242
    new-instance v1, Laie;

    invoke-direct {v1, p1, v0}, Laie;-><init>(Landroid/content/Context;Laif;)V

    .line 243
    invoke-virtual {p1}, Ladz;->kz()Lcom/android/launcher3/CropView;

    move-result-object v0

    .line 244
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher3/CropView;->a(Laiq;Ljava/lang/Runnable;)V

    .line 245
    invoke-virtual {p1}, Ladz;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p1}, Ladz;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/launcher3/WallpaperCropActivity;->a(Landroid/content/res/Resources;Landroid/view/WindowManager;)Landroid/graphics/Point;

    move-result-object v2

    .line 247
    invoke-virtual {v1}, Laie;->gi()I

    move-result v3

    invoke-virtual {v1}, Laie;->gj()I

    move-result v1

    iget v4, v2, Landroid/graphics/Point;->x:I

    iget v5, v2, Landroid/graphics/Point;->y:I

    invoke-static {v3, v1, v4, v5, v6}, Lcom/android/launcher3/WallpaperCropActivity;->a(IIIIZ)Landroid/graphics/RectF;

    move-result-object v1

    .line 250
    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float v1, v2, v1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CropView;->k(F)V

    .line 251
    invoke-virtual {v0, v6}, Lcom/android/launcher3/CropView;->K(Z)V

    .line 252
    invoke-virtual {p1, v6}, Ladz;->aq(Z)V

    .line 253
    return-void
.end method

.method public final isSelectable()Z
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x1

    return v0
.end method

.method public final kC()Z
    .locals 1

    .prologue
    .line 265
    const/4 v0, 0x1

    return v0
.end method

.method public final n(Ladz;)V
    .locals 3

    .prologue
    .line 256
    iget-object v0, p0, Laep;->mResources:Landroid/content/res/Resources;

    iget v1, p0, Laep;->Uw:I

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Ladz;->a(Landroid/content/res/Resources;IZ)V

    .line 258
    return-void
.end method
