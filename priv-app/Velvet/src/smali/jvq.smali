.class public final Ljvq;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eHN:F

.field private eHO:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1442
    const/16 v0, 0xb

    const-class v1, Ljvq;

    const v2, 0xe74b04a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    .line 1448
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1493
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1494
    const/4 v0, 0x0

    iput v0, p0, Ljvq;->aez:I

    const/4 v0, 0x0

    iput v0, p0, Ljvq;->eHN:F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljvq;->eHO:J

    const/4 v0, 0x0

    iput-object v0, p0, Ljvq;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljvq;->eCz:I

    .line 1495
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 1435
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljvq;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljvq;->eHN:F

    iget v0, p0, Ljvq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljvq;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljvq;->eHO:J

    iget v0, p0, Ljvq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljvq;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 1509
    iget v0, p0, Ljvq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1510
    const/4 v0, 0x1

    iget v1, p0, Ljvq;->eHN:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 1512
    :cond_0
    iget v0, p0, Ljvq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1513
    const/4 v0, 0x2

    iget-wide v2, p0, Ljvq;->eHO:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 1515
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1516
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 1520
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1521
    iget v1, p0, Ljvq;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1522
    const/4 v1, 0x1

    iget v2, p0, Ljvq;->eHN:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1525
    :cond_0
    iget v1, p0, Ljvq;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1526
    const/4 v1, 0x2

    iget-wide v2, p0, Ljvq;->eHO:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1529
    :cond_1
    return v0
.end method
