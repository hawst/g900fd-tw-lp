.class final Ljc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic ic:Lja;


# direct methods
.method private constructor <init>(Lja;)V
    .locals 0

    .prologue
    .line 684
    iput-object p1, p0, Ljc;->ic:Lja;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lja;B)V
    .locals 0

    .prologue
    .line 684
    invoke-direct {p0, p1}, Ljc;-><init>(Lja;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x0

    .line 687
    iget-object v0, p0, Ljc;->ic:Lja;

    invoke-static {v0}, Lja;->a(Lja;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 715
    :goto_0
    return-void

    .line 691
    :cond_0
    iget-object v0, p0, Ljc;->ic:Lja;

    invoke-static {v0}, Lja;->b(Lja;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 692
    iget-object v0, p0, Ljc;->ic:Lja;

    invoke-static {v0, v1}, Lja;->a(Lja;Z)Z

    .line 693
    iget-object v0, p0, Ljc;->ic:Lja;

    invoke-static {v0}, Lja;->c(Lja;)Ljb;

    move-result-object v0

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Ljb;->hV:J

    const-wide/16 v2, -0x1

    iput-wide v2, v0, Ljb;->hZ:J

    iget-wide v2, v0, Ljb;->hV:J

    iput-wide v2, v0, Ljb;->hW:J

    const/high16 v2, 0x3f000000    # 0.5f

    iput v2, v0, Ljb;->ia:F

    iput v1, v0, Ljb;->hX:I

    iput v1, v0, Ljb;->hY:I

    .line 696
    :cond_1
    iget-object v0, p0, Ljc;->ic:Lja;

    invoke-static {v0}, Lja;->c(Lja;)Ljb;

    move-result-object v2

    .line 697
    iget-wide v4, v2, Ljb;->hZ:J

    cmp-long v0, v4, v10

    if-lez v0, :cond_3

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    iget-wide v6, v2, Ljb;->hZ:J

    iget v0, v2, Ljb;->ib:I

    int-to-long v8, v0

    add-long/2addr v6, v8

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    iget-object v0, p0, Ljc;->ic:Lja;

    invoke-static {v0}, Lja;->d(Lja;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 698
    :cond_2
    iget-object v0, p0, Ljc;->ic:Lja;

    invoke-static {v0, v1}, Lja;->b(Lja;Z)Z

    goto :goto_0

    :cond_3
    move v0, v1

    .line 697
    goto :goto_1

    .line 702
    :cond_4
    iget-object v0, p0, Ljc;->ic:Lja;

    invoke-static {v0}, Lja;->e(Lja;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 703
    iget-object v0, p0, Ljc;->ic:Lja;

    invoke-static {v0, v1}, Lja;->c(Lja;Z)Z

    .line 704
    iget-object v0, p0, Ljc;->ic:Lja;

    invoke-static {v0}, Lja;->f(Lja;)V

    .line 707
    :cond_5
    iget-wide v0, v2, Ljb;->hW:J

    cmp-long v0, v0, v10

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot compute scroll delta before calling start()"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Ljb;->b(J)F

    move-result v3

    const/high16 v4, -0x3f800000    # -4.0f

    mul-float/2addr v4, v3

    mul-float/2addr v4, v3

    const/high16 v5, 0x40800000    # 4.0f

    mul-float/2addr v3, v5

    add-float/2addr v3, v4

    iget-wide v4, v2, Ljb;->hW:J

    sub-long v4, v0, v4

    iput-wide v0, v2, Ljb;->hW:J

    long-to-float v0, v4

    mul-float/2addr v0, v3

    iget v1, v2, Ljb;->hT:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, v2, Ljb;->hX:I

    long-to-float v0, v4

    mul-float/2addr v0, v3

    iget v1, v2, Ljb;->hU:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, v2, Ljb;->hY:I

    .line 709
    iget v0, v2, Ljb;->hX:I

    .line 710
    iget v1, v2, Ljb;->hY:I

    .line 711
    iget-object v2, p0, Ljc;->ic:Lja;

    invoke-virtual {v2, v0, v1}, Lja;->d(II)V

    .line 714
    iget-object v0, p0, Ljc;->ic:Lja;

    invoke-static {v0}, Lja;->g(Lja;)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p0}, Lge;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    goto/16 :goto_0
.end method
