.class public final Lkcu;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final eSn:Ljava/lang/String;

.field private static final eSo:Ljava/lang/String;

.field private static eSp:Ljava/lang/String;

.field private static eSq:Ljava/lang/String;


# instance fields
.field private final eRZ:Lkcq;

.field private eRm:J

.field private final eRo:Ljava/net/URI;

.field private eSA:I

.field private eSB:Ljava/util/Set;

.field private eSC:Ljava/lang/String;

.field private eSD:I

.field private eSa:Z

.field private eSb:I

.field private eSh:Ljava/lang/String;

.field private eSi:Ljava/lang/String;

.field private eSr:Ljava/util/Date;

.field private eSs:Ljava/util/Date;

.field private eSt:Ljava/util/Date;

.field private eSu:J

.field private eSv:Z

.field private eSw:I

.field private eSx:Z

.field private eSy:Z

.field private eSz:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lkbl;->byd()Lkbl;

    invoke-static {}, Lkbl;->getPrefix()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-Sent-Millis"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lkcu;->eSn:Ljava/lang/String;

    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lkbl;->byd()Lkbl;

    invoke-static {}, Lkbl;->getPrefix()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-Received-Millis"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lkcu;->eSo:Ljava/lang/String;

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lkbl;->byd()Lkbl;

    invoke-static {}, Lkbl;->getPrefix()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-Response-Source"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lkcu;->eSp:Ljava/lang/String;

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lkbl;->byd()Lkbl;

    invoke-static {}, Lkbl;->getPrefix()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-Selected-Transport"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lkcu;->eSq:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/net/URI;Lkcq;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput v2, p0, Lkcu;->eSb:I

    .line 98
    iput v2, p0, Lkcu;->eSw:I

    .line 110
    iput v2, p0, Lkcu;->eSA:I

    .line 113
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lkcu;->eSB:Ljava/util/Set;

    .line 117
    iput v2, p0, Lkcu;->eSD:I

    .line 121
    iput-object p1, p0, Lkcu;->eRo:Ljava/net/URI;

    .line 122
    iput-object p2, p0, Lkcu;->eRZ:Lkcq;

    .line 124
    new-instance v3, Lkcv;

    invoke-direct {v3, p0}, Lkcv;-><init>(Lkcu;)V

    move v0, v1

    .line 142
    :goto_0
    invoke-virtual {p2}, Lkcq;->length()I

    move-result v2

    if-ge v0, v2, :cond_f

    .line 143
    invoke-virtual {p2, v0}, Lkcq;->tL(I)Ljava/lang/String;

    move-result-object v2

    .line 144
    invoke-virtual {p2, v0}, Lkcq;->getValue(I)Ljava/lang/String;

    move-result-object v4

    .line 145
    const-string v5, "Cache-Control"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 146
    invoke-static {v4, v3}, Lkbw;->a(Ljava/lang/String;Lkbx;)V

    .line 142
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 147
    :cond_1
    const-string v5, "Date"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 148
    invoke-static {v4}, Lkca;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    iput-object v2, p0, Lkcu;->eSr:Ljava/util/Date;

    goto :goto_1

    .line 149
    :cond_2
    const-string v5, "Expires"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 150
    invoke-static {v4}, Lkca;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    iput-object v2, p0, Lkcu;->eSt:Ljava/util/Date;

    goto :goto_1

    .line 151
    :cond_3
    const-string v5, "Last-Modified"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 152
    invoke-static {v4}, Lkca;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    iput-object v2, p0, Lkcu;->eSs:Ljava/util/Date;

    goto :goto_1

    .line 153
    :cond_4
    const-string v5, "ETag"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 154
    iput-object v4, p0, Lkcu;->eSz:Ljava/lang/String;

    goto :goto_1

    .line 155
    :cond_5
    const-string v5, "Pragma"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 156
    const-string v2, "no-cache"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 157
    const/4 v2, 0x1

    iput-boolean v2, p0, Lkcu;->eSa:Z

    goto :goto_1

    .line 159
    :cond_6
    const-string v5, "Age"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 160
    invoke-static {v4}, Lkbw;->Ax(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lkcu;->eSA:I

    goto :goto_1

    .line 161
    :cond_7
    const-string v5, "Vary"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 163
    iget-object v2, p0, Lkcu;->eSB:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 164
    new-instance v2, Ljava/util/TreeSet;

    sget-object v5, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-direct {v2, v5}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v2, p0, Lkcu;->eSB:Ljava/util/Set;

    .line 166
    :cond_8
    const-string v2, ","

    invoke-virtual {v4, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v2, v1

    :goto_2
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 167
    iget-object v7, p0, Lkcu;->eSB:Ljava/util/Set;

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v7, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 166
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 169
    :cond_9
    const-string v5, "Content-Encoding"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 170
    iput-object v4, p0, Lkcu;->eSC:Ljava/lang/String;

    goto/16 :goto_1

    .line 171
    :cond_a
    const-string v5, "Transfer-Encoding"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 172
    iput-object v4, p0, Lkcu;->eSh:Ljava/lang/String;

    goto/16 :goto_1

    .line 173
    :cond_b
    const-string v5, "Content-Length"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 175
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lkcu;->eSD:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 177
    :catch_0
    move-exception v2

    goto/16 :goto_1

    .line 178
    :cond_c
    const-string v5, "Connection"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 179
    iput-object v4, p0, Lkcu;->eSi:Ljava/lang/String;

    goto/16 :goto_1

    .line 180
    :cond_d
    sget-object v5, Lkcu;->eSn:Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 181
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lkcu;->eRm:J

    goto/16 :goto_1

    .line 182
    :cond_e
    sget-object v5, Lkcu;->eSo:Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 183
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lkcu;->eSu:J

    goto/16 :goto_1

    .line 186
    :cond_f
    return-void
.end method

.method private static AD(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 496
    const-string v0, "Connection"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Keep-Alive"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Proxy-Authenticate"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Proxy-Authorization"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "TE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Trailers"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Transfer-Encoding"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Upgrade"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lkcu;I)I
    .locals 0

    .prologue
    .line 35
    iput p1, p0, Lkcu;->eSb:I

    return p1
.end method

.method static synthetic a(Lkcu;Z)Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkcu;->eSa:Z

    return v0
.end method

.method static synthetic b(Lkcu;I)I
    .locals 0

    .prologue
    .line 35
    iput p1, p0, Lkcu;->eSw:I

    return p1
.end method

.method static synthetic b(Lkcu;Z)Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkcu;->eSv:Z

    return v0
.end method

.method static synthetic c(Lkcu;Z)Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkcu;->eSx:Z

    return v0
.end method

.method static synthetic d(Lkcu;Z)Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkcu;->eSy:Z

    return v0
.end method


# virtual methods
.method public final AC(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lkcu;->eRZ:Lkcq;

    sget-object v1, Lkcu;->eSq:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lkcq;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    return-void
.end method

.method public final a(JLkcs;)Lkav;
    .locals 11

    .prologue
    const/4 v10, -0x1

    const-wide/16 v2, 0x0

    .line 392
    invoke-virtual {p0, p3}, Lkcu;->a(Lkcs;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 393
    sget-object v0, Lkav;->ePT:Lkav;

    .line 438
    :goto_0
    return-object v0

    .line 396
    :cond_0
    iget-boolean v0, p3, Lkcs;->eSa:Z

    if-nez v0, :cond_1

    invoke-virtual {p3}, Lkcs;->byS()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 397
    :cond_1
    sget-object v0, Lkav;->ePT:Lkav;

    goto :goto_0

    .line 400
    :cond_2
    iget-object v0, p0, Lkcu;->eSr:Ljava/util/Date;

    if-eqz v0, :cond_9

    iget-wide v0, p0, Lkcu;->eSu:J

    iget-object v4, p0, Lkcu;->eSr:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    :goto_1
    iget v4, p0, Lkcu;->eSA:I

    if-eq v4, v10, :cond_3

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v5, p0, Lkcu;->eSA:I

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    :cond_3
    iget-wide v4, p0, Lkcu;->eSu:J

    iget-wide v6, p0, Lkcu;->eRm:J

    sub-long/2addr v4, v6

    iget-wide v6, p0, Lkcu;->eSu:J

    sub-long v6, p1, v6

    add-long/2addr v0, v4

    add-long/2addr v6, v0

    .line 401
    iget v0, p0, Lkcu;->eSb:I

    if-eq v0, v10, :cond_a

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v1, p0, Lkcu;->eSb:I

    int-to-long v4, v1

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 403
    :cond_4
    :goto_2
    iget v4, p3, Lkcs;->eSb:I

    if-eq v4, v10, :cond_5

    .line 404
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v5, p3, Lkcs;->eSb:I

    int-to-long v8, v5

    invoke-virtual {v4, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 408
    :cond_5
    iget v4, p3, Lkcs;->eSd:I

    if-eq v4, v10, :cond_17

    .line 409
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v5, p3, Lkcs;->eSd:I

    int-to-long v8, v5

    invoke-virtual {v4, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    .line 413
    :goto_3
    iget-boolean v8, p0, Lkcu;->eSy:Z

    if-nez v8, :cond_6

    iget v8, p3, Lkcs;->eSc:I

    if-eq v8, v10, :cond_6

    .line 414
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v3, p3, Lkcs;->eSc:I

    int-to-long v8, v3

    invoke-virtual {v2, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 417
    :cond_6
    iget-boolean v8, p0, Lkcu;->eSa:Z

    if-nez v8, :cond_11

    add-long v8, v6, v4

    add-long/2addr v2, v0

    cmp-long v2, v8, v2

    if-gez v2, :cond_11

    .line 418
    add-long v2, v6, v4

    cmp-long v0, v2, v0

    if-ltz v0, :cond_7

    .line 419
    iget-object v0, p0, Lkcu;->eRZ:Lkcq;

    const-string v1, "Warning"

    const-string v2, "110 HttpURLConnection \"Response is stale\""

    invoke-virtual {v0, v1, v2}, Lkcq;->bk(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    :cond_7
    const-wide/32 v0, 0x5265c00

    cmp-long v0, v6, v0

    if-lez v0, :cond_8

    iget v0, p0, Lkcu;->eSb:I

    if-ne v0, v10, :cond_10

    iget-object v0, p0, Lkcu;->eSt:Ljava/util/Date;

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_8

    .line 423
    iget-object v0, p0, Lkcu;->eRZ:Lkcq;

    const-string v1, "Warning"

    const-string v2, "113 HttpURLConnection \"Heuristic expiration\""

    invoke-virtual {v0, v1, v2}, Lkcq;->bk(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    :cond_8
    sget-object v0, Lkav;->ePR:Lkav;

    goto/16 :goto_0

    :cond_9
    move-wide v0, v2

    .line 400
    goto/16 :goto_1

    .line 401
    :cond_a
    iget-object v0, p0, Lkcu;->eSt:Ljava/util/Date;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lkcu;->eSr:Ljava/util/Date;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lkcu;->eSr:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    :goto_5
    iget-object v4, p0, Lkcu;->eSt:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long v0, v4, v0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_4

    move-wide v0, v2

    goto/16 :goto_2

    :cond_b
    iget-wide v0, p0, Lkcu;->eSu:J

    goto :goto_5

    :cond_c
    iget-object v0, p0, Lkcu;->eSs:Ljava/util/Date;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lkcu;->eRo:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getRawQuery()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_f

    iget-object v0, p0, Lkcu;->eSr:Ljava/util/Date;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lkcu;->eSr:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    :goto_6
    iget-object v4, p0, Lkcu;->eSs:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v0, v4

    cmp-long v4, v0, v2

    if-lez v4, :cond_e

    const-wide/16 v4, 0xa

    div-long/2addr v0, v4

    goto/16 :goto_2

    :cond_d
    iget-wide v0, p0, Lkcu;->eRm:J

    goto :goto_6

    :cond_e
    move-wide v0, v2

    goto/16 :goto_2

    :cond_f
    move-wide v0, v2

    goto/16 :goto_2

    .line 421
    :cond_10
    const/4 v0, 0x0

    goto :goto_4

    .line 428
    :cond_11
    iget-object v0, p0, Lkcu;->eSs:Ljava/util/Date;

    if-eqz v0, :cond_15

    .line 429
    iget-object v0, p0, Lkcu;->eSs:Ljava/util/Date;

    invoke-virtual {p3, v0}, Lkcs;->b(Ljava/util/Date;)V

    .line 434
    :cond_12
    :goto_7
    iget-object v0, p0, Lkcu;->eSz:Ljava/lang/String;

    if-eqz v0, :cond_14

    .line 435
    iget-object v0, p0, Lkcu;->eSz:Ljava/lang/String;

    iget-object v1, p3, Lkcs;->eSl:Ljava/lang/String;

    if-eqz v1, :cond_13

    iget-object v1, p3, Lkcs;->eRZ:Lkcq;

    const-string v2, "If-None-Match"

    invoke-virtual {v1, v2}, Lkcq;->AB(Ljava/lang/String;)V

    :cond_13
    iget-object v1, p3, Lkcs;->eRZ:Lkcq;

    const-string v2, "If-None-Match"

    invoke-virtual {v1, v2, v0}, Lkcq;->bk(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p3, Lkcs;->eSl:Ljava/lang/String;

    .line 438
    :cond_14
    invoke-virtual {p3}, Lkcs;->byS()Z

    move-result v0

    if-eqz v0, :cond_16

    sget-object v0, Lkav;->ePS:Lkav;

    goto/16 :goto_0

    .line 430
    :cond_15
    iget-object v0, p0, Lkcu;->eSr:Ljava/util/Date;

    if-eqz v0, :cond_12

    .line 431
    iget-object v0, p0, Lkcu;->eSr:Ljava/util/Date;

    invoke-virtual {p3, v0}, Lkcs;->b(Ljava/util/Date;)V

    goto :goto_7

    .line 438
    :cond_16
    sget-object v0, Lkav;->ePT:Lkav;

    goto/16 :goto_0

    :cond_17
    move-wide v4, v2

    goto/16 :goto_3
.end method

.method public final a(Ljava/util/Map;Ljava/util/Map;)Z
    .locals 3

    .prologue
    .line 379
    iget-object v0, p0, Lkcu;->eSB:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 380
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v2, v0}, Lkbt;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 381
    const/4 v0, 0x0

    .line 384
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lkcs;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 343
    iget-object v1, p0, Lkcu;->eRZ:Lkcq;

    invoke-virtual {v1}, Lkcq;->getResponseCode()I

    move-result v1

    .line 344
    const/16 v2, 0xc8

    if-eq v1, v2, :cond_1

    const/16 v2, 0xcb

    if-eq v1, v2, :cond_1

    const/16 v2, 0x12c

    if-eq v1, v2, :cond_1

    const/16 v2, 0x12d

    if-eq v1, v2, :cond_1

    const/16 v2, 0x19a

    if-eq v1, v2, :cond_1

    .line 362
    :cond_0
    :goto_0
    return v0

    .line 354
    :cond_1
    iget-boolean v1, p1, Lkcs;->eSf:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lkcu;->eSx:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lkcu;->eSy:Z

    if-nez v1, :cond_2

    iget v1, p0, Lkcu;->eSw:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 358
    :cond_2
    iget-boolean v1, p0, Lkcu;->eSv:Z

    if-nez v1, :cond_0

    .line 362
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lkcu;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 446
    iget-object v1, p1, Lkcu;->eRZ:Lkcq;

    invoke-virtual {v1}, Lkcq;->getResponseCode()I

    move-result v1

    const/16 v2, 0x130

    if-ne v1, v2, :cond_1

    .line 459
    :cond_0
    :goto_0
    return v0

    .line 453
    :cond_1
    iget-object v1, p0, Lkcu;->eSs:Ljava/util/Date;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lkcu;->eSs:Ljava/util/Date;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lkcu;->eSs:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iget-object v1, p0, Lkcu;->eSs:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 459
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lkcu;)Lkcu;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 467
    new-instance v2, Lkcq;

    invoke-direct {v2}, Lkcq;-><init>()V

    .line 468
    iget-object v0, p0, Lkcu;->eRZ:Lkcq;

    invoke-virtual {v0}, Lkcq;->byP()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lkcq;->Az(Ljava/lang/String;)V

    move v0, v1

    .line 470
    :goto_0
    iget-object v3, p0, Lkcu;->eRZ:Lkcq;

    invoke-virtual {v3}, Lkcq;->length()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 471
    iget-object v3, p0, Lkcu;->eRZ:Lkcq;

    invoke-virtual {v3, v0}, Lkcq;->tL(I)Ljava/lang/String;

    move-result-object v3

    .line 472
    iget-object v4, p0, Lkcu;->eRZ:Lkcq;

    invoke-virtual {v4, v0}, Lkcq;->getValue(I)Ljava/lang/String;

    move-result-object v4

    .line 473
    const-string v5, "Warning"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 474
    :cond_0
    invoke-static {v3}, Lkcu;->AD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p1, Lkcu;->eRZ:Lkcq;

    invoke-virtual {v5, v3}, Lkcq;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_2

    .line 477
    :cond_1
    invoke-virtual {v2, v3, v4}, Lkcq;->bk(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 481
    :cond_3
    :goto_1
    iget-object v0, p1, Lkcu;->eRZ:Lkcq;

    invoke-virtual {v0}, Lkcq;->length()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 482
    iget-object v0, p1, Lkcu;->eRZ:Lkcq;

    invoke-virtual {v0, v1}, Lkcq;->tL(I)Ljava/lang/String;

    move-result-object v0

    .line 483
    invoke-static {v0}, Lkcu;->AD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 484
    iget-object v3, p1, Lkcu;->eRZ:Lkcq;

    invoke-virtual {v3, v1}, Lkcq;->getValue(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lkcq;->bk(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 488
    :cond_5
    new-instance v0, Lkcu;

    iget-object v1, p0, Lkcu;->eRo:Ljava/net/URI;

    invoke-direct {v0, v1, v2}, Lkcu;-><init>(Ljava/net/URI;Lkcq;)V

    return-object v0
.end method

.method public final b(Lkav;)V
    .locals 4

    .prologue
    .line 282
    iget-object v0, p0, Lkcu;->eRZ:Lkcq;

    sget-object v1, Lkcu;->eSp:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lkav;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lkcu;->eRZ:Lkcq;

    invoke-virtual {v3}, Lkcq;->getResponseCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lkcq;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    return-void
.end method

.method public final byT()Z
    .locals 2

    .prologue
    .line 189
    const-string v0, "gzip"

    iget-object v1, p0, Lkcu;->eSC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final byU()V
    .locals 2

    .prologue
    .line 193
    const/4 v0, 0x0

    iput-object v0, p0, Lkcu;->eSC:Ljava/lang/String;

    .line 194
    iget-object v0, p0, Lkcu;->eRZ:Lkcq;

    const-string v1, "Content-Encoding"

    invoke-virtual {v0, v1}, Lkcq;->AB(Ljava/lang/String;)V

    .line 195
    return-void
.end method

.method public final byV()V
    .locals 2

    .prologue
    .line 198
    const/4 v0, -0x1

    iput v0, p0, Lkcu;->eSD:I

    .line 199
    iget-object v0, p0, Lkcu;->eRZ:Lkcq;

    const-string v1, "Content-Length"

    invoke-virtual {v0, v1}, Lkcq;->AB(Ljava/lang/String;)V

    .line 200
    return-void
.end method

.method public final byW()Z
    .locals 2

    .prologue
    .line 207
    const-string v0, "close"

    iget-object v1, p0, Lkcu;->eSi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final byX()Lkcq;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lkcu;->eRZ:Lkcq;

    return-object v0
.end method

.method public final byY()Ljava/util/Set;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lkcu;->eSB:Ljava/util/Set;

    return-object v0
.end method

.method public final byZ()Z
    .locals 2

    .prologue
    .line 370
    iget-object v0, p0, Lkcu;->eSB:Ljava/util/Set;

    const-string v1, "*"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final getContentLength()I
    .locals 1

    .prologue
    .line 267
    iget v0, p0, Lkcu;->eSD:I

    return v0
.end method

.method public final isChunked()Z
    .locals 2

    .prologue
    .line 203
    const-string v0, "chunked"

    iget-object v1, p0, Lkcu;->eSh:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final n(JJ)V
    .locals 3

    .prologue
    .line 275
    iput-wide p1, p0, Lkcu;->eRm:J

    .line 276
    iget-object v0, p0, Lkcu;->eRZ:Lkcq;

    sget-object v1, Lkcu;->eSn:Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lkcq;->bk(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    iput-wide p3, p0, Lkcu;->eSu:J

    .line 278
    iget-object v0, p0, Lkcu;->eRZ:Lkcq;

    sget-object v1, Lkcu;->eSo:Ljava/lang/String;

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lkcq;->bk(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    return-void
.end method
