.class public final Lkex;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eVk:I

.field private eVl:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 408
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 409
    iput v0, p0, Lkex;->aez:I

    iput v0, p0, Lkex;->eVk:I

    iput v0, p0, Lkex;->eVl:I

    const/4 v0, 0x0

    iput-object v0, p0, Lkex;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lkex;->eCz:I

    .line 410
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 351
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lkex;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    iput v0, p0, Lkex;->eVk:I

    iget v0, p0, Lkex;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkex;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    iput v0, p0, Lkex;->eVl:I

    iget v0, p0, Lkex;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkex;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 424
    iget v0, p0, Lkex;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 425
    const/4 v0, 0x1

    iget v1, p0, Lkex;->eVk:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bt(II)V

    .line 427
    :cond_0
    iget v0, p0, Lkex;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 428
    const/4 v0, 0x2

    iget v1, p0, Lkex;->eVl:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bt(II)V

    .line 430
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 431
    return-void
.end method

.method public final bzK()I
    .locals 1

    .prologue
    .line 373
    iget v0, p0, Lkex;->eVk:I

    return v0
.end method

.method public final bzL()I
    .locals 1

    .prologue
    .line 392
    iget v0, p0, Lkex;->eVl:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 435
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 436
    iget v1, p0, Lkex;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 437
    const/4 v1, 0x1

    iget v2, p0, Lkex;->eVk:I

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 440
    :cond_0
    iget v1, p0, Lkex;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 441
    const/4 v1, 0x2

    iget v2, p0, Lkex;->eVl:I

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 444
    :cond_1
    return v0
.end method

.method public final ua(I)Lkex;
    .locals 1

    .prologue
    .line 376
    iput p1, p0, Lkex;->eVk:I

    .line 377
    iget v0, p0, Lkex;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkex;->aez:I

    .line 378
    return-object p0
.end method

.method public final ub(I)Lkex;
    .locals 1

    .prologue
    .line 395
    iput p1, p0, Lkex;->eVl:I

    .line 396
    iget v0, p0, Lkex;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkex;->aez:I

    .line 397
    return-object p0
.end method
