.class public final Ljzx;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ako:Ljava/lang/String;

.field private bot:I

.field private eNS:I

.field private eOA:I

.field private eOB:I

.field private eOC:Z

.field private eOD:[I

.field private eOy:Ljava/lang/String;

.field private eOz:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1686
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1687
    iput v1, p0, Ljzx;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljzx;->eOy:Ljava/lang/String;

    iput v1, p0, Ljzx;->bot:I

    const-string v0, ""

    iput-object v0, p0, Ljzx;->ako:Ljava/lang/String;

    iput v1, p0, Ljzx;->eOz:I

    iput v1, p0, Ljzx;->eOA:I

    iput v1, p0, Ljzx;->eOB:I

    iput v1, p0, Ljzx;->eNS:I

    iput-boolean v1, p0, Ljzx;->eOC:Z

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljzx;->eOD:[I

    const/4 v0, 0x0

    iput-object v0, p0, Ljzx;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljzx;->eCz:I

    .line 1688
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1506
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljzx;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljzx;->eOy:Ljava/lang/String;

    iget v0, p0, Ljzx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzx;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzx;->bot:I

    iget v0, p0, Ljzx;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzx;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljzx;->ako:Ljava/lang/String;

    iget v0, p0, Ljzx;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljzx;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzx;->eOz:I

    iget v0, p0, Ljzx;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljzx;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzx;->eOA:I

    iget v0, p0, Ljzx;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljzx;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzx;->eOB:I

    iget v0, p0, Ljzx;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljzx;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzx;->eNS:I

    iget v0, p0, Ljzx;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljzx;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljzx;->eOC:Z

    iget v0, p0, Ljzx;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljzx;->aez:I

    goto :goto_0

    :sswitch_9
    const/16 v0, 0x48

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljzx;->eOD:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljzx;->eOD:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljzx;->eOD:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Ljzx;->eOD:[I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Ljsi;->btQ()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljzx;->eOD:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Ljzx;->eOD:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Ljzx;->eOD:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Ljzx;->eOD:[I

    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x4a -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 1709
    iget v0, p0, Ljzx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1710
    const/4 v0, 0x1

    iget-object v1, p0, Ljzx;->eOy:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1712
    :cond_0
    iget v0, p0, Ljzx;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1713
    const/4 v0, 0x2

    iget v1, p0, Ljzx;->bot:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1715
    :cond_1
    iget v0, p0, Ljzx;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 1716
    const/4 v0, 0x3

    iget-object v1, p0, Ljzx;->ako:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1718
    :cond_2
    iget v0, p0, Ljzx;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 1719
    const/4 v0, 0x4

    iget v1, p0, Ljzx;->eOz:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1721
    :cond_3
    iget v0, p0, Ljzx;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 1722
    const/4 v0, 0x5

    iget v1, p0, Ljzx;->eOA:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1724
    :cond_4
    iget v0, p0, Ljzx;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 1725
    const/4 v0, 0x6

    iget v1, p0, Ljzx;->eOB:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1727
    :cond_5
    iget v0, p0, Ljzx;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_6

    .line 1728
    const/4 v0, 0x7

    iget v1, p0, Ljzx;->eNS:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1730
    :cond_6
    iget v0, p0, Ljzx;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_7

    .line 1731
    const/16 v0, 0x8

    iget-boolean v1, p0, Ljzx;->eOC:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 1733
    :cond_7
    iget-object v0, p0, Ljzx;->eOD:[I

    if-eqz v0, :cond_8

    iget-object v0, p0, Ljzx;->eOD:[I

    array-length v0, v0

    if-lez v0, :cond_8

    .line 1734
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljzx;->eOD:[I

    array-length v1, v1

    if-ge v0, v1, :cond_8

    .line 1735
    const/16 v1, 0x9

    iget-object v2, p0, Ljzx;->eOD:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Ljsj;->bq(II)V

    .line 1734
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1738
    :cond_8
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1739
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1743
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1744
    iget v2, p0, Ljzx;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 1745
    const/4 v2, 0x1

    iget-object v3, p0, Ljzx;->eOy:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1748
    :cond_0
    iget v2, p0, Ljzx;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 1749
    const/4 v2, 0x2

    iget v3, p0, Ljzx;->bot:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1752
    :cond_1
    iget v2, p0, Ljzx;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    .line 1753
    const/4 v2, 0x3

    iget-object v3, p0, Ljzx;->ako:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1756
    :cond_2
    iget v2, p0, Ljzx;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_3

    .line 1757
    const/4 v2, 0x4

    iget v3, p0, Ljzx;->eOz:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1760
    :cond_3
    iget v2, p0, Ljzx;->aez:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_4

    .line 1761
    const/4 v2, 0x5

    iget v3, p0, Ljzx;->eOA:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1764
    :cond_4
    iget v2, p0, Ljzx;->aez:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_5

    .line 1765
    const/4 v2, 0x6

    iget v3, p0, Ljzx;->eOB:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1768
    :cond_5
    iget v2, p0, Ljzx;->aez:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_6

    .line 1769
    const/4 v2, 0x7

    iget v3, p0, Ljzx;->eNS:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1772
    :cond_6
    iget v2, p0, Ljzx;->aez:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_7

    .line 1773
    const/16 v2, 0x8

    iget-boolean v3, p0, Ljzx;->eOC:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1776
    :cond_7
    iget-object v2, p0, Ljzx;->eOD:[I

    if-eqz v2, :cond_9

    iget-object v2, p0, Ljzx;->eOD:[I

    array-length v2, v2

    if-lez v2, :cond_9

    move v2, v1

    .line 1778
    :goto_0
    iget-object v3, p0, Ljzx;->eOD:[I

    array-length v3, v3

    if-ge v1, v3, :cond_8

    .line 1779
    iget-object v3, p0, Ljzx;->eOD:[I

    aget v3, v3, v1

    .line 1780
    invoke-static {v3}, Ljsj;->sa(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 1778
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1783
    :cond_8
    add-int/2addr v0, v2

    .line 1784
    iget-object v1, p0, Ljzx;->eOD:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1786
    :cond_9
    return v0
.end method
