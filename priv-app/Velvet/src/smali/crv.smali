.class final Lcrv;
.super Lcrx;
.source "PG"


# instance fields
.field private synthetic aXK:Landroid/accounts/Account;

.field private synthetic amO:Z

.field private synthetic amQ:Lemy;

.field private synthetic bhm:Z

.field private synthetic bhn:Lcrr;


# direct methods
.method constructor <init>(Lcrr;Landroid/accounts/Account;ZZLemy;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcrv;->bhn:Lcrr;

    iput-object p2, p0, Lcrv;->aXK:Landroid/accounts/Account;

    iput-boolean p3, p0, Lcrv;->amO:Z

    iput-boolean p4, p0, Lcrv;->bhm:Z

    iput-object p5, p0, Lcrv;->amQ:Lemy;

    invoke-direct {p0, p1}, Lcrx;-><init>(Lcrr;)V

    return-void
.end method

.method private varargs Sy()Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 195
    :try_start_0
    iget-object v0, p0, Lcrv;->bhn:Lcrr;

    iget-object v1, p0, Lcrv;->aXK:Landroid/accounts/Account;

    iget-boolean v2, p0, Lcrv;->amO:Z

    iget-boolean v3, p0, Lcrv;->bhm:Z

    invoke-virtual {v0, v1, v2, v3}, Lcrr;->b(Landroid/accounts/Account;ZZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 198
    :goto_0
    return-object v0

    .line 196
    :catch_0
    move-exception v0

    .line 197
    const-string v1, "Search.SearchHistoryHelper"

    const-string v2, "Failed to set search history setting"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 198
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 191
    invoke-direct {p0}, Lcrv;->Sy()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 191
    check-cast p1, Ljava/lang/Boolean;

    iget-object v0, p0, Lcrv;->amQ:Lemy;

    invoke-interface {v0, p1}, Lemy;->aj(Ljava/lang/Object;)Z

    return-void
.end method
