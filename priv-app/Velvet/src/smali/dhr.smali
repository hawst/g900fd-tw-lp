.class public Ldhr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final avs:I

.field public final bxX:Lcom/google/android/shared/search/Query;

.field public final bxY:I

.field public final bxZ:Z

.field private final bya:Ljava/util/Set;

.field private final byb:Ljava/util/Set;


# direct methods
.method private constructor <init>(Lcom/google/android/shared/search/Query;IZILjava/util/Set;Ljava/util/Set;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Ldhr;->bxX:Lcom/google/android/shared/search/Query;

    .line 45
    invoke-static {p5}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Ldhr;->bya:Ljava/util/Set;

    .line 46
    invoke-static {p6}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Ldhr;->byb:Ljava/util/Set;

    .line 48
    invoke-interface {p5}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p6}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 49
    iput p2, p0, Ldhr;->bxY:I

    .line 50
    iput-boolean p3, p0, Ldhr;->bxZ:Z

    .line 51
    iput p4, p0, Ldhr;->avs:I

    .line 52
    return-void

    .line 48
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/google/android/shared/search/Query;IZILjava/util/Set;Ljava/util/Set;B)V
    .locals 0

    .prologue
    .line 14
    invoke-direct/range {p0 .. p6}, Ldhr;-><init>(Lcom/google/android/shared/search/Query;IZILjava/util/Set;Ljava/util/Set;)V

    return-void
.end method

.method public static b(Lcom/google/android/shared/search/Query;II)Ldhs;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Ldhs;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Ldhs;-><init>(Lcom/google/android/shared/search/Query;IIB)V

    return-object v0
.end method


# virtual methods
.method public final ao(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 55
    new-instance v1, Lcom/google/android/gms/appdatasearch/CorpusId;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    iget-object v2, p0, Ldhr;->bya:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Ldhr;->bya:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Ldhr;->byb:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
