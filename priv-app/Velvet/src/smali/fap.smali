.class public final Lfap;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfao;


# instance fields
.field private final mAccessibilityManager:Lelo;

.field private final mClock:Lemp;

.field private final mEntryProvider:Lfaq;

.field private final mLocationDisabledCardHelper:Lfbq;

.field private final mNowNotificationManager:Lfga;


# direct methods
.method public constructor <init>(Lfaq;Lfga;Lfbq;Lemp;Lelo;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lfap;->mEntryProvider:Lfaq;

    .line 38
    iput-object p2, p0, Lfap;->mNowNotificationManager:Lfga;

    .line 39
    iput-object p3, p0, Lfap;->mLocationDisabledCardHelper:Lfbq;

    .line 40
    iput-object p4, p0, Lfap;->mClock:Lemp;

    .line 41
    iput-object p5, p0, Lfap;->mAccessibilityManager:Lelo;

    .line 42
    return-void
.end method


# virtual methods
.method public final awR()Z
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 47
    iget-object v2, p0, Lfap;->mLocationDisabledCardHelper:Lfbq;

    invoke-virtual {v2}, Lfbq;->axn()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lfap;->mEntryProvider:Lfaq;

    invoke-virtual {v2}, Lfaq;->invalidate()V

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    .line 72
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 47
    goto :goto_0

    .line 52
    :cond_2
    iget-object v2, p0, Lfap;->mEntryProvider:Lfaq;

    invoke-virtual {v2}, Lfaq;->axf()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lfap;->mEntryProvider:Lfaq;

    invoke-virtual {v2}, Lfaq;->invalidate()V

    move v2, v0

    :goto_2
    if-nez v2, :cond_0

    .line 57
    iget-object v2, p0, Lfap;->mEntryProvider:Lfaq;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v2, v3}, Lfaq;->e(Ljava/util/Locale;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lfap;->mEntryProvider:Lfaq;

    invoke-virtual {v2}, Lfaq;->invalidate()V

    move v2, v0

    :goto_3
    if-nez v2, :cond_0

    .line 62
    iget-object v2, p0, Lfap;->mNowNotificationManager:Lfga;

    invoke-interface {v2}, Lfga;->azB()J

    move-result-wide v2

    iget-object v4, p0, Lfap;->mEntryProvider:Lfaq;

    invoke-virtual {v4}, Lfaq;->axa()J

    move-result-wide v4

    iget-object v6, p0, Lfap;->mClock:Lemp;

    invoke-interface {v6}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v8, v6, v2

    if-lez v8, :cond_3

    cmp-long v6, v6, v4

    if-gtz v6, :cond_6

    :cond_3
    move v2, v1

    :goto_4
    if-nez v2, :cond_0

    .line 67
    iget-object v2, p0, Lfap;->mEntryProvider:Lfaq;

    invoke-virtual {v2}, Lfaq;->axi()Z

    move-result v2

    if-nez v2, :cond_0

    .line 72
    iget-object v2, p0, Lfap;->mEntryProvider:Lfaq;

    invoke-virtual {v2}, Lfaq;->mP()Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lfap;->mAccessibilityManager:Lelo;

    invoke-virtual {v2}, Lelo;->aum()Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v1, p0, Lfap;->mEntryProvider:Lfaq;

    invoke-virtual {v1}, Lfaq;->invalidate()V

    goto :goto_1

    :cond_4
    move v2, v1

    .line 52
    goto :goto_2

    :cond_5
    move v2, v1

    .line 57
    goto :goto_3

    .line 62
    :cond_6
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-lez v6, :cond_7

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x7530

    cmp-long v2, v2, v4

    if-lez v2, :cond_7

    iget-object v2, p0, Lfap;->mEntryProvider:Lfaq;

    invoke-virtual {v2}, Lfaq;->invalidate()V

    move v2, v0

    goto :goto_4

    :cond_7
    move v2, v1

    goto :goto_4

    :cond_8
    move v0, v1

    .line 72
    goto :goto_1
.end method
