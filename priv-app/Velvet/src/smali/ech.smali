.class public abstract Lech;
.super Landroid/os/Binder;
.source "PG"

# interfaces
.implements Lecg;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 23
    const-string v0, "com.google.android.search.shared.service.ISearchService"

    invoke-virtual {p0, p0, v0}, Lech;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 24
    return-void
.end method

.method public static E(Landroid/os/IBinder;)Lecg;
    .locals 2

    .prologue
    .line 31
    if-nez p0, :cond_0

    .line 32
    const/4 v0, 0x0

    .line 38
    :goto_0
    return-object v0

    .line 34
    :cond_0
    const-string v0, "com.google.android.search.shared.service.ISearchService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 35
    if-eqz v0, :cond_1

    instance-of v1, v0, Lecg;

    if-eqz v1, :cond_1

    .line 36
    check-cast v0, Lecg;

    goto :goto_0

    .line 38
    :cond_1
    new-instance v0, Leci;

    invoke-direct {v0, p0}, Leci;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 42
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 46
    sparse-switch p1, :sswitch_data_0

    .line 202
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v4

    :goto_0
    return v4

    .line 50
    :sswitch_0
    const-string v0, "com.google.android.search.shared.service.ISearchService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 55
    :sswitch_1
    const-string v0, "com.google.android.search.shared.service.ISearchService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    sget-object v0, Lcom/google/android/shared/search/Query;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    .line 63
    :goto_1
    invoke-virtual {p0, v0}, Lech;->x(Lcom/google/android/shared/search/Query;)V

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 61
    goto :goto_1

    .line 68
    :sswitch_2
    const-string v0, "com.google.android.search.shared.service.ISearchService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 70
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    sget-object v0, Lcom/google/android/shared/search/Query;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    .line 76
    :goto_2
    invoke-virtual {p0, v0}, Lech;->y(Lcom/google/android/shared/search/Query;)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 74
    goto :goto_2

    .line 81
    :sswitch_3
    const-string v0, "com.google.android.search.shared.service.ISearchService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p0}, Lech;->UF()V

    goto :goto_0

    .line 87
    :sswitch_4
    const-string v0, "com.google.android.search.shared.service.ISearchService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 88
    invoke-virtual {p0}, Lech;->cancel()V

    goto :goto_0

    .line 93
    :sswitch_5
    const-string v0, "com.google.android.search.shared.service.ISearchService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p0}, Lech;->stopListening()V

    goto :goto_0

    .line 99
    :sswitch_6
    const-string v0, "com.google.android.search.shared.service.ISearchService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 101
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    .line 102
    sget-object v0, Lcom/google/android/shared/search/Suggestion;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    move-object v2, v0

    .line 108
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    .line 109
    sget-object v0, Lcom/google/android/shared/search/SearchBoxStats;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/SearchBoxStats;

    .line 114
    :goto_4
    invoke-virtual {p0, v2, v0}, Lech;->a(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;)V

    goto :goto_0

    :cond_2
    move-object v2, v1

    .line 105
    goto :goto_3

    :cond_3
    move-object v0, v1

    .line 112
    goto :goto_4

    .line 119
    :sswitch_7
    const-string v0, "com.google.android.search.shared.service.ISearchService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 121
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    .line 122
    sget-object v0, Lcom/google/android/shared/search/Suggestion;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    move-object v2, v0

    .line 128
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    .line 129
    sget-object v0, Lcom/google/android/shared/search/SearchBoxStats;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/SearchBoxStats;

    .line 134
    :goto_6
    invoke-virtual {p0, v2, v0}, Lech;->b(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;)V

    goto/16 :goto_0

    :cond_4
    move-object v2, v1

    .line 125
    goto :goto_5

    :cond_5
    move-object v0, v1

    .line 132
    goto :goto_6

    .line 139
    :sswitch_8
    const-string v0, "com.google.android.search.shared.service.ISearchService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 141
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_7

    .line 142
    sget-object v0, Lcom/google/android/shared/search/Suggestion;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    move-object v2, v0

    .line 148
    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    move v3, v4

    .line 150
    :cond_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_8

    .line 151
    sget-object v0, Lcom/google/android/shared/search/SearchBoxStats;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/SearchBoxStats;

    .line 156
    :goto_8
    invoke-virtual {p0, v2, v3, v0}, Lech;->a(Lcom/google/android/shared/search/Suggestion;ZLcom/google/android/shared/search/SearchBoxStats;)V

    goto/16 :goto_0

    :cond_7
    move-object v2, v1

    .line 145
    goto :goto_7

    :cond_8
    move-object v0, v1

    .line 154
    goto :goto_8

    .line 161
    :sswitch_9
    const-string v0, "com.google.android.search.shared.service.ISearchService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 163
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    .line 164
    sget-object v0, Lcom/google/android/shared/search/Suggestion;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    .line 169
    :goto_9
    invoke-virtual {p0, v0}, Lech;->c(Lcom/google/android/shared/search/Suggestion;)V

    goto/16 :goto_0

    :cond_9
    move-object v0, v1

    .line 167
    goto :goto_9

    .line 174
    :sswitch_a
    const-string v0, "com.google.android.search.shared.service.ISearchService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 176
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_a

    move v3, v4

    .line 177
    :cond_a
    invoke-virtual {p0, v3}, Lech;->bq(Z)V

    goto/16 :goto_0

    .line 182
    :sswitch_b
    const-string v0, "com.google.android.search.shared.service.ISearchService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 183
    invoke-virtual {p0}, Lech;->Bq()V

    goto/16 :goto_0

    .line 188
    :sswitch_c
    const-string v0, "com.google.android.search.shared.service.ISearchService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 190
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_b

    .line 191
    sget-object v0, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;

    .line 197
    :goto_a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 198
    invoke-virtual {p0, v0, v1}, Lech;->a(Lcom/google/android/search/shared/actions/ParcelableVoiceAction;I)V

    goto/16 :goto_0

    :cond_b
    move-object v0, v1

    .line 194
    goto :goto_a

    .line 46
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
