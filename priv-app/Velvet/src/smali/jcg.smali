.class public final Ljcg;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eac:[Ljcg;


# instance fields
.field private aez:I

.field private afO:I

.field private agv:I

.field public dRY:Ljie;

.field private dUR:J

.field private ead:Ljava/lang/String;

.field private eae:J

.field private eaf:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v1, p0, Ljcg;->aez:I

    iput v2, p0, Ljcg;->agv:I

    const-string v0, ""

    iput-object v0, p0, Ljcg;->ead:Ljava/lang/String;

    iput-wide v4, p0, Ljcg;->eae:J

    iput-object v3, p0, Ljcg;->dRY:Ljie;

    iput-wide v4, p0, Ljcg;->dUR:J

    iput-boolean v2, p0, Ljcg;->eaf:Z

    iput v1, p0, Ljcg;->afO:I

    iput-object v3, p0, Ljcg;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljcg;->eCz:I

    return-void
.end method

.method public static bgc()[Ljcg;
    .locals 2

    sget-object v0, Ljcg;->eac:[Ljcg;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljcg;->eac:[Ljcg;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljcg;

    sput-object v0, Ljcg;->eac:[Ljcg;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Ljcg;->eac:[Ljcg;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljcg;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljcg;->agv:I

    iget v0, p0, Ljcg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcg;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcg;->ead:Ljava/lang/String;

    iget v0, p0, Ljcg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljcg;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljcg;->eae:J

    iget v0, p0, Ljcg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljcg;->aez:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljcg;->dRY:Ljie;

    if-nez v0, :cond_1

    new-instance v0, Ljie;

    invoke-direct {v0}, Ljie;-><init>()V

    iput-object v0, p0, Ljcg;->dRY:Ljie;

    :cond_1
    iget-object v0, p0, Ljcg;->dRY:Ljie;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljcg;->dUR:J

    iget v0, p0, Ljcg;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljcg;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljcg;->eaf:Z

    iget v0, p0, Ljcg;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljcg;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljcg;->afO:I

    iget v0, p0, Ljcg;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljcg;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    iget v0, p0, Ljcg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Ljcg;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_0
    iget v0, p0, Ljcg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljcg;->ead:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_1
    iget v0, p0, Ljcg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-wide v2, p0, Ljcg;->eae:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_2
    iget-object v0, p0, Ljcg;->dRY:Ljie;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ljcg;->dRY:Ljie;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_3
    iget v0, p0, Ljcg;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-wide v2, p0, Ljcg;->dUR:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_4
    iget v0, p0, Ljcg;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-boolean v1, p0, Ljcg;->eaf:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    :cond_5
    iget v0, p0, Ljcg;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget v1, p0, Ljcg;->afO:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final bdl()J
    .locals 2

    iget-wide v0, p0, Ljcg;->dUR:J

    return-wide v0
.end method

.method public final bdm()Z
    .locals 1

    iget v0, p0, Ljcg;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bgd()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljcg;->ead:Ljava/lang/String;

    return-object v0
.end method

.method public final bge()Z
    .locals 1

    iget v0, p0, Ljcg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bgf()Z
    .locals 1

    iget-boolean v0, p0, Ljcg;->eaf:Z

    return v0
.end method

.method public final cN(J)Ljcg;
    .locals 1

    iput-wide p1, p0, Ljcg;->dUR:J

    iget v0, p0, Ljcg;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljcg;->aez:I

    return-object p0
.end method

.method public final getType()I
    .locals 1

    iget v0, p0, Ljcg;->agv:I

    return v0
.end method

.method protected final lF()I
    .locals 4

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljcg;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Ljcg;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Ljcg;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljcg;->ead:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Ljcg;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-wide v2, p0, Ljcg;->eae:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Ljcg;->dRY:Ljie;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Ljcg;->dRY:Ljie;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Ljcg;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-wide v2, p0, Ljcg;->dUR:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Ljcg;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-boolean v2, p0, Ljcg;->eaf:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Ljcg;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget v2, p0, Ljcg;->afO:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    return v0
.end method

.method public final nX()I
    .locals 1

    iget v0, p0, Ljcg;->afO:I

    return v0
.end method

.method public final nY()Z
    .locals 1

    iget v0, p0, Ljcg;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final oB(I)Ljcg;
    .locals 1

    iput p1, p0, Ljcg;->agv:I

    iget v0, p0, Ljcg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcg;->aez:I

    return-object p0
.end method

.method public final oC(I)Ljcg;
    .locals 1

    iput p1, p0, Ljcg;->afO:I

    iget v0, p0, Ljcg;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljcg;->aez:I

    return-object p0
.end method

.method public final oP()Z
    .locals 1

    iget v0, p0, Ljcg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final sU(Ljava/lang/String;)Ljcg;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljcg;->ead:Ljava/lang/String;

    iget v0, p0, Ljcg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljcg;->aez:I

    return-object p0
.end method
