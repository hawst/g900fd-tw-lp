.class public final Ldqq;
.super Landroid/graphics/drawable/Drawable;
.source "PG"


# instance fields
.field private Gr:I

.field private final bFI:Landroid/graphics/Shader;

.field private final bFJ:Landroid/graphics/Paint;

.field private final bFK:Landroid/graphics/Paint;

.field private final bFL:I

.field private final bFM:I

.field private bFN:I

.field private bFO:Landroid/animation/ArgbEvaluator;


# direct methods
.method public constructor <init>(III)V
    .locals 8

    .prologue
    const/high16 v5, -0x1000000

    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 23
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Ldqq;->bFJ:Landroid/graphics/Paint;

    .line 25
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Ldqq;->bFK:Landroid/graphics/Paint;

    .line 36
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    iput-object v0, p0, Ldqq;->bFO:Landroid/animation/ArgbEvaluator;

    .line 44
    iput p1, p0, Ldqq;->bFL:I

    .line 45
    or-int v0, v5, p2

    iput v0, p0, Ldqq;->bFM:I

    .line 46
    new-instance v0, Landroid/graphics/LinearGradient;

    int-to-float v2, p3

    const v3, 0x3f4ccccd    # 0.8f

    mul-float/2addr v2, v3

    int-to-float v4, p3

    iget v3, p0, Ldqq;->bFL:I

    or-int/2addr v5, v3

    const v3, 0xffffff

    iget v6, p0, Ldqq;->bFL:I

    and-int/2addr v6, v3

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v3, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Ldqq;->bFI:Landroid/graphics/Shader;

    .line 50
    iget-object v0, p0, Ldqq;->bFJ:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 51
    iget-object v0, p0, Ldqq;->bFJ:Landroid/graphics/Paint;

    iget-object v1, p0, Ldqq;->bFI:Landroid/graphics/Shader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 53
    invoke-direct {p0}, Ldqq;->aeO()V

    .line 54
    return-void
.end method

.method private aeO()V
    .locals 4

    .prologue
    .line 105
    iget v0, p0, Ldqq;->bFN:I

    int-to-float v0, v0

    const/high16 v1, 0x437f0000    # 255.0f

    div-float/2addr v0, v1

    .line 106
    iget-object v1, p0, Ldqq;->bFO:Landroid/animation/ArgbEvaluator;

    iget v2, p0, Ldqq;->bFL:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p0, Ldqq;->bFM:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Landroid/animation/ArgbEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 108
    iget v1, p0, Ldqq;->Gr:I

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    mul-int/2addr v1, v2

    div-int/lit16 v1, v1, 0xff

    .line 110
    iget-object v2, p0, Ldqq;->bFK:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 111
    iget-object v0, p0, Ldqq;->bFK:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 112
    iget-object v0, p0, Ldqq;->bFJ:Landroid/graphics/Paint;

    iget v1, p0, Ldqq;->Gr:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 113
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Ldqq;->bFK:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    .line 62
    return-void
.end method

.method public final getAlpha()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Ldqq;->Gr:I

    return v0
.end method

.method public final getOpacity()I
    .locals 2

    .prologue
    const/16 v1, 0xff

    .line 82
    iget v0, p0, Ldqq;->bFN:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Ldqq;->Gr:I

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, -0x3

    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final gi(I)V
    .locals 0

    .prologue
    .line 71
    iput p1, p0, Ldqq;->bFN:I

    .line 72
    invoke-direct {p0}, Ldqq;->aeO()V

    .line 73
    invoke-virtual {p0}, Ldqq;->invalidateSelf()V

    .line 74
    return-void
.end method

.method public final setAlpha(I)V
    .locals 0

    .prologue
    .line 87
    iput p1, p0, Ldqq;->Gr:I

    .line 88
    invoke-direct {p0}, Ldqq;->aeO()V

    .line 89
    invoke-virtual {p0}, Ldqq;->invalidateSelf()V

    .line 90
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Ldqq;->bFJ:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 100
    iget-object v0, p0, Ldqq;->bFK:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 101
    return-void
.end method
