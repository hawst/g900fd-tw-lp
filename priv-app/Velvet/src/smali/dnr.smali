.class final Ldnr;
.super Lenp;
.source "PG"


# instance fields
.field private synthetic bDI:Ldnm;

.field private synthetic bDK:Lhlk;

.field private synthetic bDL:Lcom/google/android/search/shared/actions/VoiceAction;

.field private synthetic bDM:I


# direct methods
.method varargs constructor <init>(Ldnm;Ljava/lang/String;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;[ILhlk;Lcom/google/android/search/shared/actions/VoiceAction;I)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Ldnr;->bDI:Ldnm;

    iput-object p6, p0, Ldnr;->bDK:Lhlk;

    iput-object p7, p0, Ldnr;->bDL:Lcom/google/android/search/shared/actions/VoiceAction;

    iput p8, p0, Ldnr;->bDM:I

    invoke-direct {p0, p2, p3, p4, p5}, Lenp;-><init>(Ljava/lang/String;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;[I)V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 370
    iget-object v0, p0, Ldnr;->bDK:Lhlk;

    iget-object v1, p0, Ldnr;->bDL:Lcom/google/android/search/shared/actions/VoiceAction;

    iget v2, p0, Ldnr;->bDM:I

    invoke-interface {v0, v1, v2}, Lhlk;->d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 370
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldnr;->bDL:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/util/MatchingAppInfo;->avk()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Ldnr;->bDM:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Ldnr;->bDL:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agd()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Ldnr;->bDI:Ldnm;

    iget-object v1, p0, Ldnr;->bDL:Lcom/google/android/search/shared/actions/VoiceAction;

    iget-object v0, v0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0, v1}, Ldbd;->l(Lcom/google/android/search/shared/actions/VoiceAction;)V

    :goto_0
    iget-object v0, p0, Ldnr;->bDI:Ldnm;

    invoke-virtual {v0}, Ldnm;->adW()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldnr;->bDL:Lcom/google/android/search/shared/actions/VoiceAction;

    instance-of v0, v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldnr;->bDL:Lcom/google/android/search/shared/actions/VoiceAction;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajm()Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;->ajq()Ljqs;

    move-result-object v1

    invoke-virtual {v1}, Ljqs;->bss()I

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajk()I

    move-result v0

    :goto_1
    iget-object v1, p0, Ldnr;->bDI:Ldnm;

    iget-object v1, v1, Ldnm;->mActionCardEventLogger:Legs;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Legs;->aI(II)V

    :cond_1
    :goto_2
    return-void

    :cond_2
    iget-object v0, p0, Ldnr;->bDI:Ldnm;

    iget-object v1, p0, Ldnr;->bDL:Lcom/google/android/search/shared/actions/VoiceAction;

    iget-object v0, v0, Ldnm;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0, v1}, Ldbd;->n(Lcom/google/android/search/shared/actions/VoiceAction;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ldnr;->bDI:Ldnm;

    iget-object v1, p0, Ldnr;->bDL:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-virtual {v0, v1}, Ldnm;->v(Lcom/google/android/search/shared/actions/VoiceAction;)V

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1
.end method
