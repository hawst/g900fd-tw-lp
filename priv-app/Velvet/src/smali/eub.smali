.class final Leub;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private chU:J

.field final cja:Leub;

.field cjb:J

.field cjc:Ljava/util/List;

.field private mName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;JLeub;)V
    .locals 0
    .param p4    # Leub;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object p1, p0, Leub;->mName:Ljava/lang/String;

    .line 115
    iput-wide p2, p0, Leub;->chU:J

    .line 116
    iput-object p4, p0, Leub;->cja:Leub;

    .line 117
    return-void
.end method


# virtual methods
.method final a(Letj;J)V
    .locals 6

    .prologue
    .line 127
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Leub;->cjb:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Leub;->chU:J

    sub-long v2, p2, v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "+"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 128
    iget-object v1, p0, Leub;->mName:Ljava/lang/String;

    invoke-virtual {p1, v1}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 129
    iget-object v0, p0, Leub;->cjc:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Leub;->cjc:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leub;

    .line 131
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v2

    invoke-virtual {v0, v2, p2, p3}, Leub;->a(Letj;J)V

    goto :goto_1

    .line 127
    :cond_0
    iget-wide v2, p0, Leub;->cjb:J

    iget-wide v4, p0, Leub;->chU:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 134
    :cond_1
    return-void
.end method
