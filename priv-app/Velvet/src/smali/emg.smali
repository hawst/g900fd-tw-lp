.class public final Lemg;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final cfi:Ljava/util/Locale;

.field private static cfj:Ljava/util/Locale;

.field private static cfk:Ldi;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Ljava/util/Locale;

    const-string v1, "ar"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sput-object v0, Lemg;->cfi:Ljava/util/Locale;

    .line 23
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sput-object v0, Lemg;->cfj:Ljava/util/Locale;

    .line 24
    invoke-static {}, Ldi;->ak()Ldi;

    move-result-object v0

    sput-object v0, Lemg;->cfk:Ldi;

    return-void
.end method

.method public static a(DII)Ljava/lang/String;
    .locals 4

    .prologue
    .line 39
    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v0

    .line 40
    invoke-virtual {v0, p2}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 41
    invoke-virtual {v0, p3}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 43
    invoke-static {}, Lemg;->auy()Ldi;

    move-result-object v1

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    div-double v2, p0, v2

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lemg;->aux()Ldq;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ldi;->a(Ljava/lang/String;Ldq;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/text/NumberFormat;D)Ljava/lang/String;
    .locals 3

    .prologue
    .line 51
    invoke-static {}, Lemg;->auy()Ldi;

    move-result-object v0

    invoke-virtual {p0, p1, p2}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ldr;->eC:Ldq;

    invoke-virtual {v0, v1, v2}, Ldi;->a(Ljava/lang/String;Ldq;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static auw()Z
    .locals 2

    .prologue
    .line 103
    sget-object v0, Lemg;->cfi:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static aux()Ldq;
    .locals 1

    .prologue
    .line 110
    invoke-static {}, Lemg;->auw()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ldr;->eD:Ldq;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ldr;->eC:Ldq;

    goto :goto_0
.end method

.method private static auy()Ldi;
    .locals 2

    .prologue
    .line 119
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 120
    sget-object v1, Lemg;->cfj:Ljava/util/Locale;

    invoke-virtual {v1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 121
    sput-object v0, Lemg;->cfj:Ljava/util/Locale;

    .line 122
    invoke-static {v0}, Ldi;->a(Ljava/util/Locale;)Ldi;

    move-result-object v0

    sput-object v0, Lemg;->cfk:Ldi;

    .line 124
    :cond_0
    sget-object v0, Lemg;->cfk:Ldi;

    return-object v0
.end method

.method public static ih(I)Ljava/lang/String;
    .locals 6

    .prologue
    .line 73
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v0

    .line 74
    invoke-static {}, Lemg;->auy()Ldi;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lemg;->auy()Ldi;

    move-result-object v3

    int-to-long v4, p0

    invoke-virtual {v0, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ldi;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0xb0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lemg;->aux()Ldq;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ldi;->a(Ljava/lang/String;Ldq;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static le(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 141
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 142
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lemg;->auy()Ldi;

    move-result-object v0

    sget-object v1, Ldr;->eC:Ldq;

    invoke-virtual {v0, p0, v1}, Ldi;->a(Ljava/lang/String;Ldq;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static unicodeWrap(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 132
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lemg;->auy()Ldi;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldi;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
