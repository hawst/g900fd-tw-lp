.class public final Ldqz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# static fields
.field public static final bGE:Ldqz;

.field public static final bGF:Ldqz;


# instance fields
.field private bGG:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 6
    new-instance v0, Ldqz;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ldqz;-><init>(Z)V

    sput-object v0, Ldqz;->bGE:Ldqz;

    .line 7
    new-instance v0, Ldqz;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ldqz;-><init>(Z)V

    sput-object v0, Ldqz;->bGF:Ldqz;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-boolean p1, p0, Ldqz;->bGG:Z

    .line 13
    return-void
.end method


# virtual methods
.method public final getInterpolation(F)F
    .locals 2

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 17
    iget-boolean v1, p0, Ldqz;->bGG:Z

    if-eqz v1, :cond_0

    cmpl-float v1, p1, v0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
