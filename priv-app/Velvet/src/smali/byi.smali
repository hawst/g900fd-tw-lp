.class public final Lbyi;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private aOM:Z

.field private aON:Z

.field private ag:I

.field private mListener:Lbyj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lbyi;->ag:I

    .line 37
    return-void
.end method

.method private setState(I)V
    .locals 2

    .prologue
    .line 160
    iget v0, p0, Lbyi;->ag:I

    if-ne p1, v0, :cond_0

    .line 166
    :goto_0
    return-void

    .line 162
    :cond_0
    iget-object v0, p0, Lbyi;->mListener:Lbyj;

    if-eqz v0, :cond_1

    .line 163
    iget-object v0, p0, Lbyi;->mListener:Lbyj;

    iget v1, p0, Lbyi;->ag:I

    invoke-interface {v0, v1, p1}, Lbyj;->ad(II)V

    .line 165
    :cond_1
    iput p1, p0, Lbyi;->ag:I

    goto :goto_0
.end method


# virtual methods
.method public final BH()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 88
    iget-object v0, p0, Lbyi;->mListener:Lbyj;

    if-nez v0, :cond_0

    .line 110
    :goto_0
    return-void

    .line 92
    :cond_0
    iget v0, p0, Lbyi;->ag:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 95
    iget-object v0, p0, Lbyi;->mListener:Lbyj;

    invoke-interface {v0}, Lbyj;->Bp()V

    .line 96
    invoke-direct {p0, v2}, Lbyi;->setState(I)V

    .line 97
    iput-boolean v2, p0, Lbyi;->aON:Z

    goto :goto_0

    .line 98
    :cond_1
    iget v0, p0, Lbyi;->ag:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 101
    iget-object v0, p0, Lbyi;->mListener:Lbyj;

    invoke-interface {v0}, Lbyj;->Bp()V

    goto :goto_0

    .line 102
    :cond_2
    iget v0, p0, Lbyi;->ag:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 106
    iget-object v0, p0, Lbyi;->mListener:Lbyj;

    invoke-interface {v0}, Lbyj;->Bq()V

    goto :goto_0

    .line 108
    :cond_3
    iget-object v0, p0, Lbyi;->mListener:Lbyj;

    invoke-interface {v0}, Lbyj;->Bo()V

    goto :goto_0
.end method

.method public final a(Lbyj;)V
    .locals 1

    .prologue
    .line 43
    iput-object p1, p0, Lbyi;->mListener:Lbyj;

    .line 44
    if-nez p1, :cond_0

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lbyi;->ag:I

    .line 47
    :cond_0
    return-void
.end method

.method public final eq(I)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v1, 0x1

    .line 57
    and-int/lit8 v0, p1, 0x40

    if-eqz v0, :cond_2

    move v0, v1

    .line 59
    :goto_0
    iget-boolean v2, p0, Lbyi;->aOM:Z

    if-eq v0, v2, :cond_0

    .line 60
    iput-boolean v0, p0, Lbyi;->aOM:Z

    .line 61
    if-eqz v0, :cond_3

    .line 62
    iget-object v2, p0, Lbyi;->mListener:Lbyj;

    invoke-interface {v2}, Lbyj;->Br()V

    .line 68
    :cond_0
    :goto_1
    and-int/lit8 v2, p1, 0x10

    if-eqz v2, :cond_4

    .line 70
    invoke-direct {p0, v3}, Lbyi;->setState(I)V

    .line 81
    :cond_1
    :goto_2
    return-void

    .line 57
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 64
    :cond_3
    iget-object v2, p0, Lbyi;->mListener:Lbyj;

    invoke-interface {v2}, Lbyj;->Bs()V

    goto :goto_1

    .line 71
    :cond_4
    if-nez v0, :cond_6

    iget v0, p0, Lbyi;->ag:I

    const/4 v2, 0x3

    if-eq v0, v2, :cond_5

    iget v0, p0, Lbyi;->ag:I

    if-eq v0, v3, :cond_5

    iget v0, p0, Lbyi;->ag:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_6

    .line 77
    :cond_5
    invoke-direct {p0, v1}, Lbyi;->setState(I)V

    goto :goto_2

    .line 78
    :cond_6
    iget v0, p0, Lbyi;->ag:I

    if-nez v0, :cond_1

    .line 79
    invoke-direct {p0, v1}, Lbyi;->setState(I)V

    goto :goto_2
.end method

.method public final er(I)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v1, 0x1

    .line 120
    packed-switch p1, :pswitch_data_0

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unknown state: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    packed-switch p1, :pswitch_data_1

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 123
    :pswitch_0
    iget-boolean v0, p0, Lbyi;->aON:Z

    if-eqz v0, :cond_1

    .line 124
    invoke-direct {p0, v1}, Lbyi;->setState(I)V

    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbyi;->aON:Z

    goto :goto_0

    .line 127
    :cond_1
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lbyi;->setState(I)V

    goto :goto_0

    .line 131
    :pswitch_1
    invoke-direct {p0, v2}, Lbyi;->setState(I)V

    goto :goto_0

    .line 134
    :pswitch_2
    iget v0, p0, Lbyi;->ag:I

    if-ne v0, v2, :cond_0

    .line 135
    invoke-direct {p0, v3}, Lbyi;->setState(I)V

    goto :goto_0

    .line 142
    :pswitch_3
    iget v0, p0, Lbyi;->ag:I

    if-eq v0, v4, :cond_0

    iget v0, p0, Lbyi;->ag:I

    if-eq v0, v3, :cond_0

    .line 143
    invoke-direct {p0, v1}, Lbyi;->setState(I)V

    goto :goto_0

    .line 148
    :pswitch_4
    iget v0, p0, Lbyi;->ag:I

    if-eq v0, v4, :cond_0

    .line 150
    invoke-direct {p0, v1}, Lbyi;->setState(I)V

    goto :goto_0

    .line 154
    :pswitch_5
    const-string v0, "STATE_UNDEFINED"

    goto :goto_0

    :pswitch_6
    const-string v0, "STATE_INITIALIZING"

    goto :goto_0

    :pswitch_7
    const-string v0, "STATE_NOT_LISTENING"

    goto :goto_0

    :pswitch_8
    const-string v0, "STATE_LISTENING"

    goto :goto_0

    :pswitch_9
    const-string v0, "STATE_RECORDING"

    goto :goto_0

    :pswitch_a
    const-string v0, "STATE_RECOGNIZING"

    goto :goto_0

    :pswitch_b
    const-string v0, "STATE_NON_RECOGNITION"

    goto :goto_0

    .line 120
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 154
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_7
        :pswitch_b
    .end packed-switch
.end method
