.class public interface abstract Ldvn;
.super Ljava/lang/Object;
.source "PG"


# virtual methods
.method public abstract a(Lcom/google/android/search/shared/actions/AddEventAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/AddRelationshipAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/AgendaAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/ButtonAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/ContactOptInAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/EmailAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/HelpAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/LocalResultsAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/MediaControlAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/MessageSearchAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/OpenUrlAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/PhoneCallAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/PlayMediaAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/PuntAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/ReadNotificationAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/RemoveRelationshipAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/SelfNoteAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/SetAlarmAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/SetReminderAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/SetTimerAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/ShowContactInformationAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/SmsAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/SocialUpdateAction;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/google/android/search/shared/actions/modular/ModularAction;)Ljava/lang/Object;
.end method

.method public abstract b(Lcom/google/android/search/shared/actions/errors/SearchError;)Ljava/lang/Object;
.end method
