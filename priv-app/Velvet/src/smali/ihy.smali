.class abstract Lihy;
.super Lihx;
.source "PG"

# interfaces
.implements Lirq;


# static fields
.field private static final serialVersionUID:J = 0x5faae81de71c4a4L


# direct methods
.method protected constructor <init>(Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lihx;-><init>(Ljava/util/Map;)V

    .line 46
    return-void
.end method


# virtual methods
.method synthetic aWC()Ljava/util/Set;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lihy;->aWE()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method abstract aWE()Ljava/util/SortedSet;
.end method

.method synthetic aWm()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lihy;->aWE()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public aWp()Ljava/util/Map;
    .locals 1

    .prologue
    .line 110
    invoke-super {p0}, Lihx;->aWp()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic bk(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lihy;->bq(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic bl(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lihy;->bp(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic bn(Ljava/lang/Object;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lihy;->bp(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic bo(Ljava/lang/Object;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lihy;->bq(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public bp(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 65
    invoke-super {p0, p1}, Lihx;->bn(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    return-object v0
.end method

.method public bq(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 77
    invoke-super {p0, p1}, Lihx;->bo(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    return-object v0
.end method

.method public values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 120
    invoke-super {p0}, Lihx;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
