.class public final Lcgi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcgh;


# instance fields
.field private final aVQ:Ldgc;

.field private aVR:Ldio;

.field private aVS:Ldhu;

.field private aVT:Leeo;

.field private aVU:Ldgd;

.field private aVV:Ldet;

.field private aVW:Lckq;

.field private aVX:Lcrr;

.field private aVY:Ldhd;

.field private aVZ:Lepo;

.field private final aqp:Ljava/lang/Object;

.field private mActionDiscoverySource:Lddl;

.field private final mAsyncServices:Lema;

.field private final mContext:Landroid/content/Context;

.field private final mCoreServices:Lcfo;

.field private mShouldQueryStrategy:Ldfz;

.field private mSources:Ldgh;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcfo;Lema;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lcgi;->mContext:Landroid/content/Context;

    .line 67
    iput-object p2, p0, Lcgi;->mCoreServices:Lcfo;

    .line 68
    iput-object p3, p0, Lcgi;->mAsyncServices:Lema;

    .line 69
    new-instance v0, Ldgc;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldgc;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcgi;->aVQ:Ldgc;

    .line 70
    iput-object p4, p0, Lcgi;->aqp:Ljava/lang/Object;

    .line 71
    return-void
.end method

.method private EX()Ldio;
    .locals 4

    .prologue
    .line 74
    iget-object v0, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DD()Lcjs;

    move-result-object v0

    invoke-virtual {v0}, Lcjs;->Mi()Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    const/4 v0, 0x0

    .line 81
    :goto_0
    return-object v0

    .line 77
    :cond_0
    iget-object v0, p0, Lcgi;->aVR:Ldio;

    if-nez v0, :cond_1

    .line 78
    new-instance v0, Ldio;

    iget-object v1, p0, Lcgi;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcgi;->mCoreServices:Lcfo;

    iget-object v3, p0, Lcgi;->mAsyncServices:Lema;

    invoke-direct {v0, v1, v2, v3}, Ldio;-><init>(Landroid/content/Context;Lcfo;Lema;)V

    iput-object v0, p0, Lcgi;->aVR:Ldio;

    .line 81
    :cond_1
    iget-object v0, p0, Lcgi;->aVR:Ldio;

    goto :goto_0
.end method

.method private EY()Ldhu;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 93
    iget-object v0, p0, Lcgi;->aVS:Ldhu;

    if-nez v0, :cond_1

    .line 94
    invoke-direct {p0}, Lcgi;->EZ()Ldhd;

    move-result-object v1

    iget-object v0, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v4

    iget-object v0, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v5

    iget-object v0, p0, Lcgi;->aVQ:Ldgc;

    new-instance v3, Ldhv;

    iget-object v2, v1, Ldhd;->mContext:Landroid/content/Context;

    iget-object v6, v1, Ldhd;->mConfig:Lcjs;

    invoke-direct {v3, v2, v6, v0, v4}, Ldhv;-><init>(Landroid/content/Context;Lcjs;Ldgc;Lcke;)V

    new-instance v6, Ldib;

    iget-object v2, v1, Ldhd;->bxH:Ldgm;

    invoke-direct {v6, v5, v2, v0, v4}, Ldib;-><init>(Lchk;Ldgm;Ldgc;Lcke;)V

    new-instance v2, Ldhw;

    iget-object v0, v1, Ldhd;->mContext:Landroid/content/Context;

    invoke-direct {v2, v6, v0}, Ldhw;-><init>(Ldib;Landroid/content/Context;)V

    iget-object v0, v1, Ldhd;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->ML()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "Search.IcingFactory"

    const-string v1, "All icing sources disabled"

    new-array v3, v7, [Ljava/lang/Object;

    const/4 v4, 0x4

    invoke-static {v4, v0, v1, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    iput-object v2, p0, Lcgi;->aVS:Ldhu;

    .line 97
    :cond_1
    iget-object v0, p0, Lcgi;->aVS:Ldhu;

    return-object v0

    .line 94
    :cond_2
    iget-object v6, v1, Ldhd;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Ldhh;

    invoke-direct/range {v0 .. v5}, Ldhh;-><init>(Ldhd;Ldhu;Ldhv;Lcke;Lchk;)V

    invoke-static {v6, v0}, Lemz;->a(Ljava/util/concurrent/Executor;Lemy;)Lemy;

    move-result-object v0

    iget-object v5, v1, Ldhd;->mGooglePlayServicesHelper:Lcha;

    new-instance v6, Ldhj;

    invoke-direct {v6, v1, v2, v0}, Ldhj;-><init>(Ldhd;Ldhu;Lemy;)V

    invoke-virtual {v5, v6}, Lcha;->a(Lchg;)V

    iget-object v5, v1, Ldhd;->mGooglePlayServicesHelper:Lcha;

    new-instance v6, Ldhk;

    invoke-direct {v6, v1, v2, v0}, Ldhk;-><init>(Ldhd;Ldhu;Lemy;)V

    invoke-virtual {v5, v6}, Lcha;->b(Lemy;)V

    new-instance v5, Ldhl;

    invoke-direct {v5, v1, v2, v3, v0}, Ldhl;-><init>(Ldhd;Ldhu;Ldhv;Lemy;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v3, "com.google.android.gms.icing.GlobalSearchAppRegistered3"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.google.android.gms.icing.GlobalSearchableAppUnRegistered"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, v1, Ldhd;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v5, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-interface {v4}, Lcke;->Oj()Ljjf;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, v1, Ldhd;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v4, Ldhm;

    invoke-direct {v4, v1, v0, v7}, Ldhm;-><init>(Ldhd;Ljjf;Z)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private EZ()Ldhd;
    .locals 7

    .prologue
    .line 118
    iget-object v0, p0, Lcgi;->aVY:Ldhd;

    if-nez v0, :cond_0

    .line 119
    new-instance v0, Ldhd;

    iget-object v1, p0, Lcgi;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcgi;->mAsyncServices:Lema;

    invoke-virtual {v2}, Lema;->aus()Leqo;

    move-result-object v2

    iget-object v3, p0, Lcgi;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    iget-object v4, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->DD()Lcjs;

    move-result-object v4

    iget-object v5, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->BL()Lchk;

    move-result-object v5

    iget-object v6, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v6}, Lcfo;->Em()Lcha;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Ldhd;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcjs;Lchk;Lcha;)V

    iput-object v0, p0, Lcgi;->aVY:Ldhd;

    .line 126
    :cond_0
    iget-object v0, p0, Lcgi;->aVY:Ldhd;

    return-object v0
.end method

.method private Fa()Lepo;
    .locals 3

    .prologue
    .line 207
    iget-object v0, p0, Lcgi;->aVZ:Lepo;

    if-nez v0, :cond_0

    .line 208
    iget-object v0, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DD()Lcjs;

    invoke-static {}, Lcjs;->Lm()I

    .line 209
    new-instance v0, Leoa;

    const-string v1, "SourceTaskExecutor"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Leoa;-><init>(Ljava/lang/String;I)V

    .line 210
    new-instance v1, Lepz;

    new-instance v2, Leqr;

    invoke-direct {v2, v0}, Leqr;-><init>(Ljava/util/concurrent/ThreadFactory;)V

    invoke-direct {v1, v2}, Lepz;-><init>(Lenx;)V

    iput-object v1, p0, Lcgi;->aVZ:Lepo;

    .line 213
    :cond_0
    iget-object v0, p0, Lcgi;->aVZ:Lepo;

    return-object v0
.end method


# virtual methods
.method public final EN()Ldgh;
    .locals 4

    .prologue
    .line 86
    iget-object v0, p0, Lcgi;->mSources:Ldgh;

    if-nez v0, :cond_0

    .line 87
    invoke-direct {p0}, Lcgi;->EX()Ldio;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcgi;->EX()Ldio;

    move-result-object v0

    invoke-direct {p0}, Lcgi;->EY()Ldhu;

    move-result-object v1

    iget-object v2, p0, Lcgi;->aVQ:Ldgc;

    invoke-virtual {p0}, Lcgi;->ES()Lcks;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Ldio;->a(Ldhu;Ldgc;Lcks;)Ldii;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcgi;->mSources:Ldgh;

    .line 89
    :cond_0
    iget-object v0, p0, Lcgi;->mSources:Ldgh;

    return-object v0

    .line 87
    :cond_1
    invoke-direct {p0}, Lcgi;->EY()Ldhu;

    move-result-object v0

    goto :goto_0
.end method

.method public final EO()Leeo;
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Lcgi;->aVT:Leeo;

    if-nez v0, :cond_0

    .line 142
    new-instance v0, Leel;

    new-instance v1, Lekz;

    iget-object v2, p0, Lcgi;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lekz;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, Leel;-><init>(Lekz;)V

    iput-object v0, p0, Lcgi;->aVT:Leeo;

    .line 146
    :cond_0
    iget-object v0, p0, Lcgi;->aVT:Leeo;

    return-object v0
.end method

.method public final EP()Ldfz;
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcgi;->mShouldQueryStrategy:Ldfz;

    if-nez v0, :cond_0

    .line 219
    new-instance v0, Ldfz;

    iget-object v1, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->BL()Lchk;

    move-result-object v1

    invoke-direct {v0, v1}, Ldfz;-><init>(Lchk;)V

    iput-object v0, p0, Lcgi;->mShouldQueryStrategy:Ldfz;

    .line 221
    :cond_0
    iget-object v0, p0, Lcgi;->mShouldQueryStrategy:Ldfz;

    return-object v0
.end method

.method public final EQ()Ldgd;
    .locals 4

    .prologue
    .line 230
    iget-object v0, p0, Lcgi;->aVU:Ldgd;

    if-nez v0, :cond_0

    .line 231
    new-instance v0, Ldgd;

    invoke-virtual {p0}, Lcgi;->EN()Ldgh;

    move-result-object v1

    iget-object v2, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->BK()Lcke;

    move-result-object v2

    invoke-virtual {p0}, Lcgi;->ES()Lcks;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Ldgd;-><init>(Ldgh;Lcke;Lcks;)V

    iput-object v0, p0, Lcgi;->aVU:Ldgd;

    .line 236
    :cond_0
    iget-object v0, p0, Lcgi;->aVU:Ldgd;

    return-object v0
.end method

.method public final ER()Ldet;
    .locals 14

    .prologue
    .line 165
    iget-object v0, p0, Lcgi;->aVV:Ldet;

    if-nez v0, :cond_0

    .line 166
    invoke-direct {p0}, Lcgi;->EX()Ldio;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcgi;->EX()Ldio;

    move-result-object v10

    invoke-virtual {p0}, Lcgi;->EP()Ldfz;

    move-result-object v2

    invoke-direct {p0}, Lcgi;->EY()Ldhu;

    move-result-object v8

    invoke-direct {p0}, Lcgi;->Fa()Lepo;

    move-result-object v5

    iget-object v11, p0, Lcgi;->aVQ:Ldgc;

    invoke-virtual {p0}, Lcgi;->ES()Lcks;

    move-result-object v12

    new-instance v0, Ldje;

    iget-object v1, v10, Ldio;->mContext:Landroid/content/Context;

    iget-object v3, v10, Ldio;->mCoreServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DD()Lcjs;

    move-result-object v3

    iget-object v4, v10, Ldio;->mCoreServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->BL()Lchk;

    move-result-object v4

    iget-object v6, v10, Ldio;->mAsyncServices:Lema;

    invoke-virtual {v6}, Lema;->aus()Leqo;

    move-result-object v6

    new-instance v7, Ldir;

    iget-object v9, v10, Ldio;->mCoreServices:Lcfo;

    invoke-virtual {v9}, Lcfo;->BL()Lchk;

    move-result-object v9

    new-instance v13, Ldjp;

    invoke-direct {v13}, Ldjp;-><init>()V

    invoke-direct {v7, v9, v2, v13}, Ldir;-><init>(Lchk;Ldfz;Ldjp;)V

    new-instance v9, Ldgd;

    invoke-virtual {v10, v8, v11, v12}, Ldio;->a(Ldhu;Ldgc;Lcks;)Ldii;

    move-result-object v11

    iget-object v11, v11, Ldii;->byO:Ldij;

    iget-object v13, v10, Ldio;->mCoreServices:Lcfo;

    invoke-virtual {v13}, Lcfo;->BK()Lcke;

    move-result-object v13

    invoke-direct {v9, v11, v13, v12}, Ldgd;-><init>(Ldgh;Lcke;Lcks;)V

    iget-object v10, v10, Ldio;->mCoreServices:Lcfo;

    invoke-virtual {v10}, Lcfo;->Eu()Lddv;

    move-result-object v10

    invoke-direct/range {v0 .. v10}, Ldje;-><init>(Landroid/content/Context;Ldfz;Lcjs;Lchk;Lepo;Leqo;Ldir;Ldhu;Ldgd;Lddv;)V

    move-object v5, v0

    :goto_0
    new-instance v6, Lddp;

    iget-object v0, p0, Lcgi;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcgi;->Fa()Lepo;

    move-result-object v1

    iget-object v2, p0, Lcgi;->mAsyncServices:Lema;

    invoke-virtual {v2}, Lema;->aus()Leqo;

    move-result-object v2

    iget-object v3, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->Eu()Lddv;

    move-result-object v3

    invoke-direct {v6, v0, v1, v2, v3}, Lddp;-><init>(Landroid/content/Context;Lepo;Leqo;Lddv;)V

    new-instance v0, Ldet;

    invoke-virtual {p0}, Lcgi;->EP()Ldfz;

    move-result-object v1

    invoke-direct {p0}, Lcgi;->Fa()Lepo;

    move-result-object v2

    iget-object v3, p0, Lcgi;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->aus()Leqo;

    move-result-object v3

    iget-object v4, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->DF()Lcin;

    move-result-object v4

    invoke-virtual {p0}, Lcgi;->EV()Lddl;

    move-result-object v7

    iget-object v8, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v8}, Lcfo;->BL()Lchk;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Ldet;-><init>(Ldfz;Lepo;Leqo;Lcin;Ldgi;Lddp;Lddl;Lchk;)V

    iput-object v0, p0, Lcgi;->aVV:Ldet;

    .line 168
    :cond_0
    iget-object v0, p0, Lcgi;->aVV:Ldet;

    return-object v0

    .line 166
    :cond_1
    new-instance v0, Ldhn;

    iget-object v1, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DD()Lcjs;

    move-result-object v1

    iget-object v2, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->BL()Lchk;

    move-result-object v2

    invoke-virtual {p0}, Lcgi;->EP()Ldfz;

    move-result-object v3

    invoke-direct {p0}, Lcgi;->Fa()Lepo;

    move-result-object v4

    iget-object v5, p0, Lcgi;->mAsyncServices:Lema;

    invoke-virtual {v5}, Lema;->aus()Leqo;

    move-result-object v5

    invoke-direct {p0}, Lcgi;->EY()Ldhu;

    move-result-object v6

    iget-object v7, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v7}, Lcfo;->Eu()Lddv;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Ldhn;-><init>(Lcjs;Lchk;Ldfz;Lepo;Leqo;Ldhu;Lddv;)V

    move-object v5, v0

    goto :goto_0
.end method

.method public final ES()Lcks;
    .locals 5

    .prologue
    .line 241
    iget-object v0, p0, Lcgi;->aVW:Lckq;

    if-nez v0, :cond_0

    .line 242
    new-instance v0, Lckq;

    iget-object v1, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DD()Lcjs;

    move-result-object v1

    iget-object v2, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->BK()Lcke;

    move-result-object v2

    iget-object v3, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DC()Lemp;

    move-result-object v3

    iget-object v4, p0, Lcgi;->mAsyncServices:Lema;

    invoke-virtual {v4}, Lema;->aut()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lckq;-><init>(Lcjs;Lcke;Lemp;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcgi;->aVW:Lckq;

    .line 247
    :cond_0
    iget-object v0, p0, Lcgi;->aVW:Lckq;

    return-object v0
.end method

.method public final ET()Ldgm;
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Lcgi;->EZ()Ldhd;

    move-result-object v0

    iget-object v0, v0, Ldhd;->bxH:Ldgm;

    return-object v0
.end method

.method public final EU()Lcrr;
    .locals 9

    .prologue
    .line 252
    iget-object v8, p0, Lcgi;->aqp:Ljava/lang/Object;

    monitor-enter v8

    .line 253
    :try_start_0
    iget-object v0, p0, Lcgi;->aVX:Lcrr;

    if-nez v0, :cond_0

    .line 254
    new-instance v0, Lcrr;

    iget-object v1, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DG()Ldkx;

    move-result-object v1

    iget-object v2, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->DI()Lcpn;

    move-result-object v2

    iget-object v3, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DL()Lcrh;

    move-result-object v3

    iget-object v4, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->DD()Lcjs;

    move-result-object v4

    iget-object v5, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->BL()Lchk;

    move-result-object v5

    iget-object v6, p0, Lcgi;->mAsyncServices:Lema;

    invoke-virtual {v6}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v6

    iget-object v7, p0, Lcgi;->mAsyncServices:Lema;

    invoke-virtual {v7}, Lema;->aus()Leqo;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcrr;-><init>(Ldkx;Lcpn;Lcrh;Lcjs;Lchk;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcgi;->aVX:Lcrr;

    .line 256
    :cond_0
    iget-object v0, p0, Lcgi;->aVX:Lcrr;

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 257
    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0
.end method

.method public final EV()Lddl;
    .locals 3

    .prologue
    .line 151
    iget-object v0, p0, Lcgi;->mActionDiscoverySource:Lddl;

    if-nez v0, :cond_0

    .line 152
    new-instance v0, Lddl;

    iget-object v1, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->BK()Lcke;

    move-result-object v1

    iget-object v2, p0, Lcgi;->mAsyncServices:Lema;

    invoke-virtual {v2}, Lema;->aut()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lddl;-><init>(Lcke;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcgi;->mActionDiscoverySource:Lddl;

    .line 155
    :cond_0
    iget-object v0, p0, Lcgi;->mActionDiscoverySource:Lddl;

    return-object v0
.end method

.method public final EW()V
    .locals 4

    .prologue
    .line 107
    iget-object v0, p0, Lcgi;->aVY:Ldhd;

    .line 108
    iget-object v1, p0, Lcgi;->aVS:Ldhu;

    .line 110
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 111
    iget-object v2, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->BK()Lcke;

    move-result-object v2

    iget-object v3, p0, Lcgi;->mCoreServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->BL()Lchk;

    move-result-object v3

    iget-object v0, v0, Ldhd;->bxH:Ldgm;

    invoke-interface {v1, v2, v3, v0}, Ldhu;->a(Lcke;Lchk;Ldgm;)V

    .line 115
    :cond_0
    return-void
.end method

.method public final a(Letj;)V
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcgi;->aVY:Ldhd;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 270
    iget-object v0, p0, Lcgi;->mSources:Ldgh;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 271
    return-void
.end method
