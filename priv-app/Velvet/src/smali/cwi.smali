.class public final Lcwi;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field private blS:Landroid/content/DialogInterface$OnDismissListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 27
    invoke-virtual {p0}, Lcwi;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "summaryId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 28
    invoke-virtual {p0}, Lcwi;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "settingKey"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 30
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcwi;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 31
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v3

    invoke-virtual {v3}, Lgql;->aJs()Lchr;

    move-result-object v3

    invoke-virtual {v3}, Lchr;->Kt()Lcyg;

    move-result-object v3

    .line 33
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v4, 0x104000a

    new-instance v5, Lcwk;

    invoke-direct {v5, p0, v3, v1}, Lcwk;-><init>(Lcwi;Landroid/content/SharedPreferences;Ljava/lang/String;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v4, 0x1040009

    new-instance v5, Lcwj;

    invoke-direct {v5, p0, v3, v1}, Lcwj;-><init>(Lcwi;Landroid/content/SharedPreferences;Ljava/lang/String;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 50
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcwi;->blS:Landroid/content/DialogInterface$OnDismissListener;

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnDismissListener;->onDismiss(Landroid/content/DialogInterface;)V

    .line 60
    return-void
.end method

.method public final setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcwi;->blS:Landroid/content/DialogInterface$OnDismissListener;

    .line 55
    return-void
.end method
