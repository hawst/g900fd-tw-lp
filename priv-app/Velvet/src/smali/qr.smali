.class final Lqr;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final US_ASCII:Ljava/nio/charset/Charset;

.field private static final vQ:S

.field private static final vR:S

.field private static final vS:S

.field private static final vT:S

.field private static final vU:S

.field private static final vV:S

.field private static final vW:S


# instance fields
.field private final vA:Lqn;

.field private final vB:I

.field private vC:I

.field private vD:I

.field private vE:I

.field private vF:Lqw;

.field private vG:Lqu;

.field private vH:Lqw;

.field private vI:Lqw;

.field private vJ:Z

.field private vK:Z

.field private vL:I

.field private vM:[B

.field private vN:I

.field private vO:I

.field private final vP:Lqp;

.field private final vX:Ljava/util/TreeMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 142
    const-string v0, "US-ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lqr;->US_ASCII:Ljava/nio/charset/Charset;

    .line 165
    sget v0, Lqp;->tH:I

    invoke-static {v0}, Lqp;->aB(I)S

    move-result v0

    sput-short v0, Lqr;->vQ:S

    .line 167
    sget v0, Lqp;->tI:I

    invoke-static {v0}, Lqp;->aB(I)S

    move-result v0

    sput-short v0, Lqr;->vR:S

    .line 168
    sget v0, Lqp;->ur:I

    invoke-static {v0}, Lqp;->aB(I)S

    move-result v0

    sput-short v0, Lqr;->vS:S

    .line 170
    sget v0, Lqp;->tJ:I

    invoke-static {v0}, Lqp;->aB(I)S

    move-result v0

    sput-short v0, Lqr;->vT:S

    .line 172
    sget v0, Lqp;->tK:I

    invoke-static {v0}, Lqp;->aB(I)S

    move-result v0

    sput-short v0, Lqr;->vU:S

    .line 174
    sget v0, Lqp;->tn:I

    invoke-static {v0}, Lqp;->aB(I)S

    move-result v0

    sput-short v0, Lqr;->vV:S

    .line 176
    sget v0, Lqp;->tr:I

    invoke-static {v0}, Lqp;->aB(I)S

    move-result v0

    sput-short v0, Lqr;->vW:S

    return-void
.end method

.method private constructor <init>(Ljava/io/InputStream;ILqp;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    iput v4, p0, Lqr;->vC:I

    .line 149
    iput v4, p0, Lqr;->vD:I

    .line 157
    iput-boolean v4, p0, Lqr;->vK:Z

    .line 159
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lqr;->vX:Ljava/util/TreeMap;

    .line 203
    if-nez p1, :cond_0

    .line 204
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Null argument inputStream to ExifParser"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :cond_0
    iput-object p3, p0, Lqr;->vP:Lqp;

    .line 210
    invoke-direct {p0, p1}, Lqr;->b(Ljava/io/InputStream;)Z

    move-result v0

    iput-boolean v0, p0, Lqr;->vK:Z

    .line 211
    new-instance v0, Lqn;

    invoke-direct {v0, p1}, Lqn;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lqr;->vA:Lqn;

    .line 212
    const/16 v0, 0x3f

    iput v0, p0, Lqr;->vB:I

    .line 213
    iget-boolean v0, p0, Lqr;->vK:Z

    if-nez v0, :cond_2

    .line 231
    :cond_1
    :goto_0
    return-void

    .line 217
    :cond_2
    iget-object v0, p0, Lqr;->vA:Lqn;

    invoke-virtual {v0}, Lqn;->readShort()S

    move-result v0

    const/16 v1, 0x4949

    if-ne v1, v0, :cond_3

    iget-object v0, p0, Lqr;->vA:Lqn;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Lqn;->a(Ljava/nio/ByteOrder;)V

    :goto_1
    iget-object v0, p0, Lqr;->vA:Lqn;

    invoke-virtual {v0}, Lqn;->readShort()S

    move-result v0

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_5

    new-instance v0, Lqq;

    const-string v1, "Invalid TIFF header"

    invoke-direct {v0, v1}, Lqq;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    const/16 v1, 0x4d4d

    if-ne v1, v0, :cond_4

    iget-object v0, p0, Lqr;->vA:Lqn;

    sget-object v1, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Lqn;->a(Ljava/nio/ByteOrder;)V

    goto :goto_1

    :cond_4
    new-instance v0, Lqq;

    const-string v1, "Invalid TIFF header"

    invoke-direct {v0, v1}, Lqq;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218
    :cond_5
    iget-object v0, p0, Lqr;->vA:Lqn;

    invoke-virtual {v0}, Lqn;->dm()J

    move-result-wide v0

    .line 219
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_6

    .line 220
    new-instance v2, Lqq;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid offset "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lqq;-><init>(Ljava/lang/String;)V

    throw v2

    .line 222
    :cond_6
    long-to-int v2, v0

    iput v2, p0, Lqr;->vN:I

    .line 223
    iput v4, p0, Lqr;->vE:I

    .line 224
    invoke-direct {p0, v4}, Lqr;->aD(I)Z

    move-result v2

    if-nez v2, :cond_7

    invoke-direct {p0}, Lqr;->dq()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 225
    :cond_7
    invoke-direct {p0, v4, v0, v1}, Lqr;->a(IJ)V

    .line 226
    const-wide/16 v2, 0x8

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 227
    long-to-int v0, v0

    add-int/lit8 v0, v0, -0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lqr;->vM:[B

    .line 228
    iget-object v0, p0, Lqr;->vM:[B

    invoke-virtual {p0, v0}, Lqr;->read([B)I

    goto :goto_0
.end method

.method protected static a(Ljava/io/InputStream;Lqp;)Lqr;
    .locals 2

    .prologue
    .line 254
    new-instance v0, Lqr;

    const/16 v1, 0x3f

    invoke-direct {v0, p0, v1, p1}, Lqr;-><init>(Ljava/io/InputStream;ILqp;)V

    return-object v0
.end method

.method private a(IJ)V
    .locals 4

    .prologue
    .line 521
    iget-object v0, p0, Lqr;->vX:Ljava/util/TreeMap;

    long-to-int v1, p2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lqt;

    invoke-direct {p0, p1}, Lqr;->aD(I)Z

    move-result v3

    invoke-direct {v2, p1, v3}, Lqt;-><init>(IZ)V

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    return-void
.end method

.method private aD(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 182
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 194
    :cond_0
    :goto_0
    return v0

    .line 184
    :pswitch_0
    iget v2, p0, Lqr;->vB:I

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 186
    :pswitch_1
    iget v2, p0, Lqr;->vB:I

    and-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 188
    :pswitch_2
    iget v2, p0, Lqr;->vB:I

    and-int/lit8 v2, v2, 0x4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 190
    :pswitch_3
    iget v2, p0, Lqr;->vB:I

    and-int/lit8 v2, v2, 0x8

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 192
    :pswitch_4
    iget v2, p0, Lqr;->vB:I

    and-int/lit8 v2, v2, 0x10

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private aE(I)V
    .locals 4

    .prologue
    .line 497
    iget-object v0, p0, Lqr;->vA:Lqn;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lqn;->d(J)V

    .line 498
    :goto_0
    iget-object v0, p0, Lqr;->vX:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lqr;->vX:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v0, p1, :cond_0

    .line 499
    iget-object v0, p0, Lqr;->vX:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->pollFirstEntry()Ljava/util/Map$Entry;

    goto :goto_0

    .line 501
    :cond_0
    return-void
.end method

.method private b(IJ)V
    .locals 4

    .prologue
    .line 529
    iget-object v0, p0, Lqr;->vX:Ljava/util/TreeMap;

    long-to-int v1, p2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lqu;

    const/4 v3, 0x4

    invoke-direct {v2, v3, p1}, Lqu;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 531
    return-void
.end method

.method private b(Lqw;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x3

    const/4 v0, 0x0

    .line 587
    invoke-virtual {p1}, Lqw;->dB()I

    move-result v1

    if-nez v1, :cond_1

    .line 635
    :cond_0
    :goto_0
    return-void

    .line 590
    :cond_1
    invoke-virtual {p1}, Lqw;->dz()S

    move-result v1

    .line 591
    invoke-virtual {p1}, Lqw;->dy()I

    move-result v2

    .line 592
    sget-short v3, Lqr;->vQ:S

    if-ne v1, v3, :cond_3

    sget v3, Lqp;->tH:I

    invoke-direct {p0, v2, v3}, Lqr;->w(II)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 593
    invoke-direct {p0, v5}, Lqr;->aD(I)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0, v4}, Lqr;->aD(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 595
    :cond_2
    invoke-virtual {p1, v0}, Lqw;->aI(I)J

    move-result-wide v0

    invoke-direct {p0, v5, v0, v1}, Lqr;->a(IJ)V

    goto :goto_0

    .line 597
    :cond_3
    sget-short v3, Lqr;->vR:S

    if-ne v1, v3, :cond_4

    sget v3, Lqp;->tI:I

    invoke-direct {p0, v2, v3}, Lqr;->w(II)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 598
    invoke-direct {p0, v6}, Lqr;->aD(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 599
    invoke-virtual {p1, v0}, Lqw;->aI(I)J

    move-result-wide v0

    invoke-direct {p0, v6, v0, v1}, Lqr;->a(IJ)V

    goto :goto_0

    .line 601
    :cond_4
    sget-short v3, Lqr;->vS:S

    if-ne v1, v3, :cond_5

    sget v3, Lqp;->ur:I

    invoke-direct {p0, v2, v3}, Lqr;->w(II)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 603
    invoke-direct {p0, v4}, Lqr;->aD(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 604
    invoke-virtual {p1, v0}, Lqw;->aI(I)J

    move-result-wide v0

    invoke-direct {p0, v4, v0, v1}, Lqr;->a(IJ)V

    goto :goto_0

    .line 606
    :cond_5
    sget-short v3, Lqr;->vT:S

    if-ne v1, v3, :cond_6

    sget v3, Lqp;->tJ:I

    invoke-direct {p0, v2, v3}, Lqr;->w(II)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 608
    invoke-direct {p0}, Lqr;->do()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 609
    invoke-virtual {p1, v0}, Lqw;->aI(I)J

    move-result-wide v0

    iget-object v2, p0, Lqr;->vX:Ljava/util/TreeMap;

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lqu;

    invoke-direct {v1, v4}, Lqu;-><init>(I)V

    invoke-virtual {v2, v0, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 611
    :cond_6
    sget-short v3, Lqr;->vU:S

    if-ne v1, v3, :cond_7

    sget v3, Lqp;->tK:I

    invoke-direct {p0, v2, v3}, Lqr;->w(II)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 613
    invoke-direct {p0}, Lqr;->do()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614
    iput-object p1, p0, Lqr;->vI:Lqw;

    goto/16 :goto_0

    .line 616
    :cond_7
    sget-short v3, Lqr;->vV:S

    if-ne v1, v3, :cond_9

    sget v3, Lqp;->tn:I

    invoke-direct {p0, v2, v3}, Lqr;->w(II)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 617
    invoke-direct {p0}, Lqr;->do()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 618
    invoke-virtual {p1}, Lqw;->hasValue()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 619
    :goto_1
    invoke-virtual {p1}, Lqw;->dB()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 620
    invoke-virtual {p1}, Lqw;->dA()S

    .line 621
    invoke-virtual {p1, v0}, Lqw;->aI(I)J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lqr;->b(IJ)V

    .line 619
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 627
    :cond_8
    iget-object v1, p0, Lqr;->vX:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lqw;->getOffset()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lqs;

    invoke-direct {v3, p1, v0}, Lqs;-><init>(Lqw;Z)V

    invoke-virtual {v1, v2, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 630
    :cond_9
    sget-short v0, Lqr;->vW:S

    if-ne v1, v0, :cond_0

    sget v0, Lqp;->tr:I

    invoke-direct {p0, v2, v0}, Lqr;->w(II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lqr;->do()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lqw;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633
    iput-object p1, p0, Lqr;->vH:Lqw;

    goto/16 :goto_0
.end method

.method private b(Ljava/io/InputStream;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 754
    new-instance v4, Lqn;

    invoke-direct {v4, p1}, Lqn;-><init>(Ljava/io/InputStream;)V

    .line 755
    invoke-virtual {v4}, Lqn;->readShort()S

    move-result v2

    const/16 v3, -0x28

    if-eq v2, v3, :cond_0

    .line 756
    new-instance v0, Lqq;

    const-string v1, "Invalid JPEG format"

    invoke-direct {v0, v1}, Lqq;-><init>(Ljava/lang/String;)V

    throw v0

    .line 759
    :cond_0
    invoke-virtual {v4}, Lqn;->readShort()S

    move-result v2

    move v3, v2

    .line 761
    :goto_0
    const/16 v2, -0x27

    if-eq v3, v2, :cond_5

    const/16 v2, -0x40

    if-lt v3, v2, :cond_1

    const/16 v2, -0x31

    if-gt v3, v2, :cond_1

    const/16 v2, -0x3c

    if-eq v3, v2, :cond_1

    const/16 v2, -0x38

    if-eq v3, v2, :cond_1

    const/16 v2, -0x34

    if-eq v3, v2, :cond_1

    move v2, v0

    :goto_1
    if-nez v2, :cond_5

    .line 762
    invoke-virtual {v4}, Lqn;->readUnsignedShort()I

    move-result v2

    .line 765
    const/16 v5, -0x1f

    if-ne v3, v5, :cond_2

    .line 766
    const/16 v3, 0x8

    if-lt v2, v3, :cond_2

    .line 769
    invoke-virtual {v4}, Lqn;->readInt()I

    move-result v3

    .line 770
    invoke-virtual {v4}, Lqn;->readShort()S

    move-result v5

    .line 771
    add-int/lit8 v2, v2, -0x6

    .line 772
    const v6, 0x45786966

    if-ne v3, v6, :cond_2

    if-nez v5, :cond_2

    .line 773
    invoke-virtual {v4}, Lqn;->dk()I

    move-result v1

    iput v1, p0, Lqr;->vO:I

    .line 774
    iput v2, p0, Lqr;->vL:I

    .line 775
    iget v1, p0, Lqr;->vO:I

    iget v1, p0, Lqr;->vL:I

    .line 786
    :goto_2
    return v0

    :cond_1
    move v2, v1

    .line 761
    goto :goto_1

    .line 780
    :cond_2
    const/4 v3, 0x2

    if-lt v2, v3, :cond_3

    add-int/lit8 v3, v2, -0x2

    int-to-long v6, v3

    add-int/lit8 v2, v2, -0x2

    int-to-long v2, v2

    invoke-virtual {v4, v2, v3}, Lqn;->skip(J)J

    move-result-wide v2

    cmp-long v2, v6, v2

    if-eqz v2, :cond_4

    .line 781
    :cond_3
    const-string v0, "ExifParser"

    const-string v2, "Invalid JPEG format."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 782
    goto :goto_2

    .line 784
    :cond_4
    invoke-virtual {v4}, Lqn;->readShort()S

    move-result v2

    move v3, v2

    .line 785
    goto :goto_0

    :cond_5
    move v0, v1

    .line 786
    goto :goto_2
.end method

.method private do()Z
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lqr;->vB:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private dp()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 364
    iget v0, p0, Lqr;->vC:I

    add-int/lit8 v0, v0, 0x2

    iget v1, p0, Lqr;->vD:I

    mul-int/lit8 v1, v1, 0xc

    add-int/2addr v1, v0

    .line 365
    iget-object v0, p0, Lqr;->vA:Lqn;

    invoke-virtual {v0}, Lqn;->dk()I

    move-result v0

    .line 366
    if-le v0, v1, :cond_1

    .line 389
    :cond_0
    :goto_0
    return-void

    .line 369
    :cond_1
    iget-boolean v2, p0, Lqr;->vJ:Z

    if-eqz v2, :cond_3

    .line 370
    :cond_2
    :goto_1
    if-ge v0, v1, :cond_4

    .line 371
    invoke-direct {p0}, Lqr;->dw()Lqw;

    move-result-object v2

    iput-object v2, p0, Lqr;->vF:Lqw;

    .line 372
    add-int/lit8 v0, v0, 0xc

    .line 373
    iget-object v2, p0, Lqr;->vF:Lqw;

    if-eqz v2, :cond_2

    .line 374
    iget-object v2, p0, Lqr;->vF:Lqw;

    invoke-direct {p0, v2}, Lqr;->b(Lqw;)V

    goto :goto_1

    .line 379
    :cond_3
    invoke-direct {p0, v1}, Lqr;->aE(I)V

    .line 381
    :cond_4
    invoke-direct {p0}, Lqr;->dx()J

    move-result-wide v0

    .line 383
    iget v2, p0, Lqr;->vE:I

    if-nez v2, :cond_0

    invoke-direct {p0, v4}, Lqr;->aD(I)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-direct {p0}, Lqr;->do()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 385
    :cond_5
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 386
    invoke-direct {p0, v4, v0, v1}, Lqr;->a(IJ)V

    goto :goto_0
.end method

.method private dq()Z
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 392
    iget v2, p0, Lqr;->vE:I

    packed-switch v2, :pswitch_data_0

    .line 403
    :cond_0
    :goto_0
    return v0

    .line 394
    :pswitch_0
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lqr;->aD(I)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x4

    invoke-direct {p0, v2}, Lqr;->aD(I)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0, v3}, Lqr;->aD(I)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0, v1}, Lqr;->aD(I)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 398
    :pswitch_1
    invoke-direct {p0}, Lqr;->do()Z

    move-result v0

    goto :goto_0

    .line 401
    :pswitch_2
    invoke-direct {p0, v3}, Lqr;->aD(I)Z

    move-result v0

    goto :goto_0

    .line 392
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private dw()Lqw;
    .locals 12

    .prologue
    const-wide/32 v10, 0x7fffffff

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 534
    iget-object v0, p0, Lqr;->vA:Lqn;

    invoke-virtual {v0}, Lqn;->readShort()S

    move-result v1

    .line 535
    iget-object v0, p0, Lqr;->vA:Lqn;

    invoke-virtual {v0}, Lqn;->readShort()S

    move-result v2

    .line 536
    iget-object v0, p0, Lqr;->vA:Lqn;

    invoke-virtual {v0}, Lqn;->dm()J

    move-result-wide v8

    .line 537
    cmp-long v0, v8, v10

    if-lez v0, :cond_0

    .line 538
    new-instance v0, Lqq;

    const-string v1, "Number of component is larger then Integer.MAX_VALUE"

    invoke-direct {v0, v1}, Lqq;-><init>(Ljava/lang/String;)V

    throw v0

    .line 542
    :cond_0
    invoke-static {v2}, Lqw;->c(S)Z

    move-result v0

    if-nez v0, :cond_1

    .line 543
    const-string v0, "ExifParser"

    const-string v3, "Tag %04x: Invalid data type %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    aput-object v1, v4, v6

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    iget-object v0, p0, Lqr;->vA:Lqn;

    const-wide/16 v2, 0x4

    invoke-virtual {v0, v2, v3}, Lqn;->skip(J)J

    .line 545
    const/4 v0, 0x0

    .line 578
    :goto_0
    return-object v0

    .line 548
    :cond_1
    new-instance v0, Lqw;

    long-to-int v3, v8

    iget v4, p0, Lqr;->vE:I

    long-to-int v7, v8

    if-eqz v7, :cond_2

    :goto_1
    invoke-direct/range {v0 .. v5}, Lqw;-><init>(SSIIZ)V

    .line 550
    invoke-virtual {v0}, Lqw;->getDataSize()I

    move-result v1

    .line 551
    const/4 v3, 0x4

    if-le v1, v3, :cond_5

    .line 552
    iget-object v1, p0, Lqr;->vA:Lqn;

    invoke-virtual {v1}, Lqn;->dm()J

    move-result-wide v4

    .line 553
    cmp-long v1, v4, v10

    if-lez v1, :cond_3

    .line 554
    new-instance v0, Lqq;

    const-string v1, "offset is larger then Integer.MAX_VALUE"

    invoke-direct {v0, v1}, Lqq;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v5, v6

    .line 548
    goto :goto_1

    .line 559
    :cond_3
    iget v1, p0, Lqr;->vN:I

    int-to-long v10, v1

    cmp-long v1, v4, v10

    if-gez v1, :cond_4

    const/4 v1, 0x7

    if-ne v2, v1, :cond_4

    .line 560
    long-to-int v1, v8

    new-array v1, v1, [B

    .line 561
    iget-object v2, p0, Lqr;->vM:[B

    long-to-int v3, v4

    add-int/lit8 v3, v3, -0x8

    long-to-int v4, v8

    invoke-static {v2, v3, v1, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 563
    invoke-virtual {v0, v1}, Lqw;->setValue([B)Z

    goto :goto_0

    .line 565
    :cond_4
    long-to-int v1, v4

    invoke-virtual {v0, v1}, Lqw;->setOffset(I)V

    goto :goto_0

    .line 568
    :cond_5
    invoke-virtual {v0}, Lqw;->dD()Z

    move-result v2

    .line 570
    invoke-virtual {v0, v6}, Lqw;->u(Z)V

    .line 572
    invoke-virtual {p0, v0}, Lqr;->c(Lqw;)V

    .line 573
    invoke-virtual {v0, v2}, Lqw;->u(Z)V

    .line 574
    iget-object v2, p0, Lqr;->vA:Lqn;

    rsub-int/lit8 v1, v1, 0x4

    int-to-long v4, v1

    invoke-virtual {v2, v4, v5}, Lqn;->skip(J)J

    .line 576
    iget-object v1, p0, Lqr;->vA:Lqn;

    invoke-virtual {v1}, Lqn;->dk()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    invoke-virtual {v0, v1}, Lqw;->setOffset(I)V

    goto :goto_0
.end method

.method private dx()J
    .locals 4

    .prologue
    .line 846
    iget-object v0, p0, Lqr;->vA:Lqn;

    invoke-virtual {v0}, Lqn;->readInt()I

    move-result v0

    int-to-long v0, v0

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    return-wide v0
.end method

.method private w(II)Z
    .locals 1

    .prologue
    .line 638
    iget-object v0, p0, Lqr;->vP:Lqp;

    invoke-virtual {v0}, Lqp;->dn()Landroid/util/SparseIntArray;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 639
    if-nez v0, :cond_0

    .line 640
    const/4 v0, 0x0

    .line 642
    :goto_0
    return v0

    :cond_0
    invoke-static {v0, p1}, Lqp;->v(II)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lqw;)V
    .locals 4

    .prologue
    .line 513
    invoke-virtual {p1}, Lqw;->getOffset()I

    move-result v0

    iget-object v1, p0, Lqr;->vA:Lqn;

    invoke-virtual {v1}, Lqn;->dk()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 514
    iget-object v0, p0, Lqr;->vX:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lqw;->getOffset()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lqs;

    const/4 v3, 0x1

    invoke-direct {v2, p1, v3}, Lqs;-><init>(Lqw;Z)V

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 516
    :cond_0
    return-void
.end method

.method protected final c(Lqw;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 647
    invoke-virtual {p1}, Lqw;->dA()S

    move-result v0

    .line 648
    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    const/4 v2, 0x7

    if-eq v0, v2, :cond_0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 650
    :cond_0
    invoke-virtual {p1}, Lqw;->dB()I

    move-result v2

    .line 651
    iget-object v0, p0, Lqr;->vX:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 652
    iget-object v0, p0, Lqr;->vX:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p0, Lqr;->vA:Lqn;

    invoke-virtual {v3}, Lqn;->dk()I

    move-result v3

    add-int/2addr v2, v3

    if-ge v0, v2, :cond_1

    .line 654
    iget-object v0, p0, Lqr;->vX:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 655
    instance-of v2, v0, Lqu;

    if-eqz v2, :cond_2

    .line 657
    const-string v0, "ExifParser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Thumbnail overlaps value for tag: \n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lqw;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    iget-object v0, p0, Lqr;->vX:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->pollFirstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    .line 659
    const-string v2, "ExifParser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid thumbnail offset: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 679
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lqw;->dA()S

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 734
    :goto_1
    :pswitch_0
    return-void

    .line 662
    :cond_2
    instance-of v2, v0, Lqt;

    if-eqz v2, :cond_4

    .line 663
    const-string v2, "ExifParser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ifd "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    check-cast v0, Lqt;

    iget v0, v0, Lqt;->wa:I

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " overlaps value for tag: \n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lqw;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    :cond_3
    :goto_2
    iget-object v0, p0, Lqr;->vX:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lqr;->vA:Lqn;

    invoke-virtual {v2}, Lqn;->dk()I

    move-result v2

    sub-int/2addr v0, v2

    .line 672
    const-string v2, "ExifParser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid size of tag: \n"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lqw;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " setting count to: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 674
    invoke-virtual {p1, v0}, Lqw;->aH(I)V

    goto :goto_0

    .line 665
    :cond_4
    instance-of v2, v0, Lqs;

    if-eqz v2, :cond_3

    .line 666
    const-string v2, "ExifParser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Tag value for tag: \n"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    check-cast v0, Lqs;

    iget-object v0, v0, Lqs;->vY:Lqw;

    invoke-virtual {v0}, Lqw;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " overlaps value for tag: \n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lqw;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 682
    :pswitch_1
    invoke-virtual {p1}, Lqw;->dB()I

    move-result v0

    new-array v0, v0, [B

    .line 683
    invoke-virtual {p0, v0}, Lqr;->read([B)I

    .line 684
    invoke-virtual {p1, v0}, Lqw;->setValue([B)Z

    goto/16 :goto_1

    .line 688
    :pswitch_2
    invoke-virtual {p1}, Lqw;->dB()I

    move-result v0

    sget-object v1, Lqr;->US_ASCII:Ljava/nio/charset/Charset;

    if-lez v0, :cond_5

    iget-object v2, p0, Lqr;->vA:Lqn;

    invoke-virtual {v2, v0, v1}, Lqn;->a(ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {p1, v0}, Lqw;->setValue(Ljava/lang/String;)Z

    goto/16 :goto_1

    :cond_5
    const-string v0, ""

    goto :goto_3

    .line 691
    :pswitch_3
    invoke-virtual {p1}, Lqw;->dB()I

    move-result v0

    new-array v0, v0, [J

    .line 692
    array-length v2, v0

    :goto_4
    if-ge v1, v2, :cond_6

    .line 693
    invoke-direct {p0}, Lqr;->dx()J

    move-result-wide v4

    aput-wide v4, v0, v1

    .line 692
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 695
    :cond_6
    invoke-virtual {p1, v0}, Lqw;->a([J)Z

    goto/16 :goto_1

    .line 699
    :pswitch_4
    invoke-virtual {p1}, Lqw;->dB()I

    move-result v0

    new-array v0, v0, [Lqy;

    .line 700
    array-length v2, v0

    :goto_5
    if-ge v1, v2, :cond_7

    .line 701
    invoke-direct {p0}, Lqr;->dx()J

    move-result-wide v4

    invoke-direct {p0}, Lqr;->dx()J

    move-result-wide v6

    new-instance v3, Lqy;

    invoke-direct {v3, v4, v5, v6, v7}, Lqy;-><init>(JJ)V

    aput-object v3, v0, v1

    .line 700
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 703
    :cond_7
    invoke-virtual {p1, v0}, Lqw;->a([Lqy;)Z

    goto/16 :goto_1

    .line 707
    :pswitch_5
    invoke-virtual {p1}, Lqw;->dB()I

    move-result v0

    new-array v0, v0, [I

    .line 708
    array-length v2, v0

    :goto_6
    if-ge v1, v2, :cond_8

    .line 709
    iget-object v3, p0, Lqr;->vA:Lqn;

    invoke-virtual {v3}, Lqn;->readShort()S

    move-result v3

    const v4, 0xffff

    and-int/2addr v3, v4

    aput v3, v0, v1

    .line 708
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 711
    :cond_8
    invoke-virtual {p1, v0}, Lqw;->d([I)Z

    goto/16 :goto_1

    .line 715
    :pswitch_6
    invoke-virtual {p1}, Lqw;->dB()I

    move-result v0

    new-array v0, v0, [I

    .line 716
    array-length v2, v0

    :goto_7
    if-ge v1, v2, :cond_9

    .line 717
    iget-object v3, p0, Lqr;->vA:Lqn;

    invoke-virtual {v3}, Lqn;->readInt()I

    move-result v3

    aput v3, v0, v1

    .line 716
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 719
    :cond_9
    invoke-virtual {p1, v0}, Lqw;->d([I)Z

    goto/16 :goto_1

    .line 723
    :pswitch_7
    invoke-virtual {p1}, Lqw;->dB()I

    move-result v0

    new-array v2, v0, [Lqy;

    .line 724
    array-length v3, v2

    move v0, v1

    :goto_8
    if-ge v0, v3, :cond_a

    .line 725
    iget-object v1, p0, Lqr;->vA:Lqn;

    invoke-virtual {v1}, Lqn;->readInt()I

    move-result v1

    iget-object v4, p0, Lqr;->vA:Lqn;

    invoke-virtual {v4}, Lqn;->readInt()I

    move-result v4

    new-instance v5, Lqy;

    int-to-long v6, v1

    int-to-long v8, v4

    invoke-direct {v5, v6, v7, v8, v9}, Lqy;-><init>(JJ)V

    aput-object v5, v2, v0

    .line 724
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 727
    :cond_a
    invoke-virtual {p1, v2}, Lqw;->a([Lqy;)Z

    goto/16 :goto_1

    .line 679
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected final dl()Ljava/nio/ByteOrder;
    .locals 1

    .prologue
    .line 914
    iget-object v0, p0, Lqr;->vA:Lqn;

    invoke-virtual {v0}, Lqn;->dl()Ljava/nio/ByteOrder;

    move-result-object v0

    return-object v0
.end method

.method protected final dr()Lqw;
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lqr;->vF:Lqw;

    return-object v0
.end method

.method protected final ds()I
    .locals 1

    .prologue
    .line 452
    iget v0, p0, Lqr;->vE:I

    return v0
.end method

.method protected final dt()I
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lqr;->vG:Lqu;

    iget v0, v0, Lqu;->wb:I

    return v0
.end method

.method protected final du()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 480
    iget-object v1, p0, Lqr;->vH:Lqw;

    if-nez v1, :cond_0

    .line 482
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lqr;->vH:Lqw;

    invoke-virtual {v1, v0}, Lqw;->aI(I)J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method protected final dv()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 490
    iget-object v1, p0, Lqr;->vI:Lqw;

    if-nez v1, :cond_0

    .line 493
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lqr;->vI:Lqw;

    invoke-virtual {v1, v0}, Lqw;->aI(I)J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method protected final next()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x4

    const/4 v2, 0x5

    const/4 v0, 0x1

    .line 272
    :cond_0
    iget-boolean v3, p0, Lqr;->vK:Z

    if-nez v3, :cond_2

    move v0, v2

    .line 353
    :cond_1
    :goto_0
    return v0

    .line 275
    :cond_2
    iget-object v3, p0, Lqr;->vA:Lqn;

    invoke-virtual {v3}, Lqn;->dk()I

    move-result v3

    .line 276
    iget v4, p0, Lqr;->vC:I

    add-int/lit8 v4, v4, 0x2

    iget v5, p0, Lqr;->vD:I

    mul-int/lit8 v5, v5, 0xc

    add-int/2addr v4, v5

    .line 277
    if-ge v3, v4, :cond_3

    .line 278
    invoke-direct {p0}, Lqr;->dw()Lqw;

    move-result-object v3

    iput-object v3, p0, Lqr;->vF:Lqw;

    .line 279
    iget-object v3, p0, Lqr;->vF:Lqw;

    if-eqz v3, :cond_0

    .line 282
    iget-boolean v1, p0, Lqr;->vJ:Z

    if-eqz v1, :cond_1

    .line 283
    iget-object v1, p0, Lqr;->vF:Lqw;

    invoke-direct {p0, v1}, Lqr;->b(Lqw;)V

    goto :goto_0

    .line 286
    :cond_3
    if-ne v3, v4, :cond_5

    .line 288
    iget v3, p0, Lqr;->vE:I

    if-nez v3, :cond_6

    .line 289
    invoke-direct {p0}, Lqr;->dx()J

    move-result-wide v4

    .line 290
    invoke-direct {p0, v0}, Lqr;->aD(I)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-direct {p0}, Lqr;->do()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 291
    :cond_4
    cmp-long v1, v4, v6

    if-eqz v1, :cond_5

    .line 292
    invoke-direct {p0, v0, v4, v5}, Lqr;->a(IJ)V

    .line 312
    :cond_5
    :goto_1
    iget-object v0, p0, Lqr;->vX:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->size()I

    move-result v0

    if-eqz v0, :cond_d

    .line 313
    iget-object v0, p0, Lqr;->vX:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->pollFirstEntry()Ljava/util/Map$Entry;

    move-result-object v3

    .line 314
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 316
    :try_start_0
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lqr;->aE(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 322
    instance-of v0, v1, Lqt;

    if-eqz v0, :cond_a

    move-object v0, v1

    .line 323
    check-cast v0, Lqt;

    iget v0, v0, Lqt;->wa:I

    iput v0, p0, Lqr;->vE:I

    .line 324
    iget-object v0, p0, Lqr;->vA:Lqn;

    invoke-virtual {v0}, Lqn;->readUnsignedShort()I

    move-result v0

    iput v0, p0, Lqr;->vD:I

    .line 325
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lqr;->vC:I

    .line 327
    iget v0, p0, Lqr;->vD:I

    mul-int/lit8 v0, v0, 0xc

    iget v3, p0, Lqr;->vC:I

    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x2

    iget v3, p0, Lqr;->vL:I

    if-le v0, v3, :cond_8

    .line 328
    const-string v0, "ExifParser"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Invalid size of IFD "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lqr;->vE:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 329
    goto/16 :goto_0

    .line 298
    :cond_6
    iget-object v0, p0, Lqr;->vX:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->size()I

    move-result v0

    if-lez v0, :cond_e

    .line 299
    iget-object v0, p0, Lqr;->vX:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p0, Lqr;->vA:Lqn;

    invoke-virtual {v3}, Lqn;->dk()I

    move-result v3

    sub-int/2addr v0, v3

    .line 302
    :goto_2
    if-ge v0, v1, :cond_7

    .line 303
    const-string v1, "ExifParser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid size of link to next IFD: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 305
    :cond_7
    invoke-direct {p0}, Lqr;->dx()J

    move-result-wide v0

    .line 306
    cmp-long v3, v0, v6

    if-eqz v3, :cond_5

    .line 307
    const-string v3, "ExifParser"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid link to next IFD: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 318
    :catch_0
    move-exception v0

    const-string v0, "ExifParser"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to skip to data at: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", the file may be broken."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 332
    :cond_8
    invoke-direct {p0}, Lqr;->dq()Z

    move-result v0

    iput-boolean v0, p0, Lqr;->vJ:Z

    .line 333
    check-cast v1, Lqt;

    iget-boolean v0, v1, Lqt;->vZ:Z

    if-eqz v0, :cond_9

    .line 334
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 336
    :cond_9
    invoke-direct {p0}, Lqr;->dp()V

    goto/16 :goto_1

    .line 338
    :cond_a
    instance-of v0, v1, Lqu;

    if-eqz v0, :cond_b

    .line 339
    check-cast v1, Lqu;

    iput-object v1, p0, Lqr;->vG:Lqu;

    .line 340
    iget-object v0, p0, Lqr;->vG:Lqu;

    iget v0, v0, Lqu;->type:I

    goto/16 :goto_0

    .line 342
    :cond_b
    check-cast v1, Lqs;

    .line 343
    iget-object v0, v1, Lqs;->vY:Lqw;

    iput-object v0, p0, Lqr;->vF:Lqw;

    .line 344
    iget-object v0, p0, Lqr;->vF:Lqw;

    invoke-virtual {v0}, Lqw;->dA()S

    move-result v0

    const/4 v3, 0x7

    if-eq v0, v3, :cond_c

    .line 345
    iget-object v0, p0, Lqr;->vF:Lqw;

    invoke-virtual {p0, v0}, Lqr;->c(Lqw;)V

    .line 346
    iget-object v0, p0, Lqr;->vF:Lqw;

    invoke-direct {p0, v0}, Lqr;->b(Lqw;)V

    .line 348
    :cond_c
    iget-boolean v0, v1, Lqs;->vZ:Z

    if-eqz v0, :cond_5

    .line 349
    const/4 v0, 0x2

    goto/16 :goto_0

    :cond_d
    move v0, v2

    .line 353
    goto/16 :goto_0

    :cond_e
    move v0, v1

    goto/16 :goto_2
.end method

.method protected final read([B)I
    .locals 1

    .prologue
    .line 808
    iget-object v0, p0, Lqr;->vA:Lqn;

    invoke-virtual {v0, p1}, Lqn;->read([B)I

    move-result v0

    return v0
.end method
