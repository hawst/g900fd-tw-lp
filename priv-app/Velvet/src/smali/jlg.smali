.class public final Ljlg;
.super Ljsl;
.source "PG"


# static fields
.field public static final ero:Ljsm;


# instance fields
.field private aez:I

.field private erc:Ljava/lang/String;

.field private erd:Ljpe;

.field public erp:Ljlq;

.field private erq:Ljlq;

.field private err:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 3499
    const/16 v0, 0xb

    const-class v1, Ljlg;

    const v2, 0xc13f242

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljlg;->ero:Ljsm;

    .line 3505
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 3562
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3563
    iput v0, p0, Ljlg;->aez:I

    iput-object v1, p0, Ljlg;->erp:Ljlq;

    iput-object v1, p0, Ljlg;->erq:Ljlq;

    iput v0, p0, Ljlg;->err:I

    const-string v0, ""

    iput-object v0, p0, Ljlg;->erc:Ljava/lang/String;

    iput-object v1, p0, Ljlg;->erd:Ljpe;

    iput-object v1, p0, Ljlg;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljlg;->eCz:I

    .line 3564
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 3492
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljlg;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljlg;->erp:Ljlq;

    if-nez v0, :cond_1

    new-instance v0, Ljlq;

    invoke-direct {v0}, Ljlq;-><init>()V

    iput-object v0, p0, Ljlg;->erp:Ljlq;

    :cond_1
    iget-object v0, p0, Ljlg;->erp:Ljlq;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljlg;->erq:Ljlq;

    if-nez v0, :cond_2

    new-instance v0, Ljlq;

    invoke-direct {v0}, Ljlq;-><init>()V

    iput-object v0, p0, Ljlg;->erq:Ljlq;

    :cond_2
    iget-object v0, p0, Ljlg;->erq:Ljlq;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljlg;->err:I

    iget v0, p0, Ljlg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlg;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlg;->erc:Ljava/lang/String;

    iget v0, p0, Ljlg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlg;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljlg;->erd:Ljpe;

    if-nez v0, :cond_3

    new-instance v0, Ljpe;

    invoke-direct {v0}, Ljpe;-><init>()V

    iput-object v0, p0, Ljlg;->erd:Ljpe;

    :cond_3
    iget-object v0, p0, Ljlg;->erd:Ljpe;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 3581
    iget-object v0, p0, Ljlg;->erp:Ljlq;

    if-eqz v0, :cond_0

    .line 3582
    const/4 v0, 0x1

    iget-object v1, p0, Ljlg;->erp:Ljlq;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 3584
    :cond_0
    iget-object v0, p0, Ljlg;->erq:Ljlq;

    if-eqz v0, :cond_1

    .line 3585
    const/4 v0, 0x2

    iget-object v1, p0, Ljlg;->erq:Ljlq;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 3587
    :cond_1
    iget v0, p0, Ljlg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 3588
    const/4 v0, 0x3

    iget v1, p0, Ljlg;->err:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 3590
    :cond_2
    iget v0, p0, Ljlg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 3591
    const/4 v0, 0x4

    iget-object v1, p0, Ljlg;->erc:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3593
    :cond_3
    iget-object v0, p0, Ljlg;->erd:Ljpe;

    if-eqz v0, :cond_4

    .line 3594
    const/4 v0, 0x5

    iget-object v1, p0, Ljlg;->erd:Ljpe;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 3596
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3597
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 3601
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3602
    iget-object v1, p0, Ljlg;->erp:Ljlq;

    if-eqz v1, :cond_0

    .line 3603
    const/4 v1, 0x1

    iget-object v2, p0, Ljlg;->erp:Ljlq;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3606
    :cond_0
    iget-object v1, p0, Ljlg;->erq:Ljlq;

    if-eqz v1, :cond_1

    .line 3607
    const/4 v1, 0x2

    iget-object v2, p0, Ljlg;->erq:Ljlq;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3610
    :cond_1
    iget v1, p0, Ljlg;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    .line 3611
    const/4 v1, 0x3

    iget v2, p0, Ljlg;->err:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3614
    :cond_2
    iget v1, p0, Ljlg;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_3

    .line 3615
    const/4 v1, 0x4

    iget-object v2, p0, Ljlg;->erc:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3618
    :cond_3
    iget-object v1, p0, Ljlg;->erd:Ljpe;

    if-eqz v1, :cond_4

    .line 3619
    const/4 v1, 0x5

    iget-object v2, p0, Ljlg;->erd:Ljpe;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3622
    :cond_4
    return v0
.end method
