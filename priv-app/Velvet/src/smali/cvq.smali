.class public final Lcvq;
.super Lcyc;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# static fields
.field private static final bld:[Ljava/lang/CharSequence;


# instance fields
.field private final ble:Lhxr;

.field private final mContext:Landroid/content/Context;

.field private final mDebugFeatures:Lckw;

.field private final mGsaConfigFlags:Lchk;

.field private final mNetworkInformation:Lgno;

.field private final mSearchConfig:Lcjs;

.field final mVoiceSettings:Lhym;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/CharSequence;

    sput-object v0, Lcvq;->bld:[Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>(Lckw;Lhym;Lhxr;Lgno;Landroid/content/Context;Lcjs;Lchk;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcyc;-><init>()V

    .line 64
    iput-object p1, p0, Lcvq;->mDebugFeatures:Lckw;

    .line 65
    iput-object p2, p0, Lcvq;->mVoiceSettings:Lhym;

    .line 66
    iput-object p3, p0, Lcvq;->ble:Lhxr;

    .line 67
    iput-object p4, p0, Lcvq;->mNetworkInformation:Lgno;

    .line 68
    iput-object p5, p0, Lcvq;->mContext:Landroid/content/Context;

    .line 69
    iput-object p6, p0, Lcvq;->mSearchConfig:Lcjs;

    .line 70
    iput-object p7, p0, Lcvq;->mGsaConfigFlags:Lchk;

    .line 71
    return-void
.end method

.method private Tk()Ljava/lang/String;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcvq;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->Mc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 298
    iget-object v0, p0, Lcvq;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->Mc()Ljava/lang/String;

    move-result-object v0

    .line 305
    :goto_0
    return-object v0

    .line 301
    :cond_0
    iget-object v0, p0, Lcvq;->mSearchConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Mc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 302
    iget-object v0, p0, Lcvq;->mSearchConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Mc()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 305
    :cond_1
    const-string v0, "Production SSL/HTTPS"

    goto :goto_0
.end method

.method private a(Landroid/preference/ListPreference;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 225
    iget-object v0, p0, Lcvq;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aUo()Ljava/lang/String;

    move-result-object v0

    .line 226
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 227
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Manual sandbox override \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" in use."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 228
    sget-object v0, Lcvq;->bld:[Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 229
    sget-object v0, Lcvq;->bld:[Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 230
    invoke-virtual {p1, v3}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 247
    :goto_0
    return-void

    .line 232
    :cond_0
    iget-object v0, p0, Lcvq;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    .line 234
    iget-object v1, v0, Ljze;->eNp:Ljzf;

    if-eqz v1, :cond_1

    .line 235
    iget-object v0, v0, Ljze;->eNp:Ljzf;

    iget-object v0, v0, Ljzf;->eNv:[Ljzg;

    .line 236
    invoke-static {v0}, Lcvq;->a([Ljzg;)[Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 237
    invoke-static {v0}, Lcvq;->a([Ljzg;)[Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 238
    invoke-direct {p0}, Lcvq;->Tk()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 239
    invoke-direct {p0}, Lcvq;->Tk()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 241
    :cond_1
    const-string v0, "[DEBUG] Config sync error! Try checking in."

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 242
    sget-object v0, Lcvq;->bld:[Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 243
    sget-object v0, Lcvq;->bld:[Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 244
    invoke-virtual {p1, v3}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto :goto_0
.end method

.method private a(Landroid/preference/SwitchPreference;)V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcvq;->ble:Lhxr;

    invoke-virtual {v0}, Lhxr;->aTt()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 117
    return-void
.end method

.method private static a([Ljzg;)[Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 309
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    new-array v1, v0, [Ljava/lang/CharSequence;

    .line 310
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 311
    aget-object v2, p0, v0

    invoke-virtual {v2}, Ljzg;->getLabel()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 310
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 313
    :cond_0
    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    const-string v2, "Clear override"

    aput-object v2, v1, v0

    .line 314
    return-object v1
.end method

.method private b(Landroid/preference/Preference;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 318
    iget-object v1, p0, Lcvq;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f004e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 320
    iget-object v2, p0, Lcvq;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f004d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 322
    if-nez p2, :cond_2

    .line 323
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 325
    :cond_1
    add-int/lit8 v0, v0, 0x1

    :cond_2
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 326
    aget-object v3, v1, v0

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 327
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 85
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debugS3Server"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 86
    check-cast v0, Landroid/preference/ListPreference;

    invoke-direct {p0, v0}, Lcvq;->a(Landroid/preference/ListPreference;)V

    .line 87
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debugS3Logging"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 89
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_0

    .line 90
    :cond_2
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debugPersonalization"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 91
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 92
    check-cast p1, Landroid/preference/SwitchPreference;

    invoke-direct {p0, p1}, Lcvq;->a(Landroid/preference/SwitchPreference;)V

    goto :goto_0

    .line 93
    :cond_3
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debugFreshContacts"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 94
    check-cast p1, Landroid/preference/ListPreference;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    new-instance v1, Lghs;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BL()Lchk;

    move-result-object v2

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    new-instance v3, Lgho;

    iget-object v4, p0, Lcvq;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-direct {v3, v4}, Lgho;-><init>(Landroid/content/ContentResolver;)V

    invoke-direct {v1, v2, v0, v3}, Lghs;-><init>(Lchk;Lcke;Lgho;)V

    invoke-virtual {v1}, Lghs;->aFL()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lesp;->L(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    if-lez v1, :cond_4

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    new-array v0, v7, [Ljava/lang/CharSequence;

    const-string v1, "No fresh contacts for you!"

    aput-object v1, v0, v6

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    new-array v0, v7, [Ljava/lang/CharSequence;

    const-string v1, "No fresh contacts for you!"

    aput-object v1, v0, v6

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 95
    :cond_5
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debugTopContacts"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 96
    check-cast p1, Landroid/preference/ListPreference;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    new-instance v1, Lghl;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BL()Lchk;

    move-result-object v2

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->BK()Lcke;

    move-result-object v3

    new-instance v4, Lghj;

    iget-object v5, p0, Lcvq;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-direct {v4, v5}, Lghj;-><init>(Landroid/content/ContentResolver;)V

    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    invoke-direct {v1, v2, v3, v4, v0}, Lghl;-><init>(Lchk;Lcke;Lghj;Ljava/util/concurrent/Executor;)V

    invoke-virtual {v1}, Lghl;->aFL()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lesp;->L(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    array-length v1, v0

    if-lez v1, :cond_6

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_6
    new-array v0, v7, [Ljava/lang/CharSequence;

    const-string v1, "No top contacts for you!"

    aput-object v1, v0, v6

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    new-array v0, v7, [Ljava/lang/CharSequence;

    const-string v1, "No top contacts for you!"

    aput-object v1, v0, v6

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 97
    :cond_7
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debugQuixote"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 98
    check-cast p1, Landroid/preference/ListPreference;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    new-instance v1, Lgih;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-direct {v1, v0}, Lgih;-><init>(Lchk;)V

    invoke-virtual {v1}, Lgih;->aFL()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lesp;->L(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    if-lez v1, :cond_8

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_8
    new-array v0, v7, [Ljava/lang/CharSequence;

    const-string v1, "No phrases for you!"

    aput-object v1, v0, v6

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    new-array v0, v7, [Ljava/lang/CharSequence;

    const-string v1, "No phrases for you!"

    aput-object v1, v0, v6

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 99
    :cond_9
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debugConfigurationDate"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 100
    iget-object v0, p0, Lcvq;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aUm()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 101
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto/16 :goto_0

    .line 102
    :cond_a
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debugConfigurationExperiment"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 103
    iget-object v0, p0, Lcvq;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->auq:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 104
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto/16 :goto_0

    .line 105
    :cond_b
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debugSendLoggedAudio"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 106
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto/16 :goto_0

    .line 107
    :cond_c
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "audioLoggingEnabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 108
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto/16 :goto_0

    .line 109
    :cond_d
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "debugRecognitionEngineRestrict"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 111
    iget-object v0, p0, Lcvq;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aUl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcvq;->b(Landroid/preference/Preference;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final d(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 75
    const-string v2, "debugPersonalization"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 77
    iget-object v2, p0, Lcvq;->mDebugFeatures:Lckw;

    invoke-virtual {v2}, Lckw;->Pa()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcvq;->mGsaConfigFlags:Lchk;

    invoke-virtual {v2}, Lchk;->Ga()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 80
    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v2, p0, Lcvq;->mDebugFeatures:Lckw;

    invoke-virtual {v2}, Lckw;->Pa()Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 121
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v3, "debugPersonalization"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    check-cast p1, Landroid/preference/SwitchPreference;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcvq;->mNetworkInformation:Lgno;

    invoke-virtual {v0}, Lgno;->aAj()I

    move-result v0

    :goto_0
    iget-object v3, p0, Lcvq;->mVoiceSettings:Lhym;

    invoke-virtual {v3}, Lhym;->aTC()Ljze;

    move-result-object v3

    new-instance v4, Ljzu;

    invoke-direct {v4}, Ljzu;-><init>()V

    new-array v5, v2, [I

    aput v0, v5, v1

    iput-object v5, v4, Ljzu;->eOt:[I

    const-string v0, "http://www.google.com"

    invoke-virtual {v4, v0}, Ljzu;->An(Ljava/lang/String;)Ljzu;

    const-string v0, "http://www.google.com"

    invoke-virtual {v4, v0}, Ljzu;->Ao(Ljava/lang/String;)Ljzu;

    iput-object v4, v3, Ljze;->eNc:Ljzu;

    iget-object v0, p0, Lcvq;->mVoiceSettings:Lhym;

    invoke-virtual {v0, v3}, Lhym;->l(Ljze;)V

    invoke-direct {p0, p1}, Lcvq;->a(Landroid/preference/SwitchPreference;)V

    move v1, v2

    .line 147
    :cond_0
    :goto_1
    return v1

    .line 124
    :cond_1
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v3, "audioLoggingEnabled"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 125
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 126
    iget-object v0, p0, Lcvq;->mContext:Landroid/content/Context;

    const-string v3, "debug"

    invoke-virtual {v0, v3, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    array-length v5, v4

    :goto_2
    if-ge v1, v5, :cond_2

    aget-object v6, v4, v1

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v3, "logs.zip"

    invoke-direct {v1, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_3
    move v1, v2

    .line 128
    goto :goto_1

    .line 129
    :cond_4
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v3, "debugRecognitionEngineRestrict"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 130
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcvq;->b(Landroid/preference/Preference;Ljava/lang/String;)V

    move v1, v2

    .line 131
    goto :goto_1

    .line 132
    :cond_5
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v3, "debugS3Server"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 133
    check-cast p2, Ljava/lang/String;

    .line 134
    const-string v0, "Clear override"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 135
    iget-object v0, p0, Lcvq;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aUk()V

    .line 139
    :goto_3
    check-cast p1, Landroid/preference/ListPreference;

    invoke-direct {p0, p1}, Lcvq;->a(Landroid/preference/ListPreference;)V

    goto :goto_1

    .line 137
    :cond_6
    iget-object v0, p0, Lcvq;->mVoiceSettings:Lhym;

    invoke-virtual {v0, p2}, Lhym;->oP(Ljava/lang/String;)V

    goto :goto_3

    .line 141
    :cond_7
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v3, "debugS3Logging"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    check-cast p2, Ljava/lang/Boolean;

    .line 143
    iget-object v0, p0, Lcvq;->mVoiceSettings:Lhym;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lhym;->gO(Z)V

    move v1, v2

    .line 144
    goto/16 :goto_1

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, -0x1

    const/4 v0, 0x1

    .line 152
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "debugConfigurationDate"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 153
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "text/plain"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "android.intent.extra.SUBJECT"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Configuration:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcvq;->mVoiceSettings:Lhym;

    invoke-virtual {v4}, Lhym;->aUm()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "android.intent.extra.TEXT"

    iget-object v3, p0, Lcvq;->mVoiceSettings:Lhym;

    invoke-virtual {v3}, Lhym;->aER()Ljze;

    move-result-object v3

    invoke-static {v3}, Leqh;->d(Ljsr;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcvq;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 162
    :cond_0
    :goto_0
    return v0

    .line 155
    :cond_1
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "debugConfigurationExperiment"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 156
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcvq;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/EditText;

    iget-object v3, p0, Lcvq;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p1}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v3, 0x104000a

    new-instance v4, Lcvr;

    invoke-direct {v4, p0, v2, p1}, Lcvr;-><init>(Lcvq;Landroid/widget/EditText;Landroid/preference/Preference;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 158
    :cond_2
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "debugSendLoggedAudio"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 159
    iget-object v2, p0, Lcvq;->mContext:Landroid/content/Context;

    const-string v3, "debug"

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v2}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v3

    new-instance v4, Ljava/io/File;

    const-string v5, "logs.zip"

    invoke-direct {v4, v3, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    invoke-static {v2, v4, v1}, Lgep;->a(Landroid/content/Context;Ljava/io/File;[Ljava/io/File;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "text/plain"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "android.intent.extra.SUBJECT"

    const-string v5, "Debug logged audio."

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "android.intent.extra.STREAM"

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    const-string v1, "Don\'t forget to turn off debug logging once your email has been sent."

    invoke-static {v2, v1}, Lgep;->o(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    move v0, v1

    .line 162
    goto/16 :goto_0
.end method
