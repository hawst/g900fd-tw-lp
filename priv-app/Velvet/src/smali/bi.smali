.class Lbi;
.super Lbe;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 558
    invoke-direct {p0}, Lbe;-><init>()V

    return-void
.end method


# virtual methods
.method public a([Lau;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 598
    invoke-static {p1}, Lbs;->a([Lbr;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public b(Lay;)Landroid/app/Notification;
    .locals 23

    .prologue
    .line 561
    new-instance v1, Lbt;

    move-object/from16 v0, p1

    iget-object v2, v0, Lay;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Lay;->cU:Landroid/app/Notification;

    move-object/from16 v0, p1

    iget-object v4, v0, Lay;->cI:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v5, v0, Lay;->cJ:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v6, v0, Lay;->cL:Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p1

    iget-object v9, v0, Lay;->cK:Landroid/app/PendingIntent;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p1

    iget v0, v0, Lay;->cM:I

    move/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lay;->cF:Landroid/os/Bundle;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-direct/range {v1 .. v22}, Lbt;-><init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZILjava/lang/CharSequence;ZLandroid/os/Bundle;Ljava/lang/String;ZLjava/lang/String;)V

    .line 567
    move-object/from16 v0, p1

    iget-object v2, v0, Lay;->cP:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Lat;->a(Lar;Ljava/util/ArrayList;)V

    .line 568
    move-object/from16 v0, p1

    iget-object v2, v0, Lay;->cO:Lbk;

    invoke-static {v1, v2}, Lat;->a(Las;Lbk;)V

    .line 569
    iget-object v2, v1, Lbt;->dj:Landroid/app/Notification$Builder;

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    invoke-static {v3}, Lbs;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v4

    new-instance v5, Landroid/os/Bundle;

    iget-object v2, v1, Lbt;->cF:Landroid/os/Bundle;

    invoke-direct {v5, v2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    iget-object v2, v1, Lbt;->cF:Landroid/os/Bundle;

    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v4, v5}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    iget-object v1, v1, Lbt;->dn:Ljava/util/List;

    invoke-static {v1}, Lbs;->a(Ljava/util/List;)Landroid/util/SparseArray;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {v3}, Lbs;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "android.support.actionExtras"

    invoke-virtual {v2, v4, v1}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    :cond_2
    return-object v3
.end method
