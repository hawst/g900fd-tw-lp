.class public final Lgyg;
.super Lgxi;
.source "PG"

# interfaces
.implements Lder;


# instance fields
.field private final cZj:Ldgd;

.field private final cZk:Ljava/util/List;

.field private final cZl:Ljava/util/List;

.field final cZm:I

.field final cZn:I

.field private cZo:Landroid/view/View;

.field private cq:Z


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/ui/MainContentView;Ldgd;II)V
    .locals 1

    .prologue
    .line 45
    const-string v0, "summons"

    invoke-direct {p0, v0, p1}, Lgxi;-><init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentView;)V

    .line 46
    iput-object p2, p0, Lgyg;->cZj:Ldgd;

    .line 47
    iput p3, p0, Lgyg;->cZm:I

    .line 48
    iput p4, p0, Lgyg;->cZn:I

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgyg;->cZk:Ljava/util/List;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgyg;->cZl:Ljava/util/List;

    .line 51
    return-void
.end method


# virtual methods
.method protected final X(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lgyg;->aLt()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {p0, v0}, Lgpu;->f(Lgyy;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgyg;->cZo:Landroid/view/View;

    .line 56
    iget-object v0, p0, Lgyg;->cZo:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 57
    return-void
.end method

.method public final a(Ldep;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 104
    iget-object v0, p1, Ldep;->mSuggestions:Ldem;

    if-eqz v0, :cond_1

    iget-object v0, p1, Ldep;->mSuggestions:Ldem;

    invoke-virtual {v0}, Ldem;->abl()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ldep;->abp()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 105
    iget-object v0, p0, Lgyg;->cZo:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lgyg;->cZo:Landroid/view/View;

    invoke-virtual {p0, v0, v1}, Lgyg;->B(Landroid/view/View;I)V

    .line 118
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 104
    goto :goto_0

    .line 109
    :cond_2
    invoke-virtual {p1}, Ldep;->abp()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgyg;->cZo:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 115
    iget-object v0, p0, Lgyg;->cZo:Landroid/view/View;

    invoke-virtual {p0, v0, v2}, Lgyg;->B(Landroid/view/View;I)V

    goto :goto_1
.end method

.method protected final aKF()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public final onStart()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v5, 0x0

    .line 65
    invoke-super {p0}, Lgxi;->onStart()V

    .line 66
    iput-boolean v0, p0, Lgyg;->cq:Z

    .line 67
    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lgyg;->cZo:Landroid/view/View;

    aput-object v1, v0, v5

    invoke-virtual {p0, v0}, Lgyg;->a([Landroid/view/View;)V

    .line 68
    invoke-virtual {p0}, Lgyg;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->UG()Ldep;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldep;->a(Lder;)V

    .line 69
    iget-object v0, p0, Lgyg;->cZj:Ldgd;

    invoke-virtual {v0}, Ldgd;->abR()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lgyg;->aAU()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lgyg;->cq:Z

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgb;

    iget-object v2, p0, Lgyy;->mPresenter:Lgyz;

    invoke-virtual {v2}, Lgyz;->aJU()Lgpu;

    move-result-object v2

    invoke-virtual {p0}, Lgyg;->aLt()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v2, p0, v3}, Lgpu;->b(Lgyy;Landroid/view/ViewGroup;)Lcom/google/android/search/suggest/SuggestionListView;

    move-result-object v2

    iget-object v3, p0, Lgyg;->cZl:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lgyg;->cZk:Ljava/util/List;

    new-instance v4, Lgyh;

    invoke-direct {v4, p0, v0, v2}, Lgyh;-><init>(Lgyg;Ldgb;Lcom/google/android/search/suggest/SuggestionListView;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lgyg;->cZl:Ljava/util/List;

    invoke-virtual {p0, v5, v0}, Lgyg;->a(ILjava/lang/Iterable;)V

    .line 70
    :cond_1
    return-void
.end method

.method public final onStop()V
    .locals 2

    .prologue
    .line 74
    invoke-super {p0}, Lgxi;->onStop()V

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgyg;->cq:Z

    .line 76
    invoke-virtual {p0}, Lgyg;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->UG()Ldep;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldep;->b(Lder;)V

    .line 77
    iget-object v0, p0, Lgyg;->cZk:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgyh;

    .line 78
    invoke-virtual {v0}, Lgyh;->ca()V

    goto :goto_0

    .line 80
    :cond_0
    iget-object v0, p0, Lgyg;->cZk:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 81
    iget-object v0, p0, Lgyg;->cZl:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 82
    sget-object v0, Lgxi;->cYu:Lesj;

    invoke-virtual {p0, v0}, Lgxi;->a(Lesj;)V

    .line 83
    sget-object v0, Lgxi;->cYt:Lesj;

    invoke-virtual {p0, v0}, Lgxi;->a(Lesj;)V

    .line 84
    return-void
.end method
