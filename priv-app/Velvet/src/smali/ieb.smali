.class public final Lieb;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ajs:I

.field public dzA:[Lidz;

.field private dzB:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1145
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1146
    iput v1, p0, Lieb;->aez:I

    invoke-static {}, Lidz;->aVe()[Lidz;

    move-result-object v0

    iput-object v0, p0, Lieb;->dzA:[Lidz;

    iput v1, p0, Lieb;->ajs:I

    const-string v0, ""

    iput-object v0, p0, Lieb;->dzB:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lieb;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lieb;->eCz:I

    .line 1147
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1077
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lieb;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lieb;->dzA:[Lidz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lidz;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lieb;->dzA:[Lidz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lidz;

    invoke-direct {v3}, Lidz;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lieb;->dzA:[Lidz;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lidz;

    invoke-direct {v3}, Lidz;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lieb;->dzA:[Lidz;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lieb;->ajs:I

    iget v0, p0, Lieb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lieb;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lieb;->dzB:Ljava/lang/String;

    iget v0, p0, Lieb;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lieb;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 1162
    iget-object v0, p0, Lieb;->dzA:[Lidz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lieb;->dzA:[Lidz;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1163
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lieb;->dzA:[Lidz;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1164
    iget-object v1, p0, Lieb;->dzA:[Lidz;

    aget-object v1, v1, v0

    .line 1165
    if-eqz v1, :cond_0

    .line 1166
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 1163
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1170
    :cond_1
    iget v0, p0, Lieb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 1171
    const/4 v0, 0x2

    iget v1, p0, Lieb;->ajs:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1173
    :cond_2
    iget v0, p0, Lieb;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 1174
    const/4 v0, 0x3

    iget-object v1, p0, Lieb;->dzB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1176
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1177
    return-void
.end method

.method public final aVg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1126
    iget-object v0, p0, Lieb;->dzB:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 1181
    invoke-super {p0}, Ljsl;->lF()I

    move-result v1

    .line 1182
    iget-object v0, p0, Lieb;->dzA:[Lidz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lieb;->dzA:[Lidz;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1183
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lieb;->dzA:[Lidz;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1184
    iget-object v2, p0, Lieb;->dzA:[Lidz;

    aget-object v2, v2, v0

    .line 1185
    if-eqz v2, :cond_0

    .line 1186
    const/4 v3, 0x1

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1183
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1191
    :cond_1
    iget v0, p0, Lieb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 1192
    const/4 v0, 0x2

    iget v2, p0, Lieb;->ajs:I

    invoke-static {v0, v2}, Ljsj;->bv(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 1195
    :cond_2
    iget v0, p0, Lieb;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 1196
    const/4 v0, 0x3

    iget-object v2, p0, Lieb;->dzB:Ljava/lang/String;

    invoke-static {v0, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1199
    :cond_3
    return v1
.end method
