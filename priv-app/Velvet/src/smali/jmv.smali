.class public final Ljmv;
.super Ljsl;
.source "PG"


# static fields
.field public static final euA:Ljsm;


# instance fields
.field private aez:I

.field private euB:Ljava/lang/String;

.field private euC:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 11557
    const/16 v0, 0xb

    const-class v1, Ljmv;

    const v2, 0xdb18b92

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljmv;->euA:Ljsm;

    .line 11569
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11617
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 11618
    iput v1, p0, Ljmv;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljmv;->euB:Ljava/lang/String;

    iput v1, p0, Ljmv;->euC:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljmv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljmv;->eCz:I

    .line 11619
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 11550
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljmv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmv;->euB:Ljava/lang/String;

    iget v0, p0, Ljmv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmv;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljmv;->euC:I

    iget v0, p0, Ljmv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmv;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 11633
    iget v0, p0, Ljmv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 11634
    const/4 v0, 0x1

    iget-object v1, p0, Ljmv;->euB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 11636
    :cond_0
    iget v0, p0, Ljmv;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 11637
    const/4 v0, 0x2

    iget v1, p0, Ljmv;->euC:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 11639
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 11640
    return-void
.end method

.method public final bqu()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11579
    iget-object v0, p0, Ljmv;->euB:Ljava/lang/String;

    return-object v0
.end method

.method public final bqv()Z
    .locals 1

    .prologue
    .line 11590
    iget v0, p0, Ljmv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 11644
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 11645
    iget v1, p0, Ljmv;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 11646
    const/4 v1, 0x1

    iget-object v2, p0, Ljmv;->euB:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11649
    :cond_0
    iget v1, p0, Ljmv;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 11650
    const/4 v1, 0x2

    iget v2, p0, Ljmv;->euC:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11653
    :cond_1
    return v0
.end method

.method public final xk(Ljava/lang/String;)Ljmv;
    .locals 1

    .prologue
    .line 11582
    if-nez p1, :cond_0

    .line 11583
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 11585
    :cond_0
    iput-object p1, p0, Ljmv;->euB:Ljava/lang/String;

    .line 11586
    iget v0, p0, Ljmv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmv;->aez:I

    .line 11587
    return-object p0
.end method
