.class public final Ljzz;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eOE:Z

.field private eOF:F

.field private eOG:Z

.field public eOH:[Ljava/lang/String;

.field private eOI:Z

.field private eOJ:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5288
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 5289
    iput v1, p0, Ljzz;->aez:I

    iput-boolean v1, p0, Ljzz;->eOE:Z

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ljzz;->eOF:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljzz;->eOG:Z

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljzz;->eOH:[Ljava/lang/String;

    iput-boolean v1, p0, Ljzz;->eOI:Z

    iput-boolean v1, p0, Ljzz;->eOJ:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljzz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljzz;->eCz:I

    .line 5290
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5171
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljzz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljzz;->eOE:Z

    iget v0, p0, Ljzz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzz;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljzz;->eOF:F

    iget v0, p0, Ljzz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzz;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljzz;->eOG:Z

    iget v0, p0, Ljzz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljzz;->aez:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljzz;->eOH:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljzz;->eOH:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljzz;->eOH:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljzz;->eOH:[Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljzz;->eOI:Z

    iget v0, p0, Ljzz;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljzz;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljzz;->eOJ:Z

    iget v0, p0, Ljzz;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljzz;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 5308
    iget v0, p0, Ljzz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 5309
    const/4 v0, 0x1

    iget-boolean v1, p0, Ljzz;->eOE:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 5311
    :cond_0
    iget v0, p0, Ljzz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 5312
    const/4 v0, 0x2

    iget v1, p0, Ljzz;->eOF:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 5314
    :cond_1
    iget v0, p0, Ljzz;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 5315
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljzz;->eOG:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 5317
    :cond_2
    iget-object v0, p0, Ljzz;->eOH:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljzz;->eOH:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 5318
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljzz;->eOH:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 5319
    iget-object v1, p0, Ljzz;->eOH:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 5320
    if-eqz v1, :cond_3

    .line 5321
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 5318
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5325
    :cond_4
    iget v0, p0, Ljzz;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 5326
    const/4 v0, 0x5

    iget-boolean v1, p0, Ljzz;->eOI:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 5328
    :cond_5
    iget v0, p0, Ljzz;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    .line 5329
    const/4 v0, 0x6

    iget-boolean v1, p0, Ljzz;->eOJ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 5331
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 5332
    return-void
.end method

.method public final bxA()Z
    .locals 1

    .prologue
    .line 5253
    iget-boolean v0, p0, Ljzz;->eOI:Z

    return v0
.end method

.method public final bxw()Z
    .locals 1

    .prologue
    .line 5193
    iget-boolean v0, p0, Ljzz;->eOE:Z

    return v0
.end method

.method public final bxx()F
    .locals 1

    .prologue
    .line 5212
    iget v0, p0, Ljzz;->eOF:F

    return v0
.end method

.method public final bxy()Z
    .locals 1

    .prologue
    .line 5220
    iget v0, p0, Ljzz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bxz()Z
    .locals 1

    .prologue
    .line 5231
    iget-boolean v0, p0, Ljzz;->eOG:Z

    return v0
.end method

.method public final jG(Z)Ljzz;
    .locals 1

    .prologue
    .line 5196
    iput-boolean p1, p0, Ljzz;->eOE:Z

    .line 5197
    iget v0, p0, Ljzz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzz;->aez:I

    .line 5198
    return-object p0
.end method

.method public final jH(Z)Ljzz;
    .locals 1

    .prologue
    .line 5256
    iput-boolean p1, p0, Ljzz;->eOI:Z

    .line 5257
    iget v0, p0, Ljzz;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljzz;->aez:I

    .line 5258
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 5336
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 5337
    iget v2, p0, Ljzz;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 5338
    const/4 v2, 0x1

    iget-boolean v3, p0, Ljzz;->eOE:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 5341
    :cond_0
    iget v2, p0, Ljzz;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 5342
    const/4 v2, 0x2

    iget v3, p0, Ljzz;->eOF:F

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 5345
    :cond_1
    iget v2, p0, Ljzz;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    .line 5346
    const/4 v2, 0x3

    iget-boolean v3, p0, Ljzz;->eOG:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 5349
    :cond_2
    iget-object v2, p0, Ljzz;->eOH:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ljzz;->eOH:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v1

    move v3, v1

    .line 5352
    :goto_0
    iget-object v4, p0, Ljzz;->eOH:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_4

    .line 5353
    iget-object v4, p0, Ljzz;->eOH:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 5354
    if-eqz v4, :cond_3

    .line 5355
    add-int/lit8 v3, v3, 0x1

    .line 5356
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 5352
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5360
    :cond_4
    add-int/2addr v0, v2

    .line 5361
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 5363
    :cond_5
    iget v1, p0, Ljzz;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_6

    .line 5364
    const/4 v1, 0x5

    iget-boolean v2, p0, Ljzz;->eOI:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5367
    :cond_6
    iget v1, p0, Ljzz;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_7

    .line 5368
    const/4 v1, 0x6

    iget-boolean v2, p0, Ljzz;->eOJ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5371
    :cond_7
    return v0
.end method
