.class public final Lsv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private synthetic AR:Lcom/android/launcher3/CellLayout$LayoutParams;

.field private synthetic AS:I

.field private synthetic AT:I

.field private synthetic AU:I

.field private synthetic AV:I

.field private synthetic AW:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/CellLayout;Lcom/android/launcher3/CellLayout$LayoutParams;IIIILandroid/view/View;)V
    .locals 0

    .prologue
    .line 1023
    iput-object p2, p0, Lsv;->AR:Lcom/android/launcher3/CellLayout$LayoutParams;

    iput p3, p0, Lsv;->AS:I

    iput p4, p0, Lsv;->AT:I

    iput p5, p0, Lsv;->AU:I

    iput p6, p0, Lsv;->AV:I

    iput-object p7, p0, Lsv;->AW:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1026
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1027
    iget-object v1, p0, Lsv;->AR:Lcom/android/launcher3/CellLayout$LayoutParams;

    sub-float v2, v4, v0

    iget v3, p0, Lsv;->AS:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget v3, p0, Lsv;->AT:I

    int-to-float v3, v3

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->x:I

    .line 1028
    iget-object v1, p0, Lsv;->AR:Lcom/android/launcher3/CellLayout$LayoutParams;

    sub-float v2, v4, v0

    iget v3, p0, Lsv;->AU:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget v3, p0, Lsv;->AV:I

    int-to-float v3, v3

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->y:I

    .line 1029
    iget-object v0, p0, Lsv;->AW:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1030
    return-void
.end method
