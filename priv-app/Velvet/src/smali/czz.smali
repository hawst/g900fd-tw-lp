.class public final Lczz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final boC:Lefk;

.field private final boD:Lefk;

.field public final boE:Ljava/util/Map;

.field private boF:Z

.field private boG:Ljava/util/Map;

.field public final dK:Ljava/lang/Object;

.field private final mClock:Lemp;

.field private final mDictionaryCache:Lczu;

.field private final mFetcher:Lczx;

.field private final mGsaConfigFlags:Lchk;

.field private final mNetworkInformation:Lgno;


# direct methods
.method public constructor <init>(Lczx;Lczu;Lemp;Lchk;Lgno;)V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lczz;->dK:Ljava/lang/Object;

    .line 67
    new-instance v0, Ldaa;

    invoke-direct {v0, p0}, Ldaa;-><init>(Lczz;)V

    iput-object v0, p0, Lczz;->boC:Lefk;

    .line 75
    new-instance v0, Ldab;

    invoke-direct {v0, p0}, Ldab;-><init>(Lczz;)V

    iput-object v0, p0, Lczz;->boD:Lefk;

    .line 100
    iput-object p1, p0, Lczz;->mFetcher:Lczx;

    .line 101
    iput-object p2, p0, Lczz;->mDictionaryCache:Lczu;

    .line 102
    iput-object p3, p0, Lczz;->mClock:Lemp;

    .line 103
    iput-object p4, p0, Lczz;->mGsaConfigFlags:Lchk;

    .line 104
    iput-object p5, p0, Lczz;->mNetworkInformation:Lgno;

    .line 105
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lczz;->boE:Ljava/util/Map;

    .line 106
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lczz;->boG:Ljava/util/Map;

    .line 107
    return-void
.end method

.method private Ur()Z
    .locals 2

    .prologue
    .line 114
    iget-object v1, p0, Lczz;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 115
    :try_start_0
    iget-boolean v0, p0, Lczz;->boF:Z

    if-nez v0, :cond_0

    .line 117
    const/4 v0, 0x0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    :goto_0
    return v0

    .line 119
    :cond_0
    monitor-exit v1

    .line 120
    const/4 v0, 0x1

    goto :goto_0

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/net/URL;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 389
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 391
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 392
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 393
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 394
    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 397
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 398
    invoke-virtual {p1}, Ljava/net/URL;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 401
    :cond_1
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private af(Ljava/lang/String;Ljava/lang/String;)Lczt;
    .locals 8
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 337
    invoke-static {v7}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 338
    iget-object v2, p0, Lczz;->dK:Ljava/lang/Object;

    monitor-enter v2

    .line 339
    :try_start_0
    iget-object v0, p0, Lczz;->boE:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lczt;

    .line 340
    iget-object v4, v0, Lczt;->bow:Lczr;

    .line 345
    iget-object v5, v4, Lczr;->boq:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v4, v4, Lczr;->acp:Ljava/lang/String;

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 346
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 349
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 351
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 352
    const/4 v0, 0x0

    .line 361
    :goto_1
    return-object v0

    .line 355
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v7, :cond_3

    .line 356
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lczt;

    goto :goto_1

    .line 361
    :cond_3
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lczt;

    goto :goto_1
.end method


# virtual methods
.method public final Uq()V
    .locals 6

    .prologue
    .line 110
    iget-object v0, p0, Lczz;->mDictionaryCache:Lczu;

    iget-object v1, p0, Lczz;->boD:Lefk;

    iget-object v2, v0, Lczu;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v3, Lczv;

    const-string v4, "Load all dictionaries"

    const/4 v5, 0x0

    new-array v5, v5, [I

    invoke-direct {v3, v0, v4, v5, v1}, Lczv;-><init>(Lczu;Ljava/lang/String;[ILefk;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 111
    return-void
.end method

.method public a(Ldyj;Ljava/net/URL;Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 240
    const-string v0, "Get-Dictionary"

    invoke-virtual {p1, v0}, Ldyj;->aF(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 241
    const-string v0, "Content-Encoding"

    invoke-virtual {p1, v0}, Ldyj;->aF(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 246
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 248
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, p2}, Lczz;->a(Ljava/lang/String;Ljava/net/URL;)Ljava/lang/String;

    move-result-object v1

    .line 251
    iget-object v5, p0, Lczz;->dK:Ljava/lang/Object;

    monitor-enter v5

    .line 254
    :try_start_0
    iget-object v0, p0, Lczz;->boG:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 255
    iget-object v0, p0, Lczz;->boG:Ljava/util/Map;

    sget-object v6, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v3

    .line 258
    :goto_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lczz;->mFetcher:Lczx;

    iget-object v5, p0, Lczz;->boC:Lefk;

    invoke-virtual {v0, v1, v5}, Lczx;->a(Ljava/lang/String;Lefk;)V

    .line 271
    :cond_0
    if-eqz v4, :cond_3

    .line 272
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    move v4, v2

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 273
    const-string v6, "sdch"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v4, v3

    .line 274
    goto :goto_1

    .line 258
    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    .line 275
    :cond_1
    const-string v6, "gzip"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v1, v3

    .line 276
    goto :goto_1

    .line 278
    :cond_2
    const-string v6, "Sdch.Manager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unknown content encoding: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v6, v0, v7}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    move v1, v2

    move v4, v2

    .line 284
    :cond_4
    if-eqz v1, :cond_5

    .line 286
    new-instance v1, Ldac;

    invoke-direct {v1, p0, p3}, Ldac;-><init>(Lczz;Ljava/io/InputStream;)V

    .line 291
    :goto_2
    if-eqz v4, :cond_6

    .line 293
    new-instance v0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;

    const/16 v2, 0x1000

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;-><init>(Ljava/io/InputStream;Lczz;I)V

    .line 296
    :goto_3
    return-object v0

    :cond_5
    move-object v1, p3

    .line 288
    goto :goto_2

    :cond_6
    move-object v0, v1

    .line 296
    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;Ljwq;)V
    .locals 4

    .prologue
    .line 181
    iget-object v0, p0, Lczz;->mGsaConfigFlags:Lchk;

    iget-object v1, p0, Lczz;->mNetworkInformation:Lgno;

    invoke-virtual {v1}, Lgno;->aAn()I

    move-result v1

    invoke-virtual {v0, v1}, Lchk;->eJ(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    invoke-direct {p0}, Lczz;->Ur()Z

    move-result v0

    if-nez v0, :cond_2

    .line 187
    const/16 v0, 0x95

    invoke-static {v0}, Lege;->ht(I)V

    goto :goto_0

    .line 192
    :cond_2
    const/16 v0, 0x7e

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lczz;->mNetworkInformation:Lgno;

    invoke-virtual {v1}, Lgno;->aAn()I

    move-result v1

    invoke-virtual {v0, v1}, Litu;->mG(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 199
    iget-object v0, p2, Ljwq;->eJv:[Ljwo;

    new-instance v1, Ljwo;

    invoke-direct {v1}, Ljwo;-><init>()V

    const-string v2, "Accept-Encoding"

    invoke-virtual {v1, v2}, Ljwo;->zs(Ljava/lang/String;)Ljwo;

    move-result-object v1

    const-string v2, "sdch"

    invoke-virtual {v1, v2}, Ljwo;->zt(Ljava/lang/String;)Ljwo;

    move-result-object v1

    invoke-static {v0, v1}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljwo;

    iput-object v0, p2, Ljwq;->eJv:[Ljwo;

    .line 202
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lczz;->af(Ljava/lang/String;Ljava/lang/String;)Lczt;

    move-result-object v0

    .line 205
    if-eqz v0, :cond_0

    .line 206
    iget-object v0, v0, Lczt;->bow:Lczr;

    iget-object v0, v0, Lczr;->boo:Ljava/lang/String;

    .line 208
    const/16 v1, 0x80

    invoke-static {v1}, Lege;->hs(I)Litu;

    move-result-object v1

    invoke-virtual {v1, v0}, Litu;->pO(Ljava/lang/String;)Litu;

    move-result-object v1

    invoke-static {v1}, Lege;->a(Litu;)V

    .line 211
    iget-object v1, p2, Ljwq;->eJv:[Ljwo;

    new-instance v2, Ljwo;

    invoke-direct {v2}, Ljwo;-><init>()V

    const-string v3, "Avail-Dictionary"

    invoke-virtual {v2, v3}, Ljwo;->zs(Ljava/lang/String;)Ljwo;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljwo;->zt(Ljava/lang/String;)Ljwo;

    move-result-object v0

    invoke-static {v1, v0}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljwo;

    iput-object v0, p2, Ljwq;->eJv:[Ljwo;

    goto :goto_0
.end method

.method final a(Landroid/util/Pair;)V
    .locals 3
    .param p1    # Landroid/util/Pair;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 311
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, [B

    iget-object v2, p0, Lczz;->mClock:Lemp;

    invoke-static {v0, v1, v2}, Lczt;->a(Ljava/lang/String;[BLemp;)Lczt;

    move-result-object v1

    .line 313
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lczz;->a(Ljava/lang/String;Lczt;)V

    .line 314
    return-void
.end method

.method public final a(Lbzh;)V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lczz;->mGsaConfigFlags:Lchk;

    iget-object v1, p0, Lczz;->mNetworkInformation:Lgno;

    invoke-virtual {v1}, Lgno;->aAn()I

    move-result v1

    invoke-virtual {v0, v1}, Lchk;->eK(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    invoke-direct {p0}, Lczz;->Ur()Z

    move-result v0

    if-nez v0, :cond_2

    .line 154
    const/16 v0, 0x95

    invoke-static {v0}, Lege;->ht(I)V

    goto :goto_0

    .line 159
    :cond_2
    const/16 v0, 0x7e

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lczz;->mNetworkInformation:Lgno;

    invoke-virtual {v1}, Lgno;->aAn()I

    move-result v1

    invoke-virtual {v0, v1}, Litu;->mG(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 163
    const-string v0, "Accept-Encoding"

    const-string v1, "gzip, sdch"

    invoke-interface {p1, v0, v1}, Lbzh;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    invoke-interface {p1}, Lbzh;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lbzh;->getURL()Ljava/net/URL;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lczz;->af(Ljava/lang/String;Ljava/lang/String;)Lczt;

    move-result-object v0

    .line 168
    if-eqz v0, :cond_0

    .line 169
    iget-object v0, v0, Lczt;->bow:Lczr;

    iget-object v0, v0, Lczr;->boo:Ljava/lang/String;

    .line 171
    const/16 v1, 0x80

    invoke-static {v1}, Lege;->hs(I)Litu;

    move-result-object v1

    invoke-virtual {v1, v0}, Litu;->pO(Ljava/lang/String;)Litu;

    move-result-object v1

    invoke-static {v1}, Lege;->a(Litu;)V

    .line 174
    const-string v1, "Avail-Dictionary"

    invoke-interface {p1, v1, v0}, Lbzh;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lczt;)V
    .locals 3

    .prologue
    .line 318
    if-eqz p2, :cond_0

    .line 321
    iget-object v1, p0, Lczz;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 322
    :try_start_0
    iget-object v0, p0, Lczz;->boE:Ljava/util/Map;

    iget-object v2, p2, Lczt;->bow:Lczr;

    iget-object v2, v2, Lczr;->bop:Ljava/lang/String;

    invoke-interface {v0, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    iget-object v0, p0, Lczz;->boG:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330
    iget-object v0, p0, Lczz;->mDictionaryCache:Lczu;

    invoke-virtual {v0, p2}, Lczu;->a(Lczt;)V

    .line 332
    :cond_0
    return-void

    .line 326
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Lbzh;)Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 220
    invoke-static {p1}, Ldmc;->e(Lbzh;)Ljava/io/InputStream;

    move-result-object v0

    .line 221
    new-instance v1, Ldyj;

    invoke-interface {p1}, Lbzh;->getHeaderFields()Ljava/util/Map;

    move-result-object v2

    invoke-direct {v1, v2}, Ldyj;-><init>(Ljava/util/Map;)V

    invoke-interface {p1}, Lbzh;->getURL()Ljava/net/URL;

    move-result-object v2

    invoke-virtual {p0, v1, v2, v0}, Lczz;->a(Ldyj;Ljava/net/URL;Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public final jo(Ljava/lang/String;)Lczt;
    .locals 2

    .prologue
    .line 124
    iget-object v1, p0, Lczz;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 126
    :try_start_0
    iget-object v0, p0, Lczz;->boE:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lczt;

    .line 127
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    return-object v0

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final q(Ljava/util/List;)V
    .locals 5

    .prologue
    .line 366
    iget-object v1, p0, Lczz;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 367
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lczt;

    .line 368
    iget-object v3, p0, Lczz;->boE:Ljava/util/Map;

    iget-object v4, v0, Lczt;->bow:Lczr;

    iget-object v4, v4, Lczr;->bop:Ljava/lang/String;

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 373
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 372
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lczz;->boF:Z

    .line 373
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 375
    const-string v0, "Sdch.Manager"

    const-string v1, "SDCH cache load complete."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x4

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 376
    return-void
.end method
