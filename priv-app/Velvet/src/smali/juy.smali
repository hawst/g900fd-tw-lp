.class public final Ljuy;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eFQ:[Ljuy;


# instance fields
.field private aez:I

.field private eFP:I

.field private eFR:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2223
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2224
    iput v1, p0, Ljuy;->aez:I

    const/4 v0, 0x1

    iput v0, p0, Ljuy;->eFP:I

    iput-boolean v1, p0, Ljuy;->eFR:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljuy;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljuy;->eCz:I

    .line 2225
    return-void
.end method

.method public static buS()[Ljuy;
    .locals 2

    .prologue
    .line 2172
    sget-object v0, Ljuy;->eFQ:[Ljuy;

    if-nez v0, :cond_1

    .line 2173
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 2175
    :try_start_0
    sget-object v0, Ljuy;->eFQ:[Ljuy;

    if-nez v0, :cond_0

    .line 2176
    const/4 v0, 0x0

    new-array v0, v0, [Ljuy;

    sput-object v0, Ljuy;->eFQ:[Ljuy;

    .line 2178
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2180
    :cond_1
    sget-object v0, Ljuy;->eFQ:[Ljuy;

    return-object v0

    .line 2178
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 2163
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljuy;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljuy;->eFP:I

    iget v0, p0, Ljuy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljuy;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljuy;->eFR:Z

    iget v0, p0, Ljuy;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljuy;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 2239
    iget v0, p0, Ljuy;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2240
    const/4 v0, 0x1

    iget v1, p0, Ljuy;->eFP:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2242
    :cond_0
    iget v0, p0, Ljuy;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2243
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljuy;->eFR:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 2245
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2246
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 2250
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2251
    iget v1, p0, Ljuy;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2252
    const/4 v1, 0x1

    iget v2, p0, Ljuy;->eFP:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2255
    :cond_0
    iget v1, p0, Ljuy;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2256
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljuy;->eFR:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2259
    :cond_1
    return v0
.end method
