.class public final Lcyy;
.super Landroid/app/Fragment;
.source "PG"

# interfaces
.implements Ldjw;


# instance fields
.field volatile bnA:Z

.field bnz:Lcza;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 213
    return-void
.end method


# virtual methods
.method public final Ud()Z
    .locals 1

    .prologue
    .line 207
    iget-boolean v0, p0, Lcyy;->bnA:Z

    return v0
.end method

.method public final jd(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 198
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcyy;->bnz:Lcza;

    invoke-virtual {v0}, Lcza;->clear()V

    .line 203
    :goto_0
    return-void

    .line 202
    :cond_0
    new-instance v0, Lcyz;

    invoke-direct {v0, p0, p1}, Lcyz;-><init>(Lcyy;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcyz;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 181
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 183
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 184
    new-instance v1, Lcza;

    invoke-virtual {p0}, Lcyy;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcza;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v1, p0, Lcyy;->bnz:Lcza;

    .line 185
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 175
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 176
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcyy;->setRetainInstance(Z)V

    .line 177
    return-void
.end method
