.class public final Lfvf;
.super Lfus;
.source "PG"


# instance fields
.field private final mPhotoWithAttributionDecorator:Lgbd;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lgbr;Lgbd;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3, p4}, Lfus;-><init>(Lizj;Lftz;Lemp;Lgbr;)V

    .line 33
    iput-object p5, p0, Lfvf;->mPhotoWithAttributionDecorator:Lgbd;

    .line 34
    return-void
.end method

.method public constructor <init>(Lizq;Lftz;Lemp;Lgbr;Lgbd;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lfus;-><init>(Lizq;Lftz;Lemp;Lgbr;)V

    .line 42
    iput-object p5, p0, Lfvf;->mPhotoWithAttributionDecorator:Lgbd;

    .line 43
    return-void
.end method

.method private a(Landroid/content/Context;Liys;II)Lanj;
    .locals 4

    .prologue
    .line 49
    new-instance v0, Lanj;

    invoke-direct {v0}, Lanj;-><init>()V

    .line 50
    iget-object v1, p2, Liys;->dQX:Ljhe;

    if-eqz v1, :cond_0

    .line 51
    iget-object v1, p0, Lfvf;->mStringEvaluator:Lgbr;

    iget-object v2, p2, Liys;->dQX:Ljhe;

    invoke-virtual {v1, p1, v2}, Lgbr;->b(Landroid/content/Context;Ljhe;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lanj;->ao(Ljava/lang/String;)Lanj;

    .line 54
    :cond_0
    iget-object v1, p2, Liys;->dQY:Ljhe;

    if-eqz v1, :cond_1

    .line 55
    iget-object v1, p0, Lfvf;->mStringEvaluator:Lgbr;

    iget-object v2, p2, Liys;->dQY:Ljhe;

    invoke-virtual {v1, p1, v2}, Lgbr;->b(Landroid/content/Context;Ljhe;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lanj;->ap(Ljava/lang/String;)Lanj;

    .line 58
    :cond_1
    iget-object v1, p2, Liys;->dRa:Ljhe;

    if-eqz v1, :cond_2

    .line 59
    iget-object v1, p0, Lfvf;->mStringEvaluator:Lgbr;

    iget-object v2, p2, Liys;->dRa:Ljhe;

    invoke-virtual {v1, p1, v2}, Lgbr;->b(Landroid/content/Context;Ljhe;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lanj;->aq(Ljava/lang/String;)Lanj;

    .line 62
    :cond_2
    iget-object v1, p2, Liys;->aiS:Ljcn;

    if-eqz v1, :cond_3

    .line 64
    iget-object v1, p0, Lfvf;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v2, p2, Liys;->aiS:Ljcn;

    invoke-virtual {v1, p1, v2, p3, p4}, Lgbd;->b(Landroid/content/Context;Ljcn;II)Laoi;

    move-result-object v1

    .line 69
    iget-object v2, p0, Lfvf;->mPhotoWithAttributionDecorator:Lgbd;

    const/16 v2, 0x4c

    iget-object v3, p2, Liys;->aiS:Ljcn;

    invoke-static {v2, v3, v1}, Lgbd;->a(ILjcn;Laoi;)V

    .line 71
    iget-object v2, p0, Lfvf;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v2, p2, Liys;->aiS:Ljcn;

    invoke-static {v2, v1}, Lgbd;->a(Ljcn;Laoi;)V

    .line 73
    iput-object v1, v0, Lanj;->ahM:Laoi;

    .line 76
    :cond_3
    return-object v0
.end method

.method private static a(Lanh;Lizj;Liys;)V
    .locals 3

    .prologue
    const v2, 0x7f0201ab

    const/4 v1, 0x3

    .line 151
    iget-object v0, p2, Liys;->dRc:[Lixx;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 152
    new-instance v0, Lfsa;

    invoke-direct {v0, v1}, Lfsa;-><init>(I)V

    invoke-virtual {v0, v2}, Lfsa;->jB(I)Lfsa;

    move-result-object v0

    iget-object v1, p2, Liys;->dRc:[Lixx;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lfsa;->b(Lixx;)Lani;

    move-result-object v0

    iput-object v0, p0, Lanh;->ahs:Lani;

    .line 160
    :cond_0
    :goto_0
    iget-object v0, p0, Lanh;->ahs:Lani;

    iget-object v0, v0, Lani;->ahE:Laoe;

    if-eqz v0, :cond_1

    .line 161
    iget-object v0, p0, Lanh;->ahs:Lani;

    iget-object v0, v0, Lani;->ahE:Laoe;

    invoke-virtual {p2}, Liys;->bcs()Z

    move-result v1

    invoke-virtual {v0, v1}, Laoe;->bb(Z)Laoe;

    .line 163
    :cond_1
    return-void

    .line 155
    :cond_2
    iget-object v0, p1, Lizj;->dUp:Lixx;

    if-eqz v0, :cond_0

    .line 156
    new-instance v0, Lfsa;

    invoke-direct {v0, v1}, Lfsa;-><init>(I)V

    invoke-virtual {v0, v2}, Lfsa;->jB(I)Lfsa;

    move-result-object v0

    iget-object v1, p1, Lizj;->dUp:Lixx;

    invoke-virtual {v0, v1}, Lfsa;->b(Lixx;)Lani;

    move-result-object v0

    iput-object v0, p0, Lanh;->ahs:Lani;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lizj;)Lanh;
    .locals 4

    .prologue
    const v3, 0x7f0d015b

    .line 81
    iget-object v0, p2, Lizj;->dUa:Liys;

    .line 83
    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    .line 84
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lanh;->cm(I)Lanh;

    .line 85
    invoke-direct {p0, p1, v0, v3, v3}, Lfvf;->a(Landroid/content/Context;Liys;II)Lanj;

    move-result-object v2

    iput-object v2, v1, Lanh;->agF:Lanj;

    .line 88
    iput-object p2, v1, Lanh;->ahu:Lizj;

    .line 89
    invoke-static {v1, p2, v0}, Lfvf;->a(Lanh;Lizj;Liys;)V

    .line 90
    return-object v1
.end method

.method public final aV(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    .line 97
    iget-object v1, v0, Lizj;->dUc:Liyt;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lizj;->dUc:Liyt;

    iget-object v1, v1, Liyt;->dQX:Ljhe;

    if-eqz v1, :cond_0

    .line 99
    iget-object v1, p0, Lfvf;->mStringEvaluator:Lgbr;

    iget-object v0, v0, Lizj;->dUc:Liyt;

    iget-object v0, v0, Liyt;->dQX:Ljhe;

    invoke-virtual {v1, p1, v0}, Lgbr;->b(Landroid/content/Context;Ljhe;)Ljava/lang/String;

    move-result-object v0

    .line 101
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aX(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    .line 108
    invoke-virtual {v0}, Lizj;->bcX()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lizj;->getReason()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lizj;)[Lanh;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 113
    iget-object v1, p2, Lizj;->dUa:Liys;

    .line 115
    new-instance v2, Lans;

    invoke-direct {v2}, Lans;-><init>()V

    .line 116
    const v0, 0x7f0d012b

    const v3, 0x7f0d012c

    invoke-direct {p0, p1, v1, v0, v3}, Lfvf;->a(Landroid/content/Context;Liys;II)Lanj;

    move-result-object v0

    iput-object v0, v2, Lans;->aiw:Lanj;

    .line 119
    iget-object v0, v1, Liys;->dRb:Ljhe;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lfvf;->mStringEvaluator:Lgbr;

    iget-object v3, v1, Liys;->dRb:Ljhe;

    invoke-virtual {v0, p1, v3}, Lgbr;->b(Landroid/content/Context;Ljhe;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lans;->aU(Ljava/lang/String;)Lans;

    .line 126
    :cond_0
    iget-object v0, p0, Lfuz;->mEntryTreeNode:Lizq;

    if-eqz v0, :cond_3

    .line 127
    iget-object v0, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v0, v0, Lizq;->dUZ:Lizj;

    invoke-virtual {v0}, Lizj;->getReason()Ljava/lang/String;

    move-result-object v0

    .line 131
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 132
    iget-object v3, v2, Lans;->aiw:Lanj;

    invoke-virtual {v3, v0}, Lanj;->ar(Ljava/lang/String;)Lanj;

    .line 135
    :cond_1
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 136
    const/16 v3, 0x9

    invoke-virtual {v0, v3}, Lanh;->cm(I)Lanh;

    .line 137
    iput-object v2, v0, Lanh;->agG:Lans;

    .line 138
    iput-object p2, v0, Lanh;->ahu:Lizj;

    .line 140
    invoke-virtual {v1}, Liys;->oX()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 141
    invoke-virtual {v0, v4}, Lanh;->aY(Z)Lanh;

    .line 144
    :cond_2
    invoke-static {v0, p2, v1}, Lfvf;->a(Lanh;Lizj;Liys;)V

    .line 146
    new-array v1, v4, [Lanh;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    return-object v1

    .line 129
    :cond_3
    invoke-virtual {p2}, Lizj;->getReason()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
