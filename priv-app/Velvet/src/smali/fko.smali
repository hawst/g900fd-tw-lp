.class public final Lfko;
.super Lfjz;
.source "PG"


# instance fields
.field private final cud:Z

.field private final cue:Z


# direct methods
.method public constructor <init>(Lfzw;ZZ)V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    invoke-direct {p0, v0, p1}, Lfjz;-><init>(Lizj;Lfzw;)V

    .line 28
    iput-boolean p2, p0, Lfko;->cud:Z

    .line 29
    iput-boolean p3, p0, Lfko;->cue:Z

    .line 30
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v7, 0x8

    .line 35
    const v0, 0x7f0400d7

    const/4 v1, 0x0

    invoke-virtual {p3, v0, p4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 37
    const v0, 0x7f11028e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 38
    iget-boolean v1, p0, Lfko;->cud:Z

    if-eqz v1, :cond_0

    .line 39
    const v1, 0x7f0a0479

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v3, 0x7f0a047a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f0a047b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const v5, 0x7f0a047c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const v6, 0x7f0a047d

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v1, v3, v4, v5, v6}, Lesi;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 44
    :goto_0
    const v0, 0x7f11028f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 45
    iget-boolean v1, p0, Lfko;->cue:Z

    if-eqz v1, :cond_1

    .line 46
    new-instance v1, Lfkp;

    invoke-direct {v1, p0, p1}, Lfkp;-><init>(Lfko;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    :goto_1
    return-object v2

    .line 41
    :cond_0
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 54
    :cond_1
    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1
.end method
