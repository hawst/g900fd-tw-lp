.class public final Ljwb;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private agv:I

.field private eGk:I

.field private eGl:I

.field private eGo:F

.field private eGp:F

.field private eIA:Z

.field public eIB:Ljtl;

.field private eIC:I

.field private eID:Z

.field private eIE:I

.field private eIF:F

.field private eIG:Z

.field private eIH:Ljava/lang/String;

.field private eII:Z

.field private eIJ:Z

.field private eIK:Z

.field private eIL:Z

.field private eIM:Ljava/lang/String;

.field private eIN:I

.field private eIO:Ljava/lang/String;

.field private eIP:I

.field private eIx:Ljava/lang/String;

.field private eIz:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15
    const/16 v0, 0xb

    const-class v1, Ljwb;

    const v2, 0xa4f274a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    .line 33
    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 473
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 474
    iput v1, p0, Ljwb;->aez:I

    iput v1, p0, Ljwb;->agv:I

    const/high16 v0, 0x45fa0000    # 8000.0f

    iput v0, p0, Ljwb;->eGo:F

    iput-boolean v2, p0, Ljwb;->eIz:Z

    iput-boolean v1, p0, Ljwb;->eIA:Z

    iput-object v4, p0, Ljwb;->eIB:Ljtl;

    iput v1, p0, Ljwb;->eIC:I

    iput-boolean v2, p0, Ljwb;->eID:Z

    iput v1, p0, Ljwb;->eIE:I

    iput v3, p0, Ljwb;->eIF:F

    iput v2, p0, Ljwb;->eGk:I

    iput-boolean v1, p0, Ljwb;->eIG:Z

    const-string v0, ""

    iput-object v0, p0, Ljwb;->eIH:Ljava/lang/String;

    iput-boolean v1, p0, Ljwb;->eII:Z

    iput-boolean v1, p0, Ljwb;->eIJ:Z

    iput-boolean v1, p0, Ljwb;->eIK:Z

    iput-boolean v1, p0, Ljwb;->eIL:Z

    const-string v0, ""

    iput-object v0, p0, Ljwb;->eIx:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljwb;->eIM:Ljava/lang/String;

    iput v1, p0, Ljwb;->eIN:I

    iput v3, p0, Ljwb;->eGp:F

    iput v2, p0, Ljwb;->eGl:I

    const-string v0, ""

    iput-object v0, p0, Ljwb;->eIO:Ljava/lang/String;

    iput v1, p0, Ljwb;->eIP:I

    iput-object v4, p0, Ljwb;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljwb;->eCz:I

    .line 475
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljwb;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljwb;->agv:I

    iget v0, p0, Ljwb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwb;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljwb;->eGo:F

    iget v0, p0, Ljwb;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwb;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljwb;->eIz:Z

    iget v0, p0, Ljwb;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljwb;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljwb;->eIA:Z

    iget v0, p0, Ljwb;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljwb;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljwb;->eIB:Ljtl;

    if-nez v0, :cond_1

    new-instance v0, Ljtl;

    invoke-direct {v0}, Ljtl;-><init>()V

    iput-object v0, p0, Ljwb;->eIB:Ljtl;

    :cond_1
    iget-object v0, p0, Ljwb;->eIB:Ljtl;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljwb;->eIC:I

    iget v0, p0, Ljwb;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljwb;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljwb;->eID:Z

    iget v0, p0, Ljwb;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljwb;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljwb;->eIE:I

    iget v0, p0, Ljwb;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljwb;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljwb;->eIF:F

    iget v0, p0, Ljwb;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljwb;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljwb;->eGk:I

    iget v0, p0, Ljwb;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljwb;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljwb;->eIG:Z

    iget v0, p0, Ljwb;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljwb;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwb;->eIH:Ljava/lang/String;

    iget v0, p0, Ljwb;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljwb;->aez:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljwb;->eII:Z

    iget v0, p0, Ljwb;->aez:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Ljwb;->aez:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljwb;->eIL:Z

    iget v0, p0, Ljwb;->aez:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Ljwb;->aez:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwb;->eIx:Ljava/lang/String;

    iget v0, p0, Ljwb;->aez:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Ljwb;->aez:I

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwb;->eIM:Ljava/lang/String;

    iget v0, p0, Ljwb;->aez:I

    const/high16 v1, 0x10000

    or-int/2addr v0, v1

    iput v0, p0, Ljwb;->aez:I

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljwb;->eIK:Z

    iget v0, p0, Ljwb;->aez:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Ljwb;->aez:I

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_0

    :pswitch_1
    iput v0, p0, Ljwb;->eIN:I

    iget v0, p0, Ljwb;->aez:I

    const/high16 v1, 0x20000

    or-int/2addr v0, v1

    iput v0, p0, Ljwb;->aez:I

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljwb;->eGp:F

    iget v0, p0, Ljwb;->aez:I

    const/high16 v1, 0x40000

    or-int/2addr v0, v1

    iput v0, p0, Ljwb;->aez:I

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljwb;->eGl:I

    iget v0, p0, Ljwb;->aez:I

    const/high16 v1, 0x80000

    or-int/2addr v0, v1

    iput v0, p0, Ljwb;->aez:I

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljwb;->eIJ:Z

    iget v0, p0, Ljwb;->aez:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Ljwb;->aez:I

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwb;->eIO:Ljava/lang/String;

    iget v0, p0, Ljwb;->aez:I

    const/high16 v1, 0x100000

    or-int/2addr v0, v1

    iput v0, p0, Ljwb;->aez:I

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto/16 :goto_0

    :pswitch_2
    iput v0, p0, Ljwb;->eIP:I

    iget v0, p0, Ljwb;->aez:I

    const/high16 v1, 0x200000

    or-int/2addr v0, v1

    iput v0, p0, Ljwb;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4d -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0x9d -> :sswitch_13
        0xa0 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xb2 -> :sswitch_16
        0xb8 -> :sswitch_17
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 510
    iget v0, p0, Ljwb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 511
    const/4 v0, 0x1

    iget v1, p0, Ljwb;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 513
    :cond_0
    iget v0, p0, Ljwb;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 514
    const/4 v0, 0x2

    iget v1, p0, Ljwb;->eGo:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 516
    :cond_1
    iget v0, p0, Ljwb;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 517
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljwb;->eIz:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 519
    :cond_2
    iget v0, p0, Ljwb;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 520
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljwb;->eIA:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 522
    :cond_3
    iget-object v0, p0, Ljwb;->eIB:Ljtl;

    if-eqz v0, :cond_4

    .line 523
    const/4 v0, 0x5

    iget-object v1, p0, Ljwb;->eIB:Ljtl;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 525
    :cond_4
    iget v0, p0, Ljwb;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 526
    const/4 v0, 0x6

    iget v1, p0, Ljwb;->eIC:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 528
    :cond_5
    iget v0, p0, Ljwb;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_6

    .line 529
    const/4 v0, 0x7

    iget-boolean v1, p0, Ljwb;->eID:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 531
    :cond_6
    iget v0, p0, Ljwb;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_7

    .line 532
    const/16 v0, 0x8

    iget v1, p0, Ljwb;->eIE:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 534
    :cond_7
    iget v0, p0, Ljwb;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_8

    .line 535
    const/16 v0, 0x9

    iget v1, p0, Ljwb;->eIF:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 537
    :cond_8
    iget v0, p0, Ljwb;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_9

    .line 538
    const/16 v0, 0xa

    iget v1, p0, Ljwb;->eGk:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 540
    :cond_9
    iget v0, p0, Ljwb;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_a

    .line 541
    const/16 v0, 0xb

    iget-boolean v1, p0, Ljwb;->eIG:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 543
    :cond_a
    iget v0, p0, Ljwb;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_b

    .line 544
    const/16 v0, 0xc

    iget-object v1, p0, Ljwb;->eIH:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 546
    :cond_b
    iget v0, p0, Ljwb;->aez:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_c

    .line 547
    const/16 v0, 0xd

    iget-boolean v1, p0, Ljwb;->eII:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 549
    :cond_c
    iget v0, p0, Ljwb;->aez:I

    and-int/lit16 v0, v0, 0x4000

    if-eqz v0, :cond_d

    .line 550
    const/16 v0, 0xe

    iget-boolean v1, p0, Ljwb;->eIL:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 552
    :cond_d
    iget v0, p0, Ljwb;->aez:I

    const v1, 0x8000

    and-int/2addr v0, v1

    if-eqz v0, :cond_e

    .line 553
    const/16 v0, 0xf

    iget-object v1, p0, Ljwb;->eIx:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 555
    :cond_e
    iget v0, p0, Ljwb;->aez:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    if-eqz v0, :cond_f

    .line 556
    const/16 v0, 0x10

    iget-object v1, p0, Ljwb;->eIM:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 558
    :cond_f
    iget v0, p0, Ljwb;->aez:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_10

    .line 559
    const/16 v0, 0x11

    iget-boolean v1, p0, Ljwb;->eIK:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 561
    :cond_10
    iget v0, p0, Ljwb;->aez:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    if-eqz v0, :cond_11

    .line 562
    const/16 v0, 0x12

    iget v1, p0, Ljwb;->eIN:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 564
    :cond_11
    iget v0, p0, Ljwb;->aez:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    if-eqz v0, :cond_12

    .line 565
    const/16 v0, 0x13

    iget v1, p0, Ljwb;->eGp:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 567
    :cond_12
    iget v0, p0, Ljwb;->aez:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    if-eqz v0, :cond_13

    .line 568
    const/16 v0, 0x14

    iget v1, p0, Ljwb;->eGl:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 570
    :cond_13
    iget v0, p0, Ljwb;->aez:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_14

    .line 571
    const/16 v0, 0x15

    iget-boolean v1, p0, Ljwb;->eIJ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 573
    :cond_14
    iget v0, p0, Ljwb;->aez:I

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    if-eqz v0, :cond_15

    .line 574
    const/16 v0, 0x16

    iget-object v1, p0, Ljwb;->eIO:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 576
    :cond_15
    iget v0, p0, Ljwb;->aez:I

    const/high16 v1, 0x200000

    and-int/2addr v0, v1

    if-eqz v0, :cond_16

    .line 577
    const/16 v0, 0x17

    iget v1, p0, Ljwb;->eIP:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 579
    :cond_16
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 580
    return-void
.end method

.method public final ab(F)Ljwb;
    .locals 1

    .prologue
    .line 65
    iput p1, p0, Ljwb;->eGo:F

    .line 66
    iget v0, p0, Ljwb;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwb;->aez:I

    .line 67
    return-object p0
.end method

.method public final bvi()F
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Ljwb;->eGo:F

    return v0
.end method

.method public final bvj()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Ljwb;->eIz:Z

    return v0
.end method

.method public final bvk()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Ljwb;->eIA:Z

    return v0
.end method

.method public final bvl()Z
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Ljwb;->eID:Z

    return v0
.end method

.method public final jc(Z)Ljwb;
    .locals 1

    .prologue
    .line 84
    iput-boolean p1, p0, Ljwb;->eIz:Z

    .line 85
    iget v0, p0, Ljwb;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljwb;->aez:I

    .line 86
    return-object p0
.end method

.method public final jd(Z)Ljwb;
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljwb;->eIA:Z

    .line 104
    iget v0, p0, Ljwb;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljwb;->aez:I

    .line 105
    return-object p0
.end method

.method public final je(Z)Ljwb;
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljwb;->eID:Z

    .line 145
    iget v0, p0, Ljwb;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljwb;->aez:I

    .line 146
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 584
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 585
    iget v1, p0, Ljwb;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 586
    const/4 v1, 0x1

    iget v2, p0, Ljwb;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 589
    :cond_0
    iget v1, p0, Ljwb;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 590
    const/4 v1, 0x2

    iget v2, p0, Ljwb;->eGo:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 593
    :cond_1
    iget v1, p0, Ljwb;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 594
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljwb;->eIz:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 597
    :cond_2
    iget v1, p0, Ljwb;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 598
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljwb;->eIA:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 601
    :cond_3
    iget-object v1, p0, Ljwb;->eIB:Ljtl;

    if-eqz v1, :cond_4

    .line 602
    const/4 v1, 0x5

    iget-object v2, p0, Ljwb;->eIB:Ljtl;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 605
    :cond_4
    iget v1, p0, Ljwb;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_5

    .line 606
    const/4 v1, 0x6

    iget v2, p0, Ljwb;->eIC:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 609
    :cond_5
    iget v1, p0, Ljwb;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_6

    .line 610
    const/4 v1, 0x7

    iget-boolean v2, p0, Ljwb;->eID:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 613
    :cond_6
    iget v1, p0, Ljwb;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_7

    .line 614
    const/16 v1, 0x8

    iget v2, p0, Ljwb;->eIE:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 617
    :cond_7
    iget v1, p0, Ljwb;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_8

    .line 618
    const/16 v1, 0x9

    iget v2, p0, Ljwb;->eIF:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 621
    :cond_8
    iget v1, p0, Ljwb;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_9

    .line 622
    const/16 v1, 0xa

    iget v2, p0, Ljwb;->eGk:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 625
    :cond_9
    iget v1, p0, Ljwb;->aez:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_a

    .line 626
    const/16 v1, 0xb

    iget-boolean v2, p0, Ljwb;->eIG:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 629
    :cond_a
    iget v1, p0, Ljwb;->aez:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_b

    .line 630
    const/16 v1, 0xc

    iget-object v2, p0, Ljwb;->eIH:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 633
    :cond_b
    iget v1, p0, Ljwb;->aez:I

    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_c

    .line 634
    const/16 v1, 0xd

    iget-boolean v2, p0, Ljwb;->eII:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 637
    :cond_c
    iget v1, p0, Ljwb;->aez:I

    and-int/lit16 v1, v1, 0x4000

    if-eqz v1, :cond_d

    .line 638
    const/16 v1, 0xe

    iget-boolean v2, p0, Ljwb;->eIL:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 641
    :cond_d
    iget v1, p0, Ljwb;->aez:I

    const v2, 0x8000

    and-int/2addr v1, v2

    if-eqz v1, :cond_e

    .line 642
    const/16 v1, 0xf

    iget-object v2, p0, Ljwb;->eIx:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 645
    :cond_e
    iget v1, p0, Ljwb;->aez:I

    const/high16 v2, 0x10000

    and-int/2addr v1, v2

    if-eqz v1, :cond_f

    .line 646
    const/16 v1, 0x10

    iget-object v2, p0, Ljwb;->eIM:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 649
    :cond_f
    iget v1, p0, Ljwb;->aez:I

    and-int/lit16 v1, v1, 0x2000

    if-eqz v1, :cond_10

    .line 650
    const/16 v1, 0x11

    iget-boolean v2, p0, Ljwb;->eIK:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 653
    :cond_10
    iget v1, p0, Ljwb;->aez:I

    const/high16 v2, 0x20000

    and-int/2addr v1, v2

    if-eqz v1, :cond_11

    .line 654
    const/16 v1, 0x12

    iget v2, p0, Ljwb;->eIN:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 657
    :cond_11
    iget v1, p0, Ljwb;->aez:I

    const/high16 v2, 0x40000

    and-int/2addr v1, v2

    if-eqz v1, :cond_12

    .line 658
    const/16 v1, 0x13

    iget v2, p0, Ljwb;->eGp:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 661
    :cond_12
    iget v1, p0, Ljwb;->aez:I

    const/high16 v2, 0x80000

    and-int/2addr v1, v2

    if-eqz v1, :cond_13

    .line 662
    const/16 v1, 0x14

    iget v2, p0, Ljwb;->eGl:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 665
    :cond_13
    iget v1, p0, Ljwb;->aez:I

    and-int/lit16 v1, v1, 0x1000

    if-eqz v1, :cond_14

    .line 666
    const/16 v1, 0x15

    iget-boolean v2, p0, Ljwb;->eIJ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 669
    :cond_14
    iget v1, p0, Ljwb;->aez:I

    const/high16 v2, 0x100000

    and-int/2addr v1, v2

    if-eqz v1, :cond_15

    .line 670
    const/16 v1, 0x16

    iget-object v2, p0, Ljwb;->eIO:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 673
    :cond_15
    iget v1, p0, Ljwb;->aez:I

    const/high16 v2, 0x200000

    and-int/2addr v1, v2

    if-eqz v1, :cond_16

    .line 674
    const/16 v1, 0x17

    iget v2, p0, Ljwb;->eIP:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 677
    :cond_16
    return v0
.end method

.method public final sE(I)Ljwb;
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    iput v0, p0, Ljwb;->agv:I

    .line 47
    iget v0, p0, Ljwb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwb;->aez:I

    .line 48
    return-object p0
.end method
