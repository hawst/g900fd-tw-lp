.class public final Lhkd;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final diY:Lhsx;


# instance fields
.field Oq:I

.field private aXe:Ljava/util/concurrent/Executor;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bbe:Ljava/lang/String;

.field private cZ:I

.field diT:Lglu;

.field diU:Lhsx;

.field private diV:Lhkg;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private diW:Ljava/lang/String;

.field private diX:Z

.field private mNetworkInformation:Lgno;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private mOfflineActionsManager:Lgkg;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private mRecognizer:Lgdb;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field mSoundManager:Lhik;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final mVoiceSearchServices:Lhhq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 653
    new-instance v0, Lhkf;

    invoke-direct {v0}, Lhkf;-><init>()V

    sput-object v0, Lhkd;->diY:Lhsx;

    return-void
.end method

.method public constructor <init>(Lhhq;)V
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    iput-object p1, p0, Lhkd;->mVoiceSearchServices:Lhhq;

    .line 129
    sget-object v0, Lhkd;->diY:Lhsx;

    iput-object v0, p0, Lhkd;->diU:Lhsx;

    .line 130
    return-void
.end method

.method private a(IZLandroid/net/Uri;)Lgnk;
    .locals 8
    .param p3    # Landroid/net/Uri;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x5

    const/4 v4, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 344
    new-instance v1, Lgmy;

    invoke-direct {v1}, Lgmy;-><init>()V

    .line 345
    invoke-virtual {p0, v4}, Lhkd;->li(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346
    iput-boolean v5, v1, Lgmy;->cPF:Z

    .line 348
    :cond_0
    iput-object p3, v1, Lgmy;->cPM:Landroid/net/Uri;

    .line 350
    new-instance v2, Lgnk;

    invoke-direct {v2}, Lgnk;-><init>()V

    .line 351
    iget-object v0, p0, Lhkd;->mVoiceSearchServices:Lhhq;

    iget-object v3, v0, Lhhq;->mSettings:Lhym;

    .line 352
    invoke-virtual {v3}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lgnk;->cQb:Ljava/lang/String;

    if-ne p1, v7, :cond_5

    sget-object v0, Lgjl;->cMC:Lgjl;

    :goto_0
    iput-object v0, v2, Lgnk;->cOM:Lgjl;

    if-eq p1, v6, :cond_1

    if-eq p1, v7, :cond_1

    if-ne p1, v4, :cond_6

    :cond_1
    sget-object v0, Lgjo;->cMP:Lgjo;

    :goto_1
    iput-object v0, v2, Lgnk;->cQe:Lgjo;

    iput-boolean v5, v2, Lgnk;->cQa:Z

    invoke-virtual {v2, p1}, Lgnk;->kt(I)Lgnk;

    move-result-object v0

    new-instance v4, Ljtp;

    invoke-direct {v4}, Ljtp;-><init>()V

    iput-object v4, v0, Lgnk;->cPw:Ljtp;

    invoke-virtual {v3}, Lhym;->aHP()Z

    move-result v3

    iput-boolean v3, v0, Lgnk;->cPu:Z

    invoke-virtual {v1}, Lgmy;->aHB()Lgmx;

    move-result-object v1

    iput-object v1, v0, Lgnk;->cPY:Lgmx;

    .line 360
    if-ne p1, v6, :cond_3

    .line 361
    iget-object v0, p0, Lhkd;->mVoiceSearchServices:Lhhq;

    iget-object v0, v0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DD()Lcjs;

    move-result-object v0

    invoke-virtual {v0}, Lcjs;->Mb()Ljava/lang/String;

    move-result-object v0

    .line 363
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 364
    iput-object v0, v2, Lgnk;->aTr:Ljava/lang/String;

    .line 367
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, v2, Lgnk;->cFx:Z

    .line 369
    :cond_3
    invoke-virtual {p0, v6}, Lhkd;->li(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 370
    iput-boolean v5, v2, Lgnk;->cQg:Z

    .line 372
    :cond_4
    return-object v2

    .line 352
    :cond_5
    sget-object v0, Lgjl;->cMB:Lgjl;

    goto :goto_0

    :cond_6
    sget-object v0, Lgjo;->cML:Lgjo;

    goto :goto_1
.end method

.method public static a(Lhhq;)Lhkd;
    .locals 1

    .prologue
    .line 123
    new-instance v0, Lhkd;

    invoke-direct {v0, p0}, Lhkd;-><init>(Lhhq;)V

    return-object v0
.end method

.method private a(Lglx;I)V
    .locals 6

    .prologue
    .line 257
    new-instance v0, Lhkg;

    invoke-direct {v0, p0, p1, p2}, Lhkg;-><init>(Lhkd;Lglx;I)V

    iput-object v0, p0, Lhkd;->diV:Lhkg;

    .line 260
    invoke-static {p2}, Lgnj;->ks(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "en-US"

    .line 268
    :goto_0
    iget-object v1, p0, Lhkd;->mOfflineActionsManager:Lgkg;

    iget-object v2, p0, Lhkd;->diV:Lhkg;

    const/4 v3, 0x2

    new-array v3, v3, [Lgjl;

    const/4 v4, 0x0

    sget-object v5, Lgjl;->cMB:Lgjl;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lgjl;->cMC:Lgjl;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v0, v3}, Lgkg;->a(Lefk;Ljava/lang/String;[Lgjl;)V

    .line 273
    return-void

    .line 260
    :cond_0
    iget-object v0, p0, Lhkd;->mVoiceSearchServices:Lhhq;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private auJ()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lhkd;->mRecognizer:Lgdb;

    if-nez v0, :cond_0

    .line 134
    iget-object v0, p0, Lhkd;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aOR()Lgdb;

    move-result-object v0

    iput-object v0, p0, Lhkd;->mRecognizer:Lgdb;

    .line 135
    iget-object v0, p0, Lhkd;->mVoiceSearchServices:Lhhq;

    iget-object v0, v0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->aus()Leqo;

    move-result-object v0

    iput-object v0, p0, Lhkd;->aXe:Ljava/util/concurrent/Executor;

    .line 136
    iget-object v0, p0, Lhkd;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aOZ()Lhik;

    move-result-object v0

    iput-object v0, p0, Lhkd;->mSoundManager:Lhik;

    .line 137
    iget-object v0, p0, Lhkd;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aPk()Lgkg;

    move-result-object v0

    iput-object v0, p0, Lhkd;->mOfflineActionsManager:Lgkg;

    .line 138
    iget-object v0, p0, Lhkd;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aAq()Lgno;

    move-result-object v0

    iput-object v0, p0, Lhkd;->mNetworkInformation:Lgno;

    .line 140
    :cond_0
    return-void
.end method

.method private b(Lgnj;Lglx;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 312
    invoke-virtual {p1}, Lgnj;->zK()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v3}, Lhkd;->ct(Z)V

    invoke-virtual {p1}, Lgnj;->getMode()I

    move-result v0

    iput v0, p0, Lhkd;->Oq:I

    iput-boolean v3, p0, Lhkd;->diX:Z

    if-eqz p2, :cond_0

    new-instance v0, Lglv;

    invoke-direct {v0}, Lglv;-><init>()V

    new-instance v2, Lhkh;

    invoke-direct {v2, p0, v1}, Lhkh;-><init>(Lhkd;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lglv;->b(Lglx;)V

    invoke-virtual {v0, p2}, Lglv;->b(Lglx;)V

    :goto_0
    new-instance v2, Lglu;

    invoke-direct {v2, v0}, Lglu;-><init>(Lglx;)V

    iput-object v2, p0, Lhkd;->diT:Lglu;

    iput-object v1, p0, Lhkd;->bbe:Ljava/lang/String;

    .line 314
    invoke-virtual {p0, v3}, Lhkd;->li(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 315
    iget-object v0, p0, Lhkd;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aPj()Lice;

    move-result-object v0

    iget-object v1, p0, Lhkd;->diW:Ljava/lang/String;

    new-instance v2, Lhke;

    const-string v3, "Really start listening"

    invoke-direct {v2, p0, v3, p1}, Lhke;-><init>(Lhkd;Ljava/lang/String;Lgnj;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lice;->a(Ljava/lang/String;Lesk;Ljava/lang/String;)V

    .line 328
    :goto_1
    return-void

    .line 312
    :cond_0
    new-instance v0, Lhkh;

    invoke-direct {v0, p0, v1}, Lhkh;-><init>(Lhkd;Ljava/lang/String;)V

    goto :goto_0

    .line 324
    :cond_1
    invoke-virtual {p0, p1}, Lhkd;->e(Lgnj;)V

    goto :goto_1
.end method

.method static m(Leiq;)V
    .locals 2

    .prologue
    .line 646
    instance-of v0, p0, Lehz;

    if-eqz v0, :cond_0

    .line 647
    const-string v0, "HandsFreeRecognizerController"

    const-string v1, "No recognizers available."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 651
    :goto_0
    return-void

    .line 649
    :cond_0
    const-string v0, "HandsFreeRecognizerController"

    const-string v1, "onError"

    invoke-static {v0, v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method final a(ILglx;)V
    .locals 2

    .prologue
    .line 281
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lhkd;->a(IZLandroid/net/Uri;)Lgnk;

    move-result-object v0

    invoke-virtual {v0}, Lgnk;->aHW()Lgnj;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lhkd;->b(Lgnj;Lglx;)V

    .line 285
    return-void
.end method

.method public final a(Lglx;ILjava/lang/String;)V
    .locals 2
    .param p1    # Lglx;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 220
    invoke-direct {p0}, Lhkd;->auJ()V

    .line 222
    iput p2, p0, Lhkd;->cZ:I

    .line 223
    iput-object p3, p0, Lhkd;->diW:Ljava/lang/String;

    .line 225
    iget-object v0, p0, Lhkd;->diU:Lhsx;

    iget-object v1, p0, Lhkd;->diU:Lhsx;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lhkd;->diU:Lhsx;

    sget-object v1, Lhkd;->diY:Lhsx;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    sget-object v0, Lhkd;->diY:Lhsx;

    iput-object v0, p0, Lhkd;->diU:Lhsx;

    .line 227
    const/4 v0, 0x5

    invoke-direct {p0, p1, v0}, Lhkd;->a(Lglx;I)V

    .line 228
    return-void

    .line 225
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lhsx;)V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lhkd;->diU:Lhsx;

    sget-object v1, Lhkd;->diY:Lhsx;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 146
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsx;

    iput-object v0, p0, Lhkd;->diU:Lhsx;

    .line 147
    return-void

    .line 145
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lglx;)V
    .locals 5
    .param p1    # Lglx;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 231
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-direct {p0}, Lhkd;->auJ()V

    iput v3, p0, Lhkd;->cZ:I

    const/4 v2, 0x0

    iput-object v2, p0, Lhkd;->diW:Ljava/lang/String;

    iget-object v2, p0, Lhkd;->mVoiceSearchServices:Lhhq;

    iget-object v2, v2, Lhhq;->mSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aFf()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lhkd;->mNetworkInformation:Lgno;

    invoke-virtual {v2}, Lgno;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhkd;->mVoiceSearchServices:Lhhq;

    iget-object v2, v2, Lhhq;->mSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aFg()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    invoke-direct {p0, v0, v4}, Lhkd;->a(Lglx;I)V

    .line 233
    :goto_0
    return-void

    .line 231
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqY()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v4, v3, v1}, Lhkd;->a(IZLandroid/net/Uri;)Lgnk;

    move-result-object v1

    invoke-virtual {v1}, Lgnk;->aHW()Lgnj;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lhkd;->b(Lgnj;Lglx;)V

    goto :goto_0
.end method

.method public final cancel()V
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lhkd;->diX:Z

    if-eqz v0, :cond_0

    .line 168
    const/16 v0, 0x12

    invoke-static {v0}, Lege;->ht(I)V

    .line 171
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lhkd;->ct(Z)V

    .line 172
    return-void
.end method

.method final ct(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 184
    iget-boolean v0, p0, Lhkd;->diX:Z

    if-eqz v0, :cond_1

    .line 185
    const-string v0, "no_match"

    invoke-static {v0}, Lgnm;->nk(Ljava/lang/String;)V

    .line 190
    if-eqz p1, :cond_0

    .line 192
    iget-object v0, p0, Lhkd;->diT:Lglu;

    invoke-virtual {v0}, Lglu;->aEM()V

    .line 197
    :cond_0
    iget-object v0, p0, Lhkd;->mRecognizer:Lgdb;

    iget-object v1, p0, Lhkd;->bbe:Ljava/lang/String;

    invoke-interface {v0, v1}, Lgdb;->ms(Ljava/lang/String;)V

    .line 198
    iput-object v2, p0, Lhkd;->bbe:Ljava/lang/String;

    .line 200
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhkd;->diX:Z

    .line 206
    :cond_1
    iget-object v0, p0, Lhkd;->diT:Lglu;

    if-eqz v0, :cond_2

    .line 207
    iget-object v0, p0, Lhkd;->diT:Lglu;

    invoke-virtual {v0}, Lglu;->invalidate()V

    .line 208
    iput-object v2, p0, Lhkd;->diT:Lglu;

    .line 212
    :cond_2
    iget-object v0, p0, Lhkd;->mOfflineActionsManager:Lgkg;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhkd;->diV:Lhkg;

    if-eqz v0, :cond_3

    .line 213
    iget-object v0, p0, Lhkd;->mOfflineActionsManager:Lgkg;

    iget-object v1, p0, Lhkd;->diV:Lhkg;

    invoke-virtual {v0, v1}, Lgkg;->c(Lefk;)V

    .line 214
    iput-object v2, p0, Lhkd;->diV:Lhkg;

    .line 216
    :cond_3
    return-void
.end method

.method final e(Lgnj;)V
    .locals 4

    .prologue
    .line 338
    iget-object v0, p0, Lhkd;->mRecognizer:Lgdb;

    iget-object v1, p0, Lhkd;->diT:Lglu;

    iget-object v2, p0, Lhkd;->aXe:Ljava/util/concurrent/Executor;

    iget-object v3, p0, Lhkd;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v3}, Lhhq;->aPg()Lgfb;

    move-result-object v3

    invoke-interface {v0, p1, v1, v2, v3}, Lgdb;->a(Lgnj;Lglx;Ljava/util/concurrent/Executor;Lgfb;)V

    .line 340
    return-void
.end method

.method final li(I)Z
    .locals 1

    .prologue
    .line 332
    iget v0, p0, Lhkd;->cZ:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
