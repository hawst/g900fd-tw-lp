.class public Ldbd;
.super Lddj;
.source "PG"


# instance fields
.field private bqA:Lcom/google/android/search/shared/actions/VoiceAction;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bqs:Ldbe;

.field private final bqt:Lcom/google/android/shared/util/BitFlags;

.field private bqu:Z

.field private bqv:Lcom/google/android/shared/search/Query;

.field private bqw:Ljava/util/List;

.field private bqx:I

.field private bqy:Lcom/google/android/shared/search/Query;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bqz:Ljava/util/List;

.field private mActionData:Lcom/google/android/velvet/ActionData;

.field private mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mClock:Lemp;

.field private final mEventBus:Ldda;

.field private final mPersonShortcutManager:Lciy;


# direct methods
.method public constructor <init>(Ldda;Lchk;Lciy;Lemp;)V
    .locals 2
    .param p1    # Ldda;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lciy;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Lemp;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 117
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lddj;-><init>(Ldda;I)V

    .line 75
    new-instance v0, Ldbe;

    invoke-direct {v0, p0}, Ldbe;-><init>(Ldbd;)V

    iput-object v0, p0, Ldbd;->bqs:Ldbe;

    .line 76
    new-instance v0, Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/shared/util/BitFlags;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    .line 89
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldbd;->bqz:Ljava/util/List;

    .line 118
    iput-object p1, p0, Ldbd;->mEventBus:Ldda;

    .line 119
    iput-object p3, p0, Ldbd;->mPersonShortcutManager:Lciy;

    .line 121
    iput-object p4, p0, Ldbd;->mClock:Lemp;

    .line 122
    return-void
.end method

.method private VB()Z
    .locals 4

    .prologue
    .line 264
    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x8000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private VM()Z
    .locals 4

    .prologue
    .line 537
    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldbd;->bqw:Ljava/util/List;

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x20000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Ldbd;->bqu:Z

    if-eqz v0, :cond_2

    .line 539
    :cond_1
    const/4 v0, 0x2

    const/16 v1, 0xc3

    invoke-direct {p0, v0, v1}, Ldbd;->an(II)Z

    move-result v0

    .line 542
    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    goto :goto_0
.end method

.method private VY()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1014
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ldbd;->bqz:Ljava/util/List;

    .line 1016
    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-interface {v2}, Lcom/google/android/search/shared/actions/VoiceAction;->Wb()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Lcom/google/android/search/shared/actions/VoiceAction;->ago()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldbd;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    if-eqz v3, :cond_3

    iget-object v3, p0, Ldbd;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alg()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Lcom/google/android/search/shared/actions/VoiceAction;->canExecute()Z

    move-result v3

    if-nez v3, :cond_3

    :cond_0
    instance-of v2, v2, Lcom/google/android/search/shared/actions/modular/ModularAction;

    if-nez v2, :cond_3

    move v2, v1

    :goto_0
    if-eqz v2, :cond_4

    .line 1017
    iget-object v2, p0, Ldbd;->bqs:Ldbe;

    sget-object v3, Lcom/google/android/search/shared/actions/ButtonAction;->bLT:Lcom/google/android/search/shared/actions/ButtonAction;

    iput-object v3, v2, Ldbe;->bqC:Lcom/google/android/search/shared/actions/VoiceAction;

    .line 1025
    :goto_1
    iget-object v2, p0, Ldbd;->bqs:Ldbe;

    invoke-virtual {v2}, Ldbe;->VZ()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Ldbd;->bqs:Ldbe;

    invoke-virtual {v2}, Ldbe;->Wa()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Ldbd;->bqw:Ljava/util/List;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ldbd;->bqw:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    if-eqz v0, :cond_7

    .line 1026
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x20000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 1027
    iget-boolean v0, p0, Ldbd;->bqu:Z

    if-nez v0, :cond_7

    .line 1039
    :goto_2
    return-void

    :cond_3
    move v2, v0

    .line 1016
    goto :goto_0

    .line 1018
    :cond_4
    iget-object v2, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v2}, Lcom/google/android/velvet/ActionData;->aIP()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Ldbd;->bqw:Ljava/util/List;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ldbd;->bqw:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {p0}, Ldbd;->VS()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqo()Z

    move-result v2

    if-nez v2, :cond_5

    move v2, v1

    :goto_3
    if-eqz v2, :cond_6

    .line 1019
    iget-object v2, p0, Ldbd;->bqs:Ldbe;

    sget-object v3, Lcom/google/android/search/shared/actions/ButtonAction;->bLU:Lcom/google/android/search/shared/actions/ButtonAction;

    iput-object v3, v2, Ldbe;->bqC:Lcom/google/android/search/shared/actions/VoiceAction;

    goto :goto_1

    :cond_5
    move v2, v0

    .line 1018
    goto :goto_3

    .line 1021
    :cond_6
    iget-object v2, p0, Ldbd;->bqs:Ldbe;

    const/4 v3, 0x0

    iput-object v3, v2, Ldbe;->bqC:Lcom/google/android/search/shared/actions/VoiceAction;

    goto :goto_1

    .line 1032
    :cond_7
    iget-object v0, p0, Ldbd;->bqz:Ljava/util/List;

    iget-object v1, p0, Ldbd;->bqs:Ldbe;

    invoke-virtual {v1}, Ldbe;->VZ()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1034
    iget-object v0, p0, Ldbd;->bqw:Ljava/util/List;

    if-eqz v0, :cond_8

    .line 1035
    iget-object v0, p0, Ldbd;->bqz:Ljava/util/List;

    iget-object v1, p0, Ldbd;->bqw:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1037
    :cond_8
    iget-object v0, p0, Ldbd;->bqz:Ljava/util/List;

    iget-object v1, p0, Ldbd;->bqs:Ldbe;

    invoke-virtual {v1}, Ldbe;->Wa()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method private Vt()Z
    .locals 4

    .prologue
    .line 200
    invoke-virtual {p0}, Ldbd;->yo()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x8

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x20

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private an(II)Z
    .locals 4

    .prologue
    .line 1117
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1118
    invoke-static {p2}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 1120
    const/4 v0, 0x1

    .line 1122
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private fA(I)I
    .locals 4

    .prologue
    .line 1090
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x100

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x40

    :goto_0
    or-int/lit16 v0, v0, 0x800

    return v0

    :cond_0
    const/16 v0, 0x80

    goto :goto_0
.end method

.method private r(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 4

    .prologue
    .line 1096
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    if-eq p1, v0, :cond_1

    .line 1105
    :cond_0
    :goto_0
    return-void

    .line 1100
    :cond_1
    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->aIU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1104
    iget-object v0, p0, Ldbd;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v2}, Lcom/google/android/velvet/ActionData;->aIV()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-interface {p1, v0, v1}, Lcom/google/android/search/shared/actions/VoiceAction;->aw(J)V

    goto :goto_0
.end method


# virtual methods
.method public final A(Lcom/google/android/shared/search/Query;)Z
    .locals 4

    .prologue
    .line 143
    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v0

    iget-object v2, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final B(Lcom/google/android/shared/search/Query;)Z
    .locals 1

    .prologue
    .line 988
    invoke-virtual {p0, p1}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->aIA()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Qs()Lcom/google/android/velvet/ActionData;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    return-object v0
.end method

.method public final VA()Z
    .locals 4

    .prologue
    .line 251
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x8000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    return v0
.end method

.method public final VC()V
    .locals 4

    .prologue
    .line 271
    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    .line 272
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agh()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x8040

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 274
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    .line 276
    :cond_0
    return-void
.end method

.method public final VD()Lcom/google/android/search/shared/actions/VoiceAction;
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Ldbd;->bqA:Lcom/google/android/search/shared/actions/VoiceAction;

    .line 290
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agn()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/util/MatchingAppInfo;->avn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 292
    iget-object v1, p0, Ldbd;->mPersonShortcutManager:Lciy;

    invoke-virtual {v1, v0}, Lciy;->c(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 293
    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agi()Z

    .line 296
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final VE()Z
    .locals 4

    .prologue
    .line 304
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x80

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    return v0
.end method

.method public final VF()V
    .locals 4

    .prologue
    .line 311
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x8140

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Ldbd;->VB()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 313
    :cond_0
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    .line 315
    :cond_1
    return-void
.end method

.method public final VG()Z
    .locals 4

    .prologue
    .line 321
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x100

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final VH()Z
    .locals 4

    .prologue
    .line 329
    iget-object v0, p0, Ldbd;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbd;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alg()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbd;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/CardDecision;->ali()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Ldbd;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/CardDecision;->Zv()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final VI()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 343
    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v1

    .line 344
    if-nez v1, :cond_1

    .line 350
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Ldbd;->bqA:Lcom/google/android/search/shared/actions/VoiceAction;

    if-nez v1, :cond_2

    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/search/shared/actions/VoiceAction;->canExecute()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v1

    invoke-virtual {p0, v1}, Ldbd;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alg()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/search/shared/actions/VoiceAction;->ago()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final VJ()Z
    .locals 2

    .prologue
    .line 452
    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    .line 453
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agq()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agr()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final VK()Z
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Ldbd;->bqz:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final VL()Lcom/google/android/shared/search/Query;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 495
    iget-object v0, p0, Ldbd;->bqy:Lcom/google/android/shared/search/Query;

    .line 496
    const/4 v1, 0x0

    iput-object v1, p0, Ldbd;->bqy:Lcom/google/android/shared/search/Query;

    .line 497
    return-object v0
.end method

.method public final VN()Z
    .locals 4

    .prologue
    .line 568
    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    .line 569
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agl()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 570
    iget-object v0, p0, Ldbd;->bqA:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-direct {p0, v0}, Ldbd;->r(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 572
    iget-object v0, p0, Ldbd;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-virtual {v0}, Ldcy;->ZZ()V

    .line 573
    invoke-direct {p0}, Ldbd;->VY()V

    .line 574
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x48940

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 578
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    .line 579
    const/4 v0, 0x1

    .line 581
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final VO()Ljava/util/List;
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Ldbd;->bqw:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, Ldbd;->bqz:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldbd;->bqz:Ljava/util/List;

    goto :goto_0
.end method

.method public final VP()Ljava/util/List;
    .locals 1

    .prologue
    .line 610
    iget-object v0, p0, Ldbd;->bqw:Ljava/util/List;

    return-object v0
.end method

.method public final VQ()Lcom/google/android/search/shared/actions/VoiceAction;
    .locals 2

    .prologue
    .line 617
    iget-object v0, p0, Ldbd;->bqw:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbd;->bqw:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 618
    iget-object v0, p0, Ldbd;->bqw:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/VoiceAction;

    .line 620
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final VR()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 668
    invoke-virtual {p0}, Ldbd;->VU()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 669
    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/search/shared/actions/VoiceAction;->agh()Z

    .line 670
    iget-object v1, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x40

    invoke-virtual {v1, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Ldbd;->VB()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 671
    :cond_0
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    .line 680
    :goto_0
    return v0

    .line 675
    :cond_1
    iget-object v1, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x4000

    invoke-virtual {v1, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0}, Ldbd;->VB()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 676
    :cond_2
    invoke-direct {p0}, Ldbd;->VY()V

    .line 677
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    goto :goto_0

    .line 680
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final VS()Z
    .locals 4

    .prologue
    .line 688
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x4000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final VT()V
    .locals 3

    .prologue
    .line 692
    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldbd;->VJ()Z

    move-result v0

    if-nez v0, :cond_0

    .line 693
    iget-object v0, p0, Ldbd;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aat()Ldcf;

    move-result-object v0

    iget-object v1, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Ldcf;->a(Lcom/google/android/velvet/ActionData;I)V

    .line 696
    :cond_0
    return-void
.end method

.method public final VU()Z
    .locals 1

    .prologue
    .line 702
    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    .line 703
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->ags()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final VV()I
    .locals 1

    .prologue
    .line 723
    iget v0, p0, Ldbd;->bqx:I

    return v0
.end method

.method public final VW()Z
    .locals 4

    .prologue
    .line 996
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x20000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final VX()V
    .locals 1

    .prologue
    .line 1003
    iget-boolean v0, p0, Ldbd;->bqu:Z

    if-nez v0, :cond_0

    .line 1004
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbd;->bqu:Z

    .line 1005
    invoke-direct {p0}, Ldbd;->VY()V

    .line 1006
    invoke-direct {p0}, Ldbd;->VM()Z

    .line 1007
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    .line 1009
    :cond_0
    return-void
.end method

.method public final Vo()Lcom/google/android/velvet/ActionData;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->aII()Lieb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Vp()Lcom/google/android/velvet/ActionData;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/16 v4, 0xc2

    const/4 v0, 0x1

    .line 163
    iget-object v1, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v1}, Lcom/google/android/velvet/ActionData;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 164
    invoke-direct {p0, v0, v4}, Ldbd;->an(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    .line 179
    :goto_0
    return-object v0

    .line 168
    :cond_0
    iget-object v1, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x200

    invoke-virtual {v1, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v4}, Lege;->hs(I)Litu;

    move-result-object v1

    iget-object v2, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v1

    invoke-static {v1}, Lege;->a(Litu;)V

    :goto_1
    if-eqz v0, :cond_2

    .line 172
    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    goto :goto_0

    .line 168
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 173
    :cond_2
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x2000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 175
    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    goto :goto_0

    .line 179
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Vq()Z
    .locals 4

    .prologue
    .line 183
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    return v0
.end method

.method public final Vr()V
    .locals 4

    .prologue
    .line 190
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x40000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    .line 193
    :cond_0
    return-void
.end method

.method public final Vs()Z
    .locals 4

    .prologue
    .line 196
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x40000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    return v0
.end method

.method public final Vu()Z
    .locals 4

    .prologue
    .line 206
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x10

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    return v0
.end method

.method public final Vv()Z
    .locals 4

    .prologue
    .line 210
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x20

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    return v0
.end method

.method public final Vw()Z
    .locals 4

    .prologue
    .line 214
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x800

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    return v0
.end method

.method public final Vx()Z
    .locals 4

    .prologue
    .line 218
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x400

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    return v0
.end method

.method public final Vy()Z
    .locals 4

    .prologue
    .line 225
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x40

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    return v0
.end method

.method public final Vz()Z
    .locals 4

    .prologue
    .line 232
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x80000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/search/shared/actions/VoiceAction;IZ)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x0

    .line 367
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 368
    iget-object v0, p0, Ldbd;->bqA:Lcom/google/android/search/shared/actions/VoiceAction;

    if-eqz v0, :cond_1

    .line 370
    const-string v0, "ActionState"

    const-string v1, "requestExecuteAction called when an action is already executing, return."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 390
    :cond_0
    :goto_0
    return-void

    .line 373
    :cond_1
    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->agp()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 374
    const-string v0, "ActionState"

    const-string v1, "requestExecuteAction called on an already executing action."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 377
    :cond_2
    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    if-ne p1, v0, :cond_3

    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->ago()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x100

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384
    :cond_3
    iput p2, p0, Ldbd;->bqx:I

    .line 385
    iput-object p1, p0, Ldbd;->bqA:Lcom/google/android/search/shared/actions/VoiceAction;

    .line 386
    invoke-interface {p1, p3}, Lcom/google/android/search/shared/actions/VoiceAction;->ei(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    invoke-direct {p0}, Ldbd;->VY()V

    .line 388
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;)V
    .locals 4

    .prologue
    .line 655
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Ldbd;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    invoke-virtual {p2, v0}, Lcom/google/android/search/shared/actions/utils/CardDecision;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 657
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/utils/CardDecision;

    iput-object v0, p0, Ldbd;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    .line 658
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x40

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 659
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    .line 661
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/velvet/ActionData;Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/ActionData;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Ldbd;->bqy:Lcom/google/android/shared/search/Query;

    .line 461
    invoke-virtual {p0}, Ldbd;->yo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    .line 466
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/velvet/ActionData;Ljava/util/List;Lcom/google/android/search/shared/actions/utils/CardDecision;)V
    .locals 4

    .prologue
    .line 502
    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/ActionData;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 503
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    check-cast p2, Ljava/util/List;

    iput-object p2, p0, Ldbd;->bqw:Ljava/util/List;

    .line 504
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/utils/CardDecision;

    iput-object v0, p0, Ldbd;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    .line 506
    invoke-direct {p0}, Ldbd;->VY()V

    .line 507
    invoke-direct {p0}, Ldbd;->VM()Z

    .line 509
    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    .line 510
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agz()Z

    move-result v0

    if-nez v0, :cond_1

    .line 513
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x100

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 516
    :cond_1
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    .line 518
    :cond_2
    return-void
.end method

.method public final a(Lddb;)V
    .locals 10

    .prologue
    .line 751
    const/4 v1, 0x0

    .line 752
    iget-object v0, p0, Ldbd;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v5

    .line 753
    iget-object v0, p0, Ldbd;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v6

    .line 754
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lddb;->aaH()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 755
    :cond_0
    invoke-virtual {v5}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v7

    .line 756
    invoke-virtual {v5, v7}, Lcom/google/android/search/core/state/QueryState;->X(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    invoke-virtual {v7}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v2

    iget-object v4, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v8

    cmp-long v2, v2, v8

    if-eqz v2, :cond_f

    if-eqz v0, :cond_f

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_11

    invoke-virtual {v7}, Lcom/google/android/shared/search/Query;->aoQ()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {v7}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    if-eqz v0, :cond_10

    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    sget-object v2, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    if-eq v0, v2, :cond_10

    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->aIz()Lcom/google/android/velvet/ActionData;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_11

    .line 764
    :goto_2
    invoke-virtual {v5}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v5}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 766
    invoke-virtual {p0}, Ldbd;->VN()Z

    .line 769
    :cond_1
    invoke-virtual {v7}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v2

    iget-object v4, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v8

    cmp-long v2, v2, v8

    if-eqz v2, :cond_2

    .line 770
    iget-object v2, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v8, 0x4

    invoke-virtual {v2, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 772
    invoke-direct {p0}, Ldbd;->Vt()Z

    move-result v1

    or-int/lit8 v1, v1, 0x0

    .line 776
    :cond_2
    invoke-virtual {v5}, Lcom/google/android/search/core/state/QueryState;->Yi()Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v3

    .line 777
    iget-object v2, p0, Ldbd;->bqs:Ldbe;

    iget-object v4, v2, Ldbe;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    if-eq v4, v3, :cond_12

    iput-object v3, v2, Ldbe;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    const/4 v2, 0x1

    :goto_3
    or-int/2addr v2, v1

    .line 780
    iget-object v1, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-static {v0, v1}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1e

    if-eqz v3, :cond_3

    iget-object v1, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    if-eqz v1, :cond_3

    sget-object v1, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    if-eq v0, v1, :cond_1e

    .line 783
    :cond_3
    invoke-virtual {v7}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v1

    if-eqz v1, :cond_13

    invoke-virtual {v6}, Ldcy;->ZB()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v6}, Ldcy;->ZA()Z

    move-result v1

    if-eqz v1, :cond_13

    :cond_4
    const/4 v1, 0x1

    .line 788
    :goto_4
    iget-object v3, p0, Ldbd;->bqw:Ljava/util/List;

    if-eqz v3, :cond_14

    iget-object v3, p0, Ldbd;->bqw:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_14

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->aIP()Z

    move-result v3

    if-eqz v3, :cond_14

    :cond_5
    const/4 v3, 0x1

    move v4, v3

    .line 791
    :goto_5
    if-nez v0, :cond_15

    const/4 v3, 0x0

    .line 794
    :goto_6
    if-eqz v3, :cond_16

    .line 797
    iput-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    .line 798
    iput-object v7, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    .line 799
    const/4 v0, 0x0

    iput-object v0, p0, Ldbd;->bqy:Lcom/google/android/shared/search/Query;

    .line 800
    iput-object v3, p0, Ldbd;->bqw:Ljava/util/List;

    .line 801
    const/4 v0, 0x0

    iput-object v0, p0, Ldbd;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    .line 803
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0}, Lcom/google/android/shared/util/BitFlags;->auz()Z

    .line 804
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x18505

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 807
    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    .line 808
    if-eqz v0, :cond_6

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->ago()Z

    move-result v1

    if-nez v1, :cond_6

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->ags()Z

    move-result v0

    if-nez v0, :cond_6

    .line 812
    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldbd;->l(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 814
    :cond_6
    const/4 v2, 0x1

    .line 862
    :cond_7
    :goto_7
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v8, 0x10000

    invoke-virtual {v0, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-nez v0, :cond_8

    .line 863
    invoke-virtual {v5}, Lcom/google/android/search/core/state/QueryState;->XK()Lcom/google/android/velvet/ActionData;

    move-result-object v0

    .line 864
    if-eqz v0, :cond_8

    .line 865
    const/16 v1, 0x6c

    invoke-static {v1}, Lege;->hs(I)Litu;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->oY()I

    move-result v0

    invoke-virtual {v1, v0}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 869
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v8, 0x10000

    invoke-virtual {v0, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 873
    :cond_8
    if-eqz v2, :cond_9

    .line 875
    invoke-direct {p0}, Ldbd;->VY()V

    .line 876
    invoke-direct {p0}, Ldbd;->VM()Z

    .line 881
    :cond_9
    :goto_8
    invoke-virtual {p1}, Lddb;->aaF()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 882
    iget-object v0, p0, Ldbd;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aav()Ldcv;

    move-result-object v0

    iget-object v1, p0, Ldbd;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldcv;->as(Lcom/google/android/shared/search/Query;)Lcom/google/android/velvet/ActionData;

    move-result-object v0

    .line 884
    if-eqz v0, :cond_a

    iget-object v1, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-static {v0, v1}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 885
    iput-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    .line 886
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x4000

    const-wide/16 v8, 0x2000

    invoke-virtual {v0, v2, v3, v8, v9}, Lcom/google/android/shared/util/BitFlags;->e(JJ)Z

    .line 887
    const/4 v2, 0x1

    .line 891
    :cond_a
    invoke-virtual {p1}, Lddb;->aay()Z

    move-result v0

    if-nez v0, :cond_b

    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 892
    :cond_b
    invoke-virtual {v6}, Ldcy;->Zx()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {v6}, Ldcy;->Zy()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {v6}, Ldcy;->Zz()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {v5}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 896
    :cond_c
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v4, 0x400

    invoke-virtual {v0, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 897
    invoke-direct {p0}, Ldbd;->Vt()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 898
    const/4 v2, 0x1

    .line 903
    :cond_d
    if-eqz v2, :cond_e

    .line 904
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    .line 906
    :cond_e
    return-void

    .line 756
    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_11
    iget-object v0, p0, Ldbd;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v0

    goto/16 :goto_2

    .line 777
    :cond_12
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 783
    :cond_13
    const/4 v1, 0x0

    goto/16 :goto_4

    .line 788
    :cond_14
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_5

    .line 791
    :cond_15
    iget-object v3, p0, Ldbd;->mEventBus:Ldda;

    invoke-virtual {v3}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/search/core/state/QueryState;->g(Lcom/google/android/velvet/ActionData;)Ljava/util/List;

    move-result-object v3

    goto/16 :goto_6

    .line 815
    :cond_16
    invoke-virtual {p0, v7}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v3

    if-eqz v3, :cond_18

    iget-object v3, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v3}, Lcom/google/android/velvet/ActionData;->agw()Z

    move-result v3

    if-nez v3, :cond_18

    .line 820
    invoke-virtual {v7}, Lcom/google/android/shared/search/Query;->aqu()Z

    move-result v0

    if-nez v0, :cond_17

    .line 821
    const-string v0, "ActionState"

    const-string v1, "New ActionData for non-restored query."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v0, v1, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 823
    :cond_17
    iput-object v7, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    goto/16 :goto_7

    .line 824
    :cond_18
    if-eqz v0, :cond_1a

    if-eqz v4, :cond_1a

    .line 826
    iget-object v1, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x4400

    const-wide/16 v8, 0x200

    invoke-virtual {v1, v2, v3, v8, v9}, Lcom/google/android/shared/util/BitFlags;->e(JJ)Z

    .line 845
    :cond_19
    :goto_9
    iput-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    .line 846
    iput-object v7, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    .line 847
    const/4 v0, 0x0

    iput-object v0, p0, Ldbd;->bqy:Lcom/google/android/shared/search/Query;

    .line 848
    const/4 v0, 0x1

    :goto_a
    move v2, v0

    .line 852
    goto/16 :goto_7

    .line 833
    :cond_1a
    if-nez v0, :cond_1b

    if-nez v1, :cond_22

    .line 836
    :cond_1b
    iget-object v1, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v1}, Lcom/google/android/shared/util/BitFlags;->auz()Z

    .line 837
    if-eqz v0, :cond_1c

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 838
    iget-object v1, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x800

    invoke-virtual {v1, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 841
    :cond_1c
    iget-object v1, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    if-eqz v1, :cond_1d

    iget-object v1, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v1}, Lcom/google/android/velvet/ActionData;->agw()Z

    move-result v1

    if-nez v1, :cond_19

    .line 842
    :cond_1d
    const/4 v1, 0x0

    iput-object v1, p0, Ldbd;->bqw:Ljava/util/List;

    .line 843
    const/4 v1, 0x0

    iput-object v1, p0, Ldbd;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_9

    .line 852
    :cond_1e
    iget-object v1, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-static {v0, v1}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-virtual {p0, v7}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_20

    :cond_1f
    invoke-virtual {v7}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v0

    iget-object v3, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v8

    cmp-long v0, v0, v8

    if-nez v0, :cond_7

    .line 856
    :cond_20
    iget-object v0, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v7, v0}, Lcom/google/android/shared/search/Query;->aW(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_21

    .line 857
    const/4 v2, 0x1

    .line 859
    :cond_21
    iput-object v7, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    goto/16 :goto_7

    :cond_22
    move v0, v2

    goto :goto_a

    :cond_23
    move v2, v1

    goto/16 :goto_8
.end method

.method public final a(Letj;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1136
    const-string v0, "ActionState"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 1137
    const-string v0, "Flags"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v2, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v2}, Lcom/google/android/shared/util/BitFlags;->auA()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 1138
    const-string v0, "CurrentQuery"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v2, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v2}, Letn;->b(Leti;)V

    .line 1139
    const-string v0, "ActionData"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v2, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0, v2}, Letn;->b(Leti;)V

    .line 1140
    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ActionData;->ku(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->aIG()Lcom/google/android/speech/embedded/TaggerResult;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1143
    :cond_0
    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->oY()I

    move-result v0

    .line 1146
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v1

    .line 1220
    :goto_0
    if-eqz v0, :cond_1

    .line 1221
    const v2, 0x7f0a0744

    invoke-virtual {p1, v2}, Letj;->in(I)Letn;

    move-result-object v2

    invoke-virtual {v2}, Letn;->avS()Letn;

    move-result-object v2

    invoke-virtual {v2, v0}, Letn;->io(I)V

    .line 1226
    :cond_1
    const-string v0, "ModifiedCommit"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v2, p0, Ldbd;->bqy:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v2}, Letn;->b(Leti;)V

    .line 1227
    const-string v0, "VoiceActions"

    iget-object v2, p0, Ldbd;->bqw:Ljava/util/List;

    if-nez v2, :cond_2

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 1228
    :goto_1
    return-void

    .line 1148
    :pswitch_1
    const v0, 0x7f0a072c

    .line 1149
    goto :goto_0

    .line 1151
    :pswitch_2
    const v0, 0x7f0a072d

    .line 1152
    goto :goto_0

    .line 1154
    :pswitch_3
    const v0, 0x7f0a072e

    .line 1155
    goto :goto_0

    .line 1157
    :pswitch_4
    const v0, 0x7f0a072f

    .line 1158
    goto :goto_0

    .line 1160
    :pswitch_5
    const v0, 0x7f0a0730

    .line 1161
    goto :goto_0

    .line 1163
    :pswitch_6
    const v0, 0x7f0a0731

    .line 1164
    goto :goto_0

    .line 1166
    :pswitch_7
    const v0, 0x7f0a0732

    .line 1167
    goto :goto_0

    .line 1169
    :pswitch_8
    const v0, 0x7f0a0733

    .line 1170
    goto :goto_0

    .line 1172
    :pswitch_9
    const v0, 0x7f0a0734

    .line 1173
    goto :goto_0

    .line 1175
    :pswitch_a
    const v0, 0x7f0a0735

    .line 1176
    goto :goto_0

    .line 1178
    :pswitch_b
    const v0, 0x7f0a0736

    .line 1179
    goto :goto_0

    .line 1181
    :pswitch_c
    const v0, 0x7f0a0737

    .line 1182
    goto :goto_0

    .line 1184
    :pswitch_d
    const v0, 0x7f0a0738

    .line 1185
    goto :goto_0

    .line 1187
    :pswitch_e
    const v0, 0x7f0a0739

    .line 1188
    goto :goto_0

    .line 1190
    :pswitch_f
    const v0, 0x7f0a073a

    .line 1191
    goto :goto_0

    .line 1193
    :pswitch_10
    const v0, 0x7f0a073b

    .line 1194
    goto :goto_0

    .line 1196
    :pswitch_11
    const v0, 0x7f0a073c

    .line 1197
    goto :goto_0

    .line 1199
    :pswitch_12
    const v0, 0x7f0a073d

    .line 1200
    goto :goto_0

    .line 1202
    :pswitch_13
    const v0, 0x7f0a073e

    .line 1203
    goto :goto_0

    .line 1205
    :pswitch_14
    const v0, 0x7f0a073f

    .line 1206
    goto :goto_0

    .line 1208
    :pswitch_15
    const v0, 0x7f0a0740

    .line 1209
    goto :goto_0

    .line 1211
    :pswitch_16
    const v0, 0x7f0a0741

    .line 1212
    goto/16 :goto_0

    .line 1214
    :pswitch_17
    const v0, 0x7f0a0742

    .line 1215
    goto/16 :goto_0

    .line 1217
    :pswitch_18
    const v0, 0x7f0a0743

    goto/16 :goto_0

    .line 1227
    :cond_2
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v1

    invoke-virtual {v1, v0}, Letj;->lt(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Letj;->f(Ljava/lang/Iterable;)V

    goto :goto_1

    .line 1146
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_b
        :pswitch_8
        :pswitch_0
        :pswitch_e
        :pswitch_11
        :pswitch_14
        :pswitch_6
        :pswitch_4
        :pswitch_2
        :pswitch_16
        :pswitch_c
        :pswitch_7
        :pswitch_f
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_17
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_a
        :pswitch_d
        :pswitch_13
        :pswitch_12
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_18
        :pswitch_0
        :pswitch_0
        :pswitch_10
        :pswitch_0
        :pswitch_15
    .end packed-switch
.end method

.method public final d(Lcom/google/android/velvet/ActionData;)V
    .locals 1

    .prologue
    .line 551
    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/ActionData;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldbd;->bqw:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbd;->bqw:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 552
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ldbd;->bqw:Ljava/util/List;

    .line 553
    const/4 v0, 0x0

    iput-object v0, p0, Ldbd;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    .line 554
    invoke-direct {p0}, Ldbd;->VY()V

    .line 555
    invoke-direct {p0}, Ldbd;->VM()Z

    .line 556
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    .line 558
    :cond_1
    return-void
.end method

.method public final hasError()Z
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Ldbd;->bqs:Ldbe;

    iget-object v0, v0, Ldbe;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 4

    .prologue
    .line 240
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x88040

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    .line 245
    :cond_0
    return-void
.end method

.method public final j(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 1

    .prologue
    .line 258
    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Ldbd;->VB()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    .line 261
    :cond_0
    return-void
.end method

.method public final k(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 4

    .prologue
    .line 397
    iget-object v0, p0, Ldbd;->bqA:Lcom/google/android/search/shared/actions/VoiceAction;

    .line 398
    if-eq v0, p1, :cond_1

    .line 399
    const-string v0, "ActionState"

    const-string v1, "#onExecutionError for unrecognized action."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 409
    :cond_0
    :goto_0
    return-void

    .line 402
    :cond_1
    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agm()Z

    move-result v0

    .line 403
    const/4 v1, 0x0

    iput-object v1, p0, Ldbd;->bqA:Lcom/google/android/search/shared/actions/VoiceAction;

    .line 404
    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x8140

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 407
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    goto :goto_0
.end method

.method public final l(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 4

    .prologue
    .line 419
    iget-object v0, p0, Ldbd;->bqA:Lcom/google/android/search/shared/actions/VoiceAction;

    .line 420
    if-eq v0, p1, :cond_1

    .line 421
    const-string v0, "ActionState"

    const-string v1, "#onUncertainResult for unrecognized action."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 425
    :cond_1
    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agd()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->Wc()Z

    move-result v1

    if-nez v1, :cond_2

    .line 427
    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agj()Z

    move-result v0

    .line 431
    :goto_1
    iget-object v1, p0, Ldbd;->bqA:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-direct {p0, v1}, Ldbd;->r(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 433
    const/4 v1, 0x0

    iput-object v1, p0, Ldbd;->bqA:Lcom/google/android/search/shared/actions/VoiceAction;

    .line 434
    if-eqz v0, :cond_0

    .line 435
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const/16 v1, 0x800

    invoke-direct {p0, v1}, Ldbd;->fA(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 436
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    goto :goto_0

    .line 429
    :cond_2
    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agk()Z

    move-result v0

    goto :goto_1
.end method

.method public final m(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 4

    .prologue
    .line 441
    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    .line 442
    if-eq v0, p1, :cond_1

    .line 443
    const-string v0, "ActionState"

    const-string v1, "#onMatchingAppInfoRefreshed for unrecognized action."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 449
    :cond_0
    :goto_0
    return-void

    .line 446
    :cond_1
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x8040

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 447
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    goto :goto_0
.end method

.method public final n(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 4

    .prologue
    .line 469
    iget-object v0, p0, Ldbd;->bqA:Lcom/google/android/search/shared/actions/VoiceAction;

    if-ne v0, p1, :cond_1

    .line 474
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const/16 v1, 0x800

    invoke-direct {p0, v1}, Ldbd;->fA(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 475
    iget-object v0, p0, Ldbd;->bqA:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agj()Z

    .line 476
    iget-object v0, p0, Ldbd;->bqA:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-direct {p0, v0}, Ldbd;->r(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 477
    invoke-direct {p0}, Ldbd;->VY()V

    .line 478
    instance-of v0, p1, Lcom/google/android/search/shared/actions/ContactOptInAction;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->agq()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    iget-object v1, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->kX(Ljava/lang/String;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Ldbd;->bqy:Lcom/google/android/shared/search/Query;

    .line 479
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ldbd;->bqA:Lcom/google/android/search/shared/actions/VoiceAction;

    .line 480
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    .line 484
    :goto_0
    return-void

    .line 482
    :cond_1
    const-string v0, "ActionState"

    const-string v1, "#onCardActionComplete for unrecognized data."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final o(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 521
    .line 522
    iget-object v2, p0, Ldbd;->bqs:Ldbe;

    iget-object v3, v2, Ldbe;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    if-eqz v3, :cond_1

    iget-object v3, v2, Ldbe;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    if-ne p1, v3, :cond_1

    iput-object v4, v2, Ldbe;->bqB:Lcom/google/android/search/shared/actions/errors/SearchError;

    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    .line 528
    :goto_1
    if-eqz v0, :cond_0

    .line 529
    invoke-direct {p0}, Ldbd;->VY()V

    .line 530
    invoke-direct {p0}, Ldbd;->VM()Z

    .line 531
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    .line 533
    :cond_0
    return-void

    .line 522
    :cond_1
    iget-object v3, v2, Ldbe;->bqC:Lcom/google/android/search/shared/actions/VoiceAction;

    if-eqz v3, :cond_2

    iget-object v3, v2, Ldbe;->bqC:Lcom/google/android/search/shared/actions/VoiceAction;

    if-ne p1, v3, :cond_2

    iput-object v4, v2, Ldbe;->bqC:Lcom/google/android/search/shared/actions/VoiceAction;

    move v2, v0

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_0

    .line 524
    :cond_3
    iget-object v0, p0, Ldbd;->bqw:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 525
    iget-object v0, p0, Ldbd;->bqw:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final p(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 1
    .param p1    # Lcom/google/android/search/shared/actions/VoiceAction;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 597
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    if-ne p1, v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/util/MatchingAppInfo;->avl()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599
    invoke-virtual {p0}, Ldbd;->notifyChanged()V

    .line 601
    :cond_0
    return-void
.end method

.method public final q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 641
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Ldbd;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    if-eqz v0, :cond_0

    .line 644
    iget-object v0, p0, Ldbd;->mCardDecision:Lcom/google/android/search/shared/actions/utils/CardDecision;

    .line 646
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_0
.end method

.method public final s(Lcom/google/android/search/shared/actions/VoiceAction;)Z
    .locals 4
    .param p1    # Lcom/google/android/search/shared/actions/VoiceAction;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1112
    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->agx()J

    move-result-wide v0

    .line 1113
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Ldbd;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->elapsedRealtime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 728
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "Action{"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 730
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Identity="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 731
    const-string v0, " Flags="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v2}, Lcom/google/android/shared/util/BitFlags;->auA()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 732
    const-string v0, " CurrentQuery="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Ldbd;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 733
    const-string v0, " ExtraCards="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Ldbd;->bqs:Ldbe;

    invoke-virtual {v2}, Ldbe;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 734
    const-string v0, " "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    if-nez v0, :cond_0

    const-string v0, "null data"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 735
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 736
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 734
    :cond_0
    iget-object v0, p0, Ldbd;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final yo()Z
    .locals 4

    .prologue
    .line 487
    iget-object v0, p0, Ldbd;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method
