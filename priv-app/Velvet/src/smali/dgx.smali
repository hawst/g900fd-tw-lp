.class final Ldgx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic anv:Lemy;

.field private synthetic bxg:Ldgm;


# direct methods
.method constructor <init>(Ldgm;Lemy;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Ldgx;->bxg:Ldgm;

    iput-object p2, p0, Ldgx;->anv:Lemy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x0

    .line 287
    iget-object v0, p0, Ldgx;->bxg:Ldgm;

    iget-object v0, v0, Ldgm;->auh:Lbhi;

    sget-wide v2, Ldgm;->bwY:J

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v1}, Lbhi;->b(JLjava/util/concurrent/TimeUnit;)Lbgm;

    move-result-object v0

    .line 289
    invoke-virtual {v0}, Lbgm;->yk()Z

    move-result v0

    if-nez v0, :cond_0

    .line 290
    const-string v0, "Search.IcingConnection"

    const-string v1, "Failed to connect to GoogleApiClient."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v5, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 301
    :goto_0
    iget-object v0, p0, Ldgx;->bxg:Ldgm;

    iget-object v0, v0, Ldgm;->auh:Lbhi;

    invoke-interface {v0}, Lbhi;->disconnect()V

    .line 302
    return-void

    .line 292
    :cond_0
    iget-object v0, p0, Ldgx;->bxg:Ldgm;

    invoke-static {v0}, Ldgm;->a(Ldgm;)Lbvh;

    move-result-object v0

    iget-object v1, p0, Ldgx;->bxg:Ldgm;

    iget-object v1, v1, Ldgm;->auh:Lbhi;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lbvh;->a(Lbhi;Z)Lbhm;

    move-result-object v0

    sget-wide v2, Ldgm;->bwY:J

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v1}, Lbhm;->a(JLjava/util/concurrent/TimeUnit;)Lbho;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;

    .line 294
    invoke-virtual {v0}, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;->wZ()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->yk()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 295
    iget-object v1, p0, Ldgx;->anv:Lemy;

    iget-object v0, v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;->aLw:[Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$GlobalSearchSource;

    invoke-interface {v1, v0}, Lemy;->aj(Ljava/lang/Object;)Z

    goto :goto_0

    .line 297
    :cond_1
    const-string v0, "Search.IcingConnection"

    const-string v1, "getGlobalSearchSources call failed."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v5, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 298
    iget-object v0, p0, Ldgx;->anv:Lemy;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lemy;->aj(Ljava/lang/Object;)Z

    goto :goto_0
.end method
