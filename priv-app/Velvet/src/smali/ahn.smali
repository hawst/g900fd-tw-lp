.class public abstract Lahn;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static Yl:Ljava/lang/Object;

.field private static Ys:Lahn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lahn;->Yl:Ljava/lang/Object;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    return-void
.end method

.method public static B(Landroid/content/Context;)Lahn;
    .locals 3

    .prologue
    .line 52
    sget-object v1, Lahn;->Yl:Ljava/lang/Object;

    monitor-enter v1

    .line 53
    :try_start_0
    sget-object v0, Lahn;->Ys:Lahn;

    if-nez v0, :cond_0

    .line 54
    invoke-static {}, Ladp;->km()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    new-instance v0, Lahr;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lahr;-><init>(Landroid/content/Context;)V

    sput-object v0, Lahn;->Ys:Lahn;

    .line 60
    :cond_0
    :goto_0
    sget-object v0, Lahn;->Ys:Lahn;

    monitor-exit v1

    return-object v0

    .line 57
    :cond_1
    new-instance v0, Lahp;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lahp;-><init>(Landroid/content/Context;)V

    sput-object v0, Lahn;->Ys:Lahn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public abstract a(Laho;)V
.end method

.method public abstract a(Landroid/content/ComponentName;Lahz;Landroid/graphics/Rect;Landroid/os/Bundle;)V
.end method

.method public abstract c(Landroid/content/Intent;Lahz;)Lahk;
.end method

.method public abstract d(Landroid/content/ComponentName;Lahz;)V
.end method

.method public abstract e(Landroid/content/ComponentName;Lahz;)Z
.end method

.method public abstract i(Ljava/lang/String;Lahz;)Ljava/util/List;
.end method

.method public abstract j(Ljava/lang/String;Lahz;)Z
.end method
