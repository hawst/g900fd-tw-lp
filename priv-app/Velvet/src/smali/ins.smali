.class final Lins;
.super Ljava/util/concurrent/locks/ReentrantLock;
.source "PG"


# instance fields
.field volatile count:I

.field private dFA:Ljava/util/Queue;

.field private dFB:Ljava/util/concurrent/atomic/AtomicInteger;

.field private dFC:Ljava/util/Queue;

.field private dFD:Ljava/util/Queue;

.field private dFu:Limn;

.field private dFv:I

.field volatile dFw:Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field private dFx:I

.field final dFy:Ljava/lang/ref/ReferenceQueue;

.field final dFz:Ljava/lang/ref/ReferenceQueue;

.field modCount:I


# direct methods
.method constructor <init>(Limn;II)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2140
    invoke-direct {p0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    .line 2124
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lins;->dFB:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 2141
    iput-object p1, p0, Lins;->dFu:Limn;

    .line 2142
    iput p3, p0, Lins;->dFx:I

    .line 2143
    invoke-static {p2}, Lins;->mj(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x4

    iput v2, p0, Lins;->dFv:I

    iget v2, p0, Lins;->dFv:I

    iget v3, p0, Lins;->dFx:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lins;->dFv:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lins;->dFv:I

    :cond_0
    iput-object v0, p0, Lins;->dFw:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2145
    invoke-virtual {p1}, Limn;->aXx()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :goto_0
    iput-object v0, p0, Lins;->dFy:Ljava/lang/ref/ReferenceQueue;

    .line 2148
    invoke-virtual {p1}, Limn;->aXy()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v1, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v1}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :cond_1
    iput-object v1, p0, Lins;->dFz:Ljava/lang/ref/ReferenceQueue;

    .line 2151
    invoke-virtual {p1}, Limn;->aXu()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Limn;->aXw()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    :goto_1
    iput-object v0, p0, Lins;->dFA:Ljava/util/Queue;

    .line 2155
    invoke-virtual {p1}, Limn;->aXu()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Linh;

    invoke-direct {v0}, Linh;-><init>()V

    :goto_2
    iput-object v0, p0, Lins;->dFC:Ljava/util/Queue;

    .line 2159
    invoke-virtual {p1}, Limn;->aXv()Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Link;

    invoke-direct {v0}, Link;-><init>()V

    :goto_3
    iput-object v0, p0, Lins;->dFD:Ljava/util/Queue;

    .line 2162
    return-void

    :cond_3
    move-object v0, v1

    .line 2145
    goto :goto_0

    .line 2151
    :cond_4
    invoke-static {}, Limn;->aXB()Ljava/util/Queue;

    move-result-object v0

    goto :goto_1

    .line 2155
    :cond_5
    invoke-static {}, Limn;->aXB()Ljava/util/Queue;

    move-result-object v0

    goto :goto_2

    .line 2159
    :cond_6
    invoke-static {}, Limn;->aXB()Ljava/util/Queue;

    move-result-object v0

    goto :goto_3
.end method

.method private a(Linr;Limf;)V
    .locals 2

    .prologue
    .line 2394
    invoke-interface {p1}, Linr;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1}, Linr;->aXH()I

    invoke-interface {p1}, Linr;->aXF()Lioj;

    move-result-object v1

    invoke-interface {v1}, Lioj;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, Lins;->a(Ljava/lang/Object;Ljava/lang/Object;Limf;)V

    .line 2395
    return-void
.end method

.method private a(Linr;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2195
    iget-object v0, p0, Lins;->dFu:Limn;

    iget-object v0, v0, Limn;->dEs:Linz;

    invoke-virtual {v0, p0, p1, p2}, Linz;->a(Lins;Linr;Ljava/lang/Object;)Lioj;

    move-result-object v0

    .line 2196
    invoke-interface {p1, v0}, Linr;->b(Lioj;)V

    .line 2197
    invoke-direct {p0}, Lins;->aXS()V

    iget-object v0, p0, Lins;->dFC:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lins;->dFu:Limn;

    invoke-virtual {v0}, Limn;->aXv()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lins;->dFu:Limn;

    invoke-virtual {v0}, Limn;->aXw()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lins;->dFu:Limn;

    iget-wide v0, v0, Limn;->dEu:J

    :goto_0
    invoke-direct {p0, p1, v0, v1}, Lins;->b(Linr;J)V

    iget-object v0, p0, Lins;->dFD:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2198
    :cond_0
    return-void

    .line 2197
    :cond_1
    iget-object v0, p0, Lins;->dFu:Limn;

    iget-wide v0, v0, Limn;->dEt:J

    goto :goto_0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Limf;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2398
    iget-object v0, p0, Lins;->dFu:Limn;

    iget-object v0, v0, Limn;->dEI:Ljava/util/Queue;

    sget-object v1, Limn;->dEM:Ljava/util/Queue;

    if-eq v0, v1, :cond_0

    .line 2399
    new-instance v0, Limm;

    invoke-direct {v0, p1, p2, p3}, Limm;-><init>(Ljava/lang/Object;Ljava/lang/Object;Limf;)V

    .line 2400
    iget-object v1, p0, Lins;->dFu:Limn;

    iget-object v1, v1, Limn;->dEI:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 2402
    :cond_0
    return-void
.end method

.method private a(Linr;ILimf;)Z
    .locals 6

    .prologue
    .line 3024
    iget v0, p0, Lins;->count:I

    .line 3025
    iget-object v2, p0, Lins;->dFw:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3026
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v3, p2, v0

    .line 3027
    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linr;

    move-object v1, v0

    .line 3029
    :goto_0
    if-eqz v1, :cond_1

    .line 3030
    if-ne v1, p1, :cond_0

    .line 3031
    iget v4, p0, Lins;->modCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lins;->modCount:I

    .line 3032
    invoke-interface {v1}, Linr;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1}, Linr;->aXF()Lioj;

    move-result-object v5

    invoke-interface {v5}, Lioj;->get()Ljava/lang/Object;

    move-result-object v5

    invoke-direct {p0, v4, v5, p3}, Lins;->a(Ljava/lang/Object;Ljava/lang/Object;Limf;)V

    .line 3033
    invoke-direct {p0, v0, v1}, Lins;->f(Linr;Linr;)Linr;

    move-result-object v0

    .line 3034
    iget v1, p0, Lins;->count:I

    add-int/lit8 v1, v1, -0x1

    .line 3035
    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 3036
    iput v1, p0, Lins;->count:I

    .line 3037
    const/4 v0, 0x1

    .line 3041
    :goto_1
    return v0

    .line 3029
    :cond_0
    invoke-interface {v1}, Linr;->aXG()Linr;

    move-result-object v1

    goto :goto_0

    .line 3041
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private aXQ()V
    .locals 1

    .prologue
    .line 2206
    invoke-virtual {p0}, Lins;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2208
    :try_start_0
    invoke-direct {p0}, Lins;->aXR()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2210
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2213
    :cond_0
    return-void

    .line 2210
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lins;->unlock()V

    throw v0
.end method

.method private aXR()V
    .locals 5

    .prologue
    const/16 v4, 0x10

    const/4 v2, 0x0

    .line 2221
    iget-object v0, p0, Lins;->dFu:Limn;

    invoke-virtual {v0}, Limn;->aXx()Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    .line 2222
    :goto_0
    iget-object v0, p0, Lins;->dFy:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Linr;

    iget-object v3, p0, Lins;->dFu:Limn;

    invoke-virtual {v3, v0}, Limn;->a(Linr;)V

    add-int/lit8 v0, v1, 0x1

    if-eq v0, v4, :cond_0

    move v1, v0

    goto :goto_0

    .line 2224
    :cond_0
    iget-object v0, p0, Lins;->dFu:Limn;

    invoke-virtual {v0}, Limn;->aXy()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2225
    :cond_1
    iget-object v0, p0, Lins;->dFz:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Lioj;

    iget-object v1, p0, Lins;->dFu:Limn;

    invoke-virtual {v1, v0}, Limn;->a(Lioj;)V

    add-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_1

    .line 2227
    :cond_2
    return-void
.end method

.method private aXS()V
    .locals 2

    .prologue
    .line 2338
    :cond_0
    :goto_0
    iget-object v0, p0, Lins;->dFA:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linr;

    if-eqz v0, :cond_2

    .line 2343
    iget-object v1, p0, Lins;->dFC:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2344
    iget-object v1, p0, Lins;->dFC:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2346
    :cond_1
    iget-object v1, p0, Lins;->dFu:Limn;

    invoke-virtual {v1}, Limn;->aXw()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lins;->dFD:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2347
    iget-object v1, p0, Lins;->dFD:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2350
    :cond_2
    return-void
.end method

.method private aXT()V
    .locals 1

    .prologue
    .line 2363
    invoke-virtual {p0}, Lins;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2365
    :try_start_0
    invoke-direct {p0}, Lins;->aXU()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2367
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2371
    :cond_0
    return-void

    .line 2367
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lins;->unlock()V

    throw v0
.end method

.method private aXU()V
    .locals 5

    .prologue
    .line 2375
    invoke-direct {p0}, Lins;->aXS()V

    .line 2377
    iget-object v0, p0, Lins;->dFD:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2389
    :cond_0
    return-void

    .line 2382
    :cond_1
    iget-object v0, p0, Lins;->dFu:Limn;

    iget-object v0, v0, Limn;->dEK:Lign;

    invoke-virtual {v0}, Lign;->aWc()J

    move-result-wide v2

    .line 2384
    :cond_2
    iget-object v0, p0, Lins;->dFD:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linr;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lins;->dFu:Limn;

    invoke-static {v0, v2, v3}, Limn;->a(Linr;J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2385
    invoke-interface {v0}, Linr;->aXH()I

    move-result v1

    sget-object v4, Limf;->dEC:Limf;

    invoke-direct {p0, v0, v1, v4}, Lins;->a(Linr;ILimf;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2386
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private aXW()V
    .locals 2

    .prologue
    .line 3123
    invoke-virtual {p0}, Lins;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3125
    :try_start_0
    invoke-direct {p0}, Lins;->aXR()V

    .line 3126
    invoke-direct {p0}, Lins;->aXU()V

    .line 3127
    iget-object v0, p0, Lins;->dFB:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3129
    invoke-virtual {p0}, Lins;->unlock()V

    .line 3132
    :cond_0
    return-void

    .line 3129
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lins;->unlock()V

    throw v0
.end method

.method private aXX()V
    .locals 1

    .prologue
    .line 3136
    invoke-virtual {p0}, Lins;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3137
    iget-object v0, p0, Lins;->dFu:Limn;

    invoke-virtual {v0}, Limn;->aXC()V

    .line 3139
    :cond_0
    return-void
.end method

.method private b(Linr;J)V
    .locals 2

    .prologue
    .line 2356
    iget-object v0, p0, Lins;->dFu:Limn;

    iget-object v0, v0, Limn;->dEK:Lign;

    invoke-virtual {v0}, Lign;->aWc()J

    move-result-wide v0

    add-long/2addr v0, p2

    invoke-interface {p1, v0, v1}, Linr;->bR(J)V

    .line 2357
    return-void
.end method

.method private static c(Lioj;)Z
    .locals 1

    .prologue
    .line 3060
    invoke-interface {p0}, Lioj;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Linr;Linr;)Linr;
    .locals 3

    .prologue
    .line 2184
    invoke-interface {p1}, Linr;->aXF()Lioj;

    move-result-object v0

    .line 2185
    iget-object v1, p0, Lins;->dFu:Limn;

    iget-object v1, v1, Limn;->dEJ:Lims;

    invoke-virtual {v1, p0, p1, p2}, Lims;->a(Lins;Linr;Linr;)Linr;

    move-result-object v1

    .line 2186
    iget-object v2, p0, Lins;->dFz:Ljava/lang/ref/ReferenceQueue;

    invoke-interface {v0, v2, v1}, Lioj;->a(Ljava/lang/ref/ReferenceQueue;Linr;)Lioj;

    move-result-object v0

    invoke-interface {v1, v0}, Linr;->b(Lioj;)V

    .line 2187
    return-object v1
.end method

.method private f(Linr;Linr;)Linr;
    .locals 3

    .prologue
    .line 2899
    iget-object v0, p0, Lins;->dFC:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 2900
    iget-object v0, p0, Lins;->dFD:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 2902
    iget v1, p0, Lins;->count:I

    .line 2903
    invoke-interface {p2}, Linr;->aXG()Linr;

    move-result-object v0

    .line 2904
    :goto_0
    if-eq p1, p2, :cond_1

    .line 2905
    invoke-direct {p0, p1}, Lins;->m(Linr;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2906
    invoke-direct {p0, p1}, Lins;->l(Linr;)V

    .line 2907
    add-int/lit8 v1, v1, -0x1

    .line 2904
    :goto_1
    invoke-interface {p1}, Linr;->aXG()Linr;

    move-result-object p1

    goto :goto_0

    .line 2909
    :cond_0
    invoke-direct {p0, p1, v0}, Lins;->e(Linr;Linr;)Linr;

    move-result-object v0

    goto :goto_1

    .line 2912
    :cond_1
    iput v1, p0, Lins;->count:I

    .line 2913
    return-object v0
.end method

.method private k(Ljava/lang/Object;I)Linr;
    .locals 3

    .prologue
    .line 2436
    iget v0, p0, Lins;->count:I

    if-eqz v0, :cond_2

    .line 2437
    iget-object v0, p0, Lins;->dFw:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v1, p2

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linr;

    :goto_0
    if-eqz v0, :cond_2

    .line 2438
    invoke-interface {v0}, Linr;->aXH()I

    move-result v1

    if-ne v1, p2, :cond_0

    .line 2439
    invoke-interface {v0}, Linr;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 2443
    if-nez v1, :cond_1

    .line 2444
    invoke-direct {p0}, Lins;->aXQ()V

    .line 2437
    :cond_0
    invoke-interface {v0}, Linr;->aXG()Linr;

    move-result-object v0

    goto :goto_0

    .line 2448
    :cond_1
    iget-object v2, p0, Lins;->dFu:Limn;

    iget-object v2, v2, Limn;->dEw:Lifc;

    invoke-virtual {v2, p1, v1}, Lifc;->l(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2454
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private k(Linr;)V
    .locals 2

    .prologue
    .line 2302
    iget-object v0, p0, Lins;->dFC:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2303
    iget-object v0, p0, Lins;->dFu:Limn;

    invoke-virtual {v0}, Limn;->aXw()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2304
    iget-object v0, p0, Lins;->dFu:Limn;

    iget-wide v0, v0, Limn;->dEu:J

    invoke-direct {p0, p1, v0, v1}, Lins;->b(Linr;J)V

    .line 2305
    iget-object v0, p0, Lins;->dFD:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 2307
    :cond_0
    return-void
.end method

.method private l(Ljava/lang/Object;I)Linr;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2458
    invoke-direct {p0, p1, p2}, Lins;->k(Ljava/lang/Object;I)Linr;

    move-result-object v1

    .line 2459
    if-nez v1, :cond_0

    .line 2465
    :goto_0
    return-object v0

    .line 2461
    :cond_0
    iget-object v2, p0, Lins;->dFu:Limn;

    invoke-virtual {v2}, Limn;->aXv()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lins;->dFu:Limn;

    invoke-virtual {v2, v1}, Limn;->c(Linr;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2462
    invoke-direct {p0}, Lins;->aXT()V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 2465
    goto :goto_0
.end method

.method private l(Linr;)V
    .locals 1

    .prologue
    .line 2917
    sget-object v0, Limf;->dEB:Limf;

    invoke-direct {p0, p1, v0}, Lins;->a(Linr;Limf;)V

    .line 2918
    iget-object v0, p0, Lins;->dFC:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 2919
    iget-object v0, p0, Lins;->dFD:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 2920
    return-void
.end method

.method private m(Linr;)Z
    .locals 1

    .prologue
    .line 3049
    invoke-interface {p1}, Linr;->getKey()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3050
    const/4 v0, 0x1

    .line 3052
    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Linr;->aXF()Lioj;

    move-result-object v0

    invoke-static {v0}, Lins;->c(Lioj;)Z

    move-result v0

    goto :goto_0
.end method

.method private static mj(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .locals 1

    .prologue
    .line 2165
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-direct {v0, p0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method final a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2724
    invoke-virtual {p0}, Lins;->lock()V

    .line 2726
    :try_start_0
    invoke-direct {p0}, Lins;->aXW()V

    .line 2728
    iget-object v4, p0, Lins;->dFw:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2729
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    .line 2730
    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linr;

    move-object v3, v0

    .line 2732
    :goto_0
    if-eqz v3, :cond_3

    .line 2733
    invoke-interface {v3}, Linr;->getKey()Ljava/lang/Object;

    move-result-object v6

    .line 2734
    invoke-interface {v3}, Linr;->aXH()I

    move-result v2

    if-ne v2, p2, :cond_2

    if-eqz v6, :cond_2

    iget-object v2, p0, Lins;->dFu:Limn;

    iget-object v2, v2, Limn;->dEw:Lifc;

    invoke-virtual {v2, p1, v6}, Lifc;->l(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2738
    invoke-interface {v3}, Linr;->aXF()Lioj;

    move-result-object v7

    .line 2739
    invoke-interface {v7}, Lioj;->get()Ljava/lang/Object;

    move-result-object v2

    .line 2740
    if-nez v2, :cond_1

    .line 2741
    invoke-static {v7}, Lins;->c(Lioj;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2742
    iget v7, p0, Lins;->count:I

    .line 2743
    iget v7, p0, Lins;->modCount:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lins;->modCount:I

    .line 2744
    sget-object v7, Limf;->dEB:Limf;

    invoke-direct {p0, v6, v2, v7}, Lins;->a(Ljava/lang/Object;Ljava/lang/Object;Limf;)V

    .line 2745
    invoke-direct {p0, v0, v3}, Lins;->f(Linr;Linr;)Linr;

    move-result-object v0

    .line 2746
    iget v2, p0, Lins;->count:I

    add-int/lit8 v2, v2, -0x1

    .line 2747
    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2748
    iput v2, p0, Lins;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2750
    :cond_0
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2763
    invoke-direct {p0}, Lins;->aXX()V

    move-object v0, v1

    :goto_1
    return-object v0

    .line 2753
    :cond_1
    :try_start_1
    iget v0, p0, Lins;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lins;->modCount:I

    .line 2754
    sget-object v0, Limf;->dEA:Limf;

    invoke-direct {p0, p1, v2, v0}, Lins;->a(Ljava/lang/Object;Ljava/lang/Object;Limf;)V

    .line 2755
    invoke-direct {p0, v3, p3}, Lins;->a(Linr;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2762
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2763
    invoke-direct {p0}, Lins;->aXX()V

    move-object v0, v2

    goto :goto_1

    .line 2732
    :cond_2
    :try_start_2
    invoke-interface {v3}, Linr;->aXG()Linr;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    move-object v3, v2

    goto :goto_0

    .line 2760
    :cond_3
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2763
    invoke-direct {p0}, Lins;->aXX()V

    move-object v0, v1

    goto :goto_1

    .line 2762
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lins;->unlock()V

    .line 2763
    invoke-direct {p0}, Lins;->aXX()V

    throw v0
.end method

.method final a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2533
    invoke-virtual {p0}, Lins;->lock()V

    .line 2535
    :try_start_0
    invoke-direct {p0}, Lins;->aXW()V

    .line 2537
    iget v0, p0, Lins;->count:I

    add-int/lit8 v1, v0, 0x1

    .line 2538
    iget v0, p0, Lins;->dFv:I

    if-le v1, v0, :cond_5

    .line 2539
    iget-object v7, p0, Lins;->dFw:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v8

    const/high16 v0, 0x40000000    # 2.0f

    if-ge v8, v0, :cond_4

    iget v1, p0, Lins;->count:I

    shl-int/lit8 v0, v8, 0x1

    invoke-static {v0}, Lins;->mj(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, Lins;->dFv:I

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v8, :cond_3

    invoke-virtual {v7, v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linr;

    if-eqz v0, :cond_d

    invoke-interface {v0}, Linr;->aXG()Linr;

    move-result-object v3

    invoke-interface {v0}, Linr;->aXH()I

    move-result v2

    and-int v5, v2, v10

    if-nez v3, :cond_0

    invoke-virtual {v9, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v0, v1

    :goto_1
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move v1, v0

    goto :goto_0

    :cond_0
    move-object v4, v0

    :goto_2
    if-eqz v3, :cond_1

    invoke-interface {v3}, Linr;->aXH()I

    move-result v2

    and-int/2addr v2, v10

    if-eq v2, v5, :cond_e

    move v4, v2

    move-object v2, v3

    :goto_3
    invoke-interface {v3}, Linr;->aXG()Linr;

    move-result-object v3

    move v5, v4

    move-object v4, v2

    goto :goto_2

    :cond_1
    invoke-virtual {v9, v5, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move-object v2, v0

    :goto_4
    if-eq v2, v4, :cond_d

    invoke-direct {p0, v2}, Lins;->m(Linr;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v2}, Lins;->l(Linr;)V

    add-int/lit8 v1, v1, -0x1

    :goto_5
    invoke-interface {v2}, Linr;->aXG()Linr;

    move-result-object v0

    move-object v2, v0

    goto :goto_4

    :cond_2
    invoke-interface {v2}, Linr;->aXH()I

    move-result v0

    and-int v3, v0, v10

    invoke-virtual {v9, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linr;

    invoke-direct {p0, v2, v0}, Lins;->e(Linr;Linr;)Linr;

    move-result-object v0

    invoke-virtual {v9, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_5

    .line 2595
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lins;->unlock()V

    .line 2596
    invoke-direct {p0}, Lins;->aXX()V

    throw v0

    .line 2539
    :cond_3
    :try_start_1
    iput-object v9, p0, Lins;->dFw:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput v1, p0, Lins;->count:I

    .line 2540
    :cond_4
    iget v0, p0, Lins;->count:I

    add-int/lit8 v1, v0, 0x1

    .line 2543
    :cond_5
    iget-object v3, p0, Lins;->dFw:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2544
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    .line 2545
    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linr;

    move-object v2, v0

    .line 2548
    :goto_6
    if-eqz v2, :cond_9

    .line 2549
    invoke-interface {v2}, Linr;->getKey()Ljava/lang/Object;

    move-result-object v5

    .line 2550
    invoke-interface {v2}, Linr;->aXH()I

    move-result v6

    if-ne v6, p2, :cond_8

    if-eqz v5, :cond_8

    iget-object v6, p0, Lins;->dFu:Limn;

    iget-object v6, v6, Limn;->dEw:Lifc;

    invoke-virtual {v6, p1, v5}, Lifc;->l(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 2554
    invoke-interface {v2}, Linr;->aXF()Lioj;

    move-result-object v0

    .line 2555
    invoke-interface {v0}, Lioj;->get()Ljava/lang/Object;

    move-result-object v0

    .line 2557
    if-nez v0, :cond_6

    .line 2558
    iget v1, p0, Lins;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lins;->modCount:I

    .line 2559
    invoke-direct {p0, v2, p3}, Lins;->a(Linr;Ljava/lang/Object;)V

    .line 2560
    sget-object v1, Limf;->dEB:Limf;

    invoke-direct {p0, p1, v0, v1}, Lins;->a(Ljava/lang/Object;Ljava/lang/Object;Limf;)V

    .line 2562
    iget v0, p0, Lins;->count:I

    .line 2563
    iput v0, p0, Lins;->count:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2567
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2596
    invoke-direct {p0}, Lins;->aXX()V

    const/4 v0, 0x0

    :goto_7
    return-object v0

    .line 2568
    :cond_6
    if-eqz p4, :cond_7

    .line 2572
    :try_start_2
    invoke-direct {p0, v2}, Lins;->k(Linr;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2595
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2596
    invoke-direct {p0}, Lins;->aXX()V

    goto :goto_7

    .line 2576
    :cond_7
    :try_start_3
    iget v1, p0, Lins;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lins;->modCount:I

    .line 2577
    sget-object v1, Limf;->dEA:Limf;

    invoke-direct {p0, p1, v0, v1}, Lins;->a(Ljava/lang/Object;Ljava/lang/Object;Limf;)V

    .line 2578
    invoke-direct {p0, v2, p3}, Lins;->a(Linr;Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2595
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2596
    invoke-direct {p0}, Lins;->aXX()V

    goto :goto_7

    .line 2548
    :cond_8
    :try_start_4
    invoke-interface {v2}, Linr;->aXG()Linr;

    move-result-object v2

    goto :goto_6

    .line 2585
    :cond_9
    iget v2, p0, Lins;->modCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lins;->modCount:I

    .line 2586
    iget-object v2, p0, Lins;->dFu:Limn;

    iget-object v2, v2, Limn;->dEJ:Lims;

    invoke-virtual {v2, p0, p1, p2, v0}, Lims;->a(Lins;Ljava/lang/Object;ILinr;)Linr;

    move-result-object v0

    .line 2587
    invoke-direct {p0, v0, p3}, Lins;->a(Linr;Ljava/lang/Object;)V

    .line 2588
    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2589
    iget-object v0, p0, Lins;->dFu:Limn;

    invoke-virtual {v0}, Limn;->aXu()Z

    move-result v0

    if-eqz v0, :cond_b

    iget v0, p0, Lins;->count:I

    iget v2, p0, Lins;->dFx:I

    if-lt v0, v2, :cond_b

    invoke-direct {p0}, Lins;->aXS()V

    iget-object v0, p0, Lins;->dFC:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linr;

    invoke-interface {v0}, Linr;->aXH()I

    move-result v2

    sget-object v3, Limf;->dED:Limf;

    invoke-direct {p0, v0, v2, v3}, Lins;->a(Linr;ILimf;)Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_a
    const/4 v0, 0x1

    :goto_8
    if-eqz v0, :cond_c

    .line 2590
    iget v0, p0, Lins;->count:I

    add-int/lit8 v0, v0, 0x1

    .line 2592
    :goto_9
    iput v0, p0, Lins;->count:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2593
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2596
    invoke-direct {p0}, Lins;->aXX()V

    const/4 v0, 0x0

    goto :goto_7

    .line 2589
    :cond_b
    const/4 v0, 0x0

    goto :goto_8

    :cond_c
    move v0, v1

    goto :goto_9

    :cond_d
    move v0, v1

    goto/16 :goto_1

    :cond_e
    move-object v2, v4

    move v4, v5

    goto/16 :goto_3
.end method

.method final a(Linr;I)Z
    .locals 7

    .prologue
    .line 2926
    invoke-virtual {p0}, Lins;->lock()V

    .line 2928
    :try_start_0
    iget v0, p0, Lins;->count:I

    .line 2929
    iget-object v2, p0, Lins;->dFw:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2930
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v3, p2, v0

    .line 2931
    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linr;

    move-object v1, v0

    .line 2933
    :goto_0
    if-eqz v1, :cond_1

    .line 2934
    if-ne v1, p1, :cond_0

    .line 2935
    iget v4, p0, Lins;->modCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lins;->modCount:I

    .line 2936
    invoke-interface {v1}, Linr;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1}, Linr;->aXF()Lioj;

    move-result-object v5

    invoke-interface {v5}, Lioj;->get()Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Limf;->dEB:Limf;

    invoke-direct {p0, v4, v5, v6}, Lins;->a(Ljava/lang/Object;Ljava/lang/Object;Limf;)V

    .line 2938
    invoke-direct {p0, v0, v1}, Lins;->f(Linr;Linr;)Linr;

    move-result-object v0

    .line 2939
    iget v1, p0, Lins;->count:I

    add-int/lit8 v1, v1, -0x1

    .line 2940
    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2941
    iput v1, p0, Lins;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2942
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2949
    invoke-direct {p0}, Lins;->aXX()V

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 2933
    :cond_0
    :try_start_1
    invoke-interface {v1}, Linr;->aXG()Linr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 2946
    :cond_1
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2949
    invoke-direct {p0}, Lins;->aXX()V

    const/4 v0, 0x0

    goto :goto_1

    .line 2948
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lins;->unlock()V

    .line 2949
    invoke-direct {p0}, Lins;->aXX()V

    throw v0
.end method

.method final a(Ljava/lang/Object;ILioj;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2957
    invoke-virtual {p0}, Lins;->lock()V

    .line 2959
    :try_start_0
    iget v0, p0, Lins;->count:I

    .line 2960
    iget-object v3, p0, Lins;->dFw:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2961
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    .line 2962
    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linr;

    move-object v2, v0

    .line 2964
    :goto_0
    if-eqz v2, :cond_4

    .line 2965
    invoke-interface {v2}, Linr;->getKey()Ljava/lang/Object;

    move-result-object v5

    .line 2966
    invoke-interface {v2}, Linr;->aXH()I

    move-result v6

    if-ne v6, p2, :cond_3

    if-eqz v5, :cond_3

    iget-object v6, p0, Lins;->dFu:Limn;

    iget-object v6, v6, Limn;->dEw:Lifc;

    invoke-virtual {v6, p1, v5}, Lifc;->l(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2968
    invoke-interface {v2}, Linr;->aXF()Lioj;

    move-result-object v5

    .line 2969
    if-ne v5, p3, :cond_1

    .line 2970
    iget v1, p0, Lins;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lins;->modCount:I

    .line 2971
    invoke-interface {p3}, Lioj;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v5, Limf;->dEB:Limf;

    invoke-direct {p0, p1, v1, v5}, Lins;->a(Ljava/lang/Object;Ljava/lang/Object;Limf;)V

    .line 2972
    invoke-direct {p0, v0, v2}, Lins;->f(Linr;Linr;)Linr;

    move-result-object v0

    .line 2973
    iget v1, p0, Lins;->count:I

    add-int/lit8 v1, v1, -0x1

    .line 2974
    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2975
    iput v1, p0, Lins;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2976
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2985
    invoke-virtual {p0}, Lins;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2986
    invoke-direct {p0}, Lins;->aXX()V

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 2978
    :cond_1
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2985
    invoke-virtual {p0}, Lins;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2986
    invoke-direct {p0}, Lins;->aXX()V

    :cond_2
    move v0, v1

    goto :goto_1

    .line 2964
    :cond_3
    :try_start_1
    invoke-interface {v2}, Linr;->aXG()Linr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 2982
    :cond_4
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2985
    invoke-virtual {p0}, Lins;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2986
    invoke-direct {p0}, Lins;->aXX()V

    :cond_5
    move v0, v1

    goto :goto_1

    .line 2984
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lins;->unlock()V

    .line 2985
    invoke-virtual {p0}, Lins;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_6

    .line 2986
    invoke-direct {p0}, Lins;->aXX()V

    :cond_6
    throw v0
.end method

.method final a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2673
    invoke-virtual {p0}, Lins;->lock()V

    .line 2675
    :try_start_0
    invoke-direct {p0}, Lins;->aXW()V

    .line 2677
    iget-object v3, p0, Lins;->dFw:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2678
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    .line 2679
    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linr;

    move-object v2, v0

    .line 2681
    :goto_0
    if-eqz v2, :cond_4

    .line 2682
    invoke-interface {v2}, Linr;->getKey()Ljava/lang/Object;

    move-result-object v5

    .line 2683
    invoke-interface {v2}, Linr;->aXH()I

    move-result v6

    if-ne v6, p2, :cond_3

    if-eqz v5, :cond_3

    iget-object v6, p0, Lins;->dFu:Limn;

    iget-object v6, v6, Limn;->dEw:Lifc;

    invoke-virtual {v6, p1, v5}, Lifc;->l(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2687
    invoke-interface {v2}, Linr;->aXF()Lioj;

    move-result-object v6

    .line 2688
    invoke-interface {v6}, Lioj;->get()Ljava/lang/Object;

    move-result-object v7

    .line 2689
    if-nez v7, :cond_1

    .line 2690
    invoke-static {v6}, Lins;->c(Lioj;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2691
    iget v6, p0, Lins;->count:I

    .line 2692
    iget v6, p0, Lins;->modCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lins;->modCount:I

    .line 2693
    sget-object v6, Limf;->dEB:Limf;

    invoke-direct {p0, v5, v7, v6}, Lins;->a(Ljava/lang/Object;Ljava/lang/Object;Limf;)V

    .line 2694
    invoke-direct {p0, v0, v2}, Lins;->f(Linr;Linr;)Linr;

    move-result-object v0

    .line 2695
    iget v2, p0, Lins;->count:I

    add-int/lit8 v2, v2, -0x1

    .line 2696
    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2697
    iput v2, p0, Lins;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2699
    :cond_0
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2719
    invoke-direct {p0}, Lins;->aXX()V

    move v0, v1

    :goto_1
    return v0

    .line 2702
    :cond_1
    :try_start_1
    iget-object v0, p0, Lins;->dFu:Limn;

    iget-object v0, v0, Limn;->dEx:Lifc;

    invoke-virtual {v0, p3, v7}, Lifc;->l(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2703
    iget v0, p0, Lins;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lins;->modCount:I

    .line 2704
    sget-object v0, Limf;->dEA:Limf;

    invoke-direct {p0, p1, v7, v0}, Lins;->a(Ljava/lang/Object;Ljava/lang/Object;Limf;)V

    .line 2705
    invoke-direct {p0, v2, p4}, Lins;->a(Linr;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2706
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2719
    invoke-direct {p0}, Lins;->aXX()V

    const/4 v0, 0x1

    goto :goto_1

    .line 2710
    :cond_2
    :try_start_2
    invoke-direct {p0, v2}, Lins;->k(Linr;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2711
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2719
    invoke-direct {p0}, Lins;->aXX()V

    move v0, v1

    goto :goto_1

    .line 2681
    :cond_3
    :try_start_3
    invoke-interface {v2}, Linr;->aXG()Linr;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 2716
    :cond_4
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2719
    invoke-direct {p0}, Lins;->aXX()V

    move v0, v1

    goto :goto_1

    .line 2718
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lins;->unlock()V

    .line 2719
    invoke-direct {p0}, Lins;->aXX()V

    throw v0
.end method

.method final aXV()V
    .locals 1

    .prologue
    .line 3094
    iget-object v0, p0, Lins;->dFB:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    and-int/lit8 v0, v0, 0x3f

    if-nez v0, :cond_0

    .line 3095
    invoke-direct {p0}, Lins;->aXW()V

    invoke-direct {p0}, Lins;->aXX()V

    .line 3097
    :cond_0
    return-void
.end method

.method final b(Linr;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3071
    invoke-interface {p1}, Linr;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 3072
    invoke-direct {p0}, Lins;->aXQ()V

    .line 3085
    :goto_0
    return-object v0

    .line 3075
    :cond_0
    invoke-interface {p1}, Linr;->aXF()Lioj;

    move-result-object v1

    invoke-interface {v1}, Lioj;->get()Ljava/lang/Object;

    move-result-object v1

    .line 3076
    if-nez v1, :cond_1

    .line 3077
    invoke-direct {p0}, Lins;->aXQ()V

    goto :goto_0

    .line 3081
    :cond_1
    iget-object v2, p0, Lins;->dFu:Limn;

    invoke-virtual {v2}, Limn;->aXv()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lins;->dFu:Limn;

    invoke-virtual {v2, p1}, Limn;->c(Linr;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3082
    invoke-direct {p0}, Lins;->aXT()V

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 3085
    goto :goto_0
.end method

.method final b(Ljava/lang/Object;ILjava/lang/Object;)Z
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2811
    invoke-virtual {p0}, Lins;->lock()V

    .line 2813
    :try_start_0
    invoke-direct {p0}, Lins;->aXW()V

    .line 2815
    iget v0, p0, Lins;->count:I

    .line 2816
    iget-object v4, p0, Lins;->dFw:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2817
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    .line 2818
    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linr;

    move-object v3, v0

    .line 2820
    :goto_0
    if-eqz v3, :cond_4

    .line 2821
    invoke-interface {v3}, Linr;->getKey()Ljava/lang/Object;

    move-result-object v6

    .line 2822
    invoke-interface {v3}, Linr;->aXH()I

    move-result v2

    if-ne v2, p2, :cond_3

    if-eqz v6, :cond_3

    iget-object v2, p0, Lins;->dFu:Limn;

    iget-object v2, v2, Limn;->dEw:Lifc;

    invoke-virtual {v2, p1, v6}, Lifc;->l(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2824
    invoke-interface {v3}, Linr;->aXF()Lioj;

    move-result-object v2

    .line 2825
    invoke-interface {v2}, Lioj;->get()Ljava/lang/Object;

    move-result-object v7

    .line 2828
    iget-object v8, p0, Lins;->dFu:Limn;

    iget-object v8, v8, Limn;->dEx:Lifc;

    invoke-virtual {v8, p3, v7}, Lifc;->l(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2829
    sget-object v2, Limf;->dEz:Limf;

    .line 2836
    :goto_1
    iget v8, p0, Lins;->modCount:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lins;->modCount:I

    .line 2837
    invoke-direct {p0, v6, v7, v2}, Lins;->a(Ljava/lang/Object;Ljava/lang/Object;Limf;)V

    .line 2838
    invoke-direct {p0, v0, v3}, Lins;->f(Linr;Linr;)Linr;

    move-result-object v0

    .line 2839
    iget v3, p0, Lins;->count:I

    add-int/lit8 v3, v3, -0x1

    .line 2840
    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2841
    iput v3, p0, Lins;->count:I

    .line 2842
    sget-object v0, Limf;->dEz:Limf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v2, v0, :cond_2

    const/4 v0, 0x1

    .line 2848
    :goto_2
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2849
    invoke-direct {p0}, Lins;->aXX()V

    move v1, v0

    :goto_3
    return v1

    .line 2830
    :cond_0
    :try_start_1
    invoke-static {v2}, Lins;->c(Lioj;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2831
    sget-object v2, Limf;->dEB:Limf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2833
    :cond_1
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2849
    invoke-direct {p0}, Lins;->aXX()V

    goto :goto_3

    :cond_2
    move v0, v1

    .line 2842
    goto :goto_2

    .line 2820
    :cond_3
    :try_start_2
    invoke-interface {v3}, Linr;->aXG()Linr;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    move-object v3, v2

    goto :goto_0

    .line 2846
    :cond_4
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2849
    invoke-direct {p0}, Lins;->aXX()V

    goto :goto_3

    .line 2848
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lins;->unlock()V

    .line 2849
    invoke-direct {p0}, Lins;->aXX()V

    throw v0
.end method

.method final clear()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2854
    iget v0, p0, Lins;->count:I

    if-eqz v0, :cond_7

    .line 2855
    invoke-virtual {p0}, Lins;->lock()V

    .line 2857
    :try_start_0
    iget-object v3, p0, Lins;->dFw:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2858
    iget-object v0, p0, Lins;->dFu:Limn;

    iget-object v0, v0, Limn;->dEI:Ljava/util/Queue;

    sget-object v2, Limn;->dEM:Ljava/util/Queue;

    if-eq v0, v2, :cond_1

    move v2, v1

    .line 2859
    :goto_0
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 2860
    invoke-virtual {v3, v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linr;

    :goto_1
    if-eqz v0, :cond_0

    .line 2862
    invoke-interface {v0}, Linr;->aXF()Lioj;

    .line 2863
    sget-object v4, Limf;->dEz:Limf;

    invoke-direct {p0, v0, v4}, Lins;->a(Linr;Limf;)V

    .line 2860
    invoke-interface {v0}, Linr;->aXG()Linr;

    move-result-object v0

    goto :goto_1

    .line 2859
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2868
    :goto_2
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 2869
    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2868
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2871
    :cond_2
    iget-object v0, p0, Lins;->dFu:Limn;

    invoke-virtual {v0}, Limn;->aXx()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, Lins;->dFy:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_4
    iget-object v0, p0, Lins;->dFu:Limn;

    invoke-virtual {v0}, Limn;->aXy()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    iget-object v0, p0, Lins;->dFz:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_5

    .line 2872
    :cond_6
    iget-object v0, p0, Lins;->dFC:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 2873
    iget-object v0, p0, Lins;->dFD:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 2874
    iget-object v0, p0, Lins;->dFB:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 2876
    iget v0, p0, Lins;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lins;->modCount:I

    .line 2877
    const/4 v0, 0x0

    iput v0, p0, Lins;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2879
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2880
    invoke-direct {p0}, Lins;->aXX()V

    .line 2883
    :cond_7
    return-void

    .line 2879
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lins;->unlock()V

    .line 2880
    invoke-direct {p0}, Lins;->aXX()V

    throw v0
.end method

.method final get(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2470
    :try_start_0
    invoke-direct {p0, p1, p2}, Lins;->l(Ljava/lang/Object;I)Linr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2471
    if-nez v1, :cond_0

    .line 2472
    invoke-virtual {p0}, Lins;->aXV()V

    const/4 v0, 0x0

    .line 2483
    :goto_0
    return-object v0

    .line 2475
    :cond_0
    :try_start_1
    invoke-interface {v1}, Linr;->aXF()Lioj;

    move-result-object v0

    invoke-interface {v0}, Lioj;->get()Ljava/lang/Object;

    move-result-object v0

    .line 2476
    if-eqz v0, :cond_2

    .line 2477
    iget-object v2, p0, Lins;->dFu:Limn;

    invoke-virtual {v2}, Limn;->aXw()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lins;->dFu:Limn;

    iget-wide v2, v2, Limn;->dEu:J

    invoke-direct {p0, v1, v2, v3}, Lins;->b(Linr;J)V

    :cond_1
    iget-object v2, p0, Lins;->dFA:Ljava/util/Queue;

    invoke-interface {v2, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2483
    :goto_1
    invoke-virtual {p0}, Lins;->aXV()V

    goto :goto_0

    .line 2479
    :cond_2
    :try_start_2
    invoke-direct {p0}, Lins;->aXQ()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 2483
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lins;->aXV()V

    throw v0
.end method

.method final m(Ljava/lang/Object;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2489
    :try_start_0
    iget v1, p0, Lins;->count:I

    if-eqz v1, :cond_2

    .line 2490
    invoke-direct {p0, p1, p2}, Lins;->l(Ljava/lang/Object;I)Linr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2491
    if-nez v1, :cond_0

    .line 2492
    invoke-virtual {p0}, Lins;->aXV()V

    .line 2499
    :goto_0
    return v0

    .line 2494
    :cond_0
    :try_start_1
    invoke-interface {v1}, Linr;->aXF()Lioj;

    move-result-object v1

    invoke-interface {v1}, Lioj;->get()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 2499
    :cond_1
    invoke-virtual {p0}, Lins;->aXV()V

    goto :goto_0

    .line 2497
    :cond_2
    invoke-virtual {p0}, Lins;->aXV()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lins;->aXV()V

    throw v0
.end method

.method final n(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2768
    invoke-virtual {p0}, Lins;->lock()V

    .line 2770
    :try_start_0
    invoke-direct {p0}, Lins;->aXW()V

    .line 2772
    iget v0, p0, Lins;->count:I

    .line 2773
    iget-object v4, p0, Lins;->dFw:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 2774
    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v5, p2, v0

    .line 2775
    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linr;

    move-object v3, v0

    .line 2777
    :goto_0
    if-eqz v3, :cond_3

    .line 2778
    invoke-interface {v3}, Linr;->getKey()Ljava/lang/Object;

    move-result-object v6

    .line 2779
    invoke-interface {v3}, Linr;->aXH()I

    move-result v2

    if-ne v2, p2, :cond_2

    if-eqz v6, :cond_2

    iget-object v2, p0, Lins;->dFu:Limn;

    iget-object v2, v2, Limn;->dEw:Lifc;

    invoke-virtual {v2, p1, v6}, Lifc;->l(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2781
    invoke-interface {v3}, Linr;->aXF()Lioj;

    move-result-object v7

    .line 2782
    invoke-interface {v7}, Lioj;->get()Ljava/lang/Object;

    move-result-object v2

    .line 2785
    if-eqz v2, :cond_0

    .line 2786
    sget-object v1, Limf;->dEz:Limf;

    .line 2793
    :goto_1
    iget v7, p0, Lins;->modCount:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lins;->modCount:I

    .line 2794
    invoke-direct {p0, v6, v2, v1}, Lins;->a(Ljava/lang/Object;Ljava/lang/Object;Limf;)V

    .line 2795
    invoke-direct {p0, v0, v3}, Lins;->f(Linr;Linr;)Linr;

    move-result-object v0

    .line 2796
    iget v1, p0, Lins;->count:I

    add-int/lit8 v1, v1, -0x1

    .line 2797
    invoke-virtual {v4, v5, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 2798
    iput v1, p0, Lins;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2805
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2806
    invoke-direct {p0}, Lins;->aXX()V

    move-object v0, v2

    :goto_2
    return-object v0

    .line 2787
    :cond_0
    :try_start_1
    invoke-static {v7}, Lins;->c(Lioj;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2788
    sget-object v1, Limf;->dEB:Limf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2790
    :cond_1
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2806
    invoke-direct {p0}, Lins;->aXX()V

    move-object v0, v1

    goto :goto_2

    .line 2777
    :cond_2
    :try_start_2
    invoke-interface {v3}, Linr;->aXG()Linr;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    move-object v3, v2

    goto :goto_0

    .line 2803
    :cond_3
    invoke-virtual {p0}, Lins;->unlock()V

    .line 2806
    invoke-direct {p0}, Lins;->aXX()V

    move-object v0, v1

    goto :goto_2

    .line 2805
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lins;->unlock()V

    .line 2806
    invoke-direct {p0}, Lins;->aXX()V

    throw v0
.end method
