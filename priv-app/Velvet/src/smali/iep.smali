.class public final Liep;
.super Ljsl;
.source "PG"


# instance fields
.field public hour:I

.field public minute:I

.field public second:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 47
    iput v0, p0, Liep;->hour:I

    iput v0, p0, Liep;->minute:I

    iput v0, p0, Liep;->second:I

    const/4 v0, 0x0

    iput-object v0, p0, Liep;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Liep;->eCz:I

    .line 48
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 20
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liep;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Liep;->hour:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Liep;->minute:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Liep;->second:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 62
    const/4 v0, 0x1

    iget v1, p0, Liep;->hour:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 63
    const/4 v0, 0x2

    iget v1, p0, Liep;->minute:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 64
    const/4 v0, 0x3

    iget v1, p0, Liep;->second:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 65
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 66
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 70
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 71
    const/4 v1, 0x1

    iget v2, p0, Liep;->hour:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    const/4 v1, 0x2

    iget v2, p0, Liep;->minute:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    const/4 v1, 0x3

    iget v2, p0, Liep;->second:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    return v0
.end method
