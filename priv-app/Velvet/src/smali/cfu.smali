.class public final Lcfu;
.super Landroid/database/DataSetObservable;
.source "PG"

# interfaces
.implements Lcox;


# instance fields
.field private final aUA:I

.field final aUB:Ljava/util/LinkedHashMap;

.field private final aUC:Ljava/util/List;

.field private aUD:Ljiz;

.field private anR:Z

.field private mContext:Landroid/content/Context;

.field private mSettings:Lcke;

.field private final mUiExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcke;Ljava/util/concurrent/Executor;I)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/database/DataSetObservable;-><init>()V

    .line 61
    iput-object p2, p0, Lcfu;->mSettings:Lcke;

    .line 62
    iput-object p3, p0, Lcfu;->mUiExecutor:Ljava/util/concurrent/Executor;

    .line 63
    iput p4, p0, Lcfu;->aUA:I

    .line 65
    invoke-static {}, Lior;->aYa()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcfu;->aUB:Ljava/util/LinkedHashMap;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcfu;->aUC:Ljava/util/List;

    .line 67
    iput-object p1, p0, Lcfu;->mContext:Landroid/content/Context;

    .line 68
    return-void
.end method

.method private EE()Ljiz;
    .locals 2

    .prologue
    .line 233
    :try_start_0
    iget-object v0, p0, Lcfu;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080003

    invoke-static {v0, v1}, Lesp;->c(Landroid/content/res/Resources;I)[B

    move-result-object v0

    invoke-static {v0}, Ljiz;->an([B)Ljiz;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 235
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot read defalut corpora from resources."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private m([B)Ljiz;
    .locals 1

    .prologue
    .line 240
    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    .line 242
    :try_start_0
    invoke-static {p1}, Ljiz;->an([B)Ljiz;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 247
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    invoke-direct {p0}, Lcfu;->EE()Ljiz;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final EA()Lckh;
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcfu;->aUB:Ljava/util/LinkedHashMap;

    const-string v1, "web"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lckh;

    return-object v0
.end method

.method public final EB()Z
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0}, Lcfu;->EA()Lckh;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final EC()Ljava/lang/Iterable;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcfu;->aUC:Ljava/util/List;

    return-object v0
.end method

.method public final ED()Ljava/lang/Iterable;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcfu;->aUB:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final Ez()I
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x2

    return v0
.end method

.method public final a(Ljiz;)V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 161
    iget-object v0, p0, Lcfu;->aUD:Ljiz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcfu;->aUD:Ljiz;

    invoke-static {v0, p1}, Leqh;->c(Ljsr;Ljsr;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 163
    :cond_0
    iput-object p1, p0, Lcfu;->aUD:Ljiz;

    .line 164
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 165
    const-string v1, ""

    .line 166
    iget-object v8, p1, Ljiz;->eoe:[Ljja;

    array-length v9, v8

    move v6, v3

    :goto_0
    if-ge v6, v9, :cond_b

    aget-object v10, v8, v6

    .line 167
    invoke-virtual {v10}, Ljja;->bnH()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcfu;->aUA:I

    invoke-virtual {v10}, Ljja;->bnG()I

    move-result v4

    if-ge v0, v4, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_1

    .line 168
    invoke-virtual {v10}, Ljja;->bnL()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    :goto_2
    invoke-virtual {v10}, Ljja;->bnM()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    move v4, v2

    :goto_3
    invoke-virtual {v10}, Ljja;->bnN()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    move v5, v2

    :goto_4
    if-nez v0, :cond_7

    if-nez v4, :cond_7

    if-nez v5, :cond_7

    move v0, v3

    :goto_5
    if-eqz v0, :cond_9

    .line 169
    invoke-static {v10}, Lcic;->a(Ljja;)Lcic;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    :cond_1
    :goto_6
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 167
    :cond_2
    invoke-virtual {v10}, Ljja;->bnJ()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcfu;->aUA:I

    invoke-virtual {v10}, Ljja;->bnI()I

    move-result v4

    if-le v0, v4, :cond_3

    move v0, v3

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v3

    .line 168
    goto :goto_2

    :cond_5
    move v4, v3

    goto :goto_3

    :cond_6
    move v5, v3

    goto :goto_4

    :cond_7
    if-eqz v0, :cond_8

    if-eqz v4, :cond_8

    if-eqz v5, :cond_8

    move v0, v2

    goto :goto_5

    :cond_8
    const-string v0, "Velvet.IntentCorpus"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Only some fields of intent corpora set\nmIntentPackage: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljja;->bnL()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\nmIntentActivity: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v10}, Ljja;->bnM()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\nmIntentUriPattern: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v10}, Ljja;->bnN()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v3, [Ljava/lang/Object;

    const/4 v11, 0x5

    invoke-static {v11, v0, v4, v5}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v0, v3

    goto :goto_5

    .line 172
    :cond_9
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 173
    const-string v0, ""

    invoke-static {v10, v0}, Lckh;->a(Ljja;Ljava/lang/String;)Lckh;

    move-result-object v0

    .line 174
    invoke-virtual {v0}, Lckh;->OL()Ljava/lang/String;

    move-result-object v1

    .line 176
    iget-object v4, v0, Lcfy;->aqV:Ljava/lang/String;

    const-string v5, "web"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    invoke-static {v4}, Lifv;->gY(Z)V

    .line 182
    :goto_7
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 178
    :cond_a
    invoke-static {v10, v1}, Lckh;->a(Ljja;Ljava/lang/String;)Lckh;

    move-result-object v0

    .line 180
    iget-object v4, p0, Lcfu;->aUC:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 186
    :cond_b
    iget-object v0, p0, Lcfu;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcfv;

    const-string v2, "addCorpora"

    invoke-direct {v1, p0, v2, v7}, Lcfv;-><init>(Lcfu;Ljava/lang/String;Ljava/lang/Iterable;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 188
    :cond_c
    return-void
.end method

.method public final a(Ljje;Z)V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p1, Ljje;->eoC:Ljiz;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcfu;->mSettings:Lcke;

    iget-object v1, p1, Ljje;->eoC:Ljiz;

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-interface {v0, v1}, Lcke;->q([B)V

    .line 82
    if-nez p2, :cond_0

    .line 83
    iget-object v0, p1, Ljje;->eoC:Ljiz;

    invoke-virtual {p0, v0}, Lcfu;->a(Ljiz;)V

    .line 86
    :cond_0
    return-void
.end method

.method public final ci(Z)V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcfu;->mSettings:Lcke;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcke;->q([B)V

    .line 91
    if-nez p1, :cond_0

    .line 92
    invoke-direct {p0}, Lcfu;->EE()Ljiz;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcfu;->a(Ljiz;)V

    .line 94
    :cond_0
    return-void
.end method

.method public final declared-synchronized ed()V
    .locals 7

    .prologue
    .line 133
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcfu;->anR:Z

    if-nez v0, :cond_0

    .line 134
    new-instance v0, Lcfy;

    const-string v1, "summons"

    const/4 v2, -0x1

    iget-object v3, p0, Lcfu;->mContext:Landroid/content/Context;

    const v4, 0x7f020208

    invoke-static {v3, v4}, Lesp;->f(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcfu;->mContext:Landroid/content/Context;

    const v5, 0x7f0a0577

    invoke-static {v4, v5}, Lesp;->f(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v4

    const v5, 0x7f04004e

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcfy;-><init>(Ljava/lang/String;ILandroid/net/Uri;Landroid/net/Uri;ILjava/util/Map;)V

    iget-object v1, p0, Lcfu;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcfw;

    const-string v3, "addSummonsCorpus"

    invoke-direct {v2, p0, v3, v0}, Lcfw;-><init>(Lcfu;Ljava/lang/String;Lcfy;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 135
    iget-object v0, p0, Lcfu;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->NW()[B

    move-result-object v0

    .line 136
    invoke-direct {p0, v0}, Lcfu;->m([B)Ljiz;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcfu;->a(Ljiz;)V

    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcfu;->anR:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    :cond_0
    monitor-exit p0

    return-void

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final gh(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "web."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 114
    iget-object v1, p0, Lcfu;->aUB:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "web"

    goto :goto_0
.end method

.method public final gi(Ljava/lang/String;)Lcfy;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 118
    if-nez p1, :cond_0

    .line 119
    const/4 v0, 0x0

    .line 129
    :goto_0
    return-object v0

    .line 122
    :cond_0
    iget-object v0, p0, Lcfu;->aUB:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfy;

    .line 123
    if-nez v0, :cond_1

    .line 124
    const/16 v0, 0x2e

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 125
    if-ltz v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 126
    iget-object v0, p0, Lcfu;->aUB:Ljava/util/LinkedHashMap;

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfy;

    .line 128
    :cond_1
    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    move v0, v1

    .line 125
    goto :goto_1
.end method
