.class final Lboa;
.super Lbnk;


# instance fields
.field private final aGA:Lbhh;

.field private synthetic aGz:Lbnv;


# direct methods
.method public constructor <init>(Lbnv;Lbhh;)V
    .locals 0

    iput-object p1, p0, Lboa;->aGz:Lbnv;

    invoke-direct {p0}, Lbnk;-><init>()V

    iput-object p2, p0, Lboa;->aGA:Lbhh;

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x3

    const-string v2, "PeopleService"

    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GaiaId callback: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nresolution="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nholder="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {p1, v0, p2}, Lbnv;->b(ILjava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    if-nez p3, :cond_1

    :goto_0
    iget-object v2, p0, Lboa;->aGz:Lbnv;

    new-instance v3, Lbny;

    iget-object v4, p0, Lboa;->aGz:Lbnv;

    iget-object v5, p0, Lboa;->aGA:Lbhh;

    invoke-direct {v3, v4, v5, v1, v0}, Lbny;-><init>(Lbnv;Lbhh;Lcom/google/android/gms/common/api/Status;Lbun;)V

    invoke-virtual {v2, v3}, Lbnv;->b(Lbit;)V

    return-void

    :cond_1
    new-instance v0, Lbun;

    invoke-direct {v0, p3}, Lbun;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method
