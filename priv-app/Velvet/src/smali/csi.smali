.class public final Lcsi;
.super Ljsl;
.source "PG"


# instance fields
.field private aeE:Ljava/lang/String;

.field private aez:I

.field private alT:Ljava/lang/String;

.field private bhK:Ljava/lang/String;

.field private bhL:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 110
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 111
    iput v1, p0, Lcsi;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lcsi;->aeE:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcsi;->alT:Ljava/lang/String;

    const-string v0, "Ok Google"

    iput-object v0, p0, Lcsi;->bhK:Ljava/lang/String;

    iput-boolean v1, p0, Lcsi;->bhL:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcsi;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lcsi;->eCz:I

    .line 112
    return-void
.end method


# virtual methods
.method public final SD()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcsi;->bhL:Z

    return v0
.end method

.method public final SE()Z
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcsi;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcsi;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcsi;->aeE:Ljava/lang/String;

    iget v0, p0, Lcsi;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcsi;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcsi;->alT:Ljava/lang/String;

    iget v0, p0, Lcsi;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcsi;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcsi;->bhK:Ljava/lang/String;

    iget v0, p0, Lcsi;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcsi;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lcsi;->bhL:Z

    iget v0, p0, Lcsi;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcsi;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 128
    iget v0, p0, Lcsi;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 129
    const/4 v0, 0x1

    iget-object v1, p0, Lcsi;->aeE:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 131
    :cond_0
    iget v0, p0, Lcsi;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 132
    const/4 v0, 0x2

    iget-object v1, p0, Lcsi;->alT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 134
    :cond_1
    iget v0, p0, Lcsi;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 135
    const/4 v0, 0x3

    iget-object v1, p0, Lcsi;->bhK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 137
    :cond_2
    iget v0, p0, Lcsi;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 138
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcsi;->bhL:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 140
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 141
    return-void
.end method

.method public final cG(Z)Lcsi;
    .locals 1

    .prologue
    .line 97
    iput-boolean p1, p0, Lcsi;->bhL:Z

    .line 98
    iget v0, p0, Lcsi;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcsi;->aez:I

    .line 99
    return-object p0
.end method

.method public final getLocale()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcsi;->aeE:Ljava/lang/String;

    return-object v0
.end method

.method public final getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcsi;->alT:Ljava/lang/String;

    return-object v0
.end method

.method public final getPrompt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcsi;->bhK:Ljava/lang/String;

    return-object v0
.end method

.method public final iM(Ljava/lang/String;)Lcsi;
    .locals 1

    .prologue
    .line 31
    if-nez p1, :cond_0

    .line 32
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 34
    :cond_0
    iput-object p1, p0, Lcsi;->aeE:Ljava/lang/String;

    .line 35
    iget v0, p0, Lcsi;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcsi;->aez:I

    .line 36
    return-object p0
.end method

.method public final iN(Ljava/lang/String;)Lcsi;
    .locals 1

    .prologue
    .line 53
    if-nez p1, :cond_0

    .line 54
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56
    :cond_0
    iput-object p1, p0, Lcsi;->alT:Ljava/lang/String;

    .line 57
    iget v0, p0, Lcsi;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcsi;->aez:I

    .line 58
    return-object p0
.end method

.method public final iO(Ljava/lang/String;)Lcsi;
    .locals 1

    .prologue
    .line 75
    if-nez p1, :cond_0

    .line 76
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 78
    :cond_0
    iput-object p1, p0, Lcsi;->bhK:Ljava/lang/String;

    .line 79
    iget v0, p0, Lcsi;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcsi;->aez:I

    .line 80
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 145
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 146
    iget v1, p0, Lcsi;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 147
    const/4 v1, 0x1

    iget-object v2, p0, Lcsi;->aeE:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 150
    :cond_0
    iget v1, p0, Lcsi;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 151
    const/4 v1, 0x2

    iget-object v2, p0, Lcsi;->alT:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 154
    :cond_1
    iget v1, p0, Lcsi;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 155
    const/4 v1, 0x3

    iget-object v2, p0, Lcsi;->bhK:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 158
    :cond_2
    iget v1, p0, Lcsi;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 159
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcsi;->bhL:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 162
    :cond_3
    return v0
.end method
