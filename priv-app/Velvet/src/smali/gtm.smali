.class public final Lgtm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final cUY:[Lgto;


# direct methods
.method public constructor <init>([Lgto;)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-static {p1}, Lgtm;->a([Lgto;)V

    .line 101
    iput-object p1, p0, Lgtm;->cUY:[Lgto;

    .line 102
    return-void
.end method

.method private static a([Lgto;)V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 111
    array-length v0, p0

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 115
    array-length v8, p0

    move v5, v2

    move v6, v2

    move v0, v3

    move v7, v3

    :goto_1
    if-ge v5, v8, :cond_4

    aget-object v9, p0, v5

    .line 116
    iget v4, v9, Lgto;->cVd:I

    if-eq v4, v7, :cond_0

    move v0, v3

    .line 119
    :cond_0
    iget v4, v9, Lgto;->cVd:I

    add-int/lit8 v10, v7, 0x1

    if-eq v4, v10, :cond_1

    iget v4, v9, Lgto;->cVd:I

    if-ne v4, v7, :cond_3

    iget v4, v9, Lgto;->cVe:I

    if-le v4, v0, :cond_3

    :cond_1
    move v4, v1

    :goto_2
    const-string v10, "expected numberOfAttempts=%s or minNumberOfItems>=%s for ResourceSet %s  (actual: numberOfAttempts=%s, minNumberOfItems=%s)"

    const/4 v11, 0x5

    new-array v11, v11, [Ljava/lang/Object;

    add-int/lit8 v7, v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v11, v2

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v11, v1

    const/4 v0, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v11, v0

    const/4 v0, 0x3

    iget v7, v9, Lgto;->cVd:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v11, v0

    const/4 v0, 0x4

    iget v7, v9, Lgto;->cVe:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v11, v0

    invoke-static {v4, v10, v11}, Lifv;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 126
    add-int/lit8 v4, v6, 0x1

    .line 127
    iget v7, v9, Lgto;->cVd:I

    .line 128
    iget v6, v9, Lgto;->cVe:I

    .line 115
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v0, v6

    move v6, v4

    goto :goto_1

    :cond_2
    move v0, v2

    .line 111
    goto :goto_0

    :cond_3
    move v4, v2

    .line 119
    goto :goto_2

    .line 130
    :cond_4
    return-void
.end method


# virtual methods
.method public final aKq()I
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, Lgtm;->cUY:[Lgto;

    iget-object v1, p0, Lgtm;->cUY:[Lgto;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iget v0, v0, Lgto;->cVd:I

    return v0
.end method

.method public final bd(II)Lgto;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 157
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 158
    if-ltz p2, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 160
    invoke-virtual {p0}, Lgtm;->aKq()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 162
    new-instance v4, Lgtn;

    invoke-direct {v4}, Lgtn;-><init>()V

    :goto_2
    iget v0, v4, Lgtn;->cUZ:I

    iget-object v5, p0, Lgtm;->cUY:[Lgto;

    array-length v5, v5

    if-ge v0, v5, :cond_2

    iget-object v0, p0, Lgtm;->cUY:[Lgto;

    iget v5, v4, Lgtn;->cUZ:I

    aget-object v0, v0, v5

    iget v0, v0, Lgto;->cVd:I

    if-eq v0, v3, :cond_2

    iget v0, v4, Lgtn;->cUZ:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v4, Lgtn;->cUZ:I

    goto :goto_2

    :cond_0
    move v0, v2

    .line 157
    goto :goto_0

    :cond_1
    move v0, v2

    .line 158
    goto :goto_1

    .line 162
    :cond_2
    iget v0, v4, Lgtn;->cUZ:I

    iput v0, v4, Lgtn;->abk:I

    :goto_3
    iget v0, v4, Lgtn;->abk:I

    iget-object v5, p0, Lgtm;->cUY:[Lgto;

    array-length v5, v5

    if-ge v0, v5, :cond_3

    iget-object v0, p0, Lgtm;->cUY:[Lgto;

    iget v5, v4, Lgtn;->abk:I

    aget-object v0, v0, v5

    iget v0, v0, Lgto;->cVd:I

    if-ne v0, v3, :cond_3

    iget v0, v4, Lgtn;->abk:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v4, Lgtn;->abk:I

    goto :goto_3

    .line 163
    :cond_3
    iget v0, v4, Lgtn;->abk:I

    add-int/lit8 v0, v0, -0x1

    :goto_4
    iget v5, v4, Lgtn;->cUZ:I

    if-lt v0, v5, :cond_5

    .line 164
    iget-object v5, p0, Lgtm;->cUY:[Lgto;

    aget-object v5, v5, v0

    .line 165
    iget v6, v5, Lgto;->cVe:I

    if-gt v6, p2, :cond_4

    return-object v5

    .line 163
    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_4

    .line 167
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v4, "number of items %s is less than the minimum number of items of all ResourceSets for attempt %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
