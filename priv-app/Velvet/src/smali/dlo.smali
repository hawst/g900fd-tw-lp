.class public final Ldlo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static final bBA:[B

.field private static final bBB:[B

.field private static final bBC:[B


# instance fields
.field private final bBD:Ldmr;

.field private bBE:Z

.field private bBF:I

.field private bBG:I

.field private bBH:I

.field private final bBI:Ljava/util/List;

.field private bBJ:Landroid/util/JsonToken;

.field private final bBK:Ldlq;

.field private bBL:Ldlq;

.field private bBM:Ldlq;

.field private buffer:[B

.field private final in:Ljava/io/InputStream;

.field private pos:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 143
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Ldlo;->bBA:[B

    .line 144
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    sput-object v0, Ldlo;->bBB:[B

    .line 145
    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_2

    sput-object v0, Ldlo;->bBC:[B

    return-void

    .line 143
    :array_0
    .array-data 1
        0x6et
        0x75t
        0x6ct
        0x6ct
    .end array-data

    .line 144
    :array_1
    .array-data 1
        0x74t
        0x72t
        0x75t
        0x65t
    .end array-data

    .line 145
    :array_2
    .array-data 1
        0x66t
        0x61t
        0x6ct
        0x73t
        0x65t
    .end array-data
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    new-instance v0, Ldmr;

    invoke-direct {v0}, Ldmr;-><init>()V

    iput-object v0, p0, Ldlo;->bBD:Ldmr;

    .line 159
    iput-boolean v1, p0, Ldlo;->bBE:Z

    .line 167
    const/16 v0, 0x2000

    new-array v0, v0, [B

    iput-object v0, p0, Ldlo;->buffer:[B

    .line 168
    iput v1, p0, Ldlo;->pos:I

    .line 169
    iput v1, p0, Ldlo;->bBF:I

    .line 170
    const/4 v0, 0x1

    iput v0, p0, Ldlo;->bBG:I

    .line 176
    const/4 v0, -0x1

    iput v0, p0, Ldlo;->bBH:I

    .line 178
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldlo;->bBI:Ljava/util/List;

    .line 180
    sget-object v0, Ldln;->bBw:Ldln;

    invoke-direct {p0, v0}, Ldlo;->a(Ldln;)V

    .line 194
    new-instance v0, Ldlq;

    invoke-direct {v0}, Ldlq;-><init>()V

    iput-object v0, p0, Ldlo;->bBK:Ldlq;

    .line 206
    if-nez p1, :cond_0

    .line 207
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "in == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :cond_0
    iput-object p1, p0, Ldlo;->in:Ljava/io/InputStream;

    .line 210
    return-void
.end method

.method private a(B)Ldlq;
    .locals 11

    .prologue
    const/16 v5, 0xa

    const/4 v10, 0x4

    const/4 v1, 0x0

    .line 890
    move v0, v1

    .line 894
    :cond_0
    iget v7, p0, Ldlo;->pos:I

    .line 895
    iget v2, p0, Ldlo;->pos:I

    add-int/2addr v0, v2

    iput v0, p0, Ldlo;->pos:I

    .line 896
    iget v0, p0, Ldlo;->pos:I

    .line 897
    const/4 v3, 0x1

    move v2, v0

    .line 902
    :cond_1
    :goto_0
    iget v4, p0, Ldlo;->pos:I

    iget v6, p0, Ldlo;->bBF:I

    if-ge v4, v6, :cond_b

    .line 903
    iget-object v4, p0, Ldlo;->buffer:[B

    iget v6, p0, Ldlo;->pos:I

    add-int/lit8 v8, v6, 0x1

    iput v8, p0, Ldlo;->pos:I

    aget-byte v4, v4, v6

    .line 904
    if-ne v4, p1, :cond_2

    .line 905
    iget v1, p0, Ldlo;->pos:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v2, v0, v1}, Ldlo;->q(III)I

    move-result v0

    .line 906
    iget-object v1, p0, Ldlo;->bBK:Ldlq;

    iget-object v2, p0, Ldlo;->buffer:[B

    sub-int/2addr v0, v7

    invoke-virtual {v1, v2, v7, v0}, Ldlq;->g([BII)V

    .line 907
    iget-object v0, p0, Ldlo;->bBK:Ldlq;

    return-object v0

    .line 908
    :cond_2
    const/16 v6, 0x5c

    if-ne v4, v6, :cond_c

    .line 909
    iget v4, p0, Ldlo;->pos:I

    add-int/lit8 v4, v4, -0x1

    invoke-direct {p0, v2, v0, v4}, Ldlo;->q(III)I

    move-result v4

    .line 910
    iget v0, p0, Ldlo;->pos:I

    add-int/lit8 v0, v0, 0x5

    iget v2, p0, Ldlo;->bBF:I

    if-le v0, v2, :cond_3

    iget v0, p0, Ldlo;->pos:I

    iget v2, p0, Ldlo;->bBF:I

    if-ge v0, v2, :cond_a

    iget-object v0, p0, Ldlo;->buffer:[B

    iget v2, p0, Ldlo;->pos:I

    aget-byte v0, v0, v2

    const/16 v2, 0x75

    if-eq v0, v2, :cond_a

    .line 912
    :cond_3
    :try_start_0
    iget-object v0, p0, Ldlo;->buffer:[B

    iget v2, p0, Ldlo;->pos:I

    add-int/lit8 v6, v2, 0x1

    iput v6, p0, Ldlo;->pos:I

    aget-byte v0, v0, v2

    sparse-switch v0, :sswitch_data_0

    :goto_1
    iget-object v6, p0, Ldlo;->buffer:[B

    add-int/lit8 v2, v4, 0x1

    aput-byte v0, v6, v4
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 919
    :goto_2
    iget v0, p0, Ldlo;->pos:I

    goto :goto_0

    :sswitch_0
    move v2, v1

    move v6, v1

    .line 912
    :goto_3
    if-eq v2, v10, :cond_7

    :try_start_1
    iget-object v0, p0, Ldlo;->buffer:[B

    iget v8, p0, Ldlo;->pos:I

    add-int/2addr v8, v2

    aget-byte v0, v0, v8

    const/16 v8, 0x30

    if-gt v8, v0, :cond_4

    const/16 v8, 0x39

    if-gt v0, v8, :cond_4

    add-int/lit8 v0, v0, -0x30

    :goto_4
    shl-int/lit8 v6, v6, 0x4

    add-int/2addr v6, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_4
    const/16 v8, 0x61

    if-gt v8, v0, :cond_5

    const/16 v8, 0x66

    if-gt v0, v8, :cond_5

    add-int/lit8 v0, v0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_4

    :cond_5
    const/16 v8, 0x41

    if-gt v8, v0, :cond_6

    const/16 v8, 0x46

    if-gt v0, v8, :cond_6

    add-int/lit8 v0, v0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_4

    :cond_6
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad unicode escape sequence: \\u"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Ldlo;->buffer:[B

    iget v5, p0, Ldlo;->pos:I

    const/4 v6, 0x4

    invoke-direct {v2, v3, v5, v6}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    .line 913
    :catch_0
    move-exception v0

    .line 915
    iget-object v1, p0, Ldlo;->bBK:Ldlq;

    iget-object v2, p0, Ldlo;->buffer:[B

    sub-int v3, v4, v7

    invoke-virtual {v1, v2, v7, v3}, Ldlq;->g([BII)V

    .line 916
    iget v1, p0, Ldlo;->pos:I

    add-int/lit8 v1, v1, -0x2

    iput v1, p0, Ldlo;->pos:I

    .line 917
    throw v0

    .line 912
    :cond_7
    :try_start_2
    iget v0, p0, Ldlo;->pos:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Ldlo;->pos:I

    const/16 v0, 0x80

    if-ge v6, v0, :cond_8

    iget-object v2, p0, Ldlo;->buffer:[B

    add-int/lit8 v0, v4, 0x1

    int-to-byte v6, v6

    aput-byte v6, v2, v4

    :goto_5
    move v2, v0

    goto :goto_2

    :cond_8
    const/16 v0, 0x800

    if-ge v6, v0, :cond_9

    iget-object v0, p0, Ldlo;->buffer:[B

    add-int/lit8 v2, v4, 0x1

    shr-int/lit8 v8, v6, 0x6

    or-int/lit16 v8, v8, 0xc0

    int-to-byte v8, v8

    aput-byte v8, v0, v4

    iget-object v8, p0, Ldlo;->buffer:[B

    add-int/lit8 v0, v2, 0x1

    and-int/lit8 v6, v6, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, v8, v2

    goto :goto_5

    :cond_9
    iget-object v0, p0, Ldlo;->buffer:[B

    add-int/lit8 v2, v4, 0x1

    shr-int/lit8 v8, v6, 0xc

    or-int/lit16 v8, v8, 0xe0

    int-to-byte v8, v8

    aput-byte v8, v0, v4

    iget-object v0, p0, Ldlo;->buffer:[B

    add-int/lit8 v8, v2, 0x1

    shr-int/lit8 v9, v6, 0x6

    and-int/lit8 v9, v9, 0x3f

    or-int/lit16 v9, v9, 0x80

    int-to-byte v9, v9

    aput-byte v9, v0, v2

    iget-object v2, p0, Ldlo;->buffer:[B

    add-int/lit8 v0, v8, 0x1

    and-int/lit8 v6, v6, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, v2, v8

    goto :goto_5

    :sswitch_1
    const/16 v0, 0x9

    goto/16 :goto_1

    :sswitch_2
    const/16 v0, 0x8

    goto/16 :goto_1

    :sswitch_3
    move v0, v5

    goto/16 :goto_1

    :sswitch_4
    const/16 v0, 0xd

    goto/16 :goto_1

    :sswitch_5
    const/16 v0, 0xc

    goto/16 :goto_1

    :sswitch_6
    const v0, 0x83e09b

    invoke-static {v0}, Lhwt;->lx(I)V

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Found \\x in JSON"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_7
    iget v2, p0, Ldlo;->bBG:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Ldlo;->bBG:I

    iget v2, p0, Ldlo;->pos:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Ldlo;->bBH:I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 921
    :cond_a
    iget v0, p0, Ldlo;->pos:I

    iget v2, p0, Ldlo;->bBF:I

    if-ge v0, v2, :cond_e

    .line 922
    iget v0, p0, Ldlo;->pos:I

    add-int/lit8 v0, v0, 0x5

    iget v2, p0, Ldlo;->bBF:I

    sub-int/2addr v0, v2

    .line 924
    :goto_6
    iget v2, p0, Ldlo;->pos:I

    add-int/lit8 v2, v2, -0x1

    move v3, v0

    move v0, v2

    move v2, v4

    .line 933
    :cond_b
    iget v4, p0, Ldlo;->bBF:I

    invoke-direct {p0, v2, v0, v4}, Ldlo;->q(III)I

    move-result v0

    iput v0, p0, Ldlo;->bBF:I

    .line 935
    iput v7, p0, Ldlo;->pos:I

    .line 936
    iget v0, p0, Ldlo;->pos:I

    sub-int v0, v2, v0

    .line 937
    iget v2, p0, Ldlo;->bBF:I

    iget v4, p0, Ldlo;->pos:I

    sub-int/2addr v2, v4

    add-int/2addr v2, v3

    invoke-direct {p0, v2}, Ldlo;->fZ(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 939
    iget v0, p0, Ldlo;->pos:I

    iget v1, p0, Ldlo;->bBF:I

    if-ne v0, v1, :cond_d

    const-string v0, "Unterminated string"

    :goto_7
    invoke-direct {p0, v0}, Ldlo;->jR(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 927
    :cond_c
    if-ne v4, v5, :cond_1

    .line 928
    iget v4, p0, Ldlo;->bBG:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Ldlo;->bBG:I

    .line 929
    iget v4, p0, Ldlo;->pos:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Ldlo;->bBH:I

    goto/16 :goto_0

    .line 939
    :cond_d
    const-string v0, "Unterminated escape sequence"

    goto :goto_7

    :cond_e
    move v0, v3

    goto :goto_6

    .line 912
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_7
        0x62 -> :sswitch_2
        0x66 -> :sswitch_5
        0x6e -> :sswitch_3
        0x72 -> :sswitch_4
        0x74 -> :sswitch_1
        0x75 -> :sswitch_0
        0x78 -> :sswitch_6
    .end sparse-switch
.end method

.method private a(Landroid/util/JsonToken;)V
    .locals 3

    .prologue
    .line 287
    invoke-virtual {p0}, Ldlo;->peek()Landroid/util/JsonToken;

    .line 288
    iget-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    if-eq v0, p1, :cond_0

    .line 289
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ldlo;->peek()Landroid/util/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 291
    :cond_0
    invoke-direct {p0}, Ldlo;->acU()Landroid/util/JsonToken;

    .line 292
    return-void
.end method

.method private a(Ldln;)V
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Ldlo;->bBI:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 600
    return-void
.end method

.method private acU()Landroid/util/JsonToken;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 350
    invoke-virtual {p0}, Ldlo;->peek()Landroid/util/JsonToken;

    .line 352
    iget-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    .line 353
    iput-object v1, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    .line 354
    iput-object v1, p0, Ldlo;->bBM:Ldlq;

    .line 355
    iput-object v1, p0, Ldlo;->bBL:Ldlq;

    .line 356
    return-object v0
.end method

.method private acW()Ldlq;
    .locals 3

    .prologue
    .line 405
    invoke-virtual {p0}, Ldlo;->peek()Landroid/util/JsonToken;

    .line 406
    iget-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    sget-object v1, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    sget-object v1, Landroid/util/JsonToken;->NUMBER:Landroid/util/JsonToken;

    if-eq v0, v1, :cond_0

    .line 407
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected a string but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ldlo;->peek()Landroid/util/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 410
    :cond_0
    iget-object v0, p0, Ldlo;->bBM:Ldlq;

    iget-object v1, p0, Ldlo;->bBK:Ldlq;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 411
    invoke-direct {p0}, Ldlo;->acU()Landroid/util/JsonToken;

    .line 412
    iget-object v0, p0, Ldlo;->bBK:Ldlq;

    return-object v0

    .line 410
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private acY()Ldln;
    .locals 2

    .prologue
    .line 595
    iget-object v0, p0, Ldlo;->bBI:Ljava/util/List;

    iget-object v1, p0, Ldlo;->bBI:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldln;

    return-object v0
.end method

.method private acZ()Landroid/util/JsonToken;
    .locals 11

    .prologue
    const/16 v9, 0x65

    const/16 v5, 0x4c

    const/16 v8, 0x45

    const/16 v7, 0x39

    const/16 v6, 0x30

    .line 719
    invoke-direct {p0}, Ldlo;->ada()I

    move-result v0

    .line 720
    sparse-switch v0, :sswitch_data_0

    .line 736
    iget v0, p0, Ldlo;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Ldlo;->pos:I

    .line 737
    invoke-direct {p0}, Ldlo;->add()Ldlq;

    move-result-object v0

    iput-object v0, p0, Ldlo;->bBM:Ldlq;

    iget-object v0, p0, Ldlo;->bBM:Ldlq;

    iget v0, v0, Ldlq;->mLength:I

    if-nez v0, :cond_0

    const-string v0, "Expected literal value"

    invoke-direct {p0, v0}, Ldlo;->jR(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 722
    :sswitch_0
    sget-object v0, Ldln;->bBt:Ldln;

    invoke-direct {p0, v0}, Ldlo;->a(Ldln;)V

    .line 723
    sget-object v0, Landroid/util/JsonToken;->BEGIN_OBJECT:Landroid/util/JsonToken;

    iput-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    .line 737
    :goto_0
    return-object v0

    .line 726
    :sswitch_1
    sget-object v0, Ldln;->bBr:Ldln;

    invoke-direct {p0, v0}, Ldlo;->a(Ldln;)V

    .line 727
    sget-object v0, Landroid/util/JsonToken;->BEGIN_ARRAY:Landroid/util/JsonToken;

    iput-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    goto :goto_0

    .line 730
    :sswitch_2
    invoke-direct {p0}, Ldlo;->adb()V

    .line 732
    :sswitch_3
    int-to-byte v0, v0

    invoke-direct {p0, v0}, Ldlo;->a(B)Ldlq;

    move-result-object v0

    iput-object v0, p0, Ldlo;->bBM:Ldlq;

    .line 733
    sget-object v0, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    iput-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    goto :goto_0

    .line 737
    :cond_0
    iget-object v0, p0, Ldlo;->bBM:Ldlq;

    iget-object v1, p0, Ldlo;->bBK:Ldlq;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Ldlo;->bBM:Ldlq;

    iget-object v0, v0, Ldlq;->rQ:[B

    iget-object v1, p0, Ldlo;->buffer:[B

    if-ne v0, v1, :cond_6

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, p0, Ldlo;->bBM:Ldlq;

    iget v3, v0, Ldlq;->mLength:I

    iget-object v0, p0, Ldlo;->bBM:Ldlq;

    iget v2, v0, Ldlq;->ff:I

    const/4 v0, 0x4

    if-ne v3, v0, :cond_7

    const/16 v0, 0x6e

    iget-object v1, p0, Ldlo;->buffer:[B

    aget-byte v1, v1, v2

    if-eq v0, v1, :cond_1

    const/16 v0, 0x4e

    iget-object v1, p0, Ldlo;->buffer:[B

    aget-byte v1, v1, v2

    if-ne v0, v1, :cond_7

    :cond_1
    const/16 v0, 0x75

    iget-object v1, p0, Ldlo;->buffer:[B

    add-int/lit8 v4, v2, 0x1

    aget-byte v1, v1, v4

    if-eq v0, v1, :cond_2

    const/16 v0, 0x55

    iget-object v1, p0, Ldlo;->buffer:[B

    add-int/lit8 v4, v2, 0x1

    aget-byte v1, v1, v4

    if-ne v0, v1, :cond_7

    :cond_2
    const/16 v0, 0x6c

    iget-object v1, p0, Ldlo;->buffer:[B

    add-int/lit8 v4, v2, 0x2

    aget-byte v1, v1, v4

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Ldlo;->buffer:[B

    add-int/lit8 v1, v2, 0x2

    aget-byte v0, v0, v1

    if-ne v5, v0, :cond_7

    :cond_3
    const/16 v0, 0x6c

    iget-object v1, p0, Ldlo;->buffer:[B

    add-int/lit8 v4, v2, 0x3

    aget-byte v1, v1, v4

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Ldlo;->buffer:[B

    add-int/lit8 v1, v2, 0x3

    aget-byte v0, v0, v1

    if-ne v5, v0, :cond_7

    :cond_4
    iget-object v0, p0, Ldlo;->bBM:Ldlq;

    sget-object v1, Ldlo;->bBA:[B

    invoke-virtual {v0, v1}, Ldlq;->G([B)V

    sget-object v0, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    :goto_2
    iput-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    iget-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    sget-object v1, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    if-ne v0, v1, :cond_5

    invoke-direct {p0}, Ldlo;->adb()V

    :cond_5
    iget-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    const/4 v0, 0x4

    if-ne v3, v0, :cond_c

    const/16 v0, 0x74

    iget-object v1, p0, Ldlo;->buffer:[B

    aget-byte v1, v1, v2

    if-eq v0, v1, :cond_8

    const/16 v0, 0x54

    iget-object v1, p0, Ldlo;->buffer:[B

    aget-byte v1, v1, v2

    if-ne v0, v1, :cond_c

    :cond_8
    const/16 v0, 0x72

    iget-object v1, p0, Ldlo;->buffer:[B

    add-int/lit8 v4, v2, 0x1

    aget-byte v1, v1, v4

    if-eq v0, v1, :cond_9

    const/16 v0, 0x52

    iget-object v1, p0, Ldlo;->buffer:[B

    add-int/lit8 v4, v2, 0x1

    aget-byte v1, v1, v4

    if-ne v0, v1, :cond_c

    :cond_9
    const/16 v0, 0x75

    iget-object v1, p0, Ldlo;->buffer:[B

    add-int/lit8 v4, v2, 0x2

    aget-byte v1, v1, v4

    if-eq v0, v1, :cond_a

    const/16 v0, 0x55

    iget-object v1, p0, Ldlo;->buffer:[B

    add-int/lit8 v4, v2, 0x2

    aget-byte v1, v1, v4

    if-ne v0, v1, :cond_c

    :cond_a
    iget-object v0, p0, Ldlo;->buffer:[B

    add-int/lit8 v1, v2, 0x3

    aget-byte v0, v0, v1

    if-eq v9, v0, :cond_b

    iget-object v0, p0, Ldlo;->buffer:[B

    add-int/lit8 v1, v2, 0x3

    aget-byte v0, v0, v1

    if-ne v8, v0, :cond_c

    :cond_b
    iget-object v0, p0, Ldlo;->bBM:Ldlq;

    sget-object v1, Ldlo;->bBB:[B

    invoke-virtual {v0, v1}, Ldlq;->G([B)V

    sget-object v0, Landroid/util/JsonToken;->BOOLEAN:Landroid/util/JsonToken;

    goto :goto_2

    :cond_c
    const/4 v0, 0x5

    if-ne v3, v0, :cond_12

    const/16 v0, 0x66

    iget-object v1, p0, Ldlo;->buffer:[B

    aget-byte v1, v1, v2

    if-eq v0, v1, :cond_d

    const/16 v0, 0x46

    iget-object v1, p0, Ldlo;->buffer:[B

    aget-byte v1, v1, v2

    if-ne v0, v1, :cond_12

    :cond_d
    const/16 v0, 0x61

    iget-object v1, p0, Ldlo;->buffer:[B

    add-int/lit8 v4, v2, 0x1

    aget-byte v1, v1, v4

    if-eq v0, v1, :cond_e

    const/16 v0, 0x41

    iget-object v1, p0, Ldlo;->buffer:[B

    add-int/lit8 v4, v2, 0x1

    aget-byte v1, v1, v4

    if-ne v0, v1, :cond_12

    :cond_e
    const/16 v0, 0x6c

    iget-object v1, p0, Ldlo;->buffer:[B

    add-int/lit8 v4, v2, 0x2

    aget-byte v1, v1, v4

    if-eq v0, v1, :cond_f

    iget-object v0, p0, Ldlo;->buffer:[B

    add-int/lit8 v1, v2, 0x2

    aget-byte v0, v0, v1

    if-ne v5, v0, :cond_12

    :cond_f
    const/16 v0, 0x73

    iget-object v1, p0, Ldlo;->buffer:[B

    add-int/lit8 v4, v2, 0x3

    aget-byte v1, v1, v4

    if-eq v0, v1, :cond_10

    const/16 v0, 0x53

    iget-object v1, p0, Ldlo;->buffer:[B

    add-int/lit8 v4, v2, 0x3

    aget-byte v1, v1, v4

    if-ne v0, v1, :cond_12

    :cond_10
    iget-object v0, p0, Ldlo;->buffer:[B

    add-int/lit8 v1, v2, 0x4

    aget-byte v0, v0, v1

    if-eq v9, v0, :cond_11

    iget-object v0, p0, Ldlo;->buffer:[B

    add-int/lit8 v1, v2, 0x4

    aget-byte v0, v0, v1

    if-ne v8, v0, :cond_12

    :cond_11
    iget-object v0, p0, Ldlo;->bBM:Ldlq;

    sget-object v1, Ldlo;->bBC:[B

    invoke-virtual {v0, v1}, Ldlq;->G([B)V

    sget-object v0, Landroid/util/JsonToken;->BOOLEAN:Landroid/util/JsonToken;

    goto/16 :goto_2

    :cond_12
    iget-object v0, p0, Ldlo;->bBM:Ldlq;

    iget-object v1, p0, Ldlo;->buffer:[B

    invoke-virtual {v0, v1, v2, v3}, Ldlq;->g([BII)V

    iget-object v4, p0, Ldlo;->buffer:[B

    aget-byte v0, v4, v2

    const/16 v1, 0x2d

    if-ne v0, v1, :cond_1d

    add-int/lit8 v1, v2, 0x1

    aget-byte v0, v4, v1

    :goto_3
    if-ne v0, v6, :cond_14

    add-int/lit8 v1, v1, 0x1

    aget-byte v0, v4, v1

    :cond_13
    const/16 v5, 0x2e

    if-ne v0, v5, :cond_16

    add-int/lit8 v1, v1, 0x1

    aget-byte v0, v4, v1

    :goto_4
    if-lt v0, v6, :cond_16

    if-gt v0, v7, :cond_16

    add-int/lit8 v1, v1, 0x1

    aget-byte v0, v4, v1

    goto :goto_4

    :cond_14
    const/16 v5, 0x31

    if-lt v0, v5, :cond_15

    if-gt v0, v7, :cond_15

    add-int/lit8 v1, v1, 0x1

    aget-byte v0, v4, v1

    :goto_5
    if-lt v0, v6, :cond_13

    if-gt v0, v7, :cond_13

    add-int/lit8 v1, v1, 0x1

    aget-byte v0, v4, v1

    goto :goto_5

    :cond_15
    sget-object v0, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    goto/16 :goto_2

    :cond_16
    move v10, v0

    move v0, v1

    move v1, v10

    if-eq v1, v9, :cond_17

    if-ne v1, v8, :cond_1b

    :cond_17
    add-int/lit8 v1, v0, 0x1

    aget-byte v0, v4, v1

    const/16 v5, 0x2b

    if-eq v0, v5, :cond_18

    const/16 v5, 0x2d

    if-ne v0, v5, :cond_19

    :cond_18
    add-int/lit8 v1, v1, 0x1

    aget-byte v0, v4, v1

    :cond_19
    if-lt v0, v6, :cond_1a

    if-gt v0, v7, :cond_1a

    add-int/lit8 v1, v1, 0x1

    aget-byte v0, v4, v1

    move v10, v0

    move v0, v1

    move v1, v10

    :goto_6
    if-lt v1, v6, :cond_1b

    if-gt v1, v7, :cond_1b

    add-int/lit8 v1, v0, 0x1

    aget-byte v0, v4, v1

    move v10, v0

    move v0, v1

    move v1, v10

    goto :goto_6

    :cond_1a
    sget-object v0, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    goto/16 :goto_2

    :cond_1b
    add-int v1, v2, v3

    if-ne v0, v1, :cond_1c

    sget-object v0, Landroid/util/JsonToken;->NUMBER:Landroid/util/JsonToken;

    goto/16 :goto_2

    :cond_1c
    sget-object v0, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    goto/16 :goto_2

    :cond_1d
    move v1, v2

    goto :goto_3

    .line 720
    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_3
        0x27 -> :sswitch_2
        0x5b -> :sswitch_1
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method private ada()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 774
    :goto_0
    :sswitch_0
    iget v0, p0, Ldlo;->pos:I

    iget v2, p0, Ldlo;->bBF:I

    if-lt v0, v2, :cond_0

    invoke-direct {p0, v1}, Ldlo;->fZ(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 775
    :cond_0
    iget-object v0, p0, Ldlo;->buffer:[B

    iget v2, p0, Ldlo;->pos:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Ldlo;->pos:I

    aget-byte v0, v0, v2

    .line 776
    sparse-switch v0, :sswitch_data_0

    .line 825
    :cond_1
    :goto_1
    return v0

    .line 778
    :sswitch_1
    iget v0, p0, Ldlo;->bBG:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldlo;->bBG:I

    .line 779
    iget v0, p0, Ldlo;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Ldlo;->bBH:I

    goto :goto_0

    .line 788
    :sswitch_2
    iget v2, p0, Ldlo;->pos:I

    iget v3, p0, Ldlo;->bBF:I

    if-ne v2, v3, :cond_2

    invoke-direct {p0, v1}, Ldlo;->fZ(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 792
    :cond_2
    invoke-direct {p0}, Ldlo;->adb()V

    .line 793
    iget-object v2, p0, Ldlo;->buffer:[B

    iget v3, p0, Ldlo;->pos:I

    aget-byte v2, v2, v3

    .line 794
    sparse-switch v2, :sswitch_data_1

    goto :goto_1

    .line 797
    :sswitch_3
    iget v0, p0, Ldlo;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldlo;->pos:I

    .line 798
    const-string v0, "*/"

    :goto_2
    iget v2, p0, Ldlo;->pos:I

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    iget v3, p0, Ldlo;->bBF:I

    if-le v2, v3, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-direct {p0, v2}, Ldlo;->fZ(I)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_3
    invoke-direct {p0, v0}, Ldlo;->jQ(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    :goto_3
    if-nez v0, :cond_6

    .line 799
    const-string v0, "Unterminated comment"

    invoke-direct {p0, v0}, Ldlo;->jR(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 798
    :cond_4
    iget v2, p0, Ldlo;->pos:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Ldlo;->pos:I

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    .line 801
    :cond_6
    iget v0, p0, Ldlo;->pos:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Ldlo;->pos:I

    goto :goto_0

    .line 806
    :sswitch_4
    iget v0, p0, Ldlo;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldlo;->pos:I

    .line 807
    invoke-direct {p0}, Ldlo;->adc()V

    goto/16 :goto_0

    .line 820
    :sswitch_5
    invoke-direct {p0}, Ldlo;->adb()V

    .line 821
    invoke-direct {p0}, Ldlo;->adc()V

    goto/16 :goto_0

    .line 829
    :cond_7
    new-instance v0, Ljava/io/EOFException;

    const-string v1, "End of input"

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 776
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xa -> :sswitch_1
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x23 -> :sswitch_5
        0x2f -> :sswitch_2
    .end sparse-switch

    .line 794
    :sswitch_data_1
    .sparse-switch
        0x2a -> :sswitch_3
        0x2f -> :sswitch_4
    .end sparse-switch
.end method

.method private adb()V
    .locals 1

    .prologue
    .line 833
    iget-boolean v0, p0, Ldlo;->bBE:Z

    if-nez v0, :cond_0

    .line 834
    const-string v0, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v0}, Ldlo;->jR(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 836
    :cond_0
    return-void
.end method

.method private adc()V
    .locals 3

    .prologue
    .line 844
    :cond_0
    iget v0, p0, Ldlo;->pos:I

    iget v1, p0, Ldlo;->bBF:I

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ldlo;->fZ(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 845
    :cond_1
    iget-object v0, p0, Ldlo;->buffer:[B

    iget v1, p0, Ldlo;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Ldlo;->pos:I

    aget-byte v0, v0, v1

    .line 846
    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    .line 847
    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 850
    iget v0, p0, Ldlo;->bBG:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldlo;->bBG:I

    .line 851
    iget v0, p0, Ldlo;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Ldlo;->bBH:I

    .line 855
    :cond_2
    return-void
.end method

.method private add()Ldlq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 955
    move v0, v1

    .line 959
    :cond_0
    :goto_0
    iget v2, p0, Ldlo;->pos:I

    add-int/2addr v2, v0

    iget v3, p0, Ldlo;->bBF:I

    if-ge v2, v3, :cond_1

    .line 960
    iget-object v2, p0, Ldlo;->buffer:[B

    iget v3, p0, Ldlo;->pos:I

    add-int/2addr v3, v0

    aget-byte v2, v2, v3

    sparse-switch v2, :sswitch_data_0

    .line 959
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 966
    :sswitch_0
    invoke-direct {p0}, Ldlo;->adb()V

    .line 994
    :goto_1
    :sswitch_1
    iget-object v1, p0, Ldlo;->bBK:Ldlq;

    iget-object v2, p0, Ldlo;->buffer:[B

    iget v3, p0, Ldlo;->pos:I

    invoke-virtual {v1, v2, v3, v0}, Ldlq;->g([BII)V

    .line 999
    iget v1, p0, Ldlo;->pos:I

    add-int/2addr v0, v1

    iput v0, p0, Ldlo;->pos:I

    .line 1000
    iget-object v0, p0, Ldlo;->bBK:Ldlq;

    return-object v0

    .line 979
    :sswitch_2
    iget v1, p0, Ldlo;->bBG:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Ldlo;->bBG:I

    .line 980
    iget v1, p0, Ldlo;->pos:I

    add-int/2addr v1, v0

    iput v1, p0, Ldlo;->bBH:I

    goto :goto_1

    .line 990
    :cond_1
    add-int/lit8 v2, v0, 0x1

    invoke-direct {p0, v2}, Ldlo;->fZ(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 991
    iget-object v2, p0, Ldlo;->buffer:[B

    iget v3, p0, Ldlo;->bBF:I

    aput-byte v1, v2, v3

    goto :goto_1

    .line 960
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_1
        0xa -> :sswitch_2
        0xc -> :sswitch_1
        0xd -> :sswitch_1
        0x20 -> :sswitch_1
        0x23 -> :sswitch_0
        0x2c -> :sswitch_1
        0x2f -> :sswitch_0
        0x3a -> :sswitch_1
        0x3b -> :sswitch_0
        0x3d -> :sswitch_0
        0x5b -> :sswitch_1
        0x5c -> :sswitch_0
        0x5d -> :sswitch_1
        0x7b -> :sswitch_1
        0x7d -> :sswitch_1
    .end sparse-switch
.end method

.method private b(Ldln;)V
    .locals 2

    .prologue
    .line 606
    iget-object v0, p0, Ldlo;->bBI:Ljava/util/List;

    iget-object v1, p0, Ldlo;->bBI:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 607
    return-void
.end method

.method private dA(Z)Landroid/util/JsonToken;
    .locals 2

    .prologue
    .line 610
    if-eqz p1, :cond_0

    .line 611
    sget-object v0, Ldln;->bBs:Ldln;

    invoke-direct {p0, v0}, Ldlo;->b(Ldln;)V

    .line 627
    :goto_0
    :sswitch_0
    invoke-direct {p0}, Ldlo;->ada()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 642
    iget v0, p0, Ldlo;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Ldlo;->pos:I

    .line 643
    invoke-direct {p0}, Ldlo;->acZ()Landroid/util/JsonToken;

    move-result-object v0

    :goto_1
    return-object v0

    .line 614
    :cond_0
    invoke-direct {p0}, Ldlo;->ada()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 623
    const-string v0, "Unterminated array"

    invoke-direct {p0, v0}, Ldlo;->jR(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 616
    :sswitch_1
    invoke-direct {p0}, Ldlo;->acY()Ldln;

    .line 617
    sget-object v0, Landroid/util/JsonToken;->END_ARRAY:Landroid/util/JsonToken;

    iput-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    goto :goto_1

    .line 619
    :sswitch_2
    invoke-direct {p0}, Ldlo;->adb()V

    goto :goto_0

    .line 629
    :sswitch_3
    if-eqz p1, :cond_1

    .line 630
    invoke-direct {p0}, Ldlo;->acY()Ldln;

    .line 631
    sget-object v0, Landroid/util/JsonToken;->END_ARRAY:Landroid/util/JsonToken;

    iput-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    goto :goto_1

    .line 637
    :cond_1
    :sswitch_4
    invoke-direct {p0}, Ldlo;->adb()V

    .line 638
    iget v0, p0, Ldlo;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Ldlo;->pos:I

    .line 639
    iget-object v0, p0, Ldlo;->bBM:Ldlq;

    sget-object v1, Ldlo;->bBA:[B

    invoke-virtual {v0, v1}, Ldlq;->G([B)V

    .line 640
    sget-object v0, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    iput-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    goto :goto_1

    .line 627
    :sswitch_data_0
    .sparse-switch
        0x2c -> :sswitch_4
        0x3b -> :sswitch_4
        0x5d -> :sswitch_3
    .end sparse-switch

    .line 614
    :sswitch_data_1
    .sparse-switch
        0x2c -> :sswitch_0
        0x3b -> :sswitch_2
        0x5d -> :sswitch_1
    .end sparse-switch
.end method

.method private dB(Z)Landroid/util/JsonToken;
    .locals 1

    .prologue
    .line 653
    if-eqz p1, :cond_0

    .line 655
    invoke-direct {p0}, Ldlo;->ada()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 660
    iget v0, p0, Ldlo;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Ldlo;->pos:I

    .line 676
    :sswitch_0
    invoke-direct {p0}, Ldlo;->ada()I

    move-result v0

    .line 677
    sparse-switch v0, :sswitch_data_0

    .line 684
    invoke-direct {p0}, Ldlo;->adb()V

    .line 685
    iget v0, p0, Ldlo;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Ldlo;->pos:I

    .line 686
    invoke-direct {p0}, Ldlo;->add()Ldlq;

    move-result-object v0

    iput-object v0, p0, Ldlo;->bBL:Ldlq;

    .line 687
    iget-object v0, p0, Ldlo;->bBL:Ldlq;

    iget v0, v0, Ldlq;->mLength:I

    if-nez v0, :cond_1

    .line 688
    const-string v0, "Expected name"

    invoke-direct {p0, v0}, Ldlo;->jR(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 657
    :pswitch_0
    invoke-direct {p0}, Ldlo;->acY()Ldln;

    .line 658
    sget-object v0, Landroid/util/JsonToken;->END_OBJECT:Landroid/util/JsonToken;

    iput-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    .line 693
    :goto_0
    return-object v0

    .line 663
    :cond_0
    invoke-direct {p0}, Ldlo;->ada()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    .line 671
    const-string v0, "Unterminated object"

    invoke-direct {p0, v0}, Ldlo;->jR(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 665
    :sswitch_1
    invoke-direct {p0}, Ldlo;->acY()Ldln;

    .line 666
    sget-object v0, Landroid/util/JsonToken;->END_OBJECT:Landroid/util/JsonToken;

    iput-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    goto :goto_0

    .line 679
    :sswitch_2
    invoke-direct {p0}, Ldlo;->adb()V

    .line 681
    :sswitch_3
    int-to-byte v0, v0

    invoke-direct {p0, v0}, Ldlo;->a(B)Ldlq;

    move-result-object v0

    iput-object v0, p0, Ldlo;->bBL:Ldlq;

    .line 692
    :cond_1
    sget-object v0, Ldln;->bBu:Ldln;

    invoke-direct {p0, v0}, Ldlo;->b(Ldln;)V

    .line 693
    sget-object v0, Landroid/util/JsonToken;->NAME:Landroid/util/JsonToken;

    iput-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    goto :goto_0

    .line 655
    :pswitch_data_0
    .packed-switch 0x7d
        :pswitch_0
    .end packed-switch

    .line 677
    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_3
        0x27 -> :sswitch_2
    .end sparse-switch

    .line 663
    :sswitch_data_1
    .sparse-switch
        0x2c -> :sswitch_0
        0x3b -> :sswitch_0
        0x7d -> :sswitch_1
    .end sparse-switch
.end method

.method private fZ(I)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 747
    iget v1, p0, Ldlo;->bBF:I

    iget v2, p0, Ldlo;->pos:I

    sub-int/2addr v1, v2

    iput v1, p0, Ldlo;->bBF:I

    .line 748
    iget v1, p0, Ldlo;->bBH:I

    iget v2, p0, Ldlo;->pos:I

    sub-int/2addr v1, v2

    iput v1, p0, Ldlo;->bBH:I

    .line 749
    iget-object v1, p0, Ldlo;->buffer:[B

    array-length v1, v1

    if-le p1, v1, :cond_3

    .line 750
    iget-object v1, p0, Ldlo;->buffer:[B

    .line 751
    array-length v2, v1

    mul-int/lit8 v2, v2, 0x2

    invoke-static {p1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    new-array v2, v2, [B

    iput-object v2, p0, Ldlo;->buffer:[B

    .line 752
    iget v2, p0, Ldlo;->pos:I

    iget-object v3, p0, Ldlo;->buffer:[B

    iget v4, p0, Ldlo;->bBF:I

    invoke-static {v1, v2, v3, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 757
    :cond_0
    :goto_0
    iput v0, p0, Ldlo;->pos:I

    .line 759
    :cond_1
    iget-object v1, p0, Ldlo;->in:Ljava/io/InputStream;

    iget-object v2, p0, Ldlo;->buffer:[B

    iget v3, p0, Ldlo;->bBF:I

    iget-object v4, p0, Ldlo;->buffer:[B

    array-length v4, v4

    iget v5, p0, Ldlo;->bBF:I

    sub-int/2addr v4, v5

    const/16 v5, 0x2000

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 760
    iget v2, p0, Ldlo;->bBF:I

    add-int/2addr v1, v2

    iput v1, p0, Ldlo;->bBF:I

    .line 765
    iget v1, p0, Ldlo;->bBF:I

    if-lt v1, p1, :cond_1

    .line 766
    const/4 v0, 0x1

    .line 770
    :cond_2
    return v0

    .line 753
    :cond_3
    iget v1, p0, Ldlo;->bBF:I

    if-eqz v1, :cond_0

    iget v1, p0, Ldlo;->pos:I

    if-eqz v1, :cond_0

    .line 754
    iget-object v1, p0, Ldlo;->buffer:[B

    iget v2, p0, Ldlo;->pos:I

    iget-object v3, p0, Ldlo;->buffer:[B

    iget v4, p0, Ldlo;->bBF:I

    invoke-static {v1, v2, v3, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method private jQ(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 859
    move v0, v1

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 860
    iget-object v2, p0, Ldlo;->buffer:[B

    iget v3, p0, Ldlo;->pos:I

    add-int/2addr v3, v0

    aget-byte v2, v2, v3

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v2, v3, :cond_0

    .line 864
    :goto_1
    return v1

    .line 859
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 864
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private jR(Ljava/lang/String;)Ljava/io/IOException;
    .locals 4

    .prologue
    .line 1210
    new-instance v0, Landroid/util/MalformedJsonException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ldlo;->bBG:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ldlo;->pos:I

    iget v3, p0, Ldlo;->bBH:I

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/util/MalformedJsonException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private q(III)I
    .locals 3

    .prologue
    .line 943
    sub-int v0, p3, p2

    .line 944
    if-eqz v0, :cond_0

    if-eq p2, p1, :cond_0

    .line 945
    iget-object v1, p0, Ldlo;->buffer:[B

    iget-object v2, p0, Ldlo;->buffer:[B

    invoke-static {v1, p2, v2, p1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 947
    :cond_0
    add-int/2addr v0, p1

    return v0
.end method


# virtual methods
.method public final acV()Ldlq;
    .locals 3

    .prologue
    .line 370
    invoke-virtual {p0}, Ldlo;->peek()Landroid/util/JsonToken;

    .line 371
    iget-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    sget-object v1, Landroid/util/JsonToken;->NAME:Landroid/util/JsonToken;

    if-eq v0, v1, :cond_0

    .line 372
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected a name but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ldlo;->peek()Landroid/util/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 375
    :cond_0
    iget-object v0, p0, Ldlo;->bBL:Ldlq;

    iget-object v1, p0, Ldlo;->bBK:Ldlq;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 376
    invoke-direct {p0}, Ldlo;->acU()Landroid/util/JsonToken;

    .line 377
    iget-object v0, p0, Ldlo;->bBK:Ldlq;

    return-object v0

    .line 375
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final acX()[B
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 437
    invoke-direct {p0}, Ldlo;->acW()Ldlq;

    move-result-object v0

    .line 438
    iget-object v4, p0, Ldlo;->bBD:Ldmr;

    iget-object v5, v0, Ldlq;->rQ:[B

    iget v6, v0, Ldlq;->ff:I

    iget v7, v0, Ldlq;->mLength:I

    const/16 v0, 0x200

    if-le v7, v0, :cond_0

    invoke-static {v5, v6, v7}, Ldmr;->j([BII)[B

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v4, v5, v6, v7}, Ldmr;->h([BII)I

    move-result v8

    iget-object v0, v4, Ldmr;->bCz:[Ljava/lang/Object;

    aget-object v1, v0, v8

    instance-of v0, v1, [B

    if-eqz v0, :cond_4

    move-object v0, v1

    check-cast v0, [B

    check-cast v0, [B

    array-length v3, v0

    if-eq v3, v7, :cond_1

    move v0, v2

    :goto_1
    if-eqz v0, :cond_4

    check-cast v1, [B

    check-cast v1, [B

    goto :goto_0

    :cond_1
    move v3, v2

    :goto_2
    if-ge v3, v7, :cond_3

    add-int v9, v6, v3

    aget-byte v9, v5, v9

    aget-byte v10, v0, v3

    if-eq v9, v10, :cond_2

    move v0, v2

    goto :goto_1

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    invoke-static {v5, v6, v7}, Ldmr;->j([BII)[B

    move-result-object v1

    iget-object v0, v4, Ldmr;->bCz:[Ljava/lang/Object;

    aput-object v1, v0, v8

    goto :goto_0
.end method

.method public final beginArray()V
    .locals 1

    .prologue
    .line 256
    sget-object v0, Landroid/util/JsonToken;->BEGIN_ARRAY:Landroid/util/JsonToken;

    invoke-direct {p0, v0}, Ldlo;->a(Landroid/util/JsonToken;)V

    .line 257
    return-void
.end method

.method public final beginObject()V
    .locals 1

    .prologue
    .line 272
    sget-object v0, Landroid/util/JsonToken;->BEGIN_OBJECT:Landroid/util/JsonToken;

    invoke-direct {p0, v0}, Ldlo;->a(Landroid/util/JsonToken;)V

    .line 273
    return-void
.end method

.method public final close()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 566
    iput-object v0, p0, Ldlo;->bBM:Ldlq;

    .line 567
    iput-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    .line 568
    iget-object v0, p0, Ldlo;->bBI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 569
    iget-object v0, p0, Ldlo;->bBI:Ljava/util/List;

    sget-object v1, Ldln;->bBy:Ldln;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 570
    iget-object v0, p0, Ldlo;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 571
    return-void
.end method

.method public final endArray()V
    .locals 1

    .prologue
    .line 264
    sget-object v0, Landroid/util/JsonToken;->END_ARRAY:Landroid/util/JsonToken;

    invoke-direct {p0, v0}, Ldlo;->a(Landroid/util/JsonToken;)V

    .line 265
    return-void
.end method

.method public final endObject()V
    .locals 1

    .prologue
    .line 280
    sget-object v0, Landroid/util/JsonToken;->END_OBJECT:Landroid/util/JsonToken;

    invoke-direct {p0, v0}, Ldlo;->a(Landroid/util/JsonToken;)V

    .line 281
    return-void
.end method

.method public final hasNext()Z
    .locals 2

    .prologue
    .line 298
    invoke-virtual {p0}, Ldlo;->peek()Landroid/util/JsonToken;

    .line 299
    iget-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    sget-object v1, Landroid/util/JsonToken;->END_OBJECT:Landroid/util/JsonToken;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    sget-object v1, Landroid/util/JsonToken;->END_ARRAY:Landroid/util/JsonToken;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final jP(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 245
    iget v0, p0, Ldlo;->pos:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Ldlo;->bBF:I

    if-le v0, v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {p0, v0}, Ldlo;->fZ(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Ldlo;->jQ(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 247
    :cond_0
    iget v0, p0, Ldlo;->pos:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Ldlo;->pos:I

    .line 249
    :cond_1
    return-void
.end method

.method public final nextBoolean()Z
    .locals 3

    .prologue
    .line 450
    invoke-virtual {p0}, Ldlo;->peek()Landroid/util/JsonToken;

    .line 451
    iget-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    sget-object v1, Landroid/util/JsonToken;->BOOLEAN:Landroid/util/JsonToken;

    if-eq v0, v1, :cond_0

    .line 452
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected a boolean but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 455
    :cond_0
    iget-object v0, p0, Ldlo;->bBM:Ldlq;

    iget-object v0, v0, Ldlq;->rQ:[B

    sget-object v1, Ldlo;->bBB:[B

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    .line 456
    :goto_0
    invoke-direct {p0}, Ldlo;->acU()Landroid/util/JsonToken;

    .line 457
    return v0

    .line 455
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nextInt()I
    .locals 6

    .prologue
    .line 539
    invoke-virtual {p0}, Ldlo;->peek()Landroid/util/JsonToken;

    .line 540
    iget-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    sget-object v1, Landroid/util/JsonToken;->STRING:Landroid/util/JsonToken;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    sget-object v1, Landroid/util/JsonToken;->NUMBER:Landroid/util/JsonToken;

    if-eq v0, v1, :cond_0

    .line 541
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected an int but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 545
    :cond_0
    iget-object v0, p0, Ldlo;->bBD:Ldmr;

    iget-object v1, p0, Ldlo;->bBM:Ldlq;

    iget-object v1, v1, Ldlq;->rQ:[B

    iget-object v2, p0, Ldlo;->bBM:Ldlq;

    iget v2, v2, Ldlq;->ff:I

    iget-object v3, p0, Ldlo;->bBM:Ldlq;

    iget v3, v3, Ldlq;->mLength:I

    invoke-virtual {v0, v1, v2, v3}, Ldmr;->i([BII)Ljava/lang/String;

    move-result-object v1

    .line 547
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 557
    :cond_1
    invoke-direct {p0}, Ldlo;->acU()Landroid/util/JsonToken;

    .line 558
    return v0

    .line 550
    :catch_0
    move-exception v0

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 551
    double-to-int v0, v2

    .line 552
    int-to-double v4, v0

    cmpl-double v2, v4, v2

    if-eqz v2, :cond_1

    .line 553
    new-instance v0, Ljava/lang/NumberFormatException;

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final nextName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 389
    invoke-virtual {p0}, Ldlo;->acV()Ldlq;

    move-result-object v0

    .line 390
    iget-object v1, p0, Ldlo;->bBD:Ldmr;

    iget-object v2, v0, Ldlq;->rQ:[B

    iget v3, v0, Ldlq;->ff:I

    iget v0, v0, Ldlq;->mLength:I

    invoke-virtual {v1, v2, v3, v0}, Ldmr;->i([BII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final nextNull()V
    .locals 3

    .prologue
    .line 468
    invoke-virtual {p0}, Ldlo;->peek()Landroid/util/JsonToken;

    .line 469
    iget-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    sget-object v1, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v0, v1, :cond_0

    .line 470
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected null but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 473
    :cond_0
    invoke-direct {p0}, Ldlo;->acU()Landroid/util/JsonToken;

    .line 474
    return-void
.end method

.method public final nextString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 424
    invoke-direct {p0}, Ldlo;->acW()Ldlq;

    move-result-object v0

    .line 425
    iget-object v1, p0, Ldlo;->bBD:Ldmr;

    iget-object v2, v0, Ldlq;->rQ:[B

    iget v3, v0, Ldlq;->ff:I

    iget v0, v0, Ldlq;->mLength:I

    invoke-virtual {v1, v2, v3, v0}, Ldmr;->i([BII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final peek()Landroid/util/JsonToken;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 306
    iget-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    if-eqz v0, :cond_1

    .line 307
    iget-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    .line 337
    :cond_0
    :goto_0
    return-object v0

    .line 310
    :cond_1
    sget-object v1, Ldlp;->bBN:[I

    iget-object v0, p0, Ldlo;->bBI:Ljava/util/List;

    iget-object v2, p0, Ldlo;->bBI:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldln;

    invoke-virtual {v0}, Ldln;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 342
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 312
    :pswitch_0
    sget-object v0, Ldln;->bBx:Ldln;

    invoke-direct {p0, v0}, Ldlo;->b(Ldln;)V

    .line 313
    invoke-direct {p0}, Ldlo;->acZ()Landroid/util/JsonToken;

    move-result-object v0

    .line 314
    iget-boolean v1, p0, Ldlo;->bBE:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    sget-object v2, Landroid/util/JsonToken;->BEGIN_ARRAY:Landroid/util/JsonToken;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    sget-object v2, Landroid/util/JsonToken;->BEGIN_OBJECT:Landroid/util/JsonToken;

    if-eq v1, v2, :cond_0

    .line 315
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected JSON document to start with \'[\' or \'{\' but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 320
    :pswitch_1
    invoke-direct {p0, v3}, Ldlo;->dA(Z)Landroid/util/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 322
    :pswitch_2
    invoke-direct {p0, v4}, Ldlo;->dA(Z)Landroid/util/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 324
    :pswitch_3
    invoke-direct {p0, v3}, Ldlo;->dB(Z)Landroid/util/JsonToken;

    move-result-object v0

    goto :goto_0

    .line 326
    :pswitch_4
    invoke-direct {p0}, Ldlo;->ada()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :pswitch_5
    const-string v0, "Expected \':\'"

    invoke-direct {p0, v0}, Ldlo;->jR(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :pswitch_6
    invoke-direct {p0}, Ldlo;->adb()V

    iget v0, p0, Ldlo;->pos:I

    iget v1, p0, Ldlo;->bBF:I

    if-lt v0, v1, :cond_2

    invoke-direct {p0, v3}, Ldlo;->fZ(I)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Ldlo;->buffer:[B

    iget v1, p0, Ldlo;->pos:I

    aget-byte v0, v0, v1

    const/16 v1, 0x3e

    if-ne v0, v1, :cond_3

    iget v0, p0, Ldlo;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldlo;->pos:I

    :cond_3
    :pswitch_7
    sget-object v0, Ldln;->bBv:Ldln;

    invoke-direct {p0, v0}, Ldlo;->b(Ldln;)V

    invoke-direct {p0}, Ldlo;->acZ()Landroid/util/JsonToken;

    move-result-object v0

    goto/16 :goto_0

    .line 328
    :pswitch_8
    invoke-direct {p0, v4}, Ldlo;->dB(Z)Landroid/util/JsonToken;

    move-result-object v0

    goto/16 :goto_0

    .line 331
    :pswitch_9
    :try_start_0
    invoke-direct {p0}, Ldlo;->acZ()Landroid/util/JsonToken;

    move-result-object v0

    .line 332
    iget-boolean v1, p0, Ldlo;->bBE:Z

    if-nez v1, :cond_0

    .line 335
    const-string v0, "Expected EOF"

    invoke-direct {p0, v0}, Ldlo;->jR(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    .line 337
    :catch_0
    move-exception v0

    sget-object v0, Landroid/util/JsonToken;->END_DOCUMENT:Landroid/util/JsonToken;

    iput-object v0, p0, Ldlo;->bBJ:Landroid/util/JsonToken;

    goto/16 :goto_0

    .line 340
    :pswitch_a
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "JsonReader is closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 310
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 326
    :pswitch_data_1
    .packed-switch 0x3a
        :pswitch_7
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final setLenient(Z)V
    .locals 0

    .prologue
    .line 234
    iput-boolean p1, p0, Ldlo;->bBE:Z

    .line 235
    return-void
.end method

.method public final skipValue()V
    .locals 3

    .prologue
    .line 579
    const/4 v0, 0x0

    .line 581
    :cond_0
    invoke-direct {p0}, Ldlo;->acU()Landroid/util/JsonToken;

    move-result-object v1

    .line 582
    sget-object v2, Landroid/util/JsonToken;->BEGIN_ARRAY:Landroid/util/JsonToken;

    if-eq v1, v2, :cond_1

    sget-object v2, Landroid/util/JsonToken;->BEGIN_OBJECT:Landroid/util/JsonToken;

    if-ne v1, v2, :cond_3

    .line 583
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 587
    :cond_2
    :goto_0
    if-nez v0, :cond_0

    .line 588
    return-void

    .line 584
    :cond_3
    sget-object v2, Landroid/util/JsonToken;->END_ARRAY:Landroid/util/JsonToken;

    if-eq v1, v2, :cond_4

    sget-object v2, Landroid/util/JsonToken;->END_OBJECT:Landroid/util/JsonToken;

    if-ne v1, v2, :cond_2

    .line 585
    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v4, 0x14

    .line 1004
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "JsonUtf8Reader near "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Ldlo;->pos:I

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget v2, p0, Ldlo;->bBF:I

    iget v3, p0, Ldlo;->pos:I

    sub-int/2addr v2, v3

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    add-int v3, v1, v2

    new-array v3, v3, [B

    iget-object v4, p0, Ldlo;->buffer:[B

    iget v5, p0, Ldlo;->pos:I

    sub-int/2addr v5, v1

    const/4 v6, 0x0

    invoke-static {v4, v5, v3, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v4, p0, Ldlo;->buffer:[B

    iget v5, p0, Ldlo;->pos:I

    invoke-static {v4, v5, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
