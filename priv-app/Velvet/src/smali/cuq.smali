.class final Lcuq;
.super Lctt;
.source "PG"


# instance fields
.field private final bjY:I

.field private final bjZ:I

.field private final bjk:I

.field private final bjl:I

.field private final bjm:I

.field private final bjo:I

.field private final bka:I

.field private final bkb:I

.field private final bkc:I

.field private final bkd:I

.field private final bke:I

.field private final bkf:I

.field private final bkg:I

.field private bkh:J

.field final mCursor:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 104
    invoke-direct {p0}, Lctt;-><init>()V

    .line 105
    const-string v1, "contacts"

    sget-object v2, Lctr;->bik:[Ljava/lang/String;

    const-string v7, "contact_id"

    move-object v0, p1

    move-object v3, p2

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcuq;->mCursor:Landroid/database/Cursor;

    .line 108
    iget-object v0, p0, Lcuq;->mCursor:Landroid/database/Cursor;

    const-string v1, "contact_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuq;->bjk:I

    .line 109
    iget-object v0, p0, Lcuq;->mCursor:Landroid/database/Cursor;

    const-string v1, "lookup_key"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuq;->bjo:I

    .line 110
    iget-object v0, p0, Lcuq;->mCursor:Landroid/database/Cursor;

    const-string v1, "icon_uri"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuq;->bjZ:I

    .line 111
    iget-object v0, p0, Lcuq;->mCursor:Landroid/database/Cursor;

    const-string v1, "display_name"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuq;->bjl:I

    .line 112
    iget-object v0, p0, Lcuq;->mCursor:Landroid/database/Cursor;

    const-string v1, "given_names"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuq;->bka:I

    .line 113
    iget-object v0, p0, Lcuq;->mCursor:Landroid/database/Cursor;

    const-string v1, "score"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuq;->bjY:I

    .line 114
    iget-object v0, p0, Lcuq;->mCursor:Landroid/database/Cursor;

    const-string v1, "emails"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuq;->bkb:I

    .line 115
    iget-object v0, p0, Lcuq;->mCursor:Landroid/database/Cursor;

    const-string v1, "nickname"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuq;->bkc:I

    .line 116
    iget-object v0, p0, Lcuq;->mCursor:Landroid/database/Cursor;

    const-string v1, "note"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuq;->bkd:I

    .line 117
    iget-object v0, p0, Lcuq;->mCursor:Landroid/database/Cursor;

    const-string v1, "organization"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuq;->bke:I

    .line 118
    iget-object v0, p0, Lcuq;->mCursor:Landroid/database/Cursor;

    const-string v1, "phone_numbers"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuq;->bkf:I

    .line 119
    iget-object v0, p0, Lcuq;->mCursor:Landroid/database/Cursor;

    const-string v1, "postal_address"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuq;->bkg:I

    .line 120
    iget-object v0, p0, Lcuq;->mCursor:Landroid/database/Cursor;

    const-string v1, "phonetic_name"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcuq;->bjm:I

    .line 121
    return-void
.end method


# virtual methods
.method protected final synthetic ST()Ljava/lang/Object;
    .locals 18

    .prologue
    .line 85
    move-object/from16 v0, p0

    iget-object v2, v0, Lcuq;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcuq;->bkh:J

    move-object/from16 v0, p0

    iget-object v4, v0, Lcuq;->mCursor:Landroid/database/Cursor;

    const-wide/16 v4, 0x0

    add-long/2addr v2, v4

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcuq;->bkh:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcuq;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget v3, v0, Lcuq;->bjk:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcuq;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget v5, v0, Lcuq;->bjo:I

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcuq;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget v6, v0, Lcuq;->bjZ:I

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcuq;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget v7, v0, Lcuq;->bjl:I

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcuq;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget v8, v0, Lcuq;->bka:I

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcuq;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget v9, v0, Lcuq;->bjY:I

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lcuq;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget v11, v0, Lcuq;->bkb:I

    invoke-interface {v10, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcuq;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget v12, v0, Lcuq;->bkc:I

    invoke-interface {v11, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcuq;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget v13, v0, Lcuq;->bkd:I

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcuq;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget v14, v0, Lcuq;->bke:I

    invoke-interface {v13, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcuq;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget v15, v0, Lcuq;->bkf:I

    invoke-interface {v14, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcuq;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget v0, v0, Lcuq;->bkg:I

    move/from16 v16, v0

    invoke-interface/range {v15 .. v16}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcuq;->mCursor:Landroid/database/Cursor;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcuq;->bjm:I

    move/from16 v17, v0

    invoke-interface/range {v16 .. v17}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v2 .. v16}, Lcto;->a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
