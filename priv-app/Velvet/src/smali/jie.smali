.class public final Ljie;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile enb:[Ljie;


# instance fields
.field private aez:I

.field private dLZ:[B

.field private dQU:Liym;

.field public eaW:[Ljbp;

.field private ede:Liyn;

.field private enc:J

.field public end:[I

.field public ene:[Lize;

.field private enf:J

.field private eng:J

.field public enh:[Liyz;


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ljie;->aez:I

    iput-wide v2, p0, Ljie;->enc:J

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljie;->end:[I

    invoke-static {}, Lize;->bcK()[Lize;

    move-result-object v0

    iput-object v0, p0, Ljie;->ene:[Lize;

    invoke-static {}, Ljbp;->beZ()[Ljbp;

    move-result-object v0

    iput-object v0, p0, Ljie;->eaW:[Ljbp;

    iput-wide v2, p0, Ljie;->enf:J

    iput-wide v2, p0, Ljie;->eng:J

    iput-object v1, p0, Ljie;->ede:Liyn;

    iput-object v1, p0, Ljie;->dQU:Liym;

    invoke-static {}, Liyz;->bcC()[Liyz;

    move-result-object v0

    iput-object v0, p0, Ljie;->enh:[Liyz;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljie;->dLZ:[B

    iput-object v1, p0, Ljie;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljie;->eCz:I

    return-void
.end method

.method public static bmS()[Ljie;
    .locals 2

    sget-object v0, Ljie;->enb:[Ljie;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljie;->enb:[Ljie;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljie;

    sput-object v0, Ljie;->enb:[Ljie;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Ljie;->enb:[Ljie;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 7

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljie;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Ljsi;->btN()I

    :cond_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_1
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Ljie;->end:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Ljie;->end:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ljie;->end:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Ljie;->end:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Ljie;->end:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    :pswitch_2
    goto :goto_4

    :pswitch_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Ljsi;->rX(I)V

    iget-object v1, p0, Ljie;->end:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Ljie;->end:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    :pswitch_4
    goto :goto_6

    :pswitch_5
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Ljie;->end:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Ljie;->end:[I

    :cond_a
    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v1

    iget-object v0, p0, Ljie;->eaW:[Ljbp;

    if-nez v0, :cond_c

    move v0, v2

    :goto_7
    add-int/2addr v1, v0

    new-array v1, v1, [Ljbp;

    if-eqz v0, :cond_b

    iget-object v3, p0, Ljie;->eaW:[Ljbp;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_8
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    new-instance v3, Ljbp;

    invoke-direct {v3}, Ljbp;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_c
    iget-object v0, p0, Ljie;->eaW:[Ljbp;

    array-length v0, v0

    goto :goto_7

    :cond_d
    new-instance v3, Ljbp;

    invoke-direct {v3}, Ljbp;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v1, p0, Ljie;->eaW:[Ljbp;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljie;->enf:J

    iget v0, p0, Ljie;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljie;->aez:I

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v1

    iget-object v0, p0, Ljie;->ene:[Lize;

    if-nez v0, :cond_f

    move v0, v2

    :goto_9
    add-int/2addr v1, v0

    new-array v1, v1, [Lize;

    if-eqz v0, :cond_e

    iget-object v3, p0, Ljie;->ene:[Lize;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_e
    :goto_a
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_10

    new-instance v3, Lize;

    invoke-direct {v3}, Lize;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_f
    iget-object v0, p0, Ljie;->ene:[Lize;

    array-length v0, v0

    goto :goto_9

    :cond_10
    new-instance v3, Lize;

    invoke-direct {v3}, Lize;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v1, p0, Ljie;->ene:[Lize;

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Ljie;->ede:Liyn;

    if-nez v0, :cond_11

    new-instance v0, Liyn;

    invoke-direct {v0}, Liyn;-><init>()V

    iput-object v0, p0, Ljie;->ede:Liyn;

    :cond_11
    iget-object v0, p0, Ljie;->ede:Liyn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljie;->eng:J

    iget v0, p0, Ljie;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljie;->aez:I

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ljie;->dQU:Liym;

    if-nez v0, :cond_12

    new-instance v0, Liym;

    invoke-direct {v0}, Liym;-><init>()V

    iput-object v0, p0, Ljie;->dQU:Liym;

    :cond_12
    iget-object v0, p0, Ljie;->dQU:Liym;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v1

    iget-object v0, p0, Ljie;->enh:[Liyz;

    if-nez v0, :cond_14

    move v0, v2

    :goto_b
    add-int/2addr v1, v0

    new-array v1, v1, [Liyz;

    if-eqz v0, :cond_13

    iget-object v3, p0, Ljie;->enh:[Liyz;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_13
    :goto_c
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_15

    new-instance v3, Liyz;

    invoke-direct {v3}, Liyz;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_14
    iget-object v0, p0, Ljie;->enh:[Liyz;

    array-length v0, v0

    goto :goto_b

    :cond_15
    new-instance v3, Liyz;

    invoke-direct {v3}, Liyz;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v1, p0, Ljie;->enh:[Liyz;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljie;->enc:J

    iget v0, p0, Ljie;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljie;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljie;->dLZ:[B

    iget v0, p0, Ljie;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljie;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
        0x18 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Ljie;->end:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljie;->end:[I

    array-length v0, v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Ljie;->end:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    const/4 v2, 0x1

    iget-object v3, p0, Ljie;->end:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljsj;->bq(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ljie;->eaW:[Ljbp;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljie;->eaW:[Ljbp;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_1
    iget-object v2, p0, Ljie;->eaW:[Ljbp;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Ljie;->eaW:[Ljbp;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget v0, p0, Ljie;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    iget-wide v2, p0, Ljie;->enf:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_3
    iget-object v0, p0, Ljie;->ene:[Lize;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljie;->ene:[Lize;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    :goto_2
    iget-object v2, p0, Ljie;->ene:[Lize;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ljie;->ene:[Lize;

    aget-object v2, v2, v0

    if-eqz v2, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ljie;->ede:Liyn;

    if-eqz v0, :cond_6

    const/4 v0, 0x6

    iget-object v2, p0, Ljie;->ede:Liyn;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    :cond_6
    iget v0, p0, Ljie;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_7

    const/4 v0, 0x7

    iget-wide v2, p0, Ljie;->eng:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_7
    iget-object v0, p0, Ljie;->dQU:Liym;

    if-eqz v0, :cond_8

    const/16 v0, 0x8

    iget-object v2, p0, Ljie;->dQU:Liym;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    :cond_8
    iget-object v0, p0, Ljie;->enh:[Liyz;

    if-eqz v0, :cond_a

    iget-object v0, p0, Ljie;->enh:[Liyz;

    array-length v0, v0

    if-lez v0, :cond_a

    :goto_3
    iget-object v0, p0, Ljie;->enh:[Liyz;

    array-length v0, v0

    if-ge v1, v0, :cond_a

    iget-object v0, p0, Ljie;->enh:[Liyz;

    aget-object v0, v0, v1

    if-eqz v0, :cond_9

    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_a
    iget v0, p0, Ljie;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_b

    const/16 v0, 0xa

    iget-wide v2, p0, Ljie;->enc:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_b
    iget v0, p0, Ljie;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_c

    const/16 v0, 0xb

    iget-object v1, p0, Ljie;->dLZ:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    :cond_c
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final am([B)Ljie;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljie;->dLZ:[B

    iget v0, p0, Ljie;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljie;->aez:I

    return-object p0
.end method

.method public final bkv()Z
    .locals 1

    iget v0, p0, Ljie;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bmT()J
    .locals 2

    iget-wide v0, p0, Ljie;->enc:J

    return-wide v0
.end method

.method public final bmU()Z
    .locals 1

    iget v0, p0, Ljie;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bmV()J
    .locals 2

    iget-wide v0, p0, Ljie;->enf:J

    return-wide v0
.end method

.method public final bmW()J
    .locals 2

    iget-wide v0, p0, Ljie;->eng:J

    return-wide v0
.end method

.method public final bmX()Z
    .locals 1

    iget v0, p0, Ljie;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bmY()[B
    .locals 1

    iget-object v0, p0, Ljie;->dLZ:[B

    return-object v0
.end method

.method public final do(J)Ljie;
    .locals 1

    iput-wide p1, p0, Ljie;->enc:J

    iget v0, p0, Ljie;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljie;->aez:I

    return-object p0
.end method

.method public final dp(J)Ljie;
    .locals 1

    iput-wide p1, p0, Ljie;->enf:J

    iget v0, p0, Ljie;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljie;->aez:I

    return-object p0
.end method

.method public final dq(J)Ljie;
    .locals 1

    iput-wide p1, p0, Ljie;->eng:J

    iget v0, p0, Ljie;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljie;->aez:I

    return-object p0
.end method

.method protected final lF()I
    .locals 6

    const/4 v1, 0x0

    invoke-super {p0}, Ljsl;->lF()I

    move-result v3

    iget-object v0, p0, Ljie;->end:[I

    if-eqz v0, :cond_f

    iget-object v0, p0, Ljie;->end:[I

    array-length v0, v0

    if-lez v0, :cond_f

    move v0, v1

    move v2, v1

    :goto_0
    iget-object v4, p0, Ljie;->end:[I

    array-length v4, v4

    if-ge v0, v4, :cond_0

    iget-object v4, p0, Ljie;->end:[I

    aget v4, v4, v0

    invoke-static {v4}, Ljsj;->sa(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    add-int v0, v3, v2

    iget-object v2, p0, Ljie;->end:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :goto_1
    iget-object v2, p0, Ljie;->eaW:[Ljbp;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljie;->eaW:[Ljbp;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    :goto_2
    iget-object v3, p0, Ljie;->eaW:[Ljbp;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    iget-object v3, p0, Ljie;->eaW:[Ljbp;

    aget-object v3, v3, v0

    if-eqz v3, :cond_1

    const/4 v4, 0x2

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move v0, v2

    :cond_3
    iget v2, p0, Ljie;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_4

    const/4 v2, 0x3

    iget-wide v4, p0, Ljie;->enf:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Ljie;->ene:[Lize;

    if-eqz v2, :cond_7

    iget-object v2, p0, Ljie;->ene:[Lize;

    array-length v2, v2

    if-lez v2, :cond_7

    move v2, v0

    move v0, v1

    :goto_3
    iget-object v3, p0, Ljie;->ene:[Lize;

    array-length v3, v3

    if-ge v0, v3, :cond_6

    iget-object v3, p0, Ljie;->ene:[Lize;

    aget-object v3, v3, v0

    if-eqz v3, :cond_5

    const/4 v4, 0x5

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    move v0, v2

    :cond_7
    iget-object v2, p0, Ljie;->ede:Liyn;

    if-eqz v2, :cond_8

    const/4 v2, 0x6

    iget-object v3, p0, Ljie;->ede:Liyn;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iget v2, p0, Ljie;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_9

    const/4 v2, 0x7

    iget-wide v4, p0, Ljie;->eng:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_9
    iget-object v2, p0, Ljie;->dQU:Liym;

    if-eqz v2, :cond_a

    const/16 v2, 0x8

    iget-object v3, p0, Ljie;->dQU:Liym;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_a
    iget-object v2, p0, Ljie;->enh:[Liyz;

    if-eqz v2, :cond_c

    iget-object v2, p0, Ljie;->enh:[Liyz;

    array-length v2, v2

    if-lez v2, :cond_c

    :goto_4
    iget-object v2, p0, Ljie;->enh:[Liyz;

    array-length v2, v2

    if-ge v1, v2, :cond_c

    iget-object v2, p0, Ljie;->enh:[Liyz;

    aget-object v2, v2, v1

    if-eqz v2, :cond_b

    const/16 v3, 0x9

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_c
    iget v1, p0, Ljie;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_d

    const/16 v1, 0xa

    iget-wide v2, p0, Ljie;->enc:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget v1, p0, Ljie;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_e

    const/16 v1, 0xb

    iget-object v2, p0, Ljie;->dLZ:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    return v0

    :cond_f
    move v0, v3

    goto/16 :goto_1
.end method
