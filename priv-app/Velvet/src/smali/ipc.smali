.class final Lipc;
.super Ljava/util/AbstractSet;
.source "PG"


# instance fields
.field private synthetic dFT:Lioy;


# direct methods
.method private constructor <init>(Lioy;)V
    .locals 0

    .prologue
    .line 1846
    iput-object p1, p0, Lipc;->dFT:Lioy;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lioy;B)V
    .locals 0

    .prologue
    .line 1846
    invoke-direct {p0, p1}, Lipc;-><init>(Lioy;)V

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 1

    .prologue
    .line 1867
    iget-object v0, p0, Lipc;->dFT:Lioy;

    iget-object v0, v0, Lioy;->dFS:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1868
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1871
    iget-object v0, p0, Lipc;->dFT:Lioy;

    invoke-virtual {v0, p1}, Lioy;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 1848
    iget-object v0, p0, Lipc;->dFT:Lioy;

    iget-object v0, v0, Lioy;->dFS:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1849
    new-instance v1, Lipd;

    invoke-direct {v1, p0, v0}, Lipd;-><init>(Lipc;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1875
    iget-object v0, p0, Lipc;->dFT:Lioy;

    invoke-virtual {v0, p1}, Lioy;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1876
    iget-object v0, p0, Lipc;->dFT:Lioy;

    iget-object v0, v0, Lioy;->dFP:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1877
    const/4 v0, 0x1

    .line 1879
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .locals 3

    .prologue
    .line 1883
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1884
    const/4 v0, 0x0

    .line 1885
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1886
    invoke-virtual {p0, v2}, Lipc;->remove(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v0, v2

    .line 1887
    goto :goto_0

    .line 1888
    :cond_0
    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .locals 4

    .prologue
    .line 1892
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1893
    const/4 v0, 0x0

    .line 1894
    iget-object v1, p0, Lipc;->dFT:Lioy;

    iget-object v1, v1, Lioy;->dFP:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    .line 1895
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1896
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1897
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lipc;->dFT:Lioy;

    iget-object v3, v3, Lioy;->dBv:Lifw;

    invoke-interface {v3, v0}, Lifw;->apply(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1898
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 1899
    const/4 v1, 0x1

    move v0, v1

    :goto_1
    move v1, v0

    .line 1901
    goto :goto_0

    .line 1902
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1863
    iget-object v0, p0, Lipc;->dFT:Lioy;

    iget-object v0, v0, Lioy;->dFS:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1907
    invoke-virtual {p0}, Lipc;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lilw;->g(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1911
    invoke-virtual {p0}, Lipc;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lilw;->g(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
