.class public final Lcxz;
.super Lcyc;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private bmB:Landroid/database/DataSetObserver;

.field private bmC:Landroid/preference/PreferenceGroup;

.field private final mContext:Landroid/content/Context;

.field private final mGss:Lcgh;


# direct methods
.method public constructor <init>(Lcke;Lcgh;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcyc;-><init>(Lcke;)V

    .line 45
    iput-object p2, p0, Lcxz;->mGss:Lcgh;

    .line 46
    iput-object p3, p0, Lcxz;->mContext:Landroid/content/Context;

    .line 47
    return-void
.end method

.method static synthetic a(Lcxz;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcxz;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 2

    .prologue
    .line 61
    check-cast p1, Landroid/preference/PreferenceGroup;

    iput-object p1, p0, Lcxz;->bmC:Landroid/preference/PreferenceGroup;

    .line 62
    iget-object v0, p0, Lcxz;->bmC:Landroid/preference/PreferenceGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->setOrderingAsAdded(Z)V

    .line 64
    iget-object v0, p0, Lcxz;->mGss:Lcgh;

    invoke-interface {v0}, Lcgh;->EN()Ldgh;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcxz;->bmB:Landroid/database/DataSetObserver;

    if-nez v1, :cond_0

    .line 66
    new-instance v1, Lcya;

    invoke-direct {v1, p0, v0}, Lcya;-><init>(Lcxz;Ldgh;)V

    iput-object v1, p0, Lcxz;->bmB:Landroid/database/DataSetObserver;

    .line 73
    iget-object v1, p0, Lcxz;->bmB:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Ldgh;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 76
    :cond_0
    invoke-interface {v0}, Ldgh;->abS()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcxz;->i(Ljava/util/Collection;)V

    .line 77
    return-void
.end method

.method final declared-synchronized i(Ljava/util/Collection;)V
    .locals 8

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcxz;->bmC:Landroid/preference/PreferenceGroup;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 83
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcxz;->bmC:Landroid/preference/PreferenceGroup;

    invoke-virtual {v2}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 84
    iget-object v2, p0, Lcxz;->bmC:Landroid/preference/PreferenceGroup;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgb;

    .line 88
    invoke-static {v0}, Lckf;->f(Ldgb;)Ljava/lang/String;

    move-result-object v3

    .line 90
    iget-object v4, p0, Lcxz;->bmC:Landroid/preference/PreferenceGroup;

    invoke-virtual {v4, v3}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    .line 91
    if-eqz v3, :cond_1

    .line 92
    invoke-interface {v1, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 95
    :cond_1
    :try_start_1
    new-instance v3, Lcxy;

    iget-object v4, p0, Lcxz;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcxy;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lckf;->f(Ldgb;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcxy;->setKey(Ljava/lang/String;)V

    invoke-interface {v0}, Ldgb;->isEnabled()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcxy;->setChecked(Z)V

    invoke-virtual {v3, p0}, Lcxy;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v4, p0, Lcxz;->mContext:Landroid/content/Context;

    invoke-static {v4, v0}, Ldgg;->a(Landroid/content/Context;Ldgb;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcxy;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcxz;->mContext:Landroid/content/Context;

    invoke-interface {v0}, Ldgb;->abM()I

    move-result v5

    invoke-static {v4, v0, v5}, Ldgg;->a(Landroid/content/Context;Ldgb;I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcxy;->setSummaryOn(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v4}, Lcxy;->setSummaryOff(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcxz;->mContext:Landroid/content/Context;

    invoke-interface {v0}, Ldgb;->abN()I

    move-result v5

    if-nez v5, :cond_3

    const/4 v0, 0x0

    :goto_2
    if-nez v0, :cond_2

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0202c3

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_2
    invoke-virtual {v3, v0}, Lcxy;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 96
    iget-object v0, p0, Lcxz;->bmC:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_1

    .line 95
    :cond_3
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-interface {v0}, Ldgb;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0}, Ldgb;->abN()I

    move-result v7

    invoke-interface {v0}, Ldgb;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    invoke-virtual {v5, v6, v7, v0}, Landroid/content/pm/PackageManager;->getDrawable(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_2

    .line 100
    :cond_4
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    .line 102
    iget-object v2, p0, Lcxz;->bmC:Landroid/preference/PreferenceGroup;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 104
    :cond_5
    monitor-exit p0

    return-void
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcxz;->bmB:Landroid/database/DataSetObserver;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcxz;->mGss:Lcgh;

    invoke-interface {v0}, Lcgh;->EN()Ldgh;

    move-result-object v0

    iget-object v1, p0, Lcxz;->bmB:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Ldgh;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcxz;->bmB:Landroid/database/DataSetObserver;

    .line 56
    :cond_0
    invoke-super {p0}, Lcyc;->onDestroy()V

    .line 57
    return-void
.end method

.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 127
    invoke-virtual {p0}, Lcxz;->TL()Lcke;

    move-result-object v1

    .line 128
    invoke-interface {v1}, Lcke;->Oa()V

    .line 130
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 131
    const-string v2, "enable_corpus_"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 132
    const-string v1, "Search.SearchableItemsSettings"

    const-string v2, "Key %s did not start with %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const-string v0, "enable_corpus_"

    aput-object v0, v3, v8

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 159
    :cond_0
    return v8

    .line 137
    :cond_1
    const/16 v2, 0xe

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 139
    iget-object v0, p0, Lcxz;->mGss:Lcgh;

    invoke-interface {v0}, Lcgh;->EN()Ldgh;

    move-result-object v0

    invoke-interface {v0}, Ldgh;->abS()Ljava/util/Collection;

    move-result-object v0

    .line 140
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgb;

    .line 141
    invoke-interface {v0}, Ldgb;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 142
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 143
    invoke-interface {v0, v4}, Ldgb;->setEnabled(Z)V

    .line 144
    invoke-interface {v1, v0}, Lcke;->e(Ldgb;)V

    .line 145
    new-instance v5, Lcyb;

    invoke-direct {v5, p0}, Lcyb;-><init>(Lcxz;)V

    .line 153
    invoke-interface {v0}, Ldgb;->getPackageName()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcxz;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 154
    iget-object v6, p0, Lcxz;->mGss:Lcgh;

    invoke-interface {v6}, Lcgh;->ET()Ldgm;

    move-result-object v6

    invoke-interface {v0}, Ldgb;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0, v4, v5}, Ldgm;->a(Ljava/lang/String;ZLjava/lang/Runnable;)V

    goto :goto_0
.end method
