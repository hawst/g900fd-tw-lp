.class public final Lgcq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final cGb:Lgcq;

.field public static final cGc:Lgcq;


# instance fields
.field private final ag:I

.field private final cGd:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cGe:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lgcq;

    invoke-direct {v0, v3, v2, v2}, Lgcq;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lgcq;->cGb:Lgcq;

    .line 27
    new-instance v0, Lgcq;

    const/4 v1, 0x1

    invoke-direct {v0, v3, v1, v2}, Lgcq;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lgcq;->cGc:Lgcq;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IZ)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lgcq;->cGd:Ljava/lang/String;

    .line 48
    iput p2, p0, Lgcq;->ag:I

    .line 49
    iput-boolean p3, p0, Lgcq;->cGe:Z

    .line 50
    return-void
.end method


# virtual methods
.method public final Qn()Z
    .locals 2

    .prologue
    .line 84
    iget v0, p0, Lgcq;->ag:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Wy()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lgcq;->cGe:Z

    return v0
.end method

.method public final aET()Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lgcq;->cGd:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aEU()Z
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lgcq;->cGc:Lgcq;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aEV()Z
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lgcq;->ag:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 89
    instance-of v1, p1, Lgcq;

    if-eqz v1, :cond_0

    .line 90
    check-cast p1, Lgcq;

    .line 91
    iget v1, p0, Lgcq;->ag:I

    iget v2, p1, Lgcq;->ag:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lgcq;->cGd:Ljava/lang/String;

    iget-object v2, p1, Lgcq;->cGd:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 93
    :cond_0
    return v0
.end method

.method public final getPrompt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lgcq;->cGd:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Ok Google"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lgcq;->cGd:Ljava/lang/String;

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lgcq;->cGd:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lgcq;->ag:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lgcq;->cGd:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HotwordModelInfo[\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lgcq;->cGd:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lgcq;->ag:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final yo()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 74
    iget v1, p0, Lgcq;->ag:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
