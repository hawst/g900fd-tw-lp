.class public final Lfvw;
.super Lfuz;
.source "PG"

# interfaces
.implements Lfyj;


# instance fields
.field private final mPhotoWithAttributionDecorator:Lgbd;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lgbd;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 38
    iput-object p4, p0, Lfvw;->mPhotoWithAttributionDecorator:Lgbd;

    .line 39
    return-void
.end method

.method private static a(Ljbw;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 135
    const/4 v0, 0x3

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 136
    iget-object v1, p0, Ljbw;->dZG:[Ljdq;

    array-length v1, v1

    if-lez v1, :cond_0

    iget-object v1, p0, Ljbw;->dZG:[Ljdq;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljdq;->qo()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 138
    iget-object v1, p0, Ljbw;->dZG:[Ljdq;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljdq;->qo()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    :cond_0
    invoke-virtual {p0}, Ljbw;->bfI()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 141
    invoke-virtual {p0}, Ljbw;->bfI()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    :cond_1
    invoke-virtual {p0}, Ljbw;->bfJ()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 144
    invoke-virtual {p0}, Ljbw;->bfJ()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_3

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_3
    const-string v1, " \u00b7 "

    invoke-static {v1, v0}, Lgab;->e(Ljava/lang/String;Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Ljbw;)Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Ljbw;->dZA:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 153
    const-string v0, ", "

    iget-object v1, p0, Ljbw;->dZA:[Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 155
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final M(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSI:Ljbx;

    iget-object v0, v0, Ljbx;->dZH:Ljbw;

    .line 205
    iget-object v1, v0, Ljbw;->aiX:Ljcn;

    if-eqz v1, :cond_0

    .line 206
    iget-object v1, p0, Lfvw;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v0, v0, Ljbw;->aiX:Ljcn;

    const v2, 0x7f0d0220

    const v3, 0x7f0d0214

    invoke-virtual {v1, p1, v0, v2, v3}, Lgbd;->c(Landroid/content/Context;Ljcn;II)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 209
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 10

    .prologue
    .line 43
    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    .line 44
    iget-object v3, v2, Lizj;->dSI:Ljbx;

    .line 46
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 47
    iget-object v0, p0, Lfvw;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUu:Ljdv;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lfvw;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUu:Ljdv;

    invoke-virtual {v0}, Ljdv;->bhO()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 49
    :goto_0
    iget-object v1, v3, Ljbx;->dZH:Ljbw;

    new-instance v5, Lfrs;

    invoke-virtual {v1}, Ljbw;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, p1, v6, v2}, Lfrs;-><init>(Landroid/content/Context;Ljava/lang/String;Lizj;)V

    iput-boolean v0, v5, Lfrs;->cBf:Z

    invoke-static {v1}, Lfvw;->a(Ljbw;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lfrs;->cBj:Ljava/lang/String;

    iget-object v6, v1, Ljbw;->aiX:Ljcn;

    iget-object v7, p0, Lfvw;->mPhotoWithAttributionDecorator:Lgbd;

    invoke-virtual {v5, v6, v7}, Lfrs;->a(Ljcn;Lgbd;)Lfrs;

    move-result-object v5

    if-nez v0, :cond_1

    invoke-virtual {v1}, Ljbw;->bdu()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v5, Lfrs;->cBi:Ljava/lang/String;

    new-instance v1, Lapp;

    invoke-direct {v1}, Lapp;-><init>()V

    iget-object v6, v3, Ljbx;->dZH:Ljbw;

    invoke-static {v6}, Lfvw;->b(Ljbw;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v1, v6}, Lapp;->dy(Ljava/lang/String;)Lapp;

    :cond_0
    iput-object v1, v5, Lfrs;->cBl:Lapp;

    :cond_1
    invoke-virtual {v5}, Lfrs;->aDh()Lanh;

    move-result-object v5

    .line 50
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    invoke-virtual {v5}, Lanh;->oV()Z

    move-result v6

    iget-object v7, v3, Ljbx;->dZH:Ljbw;

    invoke-virtual {v7}, Ljbw;->bfL()Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x0

    :goto_1
    invoke-static {v4, v1}, Lfvw;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 54
    invoke-virtual {v5}, Lanh;->getType()I

    move-result v1

    const/16 v5, 0x21

    if-ne v1, v5, :cond_6

    const/4 v1, 0x1

    .line 55
    :goto_2
    if-nez v0, :cond_2

    if-eqz v1, :cond_3

    .line 56
    :cond_2
    iget-object v1, v3, Ljbx;->dZH:Ljbw;

    invoke-static {v1}, Lfvw;->b(Ljbw;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    const/4 v0, 0x0

    :goto_3
    invoke-static {v4, v0}, Lfvw;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 59
    :cond_3
    iget-object v0, v3, Ljbx;->dZH:Ljbw;

    invoke-virtual {v0}, Ljbw;->bfN()Z

    move-result v1

    if-nez v1, :cond_a

    const/4 v0, 0x0

    :goto_4
    invoke-static {v4, v0}, Lfvw;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 61
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 62
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 63
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lang;->aS(Z)Lang;

    .line 64
    return-object v1

    .line 47
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 51
    :cond_5
    new-instance v8, Laoj;

    invoke-direct {v8}, Laoj;-><init>()V

    const v1, 0x7f0a035e

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    const/4 v9, 0x3

    invoke-virtual {v1, v9}, Lanh;->cm(I)Lanh;

    iput-object v2, v1, Lanh;->ahu:Lizj;

    iput-object v8, v1, Lanh;->agz:Laoj;

    new-instance v8, Lfsa;

    const/16 v9, 0x4a

    invoke-direct {v8, v9}, Lfsa;-><init>(I)V

    const v9, 0x7f0201ba

    invoke-virtual {v8, v9}, Lfsa;->jB(I)Lfsa;

    move-result-object v8

    invoke-virtual {v7}, Ljbw;->bfK()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    invoke-virtual {v8, v7, v9}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v7

    iput-object v7, v1, Lanh;->ahs:Lani;

    invoke-virtual {v1, v6}, Lanh;->aW(Z)Lanh;

    invoke-virtual {v1, v6}, Lanh;->aX(Z)Lanh;

    goto/16 :goto_1

    .line 54
    :cond_6
    const/4 v1, 0x0

    goto :goto_2

    .line 56
    :cond_7
    new-instance v5, Lany;

    invoke-direct {v5}, Lany;-><init>()V

    invoke-virtual {v5, v1}, Lany;->bk(Ljava/lang/String;)Lany;

    if-eqz v0, :cond_9

    iget-object v0, v2, Lizj;->dUu:Ljdv;

    invoke-virtual {v0}, Ljdv;->bhN()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, v2, Lizj;->dUu:Ljdv;

    invoke-virtual {v0}, Ljdv;->bhN()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lany;->bj(Ljava/lang/String;)Lany;

    const v0, 0x7f0201cd

    invoke-virtual {v5, v0}, Lany;->cz(I)Lany;

    :cond_8
    :goto_5
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Lanh;->cm(I)Lanh;

    iput-object v5, v0, Lanh;->agw:Lany;

    iput-object v2, v0, Lanh;->ahu:Lizj;

    goto/16 :goto_3

    :cond_9
    iget-object v0, v3, Ljbx;->dZH:Ljbw;

    invoke-virtual {v0}, Ljbw;->bdu()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, v3, Ljbx;->dZH:Ljbw;

    invoke-virtual {v0}, Ljbw;->bdu()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lany;->bj(Ljava/lang/String;)Lany;

    goto :goto_5

    .line 59
    :cond_a
    new-instance v1, Laoj;

    invoke-direct {v1}, Laoj;-><init>()V

    const v3, 0x7f0a035f

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v3, Lfsa;

    const/4 v5, 0x3

    invoke-direct {v3, v5}, Lfsa;-><init>(I)V

    const v5, 0x7f0200ed

    invoke-virtual {v3, v5}, Lfsa;->jB(I)Lfsa;

    move-result-object v3

    invoke-virtual {v0}, Ljbw;->bfM()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v3

    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    const/4 v5, 0x3

    invoke-virtual {v0, v5}, Lanh;->cm(I)Lanh;

    iput-object v1, v0, Lanh;->agz:Laoj;

    iput-object v3, v0, Lanh;->ahs:Lani;

    iput-object v2, v0, Lanh;->ahu:Lizj;

    goto/16 :goto_4
.end method

.method public final aDC()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 198
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSI:Ljbx;

    iget-object v0, v0, Ljbx;->dZH:Ljbw;

    invoke-virtual {v0}, Ljbw;->bdu()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bd(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSI:Ljbx;

    iget-object v0, v0, Ljbx;->dZH:Ljbw;

    invoke-virtual {v0}, Ljbw;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final be(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 187
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSI:Ljbx;

    iget-object v1, v0, Ljbx;->dZH:Ljbw;

    .line 188
    invoke-static {v1}, Lfvw;->b(Ljbw;)Ljava/lang/String;

    move-result-object v0

    .line 189
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 190
    invoke-static {v1}, Lfvw;->a(Ljbw;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 192
    :cond_0
    return-object v0
.end method
