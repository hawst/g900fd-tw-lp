.class public final Lnr;
.super Lju;
.source "PG"


# instance fields
.field private final gn:Landroid/graphics/Rect;

.field private final nP:Ljava/util/Calendar;

.field private synthetic nQ:Lnq;


# direct methods
.method public constructor <init>(Lnq;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 701
    iput-object p1, p0, Lnr;->nQ:Lnq;

    .line 702
    invoke-direct {p0, p2}, Lju;-><init>(Landroid/view/View;)V

    .line 698
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lnr;->gn:Landroid/graphics/Rect;

    .line 699
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lnr;->nP:Ljava/util/Calendar;

    .line 703
    return-void
.end method

.method private ai(I)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 797
    iget-object v0, p0, Lnr;->nP:Ljava/util/Calendar;

    iget-object v1, p0, Lnr;->nQ:Lnq;

    iget v1, v1, Lnq;->nx:I

    iget-object v2, p0, Lnr;->nQ:Lnq;

    iget v2, v2, Lnq;->nw:I

    invoke-virtual {v0, v1, v2, p1}, Ljava/util/Calendar;->set(III)V

    .line 798
    const-string v0, "dd MMMM yyyy"

    iget-object v1, p0, Lnr;->nP:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    .line 801
    iget-object v1, p0, Lnr;->nQ:Lnq;

    iget v1, v1, Lnq;->nB:I

    if-ne p1, v1, :cond_0

    .line 802
    iget-object v1, p0, Lnr;->nQ:Lnq;

    invoke-virtual {v1}, Lnq;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0064

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 805
    :cond_0
    return-object v0
.end method


# virtual methods
.method protected final a(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 738
    invoke-direct {p0, p1}, Lnr;->ai(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 739
    return-void
.end method

.method protected final a(ILhz;)V
    .locals 7

    .prologue
    .line 744
    iget-object v0, p0, Lnr;->gn:Landroid/graphics/Rect;

    iget-object v1, p0, Lnr;->nQ:Lnq;

    iget-object v1, p0, Lnr;->nQ:Lnq;

    invoke-static {}, Lnq;->cu()I

    move-result v1

    iget-object v2, p0, Lnr;->nQ:Lnq;

    iget v2, v2, Lnq;->nz:I

    iget-object v3, p0, Lnr;->nQ:Lnq;

    iget v3, v3, Lnq;->ny:I

    iget-object v4, p0, Lnr;->nQ:Lnq;

    add-int/lit8 v3, v3, 0x0

    iget-object v4, p0, Lnr;->nQ:Lnq;

    iget v4, v4, Lnq;->nD:I

    div-int/2addr v3, v4

    add-int/lit8 v4, p1, -0x1

    iget-object v5, p0, Lnr;->nQ:Lnq;

    invoke-virtual {v5}, Lnq;->cv()I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lnr;->nQ:Lnq;

    iget v5, v5, Lnq;->nD:I

    div-int v5, v4, v5

    iget-object v6, p0, Lnr;->nQ:Lnq;

    iget v6, v6, Lnq;->nD:I

    rem-int/2addr v4, v6

    mul-int/2addr v4, v3

    add-int/lit8 v4, v4, 0x0

    mul-int/2addr v5, v2

    add-int/2addr v1, v5

    add-int/2addr v3, v4

    add-int/2addr v2, v1

    invoke-virtual {v0, v4, v1, v3, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 746
    invoke-direct {p0, p1}, Lnr;->ai(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lhz;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 747
    iget-object v0, p0, Lnr;->gn:Landroid/graphics/Rect;

    invoke-virtual {p2, v0}, Lhz;->setBoundsInParent(Landroid/graphics/Rect;)V

    .line 748
    const/16 v0, 0x10

    invoke-virtual {p2, v0}, Lhz;->addAction(I)V

    .line 750
    iget-object v0, p0, Lnr;->nQ:Lnq;

    iget v0, v0, Lnq;->nB:I

    if-ne p1, v0, :cond_0

    .line 751
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lhz;->setSelected(Z)V

    .line 754
    :cond_0
    return-void
.end method

.method public final ah(I)V
    .locals 3

    .prologue
    .line 706
    iget-object v0, p0, Lnr;->nQ:Lnq;

    invoke-virtual {p0, v0}, Lnr;->d(Landroid/view/View;)Lih;

    move-result-object v0

    const/16 v1, 0x40

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lih;->performAction(IILandroid/os/Bundle;)Z

    .line 708
    return-void
.end method

.method protected final b(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 731
    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lnr;->nQ:Lnq;

    iget v1, v1, Lnq;->nE:I

    if-gt v0, v1, :cond_0

    .line 732
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 731
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 734
    :cond_0
    return-void
.end method

.method public final cy()V
    .locals 4

    .prologue
    .line 711
    iget v0, p0, Lju;->iV:I

    .line 712
    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 713
    iget-object v1, p0, Lnr;->nQ:Lnq;

    invoke-virtual {p0, v1}, Lnr;->d(Landroid/view/View;)Lih;

    move-result-object v1

    const/16 v2, 0x80

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lih;->performAction(IILandroid/os/Bundle;)Z

    .line 718
    :cond_0
    return-void
.end method

.method protected final d(FF)I
    .locals 1

    .prologue
    .line 722
    iget-object v0, p0, Lnr;->nQ:Lnq;

    invoke-virtual {v0, p1, p2}, Lnq;->g(FF)I

    move-result v0

    .line 723
    if-ltz v0, :cond_0

    .line 726
    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x80000000

    goto :goto_0
.end method

.method protected final h(II)Z
    .locals 1

    .prologue
    .line 759
    packed-switch p2, :pswitch_data_0

    .line 765
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 761
    :pswitch_0
    iget-object v0, p0, Lnr;->nQ:Lnq;

    invoke-static {v0, p1}, Lnq;->a(Lnq;I)V

    .line 762
    const/4 v0, 0x1

    goto :goto_0

    .line 759
    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_0
    .end packed-switch
.end method
