.class public final Ljje;
.super Ljsl;
.source "PG"


# instance fields
.field public eoB:Ljjf;

.field public eoC:Ljiz;

.field public eoD:Ljtd;

.field public eoE:Ljji;

.field public eoF:Ljta;

.field private eoG:Ljjd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 528
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 529
    iput-object v0, p0, Ljje;->eoB:Ljjf;

    iput-object v0, p0, Ljje;->eoC:Ljiz;

    iput-object v0, p0, Ljje;->eoD:Ljtd;

    iput-object v0, p0, Ljje;->eoE:Ljji;

    iput-object v0, p0, Ljje;->eoF:Ljta;

    iput-object v0, p0, Ljje;->eoG:Ljjd;

    iput-object v0, p0, Ljje;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljje;->eCz:I

    .line 530
    return-void
.end method

.method public static ao([B)Ljje;
    .locals 1

    .prologue
    .line 661
    new-instance v0, Ljje;

    invoke-direct {v0}, Ljje;-><init>()V

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Ljje;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 493
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljje;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljje;->eoB:Ljjf;

    if-nez v0, :cond_1

    new-instance v0, Ljjf;

    invoke-direct {v0}, Ljjf;-><init>()V

    iput-object v0, p0, Ljje;->eoB:Ljjf;

    :cond_1
    iget-object v0, p0, Ljje;->eoB:Ljjf;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljje;->eoC:Ljiz;

    if-nez v0, :cond_2

    new-instance v0, Ljiz;

    invoke-direct {v0}, Ljiz;-><init>()V

    iput-object v0, p0, Ljje;->eoC:Ljiz;

    :cond_2
    iget-object v0, p0, Ljje;->eoC:Ljiz;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljje;->eoD:Ljtd;

    if-nez v0, :cond_3

    new-instance v0, Ljtd;

    invoke-direct {v0}, Ljtd;-><init>()V

    iput-object v0, p0, Ljje;->eoD:Ljtd;

    :cond_3
    iget-object v0, p0, Ljje;->eoD:Ljtd;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljje;->eoE:Ljji;

    if-nez v0, :cond_4

    new-instance v0, Ljji;

    invoke-direct {v0}, Ljji;-><init>()V

    iput-object v0, p0, Ljje;->eoE:Ljji;

    :cond_4
    iget-object v0, p0, Ljje;->eoE:Ljji;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljje;->eoF:Ljta;

    if-nez v0, :cond_5

    new-instance v0, Ljta;

    invoke-direct {v0}, Ljta;-><init>()V

    iput-object v0, p0, Ljje;->eoF:Ljta;

    :cond_5
    iget-object v0, p0, Ljje;->eoF:Ljta;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ljje;->eoG:Ljjd;

    if-nez v0, :cond_6

    new-instance v0, Ljjd;

    invoke-direct {v0}, Ljjd;-><init>()V

    iput-object v0, p0, Ljje;->eoG:Ljjd;

    :cond_6
    iget-object v0, p0, Ljje;->eoG:Ljjd;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3ffa -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 547
    iget-object v0, p0, Ljje;->eoB:Ljjf;

    if-eqz v0, :cond_0

    .line 548
    const/4 v0, 0x1

    iget-object v1, p0, Ljje;->eoB:Ljjf;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 550
    :cond_0
    iget-object v0, p0, Ljje;->eoC:Ljiz;

    if-eqz v0, :cond_1

    .line 551
    const/4 v0, 0x2

    iget-object v1, p0, Ljje;->eoC:Ljiz;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 553
    :cond_1
    iget-object v0, p0, Ljje;->eoD:Ljtd;

    if-eqz v0, :cond_2

    .line 554
    const/4 v0, 0x3

    iget-object v1, p0, Ljje;->eoD:Ljtd;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 556
    :cond_2
    iget-object v0, p0, Ljje;->eoE:Ljji;

    if-eqz v0, :cond_3

    .line 557
    const/4 v0, 0x4

    iget-object v1, p0, Ljje;->eoE:Ljji;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 559
    :cond_3
    iget-object v0, p0, Ljje;->eoF:Ljta;

    if-eqz v0, :cond_4

    .line 560
    const/4 v0, 0x5

    iget-object v1, p0, Ljje;->eoF:Ljta;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 562
    :cond_4
    iget-object v0, p0, Ljje;->eoG:Ljjd;

    if-eqz v0, :cond_5

    .line 563
    const/16 v0, 0x7ff

    iget-object v1, p0, Ljje;->eoG:Ljjd;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 565
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 566
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 570
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 571
    iget-object v1, p0, Ljje;->eoB:Ljjf;

    if-eqz v1, :cond_0

    .line 572
    const/4 v1, 0x1

    iget-object v2, p0, Ljje;->eoB:Ljjf;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 575
    :cond_0
    iget-object v1, p0, Ljje;->eoC:Ljiz;

    if-eqz v1, :cond_1

    .line 576
    const/4 v1, 0x2

    iget-object v2, p0, Ljje;->eoC:Ljiz;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 579
    :cond_1
    iget-object v1, p0, Ljje;->eoD:Ljtd;

    if-eqz v1, :cond_2

    .line 580
    const/4 v1, 0x3

    iget-object v2, p0, Ljje;->eoD:Ljtd;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 583
    :cond_2
    iget-object v1, p0, Ljje;->eoE:Ljji;

    if-eqz v1, :cond_3

    .line 584
    const/4 v1, 0x4

    iget-object v2, p0, Ljje;->eoE:Ljji;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 587
    :cond_3
    iget-object v1, p0, Ljje;->eoF:Ljta;

    if-eqz v1, :cond_4

    .line 588
    const/4 v1, 0x5

    iget-object v2, p0, Ljje;->eoF:Ljta;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 591
    :cond_4
    iget-object v1, p0, Ljje;->eoG:Ljjd;

    if-eqz v1, :cond_5

    .line 592
    const/16 v1, 0x7ff

    iget-object v2, p0, Ljje;->eoG:Ljjd;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 595
    :cond_5
    return v0
.end method
