.class public final Lgbj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 511
    new-instance v0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    invoke-direct {v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->jS(I)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v3

    const-class v0, Ljbj;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljbj;

    invoke-virtual {v3, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->a(Ljbj;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->mn(Ljava/lang/String;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v3

    const-class v0, Ljhe;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljhe;

    invoke-virtual {v3, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->a(Ljhe;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->mo(Ljava/lang/String;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v3

    const-class v0, Ljhe;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljhe;

    invoke-virtual {v3, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->b(Ljhe;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->mp(Ljava/lang/String;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->mq(Ljava/lang/String;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v0

    const-class v3, Ljcn;

    invoke-static {p1, v3}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->b(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->ad(Ljava/util/List;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v3

    const-class v0, Lizq;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizq;

    invoke-virtual {v3, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->h(Lizq;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v3

    const-class v0, Landroid/graphics/Point;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    invoke-virtual {v3, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->c(Landroid/graphics/Point;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->jT(I)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->jU(I)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v3

    const-class v0, Landroid/graphics/Point;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    invoke-virtual {v3, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->d(Landroid/graphics/Point;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v3

    const-class v0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    invoke-virtual {v3, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->r(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->fE(Z)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->jV(I)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->fG(Z)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 511
    new-array v0, p1, [Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    return-object v0
.end method
