.class public final Ljph;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dZa:I

.field private dZb:I

.field private dZc:I

.field private dZd:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6173
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 6174
    iput v0, p0, Ljph;->aez:I

    iput v0, p0, Ljph;->dZa:I

    iput v0, p0, Ljph;->dZb:I

    iput v0, p0, Ljph;->dZc:I

    iput v0, p0, Ljph;->dZd:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljph;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljph;->eCz:I

    .line 6175
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 6078
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljph;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    iput v0, p0, Ljph;->dZa:I

    iget v0, p0, Ljph;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljph;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    iput v0, p0, Ljph;->dZb:I

    iget v0, p0, Ljph;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljph;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    iput v0, p0, Ljph;->dZc:I

    iget v0, p0, Ljph;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljph;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    iput v0, p0, Ljph;->dZd:I

    iget v0, p0, Ljph;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljph;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 6191
    iget v0, p0, Ljph;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 6192
    const/4 v0, 0x1

    iget v1, p0, Ljph;->dZa:I

    invoke-virtual {p1, v0, v1}, Ljsj;->br(II)V

    .line 6194
    :cond_0
    iget v0, p0, Ljph;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 6195
    const/4 v0, 0x2

    iget v1, p0, Ljph;->dZb:I

    invoke-virtual {p1, v0, v1}, Ljsj;->br(II)V

    .line 6197
    :cond_1
    iget v0, p0, Ljph;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 6198
    const/4 v0, 0x3

    iget v1, p0, Ljph;->dZc:I

    invoke-virtual {p1, v0, v1}, Ljsj;->br(II)V

    .line 6200
    :cond_2
    iget v0, p0, Ljph;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 6201
    const/4 v0, 0x4

    iget v1, p0, Ljph;->dZd:I

    invoke-virtual {p1, v0, v1}, Ljsj;->br(II)V

    .line 6203
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 6204
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 6208
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 6209
    iget v1, p0, Ljph;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 6210
    const/4 v1, 0x1

    iget v2, p0, Ljph;->dZa:I

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6213
    :cond_0
    iget v1, p0, Ljph;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 6214
    const/4 v1, 0x2

    iget v2, p0, Ljph;->dZb:I

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6217
    :cond_1
    iget v1, p0, Ljph;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 6218
    const/4 v1, 0x3

    iget v2, p0, Ljph;->dZc:I

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6221
    :cond_2
    iget v1, p0, Ljph;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 6222
    const/4 v1, 0x4

    iget v2, p0, Ljph;->dZd:I

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6225
    :cond_3
    return v0
.end method

.method public final re(I)Ljph;
    .locals 1

    .prologue
    .line 6103
    iput p1, p0, Ljph;->dZa:I

    .line 6104
    iget v0, p0, Ljph;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljph;->aez:I

    .line 6105
    return-object p0
.end method

.method public final rf(I)Ljph;
    .locals 1

    .prologue
    .line 6122
    iput p1, p0, Ljph;->dZb:I

    .line 6123
    iget v0, p0, Ljph;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljph;->aez:I

    .line 6124
    return-object p0
.end method

.method public final rg(I)Ljph;
    .locals 1

    .prologue
    .line 6141
    iput p1, p0, Ljph;->dZc:I

    .line 6142
    iget v0, p0, Ljph;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljph;->aez:I

    .line 6143
    return-object p0
.end method

.method public final rh(I)Ljph;
    .locals 1

    .prologue
    .line 6160
    iput p1, p0, Ljph;->dZd:I

    .line 6161
    iget v0, p0, Ljph;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljph;->aez:I

    .line 6162
    return-object p0
.end method
