.class public final Lfts;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 6

    .prologue
    const v5, 0x7f110370

    .line 31
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    .line 32
    iget-object v1, p0, Lfro;->cBc:Lanh;

    .line 33
    iget-object v2, v1, Lanh;->aho:Lanc;

    .line 34
    const v3, 0x7f1101a9

    invoke-virtual {v2}, Lanc;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lfts;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 35
    const v3, 0x7f1102f0

    invoke-virtual {v2}, Lanc;->on()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lfts;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 36
    const v3, 0x7f1100b0

    invoke-virtual {v2}, Lanc;->oo()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lfts;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 37
    iget-object v3, v2, Lanc;->agd:Lapp;

    if-eqz v3, :cond_0

    .line 38
    const v3, 0x7f1102e2

    iget-object v4, v2, Lanc;->agd:Lapp;

    invoke-virtual {v4}, Lapp;->ti()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lfts;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 40
    :cond_0
    iget-object v1, v1, Lanh;->aho:Lanc;

    invoke-virtual {v1}, Lanc;->os()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 41
    invoke-virtual {p0, v5}, Lfts;->jy(I)V

    .line 43
    :cond_1
    invoke-virtual {v2}, Lanc;->oj()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v5, v1}, Lfts;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 44
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 26
    const v0, 0x7f040144

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
