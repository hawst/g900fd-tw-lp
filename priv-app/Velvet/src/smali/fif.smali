.class public final enum Lfif;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum csA:Lfif;

.field public static final enum csB:Lfif;

.field public static final enum csC:Lfif;

.field public static final enum csD:Lfif;

.field public static final enum csE:Lfif;

.field private static final synthetic csF:[Lfif;

.field public static final enum cst:Lfif;

.field public static final enum csu:Lfif;

.field public static final enum csv:Lfif;

.field public static final enum csw:Lfif;

.field public static final enum csx:Lfif;

.field public static final enum csy:Lfif;

.field public static final enum csz:Lfif;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1593
    new-instance v0, Lfif;

    const-string v1, "YES_NO_QUESTION"

    invoke-direct {v0, v1, v3}, Lfif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfif;->cst:Lfif;

    .line 1594
    new-instance v0, Lfif;

    const-string v1, "MULTIPLE_CHOICE_QUESTION"

    invoke-direct {v0, v1, v4}, Lfif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfif;->csu:Lfif;

    .line 1595
    new-instance v0, Lfif;

    const-string v1, "CATEGORY"

    invoke-direct {v0, v1, v5}, Lfif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfif;->csv:Lfif;

    .line 1596
    new-instance v0, Lfif;

    const-string v1, "SECTION_HEADER"

    invoke-direct {v0, v1, v6}, Lfif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfif;->csw:Lfif;

    .line 1597
    new-instance v0, Lfif;

    const-string v1, "INTERESTED_IN_SECTION_HEADER"

    invoke-direct {v0, v1, v7}, Lfif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfif;->csx:Lfif;

    .line 1598
    new-instance v0, Lfif;

    const-string v1, "ADD_TEAM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lfif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfif;->csy:Lfif;

    .line 1599
    new-instance v0, Lfif;

    const-string v1, "ADD_STOCK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lfif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfif;->csz:Lfif;

    .line 1600
    new-instance v0, Lfif;

    const-string v1, "ICEBREAKER_SECTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lfif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfif;->csA:Lfif;

    .line 1601
    new-instance v0, Lfif;

    const-string v1, "INTEREST"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lfif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfif;->csB:Lfif;

    .line 1602
    new-instance v0, Lfif;

    const-string v1, "MULTIPLE_SELECT_QUESTION"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lfif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfif;->csC:Lfif;

    .line 1603
    new-instance v0, Lfif;

    const-string v1, "TV_PROVIDER"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lfif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfif;->csD:Lfif;

    .line 1604
    new-instance v0, Lfif;

    const-string v1, "DYNAMIC_MULTIPLE_CHOICE_QUESTION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lfif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfif;->csE:Lfif;

    .line 1592
    const/16 v0, 0xc

    new-array v0, v0, [Lfif;

    sget-object v1, Lfif;->cst:Lfif;

    aput-object v1, v0, v3

    sget-object v1, Lfif;->csu:Lfif;

    aput-object v1, v0, v4

    sget-object v1, Lfif;->csv:Lfif;

    aput-object v1, v0, v5

    sget-object v1, Lfif;->csw:Lfif;

    aput-object v1, v0, v6

    sget-object v1, Lfif;->csx:Lfif;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lfif;->csy:Lfif;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lfif;->csz:Lfif;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lfif;->csA:Lfif;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lfif;->csB:Lfif;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lfif;->csC:Lfif;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lfif;->csD:Lfif;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lfif;->csE:Lfif;

    aput-object v2, v0, v1

    sput-object v0, Lfif;->csF:[Lfif;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1592
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lfif;
    .locals 1

    .prologue
    .line 1592
    const-class v0, Lfif;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfif;

    return-object v0
.end method

.method public static values()[Lfif;
    .locals 1

    .prologue
    .line 1592
    sget-object v0, Lfif;->csF:[Lfif;

    invoke-virtual {v0}, [Lfif;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfif;

    return-object v0
.end method
