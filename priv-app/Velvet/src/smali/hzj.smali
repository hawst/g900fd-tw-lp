.class public final Lhzj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhzs;


# instance fields
.field private mQuery:Lcom/google/android/shared/search/Query;

.field private final mSearchUrlHelper:Lcpn;


# direct methods
.method public constructor <init>(Lcpn;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lhzj;->mSearchUrlHelper:Lcpn;

    .line 24
    return-void
.end method

.method static synthetic a(Lhzj;Lcom/google/android/shared/search/Query;Ljava/lang/String;)Ljwj;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 15
    iget-object v0, p0, Lhzj;->mSearchUrlHelper:Lcpn;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcpn;->b(Lcom/google/android/shared/search/Query;Ljava/lang/String;Z)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v4

    new-instance v5, Ljwj;

    invoke-direct {v5}, Ljwj;-><init>()V

    invoke-virtual {v4}, Lcom/google/android/search/shared/api/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    new-array v6, v1, [Ljyk;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v3

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v1, Ljyk;

    invoke-direct {v1}, Ljyk;-><init>()V

    aput-object v1, v6, v2

    aget-object v8, v6, v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v8, v1}, Ljyk;->zP(Ljava/lang/String;)Ljyk;

    aget-object v1, v6, v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljyk;->zQ(Ljava/lang/String;)Ljyk;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    iput-object v6, v5, Ljwj;->eJc:[Ljyk;

    invoke-virtual {v4}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcpn;->n(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    new-array v2, v1, [Ljyi;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v1, Ljyi;

    invoke-direct {v1}, Ljyi;-><init>()V

    aput-object v1, v2, v3

    aget-object v6, v2, v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljyi;->zN(Ljava/lang/String;)Ljyi;

    aget-object v1, v2, v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljyi;->zO(Ljava/lang/String;)Ljyi;

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    iput-object v2, v5, Ljwj;->eJe:[Ljyi;

    return-object v5
.end method


# virtual methods
.method public final a(Lhzt;)V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method public final a(Lhzt;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 45
    return-void
.end method

.method public final bd(Lcom/google/android/shared/search/Query;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lhzj;->mQuery:Lcom/google/android/shared/search/Query;

    .line 28
    return-void
.end method

.method public final oQ(Ljava/lang/String;)Ljava/util/concurrent/Callable;
    .locals 3

    .prologue
    .line 32
    iget-object v0, p0, Lhzj;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    .line 33
    new-instance v1, Lhzk;

    const-string v2, "ClockworkParamsBuilderTask"

    invoke-direct {v1, p0, v2, v0, p1}, Lhzk;-><init>(Lhzj;Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/String;)V

    return-object v1
.end method
