.class public final Ljzc;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eMW:I

.field private eMX:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 843
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 844
    iput v0, p0, Ljzc;->aez:I

    iput v0, p0, Ljzc;->eMW:I

    iput v0, p0, Ljzc;->eMX:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljzc;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljzc;->eCz:I

    .line 845
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 786
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljzc;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzc;->eMW:I

    iget v0, p0, Ljzc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzc;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzc;->eMX:I

    iget v0, p0, Ljzc;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzc;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 859
    iget v0, p0, Ljzc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 860
    const/4 v0, 0x1

    iget v1, p0, Ljzc;->eMW:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 862
    :cond_0
    iget v0, p0, Ljzc;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 863
    const/4 v0, 0x2

    iget v1, p0, Ljzc;->eMX:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 865
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 866
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 870
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 871
    iget v1, p0, Ljzc;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 872
    const/4 v1, 0x1

    iget v2, p0, Ljzc;->eMW:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 875
    :cond_0
    iget v1, p0, Ljzc;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 876
    const/4 v1, 0x2

    iget v2, p0, Ljzc;->eMX:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 879
    :cond_1
    return v0
.end method
