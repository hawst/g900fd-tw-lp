.class public final Ljoo;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ewF:[Ljoo;


# instance fields
.field private aez:I

.field private ewG:Ljava/lang/String;

.field private ewH:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 883
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 884
    const/4 v0, 0x0

    iput v0, p0, Ljoo;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljoo;->ewG:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljoo;->ewH:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljoo;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljoo;->eCz:I

    .line 885
    return-void
.end method

.method public static brs()[Ljoo;
    .locals 2

    .prologue
    .line 826
    sget-object v0, Ljoo;->ewF:[Ljoo;

    if-nez v0, :cond_1

    .line 827
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 829
    :try_start_0
    sget-object v0, Ljoo;->ewF:[Ljoo;

    if-nez v0, :cond_0

    .line 830
    const/4 v0, 0x0

    new-array v0, v0, [Ljoo;

    sput-object v0, Ljoo;->ewF:[Ljoo;

    .line 832
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 834
    :cond_1
    sget-object v0, Ljoo;->ewF:[Ljoo;

    return-object v0

    .line 832
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 820
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljoo;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljoo;->ewG:Ljava/lang/String;

    iget v0, p0, Ljoo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljoo;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljoo;->ewH:Ljava/lang/String;

    iget v0, p0, Ljoo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljoo;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 899
    iget v0, p0, Ljoo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 900
    const/4 v0, 0x1

    iget-object v1, p0, Ljoo;->ewG:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 902
    :cond_0
    iget v0, p0, Ljoo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 903
    const/4 v0, 0x2

    iget-object v1, p0, Ljoo;->ewH:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 905
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 906
    return-void
.end method

.method public final amp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 864
    iget-object v0, p0, Ljoo;->ewH:Ljava/lang/String;

    return-object v0
.end method

.method public final getCanonicalName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 842
    iget-object v0, p0, Ljoo;->ewG:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 910
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 911
    iget v1, p0, Ljoo;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 912
    const/4 v1, 0x1

    iget-object v2, p0, Ljoo;->ewG:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 915
    :cond_0
    iget v1, p0, Ljoo;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 916
    const/4 v1, 0x2

    iget-object v2, p0, Ljoo;->ewH:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 919
    :cond_1
    return v0
.end method

.method public final xL(Ljava/lang/String;)Ljoo;
    .locals 1

    .prologue
    .line 845
    if-nez p1, :cond_0

    .line 846
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 848
    :cond_0
    iput-object p1, p0, Ljoo;->ewG:Ljava/lang/String;

    .line 849
    iget v0, p0, Ljoo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljoo;->aez:I

    .line 850
    return-object p0
.end method

.method public final xM(Ljava/lang/String;)Ljoo;
    .locals 1

    .prologue
    .line 867
    if-nez p1, :cond_0

    .line 868
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 870
    :cond_0
    iput-object p1, p0, Ljoo;->ewH:Ljava/lang/String;

    .line 871
    iget v0, p0, Ljoo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljoo;->aez:I

    .line 872
    return-object p0
.end method
