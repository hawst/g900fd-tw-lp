.class public final Ljks;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eqf:[Ljks;


# instance fields
.field private aYk:Ljava/lang/String;

.field private aez:I

.field private agq:Ljava/lang/String;

.field public eqg:[Ljlc;

.field public eqh:[Ljlb;

.field public eqi:[Ljld;

.field private eqj:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2602
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2603
    const/4 v0, 0x0

    iput v0, p0, Ljks;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljks;->agq:Ljava/lang/String;

    invoke-static {}, Ljlc;->boL()[Ljlc;

    move-result-object v0

    iput-object v0, p0, Ljks;->eqg:[Ljlc;

    invoke-static {}, Ljlb;->boJ()[Ljlb;

    move-result-object v0

    iput-object v0, p0, Ljks;->eqh:[Ljlb;

    invoke-static {}, Ljld;->boO()[Ljld;

    move-result-object v0

    iput-object v0, p0, Ljks;->eqi:[Ljld;

    const-string v0, ""

    iput-object v0, p0, Ljks;->aYk:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljks;->eqj:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljks;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljks;->eCz:I

    .line 2604
    return-void
.end method

.method public static boq()[Ljks;
    .locals 2

    .prologue
    .line 2514
    sget-object v0, Ljks;->eqf:[Ljks;

    if-nez v0, :cond_1

    .line 2515
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 2517
    :try_start_0
    sget-object v0, Ljks;->eqf:[Ljks;

    if-nez v0, :cond_0

    .line 2518
    const/4 v0, 0x0

    new-array v0, v0, [Ljks;

    sput-object v0, Ljks;->eqf:[Ljks;

    .line 2520
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2522
    :cond_1
    sget-object v0, Ljks;->eqf:[Ljks;

    return-object v0

    .line 2520
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2508
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljks;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljks;->agq:Ljava/lang/String;

    iget v0, p0, Ljks;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljks;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljks;->eqg:[Ljlc;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljlc;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljks;->eqg:[Ljlc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljlc;

    invoke-direct {v3}, Ljlc;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljks;->eqg:[Ljlc;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljlc;

    invoke-direct {v3}, Ljlc;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljks;->eqg:[Ljlc;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljks;->eqh:[Ljlb;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljlb;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljks;->eqh:[Ljlb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Ljlb;

    invoke-direct {v3}, Ljlb;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljks;->eqh:[Ljlb;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Ljlb;

    invoke-direct {v3}, Ljlb;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljks;->eqh:[Ljlb;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljks;->eqj:Ljava/lang/String;

    iget v0, p0, Ljks;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljks;->aez:I

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljks;->eqi:[Ljld;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ljld;

    if-eqz v0, :cond_7

    iget-object v3, p0, Ljks;->eqi:[Ljld;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Ljld;

    invoke-direct {v3}, Ljld;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Ljks;->eqi:[Ljld;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Ljld;

    invoke-direct {v3}, Ljld;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljks;->eqi:[Ljld;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljks;->aYk:Ljava/lang/String;

    iget v0, p0, Ljks;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljks;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2622
    iget v0, p0, Ljks;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2623
    const/4 v0, 0x1

    iget-object v2, p0, Ljks;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 2625
    :cond_0
    iget-object v0, p0, Ljks;->eqg:[Ljlc;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljks;->eqg:[Ljlc;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 2626
    :goto_0
    iget-object v2, p0, Ljks;->eqg:[Ljlc;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 2627
    iget-object v2, p0, Ljks;->eqg:[Ljlc;

    aget-object v2, v2, v0

    .line 2628
    if-eqz v2, :cond_1

    .line 2629
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 2626
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2633
    :cond_2
    iget-object v0, p0, Ljks;->eqh:[Ljlb;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljks;->eqh:[Ljlb;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 2634
    :goto_1
    iget-object v2, p0, Ljks;->eqh:[Ljlb;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 2635
    iget-object v2, p0, Ljks;->eqh:[Ljlb;

    aget-object v2, v2, v0

    .line 2636
    if-eqz v2, :cond_3

    .line 2637
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 2634
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2641
    :cond_4
    iget v0, p0, Ljks;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    .line 2642
    const/4 v0, 0x4

    iget-object v2, p0, Ljks;->eqj:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 2644
    :cond_5
    iget-object v0, p0, Ljks;->eqi:[Ljld;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ljks;->eqi:[Ljld;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 2645
    :goto_2
    iget-object v0, p0, Ljks;->eqi:[Ljld;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 2646
    iget-object v0, p0, Ljks;->eqi:[Ljld;

    aget-object v0, v0, v1

    .line 2647
    if-eqz v0, :cond_6

    .line 2648
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 2645
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2652
    :cond_7
    iget v0, p0, Ljks;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_8

    .line 2653
    const/4 v0, 0x6

    iget-object v1, p0, Ljks;->aYk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2655
    :cond_8
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2656
    return-void
.end method

.method public final bor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2583
    iget-object v0, p0, Ljks;->eqj:Ljava/lang/String;

    return-object v0
.end method

.method public final bos()Z
    .locals 1

    .prologue
    .line 2594
    iget v0, p0, Ljks;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2530
    iget-object v0, p0, Ljks;->agq:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2660
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2661
    iget v2, p0, Ljks;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 2662
    const/4 v2, 0x1

    iget-object v3, p0, Ljks;->agq:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2665
    :cond_0
    iget-object v2, p0, Ljks;->eqg:[Ljlc;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljks;->eqg:[Ljlc;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 2666
    :goto_0
    iget-object v3, p0, Ljks;->eqg:[Ljlc;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 2667
    iget-object v3, p0, Ljks;->eqg:[Ljlc;

    aget-object v3, v3, v0

    .line 2668
    if-eqz v3, :cond_1

    .line 2669
    const/4 v4, 0x2

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2666
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 2674
    :cond_3
    iget-object v2, p0, Ljks;->eqh:[Ljlb;

    if-eqz v2, :cond_6

    iget-object v2, p0, Ljks;->eqh:[Ljlb;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v0

    move v0, v1

    .line 2675
    :goto_1
    iget-object v3, p0, Ljks;->eqh:[Ljlb;

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 2676
    iget-object v3, p0, Ljks;->eqh:[Ljlb;

    aget-object v3, v3, v0

    .line 2677
    if-eqz v3, :cond_4

    .line 2678
    const/4 v4, 0x3

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2675
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    move v0, v2

    .line 2683
    :cond_6
    iget v2, p0, Ljks;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_7

    .line 2684
    const/4 v2, 0x4

    iget-object v3, p0, Ljks;->eqj:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2687
    :cond_7
    iget-object v2, p0, Ljks;->eqi:[Ljld;

    if-eqz v2, :cond_9

    iget-object v2, p0, Ljks;->eqi:[Ljld;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 2688
    :goto_2
    iget-object v2, p0, Ljks;->eqi:[Ljld;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 2689
    iget-object v2, p0, Ljks;->eqi:[Ljld;

    aget-object v2, v2, v1

    .line 2690
    if-eqz v2, :cond_8

    .line 2691
    const/4 v3, 0x5

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2688
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2696
    :cond_9
    iget v1, p0, Ljks;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_a

    .line 2697
    const/4 v1, 0x6

    iget-object v2, p0, Ljks;->aYk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2700
    :cond_a
    return v0
.end method

.method public final oK()Z
    .locals 1

    .prologue
    .line 2541
    iget v0, p0, Ljks;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final wq(Ljava/lang/String;)Ljks;
    .locals 1

    .prologue
    .line 2533
    if-nez p1, :cond_0

    .line 2534
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2536
    :cond_0
    iput-object p1, p0, Ljks;->agq:Ljava/lang/String;

    .line 2537
    iget v0, p0, Ljks;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljks;->aez:I

    .line 2538
    return-object p0
.end method

.method public final wr(Ljava/lang/String;)Ljks;
    .locals 1

    .prologue
    .line 2586
    if-nez p1, :cond_0

    .line 2587
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2589
    :cond_0
    iput-object p1, p0, Ljks;->eqj:Ljava/lang/String;

    .line 2590
    iget v0, p0, Ljks;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljks;->aez:I

    .line 2591
    return-object p0
.end method
