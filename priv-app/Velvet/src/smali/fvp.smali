.class public final Lfvp;
.super Lfuz;
.source "PG"


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 12

    .prologue
    .line 37
    iget-object v4, p0, Lfuz;->mEntry:Lizj;

    .line 38
    iget-object v0, v4, Lizj;->dSh:Ljas;

    .line 40
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 41
    new-instance v1, Lanu;

    invoke-direct {v1}, Lanu;-><init>()V

    invoke-virtual {v0}, Ljas;->pb()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljas;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lanu;->aX(Ljava/lang/String;)Lanu;

    :cond_0
    invoke-virtual {v0}, Ljas;->hasText()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljas;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lanu;->aY(Ljava/lang/String;)Lanu;

    :cond_1
    invoke-virtual {v0}, Ljas;->pT()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Ljas;->pS()I

    move-result v2

    invoke-virtual {v1, v2}, Lanu;->cv(I)Lanu;

    :cond_2
    invoke-virtual {v0}, Ljas;->pU()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Ljas;->getStyle()I

    move-result v2

    invoke-virtual {v1, v2}, Lanu;->cw(I)Lanu;

    :cond_3
    iget-object v2, v0, Ljas;->dWT:[Ljcn;

    array-length v2, v2

    if-lez v2, :cond_6

    iget-object v2, v0, Ljas;->dWT:[Ljcn;

    iput-object v2, v1, Lanu;->aiF:[Ljcn;

    :cond_4
    :goto_0
    new-instance v2, Lanh;

    invoke-direct {v2}, Lanh;-><init>()V

    const/16 v3, 0x27

    invoke-virtual {v2, v3}, Lanh;->cm(I)Lanh;

    iput-object v1, v2, Lanh;->ahc:Lanu;

    iput-object v4, v2, Lanh;->ahu:Lizj;

    iget-object v1, v0, Ljas;->dXh:Lixx;

    if-eqz v1, :cond_5

    new-instance v1, Lfsa;

    const/4 v3, 0x3

    invoke-direct {v1, v3}, Lfsa;-><init>(I)V

    iget-object v3, v0, Ljas;->dXh:Lixx;

    invoke-virtual {v1, v3}, Lfsa;->b(Lixx;)Lani;

    move-result-object v1

    iput-object v1, v2, Lanh;->ahs:Lani;

    :cond_5
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    iget-object v6, v0, Ljas;->dXg:[Lixx;

    array-length v7, v6

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v7, :cond_13

    aget-object v8, v6, v2

    .line 44
    invoke-virtual {v8}, Lixx;->rn()Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x0

    .line 45
    :goto_2
    invoke-static {v5, v0}, Lfvp;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 43
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 41
    :cond_6
    invoke-virtual {v0}, Ljas;->ok()Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v2, Ljcn;

    invoke-direct {v2}, Ljcn;-><init>()V

    invoke-virtual {v0}, Ljas;->oj()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljcn;->tk(Ljava/lang/String;)Ljcn;

    invoke-virtual {v0}, Ljas;->oq()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v0}, Ljas;->or()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v0}, Ljas;->gi()I

    move-result v3

    invoke-virtual {v2, v3}, Ljcn;->oF(I)Ljcn;

    invoke-virtual {v0}, Ljas;->gj()I

    move-result v3

    invoke-virtual {v2, v3}, Ljcn;->oG(I)Ljcn;

    :cond_7
    const/4 v3, 0x1

    new-array v3, v3, [Ljcn;

    const/4 v6, 0x0

    aput-object v2, v3, v6

    iput-object v3, v1, Lanu;->aiF:[Ljcn;

    goto :goto_0

    .line 44
    :cond_8
    new-instance v9, Lfsa;

    const/16 v0, 0x81

    invoke-direct {v9, v0}, Lfsa;-><init>(I)V

    invoke-virtual {v8}, Lixx;->baY()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {v8}, Lixx;->getAction()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v8}, Lixx;->baX()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {v8}, Lixx;->getUri()Ljava/lang/String;

    move-result-object v1

    :goto_4
    const/4 v3, 0x0

    invoke-virtual {v8}, Lixx;->bbb()Z

    move-result v10

    if-nez v10, :cond_9

    const-string v10, "com.google.android.apps.maps.LOCATION_SETTINGS"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    :cond_9
    const/4 v0, 0x7

    invoke-virtual {v9, v0}, Lfsa;->jC(I)Lani;

    move-result-object v0

    :goto_5
    if-eqz v0, :cond_f

    invoke-static {v0, v8}, Lfsa;->a(Lani;Lixx;)V

    move-object v1, v0

    :goto_6
    if-nez v1, :cond_10

    const/4 v0, 0x0

    goto :goto_2

    :cond_a
    const/4 v0, 0x0

    goto :goto_3

    :cond_b
    const/4 v1, 0x0

    goto :goto_4

    :cond_c
    const-string v10, "android.settings.WIFI_SETTINGS"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    const/16 v0, 0x8

    invoke-virtual {v9, v0}, Lfsa;->jC(I)Lani;

    move-result-object v0

    goto :goto_5

    :cond_d
    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0x12

    if-lt v10, v11, :cond_e

    const-string v10, "android.net.wifi.action.REQUEST_SCAN_ALWAYS_AVAILABLE"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/16 v0, 0x9

    invoke-virtual {v9, v0}, Lfsa;->jC(I)Lani;

    move-result-object v0

    goto :goto_5

    :cond_e
    const-string v0, "https://www.google.com/history/settings"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/16 v0, 0xa

    invoke-virtual {v9, v0}, Lfsa;->jC(I)Lani;

    move-result-object v0

    goto :goto_5

    :cond_f
    invoke-virtual {v9, v8}, Lfsa;->b(Lixx;)Lani;

    move-result-object v0

    move-object v1, v0

    goto :goto_6

    :cond_10
    invoke-virtual {v1}, Lani;->getType()I

    move-result v0

    const/4 v3, 0x7

    if-ne v0, v3, :cond_11

    invoke-virtual {v8}, Lixx;->bbg()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-virtual {v8}, Lixx;->oY()I

    move-result v0

    invoke-virtual {v1, v0}, Lani;->cq(I)Lani;

    :cond_11
    iget-object v0, v1, Lani;->ahC:Lanb;

    if-nez v0, :cond_12

    new-instance v0, Lanb;

    invoke-direct {v0}, Lanb;-><init>()V

    iput-object v0, v1, Lani;->ahC:Lanb;

    iget-object v0, v1, Lani;->ahC:Lanb;

    const v3, 0x7f0200ed

    invoke-virtual {v0, v3}, Lanb;->ch(I)Lanb;

    :cond_12
    new-instance v3, Laoj;

    invoke-direct {v3}, Laoj;-><init>()V

    invoke-virtual {v8}, Lixx;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    const/4 v8, 0x3

    invoke-virtual {v0, v8}, Lanh;->cm(I)Lanh;

    iput-object v3, v0, Lanh;->agz:Laoj;

    iput-object v4, v0, Lanh;->ahu:Lizj;

    iput-object v1, v0, Lanh;->ahs:Lani;

    goto/16 :goto_2

    .line 48
    :cond_13
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 49
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 50
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lang;->aS(Z)Lang;

    .line 51
    return-object v1

    :cond_14
    move-object v0, v3

    goto/16 :goto_5
.end method
