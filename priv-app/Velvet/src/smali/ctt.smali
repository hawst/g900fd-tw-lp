.class abstract Lctt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private biE:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final SU()V
    .locals 1

    .prologue
    .line 770
    iget-object v0, p0, Lctt;->biE:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 771
    invoke-virtual {p0}, Lctt;->ST()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lctt;->biE:Ljava/lang/Object;

    .line 773
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract ST()Ljava/lang/Object;
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 777
    invoke-direct {p0}, Lctt;->SU()V

    .line 778
    iget-object v0, p0, Lctt;->biE:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 783
    invoke-direct {p0}, Lctt;->SU()V

    .line 784
    iget-object v0, p0, Lctt;->biE:Ljava/lang/Object;

    .line 785
    if-nez v0, :cond_0

    .line 786
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 788
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lctt;->biE:Ljava/lang/Object;

    .line 789
    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 794
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
