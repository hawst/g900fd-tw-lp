.class public final Lamn;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afc:Z

.field private afd:Z

.field private afe:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 954
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 955
    iput v0, p0, Lamn;->aez:I

    iput-boolean v0, p0, Lamn;->afc:Z

    iput-boolean v0, p0, Lamn;->afd:Z

    iput-boolean v0, p0, Lamn;->afe:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lamn;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lamn;->eCz:I

    .line 956
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 878
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lamn;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lamn;->afc:Z

    iget v0, p0, Lamn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamn;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lamn;->afd:Z

    iget v0, p0, Lamn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamn;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lamn;->afe:Z

    iget v0, p0, Lamn;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lamn;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 971
    iget v0, p0, Lamn;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 972
    const/4 v0, 0x1

    iget-boolean v1, p0, Lamn;->afc:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 974
    :cond_0
    iget v0, p0, Lamn;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 975
    const/4 v0, 0x2

    iget-boolean v1, p0, Lamn;->afd:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 977
    :cond_1
    iget v0, p0, Lamn;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 978
    const/4 v0, 0x3

    iget-boolean v1, p0, Lamn;->afe:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 980
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 981
    return-void
.end method

.method public final aG(Z)Lamn;
    .locals 1

    .prologue
    .line 903
    iput-boolean p1, p0, Lamn;->afc:Z

    .line 904
    iget v0, p0, Lamn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamn;->aez:I

    .line 905
    return-object p0
.end method

.method public final aH(Z)Lamn;
    .locals 1

    .prologue
    .line 922
    iput-boolean p1, p0, Lamn;->afd:Z

    .line 923
    iget v0, p0, Lamn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamn;->aez:I

    .line 924
    return-object p0
.end method

.method public final aI(Z)Lamn;
    .locals 1

    .prologue
    .line 941
    iput-boolean p1, p0, Lamn;->afe:Z

    .line 942
    iget v0, p0, Lamn;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lamn;->aez:I

    .line 943
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 985
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 986
    iget v1, p0, Lamn;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 987
    const/4 v1, 0x1

    iget-boolean v2, p0, Lamn;->afc:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 990
    :cond_0
    iget v1, p0, Lamn;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 991
    const/4 v1, 0x2

    iget-boolean v2, p0, Lamn;->afd:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 994
    :cond_1
    iget v1, p0, Lamn;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 995
    const/4 v1, 0x3

    iget-boolean v2, p0, Lamn;->afe:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 998
    :cond_2
    return v0
.end method

.method public final ne()Z
    .locals 1

    .prologue
    .line 900
    iget-boolean v0, p0, Lamn;->afc:Z

    return v0
.end method

.method public final nf()Z
    .locals 1

    .prologue
    .line 919
    iget-boolean v0, p0, Lamn;->afd:Z

    return v0
.end method

.method public final ng()Z
    .locals 1

    .prologue
    .line 938
    iget-boolean v0, p0, Lamn;->afe:Z

    return v0
.end method
