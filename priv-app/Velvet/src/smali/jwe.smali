.class public final Ljwe;
.super Ljsl;
.source "PG"


# static fields
.field public static final eIT:Ljsm;

.field public static final eIU:Ljsm;


# instance fields
.field private aez:I

.field private eGk:I

.field private eIV:I

.field private eIW:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0xb

    .line 15
    const-class v0, Ljwe;

    const v1, 0x23c762

    invoke-static {v2, v0, v1}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljwe;->eIT:Ljsm;

    .line 25
    const-class v0, Ljwe;

    const v1, 0x1f002892

    invoke-static {v2, v0, v1}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljwe;->eIU:Ljsm;

    .line 31
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 95
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 96
    iput v0, p0, Ljwe;->aez:I

    iput v0, p0, Ljwe;->eIV:I

    const/4 v0, 0x0

    iput v0, p0, Ljwe;->eIW:F

    const/4 v0, 0x1

    iput v0, p0, Ljwe;->eGk:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljwe;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljwe;->eCz:I

    .line 97
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljwe;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljwe;->eIW:F

    iget v0, p0, Ljwe;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwe;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljwe;->eIV:I

    iget v0, p0, Ljwe;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwe;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljwe;->eGk:I

    iget v0, p0, Ljwe;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljwe;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x15 -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 112
    iget v0, p0, Ljwe;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 113
    const/4 v0, 0x2

    iget v1, p0, Ljwe;->eIW:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 115
    :cond_0
    iget v0, p0, Ljwe;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 116
    const/4 v0, 0x3

    iget v1, p0, Ljwe;->eIV:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 118
    :cond_1
    iget v0, p0, Ljwe;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 119
    const/4 v0, 0x4

    iget v1, p0, Ljwe;->eGk:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 121
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 122
    return-void
.end method

.method public final ac(F)Ljwe;
    .locals 1

    .prologue
    .line 63
    iput p1, p0, Ljwe;->eIW:F

    .line 64
    iget v0, p0, Ljwe;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwe;->aez:I

    .line 65
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 126
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 127
    iget v1, p0, Ljwe;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    .line 128
    const/4 v1, 0x2

    iget v2, p0, Ljwe;->eIW:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 131
    :cond_0
    iget v1, p0, Ljwe;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 132
    const/4 v1, 0x3

    iget v2, p0, Ljwe;->eIV:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    :cond_1
    iget v1, p0, Ljwe;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 136
    const/4 v1, 0x4

    iget v2, p0, Ljwe;->eGk:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    :cond_2
    return v0
.end method

.method public final sF(I)Ljwe;
    .locals 1

    .prologue
    .line 44
    iput p1, p0, Ljwe;->eIV:I

    .line 45
    iget v0, p0, Ljwe;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwe;->aez:I

    .line 46
    return-object p0
.end method
