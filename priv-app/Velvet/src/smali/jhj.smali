.class public final Ljhj;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dPK:I

.field private elO:I

.field private elP:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27502
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 27503
    iput v1, p0, Ljhj;->aez:I

    const/4 v0, 0x3

    iput v0, p0, Ljhj;->elO:I

    iput v1, p0, Ljhj;->elP:I

    iput v1, p0, Ljhj;->dPK:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljhj;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljhj;->eCz:I

    .line 27504
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 27421
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljhj;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljhj;->elO:I

    iget v0, p0, Ljhj;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhj;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljhj;->elP:I

    iget v0, p0, Ljhj;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhj;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljhj;->dPK:I

    iget v0, p0, Ljhj;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljhj;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 27519
    iget v0, p0, Ljhj;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 27520
    const/4 v0, 0x1

    iget v1, p0, Ljhj;->elO:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 27522
    :cond_0
    iget v0, p0, Ljhj;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 27523
    const/4 v0, 0x2

    iget v1, p0, Ljhj;->elP:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 27525
    :cond_1
    iget v0, p0, Ljhj;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 27526
    const/4 v0, 0x3

    iget v1, p0, Ljhj;->dPK:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 27528
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 27529
    return-void
.end method

.method public final aEz()I
    .locals 1

    .prologue
    .line 27486
    iget v0, p0, Ljhj;->dPK:I

    return v0
.end method

.method public final blM()I
    .locals 1

    .prologue
    .line 27448
    iget v0, p0, Ljhj;->elO:I

    return v0
.end method

.method public final blN()I
    .locals 1

    .prologue
    .line 27467
    iget v0, p0, Ljhj;->elP:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 27533
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 27534
    iget v1, p0, Ljhj;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 27535
    const/4 v1, 0x1

    iget v2, p0, Ljhj;->elO:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 27538
    :cond_0
    iget v1, p0, Ljhj;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 27539
    const/4 v1, 0x2

    iget v2, p0, Ljhj;->elP:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 27542
    :cond_1
    iget v1, p0, Ljhj;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 27543
    const/4 v1, 0x3

    iget v2, p0, Ljhj;->dPK:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 27546
    :cond_2
    return v0
.end method

.method public final pS(I)Ljhj;
    .locals 1

    .prologue
    .line 27451
    iput p1, p0, Ljhj;->elO:I

    .line 27452
    iget v0, p0, Ljhj;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhj;->aez:I

    .line 27453
    return-object p0
.end method

.method public final pT(I)Ljhj;
    .locals 1

    .prologue
    .line 27470
    iput p1, p0, Ljhj;->elP:I

    .line 27471
    iget v0, p0, Ljhj;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhj;->aez:I

    .line 27472
    return-object p0
.end method

.method public final pU(I)Ljhj;
    .locals 1

    .prologue
    .line 27489
    iput p1, p0, Ljhj;->dPK:I

    .line 27490
    iget v0, p0, Ljhj;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljhj;->aez:I

    .line 27491
    return-object p0
.end method
