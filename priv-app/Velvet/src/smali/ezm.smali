.class public final Lezm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lezi;


# instance fields
.field private final aUh:Lcxs;

.field private final mClock:Lemp;

.field private final mEntry:Lizj;

.field private final mNotificationStore:Lffp;

.field private final mRoute:Liyg;


# direct methods
.method public constructor <init>(Liyg;Lcxs;Lizj;Lffp;Lemp;)V
    .locals 0
    .param p1    # Liyg;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lezm;->mRoute:Liyg;

    .line 37
    iput-object p2, p0, Lezm;->aUh:Lcxs;

    .line 38
    iput-object p3, p0, Lezm;->mEntry:Lizj;

    .line 39
    iput-object p4, p0, Lezm;->mNotificationStore:Lffp;

    .line 40
    iput-object p5, p0, Lezm;->mClock:Lemp;

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lezg;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v3, 0x2

    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 46
    iget-object v0, p2, Lezg;->cmx:Lezn;

    iget-object v1, p0, Lezm;->mRoute:Liyg;

    invoke-virtual {v0, p1, v1}, Lezn;->a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Liyg;)V

    .line 50
    invoke-static {p1}, Lfph;->k(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    iget-object v0, p0, Lezm;->aUh:Lcxs;

    invoke-virtual {v0}, Lcxs;->TH()Lcxi;

    move-result-object v5

    invoke-virtual {v5, v3, v6}, Lcxi;->c(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v5, v3, v0}, Lcxi;->c(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    move v1, v0

    :goto_1
    invoke-virtual {v5, v2, v6}, Lcxi;->c(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v2

    :goto_2
    if-eqz v0, :cond_5

    invoke-virtual {v5}, Lcxi;->TC()Z

    move-result v0

    move v3, v0

    :goto_3
    invoke-virtual {v5, v7, v6}, Lcxi;->c(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    move v0, v2

    :goto_4
    if-eqz v0, :cond_7

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v5, v7, v0}, Lcxi;->c(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    :goto_5
    new-instance v0, Lfph;

    invoke-direct {v0, v1, v3, v2}, Lfph;-><init>(ZZZ)V

    invoke-virtual {v0, p1}, Lfph;->j(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)V

    .line 55
    :cond_0
    iget-object v0, p0, Lezm;->mRoute:Liyg;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lezm;->mRoute:Liyg;

    iget-object v0, v0, Liyg;->dPL:Liyh;

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Lezm;->mNotificationStore:Lffp;

    iget-object v1, p0, Lezm;->mEntry:Lizj;

    iget-object v2, p0, Lezm;->mRoute:Liyg;

    iget-object v3, p0, Lezm;->mClock:Lemp;

    invoke-static {p1, v0, v1, v2, v3}, Lezp;->a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lffp;Lizj;Liyg;Lemp;)V

    .line 59
    :cond_1
    return-void

    :cond_2
    move v0, v4

    .line 51
    goto :goto_0

    :cond_3
    const-string v0, "LocationRenderingContextAdapter"

    const-string v1, "Incomplete configuration for trafficCardSharing.shareLocation"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    goto :goto_1

    :cond_4
    move v0, v4

    goto :goto_2

    :cond_5
    const-string v0, "LocationRenderingContextAdapter"

    const-string v3, "Incomplete configuration for trafficCardSharing.shareCommute"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    goto :goto_3

    :cond_6
    move v0, v4

    goto :goto_4

    :cond_7
    const-string v0, "LocationRenderingContextAdapter"

    const-string v4, "Incomplete configuration for trafficCardSharing.userPromptedToShareCommute"

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method
