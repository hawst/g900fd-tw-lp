.class public Lhrz;
.super Landroid/app/ListFragment;
.source "PG"


# instance fields
.field bhl:Ljava/util/concurrent/Executor;

.field private bus:I

.field doQ:Lhrk;

.field private final dpL:Lhrs;

.field dpM:Ljava/util/ArrayList;

.field private dpN:Lhsd;

.field mConfiguration:Ljze;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 41
    new-instance v0, Lhsa;

    invoke-direct {v0, p0}, Lhsa;-><init>(Lhrz;)V

    iput-object v0, p0, Lhrz;->dpL:Lhrs;

    .line 147
    return-void
.end method

.method private B(Ljava/util/Collection;)V
    .locals 3

    .prologue
    .line 138
    iget-object v0, p0, Lhrz;->dpM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 139
    invoke-static {p1}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 140
    iget-object v1, p0, Lhrz;->mConfiguration:Ljze;

    invoke-static {v1}, Lgkb;->a(Ljze;)Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 141
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljzp;

    .line 142
    iget-object v2, p0, Lhrz;->dpM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 144
    :cond_0
    return-void
.end method


# virtual methods
.method final aRN()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lhrz;->dpM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 128
    iget v0, p0, Lhrz;->bus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 129
    iget-object v0, p0, Lhrz;->doQ:Lhrk;

    invoke-virtual {v0}, Lhrk;->aGp()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, v0}, Lhrz;->B(Ljava/util/Collection;)V

    .line 134
    :goto_0
    iget-object v0, p0, Lhrz;->dpN:Lhsd;

    invoke-virtual {v0}, Lhsd;->notifyDataSetChanged()V

    .line 135
    return-void

    .line 131
    :cond_0
    iget-object v0, p0, Lhrz;->doQ:Lhrk;

    invoke-virtual {v0}, Lhrk;->aRK()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, v0}, Lhrz;->B(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 105
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 106
    iget-object v0, p0, Lhrz;->dpN:Lhsd;

    invoke-virtual {p0, v0}, Lhrz;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 107
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 76
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 77
    invoke-virtual {p0}, Lhrz;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lhrz;->bus:I

    .line 79
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Lhhq;->aOX()Lhrk;

    move-result-object v1

    iput-object v1, p0, Lhrz;->doQ:Lhrk;

    .line 82
    iget-object v1, v0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aER()Ljze;

    move-result-object v1

    iput-object v1, p0, Lhrz;->mConfiguration:Ljze;

    .line 83
    iget-object v0, v0, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->aus()Leqo;

    move-result-object v0

    iput-object v0, p0, Lhrz;->bhl:Ljava/util/concurrent/Executor;

    .line 85
    new-instance v0, Lhsd;

    invoke-direct {v0, p0}, Lhsd;-><init>(Lhrz;)V

    iput-object v0, p0, Lhrz;->dpN:Lhsd;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhrz;->dpM:Ljava/util/ArrayList;

    .line 87
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 92
    const v0, 0x7f0401bd

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 93
    const v0, 0x1020004

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 94
    iget v2, p0, Lhrz;->bus:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 95
    const v2, 0x7f0a08ee

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 100
    :goto_0
    return-object v1

    .line 97
    :cond_0
    const v2, 0x7f0a08ed

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lhrz;->doQ:Lhrk;

    iget-object v1, p0, Lhrz;->dpL:Lhrs;

    invoke-virtual {v0, v1}, Lhrk;->b(Lhrs;)V

    .line 123
    invoke-super {p0}, Landroid/app/ListFragment;->onPause()V

    .line 124
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    .line 112
    iget-object v0, p0, Lhrz;->doQ:Lhrk;

    iget-object v1, p0, Lhrz;->dpL:Lhrs;

    invoke-virtual {v0, v1}, Lhrk;->a(Lhrs;)V

    .line 113
    iget-object v0, p0, Lhrz;->doQ:Lhrk;

    invoke-virtual {v0}, Lhrk;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    iget-object v0, p0, Lhrz;->doQ:Lhrk;

    invoke-virtual {v0}, Lhrk;->cV()V

    .line 118
    :goto_0
    return-void

    .line 116
    :cond_0
    invoke-virtual {p0}, Lhrz;->aRN()V

    goto :goto_0
.end method
