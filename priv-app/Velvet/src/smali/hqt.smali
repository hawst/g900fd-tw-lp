.class public Lhqt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgiq;


# static fields
.field private static final dot:[I

.field private static final dou:[Ljava/lang/String;


# instance fields
.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x4

    .line 34
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lhqt;->dot:[I

    .line 45
    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "ep_acoustic_model"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "endpointer_dictation.config"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "endpointer_voicesearch.config"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "metadata"

    aput-object v2, v0, v1

    sput-object v0, Lhqt;->dou:[Ljava/lang/String;

    return-void

    .line 34
    nop

    :array_0
    .array-data 4
        0x7f080008
        0x7f080006
        0x7f080007
        0x7f08000a
    .end array-data
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lhqt;->mResources:Landroid/content/res/Resources;

    .line 56
    return-void
.end method


# virtual methods
.method protected a(Ligi;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-interface {p1}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 75
    new-instance v2, Ljava/io/File;

    const-string v3, "en-US"

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 77
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    const-string v0, "VS.EPModelCopier"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to create model dir: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 92
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 83
    :goto_1
    :try_start_0
    sget-object v3, Lhqt;->dot:[I

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 84
    iget-object v3, p0, Lhqt;->mResources:Landroid/content/res/Resources;

    sget-object v4, Lhqt;->dot:[I

    aget v4, v4, v0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v3

    .line 85
    new-instance v4, Ljava/io/FileOutputStream;

    new-instance v5, Ljava/io/File;

    sget-object v6, Lhqt;->dou:[Ljava/lang/String;

    aget-object v6, v6, v0

    invoke-direct {v5, v2, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 87
    invoke-static {v3, v4}, Liso;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 89
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    const-string v2, "VS.EPModelCopier"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error copying EP models: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 92
    goto :goto_0
.end method

.method public final a(Ligi;Lgix;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 62
    const-string v1, "en-US"

    sget-object v2, Lgjo;->cML:Lgjo;

    invoke-virtual {p2, v1, v2}, Lgix;->a(Ljava/lang/String;Lgjo;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lgjo;->cMM:Lgjo;

    invoke-virtual {p2, v1, v2}, Lgix;->a(Ljava/lang/String;Lgjo;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    .line 69
    :goto_1
    return v0

    :cond_0
    move v1, v0

    .line 62
    goto :goto_0

    .line 69
    :cond_1
    invoke-virtual {p0, p1}, Lhqt;->a(Ligi;)Z

    move-result v0

    goto :goto_1
.end method
