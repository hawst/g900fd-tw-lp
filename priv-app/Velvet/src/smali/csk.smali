.class public final Lcsk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lglc;


# instance fields
.field private final aGx:Ljava/lang/String;

.field private anH:Lcsm;

.field private final bhN:Lcrz;


# direct methods
.method public constructor <init>(Lcrz;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcsk;->bhN:Lcrz;

    .line 32
    iput-object p2, p0, Lcsk;->aGx:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Lger;Lggg;Lgnj;)V
    .locals 13

    .prologue
    .line 38
    invoke-virtual/range {p3 .. p3}, Lgnj;->aHG()Ljava/lang/String;

    move-result-object v6

    .line 39
    iget-object v0, p0, Lcsk;->bhN:Lcrz;

    invoke-interface {v0, v6}, Lcrz;->iE(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Leic;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not intitialized for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Leic;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lggg;->b(Leiq;)V

    .line 76
    :goto_0
    return-void

    .line 44
    :cond_0
    iget-object v0, p0, Lcsk;->bhN:Lcrz;

    invoke-interface {v0, v6}, Lcrz;->iF(Ljava/lang/String;)[B

    move-result-object v0

    .line 45
    if-nez v0, :cond_1

    .line 49
    const-string v0, "HotwordRecognitionEngn"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempted hotword recognition with missing model for: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 50
    new-instance v0, Leic;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No model available for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Leic;-><init>(Ljava/lang/String;Z)V

    invoke-interface {p2, v0}, Lggg;->b(Leiq;)V

    goto :goto_0

    .line 55
    :cond_1
    :try_start_0
    invoke-static {}, Leoh;->auT()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_2

    .line 65
    invoke-virtual/range {p3 .. p3}, Lgnj;->aHC()Lgmx;

    move-result-object v0

    invoke-virtual {v0}, Lgmx;->getSamplingRate()I

    move-result v4

    .line 67
    :try_start_1
    invoke-interface {p1}, Lger;->df()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual/range {p3 .. p3}, Lgnj;->asQ()I

    move-result v7

    invoke-virtual/range {p3 .. p3}, Lgnj;->aHM()F

    move-result v8

    invoke-virtual/range {p3 .. p3}, Lgnj;->aHN()[F

    move-result-object v9

    invoke-virtual/range {p3 .. p3}, Lgnj;->HL()[F

    move-result-object v10

    invoke-virtual/range {p3 .. p3}, Lgnj;->HM()[F

    move-result-object v11

    invoke-virtual {p0}, Lcsk;->close()V

    const-string v0, "HotwordRecognitionRunner"

    invoke-static {v0}, Lemv;->lg(Ljava/lang/String;)Lemw;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    :try_start_2
    new-instance v0, Lcsm;

    iget-object v5, p0, Lcsk;->bhN:Lcrz;

    iget-object v12, p0, Lcsk;->aGx:Ljava/lang/String;

    move-object v2, p2

    invoke-direct/range {v0 .. v12}, Lcsm;-><init>(Ljava/io/InputStream;Lggg;Ljava/util/concurrent/ExecutorService;ILcrz;Ljava/lang/String;IF[F[F[FLjava/lang/String;)V

    iput-object v0, p0, Lcsk;->anH:Lcsm;

    iget-object v0, p0, Lcsk;->anH:Lcsm;

    invoke-virtual {v0}, Lcsm;->start()V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v1, "HotwordRecognitionEngn"

    const-string v2, "Error creating HotwordRecognitionRunner %s"

    invoke-static {v1, v2, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 71
    :catch_1
    move-exception v0

    .line 72
    new-instance v1, Leic;

    const-string v2, "Unable to create input stream"

    invoke-direct {v1, v2, v0}, Leic;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {p2, v1}, Lggg;->b(Leiq;)V

    goto/16 :goto_0

    .line 56
    :catch_2
    move-exception v0

    .line 57
    const-string v1, "HotwordRecognitionEngn"

    const-string v2, "Failed to load hotword library"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 59
    const v1, 0xd007b0

    invoke-static {v1}, Lhwt;->lx(I)V

    .line 60
    new-instance v1, Leic;

    const-string v2, "Failed to load the hotword library"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v0, v3}, Leic;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    invoke-interface {p2, v1}, Lggg;->b(Leiq;)V

    goto/16 :goto_0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcsk;->anH:Lcsm;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcsk;->anH:Lcsm;

    invoke-virtual {v0}, Lcsm;->close()V

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcsk;->anH:Lcsm;

    .line 122
    :cond_0
    return-void
.end method
