.class public final Lbwa;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/search/queries/QueryCall$b;Landroid/os/Parcel;I)V
    .locals 4

    const/4 v3, 0x0

    const/16 v0, 0x4f45

    invoke-static {p1, v0}, Lbju;->n(Landroid/os/Parcel;I)I

    move-result v0

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/gms/search/queries/QueryCall$b;->auB:I

    invoke-static {p1, v1, v2}, Lbju;->c(Landroid/os/Parcel;II)V

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/search/queries/QueryCall$b;->axs:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Lbju;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/search/queries/QueryCall$b;->packageName:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Lbju;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/search/queries/QueryCall$b;->aLK:[Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Lbju;->a(Landroid/os/Parcel;I[Ljava/lang/String;Z)V

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/search/queries/QueryCall$b;->aLI:I

    invoke-static {p1, v1, v2}, Lbju;->c(Landroid/os/Parcel;II)V

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/search/queries/QueryCall$b;->aHj:I

    invoke-static {p1, v1, v2}, Lbju;->c(Landroid/os/Parcel;II)V

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/search/queries/QueryCall$b;->aLD:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    invoke-static {p1, v1, v2, p2, v3}, Lbju;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    invoke-static {p1, v0}, Lbju;->o(Landroid/os/Parcel;I)V

    return-void
.end method

.method public static aw(Landroid/os/Parcel;)Lcom/google/android/gms/search/queries/QueryCall$b;
    .locals 10

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {p0}, Lbjs;->x(Landroid/os/Parcel;)I

    move-result v8

    move v5, v6

    move-object v4, v7

    move-object v3, v7

    move-object v2, v7

    move v1, v6

    :goto_0
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-ge v0, v8, :cond_0

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const v9, 0xffff

    and-int/2addr v9, v0

    sparse-switch v9, :sswitch_data_0

    invoke-static {p0, v0}, Lbjs;->b(Landroid/os/Parcel;I)V

    goto :goto_0

    :sswitch_0
    invoke-static {p0, v0}, Lbjs;->d(Landroid/os/Parcel;I)I

    move-result v1

    goto :goto_0

    :sswitch_1
    invoke-static {p0, v0}, Lbjs;->h(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :sswitch_2
    invoke-static {p0, v0}, Lbjs;->h(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :sswitch_3
    invoke-static {p0, v0}, Lbjs;->l(Landroid/os/Parcel;I)[Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :sswitch_4
    invoke-static {p0, v0}, Lbjs;->d(Landroid/os/Parcel;I)I

    move-result v5

    goto :goto_0

    :sswitch_5
    invoke-static {p0, v0}, Lbjs;->d(Landroid/os/Parcel;I)I

    move-result v6

    goto :goto_0

    :sswitch_6
    sget-object v7, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:Lbep;

    invoke-static {p0, v0, v7}, Lbjs;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;

    move-object v7, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-eq v0, v8, :cond_1

    new-instance v0, Lbjt;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Overread allowed size end="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lbjt;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v0

    :cond_1
    new-instance v0, Lcom/google/android/gms/search/queries/QueryCall$b;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/search/queries/QueryCall$b;-><init>(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)V

    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x3e8 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lbwa;->aw(Landroid/os/Parcel;)Lcom/google/android/gms/search/queries/QueryCall$b;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/search/queries/QueryCall$b;

    return-object v0
.end method
