.class public final Ljqc;
.super Ljsl;
.source "PG"


# static fields
.field public static final eyi:Ljsm;


# instance fields
.field private aez:I

.field private egp:Ljava/lang/String;

.field private eyj:I

.field private eyk:I

.field private eyl:I

.field private eym:I

.field private eyn:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 6137
    const/16 v0, 0xb

    const-class v1, Ljqc;

    const v2, 0x20856d72

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljqc;->eyi:Ljsm;

    .line 6143
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6267
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 6268
    iput v0, p0, Ljqc;->aez:I

    iput v0, p0, Ljqc;->eyj:I

    iput v0, p0, Ljqc;->eyk:I

    iput v0, p0, Ljqc;->eyl:I

    iput v0, p0, Ljqc;->eym:I

    iput v0, p0, Ljqc;->eyn:I

    const-string v0, ""

    iput-object v0, p0, Ljqc;->egp:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljqc;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqc;->eCz:I

    .line 6269
    return-void
.end method


# virtual methods
.method public final St()Z
    .locals 1

    .prologue
    .line 6259
    iget v0, p0, Ljqc;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 6130
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqc;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqc;->eyj:I

    iget v0, p0, Ljqc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqc;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqc;->eyk:I

    iget v0, p0, Ljqc;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqc;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqc;->eyl:I

    iget v0, p0, Ljqc;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljqc;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqc;->eym:I

    iget v0, p0, Ljqc;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljqc;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqc;->eyn:I

    iget v0, p0, Ljqc;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljqc;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqc;->egp:Ljava/lang/String;

    iget v0, p0, Ljqc;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljqc;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x42 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 6287
    iget v0, p0, Ljqc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 6288
    const/4 v0, 0x1

    iget v1, p0, Ljqc;->eyj:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 6290
    :cond_0
    iget v0, p0, Ljqc;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 6291
    const/4 v0, 0x2

    iget v1, p0, Ljqc;->eyk:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 6293
    :cond_1
    iget v0, p0, Ljqc;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 6294
    const/4 v0, 0x3

    iget v1, p0, Ljqc;->eyl:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 6296
    :cond_2
    iget v0, p0, Ljqc;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 6297
    const/4 v0, 0x4

    iget v1, p0, Ljqc;->eym:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 6299
    :cond_3
    iget v0, p0, Ljqc;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 6300
    const/4 v0, 0x5

    iget v1, p0, Ljqc;->eyn:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 6302
    :cond_4
    iget v0, p0, Ljqc;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 6303
    const/16 v0, 0x8

    iget-object v1, p0, Ljqc;->egp:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6305
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 6306
    return-void
.end method

.method public final abx()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6248
    iget-object v0, p0, Ljqc;->egp:Ljava/lang/String;

    return-object v0
.end method

.method public final bsa()I
    .locals 1

    .prologue
    .line 6153
    iget v0, p0, Ljqc;->eyj:I

    return v0
.end method

.method public final bsb()Z
    .locals 1

    .prologue
    .line 6161
    iget v0, p0, Ljqc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bsc()I
    .locals 1

    .prologue
    .line 6172
    iget v0, p0, Ljqc;->eyk:I

    return v0
.end method

.method public final bsd()Z
    .locals 1

    .prologue
    .line 6180
    iget v0, p0, Ljqc;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bse()I
    .locals 1

    .prologue
    .line 6191
    iget v0, p0, Ljqc;->eyl:I

    return v0
.end method

.method public final bsf()Z
    .locals 1

    .prologue
    .line 6199
    iget v0, p0, Ljqc;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bsg()I
    .locals 1

    .prologue
    .line 6229
    iget v0, p0, Ljqc;->eyn:I

    return v0
.end method

.method public final bsh()Z
    .locals 1

    .prologue
    .line 6237
    iget v0, p0, Ljqc;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 6310
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 6311
    iget v1, p0, Ljqc;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 6312
    const/4 v1, 0x1

    iget v2, p0, Ljqc;->eyj:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6315
    :cond_0
    iget v1, p0, Ljqc;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 6316
    const/4 v1, 0x2

    iget v2, p0, Ljqc;->eyk:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6319
    :cond_1
    iget v1, p0, Ljqc;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 6320
    const/4 v1, 0x3

    iget v2, p0, Ljqc;->eyl:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6323
    :cond_2
    iget v1, p0, Ljqc;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 6324
    const/4 v1, 0x4

    iget v2, p0, Ljqc;->eym:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6327
    :cond_3
    iget v1, p0, Ljqc;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 6328
    const/4 v1, 0x5

    iget v2, p0, Ljqc;->eyn:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6331
    :cond_4
    iget v1, p0, Ljqc;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 6332
    const/16 v1, 0x8

    iget-object v2, p0, Ljqc;->egp:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6335
    :cond_5
    return v0
.end method

.method public final rm(I)Ljqc;
    .locals 1

    .prologue
    .line 6156
    iput p1, p0, Ljqc;->eyj:I

    .line 6157
    iget v0, p0, Ljqc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqc;->aez:I

    .line 6158
    return-object p0
.end method

.method public final rn(I)Ljqc;
    .locals 1

    .prologue
    .line 6175
    iput p1, p0, Ljqc;->eyk:I

    .line 6176
    iget v0, p0, Ljqc;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqc;->aez:I

    .line 6177
    return-object p0
.end method

.method public final ro(I)Ljqc;
    .locals 1

    .prologue
    .line 6194
    iput p1, p0, Ljqc;->eyl:I

    .line 6195
    iget v0, p0, Ljqc;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljqc;->aez:I

    .line 6196
    return-object p0
.end method

.method public final yc(Ljava/lang/String;)Ljqc;
    .locals 1

    .prologue
    .line 6251
    if-nez p1, :cond_0

    .line 6252
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6254
    :cond_0
    iput-object p1, p0, Ljqc;->egp:Ljava/lang/String;

    .line 6255
    iget v0, p0, Ljqc;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljqc;->aez:I

    .line 6256
    return-object p0
.end method
