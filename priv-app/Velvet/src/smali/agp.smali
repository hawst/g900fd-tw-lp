.class public final Lagp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2426
    iput-object p1, p0, Lagp;->view:Landroid/view/View;

    .line 2427
    return-void
.end method

.method public static ah(Landroid/view/View;)V
    .locals 3

    .prologue
    const v2, 0x3c23d70a    # 0.01f

    .line 2437
    invoke-static {}, Lcom/android/launcher3/Workspace;->lz()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    .line 2438
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result v1

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v0, :cond_2

    .line 2439
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2444
    :cond_0
    :goto_1
    return-void

    .line 2437
    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    .line 2440
    :cond_2
    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2442
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2448
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 2452
    iget-object v0, p0, Lagp;->view:Landroid/view/View;

    invoke-static {v0}, Lagp;->ah(Landroid/view/View;)V

    .line 2453
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2457
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 2462
    iget-object v0, p0, Lagp;->view:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2463
    return-void
.end method

.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 1

    .prologue
    .line 2431
    iget-object v0, p0, Lagp;->view:Landroid/view/View;

    invoke-static {v0}, Lagp;->ah(Landroid/view/View;)V

    .line 2432
    return-void
.end method
