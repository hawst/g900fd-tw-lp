.class public final Leqt;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static G(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 1
    .param p0    # Ljava/lang/CharSequence;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 237
    if-eqz p0, :cond_0

    instance-of v0, p0, Ljava/lang/String;

    if-nez v0, :cond_0

    instance-of v0, p0, Landroid/text/SpannedString;

    if-eqz v0, :cond_1

    .line 240
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-static {p0}, Landroid/text/SpannedString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannedString;

    move-result-object p0

    goto :goto_0
.end method

.method public static H(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 303
    instance-of v0, p0, Landroid/text/Spanned;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 304
    check-cast v0, Landroid/text/Spanned;

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const-class v3, Landroid/text/Annotation;

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/Annotation;

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 306
    invoke-virtual {v3}, Landroid/text/Annotation;->getKey()Ljava/lang/String;

    move-result-object v4

    const-string v5, "recognizerLanguage"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 307
    invoke-virtual {v3}, Landroid/text/Annotation;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 312
    :goto_1
    return-object v0

    .line 304
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 312
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/text/Spanned;ILjava/lang/Class;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 81
    invoke-interface {p0}, Landroid/text/Spanned;->length()I

    move-result v3

    .line 83
    if-lez p1, :cond_0

    if-lt p1, v3, :cond_1

    .line 97
    :cond_0
    return-object v2

    .line 87
    :cond_1
    const v1, 0x7fffffff

    .line 88
    invoke-interface {p0, v0, v3, p2}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v4

    array-length v5, v4

    move v3, v0

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v1, v4, v3

    .line 89
    invoke-interface {p0, v1}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    if-lt p1, v6, :cond_2

    invoke-interface {p0, v1}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    if-gt p1, v6, :cond_2

    .line 91
    invoke-interface {p0, v1}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    invoke-interface {p0, v1}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    sub-int/2addr v6, v7

    if-ge v6, v0, :cond_2

    .line 93
    invoke-interface {p0, v1}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    invoke-interface {p0, v1}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v2

    sub-int/2addr v0, v2

    .line 88
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    goto :goto_0

    :cond_2
    move-object v1, v2

    goto :goto_1
.end method

.method public static a(Landroid/text/Spannable;Ljava/lang/Class;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 285
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v1

    invoke-interface {p0, v0, v1, p1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 286
    invoke-interface {p0, v3}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 285
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 288
    :cond_0
    return-void
.end method

.method public static a(Landroid/text/Spanned;Landroid/text/Spannable;Ljava/lang/Class;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 106
    invoke-static {p0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 107
    invoke-interface {p0}, Landroid/text/Spanned;->length()I

    move-result v2

    .line 108
    invoke-interface {p1, v1, v2, p2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 109
    invoke-interface {p1, v5}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 108
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move-object v0, p0

    move-object v3, p2

    move-object v4, p1

    move v5, v1

    .line 111
    invoke-static/range {v0 .. v5}, Leqt;->copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V

    .line 112
    return-void
.end method

.method public static a(Ljava/util/Collection;Landroid/os/Parcel;I)V
    .locals 2
    .param p0    # Ljava/util/Collection;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 194
    if-nez p0, :cond_1

    .line 195
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 202
    :cond_0
    return-void

    .line 197
    :cond_1
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 198
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 199
    invoke-static {v0, p1, p2}, Leqt;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static a(Landroid/text/Spanned;Ljava/lang/Class;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 291
    invoke-interface {p0}, Landroid/text/Spanned;->length()I

    move-result v1

    invoke-interface {p0, v0, v1, p1}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    array-length v1, v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static aC(Landroid/os/Parcel;)Ljava/lang/CharSequence;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 161
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 162
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 163
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 164
    if-nez v3, :cond_0

    if-nez v4, :cond_0

    .line 165
    invoke-static {v0}, Leqt;->G(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 182
    :goto_0
    return-object v0

    .line 167
    :cond_0
    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v5

    move v2, v1

    .line 168
    :goto_1
    if-ge v2, v3, :cond_1

    .line 169
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 170
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .line 171
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 172
    sget-object v0, Lcom/google/android/shared/util/CorrectionSpan;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/CorrectionSpan;

    .line 173
    invoke-virtual {v5, v0, v6, v7, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 168
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 175
    :cond_1
    :goto_2
    if-ge v1, v4, :cond_2

    .line 176
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 177
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 178
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 179
    sget-object v0, Lcom/google/android/shared/util/VoiceCorrectionSpan;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/VoiceCorrectionSpan;

    .line 180
    invoke-virtual {v5, v0, v2, v3, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 175
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 182
    :cond_2
    invoke-static {v5}, Landroid/text/SpannedString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannedString;

    move-result-object v0

    goto :goto_0
.end method

.method public static aD(Landroid/os/Parcel;)Lijj;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 216
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 217
    const/4 v0, -0x1

    if-ne v1, v0, :cond_0

    .line 218
    const/4 v0, 0x0

    .line 227
    :goto_0
    return-object v0

    .line 220
    :cond_0
    invoke-static {}, Lijj;->aWX()Lijk;

    move-result-object v2

    .line 221
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    .line 222
    invoke-static {p0}, Leqt;->aC(Landroid/os/Parcel;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 223
    if-eqz v3, :cond_1

    .line 224
    invoke-virtual {v2, v3}, Lijk;->bt(Ljava/lang/Object;)Lijk;

    .line 221
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 227
    :cond_2
    iget-object v0, v2, Lijk;->IA:Ljava/util/ArrayList;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Ljava/lang/CharSequence;Ljava/lang/String;)Landroid/text/SpannedString;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 296
    invoke-static {p0}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 297
    new-instance v1, Landroid/text/Annotation;

    const-string v2, "recognizerLanguage"

    invoke-direct {v1, v2, p1}, Landroid/text/Annotation;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-interface {v0, v1, v3, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 299
    new-instance v1, Landroid/text/SpannedString;

    invoke-direct {v1, v0}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    return-object v1
.end method

.method public static c(Landroid/text/Spanned;)Landroid/text/Spannable;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 273
    new-instance v1, Landroid/text/SpannableString;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 274
    invoke-interface {p0}, Landroid/text/Spanned;->length()I

    move-result v2

    const-class v3, Ljava/lang/Object;

    invoke-interface {p0, v0, v2, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 275
    instance-of v5, v4, Landroid/text/style/SuggestionSpan;

    if-nez v5, :cond_0

    instance-of v5, v4, Lcom/google/android/shared/util/CorrectionSpan;

    if-nez v5, :cond_0

    instance-of v5, v4, Lcom/google/android/shared/util/VoiceCorrectionSpan;

    if-eqz v5, :cond_1

    .line 277
    :cond_0
    invoke-interface {p0, v4}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    invoke-interface {p0, v4}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    invoke-interface {p0, v4}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    move-result v7

    invoke-interface {v1, v4, v5, v6, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 274
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 281
    :cond_2
    return-object v1
.end method

.method public static c(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    .locals 9
    .param p0    # Ljava/lang/CharSequence;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/CharSequence;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 40
    invoke-static {p0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v1

    .line 44
    :cond_1
    instance-of v0, p0, Landroid/text/Spanned;

    if-eqz v0, :cond_4

    .line 45
    instance-of v0, p1, Landroid/text/Spanned;

    if-eqz v0, :cond_3

    .line 46
    check-cast p0, Landroid/text/Spanned;

    check-cast p1, Landroid/text/Spanned;

    invoke-interface {p0}, Landroid/text/Spanned;->length()I

    move-result v0

    const-class v3, Ljava/lang/Object;

    invoke-interface {p0, v1, v0, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1}, Landroid/text/Spanned;->length()I

    move-result v0

    const-class v4, Ljava/lang/Object;

    invoke-interface {p1, v1, v0, v4}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v4

    array-length v0, v3

    array-length v5, v4

    if-ne v0, v5, :cond_0

    move v0, v1

    :goto_1
    array-length v5, v3

    if-ge v0, v5, :cond_2

    aget-object v5, v3, v0

    aget-object v6, v4, v0

    invoke-static {v5, v6}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {p0, v5}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    invoke-interface {p1, v6}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v8

    if-ne v7, v8, :cond_0

    invoke-interface {p0, v5}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    invoke-interface {p1, v6}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    if-ne v5, v6, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_0

    .line 48
    :cond_3
    check-cast p0, Landroid/text/Spanned;

    const-class v0, Ljava/lang/Object;

    invoke-static {p0, v0}, Leqt;->a(Landroid/text/Spanned;Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v2

    goto :goto_0

    .line 50
    :cond_4
    instance-of v0, p1, Landroid/text/Spanned;

    if-eqz v0, :cond_5

    .line 51
    check-cast p1, Landroid/text/Spanned;

    const-class v0, Ljava/lang/Object;

    invoke-static {p1, v0}, Leqt;->a(Landroid/text/Spanned;Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v2

    goto :goto_0

    :cond_5
    move v1, v2

    .line 53
    goto :goto_0
.end method

.method public static copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V
    .locals 7

    .prologue
    .line 253
    invoke-interface {p4}, Landroid/text/Spannable;->length()I

    move-result v0

    sub-int/2addr v0, p5

    .line 254
    sub-int v1, p2, p1

    if-ge v0, v1, :cond_0

    .line 255
    add-int p2, p1, v0

    .line 257
    :cond_0
    invoke-interface {p0, p1, p2, p3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    .line 258
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 259
    invoke-interface {p0, v3}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    .line 260
    invoke-interface {p0, v3}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    .line 261
    if-lt v4, p1, :cond_1

    if-gt v5, p2, :cond_1

    .line 262
    add-int/2addr v4, p5

    sub-int/2addr v4, p1

    add-int/2addr v5, p5

    sub-int/2addr v5, p1

    invoke-interface {p0, v3}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    move-result v6

    invoke-interface {p4, v3, v4, v5, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 258
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 267
    :cond_2
    return-void
.end method

.method public static writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 122
    invoke-static {p0, p1, p2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 124
    instance-of v0, p0, Landroid/text/Spanned;

    if-eqz v0, :cond_1

    .line 125
    check-cast p0, Landroid/text/Spanned;

    invoke-interface {p0}, Landroid/text/Spanned;->length()I

    move-result v0

    const-class v1, Lcom/google/android/shared/util/CorrectionSpan;

    invoke-interface {p0, v2, v0, v1}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/shared/util/CorrectionSpan;

    invoke-interface {p0}, Landroid/text/Spanned;->length()I

    move-result v1

    const-class v3, Lcom/google/android/shared/util/VoiceCorrectionSpan;

    invoke-interface {p0, v2, v1, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/shared/util/VoiceCorrectionSpan;

    array-length v3, v0

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    array-length v3, v1

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    array-length v4, v0

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v0, v3

    invoke-interface {p0, v5}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p0, v5}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p0, v5}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v5, p1, p2}, Lcom/google/android/shared/util/CorrectionSpan;->writeToParcel(Landroid/os/Parcel;I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    array-length v3, v1

    move v0, v2

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v2, v1, v0

    invoke-interface {p0, v2}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p0, v2}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p0, v2}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p1, p2}, Lcom/google/android/shared/util/VoiceCorrectionSpan;->writeToParcel(Landroid/os/Parcel;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 127
    :cond_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 128
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 130
    :cond_2
    return-void
.end method
