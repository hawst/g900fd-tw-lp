.class public final Lgld;
.super Lgle;
.source "PG"


# instance fields
.field private final cOz:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lgle;-><init>()V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgld;->cOz:Ljava/util/ArrayList;

    .line 31
    return-void
.end method

.method public static a(DDD)D
    .locals 8

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    .line 106
    add-double v0, p0, v2

    add-double/2addr v2, p2

    div-double/2addr v0, v2

    .line 110
    const-wide v2, 0x420cf7c580000000L    # 1.5552E10

    invoke-static {p4, p5, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    .line 113
    const-wide v4, 0x41c9bfcc00000000L    # 8.64E8

    div-double/2addr v2, v4

    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    .line 116
    add-double/2addr v0, v2

    mul-double/2addr v0, v6

    .line 118
    return-wide v0
.end method


# virtual methods
.method public final a(Lglf;)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lgld;->cOz:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    return-void
.end method

.method protected final aHc()Ljava/util/Collection;
    .locals 9

    .prologue
    .line 57
    iget-object v0, p0, Lgld;->cOz:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgld;->cOz:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglf;

    iget v7, v0, Lglf;->cOF:I

    iget-object v0, p0, Lgld;->cOz:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lglf;

    iget v0, v6, Lglf;->cOF:I

    int-to-double v0, v0

    int-to-double v2, v7

    iget-wide v4, v6, Lglf;->cOG:J

    long-to-double v4, v4

    invoke-static/range {v0 .. v5}, Lgld;->a(DDD)D

    move-result-wide v0

    iput-wide v0, v6, Lglf;->cOH:D

    goto :goto_0

    .line 59
    :cond_0
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v1

    .line 60
    iget-object v0, p0, Lgld;->cOz:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglf;

    .line 61
    invoke-virtual {v0, v1}, Lglf;->q(Ljava/util/Map;)V

    goto :goto_1

    .line 64
    :cond_1
    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Ljava/lang/StringBuilder;)V
    .locals 1

    .prologue
    .line 40
    const-string v0, "$TARGET = $VOID;\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    const-string v0, "$VOICE_DIALING = $DIGIT_DIALING;\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    return-void
.end method

.method protected final c(Ljava/lang/StringBuilder;)V
    .locals 1

    .prologue
    .line 46
    const-string v0, "$TARGET = $CONTACT;\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    const-string v0, "$VOICE_DIALING = $CONTACT_AND_DIGIT_DIALING;\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    return-void
.end method

.method protected final d(Ljava/lang/StringBuilder;)V
    .locals 1

    .prologue
    .line 52
    const-string v0, "$CONTACT = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    return-void
.end method
