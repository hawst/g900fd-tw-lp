.class public final Lamz;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile afS:[Lamz;


# instance fields
.field private aez:I

.field private afT:J

.field private afU:J

.field public entry:Lizj;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 586
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 587
    const/4 v0, 0x0

    iput v0, p0, Lamz;->aez:I

    iput-object v1, p0, Lamz;->entry:Lizj;

    iput-wide v2, p0, Lamz;->afT:J

    iput-wide v2, p0, Lamz;->afU:J

    iput-object v1, p0, Lamz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lamz;->eCz:I

    .line 588
    return-void
.end method

.method public static oe()[Lamz;
    .locals 2

    .prologue
    .line 532
    sget-object v0, Lamz;->afS:[Lamz;

    if-nez v0, :cond_1

    .line 533
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 535
    :try_start_0
    sget-object v0, Lamz;->afS:[Lamz;

    if-nez v0, :cond_0

    .line 536
    const/4 v0, 0x0

    new-array v0, v0, [Lamz;

    sput-object v0, Lamz;->afS:[Lamz;

    .line 538
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 540
    :cond_1
    sget-object v0, Lamz;->afS:[Lamz;

    return-object v0

    .line 538
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final B(J)Lamz;
    .locals 1

    .prologue
    .line 554
    iput-wide p1, p0, Lamz;->afT:J

    .line 555
    iget v0, p0, Lamz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamz;->aez:I

    .line 556
    return-object p0
.end method

.method public final C(J)Lamz;
    .locals 1

    .prologue
    .line 573
    iput-wide p1, p0, Lamz;->afU:J

    .line 574
    iget v0, p0, Lamz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamz;->aez:I

    .line 575
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 526
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lamz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lamz;->entry:Lizj;

    if-nez v0, :cond_1

    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    iput-object v0, p0, Lamz;->entry:Lizj;

    :cond_1
    iget-object v0, p0, Lamz;->entry:Lizj;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lamz;->afT:J

    iget v0, p0, Lamz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamz;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lamz;->afU:J

    iget v0, p0, Lamz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamz;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 603
    iget-object v0, p0, Lamz;->entry:Lizj;

    if-eqz v0, :cond_0

    .line 604
    const/4 v0, 0x1

    iget-object v1, p0, Lamz;->entry:Lizj;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 606
    :cond_0
    iget v0, p0, Lamz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 607
    const/4 v0, 0x2

    iget-wide v2, p0, Lamz;->afT:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 609
    :cond_1
    iget v0, p0, Lamz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 610
    const/4 v0, 0x3

    iget-wide v2, p0, Lamz;->afU:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 612
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 613
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 617
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 618
    iget-object v1, p0, Lamz;->entry:Lizj;

    if-eqz v1, :cond_0

    .line 619
    const/4 v1, 0x1

    iget-object v2, p0, Lamz;->entry:Lizj;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 622
    :cond_0
    iget v1, p0, Lamz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 623
    const/4 v1, 0x2

    iget-wide v2, p0, Lamz;->afT:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 626
    :cond_1
    iget v1, p0, Lamz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 627
    const/4 v1, 0x3

    iget-wide v2, p0, Lamz;->afU:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 630
    :cond_2
    return v0
.end method

.method public final of()J
    .locals 2

    .prologue
    .line 551
    iget-wide v0, p0, Lamz;->afT:J

    return-wide v0
.end method

.method public final og()J
    .locals 2

    .prologue
    .line 570
    iget-wide v0, p0, Lamz;->afU:J

    return-wide v0
.end method
