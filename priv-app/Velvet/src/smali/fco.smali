.class public Lfco;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final coK:Z

.field private final mConfig:Lcjs;

.field private final mGmsLocationProvider:Lguh;


# direct methods
.method public constructor <init>(Lguh;Lcjs;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lfco;->mGmsLocationProvider:Lguh;

    .line 34
    iput-object p2, p0, Lfco;->mConfig:Lcjs;

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfco;->coK:Z

    .line 36
    return-void
.end method

.method public constructor <init>(Lguh;Lcjs;Z)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lfco;->mGmsLocationProvider:Lguh;

    .line 40
    iput-object p2, p0, Lfco;->mConfig:Lcjs;

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfco;->coK:Z

    .line 42
    return-void
.end method


# virtual methods
.method public final N(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lfco;->axx()Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lfco;->a(Ljava/util/List;Landroid/app/PendingIntent;)V

    .line 72
    return-void
.end method

.method public final O(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lfco;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Mp()Z

    move-result v0

    if-nez v0, :cond_1

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    :try_start_0
    new-instance v0, Lfcp;

    invoke-direct {v0}, Lfcp;-><init>()V

    .line 83
    iget-object v1, p0, Lfco;->mGmsLocationProvider:Lguh;

    invoke-virtual {v1, p1, v0}, Lguh;->b(Ljava/util/List;Lbrl;)V

    .line 84
    iget-boolean v1, p0, Lfco;->coK:Z

    if-eqz v1, :cond_0

    .line 85
    invoke-static {}, Lenu;->auQ()V

    .line 86
    invoke-virtual {v0}, Lfcp;->waitUntilFinished()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcgr; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 88
    :catch_0
    move-exception v0

    .line 89
    const-string v1, "GeofenceHelper"

    const-string v2, "Failed to remove geofences"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 90
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 91
    :catch_1
    move-exception v0

    .line 92
    const-string v1, "GeofenceHelper"

    const-string v2, "Failed to remove geofences"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a(Ljava/util/List;Landroid/app/PendingIntent;)V
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Lfco;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Mp()Z

    move-result v0

    if-nez v0, :cond_1

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    :try_start_0
    new-instance v0, Lfcp;

    invoke-direct {v0}, Lfcp;-><init>()V

    .line 53
    iget-object v1, p0, Lfco;->mGmsLocationProvider:Lguh;

    invoke-virtual {v1, p1, p2, v0}, Lguh;->b(Ljava/util/List;Landroid/app/PendingIntent;Lbrk;)V

    .line 54
    iget-boolean v1, p0, Lfco;->coK:Z

    if-eqz v1, :cond_0

    .line 55
    invoke-static {}, Lenu;->auQ()V

    .line 56
    invoke-virtual {v0}, Lfcp;->waitUntilFinished()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcgr; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 58
    :catch_0
    move-exception v0

    .line 59
    const-string v1, "GeofenceHelper"

    const-string v2, "Failed to register geofence"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 60
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 61
    :catch_1
    move-exception v0

    .line 62
    const-string v1, "GeofenceHelper"

    const-string v2, "Failed to register geofence"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final axw()V
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0}, Lfco;->axx()Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfco;->e(Landroid/app/PendingIntent;)V

    .line 123
    return-void
.end method

.method protected axx()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 126
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final e(Landroid/app/PendingIntent;)V
    .locals 3

    .prologue
    .line 100
    iget-object v0, p0, Lfco;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Mp()Z

    move-result v0

    if-nez v0, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    :try_start_0
    new-instance v0, Lfcp;

    invoke-direct {v0}, Lfcp;-><init>()V

    .line 105
    iget-object v1, p0, Lfco;->mGmsLocationProvider:Lguh;

    invoke-virtual {v1, p1, v0}, Lguh;->b(Landroid/app/PendingIntent;Lbrl;)V

    .line 106
    iget-boolean v1, p0, Lfco;->coK:Z

    if-eqz v1, :cond_0

    .line 107
    invoke-static {}, Lenu;->auQ()V

    .line 108
    invoke-virtual {v0}, Lfcp;->waitUntilFinished()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcgr; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 110
    :catch_0
    move-exception v0

    .line 111
    const-string v1, "GeofenceHelper"

    const-string v2, "Failed to remove geofences"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 112
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 113
    :catch_1
    move-exception v0

    .line 114
    const-string v1, "GeofenceHelper"

    const-string v2, "Failed to remove geofences"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
