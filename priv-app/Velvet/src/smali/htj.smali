.class public final Lhtj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final dqA:Lhsg;

.field public final dqB:Lhud;

.field public final dqC:Lhts;

.field public final dqD:Lhtl;

.field public final dqE:Lhsq;

.field final dqF:Lhuk;

.field public final dqG:Lhsm;

.field final dqH:Leqo;

.field public final dqy:Lhsz;

.field public final mLocalTtsManager:Lice;

.field public final mRecognizerController:Lhkd;


# direct methods
.method public constructor <init>(Lhsg;Lhsz;Lhud;Lhts;Lhtl;Lhsq;Lhuk;Lhkd;Lice;Lhsm;Leqo;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lhtj;->dqA:Lhsg;

    .line 60
    iput-object p2, p0, Lhtj;->dqy:Lhsz;

    .line 61
    iput-object p3, p0, Lhtj;->dqB:Lhud;

    .line 62
    iput-object p4, p0, Lhtj;->dqC:Lhts;

    .line 63
    iput-object p5, p0, Lhtj;->dqD:Lhtl;

    .line 64
    iput-object p6, p0, Lhtj;->dqE:Lhsq;

    .line 65
    iput-object p7, p0, Lhtj;->dqF:Lhuk;

    .line 66
    iput-object p8, p0, Lhtj;->mRecognizerController:Lhkd;

    .line 67
    iput-object p9, p0, Lhtj;->mLocalTtsManager:Lice;

    .line 68
    iput-object p10, p0, Lhtj;->dqG:Lhsm;

    .line 69
    iput-object p11, p0, Lhtj;->dqH:Leqo;

    .line 70
    return-void
.end method


# virtual methods
.method public final aRT()V
    .locals 4

    .prologue
    .line 85
    iget-object v1, p0, Lhtj;->dqB:Lhud;

    invoke-static {}, Lenu;->auR()V

    iget-object v0, v1, Lhud;->dqb:Lhtj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, v1, Lhud;->mViewDisplayer:Lhul;

    invoke-virtual {v0}, Lhul;->aSf()Lhui;

    move-result-object v0

    iput-object v0, v1, Lhud;->drc:Lhui;

    iget-object v0, v1, Lhud;->drc:Lhui;

    invoke-interface {v0, v1}, Lhui;->a(Lhud;)V

    iget-object v0, v1, Lhud;->drc:Lhui;

    iget-object v2, v1, Lhud;->dqb:Lhtj;

    iget-object v2, v2, Lhtj;->dqF:Lhuk;

    invoke-virtual {v2}, Lhuk;->aSb()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lhui;->oa(Ljava/lang/String;)V

    iget-object v0, v1, Lhud;->drc:Lhui;

    invoke-interface {v0}, Lhui;->aSa()V

    iget-object v0, v1, Lhud;->mRecognizerController:Lhkd;

    iget-object v2, v1, Lhud;->drc:Lhui;

    invoke-virtual {v0, v2}, Lhkd;->a(Lhsx;)V

    new-instance v0, Lhue;

    invoke-direct {v0, v1}, Lhue;-><init>(Lhud;)V

    iget-object v2, v1, Lhud;->aMD:Leqo;

    invoke-static {v2, v0}, Lers;->a(Ljava/util/concurrent/Executor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lesk;

    iget-object v1, v1, Lhud;->mLocalTtsManager:Lice;

    const v2, 0x7f0a05f0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lice;->a(ILesk;Ljava/lang/String;)V

    .line 86
    return-void

    .line 85
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bh(II)V
    .locals 5

    .prologue
    .line 102
    iget-object v1, p0, Lhtj;->dqE:Lhsq;

    invoke-static {}, Lenu;->auR()V

    iget-object v0, v1, Lhsq;->dqb:Lhtj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, v1, Lhsq;->mViewDisplayer:Lhul;

    invoke-virtual {v0}, Lhul;->aSg()Lhss;

    move-result-object v0

    iput-object v0, v1, Lhsq;->dqa:Lhss;

    iget-object v0, v1, Lhsq;->dqa:Lhss;

    invoke-interface {v0, p1}, Lhss;->lu(I)V

    iget-object v0, v1, Lhsq;->mLocalTtsManager:Lice;

    iget-object v2, v1, Lhsq;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lhsr;

    const-string v4, "Schedule exit"

    invoke-direct {v3, v1, v4}, Lhsr;-><init>(Lhsq;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, Lice;->a(Ljava/lang/String;Lesk;Ljava/lang/String;)V

    .line 103
    return-void

    .line 102
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lcom/google/android/search/shared/contact/Contact;)V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lhtj;->dqD:Lhtl;

    invoke-virtual {v0, p1}, Lhtl;->k(Lcom/google/android/search/shared/contact/Contact;)V

    .line 129
    return-void
.end method

.method public final exit()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lhtj;->mLocalTtsManager:Lice;

    invoke-virtual {v0}, Lice;->aUL()V

    .line 108
    iget-object v0, p0, Lhtj;->dqG:Lhsm;

    invoke-virtual {v0}, Lhsm;->aRP()V

    .line 109
    iget-object v0, p0, Lhtj;->dqA:Lhsg;

    iget-object v0, v0, Lhsg;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 110
    return-void
.end method

.method public final startActivity(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lhtj;->dqA:Lhsg;

    iget-object v0, v0, Lhsg;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 133
    return-void
.end method
