.class public final Lfig;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfdn;


# static fields
.field private static final PROTO_FACTORY:Ligi;


# instance fields
.field private final cks:Ljava/util/concurrent/CountDownLatch;

.field private final csG:Lfcf;

.field private final csH:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private csI:Ljava/util/Map;

.field private csJ:Ljava/util/Map;

.field private csK:Ljava/util/Map;

.field csL:Ljava/util/Map;

.field final csM:Ljava/util/Map;

.field final dK:Ljava/lang/Object;

.field private final mBackgroundTasks:Lgpp;

.field private final mCalendarProvider:Leym;

.field private final mClock:Lemp;

.field private final mFileWriter:Lfck;

.field private final mNetworkClient:Lfcx;

.field private final mNowOptInSettings:Lcin;

.field private final mTaskRunner:Lerk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    new-instance v0, Lfih;

    invoke-direct {v0}, Lfih;-><init>()V

    sput-object v0, Lfig;->PROTO_FACTORY:Ligi;

    return-void
.end method

.method public constructor <init>(Lfcj;Lfck;Lfcx;Lemp;Lerk;Lgpp;Leym;Lcin;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lfig;->csH:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 88
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v5}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lfig;->cks:Ljava/util/concurrent/CountDownLatch;

    .line 94
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lfig;->dK:Ljava/lang/Object;

    .line 125
    new-instance v0, Lfcf;

    sget-object v1, Lfig;->PROTO_FACTORY:Ligi;

    const-string v2, "training_question"

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lfcf;-><init>(Ligi;Ljava/lang/String;Lfcj;Lfck;Z)V

    iput-object v0, p0, Lfig;->csG:Lfcf;

    .line 127
    iput-object p2, p0, Lfig;->mFileWriter:Lfck;

    .line 128
    iput-object p3, p0, Lfig;->mNetworkClient:Lfcx;

    .line 129
    iput-object p4, p0, Lfig;->mClock:Lemp;

    .line 130
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lfig;->csM:Ljava/util/Map;

    .line 131
    iput-object p5, p0, Lfig;->mTaskRunner:Lerk;

    .line 132
    iput-object p6, p0, Lfig;->mBackgroundTasks:Lgpp;

    .line 133
    iput-object p7, p0, Lfig;->mCalendarProvider:Leym;

    .line 134
    iput-object p8, p0, Lfig;->mNowOptInSettings:Lcin;

    .line 135
    return-void
.end method

.method private a(Laqa;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 678
    iget-object v2, p0, Lfig;->dK:Ljava/lang/Object;

    monitor-enter v2

    .line 679
    :try_start_0
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lfig;->csI:Ljava/util/Map;

    .line 680
    iget-object v1, p1, Laqa;->amf:Ljha;

    if-eqz v1, :cond_0

    .line 682
    iget-object v1, p1, Laqa;->amf:Ljha;

    iget-object v3, v1, Ljha;->elu:[Ljhb;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 683
    iget-object v6, p0, Lfig;->csI:Ljava/util/Map;

    invoke-virtual {v5}, Ljhb;->getKey()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5}, Ljhb;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v6, v7, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 682
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 687
    :cond_0
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lfig;->csJ:Ljava/util/Map;

    .line 688
    iget-object v1, p1, Laqa;->amg:Ljdn;

    if-eqz v1, :cond_1

    .line 690
    iget-object v1, p1, Laqa;->amg:Ljdn;

    iget-object v3, v1, Ljdn;->ecy:[Ljdk;

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 691
    iget-object v6, p0, Lfig;->csJ:Ljava/util/Map;

    invoke-virtual {v5}, Ljdk;->getId()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 690
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 695
    :cond_1
    iget-object v1, p1, Laqa;->amh:[Ljde;

    array-length v1, v1

    invoke-static {v1}, Lior;->mk(I)Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lfig;->csK:Ljava/util/Map;

    .line 697
    iget-object v3, p1, Laqa;->amh:[Ljde;

    array-length v4, v3

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    .line 698
    iget-object v6, p0, Lfig;->csK:Ljava/util/Map;

    new-instance v7, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-direct {v7, v5}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-interface {v6, v7, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 697
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 701
    :cond_2
    iget-object v1, p1, Laqa;->ami:[Lapz;

    array-length v1, v1

    invoke-static {v1}, Lior;->mk(I)Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lfig;->csL:Ljava/util/Map;

    .line 704
    iget-object v1, p1, Laqa;->ami:[Lapz;

    array-length v3, v1

    :goto_3
    if-ge v0, v3, :cond_3

    aget-object v4, v1, v0

    .line 705
    iget-object v5, p0, Lfig;->csL:Ljava/util/Map;

    new-instance v6, Lcom/google/android/sidekick/shared/training/QuestionKey;

    iget-object v7, v4, Lapz;->ame:Ljde;

    invoke-direct {v6, v7}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 704
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 708
    :cond_3
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private c(Ljdj;)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;
    .locals 10
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 227
    iget-object v2, p1, Ljdj;->ame:Ljde;

    invoke-virtual {v2}, Ljde;->bhf()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "TrainingQuestionManagerImpl"

    const-string v1, "Question missing fingerprint"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v3

    .line 228
    :goto_0
    if-nez v1, :cond_6

    move-object v0, v3

    .line 241
    :cond_0
    return-object v0

    .line 227
    :cond_1
    iget-object v0, p0, Lfig;->csJ:Ljava/util/Map;

    invoke-virtual {v2}, Ljde;->bgX()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdk;

    if-nez v0, :cond_2

    const-string v0, "TrainingQuestionManagerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "Missing question template: "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljde;->bgX()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v3

    goto :goto_0

    :cond_2
    new-instance v5, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-direct {v5, v2}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    iget-object v1, p0, Lfig;->csM:Ljava/util/Map;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lapz;

    if-nez v1, :cond_3

    iget-object v1, p0, Lfig;->csL:Ljava/util/Map;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lapz;

    :cond_3
    if-eqz v1, :cond_a

    iget-object v1, v1, Lapz;->ame:Ljde;

    :goto_1
    if-nez v1, :cond_9

    iget-object v1, p0, Lfig;->csK:Ljava/util/Map;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljde;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljde;->bhd()Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lfig;->mClock:Lemp;

    invoke-interface {v5}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v1}, Ljde;->bha()J

    move-result-wide v8

    sub-long/2addr v6, v8

    invoke-virtual {v1}, Ljde;->bhc()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-lez v5, :cond_5

    const/4 v5, 0x1

    :goto_2
    if-eqz v5, :cond_9

    move-object v5, v3

    :goto_3
    if-eqz v5, :cond_8

    new-instance v1, Ljde;

    invoke-direct {v1}, Ljde;-><init>()V

    invoke-static {v2, v1}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v1

    check-cast v1, Ljde;

    iget-object v2, v5, Ljde;->ebz:Ljdf;

    iput-object v2, v1, Ljde;->ebz:Ljdf;

    invoke-virtual {v5}, Ljde;->bhb()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v5}, Ljde;->bha()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Ljde;->cS(J)Ljde;

    :cond_4
    :goto_4
    new-instance v2, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    iget-object v5, p0, Lfig;->csI:Ljava/util/Map;

    invoke-direct {v2, v5, v0, v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;-><init>(Ljava/util/Map;Ljdk;Ljde;)V

    move-object v1, v2

    goto/16 :goto_0

    :cond_5
    move v5, v4

    goto :goto_2

    .line 232
    :cond_6
    new-instance v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    iget-object v2, p1, Ljdj;->ecd:[Ljdf;

    invoke-direct {v0, v1, v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;-><init>(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;[Ljdf;)V

    .line 234
    iget-object v2, p1, Ljdj;->ecc:[Ljdj;

    array-length v3, v2

    move v1, v4

    :goto_5
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 235
    invoke-direct {p0, v4}, Lfig;->c(Ljdj;)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    move-result-object v4

    .line 236
    if-eqz v4, :cond_7

    .line 237
    invoke-virtual {v0, v4}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;)V

    .line 234
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_8
    move-object v1, v2

    goto :goto_4

    :cond_9
    move-object v5, v1

    goto :goto_3

    :cond_a
    move-object v1, v3

    goto :goto_1
.end method

.method private isInitialized()Z
    .locals 4

    .prologue
    .line 673
    iget-object v0, p0, Lfig;->cks:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lizj;Ljde;Liwk;)Landroid/os/AsyncTask;
    .locals 6

    .prologue
    .line 445
    new-instance v0, Leyd;

    iget-object v1, p0, Lfig;->mNetworkClient:Lfcx;

    iget-object v4, p0, Lfig;->mClock:Lemp;

    move-object v2, p1

    move-object v3, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Leyd;-><init>(Lfcx;Lizj;Liwk;Lemp;Ljde;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Leyd;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    return-object v0
.end method

.method final a(Laqa;Ljht;Ljava/lang/Iterable;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 543
    iget-object v0, p2, Ljht;->amf:Ljha;

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p2, Ljht;->amf:Ljha;

    iput-object v0, p1, Laqa;->amf:Ljha;

    .line 547
    :cond_0
    iget-object v0, p2, Ljht;->amg:Ljdn;

    if-eqz v0, :cond_1

    .line 548
    iget-object v0, p2, Ljht;->amg:Ljdn;

    iput-object v0, p1, Laqa;->amg:Ljdn;

    .line 551
    :cond_1
    iget-object v0, p2, Ljht;->emj:Liwt;

    if-eqz v0, :cond_4

    .line 553
    iget-object v0, p2, Ljht;->emj:Liwt;

    invoke-virtual {v0}, Liwt;->aZy()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_5

    .line 556
    iget-object v2, p1, Laqa;->amh:[Ljde;

    iget-object v0, p2, Ljht;->emj:Liwt;

    iget-object v3, v0, Liwt;->dMu:[Ljde;

    array-length v0, v2

    array-length v4, v3

    add-int/2addr v0, v4

    invoke-static {v0}, Lior;->mk(I)Ljava/util/HashMap;

    move-result-object v4

    array-length v5, v2

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_2

    aget-object v6, v2, v0

    new-instance v7, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-direct {v7, v6}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-interface {v4, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    array-length v2, v3

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v5, v3, v0

    new-instance v6, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-direct {v6, v5}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-interface {v4, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v2

    new-array v2, v2, [Ljde;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljde;

    .line 563
    :goto_2
    iput-object v0, p1, Laqa;->amh:[Ljde;

    .line 567
    :cond_4
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v2

    .line 568
    iget-object v3, p1, Laqa;->ami:[Lapz;

    array-length v4, v3

    move v0, v1

    :goto_3
    if-ge v0, v4, :cond_6

    aget-object v1, v3, v0

    .line 569
    new-instance v5, Lcom/google/android/sidekick/shared/training/QuestionKey;

    iget-object v6, v1, Lapz;->ame:Ljde;

    invoke-direct {v5, v6}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-interface {v2, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 568
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 560
    :cond_5
    iget-object v0, p2, Ljht;->emj:Liwt;

    iget-object v0, v0, Liwt;->dMu:[Ljde;

    goto :goto_2

    .line 571
    :cond_6
    iget-object v1, p0, Lfig;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 572
    :try_start_0
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapz;

    .line 573
    new-instance v4, Lcom/google/android/sidekick/shared/training/QuestionKey;

    iget-object v0, v0, Lapz;->ame:Ljde;

    invoke-direct {v4, v0}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    .line 574
    iget-object v0, p0, Lfig;->csM:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_4

    .line 580
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 579
    :cond_7
    :try_start_1
    iget-object v0, p0, Lfig;->csM:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 580
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 581
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapz;

    .line 582
    new-instance v3, Lcom/google/android/sidekick/shared/training/QuestionKey;

    iget-object v0, v0, Lapz;->ame:Ljde;

    invoke-direct {v3, v0}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 584
    :cond_8
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v1

    new-array v1, v1, [Lapz;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lapz;

    iput-object v0, p1, Laqa;->ami:[Lapz;

    .line 586
    return-void
.end method

.method public final a(Ljava/util/Collection;Lemy;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 140
    new-instance v0, Lfii;

    const-string v2, "TrainingQuestionResolver"

    iget-object v3, p0, Lfig;->mTaskRunner:Lerk;

    new-array v4, v7, [I

    move-object v1, p0

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lfii;-><init>(Lfig;Ljava/lang/String;Lerk;[ILjava/util/Collection;Lemy;)V

    new-array v1, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lfii;->a([Ljava/lang/Object;)Lenp;

    .line 152
    return-void
.end method

.method public final a(Ljde;Ljdf;Lizj;)V
    .locals 4
    .param p3    # Lizj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 314
    new-instance v0, Ljde;

    invoke-direct {v0}, Ljde;-><init>()V

    invoke-static {p1, v0}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Ljde;

    .line 315
    iget-object v1, p0, Lfig;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljde;->cS(J)Ljde;

    .line 316
    iput-object p2, v0, Ljde;->ebz:Ljdf;

    .line 317
    new-instance v1, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-direct {v1, v0}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    .line 319
    new-instance v2, Lapz;

    invoke-direct {v2}, Lapz;-><init>()V

    .line 320
    iput-object v0, v2, Lapz;->ame:Ljde;

    .line 321
    iput-object p3, v2, Lapz;->entry:Lizj;

    .line 323
    iget-object v3, p0, Lfig;->dK:Ljava/lang/Object;

    monitor-enter v3

    .line 324
    :try_start_0
    iget-object v0, p0, Lfig;->csM:Ljava/util/Map;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method public final a(Ljht;Ljava/lang/Iterable;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 502
    invoke-static {}, Lenu;->auQ()V

    .line 505
    iget-object v0, p1, Ljht;->emk:Ljel;

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p0, Lfig;->mNowOptInSettings:Lcin;

    iget-object v1, p1, Ljht;->emk:Ljel;

    invoke-interface {v0, v1}, Lcin;->a(Ljel;)V

    .line 510
    :cond_0
    iget-object v0, p1, Ljht;->amf:Ljha;

    if-nez v0, :cond_1

    iget-object v0, p1, Ljht;->amg:Ljdn;

    if-nez v0, :cond_1

    iget-object v0, p1, Ljht;->emj:Liwt;

    if-eqz v0, :cond_2

    :cond_1
    move v0, v3

    :goto_0
    if-nez v0, :cond_3

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 511
    const-string v0, "TrainingQuestionManagerImpl"

    const-string v1, "updateFromServerResponse: no new training mode data and no pending answered questions to clear"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 510
    goto :goto_0

    .line 516
    :cond_3
    instance-of v0, p2, Ljava/util/Collection;

    if-eqz v0, :cond_5

    move-object v0, p2

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    .line 517
    :goto_2
    iget-object v1, p1, Ljht;->emj:Liwt;

    if-eqz v1, :cond_6

    iget-object v1, p1, Ljht;->emj:Liwt;

    iget-object v1, v1, Liwt;->dMu:[Ljde;

    array-length v1, v1

    .line 519
    :goto_3
    if-lez v0, :cond_4

    if-ge v1, v0, :cond_4

    .line 520
    const-string v4, "TrainingQuestionManagerImpl"

    const-string v5, "Fewer answers received than sent: sent: %d, received: %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    :cond_4
    iget-object v0, p0, Lfig;->csG:Lfcf;

    new-instance v1, Lfij;

    invoke-direct {v1, p0, p1, p2}, Lfij;-><init>(Lfig;Ljht;Ljava/lang/Iterable;)V

    invoke-virtual {v0, v1}, Lfcf;->a(Lfcg;)V

    .line 532
    iget-object v0, p0, Lfig;->csG:Lfcf;

    invoke-virtual {v0}, Lfcf;->axq()Ljsr;

    move-result-object v0

    check-cast v0, Laqa;

    invoke-direct {p0, v0}, Lfig;->a(Laqa;)V

    goto :goto_1

    .line 516
    :cond_5
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Likr;->c(Ljava/util/Iterator;)I

    move-result v0

    goto :goto_2

    :cond_6
    move v1, v2

    .line 517
    goto :goto_3
.end method

.method public final awi()V
    .locals 4

    .prologue
    .line 713
    iget-object v0, p0, Lfig;->mBackgroundTasks:Lgpp;

    const-string v1, "clear_training_data"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lgpp;->r(Ljava/lang/String;J)V

    .line 714
    return-void
.end method

.method public final ays()V
    .locals 4

    .prologue
    .line 330
    invoke-direct {p0}, Lfig;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lfig;->isDirty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 337
    :goto_0
    return-void

    .line 336
    :cond_0
    iget-object v0, p0, Lfig;->mBackgroundTasks:Lgpp;

    const-string v1, "send_training_answers"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lgpp;->r(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public final ayt()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 341
    invoke-static {}, Lenu;->auQ()V

    .line 343
    invoke-virtual {p0}, Lfig;->ayu()Ljava/lang/Iterable;

    move-result-object v0

    .line 344
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 383
    :goto_0
    return-void

    .line 349
    :cond_0
    invoke-static {v0}, Lfqm;->k(Ljava/lang/Iterable;)Liwl;

    move-result-object v1

    .line 352
    const/4 v2, 0x5

    invoke-static {v2}, Lfjw;->iQ(I)Ljed;

    move-result-object v2

    .line 354
    iput-object v1, v2, Ljed;->eds:Liwl;

    .line 355
    new-instance v1, Ljhs;

    invoke-direct {v1}, Ljhs;-><init>()V

    iput-object v1, v2, Ljed;->edC:Ljhs;

    .line 356
    iget-object v1, v2, Ljed;->edC:Ljhs;

    invoke-virtual {p0}, Lfig;->ayv()Ljhu;

    move-result-object v3

    iput-object v3, v1, Ljhs;->emi:Ljhu;

    .line 358
    iget-object v1, p0, Lfig;->mNetworkClient:Lfcx;

    invoke-interface {v1, v2}, Lfcx;->c(Ljed;)Ljeh;

    move-result-object v1

    .line 361
    if-nez v1, :cond_1

    .line 362
    const-string v0, "TrainingQuestionManagerImpl"

    const-string v1, "Network error sending answered questions"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    :goto_1
    const-string v0, "TrainingQuestionManagerImpl"

    const-string v1, "Flushing pending changes to file"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    invoke-static {}, Lenu;->auQ()V

    iget-object v0, p0, Lfig;->csG:Lfcf;

    new-instance v1, Lfik;

    invoke-direct {v1, p0}, Lfik;-><init>(Lfig;)V

    invoke-virtual {v0, v1}, Lfcf;->a(Lfcg;)V

    goto :goto_0

    .line 363
    :cond_1
    iget-object v2, v1, Ljeh;->eeg:Liwm;

    if-eqz v2, :cond_2

    iget-object v2, v1, Ljeh;->eeg:Liwm;

    invoke-virtual {v2}, Liwm;->hasError()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 365
    const-string v2, "TrainingQuestionManagerImpl"

    const-string v3, "Server error processing answered questions: %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v1, v1, Ljeh;->eeg:Liwm;

    invoke-virtual {v1}, Liwm;->aZt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    invoke-virtual {p0, v0}, Lfig;->i(Ljava/lang/Iterable;)V

    goto :goto_1

    .line 368
    :cond_2
    iget-object v2, v1, Ljeh;->eeo:Ljht;

    if-nez v2, :cond_3

    .line 369
    const-string v1, "TrainingQuestionManagerImpl"

    const-string v2, "Response missing TrainingModeDataResponse"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    invoke-virtual {p0, v0}, Lfig;->i(Ljava/lang/Iterable;)V

    goto :goto_1

    .line 375
    :cond_3
    iget-object v1, v1, Ljeh;->eeo:Ljht;

    invoke-virtual {p0, v1, v0}, Lfig;->a(Ljht;Ljava/lang/Iterable;)V

    goto/16 :goto_0
.end method

.method public final ayu()Ljava/lang/Iterable;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 414
    invoke-static {}, Lenu;->auQ()V

    .line 416
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v2

    .line 419
    iget-object v0, p0, Lfig;->csG:Lfcf;

    invoke-virtual {v0}, Lfcf;->axq()Ljsr;

    move-result-object v0

    check-cast v0, Laqa;

    iget-object v3, v0, Laqa;->ami:[Lapz;

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 420
    new-instance v6, Lcom/google/android/sidekick/shared/training/QuestionKey;

    iget-object v7, v5, Lapz;->ame:Ljde;

    invoke-direct {v6, v7}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-interface {v2, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 419
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 424
    :cond_0
    iget-object v3, p0, Lfig;->dK:Ljava/lang/Object;

    monitor-enter v3

    .line 425
    :try_start_0
    iget-object v0, p0, Lfig;->csM:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 426
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 428
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    .line 429
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapz;

    iget-object v0, v0, Lapz;->ame:Ljde;

    iget-object v4, v0, Ljde;->ebv:[Ljdg;

    array-length v5, v4

    move v0, v1

    :goto_1
    if-ge v0, v5, :cond_1

    aget-object v6, v4, v0

    invoke-virtual {v6}, Ljdg;->bho()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v6}, Ljdg;->bhm()Ljdg;

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 426
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 430
    :cond_3
    return-object v2
.end method

.method public final ayv()Ljhu;
    .locals 11

    .prologue
    .line 451
    invoke-static {}, Lenu;->auQ()V

    .line 453
    iget-object v0, p0, Lfig;->csG:Lfcf;

    invoke-virtual {v0}, Lfcf;->axq()Ljsr;

    move-result-object v0

    check-cast v0, Laqa;

    .line 454
    new-instance v2, Ljhu;

    invoke-direct {v2}, Ljhu;-><init>()V

    .line 456
    iget-object v1, v0, Laqa;->amf:Ljha;

    if-eqz v1, :cond_0

    iget-object v1, v0, Laqa;->amf:Ljha;

    iget-object v1, v1, Ljha;->elt:Ljhc;

    if-eqz v1, :cond_0

    .line 458
    iget-object v1, v0, Laqa;->amf:Ljha;

    iget-object v1, v1, Ljha;->elt:Ljhc;

    iput-object v1, v2, Ljhu;->eml:Ljhc;

    .line 461
    :cond_0
    iget-object v1, v0, Laqa;->amg:Ljdn;

    if-eqz v1, :cond_1

    iget-object v1, v0, Laqa;->amg:Ljdn;

    iget-object v1, v1, Ljdn;->ecx:Ljdo;

    if-eqz v1, :cond_1

    .line 463
    iget-object v1, v0, Laqa;->amg:Ljdn;

    iget-object v1, v1, Ljdn;->ecx:Ljdo;

    iput-object v1, v2, Ljhu;->emm:Ljdo;

    .line 466
    :cond_1
    iget-object v3, v0, Laqa;->amh:[Ljde;

    const/4 v1, 0x0

    array-length v4, v3

    const/4 v0, 0x0

    move v10, v0

    move-object v0, v1

    move v1, v10

    :goto_0
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    invoke-virtual {v5}, Ljde;->bhb()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v5}, Ljde;->bha()J

    move-result-wide v6

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-lez v5, :cond_3

    :cond_2
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 469
    :cond_4
    if-eqz v0, :cond_5

    .line 470
    new-instance v1, Liwu;

    invoke-direct {v1}, Liwu;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Liwu;->cc(J)Liwu;

    move-result-object v0

    iput-object v0, v2, Ljhu;->emn:Liwu;

    .line 474
    :cond_5
    return-object v2
.end method

.method public final ayw()V
    .locals 2

    .prologue
    .line 718
    invoke-static {}, Lenu;->auQ()V

    .line 719
    iget-object v0, p0, Lfig;->csG:Lfcf;

    invoke-virtual {v0}, Lfcf;->axs()V

    .line 723
    iget-object v0, p0, Lfig;->csG:Lfcf;

    invoke-virtual {v0}, Lfcf;->axq()Ljsr;

    move-result-object v0

    check-cast v0, Laqa;

    invoke-direct {p0, v0}, Lfig;->a(Laqa;)V

    .line 724
    iget-object v1, p0, Lfig;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 725
    :try_start_0
    iget-object v0, p0, Lfig;->csM:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 726
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final i(Ljava/lang/Iterable;)V
    .locals 1

    .prologue
    .line 435
    invoke-static {}, Lenu;->auQ()V

    .line 439
    new-instance v0, Ljht;

    invoke-direct {v0}, Ljht;-><init>()V

    invoke-virtual {p0, v0, p1}, Lfig;->a(Ljht;Ljava/lang/Iterable;)V

    .line 440
    return-void
.end method

.method public final isDirty()Z
    .locals 2

    .prologue
    .line 406
    iget-object v1, p0, Lfig;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 407
    :try_start_0
    iget-object v0, p0, Lfig;->csM:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfig;->csL:Ljava/util/Map;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfig;->csL:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 409
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final lJ(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 731
    const/4 v0, 0x0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 733
    iget-object v0, p0, Lfig;->mFileWriter:Lfck;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_data.proto"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lfig;->csG:Lfcf;

    invoke-virtual {v2}, Lfcf;->axq()Ljsr;

    move-result-object v2

    invoke-static {v2}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfck;->h(Ljava/lang/String;[B)Z

    .line 735
    iget-object v0, p0, Lfig;->mFileWriter:Lfck;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_metadata.proto"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lfig;->ayv()Ljhu;

    move-result-object v2

    invoke-static {v2}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfck;->h(Ljava/lang/String;[B)Z

    .line 737
    return-void
.end method

.method public final t(Ljava/util/Collection;)Ljava/util/List;
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 156
    invoke-static {}, Lenu;->auQ()V

    .line 158
    invoke-direct {p0}, Lfig;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 159
    invoke-static {}, Lenu;->auQ()V

    iget-object v0, p0, Lfig;->csH:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfig;->csG:Lfcf;

    invoke-virtual {v0}, Lfcf;->axq()Ljsr;

    move-result-object v0

    check-cast v0, Laqa;

    invoke-direct {p0, v0}, Lfig;->a(Laqa;)V

    iget-object v0, p0, Lfig;->cks:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :cond_0
    :try_start_0
    iget-object v0, p0, Lfig;->cks:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdj;

    iget-object v0, v0, Ljdj;->ame:Ljde;

    iget-object v3, v0, Ljde;->ebv:[Ljdg;

    array-length v5, v3

    move v0, v4

    :goto_1
    if-ge v0, v5, :cond_2

    aget-object v6, v3, v0

    invoke-virtual {v6}, Ljdg;->bho()Z

    move-result v6

    if-eqz v6, :cond_7

    iget-object v0, p0, Lfig;->mCalendarProvider:Leym;

    invoke-interface {v0}, Leym;->aww()Ljava/util/Collection;

    move-result-object v0

    move-object v1, v0

    .line 162
    :goto_2
    iget-object v5, p0, Lfig;->dK:Ljava/lang/Object;

    monitor-enter v5

    .line 163
    :try_start_1
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 164
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdj;

    .line 165
    invoke-direct {p0, v0}, Lfig;->c(Ljdj;)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    move-result-object v3

    .line 166
    if-eqz v3, :cond_6

    if-eqz v1, :cond_6

    .line 167
    invoke-static {v1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v0

    iget-object v8, v0, Ljde;->ebv:[Ljdg;

    array-length v9, v8

    move v0, v4

    :goto_4
    if-ge v0, v9, :cond_5

    aget-object v10, v8, v0

    invoke-virtual {v10}, Ljdg;->bho()Z

    move-result v11

    if-eqz v11, :cond_a

    invoke-virtual {v10}, Ljdg;->bhn()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamm;

    invoke-virtual {v0}, Lamm;->getId()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lfzy;->mg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-virtual {v0}, Lamm;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljdg;->tz(Ljava/lang/String;)Ljdg;

    :cond_5
    move-object v0, v3

    :goto_5
    move-object v3, v0

    .line 170
    :cond_6
    if-eqz v3, :cond_3

    .line 171
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 175
    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    .line 159
    :catch_0
    move-exception v0

    const-string v0, "TrainingQuestionManagerImpl"

    const-string v1, "Initialization latch wait interrupted"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto/16 :goto_0

    .line 161
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    :cond_8
    move-object v1, v2

    goto/16 :goto_2

    :cond_9
    move-object v0, v2

    .line 167
    goto :goto_5

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 174
    :cond_b
    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-object v6
.end method
