.class public final Lcio;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcin;


# instance fields
.field private final aXF:Ljava/lang/Object;

.field private final aXG:Ljava/lang/Object;

.field private final aXH:Ljava/util/Set;

.field private final mBgExecutor:Ljava/util/concurrent/Executor;

.field private final mCardsPrefs:Lcxs;

.field private final mClock:Lemp;

.field private final mContext:Landroid/content/Context;

.field private final mGmsLocationReportingHelper:Leue;

.field private final mLoginHelper:Lcrh;

.field private final mPrefController:Lchr;

.field private final mSearchConfig:Lcjs;

.field final mVelvetServices:Lgql;


# direct methods
.method public constructor <init>(Lgql;Landroid/content/Context;Lchr;Lemp;Ljava/util/concurrent/Executor;Lcrh;Lcjs;Lcxs;Leue;)V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcio;->aXF:Ljava/lang/Object;

    .line 86
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcio;->aXG:Ljava/lang/Object;

    .line 88
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcio;->aXH:Ljava/util/Set;

    .line 96
    iput-object p1, p0, Lcio;->mVelvetServices:Lgql;

    .line 97
    iput-object p2, p0, Lcio;->mContext:Landroid/content/Context;

    .line 98
    iput-object p3, p0, Lcio;->mPrefController:Lchr;

    .line 99
    iput-object p4, p0, Lcio;->mClock:Lemp;

    .line 100
    iput-object p5, p0, Lcio;->mBgExecutor:Ljava/util/concurrent/Executor;

    .line 101
    iput-object p6, p0, Lcio;->mLoginHelper:Lcrh;

    .line 102
    iput-object p7, p0, Lcio;->mSearchConfig:Lcjs;

    .line 103
    iput-object p8, p0, Lcio;->mCardsPrefs:Lcxs;

    .line 104
    iput-object p9, p0, Lcio;->mGmsLocationReportingHelper:Leue;

    .line 105
    return-void
.end method

.method private KH()V
    .locals 2

    .prologue
    .line 587
    iget-object v0, p0, Lcio;->mCardsPrefs:Lcxs;

    invoke-virtual {v0}, Lcxs;->TK()V

    .line 590
    iget-object v0, p0, Lcio;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    .line 591
    invoke-virtual {v0}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v1

    .line 592
    invoke-virtual {v1}, Lfaq;->invalidate()V

    .line 595
    invoke-virtual {v0}, Lfdb;->axN()Leym;

    move-result-object v1

    .line 596
    invoke-interface {v1}, Leym;->awi()V

    .line 599
    invoke-virtual {v0}, Lfdb;->Sh()Lfcr;

    move-result-object v1

    invoke-interface {v1}, Lfcr;->axz()V

    .line 602
    invoke-virtual {v0}, Lfdb;->axO()Lfga;

    move-result-object v1

    invoke-interface {v1}, Lfga;->cancelAll()V

    .line 605
    invoke-virtual {v0}, Lfdb;->ayc()Lfdn;

    move-result-object v0

    invoke-interface {v0}, Lfdn;->awi()V

    .line 606
    return-void
.end method

.method private a(ILandroid/accounts/Account;)V
    .locals 2

    .prologue
    .line 344
    iget-object v0, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    invoke-static {p2}, Lcio;->n(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcyh;->l(Ljava/lang/String;I)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 345
    return-void
.end method

.method private a(Landroid/accounts/Account;ZZ)V
    .locals 3
    .param p1    # Landroid/accounts/Account;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 228
    iget-object v1, p0, Lcio;->aXF:Ljava/lang/Object;

    monitor-enter v1

    .line 229
    :try_start_0
    iget-object v0, p0, Lcio;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    invoke-static {p1, v0}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcio;->mPrefController:Lchr;

    iget-object v0, v0, Lchr;->mGelStartupPrefs:Ldku;

    const-string v2, "GEL.GSAPrefs.now_enabled"

    invoke-virtual {v0, v2, p2}, Ldku;->m(Ljava/lang/String;Z)V

    .line 232
    iget-object v0, p0, Lcio;->mPrefController:Lchr;

    iget-object v0, v0, Lchr;->mGelStartupPrefs:Ldku;

    const-string v2, "GEL.GSAPrefs.can_optin_to_now"

    invoke-virtual {v0, v2, p3}, Ldku;->m(Ljava/lang/String;Z)V

    .line 235
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Landroid/accounts/Account;Ljava/lang/Integer;)Z
    .locals 5
    .param p1    # Landroid/accounts/Account;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 127
    if-nez p1, :cond_1

    .line 132
    :cond_0
    :goto_0
    return v0

    .line 130
    :cond_1
    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2}, Lcio;->gy(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 131
    iget-object v3, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v3}, Lchr;->Ks()Lcyg;

    move-result-object v3

    const/4 v4, -0x1

    invoke-interface {v3, v2, v4}, Lcyg;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 132
    if-nez p2, :cond_2

    move v2, v1

    :goto_1
    if-lt v3, v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_1
.end method

.method private ck(Z)V
    .locals 2

    .prologue
    .line 268
    iget-object v0, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "GSAPrefs.now_promo_dismissed"

    invoke-interface {v0, v1, p1}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 270
    iget-object v0, p0, Lcio;->mPrefController:Lchr;

    iget-object v0, v0, Lchr;->mGelStartupPrefs:Ldku;

    const-string v1, "GSAPrefs.now_promo_dismissed"

    invoke-virtual {v0, v1, p1}, Ldku;->m(Ljava/lang/String;Z)V

    .line 272
    return-void
.end method

.method private static gy(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 280
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "opted_in_version_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static n(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 348
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "user_can_run_the_google_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private o(Landroid/accounts/Account;)Z
    .locals 3

    .prologue
    .line 471
    iget-object v0, p0, Lcio;->mSearchConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->LH()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lijj;->d([Ljava/lang/Object;)Lijj;

    move-result-object v0

    .line 472
    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 473
    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 474
    if-ltz v2, :cond_0

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p(Landroid/accounts/Account;)V
    .locals 4

    .prologue
    .line 530
    iget-object v0, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    .line 531
    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    .line 532
    invoke-static {p1}, Lcio;->q(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcio;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lcyh;->c(Ljava/lang/String;J)Lcyh;

    .line 534
    invoke-interface {v0}, Lcyh;->apply()V

    .line 535
    return-void
.end method

.method private static q(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 557
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "last_configuration_saved_time_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final KA()Z
    .locals 4

    .prologue
    .line 240
    iget-object v0, p0, Lcio;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 242
    iget-object v1, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Ks()Lcyg;

    move-result-object v1

    const-string v2, "first_run_screens"

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Lcyg;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 244
    if-lt v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final KB()V
    .locals 3

    .prologue
    .line 249
    iget-object v0, p0, Lcio;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 250
    iget-object v1, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Ks()Lcyg;

    move-result-object v1

    invoke-interface {v1}, Lcyg;->EH()Lcyh;

    move-result-object v1

    const-string v2, "first_run_screens"

    invoke-interface {v1, v2, v0}, Lcyh;->l(Ljava/lang/String;I)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 251
    iget-object v0, p0, Lcio;->mPrefController:Lchr;

    iget-object v0, v0, Lchr;->mGelStartupPrefs:Ldku;

    const-string v1, "GSAPrefs.first_run_screens_shown"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ldku;->m(Ljava/lang/String;Z)V

    .line 253
    return-void
.end method

.method public final KC()Z
    .locals 3

    .prologue
    .line 257
    iget-object v0, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    const-string v1, "GSAPrefs.now_promo_dismissed"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final KD()V
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcio;->ck(Z)V

    .line 263
    return-void
.end method

.method public final KE()I
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lcio;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c005f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method public final KF()I
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcio;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcio;->i(Landroid/accounts/Account;)I

    move-result v0

    return v0
.end method

.method public final KG()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 625
    invoke-virtual {p0}, Lcio;->Kz()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 636
    :goto_0
    return v0

    .line 629
    :cond_0
    iget-object v1, p0, Lcio;->mVelvetServices:Lgql;

    iget-object v1, v1, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    new-instance v2, Lcir;

    const-string v3, "stopServicesIfUserOptedOut"

    new-array v0, v0, [I

    invoke-direct {v2, p0, v3, v0}, Lcir;-><init>(Lcio;Ljava/lang/String;[I)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 636
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final Kz()Z
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lcio;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcio;->a(Landroid/accounts/Account;Ljava/lang/Integer;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/accounts/Account;I)V
    .locals 3

    .prologue
    .line 137
    iget-object v1, p0, Lcio;->aXF:Ljava/lang/Object;

    monitor-enter v1

    .line 138
    if-eqz p1, :cond_0

    .line 139
    :try_start_0
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 140
    invoke-static {v0}, Lcio;->gy(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 141
    iget-object v2, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v2}, Lchr;->Ks()Lcyg;

    move-result-object v2

    invoke-interface {v2}, Lcyg;->EH()Lcyh;

    move-result-object v2

    invoke-interface {v2, v0, p2}, Lcyh;->l(Ljava/lang/String;I)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 142
    const/4 v0, 0x1

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v2}, Lcio;->a(Landroid/accounts/Account;ZZ)V

    .line 144
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcio;->ck(Z)V

    .line 146
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Landroid/accounts/Account;Landroid/accounts/Account;)V
    .locals 5

    .prologue
    .line 194
    iget-object v1, p0, Lcio;->aXF:Ljava/lang/Object;

    monitor-enter v1

    .line 195
    :try_start_0
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Lcio;->gy(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 196
    iget-object v2, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v2}, Lchr;->Ks()Lcyg;

    move-result-object v2

    const/4 v3, -0x1

    invoke-interface {v2, v0, v3}, Lcyg;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 197
    iget-object v3, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v3}, Lcio;->gy(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 198
    iget-object v4, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v4}, Lchr;->Ks()Lcyg;

    move-result-object v4

    invoke-interface {v4}, Lcyg;->EH()Lcyh;

    move-result-object v4

    invoke-interface {v4, v0}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    move-result-object v0

    invoke-interface {v0, v3, v2}, Lcyh;->l(Ljava/lang/String;I)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 202
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    iget-object v0, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    invoke-static {p1}, Lcio;->n(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 207
    iget-object v1, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Ks()Lcyg;

    move-result-object v1

    invoke-interface {v1}, Lcyg;->EH()Lcyh;

    move-result-object v1

    invoke-static {p2}, Lcio;->n(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcyh;->l(Ljava/lang/String;I)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 212
    iget-object v0, p0, Lcio;->mCardsPrefs:Lcxs;

    invoke-virtual {v0, p1}, Lcxs;->t(Landroid/accounts/Account;)Liyl;

    move-result-object v0

    .line 213
    if-eqz v0, :cond_0

    .line 214
    iget-object v1, p0, Lcio;->mCardsPrefs:Lcxs;

    const/4 v2, 0x1

    invoke-virtual {v1, p2, v0, v2}, Lcxs;->a(Landroid/accounts/Account;Liyl;Z)V

    .line 220
    :cond_0
    invoke-virtual {p0, p2}, Lcio;->i(Landroid/accounts/Account;)I

    .line 221
    return-void

    .line 202
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Liyl;Landroid/accounts/Account;Lcom/google/android/gms/location/reporting/ReportingState;)V
    .locals 5
    .param p3    # Lcom/google/android/gms/location/reporting/ReportingState;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 517
    iget-object v2, p0, Lcio;->mCardsPrefs:Lcxs;

    invoke-virtual {v2, p2, p1, v1}, Lcxs;->a(Landroid/accounts/Account;Liyl;Z)V

    .line 519
    invoke-virtual {p0, p1, p2, p3}, Lcio;->b(Liyl;Landroid/accounts/Account;Lcom/google/android/gms/location/reporting/ReportingState;)I

    move-result v2

    .line 520
    iget-object v3, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v3}, Lchr;->Ks()Lcyg;

    move-result-object v3

    invoke-interface {v3}, Lcyg;->EH()Lcyh;

    move-result-object v3

    invoke-static {p2}, Lcio;->n(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Lcyh;->l(Ljava/lang/String;I)Lcyh;

    move-result-object v3

    invoke-interface {v3}, Lcyh;->apply()V

    .line 524
    invoke-direct {p0, p2}, Lcio;->p(Landroid/accounts/Account;)V

    .line 526
    const/4 v3, 0x0

    invoke-direct {p0, p2, v3}, Lcio;->a(Landroid/accounts/Account;Ljava/lang/Integer;)Z

    move-result v3

    if-ne v2, v0, :cond_0

    :goto_0
    invoke-direct {p0, p2, v3, v0}, Lcio;->a(Landroid/accounts/Account;ZZ)V

    .line 527
    return-void

    :cond_0
    move v0, v1

    .line 526
    goto :goto_0
.end method

.method public final a(Ljel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 563
    iget-object v0, p0, Lcio;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    .line 564
    invoke-virtual {p0, v0}, Lcio;->k(Landroid/accounts/Account;)Liyl;

    move-result-object v1

    .line 565
    if-nez v1, :cond_0

    .line 571
    :goto_0
    return-void

    .line 568
    :cond_0
    iput-object p1, v1, Liyl;->dQB:Ljel;

    .line 569
    invoke-virtual {p0, v0, v1, v2}, Lcio;->a(Landroid/accounts/Account;Liyl;Lcom/google/android/gms/location/reporting/ReportingState;)Z

    .line 570
    invoke-virtual {p0, v1, v0, v2}, Lcio;->a(Liyl;Landroid/accounts/Account;Lcom/google/android/gms/location/reporting/ReportingState;)V

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;Liyl;Lcom/google/android/gms/location/reporting/ReportingState;)Z
    .locals 2
    .param p3    # Lcom/google/android/gms/location/reporting/ReportingState;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 509
    iget-object v1, p0, Lcio;->mCardsPrefs:Lcxs;

    invoke-virtual {v1, p1, p2, v0}, Lcxs;->a(Landroid/accounts/Account;Liyl;Z)V

    .line 510
    invoke-direct {p0, p1}, Lcio;->p(Landroid/accounts/Account;)V

    .line 511
    const/4 v1, 0x0

    invoke-virtual {p0, p2, p1, v1}, Lcio;->b(Liyl;Landroid/accounts/Account;Lcom/google/android/gms/location/reporting/ReportingState;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Liyl;)Z
    .locals 1

    .prologue
    .line 651
    iget-object v0, p1, Liyl;->dQF:Ljbo;

    if-eqz v0, :cond_0

    .line 652
    iget-object v0, p1, Liyl;->dQF:Ljbo;

    invoke-virtual {v0}, Ljbo;->getEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 653
    const/4 v0, 0x1

    .line 656
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Liyl;Landroid/accounts/Account;)Z
    .locals 1

    .prologue
    .line 641
    invoke-direct {p0, p2}, Lcio;->o(Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Liyl;->dQE:Liyw;

    if-eqz v0, :cond_0

    .line 642
    iget-object v0, p1, Liyl;->dQE:Liyw;

    invoke-virtual {v0}, Liyw;->bcy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 643
    const/4 v0, 0x1

    .line 646
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Liyl;Landroid/accounts/Account;Lcom/google/android/gms/location/reporting/ReportingState;)I
    .locals 1
    .param p3    # Lcom/google/android/gms/location/reporting/ReportingState;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 481
    if-nez p1, :cond_0

    .line 482
    const/4 v0, 0x3

    .line 503
    :goto_0
    return v0

    .line 485
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcio;->a(Liyl;Landroid/accounts/Account;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 486
    const/4 v0, 0x4

    goto :goto_0

    .line 489
    :cond_1
    invoke-virtual {p0, p1}, Lcio;->a(Liyl;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 490
    const/4 v0, 0x5

    goto :goto_0

    .line 493
    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Lcom/google/android/gms/location/reporting/ReportingState;->Aw()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p3}, Lcom/google/android/gms/location/reporting/ReportingState;->Av()Z

    move-result v0

    if-nez v0, :cond_3

    .line 498
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcio;->a(Landroid/accounts/Account;Ljava/lang/Integer;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 499
    const/4 v0, 0x6

    goto :goto_0

    .line 503
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final e(Landroid/accounts/Account;)Z
    .locals 1
    .param p1    # Landroid/accounts/Account;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 118
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcio;->a(Landroid/accounts/Account;Ljava/lang/Integer;)Z

    move-result v0

    return v0
.end method

.method public final f(Landroid/accounts/Account;)Z
    .locals 1
    .param p1    # Landroid/accounts/Account;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 123
    invoke-virtual {p0}, Lcio;->KE()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcio;->a(Landroid/accounts/Account;Ljava/lang/Integer;)Z

    move-result v0

    return v0
.end method

.method public final g(Landroid/accounts/Account;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 151
    iget-object v2, p0, Lcio;->aXF:Ljava/lang/Object;

    monitor-enter v2

    .line 152
    const/4 v3, 0x0

    :try_start_0
    invoke-direct {p0, p1, v3}, Lcio;->a(Landroid/accounts/Account;Ljava/lang/Integer;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 153
    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v3}, Lcio;->gy(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 154
    iget-object v4, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v4}, Lchr;->Ks()Lcyg;

    move-result-object v4

    invoke-interface {v4}, Lcyg;->EH()Lcyh;

    move-result-object v4

    invoke-interface {v4, v3}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    move-result-object v3

    invoke-interface {v3}, Lcyh;->apply()V

    .line 155
    const/4 v3, 0x0

    invoke-virtual {p0, p1}, Lcio;->i(Landroid/accounts/Account;)I

    move-result v4

    if-ne v4, v0, :cond_1

    :goto_0
    invoke-direct {p0, p1, v3, v0}, Lcio;->a(Landroid/accounts/Account;ZZ)V

    .line 157
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_1
    move v0, v1

    .line 155
    goto :goto_0

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final gx(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 661
    if-eqz p1, :cond_1

    const-string v0, "any_account_can_run_the_google"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "opted_in_version_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h(Landroid/accounts/Account;)V
    .locals 5
    .param p1    # Landroid/accounts/Account;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 162
    const/4 v2, 0x0

    invoke-direct {p0, p1, v2}, Lcio;->a(Landroid/accounts/Account;Ljava/lang/Integer;)Z

    move-result v2

    .line 163
    invoke-virtual {p0, p1}, Lcio;->i(Landroid/accounts/Account;)I

    move-result v3

    if-ne v3, v0, :cond_1

    .line 164
    :goto_0
    invoke-direct {p0, p1, v2, v0}, Lcio;->a(Landroid/accounts/Account;ZZ)V

    .line 166
    invoke-direct {p0}, Lcio;->KH()V

    .line 168
    if-eqz v2, :cond_0

    .line 169
    iget-object v0, p0, Lcio;->mCardsPrefs:Lcxs;

    invoke-virtual {v0, p1}, Lcxs;->w(Landroid/accounts/Account;)V

    .line 176
    :cond_0
    iget-object v0, p0, Lcio;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v3, Lcip;

    const-string v4, "configureNowServices"

    new-array v1, v1, [I

    invoke-direct {v3, p0, v4, v1, v2}, Lcip;-><init>(Lcio;Ljava/lang/String;[IZ)V

    invoke-interface {v0, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 186
    return-void

    :cond_1
    move v0, v1

    .line 163
    goto :goto_0
.end method

.method public final i(Landroid/accounts/Account;)I
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x3

    const/4 v4, 0x0

    .line 295
    if-nez p1, :cond_0

    .line 296
    iget-object v0, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v2, "any_account_can_run_the_google"

    invoke-interface {v0, v2, v1}, Lcyh;->l(Ljava/lang/String;I)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    move v0, v1

    .line 339
    :goto_0
    return v0

    .line 303
    :cond_0
    iget-object v0, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    invoke-static {p1}, Lcio;->n(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v4}, Lcyg;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 307
    iget-object v0, p0, Lcio;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-direct {p0, v0}, Lcio;->o(Landroid/accounts/Account;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v3

    :goto_1
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcio;->mSearchConfig:Lcjs;

    const v5, 0x7f0c0038

    invoke-virtual {v0, v5}, Lcjs;->getInt(I)I

    move-result v0

    .line 310
    :goto_2
    iget-object v5, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v5}, Lchr;->Ks()Lcyg;

    move-result-object v5

    invoke-static {p1}, Lcio;->q(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v6

    const-wide/16 v8, 0x0

    invoke-interface {v5, v6, v8, v9}, Lcyg;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    int-to-long v8, v0

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    add-long/2addr v6, v8

    .line 314
    iget-object v0, p0, Lcio;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v8

    .line 315
    cmp-long v0, v8, v6

    if-lez v0, :cond_6

    move v0, v3

    .line 317
    :goto_3
    if-eqz v2, :cond_1

    if-eq v2, v1, :cond_1

    if-eqz v0, :cond_3

    .line 325
    :cond_1
    iget-object v1, p0, Lcio;->aXG:Ljava/lang/Object;

    monitor-enter v1

    .line 326
    :try_start_0
    iget-object v0, p0, Lcio;->aXH:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 327
    iget-object v0, p0, Lcio;->mBgExecutor:Ljava/util/concurrent/Executor;

    new-instance v3, Lciq;

    const-string v4, "fetch config"

    const/4 v5, 0x2

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    invoke-direct {v3, p0, v4, v5, p1}, Lciq;-><init>(Lcio;Ljava/lang/String;[ILandroid/accounts/Account;)V

    invoke-interface {v0, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 337
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    move v0, v2

    .line 339
    goto :goto_0

    :cond_4
    move v0, v4

    .line 307
    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcio;->mSearchConfig:Lcjs;

    const v5, 0x7f0c0037

    invoke-virtual {v0, v5}, Lcjs;->getInt(I)I

    move-result v0

    goto :goto_2

    :cond_6
    move v0, v4

    .line 315
    goto :goto_3

    .line 337
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 327
    nop

    :array_0
    .array-data 4
        0x2
        0x1
    .end array-data
.end method

.method public final j(Landroid/accounts/Account;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x2710

    const/4 v0, 0x1

    .line 354
    invoke-static {}, Lenu;->auQ()V

    .line 358
    const/4 v1, 0x0

    .line 359
    iget-object v2, p0, Lcio;->aXG:Ljava/lang/Object;

    monitor-enter v2

    .line 360
    :try_start_0
    iget-object v3, p0, Lcio;->aXH:Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 361
    iget-object v1, p0, Lcio;->aXH:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 364
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 366
    if-eqz v0, :cond_2

    .line 369
    :try_start_1
    iget-object v0, p0, Lcio;->mGmsLocationReportingHelper:Leue;

    invoke-virtual {v0, p1}, Leue;->C(Landroid/accounts/Account;)Lcgs;

    move-result-object v0

    iget-object v1, p0, Lcio;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->aJr()Lfdb;

    move-result-object v1

    invoke-virtual {v1}, Lfdb;->axI()Lfcx;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2}, Lfjw;->iQ(I)Ljed;

    move-result-object v2

    new-instance v3, Lizx;

    invoke-direct {v3}, Lizx;-><init>()V

    iput-object v3, v2, Ljed;->edu:Lizx;

    invoke-interface {v1, v2}, Lfcx;->c(Ljed;)Ljeh;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, v1, Ljeh;->eei:Lizy;

    if-eqz v2, :cond_1

    iget-object v2, v1, Ljeh;->eei:Lizy;

    iget-object v2, v2, Lizy;->dUJ:Liyl;

    if-eqz v2, :cond_1

    iget-object v1, v1, Ljeh;->eei:Lizy;

    iget-object v1, v1, Lizy;->dUJ:Liyl;

    invoke-virtual {v0}, Lcgs;->Fg()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/reporting/ReportingState;

    invoke-virtual {p0, v1, p1, v0}, Lcio;->a(Liyl;Landroid/accounts/Account;Lcom/google/android/gms/location/reporting/ReportingState;)V

    iget-object v2, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v2}, Lchr;->Ks()Lcyg;

    move-result-object v2

    invoke-interface {v2}, Lcyg;->EH()Lcyh;

    move-result-object v2

    const-string v3, "any_account_can_run_the_google"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Lcyh;->l(Ljava/lang/String;I)Lcyh;

    move-result-object v2

    invoke-interface {v2}, Lcyh;->apply()V

    invoke-virtual {p0, v1, p1, v0}, Lcio;->b(Liyl;Landroid/accounts/Account;Lcom/google/android/gms/location/reporting/ReportingState;)I

    move-result v0

    invoke-direct {p0, v0, p1}, Lcio;->a(ILandroid/accounts/Account;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 371
    :cond_0
    :goto_1
    iget-object v1, p0, Lcio;->aXG:Ljava/lang/Object;

    monitor-enter v1

    .line 372
    :try_start_2
    iget-object v0, p0, Lcio;->aXH:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 373
    iget-object v0, p0, Lcio;->aXG:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 374
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 397
    :goto_2
    return-void

    .line 364
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 369
    :cond_1
    :try_start_3
    iget-object v0, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    invoke-static {p1}, Lcio;->n(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcyg;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "NowOptInSettingsImpl"

    const-string v1, "Failed to fetch default configuration"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x3

    invoke-direct {p0, v0, p1}, Lcio;->a(ILandroid/accounts/Account;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 375
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcio;->aXG:Ljava/lang/Object;

    monitor-enter v1

    .line 372
    :try_start_4
    iget-object v2, p0, Lcio;->aXH:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 373
    iget-object v2, p0, Lcio;->aXG:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 374
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    throw v0

    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_3
    move-exception v0

    monitor-exit v1

    throw v0

    .line 377
    :cond_2
    iget-object v0, p0, Lcio;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    .line 378
    const-wide/16 v0, 0x0

    .line 379
    iget-object v6, p0, Lcio;->aXG:Ljava/lang/Object;

    monitor-enter v6

    move-wide v2, v0

    .line 381
    :goto_3
    :try_start_5
    iget-object v0, p0, Lcio;->aXH:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    move-result v0

    if-eqz v0, :cond_3

    cmp-long v0, v2, v10

    if-gez v0, :cond_3

    .line 384
    :try_start_6
    iget-object v0, p0, Lcio;->aXG:Ljava/lang/Object;

    sub-long v8, v10, v2

    invoke-virtual {v0, v8, v9}, Ljava/lang/Object;->wait(J)V

    .line 385
    iget-object v0, p0, Lcio;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    move-result-wide v0

    sub-long/2addr v0, v4

    move-wide v2, v0

    .line 388
    goto :goto_3

    .line 386
    :catch_0
    move-exception v0

    .line 387
    :try_start_7
    const-string v1, "NowOptInSettingsImpl"

    const-string v7, "Interrupted while waiting for configuration fetch"

    invoke-static {v1, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    goto :goto_3

    .line 397
    :catchall_4
    move-exception v0

    monitor-exit v6

    throw v0

    .line 393
    :cond_3
    :try_start_8
    iget-object v0, p0, Lcio;->aXH:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 394
    const-string v0, "NowOptInSettingsImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "account still pending, removing to retry later: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    iget-object v0, p0, Lcio;->aXH:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 397
    :cond_4
    monitor-exit v6
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    goto/16 :goto_2

    :cond_5
    move v0, v1

    goto/16 :goto_0
.end method

.method public final k(Landroid/accounts/Account;)Liyl;
    .locals 3
    .param p1    # Landroid/accounts/Account;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 540
    if-nez p1, :cond_1

    .line 541
    const/4 v0, 0x0

    .line 548
    :cond_0
    :goto_0
    return-object v0

    .line 544
    :cond_1
    iget-object v0, p0, Lcio;->mCardsPrefs:Lcxs;

    invoke-virtual {v0, p1}, Lcxs;->t(Landroid/accounts/Account;)Liyl;

    move-result-object v0

    .line 545
    if-nez v0, :cond_0

    .line 546
    iget-object v1, p0, Lcio;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Ks()Lcyg;

    move-result-object v1

    invoke-interface {v1}, Lcyg;->EH()Lcyh;

    move-result-object v1

    invoke-static {p1}, Lcio;->q(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    move-result-object v1

    invoke-interface {v1}, Lcyh;->apply()V

    goto :goto_0
.end method

.method public final l(Landroid/accounts/Account;)Lizy;
    .locals 2

    .prologue
    .line 447
    invoke-static {}, Lenu;->auQ()V

    .line 449
    const/4 v0, 0x1

    invoke-static {v0}, Lfjw;->iQ(I)Ljed;

    move-result-object v0

    .line 450
    new-instance v1, Lizx;

    invoke-direct {v1}, Lizx;-><init>()V

    iput-object v1, v0, Ljed;->edu:Lizx;

    .line 453
    iget-object v1, p0, Lcio;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->aJr()Lfdb;

    move-result-object v1

    invoke-virtual {v1}, Lfdb;->axI()Lfcx;

    move-result-object v1

    .line 454
    invoke-interface {v1, v0, p1}, Lfcx;->a(Ljed;Landroid/accounts/Account;)Ljeh;

    move-result-object v0

    .line 457
    if-eqz v0, :cond_0

    iget-object v1, v0, Ljeh;->eei:Lizy;

    if-eqz v1, :cond_0

    .line 459
    iget-object v0, v0, Ljeh;->eei:Lizy;

    .line 462
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m(Landroid/accounts/Account;)V
    .locals 1

    .prologue
    .line 576
    iget-object v0, p0, Lcio;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJQ()V

    .line 579
    invoke-virtual {p0, p1}, Lcio;->g(Landroid/accounts/Account;)V

    .line 581
    invoke-direct {p0}, Lcio;->KH()V

    .line 582
    return-void
.end method
