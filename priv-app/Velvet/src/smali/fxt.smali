.class public final Lfxt;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/content/Context;Laok;)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/16 v5, 0x11

    const/4 v1, 0x0

    .line 18
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 19
    invoke-virtual {p1}, Laok;->qK()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    invoke-virtual {p1}, Laok;->qJ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b00bd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-static {v0, v3}, Lgab;->d(Ljava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 24
    :cond_0
    invoke-virtual {p1}, Laok;->qr()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Laok;->qq()I

    move-result v0

    if-eqz v0, :cond_1

    .line 25
    invoke-static {v2}, Lfxt;->a(Landroid/text/SpannableStringBuilder;)V

    .line 27
    new-instance v3, Landroid/text/style/ImageSpan;

    invoke-virtual {p1}, Laok;->qq()I

    move-result v0

    const/4 v4, 0x1

    invoke-direct {v3, p0, v0, v4}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    .line 29
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 34
    :cond_1
    invoke-virtual {p1}, Laok;->qO()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Laok;->qN()I

    move-result v0

    if-eqz v0, :cond_2

    .line 35
    invoke-static {v2}, Lfxt;->a(Landroid/text/SpannableStringBuilder;)V

    .line 36
    new-instance v0, Landroid/text/style/ImageSpan;

    invoke-virtual {p1}, Laok;->qN()I

    move-result v3

    invoke-direct {v0, p0, v3, v1}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    .line 38
    const-string v1, "  "

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 39
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v2, v0, v1, v3, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 43
    :cond_2
    invoke-virtual {p1}, Laok;->qM()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 44
    invoke-static {v2}, Lfxt;->a(Landroid/text/SpannableStringBuilder;)V

    .line 45
    invoke-virtual {p1}, Laok;->qL()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 48
    :cond_3
    return-object v2

    .line 29
    :cond_4
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private static a(Landroid/text/SpannableStringBuilder;)V
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 53
    const-string v0, "  "

    invoke-virtual {p0, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 55
    :cond_0
    return-void
.end method
