.class final Ligv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private dBQ:Ljava/util/Map$Entry;

.field private synthetic dBR:Ljava/util/Iterator;

.field final synthetic dBS:Ligu;


# direct methods
.method constructor <init>(Ligu;Ljava/util/Iterator;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Ligv;->dBS:Ligu;

    iput-object p2, p0, Ligv;->dBR:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Ligv;->dBR:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Ligv;->dBR:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, Ligv;->dBQ:Ljava/util/Map$Entry;

    new-instance v1, Ligw;

    invoke-direct {v1, p0, v0}, Ligw;-><init>(Ligv;Ljava/util/Map$Entry;)V

    return-object v1
.end method

.method public final remove()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 127
    iget-object v0, p0, Ligv;->dBQ:Ljava/util/Map$Entry;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "no calls to next() since the last call to remove()"

    invoke-static {v0, v2}, Lifv;->d(ZLjava/lang/Object;)V

    .line 129
    iget-object v2, p0, Ligv;->dBS:Ligu;

    iget-object v0, p0, Ligv;->dBQ:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liik;

    invoke-virtual {v0, v1}, Liik;->getAndSet(I)I

    move-result v0

    int-to-long v0, v0

    invoke-static {v2, v0, v1}, Ligu;->a(Ligu;J)J

    .line 130
    iget-object v0, p0, Ligv;->dBR:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, Ligv;->dBQ:Ljava/util/Map$Entry;

    .line 132
    return-void

    :cond_0
    move v0, v1

    .line 127
    goto :goto_0
.end method
