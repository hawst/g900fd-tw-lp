.class public final Lgue;
.super Lcgl;
.source "PG"


# instance fields
.field final cWy:Ljava/util/concurrent/atomic/AtomicLong;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lerk;)V
    .locals 6

    .prologue
    .line 35
    const-string v1, "ActivityDetectionProvider"

    const-wide/32 v4, 0x1b7740

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcgl;-><init>(Ljava/lang/String;Landroid/content/Context;Lerk;J)V

    .line 29
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lgue;->cWy:Ljava/util/concurrent/atomic/AtomicLong;

    .line 37
    iput-object p1, p0, Lgue;->mContext:Landroid/content/Context;

    .line 38
    return-void
.end method


# virtual methods
.method protected final synthetic a(Landroid/content/Context;Lbgp;Lbgq;)Lbgo;
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lbrd;

    invoke-direct {v0, p1, p2, p3}, Lbrd;-><init>(Landroid/content/Context;Lbgp;Lbgq;)V

    return-object v0
.end method

.method protected final aKv()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 70
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.velvet.location.ACTIVITY_DETECTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 71
    iget-object v1, p0, Lgue;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    const/high16 v3, 0x10000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public final aKw()Lcgs;
    .locals 2

    .prologue
    .line 76
    new-instance v0, Lgug;

    invoke-direct {v0, p0}, Lgug;-><init>(Lgue;)V

    const-string v1, "stopActivityDetectionUpdates"

    invoke-virtual {p0, v0, v1}, Lgue;->a(Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method public bG(J)Lcgs;
    .locals 4

    .prologue
    .line 53
    new-instance v0, Lguf;

    const-wide/32 v2, 0x2bf20

    invoke-direct {v0, p0, v2, v3}, Lguf;-><init>(Lgue;J)V

    const-string v1, "startActivityDetectionUpdates"

    invoke-virtual {p0, v0, v1}, Lgue;->a(Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcgs;

    move-result-object v0

    return-object v0
.end method
