.class public final Lcpu;
.super Ljsl;
.source "PG"


# instance fields
.field public bfI:Ljava/lang/String;

.field public bfJ:[Lcpp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcpu;->bfI:Ljava/lang/String;

    invoke-static {}, Lcpp;->Sd()[Lcpp;

    move-result-object v0

    iput-object v0, p0, Lcpu;->bfJ:[Lcpp;

    const/4 v0, 0x0

    iput-object v0, p0, Lcpu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lcpu;->eCz:I

    .line 31
    return-void
.end method

.method public static y([B)Lcpu;
    .locals 1

    .prologue
    .line 118
    new-instance v0, Lcpu;

    invoke-direct {v0}, Lcpu;-><init>()V

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Lcpu;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcpu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcpu;->bfI:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lcpu;->bfJ:[Lcpp;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcpp;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcpu;->bfJ:[Lcpp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcpp;

    invoke-direct {v3}, Lcpp;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcpu;->bfJ:[Lcpp;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcpp;

    invoke-direct {v3}, Lcpp;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lcpu;->bfJ:[Lcpp;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 44
    const/4 v0, 0x1

    iget-object v1, p0, Lcpu;->bfI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcpu;->bfJ:[Lcpp;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcpu;->bfJ:[Lcpp;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 46
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcpu;->bfJ:[Lcpp;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 47
    iget-object v1, p0, Lcpu;->bfJ:[Lcpp;

    aget-object v1, v1, v0

    .line 48
    if-eqz v1, :cond_0

    .line 49
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 46
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 54
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 58
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 59
    const/4 v1, 0x1

    iget-object v2, p0, Lcpu;->bfI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v1, v0

    .line 61
    iget-object v0, p0, Lcpu;->bfJ:[Lcpp;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcpu;->bfJ:[Lcpp;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 62
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcpu;->bfJ:[Lcpp;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 63
    iget-object v2, p0, Lcpu;->bfJ:[Lcpp;

    aget-object v2, v2, v0

    .line 64
    if-eqz v2, :cond_0

    .line 65
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 62
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_1
    return v1
.end method
