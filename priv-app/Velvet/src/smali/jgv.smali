.class public final Ljgv;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dAq:I

.field private dAr:I

.field public ekX:Ljal;

.field public ekY:Ljbp;

.field private ekZ:J

.field private ela:Z

.field private elb:Z

.field private elc:Z

.field private eld:Z

.field private ele:Z

.field private elf:Z

.field private elg:[Ljcq;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 4199
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 4200
    iput v2, p0, Ljgv;->aez:I

    iput-object v3, p0, Ljgv;->ekX:Ljal;

    iput v2, p0, Ljgv;->dAq:I

    iput v2, p0, Ljgv;->dAr:I

    iput-object v3, p0, Ljgv;->ekY:Ljbp;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljgv;->ekZ:J

    iput-boolean v2, p0, Ljgv;->ela:Z

    iput-boolean v2, p0, Ljgv;->elb:Z

    iput-boolean v2, p0, Ljgv;->elc:Z

    iput-boolean v4, p0, Ljgv;->eld:Z

    iput-boolean v4, p0, Ljgv;->ele:Z

    iput-boolean v2, p0, Ljgv;->elf:Z

    invoke-static {}, Ljcq;->bgO()[Ljcq;

    move-result-object v0

    iput-object v0, p0, Ljgv;->elg:[Ljcq;

    iput-object v3, p0, Ljgv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljgv;->eCz:I

    .line 4201
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4000
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljgv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljgv;->ekX:Ljal;

    if-nez v0, :cond_1

    new-instance v0, Ljal;

    invoke-direct {v0}, Ljal;-><init>()V

    iput-object v0, p0, Ljgv;->ekX:Ljal;

    :cond_1
    iget-object v0, p0, Ljgv;->ekX:Ljal;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljgv;->dAq:I

    iget v0, p0, Ljgv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljgv;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljgv;->dAr:I

    iget v0, p0, Ljgv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljgv;->aez:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljgv;->ekY:Ljbp;

    if-nez v0, :cond_2

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljgv;->ekY:Ljbp;

    :cond_2
    iget-object v0, p0, Ljgv;->ekY:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljgv;->ekZ:J

    iget v0, p0, Ljgv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljgv;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljgv;->ela:Z

    iget v0, p0, Ljgv;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljgv;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljgv;->elb:Z

    iget v0, p0, Ljgv;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljgv;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljgv;->elc:Z

    iget v0, p0, Ljgv;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljgv;->aez:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljgv;->elf:Z

    iget v0, p0, Ljgv;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljgv;->aez:I

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x52

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljgv;->elg:[Ljcq;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljcq;

    if-eqz v0, :cond_3

    iget-object v3, p0, Ljgv;->elg:[Ljcq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Ljcq;

    invoke-direct {v3}, Ljcq;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ljgv;->elg:[Ljcq;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Ljcq;

    invoke-direct {v3}, Ljcq;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljgv;->elg:[Ljcq;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljgv;->eld:Z

    iget v0, p0, Ljgv;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljgv;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljgv;->ele:Z

    iget v0, p0, Ljgv;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljgv;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 4225
    iget-object v0, p0, Ljgv;->ekX:Ljal;

    if-eqz v0, :cond_0

    .line 4226
    const/4 v0, 0x1

    iget-object v1, p0, Ljgv;->ekX:Ljal;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 4228
    :cond_0
    iget v0, p0, Ljgv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 4229
    const/4 v0, 0x2

    iget v1, p0, Ljgv;->dAq:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 4231
    :cond_1
    iget v0, p0, Ljgv;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 4232
    const/4 v0, 0x3

    iget v1, p0, Ljgv;->dAr:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 4234
    :cond_2
    iget-object v0, p0, Ljgv;->ekY:Ljbp;

    if-eqz v0, :cond_3

    .line 4235
    const/4 v0, 0x4

    iget-object v1, p0, Ljgv;->ekY:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 4237
    :cond_3
    iget v0, p0, Ljgv;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 4238
    const/4 v0, 0x5

    iget-wide v2, p0, Ljgv;->ekZ:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 4240
    :cond_4
    iget v0, p0, Ljgv;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 4241
    const/4 v0, 0x6

    iget-boolean v1, p0, Ljgv;->ela:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 4243
    :cond_5
    iget v0, p0, Ljgv;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    .line 4244
    const/4 v0, 0x7

    iget-boolean v1, p0, Ljgv;->elb:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 4246
    :cond_6
    iget v0, p0, Ljgv;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_7

    .line 4247
    const/16 v0, 0x8

    iget-boolean v1, p0, Ljgv;->elc:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 4249
    :cond_7
    iget v0, p0, Ljgv;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_8

    .line 4250
    const/16 v0, 0x9

    iget-boolean v1, p0, Ljgv;->elf:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 4252
    :cond_8
    iget-object v0, p0, Ljgv;->elg:[Ljcq;

    if-eqz v0, :cond_a

    iget-object v0, p0, Ljgv;->elg:[Ljcq;

    array-length v0, v0

    if-lez v0, :cond_a

    .line 4253
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljgv;->elg:[Ljcq;

    array-length v1, v1

    if-ge v0, v1, :cond_a

    .line 4254
    iget-object v1, p0, Ljgv;->elg:[Ljcq;

    aget-object v1, v1, v0

    .line 4255
    if-eqz v1, :cond_9

    .line 4256
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 4253
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4260
    :cond_a
    iget v0, p0, Ljgv;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_b

    .line 4261
    const/16 v0, 0xb

    iget-boolean v1, p0, Ljgv;->eld:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 4263
    :cond_b
    iget v0, p0, Ljgv;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_c

    .line 4264
    const/16 v0, 0xc

    iget-boolean v1, p0, Ljgv;->ele:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 4266
    :cond_c
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 4267
    return-void
.end method

.method public final ih(Z)Ljgv;
    .locals 1

    .prologue
    .line 4088
    iput-boolean p1, p0, Ljgv;->ela:Z

    .line 4089
    iget v0, p0, Ljgv;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljgv;->aez:I

    .line 4090
    return-object p0
.end method

.method public final ii(Z)Ljgv;
    .locals 1

    .prologue
    .line 4107
    iput-boolean p1, p0, Ljgv;->elb:Z

    .line 4108
    iget v0, p0, Ljgv;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljgv;->aez:I

    .line 4109
    return-object p0
.end method

.method public final ij(Z)Ljgv;
    .locals 1

    .prologue
    .line 4126
    iput-boolean p1, p0, Ljgv;->elc:Z

    .line 4127
    iget v0, p0, Ljgv;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljgv;->aez:I

    .line 4128
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 4271
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 4272
    iget-object v1, p0, Ljgv;->ekX:Ljal;

    if-eqz v1, :cond_0

    .line 4273
    const/4 v1, 0x1

    iget-object v2, p0, Ljgv;->ekX:Ljal;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4276
    :cond_0
    iget v1, p0, Ljgv;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 4277
    const/4 v1, 0x2

    iget v2, p0, Ljgv;->dAq:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4280
    :cond_1
    iget v1, p0, Ljgv;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 4281
    const/4 v1, 0x3

    iget v2, p0, Ljgv;->dAr:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4284
    :cond_2
    iget-object v1, p0, Ljgv;->ekY:Ljbp;

    if-eqz v1, :cond_3

    .line 4285
    const/4 v1, 0x4

    iget-object v2, p0, Ljgv;->ekY:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4288
    :cond_3
    iget v1, p0, Ljgv;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_4

    .line 4289
    const/4 v1, 0x5

    iget-wide v2, p0, Ljgv;->ekZ:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4292
    :cond_4
    iget v1, p0, Ljgv;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_5

    .line 4293
    const/4 v1, 0x6

    iget-boolean v2, p0, Ljgv;->ela:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4296
    :cond_5
    iget v1, p0, Ljgv;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_6

    .line 4297
    const/4 v1, 0x7

    iget-boolean v2, p0, Ljgv;->elb:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4300
    :cond_6
    iget v1, p0, Ljgv;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_7

    .line 4301
    const/16 v1, 0x8

    iget-boolean v2, p0, Ljgv;->elc:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4304
    :cond_7
    iget v1, p0, Ljgv;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    .line 4305
    const/16 v1, 0x9

    iget-boolean v2, p0, Ljgv;->elf:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4308
    :cond_8
    iget-object v1, p0, Ljgv;->elg:[Ljcq;

    if-eqz v1, :cond_b

    iget-object v1, p0, Ljgv;->elg:[Ljcq;

    array-length v1, v1

    if-lez v1, :cond_b

    .line 4309
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljgv;->elg:[Ljcq;

    array-length v2, v2

    if-ge v0, v2, :cond_a

    .line 4310
    iget-object v2, p0, Ljgv;->elg:[Ljcq;

    aget-object v2, v2, v0

    .line 4311
    if-eqz v2, :cond_9

    .line 4312
    const/16 v3, 0xa

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 4309
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_a
    move v0, v1

    .line 4317
    :cond_b
    iget v1, p0, Ljgv;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_c

    .line 4318
    const/16 v1, 0xb

    iget-boolean v2, p0, Ljgv;->eld:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4321
    :cond_c
    iget v1, p0, Ljgv;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_d

    .line 4322
    const/16 v1, 0xc

    iget-boolean v2, p0, Ljgv;->ele:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4325
    :cond_d
    return v0
.end method

.method public final pO(I)Ljgv;
    .locals 1

    .prologue
    .line 4028
    iput p1, p0, Ljgv;->dAq:I

    .line 4029
    iget v0, p0, Ljgv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljgv;->aez:I

    .line 4030
    return-object p0
.end method

.method public final pP(I)Ljgv;
    .locals 1

    .prologue
    .line 4047
    iput p1, p0, Ljgv;->dAr:I

    .line 4048
    iget v0, p0, Ljgv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljgv;->aez:I

    .line 4049
    return-object p0
.end method
