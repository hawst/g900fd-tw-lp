.class public final Lizw;
.super Ljsl;
.source "PG"


# instance fields
.field public dVO:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2137
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2138
    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Lizw;->dVO:[I

    const/4 v0, 0x0

    iput-object v0, p0, Lizw;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lizw;->eCz:I

    .line 2139
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2117
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lizw;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lizw;->dVO:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Lizw;->dVO:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lizw;->dVO:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lizw;->dVO:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Ljsi;->btQ()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Lizw;->dVO:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Lizw;->dVO:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lizw;->dVO:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Lizw;->dVO:[I

    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 2151
    iget-object v0, p0, Lizw;->dVO:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lizw;->dVO:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 2152
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lizw;->dVO:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 2153
    const/4 v1, 0x1

    iget-object v2, p0, Lizw;->dVO:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Ljsj;->bq(II)V

    .line 2152
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2156
    :cond_0
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2157
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2161
    invoke-super {p0}, Ljsl;->lF()I

    move-result v2

    .line 2162
    iget-object v1, p0, Lizw;->dVO:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lizw;->dVO:[I

    array-length v1, v1

    if-lez v1, :cond_1

    move v1, v0

    .line 2164
    :goto_0
    iget-object v3, p0, Lizw;->dVO:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 2165
    iget-object v3, p0, Lizw;->dVO:[I

    aget v3, v3, v0

    .line 2166
    invoke-static {v3}, Ljsj;->sa(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 2164
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2169
    :cond_0
    add-int v0, v2, v1

    .line 2170
    iget-object v1, p0, Lizw;->dVO:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2172
    :goto_1
    return v0

    :cond_1
    move v0, v2

    goto :goto_1
.end method
