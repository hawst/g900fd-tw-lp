.class public Lcmo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private aTF:Z

.field private final bcb:Ljava/lang/StringBuilder;

.field private bcc:Ljava/lang/String;

.field private volatile bcd:I

.field private volatile bce:Leiq;

.field private final bcf:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mConsumer:Lemy;

.field private volatile mHeaders:Ldyj;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcmo;->bcf:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x800

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcmo;->bcb:Ljava/lang/StringBuilder;

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Lcmo;->bcd:I

    .line 68
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/util/Map;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 181
    const/16 v0, 0x3a

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 182
    if-gtz v0, :cond_0

    .line 183
    const-string v0, "Velvet.S3HeaderProcessor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Skipping invalid header: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :goto_0
    return-void

    .line 187
    :cond_0
    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 188
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 190
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 191
    :cond_1
    const-string v0, "Velvet.S3HeaderProcessor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Skipping invalid header: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 195
    :cond_2
    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 196
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 198
    :cond_3
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    aput-object v2, v0, v3

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static hq(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 204
    if-eqz p0, :cond_0

    const-string v1, "HTTP/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 205
    :cond_0
    const-string v1, "Velvet.S3HeaderProcessor"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid status line: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    :goto_0
    return v0

    .line 209
    :cond_1
    const-string v1, " "

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 214
    if-nez v1, :cond_2

    .line 215
    const-string v1, "Velvet.S3HeaderProcessor"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid status line: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 219
    :cond_2
    add-int/lit8 v0, v1, -0x2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    .line 220
    add-int/lit8 v0, v1, 0x3

    .line 223
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-le v0, v2, :cond_3

    .line 224
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 227
    :cond_3
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 229
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v2, v3, :cond_4

    .line 230
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    :cond_4
    move v0, v1

    .line 238
    goto :goto_0
.end method


# virtual methods
.method public final PQ()Ldyj;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 244
    iget-object v0, p0, Lcmo;->mHeaders:Ldyj;

    return-object v0
.end method

.method public final Qf()V
    .locals 4

    .prologue
    .line 114
    iget-boolean v0, p0, Lcmo;->aTF:Z

    if-eqz v0, :cond_1

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcmo;->aTF:Z

    .line 119
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v1

    .line 120
    iget-boolean v0, p0, Lcmo;->aTF:Z

    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, p0, Lcmo;->bcb:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcmo;->bcb:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 124
    :goto_1
    const/16 v2, 0xa

    invoke-static {v0, v2}, Leqw;->b(Ljava/lang/CharSequence;C)Leqw;

    move-result-object v2

    .line 127
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 128
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcmo;->hq(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcmo;->bcd:I

    .line 129
    iget v0, p0, Lcmo;->bcd:I

    const/16 v3, 0xc8

    if-eq v0, v3, :cond_3

    .line 130
    iget v0, p0, Lcmo;->bcd:I

    const/16 v1, 0x12e

    if-ne v0, v1, :cond_0

    .line 131
    const v0, 0x905acc

    invoke-static {v0}, Lhwt;->lx(I)V

    goto :goto_0

    .line 120
    :cond_2
    iget-object v0, p0, Lcmo;->bcc:Ljava/lang/String;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_1

    .line 137
    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 138
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v1}, Lcmo;->b(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_2

    .line 141
    :cond_4
    const-string v0, "Velvet.S3HeaderProcessor"

    const-string v2, "Malformed headers: no status line"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    :cond_5
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    :goto_3
    iput-object v0, p0, Lcmo;->mHeaders:Ldyj;

    goto :goto_0

    :cond_6
    new-instance v0, Ldyj;

    invoke-direct {v0, v1}, Ldyj;-><init>(Ljava/util/Map;)V

    goto :goto_3
.end method

.method public final Qg()V
    .locals 3

    .prologue
    .line 153
    iget-boolean v0, p0, Lcmo;->aTF:Z

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 154
    iget-object v0, p0, Lcmo;->bcf:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 155
    iget-object v0, p0, Lcmo;->bce:Leiq;

    if-eqz v0, :cond_0

    .line 156
    const-string v0, "Velvet.S3HeaderProcessor"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error parsing response headers: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcmo;->bce:Leiq;

    invoke-virtual {v2}, Leiq;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    iget-object v0, p0, Lcmo;->mConsumer:Lemy;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lemy;->aj(Ljava/lang/Object;)Z

    .line 159
    :cond_0
    iget-object v0, p0, Lcmo;->mConsumer:Lemy;

    iget-object v1, p0, Lcmo;->mHeaders:Ldyj;

    invoke-interface {v0, v1}, Lemy;->aj(Ljava/lang/Object;)Z

    .line 161
    :cond_1
    return-void
.end method

.method public final c(Lemy;)V
    .locals 1
    .param p1    # Lemy;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 72
    iget-object v0, p0, Lcmo;->mConsumer:Lemy;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 73
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lemy;

    iput-object v0, p0, Lcmo;->mConsumer:Lemy;

    .line 74
    return-void

    .line 72
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Leiq;)V
    .locals 1

    .prologue
    .line 100
    iput-object p1, p0, Lcmo;->bce:Leiq;

    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcmo;->aTF:Z

    .line 102
    invoke-virtual {p0}, Lcmo;->Qg()V

    .line 103
    return-void
.end method

.method public final getResponseCode()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcmo;->bcd:I

    return v0
.end method

.method public final hp(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 82
    iget-boolean v0, p0, Lcmo;->aTF:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 83
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    iget-object v0, p0, Lcmo;->bcc:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 89
    iput-object p1, p0, Lcmo;->bcc:Ljava/lang/String;

    .line 96
    :goto_1
    return-void

    .line 82
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 91
    :cond_1
    iget-object v0, p0, Lcmo;->bcb:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 92
    iget-object v0, p0, Lcmo;->bcb:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcmo;->bcc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    :cond_2
    iget-object v0, p0, Lcmo;->bcb:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 249
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "S3HeaderProcessor{mComplete:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcmo;->aTF:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mResponseCode:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcmo;->bcd:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
