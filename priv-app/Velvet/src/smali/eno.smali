.class public final Leno;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static e(Leiq;)I
    .locals 2

    .prologue
    const v0, 0x7f0a082d

    .line 69
    invoke-static {p0}, Leno;->k(Leiq;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 70
    const v0, 0x7f0a0948

    .line 91
    :cond_0
    :goto_0
    return v0

    .line 71
    :cond_1
    instance-of v1, p0, Leie;

    if-nez v1, :cond_0

    .line 73
    instance-of v1, p0, Leil;

    if-eqz v1, :cond_2

    .line 74
    const v0, 0x7f0a082c

    goto :goto_0

    .line 75
    :cond_2
    instance-of v1, p0, Lehw;

    if-eqz v1, :cond_3

    .line 76
    const v0, 0x7f0a082f

    goto :goto_0

    .line 77
    :cond_3
    instance-of v1, p0, Lehz;

    if-nez v1, :cond_0

    .line 87
    instance-of v0, p0, Leib;

    if-eqz v0, :cond_4

    .line 88
    const v0, 0x7f0a0830

    goto :goto_0

    .line 91
    :cond_4
    const v0, 0x7f0a082e

    goto :goto_0
.end method

.method public static f(Leiq;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 114
    instance-of v1, p0, Lehz;

    if-nez v1, :cond_0

    invoke-static {p0}, Leno;->k(Leiq;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 120
    :cond_0
    :goto_0
    return v0

    .line 116
    :cond_1
    instance-of v1, p0, Leil;

    if-eqz v1, :cond_0

    .line 118
    const v0, 0x7f02021e

    goto :goto_0
.end method

.method public static g(Leiq;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 125
    instance-of v2, p0, Leie;

    if-eqz v2, :cond_1

    .line 145
    :cond_0
    :goto_0
    return v0

    .line 127
    :cond_1
    invoke-static {p0}, Leno;->k(Leiq;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 131
    instance-of v2, p0, Leil;

    if-eqz v2, :cond_2

    move v0, v1

    .line 132
    goto :goto_0

    .line 133
    :cond_2
    instance-of v2, p0, Lehw;

    if-eqz v2, :cond_3

    move v0, v1

    .line 134
    goto :goto_0

    .line 135
    :cond_3
    instance-of v2, p0, Lehz;

    if-nez v2, :cond_0

    .line 138
    instance-of v2, p0, Leib;

    if-eqz v2, :cond_4

    move v0, v1

    .line 139
    goto :goto_0

    .line 140
    :cond_4
    instance-of v2, p0, Leit;

    if-eqz v2, :cond_0

    move v0, v1

    .line 141
    goto :goto_0
.end method

.method public static h(Leiq;)I
    .locals 1

    .prologue
    .line 150
    instance-of v0, p0, Lehw;

    if-eqz v0, :cond_0

    .line 151
    const/4 v0, 0x3

    .line 160
    :goto_0
    return v0

    .line 152
    :cond_0
    instance-of v0, p0, Leie;

    if-eqz v0, :cond_1

    .line 153
    const/4 v0, 0x2

    goto :goto_0

    .line 154
    :cond_1
    instance-of v0, p0, Leil;

    if-eqz v0, :cond_2

    .line 155
    const/4 v0, 0x7

    goto :goto_0

    .line 156
    :cond_2
    instance-of v0, p0, Leit;

    if-eqz v0, :cond_3

    .line 157
    const/4 v0, 0x6

    goto :goto_0

    .line 160
    :cond_3
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public static i(Leiq;)I
    .locals 1

    .prologue
    .line 165
    instance-of v0, p0, Lehw;

    if-eqz v0, :cond_0

    .line 166
    const/4 v0, 0x5

    .line 173
    :goto_0
    return v0

    .line 167
    :cond_0
    instance-of v0, p0, Leie;

    if-eqz v0, :cond_1

    .line 168
    const/4 v0, 0x4

    goto :goto_0

    .line 169
    :cond_1
    instance-of v0, p0, Leil;

    if-eqz v0, :cond_2

    .line 170
    const/4 v0, 0x1

    goto :goto_0

    .line 173
    :cond_2
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public static j(Leiq;)Z
    .locals 1

    .prologue
    .line 184
    instance-of v0, p0, Leil;

    if-eqz v0, :cond_0

    .line 185
    const/4 v0, 0x1

    .line 187
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k(Leiq;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 192
    instance-of v1, p0, Leil;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Leiq;->ata()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
