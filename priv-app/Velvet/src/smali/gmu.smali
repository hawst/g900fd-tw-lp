.class public final Lgmu;
.super Lgmq;
.source "PG"


# instance fields
.field private final axT:Ljava/lang/String;

.field private final cFx:Z

.field private final cPA:I

.field private final cPu:Z

.field private final cPw:Ljtp;

.field private final cPx:Z

.field private final cPy:Z

.field private final cPz:Z

.field private final mSpeechContext:Lgij;

.field private final mSpeechSettings:Lgdo;


# direct methods
.method public constructor <init>(Ljtp;Lgdo;Lgij;ZZZIZZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 54
    const-string v0, "S3RecognizerInfoBuilderTask"

    invoke-direct {p0, v0}, Lgmq;-><init>(Ljava/lang/String;)V

    .line 55
    iput-object p1, p0, Lgmu;->cPw:Ljtp;

    .line 56
    iput-object p2, p0, Lgmu;->mSpeechSettings:Lgdo;

    .line 57
    iput-object p3, p0, Lgmu;->mSpeechContext:Lgij;

    .line 58
    iput-boolean p4, p0, Lgmu;->cPx:Z

    .line 59
    iput-boolean p5, p0, Lgmu;->cPy:Z

    .line 60
    iput-boolean p6, p0, Lgmu;->cPz:Z

    .line 61
    iput p7, p0, Lgmu;->cPA:I

    .line 62
    iput-boolean p8, p0, Lgmu;->cFx:Z

    .line 63
    iput-boolean p9, p0, Lgmu;->cPu:Z

    .line 64
    iput-object p10, p0, Lgmu;->axT:Ljava/lang/String;

    .line 65
    return-void
.end method

.method private a(Ligi;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 173
    invoke-interface {p1}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 174
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 176
    iget-object v1, p0, Lgmu;->cPw:Ljtp;

    iget-object v1, v1, Ljtp;->eDU:[Ljtq;

    array-length v4, v1

    .line 177
    iget-object v2, p0, Lgmu;->cPw:Ljtp;

    iget-object v1, p0, Lgmu;->cPw:Ljtp;

    iget-object v1, v1, Ljtp;->eDU:[Ljtq;

    const/4 v5, 0x1

    invoke-static {v1, v5}, Leqh;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljtq;

    iput-object v1, v2, Ljtp;->eDU:[Ljtq;

    .line 180
    new-instance v5, Ljty;

    invoke-direct {v5}, Ljty;-><init>()V

    .line 182
    iget-object v1, v5, Ljty;->eEn:[Ljtx;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v1, v2}, Leqh;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljtx;

    iput-object v1, v5, Ljty;->eEn:[Ljtx;

    move v2, v3

    .line 183
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    .line 184
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 186
    iget-object v6, v5, Ljty;->eEn:[Ljtx;

    new-instance v7, Ljtx;

    invoke-direct {v7}, Ljtx;-><init>()V

    invoke-virtual {v7, v1}, Ljtx;->yW(Ljava/lang/String;)Ljtx;

    move-result-object v1

    aput-object v1, v6, v2

    .line 183
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 190
    :cond_0
    new-instance v0, Ljtq;

    invoke-direct {v0}, Ljtq;-><init>()V

    .line 191
    iput-object v5, v0, Ljtq;->eDW:Ljty;

    .line 192
    invoke-virtual {v0, v3}, Ljtq;->sx(I)Ljtq;

    .line 193
    invoke-virtual {v0, p2}, Ljtq;->yU(Ljava/lang/String;)Ljtq;

    .line 194
    iget-object v1, p0, Lgmu;->cPw:Ljtp;

    iget-object v1, v1, Ljtp;->eDU:[Ljtq;

    aput-object v0, v1, v4

    .line 196
    :cond_1
    return-void
.end method

.method static b(Ljze;)Ljtl;
    .locals 2

    .prologue
    .line 105
    new-instance v0, Ljtl;

    invoke-direct {v0}, Ljtl;-><init>()V

    iget-object v1, p0, Ljze;->eNf:Ljzi;

    invoke-virtual {v1}, Ljzi;->buv()I

    move-result v1

    invoke-virtual {v0, v1}, Ljtl;->ss(I)Ljtl;

    move-result-object v0

    iget-object v1, p0, Ljze;->eNf:Ljzi;

    invoke-virtual {v1}, Ljzi;->buw()I

    move-result v1

    invoke-virtual {v0, v1}, Ljtl;->st(I)Ljtl;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljtl;->su(I)Ljtl;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final synthetic aHr()Ljava/lang/Object;
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 29
    new-instance v4, Ljxh;

    invoke-direct {v4}, Ljxh;-><init>()V

    iget-object v0, p0, Lgmu;->cPw:Ljtp;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgmu;->mSpeechContext:Lgij;

    invoke-interface {v0}, Lgij;->aFX()Ligi;

    move-result-object v0

    const-string v1, "generictoken"

    invoke-direct {p0, v0, v1}, Lgmu;->a(Ligi;Ljava/lang/String;)V

    iget-object v0, p0, Lgmu;->mSpeechContext:Lgij;

    invoke-interface {v0}, Lgij;->aFW()Ligi;

    move-result-object v0

    const-string v1, "contactdisambig"

    invoke-direct {p0, v0, v1}, Lgmu;->a(Ligi;Ljava/lang/String;)V

    const-string v5, "topcontacts"

    new-array v6, v3, [Ligi;

    iget-object v0, p0, Lgmu;->mSpeechContext:Lgij;

    invoke-interface {v0}, Lgij;->aFV()Ligi;

    move-result-object v0

    aput-object v0, v6, v2

    iget-object v0, p0, Lgmu;->mSpeechContext:Lgij;

    invoke-interface {v0}, Lgij;->aFU()Ligi;

    move-result-object v0

    aput-object v0, v6, v9

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    array-length v8, v6

    move v1, v2

    :goto_0
    if-ge v1, v8, :cond_1

    aget-object v0, v6, v1

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v7, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lgmu;->cPw:Ljtp;

    iget-object v0, v0, Ljtp;->eDU:[Ljtq;

    array-length v6, v0

    iget-object v1, p0, Lgmu;->cPw:Ljtp;

    iget-object v0, p0, Lgmu;->cPw:Ljtp;

    iget-object v0, v0, Ljtp;->eDU:[Ljtq;

    invoke-static {v0, v9}, Leqh;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljtq;

    iput-object v0, v1, Ljtp;->eDU:[Ljtq;

    new-instance v8, Ljua;

    invoke-direct {v8}, Ljua;-><init>()V

    iget-object v0, v8, Ljua;->eEp:[Ljtz;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Leqh;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljtz;

    iput-object v0, v8, Ljua;->eEp:[Ljtz;

    move v1, v2

    :goto_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v9, v8, Ljua;->eEp:[Ljtz;

    new-instance v10, Ljtz;

    invoke-direct {v10}, Ljtz;-><init>()V

    invoke-virtual {v10, v0}, Ljtz;->yX(Ljava/lang/String;)Ljtz;

    move-result-object v0

    aput-object v0, v9, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    new-instance v0, Ljtq;

    invoke-direct {v0}, Ljtq;-><init>()V

    iput-object v8, v0, Ljtq;->eDY:Ljua;

    invoke-virtual {v0, v3}, Ljtq;->sx(I)Ljtq;

    invoke-virtual {v0, v5}, Ljtq;->yU(Ljava/lang/String;)Ljtq;

    iget-object v1, p0, Lgmu;->mSpeechSettings:Lgdo;

    invoke-interface {v1}, Lgdo;->aFc()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljtq;->yV(Ljava/lang/String;)Ljtq;

    iget-object v1, p0, Lgmu;->cPw:Ljtp;

    iget-object v1, v1, Ljtp;->eDU:[Ljtq;

    aput-object v0, v1, v6

    :cond_3
    iget-object v0, p0, Lgmu;->mSpeechContext:Lgij;

    invoke-interface {v0}, Lgij;->aGa()Ligi;

    move-result-object v0

    const-string v1, "actionstate"

    invoke-direct {p0, v0, v1}, Lgmu;->a(Ligi;Ljava/lang/String;)V

    iget-object v0, p0, Lgmu;->mSpeechContext:Lgij;

    invoke-interface {v0}, Lgij;->aFY()Ligi;

    move-result-object v0

    const-string v1, "handsfree"

    invoke-direct {p0, v0, v1}, Lgmu;->a(Ligi;Ljava/lang/String;)V

    iget-object v0, p0, Lgmu;->cPw:Ljtp;

    iput-object v0, v4, Ljxh;->eKw:Ljtp;

    :cond_4
    iget-boolean v0, p0, Lgmu;->cPx:Z

    invoke-virtual {v4, v0}, Ljxh;->jo(Z)Ljxh;

    iget-boolean v0, p0, Lgmu;->cPy:Z

    invoke-virtual {v4, v0}, Ljxh;->jq(Z)Ljxh;

    iget-boolean v0, p0, Lgmu;->cPy:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lgmu;->cPA:I

    invoke-virtual {v4, v0}, Ljxh;->sS(I)Ljxh;

    :cond_5
    iget-boolean v0, p0, Lgmu;->cPz:Z

    invoke-virtual {v4, v0}, Ljxh;->jp(Z)Ljxh;

    iget-boolean v0, p0, Lgmu;->cPz:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lgmu;->mSpeechSettings:Lgdo;

    invoke-interface {v0}, Lgdo;->aER()Ljze;

    move-result-object v0

    invoke-static {v0}, Lgmu;->b(Ljze;)Ljtl;

    move-result-object v0

    iput-object v0, v4, Ljxh;->eIB:Ljtl;

    :cond_6
    iget-boolean v0, p0, Lgmu;->cPu:Z

    if-eqz v0, :cond_7

    move v2, v3

    :cond_7
    invoke-virtual {v4, v2}, Ljxh;->sT(I)Ljxh;

    iget-object v0, p0, Lgmu;->mSpeechSettings:Lgdo;

    invoke-interface {v0}, Lgdo;->aFa()Z

    move-result v0

    invoke-virtual {v4, v0}, Ljxh;->jr(Z)Ljxh;

    iget-boolean v0, p0, Lgmu;->cFx:Z

    invoke-virtual {v4, v0}, Ljxh;->js(Z)Ljxh;

    iget-object v0, p0, Lgmu;->axT:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lgmu;->axT:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljxh;->zM(Ljava/lang/String;)Ljxh;

    :cond_8
    return-object v4
.end method
