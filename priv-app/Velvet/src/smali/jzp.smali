.class public final Ljzp;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eNY:[Ljzp;


# instance fields
.field private aez:I

.field private dUF:I

.field private eNZ:[Ljava/lang/String;

.field private eNy:Ljava/lang/String;

.field private eOa:Ljava/lang/String;

.field private eOb:I

.field private eOc:Ljava/lang/String;

.field public eOd:[I

.field private eOe:I

.field private eOf:Ljava/lang/String;

.field private eOg:I

.field private eOh:[Ljzq;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4469
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 4470
    iput v1, p0, Ljzp;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljzp;->eNy:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljzp;->eNZ:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljzp;->eOa:Ljava/lang/String;

    iput v1, p0, Ljzp;->dUF:I

    iput v1, p0, Ljzp;->eOb:I

    const-string v0, ""

    iput-object v0, p0, Ljzp;->eOc:Ljava/lang/String;

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljzp;->eOd:[I

    iput v1, p0, Ljzp;->eOe:I

    const-string v0, ""

    iput-object v0, p0, Ljzp;->eOf:Ljava/lang/String;

    iput v1, p0, Ljzp;->eOg:I

    invoke-static {}, Ljzq;->bxp()[Ljzq;

    move-result-object v0

    iput-object v0, p0, Ljzp;->eOh:[Ljzq;

    const/4 v0, 0x0

    iput-object v0, p0, Ljzp;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljzp;->eCz:I

    .line 4471
    return-void
.end method

.method public static bxi()[Ljzp;
    .locals 2

    .prologue
    .line 4283
    sget-object v0, Ljzp;->eNY:[Ljzp;

    if-nez v0, :cond_1

    .line 4284
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 4286
    :try_start_0
    sget-object v0, Ljzp;->eNY:[Ljzp;

    if-nez v0, :cond_0

    .line 4287
    const/4 v0, 0x0

    new-array v0, v0, [Ljzp;

    sput-object v0, Ljzp;->eNY:[Ljzp;

    .line 4289
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4291
    :cond_1
    sget-object v0, Ljzp;->eNY:[Ljzp;

    return-object v0

    .line 4289
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final Aj(Ljava/lang/String;)Ljzp;
    .locals 1

    .prologue
    .line 4302
    if-nez p1, :cond_0

    .line 4303
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4305
    :cond_0
    iput-object p1, p0, Ljzp;->eNy:Ljava/lang/String;

    .line 4306
    iget v0, p0, Ljzp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzp;->aez:I

    .line 4307
    return-object p0
.end method

.method public final Ak(Ljava/lang/String;)Ljzp;
    .locals 1

    .prologue
    .line 4327
    if-nez p1, :cond_0

    .line 4328
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4330
    :cond_0
    iput-object p1, p0, Ljzp;->eOa:Ljava/lang/String;

    .line 4331
    iget v0, p0, Ljzp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzp;->aez:I

    .line 4332
    return-object p0
.end method

.method public final Al(Ljava/lang/String;)Ljzp;
    .locals 1

    .prologue
    .line 4387
    if-nez p1, :cond_0

    .line 4388
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4390
    :cond_0
    iput-object p1, p0, Ljzp;->eOc:Ljava/lang/String;

    .line 4391
    iget v0, p0, Ljzp;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljzp;->aez:I

    .line 4392
    return-object p0
.end method

.method public final Am(Ljava/lang/String;)Ljzp;
    .locals 1

    .prologue
    .line 4431
    if-nez p1, :cond_0

    .line 4432
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4434
    :cond_0
    iput-object p1, p0, Ljzp;->eOf:Ljava/lang/String;

    .line 4435
    iget v0, p0, Ljzp;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljzp;->aez:I

    .line 4436
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 4070
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljzp;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljzp;->eNy:Ljava/lang/String;

    iget v0, p0, Ljzp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzp;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljzp;->eNZ:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljzp;->eNZ:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljzp;->eNZ:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljzp;->eNZ:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljzp;->eOa:Ljava/lang/String;

    iget v0, p0, Ljzp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzp;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzp;->dUF:I

    iget v0, p0, Ljzp;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljzp;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzp;->eOb:I

    iget v0, p0, Ljzp;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljzp;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljzp;->eOc:Ljava/lang/String;

    iget v0, p0, Ljzp;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljzp;->aez:I

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x38

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljzp;->eOd:[I

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljzp;->eOd:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljzp;->eOd:[I

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Ljzp;->eOd:[I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_5
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_7

    invoke-virtual {p1}, Ljsi;->btQ()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_7
    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljzp;->eOd:[I

    if-nez v2, :cond_9

    move v2, v1

    :goto_6
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_8

    iget-object v4, p0, Ljzp;->eOd:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_7
    array-length v4, v0

    if-ge v2, v4, :cond_a

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_9
    iget-object v2, p0, Ljzp;->eOd:[I

    array-length v2, v2

    goto :goto_6

    :cond_a
    iput-object v0, p0, Ljzp;->eOd:[I

    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzp;->eOe:I

    iget v0, p0, Ljzp;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljzp;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljzp;->eOf:Ljava/lang/String;

    iget v0, p0, Ljzp;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljzp;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljzp;->eOg:I

    iget v0, p0, Ljzp;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljzp;->aez:I

    goto/16 :goto_0

    :sswitch_c
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljzp;->eOh:[Ljzq;

    if-nez v0, :cond_c

    move v0, v1

    :goto_8
    add-int/2addr v2, v0

    new-array v2, v2, [Ljzq;

    if-eqz v0, :cond_b

    iget-object v3, p0, Ljzp;->eOh:[Ljzq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_9
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    new-instance v3, Ljzq;

    invoke-direct {v3}, Ljzq;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_c
    iget-object v0, p0, Ljzp;->eOh:[Ljzq;

    array-length v0, v0

    goto :goto_8

    :cond_d
    new-instance v3, Ljzq;

    invoke-direct {v3}, Ljzq;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljzp;->eOh:[Ljzq;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x3a -> :sswitch_8
        0x40 -> :sswitch_9
        0x4a -> :sswitch_a
        0x50 -> :sswitch_b
        0x5a -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4494
    iget v0, p0, Ljzp;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 4495
    const/4 v0, 0x1

    iget-object v2, p0, Ljzp;->eNy:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 4497
    :cond_0
    iget-object v0, p0, Ljzp;->eNZ:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljzp;->eNZ:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 4498
    :goto_0
    iget-object v2, p0, Ljzp;->eNZ:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 4499
    iget-object v2, p0, Ljzp;->eNZ:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 4500
    if-eqz v2, :cond_1

    .line 4501
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 4498
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4505
    :cond_2
    iget v0, p0, Ljzp;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 4506
    const/4 v0, 0x3

    iget-object v2, p0, Ljzp;->eOa:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 4508
    :cond_3
    iget v0, p0, Ljzp;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 4509
    const/4 v0, 0x4

    iget v2, p0, Ljzp;->dUF:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 4511
    :cond_4
    iget v0, p0, Ljzp;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 4512
    const/4 v0, 0x5

    iget v2, p0, Ljzp;->eOb:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 4514
    :cond_5
    iget v0, p0, Ljzp;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    .line 4515
    const/4 v0, 0x6

    iget-object v2, p0, Ljzp;->eOc:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 4517
    :cond_6
    iget-object v0, p0, Ljzp;->eOd:[I

    if-eqz v0, :cond_7

    iget-object v0, p0, Ljzp;->eOd:[I

    array-length v0, v0

    if-lez v0, :cond_7

    move v0, v1

    .line 4518
    :goto_1
    iget-object v2, p0, Ljzp;->eOd:[I

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 4519
    const/4 v2, 0x7

    iget-object v3, p0, Ljzp;->eOd:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Ljsj;->bq(II)V

    .line 4518
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 4522
    :cond_7
    iget v0, p0, Ljzp;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_8

    .line 4523
    const/16 v0, 0x8

    iget v2, p0, Ljzp;->eOe:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 4525
    :cond_8
    iget v0, p0, Ljzp;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_9

    .line 4526
    const/16 v0, 0x9

    iget-object v2, p0, Ljzp;->eOf:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 4528
    :cond_9
    iget v0, p0, Ljzp;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_a

    .line 4529
    const/16 v0, 0xa

    iget v2, p0, Ljzp;->eOg:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 4531
    :cond_a
    iget-object v0, p0, Ljzp;->eOh:[Ljzq;

    if-eqz v0, :cond_c

    iget-object v0, p0, Ljzp;->eOh:[Ljzq;

    array-length v0, v0

    if-lez v0, :cond_c

    .line 4532
    :goto_2
    iget-object v0, p0, Ljzp;->eOh:[Ljzq;

    array-length v0, v0

    if-ge v1, v0, :cond_c

    .line 4533
    iget-object v0, p0, Ljzp;->eOh:[Ljzq;

    aget-object v0, v0, v1

    .line 4534
    if-eqz v0, :cond_b

    .line 4535
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 4532
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4539
    :cond_c
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 4540
    return-void
.end method

.method public final bwQ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4299
    iget-object v0, p0, Ljzp;->eNy:Ljava/lang/String;

    return-object v0
.end method

.method public final bxj()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4324
    iget-object v0, p0, Ljzp;->eOa:Ljava/lang/String;

    return-object v0
.end method

.method public final bxk()Z
    .locals 1

    .prologue
    .line 4335
    iget v0, p0, Ljzp;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bxl()I
    .locals 1

    .prologue
    .line 4365
    iget v0, p0, Ljzp;->eOb:I

    return v0
.end method

.method public final bxm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4384
    iget-object v0, p0, Ljzp;->eOc:Ljava/lang/String;

    return-object v0
.end method

.method public final bxn()I
    .locals 1

    .prologue
    .line 4409
    iget v0, p0, Ljzp;->eOe:I

    return v0
.end method

.method public final bxo()Z
    .locals 1

    .prologue
    .line 4417
    iget v0, p0, Ljzp;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getVersion()I
    .locals 1

    .prologue
    .line 4346
    iget v0, p0, Ljzp;->dUF:I

    return v0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 4544
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 4545
    iget v1, p0, Ljzp;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 4546
    const/4 v1, 0x1

    iget-object v3, p0, Ljzp;->eNy:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4549
    :cond_0
    iget-object v1, p0, Ljzp;->eNZ:[Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljzp;->eNZ:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_3

    move v1, v2

    move v3, v2

    move v4, v2

    .line 4552
    :goto_0
    iget-object v5, p0, Ljzp;->eNZ:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_2

    .line 4553
    iget-object v5, p0, Ljzp;->eNZ:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 4554
    if-eqz v5, :cond_1

    .line 4555
    add-int/lit8 v4, v4, 0x1

    .line 4556
    invoke-static {v5}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 4552
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4560
    :cond_2
    add-int/2addr v0, v3

    .line 4561
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 4563
    :cond_3
    iget v1, p0, Ljzp;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_4

    .line 4564
    const/4 v1, 0x3

    iget-object v3, p0, Ljzp;->eOa:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4567
    :cond_4
    iget v1, p0, Ljzp;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    .line 4568
    const/4 v1, 0x4

    iget v3, p0, Ljzp;->dUF:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4571
    :cond_5
    iget v1, p0, Ljzp;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_6

    .line 4572
    const/4 v1, 0x5

    iget v3, p0, Ljzp;->eOb:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4575
    :cond_6
    iget v1, p0, Ljzp;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_7

    .line 4576
    const/4 v1, 0x6

    iget-object v3, p0, Ljzp;->eOc:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4579
    :cond_7
    iget-object v1, p0, Ljzp;->eOd:[I

    if-eqz v1, :cond_9

    iget-object v1, p0, Ljzp;->eOd:[I

    array-length v1, v1

    if-lez v1, :cond_9

    move v1, v2

    move v3, v2

    .line 4581
    :goto_1
    iget-object v4, p0, Ljzp;->eOd:[I

    array-length v4, v4

    if-ge v1, v4, :cond_8

    .line 4582
    iget-object v4, p0, Ljzp;->eOd:[I

    aget v4, v4, v1

    .line 4583
    invoke-static {v4}, Ljsj;->sa(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 4581
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4586
    :cond_8
    add-int/2addr v0, v3

    .line 4587
    iget-object v1, p0, Ljzp;->eOd:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4589
    :cond_9
    iget v1, p0, Ljzp;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_a

    .line 4590
    const/16 v1, 0x8

    iget v3, p0, Ljzp;->eOe:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4593
    :cond_a
    iget v1, p0, Ljzp;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_b

    .line 4594
    const/16 v1, 0x9

    iget-object v3, p0, Ljzp;->eOf:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4597
    :cond_b
    iget v1, p0, Ljzp;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_c

    .line 4598
    const/16 v1, 0xa

    iget v3, p0, Ljzp;->eOg:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4601
    :cond_c
    iget-object v1, p0, Ljzp;->eOh:[Ljzq;

    if-eqz v1, :cond_e

    iget-object v1, p0, Ljzp;->eOh:[Ljzq;

    array-length v1, v1

    if-lez v1, :cond_e

    .line 4602
    :goto_2
    iget-object v1, p0, Ljzp;->eOh:[Ljzq;

    array-length v1, v1

    if-ge v2, v1, :cond_e

    .line 4603
    iget-object v1, p0, Ljzp;->eOh:[Ljzq;

    aget-object v1, v1, v2

    .line 4604
    if-eqz v1, :cond_d

    .line 4605
    const/16 v3, 0xb

    invoke-static {v3, v1}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4602
    :cond_d
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 4610
    :cond_e
    return v0
.end method

.method public final tA(I)Ljzp;
    .locals 1

    .prologue
    .line 4412
    iput p1, p0, Ljzp;->eOe:I

    .line 4413
    iget v0, p0, Ljzp;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljzp;->aez:I

    .line 4414
    return-object p0
.end method

.method public final ty(I)Ljzp;
    .locals 1

    .prologue
    .line 4349
    iput p1, p0, Ljzp;->dUF:I

    .line 4350
    iget v0, p0, Ljzp;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljzp;->aez:I

    .line 4351
    return-object p0
.end method

.method public final tz(I)Ljzp;
    .locals 1

    .prologue
    .line 4368
    iput p1, p0, Ljzp;->eOb:I

    .line 4369
    iget v0, p0, Ljzp;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljzp;->aez:I

    .line 4370
    return-object p0
.end method
