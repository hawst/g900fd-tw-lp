.class final Lhso;
.super Lepm;
.source "PG"


# instance fields
.field private synthetic dpX:Lhsp;

.field private synthetic dpY:Lhsm;

.field private synthetic dpZ:Lhhv;


# direct methods
.method varargs constructor <init>(Lhsm;Ljava/lang/String;[ILhhv;Lhsp;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lhso;->dpY:Lhsm;

    iput-object p4, p0, Lhso;->dpZ:Lhhv;

    iput-object p5, p0, Lhso;->dpX:Lhsp;

    invoke-direct {p0, p2, p3}, Lepm;-><init>(Ljava/lang/String;[I)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 73
    iget-object v0, p0, Lhso;->dpY:Lhsm;

    iget-boolean v0, v0, Lhsm;->dpW:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lhso;->dpY:Lhsm;

    iget-object v0, v0, Lhsm;->mBluetoothController:Lhjb;

    invoke-virtual {v0}, Lhjb;->aPO()I

    move-result v0

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    .line 79
    :goto_0
    iget-object v3, p0, Lhso;->dpY:Lhsm;

    iget-object v3, v3, Lhsm;->mAudioRouter:Lhhu;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iget-object v4, p0, Lhso;->dpZ:Lhhv;

    invoke-interface {v3, v0, v2, v4, v1}, Lhhu;->a(IILhhv;Z)V

    .line 83
    iget-object v0, p0, Lhso;->dpY:Lhsm;

    iget-object v0, v0, Lhsm;->mAudioRouter:Lhhu;

    invoke-interface {v0, v5}, Lhhu;->nY(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 85
    iget-object v0, p0, Lhso;->dpX:Lhsp;

    invoke-interface {v0}, Lhsp;->aRQ()V

    .line 98
    :goto_2
    return-void

    :cond_0
    move v0, v1

    .line 73
    goto :goto_0

    :cond_1
    move v0, v2

    .line 79
    goto :goto_1

    .line 86
    :cond_2
    iget-object v0, p0, Lhso;->dpY:Lhsm;

    iget-object v0, v0, Lhsm;->mBluetoothController:Lhjb;

    invoke-virtual {v0}, Lhjb;->aPO()I

    move-result v0

    if-ne v0, v2, :cond_3

    .line 89
    const-string v0, "VS.AudioRouterHandsfree"

    const-string v1, "VOICE_COMMAND intent not fired from a BT device"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iget-object v0, p0, Lhso;->dpX:Lhsp;

    invoke-interface {v0}, Lhsp;->aRQ()V

    goto :goto_2

    .line 92
    :cond_3
    const-string v0, "VS.AudioRouterHandsfree"

    const-string v2, "Bluetooth routing failed"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iget-object v0, p0, Lhso;->dpY:Lhsm;

    iget-object v0, v0, Lhsm;->mAudioRouter:Lhhu;

    const/4 v2, 0x3

    invoke-interface {v0, v2, v1, v5, v1}, Lhhu;->a(IILhhv;Z)V

    .line 96
    iget-object v0, p0, Lhso;->dpX:Lhsp;

    invoke-interface {v0}, Lhsp;->aRR()V

    goto :goto_2
.end method
