.class public final Ljxs;
.super Lbqv;


# instance fields
.field private eKZ:[Ljava/lang/String;

.field private eLa:[Ljava/lang/String;

.field private eLb:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lbqv;-><init>()V

    sget-object v0, Lbrc;->aHu:[Ljava/lang/String;

    iput-object v0, p0, Ljxs;->eKZ:[Ljava/lang/String;

    sget-object v0, Lbrc;->aHu:[Ljava/lang/String;

    iput-object v0, p0, Ljxs;->eLa:[Ljava/lang/String;

    sget-object v0, Lbrc;->aHt:[I

    iput-object v0, p0, Ljxs;->eLb:[I

    const/4 v0, 0x0

    iput-object v0, p0, Ljxs;->aHk:Lbqw;

    const/4 v0, -0x1

    iput v0, p0, Ljxs;->aHr:I

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljxs;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljxs;

    iget-object v2, p0, Ljxs;->eKZ:[Ljava/lang/String;

    iget-object v3, p1, Ljxs;->eKZ:[Ljava/lang/String;

    invoke-static {v2, v3}, Lbqy;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Ljxs;->eLa:[Ljava/lang/String;

    iget-object v3, p1, Ljxs;->eLa:[Ljava/lang/String;

    invoke-static {v2, v3}, Lbqy;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Ljxs;->eLb:[I

    iget-object v3, p1, Ljxs;->eLb:[I

    invoke-static {v2, v3}, Lbqy;->equals([I[I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget-object v0, p0, Ljxs;->eKZ:[Ljava/lang/String;

    invoke-static {v0}, Lbqy;->hashCode([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Ljxs;->eLa:[Ljava/lang/String;

    invoke-static {v1}, Lbqy;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Ljxs;->eLb:[I

    invoke-static {v1}, Lbqy;->hashCode([I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    add-int/lit8 v0, v0, 0x0

    return v0
.end method
