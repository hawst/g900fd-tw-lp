.class final Lvv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private synthetic It:I

.field private synthetic Iu:Lvs;


# direct methods
.method constructor <init>(Lvs;I)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lvv;->Iu:Lvs;

    iput p2, p0, Lvv;->It:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 268
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 269
    iget-object v1, p0, Lvv;->Iu:Lvs;

    sub-float v2, v4, v0

    const v3, 0x3e99999a    # 0.3f

    mul-float/2addr v2, v3

    add-float/2addr v2, v4

    iget v3, p0, Lvv;->It:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iput v2, v1, Lvs;->Il:F

    .line 270
    iget-object v1, p0, Lvv;->Iu:Lvs;

    sub-float v0, v4, v0

    const v2, 0x3e19999a    # 0.15f

    mul-float/2addr v0, v2

    add-float/2addr v0, v4

    iget v2, p0, Lvv;->It:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    iput v0, v1, Lvs;->Im:F

    .line 271
    iget-object v0, p0, Lvv;->Iu:Lvs;

    invoke-static {v0}, Lvs;->c(Lvs;)Lcom/android/launcher3/CellLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lvv;->Iu:Lvs;

    invoke-static {v0}, Lvs;->c(Lvs;)Lcom/android/launcher3/CellLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->invalidate()V

    .line 274
    :cond_0
    return-void
.end method
