.class public final Lixo;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dOd:J

.field private dOe:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 23731
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 23732
    const/4 v0, 0x0

    iput v0, p0, Lixo;->aez:I

    iput-wide v2, p0, Lixo;->dOd:J

    iput-wide v2, p0, Lixo;->dOe:J

    const/4 v0, 0x0

    iput-object v0, p0, Lixo;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lixo;->eCz:I

    .line 23733
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 23674
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lixo;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lixo;->dOd:J

    iget v0, p0, Lixo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lixo;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lixo;->dOe:J

    iget v0, p0, Lixo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lixo;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 23747
    iget v0, p0, Lixo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 23748
    const/4 v0, 0x1

    iget-wide v2, p0, Lixo;->dOd:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 23750
    :cond_0
    iget v0, p0, Lixo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 23751
    const/4 v0, 0x2

    iget-wide v2, p0, Lixo;->dOe:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 23753
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 23754
    return-void
.end method

.method public final baB()J
    .locals 2

    .prologue
    .line 23715
    iget-wide v0, p0, Lixo;->dOe:J

    return-wide v0
.end method

.method public final baC()Z
    .locals 1

    .prologue
    .line 23723
    iget v0, p0, Lixo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 23758
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 23759
    iget v1, p0, Lixo;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 23760
    const/4 v1, 0x1

    iget-wide v2, p0, Lixo;->dOd:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 23763
    :cond_0
    iget v1, p0, Lixo;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 23764
    const/4 v1, 0x2

    iget-wide v2, p0, Lixo;->dOe:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 23767
    :cond_1
    return v0
.end method
