.class public final Lesi;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final cii:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-string v0, "\\d{1,2}\\/\\d{1,2}\\/\\d{4}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lesi;->cii:Ljava/util/regex/Pattern;

    return-void
.end method

.method private static a(Ljhl;)J
    .locals 4

    .prologue
    .line 449
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljhl;->blS()Z

    move-result v0

    if-nez v0, :cond_1

    .line 450
    :cond_0
    const-wide/16 v0, 0x0

    .line 461
    :goto_0
    return-wide v0

    .line 452
    :cond_1
    const-wide/16 v0, 0x3e8

    invoke-virtual {p0}, Ljhl;->akO()J

    move-result-wide v2

    mul-long/2addr v2, v0

    .line 455
    invoke-virtual {p0}, Ljhl;->blU()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Ljhl;->blT()Ljava/lang/String;

    move-result-object v0

    .line 457
    :goto_1
    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 458
    if-eqz v0, :cond_3

    .line 459
    invoke-virtual {v0, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, v2

    goto :goto_0

    .line 455
    :cond_2
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-wide v0, v2

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;JI)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 348
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lesi;->a(Landroid/content/Context;JIZ)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;JIZ)Ljava/lang/CharSequence;
    .locals 9

    .prologue
    const-wide/32 v6, 0xa4cb800

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 358
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 359
    invoke-static {p1, p2}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 360
    invoke-static {p3}, Lesi;->im(I)I

    move-result v0

    invoke-static {p0, p1, p2, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 361
    if-eqz p4, :cond_0

    .line 362
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0422

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 402
    :cond_0
    :goto_0
    return-object v0

    .line 366
    :cond_1
    invoke-static {p1, p2, v0, v1}, Lesi;->g(JJ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 371
    invoke-static {p3}, Lesi;->im(I)I

    move-result v0

    invoke-static {p0, p1, p2, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 372
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0423

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 373
    :cond_2
    invoke-static {v0, v1, p1, p2}, Lesi;->g(JJ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 374
    invoke-static {p3}, Lesi;->im(I)I

    move-result v0

    invoke-static {p0, p1, p2, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 375
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0424

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 381
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long v0, p1, v0

    cmp-long v0, v0, v6

    if-lez v0, :cond_5

    .line 382
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-gt v0, v1, :cond_4

    .line 383
    or-int/lit8 v0, p3, 0x10

    or-int/lit8 v0, v0, 0x1

    invoke-static {p0, p1, p2, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 386
    :cond_4
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_5

    .line 387
    invoke-static {p0, p1, p2, p3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 399
    :cond_5
    const-wide/32 v4, 0x5265c00

    or-int/lit8 v8, p3, 0x10

    move-object v1, p0

    move-wide v2, p1

    invoke-static/range {v1 .. v8}, Landroid/text/format/DateUtils;->getRelativeDateTimeString(Landroid/content/Context;JJJI)Ljava/lang/CharSequence;

    move-result-object v0

    .line 402
    invoke-static {p0, v0, p1, p2}, Lesi;->a(Landroid/content/Context;Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 475
    sget-object v0, Lesi;->cii:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 476
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 482
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 483
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, p2, p3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 485
    :cond_0
    return-object p1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;IZZ)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    .line 318
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 319
    invoke-virtual {v2, p1}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, v2, Landroid/text/format/Time;->allDay:Z

    if-nez v0, :cond_0

    .line 321
    const-string v0, "UTC"

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 323
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 324
    iget-boolean v2, v2, Landroid/text/format/Time;->allDay:Z

    if-eqz v2, :cond_3

    .line 325
    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 326
    const v0, 0x7f0a03fe

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 338
    :goto_0
    return-object v0

    .line 329
    :cond_1
    if-eqz p4, :cond_2

    const-wide/32 v4, 0x5265c00

    .line 331
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    or-int/lit8 v6, p2, 0x10

    invoke-static/range {v0 .. v6}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJI)Ljava/lang/CharSequence;

    move-result-object v2

    .line 334
    invoke-static {p0, v2, v0, v1}, Lesi;->a(Landroid/content/Context;Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 329
    :cond_2
    const-wide/32 v4, 0x240c8400

    goto :goto_1

    .line 338
    :cond_3
    invoke-static {p0, v0, v1, p2, p3}, Lesi;->a(Landroid/content/Context;JIZ)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 281
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 282
    invoke-virtual {v0, p1}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    .line 283
    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 285
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 286
    invoke-virtual {v1, p2}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    .line 287
    invoke-virtual {v1, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 289
    iget-boolean v0, v0, Landroid/text/format/Time;->allDay:Z

    if-eqz v0, :cond_1

    iget-boolean v0, v1, Landroid/text/format/Time;->allDay:Z

    if-eqz v0, :cond_1

    .line 292
    const-wide/16 v0, 0x3e8

    add-long/2addr v4, v0

    .line 297
    :goto_0
    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v4, v5}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v0

    if-nez v0, :cond_2

    .line 298
    :cond_0
    or-int/lit8 v6, p3, 0x10

    :goto_1
    move-object v1, p0

    .line 301
    invoke-static/range {v1 .. v6}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 294
    :cond_1
    or-int/lit8 p3, p3, 0x1

    goto :goto_0

    :cond_2
    move v6, p3

    goto :goto_1
.end method

.method public static a(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/16 v3, 0x11

    const/16 v2, 0xc

    const/4 v1, 0x5

    .line 596
    const/4 v0, 0x2

    if-lt p0, v0, :cond_0

    if-ge p0, v1, :cond_0

    .line 605
    :goto_0
    return-object p5

    .line 598
    :cond_0
    if-lt p0, v1, :cond_1

    if-ge p0, v2, :cond_1

    move-object p5, p1

    .line 599
    goto :goto_0

    .line 600
    :cond_1
    if-lt p0, v2, :cond_2

    if-ge p0, v3, :cond_2

    move-object p5, p2

    .line 601
    goto :goto_0

    .line 602
    :cond_2
    if-lt p0, v3, :cond_3

    const/16 v0, 0x14

    if-ge p0, v0, :cond_3

    move-object p5, p3

    .line 603
    goto :goto_0

    :cond_3
    move-object p5, p4

    .line 605
    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 588
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 589
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 590
    invoke-static/range {v0 .. v5}, Lesi;->a(ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;IJZ)Ljava/lang/String;
    .locals 4

    .prologue
    .line 127
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p0, p2, p3, p4}, Lesi;->a(Landroid/content/Context;JZ)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, p1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;J)Ljava/lang/String;
    .locals 9

    .prologue
    .line 194
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 195
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v0

    .line 196
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v2

    .line 198
    const-wide/16 v4, 0x3c

    mul-long/2addr v4, v0

    sub-long v4, p1, v4

    .line 199
    const-wide/16 v6, 0x3c

    mul-long/2addr v6, v2

    sub-long/2addr v0, v6

    .line 200
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-lez v6, :cond_1

    .line 201
    const v6, 0x7f0a0920

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v7, v8

    const/4 v2, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v7, v2

    const/4 v0, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v7, v0

    invoke-virtual {p0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 206
    :goto_1
    return-object v0

    .line 194
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 203
    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 204
    const v2, 0x7f0a0921

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v6

    const/4 v0, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 206
    :cond_2
    const v0, 0x7f0a0922

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;JZ)Ljava/lang/String;
    .locals 11

    .prologue
    const-wide v8, 0x757b12c00L

    const-wide v6, 0x9a7ec800L

    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 69
    const-wide/32 v0, 0x36ee80

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    .line 70
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v0

    long-to-int v0, v0

    .line 71
    if-gtz v0, :cond_9

    move v1, v2

    .line 74
    :goto_0
    if-eqz p3, :cond_0

    const v0, 0x7f100012

    .line 76
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v5

    invoke-virtual {v3, v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 96
    :goto_2
    return-object v0

    .line 74
    :cond_0
    const v0, 0x7f100011

    goto :goto_1

    .line 77
    :cond_1
    const-wide/32 v0, 0x5265c00

    cmp-long v0, p1, v0

    if-gez v0, :cond_3

    .line 78
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v0

    long-to-int v1, v0

    .line 79
    if-eqz p3, :cond_2

    const v0, 0x7f100010

    .line 81
    :goto_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v5

    invoke-virtual {v3, v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 79
    :cond_2
    const v0, 0x7f10000f

    goto :goto_3

    .line 82
    :cond_3
    cmp-long v0, p1, v6

    if-gez v0, :cond_5

    .line 83
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v0

    long-to-int v1, v0

    .line 84
    if-eqz p3, :cond_4

    const v0, 0x7f100014

    .line 86
    :goto_4
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v5

    invoke-virtual {v3, v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 84
    :cond_4
    const v0, 0x7f100013

    goto :goto_4

    .line 87
    :cond_5
    cmp-long v0, p1, v8

    if-gez v0, :cond_7

    .line 88
    div-long v0, p1, v6

    long-to-int v1, v0

    .line 89
    if-eqz p3, :cond_6

    const v0, 0x7f100016

    .line 91
    :goto_5
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v5

    invoke-virtual {v3, v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 89
    :cond_6
    const v0, 0x7f100015

    goto :goto_5

    .line 93
    :cond_7
    div-long v0, p1, v8

    long-to-int v1, v0

    .line 94
    if-eqz p3, :cond_8

    const v0, 0x7f100018

    .line 96
    :goto_6
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v5

    invoke-virtual {v3, v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 94
    :cond_8
    const v0, 0x7f100017

    goto :goto_6

    :cond_9
    move v1, v0

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;Ljhl;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 411
    invoke-static {p1}, Lesi;->a(Ljhl;)J

    move-result-wide v2

    .line 412
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    .line 413
    const/4 v0, 0x0

    .line 426
    :goto_0
    return-object v0

    .line 415
    :cond_0
    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 416
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a03fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 417
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Lesi;->g(JJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 418
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0416

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 420
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 421
    new-instance v1, Ljava/util/Formatter;

    invoke-direct {v1, v0}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;)V

    .line 422
    const v6, 0x8001a

    const-string v7, "UTC"

    move-object v0, p0

    move-wide v4, v2

    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    .line 426
    invoke-virtual {v1}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static aW(J)Ljava/lang/String;
    .locals 6

    .prologue
    .line 222
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 223
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 224
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%tm/%td %tk:%tM:%tS"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    const/4 v4, 0x2

    aput-object v0, v3, v4

    const/4 v4, 0x3

    aput-object v0, v3, v4

    const/4 v4, 0x4

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static aX(J)Ljava/lang/String;
    .locals 10

    .prologue
    .line 232
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 233
    invoke-static {p0, p1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 234
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s%d:%tM:%tS.%tL"

    const/4 v0, 0x5

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    cmp-long v0, p0, v6

    if-gez v0, :cond_0

    const-string v0, "-"

    :goto_0
    aput-object v0, v4, v5

    const/4 v0, 0x1

    invoke-static {p0, p1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    const-wide/32 v8, 0x36ee80

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x2

    aput-object v1, v4, v0

    const/4 v0, 0x3

    aput-object v1, v4, v0

    const/4 v0, 0x4

    aput-object v1, v4, v0

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static aY(J)Z
    .locals 2

    .prologue
    .line 492
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p0, p1, v0, v1}, Lesi;->g(JJ)Z

    move-result v0

    return v0
.end method

.method public static aZ(J)Z
    .locals 2

    .prologue
    .line 549
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p0, p1, v0, v1}, Lesi;->i(JJ)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    cmp-long v0, p0, v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static avC()Z
    .locals 3

    .prologue
    .line 614
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 615
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 616
    iget v1, v0, Landroid/text/format/Time;->hour:I

    const/4 v2, 0x6

    if-lt v1, v2, :cond_0

    iget v0, v0, Landroid/text/format/Time;->hour:I

    const/16 v1, 0x12

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;JZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 110
    const v0, 0x7f0a0400

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0, p1, p2, p3}, Lesi;->a(Landroid/content/Context;JZ)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;Ljhl;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 434
    invoke-static {p1}, Lesi;->a(Ljhl;)J

    move-result-wide v2

    .line 435
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    .line 436
    const/4 v0, 0x0

    .line 442
    :goto_0
    return-object v0

    .line 438
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 439
    new-instance v1, Ljava/util/Formatter;

    invoke-direct {v1, v0}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;)V

    .line 440
    const/4 v6, 0x1

    const-string v7, "UTC"

    move-object v0, p0

    move-wide v4, v2

    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    .line 442
    invoke-virtual {v1}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Ljkh;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/16 v2, 0x1e

    .line 623
    if-nez p0, :cond_1

    .line 635
    :cond_0
    :goto_0
    return-void

    .line 626
    :cond_1
    invoke-virtual {p0}, Ljkh;->getMinute()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Ljkh;->getMinute()I

    move-result v1

    if-eq v1, v2, :cond_0

    .line 627
    invoke-virtual {p0}, Ljkh;->getMinute()I

    move-result v1

    if-ge v1, v2, :cond_2

    .line 628
    invoke-virtual {p0, v2}, Ljkh;->qu(I)Ljkh;

    goto :goto_0

    .line 630
    :cond_2
    invoke-virtual {p0, v0}, Ljkh;->qu(I)Ljkh;

    .line 631
    invoke-virtual {p0}, Ljkh;->getHour()I

    move-result v1

    const/16 v2, 0x17

    if-ne v1, v2, :cond_3

    :goto_1
    invoke-virtual {p0, v0}, Ljkh;->qt(I)Ljkh;

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Ljkh;->getHour()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static ba(J)Z
    .locals 2

    .prologue
    .line 553
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    cmp-long v0, v0, p0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;IZ)Ljava/lang/String;
    .locals 11

    .prologue
    const v10, 0x7f100011

    const v0, 0x7f10000f

    const/4 v4, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 139
    const/16 v1, 0x2760

    if-lt p1, v1, :cond_0

    .line 141
    const v0, 0x7f0a0174

    new-array v1, v9, [Ljava/lang/Object;

    div-int/lit16 v2, p1, 0x5a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 168
    :goto_0
    return-object v0

    .line 142
    :cond_0
    const/16 v1, 0x5a0

    if-lt p1, v1, :cond_1

    .line 144
    const v0, 0x7f0a0175

    new-array v1, v4, [Ljava/lang/Object;

    div-int/lit16 v2, p1, 0x5a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    rem-int/lit16 v2, p1, 0x5a0

    div-int/lit8 v2, v2, 0x3c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v9

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 147
    :cond_1
    const/16 v1, 0x3c

    if-lt p1, v1, :cond_5

    .line 148
    div-int/lit8 v1, p1, 0x3c

    .line 149
    rem-int/lit8 v2, p1, 0x3c

    .line 150
    if-lez v2, :cond_3

    .line 152
    if-eqz p2, :cond_2

    const v0, 0x7f0a0178

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v9

    invoke-virtual {p0, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const v3, 0x7f0a0181

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v5, v0, v1, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v8

    invoke-virtual {v0, v10, v2, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v9

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 162
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz p2, :cond_4

    const v0, 0x7f100010

    :cond_4
    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v2, v0, v1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 168
    :cond_5
    if-eqz p2, :cond_6

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f100012

    new-array v2, v9, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {v0, v10, p1, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static g(JJ)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 503
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 504
    invoke-virtual {v1, p0, p1}, Landroid/text/format/Time;->set(J)V

    .line 505
    iget-wide v2, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {p0, p1, v2, v3}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    .line 506
    invoke-virtual {v1, p2, p3}, Landroid/text/format/Time;->set(J)V

    .line 507
    iget-wide v4, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {p2, p3, v4, v5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v1

    .line 509
    sub-int v1, v2, v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(JJ)Z
    .locals 2

    .prologue
    .line 520
    invoke-static {p2, p3, p0, p1}, Lesi;->g(JJ)Z

    move-result v0

    return v0
.end method

.method public static i(JJ)Z
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v0, 0x1

    .line 539
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 540
    invoke-virtual {v1, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 541
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 542
    invoke-virtual {v2, p2, p3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 544
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static im(I)I
    .locals 1

    .prologue
    .line 469
    and-int/lit8 v0, p0, -0x11

    and-int/lit8 v0, v0, -0x3

    or-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public static isToday(J)Z
    .locals 2

    .prologue
    .line 528
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p0, p1, v0, v1}, Lesi;->i(JJ)Z

    move-result v0

    return v0
.end method

.method public static j(JJ)Z
    .locals 4

    .prologue
    .line 569
    const-wide/32 v0, 0x240c8400

    add-long/2addr v0, p0

    .line 572
    cmp-long v2, p0, p2

    if-gtz v2, :cond_0

    cmp-long v2, p2, v0

    if-gtz v2, :cond_0

    invoke-static {v0, v1, p2, p3}, Lesi;->i(JJ)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k(JJ)I
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x1

    .line 644
    new-instance v0, Ljava/util/GregorianCalendar;

    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 645
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 646
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 647
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 649
    new-instance v0, Ljava/util/GregorianCalendar;

    const-string v3, "GMT"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 650
    invoke-virtual {v0, p2, p3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 651
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 652
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 654
    sub-int v1, v3, v1

    .line 655
    if-ltz v1, :cond_0

    if-ne v2, v0, :cond_0

    move v0, v1

    .line 661
    :goto_0
    return v0

    .line 657
    :cond_0
    add-int/lit8 v3, v2, 0x1

    if-ne v3, v0, :cond_2

    .line 658
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    check-cast v0, Ljava/util/GregorianCalendar;

    .line 659
    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->isLeapYear(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x16e

    :goto_1
    add-int/2addr v0, v1

    goto :goto_0

    :cond_1
    const/16 v0, 0x16d

    goto :goto_1

    .line 661
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method
