.class public final Ljhy;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile emA:[Ljhy;


# instance fields
.field private aez:I

.field private emB:I

.field private emC:I

.field private emD:J

.field private emE:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 41145
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 41146
    iput v2, p0, Ljhy;->aez:I

    iput v2, p0, Ljhy;->emB:I

    iput v2, p0, Ljhy;->emC:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljhy;->emD:J

    iput v2, p0, Ljhy;->emE:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljhy;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljhy;->eCz:I

    .line 41147
    return-void
.end method

.method public static bmq()[Ljhy;
    .locals 2

    .prologue
    .line 41056
    sget-object v0, Ljhy;->emA:[Ljhy;

    if-nez v0, :cond_1

    .line 41057
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 41059
    :try_start_0
    sget-object v0, Ljhy;->emA:[Ljhy;

    if-nez v0, :cond_0

    .line 41060
    const/4 v0, 0x0

    new-array v0, v0, [Ljhy;

    sput-object v0, Ljhy;->emA:[Ljhy;

    .line 41062
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41064
    :cond_1
    sget-object v0, Ljhy;->emA:[Ljhy;

    return-object v0

    .line 41062
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 41050
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljhy;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljhy;->emB:I

    iget v0, p0, Ljhy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhy;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljhy;->emC:I

    iget v0, p0, Ljhy;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhy;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljhy;->emD:J

    iget v0, p0, Ljhy;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljhy;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btP()I

    move-result v0

    iput v0, p0, Ljhy;->emE:I

    iget v0, p0, Ljhy;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljhy;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 41163
    iget v0, p0, Ljhy;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 41164
    const/4 v0, 0x1

    iget v1, p0, Ljhy;->emB:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 41166
    :cond_0
    iget v0, p0, Ljhy;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 41167
    const/4 v0, 0x2

    iget v1, p0, Ljhy;->emC:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 41169
    :cond_1
    iget v0, p0, Ljhy;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 41170
    const/4 v0, 0x3

    iget-wide v2, p0, Ljhy;->emD:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 41172
    :cond_2
    iget v0, p0, Ljhy;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 41173
    const/4 v0, 0x4

    iget v1, p0, Ljhy;->emE:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bu(II)V

    .line 41175
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 41176
    return-void
.end method

.method public final bmr()I
    .locals 1

    .prologue
    .line 41091
    iget v0, p0, Ljhy;->emC:I

    return v0
.end method

.method public final bms()Z
    .locals 1

    .prologue
    .line 41099
    iget v0, p0, Ljhy;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bmt()J
    .locals 2

    .prologue
    .line 41110
    iget-wide v0, p0, Ljhy;->emD:J

    return-wide v0
.end method

.method public final bmu()Z
    .locals 1

    .prologue
    .line 41118
    iget v0, p0, Ljhy;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bmv()I
    .locals 1

    .prologue
    .line 41129
    iget v0, p0, Ljhy;->emE:I

    return v0
.end method

.method public final bmw()Z
    .locals 1

    .prologue
    .line 41137
    iget v0, p0, Ljhy;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dn(J)Ljhy;
    .locals 2

    .prologue
    .line 41113
    const-wide/32 v0, 0x52af68fa

    iput-wide v0, p0, Ljhy;->emD:J

    .line 41114
    iget v0, p0, Ljhy;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljhy;->aez:I

    .line 41115
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 41180
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 41181
    iget v1, p0, Ljhy;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 41182
    const/4 v1, 0x1

    iget v2, p0, Ljhy;->emB:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 41185
    :cond_0
    iget v1, p0, Ljhy;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 41186
    const/4 v1, 0x2

    iget v2, p0, Ljhy;->emC:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 41189
    :cond_1
    iget v1, p0, Ljhy;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 41190
    const/4 v1, 0x3

    iget-wide v2, p0, Ljhy;->emD:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 41193
    :cond_2
    iget v1, p0, Ljhy;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 41194
    const/4 v1, 0x4

    iget v2, p0, Ljhy;->emE:I

    invoke-static {v1, v2}, Ljsj;->bx(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 41197
    :cond_3
    return v0
.end method

.method public final qa(I)Ljhy;
    .locals 1

    .prologue
    .line 41094
    iput p1, p0, Ljhy;->emC:I

    .line 41095
    iget v0, p0, Ljhy;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhy;->aez:I

    .line 41096
    return-object p0
.end method
