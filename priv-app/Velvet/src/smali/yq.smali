.class public final enum Lyq;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum LU:Lyq;

.field public static final enum LV:Lyq;

.field public static final enum LW:Lyq;

.field public static final enum LX:Lyq;

.field private static final synthetic LY:[Lyq;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 228
    new-instance v0, Lyq;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lyq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lyq;->LU:Lyq;

    new-instance v0, Lyq;

    const-string v1, "WORKSPACE"

    invoke-direct {v0, v1, v3}, Lyq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lyq;->LV:Lyq;

    new-instance v0, Lyq;

    const-string v1, "APPS_CUSTOMIZE"

    invoke-direct {v0, v1, v4}, Lyq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lyq;->LW:Lyq;

    new-instance v0, Lyq;

    const-string v1, "APPS_CUSTOMIZE_SPRING_LOADED"

    invoke-direct {v0, v1, v5}, Lyq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lyq;->LX:Lyq;

    const/4 v0, 0x4

    new-array v0, v0, [Lyq;

    sget-object v1, Lyq;->LU:Lyq;

    aput-object v1, v0, v2

    sget-object v1, Lyq;->LV:Lyq;

    aput-object v1, v0, v3

    sget-object v1, Lyq;->LW:Lyq;

    aput-object v1, v0, v4

    sget-object v1, Lyq;->LX:Lyq;

    aput-object v1, v0, v5

    sput-object v0, Lyq;->LY:[Lyq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 228
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lyq;
    .locals 1

    .prologue
    .line 228
    const-class v0, Lyq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lyq;

    return-object v0
.end method

.method public static values()[Lyq;
    .locals 1

    .prologue
    .line 228
    sget-object v0, Lyq;->LY:[Lyq;

    invoke-virtual {v0}, [Lyq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lyq;

    return-object v0
.end method
