.class public final Lggr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ligi;


# instance fields
.field private final aYM:Lcom/google/android/search/shared/contact/Relationship;

.field private final bRJ:Ljava/lang/String;

.field private final bSr:Ldzb;

.field private final cJJ:Ljava/util/List;

.field private final cJK:Ljop;

.field public cJL:Lifw;

.field private final mContactLookup:Lghy;

.field private final mGsaConfigFlags:Lchk;

.field private final mQuery:Lcom/google/android/shared/search/Query;

.field private final mRelationshipManager:Lcjg;

.field private final mRelationshipNameLookup:Leai;


# direct methods
.method public constructor <init>(Lcom/google/android/shared/search/Query;Lchk;Ldzb;Ljava/util/List;Lcjg;Leai;Lghy;Ljop;Ljava/lang/String;Lcom/google/android/search/shared/contact/Relationship;)V
    .locals 1
    .param p1    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Ldzb;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # Lcjg;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p6    # Leai;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p7    # Lghy;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p8    # Ljop;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Lcom/google/android/search/shared/contact/Relationship;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lggr;->mQuery:Lcom/google/android/shared/search/Query;

    .line 75
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchk;

    iput-object v0, p0, Lggr;->mGsaConfigFlags:Lchk;

    .line 76
    iput-object p3, p0, Lggr;->bSr:Ldzb;

    .line 77
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lggr;->cJJ:Ljava/util/List;

    .line 78
    iput-object p5, p0, Lggr;->mRelationshipManager:Lcjg;

    .line 79
    iput-object p6, p0, Lggr;->mRelationshipNameLookup:Leai;

    .line 80
    invoke-static {p7}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lghy;

    iput-object v0, p0, Lggr;->mContactLookup:Lghy;

    .line 81
    if-nez p8, :cond_0

    new-instance p8, Ljop;

    invoke-direct {p8}, Ljop;-><init>()V

    :cond_0
    iput-object p8, p0, Lggr;->cJK:Ljop;

    .line 83
    iget-object v0, p0, Lggr;->cJK:Ljop;

    iget-object v0, v0, Ljop;->ewI:[Ljava/lang/String;

    array-length v0, v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lggr;->cJJ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 84
    iput-object p9, p0, Lggr;->bRJ:Ljava/lang/String;

    .line 85
    iput-object p10, p0, Lggr;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    .line 86
    return-void

    .line 83
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(ZLcjf;)V
    .locals 2

    .prologue
    .line 282
    iput-boolean p0, p1, Lcjf;->aYm:Z

    .line 284
    new-instance v0, Liuc;

    invoke-direct {v0}, Liuc;-><init>()V

    .line 285
    iget-boolean v1, p1, Lcjf;->aYn:Z

    if-eqz v1, :cond_0

    .line 286
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Liuc;->mS(I)Liuc;

    .line 294
    :goto_0
    iget-boolean v1, p1, Lcjf;->aYm:Z

    invoke-virtual {v0, v1}, Liuc;->he(Z)Liuc;

    .line 295
    iget v1, p1, Lcjf;->aYq:I

    invoke-virtual {v0, v1}, Liuc;->mT(I)Liuc;

    .line 296
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Liuc;->mU(I)Liuc;

    .line 297
    iget v1, p1, Lcjf;->aYr:I

    invoke-virtual {v0, v1}, Liuc;->mV(I)Liuc;

    .line 298
    iget v1, p1, Lcjf;->aYs:I

    invoke-virtual {v0, v1}, Liuc;->mW(I)Liuc;

    .line 299
    iget v1, p1, Lcjf;->aYu:I

    invoke-virtual {v0, v1}, Liuc;->mY(I)Liuc;

    .line 300
    iget v1, p1, Lcjf;->aYt:I

    invoke-virtual {v0, v1}, Liuc;->mX(I)Liuc;

    .line 301
    iget v1, p1, Lcjf;->aYv:I

    invoke-virtual {v0, v1}, Liuc;->mZ(I)Liuc;

    .line 303
    const/16 v1, 0xa4

    invoke-static {v1}, Lege;->hs(I)Litu;

    move-result-object v1

    .line 305
    iput-object v0, v1, Litu;->dIx:Liuc;

    .line 306
    invoke-static {v1}, Lege;->a(Litu;)V

    .line 307
    return-void

    .line 287
    :cond_0
    iget-boolean v1, p1, Lcjf;->aYp:Z

    if-eqz v1, :cond_1

    .line 288
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Liuc;->mS(I)Liuc;

    goto :goto_0

    .line 289
    :cond_1
    iget-boolean v1, p1, Lcjf;->aYo:Z

    if-eqz v1, :cond_2

    .line 290
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Liuc;->mS(I)Liuc;

    goto :goto_0

    .line 292
    :cond_2
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Liuc;->mS(I)Liuc;

    goto :goto_0
.end method

.method private y(ZZ)Ljava/util/List;
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 94
    new-instance v3, Lcjf;

    invoke-direct {v3}, Lcjf;-><init>()V

    .line 95
    iput-boolean p1, v3, Lcjf;->aYp:Z

    .line 96
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lcjf;->aYo:Z

    .line 97
    iget-object v0, p0, Lggr;->mContactLookup:Lghy;

    invoke-virtual {v0, v3}, Lghy;->a(Lcjf;)V

    .line 99
    new-instance v2, Ljop;

    invoke-direct {v2}, Ljop;-><init>()V

    .line 100
    iget-object v0, p0, Lggr;->cJK:Ljop;

    iget-object v0, v0, Ljop;->dQP:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, v2, Ljop;->dQP:[I

    .line 102
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 103
    iget-object v0, p0, Lggr;->cJJ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 104
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->alX()Z

    move-result v6

    if-eqz v6, :cond_2

    if-eqz p1, :cond_2

    .line 105
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 96
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 106
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->alX()Z

    move-result v6

    if-nez v6, :cond_0

    if-nez p1, :cond_0

    .line 107
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 110
    :cond_3
    const-class v0, Ljava/lang/String;

    invoke-static {v4, v0}, Likm;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v2, Ljop;->ewI:[Ljava/lang/String;

    .line 111
    const/4 v0, 0x0

    .line 112
    iget-object v5, v2, Ljop;->ewI:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_5

    .line 113
    iget-object v0, p0, Lggr;->mContactLookup:Lghy;

    iget-object v5, p0, Lggr;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v5, v2}, Lghy;->a(Lcom/google/android/shared/search/Query;Ljop;)Ljava/util/List;

    move-result-object v2

    .line 115
    if-eqz p1, :cond_4

    .line 118
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 119
    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/contact/Person;->ey(Z)Lcom/google/android/search/shared/contact/Person;

    .line 120
    iget-object v6, p0, Lggr;->bRJ:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lcom/google/android/search/shared/contact/Person;->kA(Ljava/lang/String;)Lcom/google/android/search/shared/contact/Person;

    goto :goto_2

    .line 123
    :cond_4
    iget-object v0, p0, Lggr;->mRelationshipNameLookup:Leai;

    invoke-static {v2, v4, v0}, Lcom/google/android/search/shared/contact/Person;->a(Ljava/util/List;Ljava/util/List;Leai;)V

    move-object v0, v2

    .line 136
    :cond_5
    invoke-static {p2, v3}, Lggr;->a(ZLcjf;)V

    .line 137
    return-object v0
.end method


# virtual methods
.method public final aFL()Ljava/util/List;
    .locals 11

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 151
    new-instance v0, Lcjf;

    invoke-direct {v0}, Lcjf;-><init>()V

    .line 152
    iput-boolean v6, v0, Lcjf;->aYn:Z

    .line 153
    iget-object v1, p0, Lggr;->mContactLookup:Lghy;

    invoke-virtual {v1, v0}, Lghy;->a(Lcjf;)V

    .line 155
    iget-object v1, p0, Lggr;->mContactLookup:Lghy;

    iget-object v2, p0, Lggr;->mQuery:Lcom/google/android/shared/search/Query;

    iget-object v4, p0, Lggr;->cJK:Ljop;

    invoke-virtual {v1, v2, v4}, Lghy;->a(Lcom/google/android/shared/search/Query;Ljop;)Ljava/util/List;

    move-result-object v1

    .line 164
    iget-boolean v7, v0, Lcjf;->aYm:Z

    .line 165
    invoke-static {v7, v0}, Lggr;->a(ZLcjf;)V

    .line 167
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lggr;->cJJ:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 251
    :goto_0
    return-object v0

    .line 172
    :cond_0
    invoke-static {v1}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v5

    .line 174
    const/4 v2, 0x4

    new-array v8, v2, [Ldzl;

    new-instance v2, Ldzs;

    invoke-direct {v2}, Ldzs;-><init>()V

    aput-object v2, v8, v3

    new-instance v2, Ldzu;

    invoke-direct {v2}, Ldzu;-><init>()V

    aput-object v2, v8, v6

    const/4 v2, 0x2

    new-instance v4, Ldzw;

    invoke-direct {v4}, Ldzw;-><init>()V

    aput-object v4, v8, v2

    const/4 v2, 0x3

    iget-object v4, p0, Lggr;->bSr:Ldzb;

    invoke-static {v4}, Ldzl;->e(Ldzb;)Ldzl;

    move-result-object v4

    aput-object v4, v8, v2

    .line 181
    iget-object v2, p0, Lggr;->mGsaConfigFlags:Lchk;

    invoke-virtual {v2}, Lchk;->FS()Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz v7, :cond_3

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v6, :cond_3

    .line 185
    iget-object v1, p0, Lggr;->cJJ:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iput v1, v0, Lcjf;->aYu:I

    move-object v1, v0

    .line 216
    :goto_1
    iget-object v0, p0, Lggr;->cJL:Lifw;

    if-eqz v0, :cond_b

    .line 217
    iget-object v0, p0, Lggr;->cJL:Lifw;

    invoke-static {v5, v0}, Liia;->a(Ljava/util/Collection;Lifw;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 219
    :goto_2
    invoke-static {v0, v8}, Lcom/google/android/search/shared/contact/Person;->a(Ljava/util/List;[Ldzl;)Ljava/util/List;

    move-result-object v2

    .line 224
    iget-object v0, p0, Lggr;->mContactLookup:Lghy;

    invoke-virtual {v0, v1}, Lghy;->a(Lcjf;)V

    .line 225
    iget-object v0, p0, Lggr;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    if-eqz v0, :cond_1

    .line 234
    iget-object v0, p0, Lggr;->mContactLookup:Lghy;

    iget-object v4, p0, Lggr;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    invoke-virtual {v4}, Lcom/google/android/search/shared/contact/Relationship;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4, v2}, Lghy;->f(Ljava/lang/String;Ljava/util/List;)V

    .line 236
    iget-object v0, p0, Lggr;->mRelationshipManager:Lcjg;

    invoke-virtual {v0, v2}, Lcjg;->f(Ljava/util/Collection;)V

    .line 239
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->alO()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 186
    :cond_3
    iget-object v1, p0, Lggr;->cJJ:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 188
    iput v3, v0, Lcjf;->aYu:I

    move-object v1, v0

    goto :goto_1

    .line 191
    :cond_4
    invoke-direct {p0, v6, v7}, Lggr;->y(ZZ)Ljava/util/List;

    move-result-object v0

    .line 193
    invoke-direct {p0, v3, v7}, Lggr;->y(ZZ)Ljava/util/List;

    move-result-object v1

    .line 198
    if-eqz v0, :cond_5

    .line 199
    invoke-interface {v5, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 201
    :cond_5
    if-eqz v1, :cond_6

    .line 202
    invoke-interface {v5, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 205
    :cond_6
    iget-object v0, p0, Lggr;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    if-nez v0, :cond_7

    .line 210
    iget-object v0, p0, Lggr;->cJJ:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 212
    :cond_7
    new-instance v4, Lcjf;

    invoke-direct {v4}, Lcjf;-><init>()V

    .line 213
    iget-object v0, p0, Lggr;->cJJ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v2, v3

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_8
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/contact/Person;->a(Ldzk;)Z

    move-result v1

    if-eqz v1, :cond_8

    move v0, v6

    :goto_5
    if-nez v0, :cond_c

    add-int/lit8 v0, v2, 0x1

    :goto_6
    move v2, v0

    goto :goto_4

    :cond_9
    iput v2, v4, Lcjf;->aYu:I

    move-object v1, v4

    goto/16 :goto_1

    .line 239
    :cond_a
    iput v3, v1, Lcjf;->aYt:I

    .line 240
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v1, Lcjf;->aYv:I

    .line 241
    invoke-static {v7, v1}, Lggr;->a(ZLcjf;)V

    .line 243
    iget-object v0, p0, Lggr;->mContactLookup:Lghy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lghy;->a(Lcjf;)V

    move-object v0, v2

    .line 251
    goto/16 :goto_0

    :cond_b
    move-object v0, v5

    goto/16 :goto_2

    :cond_c
    move v0, v2

    goto :goto_6

    :cond_d
    move v0, v3

    goto :goto_5
.end method

.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lggr;->aFL()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
