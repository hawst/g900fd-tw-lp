.class public final Lbfh;
.super Ljava/lang/Object;

# interfaces
.implements Lbfg;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lbhi;Ljava/lang/String;Ljava/lang/String;)Lbhm;
    .locals 1

    new-instance v0, Lbfi;

    invoke-direct {v0, p0, p2, p3}, Lbfi;-><init>(Lbfh;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lbhi;->b(Lbhg;)Lbhg;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbhi;Ljava/lang/String;)V
    .locals 2

    :try_start_0
    sget-object v0, Lbfd;->azo:Lbhc;

    invoke-interface {p1, v0}, Lbhi;->a(Lbhc;)Lbha;

    move-result-object v0

    check-cast v0, Lblg;

    invoke-virtual {v0, p2}, Lblg;->fs(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "service error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lbhi;Ljava/lang/String;Lbfn;)V
    .locals 2

    :try_start_0
    sget-object v0, Lbfd;->azo:Lbhc;

    invoke-interface {p1, v0}, Lbhi;->a(Lbhc;)Lbha;

    move-result-object v0

    check-cast v0, Lblg;

    invoke-virtual {v0, p2, p3}, Lblg;->a(Ljava/lang/String;Lbfn;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "service error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Lbhi;)V
    .locals 2

    :try_start_0
    sget-object v0, Lbfd;->azo:Lbhc;

    invoke-interface {p1, v0}, Lbhi;->a(Lbhc;)Lbha;

    move-result-object v0

    check-cast v0, Lblg;

    invoke-virtual {v0}, Lblg;->zm()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "service error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c(Lbhi;)Lbhm;
    .locals 1

    new-instance v0, Lbfj;

    invoke-direct {v0, p0}, Lbfj;-><init>(Lbfh;)V

    invoke-interface {p1, v0}, Lbhi;->b(Lbhg;)Lbhg;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lbhi;)Lcom/google/android/gms/cast/ApplicationMetadata;
    .locals 1

    sget-object v0, Lbfd;->azo:Lbhc;

    invoke-interface {p1, v0}, Lbhi;->a(Lbhc;)Lbha;

    move-result-object v0

    check-cast v0, Lblg;

    invoke-virtual {v0}, Lblg;->xX()Lcom/google/android/gms/cast/ApplicationMetadata;

    move-result-object v0

    return-object v0
.end method

.method public final e(Lbhi;)Ljava/lang/String;
    .locals 1

    sget-object v0, Lbfd;->azo:Lbhc;

    invoke-interface {p1, v0}, Lbhi;->a(Lbhc;)Lbha;

    move-result-object v0

    check-cast v0, Lblg;

    invoke-virtual {v0}, Lblg;->zn()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
