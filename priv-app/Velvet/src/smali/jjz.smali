.class public final Ljjz;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile epC:[Ljjz;


# instance fields
.field private aez:I

.field private dON:Ljava/lang/String;

.field private dVN:J

.field private epD:Lite;

.field private epE:Ljkd;

.field public epF:Ljjy;

.field public epG:Ljjo;

.field public epH:Ljkb;

.field private epI:Ljka;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 824
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 825
    const/4 v0, 0x0

    iput v0, p0, Ljjz;->aez:I

    iput-object v2, p0, Ljjz;->epD:Lite;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljjz;->dVN:J

    iput-object v2, p0, Ljjz;->epE:Ljkd;

    iput-object v2, p0, Ljjz;->epF:Ljjy;

    iput-object v2, p0, Ljjz;->epG:Ljjo;

    iput-object v2, p0, Ljjz;->epH:Ljkb;

    iput-object v2, p0, Ljjz;->epI:Ljka;

    const-string v0, ""

    iput-object v0, p0, Ljjz;->dON:Ljava/lang/String;

    iput-object v2, p0, Ljjz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljjz;->eCz:I

    .line 826
    return-void
.end method

.method public static bof()[Ljjz;
    .locals 2

    .prologue
    .line 752
    sget-object v0, Ljjz;->epC:[Ljjz;

    if-nez v0, :cond_1

    .line 753
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 755
    :try_start_0
    sget-object v0, Ljjz;->epC:[Ljjz;

    if-nez v0, :cond_0

    .line 756
    const/4 v0, 0x0

    new-array v0, v0, [Ljjz;

    sput-object v0, Ljjz;->epC:[Ljjz;

    .line 758
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 760
    :cond_1
    sget-object v0, Ljjz;->epC:[Ljjz;

    return-object v0

    .line 758
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 746
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljjz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljjz;->epE:Ljkd;

    if-nez v0, :cond_1

    new-instance v0, Ljkd;

    invoke-direct {v0}, Ljkd;-><init>()V

    iput-object v0, p0, Ljjz;->epE:Ljkd;

    :cond_1
    iget-object v0, p0, Ljjz;->epE:Ljkd;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljjz;->epD:Lite;

    if-nez v0, :cond_2

    new-instance v0, Lite;

    invoke-direct {v0}, Lite;-><init>()V

    iput-object v0, p0, Ljjz;->epD:Lite;

    :cond_2
    iget-object v0, p0, Ljjz;->epD:Lite;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljjz;->epF:Ljjy;

    if-nez v0, :cond_3

    new-instance v0, Ljjy;

    invoke-direct {v0}, Ljjy;-><init>()V

    iput-object v0, p0, Ljjz;->epF:Ljjy;

    :cond_3
    iget-object v0, p0, Ljjz;->epF:Ljjy;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljjz;->epG:Ljjo;

    if-nez v0, :cond_4

    new-instance v0, Ljjo;

    invoke-direct {v0}, Ljjo;-><init>()V

    iput-object v0, p0, Ljjz;->epG:Ljjo;

    :cond_4
    iget-object v0, p0, Ljjz;->epG:Ljjo;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljjz;->dVN:J

    iget v0, p0, Ljjz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjz;->aez:I

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ljjz;->epH:Ljkb;

    if-nez v0, :cond_5

    new-instance v0, Ljkb;

    invoke-direct {v0}, Ljkb;-><init>()V

    iput-object v0, p0, Ljjz;->epH:Ljkb;

    :cond_5
    iget-object v0, p0, Ljjz;->epH:Ljkb;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ljjz;->epI:Ljka;

    if-nez v0, :cond_6

    new-instance v0, Ljka;

    invoke-direct {v0}, Ljka;-><init>()V

    iput-object v0, p0, Ljjz;->epI:Ljka;

    :cond_6
    iget-object v0, p0, Ljjz;->epI:Ljka;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljjz;->dON:Ljava/lang/String;

    iget v0, p0, Ljjz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljjz;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
        0x38 -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 846
    iget-object v0, p0, Ljjz;->epE:Ljkd;

    if-eqz v0, :cond_0

    .line 847
    const/4 v0, 0x2

    iget-object v1, p0, Ljjz;->epE:Ljkd;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 849
    :cond_0
    iget-object v0, p0, Ljjz;->epD:Lite;

    if-eqz v0, :cond_1

    .line 850
    const/4 v0, 0x3

    iget-object v1, p0, Ljjz;->epD:Lite;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 852
    :cond_1
    iget-object v0, p0, Ljjz;->epF:Ljjy;

    if-eqz v0, :cond_2

    .line 853
    const/4 v0, 0x5

    iget-object v1, p0, Ljjz;->epF:Ljjy;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 855
    :cond_2
    iget-object v0, p0, Ljjz;->epG:Ljjo;

    if-eqz v0, :cond_3

    .line 856
    const/4 v0, 0x6

    iget-object v1, p0, Ljjz;->epG:Ljjo;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 858
    :cond_3
    iget v0, p0, Ljjz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    .line 859
    const/4 v0, 0x7

    iget-wide v2, p0, Ljjz;->dVN:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 861
    :cond_4
    iget-object v0, p0, Ljjz;->epH:Ljkb;

    if-eqz v0, :cond_5

    .line 862
    const/16 v0, 0x8

    iget-object v1, p0, Ljjz;->epH:Ljkb;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 864
    :cond_5
    iget-object v0, p0, Ljjz;->epI:Ljka;

    if-eqz v0, :cond_6

    .line 865
    const/16 v0, 0x9

    iget-object v1, p0, Ljjz;->epI:Ljka;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 867
    :cond_6
    iget v0, p0, Ljjz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_7

    .line 868
    const/16 v0, 0xa

    iget-object v1, p0, Ljjz;->dON:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 870
    :cond_7
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 871
    return-void
.end method

.method public final bog()Z
    .locals 1

    .prologue
    .line 779
    iget v0, p0, Ljjz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dy(J)Ljjz;
    .locals 1

    .prologue
    .line 774
    iput-wide p1, p0, Ljjz;->dVN:J

    .line 775
    iget v0, p0, Ljjz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjz;->aez:I

    .line 776
    return-object p0
.end method

.method public final getTimestamp()J
    .locals 2

    .prologue
    .line 771
    iget-wide v0, p0, Ljjz;->dVN:J

    return-wide v0
.end method

.method public final getUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 805
    iget-object v0, p0, Ljjz;->dON:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 875
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 876
    iget-object v1, p0, Ljjz;->epE:Ljkd;

    if-eqz v1, :cond_0

    .line 877
    const/4 v1, 0x2

    iget-object v2, p0, Ljjz;->epE:Ljkd;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 880
    :cond_0
    iget-object v1, p0, Ljjz;->epD:Lite;

    if-eqz v1, :cond_1

    .line 881
    const/4 v1, 0x3

    iget-object v2, p0, Ljjz;->epD:Lite;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 884
    :cond_1
    iget-object v1, p0, Ljjz;->epF:Ljjy;

    if-eqz v1, :cond_2

    .line 885
    const/4 v1, 0x5

    iget-object v2, p0, Ljjz;->epF:Ljjy;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 888
    :cond_2
    iget-object v1, p0, Ljjz;->epG:Ljjo;

    if-eqz v1, :cond_3

    .line 889
    const/4 v1, 0x6

    iget-object v2, p0, Ljjz;->epG:Ljjo;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 892
    :cond_3
    iget v1, p0, Ljjz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_4

    .line 893
    const/4 v1, 0x7

    iget-wide v2, p0, Ljjz;->dVN:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 896
    :cond_4
    iget-object v1, p0, Ljjz;->epH:Ljkb;

    if-eqz v1, :cond_5

    .line 897
    const/16 v1, 0x8

    iget-object v2, p0, Ljjz;->epH:Ljkb;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 900
    :cond_5
    iget-object v1, p0, Ljjz;->epI:Ljka;

    if-eqz v1, :cond_6

    .line 901
    const/16 v1, 0x9

    iget-object v2, p0, Ljjz;->epI:Ljka;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 904
    :cond_6
    iget v1, p0, Ljjz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_7

    .line 905
    const/16 v1, 0xa

    iget-object v2, p0, Ljjz;->dON:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 908
    :cond_7
    return v0
.end method

.method public final wp(Ljava/lang/String;)Ljjz;
    .locals 1

    .prologue
    .line 808
    if-nez p1, :cond_0

    .line 809
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 811
    :cond_0
    iput-object p1, p0, Ljjz;->dON:Ljava/lang/String;

    .line 812
    iget v0, p0, Ljjz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljjz;->aez:I

    .line 813
    return-object p0
.end method
