.class public final Lcix;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile aXP:[Lcix;


# instance fields
.field private aXQ:Ljava/lang/String;

.field private aXR:Ljava/lang/String;

.field private aXS:Ljava/lang/String;

.field private aXT:J

.field private aXU:J

.field private aXV:Ljava/lang/String;

.field private aez:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 154
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 155
    const/4 v0, 0x0

    iput v0, p0, Lcix;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lcix;->aXQ:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcix;->aXR:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcix;->aXS:Ljava/lang/String;

    iput-wide v2, p0, Lcix;->aXT:J

    iput-wide v2, p0, Lcix;->aXU:J

    const-string v0, ""

    iput-object v0, p0, Lcix;->aXV:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcix;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lcix;->eCz:I

    .line 156
    return-void
.end method

.method public static KI()[Lcix;
    .locals 2

    .prologue
    .line 15
    sget-object v0, Lcix;->aXP:[Lcix;

    if-nez v0, :cond_1

    .line 16
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 18
    :try_start_0
    sget-object v0, Lcix;->aXP:[Lcix;

    if-nez v0, :cond_0

    .line 19
    const/4 v0, 0x0

    new-array v0, v0, [Lcix;

    sput-object v0, Lcix;->aXP:[Lcix;

    .line 21
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    :cond_1
    sget-object v0, Lcix;->aXP:[Lcix;

    return-object v0

    .line 21
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final KJ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcix;->aXQ:Ljava/lang/String;

    return-object v0
.end method

.method public final KK()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcix;->aXR:Ljava/lang/String;

    return-object v0
.end method

.method public final KL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcix;->aXS:Ljava/lang/String;

    return-object v0
.end method

.method public final KM()J
    .locals 2

    .prologue
    .line 97
    iget-wide v0, p0, Lcix;->aXT:J

    return-wide v0
.end method

.method public final KN()J
    .locals 2

    .prologue
    .line 116
    iget-wide v0, p0, Lcix;->aXU:J

    return-wide v0
.end method

.method public final KO()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcix;->aXV:Ljava/lang/String;

    return-object v0
.end method

.method public final S(J)Lcix;
    .locals 1

    .prologue
    .line 100
    iput-wide p1, p0, Lcix;->aXT:J

    .line 101
    iget v0, p0, Lcix;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcix;->aez:I

    .line 102
    return-object p0
.end method

.method public final T(J)Lcix;
    .locals 1

    .prologue
    .line 119
    iput-wide p1, p0, Lcix;->aXU:J

    .line 120
    iget v0, p0, Lcix;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcix;->aez:I

    .line 121
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 9
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcix;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcix;->aXQ:Ljava/lang/String;

    iget v0, p0, Lcix;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcix;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcix;->aXR:Ljava/lang/String;

    iget v0, p0, Lcix;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcix;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcix;->aXS:Ljava/lang/String;

    iget v0, p0, Lcix;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcix;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lcix;->aXT:J

    iget v0, p0, Lcix;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcix;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lcix;->aXU:J

    iget v0, p0, Lcix;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcix;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcix;->aXV:Ljava/lang/String;

    iget v0, p0, Lcix;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcix;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 174
    iget v0, p0, Lcix;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 175
    const/4 v0, 0x1

    iget-object v1, p0, Lcix;->aXQ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 177
    :cond_0
    iget v0, p0, Lcix;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 178
    const/4 v0, 0x2

    iget-object v1, p0, Lcix;->aXR:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 180
    :cond_1
    iget v0, p0, Lcix;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 181
    const/4 v0, 0x3

    iget-object v1, p0, Lcix;->aXS:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 183
    :cond_2
    iget v0, p0, Lcix;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 184
    const/4 v0, 0x4

    iget-wide v2, p0, Lcix;->aXT:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 186
    :cond_3
    iget v0, p0, Lcix;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 187
    const/4 v0, 0x5

    iget-wide v2, p0, Lcix;->aXU:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 189
    :cond_4
    iget v0, p0, Lcix;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 190
    const/4 v0, 0x6

    iget-object v1, p0, Lcix;->aXV:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 192
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 193
    return-void
.end method

.method public final gA(Ljava/lang/String;)Lcix;
    .locals 1

    .prologue
    .line 56
    if-nez p1, :cond_0

    .line 57
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59
    :cond_0
    iput-object p1, p0, Lcix;->aXR:Ljava/lang/String;

    .line 60
    iget v0, p0, Lcix;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcix;->aez:I

    .line 61
    return-object p0
.end method

.method public final gB(Ljava/lang/String;)Lcix;
    .locals 1

    .prologue
    .line 78
    if-nez p1, :cond_0

    .line 79
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 81
    :cond_0
    iput-object p1, p0, Lcix;->aXS:Ljava/lang/String;

    .line 82
    iget v0, p0, Lcix;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcix;->aez:I

    .line 83
    return-object p0
.end method

.method public final gC(Ljava/lang/String;)Lcix;
    .locals 1

    .prologue
    .line 138
    if-nez p1, :cond_0

    .line 139
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 141
    :cond_0
    iput-object p1, p0, Lcix;->aXV:Ljava/lang/String;

    .line 142
    iget v0, p0, Lcix;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcix;->aez:I

    .line 143
    return-object p0
.end method

.method public final gz(Ljava/lang/String;)Lcix;
    .locals 1

    .prologue
    .line 34
    if-nez p1, :cond_0

    .line 35
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 37
    :cond_0
    iput-object p1, p0, Lcix;->aXQ:Ljava/lang/String;

    .line 38
    iget v0, p0, Lcix;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcix;->aez:I

    .line 39
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 197
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 198
    iget v1, p0, Lcix;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 199
    const/4 v1, 0x1

    iget-object v2, p0, Lcix;->aXQ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 202
    :cond_0
    iget v1, p0, Lcix;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 203
    const/4 v1, 0x2

    iget-object v2, p0, Lcix;->aXR:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 206
    :cond_1
    iget v1, p0, Lcix;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 207
    const/4 v1, 0x3

    iget-object v2, p0, Lcix;->aXS:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 210
    :cond_2
    iget v1, p0, Lcix;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 211
    const/4 v1, 0x4

    iget-wide v2, p0, Lcix;->aXT:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 214
    :cond_3
    iget v1, p0, Lcix;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 215
    const/4 v1, 0x5

    iget-wide v2, p0, Lcix;->aXU:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 218
    :cond_4
    iget v1, p0, Lcix;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 219
    const/4 v1, 0x6

    iget-object v2, p0, Lcix;->aXV:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 222
    :cond_5
    return v0
.end method
