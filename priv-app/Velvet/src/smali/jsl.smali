.class public abstract Ljsl;
.super Ljsr;
.source "PG"


# instance fields
.field protected eCq:Ljsn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljsr;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljsm;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 74
    iget-object v1, p0, Ljsl;->eCq:Ljsn;

    if-nez v1, :cond_1

    .line 78
    :cond_0
    :goto_0
    return-object v0

    .line 77
    :cond_1
    iget-object v1, p0, Ljsl;->eCq:Ljsn;

    iget v2, p1, Ljsm;->tag:I

    invoke-static {v2}, Ljsu;->sl(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljsn;->sh(I)Ljso;

    move-result-object v1

    .line 78
    if-eqz v1, :cond_0

    iget-object v2, v1, Ljso;->ciy:Ljava/lang/Object;

    if-eqz v2, :cond_2

    iget-object v0, v1, Ljso;->eCw:Ljsm;

    if-eq v0, p1, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tried to getExtension with a differernt Extension."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iput-object p1, v1, Ljso;->eCw:Ljsm;

    iget-object v2, v1, Ljso;->eCx:Ljava/util/List;

    invoke-virtual {p1, v2}, Ljsm;->aE(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    iput-object v2, v1, Ljso;->ciy:Ljava/lang/Object;

    iput-object v0, v1, Ljso;->eCx:Ljava/util/List;

    :cond_3
    iget-object v0, v1, Ljso;->ciy:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljsm;Ljava/lang/Object;)Ljsl;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 85
    iget v0, p1, Ljsm;->tag:I

    invoke-static {v0}, Ljsu;->sl(I)I

    move-result v2

    .line 86
    if-nez p2, :cond_1

    .line 87
    iget-object v0, p0, Ljsl;->eCq:Ljsn;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Ljsl;->eCq:Ljsn;

    invoke-virtual {v0, v2}, Ljsn;->remove(I)V

    .line 89
    iget-object v0, p0, Ljsl;->eCq:Ljsn;

    invoke-virtual {v0}, Ljsn;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iput-object v1, p0, Ljsl;->eCq:Ljsn;

    .line 109
    :cond_0
    :goto_0
    return-object p0

    .line 95
    :cond_1
    iget-object v0, p0, Ljsl;->eCq:Ljsn;

    if-nez v0, :cond_2

    .line 96
    new-instance v0, Ljsn;

    invoke-direct {v0}, Ljsn;-><init>()V

    iput-object v0, p0, Ljsl;->eCq:Ljsn;

    move-object v0, v1

    .line 100
    :goto_1
    if-nez v0, :cond_3

    .line 101
    iget-object v0, p0, Ljsl;->eCq:Ljsn;

    new-instance v1, Ljso;

    invoke-direct {v1, p1, p2}, Ljso;-><init>(Ljsm;Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v1}, Ljsn;->a(ILjso;)V

    goto :goto_0

    .line 98
    :cond_2
    iget-object v0, p0, Ljsl;->eCq:Ljsn;

    invoke-virtual {v0, v2}, Ljsn;->sh(I)Ljso;

    move-result-object v0

    goto :goto_1

    .line 103
    :cond_3
    iput-object p1, v0, Ljso;->eCw:Ljsm;

    iput-object p2, v0, Ljso;->ciy:Ljava/lang/Object;

    iput-object v1, v0, Ljso;->eCx:Ljava/util/List;

    goto :goto_0
.end method

.method public a(Ljsj;)V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Ljsl;->eCq:Ljsn;

    if-nez v0, :cond_1

    .line 68
    :cond_0
    return-void

    .line 64
    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljsl;->eCq:Ljsn;

    invoke-virtual {v1}, Ljsn;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 65
    iget-object v1, p0, Ljsl;->eCq:Ljsn;

    invoke-virtual {v1, v0}, Ljsn;->si(I)Ljso;

    move-result-object v1

    .line 66
    invoke-virtual {v1, p1}, Ljso;->a(Ljsj;)V

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected final a(Ljsi;I)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 128
    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    .line 129
    invoke-virtual {p1, p2}, Ljsi;->rU(I)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 148
    :goto_0
    return v0

    .line 132
    :cond_0
    invoke-static {p2}, Ljsu;->sl(I)I

    move-result v3

    .line 133
    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v0

    .line 134
    sub-int v4, v0, v2

    if-nez v4, :cond_2

    sget-object v0, Ljsu;->eCG:[B

    .line 135
    :goto_1
    new-instance v1, Ljst;

    invoke-direct {v1, p2, v0}, Ljst;-><init>(I[B)V

    .line 137
    const/4 v0, 0x0

    .line 138
    iget-object v2, p0, Ljsl;->eCq:Ljsn;

    if-nez v2, :cond_3

    .line 139
    new-instance v2, Ljsn;

    invoke-direct {v2}, Ljsn;-><init>()V

    iput-object v2, p0, Ljsl;->eCq:Ljsn;

    .line 143
    :goto_2
    if-nez v0, :cond_1

    .line 144
    new-instance v0, Ljso;

    invoke-direct {v0}, Ljso;-><init>()V

    .line 145
    iget-object v2, p0, Ljsl;->eCq:Ljsn;

    invoke-virtual {v2, v3, v0}, Ljsn;->a(ILjso;)V

    .line 147
    :cond_1
    iget-object v0, v0, Ljso;->eCx:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    const/4 v0, 0x1

    goto :goto_0

    .line 134
    :cond_2
    new-array v0, v4, [B

    iget v5, p1, Ljsi;->eCj:I

    add-int/2addr v2, v5

    iget-object v5, p1, Ljsi;->buffer:[B

    invoke-static {v5, v2, v0, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    .line 141
    :cond_3
    iget-object v0, p0, Ljsl;->eCq:Ljsn;

    invoke-virtual {v0, v3}, Ljsn;->sh(I)Ljso;

    move-result-object v0

    goto :goto_2
.end method

.method public lF()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 49
    .line 50
    iget-object v1, p0, Ljsl;->eCq:Ljsn;

    if-eqz v1, :cond_0

    move v1, v0

    .line 51
    :goto_0
    iget-object v2, p0, Ljsl;->eCq:Ljsn;

    invoke-virtual {v2}, Ljsn;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 52
    iget-object v2, p0, Ljsl;->eCq:Ljsn;

    invoke-virtual {v2, v0}, Ljsn;->si(I)Ljso;

    move-result-object v2

    .line 53
    invoke-virtual {v2}, Ljso;->lF()I

    move-result v2

    add-int/2addr v1, v2

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    .line 56
    :cond_1
    return v1
.end method
