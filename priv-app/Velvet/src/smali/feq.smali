.class public final Lfeq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfdq;


# instance fields
.field private final bdh:Landroid/location/LocationManager;

.field private final mLocationSettings:Lcob;


# direct methods
.method public constructor <init>(Landroid/location/LocationManager;Lcob;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lfeq;->bdh:Landroid/location/LocationManager;

    .line 27
    iput-object p2, p0, Lfeq;->mLocationSettings:Lcob;

    .line 28
    return-void
.end method


# virtual methods
.method public final getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lfeq;->mLocationSettings:Lcob;

    invoke-interface {v0}, Lcob;->QO()Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    const-string v0, "SystemLocationManagerInjectable"

    const-string v1, "Location access is not permitted"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    const/4 v0, 0x0

    .line 64
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfeq;->bdh:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    goto :goto_0
.end method

.method public final isProviderEnabled(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lfeq;->bdh:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
