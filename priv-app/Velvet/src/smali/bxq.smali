.class final Lbxq;
.super Lecp;
.source "PG"


# instance fields
.field private synthetic aNZ:Lbxh;


# direct methods
.method constructor <init>(Lbxh;)V
    .locals 0

    .prologue
    .line 988
    iput-object p1, p0, Lbxq;->aNZ:Lbxh;

    invoke-direct {p0}, Lecp;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 998
    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    .line 999
    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 998
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1002
    :cond_0
    iget-object v1, p0, Lbxq;->aNZ:Lbxh;

    iput v0, v1, Lbxh;->aNQ:I

    .line 1003
    iget-object v1, p0, Lbxq;->aNZ:Lbxh;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Lbxh;->aNM:J

    .line 1008
    iget-object v1, p0, Lbxq;->aNZ:Lbxh;

    iput-boolean v0, v1, Lbxh;->aNN:Z

    .line 1009
    iget-object v1, p0, Lbxq;->aNZ:Lbxh;

    iput-boolean v0, v1, Lbxh;->aNO:Z

    .line 1010
    iget-object v1, p0, Lbxq;->aNZ:Lbxh;

    iput-boolean v0, v1, Lbxh;->aNP:Z

    .line 1011
    array-length v1, p1

    :goto_1
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    .line 1012
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 1013
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 1015
    const-string v4, "android.media.action.MEDIA_PLAY_FROM_SEARCH"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1017
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iput-boolean v5, v0, Lbxh;->aNN:Z

    .line 1033
    :cond_1
    :goto_2
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iget-object v0, v0, Lbxh;->mIntentStarter:Lelp;

    invoke-virtual {v0, p1}, Lelp;->b([Landroid/content/Intent;)Z

    .line 1034
    return-void

    .line 1020
    :cond_2
    const-string v4, "android.intent.action.CALL"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1022
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iput-boolean v5, v0, Lbxh;->aNO:Z

    goto :goto_2

    .line 1025
    :cond_3
    const-string v4, "android.intent.action.VIEW"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz v2, :cond_4

    sget-object v3, Ledx;->bWu:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1028
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iput-boolean v5, v0, Lbxh;->aNP:Z

    goto :goto_2

    .line 1011
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public final e(ILjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1038
    and-int/lit16 v0, p1, 0x80

    if-eqz v0, :cond_2

    .line 1039
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iget-boolean v0, v0, Lbxh;->aNR:Z

    .line 1042
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iput-boolean v3, v0, Lbxh;->aNR:Z

    .line 1044
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iput-boolean v4, v0, Lbxh;->aNS:Z

    .line 1072
    :cond_0
    :goto_0
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iget-object v0, v0, Lbxh;->aNx:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 1073
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iget-boolean v0, v0, Lbxh;->aNT:Z

    if-eqz v0, :cond_1

    .line 1074
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_5

    .line 1075
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iget-object v0, v0, Lbxh;->aNx:Landroid/widget/TextView;

    iget-object v1, p0, Lbxq;->aNZ:Lbxh;

    const v2, 0x7f0a09c1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v4

    invoke-virtual {v1, v2, v3}, Lbxh;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1081
    :goto_1
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iput-boolean v4, v0, Lbxh;->aNT:Z

    .line 1084
    :cond_1
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iget-object v0, v0, Lbxh;->mState:Lbyi;

    invoke-virtual {v0, p1}, Lbyi;->eq(I)V

    .line 1085
    return-void

    .line 1046
    :cond_2
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iget-boolean v0, v0, Lbxh;->aNR:Z

    if-eqz v0, :cond_3

    .line 1049
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    invoke-virtual {v0}, Lbxh;->finish()V

    goto :goto_0

    .line 1050
    :cond_3
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iget-boolean v0, v0, Lbxh;->aNS:Z

    if-nez v0, :cond_0

    .line 1051
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iput-boolean v3, v0, Lbxh;->aNS:Z

    .line 1052
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iget-object v0, v0, Lbxh;->mScreenStateHelper:Ldmh;

    invoke-virtual {v0}, Ldmh;->ado()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1054
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.search.core.action.CLEAR_MANUAL_CAR_MODE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1056
    iget-object v1, p0, Lbxq;->aNZ:Lbxh;

    const-class v2, Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1057
    iget-object v1, p0, Lbxq;->aNZ:Lbxh;

    invoke-virtual {v1, v0}, Lbxh;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 1062
    :cond_4
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.search.core.action.ENTER_MANUAL_CAR_MODE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1064
    const-string v1, "entry-point"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1066
    iget-object v1, p0, Lbxq;->aNZ:Lbxh;

    const-class v2, Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1067
    iget-object v1, p0, Lbxq;->aNZ:Lbxh;

    invoke-virtual {v1, v0}, Lbxh;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 1078
    :cond_5
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iget-object v0, v0, Lbxh;->aNx:Landroid/widget/TextView;

    const v1, 0x7f0a09c2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method public final en(I)V
    .locals 1

    .prologue
    .line 991
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iget-object v0, v0, Lbxh;->mState:Lbyi;

    invoke-virtual {v0, p1}, Lbyi;->er(I)V

    .line 992
    return-void
.end method

.method public final eo(I)V
    .locals 1

    .prologue
    .line 1089
    iget-object v0, p0, Lbxq;->aNZ:Lbxh;

    iget-object v0, v0, Lbxh;->mSpeechLevelSource:Lequ;

    invoke-virtual {v0, p1}, Lequ;->gp(I)V

    .line 1090
    return-void
.end method
