.class public final Lgps;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final aWp:Ljava/lang/String;

.field public final synthetic cSF:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

.field public final cSG:J

.field public cSH:J

.field public cSI:J


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;Ljava/lang/String;J)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 489
    iput-object p1, p0, Lgps;->cSF:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490
    iput-object p2, p0, Lgps;->aWp:Ljava/lang/String;

    .line 491
    iput-wide p3, p0, Lgps;->cSG:J

    .line 492
    iget-object v0, p1, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcke;

    invoke-interface {v0, p2}, Lcke;->gS(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lgps;->cSH:J

    .line 493
    iget-object v0, p1, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcke;

    invoke-interface {v0, p2}, Lcke;->gT(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lgps;->cSI:J

    .line 494
    iget-object v0, p0, Lgps;->cSF:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    iget-object v0, v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lgps;->cSG:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lgps;->cSH:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lgps;->cSH:J

    iget-wide v4, p0, Lgps;->cSG:J

    add-long/2addr v4, v0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    :cond_0
    iget-wide v2, p0, Lgps;->cSG:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lgps;->cSH:J

    iget-object v2, p0, Lgps;->cSF:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    iget-object v2, v2, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcke;

    iget-object v3, p0, Lgps;->aWp:Ljava/lang/String;

    iget-wide v4, p0, Lgps;->cSH:J

    invoke-interface {v2, v3, v4, v5}, Lcke;->e(Ljava/lang/String;J)V

    :cond_1
    iget-wide v2, p0, Lgps;->cSI:J

    iget-wide v4, p0, Lgps;->cSG:J

    add-long/2addr v4, v0

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid forced run for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " set it to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    iput-wide v0, p0, Lgps;->cSI:J

    iget-object v2, p0, Lgps;->cSF:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    iget-object v2, v2, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcke;

    iget-object v3, p0, Lgps;->aWp:Ljava/lang/String;

    invoke-interface {v2, v3, v0, v1}, Lcke;->f(Ljava/lang/String;J)V

    .line 495
    :cond_2
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 559
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
