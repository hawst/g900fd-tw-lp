.class public final Luy;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;
    .locals 1

    .prologue
    .line 572
    invoke-static {p0, p1}, Luy;->a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;)Ljava/util/ArrayList;

    move-result-object v0

    .line 573
    invoke-static {v0, p2, p3}, Luy;->a(Ljava/util/ArrayList;II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;
    .locals 2

    .prologue
    .line 577
    invoke-static {p0, p1}, Luy;->a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;)Ljava/util/ArrayList;

    move-result-object v0

    .line 578
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v0, v1, p3}, Luy;->a(Ljava/util/ArrayList;II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/ArrayList;II)Landroid/view/View;
    .locals 4

    .prologue
    .line 559
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 560
    add-int v0, p1, p2

    move v1, v0

    .line 561
    :goto_0
    if-ltz v1, :cond_2

    if-ge v1, v2, :cond_2

    .line 562
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 563
    instance-of v3, v0, Lcom/android/launcher3/BubbleTextView;

    if-nez v3, :cond_0

    instance-of v3, v0, Lcom/android/launcher3/FolderIcon;

    if-eqz v3, :cond_1

    .line 568
    :cond_0
    :goto_1
    return-object v0

    .line 566
    :cond_1
    add-int v0, v1, p2

    move v1, v0

    .line 567
    goto :goto_0

    .line 568
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;)Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 533
    invoke-virtual {p0}, Lcom/android/launcher3/CellLayout;->eT()I

    move-result v1

    .line 534
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 535
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 536
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 537
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 536
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 539
    :cond_0
    new-instance v0, Luz;

    invoke-direct {v0, v1}, Luz;-><init>(I)V

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 549
    return-object v3
.end method

.method public static a(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 12

    .prologue
    .line 234
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Ladg;

    if-eqz v0, :cond_0

    .line 235
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 236
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    move-object v2, v1

    .line 237
    check-cast v2, Lcom/android/launcher3/CellLayout;

    invoke-virtual {v2}, Lcom/android/launcher3/CellLayout;->eT()I

    move-result v3

    move-object v2, v1

    .line 238
    check-cast v2, Lcom/android/launcher3/CellLayout;

    invoke-virtual {v2}, Lcom/android/launcher3/CellLayout;->eU()I

    move-result v2

    move-object v4, v1

    move v1, v2

    move v2, v3

    move-object v3, v0

    .line 247
    :goto_0
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/PagedView;

    .line 248
    invoke-virtual {v3, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v6

    .line 249
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v7

    .line 250
    invoke-virtual {v0, v4}, Lcom/android/launcher3/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/android/launcher3/PagedView;->aT(I)I

    move-result v8

    .line 251
    invoke-virtual {v0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v9

    .line 253
    rem-int v10, v6, v2

    .line 254
    div-int v11, v6, v2

    .line 256
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    .line 257
    const/4 v5, 0x1

    if-eq v4, v5, :cond_1

    const/4 v4, 0x1

    move v5, v4

    .line 258
    :goto_1
    const/4 v4, 0x0

    .line 263
    sparse-switch p1, :sswitch_data_0

    move v0, v4

    .line 390
    :goto_2
    return v0

    .line 240
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move-object v1, v0

    .line 241
    check-cast v1, Lacr;

    invoke-virtual {v1}, Lacr;->jL()I

    move-result v2

    move-object v1, v0

    .line 242
    check-cast v1, Lacr;

    invoke-virtual {v1}, Lacr;->jM()I

    move-result v1

    move-object v3, v0

    move-object v4, v0

    goto :goto_0

    .line 257
    :cond_1
    const/4 v4, 0x0

    move v5, v4

    goto :goto_1

    .line 265
    :sswitch_0
    if-eqz v5, :cond_2

    .line 267
    if-lez v6, :cond_3

    .line 268
    add-int/lit8 v0, v6, -0x1

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 269
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 284
    :cond_2
    :goto_3
    const/4 v0, 0x1

    .line 285
    goto :goto_2

    .line 271
    :cond_3
    if-lez v8, :cond_2

    .line 272
    add-int/lit8 v1, v8, -0x1

    invoke-static {v0, v1}, Luy;->c(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;

    move-result-object v1

    .line 273
    if-eqz v1, :cond_2

    .line 274
    add-int/lit8 v2, v8, -0x1

    invoke-virtual {v0, v2}, Lcom/android/launcher3/PagedView;->bB(I)V

    .line 275
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 276
    if-eqz v0, :cond_2

    .line 277
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 278
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/view/View;->playSoundEffect(I)V

    goto :goto_3

    .line 287
    :sswitch_1
    if-eqz v5, :cond_4

    .line 289
    add-int/lit8 v1, v7, -0x1

    if-ge v6, v1, :cond_5

    .line 290
    add-int/lit8 v0, v6, 0x1

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 291
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 306
    :cond_4
    :goto_4
    const/4 v0, 0x1

    .line 307
    goto :goto_2

    .line 293
    :cond_5
    add-int/lit8 v1, v9, -0x1

    if-ge v8, v1, :cond_4

    .line 294
    add-int/lit8 v1, v8, 0x1

    invoke-static {v0, v1}, Luy;->c(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;

    move-result-object v1

    .line 295
    if-eqz v1, :cond_4

    .line 296
    add-int/lit8 v2, v8, 0x1

    invoke-virtual {v0, v2}, Lcom/android/launcher3/PagedView;->bB(I)V

    .line 297
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 298
    if-eqz v0, :cond_4

    .line 299
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 300
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Landroid/view/View;->playSoundEffect(I)V

    goto :goto_4

    .line 309
    :sswitch_2
    if-eqz v5, :cond_6

    .line 311
    if-lez v11, :cond_6

    .line 312
    add-int/lit8 v0, v11, -0x1

    mul-int/2addr v0, v2

    add-int/2addr v0, v10

    .line 313
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 314
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 317
    :cond_6
    const/4 v0, 0x1

    .line 318
    goto/16 :goto_2

    .line 320
    :sswitch_3
    if-eqz v5, :cond_7

    .line 322
    add-int/lit8 v0, v1, -0x1

    if-ge v11, v0, :cond_7

    .line 323
    add-int/lit8 v0, v7, -0x1

    add-int/lit8 v1, v11, 0x1

    mul-int/2addr v1, v2

    add-int/2addr v1, v10

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 324
    div-int v1, v0, v2

    .line 325
    if-eq v1, v11, :cond_7

    .line 326
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 327
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 331
    :cond_7
    const/4 v0, 0x1

    .line 332
    goto/16 :goto_2

    .line 334
    :sswitch_4
    if-eqz v5, :cond_8

    .line 337
    if-lez v8, :cond_9

    .line 338
    add-int/lit8 v1, v8, -0x1

    invoke-static {v0, v1}, Luy;->c(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;

    move-result-object v1

    .line 339
    if-eqz v1, :cond_8

    .line 340
    add-int/lit8 v2, v8, -0x1

    invoke-virtual {v0, v2}, Lcom/android/launcher3/PagedView;->bB(I)V

    .line 341
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 342
    if-eqz v0, :cond_8

    .line 343
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 344
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 352
    :cond_8
    :goto_5
    const/4 v0, 0x1

    .line 353
    goto/16 :goto_2

    .line 348
    :cond_9
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 349
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/view/View;->playSoundEffect(I)V

    goto :goto_5

    .line 355
    :sswitch_5
    if-eqz v5, :cond_a

    .line 358
    add-int/lit8 v1, v9, -0x1

    if-ge v8, v1, :cond_b

    .line 359
    add-int/lit8 v1, v8, 0x1

    invoke-static {v0, v1}, Luy;->c(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;

    move-result-object v1

    .line 360
    if-eqz v1, :cond_a

    .line 361
    add-int/lit8 v2, v8, 0x1

    invoke-virtual {v0, v2}, Lcom/android/launcher3/PagedView;->bB(I)V

    .line 362
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 363
    if-eqz v0, :cond_a

    .line 364
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 365
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 373
    :cond_a
    :goto_6
    const/4 v0, 0x1

    .line 374
    goto/16 :goto_2

    .line 369
    :cond_b
    add-int/lit8 v0, v7, -0x1

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 370
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/view/View;->playSoundEffect(I)V

    goto :goto_6

    .line 376
    :sswitch_6
    if-eqz v5, :cond_c

    .line 378
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 379
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 381
    :cond_c
    const/4 v0, 0x1

    .line 382
    goto/16 :goto_2

    .line 384
    :sswitch_7
    if-eqz v5, :cond_d

    .line 386
    add-int/lit8 v0, v7, -0x1

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 387
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 389
    :cond_d
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 263
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_2
        0x14 -> :sswitch_3
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x5c -> :sswitch_4
        0x5d -> :sswitch_5
        0x7a -> :sswitch_6
        0x7b -> :sswitch_7
    .end sparse-switch
.end method

.method public static a(Lcom/android/launcher3/AccessibleTabView;ILandroid/view/KeyEvent;)Z
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 400
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    iget-boolean v0, v0, Lyu;->Md:Z

    if-nez v0, :cond_0

    .line 444
    :goto_0
    return v3

    .line 402
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/AccessibleTabView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lva;

    .line 403
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    :goto_1
    if-eqz v1, :cond_1

    instance-of v4, v1, Lcom/android/launcher3/AppsCustomizeTabHost;

    if-nez v4, :cond_1

    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_1

    :cond_1
    check-cast v1, Lcom/android/launcher3/AppsCustomizeTabHost;

    .line 404
    invoke-virtual {v1}, Lcom/android/launcher3/AppsCustomizeTabHost;->eB()Landroid/view/ViewGroup;

    move-result-object v5

    .line 405
    invoke-virtual {v0}, Lva;->getTabCount()I

    move-result v6

    .line 406
    invoke-virtual {v0, p0}, Lva;->P(Landroid/view/View;)I

    move-result v7

    .line 408
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    .line 409
    if-eq v4, v2, :cond_2

    move v4, v2

    .line 411
    :goto_2
    packed-switch p1, :pswitch_data_0

    move v0, v3

    :goto_3
    move v3, v0

    .line 444
    goto :goto_0

    :cond_2
    move v4, v3

    .line 409
    goto :goto_2

    .line 413
    :pswitch_0
    if-eqz v4, :cond_3

    .line 415
    if-lez v7, :cond_3

    .line 416
    add-int/lit8 v1, v7, -0x1

    invoke-virtual {v0, v1}, Lva;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_3
    move v0, v2

    .line 420
    goto :goto_3

    .line 422
    :pswitch_1
    if-eqz v4, :cond_4

    .line 424
    add-int/lit8 v3, v6, -0x1

    if-ge v7, v3, :cond_5

    .line 425
    add-int/lit8 v1, v7, 0x1

    invoke-virtual {v0, v1}, Lva;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_4
    :goto_4
    move v0, v2

    .line 433
    goto :goto_3

    .line 427
    :cond_5
    invoke-virtual {p0}, Lcom/android/launcher3/AccessibleTabView;->getNextFocusRightId()I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_4

    .line 428
    invoke-virtual {p0}, Lcom/android/launcher3/AccessibleTabView;->getNextFocusRightId()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/android/launcher3/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_4

    :pswitch_2
    move v0, v2

    .line 437
    goto :goto_3

    .line 439
    :pswitch_3
    if-eqz v4, :cond_6

    .line 441
    invoke-virtual {v5}, Landroid/view/ViewGroup;->requestFocus()Z

    :cond_6
    move v0, v2

    .line 443
    goto :goto_3

    .line 411
    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static b(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;
    .locals 18

    .prologue
    .line 588
    invoke-static/range {p0 .. p1}, Luy;->a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;)Ljava/util/ArrayList;

    move-result-object v10

    .line 589
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 590
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/CellLayout;->eU()I

    move-result v3

    .line 591
    iget v11, v2, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    .line 592
    add-int v4, v11, p3

    .line 593
    if-ltz v4, :cond_7

    if-ge v4, v3, :cond_7

    .line 594
    const v4, 0x7f7fffff    # Float.MAX_VALUE

    .line 595
    const/4 v6, -0x1

    .line 596
    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v7

    .line 597
    if-gez p3, :cond_1

    const/4 v3, -0x1

    move v5, v3

    :goto_0
    move v8, v4

    .line 598
    :goto_1
    if-eq v7, v5, :cond_6

    .line 599
    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 600
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 601
    if-gez p3, :cond_3

    iget v9, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    if-ge v9, v11, :cond_2

    const/4 v9, 0x1

    .line 602
    :goto_2
    if-eqz v9, :cond_8

    instance-of v9, v3, Lcom/android/launcher3/BubbleTextView;

    if-nez v9, :cond_0

    instance-of v3, v3, Lcom/android/launcher3/FolderIcon;

    if-eqz v3, :cond_8

    .line 604
    :cond_0
    iget v3, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    iget v9, v2, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    sub-int/2addr v3, v9

    int-to-double v12, v3

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    iget v3, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    iget v4, v2, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    sub-int/2addr v3, v4

    int-to-double v14, v3

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    add-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    double-to-float v3, v12

    .line 606
    cmpg-float v4, v3, v8

    if-gez v4, :cond_8

    move v4, v3

    move v3, v7

    .line 611
    :goto_3
    if-gt v7, v5, :cond_5

    .line 612
    add-int/lit8 v7, v7, 0x1

    move v6, v3

    move v8, v4

    goto :goto_1

    .line 597
    :cond_1
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v5, v3

    goto :goto_0

    .line 601
    :cond_2
    const/4 v9, 0x0

    goto :goto_2

    :cond_3
    iget v9, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    if-le v9, v11, :cond_4

    const/4 v9, 0x1

    goto :goto_2

    :cond_4
    const/4 v9, 0x0

    goto :goto_2

    .line 614
    :cond_5
    add-int/lit8 v7, v7, -0x1

    move v6, v3

    move v8, v4

    .line 616
    goto :goto_1

    .line 617
    :cond_6
    if-ltz v6, :cond_7

    .line 618
    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 621
    :goto_4
    return-object v2

    :cond_7
    const/4 v2, 0x0

    goto :goto_4

    :cond_8
    move v3, v6

    move v4, v8

    goto :goto_3
.end method

.method static b(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 13

    .prologue
    const/4 v6, 0x0

    const/4 v12, 0x2

    const/4 v11, 0x4

    const/4 v10, -0x1

    const/4 v5, 0x1

    .line 628
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Ladg;

    .line 629
    invoke-virtual {v0}, Ladg;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/CellLayout;

    .line 630
    invoke-virtual {v1}, Lcom/android/launcher3/CellLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Lcom/android/launcher3/Workspace;

    .line 631
    invoke-virtual {v2}, Lcom/android/launcher3/Workspace;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 632
    const v4, 0x7f110241

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 633
    const v7, 0x7f11023f

    invoke-virtual {v3, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 634
    invoke-virtual {v2, v1}, Lcom/android/launcher3/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v8

    .line 635
    invoke-virtual {v2}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v9

    .line 637
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v7

    .line 638
    if-eq v7, v5, :cond_2

    move v7, v5

    .line 640
    :goto_0
    sparse-switch p1, :sswitch_data_0

    :cond_0
    move v5, v6

    .line 786
    :cond_1
    :goto_1
    return v5

    :cond_2
    move v7, v6

    .line 638
    goto :goto_0

    .line 642
    :sswitch_0
    if-eqz v7, :cond_1

    .line 644
    invoke-static {v1, v0, p0, v10}, Luy;->a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 645
    if-eqz v0, :cond_3

    .line 646
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 659
    :goto_2
    invoke-virtual {p0, v5}, Landroid/view/View;->playSoundEffect(I)V

    goto :goto_1

    .line 649
    :cond_3
    if-lez v8, :cond_1

    .line 650
    add-int/lit8 v0, v8, -0x1

    invoke-static {v2, v0}, Luy;->d(Landroid/view/ViewGroup;I)Ladg;

    move-result-object v0

    .line 651
    invoke-virtual {v0}, Ladg;->getChildCount()I

    move-result v3

    invoke-static {v1, v0, v3, v10}, Luy;->a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 653
    if-eqz v0, :cond_4

    .line 654
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_2

    .line 657
    :cond_4
    add-int/lit8 v0, v8, -0x1

    invoke-virtual {v2, v0}, Lcom/android/launcher3/Workspace;->bB(I)V

    goto :goto_2

    .line 666
    :sswitch_1
    if-eqz v7, :cond_1

    .line 668
    invoke-static {v1, v0, p0, v5}, Luy;->a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 669
    if-eqz v0, :cond_5

    .line 670
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 682
    :goto_3
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Landroid/view/View;->playSoundEffect(I)V

    goto :goto_1

    .line 673
    :cond_5
    add-int/lit8 v0, v9, -0x1

    if-ge v8, v0, :cond_1

    .line 674
    add-int/lit8 v0, v8, 0x1

    invoke-static {v2, v0}, Luy;->d(Landroid/view/ViewGroup;I)Ladg;

    move-result-object v0

    .line 675
    invoke-static {v1, v0, v10, v5}, Luy;->a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 676
    if-eqz v0, :cond_6

    .line 677
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_3

    .line 680
    :cond_6
    add-int/lit8 v0, v8, 0x1

    invoke-virtual {v2, v0}, Lcom/android/launcher3/Workspace;->bB(I)V

    goto :goto_3

    .line 689
    :sswitch_2
    if-eqz v7, :cond_0

    .line 691
    invoke-static {v1, v0, p0, v10}, Luy;->b(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 692
    if-eqz v0, :cond_7

    .line 693
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    move v0, v5

    .line 698
    :goto_4
    invoke-virtual {p0, v12}, Landroid/view/View;->playSoundEffect(I)V

    move v5, v0

    .line 699
    goto :goto_1

    .line 696
    :cond_7
    invoke-virtual {v4}, Landroid/view/ViewGroup;->requestFocus()Z

    move v0, v6

    goto :goto_4

    .line 702
    :sswitch_3
    if-eqz v7, :cond_0

    .line 704
    invoke-static {v1, v0, p0, v5}, Luy;->b(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 705
    if-eqz v0, :cond_8

    .line 706
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 707
    invoke-virtual {p0, v11}, Landroid/view/View;->playSoundEffect(I)V

    goto :goto_1

    .line 709
    :cond_8
    if-eqz v3, :cond_9

    .line 710
    invoke-virtual {v3}, Landroid/view/ViewGroup;->requestFocus()Z

    .line 711
    invoke-virtual {p0, v11}, Landroid/view/View;->playSoundEffect(I)V

    :cond_9
    move v5, v6

    .line 713
    goto/16 :goto_1

    .line 716
    :sswitch_4
    if-eqz v7, :cond_1

    .line 719
    if-lez v8, :cond_b

    .line 720
    add-int/lit8 v0, v8, -0x1

    invoke-static {v2, v0}, Luy;->d(Landroid/view/ViewGroup;I)Ladg;

    move-result-object v0

    .line 721
    invoke-static {v1, v0, v10, v5}, Luy;->a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 722
    if-eqz v0, :cond_a

    .line 723
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 728
    :goto_5
    invoke-virtual {p0, v12}, Landroid/view/View;->playSoundEffect(I)V

    goto/16 :goto_1

    .line 726
    :cond_a
    add-int/lit8 v0, v8, -0x1

    invoke-virtual {v2, v0}, Lcom/android/launcher3/Workspace;->bB(I)V

    goto :goto_5

    .line 730
    :cond_b
    invoke-static {v1, v0, v10, v5}, Luy;->a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 731
    if-eqz v0, :cond_1

    .line 732
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 733
    invoke-virtual {p0, v12}, Landroid/view/View;->playSoundEffect(I)V

    goto/16 :goto_1

    .line 740
    :sswitch_5
    if-eqz v7, :cond_1

    .line 743
    add-int/lit8 v3, v9, -0x1

    if-ge v8, v3, :cond_d

    .line 744
    add-int/lit8 v0, v8, 0x1

    invoke-static {v2, v0}, Luy;->d(Landroid/view/ViewGroup;I)Ladg;

    move-result-object v0

    .line 745
    invoke-static {v1, v0, v10, v5}, Luy;->a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 746
    if-eqz v0, :cond_c

    .line 747
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 752
    :goto_6
    invoke-virtual {p0, v11}, Landroid/view/View;->playSoundEffect(I)V

    goto/16 :goto_1

    .line 750
    :cond_c
    add-int/lit8 v0, v8, 0x1

    invoke-virtual {v2, v0}, Lcom/android/launcher3/Workspace;->bB(I)V

    goto :goto_6

    .line 754
    :cond_d
    invoke-virtual {v0}, Ladg;->getChildCount()I

    move-result v2

    invoke-static {v1, v0, v2, v10}, Luy;->a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 756
    if-eqz v0, :cond_1

    .line 757
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 758
    invoke-virtual {p0, v11}, Landroid/view/View;->playSoundEffect(I)V

    goto/16 :goto_1

    .line 765
    :sswitch_6
    if-eqz v7, :cond_1

    .line 767
    invoke-static {v1, v0, v10, v5}, Luy;->a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 768
    if-eqz v0, :cond_1

    .line 769
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 770
    invoke-virtual {p0, v12}, Landroid/view/View;->playSoundEffect(I)V

    goto/16 :goto_1

    .line 776
    :sswitch_7
    if-eqz v7, :cond_1

    .line 778
    invoke-virtual {v0}, Ladg;->getChildCount()I

    move-result v2

    invoke-static {v1, v0, v2, v10}, Luy;->a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    .line 780
    if-eqz v0, :cond_1

    .line 781
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 782
    invoke-virtual {p0, v11}, Landroid/view/View;->playSoundEffect(I)V

    goto/16 :goto_1

    .line 640
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_2
        0x14 -> :sswitch_3
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x5c -> :sswitch_4
        0x5d -> :sswitch_5
        0x7a -> :sswitch_6
        0x7b -> :sswitch_7
    .end sparse-switch
.end method

.method private static c(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 76
    check-cast p0, Lcom/android/launcher3/PagedView;

    invoke-virtual {p0, p1}, Lcom/android/launcher3/PagedView;->aS(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 77
    instance-of v1, v0, Lcom/android/launcher3/CellLayout;

    if-eqz v1, :cond_0

    .line 79
    check-cast v0, Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v0

    .line 81
    :cond_0
    return-object v0
.end method

.method private static d(Landroid/view/ViewGroup;I)Ladg;
    .locals 1

    .prologue
    .line 522
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 523
    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v0

    return-object v0
.end method
