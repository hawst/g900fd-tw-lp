.class public final Lcnr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lisu;


# instance fields
.field public final bdd:I

.field public final mInputStream:Ljava/io/InputStream;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ldyj;Ljava/io/InputStream;ILchk;)V
    .locals 9
    .param p1    # Ldyj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/io/InputStream;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v8, 0x5

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    iput-object v0, p0, Lcnr;->mInputStream:Ljava/io/InputStream;

    .line 63
    if-eq p3, v2, :cond_0

    if-eq p3, v3, :cond_0

    const/4 v0, 0x4

    if-eq p3, v0, :cond_0

    if-eq p3, v4, :cond_0

    if-ne p3, v8, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 64
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    invoke-static {p1}, Leeb;->c(Ldyj;)Leeb;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v5, v0, Leeb;->bWw:Ljava/lang/String;

    if-eqz v5, :cond_4

    iget-object v5, v0, Leeb;->bWw:Ljava/lang/String;

    invoke-virtual {p4}, Lchk;->JA()[Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcnr;->b(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v0, v2

    .line 67
    :goto_1
    if-nez v0, :cond_5

    .line 68
    const-string v0, "FormattedInputStream"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Defaulting to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3}, Lcnr;->fg(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v8, v0, v2, v1}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 69
    iput p3, p0, Lcnr;->bdd:I

    .line 90
    :goto_2
    return-void

    :cond_1
    move v0, v1

    .line 63
    goto :goto_0

    .line 66
    :cond_2
    iget-object v5, v0, Leeb;->bWw:Ljava/lang/String;

    invoke-virtual {p4}, Lchk;->JB()[Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcnr;->b(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v0, v3

    goto :goto_1

    :cond_3
    iget-object v5, v0, Leeb;->bWw:Ljava/lang/String;

    invoke-virtual {p4}, Lchk;->JC()[Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcnr;->b(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    move v0, v4

    goto :goto_1

    :cond_4
    const-string v5, "FormattedInputStream"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Could not parse response format from Content-Type: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v8, v5, v0, v6}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v0, v1

    goto :goto_1

    .line 72
    :cond_5
    if-ne v0, v3, :cond_6

    const/4 v3, 0x4

    if-ne p3, v3, :cond_6

    .line 73
    const/4 v0, 0x4

    iput v0, p0, Lcnr;->bdd:I

    goto :goto_2

    .line 74
    :cond_6
    if-ne v0, v4, :cond_7

    if-ne p3, v8, :cond_7

    .line 75
    iput v8, p0, Lcnr;->bdd:I

    goto :goto_2

    .line 77
    :cond_7
    if-eq v0, p3, :cond_8

    .line 78
    const-string v3, "FormattedInputStream"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Type mismatch. Expected "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3}, Lcnr;->fg(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", actual "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Lcnr;->fg(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v8, v3, v4, v1}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 80
    if-ne v0, v2, :cond_9

    .line 81
    const v1, 0xdeb6c1

    invoke-static {v1}, Lhwt;->lx(I)V

    .line 86
    :cond_8
    :goto_3
    iput v0, p0, Lcnr;->bdd:I

    goto :goto_2

    .line 83
    :cond_9
    const v1, 0xe4b794

    invoke-static {v1}, Lhwt;->lx(I)V

    goto :goto_3
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    iput-object v0, p0, Lcnr;->mInputStream:Ljava/io/InputStream;

    .line 123
    iput p2, p0, Lcnr;->bdd:I

    .line 124
    return-void
.end method

.method private static b(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # [Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 111
    if-eqz p1, :cond_0

    .line 112
    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    .line 113
    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 114
    const/4 v0, 0x1

    .line 118
    :cond_0
    return v0

    .line 112
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static fg(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    packed-switch p0, :pswitch_data_0

    .line 164
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 154
    :pswitch_0
    const-string v0, "RAW"

    goto :goto_0

    .line 156
    :pswitch_1
    const-string v0, "JSON"

    goto :goto_0

    .line 158
    :pswitch_2
    const-string v0, "PROTO"

    goto :goto_0

    .line 160
    :pswitch_3
    const-string v0, "JSON_JESR"

    goto :goto_0

    .line 162
    :pswitch_4
    const-string v0, "PROTO_JESR"

    goto :goto_0

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final bridge synthetic PN()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 22
    return-object p0
.end method
