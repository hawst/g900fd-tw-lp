.class final Lgyh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ldex;


# instance fields
.field private final bWW:Lcom/google/android/search/suggest/SuggestionListView;

.field private cZp:I

.field private synthetic cZq:Lgyg;


# direct methods
.method constructor <init>(Lgyg;Ldgb;Lcom/google/android/search/suggest/SuggestionListView;)V
    .locals 3

    .prologue
    .line 125
    iput-object p1, p0, Lgyh;->cZq:Lgyg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    iput-object p3, p0, Lgyh;->bWW:Lcom/google/android/search/suggest/SuggestionListView;

    .line 127
    iget v0, p1, Lgyg;->cZm:I

    iput v0, p0, Lgyh;->cZp:I

    .line 129
    invoke-virtual {p1}, Lgyg;->aJU()Lgpu;

    move-result-object v0

    invoke-virtual {v0, p2}, Lgpu;->j(Ldgb;)Lddn;

    move-result-object v0

    .line 130
    iget-object v1, p0, Lgyh;->bWW:Lcom/google/android/search/suggest/SuggestionListView;

    iget-object v2, p0, Lgyh;->bWW:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v2}, Lcom/google/android/search/suggest/SuggestionListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p2}, Ldgg;->a(Landroid/content/Context;Ldgb;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/search/suggest/SuggestionListView;->setTitle(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v1, p0, Lgyh;->bWW:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v1, p0}, Lcom/google/android/search/suggest/SuggestionListView;->d(Landroid/view/View$OnClickListener;)V

    .line 133
    invoke-virtual {p1}, Lgyg;->aLH()Lgyz;

    move-result-object v1

    invoke-virtual {v1}, Lgyz;->UG()Ldep;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Ldep;->a(Ldex;Lddn;)V

    .line 134
    return-void
.end method


# virtual methods
.method public final c(Landroid/util/SparseArray;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 142
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    .line 144
    invoke-interface {v0}, Ldef;->getCount()I

    move-result v2

    iget v3, p0, Lgyh;->cZp:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 145
    iget-object v3, p0, Lgyh;->bWW:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-interface {v0}, Ldef;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/search/suggest/SuggestionListView;->hp(I)V

    .line 146
    iget-object v3, p0, Lgyh;->bWW:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-interface {v0}, Ldef;->abb()Lcom/google/android/shared/search/Query;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Ldef;->abe()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v2}, Lcom/google/android/search/suggest/SuggestionListView;->e(Ljava/lang/String;Ljava/util/List;I)V

    .line 150
    invoke-interface {v0}, Ldef;->getCount()I

    move-result v0

    if-ge v2, v0, :cond_0

    const/4 v0, 0x1

    .line 151
    :goto_0
    iget-object v2, p0, Lgyh;->bWW:Lcom/google/android/search/suggest/SuggestionListView;

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v2, v1}, Lcom/google/android/search/suggest/SuggestionListView;->ho(I)V

    .line 152
    return-void

    :cond_0
    move v0, v1

    .line 150
    goto :goto_0

    .line 151
    :cond_1
    const/16 v1, 0x8

    goto :goto_1
.end method

.method final ca()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lgyh;->cZq:Lgyg;

    invoke-virtual {v0}, Lgyg;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->UG()Ldep;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldep;->b(Ldex;)V

    .line 138
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 156
    iget v0, p0, Lgyh;->cZp:I

    iget-object v1, p0, Lgyh;->cZq:Lgyg;

    iget v1, v1, Lgyg;->cZn:I

    add-int/2addr v0, v1

    iput v0, p0, Lgyh;->cZp:I

    .line 157
    iget-object v0, p0, Lgyh;->cZq:Lgyg;

    invoke-virtual {v0}, Lgyg;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->UG()Ldep;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldep;->c(Ldex;)V

    .line 158
    return-void
.end method
