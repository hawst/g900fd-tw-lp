.class final Liqw;
.super Liic;
.source "PG"

# interfaces
.implements Ljava/util/SortedSet;


# direct methods
.method constructor <init>(Ljava/util/SortedSet;Lifw;)V
    .locals 0

    .prologue
    .line 833
    invoke-direct {p0, p1, p2}, Liic;-><init>(Ljava/util/Collection;Lifw;)V

    .line 834
    return-void
.end method


# virtual methods
.method public final comparator()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 846
    iget-object v0, p0, Liqw;->dCG:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 837
    invoke-static {p0, p1}, Liqs;->b(Ljava/util/Set;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final first()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 867
    invoke-virtual {p0}, Liqw;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 841
    invoke-static {p0}, Liqs;->f(Ljava/util/Set;)I

    move-result v0

    return v0
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 3

    .prologue
    .line 857
    new-instance v1, Liqw;

    iget-object v0, p0, Liqw;->dCG:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v2, p0, Liqw;->dBv:Lifw;

    invoke-direct {v1, v0, v2}, Liqw;-><init>(Ljava/util/SortedSet;Lifw;)V

    return-object v1
.end method

.method public final last()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 872
    iget-object v0, p0, Liqw;->dCG:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    .line 874
    :goto_0
    invoke-interface {v0}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v1

    .line 875
    iget-object v2, p0, Liqw;->dBv:Lifw;

    invoke-interface {v2, v1}, Lifw;->apply(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 876
    return-object v1

    .line 878
    :cond_0
    invoke-interface {v0, v1}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    goto :goto_0
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 3

    .prologue
    .line 851
    new-instance v1, Liqw;

    iget-object v0, p0, Liqw;->dCG:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0, p1, p2}, Ljava/util/SortedSet;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v2, p0, Liqw;->dBv:Lifw;

    invoke-direct {v1, v0, v2}, Liqw;-><init>(Ljava/util/SortedSet;Lifw;)V

    return-object v1
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 3

    .prologue
    .line 862
    new-instance v1, Liqw;

    iget-object v0, p0, Liqw;->dCG:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v2, p0, Liqw;->dBv:Lifw;

    invoke-direct {v1, v0, v2}, Liqw;-><init>(Ljava/util/SortedSet;Lifw;)V

    return-object v1
.end method
