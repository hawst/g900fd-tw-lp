.class public final Lghl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ligi;


# instance fields
.field private final aTW:Ljava/util/concurrent/Executor;

.field private final cJB:Ljava/lang/Object;

.field private cJE:J

.field final cJY:Lchk;

.field final cKj:Lghj;

.field private cKk:Lghm;

.field private cKl:Ljava/util/List;

.field final mSearchSettings:Lcke;


# direct methods
.method public constructor <init>(Lchk;Lcke;Lghj;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcke;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lghj;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lghl;->cJY:Lchk;

    .line 49
    iput-object p2, p0, Lghl;->mSearchSettings:Lcke;

    .line 50
    iput-object p3, p0, Lghl;->cKj:Lghj;

    .line 51
    iput-object p4, p0, Lghl;->aTW:Ljava/util/concurrent/Executor;

    .line 52
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lghl;->cJB:Ljava/lang/Object;

    .line 53
    return-void
.end method


# virtual methods
.method final a(Ljava/util/List;J)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 86
    iget-object v1, p0, Lghl;->cJB:Ljava/lang/Object;

    monitor-enter v1

    .line 87
    :try_start_0
    iput-object p1, p0, Lghl;->cKl:Ljava/util/List;

    .line 88
    iput-wide p2, p0, Lghl;->cJE:J

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lghl;->cKk:Lghm;

    .line 90
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aFL()Ljava/util/List;
    .locals 6

    .prologue
    .line 57
    iget-object v1, p0, Lghl;->cJB:Ljava/lang/Object;

    monitor-enter v1

    .line 61
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lghl;->cJE:J

    sub-long/2addr v2, v4

    .line 62
    const-wide/32 v4, 0x493e0

    cmp-long v0, v2, v4

    if-gtz v0, :cond_0

    iget-object v0, p0, Lghl;->cKl:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 63
    const-string v0, "FavoriteContactNamesSupplier"

    const-string v2, "get() : Cache hit"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 80
    :goto_0
    iget-object v0, p0, Lghl;->cKl:Ljava/util/List;

    monitor-exit v1

    return-object v0

    .line 64
    :cond_0
    iget-object v0, p0, Lghl;->cKk:Lghm;

    if-eqz v0, :cond_1

    .line 65
    const-string v0, "FavoriteContactNamesSupplier"

    const-string v2, "get() : Already running"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 66
    :cond_1
    :try_start_1
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-ne v0, v2, :cond_2

    .line 70
    const-string v0, "FavoriteContactNamesSupplier"

    const-string v2, "get() : Execute runnable (UI thread)"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 71
    new-instance v0, Lghm;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lghm;-><init>(Lghl;B)V

    iput-object v0, p0, Lghl;->cKk:Lghm;

    .line 72
    iget-object v0, p0, Lghl;->aTW:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lghl;->cKk:Lghm;

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 75
    :cond_2
    const-string v0, "FavoriteContactNamesSupplier"

    const-string v2, "get() : Execute directly (BG thread)"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 76
    new-instance v0, Lghm;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lghm;-><init>(Lghl;B)V

    iput-object v0, p0, Lghl;->cKk:Lghm;

    .line 77
    iget-object v0, p0, Lghl;->cKk:Lghm;

    invoke-virtual {v0}, Lghm;->run()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lghl;->aFL()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
