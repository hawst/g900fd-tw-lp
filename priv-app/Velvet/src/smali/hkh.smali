.class final Lhkh;
.super Lgly;
.source "PG"


# instance fields
.field private final bEC:Lgnp;

.field private synthetic diZ:Lhkd;

.field private djc:Lcmk;

.field private mRequestId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lhkd;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 436
    iput-object p1, p0, Lhkh;->diZ:Lhkd;

    invoke-direct {p0}, Lgly;-><init>()V

    .line 429
    new-instance v0, Lgnp;

    invoke-direct {v0}, Lgnp;-><init>()V

    iput-object v0, p0, Lhkh;->bEC:Lgnp;

    .line 437
    iput-object p2, p0, Lhkh;->mRequestId:Ljava/lang/String;

    .line 438
    return-void
.end method


# virtual methods
.method public final CZ()V
    .locals 2

    .prologue
    .line 500
    iget-object v0, p0, Lhkh;->bEC:Lgnp;

    invoke-virtual {v0}, Lgnp;->aHZ()Z

    move-result v0

    if-nez v0, :cond_1

    .line 501
    iget-object v0, p0, Lhkh;->diZ:Lhkd;

    iget v0, v0, Lhkd;->Oq:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 507
    const-string v0, "no_match"

    invoke-static {v0}, Lgnm;->nk(Ljava/lang/String;)V

    .line 511
    :cond_0
    iget-object v0, p0, Lhkh;->diZ:Lhkd;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lhkd;->li(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 512
    iget-object v0, p0, Lhkh;->diZ:Lhkd;

    iget-object v0, v0, Lhkd;->mSoundManager:Lhik;

    invoke-virtual {v0}, Lhik;->aPF()V

    .line 516
    :cond_1
    const-string v0, "VOICE_SEARCH_COMPLETE"

    invoke-static {v0}, Lgnm;->nj(Ljava/lang/String;)V

    .line 523
    iget-object v0, p0, Lhkh;->diZ:Lhkd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhkd;->ct(Z)V

    .line 524
    return-void
.end method

.method public final Nr()V
    .locals 1

    .prologue
    .line 443
    const-string v0, "SPEAK_NOW"

    invoke-static {v0}, Lgnm;->nj(Ljava/lang/String;)V

    .line 444
    const/4 v0, 0x5

    invoke-static {v0}, Lege;->ht(I)V

    .line 446
    iget-object v0, p0, Lhkh;->diZ:Lhkd;

    iget-object v0, v0, Lhkd;->diU:Lhsx;

    invoke-interface {v0}, Lhsx;->aQp()V

    .line 447
    return-void
.end method

.method public final Nv()V
    .locals 0

    .prologue
    .line 470
    return-void
.end method

.method public final Nw()V
    .locals 2

    .prologue
    .line 463
    iget-object v0, p0, Lhkh;->diZ:Lhkd;

    iget-object v0, v0, Lhkd;->diU:Lhsx;

    invoke-interface {v0}, Lhsx;->aQo()V

    .line 464
    iget-object v0, p0, Lhkh;->diZ:Lhkd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhkd;->ct(Z)V

    .line 465
    const-string v0, "VOICE_SEARCH_COMPLETE"

    invoke-static {v0}, Lgnm;->nj(Ljava/lang/String;)V

    .line 466
    return-void
.end method

.method public final a(Ljps;)V
    .locals 2

    .prologue
    .line 530
    invoke-static {p1}, Lico;->b(Ljps;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 533
    const-string v0, "HandsFreeRecognizerController"

    const-string v1, "Unexpected majel response in stream."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    :cond_0
    return-void
.end method

.method public final a(Ljvv;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 546
    invoke-static {p1}, Lgnm;->f(Ljvv;)V

    .line 548
    iget-object v0, p0, Lhkh;->bEC:Lgnp;

    invoke-virtual {v0}, Lgnp;->aHZ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 549
    const-string v0, "HandsFreeRecognizerController"

    const-string v1, "Result after completed recognition."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    :cond_0
    :goto_0
    return-void

    .line 553
    :cond_1
    invoke-virtual {p1}, Ljvv;->getEventType()I

    move-result v0

    if-nez v0, :cond_2

    .line 559
    iget-object v0, p0, Lhkh;->bEC:Lgnp;

    invoke-virtual {v0, p1}, Lgnp;->g(Ljvv;)Landroid/util/Pair;

    move-result-object v0

    .line 561
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    .line 562
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto :goto_0

    .line 567
    :cond_2
    invoke-virtual {p1}, Ljvv;->getEventType()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 571
    iget-object v0, p0, Lhkh;->bEC:Lgnp;

    iget-object v3, p1, Ljvv;->eIo:Ljvw;

    invoke-virtual {v0, v3}, Lgnp;->a(Ljvw;)Lijj;

    move-result-object v3

    .line 585
    invoke-virtual {v3}, Lijj;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v3, v1}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/speech/Hypothesis;

    invoke-virtual {v0}, Lcom/google/android/shared/speech/Hypothesis;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    move v0, v2

    .line 589
    :goto_1
    iget-object v2, p0, Lhkh;->diZ:Lhkd;

    const/4 v4, 0x4

    invoke-virtual {v2, v4}, Lhkd;->li(I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 590
    if-eqz v0, :cond_4

    .line 591
    iget-object v2, p0, Lhkh;->diZ:Lhkd;

    iget-object v2, v2, Lhkd;->mSoundManager:Lhik;

    invoke-virtual {v2}, Lhik;->aPF()V

    .line 599
    :cond_4
    if-eqz v0, :cond_6

    .line 600
    const-string v0, "HandsFreeRecognizerController"

    const-string v1, "Empty combined result"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    const-string v0, "no_match"

    invoke-static {v0}, Lgnm;->nk(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    move v0, v1

    .line 585
    goto :goto_1

    .line 603
    :cond_6
    iget-object v0, p0, Lhkh;->diZ:Lhkd;

    iget-object v0, v0, Lhkd;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aPf()Lgel;

    move-result-object v2

    .line 605
    iget-object v4, p0, Lhkh;->mRequestId:Ljava/lang/String;

    invoke-virtual {v3, v1}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/speech/Hypothesis;

    invoke-virtual {v2, v4, v0}, Lgel;->a(Ljava/lang/String;Lcom/google/android/shared/speech/Hypothesis;)Landroid/text/SpannedString;

    .line 610
    const/4 v0, 0x0

    iput-object v0, p0, Lhkh;->djc:Lcmk;

    goto :goto_0
.end method

.method public final aEM()V
    .locals 2

    .prologue
    .line 626
    iget-object v0, p0, Lhkh;->diZ:Lhkd;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lhkd;->li(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhkh;->bEC:Lgnp;

    invoke-virtual {v0}, Lgnp;->aHZ()Z

    move-result v0

    if-nez v0, :cond_0

    .line 627
    iget-object v0, p0, Lhkh;->diZ:Lhkd;

    iget-object v0, v0, Lhkd;->mSoundManager:Lhik;

    invoke-virtual {v0}, Lhik;->aPF()V

    .line 630
    :cond_0
    return-void
.end method

.method public final au(J)V
    .locals 0

    .prologue
    .line 453
    return-void
.end method

.method public final b(Ljwp;)V
    .locals 0

    .prologue
    .line 617
    return-void
.end method

.method public final c(Leiq;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 474
    invoke-static {p1}, Lhkd;->m(Leiq;)V

    .line 475
    invoke-virtual {p1}, Leiq;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgnm;->nk(Ljava/lang/String;)V

    .line 478
    iget-object v0, p0, Lhkh;->diZ:Lhkd;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lhkd;->li(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhkh;->bEC:Lgnp;

    invoke-virtual {v0}, Lgnp;->aHZ()Z

    move-result v0

    if-nez v0, :cond_0

    .line 479
    iget-object v0, p0, Lhkh;->diZ:Lhkd;

    iget-object v0, v0, Lhkd;->mSoundManager:Lhik;

    invoke-virtual {v0}, Lhik;->aPH()V

    .line 482
    :cond_0
    iget-object v0, p0, Lhkh;->diZ:Lhkd;

    iget v0, v0, Lhkd;->Oq:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 488
    iget-object v0, p0, Lhkh;->diZ:Lhkd;

    iget-object v0, v0, Lhkd;->diT:Lglu;

    iput-boolean v4, v0, Lglu;->zE:Z

    .line 489
    iget-object v0, p0, Lhkh;->bEC:Lgnp;

    iget-object v0, v0, Lgnp;->cQr:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 490
    const-string v1, "HandsFreeRecognizerController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Got error after recognizing ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    :cond_1
    iget-object v0, p0, Lhkh;->diZ:Lhkd;

    invoke-virtual {v0, v4}, Lhkd;->ct(Z)V

    .line 494
    return-void
.end method

.method public final onEndOfSpeech()V
    .locals 0

    .prologue
    .line 459
    return-void
.end method

.method public final v([B)V
    .locals 0

    .prologue
    .line 540
    return-void
.end method
