.class public final Lhax;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private anR:Z

.field final atG:Leqo;

.field private final dbj:Lhap;

.field public final dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

.field final dbn:Lhav;

.field final dbo:Lhbt;

.field private dbp:Lhbb;

.field dbq:Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;

.field dbr:I

.field final dbs:Lhbl;

.field dbt:Ldls;

.field dbu:Ljava/lang/Runnable;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final mAppContext:Landroid/content/Context;

.field private final mHttpHelper:Ldkx;

.field final mLoginHelper:Lcrh;

.field final mUrlHelper:Lcpn;


# direct methods
.method public constructor <init>(Leqo;Ldkx;Lcpn;Landroid/content/Context;Lhbt;Lhap;Lhav;Lcom/google/android/velvet/ui/InAppWebPageActivity;Lcrh;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-boolean v0, p0, Lhax;->anR:Z

    .line 91
    iput-object v1, p0, Lhax;->dbp:Lhbb;

    .line 94
    iput v0, p0, Lhax;->dbr:I

    .line 95
    new-instance v0, Lhbl;

    invoke-direct {v0}, Lhbl;-><init>()V

    iput-object v0, p0, Lhax;->dbs:Lhbl;

    .line 97
    iput-object v1, p0, Lhax;->dbt:Ldls;

    .line 100
    iput-object v1, p0, Lhax;->dbu:Ljava/lang/Runnable;

    .line 109
    iput-object p8, p0, Lhax;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    .line 110
    iput-object p4, p0, Lhax;->mAppContext:Landroid/content/Context;

    .line 111
    iput-object p1, p0, Lhax;->atG:Leqo;

    .line 112
    iput-object p2, p0, Lhax;->mHttpHelper:Ldkx;

    .line 113
    iput-object p3, p0, Lhax;->mUrlHelper:Lcpn;

    .line 114
    iput-object p5, p0, Lhax;->dbo:Lhbt;

    .line 115
    iput-object p6, p0, Lhax;->dbj:Lhap;

    .line 116
    iput-object p7, p0, Lhax;->dbn:Lhav;

    .line 117
    iput-object p9, p0, Lhax;->mLoginHelper:Lcrh;

    .line 118
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/velvet/presenter/inappwebpage/Request;)V
    .locals 1
    .param p1    # Lcom/google/android/velvet/presenter/inappwebpage/Request;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 129
    iget-boolean v0, p0, Lhax;->anR:Z

    if-nez v0, :cond_1

    .line 130
    new-instance v0, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;

    invoke-direct {v0}, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;-><init>()V

    iput-object v0, p0, Lhax;->dbq:Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;

    .line 132
    if-eqz p1, :cond_0

    .line 133
    iget-object v0, p0, Lhax;->dbq:Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;->c(Lcom/google/android/velvet/presenter/inappwebpage/Request;)V

    .line 135
    :cond_0
    invoke-virtual {p0}, Lhax;->aNe()V

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhax;->anR:Z

    .line 138
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/util/Printer;)V
    .locals 4

    .prologue
    .line 251
    new-instance v0, Leqg;

    invoke-direct {v0, p2, p1}, Leqg;-><init>(Landroid/util/Printer;Ljava/lang/String;)V

    .line 252
    const-string v1, "InAppWebPagePresenter:"

    invoke-virtual {v0, v1}, Leqg;->println(Ljava/lang/String;)V

    .line 253
    const-string v1, "  "

    invoke-virtual {v0, v1}, Leqg;->ll(Ljava/lang/String;)V

    .line 254
    iget-object v1, p0, Lhax;->dbt:Ldls;

    if-eqz v1, :cond_0

    .line 255
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhax;->dbt:Ldls;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Leqg;->println(Ljava/lang/String;)V

    .line 257
    :cond_0
    iget-object v1, p0, Lhax;->dbn:Lhav;

    iget v1, v1, Lhav;->dbl:I

    packed-switch v1, :pswitch_data_0

    .line 258
    :goto_0
    iget-object v1, p0, Lhax;->dbq:Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;

    invoke-virtual {v1, v0}, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;->a(Landroid/util/Printer;)V

    .line 259
    const-string v1, "Log:"

    invoke-virtual {v0, v1}, Leqg;->println(Ljava/lang/String;)V

    .line 260
    const-string v1, "  "

    invoke-virtual {v0, v1}, Leqg;->ll(Ljava/lang/String;)V

    .line 261
    iget-object v1, p0, Lhax;->dbs:Lhbl;

    monitor-enter v1

    .line 262
    :try_start_0
    iget-object v2, p0, Lhax;->dbs:Lhbl;

    invoke-virtual {v2}, Lhbl;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 263
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Leqg;->println(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 265
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 257
    :pswitch_0
    const-string v1, "DisplayState: NOTHING"

    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const-string v1, "DisplayState: WEB_VIEW"

    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const-string v1, "DisplayState: LOADING"

    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    const-string v1, "DisplayState: ERROR"

    invoke-interface {v0, v1}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 265
    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    .line 257
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method final aNc()V
    .locals 2

    .prologue
    .line 277
    invoke-virtual {p0}, Lhax;->aNd()V

    .line 278
    iget-object v0, p0, Lhax;->mHttpHelper:Ldkx;

    invoke-virtual {v0}, Ldkx;->acM()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a064c

    .line 281
    :goto_0
    iget-object v1, p0, Lhax;->dbn:Lhav;

    invoke-virtual {v1, v0}, Lhav;->ex(I)V

    .line 282
    return-void

    .line 278
    :cond_0
    const v0, 0x7f0a064d

    goto :goto_0
.end method

.method aNd()V
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lhax;->dbu:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lhax;->atG:Leqo;

    iget-object v1, p0, Lhax;->dbu:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 339
    const/4 v0, 0x0

    iput-object v0, p0, Lhax;->dbu:Ljava/lang/Runnable;

    .line 341
    :cond_0
    return-void
.end method

.method public final aNe()V
    .locals 6

    .prologue
    .line 349
    iget-object v0, p0, Lhax;->dbp:Lhbb;

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lhax;->dbp:Lhbb;

    invoke-virtual {v0}, Lhbb;->stop()V

    .line 353
    :cond_0
    iget-object v0, p0, Lhax;->dbq:Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 354
    iget-object v0, p0, Lhax;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->finish()V

    .line 367
    :goto_0
    return-void

    .line 356
    :cond_1
    iget-object v0, p0, Lhax;->dbq:Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;->aNo()Lcom/google/android/velvet/presenter/inappwebpage/Request;

    move-result-object v0

    .line 357
    invoke-virtual {p0}, Lhax;->aNd()V

    .line 358
    iget-object v1, p0, Lhax;->dbn:Lhav;

    invoke-virtual {v1}, Lhav;->aMW()V

    .line 360
    iget-object v1, p0, Lhax;->dbu:Ljava/lang/Runnable;

    new-instance v1, Lhba;

    const-string v2, "Loading indicator"

    invoke-direct {v1, p0, v2}, Lhba;-><init>(Lhax;Ljava/lang/String;)V

    iput-object v1, p0, Lhax;->dbu:Ljava/lang/Runnable;

    iget-object v1, p0, Lhax;->atG:Leqo;

    iget-object v2, p0, Lhax;->dbu:Ljava/lang/Runnable;

    const-wide/16 v4, 0xbb8

    invoke-interface {v1, v2, v4, v5}, Leqo;->a(Ljava/lang/Runnable;J)V

    .line 361
    iget v1, p0, Lhax;->dbr:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lhax;->dbr:I

    .line 362
    new-instance v1, Lhbc;

    iget v2, p0, Lhax;->dbr:I

    invoke-direct {v1, p0, v2}, Lhbc;-><init>(Lhax;I)V

    .line 363
    iget-object v2, p0, Lhax;->dbj:Lhap;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/inappwebpage/Request;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lhap;->a(Landroid/net/Uri;Lhau;)Livq;

    move-result-object v1

    .line 365
    new-instance v2, Lhbb;

    invoke-direct {v2, p0, v0, v1}, Lhbb;-><init>(Lhax;Lcom/google/android/velvet/presenter/inappwebpage/Request;Livq;)V

    iput-object v2, p0, Lhax;->dbp:Lhbb;

    goto :goto_0
.end method

.method public final ac(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 144
    const-string v0, "InAppWebPage.RequestStack"

    iget-object v1, p0, Lhax;->dbq:Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 145
    return-void
.end method

.method public final ad(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 152
    iget-boolean v0, p0, Lhax;->anR:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 153
    if-eqz p1, :cond_0

    .line 154
    const-string v0, "InAppWebPage.RequestStack"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;

    iput-object v0, p0, Lhax;->dbq:Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;

    .line 155
    iget-object v0, p0, Lhax;->dbq:Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;

    if-eqz v0, :cond_0

    .line 156
    invoke-virtual {p0}, Lhax;->aNe()V

    .line 157
    iput-boolean v1, p0, Lhax;->anR:Z

    .line 160
    :cond_0
    return-void

    .line 152
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final destroy()V
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lhax;->dbp:Lhbb;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lhax;->dbp:Lhbb;

    invoke-virtual {v0}, Lhbb;->stop()V

    .line 200
    :cond_0
    iget-object v0, p0, Lhax;->dbn:Lhav;

    invoke-virtual {v0}, Lhav;->aNb()V

    .line 201
    return-void
.end method

.method public final g(Landroid/view/Menu;)V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lhax;->dbn:Lhav;

    invoke-virtual {v0, p1}, Lhav;->g(Landroid/view/Menu;)V

    .line 248
    return-void
.end method

.method public final goBack()V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lhax;->dbq:Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;

    if-nez v0, :cond_1

    .line 173
    iget-object v0, p0, Lhax;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->finish()V

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    iget-object v0, p0, Lhax;->dbn:Lhav;

    invoke-virtual {v0}, Lhav;->aNa()Z

    move-result v0

    if-nez v0, :cond_0

    .line 182
    iget-object v0, p0, Lhax;->dbq:Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 183
    iget-object v0, p0, Lhax;->dbq:Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;->aNp()V

    .line 184
    invoke-virtual {p0}, Lhax;->aNe()V

    goto :goto_0
.end method

.method public final h(Landroid/view/Menu;)V
    .locals 4

    .prologue
    const/high16 v3, 0x30000

    const/4 v2, 0x0

    .line 208
    const v0, 0x7f110007

    const v1, 0x7f0a0153

    invoke-interface {p1, v2, v0, v3, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lhay;

    invoke-direct {v1, p0}, Lhay;-><init>(Lhax;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 209
    const v0, 0x7f0a0583

    invoke-interface {p1, v2, v2, v3, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lhaz;

    invoke-direct {v1, p0}, Lhaz;-><init>(Lhax;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 210
    return-void
.end method
