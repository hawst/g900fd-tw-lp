.class public final Lfxv;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 24
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 4

    .prologue
    .line 34
    iget-object v1, p0, Lfro;->mView:Landroid/view/View;

    .line 35
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v0, v0, Lanh;->agA:Laon;

    .line 36
    invoke-virtual {v0}, Laon;->pb()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 37
    const v2, 0x7f1101a9

    invoke-virtual {v0}, Laon;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 39
    :cond_0
    invoke-virtual {v0}, Laon;->om()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 40
    const v2, 0x7f110080

    invoke-virtual {v0}, Laon;->ol()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 43
    :cond_1
    const v0, 0x7f110326

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 44
    iget-object v2, p0, Lfro;->cBc:Lanh;

    iget-object v2, v2, Lanh;->aht:Lani;

    .line 45
    if-eqz v2, :cond_4

    .line 46
    if-nez v0, :cond_5

    .line 47
    const v0, 0x7f110325

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 48
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 51
    :goto_0
    const v0, 0x7f11005e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v3, v2, Lani;->ahC:Lanb;

    invoke-virtual {v3}, Lanb;->oh()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 53
    invoke-virtual {v2}, Lani;->pa()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 54
    invoke-virtual {v2}, Lani;->oZ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 56
    :cond_2
    invoke-virtual {p0, v1, v2}, Lfxv;->a(Landroid/view/View;Lani;)V

    .line 60
    :cond_3
    :goto_1
    return-void

    .line 57
    :cond_4
    if-eqz v0, :cond_3

    .line 58
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_5
    move-object v1, v0

    goto :goto_0
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 28
    const v0, 0x7f040124

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
