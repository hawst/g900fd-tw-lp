.class public abstract Lciy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final aXW:Ljava/util/List;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lciy;->aXW:Ljava/util/List;

    .line 34
    return-void
.end method

.method private static e(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/contact/PersonDisambiguation;
    .locals 1
    .param p0    # Lcom/google/android/search/shared/actions/VoiceAction;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 238
    invoke-interface {p0}, Lcom/google/android/search/shared/actions/VoiceAction;->agy()Ldxl;

    move-result-object v0

    .line 239
    if-nez v0, :cond_0

    .line 241
    const/4 v0, 0x0

    .line 243
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ldxl;->aho()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final KP()V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lciy;->aXW:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 152
    invoke-virtual {p0}, Lciy;->KR()V

    .line 153
    return-void
.end method

.method protected abstract KQ()V
.end method

.method protected abstract KR()V
.end method

.method public final a(Lcom/google/android/search/shared/contact/PersonShortcutKey;)Lcom/google/android/search/shared/contact/PersonShortcut;
    .locals 1
    .param p1    # Lcom/google/android/search/shared/contact/PersonShortcutKey;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p0, p1}, Lciy;->c(Lcom/google/android/search/shared/contact/PersonShortcutKey;)Lcom/google/android/search/shared/contact/PersonShortcut;

    move-result-object v0

    .line 49
    if-nez v0, :cond_0

    .line 51
    const/4 v0, 0x0

    .line 54
    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/search/shared/contact/PersonShortcut;)Z
    .locals 3
    .param p1    # Lcom/google/android/search/shared/contact/PersonShortcut;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 64
    if-nez p1, :cond_1

    .line 81
    :cond_0
    :goto_0
    return v0

    .line 68
    :cond_1
    invoke-virtual {p0, p1}, Lciy;->c(Lcom/google/android/search/shared/contact/PersonShortcutKey;)Lcom/google/android/search/shared/contact/PersonShortcut;

    move-result-object v1

    .line 69
    if-eqz v1, :cond_2

    .line 71
    invoke-virtual {v1, p1}, Lcom/google/android/search/shared/contact/PersonShortcut;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 76
    iget-object v0, p0, Lciy;->aXW:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 78
    :cond_2
    const/16 v0, 0x134

    invoke-static {v0}, Lege;->ht(I)V

    .line 79
    iget-object v0, p0, Lciy;->aXW:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    invoke-virtual {p0}, Lciy;->KR()V

    .line 81
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Lcom/google/android/search/shared/contact/PersonShortcutKey;)Z
    .locals 2
    .param p1    # Lcom/google/android/search/shared/contact/PersonShortcutKey;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 91
    invoke-virtual {p0, p1}, Lciy;->c(Lcom/google/android/search/shared/contact/PersonShortcutKey;)Lcom/google/android/search/shared/contact/PersonShortcut;

    move-result-object v0

    .line 92
    if-nez v0, :cond_0

    .line 94
    const/4 v0, 0x0

    .line 100
    :goto_0
    return v0

    .line 97
    :cond_0
    const/16 v1, 0x135

    invoke-static {v1}, Lege;->ht(I)V

    .line 98
    iget-object v1, p0, Lciy;->aXW:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 99
    invoke-virtual {p0}, Lciy;->KR()V

    .line 100
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c(Lcom/google/android/search/shared/contact/PersonShortcutKey;)Lcom/google/android/search/shared/contact/PersonShortcut;
    .locals 5
    .param p1    # Lcom/google/android/search/shared/contact/PersonShortcutKey;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 248
    if-eqz p1, :cond_0

    iget-object v0, p0, Lciy;->aXW:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 259
    :goto_0
    return-object v0

    .line 251
    :cond_1
    iget-object v0, p0, Lciy;->aXW:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonShortcut;

    .line 253
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonShortcut;->amn()Ldzb;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonShortcutKey;->amn()Ldzb;

    move-result-object v4

    if-ne v3, v4, :cond_2

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonShortcut;->KK()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonShortcutKey;->KK()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonShortcut;->amo()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonShortcutKey;->amo()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 259
    goto :goto_0
.end method

.method public final c(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 7
    .param p1    # Lcom/google/android/search/shared/actions/VoiceAction;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    .line 114
    invoke-static {p1}, Lciy;->e(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v5

    .line 115
    if-nez v5, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    invoke-virtual {v5}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->amh()Lcom/google/android/search/shared/contact/PersonShortcut;

    move-result-object v0

    .line 119
    if-nez v0, :cond_0

    .line 120
    invoke-virtual {v5}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v0

    if-nez v0, :cond_3

    .line 121
    :cond_2
    :goto_1
    invoke-virtual {p0, v6}, Lciy;->a(Lcom/google/android/search/shared/contact/PersonShortcut;)Z

    goto :goto_0

    .line 120
    :cond_3
    invoke-virtual {v5}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->amg()Lcom/google/android/search/shared/contact/PersonShortcutKey;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v5}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/android/search/shared/contact/Person;

    if-nez v4, :cond_4

    const-string v0, "PersonShortcutManager"

    const-string v1, "createShortcutForRecipient() : Null Person"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v5}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alD()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v5}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alD()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    new-instance v0, Lcom/google/android/search/shared/contact/PersonShortcut;

    invoke-virtual {v4}, Lcom/google/android/search/shared/contact/Person;->getId()J

    move-result-wide v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/search/shared/contact/PersonShortcut;-><init>(Lcom/google/android/search/shared/contact/PersonShortcutKey;JJLjava/lang/String;)V

    move-object v6, v0

    goto :goto_1

    :cond_6
    invoke-virtual {v5}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alF()Landroid/os/Parcelable;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/search/shared/contact/Contact;

    new-instance v0, Lcom/google/android/search/shared/contact/PersonShortcut;

    invoke-virtual {v4}, Lcom/google/android/search/shared/contact/Person;->getId()J

    move-result-wide v4

    invoke-virtual {v6}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/search/shared/contact/PersonShortcut;-><init>(Lcom/google/android/search/shared/contact/PersonShortcutKey;JJLjava/lang/String;)V

    move-object v6, v0

    goto :goto_1
.end method

.method public final d(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 1
    .param p1    # Lcom/google/android/search/shared/actions/VoiceAction;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 135
    invoke-static {p1}, Lciy;->e(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    .line 136
    if-nez v0, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->amh()Lcom/google/android/search/shared/contact/PersonShortcut;

    move-result-object v0

    .line 140
    if-eqz v0, :cond_0

    .line 143
    invoke-virtual {p0, v0}, Lciy;->b(Lcom/google/android/search/shared/contact/PersonShortcutKey;)Z

    goto :goto_0
.end method
