.class public final Lkag;
.super Ljava/net/ResponseCache;
.source "PG"


# instance fields
.field private final ePg:Lkbb;

.field private ePh:I

.field private ePi:I

.field private ePj:I

.field private ePk:I

.field private ePl:I

.field final ePm:Lkau;


# direct methods
.method public constructor <init>(Ljava/io/File;J)V
    .locals 2

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/net/ResponseCache;-><init>()V

    .line 140
    new-instance v0, Lkah;

    invoke-direct {v0, p0}, Lkah;-><init>(Lkag;)V

    iput-object v0, p0, Lkag;->ePm:Lkau;

    .line 169
    const v0, 0x31191

    const/4 v1, 0x2

    invoke-static {p1, v0, v1, p2, p3}, Lkbb;->a(Ljava/io/File;IIJ)Lkbb;

    move-result-object v0

    iput-object v0, p0, Lkag;->ePg:Lkbb;

    .line 170
    return-void
.end method

.method static synthetic a(Lkbh;)Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 118
    new-instance v0, Lkai;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lkbh;->tJ(I)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lkai;-><init>(Ljava/io/InputStream;Lkbh;)V

    return-object v0
.end method

.method private static a(Ljava/net/URLConnection;)Lkcc;
    .locals 1

    .prologue
    .line 298
    instance-of v0, p0, Lkcj;

    if-eqz v0, :cond_0

    .line 299
    check-cast p0, Lkcj;

    invoke-virtual {p0}, Lkcj;->byK()Lkcc;

    move-result-object v0

    .line 303
    :goto_0
    return-object v0

    .line 300
    :cond_0
    instance-of v0, p0, Lkcm;

    if-eqz v0, :cond_1

    .line 301
    check-cast p0, Lkcm;

    invoke-virtual {p0}, Lkcm;->byK()Lkcc;

    move-result-object v0

    goto :goto_0

    .line 303
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lkag;)V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0}, Lkag;->bxI()V

    return-void
.end method

.method static synthetic a(Lkag;Ljava/net/CacheResponse;Ljava/net/HttpURLConnection;)V
    .locals 3

    .prologue
    .line 118
    invoke-static {p2}, Lkag;->a(Ljava/net/URLConnection;)Lkcc;

    move-result-object v0

    invoke-virtual {v0}, Lkcc;->byk()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v0}, Lkcc;->byr()Lkcu;

    move-result-object v2

    invoke-virtual {v0}, Lkcc;->byq()Lkcs;

    move-result-object v0

    iget-object v0, v0, Lkcs;->eRZ:Lkcq;

    invoke-virtual {v2}, Lkcu;->byY()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v0, v2}, Lkcq;->g(Ljava/util/Set;)Lkcq;

    move-result-object v0

    new-instance v2, Lkam;

    invoke-direct {v2, v1, v0, p2}, Lkam;-><init>(Ljava/net/URI;Lkcq;Ljava/net/HttpURLConnection;)V

    instance-of v0, p1, Lkan;

    if-eqz v0, :cond_1

    check-cast p1, Lkan;

    invoke-static {p1}, Lkan;->a(Lkan;)Lkbh;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0}, Lkbh;->byc()Lkbe;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {v2, v0}, Lkam;->b(Lkbe;)V

    invoke-virtual {v0}, Lkbe;->commit()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    :goto_1
    return-void

    :cond_1
    check-cast p1, Lkao;

    invoke-static {p1}, Lkao;->a(Lkao;)Lkbh;

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_2
    invoke-static {v0}, Lkag;->a(Lkbe;)V

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method static synthetic a(Lkag;Lkav;)V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lkag;->a(Lkav;)V

    return-void
.end method

.method private declared-synchronized a(Lkav;)V
    .locals 2

    .prologue
    .line 349
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lkag;->ePl:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkag;->ePl:I

    .line 351
    sget-object v0, Lkaj;->ePp:[I

    invoke-virtual {p1}, Lkav;->ordinal()I

    move-result v1

    aget v0, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v0, :pswitch_data_0

    .line 360
    :goto_0
    monitor-exit p0

    return-void

    .line 353
    :pswitch_0
    :try_start_1
    iget v0, p0, Lkag;->ePk:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkag;->ePk:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 349
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 357
    :pswitch_1
    :try_start_2
    iget v0, p0, Lkag;->ePj:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkag;->ePj:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 351
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lkbe;)V
    .locals 1

    .prologue
    .line 290
    if-eqz p0, :cond_0

    .line 291
    :try_start_0
    invoke-virtual {p0}, Lkbe;->abort()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/net/URI;)Z
    .locals 2

    .prologue
    .line 252
    const-string v0, "POST"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "PUT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "DELETE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 255
    :cond_0
    :try_start_0
    iget-object v0, p0, Lkag;->ePg:Lkbb;

    invoke-virtual {p2}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lkbt;->Av(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkbb;->Ar(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 259
    :goto_0
    const/4 v0, 0x1

    .line 261
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lkag;Ljava/lang/String;Ljava/net/URI;)Z
    .locals 1

    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Lkag;->a(Ljava/lang/String;Ljava/net/URI;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lkag;)I
    .locals 2

    .prologue
    .line 118
    iget v0, p0, Lkag;->ePh:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lkag;->ePh:I

    return v0
.end method

.method private declared-synchronized bxI()V
    .locals 1

    .prologue
    .line 363
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lkag;->ePk:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkag;->ePk:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 364
    monitor-exit p0

    return-void

    .line 363
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lkag;)I
    .locals 2

    .prologue
    .line 118
    iget v0, p0, Lkag;->ePi:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lkag;->ePi:I

    return v0
.end method


# virtual methods
.method public final flush()V
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lkag;->ePg:Lkbb;

    invoke-virtual {v0}, Lkbb;->flush()V

    .line 334
    return-void
.end method

.method public final get(Ljava/net/URI;Ljava/lang/String;Ljava/util/Map;)Ljava/net/CacheResponse;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 178
    invoke-virtual {p1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lkbt;->Av(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 182
    :try_start_0
    iget-object v3, p0, Lkag;->ePg:Lkbb;

    invoke-virtual {v3, v2}, Lkbb;->Ap(Ljava/lang/String;)Lkbh;

    move-result-object v2

    .line 183
    if-nez v2, :cond_0

    .line 197
    :goto_0
    return-object v0

    .line 186
    :cond_0
    new-instance v3, Lkam;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lkbh;->tJ(I)Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v3, v4}, Lkam;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    iget-object v4, v3, Lkam;->XT:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v3, Lkam;->ePw:Ljava/lang/String;

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    new-instance v4, Lkcu;

    iget-object v5, v3, Lkam;->ePx:Lkcq;

    invoke-direct {v4, p1, v5}, Lkcu;-><init>(Ljava/net/URI;Lkcq;)V

    iget-object v5, v3, Lkam;->ePv:Lkcq;

    invoke-virtual {v5, v1}, Lkcq;->jK(Z)Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v4, v5, p3}, Lkcu;->a(Ljava/util/Map;Ljava/util/Map;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v1, 0x1

    :cond_1
    if-nez v1, :cond_2

    .line 193
    invoke-virtual {v2}, Lkbh;->close()V

    goto :goto_0

    .line 197
    :cond_2
    invoke-virtual {v3}, Lkam;->bxJ()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lkao;

    invoke-direct {v0, v3, v2}, Lkao;-><init>(Lkam;Lkbh;)V

    goto :goto_0

    :cond_3
    new-instance v0, Lkan;

    invoke-direct {v0, v3, v2}, Lkan;-><init>(Lkam;Lkbh;)V

    goto :goto_0

    .line 189
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final put(Ljava/net/URI;Ljava/net/URLConnection;)Ljava/net/CacheRequest;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 202
    instance-of v1, p2, Ljava/net/HttpURLConnection;

    if-nez v1, :cond_1

    .line 243
    :cond_0
    :goto_0
    return-object v0

    .line 206
    :cond_1
    check-cast p2, Ljava/net/HttpURLConnection;

    .line 207
    invoke-virtual {p2}, Ljava/net/HttpURLConnection;->getRequestMethod()Ljava/lang/String;

    move-result-object v1

    .line 209
    invoke-direct {p0, v1, p1}, Lkag;->a(Ljava/lang/String;Ljava/net/URI;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 212
    const-string v2, "GET"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 219
    invoke-static {p2}, Lkag;->a(Ljava/net/URLConnection;)Lkcc;

    move-result-object v1

    .line 220
    if-eqz v1, :cond_0

    .line 225
    invoke-virtual {v1}, Lkcc;->byr()Lkcu;

    move-result-object v2

    .line 226
    invoke-virtual {v2}, Lkcu;->byZ()Z

    move-result v3

    if-nez v3, :cond_0

    .line 230
    invoke-virtual {v1}, Lkcc;->byq()Lkcs;

    move-result-object v1

    iget-object v1, v1, Lkcs;->eRZ:Lkcq;

    invoke-virtual {v2}, Lkcu;->byY()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Lkcq;->g(Ljava/util/Set;)Lkcq;

    move-result-object v1

    .line 232
    new-instance v3, Lkam;

    invoke-direct {v3, p1, v1, p2}, Lkam;-><init>(Ljava/net/URI;Lkcq;Ljava/net/HttpURLConnection;)V

    .line 235
    :try_start_0
    iget-object v1, p0, Lkag;->ePg:Lkbb;

    invoke-virtual {p1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lkbt;->Av(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lkbb;->Aq(Ljava/lang/String;)Lkbe;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 236
    if-eqz v2, :cond_0

    .line 239
    :try_start_1
    invoke-virtual {v3, v2}, Lkam;->b(Lkbe;)V

    .line 240
    new-instance v1, Lkak;

    invoke-direct {v1, p0, v2}, Lkak;-><init>(Lkag;Lkbe;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    goto :goto_0

    .line 242
    :catch_0
    move-exception v1

    move-object v1, v0

    :goto_1
    invoke-static {v1}, Lkag;->a(Lkbe;)V

    goto :goto_0

    :catch_1
    move-exception v1

    move-object v1, v2

    goto :goto_1
.end method
