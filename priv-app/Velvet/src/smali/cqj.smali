.class public final Lcqj;
.super Lcqs;
.source "PG"


# instance fields
.field private final bgj:Lcmw;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mNetworkInfo:Lgno;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field mSearchResult:Lcmq;

.field private final mSearchUrlHelper:Lcpn;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lchk;Ldkx;Lcpn;Lcrh;Lcmw;Lgno;)V
    .locals 1
    .param p5    # Lcmw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 73
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcrh;

    invoke-direct {p0, p1, p2, v0}, Lcqs;-><init>(Lchk;Ldkx;Lcrh;)V

    .line 74
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpn;

    iput-object v0, p0, Lcqj;->mSearchUrlHelper:Lcpn;

    .line 75
    iput-object p5, p0, Lcqj;->bgj:Lcmw;

    .line 76
    invoke-static {p6}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgno;

    iput-object v0, p0, Lcqj;->mNetworkInfo:Lgno;

    .line 77
    return-void
.end method

.method public constructor <init>(Lchk;Ldkx;Lcpn;Lgno;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0, p1, p2, v1}, Lcqs;-><init>(Lchk;Ldkx;Lcrh;)V

    .line 58
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpn;

    iput-object v0, p0, Lcqj;->mSearchUrlHelper:Lcpn;

    .line 59
    iput-object v1, p0, Lcqj;->bgj:Lcmw;

    .line 60
    iput-object p4, p0, Lcqj;->mNetworkInfo:Lgno;

    .line 61
    return-void
.end method

.method private c(Lcom/google/android/shared/search/Query;Z)Lcqt;
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0xce

    .line 180
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    .line 184
    sget-object v0, Leoi;->cgG:Leoi;

    invoke-virtual {v0}, Leoi;->auU()J

    move-result-wide v6

    .line 185
    const/16 v0, 0xcd

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Litu;->bW(J)Litu;

    move-result-object v0

    .line 190
    iget-object v2, p0, Lcqj;->mGsaConfig:Lchk;

    invoke-virtual {v2}, Lchk;->JR()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 191
    iget-object v2, p0, Lcqj;->mNetworkInfo:Lgno;

    invoke-virtual {v2}, Lgno;->aAn()I

    move-result v2

    invoke-virtual {v0, v2}, Litu;->mG(I)Litu;

    .line 194
    :cond_0
    invoke-static {v0}, Lege;->a(Litu;)V

    .line 197
    :try_start_0
    iget-object v0, p0, Lcqs;->mLoginHelper:Lcrh;

    if-nez v0, :cond_3

    move-object v4, v1

    .line 201
    :goto_0
    if-eqz p2, :cond_4

    .line 202
    iget-object v0, p0, Lcqj;->mSearchUrlHelper:Lcpn;

    invoke-virtual {v0, p1}, Lcpn;->m(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    move-object v3, v0

    .line 206
    :goto_1
    invoke-virtual {v3}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    .line 207
    invoke-virtual {v3}, Lcom/google/android/search/shared/api/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v2

    .line 208
    const/4 v0, 0x1

    .line 209
    if-nez p2, :cond_1

    .line 210
    const v0, 0x10000001

    .line 213
    invoke-virtual {v3}, Lcom/google/android/search/shared/api/UriRequest;->alI()Ljava/util/Map;

    move-result-object v2

    .line 214
    const-string v3, "Cookie"

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    :cond_1
    new-instance v3, Ldlb;

    invoke-direct {v3, v5, v2}, Ldlb;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    .line 227
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ldlb;->setUseCaches(Z)V

    .line 228
    iget-object v2, p0, Lcqj;->mGsaConfig:Lchk;

    invoke-virtual {v2}, Lchk;->JF()Z

    move-result v2

    invoke-virtual {v3, v2}, Ldlb;->dz(Z)V

    .line 231
    iget-object v2, p0, Lcqj;->mHttpHelper:Ldkx;

    invoke-virtual {v2, v3, v0}, Ldkx;->b(Ldlb;I)Ljava/lang/String;

    move-result-object v2

    .line 234
    new-instance v0, Lcqt;

    const-string v3, ""

    invoke-direct {v0, v3, v2, v4}, Lcqt;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Left; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    invoke-static {v8}, Lege;->hs(I)Litu;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Litu;->bW(J)Litu;

    move-result-object v1

    .line 244
    iget-object v2, p0, Lcqj;->mGsaConfig:Lchk;

    invoke-virtual {v2}, Lchk;->JR()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 245
    iget-object v2, p0, Lcqj;->mNetworkInfo:Lgno;

    invoke-virtual {v2}, Lgno;->aAn()I

    move-result v2

    invoke-virtual {v1, v2}, Litu;->mG(I)Litu;

    .line 247
    :cond_2
    invoke-static {v1}, Lege;->a(Litu;)V

    .line 249
    :goto_2
    return-object v0

    .line 197
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcqs;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    goto :goto_0

    .line 204
    :cond_4
    iget-object v0, p0, Lcqj;->mSearchUrlHelper:Lcpn;

    invoke-virtual {v0, p1}, Lcpn;->n(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/api/UriRequest;
    :try_end_1
    .catch Left; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    move-object v3, v0

    goto :goto_1

    .line 240
    :catch_0
    move-exception v0

    invoke-static {v8}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Litu;->bW(J)Litu;

    move-result-object v0

    .line 244
    iget-object v2, p0, Lcqj;->mGsaConfig:Lchk;

    invoke-virtual {v2}, Lchk;->JR()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 245
    iget-object v2, p0, Lcqj;->mNetworkInfo:Lgno;

    invoke-virtual {v2}, Lgno;->aAn()I

    move-result v2

    invoke-virtual {v0, v2}, Litu;->mG(I)Litu;

    .line 247
    :cond_5
    invoke-static {v0}, Lege;->a(Litu;)V

    :goto_3
    move-object v0, v1

    .line 249
    goto :goto_2

    .line 240
    :catch_1
    move-exception v0

    invoke-static {v8}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Litu;->bW(J)Litu;

    move-result-object v0

    .line 244
    iget-object v2, p0, Lcqj;->mGsaConfig:Lchk;

    invoke-virtual {v2}, Lchk;->JR()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 245
    iget-object v2, p0, Lcqj;->mNetworkInfo:Lgno;

    invoke-virtual {v2}, Lgno;->aAn()I

    move-result v2

    invoke-virtual {v0, v2}, Litu;->mG(I)Litu;

    .line 247
    :cond_6
    invoke-static {v0}, Lege;->a(Litu;)V

    goto :goto_3

    .line 240
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lege;->hs(I)Litu;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Litu;->bW(J)Litu;

    move-result-object v1

    .line 244
    iget-object v2, p0, Lcqj;->mGsaConfig:Lchk;

    invoke-virtual {v2}, Lchk;->JR()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 245
    iget-object v2, p0, Lcqj;->mNetworkInfo:Lgno;

    invoke-virtual {v2}, Lgno;->aAn()I

    move-result v2

    invoke-virtual {v1, v2}, Litu;->mG(I)Litu;

    .line 247
    :cond_7
    invoke-static {v1}, Lege;->a(Litu;)V

    .line 248
    throw v0
.end method

.method private t(Lcom/google/android/shared/search/Query;)Lcqt;
    .locals 7
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/16 v6, 0xce

    .line 122
    iget-object v0, p0, Lcqj;->bgj:Lcmw;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Leoi;->cgG:Leoi;

    invoke-virtual {v0}, Leoi;->auU()J

    move-result-wide v2

    .line 125
    new-instance v0, Legl;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->AF()J

    move-result-wide v4

    invoke-direct {v0, v4, v5, v2, v3}, Legl;-><init>(JJ)V

    .line 126
    const/16 v1, 0xcd

    invoke-static {v1}, Lege;->hs(I)Litu;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Litu;->bW(J)Litu;

    move-result-object v1

    .line 131
    iget-object v4, p0, Lcqj;->mGsaConfig:Lchk;

    invoke-virtual {v4}, Lchk;->JR()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 132
    iget-object v4, p0, Lcqj;->mNetworkInfo:Lgno;

    invoke-virtual {v4}, Lgno;->aAn()I

    move-result v4

    invoke-virtual {v1, v4}, Litu;->mG(I)Litu;

    .line 135
    :cond_0
    invoke-static {v1}, Lege;->a(Litu;)V

    .line 139
    :try_start_0
    iget-object v1, p0, Lcqj;->bgj:Lcmw;

    invoke-virtual {v1, p1, v0}, Lcmw;->a(Lcom/google/android/shared/search/Query;Legl;)Lcmq;

    move-result-object v0

    iput-object v0, p0, Lcqj;->mSearchResult:Lcmq;

    .line 143
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 144
    new-instance v1, Lcqk;

    invoke-direct {v1, p0, v0}, Lcqk;-><init>(Lcqj;Ljava/util/concurrent/CountDownLatch;)V

    .line 152
    iget-object v4, p0, Lcqj;->mSearchResult:Lcmq;

    invoke-virtual {v4, v1}, Lcmq;->addObserver(Ljava/util/Observer;)V

    .line 153
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157
    :try_start_1
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163
    :goto_0
    invoke-static {v6}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Litu;->bW(J)Litu;

    move-result-object v0

    .line 167
    iget-object v1, p0, Lcqj;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->JR()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 168
    iget-object v1, p0, Lcqj;->mNetworkInfo:Lgno;

    invoke-virtual {v1}, Lgno;->aAn()I

    move-result v1

    invoke-virtual {v0, v1}, Litu;->mG(I)Litu;

    .line 170
    :cond_1
    invoke-static {v0}, Lege;->a(Litu;)V

    .line 172
    iget-object v0, p0, Lcqj;->mSearchResult:Lcmq;

    invoke-virtual {v0}, Lcmq;->Qu()Lcqt;

    move-result-object v0

    return-object v0

    .line 160
    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lege;->hs(I)Litu;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Litu;->bW(J)Litu;

    move-result-object v1

    .line 167
    iget-object v2, p0, Lcqj;->mGsaConfig:Lchk;

    invoke-virtual {v2}, Lchk;->JR()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 168
    iget-object v2, p0, Lcqj;->mNetworkInfo:Lgno;

    invoke-virtual {v2}, Lgno;->aAn()I

    move-result v2

    invoke-virtual {v1, v2}, Litu;->mG(I)Litu;

    .line 170
    :cond_2
    invoke-static {v1}, Lege;->a(Litu;)V

    .line 171
    throw v0
.end method


# virtual methods
.method public final b(Lcom/google/android/shared/search/Query;Z)Lcqt;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 103
    if-eqz p2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcqj;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->HH()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    invoke-direct {p0, p1}, Lcqj;->t(Lcom/google/android/shared/search/Query;)Lcqt;

    move-result-object v0

    .line 111
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcqj;->c(Lcom/google/android/shared/search/Query;Z)Lcqt;

    move-result-object v0

    iget-object v1, p0, Lcqj;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->JP()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-object v2, v0, Lcqt;->bgF:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcqt;->bgF:Ljava/lang/String;

    goto :goto_0
.end method
