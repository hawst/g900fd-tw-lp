.class public final Ljqd;
.super Ljsl;
.source "PG"


# static fields
.field public static final eyo:Ljsm;

.field public static final eyp:Ljsm;


# instance fields
.field private aez:I

.field private dMI:Ljava/lang/String;

.field private dOO:Ljava/lang/String;

.field public dOb:[Ljava/lang/String;

.field private eyq:I

.field public eyr:Ljqv;

.field public eys:Ljqv;

.field public eyt:Ljqv;

.field public eyu:[Ljqe;

.field private eyv:Z

.field private eyw:Z

.field private eyx:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0xb

    .line 5281
    const-class v0, Ljqd;

    const/16 v1, 0x1f42

    invoke-static {v2, v0, v1}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljqd;->eyo:Ljsm;

    .line 5291
    const-class v0, Ljqd;

    const v1, 0x1db1e292

    invoke-static {v2, v0, v1}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljqd;->eyp:Ljsm;

    .line 5477
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 5619
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 5620
    iput v1, p0, Ljqd;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljqd;->dOO:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljqd;->dMI:Ljava/lang/String;

    iput v1, p0, Ljqd;->eyq:I

    iput-object v2, p0, Ljqd;->eyr:Ljqv;

    iput-object v2, p0, Ljqd;->eys:Ljqv;

    iput-object v2, p0, Ljqd;->eyt:Ljqv;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljqd;->dOb:[Ljava/lang/String;

    invoke-static {}, Ljqe;->bsm()[Ljqe;

    move-result-object v0

    iput-object v0, p0, Ljqd;->eyu:[Ljqe;

    iput-boolean v1, p0, Ljqd;->eyv:Z

    iput-boolean v1, p0, Ljqd;->eyw:Z

    iput-boolean v1, p0, Ljqd;->eyx:Z

    iput-object v2, p0, Ljqd;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqd;->eCz:I

    .line 5621
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5274
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqd;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqd;->dOO:Ljava/lang/String;

    iget v0, p0, Ljqd;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqd;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqd;->eyq:I

    iget v0, p0, Ljqd;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljqd;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljqd;->eyr:Ljqv;

    if-nez v0, :cond_1

    new-instance v0, Ljqv;

    invoke-direct {v0}, Ljqv;-><init>()V

    iput-object v0, p0, Ljqd;->eyr:Ljqv;

    :cond_1
    iget-object v0, p0, Ljqd;->eyr:Ljqv;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljqd;->eys:Ljqv;

    if-nez v0, :cond_2

    new-instance v0, Ljqv;

    invoke-direct {v0}, Ljqv;-><init>()V

    iput-object v0, p0, Ljqd;->eys:Ljqv;

    :cond_2
    iget-object v0, p0, Ljqd;->eys:Ljqv;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljqd;->eyu:[Ljqe;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljqe;

    if-eqz v0, :cond_3

    iget-object v3, p0, Ljqd;->eyu:[Ljqe;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Ljqe;

    invoke-direct {v3}, Ljqe;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ljqd;->eyu:[Ljqe;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Ljqe;

    invoke-direct {v3}, Ljqe;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljqd;->eyu:[Ljqe;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqd;->dMI:Ljava/lang/String;

    iget v0, p0, Ljqd;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqd;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljqd;->eyv:Z

    iget v0, p0, Ljqd;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljqd;->aez:I

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ljqd;->eyt:Ljqv;

    if-nez v0, :cond_6

    new-instance v0, Ljqv;

    invoke-direct {v0}, Ljqv;-><init>()V

    iput-object v0, p0, Ljqd;->eyt:Ljqv;

    :cond_6
    iget-object v0, p0, Ljqd;->eyt:Ljqv;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljqd;->eyw:Z

    iget v0, p0, Ljqd;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljqd;->aez:I

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x52

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljqd;->dOb:[Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v3, p0, Ljqd;->dOb:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Ljqd;->dOb:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljqd;->dOb:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljqd;->eyx:Z

    iget v0, p0, Ljqd;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljqd;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5644
    iget v0, p0, Ljqd;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 5645
    const/4 v0, 0x1

    iget-object v2, p0, Ljqd;->dOO:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 5647
    :cond_0
    iget v0, p0, Ljqd;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 5648
    const/4 v0, 0x2

    iget v2, p0, Ljqd;->eyq:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 5650
    :cond_1
    iget-object v0, p0, Ljqd;->eyr:Ljqv;

    if-eqz v0, :cond_2

    .line 5651
    const/4 v0, 0x3

    iget-object v2, p0, Ljqd;->eyr:Ljqv;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 5653
    :cond_2
    iget-object v0, p0, Ljqd;->eys:Ljqv;

    if-eqz v0, :cond_3

    .line 5654
    const/4 v0, 0x4

    iget-object v2, p0, Ljqd;->eys:Ljqv;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 5656
    :cond_3
    iget-object v0, p0, Ljqd;->eyu:[Ljqe;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljqd;->eyu:[Ljqe;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 5657
    :goto_0
    iget-object v2, p0, Ljqd;->eyu:[Ljqe;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 5658
    iget-object v2, p0, Ljqd;->eyu:[Ljqe;

    aget-object v2, v2, v0

    .line 5659
    if-eqz v2, :cond_4

    .line 5660
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 5657
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5664
    :cond_5
    iget v0, p0, Ljqd;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_6

    .line 5665
    const/4 v0, 0x6

    iget-object v2, p0, Ljqd;->dMI:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 5667
    :cond_6
    iget v0, p0, Ljqd;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_7

    .line 5668
    const/4 v0, 0x7

    iget-boolean v2, p0, Ljqd;->eyv:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 5670
    :cond_7
    iget-object v0, p0, Ljqd;->eyt:Ljqv;

    if-eqz v0, :cond_8

    .line 5671
    const/16 v0, 0x8

    iget-object v2, p0, Ljqd;->eyt:Ljqv;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 5673
    :cond_8
    iget v0, p0, Ljqd;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_9

    .line 5674
    const/16 v0, 0x9

    iget-boolean v2, p0, Ljqd;->eyw:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 5676
    :cond_9
    iget-object v0, p0, Ljqd;->dOb:[Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Ljqd;->dOb:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_b

    .line 5677
    :goto_1
    iget-object v0, p0, Ljqd;->dOb:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_b

    .line 5678
    iget-object v0, p0, Ljqd;->dOb:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 5679
    if-eqz v0, :cond_a

    .line 5680
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Ljsj;->p(ILjava/lang/String;)V

    .line 5677
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 5684
    :cond_b
    iget v0, p0, Ljqd;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_c

    .line 5685
    const/16 v0, 0xb

    iget-boolean v1, p0, Ljqd;->eyx:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 5687
    :cond_c
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 5688
    return-void
.end method

.method public final bsi()Z
    .locals 1

    .prologue
    .line 5539
    iget v0, p0, Ljqd;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bsj()Z
    .locals 1

    .prologue
    .line 5565
    iget-boolean v0, p0, Ljqd;->eyv:Z

    return v0
.end method

.method public final bsk()Z
    .locals 1

    .prologue
    .line 5584
    iget-boolean v0, p0, Ljqd;->eyw:Z

    return v0
.end method

.method public final bsl()Z
    .locals 1

    .prologue
    .line 5603
    iget-boolean v0, p0, Ljqd;->eyx:Z

    return v0
.end method

.method public final getAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5487
    iget-object v0, p0, Ljqd;->dOO:Ljava/lang/String;

    return-object v0
.end method

.method public final getFlags()I
    .locals 1

    .prologue
    .line 5531
    iget v0, p0, Ljqd;->eyq:I

    return v0
.end method

.method public final getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5509
    iget-object v0, p0, Ljqd;->dMI:Ljava/lang/String;

    return-object v0
.end method

.method public final iS(Z)Ljqd;
    .locals 1

    .prologue
    .line 5568
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljqd;->eyv:Z

    .line 5569
    iget v0, p0, Ljqd;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljqd;->aez:I

    .line 5570
    return-object p0
.end method

.method public final iT(Z)Ljqd;
    .locals 1

    .prologue
    .line 5587
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljqd;->eyw:Z

    .line 5588
    iget v0, p0, Ljqd;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljqd;->aez:I

    .line 5589
    return-object p0
.end method

.method public final iU(Z)Ljqd;
    .locals 1

    .prologue
    .line 5606
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljqd;->eyx:Z

    .line 5607
    iget v0, p0, Ljqd;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljqd;->aez:I

    .line 5608
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 5692
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 5693
    iget v2, p0, Ljqd;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 5694
    const/4 v2, 0x1

    iget-object v3, p0, Ljqd;->dOO:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5697
    :cond_0
    iget v2, p0, Ljqd;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_1

    .line 5698
    const/4 v2, 0x2

    iget v3, p0, Ljqd;->eyq:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 5701
    :cond_1
    iget-object v2, p0, Ljqd;->eyr:Ljqv;

    if-eqz v2, :cond_2

    .line 5702
    const/4 v2, 0x3

    iget-object v3, p0, Ljqd;->eyr:Ljqv;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5705
    :cond_2
    iget-object v2, p0, Ljqd;->eys:Ljqv;

    if-eqz v2, :cond_3

    .line 5706
    const/4 v2, 0x4

    iget-object v3, p0, Ljqd;->eys:Ljqv;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5709
    :cond_3
    iget-object v2, p0, Ljqd;->eyu:[Ljqe;

    if-eqz v2, :cond_6

    iget-object v2, p0, Ljqd;->eyu:[Ljqe;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v0

    move v0, v1

    .line 5710
    :goto_0
    iget-object v3, p0, Ljqd;->eyu:[Ljqe;

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 5711
    iget-object v3, p0, Ljqd;->eyu:[Ljqe;

    aget-object v3, v3, v0

    .line 5712
    if-eqz v3, :cond_4

    .line 5713
    const/4 v4, 0x5

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 5710
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v2

    .line 5718
    :cond_6
    iget v2, p0, Ljqd;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_7

    .line 5719
    const/4 v2, 0x6

    iget-object v3, p0, Ljqd;->dMI:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5722
    :cond_7
    iget v2, p0, Ljqd;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_8

    .line 5723
    const/4 v2, 0x7

    iget-boolean v3, p0, Ljqd;->eyv:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 5726
    :cond_8
    iget-object v2, p0, Ljqd;->eyt:Ljqv;

    if-eqz v2, :cond_9

    .line 5727
    const/16 v2, 0x8

    iget-object v3, p0, Ljqd;->eyt:Ljqv;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5730
    :cond_9
    iget v2, p0, Ljqd;->aez:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_a

    .line 5731
    const/16 v2, 0x9

    iget-boolean v3, p0, Ljqd;->eyw:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 5734
    :cond_a
    iget-object v2, p0, Ljqd;->dOb:[Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Ljqd;->dOb:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_d

    move v2, v1

    move v3, v1

    .line 5737
    :goto_1
    iget-object v4, p0, Ljqd;->dOb:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_c

    .line 5738
    iget-object v4, p0, Ljqd;->dOb:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 5739
    if-eqz v4, :cond_b

    .line 5740
    add-int/lit8 v3, v3, 0x1

    .line 5741
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 5737
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 5745
    :cond_c
    add-int/2addr v0, v2

    .line 5746
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 5748
    :cond_d
    iget v1, p0, Ljqd;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_e

    .line 5749
    const/16 v1, 0xb

    iget-boolean v2, p0, Ljqd;->eyx:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5752
    :cond_e
    return v0
.end method

.method public final oP()Z
    .locals 1

    .prologue
    .line 5520
    iget v0, p0, Ljqd;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final yd(Ljava/lang/String;)Ljqd;
    .locals 1

    .prologue
    .line 5490
    if-nez p1, :cond_0

    .line 5491
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5493
    :cond_0
    iput-object p1, p0, Ljqd;->dOO:Ljava/lang/String;

    .line 5494
    iget v0, p0, Ljqd;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqd;->aez:I

    .line 5495
    return-object p0
.end method

.method public final ye(Ljava/lang/String;)Ljqd;
    .locals 1

    .prologue
    .line 5512
    if-nez p1, :cond_0

    .line 5513
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5515
    :cond_0
    iput-object p1, p0, Ljqd;->dMI:Ljava/lang/String;

    .line 5516
    iget v0, p0, Ljqd;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqd;->aez:I

    .line 5517
    return-object p0
.end method
