.class public abstract Lbeg;
.super Landroid/content/ContentProvider;
.source "PG"

# interfaces
.implements Lbdy;


# instance fields
.field private final axG:Landroid/content/UriMatcher;

.field private final axH:Ljava/lang/Object;

.field private axI:Lbdx;

.field protected axJ:Lbbt;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 68
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lbeg;->axG:Landroid/content/UriMatcher;

    .line 70
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbeg;->axH:Ljava/lang/Object;

    return-void
.end method

.method private xv()[Lbej;
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0}, Lbeg;->xD()Lbdx;

    move-result-object v0

    invoke-interface {v0}, Lbdx;->xv()[Lbej;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a(Ljava/util/concurrent/Callable;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 282
    invoke-virtual {p0}, Lbeg;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 283
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " can\'t be called before onCreate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 285
    :cond_0
    invoke-virtual {p0}, Lbeg;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 286
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " can\'t be called on main thread"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 289
    :cond_1
    iget-object v1, p0, Lbeg;->axJ:Lbbt;

    monitor-enter v1

    .line 290
    :try_start_0
    iget-object v0, p0, Lbeg;->axJ:Lbbt;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v2, v3}, Lbbt;->G(J)Lbgm;

    move-result-object v0

    .line 292
    invoke-virtual {v0}, Lbgm;->yk()Z

    move-result v2

    if-nez v2, :cond_2

    .line 293
    const-string v2, ".AppDataSearchProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not connect to AppDataSearchClient for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lbgm;->getErrorCode()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    :goto_0
    return-object p3

    .line 299
    :cond_2
    :try_start_1
    invoke-interface {p1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object p3

    .line 303
    :try_start_2
    iget-object v0, p0, Lbeg;->axJ:Lbbt;

    iget-object v0, v0, Lbbt;->auz:Lbkm;

    invoke-virtual {v0}, Lbkm;->disconnect()V

    monitor-exit v1

    goto :goto_0

    .line 305
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 300
    :catch_0
    move-exception v0

    .line 301
    :try_start_3
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 303
    :catchall_1
    move-exception v0

    :try_start_4
    iget-object v2, p0, Lbeg;->axJ:Lbbt;

    iget-object v2, v2, Lbbt;->auz:Lbkm;

    invoke-virtual {v2}, Lbkm;->disconnect()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method protected abstract b(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;
.end method

.method protected abstract b(Lbdy;)Lbdx;
.end method

.method public final c(Lbej;)Z
    .locals 3

    .prologue
    .line 210
    invoke-direct {p0}, Lbeg;->xv()[Lbej;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 211
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The table "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lbej;->xF()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not have a registered CorpusTableMapping."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 215
    :cond_0
    new-instance v0, Lbeh;

    invoke-direct {v0, p0, p1}, Lbeh;-><init>(Lbeg;Lbej;)V

    .line 222
    const-string v1, "onTableChanged"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lbeg;->a(Ljava/util/concurrent/Callable;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lbeg;->axG:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 180
    invoke-virtual {p0}, Lbeg;->xC()Ljava/lang/String;

    move-result-object v0

    .line 183
    :goto_0
    return-object v0

    .line 182
    :cond_0
    invoke-virtual {p0}, Lbeg;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbbt;->R(Landroid/content/Context;)V

    .line 183
    const-string v0, "vnd.android.cursor.dir/vnd.goodle.appdatasearch"

    goto :goto_0
.end method

.method public final h(Lbej;)I
    .locals 3

    .prologue
    .line 234
    invoke-direct {p0}, Lbeg;->xv()[Lbej;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 235
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The table "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lbej;->xF()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not have a registered CorpusTableMapping."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 238
    :cond_0
    new-instance v0, Lbei;

    invoke-direct {v0, p0, p1}, Lbei;-><init>(Lbeg;Lbej;)V

    .line 274
    const-string v1, "diagnoseTable"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lbeg;->a(Ljava/util/concurrent/Callable;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method protected final i(Lbej;)Z
    .locals 4

    .prologue
    .line 317
    iget-object v0, p0, Lbeg;->axJ:Lbbt;

    invoke-virtual {p1}, Lbej;->xF()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbbt;->eI(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/CorpusStatus;

    move-result-object v0

    .line 318
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/CorpusStatus;->wR()Z

    move-result v1

    if-nez v1, :cond_2

    .line 319
    :cond_0
    if-nez v0, :cond_1

    const-string v0, "Couldn\'t fetch status for"

    .line 320
    :goto_0
    const-string v1, ".AppDataSearchProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " corpus \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lbej;->xF()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    const/4 v0, 0x0

    .line 332
    :goto_1
    return v0

    .line 319
    :cond_1
    const-string v0, "Couldn\'t find"

    goto :goto_0

    .line 324
    :cond_2
    invoke-virtual {p0}, Lbeg;->xD()Lbdx;

    move-result-object v1

    .line 325
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/CorpusStatus;->wT()J

    move-result-wide v2

    invoke-interface {v1, p1, v2, v3}, Lbdx;->a(Lbej;J)V

    .line 327
    invoke-interface {v1, p1}, Lbdx;->b(Lbej;)J

    move-result-wide v2

    .line 328
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/CorpusStatus;->wS()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-lez v0, :cond_3

    .line 330
    iget-object v0, p0, Lbeg;->axJ:Lbbt;

    invoke-virtual {p1}, Lbej;->xF()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3}, Lbbt;->a(Ljava/lang/String;J)Z

    move-result v0

    goto :goto_1

    .line 332
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public onCreate()Z
    .locals 7

    .prologue
    .line 113
    new-instance v0, Lbbt;

    invoke-virtual {p0}, Lbeg;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lbbt;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbeg;->axJ:Lbbt;

    .line 116
    invoke-virtual {p0}, Lbeg;->xB()Z

    .line 118
    invoke-virtual {p0}, Lbeg;->xA()Ljava/lang/String;

    move-result-object v1

    .line 119
    invoke-virtual {p0}, Lbeg;->xE()[Ljava/lang/String;

    move-result-object v2

    .line 120
    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 121
    iget-object v3, p0, Lbeg;->axG:Landroid/content/UriMatcher;

    aget-object v4, v2, v0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "appdatasearch/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4, v0}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 124
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7

    .prologue
    .line 143
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lbeg;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 149
    iget-object v0, p0, Lbeg;->axG:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 150
    const/4 v0, -0x1

    if-ne v1, v0, :cond_0

    .line 151
    invoke-virtual {p0, p1, p4}, Lbeg;->b(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 173
    :goto_0
    return-object v0

    .line 153
    :cond_0
    invoke-virtual {p0}, Lbeg;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbbt;->R(Landroid/content/Context;)V

    .line 154
    invoke-static {p4}, Lbcq;->a([Ljava/lang/String;)Lbcq;

    move-result-object v4

    .line 158
    invoke-virtual {p0}, Lbeg;->xD()Lbdx;

    move-result-object v0

    .line 159
    invoke-direct {p0}, Lbeg;->xv()[Lbej;

    move-result-object v2

    aget-object v1, v2, v1

    .line 161
    invoke-virtual {v4}, Lbcq;->xp()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 165
    invoke-interface {v0, v1}, Lbdx;->a(Lbej;)V

    .line 168
    :cond_1
    invoke-virtual {v4}, Lbcq;->xm()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 169
    invoke-virtual {v4}, Lbcq;->xo()J

    move-result-wide v2

    invoke-virtual {v4}, Lbcq;->xq()J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Lbdx;->a(Lbej;JJ)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 170
    :cond_2
    invoke-virtual {v4}, Lbcq;->xn()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 171
    invoke-virtual {v4}, Lbcq;->xo()J

    invoke-virtual {v4}, Lbcq;->xq()J

    invoke-interface {v0}, Lbdx;->xw()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 173
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract xA()Ljava/lang/String;
.end method

.method protected abstract xB()Z
.end method

.method protected abstract xC()Ljava/lang/String;
.end method

.method protected final xD()Lbdx;
    .locals 2

    .prologue
    .line 99
    iget-object v1, p0, Lbeg;->axH:Ljava/lang/Object;

    monitor-enter v1

    .line 100
    :try_start_0
    iget-object v0, p0, Lbeg;->axI:Lbdx;

    if-nez v0, :cond_0

    .line 101
    invoke-virtual {p0, p0}, Lbeg;->b(Lbdy;)Lbdx;

    move-result-object v0

    iput-object v0, p0, Lbeg;->axI:Lbdx;

    .line 103
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    iget-object v0, p0, Lbeg;->axI:Lbdx;

    return-object v0

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected xE()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 128
    invoke-direct {p0}, Lbeg;->xv()[Lbej;

    move-result-object v1

    .line 129
    array-length v0, v1

    new-array v2, v0, [Ljava/lang/String;

    .line 130
    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 131
    aget-object v3, v1, v0

    invoke-virtual {v3}, Lbej;->xF()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 130
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 133
    :cond_0
    return-object v2
.end method
