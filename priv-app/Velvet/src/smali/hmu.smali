.class public final Lhmu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldwu;


# instance fields
.field private aL:Landroid/view/ViewGroup;

.field private bNR:Ldvt;

.field private bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;

.field protected final dlh:Lhnc;

.field private final dli:Lesk;

.field private mImageLoader:Lesm;

.field protected final mIntentStarter:Leoj;

.field private mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lesm;Ldvt;Leoj;Lhnc;Lcom/google/android/search/shared/actions/modular/ModularAction;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lhmu;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 66
    iput-object p2, p0, Lhmu;->aL:Landroid/view/ViewGroup;

    .line 67
    iput-object p3, p0, Lhmu;->mImageLoader:Lesm;

    .line 68
    iput-object p4, p0, Lhmu;->bNR:Ldvt;

    .line 69
    iput-object p5, p0, Lhmu;->mIntentStarter:Leoj;

    .line 70
    iput-object p6, p0, Lhmu;->dlh:Lhnc;

    .line 71
    iput-object p7, p0, Lhmu;->bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;

    .line 73
    new-instance v0, Lhmv;

    invoke-direct {v0, p0}, Lhmv;-><init>(Lhmu;)V

    iput-object v0, p0, Lhmu;->dli:Lesk;

    .line 81
    return-void
.end method

.method private b(Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;)Landroid/view/View;
    .locals 4

    .prologue
    .line 314
    iget-object v0, p0, Lhmu;->mLayoutInflater:Landroid/view/LayoutInflater;

    const/high16 v1, 0x7f040000

    iget-object v2, p0, Lhmu;->aL:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;

    .line 316
    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->a(Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;)V

    .line 317
    return-object v0
.end method

.method private static lm(I)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 321
    if-ne p0, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 51
    iget-object v0, p0, Lhmu;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040053

    iget-object v2, p0, Lhmu;->aL:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->b(Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;)V

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 51
    iget-object v0, p0, Lhmu;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04005a

    iget-object v2, p0, Lhmu;->aL:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/modular/DeviceSettingsArgumentView;

    new-instance v1, Lhmw;

    invoke-direct {v1, p0, p1}, Lhmw;-><init>(Lhmu;Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/voicesearch/fragments/modular/DeviceSettingsArgumentView;->a(Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;Lhng;)V

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 51
    iget-object v0, p0, Lhmu;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04005c

    iget-object v2, p0, Lhmu;->aL:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/modular/DocumentArgumentView;

    iget-object v1, p0, Lhmu;->mImageLoader:Lesm;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/voicesearch/fragments/modular/DocumentArgumentView;->a(Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;Lesm;)V

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 51
    iget-object v0, p0, Lhmu;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040094

    iget-object v2, p0, Lhmu;->aL:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;

    iget-object v1, p0, Lhmu;->bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    move-result-object v1

    new-instance v2, Lhmx;

    invoke-direct {v2, p0, v0}, Lhmx;-><init>(Lhmu;Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;)V

    invoke-virtual {v0, v1, p0, v2}, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->a(Ldvv;Lhmu;Lefk;)V

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->b(Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;)V

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 51
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->aku()Ljpx;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljpx;->getSize()I

    move-result v0

    invoke-static {v0}, Lhmu;->lm(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhmu;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0400b9

    iget-object v2, p0, Lhmu;->aL:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->b(Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;)V

    new-instance v1, Lhmy;

    invoke-direct {v1, p0}, Lhmy;-><init>(Lhmu;)V

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->a(Lhnt;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhmu;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0400b8

    iget-object v2, p0, Lhmu;->aL:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/LocalResultsView;

    new-instance v2, Lhmz;

    invoke-direct {v2, p0, p1, v0}, Lhmz;-><init>(Lhmu;Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;Lcom/google/android/voicesearch/fragments/LocalResultsView;)V

    iget-object v1, p0, Lhmu;->dlh:Lhnc;

    invoke-virtual {v0, v3}, Lcom/google/android/voicesearch/fragments/LocalResultsView;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->ahw()Ljpe;

    move-result-object v1

    iget-object v3, p0, Lhmu;->mImageLoader:Lesm;

    iget-object v4, p0, Lhmu;->mLayoutInflater:Landroid/view/LayoutInflater;

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/fragments/LocalResultsView;->a(Ljpe;Lhkz;Lesm;Landroid/view/LayoutInflater;Z)V

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 51
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v1

    if-eqz v1, :cond_2

    move v4, v2

    :goto_0
    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Person;->oK()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->Pd()Z

    move-result v5

    if-eqz v5, :cond_0

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->ajO()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->akz()Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->akC()Ljpv;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljpv;->getSize()I

    move-result v0

    invoke-static {v0}, Lhmu;->lm(I)Z

    move-result v0

    if-eqz v0, :cond_4

    :goto_2
    if-eqz v2, :cond_5

    const v0, 0x7f0400e7

    :goto_3
    iget-object v1, p0, Lhmu;->mLayoutInflater:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lhmu;->aL:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonSelectItem;

    const v1, 0x7f1102a9

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lhna;

    invoke-direct {v2, p0, v0}, Lhna;-><init>(Lhmu;Lcom/google/android/search/shared/contact/PersonSelectItem;)V

    invoke-virtual {v0, v2}, Lcom/google/android/search/shared/contact/PersonSelectItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz v1, :cond_1

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->ajk()I

    move-result v2

    invoke-static {v1, v2}, Lehd;->s(Landroid/view/View;I)V

    :cond_1
    iget-object v1, p0, Lhmu;->dlh:Lhnc;

    iget-object v1, p0, Lhmu;->dli:Lesk;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/search/shared/contact/PersonSelectItem;->a(Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;Lesk;)V

    :goto_4
    return-object v0

    :cond_2
    move v4, v3

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    move v2, v3

    goto :goto_2

    :cond_5
    const v0, 0x7f0400e6

    goto :goto_3

    :cond_6
    if-eqz v4, :cond_7

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->oK()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lhmu;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040171

    iget-object v2, p0, Lhmu;->aL:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView;->c(Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;)V

    goto :goto_4

    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 51
    iget-object v0, p0, Lhmu;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04014d

    iget-object v2, p0, Lhmu;->aL:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;->b(Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;)V

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 51
    iget-object v0, p0, Lhmu;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04017c

    iget-object v2, p0, Lhmu;->aL:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;

    new-instance v1, Lhnb;

    invoke-direct {v1, p0, p1}, Lhnb;-><init>(Lhmu;Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;)V

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->a(Lhoz;)V

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->b(Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;)V

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 51
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->akP()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lhmu;->b(Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhmu;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04018b

    iget-object v2, p0, Lhmu;->aL:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/modular/TimeDurationArgumentView;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/fragments/modular/TimeDurationArgumentView;->b(Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;)V

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lhmu;->b(Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 51
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->isCompleted()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;

    :goto_1
    iget-object v1, p0, Lhmu;->mLayoutInflater:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lhmu;->aL:Landroid/view/ViewGroup;

    iget-object v3, p0, Lhmu;->mImageLoader:Lesm;

    iget-object v4, p0, Lhmu;->bNR:Ldvt;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->ake()Ljpw;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->a(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lesm;Ldvt;Ljpw;)Lcom/google/android/search/shared/actions/modular/EntitySelectItem;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->a(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;)V

    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final i(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Landroid/view/View;
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p1, p0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->a(Ldwu;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method
