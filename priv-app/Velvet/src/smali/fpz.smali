.class public final Lfpz;
.super Lepp;
.source "PG"


# instance fields
.field private final bMm:I

.field private final cuA:Lfkd;

.field private final czj:Landroid/view/accessibility/AccessibilityEvent;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mCardContainer:Lfmt;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(ILandroid/content/Context;Lfmt;Lfkd;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p5    # Landroid/view/accessibility/AccessibilityEvent;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Pending card settings action: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lepp;-><init>(Ljava/lang/String;)V

    .line 30
    iput p1, p0, Lfpz;->bMm:I

    .line 31
    iput-object p2, p0, Lfpz;->mContext:Landroid/content/Context;

    .line 32
    iput-object p3, p0, Lfpz;->mCardContainer:Lfmt;

    .line 33
    iput-object p4, p0, Lfpz;->cuA:Lfkd;

    .line 34
    iput-object p5, p0, Lfpz;->czj:Landroid/view/accessibility/AccessibilityEvent;

    .line 35
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 39
    iget v0, p0, Lfpz;->bMm:I

    sparse-switch v0, :sswitch_data_0

    .line 50
    :goto_0
    iget-object v0, p0, Lfpz;->czj:Landroid/view/accessibility/AccessibilityEvent;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lfpz;->mContext:Landroid/content/Context;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iget-object v1, p0, Lfpz;->czj:Landroid/view/accessibility/AccessibilityEvent;

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 54
    :cond_0
    return-void

    .line 41
    :sswitch_0
    iget-object v0, p0, Lfpz;->mCardContainer:Lfmt;

    iget-object v1, p0, Lfpz;->cuA:Lfkd;

    invoke-interface {v1}, Lfkd;->getEntry()Lizj;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lfmt;->a(Lizj;Z)V

    goto :goto_0

    .line 44
    :sswitch_1
    iget-object v0, p0, Lfpz;->mCardContainer:Lfmt;

    const/16 v1, 0x24

    invoke-interface {v0, v1}, Lfmt;->dt(I)V

    goto :goto_0

    .line 47
    :sswitch_2
    iget-object v0, p0, Lfpz;->mCardContainer:Lfmt;

    iget-object v1, p0, Lfpz;->cuA:Lfkd;

    invoke-interface {v1}, Lfkd;->getEntry()Lizj;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lfmt;->c(Lizj;Z)V

    goto :goto_0

    .line 39
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x4 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method
