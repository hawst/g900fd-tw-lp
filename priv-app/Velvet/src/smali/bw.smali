.class public final Lbw;
.super Lcd;
.source "PG"


# instance fields
.field private final cF:Landroid/os/Bundle;

.field private final do:Ljava/lang/String;

.field private final dp:Ljava/lang/CharSequence;

.field private final dq:[Ljava/lang/CharSequence;

.field private final dr:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 253
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_0

    .line 254
    new-instance v0, Lby;

    invoke-direct {v0}, Lby;-><init>()V

    .line 263
    :goto_0
    new-instance v0, Lbx;

    invoke-direct {v0}, Lbx;-><init>()V

    return-void

    .line 255
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 256
    new-instance v0, Lca;

    invoke-direct {v0}, Lca;-><init>()V

    goto :goto_0

    .line 258
    :cond_1
    new-instance v0, Lbz;

    invoke-direct {v0}, Lbz;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final getAllowFreeFormInput()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lbw;->dr:Z

    return v0
.end method

.method public final getChoices()[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lbw;->dq:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lbw;->cF:Landroid/os/Bundle;

    return-object v0
.end method

.method public final getLabel()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lbw;->dp:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getResultKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lbw;->do:Ljava/lang/String;

    return-object v0
.end method
