.class public final Lgad;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final dK:Ljava/lang/Object;

.field private final mBundle:Landroid/os/Bundle;

.field private final mCardContainer:Lfmt;


# direct methods
.method public constructor <init>(Lfmt;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lgad;->dK:Ljava/lang/Object;

    .line 34
    iput-object p1, p0, Lgad;->mCardContainer:Lfmt;

    .line 35
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lgad;->mBundle:Landroid/os/Bundle;

    .line 36
    return-void
.end method


# virtual methods
.method public final E(Ljava/lang/String;I)Lgad;
    .locals 4

    .prologue
    .line 90
    iget-object v1, p0, Lgad;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 91
    :try_start_0
    iget-object v0, p0, Lgad;->mBundle:Landroid/os/Bundle;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "i"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 92
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    return-object p0

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final apply()V
    .locals 4

    .prologue
    .line 118
    iget-object v1, p0, Lgad;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 119
    :try_start_0
    iget-object v0, p0, Lgad;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->aAD()Lfml;

    move-result-object v0

    iget-object v2, p0, Lgad;->mBundle:Landroid/os/Bundle;

    invoke-virtual {v0}, Lfml;->aBa()Lfor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-interface {v0, v2}, Lfor;->P(Landroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 120
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v1

    return-void

    .line 119
    :catch_0
    move-exception v0

    const-string v2, "NowRemoteClient"

    const-string v3, "Error making save preferences request"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final mh(Ljava/lang/String;)Lgad;
    .locals 4

    .prologue
    .line 111
    iget-object v1, p0, Lgad;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 112
    :try_start_0
    iget-object v0, p0, Lgad;->mBundle:Landroid/os/Bundle;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "r"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    .line 113
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    return-object p0

    .line 113
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
