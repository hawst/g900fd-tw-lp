.class public final Ldcs;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bsm:Laqb;

.field public mEventBus:Ldda;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Laqb;

    invoke-direct {v0}, Laqb;-><init>()V

    iput-object v0, p0, Ldcs;->bsm:Laqb;

    .line 20
    return-void
.end method


# virtual methods
.method public final a(Lddc;)V
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Lenu;->auR()V

    .line 29
    iget-object v0, p0, Ldcs;->bsm:Laqb;

    invoke-virtual {v0, p1}, Laqb;->ag(Ljava/lang/Object;)V

    .line 30
    iget-object v0, p0, Ldcs;->mEventBus:Ldda;

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Ldcs;->mEventBus:Ldda;

    invoke-virtual {v0, p1}, Ldda;->c(Lddc;)V

    .line 33
    :cond_0
    return-void
.end method

.method public final b(Ldda;)V
    .locals 3
    .param p1    # Ldda;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 50
    invoke-static {}, Lenu;->auR()V

    .line 51
    iget-object v0, p0, Ldcs;->mEventBus:Ldda;

    if-eq p1, v0, :cond_3

    .line 53
    if-eqz p1, :cond_0

    iget-object v0, p0, Ldcs;->mEventBus:Ldda;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 54
    iget-object v0, p0, Ldcs;->mEventBus:Ldda;

    if-eqz v0, :cond_2

    .line 55
    iget-object v0, p0, Ldcs;->bsm:Laqb;

    invoke-virtual {v0}, Laqb;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lddc;

    .line 56
    iget-object v2, p0, Ldcs;->mEventBus:Ldda;

    invoke-virtual {v2, v0}, Ldda;->d(Lddc;)V

    goto :goto_1

    .line 53
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 59
    :cond_2
    iput-object p1, p0, Ldcs;->mEventBus:Ldda;

    .line 60
    iget-object v0, p0, Ldcs;->mEventBus:Ldda;

    if-eqz v0, :cond_3

    .line 61
    iget-object v0, p0, Ldcs;->bsm:Laqb;

    invoke-virtual {v0}, Laqb;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lddc;

    .line 62
    iget-object v2, p0, Ldcs;->mEventBus:Ldda;

    invoke-virtual {v2, v0}, Ldda;->c(Lddc;)V

    goto :goto_2

    .line 66
    :cond_3
    return-void
.end method

.method public final b(Lddc;)V
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lenu;->auR()V

    .line 37
    iget-object v0, p0, Ldcs;->bsm:Laqb;

    invoke-virtual {v0, p1}, Laqb;->ah(Ljava/lang/Object;)V

    .line 38
    iget-object v0, p0, Ldcs;->mEventBus:Ldda;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Ldcs;->mEventBus:Ldda;

    invoke-virtual {v0, p1}, Ldda;->d(Lddc;)V

    .line 41
    :cond_0
    return-void
.end method
