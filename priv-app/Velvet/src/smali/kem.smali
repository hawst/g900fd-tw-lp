.class public final Lkem;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eUK:Ljava/lang/String;

.field private eUL:Z

.field private eUM:I

.field public eUN:[I

.field private eUO:Ljava/lang/String;

.field private eUP:Z

.field public eUQ:Lkel;

.field public eUR:Lken;

.field public eUS:Lkeo;

.field private eUT:Lkep;

.field private eUU:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 162
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 163
    iput v1, p0, Lkem;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lkem;->eUK:Ljava/lang/String;

    iput-boolean v1, p0, Lkem;->eUL:Z

    iput v1, p0, Lkem;->eUM:I

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Lkem;->eUN:[I

    const-string v0, ""

    iput-object v0, p0, Lkem;->eUO:Ljava/lang/String;

    iput-boolean v1, p0, Lkem;->eUP:Z

    iput-object v2, p0, Lkem;->eUQ:Lkel;

    iput-object v2, p0, Lkem;->eUR:Lken;

    iput-object v2, p0, Lkem;->eUS:Lkeo;

    iput-object v2, p0, Lkem;->eUT:Lkep;

    iput-boolean v1, p0, Lkem;->eUU:Z

    iput-object v2, p0, Lkem;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lkem;->eCz:I

    .line 164
    return-void
.end method


# virtual methods
.method public final AF(Ljava/lang/String;)Lkem;
    .locals 1

    .prologue
    .line 33
    if-nez p1, :cond_0

    .line 34
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 36
    :cond_0
    iput-object p1, p0, Lkem;->eUK:Ljava/lang/String;

    .line 37
    iget v0, p0, Lkem;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkem;->aez:I

    .line 38
    return-object p0
.end method

.method public final AG(Ljava/lang/String;)Lkem;
    .locals 1

    .prologue
    .line 96
    if-nez p1, :cond_0

    .line 97
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 99
    :cond_0
    iput-object p1, p0, Lkem;->eUO:Ljava/lang/String;

    .line 100
    iget v0, p0, Lkem;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkem;->aez:I

    .line 101
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lkem;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkem;->eUK:Ljava/lang/String;

    iget v0, p0, Lkem;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkem;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lkem;->eUL:Z

    iget v0, p0, Lkem;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkem;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lkem;->eUM:I

    iget v0, p0, Lkem;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkem;->aez:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x20

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lkem;->eUN:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Lkem;->eUN:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lkem;->eUN:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lkem;->eUN:[I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Ljsi;->btQ()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Lkem;->eUN:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Lkem;->eUN:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lkem;->eUN:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Lkem;->eUN:[I

    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lkem;->eUQ:Lkel;

    if-nez v0, :cond_8

    new-instance v0, Lkel;

    invoke-direct {v0}, Lkel;-><init>()V

    iput-object v0, p0, Lkem;->eUQ:Lkel;

    :cond_8
    iget-object v0, p0, Lkem;->eUQ:Lkel;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lkem;->eUR:Lken;

    if-nez v0, :cond_9

    new-instance v0, Lken;

    invoke-direct {v0}, Lken;-><init>()V

    iput-object v0, p0, Lkem;->eUR:Lken;

    :cond_9
    iget-object v0, p0, Lkem;->eUR:Lken;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lkem;->eUS:Lkeo;

    if-nez v0, :cond_a

    new-instance v0, Lkeo;

    invoke-direct {v0}, Lkeo;-><init>()V

    iput-object v0, p0, Lkem;->eUS:Lkeo;

    :cond_a
    iget-object v0, p0, Lkem;->eUS:Lkeo;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lkem;->eUT:Lkep;

    if-nez v0, :cond_b

    new-instance v0, Lkep;

    invoke-direct {v0}, Lkep;-><init>()V

    iput-object v0, p0, Lkem;->eUT:Lkep;

    :cond_b
    iget-object v0, p0, Lkem;->eUT:Lkep;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lkem;->eUP:Z

    iget v0, p0, Lkem;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkem;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkem;->eUO:Ljava/lang/String;

    iget v0, p0, Lkem;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkem;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lkem;->eUU:Z

    iget v0, p0, Lkem;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lkem;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x22 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
        0x3a -> :sswitch_8
        0x42 -> :sswitch_9
        0x48 -> :sswitch_a
        0x62 -> :sswitch_b
        0x68 -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 187
    iget v0, p0, Lkem;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 188
    const/4 v0, 0x1

    iget-object v1, p0, Lkem;->eUK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 190
    :cond_0
    iget v0, p0, Lkem;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 191
    const/4 v0, 0x2

    iget-boolean v1, p0, Lkem;->eUL:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 193
    :cond_1
    iget v0, p0, Lkem;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 194
    const/4 v0, 0x3

    iget v1, p0, Lkem;->eUM:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 196
    :cond_2
    iget-object v0, p0, Lkem;->eUN:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lkem;->eUN:[I

    array-length v0, v0

    if-lez v0, :cond_3

    .line 197
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lkem;->eUN:[I

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 198
    const/4 v1, 0x4

    iget-object v2, p0, Lkem;->eUN:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Ljsj;->bq(II)V

    .line 197
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 201
    :cond_3
    iget-object v0, p0, Lkem;->eUQ:Lkel;

    if-eqz v0, :cond_4

    .line 202
    const/4 v0, 0x5

    iget-object v1, p0, Lkem;->eUQ:Lkel;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 204
    :cond_4
    iget-object v0, p0, Lkem;->eUR:Lken;

    if-eqz v0, :cond_5

    .line 205
    const/4 v0, 0x6

    iget-object v1, p0, Lkem;->eUR:Lken;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 207
    :cond_5
    iget-object v0, p0, Lkem;->eUS:Lkeo;

    if-eqz v0, :cond_6

    .line 208
    const/4 v0, 0x7

    iget-object v1, p0, Lkem;->eUS:Lkeo;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 210
    :cond_6
    iget-object v0, p0, Lkem;->eUT:Lkep;

    if-eqz v0, :cond_7

    .line 211
    const/16 v0, 0x8

    iget-object v1, p0, Lkem;->eUT:Lkep;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 213
    :cond_7
    iget v0, p0, Lkem;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_8

    .line 214
    const/16 v0, 0x9

    iget-boolean v1, p0, Lkem;->eUP:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 216
    :cond_8
    iget v0, p0, Lkem;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_9

    .line 217
    const/16 v0, 0xc

    iget-object v1, p0, Lkem;->eUO:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 219
    :cond_9
    iget v0, p0, Lkem;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_a

    .line 220
    const/16 v0, 0xd

    iget-boolean v1, p0, Lkem;->eUU:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 222
    :cond_a
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 223
    return-void
.end method

.method public final aoM()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lkem;->eUK:Ljava/lang/String;

    return-object v0
.end method

.method public final bzA()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lkem;->eUM:I

    return v0
.end method

.method public final bzB()Z
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lkem;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bzC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lkem;->eUO:Ljava/lang/String;

    return-object v0
.end method

.method public final bzD()Z
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lkem;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bzE()Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lkem;->eUP:Z

    return v0
.end method

.method public final bzF()Z
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lkem;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bzG()Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lkem;->eUU:Z

    return v0
.end method

.method public final bzz()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lkem;->eUL:Z

    return v0
.end method

.method public final jO(Z)Lkem;
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkem;->eUL:Z

    .line 56
    iget v0, p0, Lkem;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkem;->aez:I

    .line 57
    return-object p0
.end method

.method public final jP(Z)Lkem;
    .locals 1

    .prologue
    .line 118
    iput-boolean p1, p0, Lkem;->eUP:Z

    .line 119
    iget v0, p0, Lkem;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkem;->aez:I

    .line 120
    return-object p0
.end method

.method public final jQ(Z)Lkem;
    .locals 1

    .prologue
    .line 149
    iput-boolean p1, p0, Lkem;->eUU:Z

    .line 150
    iget v0, p0, Lkem;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lkem;->aez:I

    .line 151
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 227
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 228
    iget v2, p0, Lkem;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 229
    const/4 v2, 0x1

    iget-object v3, p0, Lkem;->eUK:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 232
    :cond_0
    iget v2, p0, Lkem;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 233
    const/4 v2, 0x2

    iget-boolean v3, p0, Lkem;->eUL:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 236
    :cond_1
    iget v2, p0, Lkem;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    .line 237
    const/4 v2, 0x3

    iget v3, p0, Lkem;->eUM:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 240
    :cond_2
    iget-object v2, p0, Lkem;->eUN:[I

    if-eqz v2, :cond_4

    iget-object v2, p0, Lkem;->eUN:[I

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    .line 242
    :goto_0
    iget-object v3, p0, Lkem;->eUN:[I

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 243
    iget-object v3, p0, Lkem;->eUN:[I

    aget v3, v3, v1

    .line 244
    invoke-static {v3}, Ljsj;->sa(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 242
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 247
    :cond_3
    add-int/2addr v0, v2

    .line 248
    iget-object v1, p0, Lkem;->eUN:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 250
    :cond_4
    iget-object v1, p0, Lkem;->eUQ:Lkel;

    if-eqz v1, :cond_5

    .line 251
    const/4 v1, 0x5

    iget-object v2, p0, Lkem;->eUQ:Lkel;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 254
    :cond_5
    iget-object v1, p0, Lkem;->eUR:Lken;

    if-eqz v1, :cond_6

    .line 255
    const/4 v1, 0x6

    iget-object v2, p0, Lkem;->eUR:Lken;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 258
    :cond_6
    iget-object v1, p0, Lkem;->eUS:Lkeo;

    if-eqz v1, :cond_7

    .line 259
    const/4 v1, 0x7

    iget-object v2, p0, Lkem;->eUS:Lkeo;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 262
    :cond_7
    iget-object v1, p0, Lkem;->eUT:Lkep;

    if-eqz v1, :cond_8

    .line 263
    const/16 v1, 0x8

    iget-object v2, p0, Lkem;->eUT:Lkep;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 266
    :cond_8
    iget v1, p0, Lkem;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_9

    .line 267
    const/16 v1, 0x9

    iget-boolean v2, p0, Lkem;->eUP:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 270
    :cond_9
    iget v1, p0, Lkem;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_a

    .line 271
    const/16 v1, 0xc

    iget-object v2, p0, Lkem;->eUO:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 274
    :cond_a
    iget v1, p0, Lkem;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_b

    .line 275
    const/16 v1, 0xd

    iget-boolean v2, p0, Lkem;->eUU:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 278
    :cond_b
    return v0
.end method

.method public final tY(I)Lkem;
    .locals 1

    .prologue
    .line 74
    iput p1, p0, Lkem;->eUM:I

    .line 75
    iget v0, p0, Lkem;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkem;->aez:I

    .line 76
    return-object p0
.end method
