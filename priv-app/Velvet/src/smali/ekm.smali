.class public final Lekm;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "PG"


# instance fields
.field public cdA:Lekn;

.field public cdB:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field public cdC:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field public cdD:I

.field public cdE:I

.field public cdF:I

.field public cdG:I

.field public cdH:I

.field public cdI:I

.field public cdJ:Landroid/graphics/Rect;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cdu:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field public cdv:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field public cdw:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field public cdx:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field public cdy:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field public cdz:Lekn;

.field public column:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field


# direct methods
.method public constructor <init>(III)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1232
    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-direct {p0, v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 1169
    iput-boolean v2, p0, Lekm;->cdu:Z

    .line 1171
    iput-boolean v2, p0, Lekm;->cdv:Z

    .line 1173
    iput-boolean v3, p0, Lekm;->cdw:Z

    .line 1175
    iput-boolean v2, p0, Lekm;->cdx:Z

    .line 1233
    iput v3, p0, Lekm;->column:I

    .line 1234
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1237
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1169
    iput-boolean v2, p0, Lekm;->cdu:Z

    .line 1171
    iput-boolean v2, p0, Lekm;->cdv:Z

    .line 1173
    iput-boolean v3, p0, Lekm;->cdw:Z

    .line 1175
    iput-boolean v2, p0, Lekm;->cdx:Z

    .line 1239
    sget-object v0, Lbwe;->aMe:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1241
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lekm;->column:I

    .line 1242
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lekm;->cdu:Z

    .line 1244
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lekm;->cdv:Z

    .line 1245
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lekm;->cdw:Z

    .line 1247
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lekm;->cdx:Z

    .line 1249
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lekm;->cdB:I

    .line 1251
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lekm;->cdC:I

    .line 1255
    const/4 v1, 0x5

    sget-object v2, Lekn;->cdK:Lekn;

    invoke-static {v0, v1, v2}, Lekm;->a(Landroid/content/res/TypedArray;ILekn;)Lekn;

    move-result-object v1

    iput-object v1, p0, Lekm;->cdz:Lekn;

    .line 1260
    const/4 v1, 0x6

    sget-object v2, Lekn;->cdO:Lekn;

    invoke-static {v0, v1, v2}, Lekm;->a(Landroid/content/res/TypedArray;ILekn;)Lekn;

    move-result-object v1

    iput-object v1, p0, Lekm;->cdA:Lekn;

    .line 1263
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1264
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1210
    const/4 v0, -0x1

    iget v1, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {p0, v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 1169
    iput-boolean v2, p0, Lekm;->cdu:Z

    .line 1171
    iput-boolean v2, p0, Lekm;->cdv:Z

    .line 1173
    const/4 v0, 0x0

    iput-boolean v0, p0, Lekm;->cdw:Z

    .line 1175
    iput-boolean v2, p0, Lekm;->cdx:Z

    .line 1211
    instance-of v0, p1, Lekm;

    if-eqz v0, :cond_0

    .line 1212
    check-cast p1, Lekm;

    .line 1213
    iget v0, p1, Lekm;->column:I

    iput v0, p0, Lekm;->column:I

    .line 1214
    iget-boolean v0, p1, Lekm;->cdu:Z

    iput-boolean v0, p0, Lekm;->cdu:Z

    .line 1215
    iget-boolean v0, p1, Lekm;->cdv:Z

    iput-boolean v0, p0, Lekm;->cdv:Z

    .line 1216
    iget-object v0, p1, Lekm;->cdz:Lekn;

    iput-object v0, p0, Lekm;->cdz:Lekn;

    .line 1217
    iget-object v0, p1, Lekm;->cdA:Lekn;

    iput-object v0, p0, Lekm;->cdA:Lekn;

    .line 1218
    iget v0, p1, Lekm;->cdy:I

    iput v0, p0, Lekm;->cdy:I

    .line 1219
    iget v0, p1, Lekm;->cdB:I

    iput v0, p0, Lekm;->cdB:I

    .line 1220
    iget v0, p1, Lekm;->cdC:I

    iput v0, p0, Lekm;->cdC:I

    .line 1221
    iget v0, p1, Lekm;->cdD:I

    iput v0, p0, Lekm;->cdD:I

    .line 1222
    iget v0, p1, Lekm;->cdE:I

    iput v0, p0, Lekm;->cdE:I

    .line 1223
    iget v0, p1, Lekm;->cdF:I

    iput v0, p0, Lekm;->cdF:I

    .line 1224
    iget v0, p1, Lekm;->cdG:I

    iput v0, p0, Lekm;->cdG:I

    .line 1225
    iget v0, p1, Lekm;->cdH:I

    iput v0, p0, Lekm;->cdH:I

    .line 1226
    iget v0, p1, Lekm;->cdI:I

    iput v0, p0, Lekm;->cdI:I

    .line 1227
    iget-object v0, p1, Lekm;->cdJ:Landroid/graphics/Rect;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lekm;->cdJ:Landroid/graphics/Rect;

    .line 1229
    :cond_0
    return-void

    .line 1227
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p1, Lekm;->cdJ:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method private static a(Landroid/content/res/TypedArray;ILekn;)Lekn;
    .locals 2

    .prologue
    .line 1267
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 1268
    if-ltz v0, :cond_0

    invoke-static {}, Lekn;->values()[Lekn;

    move-result-object v1

    aget-object p2, v1, v0

    :cond_0
    return-object p2
.end method
