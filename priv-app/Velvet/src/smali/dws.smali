.class public final Ldws;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final bOO:Ldws;


# instance fields
.field private final bOP:Ljava/lang/String;

.field private final bOQ:D

.field private final bOR:I

.field private final bOS:J

.field private final bOT:Z

.field private final bOU:[Ljava/lang/String;

.field private final bus:I

.field private final dz:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 26
    new-instance v0, Ldws;

    const-wide/16 v6, 0x0

    const-wide/16 v9, 0x0

    move-object v3, v2

    move-object v4, v2

    move v5, v1

    move v8, v1

    invoke-direct/range {v0 .. v10}, Ldws;-><init>(ILjava/lang/String;[Ljava/lang/String;Landroid/net/Uri;IJZD)V

    sput-object v0, Ldws;->bOO:Ldws;

    return-void
.end method

.method public constructor <init>(D)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 67
    const/4 v1, 0x7

    const-wide/16 v6, 0x0

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move v8, v5

    move-wide v9, p1

    invoke-direct/range {v0 .. v10}, Ldws;-><init>(ILjava/lang/String;[Ljava/lang/String;Landroid/net/Uri;IJZD)V

    .line 68
    return-void
.end method

.method public constructor <init>(I)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 47
    const/4 v1, 0x3

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move v5, p1

    invoke-direct/range {v0 .. v10}, Ldws;-><init>(ILjava/lang/String;[Ljava/lang/String;Landroid/net/Uri;IJZD)V

    .line 48
    return-void
.end method

.method private constructor <init>(ILjava/lang/String;[Ljava/lang/String;Landroid/net/Uri;IJZD)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput p1, p0, Ldws;->bus:I

    .line 73
    iput-object p2, p0, Ldws;->bOP:Ljava/lang/String;

    .line 74
    iput-object p4, p0, Ldws;->dz:Landroid/net/Uri;

    .line 75
    iput p5, p0, Ldws;->bOR:I

    .line 76
    iput-wide p6, p0, Ldws;->bOS:J

    .line 77
    iput-boolean p8, p0, Ldws;->bOT:Z

    .line 78
    iput-object p3, p0, Ldws;->bOU:[Ljava/lang/String;

    .line 79
    iput-wide p9, p0, Ldws;->bOQ:D

    .line 80
    return-void
.end method

.method public constructor <init>(J)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 51
    const/4 v1, 0x4

    const-wide/16 v9, 0x0

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move-wide v6, p1

    move v8, v5

    invoke-direct/range {v0 .. v10}, Ldws;-><init>(ILjava/lang/String;[Ljava/lang/String;Landroid/net/Uri;IJZD)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 59
    const/4 v1, 0x2

    const-wide/16 v6, 0x0

    const-wide/16 v9, 0x0

    move-object v0, p0

    move-object v3, v2

    move-object v4, p1

    move v8, v5

    invoke-direct/range {v0 .. v10}, Ldws;-><init>(ILjava/lang/String;[Ljava/lang/String;Landroid/net/Uri;IJZD)V

    .line 60
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 43
    const/4 v1, 0x1

    const-wide/16 v6, 0x0

    const-wide/16 v9, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v4, v3

    move v8, v5

    invoke-direct/range {v0 .. v10}, Ldws;-><init>(ILjava/lang/String;[Ljava/lang/String;Landroid/net/Uri;IJZD)V

    .line 44
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 55
    const/4 v1, 0x5

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v9, 0x0

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move v8, p1

    invoke-direct/range {v0 .. v10}, Ldws;-><init>(ILjava/lang/String;[Ljava/lang/String;Landroid/net/Uri;IJZD)V

    .line 56
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 63
    const/4 v1, 0x6

    const-wide/16 v6, 0x0

    const-wide/16 v9, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, v2

    move v8, v5

    invoke-direct/range {v0 .. v10}, Ldws;-><init>(ILjava/lang/String;[Ljava/lang/String;Landroid/net/Uri;IJZD)V

    .line 64
    return-void
.end method


# virtual methods
.method public final Pd()Z
    .locals 1

    .prologue
    .line 83
    sget-object v0, Ldws;->bOO:Ldws;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ajU()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 129
    iget v0, p0, Ldws;->bus:I

    packed-switch v0, :pswitch_data_0

    .line 133
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ldws;->getString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    :goto_0
    return-object v0

    .line 131
    :pswitch_0
    iget-object v0, p0, Ldws;->bOU:[Ljava/lang/String;

    goto :goto_0

    .line 129
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method public final ajV()I
    .locals 3

    .prologue
    .line 149
    iget v0, p0, Ldws;->bus:I

    packed-switch v0, :pswitch_data_0

    .line 161
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t convert to integer (type="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Ldws;->bus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 151
    :pswitch_1
    iget-object v0, p0, Ldws;->bOP:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 159
    :goto_0
    return v0

    .line 153
    :pswitch_2
    iget v0, p0, Ldws;->bOR:I

    goto :goto_0

    .line 155
    :pswitch_3
    iget-wide v0, p0, Ldws;->bOS:J

    long-to-int v0, v0

    goto :goto_0

    .line 157
    :pswitch_4
    iget-boolean v0, p0, Ldws;->bOT:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 159
    :pswitch_5
    iget-wide v0, p0, Ldws;->bOQ:D

    double-to-int v0, v0

    goto :goto_0

    .line 149
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public final ajW()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 184
    iget v2, p0, Ldws;->bus:I

    packed-switch v2, :pswitch_data_0

    .line 196
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t convert to boolean (type="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Ldws;->bus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 186
    :pswitch_1
    iget-object v0, p0, Ldws;->bOP:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 194
    :cond_0
    :goto_0
    return v0

    .line 188
    :pswitch_2
    iget v2, p0, Ldws;->bOR:I

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 190
    :pswitch_3
    iget-wide v2, p0, Ldws;->bOS:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 192
    :pswitch_4
    iget-boolean v0, p0, Ldws;->bOT:Z

    goto :goto_0

    .line 194
    :pswitch_5
    iget-wide v2, p0, Ldws;->bOQ:D

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 184
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public final getDouble()D
    .locals 3

    .prologue
    .line 202
    iget v0, p0, Ldws;->bus:I

    packed-switch v0, :pswitch_data_0

    .line 214
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t convert to double (type="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Ldws;->bus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 204
    :pswitch_1
    iget-object v0, p0, Ldws;->bOP:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 212
    :goto_0
    return-wide v0

    .line 206
    :pswitch_2
    iget v0, p0, Ldws;->bOR:I

    int-to-double v0, v0

    goto :goto_0

    .line 208
    :pswitch_3
    iget-wide v0, p0, Ldws;->bOS:J

    long-to-double v0, v0

    goto :goto_0

    .line 210
    :pswitch_4
    iget-boolean v0, p0, Ldws;->bOT:Z

    if-eqz v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 212
    :pswitch_5
    iget-wide v0, p0, Ldws;->bOQ:D

    goto :goto_0

    .line 202
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public final getLong()J
    .locals 3

    .prologue
    .line 167
    iget v0, p0, Ldws;->bus:I

    packed-switch v0, :pswitch_data_0

    .line 179
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t convert to long (type="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Ldws;->bus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :pswitch_1
    iget-object v0, p0, Ldws;->bOP:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 177
    :goto_0
    return-wide v0

    .line 171
    :pswitch_2
    iget v0, p0, Ldws;->bOR:I

    int-to-long v0, v0

    goto :goto_0

    .line 173
    :pswitch_3
    iget-wide v0, p0, Ldws;->bOS:J

    goto :goto_0

    .line 175
    :pswitch_4
    iget-boolean v0, p0, Ldws;->bOT:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x1

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 177
    :pswitch_5
    iget-wide v0, p0, Ldws;->bOQ:D

    double-to-long v0, v0

    goto :goto_0

    .line 167
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public final getObject()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 87
    iget v0, p0, Ldws;->bus:I

    packed-switch v0, :pswitch_data_0

    .line 103
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t return object of unknown type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Ldws;->bus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :pswitch_0
    iget-object v0, p0, Ldws;->bOP:Ljava/lang/String;

    .line 101
    :goto_0
    return-object v0

    .line 91
    :pswitch_1
    iget-object v0, p0, Ldws;->dz:Landroid/net/Uri;

    goto :goto_0

    .line 93
    :pswitch_2
    iget v0, p0, Ldws;->bOR:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 95
    :pswitch_3
    iget-wide v0, p0, Ldws;->bOS:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 97
    :pswitch_4
    iget-boolean v0, p0, Ldws;->bOT:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 99
    :pswitch_5
    iget-object v0, p0, Ldws;->bOU:[Ljava/lang/String;

    goto :goto_0

    .line 101
    :pswitch_6
    iget-wide v0, p0, Ldws;->bOQ:D

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final getString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 109
    iget v0, p0, Ldws;->bus:I

    packed-switch v0, :pswitch_data_0

    .line 123
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t convert to String (type="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Ldws;->bus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :pswitch_1
    iget-object v0, p0, Ldws;->bOP:Ljava/lang/String;

    .line 121
    :goto_0
    return-object v0

    .line 113
    :pswitch_2
    iget-object v0, p0, Ldws;->dz:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 115
    :pswitch_3
    iget v0, p0, Ldws;->bOR:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 117
    :pswitch_4
    iget-wide v0, p0, Ldws;->bOS:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 119
    :pswitch_5
    iget-boolean v0, p0, Ldws;->bOT:Z

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 121
    :pswitch_6
    iget-wide v0, p0, Ldws;->bOQ:D

    invoke-static {v0, v1}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 109
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method public final getUri()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 138
    iget v0, p0, Ldws;->bus:I

    packed-switch v0, :pswitch_data_0

    .line 144
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t convert to URI (type="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Ldws;->bus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :pswitch_0
    iget-object v0, p0, Ldws;->bOP:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 142
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Ldws;->dz:Landroid/net/Uri;

    goto :goto_0

    .line 138
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
