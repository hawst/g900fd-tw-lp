.class public final Lixy;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dOV:[Lixy;


# instance fields
.field private aez:I

.field private bmI:Ljava/lang/String;

.field private bmJ:Z

.field private bmM:J

.field private bmN:Ljava/lang/String;

.field public dOz:Ljbg;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 40182
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 40183
    iput v2, p0, Lixy;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lixy;->bmI:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lixy;->bmN:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lixy;->bmM:J

    iput-boolean v2, p0, Lixy;->bmJ:Z

    iput-object v3, p0, Lixy;->dOz:Ljbg;

    iput-object v3, p0, Lixy;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lixy;->eCz:I

    .line 40184
    return-void
.end method

.method public static bbh()[Lixy;
    .locals 2

    .prologue
    .line 40084
    sget-object v0, Lixy;->dOV:[Lixy;

    if-nez v0, :cond_1

    .line 40085
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 40087
    :try_start_0
    sget-object v0, Lixy;->dOV:[Lixy;

    if-nez v0, :cond_0

    .line 40088
    const/4 v0, 0x0

    new-array v0, v0, [Lixy;

    sput-object v0, Lixy;->dOV:[Lixy;

    .line 40090
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40092
    :cond_1
    sget-object v0, Lixy;->dOV:[Lixy;

    return-object v0

    .line 40090
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final TO()Z
    .locals 1

    .prologue
    .line 40163
    iget-boolean v0, p0, Lixy;->bmJ:Z

    return v0
.end method

.method public final TP()Z
    .locals 1

    .prologue
    .line 40171
    iget v0, p0, Lixy;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final TT()J
    .locals 2

    .prologue
    .line 40144
    iget-wide v0, p0, Lixy;->bmM:J

    return-wide v0
.end method

.method public final TU()Z
    .locals 1

    .prologue
    .line 40152
    iget v0, p0, Lixy;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final TV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40122
    iget-object v0, p0, Lixy;->bmN:Ljava/lang/String;

    return-object v0
.end method

.method public final TW()Z
    .locals 1

    .prologue
    .line 40133
    iget v0, p0, Lixy;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 40078
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lixy;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lixy;->bmI:Ljava/lang/String;

    iget v0, p0, Lixy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lixy;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lixy;->bmN:Ljava/lang/String;

    iget v0, p0, Lixy;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lixy;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lixy;->bmM:J

    iget v0, p0, Lixy;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lixy;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lixy;->bmJ:Z

    iget v0, p0, Lixy;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lixy;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lixy;->dOz:Ljbg;

    if-nez v0, :cond_1

    new-instance v0, Ljbg;

    invoke-direct {v0}, Ljbg;-><init>()V

    iput-object v0, p0, Lixy;->dOz:Ljbg;

    :cond_1
    iget-object v0, p0, Lixy;->dOz:Ljbg;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 40201
    iget v0, p0, Lixy;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 40202
    const/4 v0, 0x1

    iget-object v1, p0, Lixy;->bmI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 40204
    :cond_0
    iget v0, p0, Lixy;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 40205
    const/4 v0, 0x2

    iget-object v1, p0, Lixy;->bmN:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 40207
    :cond_1
    iget v0, p0, Lixy;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 40208
    const/4 v0, 0x3

    iget-wide v2, p0, Lixy;->bmM:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 40210
    :cond_2
    iget v0, p0, Lixy;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 40211
    const/4 v0, 0x4

    iget-boolean v1, p0, Lixy;->bmJ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 40213
    :cond_3
    iget-object v0, p0, Lixy;->dOz:Ljbg;

    if-eqz v0, :cond_4

    .line 40214
    const/4 v0, 0x5

    iget-object v1, p0, Lixy;->dOz:Ljbg;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 40216
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 40217
    return-void
.end method

.method public final ck(J)Lixy;
    .locals 1

    .prologue
    .line 40147
    iput-wide p1, p0, Lixy;->bmM:J

    .line 40148
    iget v0, p0, Lixy;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lixy;->aez:I

    .line 40149
    return-object p0
.end method

.method public final getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40100
    iget-object v0, p0, Lixy;->bmI:Ljava/lang/String;

    return-object v0
.end method

.method public final hj(Z)Lixy;
    .locals 1

    .prologue
    .line 40166
    iput-boolean p1, p0, Lixy;->bmJ:Z

    .line 40167
    iget v0, p0, Lixy;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lixy;->aez:I

    .line 40168
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 40221
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 40222
    iget v1, p0, Lixy;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 40223
    const/4 v1, 0x1

    iget-object v2, p0, Lixy;->bmI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40226
    :cond_0
    iget v1, p0, Lixy;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 40227
    const/4 v1, 0x2

    iget-object v2, p0, Lixy;->bmN:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40230
    :cond_1
    iget v1, p0, Lixy;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 40231
    const/4 v1, 0x3

    iget-wide v2, p0, Lixy;->bmM:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 40234
    :cond_2
    iget v1, p0, Lixy;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 40235
    const/4 v1, 0x4

    iget-boolean v2, p0, Lixy;->bmJ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 40238
    :cond_3
    iget-object v1, p0, Lixy;->dOz:Ljbg;

    if-eqz v1, :cond_4

    .line 40239
    const/4 v1, 0x5

    iget-object v2, p0, Lixy;->dOz:Ljbg;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40242
    :cond_4
    return v0
.end method

.method public final rg(Ljava/lang/String;)Lixy;
    .locals 1

    .prologue
    .line 40103
    if-nez p1, :cond_0

    .line 40104
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 40106
    :cond_0
    iput-object p1, p0, Lixy;->bmI:Ljava/lang/String;

    .line 40107
    iget v0, p0, Lixy;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lixy;->aez:I

    .line 40108
    return-object p0
.end method

.method public final rh(Ljava/lang/String;)Lixy;
    .locals 1

    .prologue
    .line 40125
    if-nez p1, :cond_0

    .line 40126
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 40128
    :cond_0
    iput-object p1, p0, Lixy;->bmN:Ljava/lang/String;

    .line 40129
    iget v0, p0, Lixy;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lixy;->aez:I

    .line 40130
    return-object p0
.end method
