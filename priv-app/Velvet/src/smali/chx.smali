.class public final Lchx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private final aUA:I

.field private final mClient:Lchy;

.field private final mConfig:Lcjs;

.field private final mCoreSearchServices:Lcfo;

.field private final mGelStartupPrefs:Ldku;

.field private final mGsaConfig:Lchk;

.field private final mSettings:Lcke;

.field private final mVsSettings:Lhym;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcfo;Lhym;Ldku;I)V
    .locals 6

    .prologue
    .line 87
    new-instance v1, Lchz;

    invoke-direct {v1, p1}, Lchz;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lchx;-><init>(Lchy;Lcfo;Lhym;Ldku;I)V

    .line 89
    return-void
.end method

.method public constructor <init>(Lchy;Lcfo;Lhym;Ldku;I)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Lchx;->mClient:Lchy;

    .line 96
    invoke-virtual {p2}, Lcfo;->DD()Lcjs;

    move-result-object v0

    iput-object v0, p0, Lchx;->mConfig:Lcjs;

    .line 97
    invoke-virtual {p2}, Lcfo;->BL()Lchk;

    move-result-object v0

    iput-object v0, p0, Lchx;->mGsaConfig:Lchk;

    .line 98
    invoke-virtual {p2}, Lcfo;->BK()Lcke;

    move-result-object v0

    iput-object v0, p0, Lchx;->mSettings:Lcke;

    .line 99
    iput-object p3, p0, Lchx;->mVsSettings:Lhym;

    .line 100
    iput-object p2, p0, Lchx;->mCoreSearchServices:Lcfo;

    .line 101
    iput-object p4, p0, Lchx;->mGelStartupPrefs:Ldku;

    .line 102
    iput p5, p0, Lchx;->aUA:I

    .line 103
    return-void
.end method

.method private static a(Lcke;Lchy;I)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 185
    .line 187
    :try_start_0
    new-instance v4, Ljava/io/StringWriter;

    invoke-direct {v4}, Ljava/io/StringWriter;-><init>()V

    .line 188
    new-instance v5, Landroid/util/JsonWriter;

    invoke-direct {v5, v4}, Landroid/util/JsonWriter;-><init>(Ljava/io/Writer;)V

    .line 189
    invoke-virtual {v5}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    .line 190
    const-string v0, "qsb:"

    invoke-interface {p1, v0}, Lchy;->gu(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 191
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    move v2, v3

    :cond_0
    :goto_0
    :try_start_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 192
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v7, 0x4

    invoke-virtual {v1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 193
    const-string v7, "debug_level"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 194
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lchx;->gt(Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 197
    :cond_1
    const/16 v7, 0x3a

    invoke-virtual {v1, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    .line 198
    if-ltz v7, :cond_2

    .line 199
    const/4 v8, 0x0

    invoke-virtual {v1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, p2}, Lchx;->m(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 200
    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 204
    :cond_2
    invoke-virtual {v5, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 216
    :catch_0
    move-exception v0

    move v1, v2

    :goto_1
    const-string v0, ""

    .line 218
    if-eqz p0, :cond_3

    .line 220
    invoke-interface {p0, v1}, Lcke;->fb(I)V

    :cond_3
    :goto_2
    return-object v0

    .line 206
    :cond_4
    :try_start_2
    invoke-static {}, Lcjs;->KZ()[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    :goto_3
    if-ge v3, v1, :cond_6

    aget-object v6, v0, v3

    .line 207
    const/4 v7, 0x0

    invoke-interface {p1, v6, v7}, Lchy;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 208
    if-eqz v7, :cond_5

    .line 209
    invoke-virtual {v5, v6}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 206
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 212
    :cond_6
    invoke-virtual {v5}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;

    .line 213
    invoke-virtual {v5}, Landroid/util/JsonWriter;->close()V

    .line 214
    invoke-virtual {v4}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 218
    if-eqz p0, :cond_3

    .line 220
    invoke-interface {p0, v2}, Lcke;->fb(I)V

    goto :goto_2

    .line 218
    :catchall_0
    move-exception v0

    move v2, v3

    :goto_4
    if-eqz p0, :cond_7

    .line 220
    invoke-interface {p0, v2}, Lcke;->fb(I)V

    :cond_7
    throw v0

    .line 218
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 216
    :catch_1
    move-exception v0

    move v1, v3

    goto :goto_1
.end method

.method public static ah(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 251
    const/4 v0, 0x0

    new-instance v1, Lchz;

    invoke-direct {v1, p0}, Lchz;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->xT()I

    move-result v2

    invoke-static {v0, v1, v2}, Lchx;->a(Lcke;Lchy;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static gt(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 256
    if-nez p0, :cond_0

    .line 263
    :goto_0
    return v0

    .line 260
    :cond_0
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 262
    :catch_0
    move-exception v1

    const-string v1, "Search.GservicesUpdateTask"

    const-string v2, "Malformed integer value."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static m(Ljava/lang/String;I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 226
    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 227
    if-ltz v2, :cond_1

    .line 231
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 232
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 237
    :goto_0
    if-lt p1, v1, :cond_0

    if-gt p1, v2, :cond_0

    const/4 v0, 0x1

    .line 240
    :cond_0
    :goto_1
    return v0

    .line 234
    :cond_1
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    move v2, v1

    .line 235
    goto :goto_0

    .line 238
    :catch_0
    move-exception v1

    .line 239
    const-string v2, "Search.GservicesUpdateTask"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error parsing gservices version spec "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lchx;->ub()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final ub()Ljava/lang/Void;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 107
    const-string v0, "Search.GservicesUpdateTask"

    const-string v1, "Updating Gservices keys"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    iget-object v0, p0, Lchx;->mSettings:Lcke;

    iget-object v1, p0, Lchx;->mSettings:Lcke;

    iget-object v2, p0, Lchx;->mClient:Lchy;

    iget v3, p0, Lchx;->aUA:I

    invoke-static {v1, v2, v3}, Lchx;->a(Lcke;Lchy;I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcke;->gQ(Ljava/lang/String;)V

    .line 112
    invoke-static {}, Lckw;->OZ()Lckw;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Lckw;->Pd()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    iget-object v1, p0, Lchx;->mSettings:Lcke;

    invoke-virtual {v0, v1}, Lckw;->a(Lcke;)V

    .line 117
    :cond_0
    iget-object v0, p0, Lchx;->mVsSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aTK()V

    .line 120
    iget-object v0, p0, Lchx;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->Ef()Lgpc;

    move-result-object v0

    invoke-virtual {v0}, Lgpc;->acF()V

    .line 123
    iget-object v0, p0, Lchx;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->La()V

    .line 127
    iget-object v0, p0, Lchx;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->IW()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lchx;->mVsSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aTJ()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 129
    iget-object v0, p0, Lchx;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DO()Lgpp;

    move-result-object v0

    const-string v1, "refresh_auth_tokens"

    invoke-interface {v0, v1}, Lgpp;->nw(Ljava/lang/String;)V

    .line 133
    :cond_1
    iget-object v0, p0, Lchx;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Mr()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 134
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->ayn()Lfer;

    .line 139
    :goto_0
    iget-object v0, p0, Lchx;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Mv()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lchx;->mGelStartupPrefs:Ldku;

    const-string v1, "GSAPrefs.redirect_mfe_requests"

    invoke-virtual {v0, v1, v5}, Ldku;->m(Ljava/lang/String;Z)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lchx;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Mw()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lchx;->mGelStartupPrefs:Ldku;

    const-string v1, "GSAPrefs.redirect_mfe_to_gmm"

    invoke-virtual {v0, v1, v4}, Ldku;->m(Ljava/lang/String;Z)V

    :cond_3
    :goto_2
    iget-object v0, p0, Lchx;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Mx()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lchx;->mGelStartupPrefs:Ldku;

    const-string v1, "quantum_now_cards"

    invoke-virtual {v0, v1, v4}, Ldku;->m(Ljava/lang/String;Z)V

    :cond_4
    :goto_3
    iget-object v0, p0, Lchx;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->My()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lchx;->mGelStartupPrefs:Ldku;

    const-string v1, "traditional_view_time_recording"

    invoke-virtual {v0, v1, v5}, Ldku;->m(Ljava/lang/String;Z)V

    :goto_4
    iget-object v0, p0, Lchx;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->Og()I

    move-result v0

    iget-object v1, p0, Lchx;->mGelStartupPrefs:Ldku;

    const-string v2, "GSAPrefs.debug_features_level"

    invoke-virtual {v1, v2, v0}, Ldku;->p(Ljava/lang/String;I)V

    iget-object v0, p0, Lchx;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->wu()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lchx;->mGelStartupPrefs:Ldku;

    const-string v1, "GSAPrefs.high_contrast"

    invoke-virtual {v0, v1, v4}, Ldku;->m(Ljava/lang/String;Z)V

    .line 141
    :goto_5
    return-object v6

    .line 136
    :cond_5
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    iget-object v1, v0, Lfdb;->aqp:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lfdb;->mVehicleActivityHandler:Lfer;

    if-eqz v2, :cond_6

    iget-object v2, v0, Lfdb;->mVehicleActivityHandler:Lfer;

    invoke-virtual {v2}, Lfer;->ayN()V

    const/4 v2, 0x0

    iput-object v2, v0, Lfdb;->mVehicleActivityHandler:Lfer;

    :cond_6
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, v0, Lfdb;->coW:Lgue;

    invoke-virtual {v0}, Lgue;->aKw()Lcgs;

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 139
    :cond_7
    iget-object v0, p0, Lchx;->mGelStartupPrefs:Ldku;

    const-string v1, "GSAPrefs.redirect_mfe_requests"

    invoke-virtual {v0, v1}, Ldku;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lchx;->mGelStartupPrefs:Ldku;

    const-string v1, "GSAPrefs.redirect_mfe_requests"

    invoke-virtual {v0, v1}, Ldku;->remove(Ljava/lang/String;)V

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lchx;->mGelStartupPrefs:Ldku;

    const-string v1, "GSAPrefs.redirect_mfe_to_gmm"

    invoke-virtual {v0, v1}, Ldku;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lchx;->mGelStartupPrefs:Ldku;

    const-string v1, "GSAPrefs.redirect_mfe_to_gmm"

    invoke-virtual {v0, v1}, Ldku;->remove(Ljava/lang/String;)V

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lchx;->mGelStartupPrefs:Ldku;

    const-string v1, "quantum_now_cards"

    invoke-virtual {v0, v1}, Ldku;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lchx;->mGelStartupPrefs:Ldku;

    const-string v1, "quantum_now_cards"

    invoke-virtual {v0, v1}, Ldku;->remove(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_a
    iget-object v0, p0, Lchx;->mGelStartupPrefs:Ldku;

    const-string v1, "traditional_view_time_recording"

    invoke-virtual {v0, v1}, Ldku;->remove(Ljava/lang/String;)V

    goto :goto_4

    :cond_b
    iget-object v0, p0, Lchx;->mGelStartupPrefs:Ldku;

    const-string v1, "GSAPrefs.high_contrast"

    invoke-virtual {v0, v1}, Ldku;->remove(Ljava/lang/String;)V

    goto :goto_5
.end method
