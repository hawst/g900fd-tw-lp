.class public final Ldak;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private synthetic bpJ:Lcom/google/android/search/core/service/BroadcastListenerService;


# direct methods
.method public constructor <init>(Lcom/google/android/search/core/service/BroadcastListenerService;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Ldak;->bpJ:Lcom/google/android/search/core/service/BroadcastListenerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 82
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 83
    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 84
    iget-object v1, p0, Ldak;->bpJ:Lcom/google/android/search/core/service/BroadcastListenerService;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/search/core/service/BroadcastListenerService;->d(Landroid/content/Context;Z)V

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 86
    iget-object v0, p0, Ldak;->bpJ:Lcom/google/android/search/core/service/BroadcastListenerService;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/search/core/service/BroadcastListenerService;->d(Landroid/content/Context;Z)V

    .line 87
    iget-object v0, p0, Ldak;->bpJ:Lcom/google/android/search/core/service/BroadcastListenerService;

    iget-object v1, p0, Ldak;->bpJ:Lcom/google/android/search/core/service/BroadcastListenerService;

    iget-object v1, v1, Lcom/google/android/search/core/service/BroadcastListenerService;->bpv:Landroid/app/KeyguardManager;

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/search/core/service/BroadcastListenerService;->e(Landroid/content/Context;Z)V

    goto :goto_0

    .line 88
    :cond_2
    const-string v3, "android.intent.action.USER_PRESENT"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 89
    iget-object v0, p0, Ldak;->bpJ:Lcom/google/android/search/core/service/BroadcastListenerService;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/search/core/service/BroadcastListenerService;->e(Landroid/content/Context;Z)V

    goto :goto_0

    .line 90
    :cond_3
    const-string v3, "android.provider.Telephony.SMS_RECEIVED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 91
    iget-object v0, p0, Ldak;->bpJ:Lcom/google/android/search/core/service/BroadcastListenerService;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/search/core/service/BroadcastListenerService;->c(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 92
    :cond_4
    const-string v3, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 93
    iget-object v1, p0, Ldak;->bpJ:Lcom/google/android/search/core/service/BroadcastListenerService;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/search/core/service/BroadcastListenerService;->f(Landroid/content/Context;Z)V

    goto :goto_0

    .line 94
    :cond_5
    const-string v3, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 95
    iget-object v0, p0, Ldak;->bpJ:Lcom/google/android/search/core/service/BroadcastListenerService;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/search/core/service/BroadcastListenerService;->f(Landroid/content/Context;Z)V

    goto :goto_0

    .line 96
    :cond_6
    const-string v3, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 97
    iget-object v2, p0, Ldak;->bpJ:Lcom/google/android/search/core/service/BroadcastListenerService;

    const-string v3, "state"

    invoke-virtual {p2, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v0, :cond_7

    :goto_1
    invoke-virtual {v2, p1, v0}, Lcom/google/android/search/core/service/BroadcastListenerService;->g(Landroid/content/Context;Z)V

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_1

    .line 98
    :cond_8
    const-string v1, "com.google.android.velvet.location.ACTIVITY_DETECTION"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 100
    iget-object v1, p0, Ldak;->bpJ:Lcom/google/android/search/core/service/BroadcastListenerService;

    invoke-static {p1}, Lesp;->aA(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, p1, p2, v0, v2}, Lcom/google/android/search/core/service/BroadcastListenerService;->a(Landroid/content/Context;Landroid/content/Intent;ZZ)V

    goto :goto_0

    .line 102
    :cond_9
    const-string v0, "com.google.android.apps.gmm.NAVIGATION_STATE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Ldak;->bpJ:Lcom/google/android/search/core/service/BroadcastListenerService;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/search/core/service/BroadcastListenerService;->d(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
