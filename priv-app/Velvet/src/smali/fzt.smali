.class public final Lfzt;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 37
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const v7, 0x7f11037d

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 46
    iget-object v4, p0, Lfro;->mView:Landroid/view/View;

    .line 47
    iget-object v0, p0, Lfro;->cBc:Lanh;

    .line 48
    iget-object v5, v0, Lanh;->agC:Lapt;

    .line 50
    iget-object v6, v5, Lapt;->alM:Lapv;

    .line 51
    if-eqz v6, :cond_2

    .line 52
    invoke-virtual {v6}, Lapv;->tv()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    const v0, 0x7f110082

    invoke-virtual {v6}, Lapv;->getLocation()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v0, v3}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 56
    :cond_0
    invoke-virtual {v6}, Lapv;->ty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    invoke-virtual {v6}, Lapv;->tx()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v7, v0}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 59
    invoke-virtual {v6}, Lapv;->tx()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v3, 0x3

    if-le v0, v3, :cond_1

    .line 61
    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lfzt;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v7, 0x7f0d01ab

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 68
    :cond_1
    const v0, 0x7f11037c

    invoke-virtual {v6}, Lapv;->tw()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v4, v0, v3}, Lfzt;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 75
    :cond_2
    const-string v0, " \u00b7 "

    new-array v3, v10, [Ljava/lang/CharSequence;

    invoke-virtual {v5}, Lapt;->tq()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v2

    invoke-virtual {v5}, Lapt;->tr()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v9

    invoke-static {v0, v3}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 77
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 78
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 79
    invoke-virtual {v4}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v7, 0x7f0b00b1

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 80
    new-instance v7, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v7, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v8, 0x11

    invoke-virtual {v0, v7, v2, v3, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move-object v3, v0

    .line 90
    :goto_0
    if-eqz v6, :cond_8

    invoke-virtual {v6}, Lapv;->tA()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 91
    invoke-virtual {v6}, Lapv;->tz()Ljava/lang/String;

    move-result-object v0

    .line 94
    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 95
    :cond_3
    const-string v1, "\u2007\u2007"

    new-array v6, v10, [Ljava/lang/CharSequence;

    aput-object v0, v6, v2

    aput-object v3, v6, v9

    invoke-static {v1, v6}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 97
    const v1, 0x7f11037b

    invoke-static {v4, v1, v0}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 100
    :cond_4
    const v0, 0x7f11037e

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 101
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 102
    iget-object v1, v5, Lapt;->alP:[Lapu;

    array-length v1, v1

    if-eqz v1, :cond_7

    .line 103
    iget-object v3, v5, Lapt;->alP:[Lapu;

    array-length v4, v3

    move v1, v2

    :goto_2
    if-ge v1, v4, :cond_6

    aget-object v5, v3, v1

    .line 104
    iget-object v6, p0, Lfzt;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f0401c7

    invoke-virtual {v6, v7, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 106
    const v7, 0x7f110198

    invoke-virtual {v5}, Lapu;->getLabel()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 107
    const v7, 0x7f110380

    invoke-virtual {v5}, Lapu;->tt()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 108
    const v7, 0x7f110381

    invoke-virtual {v5}, Lapu;->tu()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 109
    const v7, 0x7f11005e

    invoke-virtual {v5}, Lapu;->ps()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v6, v7, v5}, Lfzt;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 110
    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 103
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    move-object v3, v1

    .line 86
    goto :goto_0

    .line 112
    :cond_6
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 116
    :goto_3
    return-void

    .line 114
    :cond_7
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_3

    :cond_8
    move-object v0, v1

    goto :goto_1
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 41
    const v0, 0x7f040148

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
