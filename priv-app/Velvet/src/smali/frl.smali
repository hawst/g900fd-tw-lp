.class final Lfrl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field private synthetic blf:Landroid/widget/EditText;

.field private synthetic cAV:Ljava/lang/String;

.field private synthetic cAW:Landroid/widget/EditText;

.field private synthetic cAX:Landroid/text/TextWatcher;


# direct methods
.method constructor <init>(Lfrk;Ljava/lang/String;Landroid/widget/EditText;Landroid/widget/EditText;Landroid/text/TextWatcher;)V
    .locals 0

    .prologue
    .line 88
    iput-object p2, p0, Lfrl;->cAV:Ljava/lang/String;

    iput-object p3, p0, Lfrl;->blf:Landroid/widget/EditText;

    iput-object p4, p0, Lfrl;->cAW:Landroid/widget/EditText;

    iput-object p5, p0, Lfrl;->cAX:Landroid/text/TextWatcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 91
    if-eqz p2, :cond_1

    .line 92
    iget-object v0, p0, Lfrl;->cAV:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrl;->cAV:Ljava/lang/String;

    iget-object v1, p0, Lfrl;->blf:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lfrl;->blf:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v0, p0, Lfrl;->cAW:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 96
    :cond_0
    iget-object v0, p0, Lfrl;->blf:Landroid/widget/EditText;

    iget-object v1, p0, Lfrl;->cAX:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 100
    :goto_0
    return-void

    .line 98
    :cond_1
    iget-object v0, p0, Lfrl;->blf:Landroid/widget/EditText;

    iget-object v1, p0, Lfrl;->cAX:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_0
.end method
