.class public final Lcoh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lemy;


# instance fields
.field private final bbG:Ljava/util/concurrent/atomic/AtomicInteger;

.field private bdC:Z

.field private bdD:Z

.field private bdE:Z

.field private bdF:Ljava/lang/String;

.field private bdG:Z

.field private bdH:Ljyw;

.field private bdI:Ljava/lang/Boolean;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bdz:Z

.field private final mExtrasConsumer:Lcoi;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mPreloadTaskHandler:Lcpt;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mSrpConsumer:Lcnf;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mStaticContentCache:Lcpq;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcnf;Lcoi;Ljava/lang/String;ZLcpt;Lcpq;)V
    .locals 2
    .param p1    # Lcnf;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcoi;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # Lcpt;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p6    # Lcpq;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcoh;->bbG:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcoh;->bdG:Z

    .line 92
    iput-object v1, p0, Lcoh;->bdH:Ljyw;

    .line 97
    iput-object v1, p0, Lcoh;->bdI:Ljava/lang/Boolean;

    .line 125
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnf;

    iput-object v0, p0, Lcoh;->mSrpConsumer:Lcnf;

    .line 126
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcoi;

    iput-object v0, p0, Lcoh;->mExtrasConsumer:Lcoi;

    .line 127
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    iput-boolean p4, p0, Lcoh;->bdz:Z

    .line 130
    iput-object p5, p0, Lcoh;->mPreloadTaskHandler:Lcpt;

    .line 131
    iput-object p6, p0, Lcoh;->mStaticContentCache:Lcpq;

    .line 132
    return-void
.end method

.method private a(Lkem;Z)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 203
    iget-object v2, p1, Lkem;->eUQ:Lkel;

    invoke-static {v2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    iget-object v2, p1, Lkem;->eUQ:Lkel;

    iget-object v2, v2, Lkel;->eUJ:Ljyw;

    invoke-virtual {v2}, Ljyw;->bwD()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Lkem;->eUQ:Lkel;

    iget-object v2, v2, Lkel;->eUJ:Ljyw;

    invoke-virtual {v2}, Ljyw;->bwC()Z

    move-result v2

    if-eqz v2, :cond_2

    move v6, v0

    .line 212
    :goto_0
    iget-object v2, p0, Lcoh;->bdH:Ljyw;

    if-nez v2, :cond_0

    .line 214
    new-instance v2, Ljyw;

    invoke-direct {v2}, Ljyw;-><init>()V

    iput-object v2, p0, Lcoh;->bdH:Ljyw;

    .line 216
    :cond_0
    iget-object v2, p0, Lcoh;->bdH:Ljyw;

    iget-object v3, p1, Lkem;->eUQ:Lkel;

    iget-object v3, v3, Lkel;->eUJ:Ljyw;

    if-eqz v3, :cond_3

    invoke-static {v2, v3}, Leqh;->b(Ljsr;Ljsr;)V

    .line 218
    :goto_1
    iget-object v2, p0, Lcoh;->bdI:Ljava/lang/Boolean;

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lkem;->bzB()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 220
    invoke-virtual {p1}, Lkem;->bzA()I

    move-result v2

    if-eqz v2, :cond_4

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcoh;->bdI:Ljava/lang/Boolean;

    .line 233
    :cond_1
    if-eqz v6, :cond_5

    .line 266
    :goto_3
    return v6

    :cond_2
    move v6, v1

    .line 207
    goto :goto_0

    .line 216
    :cond_3
    const-string v2, "PelletDemultiplexer"

    const-string v3, "No card metadata received - this affects logging."

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x5

    invoke-static {v5, v2, v3, v4}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_4
    move v0, v1

    .line 220
    goto :goto_2

    .line 240
    :cond_5
    if-eqz p2, :cond_6

    .line 243
    iget-object v0, p0, Lcoh;->bdH:Ljyw;

    invoke-virtual {v0}, Ljyw;->bwy()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 244
    iget-object v0, p0, Lcoh;->bdH:Ljyw;

    invoke-virtual {v0}, Ljyw;->bwx()Ljava/lang/String;

    move-result-object v2

    .line 249
    :goto_4
    iget-object v0, p0, Lcoh;->bdH:Ljyw;

    iget-object v0, v0, Ljyw;->eMz:Ljtg;

    if-eqz v0, :cond_7

    .line 251
    iget-object v0, p0, Lcoh;->bdH:Ljyw;

    iget-object v3, v0, Ljyw;->eMz:Ljtg;

    .line 253
    :goto_5
    iget-object v8, p0, Lcoh;->mExtrasConsumer:Lcoi;

    new-instance v0, Lcms;

    invoke-virtual {p1}, Lkem;->aoM()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcoh;->bdH:Ljyw;

    iget-object v5, p0, Lcoh;->bdI:Ljava/lang/Boolean;

    invoke-direct/range {v0 .. v5}, Lcms;-><init>(Ljava/lang/String;Ljava/lang/String;Ljtg;Ljyw;Ljava/lang/Boolean;)V

    invoke-interface {v8, v0}, Lcoi;->a(Lcms;)V

    .line 255
    invoke-virtual {p1}, Lkem;->bzG()Z

    move-result v0

    invoke-direct {p0, v0}, Lcoh;->cx(Z)V

    .line 258
    iput-object v7, p0, Lcoh;->bdH:Ljyw;

    .line 259
    iput-object v7, p0, Lcoh;->bdI:Ljava/lang/Boolean;

    goto :goto_3

    .line 264
    :cond_6
    iget-object v0, p0, Lcoh;->mExtrasConsumer:Lcoi;

    iget-object v1, p1, Lkem;->eUQ:Lkel;

    iget-object v1, v1, Lkel;->eUJ:Ljyw;

    invoke-interface {v0, v1}, Lcoi;->a(Ljyw;)V

    goto :goto_3

    :cond_7
    move-object v3, v7

    goto :goto_5

    :cond_8
    move-object v2, v7

    goto :goto_4
.end method

.method private cx(Z)V
    .locals 1

    .prologue
    .line 463
    iget-boolean v0, p0, Lcoh;->bdD:Z

    if-nez v0, :cond_0

    .line 465
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcoh;->bdD:Z

    .line 466
    iget-object v0, p0, Lcoh;->mExtrasConsumer:Lcoi;

    invoke-interface {v0, p1}, Lcoi;->cu(Z)V

    .line 468
    :cond_0
    return-void
.end method


# virtual methods
.method final QX()V
    .locals 1

    .prologue
    .line 483
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcoh;->cx(Z)V

    .line 484
    return-void
.end method

.method public final a(Lcog;)Z
    .locals 9
    .param p1    # Lcog;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x5

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 158
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    iget-object v5, p1, Lcog;->bdB:Lkem;

    .line 172
    iget-object v0, p0, Lcoh;->mExtrasConsumer:Lcoi;

    iget v3, p1, Lcog;->eX:I

    invoke-interface {v0, v3}, Lcoi;->fe(I)V

    .line 174
    iget-object v0, v5, Lkem;->eUS:Lkeo;

    if-eqz v0, :cond_1

    .line 175
    iget-object v0, p0, Lcoh;->mExtrasConsumer:Lcoi;

    invoke-virtual {v5}, Lkem;->aoM()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v5, Lkem;->eUS:Lkeo;

    invoke-virtual {v3}, Lkeo;->bzI()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcoi;->N(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_0
    :goto_0
    return v1

    .line 176
    :cond_1
    iget-object v0, v5, Lkem;->eUR:Lken;

    if-eqz v0, :cond_14

    .line 177
    iget-object v3, p1, Lcog;->aZF:Ljava/lang/String;

    iget-object v0, v5, Lkem;->eUR:Lken;

    if-eqz v0, :cond_8

    move v0, v1

    :goto_1
    invoke-static {v0}, Lifv;->gX(Z)V

    iget-object v0, p0, Lcoh;->bdF:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, v5, Lkem;->eUR:Lken;

    invoke-virtual {v0}, Lken;->bhT()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v5, Lkem;->eUR:Lken;

    invoke-virtual {v0}, Lken;->getQuery()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcoh;->bdF:Ljava/lang/String;

    iget-object v0, p0, Lcoh;->mExtrasConsumer:Lcoi;

    iget-object v4, p0, Lcoh;->bdF:Ljava/lang/String;

    invoke-interface {v0, v4}, Lcoi;->hn(Ljava/lang/String;)V

    :cond_2
    iget-object v0, v5, Lkem;->eUQ:Lkel;

    if-eqz v0, :cond_3

    iget-object v0, v5, Lkem;->eUQ:Lkel;

    iget-object v0, v0, Lkel;->eUJ:Ljyw;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcoh;->bdG:Z

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lcoh;->bdG:Z

    invoke-direct {p0, v5, v0}, Lcoh;->a(Lkem;Z)Z

    move-result v0

    iput-boolean v2, p0, Lcoh;->bdG:Z

    if-eqz v0, :cond_3

    const-string v0, "PelletDemultiplexer"

    const-string v4, "SRP pellet expects continuation. This is not expected!"

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v7, v0, v4, v6}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_3
    :goto_2
    invoke-virtual {v5}, Lkem;->bzF()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcoh;->bdC:Z

    if-nez v0, :cond_4

    invoke-virtual {v5}, Lkem;->bzE()Z

    move-result v0

    if-eqz v0, :cond_4

    iput-boolean v1, p0, Lcoh;->bdC:Z

    iget-object v0, p0, Lcoh;->mExtrasConsumer:Lcoi;

    invoke-interface {v0}, Lcoi;->PB()V

    :cond_4
    iget-boolean v0, p0, Lcoh;->bdE:Z

    if-nez v0, :cond_6

    iget-object v0, v5, Lkem;->eUN:[I

    if-nez v0, :cond_5

    iget-object v0, v5, Lkem;->eUR:Lken;

    invoke-virtual {v0}, Lken;->bzy()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    iput-boolean v1, p0, Lcoh;->bdE:Z

    iget-object v4, p0, Lcoh;->mExtrasConsumer:Lcoi;

    iget-object v0, v5, Lkem;->eUN:[I

    if-eqz v0, :cond_a

    iget-object v0, v5, Lkem;->eUN:[I

    :goto_3
    invoke-interface {v4, v0}, Lcoi;->g([I)V

    :cond_6
    iget-object v0, v5, Lkem;->eUR:Lken;

    invoke-virtual {v0}, Lken;->getStreamType()I

    move-result v0

    if-ne v0, v8, :cond_b

    new-array v0, v8, [[B

    iget-object v3, v5, Lkem;->eUR:Lken;

    invoke-virtual {v3}, Lken;->acS()[B

    move-result-object v3

    aput-object v3, v0, v2

    const-string v3, "/*\"\"*/"

    sget-object v4, Lesp;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {v0}, Liur;->b([[B)[B

    move-result-object v0

    move-object v3, v0

    :goto_4
    iget-object v0, p0, Lcoh;->bbG:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iget-object v4, v5, Lkem;->eUR:Lken;

    invoke-virtual {v4}, Lken;->bhf()Z

    move-result v4

    if-eqz v4, :cond_10

    iget-object v4, p0, Lcoh;->mPreloadTaskHandler:Lcpt;

    invoke-interface {v4}, Lcpt;->PC()Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcoh;->mSrpConsumer:Lcnf;

    invoke-interface {v4}, Lcnf;->QF()Z

    :cond_7
    iget-object v4, v5, Lkem;->eUR:Lken;

    iget-object v4, v4, Lken;->eUY:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_13

    iget-object v4, p0, Lcoh;->mStaticContentCache:Lcpq;

    iget-object v6, v5, Lkem;->eUR:Lken;

    iget-object v6, v6, Lken;->eUY:[Ljava/lang/String;

    invoke-virtual {v4, v6}, Lcpq;->h([Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v0

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iget-object v7, p0, Lcoh;->mSrpConsumer:Lcnf;

    new-instance v8, Ldkd;

    invoke-direct {v8, v0, v4}, Ldkd;-><init>([BI)V

    invoke-interface {v7, v8}, Lcnf;->a(Ldkd;)Z

    iget-object v0, p0, Lcoh;->bbG:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    move v4, v0

    goto :goto_5

    :cond_8
    move v0, v2

    goto/16 :goto_1

    :cond_9
    const-string v0, "PelletDemultiplexer"

    const-string v4, "SRP metadata pellet arriving with no prior EOC pellet"

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v6}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_a
    new-array v0, v2, [I

    goto/16 :goto_3

    :cond_b
    iget-boolean v4, p0, Lcoh;->bdz:Z

    if-eqz v4, :cond_c

    invoke-static {v5, v1, v3}, Lcol;->a(Lkem;ZLjava/lang/String;)[B

    move-result-object v0

    move-object v3, v0

    goto :goto_4

    :cond_c
    if-ne v0, v1, :cond_d

    iget-object v0, v5, Lkem;->eUR:Lken;

    invoke-virtual {v0}, Lken;->acS()[B

    move-result-object v0

    move-object v3, v0

    goto :goto_4

    :cond_d
    const/4 v3, -0x1

    if-ne v0, v3, :cond_e

    iget-object v0, v5, Lkem;->eUR:Lken;

    invoke-virtual {v0}, Lken;->acS()[B

    move-result-object v0

    move-object v3, v0

    goto :goto_4

    :cond_e
    const-string v3, "PelletDemultiplexer"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Unexpected stream id: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v7, v3, v0, v4}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    iget-object v0, v5, Lkem;->eUR:Lken;

    invoke-virtual {v0}, Lken;->acS()[B

    move-result-object v0

    move-object v3, v0

    goto/16 :goto_4

    :cond_f
    move v0, v4

    :cond_10
    :goto_6
    iget-object v4, p0, Lcoh;->mSrpConsumer:Lcnf;

    new-instance v6, Ldkd;

    invoke-direct {v6, v3, v0}, Ldkd;-><init>([BI)V

    invoke-interface {v4, v6}, Lcnf;->a(Ldkd;)Z

    iget-object v3, v5, Lkem;->eUR:Lken;

    invoke-virtual {v3}, Lken;->bhf()Z

    move-result v3

    if-eqz v3, :cond_12

    iget-object v3, p0, Lcoh;->mStaticContentCache:Lcpq;

    iget-object v4, v5, Lkem;->eUR:Lken;

    invoke-virtual {v4}, Lken;->acS()[B

    move-result-object v4

    iget-object v6, v5, Lkem;->eUR:Lken;

    invoke-virtual {v6}, Lken;->bzH()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v5, Lkem;->eUR:Lken;

    iget-object v7, v7, Lken;->eUY:[Ljava/lang/String;

    array-length v7, v7

    if-nez v7, :cond_11

    move v2, v1

    :cond_11
    invoke-virtual {v3, v0, v4, v6, v2}, Lcpq;->a(I[BLjava/lang/String;Z)V

    :cond_12
    invoke-virtual {v5}, Lkem;->bzG()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcoh;->cx(Z)V

    goto/16 :goto_0

    :cond_13
    iget-object v4, p0, Lcoh;->mStaticContentCache:Lcpq;

    invoke-virtual {v4}, Lcpq;->PG()I

    move-result v4

    if-ge v0, v4, :cond_10

    iget-object v4, p0, Lcoh;->mStaticContentCache:Lcpq;

    invoke-virtual {v4}, Lcpq;->PH()V

    goto :goto_6

    .line 178
    :cond_14
    invoke-virtual {v5}, Lkem;->bzz()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 179
    iget-object v0, v5, Lkem;->eUQ:Lkel;

    if-eqz v0, :cond_15

    iget-object v0, v5, Lkem;->eUQ:Lkel;

    iget-object v0, v0, Lkel;->eUJ:Ljyw;

    if-eqz v0, :cond_15

    invoke-direct {p0, v5, v1}, Lcoh;->a(Lkem;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcoh;->bdG:Z

    goto/16 :goto_0

    :cond_15
    const-string v0, "PelletDemultiplexer"

    const-string v3, "EOC pellet had null card metadata. This is not expected!"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 180
    :cond_16
    iget-object v0, v5, Lkem;->eUQ:Lkel;

    if-nez v0, :cond_17

    .line 181
    const-string v0, "PelletDemultiplexer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown pellet type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v7, v0, v3, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 182
    :cond_17
    iget-object v0, v5, Lkem;->eUQ:Lkel;

    iget-object v0, v0, Lkel;->eUG:Ljrp;

    if-eqz v0, :cond_18

    .line 183
    iget-object v0, p0, Lcoh;->mExtrasConsumer:Lcoi;

    iget-object v3, v5, Lkem;->eUQ:Lkel;

    iget-object v3, v3, Lkel;->eUG:Ljrp;

    iget-object v4, v5, Lkem;->eUQ:Lkel;

    iget-object v4, v4, Lkel;->eUJ:Ljyw;

    invoke-virtual {v5}, Lkem;->aoM()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5, v2}, Lcom/google/android/velvet/ActionData;->a(Ljrp;Ljyw;Ljava/lang/String;Z)Lcom/google/android/velvet/ActionData;

    move-result-object v2

    invoke-interface {v0, v2}, Lcoi;->a(Lcom/google/android/velvet/ActionData;)V

    goto/16 :goto_0

    .line 184
    :cond_18
    iget-object v0, v5, Lkem;->eUQ:Lkel;

    iget-object v0, v0, Lkel;->eUJ:Ljyw;

    if-eqz v0, :cond_19

    .line 187
    iget-boolean v0, p0, Lcoh;->bdG:Z

    invoke-direct {p0, v5, v0}, Lcoh;->a(Lkem;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PelletDemultiplexer"

    const-string v3, "MDP proto expects continuation. This is not expected!"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v7, v0, v3, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 189
    :cond_19
    const-string v0, "PelletDemultiplexer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pellet with action but no action data: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v7, v0, v3, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public final synthetic aj(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 38
    check-cast p1, Lcog;

    invoke-virtual {p0, p1}, Lcoh;->a(Lcog;)Z

    move-result v0

    return v0
.end method
