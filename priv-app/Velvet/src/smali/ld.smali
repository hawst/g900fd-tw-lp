.class public final Lld;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final DEBUG:Z

.field static kt:Llh;


# instance fields
.field final ku:Ljava/util/ArrayList;

.field final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 70
    const-string v0, "MediaRouter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lld;->DEBUG:Z

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lld;->ku:Ljava/util/ArrayList;

    .line 193
    iput-object p1, p0, Lld;->mContext:Landroid/content/Context;

    .line 194
    return-void
.end method

.method public static a(Llm;)V
    .locals 2

    .prologue
    .line 345
    if-nez p0, :cond_0

    .line 346
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "route must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 348
    :cond_0
    invoke-static {}, Lld;->bN()V

    .line 350
    sget-boolean v0, Lld;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 351
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "selectRoute: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 353
    :cond_1
    sget-object v0, Lld;->kt:Llh;

    invoke-virtual {v0, p0}, Llh;->a(Llm;)V

    .line 354
    return-void
.end method

.method private b(Lle;)I
    .locals 3

    .prologue
    .line 559
    iget-object v0, p0, Lld;->ku:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 560
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 561
    iget-object v0, p0, Lld;->ku:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iget-object v0, v0, Llf;->kv:Lle;

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 565
    :goto_1
    return v0

    .line 560
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 565
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 684
    if-eq p0, p1, :cond_0

    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static bL()Llm;
    .locals 1

    .prologue
    .line 254
    invoke-static {}, Lld;->bN()V

    .line 255
    sget-object v0, Lld;->kt:Llh;

    invoke-virtual {v0}, Llh;->bL()Llm;

    move-result-object v0

    return-object v0
.end method

.method public static bM()Llm;
    .locals 1

    .prologue
    .line 305
    invoke-static {}, Lld;->bN()V

    .line 306
    sget-object v0, Lld;->kt:Llh;

    invoke-virtual {v0}, Llh;->bM()Llm;

    move-result-object v0

    return-object v0
.end method

.method static bN()V
    .locals 2

    .prologue
    .line 677
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 678
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The media router service must only be accessed on the application\'s main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 681
    :cond_0
    return-void
.end method

.method static synthetic bO()Z
    .locals 1

    .prologue
    .line 68
    sget-boolean v0, Lld;->DEBUG:Z

    return v0
.end method

.method public static d(Landroid/content/Context;)Lld;
    .locals 6

    .prologue
    .line 214
    if-nez p0, :cond_0

    .line 215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 217
    :cond_0
    invoke-static {}, Lld;->bN()V

    .line 219
    sget-object v0, Lld;->kt:Llh;

    if-nez v0, :cond_1

    .line 220
    new-instance v0, Llh;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Llh;-><init>(Landroid/content/Context;)V

    .line 221
    sput-object v0, Lld;->kt:Llh;

    new-instance v1, Lmi;

    iget-object v2, v0, Llh;->mApplicationContext:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lmi;-><init>(Landroid/content/Context;Lml;)V

    iput-object v1, v0, Llh;->kE:Lmi;

    iget-object v0, v0, Llh;->kE:Lmi;

    iget-boolean v1, v0, Lmi;->lD:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, v0, Lmi;->lD:Z

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.PACKAGE_RESTARTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "package"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v2, v0, Lmi;->mContext:Landroid/content/Context;

    iget-object v3, v0, Lmi;->lE:Landroid/content/BroadcastReceiver;

    const/4 v4, 0x0

    iget-object v5, v0, Lmi;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    iget-object v1, v0, Lmi;->mHandler:Landroid/os/Handler;

    iget-object v0, v0, Lmi;->lF:Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 223
    :cond_1
    sget-object v0, Lld;->kt:Llh;

    invoke-virtual {v0, p0}, Llh;->e(Landroid/content/Context;)Lld;

    move-result-object v0

    return-object v0
.end method

.method public static getRoutes()Ljava/util/List;
    .locals 1

    .prologue
    .line 231
    invoke-static {}, Lld;->bN()V

    .line 232
    sget-object v0, Lld;->kt:Llh;

    iget-object v0, v0, Llh;->kp:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public final a(Llb;Lle;I)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 497
    if-nez p1, :cond_0

    .line 498
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 500
    :cond_0
    if-nez p2, :cond_1

    .line 501
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 503
    :cond_1
    invoke-static {}, Lld;->bN()V

    .line 505
    sget-boolean v0, Lld;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 506
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "addCallback: selector="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", callback="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 511
    :cond_2
    invoke-direct {p0, p2}, Lld;->b(Lle;)I

    move-result v0

    .line 512
    if-gez v0, :cond_5

    .line 513
    new-instance v0, Llf;

    invoke-direct {v0, p0, p2}, Llf;-><init>(Lld;Lle;)V

    .line 514
    iget-object v1, p0, Lld;->ku:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 518
    :goto_0
    const/4 v1, 0x0

    .line 519
    iget v3, v0, Llf;->cZ:I

    xor-int/lit8 v3, v3, -0x1

    and-int/2addr v3, p3

    if-eqz v3, :cond_3

    .line 520
    iget v1, v0, Llf;->cZ:I

    or-int/2addr v1, p3

    iput v1, v0, Llf;->cZ:I

    move v1, v2

    .line 523
    :cond_3
    iget-object v3, v0, Llf;->jv:Llb;

    invoke-virtual {v3, p1}, Llb;->a(Llb;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 524
    new-instance v1, Llc;

    iget-object v3, v0, Llf;->jv:Llb;

    invoke-direct {v1, v3}, Llc;-><init>(Llb;)V

    invoke-virtual {v1, p1}, Llc;->d(Llb;)Llc;

    move-result-object v1

    invoke-virtual {v1}, Llc;->bK()Llb;

    move-result-object v1

    iput-object v1, v0, Llf;->jv:Llb;

    .line 529
    :goto_1
    if-eqz v2, :cond_4

    .line 530
    sget-object v0, Lld;->kt:Llh;

    invoke-virtual {v0}, Llh;->bP()V

    .line 532
    :cond_4
    return-void

    .line 516
    :cond_5
    iget-object v1, p0, Lld;->ku:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    goto :goto_0

    :cond_6
    move v2, v1

    goto :goto_1
.end method

.method public final a(Lle;)V
    .locals 2

    .prologue
    .line 542
    if-nez p1, :cond_0

    .line 543
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 545
    :cond_0
    invoke-static {}, Lld;->bN()V

    .line 547
    sget-boolean v0, Lld;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 548
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "removeCallback: callback="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 551
    :cond_1
    invoke-direct {p0, p1}, Lld;->b(Lle;)I

    move-result v0

    .line 552
    if-ltz v0, :cond_2

    .line 553
    iget-object v1, p0, Lld;->ku:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 554
    sget-object v0, Lld;->kt:Llh;

    invoke-virtual {v0}, Llh;->bP()V

    .line 556
    :cond_2
    return-void
.end method
