.class final Leji;
.super Landroid/animation/ValueAnimator;
.source "PG"


# instance fields
.field private final cbA:I

.field private final cbB:I

.field private cbC:Z

.field private synthetic cbD:Lejg;


# direct methods
.method public constructor <init>(Lejg;)V
    .locals 2

    .prologue
    .line 280
    iput-object p1, p0, Leji;->cbD:Lejg;

    invoke-direct {p0}, Landroid/animation/ValueAnimator;-><init>()V

    .line 281
    iget v0, p1, Lejg;->cbx:I

    iput v0, p0, Leji;->cbA:I

    .line 282
    iget v0, p1, Lejg;->cby:I

    iput v0, p0, Leji;->cbB:I

    .line 283
    new-instance v0, Lejj;

    invoke-direct {v0, p0, p1}, Lejj;-><init>(Leji;Lejg;)V

    invoke-virtual {p0, v0}, Leji;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 289
    new-instance v0, Lejk;

    invoke-direct {v0, p0, p1}, Lejk;-><init>(Leji;Lejg;)V

    invoke-virtual {p0, v0}, Leji;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 300
    iget v0, p1, Lejg;->cbt:I

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Leji;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 301
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Leji;->setFloatValues([F)V

    .line 302
    return-void

    .line 301
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method


# virtual methods
.method final afD()V
    .locals 3

    .prologue
    .line 305
    invoke-virtual {p0}, Leji;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 306
    iget-object v1, p0, Leji;->cbD:Lejg;

    iget-object v1, v1, Lejg;->mScrollView:Lekf;

    iget-object v2, p0, Leji;->cbD:Lejg;

    iget-object v2, v2, Lejg;->mLayout:Landroid/view/ViewGroup;

    invoke-interface {v1, v2}, Lekf;->aH(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Leji;->cbD:Lejg;

    iget-object v2, v2, Lejg;->mCallback:Lejh;

    invoke-interface {v2}, Lejh;->atw()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Leji;->cbD:Lejg;

    iget-object v2, v2, Lejg;->mCallback:Lejh;

    invoke-interface {v2}, Lejh;->atx()I

    move-result v2

    add-int/2addr v1, v2

    .line 308
    iget-object v2, p0, Leji;->cbD:Lejg;

    invoke-virtual {v2}, Lejg;->atv()I

    move-result v2

    sub-int/2addr v1, v2

    .line 309
    iget-object v2, p0, Leji;->cbD:Lejg;

    iget v2, v2, Lejg;->cbx:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    int-to-float v1, v1

    div-float v1, v2, v1

    .line 310
    iget-object v2, p0, Leji;->cbD:Lejg;

    iget-object v2, v2, Lejg;->mCallback:Lejh;

    invoke-interface {v2, v1}, Lejh;->J(F)V

    .line 313
    iget-object v1, p0, Leji;->cbD:Lejg;

    iget v2, p0, Leji;->cbA:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    float-to-int v2, v2

    iput v2, v1, Lejg;->cbx:I

    .line 314
    iget-object v1, p0, Leji;->cbD:Lejg;

    iget v2, p0, Leji;->cbB:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, v1, Lejg;->cby:I

    .line 316
    iget-object v0, p0, Leji;->cbD:Lejg;

    iget-object v0, v0, Lejg;->mLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->postInvalidateOnAnimation()V

    .line 317
    return-void
.end method

.method final afE()V
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Leji;->cbD:Lejg;

    const/4 v1, 0x0

    iput-object v1, v0, Lejg;->cbz:Landroid/animation/ValueAnimator;

    .line 325
    iget-boolean v0, p0, Leji;->cbC:Z

    if-nez v0, :cond_0

    .line 326
    iget-object v0, p0, Leji;->cbD:Lejg;

    iget-object v0, v0, Lejg;->mCallback:Lejh;

    invoke-interface {v0}, Lejh;->atA()V

    .line 327
    iget-object v0, p0, Leji;->cbD:Lejg;

    invoke-virtual {v0}, Lejg;->reset()V

    .line 329
    :cond_0
    return-void
.end method

.method final onCancel()V
    .locals 1

    .prologue
    .line 320
    const/4 v0, 0x1

    iput-boolean v0, p0, Leji;->cbC:Z

    .line 321
    return-void
.end method
