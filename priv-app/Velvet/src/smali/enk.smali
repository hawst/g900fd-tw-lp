.class public final Lenk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/util/Printer;
.implements Leti;


# instance fields
.field private final cfZ:[Lenl;

.field private cga:I

.field private cgb:J

.field private cgc:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0xc8

    const/4 v0, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-array v1, v3, [Lenl;

    iput-object v1, p0, Lenk;->cfZ:[Lenl;

    .line 60
    iput v0, p0, Lenk;->cga:I

    .line 81
    :goto_0
    if-ge v0, v3, :cond_0

    .line 82
    iget-object v1, p0, Lenk;->cfZ:[Lenl;

    new-instance v2, Lenl;

    invoke-direct {v2}, Lenl;-><init>()V

    aput-object v2, v1, v0

    .line 81
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 84
    :cond_0
    return-void
.end method

.method private static aT(J)Ljava/lang/String;
    .locals 6

    .prologue
    .line 254
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%8.03fms"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    long-to-float v4, p0

    const v5, 0x49742400    # 1000000.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static c(JLjava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 190
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0, p1}, Lenk;->aT(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Lenk;->li(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static li(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 199
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x3c

    if-ne v0, v1, :cond_1

    .line 206
    const/16 v2, 0x1a

    .line 207
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 208
    const-string v0, ""

    move-object v6, v0

    move v0, v1

    move-object v1, v6

    .line 219
    :goto_0
    const/16 v3, 0x20

    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 220
    const/16 v4, 0x20

    add-int/lit8 v5, v3, 0x1

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    .line 221
    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 222
    add-int/lit8 v3, v4, 0x1

    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 224
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "(android.view.Choreographer$FrameHandler)"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v2, "android.view.Choreographer"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v0, "(FrameHandler) Choreographer:"

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 229
    :cond_0
    :goto_2
    return-object p0

    .line 209
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x3e

    if-ne v0, v1, :cond_0

    .line 210
    const/16 v2, 0x1d

    .line 211
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 213
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    move v0, v1

    move-object v1, v6

    goto :goto_0

    .line 224
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "(FrameHandler) "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    const-string v4, "(android.app.ActivityThread$H)"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "(ActivityThread) "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    const-string v4, "(android.os.Handler)"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "(Handler) "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_1

    .line 225
    :catch_0
    move-exception v0

    .line 226
    const-string v1, "DebugPrinter"

    const-string v2, "Problem parsing UI task name"

    invoke-static {v1, v2, v0}, Leor;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2
.end method


# virtual methods
.method public final a(Letj;)V
    .locals 12

    .prologue
    const/16 v4, 0xc8

    const/4 v1, 0x0

    .line 127
    const-string v0, "All UI-thread tasks"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 128
    const-string v0, "Name in parentheses is the Handler. After that is the Runnable, which may be null. Finally, is the message number or 0"

    invoke-virtual {p1, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 131
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v0

    const-string v2, "Executing task"

    invoke-virtual {v0, v2}, Letj;->lt(Ljava/lang/String;)V

    iget-object v2, p0, Lenk;->cgc:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iget-wide v6, p0, Lenk;->cgb:J

    sub-long/2addr v2, v6

    iget-object v5, p0, Lenk;->cgc:Ljava/lang/String;

    invoke-static {v2, v3, v5}, Lenk;->c(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Letj;->lu(Ljava/lang/String;)V

    .line 133
    :goto_0
    const-string v0, "Queued tasks: not shown"

    invoke-virtual {p1, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 134
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " of at least "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/32 v2, 0x1e8480

    invoke-static {v2, v3}, Lenk;->aT(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Finished tasks"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Letj;->lt(Ljava/lang/String;)V

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lenk;->cfZ:[Lenl;

    iget v5, p0, Lenk;->cga:I

    aget-object v0, v0, v5

    iget-object v0, v0, Lenl;->bMP:Ljava/lang/String;

    if-nez v0, :cond_2

    iget v0, p0, Lenk;->cga:I

    move v6, v1

    :goto_1
    add-int/lit8 v5, v0, -0x1

    if-eqz v0, :cond_3

    iget-object v0, p0, Lenk;->cfZ:[Lenl;

    aget-object v8, v0, v6

    add-int/lit8 v0, v6, 0x1

    if-ne v0, v4, :cond_0

    move v0, v1

    :cond_0
    iget-wide v10, v8, Lenl;->cgd:J

    add-long/2addr v2, v10

    iget-wide v10, v8, Lenl;->cgd:J

    iget-object v6, v8, Lenl;->bMP:Ljava/lang/String;

    invoke-static {v10, v11, v6}, Lenk;->c(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Letj;->lu(Ljava/lang/String;)V

    move v6, v0

    move v0, v5

    goto :goto_1

    .line 131
    :cond_1
    const-string v2, "None"

    invoke-virtual {v0, v2}, Letj;->lv(Ljava/lang/String;)V

    goto :goto_0

    .line 134
    :cond_2
    iget v0, p0, Lenk;->cga:I

    move v6, v0

    move v0, v4

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Total time: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Lenk;->aT(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 135
    return-void
.end method

.method public final println(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 92
    if-eqz p1, :cond_2

    .line 93
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 97
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x3c

    if-ne v2, v3, :cond_3

    .line 98
    iget-wide v2, p0, Lenk;->cgb:J

    sub-long/2addr v0, v2

    .line 101
    const-wide/32 v2, 0x1e8480

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 102
    iget-object v2, p0, Lenk;->cfZ:[Lenl;

    iget v3, p0, Lenk;->cga:I

    aget-object v2, v2, v3

    iget-object v3, p0, Lenk;->cgc:Ljava/lang/String;

    iput-wide v0, v2, Lenl;->cgd:J

    iput-object v3, v2, Lenl;->bMP:Ljava/lang/String;

    .line 103
    iget v2, p0, Lenk;->cga:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lenk;->cga:I

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_0

    .line 104
    iput v5, p0, Lenk;->cga:I

    .line 109
    :cond_0
    const-wide/32 v2, 0x5f5e0fc

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 110
    const-string v2, "DebugPrinter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Long-running UI-thread task, "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lenk;->aT(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lenk;->li(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v2, v0, v1}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 114
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lenk;->cgc:Ljava/lang/String;

    .line 120
    :cond_2
    :goto_0
    return-void

    .line 116
    :cond_3
    iput-wide v0, p0, Lenk;->cgb:J

    .line 117
    iput-object p1, p0, Lenk;->cgc:Ljava/lang/String;

    goto :goto_0
.end method
