.class final enum Ldln;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum bBr:Ldln;

.field public static final enum bBs:Ldln;

.field public static final enum bBt:Ldln;

.field public static final enum bBu:Ldln;

.field public static final enum bBv:Ldln;

.field public static final enum bBw:Ldln;

.field public static final enum bBx:Ldln;

.field public static final enum bBy:Ldln;

.field private static final synthetic bBz:[Ldln;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    new-instance v0, Ldln;

    const-string v1, "EMPTY_ARRAY"

    invoke-direct {v0, v1, v3}, Ldln;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldln;->bBr:Ldln;

    .line 36
    new-instance v0, Ldln;

    const-string v1, "NONEMPTY_ARRAY"

    invoke-direct {v0, v1, v4}, Ldln;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldln;->bBs:Ldln;

    .line 42
    new-instance v0, Ldln;

    const-string v1, "EMPTY_OBJECT"

    invoke-direct {v0, v1, v5}, Ldln;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldln;->bBt:Ldln;

    .line 48
    new-instance v0, Ldln;

    const-string v1, "DANGLING_NAME"

    invoke-direct {v0, v1, v6}, Ldln;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldln;->bBu:Ldln;

    .line 54
    new-instance v0, Ldln;

    const-string v1, "NONEMPTY_OBJECT"

    invoke-direct {v0, v1, v7}, Ldln;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldln;->bBv:Ldln;

    .line 59
    new-instance v0, Ldln;

    const-string v1, "EMPTY_DOCUMENT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Ldln;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldln;->bBw:Ldln;

    .line 64
    new-instance v0, Ldln;

    const-string v1, "NONEMPTY_DOCUMENT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Ldln;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldln;->bBx:Ldln;

    .line 69
    new-instance v0, Ldln;

    const-string v1, "CLOSED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Ldln;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldln;->bBy:Ldln;

    .line 24
    const/16 v0, 0x8

    new-array v0, v0, [Ldln;

    sget-object v1, Ldln;->bBr:Ldln;

    aput-object v1, v0, v3

    sget-object v1, Ldln;->bBs:Ldln;

    aput-object v1, v0, v4

    sget-object v1, Ldln;->bBt:Ldln;

    aput-object v1, v0, v5

    sget-object v1, Ldln;->bBu:Ldln;

    aput-object v1, v0, v6

    sget-object v1, Ldln;->bBv:Ldln;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Ldln;->bBw:Ldln;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ldln;->bBx:Ldln;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Ldln;->bBy:Ldln;

    aput-object v2, v0, v1

    sput-object v0, Ldln;->bBz:[Ldln;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldln;
    .locals 1

    .prologue
    .line 24
    const-class v0, Ldln;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldln;

    return-object v0
.end method

.method public static values()[Ldln;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Ldln;->bBz:[Ldln;

    invoke-virtual {v0}, [Ldln;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldln;

    return-object v0
.end method
