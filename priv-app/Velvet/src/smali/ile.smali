.class public final Lile;
.super Ligu;
.source "PG"


# static fields
.field private static final serialVersionUID:J


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 79
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-direct {p0, v0}, Ligu;-><init>(Ljava/util/Map;)V

    .line 80
    return-void
.end method

.method public static aXj()Lile;
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lile;

    invoke-direct {v0}, Lile;-><init>()V

    return-object v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 3

    .prologue
    .line 100
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 101
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    .line 102
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-static {v0}, Lior;->ml(I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    iput-object v1, p0, Ligu;->dBO:Ljava/util/Map;

    .line 104
    invoke-static {p0, p1, v0}, Liqq;->a(Lipn;Ljava/io/ObjectInputStream;I)V

    .line 105
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 0

    .prologue
    .line 93
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 94
    invoke-static {p0, p1}, Liqq;->a(Lipn;Ljava/io/ObjectOutputStream;)V

    .line 95
    return-void
.end method


# virtual methods
.method public final bridge synthetic aWA()Ljava/util/Set;
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Ligu;->aWA()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic add(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 39
    invoke-super {p0, p1}, Ligu;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic addAll(Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 39
    invoke-super {p0, p1}, Ligu;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic bj(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 39
    invoke-super {p0, p1}, Ligu;->bj(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic clear()V
    .locals 0

    .prologue
    .line 39
    invoke-super {p0}, Ligu;->clear()V

    return-void
.end method

.method public final bridge synthetic contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 39
    invoke-super {p0, p1}, Ligu;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Ligu;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 39
    invoke-super {p0, p1}, Ligu;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic hashCode()I
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Ligu;->hashCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic i(Ljava/lang/Object;I)I
    .locals 1

    .prologue
    .line 39
    invoke-super {p0, p1, p2}, Ligu;->i(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic isEmpty()Z
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Ligu;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Ligu;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic j(Ljava/lang/Object;I)I
    .locals 1

    .prologue
    .line 39
    invoke-super {p0, p1, p2}, Ligu;->j(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 39
    invoke-super {p0, p1}, Ligu;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic removeAll(Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 39
    invoke-super {p0, p1}, Ligu;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic retainAll(Ljava/util/Collection;)Z
    .locals 1

    .prologue
    .line 39
    invoke-super {p0, p1}, Ligu;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic size()I
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Ligu;->size()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Ligu;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
