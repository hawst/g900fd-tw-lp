.class public final Ljmq;
.super Ljsl;
.source "PG"


# static fields
.field public static final eul:Ljsm;


# instance fields
.field private aez:I

.field public erP:Ljkh;

.field private eum:Ljava/lang/String;

.field private eun:Ljsc;

.field private euo:Ljkg;

.field private eup:I

.field private euq:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 8182
    const/16 v0, 0xb

    const-class v1, Ljmq;

    const v2, 0xc26f212

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljmq;->eul:Ljsm;

    .line 8188
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 8264
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 8265
    iput v2, p0, Ljmq;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljmq;->eum:Ljava/lang/String;

    iput-object v1, p0, Ljmq;->eun:Ljsc;

    iput-object v1, p0, Ljmq;->erP:Ljkh;

    iput-object v1, p0, Ljmq;->euo:Ljkg;

    iput v2, p0, Ljmq;->eup:I

    iput-boolean v2, p0, Ljmq;->euq:Z

    iput-object v1, p0, Ljmq;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljmq;->eCz:I

    .line 8266
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8175
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljmq;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmq;->eum:Ljava/lang/String;

    iget v0, p0, Ljmq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmq;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljmq;->eun:Ljsc;

    if-nez v0, :cond_1

    new-instance v0, Ljsc;

    invoke-direct {v0}, Ljsc;-><init>()V

    iput-object v0, p0, Ljmq;->eun:Ljsc;

    :cond_1
    iget-object v0, p0, Ljmq;->eun:Ljsc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljmq;->erP:Ljkh;

    if-nez v0, :cond_2

    new-instance v0, Ljkh;

    invoke-direct {v0}, Ljkh;-><init>()V

    iput-object v0, p0, Ljmq;->erP:Ljkh;

    :cond_2
    iget-object v0, p0, Ljmq;->erP:Ljkh;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljmq;->euo:Ljkg;

    if-nez v0, :cond_3

    new-instance v0, Ljkg;

    invoke-direct {v0}, Ljkg;-><init>()V

    iput-object v0, p0, Ljmq;->euo:Ljkg;

    :cond_3
    iget-object v0, p0, Ljmq;->euo:Ljkg;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljmq;->eup:I

    iget v0, p0, Ljmq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmq;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljmq;->euq:Z

    iget v0, p0, Ljmq;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljmq;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 8284
    iget v0, p0, Ljmq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 8285
    const/4 v0, 0x1

    iget-object v1, p0, Ljmq;->eum:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 8287
    :cond_0
    iget-object v0, p0, Ljmq;->eun:Ljsc;

    if-eqz v0, :cond_1

    .line 8288
    const/4 v0, 0x2

    iget-object v1, p0, Ljmq;->eun:Ljsc;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 8290
    :cond_1
    iget-object v0, p0, Ljmq;->erP:Ljkh;

    if-eqz v0, :cond_2

    .line 8291
    const/4 v0, 0x3

    iget-object v1, p0, Ljmq;->erP:Ljkh;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 8293
    :cond_2
    iget-object v0, p0, Ljmq;->euo:Ljkg;

    if-eqz v0, :cond_3

    .line 8294
    const/4 v0, 0x4

    iget-object v1, p0, Ljmq;->euo:Ljkg;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 8296
    :cond_3
    iget v0, p0, Ljmq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    .line 8297
    const/4 v0, 0x5

    iget v1, p0, Ljmq;->eup:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 8299
    :cond_4
    iget v0, p0, Ljmq;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    .line 8300
    const/4 v0, 0x6

    iget-boolean v1, p0, Ljmq;->euq:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 8302
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 8303
    return-void
.end method

.method public final bqn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8198
    iget-object v0, p0, Ljmq;->eum:Ljava/lang/String;

    return-object v0
.end method

.method public final bqo()I
    .locals 1

    .prologue
    .line 8229
    iget v0, p0, Ljmq;->eup:I

    return v0
.end method

.method public final bqp()Z
    .locals 1

    .prologue
    .line 8237
    iget v0, p0, Ljmq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bqq()Z
    .locals 1

    .prologue
    .line 8248
    iget-boolean v0, p0, Ljmq;->euq:Z

    return v0
.end method

.method public final iH(Z)Ljmq;
    .locals 1

    .prologue
    .line 8251
    iput-boolean p1, p0, Ljmq;->euq:Z

    .line 8252
    iget v0, p0, Ljmq;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljmq;->aez:I

    .line 8253
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 8307
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 8308
    iget v1, p0, Ljmq;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 8309
    const/4 v1, 0x1

    iget-object v2, p0, Ljmq;->eum:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8312
    :cond_0
    iget-object v1, p0, Ljmq;->eun:Ljsc;

    if-eqz v1, :cond_1

    .line 8313
    const/4 v1, 0x2

    iget-object v2, p0, Ljmq;->eun:Ljsc;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8316
    :cond_1
    iget-object v1, p0, Ljmq;->erP:Ljkh;

    if-eqz v1, :cond_2

    .line 8317
    const/4 v1, 0x3

    iget-object v2, p0, Ljmq;->erP:Ljkh;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8320
    :cond_2
    iget-object v1, p0, Ljmq;->euo:Ljkg;

    if-eqz v1, :cond_3

    .line 8321
    const/4 v1, 0x4

    iget-object v2, p0, Ljmq;->euo:Ljkg;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8324
    :cond_3
    iget v1, p0, Ljmq;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_4

    .line 8325
    const/4 v1, 0x5

    iget v2, p0, Ljmq;->eup:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8328
    :cond_4
    iget v1, p0, Ljmq;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    .line 8329
    const/4 v1, 0x6

    iget-boolean v2, p0, Ljmq;->euq:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8332
    :cond_5
    return v0
.end method

.method public final qO(I)Ljmq;
    .locals 1

    .prologue
    .line 8232
    iput p1, p0, Ljmq;->eup:I

    .line 8233
    iget v0, p0, Ljmq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmq;->aez:I

    .line 8234
    return-object p0
.end method

.method public final xi(Ljava/lang/String;)Ljmq;
    .locals 1

    .prologue
    .line 8201
    if-nez p1, :cond_0

    .line 8202
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8204
    :cond_0
    iput-object p1, p0, Ljmq;->eum:Ljava/lang/String;

    .line 8205
    iget v0, p0, Ljmq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmq;->aez:I

    .line 8206
    return-object p0
.end method
