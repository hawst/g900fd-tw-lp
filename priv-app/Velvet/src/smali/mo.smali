.class abstract Lmo;
.super Lkt;
.source "PG"


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 47
    new-instance v0, Lkw;

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "android"

    const-class v3, Lmo;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lkw;-><init>(Landroid/content/ComponentName;)V

    invoke-direct {p0, p1, v0}, Lkt;-><init>(Landroid/content/Context;Lkw;)V

    .line 49
    return-void
.end method

.method public static a(Landroid/content/Context;Lmy;)Lmo;
    .locals 2

    .prologue
    .line 52
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 53
    new-instance v0, Lmu;

    invoke-direct {v0, p0, p1}, Lmu;-><init>(Landroid/content/Context;Lmy;)V

    .line 61
    :goto_0
    return-object v0

    .line 55
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    .line 56
    new-instance v0, Lmt;

    invoke-direct {v0, p0, p1}, Lmt;-><init>(Landroid/content/Context;Lmy;)V

    goto :goto_0

    .line 58
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_2

    .line 59
    new-instance v0, Lmp;

    invoke-direct {v0, p0, p1}, Lmp;-><init>(Landroid/content/Context;Lmy;)V

    goto :goto_0

    .line 61
    :cond_2
    new-instance v0, Lmv;

    invoke-direct {v0, p0}, Lmv;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public d(Llm;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public e(Llm;)V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method public f(Llm;)V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public g(Llm;)V
    .locals 0

    .prologue
    .line 90
    return-void
.end method
