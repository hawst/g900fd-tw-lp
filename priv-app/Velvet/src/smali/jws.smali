.class public final Ljws;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eJx:[Ljws;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private dNw:Ljava/lang/String;

.field private eJA:J

.field private eJB:I

.field private eJC:Z

.field private eJy:Ljava/lang/String;

.field private eJz:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 378
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 379
    iput v2, p0, Ljws;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljws;->agq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljws;->eJy:Ljava/lang/String;

    iput-boolean v2, p0, Ljws;->eJz:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljws;->eJA:J

    const-string v0, ""

    iput-object v0, p0, Ljws;->dNw:Ljava/lang/String;

    iput v2, p0, Ljws;->eJB:I

    iput-boolean v2, p0, Ljws;->eJC:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljws;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljws;->eCz:I

    .line 380
    return-void
.end method

.method public static bvv()[Ljws;
    .locals 2

    .prologue
    .line 223
    sget-object v0, Ljws;->eJx:[Ljws;

    if-nez v0, :cond_1

    .line 224
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 226
    :try_start_0
    sget-object v0, Ljws;->eJx:[Ljws;

    if-nez v0, :cond_0

    .line 227
    const/4 v0, 0x0

    new-array v0, v0, [Ljws;

    sput-object v0, Ljws;->eJx:[Ljws;

    .line 229
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 231
    :cond_1
    sget-object v0, Ljws;->eJx:[Ljws;

    return-object v0

    .line 229
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 213
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljws;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljws;->agq:Ljava/lang/String;

    iget v0, p0, Ljws;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljws;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljws;->eJy:Ljava/lang/String;

    iget v0, p0, Ljws;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljws;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljws;->eJA:J

    iget v0, p0, Ljws;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljws;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljws;->eJB:I

    iget v0, p0, Ljws;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljws;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljws;->dNw:Ljava/lang/String;

    iget v0, p0, Ljws;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljws;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljws;->eJz:Z

    iget v0, p0, Ljws;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljws;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljws;->eJC:Z

    iget v0, p0, Ljws;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljws;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 399
    iget v0, p0, Ljws;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 400
    const/4 v0, 0x1

    iget-object v1, p0, Ljws;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 402
    :cond_0
    iget v0, p0, Ljws;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 403
    const/4 v0, 0x2

    iget-object v1, p0, Ljws;->eJy:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 405
    :cond_1
    iget v0, p0, Ljws;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    .line 406
    const/4 v0, 0x3

    iget-wide v2, p0, Ljws;->eJA:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 408
    :cond_2
    iget v0, p0, Ljws;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_3

    .line 409
    const/4 v0, 0x4

    iget v1, p0, Ljws;->eJB:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 411
    :cond_3
    iget v0, p0, Ljws;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 412
    const/4 v0, 0x5

    iget-object v1, p0, Ljws;->dNw:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 414
    :cond_4
    iget v0, p0, Ljws;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    .line 415
    const/4 v0, 0x7

    iget-boolean v1, p0, Ljws;->eJz:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 417
    :cond_5
    iget v0, p0, Ljws;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_6

    .line 418
    const/16 v0, 0x8

    iget-boolean v1, p0, Ljws;->eJC:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 420
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 421
    return-void
.end method

.method public final bvw()Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Ljws;->eJy:Ljava/lang/String;

    return-object v0
.end method

.method public final bvx()Z
    .locals 1

    .prologue
    .line 283
    iget-boolean v0, p0, Ljws;->eJz:Z

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Ljws;->agq:Ljava/lang/String;

    return-object v0
.end method

.method public final ji(Z)Ljws;
    .locals 1

    .prologue
    .line 286
    iput-boolean p1, p0, Ljws;->eJz:Z

    .line 287
    iget v0, p0, Ljws;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljws;->aez:I

    .line 288
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 425
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 426
    iget v1, p0, Ljws;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 427
    const/4 v1, 0x1

    iget-object v2, p0, Ljws;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 430
    :cond_0
    iget v1, p0, Ljws;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 431
    const/4 v1, 0x2

    iget-object v2, p0, Ljws;->eJy:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 434
    :cond_1
    iget v1, p0, Ljws;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    .line 435
    const/4 v1, 0x3

    iget-wide v2, p0, Ljws;->eJA:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 438
    :cond_2
    iget v1, p0, Ljws;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_3

    .line 439
    const/4 v1, 0x4

    iget v2, p0, Ljws;->eJB:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 442
    :cond_3
    iget v1, p0, Ljws;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 443
    const/4 v1, 0x5

    iget-object v2, p0, Ljws;->dNw:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 446
    :cond_4
    iget v1, p0, Ljws;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    .line 447
    const/4 v1, 0x7

    iget-boolean v2, p0, Ljws;->eJz:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 450
    :cond_5
    iget v1, p0, Ljws;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_6

    .line 451
    const/16 v1, 0x8

    iget-boolean v2, p0, Ljws;->eJC:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 454
    :cond_6
    return v0
.end method

.method public final zw(Ljava/lang/String;)Ljws;
    .locals 1

    .prologue
    .line 242
    if-nez p1, :cond_0

    .line 243
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 245
    :cond_0
    iput-object p1, p0, Ljws;->agq:Ljava/lang/String;

    .line 246
    iget v0, p0, Ljws;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljws;->aez:I

    .line 247
    return-object p0
.end method

.method public final zx(Ljava/lang/String;)Ljws;
    .locals 1

    .prologue
    .line 264
    if-nez p1, :cond_0

    .line 265
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 267
    :cond_0
    iput-object p1, p0, Ljws;->eJy:Ljava/lang/String;

    .line 268
    iget v0, p0, Ljws;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljws;->aez:I

    .line 269
    return-object p0
.end method
