.class final Lzx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic Nf:Lzi;

.field private synthetic Np:Landroid/content/Context;

.field final synthetic Nq:Laaf;

.field private synthetic Ny:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lzi;Landroid/content/Context;Ljava/util/ArrayList;Laaf;)V
    .locals 0

    .prologue
    .line 422
    iput-object p1, p0, Lzx;->Nf:Lzi;

    iput-object p2, p0, Lzx;->Np:Landroid/content/Context;

    iput-object p3, p0, Lzx;->Ny:Ljava/util/ArrayList;

    iput-object p4, p0, Lzx;->Nq:Laaf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    .line 424
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 425
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 426
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 431
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 432
    iget-object v0, p0, Lzx;->Np:Landroid/content/Context;

    invoke-static {v0}, Lzi;->u(Landroid/content/Context;)Ljava/util/TreeMap;

    move-result-object v1

    .line 433
    invoke-virtual {v1}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 434
    invoke-virtual {v1, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 435
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 438
    :cond_0
    sget-object v13, Lzi;->MV:Ljava/lang/Object;

    monitor-enter v13

    .line 439
    :try_start_0
    iget-object v0, p0, Lzx;->Ny:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 440
    :cond_1
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 441
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwq;

    .line 442
    iget-object v1, v0, Lwq;->title:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 443
    invoke-virtual {v0}, Lwq;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 446
    iget-object v3, p0, Lzx;->Np:Landroid/content/Context;

    invoke-static {v3, v1, v2}, Lzi;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 448
    instance-of v1, v0, Lrr;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lzx;->Np:Landroid/content/Context;

    iget-object v1, v0, Lwq;->Jl:Lahz;

    invoke-static {v2, v1}, Lzi;->b(Landroid/content/Intent;Lahz;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 450
    check-cast v0, Lrr;

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 502
    :catchall_0
    move-exception v0

    monitor-exit v13

    throw v0

    .line 458
    :cond_2
    :try_start_1
    invoke-virtual {v12}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    move v2, v1

    .line 459
    :goto_2
    iget-object v1, p0, Lzx;->Np:Landroid/content/Context;

    invoke-static {v1, v2, v12}, Lzi;->a(Landroid/content/Context;ILjava/util/ArrayList;)Landroid/util/Pair;

    move-result-object v1

    .line 461
    if-nez v1, :cond_a

    .line 462
    invoke-static {}, Lyu;->il()Lcom/android/launcher3/LauncherProvider;

    move-result-object v3

    .line 468
    const/4 v1, 0x1

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 470
    :goto_3
    if-lez v1, :cond_4

    .line 471
    invoke-virtual {v3}, Lcom/android/launcher3/LauncherProvider;->iL()J

    move-result-wide v4

    .line 473
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v12, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 474
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 475
    add-int/lit8 v1, v1, -0x1

    .line 476
    goto :goto_3

    .line 458
    :cond_3
    const/4 v1, 0x1

    move v2, v1

    goto :goto_2

    .line 479
    :cond_4
    iget-object v1, p0, Lzx;->Np:Landroid/content/Context;

    invoke-static {v1, v2, v12}, Lzi;->a(Landroid/content/Context;ILjava/util/ArrayList;)Landroid/util/Pair;

    move-result-object v1

    move-object v7, v1

    .line 482
    :goto_4
    if-nez v7, :cond_5

    .line 483
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Coordinates should not be null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 487
    :cond_5
    instance-of v1, v0, Ladh;

    if-eqz v1, :cond_6

    .line 488
    check-cast v0, Ladh;

    move-object v1, v0

    .line 496
    :goto_5
    iget-object v0, p0, Lzx;->Np:Landroid/content/Context;

    const-wide/16 v2, -0x64

    iget-object v4, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v6, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, [I

    const/4 v8, 0x0

    aget v6, v6, v8

    iget-object v7, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, [I

    const/4 v8, 0x1

    aget v7, v7, v8

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lzi;->a(Landroid/content/Context;Lwq;JJIIZ)V

    .line 500
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 489
    :cond_6
    instance-of v1, v0, Lrr;

    if-eqz v1, :cond_7

    .line 490
    check-cast v0, Lrr;

    invoke-virtual {v0}, Lrr;->dX()Ladh;

    move-result-object v1

    goto :goto_5

    .line 492
    :cond_7
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected info type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 502
    :cond_8
    monitor-exit v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 505
    iget-object v0, p0, Lzx;->Nf:Lzi;

    iget-object v1, p0, Lzx;->Np:Landroid/content/Context;

    invoke-virtual {v0, v1, v12}, Lzi;->d(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 507
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 508
    iget-object v0, p0, Lzx;->Nf:Lzi;

    new-instance v1, Lzy;

    invoke-direct {v1, p0, v9, v10, v11}, Lzy;-><init>(Lzx;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-static {v0, v1}, Lzi;->a(Lzi;Ljava/lang/Runnable;)V

    .line 534
    :cond_9
    return-void

    :cond_a
    move-object v7, v1

    goto :goto_4
.end method
