.class public final Lhqe;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lekh;


# instance fields
.field private synthetic dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lhqe;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final hZ(I)V
    .locals 4

    .prologue
    .line 192
    iget-object v0, p0, Lhqe;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lhqe;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnV:Lelg;

    invoke-virtual {v0, p1}, Lelg;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lelh;

    .line 195
    iget-boolean v1, v0, Lelh;->ceC:Z

    if-eqz v1, :cond_1

    .line 196
    iget-object v0, p0, Lhqe;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iget-object v2, v0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/SetReminderAction;->aio()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v2, v0, Lhpn;->dnE:Lhpq;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lhpn;->dnE:Lhpq;

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/16 v3, 0xc

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-interface {v0, v2, v1}, Lhpq;->Z(II)V

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    iget-object v1, v0, Lelh;->ceB:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 199
    iget-object v1, p0, Lhqe;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v1, v1, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    iget-object v0, v0, Lelh;->ceB:Ljava/lang/Object;

    check-cast v0, Ldyf;

    invoke-virtual {v1, v0}, Lhpn;->a(Ldyf;)V

    goto :goto_0
.end method
