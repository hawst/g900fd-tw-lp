.class final Lkcf;
.super Lkbv;
.source "PG"


# instance fields
.field private eRA:Z

.field private final eRy:Lkce;

.field private eRz:I


# direct methods
.method constructor <init>(Ljava/io/InputStream;Ljava/net/CacheRequest;Lkce;)V
    .locals 1

    .prologue
    .line 428
    iget-object v0, p3, Lkce;->eQU:Lkcc;

    invoke-direct {p0, p1, v0, p2}, Lkbv;-><init>(Ljava/io/InputStream;Lkcc;Ljava/net/CacheRequest;)V

    .line 423
    const/4 v0, -0x1

    iput v0, p0, Lkcf;->eRz:I

    .line 424
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkcf;->eRA:Z

    .line 429
    iput-object p3, p0, Lkcf;->eRy:Lkce;

    .line 430
    return-void
.end method


# virtual methods
.method public final available()I
    .locals 2

    .prologue
    .line 480
    invoke-virtual {p0}, Lkcf;->bxV()V

    .line 481
    iget-boolean v0, p0, Lkcf;->eRA:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lkcf;->eRz:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 482
    :cond_0
    const/4 v0, 0x0

    .line 484
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lkcf;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    iget v1, p0, Lkcf;->eRz:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 488
    iget-boolean v0, p0, Lkcf;->eQb:Z

    if-eqz v0, :cond_0

    .line 495
    :goto_0
    return-void

    .line 491
    :cond_0
    iget-boolean v0, p0, Lkcf;->eRA:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkcf;->eQU:Lkcc;

    invoke-static {v0, p0}, Lkce;->a(Lkcc;Ljava/io/InputStream;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 492
    invoke-virtual {p0}, Lkcf;->byj()V

    .line 494
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkcf;->eQb:Z

    goto :goto_0
.end method

.method public final read([BII)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, -0x1

    .line 433
    array-length v1, p1

    invoke-static {v1, p2, p3}, Lkbt;->D(III)V

    .line 434
    invoke-virtual {p0}, Lkcf;->bxV()V

    .line 436
    iget-boolean v1, p0, Lkcf;->eRA:Z

    if-nez v1, :cond_1

    .line 452
    :cond_0
    :goto_0
    return v0

    .line 439
    :cond_1
    iget v1, p0, Lkcf;->eRz:I

    if-eqz v1, :cond_2

    iget v1, p0, Lkcf;->eRz:I

    if-ne v1, v0, :cond_6

    .line 440
    :cond_2
    iget v1, p0, Lkcf;->eRz:I

    if-eq v1, v0, :cond_3

    iget-object v1, p0, Lkcf;->in:Ljava/io/InputStream;

    invoke-static {v1}, Lkbt;->p(Ljava/io/InputStream;)Ljava/lang/String;

    :cond_3
    iget-object v1, p0, Lkcf;->in:Ljava/io/InputStream;

    invoke-static {v1}, Lkbt;->p(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v0, :cond_4

    invoke-virtual {v1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :cond_4
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lkcf;->eRz:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    iget v1, p0, Lkcf;->eRz:I

    if-nez v1, :cond_5

    iput-boolean v4, p0, Lkcf;->eRA:Z

    iget-object v1, p0, Lkcf;->eQU:Lkcc;

    iget-object v1, v1, Lkcc;->eRq:Lkcu;

    invoke-virtual {v1}, Lkcu;->byX()Lkcq;

    move-result-object v1

    iget-object v2, p0, Lkcf;->eRy:Lkce;

    iget-object v2, v2, Lkce;->eRv:Ljava/io/InputStream;

    invoke-static {v2, v1}, Lkcq;->a(Ljava/io/InputStream;Lkcq;)V

    iget-object v2, p0, Lkcf;->eQU:Lkcc;

    invoke-virtual {v2, v1}, Lkcc;->a(Lkcq;)V

    invoke-virtual {p0}, Lkcf;->byi()V

    .line 441
    :cond_5
    iget-boolean v1, p0, Lkcf;->eRA:Z

    if-eqz v1, :cond_0

    .line 445
    :cond_6
    iget-object v1, p0, Lkcf;->in:Ljava/io/InputStream;

    iget v2, p0, Lkcf;->eRz:I

    invoke-static {p3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {v1, p1, p2, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 446
    if-ne v1, v0, :cond_7

    .line 447
    invoke-virtual {p0}, Lkcf;->byj()V

    .line 448
    new-instance v0, Ljava/io/IOException;

    const-string v1, "unexpected end of stream"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 440
    :catch_0
    move-exception v0

    new-instance v0, Ljava/net/ProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected a hex chunk size but was "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 450
    :cond_7
    iget v0, p0, Lkcf;->eRz:I

    sub-int/2addr v0, v1

    iput v0, p0, Lkcf;->eRz:I

    .line 451
    invoke-virtual {p0, p1, p2, v1}, Lkcf;->n([BII)V

    move v0, v1

    .line 452
    goto/16 :goto_0
.end method
