.class public final Ljot;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ewT:[Ljot;


# instance fields
.field private aez:I

.field private dHr:Ljava/lang/String;

.field private ecP:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 438
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 439
    const/4 v0, 0x0

    iput v0, p0, Ljot;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljot;->dHr:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Ljot;->ecP:F

    const/4 v0, 0x0

    iput-object v0, p0, Ljot;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljot;->eCz:I

    .line 440
    return-void
.end method

.method public static brC()[Ljot;
    .locals 2

    .prologue
    .line 384
    sget-object v0, Ljot;->ewT:[Ljot;

    if-nez v0, :cond_1

    .line 385
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 387
    :try_start_0
    sget-object v0, Ljot;->ewT:[Ljot;

    if-nez v0, :cond_0

    .line 388
    const/4 v0, 0x0

    new-array v0, v0, [Ljot;

    sput-object v0, Ljot;->ewT:[Ljot;

    .line 390
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 392
    :cond_1
    sget-object v0, Ljot;->ewT:[Ljot;

    return-object v0

    .line 390
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 378
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljot;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljot;->dHr:Ljava/lang/String;

    iget v0, p0, Ljot;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljot;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljot;->ecP:F

    iget v0, p0, Ljot;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljot;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 454
    iget v0, p0, Ljot;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 455
    const/4 v0, 0x1

    iget-object v1, p0, Ljot;->dHr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 457
    :cond_0
    iget v0, p0, Ljot;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 458
    const/4 v0, 0x2

    iget v1, p0, Ljot;->ecP:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 460
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 461
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 465
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 466
    iget v1, p0, Ljot;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 467
    const/4 v1, 0x1

    iget-object v2, p0, Ljot;->dHr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 470
    :cond_0
    iget v1, p0, Ljot;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 471
    const/4 v1, 0x2

    iget v2, p0, Ljot;->ecP:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 474
    :cond_1
    return v0
.end method
