.class public final Ldxk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 409
    new-instance v3, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    const-class v0, Ljqg;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljqg;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-direct {v3, v0, v1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;-><init>(Ljqg;Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    invoke-static {v3, v0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->a(Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;Z)Z

    return-object v3

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 409
    new-array v0, p1, [Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    return-object v0
.end method
