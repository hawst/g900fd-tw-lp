.class public final Ljpz;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eyc:Z

.field private eyd:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 462
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 463
    const/4 v0, 0x0

    iput v0, p0, Ljpz;->aez:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljpz;->eyc:Z

    const/16 v0, 0x1e

    iput v0, p0, Ljpz;->eyd:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljpz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljpz;->eCz:I

    .line 464
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 405
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljpz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljpz;->eyc:Z

    iget v0, p0, Ljpz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpz;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljpz;->eyd:I

    iget v0, p0, Ljpz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljpz;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 478
    iget v0, p0, Ljpz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 479
    const/4 v0, 0x1

    iget-boolean v1, p0, Ljpz;->eyc:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 481
    :cond_0
    iget v0, p0, Ljpz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 482
    const/4 v0, 0x2

    iget v1, p0, Ljpz;->eyd:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 484
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 485
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 489
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 490
    iget v1, p0, Ljpz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 491
    const/4 v1, 0x1

    iget-boolean v2, p0, Ljpz;->eyc:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 494
    :cond_0
    iget v1, p0, Ljpz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 495
    const/4 v1, 0x2

    iget v2, p0, Ljpz;->eyd:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 498
    :cond_1
    return v0
.end method
