.class public final Ljdg;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ebJ:[Ljdg;


# instance fields
.field private aez:I

.field public bnG:Ljfl;

.field private dHR:I

.field private dHr:Ljava/lang/String;

.field public ebK:Ljfh;

.field public ebL:Ljfi;

.field private ebM:Ljfj;

.field private ebN:Ljfo;

.field private ebO:Ljava/lang/String;

.field private ebP:Ljava/lang/String;

.field private ebQ:I

.field private ebR:Ljava/lang/String;

.field private ebS:Ljava/lang/String;

.field private ebT:Ljfz;

.field private ebU:Ljdh;

.field public ebV:Ljfs;

.field private ebW:Ljava/lang/String;

.field private ebX:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v2, p0, Ljdg;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljdg;->dHr:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Ljdg;->dHR:I

    iput-object v1, p0, Ljdg;->ebK:Ljfh;

    iput-object v1, p0, Ljdg;->bnG:Ljfl;

    iput-object v1, p0, Ljdg;->ebL:Ljfi;

    iput-object v1, p0, Ljdg;->ebM:Ljfj;

    iput-object v1, p0, Ljdg;->ebN:Ljfo;

    const-string v0, ""

    iput-object v0, p0, Ljdg;->ebO:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljdg;->ebP:Ljava/lang/String;

    iput v2, p0, Ljdg;->ebQ:I

    const-string v0, ""

    iput-object v0, p0, Ljdg;->ebR:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljdg;->ebS:Ljava/lang/String;

    iput-object v1, p0, Ljdg;->ebT:Ljfz;

    iput-object v1, p0, Ljdg;->ebU:Ljdh;

    iput-object v1, p0, Ljdg;->ebV:Ljfs;

    const-string v0, ""

    iput-object v0, p0, Ljdg;->ebW:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljdg;->ebX:Ljava/lang/String;

    iput-object v1, p0, Ljdg;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljdg;->eCz:I

    return-void
.end method

.method public static bhl()[Ljdg;
    .locals 2

    sget-object v0, Ljdg;->ebJ:[Ljdg;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljdg;->ebJ:[Ljdg;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljdg;

    sput-object v0, Ljdg;->ebJ:[Ljdg;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Ljdg;->ebJ:[Ljdg;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljdg;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdg;->dHr:Ljava/lang/String;

    iget v0, p0, Ljdg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdg;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljdg;->ebK:Ljfh;

    if-nez v0, :cond_1

    new-instance v0, Ljfh;

    invoke-direct {v0}, Ljfh;-><init>()V

    iput-object v0, p0, Ljdg;->ebK:Ljfh;

    :cond_1
    iget-object v0, p0, Ljdg;->ebK:Ljfh;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljdg;->bnG:Ljfl;

    if-nez v0, :cond_2

    new-instance v0, Ljfl;

    invoke-direct {v0}, Ljfl;-><init>()V

    iput-object v0, p0, Ljdg;->bnG:Ljfl;

    :cond_2
    iget-object v0, p0, Ljdg;->bnG:Ljfl;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljdg;->ebN:Ljfo;

    if-nez v0, :cond_3

    new-instance v0, Ljfo;

    invoke-direct {v0}, Ljfo;-><init>()V

    iput-object v0, p0, Ljdg;->ebN:Ljfo;

    :cond_3
    iget-object v0, p0, Ljdg;->ebN:Ljfo;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdg;->ebP:Ljava/lang/String;

    iget v0, p0, Ljdg;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljdg;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljdg;->dHR:I

    iget v0, p0, Ljdg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljdg;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljdg;->ebQ:I

    iget v0, p0, Ljdg;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljdg;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdg;->ebO:Ljava/lang/String;

    iget v0, p0, Ljdg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljdg;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdg;->ebR:Ljava/lang/String;

    iget v0, p0, Ljdg;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljdg;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdg;->ebS:Ljava/lang/String;

    iget v0, p0, Ljdg;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljdg;->aez:I

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Ljdg;->ebT:Ljfz;

    if-nez v0, :cond_4

    new-instance v0, Ljfz;

    invoke-direct {v0}, Ljfz;-><init>()V

    iput-object v0, p0, Ljdg;->ebT:Ljfz;

    :cond_4
    iget-object v0, p0, Ljdg;->ebT:Ljfz;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Ljdg;->ebU:Ljdh;

    if-nez v0, :cond_5

    new-instance v0, Ljdh;

    invoke-direct {v0}, Ljdh;-><init>()V

    iput-object v0, p0, Ljdg;->ebU:Ljdh;

    :cond_5
    iget-object v0, p0, Ljdg;->ebU:Ljdh;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Ljdg;->ebV:Ljfs;

    if-nez v0, :cond_6

    new-instance v0, Ljfs;

    invoke-direct {v0}, Ljfs;-><init>()V

    iput-object v0, p0, Ljdg;->ebV:Ljfs;

    :cond_6
    iget-object v0, p0, Ljdg;->ebV:Ljfs;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdg;->ebX:Ljava/lang/String;

    iget v0, p0, Ljdg;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljdg;->aez:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdg;->ebW:Ljava/lang/String;

    iget v0, p0, Ljdg;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljdg;->aez:I

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Ljdg;->ebL:Ljfi;

    if-nez v0, :cond_7

    new-instance v0, Ljfi;

    invoke-direct {v0}, Ljfi;-><init>()V

    iput-object v0, p0, Ljdg;->ebL:Ljfi;

    :cond_7
    iget-object v0, p0, Ljdg;->ebL:Ljfi;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_11
    iget-object v0, p0, Ljdg;->ebM:Ljfj;

    if-nez v0, :cond_8

    new-instance v0, Ljfj;

    invoke-direct {v0}, Ljfj;-><init>()V

    iput-object v0, p0, Ljdg;->ebM:Ljfj;

    :cond_8
    iget-object v0, p0, Ljdg;->ebM:Ljfj;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    iget v0, p0, Ljdg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljdg;->dHr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Ljdg;->ebK:Ljfh;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljdg;->ebK:Ljfh;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_1
    iget-object v0, p0, Ljdg;->bnG:Ljfl;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ljdg;->bnG:Ljfl;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_2
    iget-object v0, p0, Ljdg;->ebN:Ljfo;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ljdg;->ebN:Ljfo;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_3
    iget v0, p0, Ljdg;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Ljdg;->ebP:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_4
    iget v0, p0, Ljdg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_5

    const/4 v0, 0x7

    iget v1, p0, Ljdg;->dHR:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_5
    iget v0, p0, Ljdg;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    const/16 v0, 0x8

    iget v1, p0, Ljdg;->ebQ:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_6
    iget v0, p0, Ljdg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_7

    const/16 v0, 0x9

    iget-object v1, p0, Ljdg;->ebO:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_7
    iget v0, p0, Ljdg;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_8

    const/16 v0, 0xa

    iget-object v1, p0, Ljdg;->ebR:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_8
    iget v0, p0, Ljdg;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_9

    const/16 v0, 0xb

    iget-object v1, p0, Ljdg;->ebS:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_9
    iget-object v0, p0, Ljdg;->ebT:Ljfz;

    if-eqz v0, :cond_a

    const/16 v0, 0xc

    iget-object v1, p0, Ljdg;->ebT:Ljfz;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_a
    iget-object v0, p0, Ljdg;->ebU:Ljdh;

    if-eqz v0, :cond_b

    const/16 v0, 0xd

    iget-object v1, p0, Ljdg;->ebU:Ljdh;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_b
    iget-object v0, p0, Ljdg;->ebV:Ljfs;

    if-eqz v0, :cond_c

    const/16 v0, 0xe

    iget-object v1, p0, Ljdg;->ebV:Ljfs;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_c
    iget v0, p0, Ljdg;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_d

    const/16 v0, 0xf

    iget-object v1, p0, Ljdg;->ebX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_d
    iget v0, p0, Ljdg;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_e

    const/16 v0, 0x10

    iget-object v1, p0, Ljdg;->ebW:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_e
    iget-object v0, p0, Ljdg;->ebL:Ljfi;

    if-eqz v0, :cond_f

    const/16 v0, 0x11

    iget-object v1, p0, Ljdg;->ebL:Ljfi;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_f
    iget-object v0, p0, Ljdg;->ebM:Ljfj;

    if-eqz v0, :cond_10

    const/16 v0, 0x12

    iget-object v1, p0, Ljdg;->ebM:Ljfj;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_10
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final bhm()Ljdg;
    .locals 1

    const-string v0, ""

    iput-object v0, p0, Ljdg;->dHr:Ljava/lang/String;

    iget v0, p0, Ljdg;->aez:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Ljdg;->aez:I

    return-object p0
.end method

.method public final bhn()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljdg;->ebO:Ljava/lang/String;

    return-object v0
.end method

.method public final bho()Z
    .locals 1

    iget v0, p0, Ljdg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getSource()I
    .locals 1

    iget v0, p0, Ljdg;->dHR:I

    return v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljdg;->dHr:Ljava/lang/String;

    return-object v0
.end method

.method public final hasValue()Z
    .locals 1

    iget v0, p0, Ljdg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 3

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljdg;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljdg;->dHr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Ljdg;->ebK:Ljfh;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljdg;->ebK:Ljfh;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Ljdg;->bnG:Ljfl;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Ljdg;->bnG:Ljfl;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Ljdg;->ebN:Ljfo;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Ljdg;->ebN:Ljfo;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Ljdg;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Ljdg;->ebP:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Ljdg;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_5

    const/4 v1, 0x7

    iget v2, p0, Ljdg;->dHR:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Ljdg;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_6

    const/16 v1, 0x8

    iget v2, p0, Ljdg;->ebQ:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Ljdg;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_7

    const/16 v1, 0x9

    iget-object v2, p0, Ljdg;->ebO:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget v1, p0, Ljdg;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_8

    const/16 v1, 0xa

    iget-object v2, p0, Ljdg;->ebR:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget v1, p0, Ljdg;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_9

    const/16 v1, 0xb

    iget-object v2, p0, Ljdg;->ebS:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Ljdg;->ebT:Ljfz;

    if-eqz v1, :cond_a

    const/16 v1, 0xc

    iget-object v2, p0, Ljdg;->ebT:Ljfz;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, Ljdg;->ebU:Ljdh;

    if-eqz v1, :cond_b

    const/16 v1, 0xd

    iget-object v2, p0, Ljdg;->ebU:Ljdh;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Ljdg;->ebV:Ljfs;

    if-eqz v1, :cond_c

    const/16 v1, 0xe

    iget-object v2, p0, Ljdg;->ebV:Ljfs;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget v1, p0, Ljdg;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_d

    const/16 v1, 0xf

    iget-object v2, p0, Ljdg;->ebX:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget v1, p0, Ljdg;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_e

    const/16 v1, 0x10

    iget-object v2, p0, Ljdg;->ebW:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iget-object v1, p0, Ljdg;->ebL:Ljfi;

    if-eqz v1, :cond_f

    const/16 v1, 0x11

    iget-object v2, p0, Ljdg;->ebL:Ljfi;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iget-object v1, p0, Ljdg;->ebM:Ljfj;

    if-eqz v1, :cond_10

    const/16 v1, 0x12

    iget-object v2, p0, Ljdg;->ebM:Ljfj;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    return v0
.end method

.method public final oM(I)Ljdg;
    .locals 1

    const/4 v0, 0x2

    iput v0, p0, Ljdg;->dHR:I

    iget v0, p0, Ljdg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljdg;->aez:I

    return-object p0
.end method

.method public final tA(Ljava/lang/String;)Ljdg;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljdg;->ebO:Ljava/lang/String;

    iget v0, p0, Ljdg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljdg;->aez:I

    return-object p0
.end method

.method public final tB(Ljava/lang/String;)Ljdg;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljdg;->ebP:Ljava/lang/String;

    iget v0, p0, Ljdg;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljdg;->aez:I

    return-object p0
.end method

.method public final tC(Ljava/lang/String;)Ljdg;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljdg;->ebR:Ljava/lang/String;

    iget v0, p0, Ljdg;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljdg;->aez:I

    return-object p0
.end method

.method public final tD(Ljava/lang/String;)Ljdg;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljdg;->ebS:Ljava/lang/String;

    iget v0, p0, Ljdg;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljdg;->aez:I

    return-object p0
.end method

.method public final tz(Ljava/lang/String;)Ljdg;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljdg;->dHr:Ljava/lang/String;

    iget v0, p0, Ljdg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdg;->aez:I

    return-object p0
.end method
