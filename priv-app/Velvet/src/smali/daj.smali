.class final Ldaj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldex;
.implements Ldfk;
.implements Ldyl;
.implements Leqv;


# instance fields
.field private synthetic bpp:Ldah;

.field private final bpq:Lecm;


# direct methods
.method constructor <init>(Ldah;Lecm;)V
    .locals 3

    .prologue
    .line 546
    iput-object p1, p0, Ldaj;->bpp:Ldah;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 547
    iput-object p2, p0, Ldaj;->bpq:Lecm;

    .line 548
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v0}, Lecm;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p1, Ldah;->bpb:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 549
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 5

    .prologue
    .line 560
    const-string v0, "AttachedClient"

    const-string v1, "%s: failed callback %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Ldaj;->bpp:Ldah;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, p2, v1, v2}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 561
    iget-object v0, p0, Ldaj;->bpp:Ldah;

    iget-object v0, v0, Ldah;->mService:Lcom/google/android/search/core/service/SearchService;

    iget-object v1, p0, Ldaj;->bpp:Ldah;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/service/SearchService;->c(Ldah;)V

    .line 562
    return-void
.end method


# virtual methods
.method public final Bm()Lcom/google/android/search/shared/service/ClientConfig;
    .locals 1

    .prologue
    .line 737
    iget-object v0, p0, Ldaj;->bpp:Ldah;

    invoke-static {v0}, Ldah;->b(Ldah;)Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    return-object v0
.end method

.method public final CV()V
    .locals 2

    .prologue
    .line 620
    :try_start_0
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v0}, Lecm;->CV()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 624
    :goto_0
    return-void

    .line 621
    :catch_0
    move-exception v0

    .line 622
    const-string v1, "updateActionUI()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method final UJ()V
    .locals 3

    .prologue
    .line 552
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v0}, Lecm;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Ldaj;->bpp:Ldah;

    iget-object v1, v1, Ldah;->bpb:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 553
    return-void
.end method

.method public final UK()V
    .locals 2

    .prologue
    .line 719
    :try_start_0
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v0}, Lecm;->UK()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 723
    :goto_0
    return-void

    .line 720
    :catch_0
    move-exception v0

    .line 721
    const-string v1, "focusQueryAndShowKeyboard()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;Z)V
    .locals 2

    .prologue
    .line 665
    :try_start_0
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v0, p1, p2}, Lecm;->e(ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 669
    :goto_0
    return-void

    .line 666
    :catch_0
    move-exception v0

    .line 667
    const-string v1, "setExternalFlags()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/search/shared/actions/ParcelableVoiceAction;)V
    .locals 2

    .prologue
    .line 701
    :try_start_0
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v0, p1}, Lecm;->a(Lcom/google/android/search/shared/actions/ParcelableVoiceAction;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 705
    :goto_0
    return-void

    .line 702
    :catch_0
    move-exception v0

    .line 703
    const-string v1, "showError()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;)V
    .locals 2

    .prologue
    .line 586
    :try_start_0
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    new-instance v1, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;

    invoke-direct {v1, p2}, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;-><init>(Lcom/google/android/search/shared/actions/VoiceAction;)V

    invoke-interface {v0, p1, v1, p3}, Lecm;->a(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/ParcelableVoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 591
    :goto_0
    return-void

    .line 588
    :catch_0
    move-exception v0

    .line 589
    const-string v1, "showVoiceAction()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/shared/search/Query;[Ljava/lang/String;[F)V
    .locals 2

    .prologue
    .line 604
    :try_start_0
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v0, p1, p2, p3}, Lecm;->a(Lcom/google/android/shared/search/Query;[Ljava/lang/String;[F)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 608
    :goto_0
    return-void

    .line 605
    :catch_0
    move-exception v0

    .line 606
    const-string v1, "onTranscriptionUpdate()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/shared/speech/HotwordResult;)V
    .locals 2

    .prologue
    .line 612
    :try_start_0
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v0, p1}, Lecm;->a(Lcom/google/android/shared/speech/HotwordResult;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 616
    :goto_0
    return-void

    .line 613
    :catch_0
    move-exception v0

    .line 614
    const-string v1, "showHotwordTriggered()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final a(Ldem;)V
    .locals 1
    .param p1    # Ldem;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 729
    iget-object v0, p0, Ldaj;->bpp:Ldah;

    invoke-static {v0}, Ldah;->b(Ldah;)Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->anc()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 730
    iget-object v0, p0, Ldaj;->bpp:Ldah;

    invoke-virtual {v0}, Ldah;->UG()Ldep;

    move-result-object v0

    .line 732
    invoke-virtual {v0, p1}, Ldep;->b(Ldem;)V

    .line 733
    return-void
.end method

.method public final varargs a([Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 569
    :try_start_0
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v0, p1}, Lecm;->a([Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 573
    :goto_0
    return-void

    .line 570
    :catch_0
    move-exception v0

    .line 571
    const-string v1, "launchIntent()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final c(IIZ)V
    .locals 2

    .prologue
    .line 647
    :try_start_0
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v0, p1, p2, p3}, Lecm;->d(IIZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 651
    :goto_0
    return-void

    .line 648
    :catch_0
    move-exception v0

    .line 649
    const-string v1, "setSearchPlateMode()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final c(Landroid/util/SparseArray;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 746
    :try_start_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x1

    move v3, v1

    :goto_0
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    invoke-virtual {p1, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    invoke-virtual {p1, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    invoke-interface {v0}, Ldef;->abe()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    new-instance v6, Lcom/google/android/search/shared/api/SuggestionsGroup;

    invoke-interface {v0}, Ldef;->abe()Ljava/util/List;

    move-result-object v7

    invoke-direct {v6, v7, v5}, Lcom/google/android/search/shared/api/SuggestionsGroup;-><init>(Ljava/util/List;I)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-interface {v0}, Ldef;->abc()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    :cond_1
    if-nez v2, :cond_2

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v1, p0, Ldaj;->bpq:Lecm;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    invoke-interface {v0}, Ldef;->abb()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-static {p1}, Lcpd;->b(Landroid/util/SparseArray;)Lcom/google/android/shared/search/SuggestionLogInfo;

    move-result-object v3

    invoke-interface {v1, v0, v4, v2, v3}, Lecm;->a(Lcom/google/android/shared/search/Query;Ljava/util/List;ZLcom/google/android/shared/search/SuggestionLogInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 750
    :cond_3
    :goto_2
    return-void

    .line 747
    :catch_0
    move-exception v0

    .line 748
    const-string v1, "showSuggestions/hideSuggestions()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public final c(Ljyl;)V
    .locals 2

    .prologue
    .line 595
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 596
    :goto_0
    :try_start_0
    iget-object v1, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v1, v0}, Lecm;->j([B)V

    .line 600
    :goto_1
    return-void

    .line 595
    :cond_0
    invoke-static {p1}, Ljsr;->m(Ljsr;)[B
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 597
    :catch_0
    move-exception v0

    .line 598
    const-string v1, "showClockworkResult()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public final en(I)V
    .locals 2

    .prologue
    .line 674
    :try_start_0
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v0, p1}, Lecm;->en(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 678
    :goto_0
    return-void

    .line 675
    :catch_0
    move-exception v0

    .line 676
    const-string v1, "showRecognitionState()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final fv(I)V
    .locals 2

    .prologue
    .line 710
    :try_start_0
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v0, p1}, Lecm;->fv(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 714
    :goto_0
    return-void

    .line 711
    :catch_0
    move-exception v0

    .line 712
    const-string v1, "updateProgress()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final fw(I)V
    .locals 2

    .prologue
    .line 786
    :try_start_0
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v0, p1}, Lecm;->eo(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 790
    :goto_0
    return-void

    .line 787
    :catch_0
    move-exception v0

    .line 788
    const-string v1, "updateSpeechLevel()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final n(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 692
    :try_start_0
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lecm;->gb(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 696
    :goto_0
    return-void

    .line 693
    :catch_0
    move-exception v0

    .line 694
    const-string v1, "setFinalRecognizedText()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method final pingBinder()Z
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v0}, Lecm;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v0}, Landroid/os/IBinder;->pingBinder()Z

    move-result v0

    return v0
.end method

.method public final ua()V
    .locals 2

    .prologue
    .line 628
    :try_start_0
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v0}, Lecm;->ua()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 632
    :goto_0
    return-void

    .line 629
    :catch_0
    move-exception v0

    .line 630
    const-string v1, "onHotwordDetectionError()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final w(Lcom/google/android/shared/search/Query;)V
    .locals 2

    .prologue
    .line 577
    :try_start_0
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v0, p1}, Lecm;->w(Lcom/google/android/shared/search/Query;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 581
    :goto_0
    return-void

    .line 578
    :catch_0
    move-exception v0

    .line 579
    const-string v1, "handlePlainQuery()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final x(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 683
    :try_start_0
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v0, p1, p2}, Lecm;->x(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 687
    :goto_0
    return-void

    .line 684
    :catch_0
    move-exception v0

    .line 685
    const-string v1, "updateRecognizedText()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final z(Lcom/google/android/shared/search/Query;)V
    .locals 2

    .prologue
    .line 656
    :try_start_0
    iget-object v0, p0, Ldaj;->bpq:Lecm;

    invoke-interface {v0, p1}, Lecm;->z(Lcom/google/android/shared/search/Query;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 660
    :goto_0
    return-void

    .line 657
    :catch_0
    move-exception v0

    .line 658
    const-string v1, "setQuery()"

    invoke-direct {p0, v1, v0}, Ldaj;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
