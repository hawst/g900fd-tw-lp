.class public abstract Lhfc;
.super Lefv;
.source "PG"


# instance fields
.field public final cRm:Landroid/app/Activity;

.field protected cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

.field protected deE:Ljava/lang/CharSequence;

.field private deG:Lfnx;

.field private mDrawerListener:Lfoh;

.field protected mVelvetServices:Lgql;


# direct methods
.method public constructor <init>(Lq;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lefv;-><init>(Landroid/app/Activity;)V

    .line 32
    iput-object p1, p0, Lhfc;->cRm:Landroid/app/Activity;

    .line 33
    return-void
.end method


# virtual methods
.method public cN()Z
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lhfc;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->cN()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    const/4 v0, 0x1

    .line 111
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lefv;->cN()Z

    move-result v0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 37
    invoke-super {p0, p1}, Lefv;->onCreate(Landroid/os/Bundle;)V

    .line 38
    invoke-virtual {p0}, Lhfc;->wd()I

    move-result v0

    invoke-virtual {p0, v0}, Lhfc;->setContentView(I)V

    .line 40
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    iput-object v0, p0, Lhfc;->mVelvetServices:Lgql;

    .line 42
    const v0, 0x7f110295

    invoke-virtual {p0, v0}, Lhfc;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    iput-object v0, p0, Lhfc;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    .line 44
    iget-object v0, p0, Lhfc;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    new-instance v1, Lhfd;

    invoke-direct {v1, p0}, Lhfd;-><init>(Lhfc;)V

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->a(Lejr;)V

    .line 66
    new-instance v0, Lfnx;

    iget-object v1, p0, Lhfc;->cRm:Landroid/app/Activity;

    iget-object v2, p0, Lhfc;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->aCL()Lfqn;

    move-result-object v2

    iget-object v3, p0, Lhfc;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    new-instance v4, Lgyx;

    iget-object v5, p0, Lhfc;->mVelvetServices:Lgql;

    invoke-direct {v4, v5}, Lgyx;-><init>(Lgql;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lfnx;-><init>(Landroid/app/Activity;Lfom;Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;Lfon;)V

    iput-object v0, p0, Lhfc;->deG:Lfnx;

    .line 71
    iget-object v0, p0, Lhfc;->deG:Lfnx;

    iget-object v1, p0, Lhfc;->mDrawerListener:Lfoh;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lhfc;->we()Lfoh;

    move-result-object v1

    iput-object v1, p0, Lhfc;->mDrawerListener:Lfoh;

    :cond_0
    iget-object v1, p0, Lhfc;->mDrawerListener:Lfoh;

    invoke-virtual {v0, v1}, Lfnx;->a(Lfoh;)V

    .line 73
    iget-object v0, p0, Lhfc;->deG:Lfnx;

    invoke-virtual {v0, p1}, Lfnx;->onPostCreate(Landroid/os/Bundle;)V

    .line 74
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lhfc;->deG:Lfnx;

    invoke-virtual {v0}, Lfnx;->onDestroy()V

    .line 79
    invoke-super {p0}, Lefv;->onDestroy()V

    .line 80
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lhfc;->deG:Lfnx;

    invoke-virtual {v0}, Lfnx;->onResume()V

    .line 85
    invoke-super {p0}, Lefv;->onResume()V

    .line 86
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 102
    invoke-super {p0, p1}, Lefv;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 103
    iget-object v0, p0, Lhfc;->deG:Lfnx;

    invoke-virtual {v0, p1}, Lfnx;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 104
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lhfc;->deG:Lfnx;

    invoke-virtual {v0}, Lfnx;->onStart()V

    .line 91
    invoke-super {p0}, Lefv;->onStart()V

    .line 92
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lhfc;->deG:Lfnx;

    invoke-virtual {v0}, Lfnx;->onStop()V

    .line 97
    invoke-super {p0}, Lefv;->onStop()V

    .line 98
    return-void
.end method

.method protected abstract wd()I
.end method

.method protected abstract we()Lfoh;
.end method
