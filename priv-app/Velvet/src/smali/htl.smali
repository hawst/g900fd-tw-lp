.class public final Lhtl;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private bLG:I

.field private bRc:Lcom/google/android/search/shared/contact/Contact;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final dqJ:Lhth;

.field dqK:Lhto;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private dqb:Lhtj;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mRecognizerController:Lhkd;

.field private final mResources:Landroid/content/res/Resources;

.field private final mSoundManager:Lhik;

.field private final mViewDisplayer:Lhul;


# direct methods
.method public constructor <init>(Lhkd;Landroid/content/res/Resources;Lhul;Lhik;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p2, p0, Lhtl;->mResources:Landroid/content/res/Resources;

    .line 55
    iput-object p1, p0, Lhtl;->mRecognizerController:Lhkd;

    .line 56
    new-instance v0, Lhth;

    new-instance v1, Lhtm;

    invoke-direct {v1, p0}, Lhtm;-><init>(Lhtl;)V

    invoke-direct {v0, v1}, Lhth;-><init>(Lhti;)V

    iput-object v0, p0, Lhtl;->dqJ:Lhth;

    .line 58
    iput-object p3, p0, Lhtl;->mViewDisplayer:Lhul;

    .line 59
    iput-object p4, p0, Lhtl;->mSoundManager:Lhik;

    .line 60
    return-void
.end method


# virtual methods
.method public final a(Lhtj;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lhtl;->dqb:Lhtj;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 64
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtj;

    iput-object v0, p0, Lhtl;->dqb:Lhtj;

    .line 65
    return-void

    .line 63
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aRU()V
    .locals 2

    .prologue
    .line 99
    const/16 v0, 0xe

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget v1, p0, Lhtl;->bLG:I

    invoke-virtual {v0, v1}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 103
    const/4 v0, 0x0

    iput-object v0, p0, Lhtl;->bRc:Lcom/google/android/search/shared/contact/Contact;

    .line 104
    iget-object v0, p0, Lhtl;->dqb:Lhtj;

    invoke-virtual {v0}, Lhtj;->exit()V

    .line 105
    return-void
.end method

.method public final aRV()V
    .locals 2

    .prologue
    .line 108
    const/16 v0, 0xe

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget v1, p0, Lhtl;->bLG:I

    invoke-virtual {v0, v1}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 112
    iget-object v0, p0, Lhtl;->mSoundManager:Lhik;

    invoke-virtual {v0}, Lhik;->aPG()V

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lhtl;->bRc:Lcom/google/android/search/shared/contact/Contact;

    .line 114
    iget-object v0, p0, Lhtl;->dqb:Lhtj;

    invoke-virtual {v0}, Lhtj;->exit()V

    .line 115
    return-void
.end method

.method public final aRW()V
    .locals 2

    .prologue
    .line 118
    const/16 v0, 0xd

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget v1, p0, Lhtl;->bLG:I

    invoke-virtual {v0, v1}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 122
    iget-object v0, p0, Lhtl;->dqb:Lhtj;

    iget-object v1, p0, Lhtl;->bRc:Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lico;->pa(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhtj;->startActivity(Landroid/content/Intent;)V

    .line 123
    iget-object v0, p0, Lhtl;->dqb:Lhtj;

    invoke-virtual {v0}, Lhtj;->exit()V

    .line 124
    return-void
.end method

.method public final aRX()V
    .locals 2

    .prologue
    .line 127
    const/16 v0, 0xd

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget v1, p0, Lhtl;->bLG:I

    invoke-virtual {v0, v1}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 131
    iget-object v0, p0, Lhtl;->mSoundManager:Lhik;

    invoke-virtual {v0}, Lhik;->aPD()V

    .line 132
    iget-object v0, p0, Lhtl;->dqb:Lhtj;

    iget-object v1, p0, Lhtl;->bRc:Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lico;->pa(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhtj;->startActivity(Landroid/content/Intent;)V

    .line 133
    iget-object v0, p0, Lhtl;->dqb:Lhtj;

    invoke-virtual {v0}, Lhtj;->exit()V

    .line 134
    return-void
.end method

.method public final aRY()V
    .locals 3

    .prologue
    const v2, 0x7f0a0930

    .line 137
    const/16 v0, 0xe

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget v1, p0, Lhtl;->bLG:I

    invoke-virtual {v0, v1}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 141
    iget-object v0, p0, Lhtl;->dqb:Lhtj;

    invoke-virtual {v0, v2, v2}, Lhtj;->bh(II)V

    .line 142
    return-void
.end method

.method public final k(Lcom/google/android/search/shared/contact/Contact;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 68
    invoke-static {}, Lenu;->auR()V

    .line 69
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    iput-object v0, p0, Lhtl;->bRc:Lcom/google/android/search/shared/contact/Contact;

    .line 73
    iget-object v1, p0, Lhtl;->mRecognizerController:Lhkd;

    new-instance v2, Lhtn;

    invoke-direct {v2, p0}, Lhtn;-><init>(Lhtl;)V

    const/4 v3, 0x3

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Contact;->oK()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhtl;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0a05f2

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Contact;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, Lhkd;->a(Lglx;ILjava/lang/String;)V

    .line 78
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Contact;->oK()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    const/16 v0, 0xa

    iput v0, p0, Lhtl;->bLG:I

    .line 80
    iget-object v0, p0, Lhtl;->mViewDisplayer:Lhul;

    invoke-virtual {v0}, Lhul;->aSd()Lhto;

    move-result-object v0

    iput-object v0, p0, Lhtl;->dqK:Lhto;

    .line 85
    :goto_1
    iget-object v0, p0, Lhtl;->dqK:Lhto;

    invoke-interface {v0, p0}, Lhto;->a(Lhtl;)V

    .line 86
    iget-object v0, p0, Lhtl;->dqK:Lhto;

    invoke-interface {v0, p1}, Lhto;->l(Lcom/google/android/search/shared/contact/Contact;)V

    .line 87
    iget-object v0, p0, Lhtl;->dqK:Lhto;

    iget-object v1, p0, Lhtl;->dqb:Lhtj;

    iget-object v1, v1, Lhtj;->dqF:Lhuk;

    invoke-virtual {v1}, Lhuk;->aSb()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lhto;->oa(Ljava/lang/String;)V

    .line 88
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lhtl;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0a05f3

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lico;->pb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 82
    :cond_1
    const/16 v0, 0x1c

    iput v0, p0, Lhtl;->bLG:I

    .line 83
    iget-object v0, p0, Lhtl;->mViewDisplayer:Lhul;

    invoke-virtual {v0}, Lhul;->aSe()Lhto;

    move-result-object v0

    iput-object v0, p0, Lhtl;->dqK:Lhto;

    goto :goto_1
.end method
