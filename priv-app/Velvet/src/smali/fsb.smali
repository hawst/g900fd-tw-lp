.class public final Lfsb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final mActivityHelper:Lfzw;

.field final mCardContainer:Lfmt;

.field final mContext:Landroid/content/Context;

.field final mModulePresenter:Lfro;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;Lfro;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lfsb;->mContext:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lfsb;->mCardContainer:Lfmt;

    .line 62
    iput-object p3, p0, Lfsb;->mActivityHelper:Lfzw;

    .line 63
    iput-object p4, p0, Lfsb;->mModulePresenter:Lfro;

    .line 64
    return-void
.end method


# virtual methods
.method public final a(Lani;)Ljava/lang/Runnable;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 68
    const/4 v0, 0x0

    .line 69
    invoke-virtual {p1}, Lani;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 179
    :pswitch_0
    const-string v1, "ClientActionHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown client action type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lani;->getType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :cond_0
    :goto_0
    return-object v0

    .line 71
    :pswitch_1
    new-instance v0, Lfsc;

    invoke-direct {v0, p0, p1}, Lfsc;-><init>(Lfsb;Lani;)V

    goto :goto_0

    .line 79
    :pswitch_2
    new-instance v0, Lfsj;

    invoke-direct {v0, p0, p1}, Lfsj;-><init>(Lfsb;Lani;)V

    goto :goto_0

    .line 87
    :pswitch_3
    new-instance v0, Lfsk;

    invoke-direct {v0, p0, p1}, Lfsk;-><init>(Lfsb;Lani;)V

    goto :goto_0

    .line 95
    :pswitch_4
    new-instance v0, Lfsl;

    invoke-direct {v0, p0, p1}, Lfsl;-><init>(Lfsb;Lani;)V

    goto :goto_0

    .line 103
    :pswitch_5
    new-instance v0, Lfsm;

    invoke-direct {v0, p0}, Lfsm;-><init>(Lfsb;)V

    goto :goto_0

    .line 111
    :pswitch_6
    new-instance v0, Lfsn;

    invoke-direct {v0, p0, p1}, Lfsn;-><init>(Lfsb;Lani;)V

    goto :goto_0

    .line 119
    :pswitch_7
    new-instance v0, Lfso;

    invoke-direct {v0, p0}, Lfso;-><init>(Lfsb;)V

    goto :goto_0

    .line 129
    :pswitch_8
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 130
    new-instance v0, Lfsp;

    invoke-direct {v0, p0}, Lfsp;-><init>(Lfsb;)V

    goto :goto_0

    .line 139
    :pswitch_9
    new-instance v0, Lfsq;

    invoke-direct {v0, p0}, Lfsq;-><init>(Lfsb;)V

    goto :goto_0

    .line 147
    :pswitch_a
    new-instance v0, Lfsd;

    invoke-direct {v0, p0}, Lfsd;-><init>(Lfsb;)V

    goto :goto_0

    .line 155
    :pswitch_b
    new-instance v0, Lfse;

    invoke-direct {v0, p0, p1}, Lfse;-><init>(Lfsb;Lani;)V

    goto :goto_0

    .line 163
    :pswitch_c
    new-instance v0, Lfsf;

    invoke-direct {v0, p0}, Lfsf;-><init>(Lfsb;)V

    goto :goto_0

    .line 171
    :pswitch_d
    new-instance v0, Lfsg;

    invoke-direct {v0, p0, p1}, Lfsg;-><init>(Lfsb;Lani;)V

    goto :goto_0

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method final aDj()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 188
    iget-object v0, p0, Lfsb;->mModulePresenter:Lfro;

    invoke-virtual {v0}, Lfro;->aDe()Lanh;

    move-result-object v0

    iget-object v0, v0, Lanh;->ahu:Lizj;

    .line 189
    const/16 v1, 0xd

    new-array v2, v3, [I

    invoke-static {v0, v1, v2}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v1

    .line 190
    const/16 v2, 0x20

    new-array v3, v3, [I

    invoke-static {v0, v2, v3}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v2

    .line 191
    iget-object v3, p0, Lfsb;->mContext:Landroid/content/Context;

    invoke-static {}, Lfzv;->aDW()Landroid/content/Intent;

    move-result-object v3

    .line 192
    const-string v4, "action_type"

    const/16 v5, 0x93

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 193
    const-string v4, "entry"

    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 194
    const-string v0, "action"

    invoke-static {v3, v0, v1}, Leqh;->a(Landroid/content/Intent;Ljava/lang/String;Ljsr;)V

    .line 195
    const-string v0, "delete_action"

    invoke-static {v3, v0, v2}, Leqh;->a(Landroid/content/Intent;Ljava/lang/String;Ljsr;)V

    .line 196
    iget-object v0, p0, Lfsb;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 197
    return-void
.end method

.method final aDk()V
    .locals 3

    .prologue
    .line 228
    iget-object v0, p0, Lfsb;->mModulePresenter:Lfro;

    invoke-virtual {v0}, Lfro;->aDe()Lanh;

    move-result-object v0

    iget-object v0, v0, Lanh;->ahu:Lizj;

    .line 229
    iget-object v1, p0, Lfsb;->mCardContainer:Lfmt;

    invoke-interface {v1}, Lfmt;->aAD()Lfml;

    move-result-object v1

    iget-object v2, v0, Lizj;->dSU:Ljdx;

    invoke-virtual {v2}, Ljdx;->bif()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lfml;->wt()V

    :cond_0
    invoke-virtual {v1}, Lfml;->aBa()Lfor;

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_0
    invoke-static {v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v0

    invoke-interface {v1, v0}, Lfor;->b(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :cond_1
    :goto_0
    return-void

    .line 229
    :catch_0
    move-exception v0

    const-string v1, "NowRemoteClient"

    const-string v2, "Error making snoozeReminder request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method final aDl()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 346
    iget-object v0, p0, Lfsb;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->aAD()Lfml;

    move-result-object v1

    new-instance v6, Lfsi;

    invoke-direct {v6, p0}, Lfsi;-><init>(Lfsb;)V

    invoke-virtual {v1}, Lfml;->aBa()Lfor;

    move-result-object v5

    if-eqz v5, :cond_0

    new-instance v0, Lfmo;

    const-string v2, "EnableSearchHistory"

    iget-object v3, v1, Lfml;->mTaskRunner:Lerk;

    new-array v4, v7, [I

    invoke-direct/range {v0 .. v6}, Lfmo;-><init>(Lfml;Ljava/lang/String;Lerk;[ILfor;Lemy;)V

    new-array v1, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lfmo;->a([Ljava/lang/Object;)Lenp;

    .line 364
    :cond_0
    iget-object v0, p0, Lfsb;->mCardContainer:Lfmt;

    iget-object v1, p0, Lfsb;->mModulePresenter:Lfro;

    invoke-virtual {v1}, Lfro;->aDe()Lanh;

    move-result-object v1

    iget-object v1, v1, Lanh;->ahu:Lizj;

    invoke-interface {v0, v1, v7}, Lfmt;->a(Lizj;Z)V

    .line 365
    return-void
.end method

.method final b(Lani;)V
    .locals 4

    .prologue
    .line 252
    iget-object v0, p1, Lani;->ahG:Lanp;

    .line 253
    iget-object v1, p0, Lfsb;->mModulePresenter:Lfro;

    invoke-virtual {v1}, Lfro;->aDg()Lfuz;

    move-result-object v1

    .line 254
    iget-object v2, p0, Lfsb;->mModulePresenter:Lfro;

    invoke-virtual {v1, v2}, Lfuz;->a(Lfro;)V

    .line 255
    iget-object v2, p0, Lfsb;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lfsb;->mCardContainer:Lfmt;

    iget-object v0, v0, Lanp;->aii:[Lanh;

    invoke-virtual {v1, v2, v3, v0}, Lfuz;->a(Landroid/content/Context;Lfmt;[Lanh;)V

    .line 258
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lfuz;->bP(Z)V

    .line 259
    return-void
.end method

.method final c(Lani;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 278
    iget-object v8, p1, Lani;->ahH:Laom;

    .line 279
    iget-object v1, p0, Lfsb;->mModulePresenter:Lfro;

    invoke-virtual {v1}, Lfro;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    .line 280
    iget-object v2, p0, Lfsb;->mModulePresenter:Lfro;

    invoke-virtual {v2}, Lfro;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f1102eb

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 281
    iget-object v4, p0, Lfsb;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lfsb;->mCardContainer:Lfmt;

    iget-object v5, v8, Laom;->ajE:Ljei;

    invoke-virtual {v8}, Laom;->pb()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v8}, Laom;->getTitle()Ljava/lang/String;

    move-result-object v3

    :goto_0
    if-eqz v5, :cond_0

    iget-object v0, v5, Ljei;->eeu:Ljcn;

    :cond_0
    new-instance v5, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    invoke-direct {v5}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;-><init>()V

    iget-object v10, v8, Laom;->afA:Ljbj;

    invoke-virtual {v5, v10}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->a(Ljbj;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->mn(Ljava/lang/String;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->b(Ljcn;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v0

    iget-object v3, v8, Laom;->ajK:Lizq;

    invoke-interface {v9}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->a(Landroid/view/View;Landroid/view/View;Lizq;Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v1

    iget-object v0, v8, Laom;->ajF:Ljhe;

    if-eqz v0, :cond_1

    iget-object v0, v8, Laom;->ajF:Ljhe;

    invoke-virtual {v1, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->a(Ljhe;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    :cond_1
    invoke-virtual {v8}, Laom;->om()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v8}, Laom;->ol()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->mo(Ljava/lang/String;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    :cond_2
    iget-object v0, v8, Laom;->ajG:Ljhe;

    if-eqz v0, :cond_3

    iget-object v0, v8, Laom;->ajG:Ljhe;

    invoke-virtual {v1, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->b(Ljhe;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    :cond_3
    iget-object v0, v8, Laom;->ajH:[Ljcn;

    array-length v0, v0

    if-lez v0, :cond_6

    iget-object v2, v8, Laom;->ajH:[Ljcn;

    array-length v3, v2

    move v0, v7

    :goto_1
    if-ge v0, v3, :cond_6

    aget-object v4, v2, v0

    invoke-virtual {v1, v4}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->b(Ljcn;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    if-eqz v5, :cond_5

    invoke-virtual {v5}, Ljei;->getTitle()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_5
    move-object v3, v0

    goto :goto_0

    :cond_6
    invoke-virtual {v8}, Laom;->qG()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v8}, Laom;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->mp(Ljava/lang/String;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    :cond_7
    invoke-virtual {v8}, Laom;->qz()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->fF(Z)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    invoke-interface {v9}, Lfmt;->wv()Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v6

    :goto_2
    invoke-virtual {v1, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->fG(Z)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    invoke-virtual {v8}, Laom;->qR()Z

    move-result v0

    if-eqz v0, :cond_d

    :goto_3
    invoke-virtual {v8}, Laom;->qS()Z

    move-result v0

    if-eqz v0, :cond_8

    or-int/lit8 v6, v6, 0x2

    :cond_8
    if-eqz v6, :cond_9

    invoke-virtual {v1, v6}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->jS(I)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    .line 284
    :cond_9
    iget-object v0, p0, Lfsb;->mModulePresenter:Lfro;

    invoke-virtual {v0}, Lfro;->aDg()Lfuz;

    move-result-object v0

    invoke-virtual {v0}, Lfuz;->aDA()I

    move-result v0

    .line 285
    if-eqz v0, :cond_a

    .line 286
    invoke-virtual {v1, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->jV(I)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    .line 288
    :cond_a
    iget-object v2, p0, Lfsb;->mActivityHelper:Lfzw;

    iget-object v3, p0, Lfsb;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lfsb;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-static {v0, v1}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;)Landroid/content/Intent;

    move-result-object v0

    :goto_4
    invoke-virtual {v2, v3, v0}, Lfzw;->h(Landroid/content/Context;Landroid/content/Intent;)Z

    .line 290
    return-void

    :cond_b
    move v0, v7

    .line 281
    goto :goto_2

    .line 288
    :cond_c
    invoke-static {v0, v1}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil;->b(Landroid/content/Context;Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_4

    :cond_d
    move v6, v7

    goto :goto_3
.end method

.method final d(Lani;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 294
    invoke-virtual {p1}, Lani;->oY()I

    move-result v1

    .line 297
    iget-object v2, p0, Lfsb;->mModulePresenter:Lfro;

    invoke-virtual {v2}, Lfro;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 300
    iget-object v2, p0, Lfsb;->mCardContainer:Lfmt;

    invoke-interface {v2}, Lfmt;->aAD()Lfml;

    move-result-object v2

    invoke-virtual {v2}, Lfml;->ayI()V

    .line 304
    const/16 v2, 0x81

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 306
    :cond_0
    iget-object v1, p0, Lfsb;->mCardContainer:Lfmt;

    iget-object v2, p0, Lfsb;->mModulePresenter:Lfro;

    invoke-virtual {v2}, Lfro;->aDe()Lanh;

    move-result-object v2

    iget-object v2, v2, Lanh;->ahu:Lizj;

    invoke-interface {v1, v2, v0}, Lfmt;->a(Lizj;Z)V

    .line 308
    return-void
.end method

.method final e(Lani;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 369
    iget-object v0, p1, Lani;->ahJ:Laoo;

    iget-object v0, v0, Laoo;->ajM:[Ljcg;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 371
    const-string v0, "ClientActionHandler"

    const-string v1, "No alarm notification in the alarm ClientAction."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    :goto_0
    return-void

    .line 376
    :cond_0
    iget-object v0, p0, Lfsb;->mModulePresenter:Lfro;

    invoke-virtual {v0}, Lfro;->aDe()Lanh;

    move-result-object v0

    iget-object v0, v0, Lanh;->ahu:Lizj;

    invoke-static {v0}, Leqh;->f(Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 377
    iget-object v1, p1, Lani;->ahJ:Laoo;

    iget-object v1, v1, Laoo;->ajM:[Ljcg;

    aget-object v1, v1, v3

    iput-object v1, v0, Lizj;->dUr:Ljcg;

    .line 378
    iget-object v1, p0, Lfsb;->mCardContainer:Lfmt;

    invoke-interface {v1}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/sidekick/shared/renderingcontext/NotificationContext;->n(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/renderingcontext/NotificationContext;

    move-result-object v1

    .line 380
    if-eqz v1, :cond_1

    .line 381
    invoke-virtual {v1, v0}, Lcom/google/android/sidekick/shared/renderingcontext/NotificationContext;->y(Lizj;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 383
    invoke-virtual {v1, v0, v3}, Lcom/google/android/sidekick/shared/renderingcontext/NotificationContext;->e(Lizj;Z)V

    .line 384
    iget-object v1, p0, Lfsb;->mCardContainer:Lfmt;

    invoke-interface {v1}, Lfmt;->aAD()Lfml;

    move-result-object v1

    invoke-virtual {v1, v0}, Lfml;->x(Lizj;)V

    .line 386
    iget-object v0, p0, Lfsb;->mContext:Landroid/content/Context;

    iget-object v1, p1, Lani;->ahJ:Laoo;

    invoke-virtual {v1}, Laoo;->qW()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 398
    :cond_1
    :goto_1
    iget-object v0, p0, Lfsb;->mModulePresenter:Lfro;

    invoke-virtual {v0}, Lfro;->aDg()Lfuz;

    move-result-object v0

    .line 399
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, Lfuz;->G(IZ)V

    .line 400
    const/16 v1, 0x32

    invoke-virtual {v0, v1, v4}, Lfuz;->G(IZ)V

    goto :goto_0

    .line 390
    :cond_2
    invoke-virtual {v1, v0, v4}, Lcom/google/android/sidekick/shared/renderingcontext/NotificationContext;->e(Lizj;Z)V

    .line 391
    iget-object v1, p0, Lfsb;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lfsb;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lijp;->bu(Ljava/lang/Object;)Lijp;

    move-result-object v0

    invoke-static {v0}, Lgbc;->y(Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 393
    iget-object v0, p0, Lfsb;->mContext:Landroid/content/Context;

    iget-object v1, p1, Lani;->ahJ:Laoo;

    invoke-virtual {v1}, Laoo;->qV()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
