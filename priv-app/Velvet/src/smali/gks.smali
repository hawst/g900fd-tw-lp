.class final Lgks;
.super Lepm;
.source "PG"


# instance fields
.field final synthetic cNX:Lgkp;

.field private synthetic cNZ:Ljava/lang/String;

.field private synthetic cOa:Z

.field private synthetic cOb:Lijj;

.field private synthetic cOc:I

.field final synthetic cOd:Lefk;

.field final synthetic cOe:Lefk;

.field final synthetic cOf:Lefk;


# direct methods
.method varargs constructor <init>(Lgkp;Ljava/lang/String;[ILjava/lang/String;ZLijj;ILefk;Lefk;Lefk;)V
    .locals 0

    .prologue
    .line 585
    iput-object p1, p0, Lgks;->cNX:Lgkp;

    iput-object p4, p0, Lgks;->cNZ:Ljava/lang/String;

    iput-boolean p5, p0, Lgks;->cOa:Z

    iput-object p6, p0, Lgks;->cOb:Lijj;

    iput p7, p0, Lgks;->cOc:I

    iput-object p8, p0, Lgks;->cOd:Lefk;

    iput-object p9, p0, Lgks;->cOe:Lefk;

    iput-object p10, p0, Lgks;->cOf:Lefk;

    invoke-direct {p0, p2, p3}, Lepm;-><init>(Ljava/lang/String;[I)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 588
    new-instance v4, Litp;

    invoke-direct {v4}, Litp;-><init>()V

    .line 591
    iget-object v0, p0, Lgks;->cNX:Lgkp;

    iget-object v1, p0, Lgks;->cNZ:Ljava/lang/String;

    iget-boolean v2, p0, Lgks;->cOa:Z

    iget-object v3, p0, Lgks;->cNX:Lgkp;

    invoke-static {v3}, Lgkp;->b(Lgkp;)Lcom/google/speech/grammar/pumpkin/ActionFrame;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lgkp;->a(Ljava/lang/String;ZLcom/google/speech/grammar/pumpkin/ActionFrame;)Ljup;

    move-result-object v0

    .line 597
    iget-object v1, p0, Lgks;->cNX:Lgkp;

    invoke-static {v0}, Lgkp;->b(Ljup;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 598
    iget-object v1, p0, Lgks;->cNX:Lgkp;

    invoke-virtual {v1}, Lgkp;->aGW()Z

    move-result v1

    if-nez v1, :cond_1

    .line 600
    iget-object v0, p0, Lgks;->cNX:Lgkp;

    iget-object v1, p0, Lgks;->cNZ:Ljava/lang/String;

    iget-boolean v2, p0, Lgks;->cOa:Z

    iget-object v3, p0, Lgks;->cNX:Lgkp;

    invoke-static {v3}, Lgkp;->c(Lgkp;)Lcom/google/speech/grammar/pumpkin/ActionFrame;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lgkp;->a(Ljava/lang/String;ZLcom/google/speech/grammar/pumpkin/ActionFrame;)Ljup;

    move-result-object v0

    .line 601
    iget-object v1, p0, Lgks;->cNX:Lgkp;

    invoke-static {v0}, Lgkp;->b(Ljup;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 604
    iget-object v0, p0, Lgks;->cNX:Lgkp;

    iget-object v1, p0, Lgks;->cOb:Lijj;

    iget-boolean v2, p0, Lgks;->cOa:Z

    const/4 v3, 0x4

    iget-object v5, p0, Lgks;->cNX:Lgkp;

    invoke-static {v5}, Lgkp;->b(Lgkp;)Lcom/google/speech/grammar/pumpkin/ActionFrame;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lgkp;->a(Lijj;ZILitp;Lcom/google/speech/grammar/pumpkin/ActionFrame;)Ljup;

    move-result-object v0

    .line 621
    :cond_0
    :goto_0
    iget-object v1, p0, Lgks;->cNX:Lgkp;

    invoke-static {v0}, Lgkp;->b(Ljup;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lgks;->cNX:Lgkp;

    invoke-virtual {v1, v0}, Lgkp;->a(Ljup;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 622
    new-instance v1, Lcom/google/android/speech/embedded/TaggerResult;

    iget-object v0, v0, Ljup;->eFE:[Ljuo;

    aget-object v0, v0, v12

    invoke-direct {v1, v0, v4}, Lcom/google/android/speech/embedded/TaggerResult;-><init>(Ljuo;Litp;)V

    move-object v0, v1

    .line 627
    :goto_1
    iget-object v1, p0, Lgks;->cNX:Lgkp;

    iget-object v1, v1, Lgkp;->bhl:Ljava/util/concurrent/Executor;

    new-instance v2, Lgkt;

    const-string v3, "Received taggerResult"

    invoke-direct {v2, p0, v3, v0}, Lgkt;-><init>(Lgks;Ljava/lang/String;Lcom/google/android/speech/embedded/TaggerResult;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 639
    return-void

    .line 610
    :cond_1
    iget v1, p0, Lgks;->cOc:I

    if-lez v1, :cond_0

    .line 611
    iget-object v5, p0, Lgks;->cNX:Lgkp;

    iget-object v6, p0, Lgks;->cNZ:Ljava/lang/String;

    iget-object v7, p0, Lgks;->cOb:Lijj;

    iget-boolean v8, p0, Lgks;->cOa:Z

    iget v9, p0, Lgks;->cOc:I

    iget-object v0, p0, Lgks;->cNX:Lgkp;

    invoke-static {v0}, Lgkp;->c(Lgkp;)Lcom/google/speech/grammar/pumpkin/ActionFrame;

    move-result-object v11

    move-object v10, v4

    invoke-virtual/range {v5 .. v11}, Lgkp;->a(Ljava/lang/String;Lijj;ZILitp;Lcom/google/speech/grammar/pumpkin/ActionFrame;)Ljup;

    move-result-object v0

    goto :goto_0

    .line 617
    :cond_2
    invoke-virtual {v4, v12}, Litp;->mw(I)Litp;

    goto :goto_0

    .line 624
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
