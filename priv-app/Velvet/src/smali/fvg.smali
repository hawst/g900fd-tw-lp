.class public final Lfvg;
.super Lfuz;
.source "PG"


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 32
    return-void
.end method

.method private static mc(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 83
    :try_start_0
    invoke-static {p0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    const/4 v0, 0x1

    .line 86
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private md(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 94
    invoke-static {p1}, Lfvg;->mc(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-static {p1}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    .line 96
    const-string v1, "%s \u00b7 %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/util/Currency;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v0}, Ljava/util/Currency;->getSymbol()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 100
    :cond_0
    return-object p1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 36
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    .line 38
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 40
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 43
    iget-object v3, v0, Lizj;->dSr:Liyv;

    new-instance v4, Lanh;

    invoke-direct {v4}, Lanh;-><init>()V

    const/16 v5, 0x15

    invoke-virtual {v4, v5}, Lanh;->cm(I)Lanh;

    iput-object v0, v4, Lanh;->ahu:Lizj;

    new-instance v5, Lank;

    invoke-direct {v5}, Lank;-><init>()V

    invoke-virtual {v3}, Liyv;->bcw()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lfvg;->md(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lank;->as(Ljava/lang/String;)Lank;

    invoke-virtual {v3}, Liyv;->bcx()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lank;->av(Ljava/lang/String;)Lank;

    invoke-virtual {v3}, Liyv;->bcv()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lfvg;->md(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lank;->au(Ljava/lang/String;)Lank;

    invoke-virtual {v5, v7}, Lank;->cr(I)Lank;

    new-instance v6, Lann;

    invoke-direct {v6}, Lann;-><init>()V

    iput-object v6, v4, Lanh;->agR:Lann;

    iget-object v6, v4, Lanh;->agR:Lann;

    iput-object v5, v6, Lann;->aie:Lank;

    iget-object v5, v4, Lanh;->agR:Lann;

    invoke-virtual {v3}, Liyv;->bcu()F

    move-result v6

    invoke-virtual {v5, v6}, Lann;->x(F)Lann;

    invoke-virtual {v3}, Liyv;->bcv()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lfvg;->mc(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, v4, Lanh;->agR:Lann;

    invoke-virtual {v3}, Liyv;->bcv()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lann;->aD(Ljava/lang/String;)Lann;

    :cond_0
    invoke-virtual {v3}, Liyv;->bcw()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lfvg;->mc(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, v4, Lanh;->agR:Lann;

    invoke-virtual {v3}, Liyv;->bcw()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Lann;->aE(Ljava/lang/String;)Lann;

    :cond_1
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    new-instance v3, Lanh;

    invoke-direct {v3}, Lanh;-><init>()V

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lanh;->cm(I)Lanh;

    iput-object v0, v3, Lanh;->ahu:Lizj;

    new-instance v0, Laoj;

    invoke-direct {v0}, Laoj;-><init>()V

    iput-object v0, v3, Lanh;->agz:Laoj;

    iget-object v0, v3, Lanh;->agz:Laoj;

    const v4, 0x7f0a0327

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Laoj;->bT(Ljava/lang/String;)Laoj;

    const v0, 0x7f0a012f

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, v3, Lanh;->agz:Laoj;

    new-instance v5, Lfsa;

    const/16 v6, 0x35

    invoke-direct {v5, v6}, Lfsa;-><init>(I)V

    invoke-virtual {v5, v0, v7}, Lfsa;->D(Ljava/lang/String;I)Lani;

    move-result-object v0

    iput-object v0, v4, Laoj;->ajx:Lani;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 49
    return-object v1
.end method
