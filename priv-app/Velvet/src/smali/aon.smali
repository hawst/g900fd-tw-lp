.class public final Laon;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afX:Ljava/lang/String;

.field private afh:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5464
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 5465
    const/4 v0, 0x0

    iput v0, p0, Laon;->aez:I

    const-string v0, ""

    iput-object v0, p0, Laon;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Laon;->afX:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Laon;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Laon;->eCz:I

    .line 5466
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 5401
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Laon;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laon;->afh:Ljava/lang/String;

    iget v0, p0, Laon;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laon;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laon;->afX:Ljava/lang/String;

    iget v0, p0, Laon;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laon;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 5480
    iget v0, p0, Laon;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 5481
    const/4 v0, 0x2

    iget-object v1, p0, Laon;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 5483
    :cond_0
    iget v0, p0, Laon;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 5484
    const/4 v0, 0x3

    iget-object v1, p0, Laon;->afX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 5486
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 5487
    return-void
.end method

.method public final ca(Ljava/lang/String;)Laon;
    .locals 1

    .prologue
    .line 5426
    if-nez p1, :cond_0

    .line 5427
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5429
    :cond_0
    iput-object p1, p0, Laon;->afh:Ljava/lang/String;

    .line 5430
    iget v0, p0, Laon;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laon;->aez:I

    .line 5431
    return-object p0
.end method

.method public final cb(Ljava/lang/String;)Laon;
    .locals 1

    .prologue
    .line 5448
    if-nez p1, :cond_0

    .line 5449
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5451
    :cond_0
    iput-object p1, p0, Laon;->afX:Ljava/lang/String;

    .line 5452
    iget v0, p0, Laon;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laon;->aez:I

    .line 5453
    return-object p0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5423
    iget-object v0, p0, Laon;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 5491
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 5492
    iget v1, p0, Laon;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 5493
    const/4 v1, 0x2

    iget-object v2, p0, Laon;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5496
    :cond_0
    iget v1, p0, Laon;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 5497
    const/4 v1, 0x3

    iget-object v2, p0, Laon;->afX:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5500
    :cond_1
    return v0
.end method

.method public final ol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5445
    iget-object v0, p0, Laon;->afX:Ljava/lang/String;

    return-object v0
.end method

.method public final om()Z
    .locals 1

    .prologue
    .line 5456
    iget v0, p0, Laon;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final pb()Z
    .locals 1

    .prologue
    .line 5434
    iget v0, p0, Laon;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
