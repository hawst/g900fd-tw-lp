.class public Lcid;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcey;


# static fields
.field private static final TAG:Ljava/lang/String; = "JavascriptExtensions"


# instance fields
.field final mApplicationContext:Landroid/content/Context;

.field private final mEventBus:Ldda;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final mIntentStarter:Leoj;

.field private final mNetworkInfo:Lgno;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private final mPageEventListener:Lcik;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mSearchUrlHelper:Lcpn;

.field private final mSettings:Lcke;

.field private final mTrustPolicy:Lcil;

.field private final mUiThread:Lemm;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private mWebViewQuery:Lcom/google/android/shared/search/Query;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leoj;Lcpn;Lcil;Lcke;Lgno;Lcik;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcid;->mApplicationContext:Landroid/content/Context;

    .line 107
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leoj;

    iput-object v0, p0, Lcid;->mIntentStarter:Leoj;

    .line 108
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcil;

    iput-object v0, p0, Lcid;->mTrustPolicy:Lcil;

    .line 109
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpn;

    iput-object v0, p0, Lcid;->mSearchUrlHelper:Lcpn;

    .line 110
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManager;

    iput-object v0, p0, Lcid;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 111
    invoke-static {p5}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcke;

    iput-object v0, p0, Lcid;->mSettings:Lcke;

    .line 112
    invoke-static {p6}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgno;

    iput-object v0, p0, Lcid;->mNetworkInfo:Lgno;

    .line 113
    invoke-static {p7}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcik;

    iput-object v0, p0, Lcid;->mPageEventListener:Lcik;

    .line 114
    iput-object v1, p0, Lcid;->mEventBus:Ldda;

    .line 115
    iput-object v1, p0, Lcid;->mUiThread:Lemm;

    .line 116
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Leoj;Lcpn;Lcil;Lcke;Lgno;Ldda;Lemm;)V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcid;->mApplicationContext:Landroid/content/Context;

    .line 84
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leoj;

    iput-object v0, p0, Lcid;->mIntentStarter:Leoj;

    .line 85
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcil;

    iput-object v0, p0, Lcid;->mTrustPolicy:Lcil;

    .line 86
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpn;

    iput-object v0, p0, Lcid;->mSearchUrlHelper:Lcpn;

    .line 87
    invoke-static {p5}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcke;

    iput-object v0, p0, Lcid;->mSettings:Lcke;

    .line 88
    invoke-static {p6}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgno;

    iput-object v0, p0, Lcid;->mNetworkInfo:Lgno;

    .line 89
    invoke-static {p7}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldda;

    iput-object v0, p0, Lcid;->mEventBus:Ldda;

    .line 90
    invoke-static {p8}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lemm;

    iput-object v0, p0, Lcid;->mUiThread:Lemm;

    .line 91
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManager;

    iput-object v0, p0, Lcid;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 92
    new-instance v0, Lcij;

    invoke-direct {v0, p0}, Lcij;-><init>(Lcid;)V

    iput-object v0, p0, Lcid;->mPageEventListener:Lcik;

    .line 93
    return-void
.end method

.method static synthetic access$000(Lcid;)Ldda;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcid;->mEventBus:Ldda;

    return-object v0
.end method

.method private createAppLaunchIntent(Landroid/content/ComponentName;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 582
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private createAppLaunchIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 589
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private createPackageSpecificUriIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 534
    iget-object v0, p0, Lcid;->mSearchUrlHelper:Lcpn;

    invoke-static {p1}, Lcpn;->hI(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 538
    return-object v0
.end method

.method private getResolvedAppLaunchIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 543
    invoke-static {p1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 544
    if-eqz v0, :cond_0

    .line 546
    invoke-direct {p0, v0}, Lcid;->getResolvedAppLaunchIntentForComponentName(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 549
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcid;->getResolvedAppLaunchIntentForPackageName(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method private getResolvedAppLaunchIntentForComponentName(Landroid/content/ComponentName;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 554
    invoke-direct {p0, p1}, Lcid;->createAppLaunchIntent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 555
    iget-object v1, p0, Lcid;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 556
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 557
    const/4 v0, 0x0

    .line 559
    :cond_0
    return-object v0
.end method

.method private getResolvedAppLaunchIntentForPackageName(Ljava/lang/String;)Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 564
    invoke-direct {p0, p1}, Lcid;->createAppLaunchIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 565
    invoke-direct {p0, p1}, Lcid;->createAppLaunchIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "android.intent.category.DEFAULT"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 567
    iget-object v3, p0, Lcid;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v4, 0x1

    new-array v4, v4, [Landroid/content/Intent;

    aput-object v2, v4, v5

    invoke-virtual {v3, v0, v4, v1, v5}, Landroid/content/pm/PackageManager;->queryIntentActivityOptions(Landroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 570
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 577
    :goto_0
    return-object v0

    .line 573
    :cond_0
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 574
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    invoke-direct {p0, v1}, Lcid;->createAppLaunchIntent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method private declared-synchronized getWebViewQuery()Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcid;->mWebViewQuery:Lcom/google/android/shared/search/Query;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static permissiveTrustPolicy()Lcil;
    .locals 1

    .prologue
    .line 667
    new-instance v0, Lcii;

    invoke-direct {v0}, Lcii;-><init>()V

    return-object v0
.end method

.method public static searchResultsTrustPolicy(Lcpn;)Lcil;
    .locals 1

    .prologue
    .line 653
    new-instance v0, Lcih;

    invoke-direct {v0, p0}, Lcih;-><init>(Lcpn;)V

    return-object v0
.end method


# virtual methods
.method public addInAppUrlPattern(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 377
    :try_start_0
    iget-object v0, p0, Lcid;->mPageEventListener:Lcik;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcid;->mPageEventListener:Lcik;

    invoke-interface {v0, p1}, Lcik;->addInAppUrlPattern(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 384
    :cond_0
    :goto_0
    return-void

    .line 380
    :catch_0
    move-exception v0

    .line 382
    invoke-static {v0}, Ligm;->g(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    goto :goto_0
.end method

.method public addOptionsMenuItem(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 392
    :try_start_0
    iget-object v0, p0, Lcid;->mPageEventListener:Lcik;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcid;->mPageEventListener:Lcik;

    invoke-interface {v0, p1, p2, p3, p4}, Lcik;->addOptionsMenuItem(Ljava/lang/String;ILjava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 399
    :cond_0
    :goto_0
    return-void

    .line 395
    :catch_0
    move-exception v0

    .line 397
    invoke-static {v0}, Ligm;->g(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    goto :goto_0
.end method

.method public canLaunchApp(Ljava/lang/String;)Z
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 293
    :try_start_0
    iget-object v1, p0, Lcid;->mTrustPolicy:Lcil;

    invoke-interface {v1}, Lcil;->isTrusted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 304
    :cond_0
    :goto_0
    return v0

    .line 298
    :cond_1
    invoke-direct {p0, p1}, Lcid;->getResolvedAppLaunchIntent(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 299
    if-eqz v1, :cond_0

    .line 304
    const/4 v0, 0x1

    goto :goto_0

    .line 306
    :catch_0
    move-exception v0

    .line 308
    invoke-static {v0}, Ligm;->g(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public canUriBeHandled(Ljava/lang/String;)Z
    .locals 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 171
    :try_start_0
    iget-object v1, p0, Lcid;->mTrustPolicy:Lcil;

    invoke-interface {v1}, Lcil;->isTrusted()Z

    .line 175
    iget-object v1, p0, Lcid;->mSearchUrlHelper:Lcpn;

    invoke-static {p1}, Lcpn;->hI(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 176
    iget-object v2, p0, Lcid;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 177
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 183
    :cond_0
    :goto_0
    return v0

    .line 184
    :catch_0
    move-exception v0

    .line 186
    invoke-static {v0}, Ligm;->g(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 183
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public canUriBeHandledByPackage(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 199
    :try_start_0
    iget-object v1, p0, Lcid;->mTrustPolicy:Lcil;

    invoke-interface {v1}, Lcil;->isTrusted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 212
    :cond_0
    :goto_0
    return v0

    .line 204
    :cond_1
    invoke-direct {p0, p1, p2}, Lcid;->createPackageSpecificUriIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 205
    iget-object v2, p0, Lcid;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 206
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 213
    :catch_0
    move-exception v0

    .line 215
    invoke-static {v0}, Ligm;->g(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 212
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public delayedPageLoad()V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 343
    :try_start_0
    iget-object v0, p0, Lcid;->mPageEventListener:Lcik;

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lcid;->mPageEventListener:Lcik;

    invoke-interface {v0}, Lcik;->delayedPageLoad()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 349
    :cond_0
    return-void

    .line 346
    :catch_0
    move-exception v0

    .line 348
    invoke-static {v0}, Ligm;->g(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getDetailedNetworkConnectionType()I
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 502
    iget-object v0, p0, Lcid;->mNetworkInfo:Lgno;

    invoke-virtual {v0}, Lgno;->aAn()I

    move-result v0

    return v0
.end method

.method public getHttpHeadersAndCgiParameters()Ljava/lang/String;
    .locals 7
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 508
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    const-string v1, "#"

    const-string v2, "/search?q=_"

    const-wide/16 v4, 0x0

    invoke-static {v1, v2, v4, v5}, Lehm;->e(Ljava/lang/String;Ljava/lang/String;J)Lehm;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->a(Lehm;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 511
    iget-object v1, p0, Lcid;->mSearchUrlHelper:Lcpn;

    invoke-virtual {v1, v0}, Lcpn;->j(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v2

    .line 514
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 515
    invoke-virtual {v2}, Lcom/google/android/search/shared/api/UriRequest;->alI()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 516
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 526
    :catch_0
    move-exception v0

    .line 527
    const-string v1, "JavascriptExtensions"

    const-string v2, "getHttpHeadersAndCgiParameters"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 529
    const-string v0, "[{}, {}]"

    :goto_1
    return-object v0

    .line 518
    :cond_0
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 519
    invoke-virtual {v2}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v2

    .line 520
    invoke-virtual {v2}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 521
    const-string v5, "q"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 522
    invoke-virtual {v2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    .line 525
    :cond_2
    new-instance v0, Lorg/json/JSONArray;

    const/4 v2, 0x2

    new-array v2, v2, [Lorg/json/JSONObject;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const/4 v3, 0x1

    aput-object v1, v2, v3

    invoke-direct {v0, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1
.end method

.method public getNetworkConnectionType()Ljava/lang/String;
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 465
    iget-object v0, p0, Lcid;->mNetworkInfo:Lgno;

    invoke-virtual {v0}, Lgno;->aHX()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isTrusted()Z
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 131
    :try_start_0
    iget-object v0, p0, Lcid;->mTrustPolicy:Lcil;

    invoke-interface {v0}, Lcil;->isTrusted()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 133
    return v0

    .line 134
    :catch_0
    move-exception v0

    .line 136
    invoke-static {v0}, Ligm;->g(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public launchApp(Ljava/lang/String;)Z
    .locals 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 318
    :try_start_0
    iget-object v1, p0, Lcid;->mTrustPolicy:Lcil;

    invoke-interface {v1}, Lcil;->isTrusted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 330
    :cond_0
    :goto_0
    return v0

    .line 323
    :cond_1
    invoke-direct {p0, p1}, Lcid;->getResolvedAppLaunchIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 324
    if-eqz v1, :cond_0

    .line 328
    iget-object v0, p0, Lcid;->mIntentStarter:Leoj;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/content/Intent;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-interface {v0, v2}, Leoj;->b([Landroid/content/Intent;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 332
    :catch_0
    move-exception v0

    .line 334
    invoke-static {v0}, Ligm;->g(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public logClientEvent(I)V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 450
    :try_start_0
    iget-object v0, p0, Lcid;->mTrustPolicy:Lcil;

    invoke-interface {v0}, Lcil;->isTrusted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 460
    :goto_0
    return-void

    .line 455
    :cond_0
    invoke-static {p1}, Lege;->ht(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 456
    :catch_0
    move-exception v0

    .line 458
    invoke-static {v0}, Ligm;->g(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    goto :goto_0
.end method

.method public openInApp(Ljava/lang/String;)Z
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 145
    :try_start_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 147
    iget-object v2, p0, Lcid;->mTrustPolicy:Lcil;

    invoke-interface {v2}, Lcil;->isTrusted()Z

    move-result v2

    if-nez v2, :cond_1

    .line 158
    :cond_0
    :goto_0
    return v0

    .line 152
    :cond_1
    iget-object v2, p0, Lcid;->mSearchUrlHelper:Lcpn;

    invoke-virtual {v2, v1}, Lcpn;->q(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 157
    iget-object v0, p0, Lcid;->mPageEventListener:Lcik;

    invoke-interface {v0, v1}, Lcik;->j(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    const/4 v0, 0x1

    goto :goto_0

    .line 159
    :catch_0
    move-exception v0

    .line 161
    invoke-static {v0}, Ligm;->g(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public openWithPackage(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 227
    :try_start_0
    iget-object v1, p0, Lcid;->mTrustPolicy:Lcil;

    invoke-interface {v1}, Lcil;->isTrusted()Z

    move-result v1

    if-nez v1, :cond_0

    .line 239
    :goto_0
    return v0

    .line 232
    :cond_0
    invoke-direct {p0, p1, p2}, Lcid;->createPackageSpecificUriIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 233
    iget-object v2, p0, Lcid;->mIntentStarter:Leoj;

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/content/Intent;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-interface {v2, v3}, Leoj;->b([Landroid/content/Intent;)Z
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 240
    :catch_0
    move-exception v0

    .line 242
    invoke-static {v0}, Ligm;->g(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 239
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public openWithPackageWithAccountExtras(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 255
    :try_start_0
    iget-object v1, p0, Lcid;->mTrustPolicy:Lcil;

    invoke-interface {v1}, Lcil;->isTrusted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 280
    :cond_0
    :goto_0
    return v0

    .line 261
    :cond_1
    iget-object v1, p0, Lcid;->mSettings:Lcke;

    invoke-interface {v1}, Lcke;->ND()Ljava/lang/String;

    move-result-object v1

    .line 263
    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 270
    invoke-direct {p0, p1, p2}, Lcid;->createPackageSpecificUriIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 272
    iget-object v3, p0, Lcid;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/identity/accounts/api/AccountData;->fj(Ljava/lang/String;)Lcom/google/android/gms/identity/accounts/api/AccountData;

    move-result-object v1

    invoke-static {v3, v2, v1}, Lbkf;->a(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/gms/identity/accounts/api/AccountData;)Z

    .line 274
    iget-object v1, p0, Lcid;->mIntentStarter:Leoj;

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/content/Intent;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-interface {v1, v3}, Leoj;->b([Landroid/content/Intent;)Z
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 281
    :catch_0
    move-exception v0

    .line 283
    invoke-static {v0}, Ligm;->g(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 280
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public pageReady()V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 357
    :try_start_0
    iget-object v0, p0, Lcid;->mPageEventListener:Lcik;

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcid;->mPageEventListener:Lcik;

    invoke-interface {v0}, Lcik;->pageReady()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 363
    :cond_0
    return-void

    .line 360
    :catch_0
    move-exception v0

    .line 362
    invoke-static {v0}, Ligm;->g(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public prefetch(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 370
    return-void
.end method

.method public requestUpdateHostApp()V
    .locals 6
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 407
    :try_start_0
    iget-object v0, p0, Lcid;->mTrustPolicy:Lcil;

    invoke-interface {v0}, Lcil;->isTrusted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 442
    :goto_0
    return-void

    .line 412
    :cond_0
    iget-object v0, p0, Lcid;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 415
    iget-object v0, p0, Lcid;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.android.vending.billing.PURCHASE"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "com.android.vending"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "backend"

    const/4 v4, 0x3

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "document_type"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "full_docid"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "backend_docid"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "offer_type"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v3, 0x10000

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-nez v2, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "https://play.google.com/store/apps/details?id=%1$s&rdid=%1$s&rdot=%2$d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "use_direct_purchase"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 419
    :cond_1
    iget-object v1, p0, Lcid;->mIntentStarter:Leoj;

    new-instance v2, Lcie;

    invoke-direct {v2, p0}, Lcie;-><init>(Lcid;)V

    invoke-interface {v1, v0, v2}, Leoj;->a(Landroid/content/Intent;Leol;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 438
    :catch_0
    move-exception v0

    .line 440
    invoke-static {v0}, Ligm;->g(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    goto :goto_0
.end method

.method public setFullscreen(Z)V
    .locals 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 471
    invoke-direct {p0}, Lcid;->getWebViewQuery()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 472
    iget-object v1, p0, Lcid;->mUiThread:Lemm;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcid;->mEventBus:Ldda;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 473
    iget-object v1, p0, Lcid;->mUiThread:Lemm;

    new-instance v2, Lcif;

    const-string v3, "Set fullscreen transition"

    invoke-direct {v2, p0, v3, v0, p1}, Lcif;-><init>(Lcid;Ljava/lang/String;Lcom/google/android/shared/search/Query;Z)V

    invoke-interface {v1, v2}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 482
    :goto_0
    return-void

    .line 480
    :cond_0
    const-string v0, "JavascriptExtensions"

    const-string v1, "setFullscreen called from outside SRP WebView"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setNativeUiState(II)V
    .locals 7
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 487
    invoke-direct {p0}, Lcid;->getWebViewQuery()Lcom/google/android/shared/search/Query;

    move-result-object v3

    .line 488
    iget-object v0, p0, Lcid;->mUiThread:Lemm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcid;->mEventBus:Ldda;

    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 489
    iget-object v6, p0, Lcid;->mUiThread:Lemm;

    new-instance v0, Lcig;

    const-string v2, "Set native UI state"

    move-object v1, p0

    move v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcig;-><init>(Lcid;Ljava/lang/String;Lcom/google/android/shared/search/Query;II)V

    invoke-interface {v6, v0}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 498
    :goto_0
    return-void

    .line 496
    :cond_0
    const-string v0, "JavascriptExtensions"

    const-string v1, "setNativeUiState called from outside SRP WebView"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public declared-synchronized setWebViewQuery(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 119
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcid;->mWebViewQuery:Lcom/google/android/shared/search/Query;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    monitor-exit p0

    return-void

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
