.class public final Lhqh;
.super Legx;
.source "PG"


# instance fields
.field private synthetic dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lhqh;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    invoke-direct {p0}, Legx;-><init>()V

    return-void
.end method


# virtual methods
.method protected final aj(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 229
    iget-object v0, p0, Lhqh;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lhqh;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mTimeTriggerView:Landroid/widget/RadioButton;

    if-ne p1, v0, :cond_1

    .line 233
    iget-object v0, p0, Lhqh;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->a(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 234
    iget-object v0, p0, Lhqh;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->a(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->getSelectedItemPosition()I

    move-result v0

    .line 235
    iget-object v3, p0, Lhqh;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v3, v3, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnU:Lelg;

    invoke-virtual {v3, v0}, Lelg;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lelh;

    .line 237
    iget-object v3, v0, Lelh;->ceB:Ljava/lang/Object;

    if-eqz v3, :cond_3

    iget-object v0, v0, Lelh;->ceB:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_3

    .line 240
    const/4 v0, 0x4

    .line 248
    :goto_0
    iget-object v2, p0, Lhqh;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    invoke-virtual {v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->onUserInteraction()V

    .line 249
    iget-object v2, p0, Lhqh;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v2, v2, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    iget-object v2, v2, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiy()I

    move-result v2

    if-eq v0, v2, :cond_2

    .line 250
    :goto_1
    iget-object v2, p0, Lhqh;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v2, v2, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    invoke-virtual {v2, v0}, Lhpn;->dj(I)V

    .line 251
    if-eqz v1, :cond_0

    iget-object v0, p0, Lhqh;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->b(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)Legs;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lhqh;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->b(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)Legs;

    move-result-object v0

    iget-object v1, p0, Lhqh;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    invoke-virtual {v0, v1}, Legs;->aF(Landroid/view/View;)V

    .line 255
    :cond_0
    return-void

    .line 243
    :cond_1
    iget-object v0, p0, Lhqh;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mLocationTriggerView:Landroid/widget/RadioButton;

    if-ne p1, v0, :cond_0

    move v0, v2

    .line 244
    goto :goto_0

    .line 249
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method
