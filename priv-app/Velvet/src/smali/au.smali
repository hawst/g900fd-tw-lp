.class public final Lau;
.super Lbr;
.source "PG"


# instance fields
.field private actionIntent:Landroid/app/PendingIntent;

.field private final cF:Landroid/os/Bundle;

.field private final cG:[Lbw;

.field private icon:I

.field private title:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2104
    new-instance v0, Lav;

    invoke-direct {v0}, Lav;-><init>()V

    return-void
.end method

.method public constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V
    .locals 6

    .prologue
    .line 1762
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lau;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Lbw;)V

    .line 1763
    return-void
.end method

.method private constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Lbw;)V
    .locals 1

    .prologue
    .line 1766
    invoke-direct {p0}, Lbr;-><init>()V

    .line 1767
    iput p1, p0, Lau;->icon:I

    .line 1768
    invoke-static {p2}, Lay;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lau;->title:Ljava/lang/CharSequence;

    .line 1769
    iput-object p3, p0, Lau;->actionIntent:Landroid/app/PendingIntent;

    .line 1770
    if-eqz p4, :cond_0

    :goto_0
    iput-object p4, p0, Lau;->cF:Landroid/os/Bundle;

    .line 1771
    const/4 v0, 0x0

    iput-object v0, p0, Lau;->cG:[Lbw;

    .line 1772
    return-void

    .line 1770
    :cond_0
    new-instance p4, Landroid/os/Bundle;

    invoke-direct {p4}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method protected final ag()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 1786
    iget-object v0, p0, Lau;->actionIntent:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public final bridge synthetic ah()[Lcd;
    .locals 1

    .prologue
    .line 1743
    iget-object v0, p0, Lau;->cG:[Lbw;

    return-object v0
.end method

.method public final getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1793
    iget-object v0, p0, Lau;->cF:Landroid/os/Bundle;

    return-object v0
.end method

.method protected final getIcon()I
    .locals 1

    .prologue
    .line 1776
    iget v0, p0, Lau;->icon:I

    return v0
.end method

.method protected final getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1781
    iget-object v0, p0, Lau;->title:Ljava/lang/CharSequence;

    return-object v0
.end method
