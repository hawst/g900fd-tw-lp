.class public final Lhwj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhkq;


# instance fields
.field private Oq:I

.field private aNF:Ldtr;

.field private atJ:Landroid/widget/ProgressBar;

.field private dqZ:Landroid/widget/TextView;

.field private dth:Landroid/widget/ImageView;

.field private dti:Landroid/widget/TextView;

.field private dtj:Landroid/widget/TextView;

.field private dtk:Landroid/widget/Button;

.field private dtl:Landroid/widget/Button;

.field private dtm:Landroid/widget/Button;

.field public dtn:Lhkr;

.field private dto:Ljava/lang/String;

.field private dtp:Ljava/lang/String;

.field dtq:I

.field private dtr:Landroid/view/View$OnClickListener;

.field dts:Z

.field private dtt:Landroid/view/View$OnClickListener;

.field dtu:I

.field private dtv:Landroid/view/View$OnClickListener;

.field private final mActivity:Landroid/app/Activity;

.field private final mSpeechLevelSource:Lequ;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lequ;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lhwj;->Oq:I

    .line 53
    iput-object v1, p0, Lhwj;->dtn:Lhkr;

    .line 54
    iput-object v1, p0, Lhwj;->dto:Ljava/lang/String;

    .line 55
    iput-object v1, p0, Lhwj;->dtp:Ljava/lang/String;

    .line 58
    new-instance v0, Lhwk;

    invoke-direct {v0, p0}, Lhwk;-><init>(Lhwj;)V

    iput-object v0, p0, Lhwj;->dtr:Landroid/view/View$OnClickListener;

    .line 68
    new-instance v0, Lhwl;

    invoke-direct {v0, p0}, Lhwl;-><init>(Lhwj;)V

    iput-object v0, p0, Lhwj;->dtt:Landroid/view/View$OnClickListener;

    .line 80
    new-instance v0, Lhwm;

    invoke-direct {v0, p0}, Lhwm;-><init>(Lhwj;)V

    iput-object v0, p0, Lhwj;->dtv:Landroid/view/View$OnClickListener;

    .line 101
    iput-object p1, p0, Lhwj;->mActivity:Landroid/app/Activity;

    .line 102
    iput-object p2, p0, Lhwj;->mSpeechLevelSource:Lequ;

    .line 103
    const v0, 0x7f0a0025

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhwj;->dto:Ljava/lang/String;

    .line 104
    return-void
.end method

.method private aTk()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 226
    iget v0, p0, Lhwj;->Oq:I

    if-eq v0, v3, :cond_2

    .line 227
    iget-object v0, p0, Lhwj;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0400ad

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(I)V

    .line 228
    iput v3, p0, Lhwj;->Oq:I

    .line 230
    iput-object v2, p0, Lhwj;->dtj:Landroid/widget/TextView;

    .line 231
    iput-object v2, p0, Lhwj;->dtk:Landroid/widget/Button;

    .line 232
    iput-object v2, p0, Lhwj;->dtl:Landroid/widget/Button;

    .line 233
    iput-object v2, p0, Lhwj;->dtm:Landroid/widget/Button;

    .line 235
    const v0, 0x7f11022f

    invoke-direct {p0, v0}, Lhwj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lhwj;->dth:Landroid/widget/ImageView;

    .line 236
    const v0, 0x7f110233

    invoke-direct {p0, v0}, Lhwj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhwj;->dti:Landroid/widget/TextView;

    .line 237
    const v0, 0x7f110231

    invoke-direct {p0, v0}, Lhwj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhwj;->dqZ:Landroid/widget/TextView;

    .line 238
    const v0, 0x7f11021e

    invoke-direct {p0, v0}, Lhwj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lhwj;->atJ:Landroid/widget/ProgressBar;

    .line 240
    iget-object v0, p0, Lhwj;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 241
    new-instance v1, Ldsv;

    const v2, 0x7f0d0112

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v3, 0x7f0d0113

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const v4, 0x7f0b0070

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {v1, v2, v3, v0}, Ldsv;-><init>(III)V

    .line 245
    iget-object v0, p0, Lhwj;->dth:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 246
    iget-object v0, p0, Lhwj;->mSpeechLevelSource:Lequ;

    if-eqz v0, :cond_0

    .line 247
    new-instance v0, Ldtr;

    iget-object v2, p0, Lhwj;->mSpeechLevelSource:Lequ;

    invoke-static {v2}, Leef;->a(Lequ;)Ldtb;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ldtr;-><init>(Ldsv;Ldtb;)V

    iput-object v0, p0, Lhwj;->aNF:Ldtr;

    .line 251
    :cond_0
    iget-object v0, p0, Lhwj;->dth:Landroid/widget/ImageView;

    iget-object v1, p0, Lhwj;->dtv:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 252
    iget-object v0, p0, Lhwj;->dto:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 253
    iget-object v0, p0, Lhwj;->dti:Landroid/widget/TextView;

    iget-object v1, p0, Lhwj;->dto:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    :cond_1
    iget-object v0, p0, Lhwj;->dtp:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 256
    iget-object v0, p0, Lhwj;->dqZ:Landroid/widget/TextView;

    iget-object v1, p0, Lhwj;->dtp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    :cond_2
    return-void
.end method

.method private findViewById(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lhwj;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final a(Lhkr;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lhwj;->dtn:Lhkr;

    .line 109
    return-void
.end method

.method public final aQp()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 162
    invoke-direct {p0}, Lhwj;->aTk()V

    .line 164
    iget-object v0, p0, Lhwj;->dqZ:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 165
    iget-object v0, p0, Lhwj;->dti:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 167
    iget-object v0, p0, Lhwj;->aNF:Ldtr;

    invoke-virtual {v0, v2}, Ldtr;->eg(Z)V

    .line 168
    iget-object v0, p0, Lhwj;->atJ:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 170
    iput v2, p0, Lhwj;->dtu:I

    .line 171
    iget-object v0, p0, Lhwj;->dth:Landroid/widget/ImageView;

    const v1, 0x7f02024d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 172
    iget-object v0, p0, Lhwj;->dth:Landroid/widget/ImageView;

    iget-object v1, p0, Lhwj;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0a0031

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 173
    return-void
.end method

.method public final aQv()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x4

    .line 114
    invoke-direct {p0}, Lhwj;->aTk()V

    .line 116
    iget-object v0, p0, Lhwj;->aNF:Ldtr;

    invoke-virtual {v0, v2}, Ldtr;->eg(Z)V

    .line 117
    iget-object v0, p0, Lhwj;->dqZ:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lhwj;->dti:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 120
    iget-object v0, p0, Lhwj;->atJ:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 122
    iput v2, p0, Lhwj;->dtu:I

    .line 123
    iget-object v0, p0, Lhwj;->dth:Landroid/widget/ImageView;

    const v1, 0x7f02016d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 124
    iget-object v0, p0, Lhwj;->dth:Landroid/widget/ImageView;

    iget-object v1, p0, Lhwj;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0a0031

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 125
    return-void
.end method

.method public final aQw()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 146
    invoke-direct {p0}, Lhwj;->aTk()V

    .line 148
    iget-object v0, p0, Lhwj;->aNF:Ldtr;

    invoke-virtual {v0, v2}, Ldtr;->eg(Z)V

    .line 149
    iget-object v0, p0, Lhwj;->dqZ:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lhwj;->dti:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 152
    iget-object v0, p0, Lhwj;->atJ:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 154
    iput v2, p0, Lhwj;->dtu:I

    .line 155
    iget-object v0, p0, Lhwj;->dth:Landroid/widget/ImageView;

    const v1, 0x7f020250

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 156
    iget-object v0, p0, Lhwj;->dth:Landroid/widget/ImageView;

    iget-object v1, p0, Lhwj;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0a08de

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 157
    return-void
.end method

.method public final aQx()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 130
    invoke-direct {p0}, Lhwj;->aTk()V

    .line 132
    iget-object v0, p0, Lhwj;->aNF:Ldtr;

    invoke-virtual {v0, v2}, Ldtr;->eg(Z)V

    .line 133
    iget-object v0, p0, Lhwj;->dqZ:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lhwj;->dti:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lhwj;->atJ:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 138
    iput v2, p0, Lhwj;->dtu:I

    .line 139
    iget-object v0, p0, Lhwj;->dth:Landroid/widget/ImageView;

    const v1, 0x7f02024a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 140
    iget-object v0, p0, Lhwj;->dth:Landroid/widget/ImageView;

    iget-object v1, p0, Lhwj;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0a0031

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 141
    return-void
.end method

.method public final b(IIZZ)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v4, 0x8

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 206
    iget v0, p0, Lhwj;->Oq:I

    if-eq v0, v5, :cond_0

    iget-object v0, p0, Lhwj;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0400ae

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setContentView(I)V

    iput v5, p0, Lhwj;->Oq:I

    iput-object v3, p0, Lhwj;->aNF:Ldtr;

    iput-object v3, p0, Lhwj;->dth:Landroid/widget/ImageView;

    iput-object v3, p0, Lhwj;->dti:Landroid/widget/TextView;

    iput-object v3, p0, Lhwj;->atJ:Landroid/widget/ProgressBar;

    iput-object v3, p0, Lhwj;->dqZ:Landroid/widget/TextView;

    const v0, 0x7f1100bc

    invoke-direct {p0, v0}, Lhwj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhwj;->dtj:Landroid/widget/TextView;

    const v0, 0x7f110235

    invoke-direct {p0, v0}, Lhwj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lhwj;->dtk:Landroid/widget/Button;

    const v0, 0x7f110236

    invoke-direct {p0, v0}, Lhwj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lhwj;->dtl:Landroid/widget/Button;

    const v0, 0x7f110234

    invoke-direct {p0, v0}, Lhwj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lhwj;->dtm:Landroid/widget/Button;

    iget-object v0, p0, Lhwj;->dtk:Landroid/widget/Button;

    iget-object v2, p0, Lhwj;->dtt:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lhwj;->dtl:Landroid/widget/Button;

    iget-object v2, p0, Lhwj;->dtr:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lhwj;->dtm:Landroid/widget/Button;

    iget-object v2, p0, Lhwj;->dtr:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 208
    :cond_0
    iget-object v0, p0, Lhwj;->dtj:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 210
    iput p2, p0, Lhwj;->dtq:I

    .line 211
    if-nez p3, :cond_1

    if-eqz p4, :cond_3

    .line 212
    :cond_1
    iget-object v0, p0, Lhwj;->dtk:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 213
    iget-object v0, p0, Lhwj;->dtl:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 214
    iget-object v0, p0, Lhwj;->dtm:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 217
    if-nez p3, :cond_2

    if-eqz p4, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lhwj;->dts:Z

    .line 223
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 217
    goto :goto_0

    .line 219
    :cond_3
    iget-object v0, p0, Lhwj;->dtk:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 220
    iget-object v0, p0, Lhwj;->dtl:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 221
    iget-object v0, p0, Lhwj;->dtm:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1
.end method

.method public final lk(I)V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lhwj;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhwj;->dto:Ljava/lang/String;

    .line 179
    iget v0, p0, Lhwj;->Oq:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 180
    iget-object v0, p0, Lhwj;->dti:Landroid/widget/TextView;

    iget-object v1, p0, Lhwj;->dto:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    :cond_0
    return-void
.end method

.method public final oa(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 195
    iput-object p1, p0, Lhwj;->dtp:Ljava/lang/String;

    .line 196
    iget v0, p0, Lhwj;->Oq:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 197
    iget-object v0, p0, Lhwj;->dqZ:Landroid/widget/TextView;

    iget-object v1, p0, Lhwj;->dtp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    :cond_0
    return-void
.end method

.method public final ob(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 186
    iput-object p1, p0, Lhwj;->dto:Ljava/lang/String;

    .line 188
    iget v0, p0, Lhwj;->Oq:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 189
    iget-object v0, p0, Lhwj;->dti:Landroid/widget/TextView;

    iget-object v1, p0, Lhwj;->dto:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    :cond_0
    return-void
.end method
