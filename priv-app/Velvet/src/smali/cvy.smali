.class public Lcvy;
.super Lcyc;
.source "PG"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private final aUm:Landroid/database/DataSetObservable;

.field private final amE:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private amH:Lcxd;

.field public amK:Landroid/preference/PreferenceGroup;

.field private final amX:Lcsq;

.field blA:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final blB:Landroid/database/DataSetObserver;

.field public blp:Landroid/preference/SwitchPreference;

.field public blq:Landroid/preference/Preference;

.field public blr:Landroid/preference/Preference;

.field private bls:Z

.field public blt:Lcom/google/android/search/core/preferences/SelectAccountPreference;

.field private final blu:Lcrr;

.field private final blv:Lerp;

.field private final blw:Z

.field private final blx:Z

.field private final bly:Z

.field blz:Ljava/lang/Boolean;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mAccountManager:Landroid/accounts/AccountManager;

.field private final mActivity:Landroid/app/Activity;

.field private final mFlags:Lchk;

.field private final mGmsLocationReportingHelper:Leue;

.field private final mIntentUtils:Leom;

.field final mLocationSettings:Lcob;

.field private final mLoginHelper:Lcrh;

.field private final mNetworkClient:Lfcx;

.field private final mSettings:Lcke;

.field private final mUrlHelper:Lcpn;

.field private final mVoiceSettings:Lhym;


# direct methods
.method public constructor <init>(Lcke;Lcrh;Lchk;Landroid/app/Activity;Lcpn;Landroid/database/DataSetObservable;Lfcx;Leom;Lcrr;Lcob;Leue;Lerp;Landroid/accounts/AccountManager;ZZLhym;Lcsq;)V
    .locals 3

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lcyc;-><init>(Lcke;)V

    .line 121
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, Lcvy;->blA:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 123
    new-instance v1, Lcvz;

    invoke-direct {v1, p0}, Lcvz;-><init>(Lcvy;)V

    iput-object v1, p0, Lcvy;->blB:Landroid/database/DataSetObserver;

    .line 130
    new-instance v1, Lcwa;

    invoke-direct {v1, p0}, Lcwa;-><init>(Lcvy;)V

    iput-object v1, p0, Lcvy;->amE:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 161
    iput-object p1, p0, Lcvy;->mSettings:Lcke;

    .line 162
    iget-object v1, p0, Lcvy;->mSettings:Lcke;

    iget-object v2, p0, Lcvy;->amE:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v1, v2}, Lcke;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 163
    iput-object p2, p0, Lcvy;->mLoginHelper:Lcrh;

    .line 164
    iput-object p4, p0, Lcvy;->mActivity:Landroid/app/Activity;

    .line 165
    iput-object p3, p0, Lcvy;->mFlags:Lchk;

    .line 166
    iput-object p5, p0, Lcvy;->mUrlHelper:Lcpn;

    .line 167
    iput-object p6, p0, Lcvy;->aUm:Landroid/database/DataSetObservable;

    .line 168
    iput-object p7, p0, Lcvy;->mNetworkClient:Lfcx;

    .line 169
    iput-object p8, p0, Lcvy;->mIntentUtils:Leom;

    .line 170
    iput-object p9, p0, Lcvy;->blu:Lcrr;

    .line 171
    iput-object p10, p0, Lcvy;->mLocationSettings:Lcob;

    .line 172
    iput-object p11, p0, Lcvy;->mGmsLocationReportingHelper:Leue;

    .line 173
    iput-object p12, p0, Lcvy;->blv:Lerp;

    .line 174
    move-object/from16 v0, p13

    iput-object v0, p0, Lcvy;->mAccountManager:Landroid/accounts/AccountManager;

    .line 175
    move/from16 v0, p14

    iput-boolean v0, p0, Lcvy;->blx:Z

    .line 176
    move/from16 v0, p15

    iput-boolean v0, p0, Lcvy;->bly:Z

    .line 177
    move-object/from16 v0, p16

    iput-object v0, p0, Lcvy;->mVoiceSettings:Lhym;

    .line 178
    move-object/from16 v0, p17

    iput-object v0, p0, Lcvy;->amX:Lcsq;

    .line 179
    iget-object v1, p0, Lcvy;->amX:Lcsq;

    if-eqz v1, :cond_0

    invoke-virtual/range {p16 .. p16}, Lhym;->aTR()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcvy;->blw:Z

    .line 180
    return-void

    .line 179
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private Tm()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 429
    const/4 v0, 0x0

    .line 430
    iget-boolean v1, p0, Lcvy;->blx:Z

    if-nez v1, :cond_0

    .line 431
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcvy;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/google/android/googlequicksearchbox/SearchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 432
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 434
    :cond_0
    return-object v0
.end method

.method static synthetic a(Lcvy;)Lcsq;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcvy;->amX:Lcsq;

    return-object v0
.end method

.method static synthetic b(Lcvy;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcvy;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static ea(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 307
    const-string v0, "cloud_search_history"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "manage_search_history"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "manage_location_history"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "google_account"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private iX(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 417
    iget-boolean v0, p0, Lcvy;->bly:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcvy;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Su()Z

    move-result v0

    if-nez v0, :cond_0

    .line 420
    iget-object v0, p0, Lcvy;->mActivity:Landroid/app/Activity;

    iget-boolean v1, p0, Lcvy;->blx:Z

    invoke-static {v0, p1, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->c(Landroid/content/Context;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 425
    :goto_0
    return-object v0

    .line 423
    :cond_0
    invoke-direct {p0}, Lcvy;->Tm()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected Tl()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 367
    iget-object v0, p0, Lcvy;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Sr()[Ljava/lang/String;

    move-result-object v0

    .line 370
    iget-object v1, p0, Lcvy;->mLoginHelper:Lcrh;

    invoke-virtual {v1}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v1

    .line 374
    array-length v2, v0

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    .line 375
    array-length v3, v0

    invoke-static {v0, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 377
    if-eqz v1, :cond_1

    .line 378
    iget-object v0, p0, Lcvy;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a05ae

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 382
    :goto_0
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    aput-object v0, v2, v3

    .line 386
    iget-object v0, p0, Lcvy;->blt:Lcom/google/android/search/core/preferences/SelectAccountPreference;

    invoke-virtual {v0, v2}, Lcom/google/android/search/core/preferences/SelectAccountPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 387
    iget-object v0, p0, Lcvy;->blt:Lcom/google/android/search/core/preferences/SelectAccountPreference;

    invoke-virtual {v0, v2}, Lcom/google/android/search/core/preferences/SelectAccountPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 388
    if-eqz v1, :cond_0

    .line 389
    iget-object v0, p0, Lcvy;->blt:Lcom/google/android/search/core/preferences/SelectAccountPreference;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/preferences/SelectAccountPreference;->setValue(Ljava/lang/String;)V

    .line 392
    :cond_0
    invoke-virtual {p0}, Lcvy;->tP()V

    .line 393
    return-void

    .line 380
    :cond_1
    iget-object v0, p0, Lcvy;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a05af

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final Tn()V
    .locals 2

    .prologue
    .line 528
    iget-object v0, p0, Lcvy;->blt:Lcom/google/android/search/core/preferences/SelectAccountPreference;

    invoke-virtual {v0}, Lcom/google/android/search/core/preferences/SelectAccountPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 529
    :goto_0
    iget-object v1, p0, Lcvy;->blq:Landroid/preference/Preference;

    invoke-virtual {p0, v1, v0}, Lcvy;->a(Landroid/preference/Preference;Z)V

    .line 530
    return-void

    .line 528
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final To()V
    .locals 4

    .prologue
    .line 534
    iget-object v0, p0, Lcvy;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    .line 536
    if-nez v0, :cond_1

    .line 537
    iget-object v1, p0, Lcvy;->blr:Landroid/preference/Preference;

    iget-object v0, p0, Lcvy;->mLocationSettings:Lcob;

    invoke-interface {v0}, Lcob;->QQ()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcvy;->a(Landroid/preference/Preference;Z)V

    .line 561
    :goto_1
    return-void

    .line 537
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 542
    :cond_1
    iget-object v1, p0, Lcvy;->mGmsLocationReportingHelper:Leue;

    invoke-virtual {v1, v0}, Leue;->C(Landroid/accounts/Account;)Lcgs;

    move-result-object v0

    .line 545
    new-instance v1, Lcwd;

    invoke-direct {v1, p0}, Lcwd;-><init>(Lcvy;)V

    iget-object v2, p0, Lcvy;->blv:Lerp;

    const-string v3, "handleReportingStateForSettings"

    invoke-virtual {v0, v1, v2, v3}, Lcgs;->a(Lemy;Lerp;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Landroid/preference/Preference;)V
    .locals 2

    .prologue
    .line 315
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 317
    const-string v1, "cloud_search_history"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 318
    check-cast p1, Landroid/preference/SwitchPreference;

    iput-object p1, p0, Lcvy;->blp:Landroid/preference/SwitchPreference;

    iget-object v0, p0, Lcvy;->blp:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 326
    :cond_0
    :goto_0
    return-void

    .line 319
    :cond_1
    const-string v1, "manage_search_history"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 320
    iput-object p1, p0, Lcvy;->blq:Landroid/preference/Preference;

    iget-object v0, p0, Lcvy;->blq:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_0

    .line 321
    :cond_2
    const-string v1, "manage_location_history"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 322
    invoke-virtual {p0, p1}, Lcvy;->i(Landroid/preference/Preference;)V

    goto :goto_0

    .line 323
    :cond_3
    const-string v1, "google_account"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    invoke-virtual {p0, p1}, Lcvy;->j(Landroid/preference/Preference;)V

    goto :goto_0
.end method

.method final a(Landroid/preference/Preference;Z)V
    .locals 2

    .prologue
    .line 564
    if-nez p2, :cond_1

    .line 565
    iget-object v0, p0, Lcvy;->amK:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 569
    :cond_0
    :goto_0
    return-void

    .line 566
    :cond_1
    iget-object v0, p0, Lcvy;->amK:Landroid/preference/PreferenceGroup;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_0

    .line 567
    iget-object v0, p0, Lcvy;->amK:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method public final a(Landroid/preference/PreferenceScreen;)V
    .locals 0

    .prologue
    .line 302
    invoke-super {p0, p1}, Lcyc;->a(Landroid/preference/PreferenceScreen;)V

    .line 303
    iput-object p1, p0, Lcvy;->amK:Landroid/preference/PreferenceGroup;

    .line 304
    return-void
.end method

.method protected cZ(I)V
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lcvy;->mActivity:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 414
    return-void
.end method

.method public final d(Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x0

    return v0
.end method

.method public final getResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcvy;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public final i(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 200
    invoke-super {p0, p1}, Lcyc;->i(Landroid/os/Bundle;)V

    .line 201
    iget-object v0, p0, Lcvy;->mLoginHelper:Lcrh;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcvy;->mLoginHelper:Lcrh;

    iget-object v1, p0, Lcvy;->blB:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcrh;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 204
    :cond_0
    return-void
.end method

.method public final i(Landroid/preference/Preference;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 340
    iput-object p1, p0, Lcvy;->blr:Landroid/preference/Preference;

    .line 342
    iget-object v0, p0, Lcvy;->mIntentUtils:Leom;

    iget-object v2, p0, Lcvy;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcvy;->blr:Landroid/preference/Preference;

    invoke-virtual {v3}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Leom;->e(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcvy;->bls:Z

    .line 344
    iget-boolean v0, p0, Lcvy;->bls:Z

    if-eqz v0, :cond_1

    .line 345
    iget-object v0, p0, Lcvy;->blr:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 349
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 342
    goto :goto_0

    .line 347
    :cond_1
    iget-object v0, p0, Lcvy;->blr:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_1
.end method

.method public final j(Landroid/preference/Preference;)V
    .locals 2

    .prologue
    .line 353
    check-cast p1, Lcom/google/android/search/core/preferences/SelectAccountPreference;

    iput-object p1, p0, Lcvy;->blt:Lcom/google/android/search/core/preferences/SelectAccountPreference;

    .line 354
    iget-object v0, p0, Lcvy;->blt:Lcom/google/android/search/core/preferences/SelectAccountPreference;

    iget-object v1, p0, Lcvy;->mNetworkClient:Lfcx;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/preferences/SelectAccountPreference;->a(Lfcx;)V

    .line 355
    iget-object v0, p0, Lcvy;->blt:Lcom/google/android/search/core/preferences/SelectAccountPreference;

    invoke-virtual {v0, p0}, Lcom/google/android/search/core/preferences/SelectAccountPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 356
    invoke-virtual {p0}, Lcvy;->Tl()V

    .line 358
    iget-boolean v0, p0, Lcvy;->blx:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcvy;->blt:Lcom/google/android/search/core/preferences/SelectAccountPreference;

    invoke-virtual {v0}, Lcom/google/android/search/core/preferences/SelectAccountPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvy;->blt:Lcom/google/android/search/core/preferences/SelectAccountPreference;

    invoke-virtual {v0}, Lcom/google/android/search/core/preferences/SelectAccountPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 361
    :cond_0
    iget-object v0, p0, Lcvy;->blt:Lcom/google/android/search/core/preferences/SelectAccountPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/preferences/SelectAccountPreference;->showDialog(Landroid/os/Bundle;)V

    .line 363
    :cond_1
    return-void
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lcvy;->mLoginHelper:Lcrh;

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcvy;->mLoginHelper:Lcrh;

    iget-object v1, p0, Lcvy;->blB:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcrh;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 288
    :cond_0
    iget-object v0, p0, Lcvy;->mSettings:Lcke;

    iget-object v1, p0, Lcvy;->amE:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Lcke;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 289
    invoke-super {p0}, Lcyc;->onDestroy()V

    .line 290
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 11

    .prologue
    const/4 v0, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 441
    iget-object v1, p0, Lcvy;->blt:Lcom/google/android/search/core/preferences/SelectAccountPreference;

    if-ne p1, v1, :cond_6

    .line 442
    iget-object v1, p0, Lcvy;->aUm:Landroid/database/DataSetObservable;

    invoke-virtual {v1}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 443
    check-cast p2, Ljava/lang/String;

    .line 444
    iget-object v1, p0, Lcvy;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a05af

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 448
    iget-object v0, p0, Lcvy;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v1, "com.google"

    iget-object v5, p0, Lcvy;->mActivity:Landroid/app/Activity;

    move-object v3, v2

    move-object v4, v2

    move-object v6, p0

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move v0, v8

    .line 523
    :cond_0
    :goto_0
    return v0

    .line 455
    :cond_1
    iget-object v1, p0, Lcvy;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a05ae

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 458
    iget-object v1, p0, Lcvy;->blt:Lcom/google/android/search/core/preferences/SelectAccountPreference;

    iget-object v3, p0, Lcvy;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a05b1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/search/core/preferences/SelectAccountPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 460
    iget-object v1, p0, Lcvy;->mLoginHelper:Lcrh;

    invoke-virtual {v1}, Lcrh;->Sw()V

    .line 462
    invoke-direct {p0}, Lcvy;->Tm()Landroid/content/Intent;

    move-result-object v1

    move-object v3, v1

    move-object v1, v2

    .line 481
    :goto_1
    if-eqz v3, :cond_2

    .line 482
    iget-object v4, p0, Lcvy;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 484
    :cond_2
    iget-boolean v3, p0, Lcvy;->blw:Z

    if-eqz v3, :cond_5

    iget-object v1, p0, Lcvy;->amX:Lcsq;

    invoke-interface {v1}, Lcsq;->SO()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcvy;->amX:Lcsq;

    new-instance v2, Lcwf;

    invoke-direct {v2, p0}, Lcwf;-><init>(Lcvy;)V

    invoke-interface {v1, v2}, Lcsq;->f(Lcsr;)Z

    .line 485
    :cond_3
    :goto_2
    iget-object v1, p0, Lcvy;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 465
    :cond_4
    iget-object v1, p0, Lcvy;->mLoginHelper:Lcrh;

    invoke-virtual {v1}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v1

    .line 466
    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 472
    :try_start_0
    iget-object v3, p0, Lcvy;->mLoginHelper:Lcrh;

    invoke-virtual {v3, p2}, Lcrh;->iz(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/accounts/AccountsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 478
    invoke-direct {p0, p2}, Lcvy;->iX(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    goto :goto_1

    .line 475
    :catch_0
    move-exception v0

    move v0, v8

    goto :goto_0

    .line 484
    :cond_5
    iget-object v3, p0, Lcvy;->mVoiceSettings:Lhym;

    invoke-virtual {v3}, Lhym;->aTR()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v3, p0, Lcvy;->mActivity:Landroid/app/Activity;

    invoke-static {}, Lcry;->SA()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "com.google.android.googlequicksearchbox.LAUNCH_FROM_DSP_HOTWORD"

    new-instance v6, Lcwe;

    invoke-direct {v6, p0, v8}, Lcwe;-><init>(Lcvy;B)V

    const/4 v8, -0x1

    move-object v7, v2

    move-object v9, v2

    move-object v10, v2

    invoke-virtual/range {v3 .. v10}, Landroid/app/Activity;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    goto :goto_2

    .line 486
    :cond_6
    iget-object v1, p0, Lcvy;->blp:Landroid/preference/SwitchPreference;

    if-ne p1, v1, :cond_0

    .line 487
    check-cast p1, Landroid/preference/SwitchPreference;

    .line 488
    iget-object v1, p0, Lcvy;->blz:Ljava/lang/Boolean;

    if-nez v1, :cond_7

    move v0, v8

    .line 491
    goto/16 :goto_0

    .line 493
    :cond_7
    iget-object v1, p0, Lcvy;->blz:Ljava/lang/Boolean;

    invoke-virtual {v1, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 494
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 495
    const v2, 0x7f0a05a1

    invoke-virtual {p0, v2}, Lcvy;->cZ(I)V

    .line 496
    invoke-virtual {p1, v8}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 497
    iget-object v2, p0, Lcvy;->blA:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    .line 498
    iget-object v3, p0, Lcvy;->blu:Lcrr;

    iget-object v4, p0, Lcvy;->mLoginHelper:Lcrh;

    invoke-virtual {v4}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v4

    new-instance v5, Lcwc;

    invoke-direct {v5, p0, v1, p1, v2}, Lcwc;-><init>(Lcvy;ZLandroid/preference/SwitchPreference;I)V

    invoke-virtual {v3, v4, v1, v8, v5}, Lcrr;->a(Landroid/accounts/Account;ZZLemy;)V

    goto/16 :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6

    .prologue
    .line 399
    iget-object v0, p0, Lcvy;->blq:Landroid/preference/Preference;

    if-ne p1, v0, :cond_0

    .line 400
    new-instance v0, Lcxd;

    iget-object v1, p0, Lcvy;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcvy;->mFlags:Lchk;

    iget-object v3, p0, Lcvy;->mLoginHelper:Lcrh;

    iget-object v4, p0, Lcvy;->mUrlHelper:Lcpn;

    iget-object v5, p0, Lcvy;->mFlags:Lchk;

    invoke-virtual {v5}, Lchk;->GY()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcxd;-><init>(Landroid/content/Context;Lchk;Lcrh;Lcpn;Ljava/lang/String;)V

    iput-object v0, p0, Lcvy;->amH:Lcxd;

    iget-object v0, p0, Lcvy;->amH:Lcxd;

    invoke-virtual {v0}, Lcxd;->start()V

    .line 402
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 216
    invoke-super {p0}, Lcyc;->onResume()V

    .line 217
    invoke-virtual {p0}, Lcvy;->Tn()V

    .line 218
    invoke-virtual {p0}, Lcvy;->To()V

    .line 219
    invoke-virtual {p0}, Lcvy;->Tl()V

    .line 224
    iget-object v0, p0, Lcvy;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcvy;->s(Landroid/accounts/Account;)V

    .line 225
    return-void
.end method

.method public final onStart()V
    .locals 2

    .prologue
    .line 185
    invoke-super {p0}, Lcyc;->onStart()V

    .line 187
    iget-boolean v0, p0, Lcvy;->blw:Z

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcvy;->amX:Lcsq;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcsq;->a(Lcsr;)V

    .line 191
    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcvy;->amH:Lcxd;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcvy;->amH:Lcxd;

    invoke-virtual {v0}, Lcxd;->Tw()V

    .line 273
    const/4 v0, 0x0

    iput-object v0, p0, Lcvy;->amH:Lcxd;

    .line 276
    :cond_0
    iget-boolean v0, p0, Lcvy;->blw:Z

    if-eqz v0, :cond_1

    .line 277
    iget-object v0, p0, Lcvy;->amX:Lcsq;

    invoke-interface {v0}, Lcsq;->disconnect()V

    .line 280
    :cond_1
    invoke-super {p0}, Lcyc;->onStop()V

    .line 281
    return-void
.end method

.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 642
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 643
    if-eqz v0, :cond_2

    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 645
    :goto_0
    const-string v1, "GoogleAccountSettings"

    const-string v2, "Add account activity returned account name: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x4

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 647
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 648
    iget-object v1, p0, Lcvy;->mLoginHelper:Lcrh;

    invoke-virtual {v1}, Lcrh;->refresh()V

    .line 649
    iget-object v1, p0, Lcvy;->mLoginHelper:Lcrh;

    invoke-virtual {v1, v0}, Lcrh;->iz(Ljava/lang/String;)V

    .line 651
    invoke-direct {p0, v0}, Lcvy;->iX(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 652
    if-eqz v0, :cond_0

    .line 653
    iget-object v1, p0, Lcvy;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 655
    :cond_0
    iget-object v0, p0, Lcvy;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 665
    :cond_1
    :goto_1
    return-void

    .line 643
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 657
    :catch_0
    move-exception v0

    .line 659
    const-string v1, "GoogleAccountSettings"

    const-string v2, "Adding new account cancelled"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v7, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 660
    :catch_1
    move-exception v0

    .line 661
    const-string v1, "GoogleAccountSettings"

    const-string v2, "Account not found"

    invoke-static {v1, v2, v0}, Leor;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 662
    :catch_2
    move-exception v0

    .line 663
    const-string v1, "GoogleAccountSettings"

    const-string v2, "Failed to add new account"

    invoke-static {v1, v2, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final s(Landroid/accounts/Account;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 229
    const/4 v0, 0x0

    iput-object v0, p0, Lcvy;->blz:Ljava/lang/Boolean;

    .line 230
    if-nez p1, :cond_0

    .line 231
    iget-object v0, p0, Lcvy;->blp:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v2}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 267
    :goto_0
    return-void

    .line 233
    :cond_0
    iget-object v0, p0, Lcvy;->blp:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v2}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 234
    iget-object v0, p0, Lcvy;->blp:Landroid/preference/SwitchPreference;

    const v1, 0x7f0a059e

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(I)V

    .line 236
    iget-object v0, p0, Lcvy;->blA:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 237
    iget-object v0, p0, Lcvy;->blu:Lcrr;

    new-instance v1, Lcwb;

    invoke-direct {v1, p0, p1}, Lcwb;-><init>(Lcvy;Landroid/accounts/Account;)V

    invoke-virtual {v0, p1, v2, v1}, Lcrr;->a(Landroid/accounts/Account;ZLemy;)V

    goto :goto_0
.end method

.method final tP()V
    .locals 9

    .prologue
    const v3, 0x7f0a05a9

    const v7, 0x7f0a05a8

    const v8, 0x7f0a05a5

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 581
    iget-object v0, p0, Lcvy;->mLoginHelper:Lcrh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcvy;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Ss()[Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    .line 582
    iget-object v0, p0, Lcvy;->blt:Lcom/google/android/search/core/preferences/SelectAccountPreference;

    const v1, 0x7f0a05ad

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/preferences/SelectAccountPreference;->setSummary(I)V

    .line 585
    iget-object v0, p0, Lcvy;->blq:Landroid/preference/Preference;

    invoke-virtual {v0, v7}, Landroid/preference/Preference;->setTitle(I)V

    .line 586
    iget-object v0, p0, Lcvy;->blq:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setSummary(I)V

    .line 588
    iget-object v0, p0, Lcvy;->blq:Landroid/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 590
    iget-object v0, p0, Lcvy;->blr:Landroid/preference/Preference;

    invoke-virtual {v0, v8}, Landroid/preference/Preference;->setTitle(I)V

    .line 591
    iget-object v0, p0, Lcvy;->blr:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setSummary(I)V

    .line 593
    iget-object v0, p0, Lcvy;->blr:Landroid/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 625
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 581
    goto :goto_0

    .line 595
    :cond_2
    iget-object v0, p0, Lcvy;->mLoginHelper:Lcrh;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcvy;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 596
    iget-object v0, p0, Lcvy;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v3

    .line 598
    iget-object v0, p0, Lcvy;->blt:Lcom/google/android/search/core/preferences/SelectAccountPreference;

    iget-object v4, p0, Lcvy;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a05b0

    new-array v6, v1, [Ljava/lang/Object;

    aput-object v3, v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/search/core/preferences/SelectAccountPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 602
    iget-object v0, p0, Lcvy;->blq:Landroid/preference/Preference;

    invoke-virtual {v0, v7}, Landroid/preference/Preference;->setTitle(I)V

    .line 603
    iget-object v4, p0, Lcvy;->blq:Landroid/preference/Preference;

    iget-object v0, p0, Lcvy;->mFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Ha()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcvy;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0a05ab

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 605
    iget-object v0, p0, Lcvy;->blq:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 607
    iget-object v0, p0, Lcvy;->blr:Landroid/preference/Preference;

    invoke-virtual {v0, v8}, Landroid/preference/Preference;->setTitle(I)V

    .line 608
    iget-object v0, p0, Lcvy;->blr:Landroid/preference/Preference;

    iget-object v4, p0, Lcvy;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a05a6

    new-array v6, v1, [Ljava/lang/Object;

    aput-object v3, v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 610
    iget-boolean v0, p0, Lcvy;->bls:Z

    if-nez v0, :cond_0

    .line 611
    iget-object v0, p0, Lcvy;->blr:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_1

    :cond_3
    move v0, v2

    .line 595
    goto :goto_2

    .line 603
    :cond_4
    iget-object v0, p0, Lcvy;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0a05aa

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v3}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v0, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 615
    :cond_5
    iget-object v0, p0, Lcvy;->blt:Lcom/google/android/search/core/preferences/SelectAccountPreference;

    iget-object v1, p0, Lcvy;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a05b2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/preferences/SelectAccountPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 618
    iget-object v0, p0, Lcvy;->amK:Landroid/preference/PreferenceGroup;

    if-eqz v0, :cond_0

    .line 619
    iget-object v0, p0, Lcvy;->amK:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcvy;->blr:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 620
    iget-object v0, p0, Lcvy;->amK:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcvy;->blp:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 621
    iget-object v0, p0, Lcvy;->amK:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcvy;->blq:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1
.end method
