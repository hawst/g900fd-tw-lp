.class public final Ldfw;
.super Lddx;
.source "PG"


# instance fields
.field private final aGx:Ljava/lang/String;

.field private final aZG:J

.field private final bwI:Z


# direct methods
.method public constructor <init>(Ldfw;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lddx;-><init>(Ldef;)V

    .line 49
    iget-wide v0, p1, Ldfw;->aZG:J

    iput-wide v0, p0, Ldfw;->aZG:J

    .line 50
    iget-object v0, p1, Ldfw;->aGx:Ljava/lang/String;

    iput-object v0, p0, Ldfw;->aGx:Ljava/lang/String;

    .line 51
    iget-boolean v0, p1, Ldfw;->bwI:Z

    iput-boolean v0, p0, Ldfw;->bwI:Z

    .line 52
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V
    .locals 8

    .prologue
    .line 44
    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Ldfw;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;JLjava/lang/String;Z)V

    .line 45
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;JLjava/lang/String;Z)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lddx;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    .line 34
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldfw;->aZG:J

    .line 35
    iput-object p5, p0, Ldfw;->aGx:Ljava/lang/String;

    .line 36
    iput-boolean p6, p0, Ldfw;->bwI:Z

    .line 37
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 40
    const-wide/16 v4, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v7}, Ldfw;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;JLjava/lang/String;Z)V

    .line 41
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/util/List;JLjava/lang/String;)V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lddx;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/util/List;)V

    .line 26
    iput-wide p4, p0, Ldfw;->aZG:J

    .line 27
    iput-object p6, p0, Ldfw;->aGx:Ljava/lang/String;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldfw;->bwI:Z

    .line 29
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/String;)Ldfw;
    .locals 8

    .prologue
    .line 56
    new-instance v1, Ldfw;

    const-wide/16 v4, 0x0

    const/4 v7, 0x1

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-direct/range {v1 .. v7}, Ldfw;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;JLjava/lang/String;Z)V

    return-object v1
.end method


# virtual methods
.method public final abx()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Ldfw;->aGx:Ljava/lang/String;

    return-object v0
.end method

.method public final aby()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Ldfw;->bwI:Z

    return v0
.end method

.method public final getCreationTime()J
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Ldfw;->aZG:J

    return-wide v0
.end method
