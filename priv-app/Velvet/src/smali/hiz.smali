.class public final Lhiz;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final din:[I

.field private static final dio:Ljava/util/LinkedHashMap;


# instance fields
.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 36
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lhiz;->din:[I

    .line 47
    new-instance v0, Lhja;

    const/4 v1, 0x6

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lhja;-><init>(IFZ)V

    sput-object v0, Lhiz;->dio:Ljava/util/LinkedHashMap;

    return-void

    .line 36
    :array_0
    .array-data 4
        0x404
        0x408
        0x418
        0x420
    .end array-data
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lhiz;->mResources:Landroid/content/res/Resources;

    .line 62
    return-void
.end method

.method public static a(Lhji;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 87
    sget-object v2, Lcgg;->aVt:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcgg;->aVv:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 115
    :goto_0
    return v1

    .line 91
    :cond_0
    sget-object v2, Lcgg;->aVo:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 95
    invoke-virtual {p0}, Lhji;->aPV()I

    move-result v2

    sget-object v3, Lhiz;->din:[I

    array-length v4, v3

    :goto_1
    if-ge v1, v4, :cond_2

    aget v5, v3, v1

    if-ne v2, v5, :cond_1

    :goto_2
    move v1, v0

    .line 96
    goto :goto_0

    .line 95
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Lhji;->hasService(I)Z

    move-result v0

    goto :goto_2

    .line 105
    :cond_3
    invoke-virtual {p0}, Lhji;->getName()Ljava/lang/String;

    move-result-object v2

    .line 106
    if-nez v2, :cond_4

    .line 107
    const-string v0, "BluetoothClassifier"

    const-string v2, "No device name available"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 110
    :cond_4
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 111
    const-string v3, "car"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "bmw"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "audi"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    :goto_3
    move v1, v0

    .line 115
    goto :goto_0

    :cond_6
    move v0, v1

    .line 111
    goto :goto_3
.end method


# virtual methods
.method public final b(Lhji;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 120
    if-nez p1, :cond_0

    move v0, v1

    .line 152
    :goto_0
    return v0

    .line 123
    :cond_0
    invoke-virtual {p1}, Lhji;->getName()Ljava/lang/String;

    move-result-object v3

    .line 124
    const-string v0, "BluetoothClassifier"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Bluetooth Device Name: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    sget-object v0, Lhiz;->dio:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 126
    if-eqz v0, :cond_1

    .line 127
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 130
    :cond_1
    iget-object v0, p0, Lhiz;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f080001

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 131
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    invoke-direct {v5, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v4, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 135
    :cond_2
    :try_start_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 136
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 139
    sget-object v0, Lhiz;->dio:Ljava/util/LinkedHashMap;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v0, v3, v5}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    invoke-static {v4}, Lisq;->b(Ljava/io/Closeable;)V

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-static {v4}, Lisq;->b(Ljava/io/Closeable;)V

    .line 151
    :goto_1
    sget-object v0, Lhiz;->dio:Ljava/util/LinkedHashMap;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 152
    goto :goto_0

    .line 143
    :catch_0
    move-exception v0

    .line 144
    :try_start_1
    const-string v2, "BluetoothClassifier"

    const-string v5, "BVRA Whitelist was not loaded properly. isBVRADevice will (likely) always return false."

    invoke-static {v2, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147
    invoke-static {v4}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-static {v4}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0
.end method
