.class public final Ljfw;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ejb:Z

.field private ejc:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 16433
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 16434
    const/4 v0, 0x0

    iput v0, p0, Ljfw;->aez:I

    iput-boolean v1, p0, Ljfw;->ejb:Z

    iput-boolean v1, p0, Ljfw;->ejc:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljfw;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljfw;->eCz:I

    .line 16435
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 16376
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljfw;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfw;->ejb:Z

    iget v0, p0, Ljfw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfw;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfw;->ejc:Z

    iget v0, p0, Ljfw;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfw;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 16449
    iget v0, p0, Ljfw;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 16450
    const/4 v0, 0x1

    iget-boolean v1, p0, Ljfw;->ejb:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 16452
    :cond_0
    iget v0, p0, Ljfw;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 16453
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljfw;->ejc:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 16455
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 16456
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 16460
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 16461
    iget v1, p0, Ljfw;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 16462
    const/4 v1, 0x1

    iget-boolean v2, p0, Ljfw;->ejb:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 16465
    :cond_0
    iget v1, p0, Ljfw;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 16466
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljfw;->ejc:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 16469
    :cond_1
    return v0
.end method
