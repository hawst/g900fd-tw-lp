.class public final Lfjo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final ctB:Leqo;

.field private ctC:Lfjp;

.field final mAppContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leqo;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lfjo;->mAppContext:Landroid/content/Context;

    .line 32
    iput-object p2, p0, Lfjo;->ctB:Leqo;

    .line 33
    return-void
.end method


# virtual methods
.method public final aAt()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lfjp;

    invoke-direct {v0, p0}, Lfjp;-><init>(Lfjo;)V

    .line 40
    monitor-enter p0

    .line 41
    :try_start_0
    iget-object v1, p0, Lfjo;->ctC:Lfjp;

    if-eqz v1, :cond_0

    .line 42
    iget-object v1, p0, Lfjo;->ctC:Lfjp;

    invoke-virtual {v1}, Lfjp;->cancel()V

    .line 44
    :cond_0
    iput-object v0, p0, Lfjo;->ctC:Lfjp;

    .line 45
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    iget-object v1, p0, Lfjo;->ctB:Leqo;

    invoke-interface {v1, v0}, Leqo;->j(Ljava/lang/Runnable;)V

    .line 47
    return-void

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final aAu()I
    .locals 4

    .prologue
    .line 69
    iget-object v0, p0, Lfjo;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 70
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lfjo;->mAppContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/apps/sidekick/widget/PredictiveCardsWidgetProvider;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 72
    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 73
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    array-length v0, v0

    goto :goto_0
.end method
