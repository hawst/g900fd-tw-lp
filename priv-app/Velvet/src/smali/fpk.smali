.class public Lfpk;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final cyp:Ljava/lang/String;


# instance fields
.field private final cyA:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lfpk;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfpk;->cyp:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lfpk;->cyA:Landroid/os/Bundle;

    .line 22
    return-void
.end method

.method public static o(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lfpk;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 98
    sget-object v0, Lfpk;->cyp:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->lT(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 100
    new-instance v1, Lfpk;

    invoke-direct {v1, v0}, Lfpk;-><init>(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public final lX(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lfpk;->cyA:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 80
    iget-object v0, p0, Lfpk;->cyA:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
