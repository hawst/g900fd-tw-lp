.class public final Lfhf;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lfhe;


# instance fields
.field private crA:Lfhn;

.field private crB:Lfhi;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private crv:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private crw:Landroid/util/SparseArray;

.field private crx:Landroid/view/ViewGroup;

.field private cry:I

.field private crz:Landroid/widget/Button;

.field private mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 68
    new-instance v0, Lfhn;

    invoke-direct {v0}, Lfhn;-><init>()V

    iput-object v0, p0, Lfhf;->crA:Lfhn;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfhf;->setOrientation(I)V

    invoke-virtual {p0}, Lfhf;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lfhf;->mLayoutInflater:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lfhf;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040196

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f110464

    invoke-virtual {p0, v0}, Lfhf;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lfhf;->crx:Landroid/view/ViewGroup;

    iget-object v0, p0, Lfhf;->crx:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    iput v0, p0, Lfhf;->cry:I

    const v0, 0x7f110465

    invoke-virtual {p0, v0}, Lfhf;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lfhf;->crz:Landroid/widget/Button;

    iget-object v0, p0, Lfhf;->crz:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    return-void
.end method

.method private a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;)Landroid/view/View;
    .locals 4

    .prologue
    .line 165
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v2

    .line 167
    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lfqk;->F(IZ)Ljava/lang/Integer;

    move-result-object v0

    .line 170
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Can\'t build view for invalid question type: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lifv;->o(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    iget-object v1, p0, Lfhf;->mLayoutInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 174
    invoke-virtual {p0}, Lfhf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v1, v0}, Lfqk;->a(Landroid/view/View;Landroid/content/res/Resources;)V

    move-object v0, v1

    .line 176
    check-cast v0, Lfqb;

    .line 177
    invoke-interface {v0, v2}, Lfqb;->b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;)V

    .line 178
    new-instance v2, Lfhg;

    invoke-direct {v2, p0, p1}, Lfhg;-><init>(Lfhf;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;)V

    invoke-interface {v0, v2}, Lfqb;->a(Lfqc;)V

    .line 200
    return-object v1
.end method

.method static synthetic a(Lfhf;)Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lfhf;->crv:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    return-object v0
.end method

.method private static a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 154
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v1

    invoke-static {v1, v0}, Lfqk;->F(IZ)Ljava/lang/Integer;

    move-result-object v1

    .line 157
    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private azU()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 288
    iget-object v1, p0, Lfhf;->crv:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    invoke-virtual {v1}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->azR()I

    move-result v1

    .line 289
    iget-object v3, p0, Lfhf;->crv:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    invoke-virtual {v3}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->azS()I

    move-result v3

    .line 291
    if-lez v1, :cond_0

    invoke-static {v3, v1}, Lfqk;->aW(II)I

    move-result v1

    if-le v1, v0, :cond_0

    move v1, v0

    .line 294
    :goto_0
    iget-object v3, p0, Lfhf;->crz:Landroid/widget/Button;

    if-eqz v1, :cond_1

    move v0, v2

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 296
    iget-object v0, p0, Lfhf;->crx:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 297
    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget v4, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget v5, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    if-eqz v1, :cond_2

    :goto_2
    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 301
    return-void

    :cond_0
    move v1, v2

    .line 291
    goto :goto_0

    .line 294
    :cond_1
    const/16 v0, 0x8

    goto :goto_1

    .line 297
    :cond_2
    iget v2, p0, Lfhf;->cry:I

    goto :goto_2
.end method

.method static synthetic b(Lfhf;)Lfhi;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lfhf;->crB:Lfhi;

    return-object v0
.end method

.method static synthetic c(Lfhf;)Lfhn;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lfhf;->crA:Lfhn;

    return-object v0
.end method

.method private d(ILandroid/view/View;)V
    .locals 4

    .prologue
    .line 267
    iget-object v0, p0, Lfhf;->crx:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 268
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 270
    invoke-virtual {p0}, Lfhf;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f05000c

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 272
    iget-object v3, p0, Lfhf;->crA:Lfhn;

    invoke-static {v3, v1, v0, v1}, Lfhh;->a(Lfhn;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;)Lfhh;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 274
    iget-object v3, p0, Lfhf;->crA:Lfhn;

    invoke-virtual {v3}, Lfhn;->azV()V

    .line 276
    invoke-virtual {v1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 278
    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 279
    invoke-virtual {p0}, Lfhf;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f05000b

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 281
    iget-object v1, p0, Lfhf;->crA:Lfhn;

    invoke-static {v1, p2}, Lfhh;->a(Lfhn;Landroid/view/View;)Lfhh;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 283
    iget-object v1, p0, Lfhf;->crA:Lfhn;

    invoke-virtual {v1}, Lfhn;->azV()V

    .line 284
    invoke-virtual {p2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 285
    return-void
.end method

.method private fn(Z)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 113
    iget-object v0, p0, Lfhf;->crv:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    invoke-virtual {v0}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->azR()I

    move-result v4

    .line 114
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v4, :cond_7

    .line 115
    iget-object v0, p0, Lfhf;->crv:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->iM(I)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_1

    new-instance v1, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    .line 119
    :goto_1
    if-eqz v1, :cond_0

    iget-object v5, p0, Lfhf;->crw:Landroid/util/SparseArray;

    invoke-virtual {v5, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/sidekick/shared/training/QuestionKey;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 123
    :cond_0
    :goto_2
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v5

    invoke-static {v5}, Lfhf;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 128
    iget-object v0, p0, Lfhf;->crv:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->iN(I)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    move-result-object v0

    goto :goto_2

    :cond_1
    move-object v1, v2

    .line 116
    goto :goto_1

    .line 131
    :cond_2
    if-eqz v0, :cond_6

    .line 132
    invoke-direct {p0, v0}, Lfhf;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;)Landroid/view/View;

    move-result-object v5

    .line 133
    invoke-direct {p0, v3}, Lfhf;->iO(I)I

    move-result v0

    .line 134
    const/4 v6, -0x1

    if-ne v0, v6, :cond_5

    .line 138
    iget-object v0, p0, Lfhf;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f040197

    invoke-virtual {v0, v6, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "TAG_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v6, p0, Lfhf;->crx:Landroid/view/ViewGroup;

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lfhf;->getContext()Landroid/content/Context;

    move-result-object v0

    const v6, 0x7f05000b

    invoke-static {v0, v6}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v6, p0, Lfhf;->crA:Lfhn;

    invoke-static {v6, v5}, Lfhh;->a(Lfhn;Landroid/view/View;)Lfhh;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v6, p0, Lfhf;->crA:Lfhn;

    invoke-virtual {v6}, Lfhn;->azV()V

    invoke-virtual {v5, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 142
    :cond_3
    :goto_3
    iget-object v0, p0, Lfhf;->crw:Landroid/util/SparseArray;

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 114
    :cond_4
    :goto_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    .line 140
    :cond_5
    invoke-direct {p0, v0, v5}, Lfhf;->d(ILandroid/view/View;)V

    goto :goto_3

    .line 145
    :cond_6
    invoke-direct {p0, v3}, Lfhf;->iP(I)V

    .line 146
    iget-object v0, p0, Lfhf;->crw:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_4

    .line 150
    :cond_7
    invoke-direct {p0}, Lfhf;->azU()V

    .line 151
    return-void
.end method

.method private iO(I)I
    .locals 3

    .prologue
    .line 223
    iget-object v0, p0, Lfhf;->crx:Landroid/view/ViewGroup;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TAG_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lfhf;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method private iP(I)V
    .locals 5

    .prologue
    .line 247
    invoke-direct {p0, p1}, Lfhf;->iO(I)I

    move-result v0

    .line 249
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 252
    iget-object v1, p0, Lfhf;->crx:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 253
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 254
    invoke-virtual {p0}, Lfhf;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f05000c

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 257
    iget-object v3, p0, Lfhf;->crA:Lfhn;

    iget-object v4, p0, Lfhf;->crx:Landroid/view/ViewGroup;

    invoke-static {v3, v1, v4, v0}, Lfhh;->a(Lfhn;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;)Lfhh;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 259
    iget-object v0, p0, Lfhf;->crA:Lfhn;

    invoke-virtual {v0}, Lfhn;->azV()V

    .line 260
    invoke-virtual {v1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 262
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;)V
    .locals 1
    .param p1    # Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 94
    iget-object v0, p0, Lfhf;->crv:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lfhf;->crv:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    invoke-virtual {v0, p0}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->b(Lfhe;)V

    .line 97
    :cond_0
    iput-object p1, p0, Lfhf;->crv:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    .line 98
    iget-object v0, p0, Lfhf;->crx:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 99
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lfhf;->crw:Landroid/util/SparseArray;

    .line 100
    iget-object v0, p0, Lfhf;->crv:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lfhf;->crv:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    invoke-virtual {v0, p0}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->a(Lfhe;)V

    .line 102
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lfhf;->fn(Z)V

    .line 104
    :cond_1
    return-void
.end method

.method public final a(Lfhi;)V
    .locals 0
    .param p1    # Lfhi;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 77
    iput-object p1, p0, Lfhf;->crB:Lfhi;

    .line 78
    return-void
.end method

.method public final notifyChanged()V
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lfhf;->fn(Z)V

    .line 109
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 306
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lfhf;->crv:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    invoke-virtual {v1}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->azR()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 307
    iget-object v1, p0, Lfhf;->crv:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->iM(I)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 308
    iget-object v1, p0, Lfhf;->crv:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->iN(I)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    move-result-object v1

    :goto_1
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v2

    invoke-static {v2}, Lfhf;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, p0, Lfhf;->crv:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->iN(I)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    move-result-object v1

    goto :goto_1

    :cond_0
    if-nez v1, :cond_2

    invoke-direct {p0, v0}, Lfhf;->iP(I)V

    iget-object v1, p0, Lfhf;->crw:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->remove(I)V

    .line 306
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 308
    :cond_2
    invoke-direct {p0, v1}, Lfhf;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0}, Lfhf;->iO(I)I

    move-result v3

    invoke-direct {p0, v3, v2}, Lfhf;->d(ILandroid/view/View;)V

    iget-object v2, p0, Lfhf;->crw:Landroid/util/SparseArray;

    new-instance v3, Lcom/google/android/sidekick/shared/training/QuestionKey;

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v1

    invoke-direct {v3, v1}, Lcom/google/android/sidekick/shared/training/QuestionKey;-><init>(Ljde;)V

    invoke-virtual {v2, v0, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_2

    .line 311
    :cond_3
    invoke-direct {p0}, Lfhf;->azU()V

    .line 312
    iget-object v0, p0, Lfhf;->crB:Lfhi;

    if-eqz v0, :cond_4

    .line 313
    iget-object v0, p0, Lfhf;->crB:Lfhi;

    .line 315
    :cond_4
    return-void
.end method
