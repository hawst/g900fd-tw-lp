.class public final Lfzu;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 5

    .prologue
    .line 31
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    .line 32
    iget-object v1, p0, Lfro;->cBc:Lanh;

    .line 33
    iget-object v2, v1, Lanh;->agB:Lapv;

    .line 35
    invoke-virtual {v2}, Lapv;->tv()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 36
    const v3, 0x7f110082

    invoke-virtual {v2}, Lapv;->getLocation()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 39
    :cond_0
    invoke-virtual {v2}, Lapv;->ty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 40
    const v3, 0x7f11037d

    invoke-virtual {v2}, Lapv;->tx()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 43
    :cond_1
    invoke-virtual {v2}, Lapv;->tA()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 44
    const v3, 0x7f11037b

    invoke-virtual {v2}, Lapv;->tz()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 47
    :cond_2
    invoke-virtual {v2}, Lapv;->tB()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 48
    const v3, 0x7f110380

    invoke-virtual {v2}, Lapv;->tt()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 51
    :cond_3
    invoke-virtual {v2}, Lapv;->tC()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 52
    const v3, 0x7f110381

    invoke-virtual {v2}, Lapv;->tu()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 55
    :cond_4
    invoke-virtual {v2}, Lapv;->tB()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v2}, Lapv;->tC()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 56
    :cond_5
    const v2, 0x7f11037f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 59
    :cond_6
    const v2, 0x7f11037c

    iget-object v1, v1, Lanh;->agB:Lapv;

    invoke-virtual {v1}, Lapv;->tw()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v2, v1}, Lfzu;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 61
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 26
    const v0, 0x7f040149

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
