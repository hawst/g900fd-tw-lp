.class public Lguz;
.super Lgxi;
.source "PG"

# interfaces
.implements Lddc;
.implements Lemy;


# instance fields
.field private final aMD:Leqo;

.field final cXj:Ljava/util/List;

.field private cXk:Libv;

.field private cXl:Z

.field private cXm:Z

.field private cXn:Z

.field cXo:I

.field cXp:Z

.field private final mContext:Landroid/content/Context;

.field final mCoreSearchServices:Lcfo;

.field mDiscoveryState:Ldbj;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/ui/MainContentView;Landroid/content/Context;Lcfo;Leqo;)V
    .locals 1

    .prologue
    .line 78
    const-string v0, "actiondiscovery"

    invoke-direct {p0, v0, p1}, Lgxi;-><init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentView;)V

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lguz;->cXj:Ljava/util/List;

    .line 79
    iput-object p2, p0, Lguz;->mContext:Landroid/content/Context;

    .line 80
    iput-object p3, p0, Lguz;->mCoreSearchServices:Lcfo;

    .line 81
    iput-object p4, p0, Lguz;->aMD:Leqo;

    .line 82
    return-void
.end method

.method private Wq()V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lguz;->mDiscoveryState:Ldbj;

    invoke-virtual {v0}, Ldbj;->Wq()V

    .line 222
    iget-boolean v0, p0, Lguz;->cXp:Z

    if-eqz v0, :cond_0

    .line 223
    const/4 v0, 0x0

    iput-boolean v0, p0, Lguz;->cXp:Z

    .line 224
    const/16 v0, 0xd0

    invoke-static {v0}, Lege;->ht(I)V

    .line 226
    :cond_0
    return-void
.end method

.method private aKN()V
    .locals 1

    .prologue
    .line 262
    new-instance v0, Lgvc;

    invoke-direct {v0, p0}, Lgvc;-><init>(Lguz;)V

    invoke-virtual {p0, v0}, Lguz;->a(Lesj;)V

    .line 269
    const/4 v0, 0x0

    iput-boolean v0, p0, Lguz;->cXl:Z

    .line 270
    return-void
.end method

.method private aKO()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 276
    iput-boolean v0, p0, Lguz;->cXn:Z

    .line 277
    iput-boolean v0, p0, Lguz;->cXm:Z

    .line 278
    iput-boolean v0, p0, Lguz;->cXp:Z

    .line 279
    iget-object v0, p0, Lguz;->cXj:Ljava/util/List;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lgxs;

    const-string v2, "removeViews"

    invoke-direct {v1, p0, v2, v0, v0}, Lgxs;-><init>(Lgxi;Ljava/lang/String;Ljava/lang/Object;Lijj;)V

    invoke-virtual {p0, v1}, Lgxi;->a(Lesj;)V

    .line 280
    iget-object v0, p0, Lguz;->cXj:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 281
    return-void
.end method

.method private fU(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 118
    invoke-static {}, Lenu;->auR()V

    .line 119
    iget-object v2, p0, Lguz;->mCoreSearchServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->Ef()Lgpc;

    move-result-object v2

    .line 120
    invoke-virtual {v2}, Lgpc;->auB()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 123
    invoke-virtual {v2}, Lgpc;->aIX()Ljli;

    move-result-object v3

    .line 128
    if-eqz v3, :cond_1

    .line 129
    iget-boolean v2, p0, Lguz;->cXm:Z

    if-nez v2, :cond_4

    .line 130
    iput-boolean v0, p0, Lguz;->cXm:Z

    move v2, v0

    .line 134
    :goto_0
    if-eqz p1, :cond_3

    iget-boolean v4, p0, Lguz;->cXn:Z

    if-nez v4, :cond_3

    .line 135
    iput-boolean v0, p0, Lguz;->cXn:Z

    .line 139
    :goto_1
    if-nez v2, :cond_0

    if-eqz v0, :cond_1

    .line 140
    :cond_0
    invoke-virtual {p0, v3, v2, v0}, Lguz;->a(Ljli;ZZ)V

    .line 147
    :cond_1
    :goto_2
    return-void

    .line 145
    :cond_2
    invoke-virtual {v2, p0}, Lgpc;->e(Lemy;)V

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v2, v1

    goto :goto_0
.end method


# virtual methods
.method public final Bd()I
    .locals 1

    .prologue
    .line 231
    const/16 v0, 0x10

    return v0
.end method

.method protected final X(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p0}, Lguz;->aLH()Lgyz;

    move-result-object v0

    iget-object v0, v0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DD()Lcjs;

    move-result-object v0

    invoke-virtual {v0}, Lcjs;->LG()Z

    move-result v0

    invoke-virtual {p0, v0}, Lguz;->fY(Z)V

    .line 170
    invoke-virtual {p0}, Lguz;->aLH()Lgyz;

    move-result-object v0

    iget-object v0, v0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DD()Lcjs;

    move-result-object v0

    invoke-virtual {v0}, Lcjs;->LF()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    iput v0, p0, Lguz;->cXo:I

    .line 172
    invoke-virtual {p0}, Lguz;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aas()Ldbj;

    move-result-object v0

    iput-object v0, p0, Lguz;->mDiscoveryState:Ldbj;

    .line 173
    invoke-virtual {p0, p0}, Lguz;->a(Lddc;)V

    .line 174
    return-void

    .line 170
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lhld;)Landroid/view/View;
    .locals 2

    .prologue
    .line 100
    new-instance v0, Lhlb;

    iget-object v1, p0, Lguz;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lhlb;-><init>(Landroid/content/Context;)V

    .line 102
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    invoke-virtual {v1}, Lgql;->anM()Lesm;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhlb;->a(Lesm;)V

    .line 103
    new-instance v1, Lgva;

    invoke-direct {v1, p0, p1}, Lgva;-><init>(Lguz;Lhld;)V

    invoke-virtual {v0, v1}, Lhlb;->e(Landroid/view/View$OnClickListener;)V

    .line 109
    invoke-virtual {p1, v0}, Lhld;->a(Lhle;)V

    .line 110
    return-object v0
.end method

.method protected final a(Landroid/view/View;Lejm;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 189
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lguz;->cXl:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lguz;->mDiscoveryState:Ldbj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lguz;->mDiscoveryState:Ldbj;

    invoke-virtual {v0}, Ldbj;->Wo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lguz;->mDiscoveryState:Ldbj;

    invoke-virtual {v0}, Ldbj;->Wp()V

    .line 194
    iget-object v0, p0, Lgyy;->mPresenter:Lgyz;

    invoke-virtual {v0}, Lgyz;->wj()Lekf;

    move-result-object v0

    invoke-interface {v0}, Lekf;->ate()I

    move-result v0

    invoke-interface {p2}, Lejm;->atB()Lcom/google/android/shared/ui/SuggestionGridLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    const v1, 0x7f0d01b5

    invoke-virtual {p0, v1}, Lguz;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    const v1, 0x7f0d00c7

    invoke-virtual {p0, v1}, Lguz;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-interface {p2, v0}, Lejm;->hQ(I)V

    .line 198
    iput-boolean v2, p0, Lguz;->cXl:Z

    .line 201
    const/16 v0, 0xcf

    invoke-static {v0}, Lege;->ht(I)V

    .line 203
    :cond_0
    return-void
.end method

.method final a(Ldbj;)V
    .locals 2

    .prologue
    .line 241
    invoke-virtual {p1}, Ldbj;->Wl()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    sget-object v0, Lgxi;->cYt:Lesj;

    invoke-virtual {p0, v0}, Lgxi;->a(Lesj;)V

    .line 244
    invoke-direct {p0}, Lguz;->aKO()V

    .line 255
    :goto_0
    return-void

    .line 245
    :cond_0
    invoke-virtual {p1}, Ldbj;->Wm()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    sget-object v0, Lgxi;->cYt:Lesj;

    invoke-virtual {p0, v0}, Lgxi;->a(Lesj;)V

    .line 248
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lguz;->fU(Z)V

    goto :goto_0

    .line 249
    :cond_1
    invoke-virtual {p1}, Ldbj;->Wn()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 251
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lguz;->fU(Z)V

    goto :goto_0

    .line 253
    :cond_2
    const-string v0, "Velvet.ActionDiscoveryPresenter"

    const-string v1, "Unhandled unknown discovery state!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Lddb;)V
    .locals 1

    .prologue
    .line 236
    invoke-virtual {p1}, Lddb;->aaA()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 237
    invoke-virtual {p1}, Lddb;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aas()Ldbj;

    move-result-object v0

    invoke-virtual {p0, v0}, Lguz;->a(Ldbj;)V

    .line 238
    return-void
.end method

.method public final a(Letj;)V
    .locals 2

    .prologue
    .line 402
    invoke-super {p0, p1}, Lgxi;->a(Letj;)V

    .line 403
    const-string v0, "ActionDiscoveryPresenter"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 404
    const-string v0, "mMarginApplied"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lguz;->cXl:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 405
    const-string v0, "DiscoveryState"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lguz;->mDiscoveryState:Ldbj;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 406
    return-void
.end method

.method protected a(Ljli;ZZ)V
    .locals 2

    .prologue
    .line 397
    new-instance v0, Lgve;

    invoke-direct {v0, p0, p1, p2, p3}, Lgve;-><init>(Lguz;Ljli;ZZ)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lgve;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 398
    return-void
.end method

.method public final a(Ljli;)Z
    .locals 3

    .prologue
    .line 153
    if-eqz p1, :cond_0

    .line 154
    iget-object v0, p0, Lguz;->aMD:Leqo;

    new-instance v1, Lgvb;

    const-string v2, "Discovery state changed"

    invoke-direct {v1, p0, v2}, Lgvb;-><init>(Lguz;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 163
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected final aKF()V
    .locals 1

    .prologue
    .line 178
    invoke-virtual {p0, p0}, Lguz;->b(Lddc;)V

    .line 179
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lguz;->fY(Z)V

    .line 180
    invoke-direct {p0}, Lguz;->aKO()V

    .line 181
    iget-boolean v0, p0, Lguz;->cXl:Z

    if-eqz v0, :cond_0

    .line 182
    invoke-direct {p0}, Lguz;->aKN()V

    .line 184
    :cond_0
    sget-object v0, Lgxi;->cYt:Lesj;

    invoke-virtual {p0, v0}, Lgxi;->a(Lesj;)V

    .line 185
    const/4 v0, 0x0

    iput-object v0, p0, Lguz;->mDiscoveryState:Ldbj;

    .line 186
    return-void
.end method

.method final aKM()Libv;
    .locals 3

    .prologue
    .line 85
    iget-object v0, p0, Lguz;->cXk:Libv;

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Libv;

    new-instance v1, Lghd;

    iget-object v2, p0, Lguz;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, Lghd;-><init>(Landroid/content/ContentResolver;)V

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Libv;-><init>(Lghd;Z)V

    iput-object v0, p0, Lguz;->cXk:Libv;

    .line 90
    :cond_0
    iget-object v0, p0, Lguz;->cXk:Libv;

    return-object v0
.end method

.method final aKP()Z
    .locals 1

    .prologue
    .line 284
    invoke-virtual {p0}, Lguz;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lguz;->mDiscoveryState:Ldbj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lguz;->mDiscoveryState:Ldbj;

    invoke-virtual {v0}, Ldbj;->Wl()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic aj(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 56
    check-cast p1, Ljli;

    invoke-virtual {p0, p1}, Lguz;->a(Ljli;)Z

    move-result v0

    return v0
.end method

.method protected b(Lcom/google/android/search/shared/actions/HelpAction;)Lhld;
    .locals 2

    .prologue
    .line 95
    new-instance v0, Lhld;

    iget-object v1, p0, Lguz;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    invoke-direct {v0, p1, v1}, Lhld;-><init>(Lcom/google/android/search/shared/actions/HelpAction;Z)V

    return-object v0
.end method

.method public final fT(Z)V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lguz;->mDiscoveryState:Ldbj;

    if-eqz v0, :cond_1

    .line 208
    invoke-virtual {p0}, Lguz;->aKQ()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 209
    invoke-direct {p0}, Lguz;->Wq()V

    .line 212
    :cond_0
    iget-boolean v0, p0, Lguz;->cXl:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 213
    invoke-direct {p0}, Lguz;->aKN()V

    .line 214
    invoke-direct {p0}, Lguz;->Wq()V

    .line 217
    :cond_1
    return-void
.end method
