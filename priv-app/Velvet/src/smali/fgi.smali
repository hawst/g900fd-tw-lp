.class public final Lfgi;
.super Lfeu;
.source "PG"


# instance fields
.field private final cqP:Ljdx;

.field private final mClock:Lemp;

.field private final mReminderSmartActionUtil:Leqm;


# direct methods
.method public constructor <init>(Lizj;Lemp;Leqm;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lfeu;-><init>(Lizj;)V

    .line 36
    iget-object v0, p1, Lizj;->dSU:Ljdx;

    iput-object v0, p0, Lfgi;->cqP:Ljdx;

    .line 37
    iput-object p2, p0, Lfgi;->mClock:Lemp;

    .line 38
    iput-object p3, p0, Lfgi;->mReminderSmartActionUtil:Leqm;

    .line 39
    return-void
.end method

.method public static c(Ljcg;)Z
    .locals 2

    .prologue
    .line 114
    invoke-virtual {p0}, Ljcg;->getType()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Ljcg;->getType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lfgi;->cqP:Ljdx;

    invoke-virtual {v0}, Ljdx;->bhW()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aH(Landroid/content/Context;)Landroid/content/Intent;
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 120
    iget-object v0, p0, Lfgi;->cqP:Ljdx;

    iget-object v0, v0, Ljdx;->aeB:Ljbp;

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, p0, Lfgi;->cqP:Ljdx;

    iget-object v0, v0, Ljdx;->aeB:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    .line 122
    iget-object v1, p0, Lfgi;->cqP:Ljdx;

    iget-object v1, v1, Ljdx;->aeB:Ljbp;

    invoke-virtual {v1}, Ljbp;->bff()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    iget-object v1, p0, Lfgi;->cqP:Ljdx;

    iget-object v1, v1, Ljdx;->aeB:Ljbp;

    invoke-virtual {v1}, Ljbp;->bfe()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 132
    :cond_0
    :goto_0
    new-instance v1, Lffz;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0496

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v6

    iget-object v0, p0, Lfgi;->cqP:Ljdx;

    invoke-virtual {v0}, Ljdx;->bhW()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lffz;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lfgi;->cqP:Ljdx;

    iget-object v0, v0, Ljdx;->aeB:Ljbp;

    invoke-virtual {v1, v0, p1}, Lffz;->a(Ljbp;Landroid/content/Context;)Lffz;

    move-result-object v0

    invoke-virtual {v0}, Lffz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 149
    :goto_1
    return-object v0

    .line 125
    :pswitch_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0082

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 128
    :pswitch_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0172

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 137
    :cond_1
    iget-object v0, p0, Lfgi;->cqP:Ljdx;

    invoke-virtual {v0}, Ljdx;->bib()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 138
    iget-object v0, p0, Lfgi;->cqP:Ljdx;

    iget-object v1, p0, Lfgi;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {p1, v0, v2, v3}, Lfwf;->b(Landroid/content/Context;Ljdx;J)Ljava/lang/CharSequence;

    move-result-object v0

    .line 140
    if-nez v0, :cond_2

    .line 141
    iget-object v0, p0, Lfgi;->cqP:Ljdx;

    invoke-virtual {v0}, Ljdx;->bia()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-static {p1, v0, v1, v5}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 145
    :cond_2
    new-instance v1, Lffz;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0497

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v6

    iget-object v0, p0, Lfgi;->cqP:Ljdx;

    invoke-virtual {v0}, Ljdx;->bhW()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lffz;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lffz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 149
    :cond_3
    new-instance v0, Lffz;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0498

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lfgi;->cqP:Ljdx;

    invoke-virtual {v4}, Ljdx;->bhW()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lffz;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lffz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 123
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final aI(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 77
    iget-object v0, p0, Lfgi;->cqP:Ljdx;

    iget-object v1, p0, Lfgi;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {p1, v0, v2, v3}, Lfwf;->a(Landroid/content/Context;Ljdx;J)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final ayR()Z
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0}, Lfgi;->getEntry()Lizj;

    move-result-object v0

    iget-object v0, v0, Lizj;->dUr:Ljcg;

    invoke-static {v0}, Lfgi;->c(Ljcg;)Z

    move-result v0

    return v0
.end method

.method public final ayY()I
    .locals 1

    .prologue
    .line 83
    const v0, 0x7f0201cd

    return v0
.end method

.method public final ayZ()Lfgb;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lfgi;->ayQ()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lfgb;->cqz:Lfgb;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lfgb;->cqE:Lfgb;

    goto :goto_0
.end method

.method public final azE()Ljava/util/List;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-virtual {p0}, Lfgi;->getEntry()Lizj;

    move-result-object v2

    .line 44
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 45
    iget-object v0, v2, Lizj;->dUu:Ljdv;

    if-eqz v0, :cond_1

    .line 46
    iget-object v0, v2, Lizj;->dUu:Ljdv;

    invoke-virtual {v0}, Ljdv;->bhP()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    new-instance v0, Lfgk;

    invoke-virtual {p0}, Lfgi;->getEntry()Lizj;

    move-result-object v4

    invoke-direct {v0, v4}, Lfgk;-><init>(Lizj;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    :cond_0
    iget-object v0, v2, Lizj;->dUu:Ljdv;

    iget-object v4, v0, Ljdv;->ecV:[Ljdw;

    array-length v5, v4

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_1

    aget-object v6, v4, v0

    .line 50
    invoke-virtual {v6}, Ljdw;->getType()I

    move-result v7

    new-array v8, v1, [I

    invoke-static {v2, v7, v8}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 52
    iget-object v7, p0, Lfgi;->mReminderSmartActionUtil:Leqm;

    invoke-virtual {v6}, Ljdw;->getType()I

    move-result v8

    invoke-virtual {v7, v8}, Leqm;->il(I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 56
    invoke-virtual {v6}, Ljdw;->getType()I

    move-result v7

    const/16 v8, 0x94

    if-ne v7, v8, :cond_2

    .line 60
    new-instance v0, Lfgj;

    invoke-direct {v0, v6}, Lfgj;-><init>(Ljdw;)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lfgj;->cqR:Z

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    :cond_1
    return-object v3

    .line 49
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lfgi;->cqP:Ljdx;

    invoke-virtual {v0}, Ljdx;->bhW()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/Iterable;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lfgi;->azE()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
