.class public final Ljlc;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile erf:[Ljlc;


# instance fields
.field private aez:I

.field private dMI:Ljava/lang/String;

.field private erg:[I

.field private erh:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2121
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2122
    const/4 v0, 0x0

    iput v0, p0, Ljlc;->aez:I

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljlc;->erg:[I

    const-string v0, ""

    iput-object v0, p0, Ljlc;->erh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljlc;->dMI:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljlc;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljlc;->eCz:I

    .line 2123
    return-void
.end method

.method public static boL()[Ljlc;
    .locals 2

    .prologue
    .line 2061
    sget-object v0, Ljlc;->erf:[Ljlc;

    if-nez v0, :cond_1

    .line 2062
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 2064
    :try_start_0
    sget-object v0, Ljlc;->erf:[Ljlc;

    if-nez v0, :cond_0

    .line 2065
    const/4 v0, 0x0

    new-array v0, v0, [Ljlc;

    sput-object v0, Ljlc;->erf:[Ljlc;

    .line 2067
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2069
    :cond_1
    sget-object v0, Ljlc;->erf:[Ljlc;

    return-object v0

    .line 2067
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2055
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljlc;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljlc;->erg:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljlc;->erg:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljlc;->erg:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Ljlc;->erg:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Ljsi;->btQ()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljlc;->erg:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Ljlc;->erg:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Ljlc;->erg:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Ljlc;->erg:[I

    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlc;->dMI:Ljava/lang/String;

    iget v0, p0, Ljlc;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlc;->aez:I

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlc;->erh:Ljava/lang/String;

    iget v0, p0, Ljlc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlc;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 2138
    iget-object v0, p0, Ljlc;->erg:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljlc;->erg:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 2139
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljlc;->erg:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 2140
    const/4 v1, 0x1

    iget-object v2, p0, Ljlc;->erg:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Ljsj;->bq(II)V

    .line 2139
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2143
    :cond_0
    iget v0, p0, Ljlc;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2144
    const/4 v0, 0x2

    iget-object v1, p0, Ljlc;->dMI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2146
    :cond_1
    iget v0, p0, Ljlc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 2147
    const/4 v0, 0x3

    iget-object v1, p0, Ljlc;->erh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2149
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2150
    return-void
.end method

.method public final bky()Z
    .locals 1

    .prologue
    .line 2091
    iget v0, p0, Ljlc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final boM()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2080
    iget-object v0, p0, Ljlc;->erh:Ljava/lang/String;

    return-object v0
.end method

.method public final boN()Ljlc;
    .locals 1

    .prologue
    .line 2116
    const-string v0, ""

    iput-object v0, p0, Ljlc;->dMI:Ljava/lang/String;

    .line 2117
    iget v0, p0, Ljlc;->aez:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Ljlc;->aez:I

    .line 2118
    return-object p0
.end method

.method public final getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2102
    iget-object v0, p0, Ljlc;->dMI:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2154
    invoke-super {p0}, Ljsl;->lF()I

    move-result v2

    .line 2155
    iget-object v1, p0, Ljlc;->erg:[I

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljlc;->erg:[I

    array-length v1, v1

    if-lez v1, :cond_3

    move v1, v0

    .line 2157
    :goto_0
    iget-object v3, p0, Ljlc;->erg:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 2158
    iget-object v3, p0, Ljlc;->erg:[I

    aget v3, v3, v0

    .line 2159
    invoke-static {v3}, Ljsj;->sa(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 2157
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2162
    :cond_0
    add-int v0, v2, v1

    .line 2163
    iget-object v1, p0, Ljlc;->erg:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2165
    :goto_1
    iget v1, p0, Ljlc;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2166
    const/4 v1, 0x2

    iget-object v2, p0, Ljlc;->dMI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2169
    :cond_1
    iget v1, p0, Ljlc;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    .line 2170
    const/4 v1, 0x3

    iget-object v2, p0, Ljlc;->erh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2173
    :cond_2
    return v0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public final oP()Z
    .locals 1

    .prologue
    .line 2113
    iget v0, p0, Ljlc;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final wB(Ljava/lang/String;)Ljlc;
    .locals 1

    .prologue
    .line 2083
    if-nez p1, :cond_0

    .line 2084
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2086
    :cond_0
    iput-object p1, p0, Ljlc;->erh:Ljava/lang/String;

    .line 2087
    iget v0, p0, Ljlc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlc;->aez:I

    .line 2088
    return-object p0
.end method

.method public final wC(Ljava/lang/String;)Ljlc;
    .locals 1

    .prologue
    .line 2105
    if-nez p1, :cond_0

    .line 2106
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2108
    :cond_0
    iput-object p1, p0, Ljlc;->dMI:Ljava/lang/String;

    .line 2109
    iget v0, p0, Ljlc;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlc;->aez:I

    .line 2110
    return-object p0
.end method
