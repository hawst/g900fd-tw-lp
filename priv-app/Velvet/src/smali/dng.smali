.class final Ldng;
.super Landroid/webkit/WebViewClient;
.source "PG"


# instance fields
.field private synthetic bDs:Ldnb;


# direct methods
.method constructor <init>(Ldnb;)V
    .locals 0

    .prologue
    .line 912
    iput-object p1, p0, Ldng;->bDs:Ldnb;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Ldyo;Lcom/google/android/shared/search/Query;)Landroid/webkit/WebResourceResponse;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ldyo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1115
    :try_start_0
    invoke-virtual {p2}, Ldyo;->alL()Landroid/webkit/WebResourceResponse;

    move-result-object v0

    .line 1116
    if-eqz v0, :cond_0

    .line 1119
    new-instance v2, Ldnk;

    invoke-virtual {v0}, Landroid/webkit/WebResourceResponse;->getData()Ljava/io/InputStream;

    move-result-object v3

    iget-object v4, p0, Ldng;->bDs:Ldnb;

    iget-object v4, v4, Ldnb;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v5, p0, Ldng;->bDs:Ldnb;

    iget-object v5, v5, Ldnb;->atG:Leqo;

    invoke-direct {v2, p3, v3, v4, v5}, Ldnk;-><init>(Lcom/google/android/shared/search/Query;Ljava/io/InputStream;Lcom/google/android/search/core/state/QueryState;Ljava/util/concurrent/Executor;)V

    .line 1121
    invoke-virtual {v0, v2}, Landroid/webkit/WebResourceResponse;->setData(Ljava/io/InputStream;)V

    .line 1135
    :goto_0
    return-object v0

    .line 1124
    :cond_0
    const-string v0, "GsaWebView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing headers or response: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1125
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, p3, v2}, Ldnb;->a(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/Exception;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move-object v0, v1

    .line 1135
    goto :goto_0

    .line 1127
    :catch_0
    move-exception v0

    .line 1129
    if-eqz p3, :cond_1

    .line 1130
    iget-object v2, p0, Ldng;->bDs:Ldnb;

    invoke-virtual {v2, p1, p3, v0}, Ldnb;->a(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/lang/Exception;)V

    goto :goto_1

    .line 1132
    :cond_1
    const-string v2, "GsaWebView"

    const-string v3, "Could not send exception to QueryState because query == null"

    invoke-static {v2, v3, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method


# virtual methods
.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 928
    const-string v0, "about:blank"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 929
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    invoke-virtual {v0, p2}, Ldnb;->ka(Ljava/lang/String;)V

    .line 931
    :cond_0
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    invoke-static {v0, p2}, Ldnb;->a(Ldnb;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 934
    const/16 v0, 0x147

    iget-object v1, p0, Ldng;->bDs:Ldnb;

    invoke-static {v1}, Ldnb;->a(Ldnb;)Legl;

    move-result-object v1

    invoke-static {v0, v1}, Lege;->a(ILegl;)V

    .line 938
    :cond_1
    return-void
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 920
    const-string v0, "about:blank"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 921
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    invoke-virtual {v0, p2}, Ldnb;->jZ(Ljava/lang/String;)V

    .line 923
    :cond_0
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 947
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    iget-object v1, p0, Ldng;->bDs:Ldnb;

    invoke-virtual {v1}, Ldnb;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, p2, v1, p3}, Ldnb;->a(ILcom/google/android/shared/search/Query;Ljava/lang/String;)V

    .line 948
    return-void
.end method

.method public final onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 0

    .prologue
    .line 953
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V

    .line 955
    return-void
.end method

.method public final shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 999
    invoke-static {}, Lgpf;->aIY()V

    .line 1002
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    invoke-static {v0}, Ldnb;->b(Ldnb;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    .line 1003
    :try_start_0
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    invoke-static {v0}, Ldnb;->b(Ldnb;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1008
    if-eqz p2, :cond_a

    const-string v0, "http"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1010
    const-string v0, "/favicon.ico"

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1011
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1012
    invoke-virtual {v0}, Landroid/net/Uri;->isRelative()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Ldng;->bDs:Ldnb;

    iget-object v1, v1, Ldnb;->mUrlHelper:Lcpn;

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcpn;->hH(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "/favicon.ico"

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1015
    :cond_0
    new-instance v0, Landroid/webkit/WebResourceResponse;

    const-string v1, "image/x-icon"

    const-string v2, ""

    new-instance v3, Ljava/io/ByteArrayInputStream;

    sget-object v4, Ldnb;->bCP:[B

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1, v2, v3}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    .line 1100
    :cond_1
    :goto_0
    return-object v0

    .line 1004
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1022
    :cond_2
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    iget-object v0, v0, Ldnb;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Id()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldng;->bDs:Ldnb;

    invoke-static {v0, p2}, Ldnb;->a(Ldnb;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1023
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    iget-object v1, v0, Ldnb;->bDo:Ljava/lang/Object;

    monitor-enter v1

    .line 1025
    :try_start_1
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    invoke-static {v0}, Ldnb;->c(Ldnb;)Ldyo;

    move-result-object v0

    .line 1026
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1027
    if-eqz v0, :cond_3

    .line 1030
    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-direct {p0, p2, v0, v1}, Ldng;->a(Ljava/lang/String;Ldyo;Lcom/google/android/shared/search/Query;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    .line 1031
    if-nez v0, :cond_1

    .line 1044
    :cond_3
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    iget-object v4, v0, Ldnb;->bDg:Ljava/lang/Object;

    monitor-enter v4

    .line 1045
    :try_start_2
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    invoke-virtual {v0, p2}, Ldnb;->jY(Ljava/lang/String;)Z

    move-result v5

    .line 1046
    if-eqz v5, :cond_b

    .line 1047
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    iget-object v3, v0, Ldnb;->bDj:Ldyo;

    .line 1048
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    iget-object v1, v0, Ldnb;->bDl:Lcom/google/android/shared/search/Query;

    .line 1049
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    iget-object v0, v0, Ldnb;->bDk:Lcnb;

    iget-wide v6, v0, Lcnb;->amT:J

    invoke-static {v6, v7}, Leoi;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 1051
    :goto_1
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1054
    if-eqz v3, :cond_7

    .line 1056
    invoke-direct {p0, p2, v3, v1}, Ldng;->a(Ljava/lang/String;Ldyo;Lcom/google/android/shared/search/Query;)Landroid/webkit/WebResourceResponse;

    move-result-object v1

    .line 1057
    if-eqz v1, :cond_5

    .line 1058
    const/16 v3, 0xfe

    invoke-static {v3}, Lege;->hs(I)Litu;

    move-result-object v3

    .line 1060
    if-eqz v0, :cond_4

    .line 1061
    invoke-virtual {v3, v0}, Litu;->pL(Ljava/lang/String;)Litu;

    .line 1063
    :cond_4
    invoke-static {v3}, Lege;->a(Litu;)V

    .line 1066
    :cond_5
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    const/4 v3, 0x0

    invoke-static {v0, v3}, Ldnb;->a(Ldnb;Z)Z

    .line 1067
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "shouldInterceptRequest: returning WebResourceResponse, url hash="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Ldnb;->kb(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1070
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    invoke-static {v0}, Ldnb;->d(Ldnb;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livy;

    .line 1071
    if-eqz v0, :cond_6

    .line 1072
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Livy;->bB(Ljava/lang/Object;)Z

    :cond_6
    move-object v0, v1

    .line 1075
    goto/16 :goto_0

    .line 1026
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1051
    :catchall_2
    move-exception v0

    monitor-exit v4

    throw v0

    .line 1081
    :cond_7
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    invoke-static {v0}, Ldnb;->e(Ldnb;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1082
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "shouldInterceptRequest: ignoring URL, url hash="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Ldnb;->kb(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isLastLoadedUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", WebPage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v3, :cond_9

    const-string v0, "null"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1098
    :cond_8
    :goto_3
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    invoke-virtual {v0}, Ldnb;->adR()V

    move-object v0, v2

    .line 1100
    goto/16 :goto_0

    .line 1082
    :cond_9
    const-string v0, "non-null"

    goto :goto_2

    .line 1089
    :cond_a
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    invoke-static {v0}, Ldnb;->e(Ldnb;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1090
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "shouldInterceptRequest: ignoring url, hash="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Ldnb;->kb(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_b
    move-object v0, v2

    move-object v1, v2

    move-object v3, v2

    goto/16 :goto_1
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 964
    const-string v1, "about:blank"

    invoke-static {v1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 984
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 985
    iget-object v1, p0, Ldng;->bDs:Ldnb;

    iget-object v1, v1, Ldnb;->mWebAppState:Lddk;

    invoke-virtual {v1}, Lddk;->aaP()V

    .line 987
    :cond_1
    return v0

    .line 971
    :cond_2
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 972
    iget-object v2, p0, Ldng;->bDs:Ldnb;

    iget-object v2, v2, Ldnb;->mConfig:Lcjs;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcjs;->gJ(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 974
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    iget-object v0, v0, Ldnb;->bCT:Ldnj;

    invoke-interface {v0}, Ldnj;->adV()V

    .line 975
    const/4 v0, 0x1

    goto :goto_0

    .line 976
    :cond_3
    invoke-virtual {v1}, Landroid/net/Uri;->isRelative()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Ldng;->bDs:Ldnb;

    iget-object v2, v2, Ldnb;->mUrlHelper:Lcpn;

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcpn;->hH(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    iget-object v2, p0, Ldng;->bDs:Ldnb;

    iget-object v2, v2, Ldnb;->mConfig:Lcjs;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcjs;->gK(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 981
    :cond_5
    iget-object v0, p0, Ldng;->bDs:Ldnb;

    invoke-virtual {v0, v1}, Ldnb;->B(Landroid/net/Uri;)Z

    move-result v0

    goto :goto_0
.end method
