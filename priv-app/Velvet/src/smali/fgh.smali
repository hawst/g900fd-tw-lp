.class public final Lfgh;
.super Lfeu;
.source "PG"


# direct methods
.method public constructor <init>(Lizj;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lfeu;-><init>(Lizj;)V

    .line 23
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lfgh;->ayX()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aG(Landroid/content/Context;)Lbk;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-virtual {p0}, Lfgh;->getEntry()Lizj;

    move-result-object v0

    iget-object v2, v0, Lizj;->dSu:Ljdc;

    .line 55
    iget-object v0, v2, Ljdc;->aeB:Ljbp;

    if-eqz v0, :cond_1

    iget-object v0, v2, Ljdc;->aeB:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    .line 57
    :goto_0
    invoke-virtual {v2}, Ljdc;->bai()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Ljdc;->tn()Ljava/lang/String;

    move-result-object v2

    .line 59
    :goto_1
    if-eqz v0, :cond_0

    if-nez v2, :cond_3

    .line 65
    :cond_0
    :goto_2
    return-object v1

    :cond_1
    move-object v0, v1

    .line 55
    goto :goto_0

    :cond_2
    move-object v2, v1

    .line 57
    goto :goto_1

    .line 62
    :cond_3
    new-instance v1, Lba;

    invoke-direct {v1}, Lba;-><init>()V

    .line 63
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<b>"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "</b>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Lba;->g(Ljava/lang/CharSequence;)Lba;

    .line 64
    invoke-virtual {v1, v2}, Lba;->g(Ljava/lang/CharSequence;)Lba;

    goto :goto_2
.end method

.method public final aH(Landroid/content/Context;)Landroid/content/Intent;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 93
    invoke-virtual {p0}, Lfgh;->getEntry()Lizj;

    move-result-object v0

    iget-object v0, v0, Lizj;->dSu:Ljdc;

    .line 94
    iget-object v1, v0, Ljdc;->aeB:Ljbp;

    if-eqz v1, :cond_0

    iget-object v1, v0, Ljdc;->aeB:Ljbp;

    invoke-virtual {v1}, Ljbp;->oK()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    new-instance v1, Lffz;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a049c

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v0, Ljdc;->aeB:Ljbp;

    invoke-virtual {v5}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v0}, Ljdc;->getTitle()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lffz;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Ljdc;->aeB:Ljbp;

    invoke-virtual {v1, v0, p1}, Lffz;->a(Ljbp;Landroid/content/Context;)Lffz;

    move-result-object v0

    invoke-virtual {v0}, Lffz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 102
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lffz;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a049b

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljdc;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lffz;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lffz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public final aI(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-virtual {p0}, Lfgh;->getEntry()Lizj;

    move-result-object v0

    iget-object v2, v0, Lizj;->dSu:Ljdc;

    .line 35
    iget-object v0, v2, Ljdc;->aeB:Ljbp;

    if-eqz v0, :cond_1

    iget-object v0, v2, Ljdc;->aeB:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    .line 37
    :goto_0
    invoke-virtual {v2}, Ljdc;->bai()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Ljdc;->tn()Ljava/lang/String;

    move-result-object v2

    .line 39
    :goto_1
    if-eqz v0, :cond_3

    if-eqz v2, :cond_3

    .line 40
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " &ndash; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 48
    :cond_0
    :goto_2
    return-object v1

    :cond_1
    move-object v0, v1

    .line 35
    goto :goto_0

    :cond_2
    move-object v2, v1

    .line 37
    goto :goto_1

    .line 42
    :cond_3
    if-eqz v0, :cond_4

    .line 43
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<b>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</b>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    goto :goto_2

    .line 45
    :cond_4
    if-eqz v2, :cond_0

    move-object v1, v2

    .line 46
    goto :goto_2
.end method

.method public final ayY()I
    .locals 1

    .prologue
    .line 70
    const v0, 0x7f0202d6

    return v0
.end method

.method public final ayZ()Lfgb;
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lfgh;->ayQ()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lfgb;->cqz:Lfgb;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lfgb;->cqA:Lfgb;

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lfgh;->ayX()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getVisibility()I
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x1

    return v0
.end method
