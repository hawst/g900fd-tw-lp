.class abstract Linn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private synthetic dFd:Limn;

.field private dFl:I

.field private dFm:I

.field private dFn:Lins;

.field private dFo:Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field private dFp:Linr;

.field private dFq:Lioq;

.field private dFr:Lioq;


# direct methods
.method constructor <init>(Limn;)V
    .locals 1

    .prologue
    .line 3656
    iput-object p1, p0, Linn;->dFd:Limn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3657
    iget-object v0, p1, Limn;->dEH:[Lins;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Linn;->dFl:I

    .line 3658
    const/4 v0, -0x1

    iput v0, p0, Linn;->dFm:I

    .line 3659
    invoke-direct {p0}, Linn;->advance()V

    .line 3660
    return-void
.end method

.method private aXN()Z
    .locals 1

    .prologue
    .line 3689
    iget-object v0, p0, Linn;->dFp:Linr;

    if-eqz v0, :cond_1

    .line 3690
    iget-object v0, p0, Linn;->dFp:Linr;

    invoke-interface {v0}, Linr;->aXG()Linr;

    move-result-object v0

    iput-object v0, p0, Linn;->dFp:Linr;

    :goto_0
    iget-object v0, p0, Linn;->dFp:Linr;

    if-eqz v0, :cond_1

    .line 3691
    iget-object v0, p0, Linn;->dFp:Linr;

    invoke-direct {p0, v0}, Linn;->j(Linr;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3692
    const/4 v0, 0x1

    .line 3696
    :goto_1
    return v0

    .line 3690
    :cond_0
    iget-object v0, p0, Linn;->dFp:Linr;

    invoke-interface {v0}, Linr;->aXG()Linr;

    move-result-object v0

    iput-object v0, p0, Linn;->dFp:Linr;

    goto :goto_0

    .line 3696
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private aXO()Z
    .locals 3

    .prologue
    .line 3703
    :cond_0
    iget v0, p0, Linn;->dFm:I

    if-ltz v0, :cond_2

    .line 3704
    iget-object v0, p0, Linn;->dFo:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v1, p0, Linn;->dFm:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Linn;->dFm:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linr;

    iput-object v0, p0, Linn;->dFp:Linr;

    if-eqz v0, :cond_0

    .line 3705
    iget-object v0, p0, Linn;->dFp:Linr;

    invoke-direct {p0, v0}, Linn;->j(Linr;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Linn;->aXN()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3706
    :cond_1
    const/4 v0, 0x1

    .line 3710
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private advance()V
    .locals 3

    .prologue
    .line 3663
    const/4 v0, 0x0

    iput-object v0, p0, Linn;->dFq:Lioq;

    .line 3665
    invoke-direct {p0}, Linn;->aXN()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3683
    :cond_0
    :goto_0
    return-void

    .line 3669
    :cond_1
    invoke-direct {p0}, Linn;->aXO()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3673
    :cond_2
    iget v0, p0, Linn;->dFl:I

    if-ltz v0, :cond_0

    .line 3674
    iget-object v0, p0, Linn;->dFd:Limn;

    iget-object v0, v0, Limn;->dEH:[Lins;

    iget v1, p0, Linn;->dFl:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Linn;->dFl:I

    aget-object v0, v0, v1

    iput-object v0, p0, Linn;->dFn:Lins;

    .line 3675
    iget-object v0, p0, Linn;->dFn:Lins;

    iget v0, v0, Lins;->count:I

    if-eqz v0, :cond_2

    .line 3676
    iget-object v0, p0, Linn;->dFn:Lins;

    iget-object v0, v0, Lins;->dFw:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput-object v0, p0, Linn;->dFo:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3677
    iget-object v0, p0, Linn;->dFo:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Linn;->dFm:I

    .line 3678
    invoke-direct {p0}, Linn;->aXO()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0
.end method

.method private j(Linr;)Z
    .locals 4

    .prologue
    .line 3719
    :try_start_0
    invoke-interface {p1}, Linr;->getKey()Ljava/lang/Object;

    move-result-object v0

    .line 3720
    iget-object v1, p0, Linn;->dFd:Limn;

    invoke-virtual {v1, p1}, Limn;->b(Linr;)Ljava/lang/Object;

    move-result-object v1

    .line 3721
    if-eqz v1, :cond_0

    .line 3722
    new-instance v2, Lioq;

    iget-object v3, p0, Linn;->dFd:Limn;

    invoke-direct {v2, v3, v0, v1}, Lioq;-><init>(Limn;Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v2, p0, Linn;->dFq:Lioq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3723
    iget-object v0, p0, Linn;->dFn:Lins;

    invoke-virtual {v0}, Lins;->aXV()V

    const/4 v0, 0x1

    .line 3726
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Linn;->dFn:Lins;

    invoke-virtual {v0}, Lins;->aXV()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Linn;->dFn:Lins;

    invoke-virtual {v1}, Lins;->aXV()V

    throw v0
.end method


# virtual methods
.method final aXP()Lioq;
    .locals 1

    .prologue
    .line 3738
    iget-object v0, p0, Linn;->dFq:Lioq;

    if-nez v0, :cond_0

    .line 3739
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 3741
    :cond_0
    iget-object v0, p0, Linn;->dFq:Lioq;

    iput-object v0, p0, Linn;->dFr:Lioq;

    .line 3742
    invoke-direct {p0}, Linn;->advance()V

    .line 3743
    iget-object v0, p0, Linn;->dFr:Lioq;

    return-object v0
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 3734
    iget-object v0, p0, Linn;->dFq:Lioq;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 3747
    iget-object v0, p0, Linn;->dFr:Lioq;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 3748
    iget-object v0, p0, Linn;->dFd:Limn;

    iget-object v1, p0, Linn;->dFr:Lioq;

    invoke-virtual {v1}, Lioq;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Limn;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3749
    const/4 v0, 0x0

    iput-object v0, p0, Linn;->dFr:Lioq;

    .line 3750
    return-void

    .line 3747
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
