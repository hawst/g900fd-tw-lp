.class final Lgeu;
.super Lget;
.source "PG"


# direct methods
.method constructor <init>(IILjava/io/InputStream;I)V
    .locals 6

    .prologue
    .line 280
    const-string v1, "ClampedLengthRecordingThread"

    move-object v0, p0

    move-object v2, p3

    move v3, p1

    move v4, p2

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lget;-><init>(Ljava/lang/String;Ljava/io/InputStream;III)V

    .line 281
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const/4 v2, 0x3

    const/4 v8, 0x2

    const/4 v3, -0x1

    const/4 v0, 0x0

    .line 286
    move v1, v0

    .line 290
    :goto_0
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 292
    if-eq v0, v3, :cond_0

    :try_start_1
    iget v4, p0, Lgeu;->ag:I

    if-ne v4, v8, :cond_1

    .line 293
    :cond_0
    iput v1, p0, Lgeu;->cIe:I

    .line 294
    const/4 v0, 0x3

    iput v0, p0, Lgeu;->ag:I

    .line 295
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 328
    iget-object v0, p0, Lgeu;->cIb:Ljava/io/InputStream;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 330
    :goto_1
    return-void

    .line 298
    :cond_1
    add-int v4, v1, v0

    :try_start_2
    iget v5, p0, Lgeu;->cIc:I

    if-le v4, v5, :cond_2

    .line 299
    iget v0, p0, Lgeu;->cIc:I

    iput v0, p0, Lgeu;->cIe:I

    .line 300
    const/4 v0, -0x2

    iput v0, p0, Lgeu;->ag:I

    .line 301
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 328
    iget-object v0, p0, Lgeu;->cIb:Ljava/io/InputStream;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_1

    .line 303
    :cond_2
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 304
    add-int/2addr v0, v1

    .line 306
    :try_start_4
    iget v1, p0, Lgeu;->cIc:I

    if-ge v0, v1, :cond_4

    .line 308
    iget v1, p0, Lgeu;->cHI:I

    add-int/2addr v1, v0

    iget v4, p0, Lgeu;->cIc:I

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 309
    iget-object v4, p0, Lgeu;->cId:[B

    array-length v4, v4

    if-le v1, v4, :cond_3

    .line 310
    mul-int/lit8 v4, v1, 0x2

    iget v5, p0, Lgeu;->cIc:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    new-array v4, v4, [B

    .line 311
    iget-object v5, p0, Lgeu;->cId:[B

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v5, v6, v4, v7, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 312
    iput-object v4, p0, Lgeu;->cId:[B

    .line 315
    :cond_3
    sub-int/2addr v1, v0

    .line 316
    iget-object v4, p0, Lgeu;->cIb:Ljava/io/InputStream;

    iget-object v5, p0, Lgeu;->cId:[B

    invoke-virtual {v4, v5, v0, v1}, Ljava/io/InputStream;->read([BII)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result v1

    move v9, v1

    move v1, v0

    move v0, v9

    .line 317
    goto :goto_0

    .line 303
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit p0

    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 323
    :catch_0
    move-exception v0

    move v0, v1

    :goto_2
    :try_start_6
    monitor-enter p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 324
    :try_start_7
    iput v0, p0, Lgeu;->cIe:I

    .line 325
    iget v0, p0, Lgeu;->ag:I

    if-ne v0, v8, :cond_5

    move v0, v2

    :goto_3
    iput v0, p0, Lgeu;->ag:I

    .line 326
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 328
    iget-object v0, p0, Lgeu;->cIb:Ljava/io/InputStream;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_1

    .line 319
    :cond_4
    :try_start_8
    iget-object v1, p0, Lgeu;->cIb:Ljava/io/InputStream;

    const/4 v4, 0x1

    new-array v4, v4, [B

    invoke-virtual {v1, v4}, Ljava/io/InputStream;->read([B)I
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    move-result v1

    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_0

    :cond_5
    move v0, v3

    .line 325
    goto :goto_3

    .line 326
    :catchall_1
    move-exception v0

    :try_start_9
    monitor-exit p0

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 328
    :catchall_2
    move-exception v0

    iget-object v1, p0, Lgeu;->cIb:Ljava/io/InputStream;

    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    .line 323
    :catch_1
    move-exception v1

    goto :goto_2
.end method
