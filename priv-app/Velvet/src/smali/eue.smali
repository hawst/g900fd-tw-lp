.class public Leue;
.super Lcgl;
.source "PG"


# instance fields
.field final cje:Ljava/util/Map;

.field final mClock:Lemp;

.field private final mLoginHelper:Lcrh;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lerk;Lcrh;Lemp;)V
    .locals 6

    .prologue
    .line 51
    const-string v1, "GmsLocationReportingHelper"

    const-wide/16 v4, 0x7530

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcgl;-><init>(Ljava/lang/String;Landroid/content/Context;Lerk;J)V

    .line 43
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Leue;->cje:Ljava/util/Map;

    .line 52
    iput-object p3, p0, Leue;->mLoginHelper:Lcrh;

    .line 53
    iput-object p4, p0, Leue;->mClock:Lemp;

    .line 54
    return-void
.end method

.method private lz(Ljava/lang/String;)Lcgs;
    .locals 2

    .prologue
    .line 198
    iget-object v1, p0, Leue;->cje:Ljava/util/Map;

    monitor-enter v1

    .line 199
    :try_start_0
    iget-object v0, p0, Leue;->cje:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcgs;->au(Ljava/lang/Object;)Lcgs;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    :goto_0
    return-object v0

    .line 203
    :cond_0
    monitor-exit v1

    .line 205
    new-instance v0, Leui;

    invoke-direct {v0, p0, p1}, Leui;-><init>(Leue;Ljava/lang/String;)V

    const-string v1, "cancelBurstMode"

    invoke-virtual {p0, v0, v1}, Leue;->a(Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcgs;

    move-result-object v0

    goto :goto_0

    .line 203
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final B(Landroid/accounts/Account;)Lcgs;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Leuf;

    invoke-direct {v0, p0, p1}, Leuf;-><init>(Leue;Landroid/accounts/Account;)V

    const-string v1, "tryOptIn"

    invoke-virtual {p0, v0, v1}, Leue;->a(Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method public final C(Landroid/accounts/Account;)Lcgs;
    .locals 2

    .prologue
    .line 73
    new-instance v0, Leug;

    invoke-direct {v0, p0, p1}, Leug;-><init>(Leue;Landroid/accounts/Account;)V

    const-string v1, "getReportingState"

    invoke-virtual {p0, v0, v1}, Leue;->a(Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic a(Landroid/content/Context;Lbgp;Lbgq;)Lbgo;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p1, p2, p3}, Leue;->b(Landroid/content/Context;Lbgp;Lbgq;)Lbse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljbs;)Lcgs;
    .locals 10

    .prologue
    .line 103
    iget-object v0, p0, Leue;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v1

    .line 104
    if-nez v1, :cond_0

    .line 106
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcgs;->au(Ljava/lang/Object;)Lcgs;

    move-result-object v0

    .line 157
    :goto_0
    return-object v0

    .line 109
    :cond_0
    invoke-virtual {p1}, Ljbs;->AD()Ljava/lang/String;

    move-result-object v2

    .line 110
    invoke-static {v2}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid app-specific key"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcgs;->c(Ljava/lang/Throwable;)Lcgs;

    move-result-object v0

    goto :goto_0

    .line 117
    :cond_1
    invoke-virtual {p1}, Ljbs;->bfq()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 118
    invoke-direct {p0, v2}, Leue;->lz(Ljava/lang/String;)Lcgs;

    move-result-object v0

    goto :goto_0

    .line 125
    :cond_2
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Ljbs;->bfr()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    .line 127
    iget-object v0, p0, Leue;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 128
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-gez v0, :cond_3

    .line 130
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcgs;->au(Ljava/lang/Object;)Lcgs;

    move-result-object v0

    goto :goto_0

    .line 133
    :cond_3
    iget-object v0, p0, Leue;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->elapsedRealtime()J

    move-result-wide v6

    add-long/2addr v6, v4

    .line 135
    iget-object v3, p0, Leue;->cje:Ljava/util/Map;

    monitor-enter v3

    .line 136
    :try_start_0
    iget-object v0, p0, Leue;->cje:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leuj;

    .line 137
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    if-eqz v0, :cond_4

    .line 139
    iget-wide v8, v0, Leuj;->cji:J

    cmp-long v0, v6, v8

    if-gtz v0, :cond_4

    .line 143
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcgs;->au(Ljava/lang/Object;)Lcgs;

    move-result-object v0

    goto :goto_0

    .line 137
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 150
    :cond_4
    invoke-virtual {p1}, Ljbs;->getReason()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v4, v5}, Lcom/google/android/gms/location/reporting/UploadRequest;->a(Landroid/accounts/Account;Ljava/lang/String;J)Lbsg;

    move-result-object v0

    iput-object v2, v0, Lbsg;->aIx:Ljava/lang/String;

    invoke-virtual {p1}, Ljbs;->bfs()I

    move-result v1

    int-to-long v2, v1

    iput-wide v2, v0, Lbsg;->aIy:J

    invoke-virtual {p1}, Ljbs;->bft()I

    move-result v1

    int-to-long v2, v1

    iput-wide v2, v0, Lbsg;->aIz:J

    invoke-virtual {v0}, Lbsg;->AE()Lcom/google/android/gms/location/reporting/UploadRequest;

    move-result-object v0

    .line 157
    invoke-virtual {p0, v0}, Leue;->b(Lcom/google/android/gms/location/reporting/UploadRequest;)Lcgs;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final avX()Lcgs;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Leue;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    .line 88
    if-nez v0, :cond_0

    .line 89
    const/4 v0, 0x0

    invoke-static {v0}, Lcgs;->au(Ljava/lang/Object;)Lcgs;

    move-result-object v0

    .line 91
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Leue;->C(Landroid/accounts/Account;)Lcgs;

    move-result-object v0

    goto :goto_0
.end method

.method protected b(Landroid/content/Context;Lbgp;Lbgq;)Lbse;
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lbse;

    invoke-direct {v0, p1, p2, p3}, Lbse;-><init>(Landroid/content/Context;Lbgp;Lbgq;)V

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/location/reporting/UploadRequest;)Lcgs;
    .locals 2

    .prologue
    .line 167
    new-instance v0, Leuh;

    invoke-direct {v0, p0, p1}, Leuh;-><init>(Leue;Lcom/google/android/gms/location/reporting/UploadRequest;)V

    const-string v1, "requestBurstMode"

    invoke-virtual {p0, v0, v1}, Leue;->a(Ljava/util/concurrent/Callable;Ljava/lang/String;)Lcgs;

    move-result-object v0

    return-object v0
.end method

.method final lA(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 224
    iget-object v2, p0, Leue;->cje:Ljava/util/Map;

    monitor-enter v2

    .line 225
    :try_start_0
    iget-object v0, p0, Leue;->cje:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leuj;

    .line 226
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    if-eqz v0, :cond_1

    .line 228
    iget-wide v2, v0, Leuj;->amT:J

    .line 229
    iget-object v0, p0, Lcgl;->aWd:Lbgo;

    check-cast v0, Lbse;

    invoke-virtual {v0, v2, v3}, Lbse;->K(J)I

    move-result v0

    .line 232
    const/16 v2, 0x64

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 240
    :cond_0
    :goto_0
    return v0

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    move v0, v1

    .line 240
    goto :goto_0
.end method
