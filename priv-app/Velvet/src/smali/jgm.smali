.class public final Ljgm;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ekg:[Ljgm;


# instance fields
.field private aez:I

.field public ekh:[Ljava/lang/String;

.field private eki:I

.field private ekj:I

.field private ekk:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 51562
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 51563
    iput v1, p0, Ljgm;->aez:I

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljgm;->ekh:[Ljava/lang/String;

    iput v1, p0, Ljgm;->eki:I

    iput v1, p0, Ljgm;->ekj:I

    iput v1, p0, Ljgm;->ekk:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljgm;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljgm;->eCz:I

    .line 51564
    return-void
.end method

.method public static bkt()[Ljgm;
    .locals 2

    .prologue
    .line 51489
    sget-object v0, Ljgm;->ekg:[Ljgm;

    if-nez v0, :cond_1

    .line 51490
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 51492
    :try_start_0
    sget-object v0, Ljgm;->ekg:[Ljgm;

    if-nez v0, :cond_0

    .line 51493
    const/4 v0, 0x0

    new-array v0, v0, [Ljgm;

    sput-object v0, Ljgm;->ekg:[Ljgm;

    .line 51495
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51497
    :cond_1
    sget-object v0, Ljgm;->ekg:[Ljgm;

    return-object v0

    .line 51495
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 51483
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljgm;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljgm;->ekh:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljgm;->ekh:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljgm;->ekh:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljgm;->ekh:[Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljgm;->eki:I

    iget v0, p0, Ljgm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljgm;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljgm;->ekj:I

    iget v0, p0, Ljgm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljgm;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljgm;->ekk:I

    iget v0, p0, Ljgm;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljgm;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 51580
    iget-object v0, p0, Ljgm;->ekh:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljgm;->ekh:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 51581
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljgm;->ekh:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 51582
    iget-object v1, p0, Ljgm;->ekh:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 51583
    if-eqz v1, :cond_0

    .line 51584
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 51581
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51588
    :cond_1
    iget v0, p0, Ljgm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 51589
    const/4 v0, 0x2

    iget v1, p0, Ljgm;->eki:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 51591
    :cond_2
    iget v0, p0, Ljgm;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 51592
    const/4 v0, 0x3

    iget v1, p0, Ljgm;->ekj:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 51594
    :cond_3
    iget v0, p0, Ljgm;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 51595
    const/4 v0, 0x4

    iget v1, p0, Ljgm;->ekk:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 51597
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 51598
    return-void
.end method

.method public final bku()I
    .locals 1

    .prologue
    .line 51508
    iget v0, p0, Ljgm;->eki:I

    return v0
.end method

.method public final bkv()Z
    .locals 1

    .prologue
    .line 51516
    iget v0, p0, Ljgm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bkw()I
    .locals 1

    .prologue
    .line 51527
    iget v0, p0, Ljgm;->ekj:I

    return v0
.end method

.method public final bkx()I
    .locals 1

    .prologue
    .line 51546
    iget v0, p0, Ljgm;->ekk:I

    return v0
.end method

.method public final bky()Z
    .locals 1

    .prologue
    .line 51554
    iget v0, p0, Ljgm;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 51602
    invoke-super {p0}, Ljsl;->lF()I

    move-result v3

    .line 51603
    iget-object v1, p0, Ljgm;->ekh:[Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Ljgm;->ekh:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_5

    move v1, v0

    move v2, v0

    .line 51606
    :goto_0
    iget-object v4, p0, Ljgm;->ekh:[Ljava/lang/String;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 51607
    iget-object v4, p0, Ljgm;->ekh:[Ljava/lang/String;

    aget-object v4, v4, v0

    .line 51608
    if-eqz v4, :cond_0

    .line 51609
    add-int/lit8 v2, v2, 0x1

    .line 51610
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 51606
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51614
    :cond_1
    add-int v0, v3, v1

    .line 51615
    mul-int/lit8 v1, v2, 0x1

    add-int/2addr v0, v1

    .line 51617
    :goto_1
    iget v1, p0, Ljgm;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    .line 51618
    const/4 v1, 0x2

    iget v2, p0, Ljgm;->eki:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 51621
    :cond_2
    iget v1, p0, Ljgm;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_3

    .line 51622
    const/4 v1, 0x3

    iget v2, p0, Ljgm;->ekj:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 51625
    :cond_3
    iget v1, p0, Ljgm;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_4

    .line 51626
    const/4 v1, 0x4

    iget v2, p0, Ljgm;->ekk:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 51629
    :cond_4
    return v0

    :cond_5
    move v0, v3

    goto :goto_1
.end method

.method public final pC(I)Ljgm;
    .locals 1

    .prologue
    .line 51530
    iput p1, p0, Ljgm;->ekj:I

    .line 51531
    iget v0, p0, Ljgm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljgm;->aez:I

    .line 51532
    return-object p0
.end method

.method public final pD(I)Ljgm;
    .locals 1

    .prologue
    .line 51549
    iput p1, p0, Ljgm;->ekk:I

    .line 51550
    iget v0, p0, Ljgm;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljgm;->aez:I

    .line 51551
    return-object p0
.end method
