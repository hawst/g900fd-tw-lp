.class final Ldle;
.super Ldlg;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 309
    invoke-direct {p0}, Ldlg;-><init>()V

    return-void
.end method

.method private e(Lbzh;[BI)Ljava/lang/String;
    .locals 4
    .param p1    # Lbzh;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 313
    :try_start_0
    invoke-static {p3}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    .line 317
    invoke-static {p1, p2}, Ldle;->a(Lbzh;[B)V

    .line 318
    invoke-static {p1}, Ldkx;->c(Lbzh;)Ljava/nio/charset/Charset;

    move-result-object v0

    .line 320
    invoke-interface {p1}, Lbzh;->getContentLength()I

    move-result v1

    .line 327
    const/16 v2, 0x200

    const/16 v3, 0x2000

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 328
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 329
    const/4 v1, 0x0

    .line 331
    :try_start_1
    invoke-static {p1}, Ldmc;->e(Lbzh;)Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 333
    :try_start_2
    invoke-static {v1, v2}, Leoo;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 339
    :try_start_3
    invoke-static {v1}, Leoo;->i(Ljava/io/InputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 349
    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 352
    invoke-interface {p1}, Lbzh;->disconnect()V

    .line 356
    :try_start_4
    invoke-virtual {v0}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v0

    return-object v0

    .line 334
    :catch_0
    move-exception v0

    .line 335
    :try_start_5
    new-instance v2, Lefs;

    const v3, 0x40009

    invoke-direct {v2, v0, v3}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 339
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v1}, Leoo;->i(Ljava/io/InputStream;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 349
    :catchall_1
    move-exception v0

    invoke-static {}, Landroid/net/TrafficStats;->clearThreadStatsTag()V

    .line 352
    invoke-interface {p1}, Lbzh;->disconnect()V

    throw v0

    .line 357
    :catch_1
    move-exception v0

    .line 358
    new-instance v1, Lefs;

    const v2, 0x4000a

    invoke-direct {v1, v0, v2}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v1
.end method


# virtual methods
.method public final synthetic b(Lbzh;[BI)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 308
    invoke-direct {p0, p1, p2, p3}, Ldle;->e(Lbzh;[BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
