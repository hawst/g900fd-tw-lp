.class public Lfkq;
.super Lfjz;
.source "PG"


# instance fields
.field private final cuf:J

.field private final mClock:Lemp;


# direct methods
.method public constructor <init>(Lfzw;Lemp;J)V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    invoke-direct {p0, v0, p1}, Lfjz;-><init>(Lizj;Lfzw;)V

    .line 32
    iput-object p2, p0, Lfkq;->mClock:Lemp;

    .line 33
    iput-wide p3, p0, Lfkq;->cuf:J

    .line 34
    return-void
.end method

.method private a(Landroid/view/View;Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 53
    iget-object v0, p0, Lfkq;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lfkq;->cuf:J

    sub-long v2, v0, v2

    .line 55
    const/4 v0, 0x0

    .line 56
    const-wide/32 v4, 0x2bf20

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const-wide v4, 0x1cf7c5800L

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 58
    const v0, 0x7f0a04d6

    const/4 v1, 0x0

    invoke-static {p2, v0, v2, v3, v1}, Lesi;->a(Landroid/content/Context;IJZ)Ljava/lang/String;

    move-result-object v0

    .line 61
    :cond_0
    const v1, 0x7f11029a

    invoke-static {p1, v1, v0}, Lgab;->c(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 62
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 39
    const v0, 0x7f0400df

    const/4 v1, 0x0

    invoke-virtual {p3, v0, p4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 41
    invoke-direct {p0, v0, p1}, Lfkq;->a(Landroid/view/View;Landroid/content/Context;)V

    .line 43
    return-object v0
.end method

.method public final a(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p5, p1}, Lfkq;->a(Landroid/view/View;Landroid/content/Context;)V

    .line 50
    return-void
.end method
