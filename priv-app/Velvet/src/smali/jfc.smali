.class public final Ljfc;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private efN:Ljez;

.field private efQ:I

.field private egC:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 12690
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 12691
    const/4 v0, 0x0

    iput v0, p0, Ljfc;->aez:I

    iput-object v2, p0, Ljfc;->efN:Ljez;

    iput v1, p0, Ljfc;->efQ:I

    iput-boolean v1, p0, Ljfc;->egC:Z

    iput-object v2, p0, Ljfc;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljfc;->eCz:I

    .line 12692
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 12630
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljfc;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljfc;->efN:Ljez;

    if-nez v0, :cond_1

    new-instance v0, Ljez;

    invoke-direct {v0}, Ljez;-><init>()V

    iput-object v0, p0, Ljfc;->efN:Ljez;

    :cond_1
    iget-object v0, p0, Ljfc;->efN:Ljez;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfc;->egC:Z

    iget v0, p0, Ljfc;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfc;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljfc;->efQ:I

    iget v0, p0, Ljfc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfc;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 12707
    iget-object v0, p0, Ljfc;->efN:Ljez;

    if-eqz v0, :cond_0

    .line 12708
    const/4 v0, 0x1

    iget-object v1, p0, Ljfc;->efN:Ljez;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 12710
    :cond_0
    iget v0, p0, Ljfc;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 12711
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljfc;->egC:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 12713
    :cond_1
    iget v0, p0, Ljfc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 12714
    const/4 v0, 0x3

    iget v1, p0, Ljfc;->efQ:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 12716
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 12717
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 12721
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 12722
    iget-object v1, p0, Ljfc;->efN:Ljez;

    if-eqz v1, :cond_0

    .line 12723
    const/4 v1, 0x1

    iget-object v2, p0, Ljfc;->efN:Ljez;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12726
    :cond_0
    iget v1, p0, Ljfc;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 12727
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljfc;->egC:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12730
    :cond_1
    iget v1, p0, Ljfc;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    .line 12731
    const/4 v1, 0x3

    iget v2, p0, Ljfc;->efQ:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12734
    :cond_2
    return v0
.end method
