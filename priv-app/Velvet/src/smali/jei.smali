.class public final Ljei;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afX:Ljava/lang/String;

.field private afh:Ljava/lang/String;

.field private eet:Ljhe;

.field public eeu:Ljcn;

.field private eev:Ljhe;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ljei;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljei;->afh:Ljava/lang/String;

    iput-object v1, p0, Ljei;->eet:Ljhe;

    iput-object v1, p0, Ljei;->eeu:Ljcn;

    const-string v0, ""

    iput-object v0, p0, Ljei;->afX:Ljava/lang/String;

    iput-object v1, p0, Ljei;->eev:Ljhe;

    iput-object v1, p0, Ljei;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljei;->eCz:I

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljei;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljei;->afh:Ljava/lang/String;

    iget v0, p0, Ljei;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljei;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljei;->eeu:Ljcn;

    if-nez v0, :cond_1

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljei;->eeu:Ljcn;

    :cond_1
    iget-object v0, p0, Ljei;->eeu:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljei;->afX:Ljava/lang/String;

    iget v0, p0, Ljei;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljei;->aez:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljei;->eet:Ljhe;

    if-nez v0, :cond_2

    new-instance v0, Ljhe;

    invoke-direct {v0}, Ljhe;-><init>()V

    iput-object v0, p0, Ljei;->eet:Ljhe;

    :cond_2
    iget-object v0, p0, Ljei;->eet:Ljhe;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljei;->eev:Ljhe;

    if-nez v0, :cond_3

    new-instance v0, Ljhe;

    invoke-direct {v0}, Ljhe;-><init>()V

    iput-object v0, p0, Ljei;->eev:Ljhe;

    :cond_3
    iget-object v0, p0, Ljei;->eev:Ljhe;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    iget v0, p0, Ljei;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljei;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Ljei;->eeu:Ljcn;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljei;->eeu:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_1
    iget v0, p0, Ljei;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ljei;->afX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_2
    iget-object v0, p0, Ljei;->eet:Ljhe;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ljei;->eet:Ljhe;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_3
    iget-object v0, p0, Ljei;->eev:Ljhe;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Ljei;->eev:Ljhe;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljei;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljei;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljei;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Ljei;->eeu:Ljcn;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljei;->eeu:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Ljei;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Ljei;->afX:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Ljei;->eet:Ljhe;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Ljei;->eet:Ljhe;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Ljei;->eev:Ljhe;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Ljei;->eev:Ljhe;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    return v0
.end method

.method public final pb()Z
    .locals 1

    iget v0, p0, Ljei;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ud(Ljava/lang/String;)Ljei;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljei;->afh:Ljava/lang/String;

    iget v0, p0, Ljei;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljei;->aez:I

    return-object p0
.end method
