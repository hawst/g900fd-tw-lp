.class public final Lckf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcke;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mPrefController:Lchr;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lchr;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    iput-object p1, p0, Lckf;->mContext:Landroid/content/Context;

    .line 140
    iput-object p2, p0, Lckf;->mPrefController:Lchr;

    .line 141
    return-void
.end method

.method private M(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcyh;->B(Ljava/lang/String;Ljava/lang/String;)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 390
    return-void
.end method

.method private declared-synchronized OI()Ljava/lang/String;
    .locals 2

    .prologue
    .line 670
    monitor-enter p0

    :try_start_0
    invoke-static {}, Leoi;->auW()Ljava/lang/String;

    move-result-object v0

    .line 671
    const-string v1, "install-id"

    invoke-direct {p0, v1, v0}, Lckf;->M(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 672
    monitor-exit p0

    return-object v0

    .line 670
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private e(Ljava/lang/String;[B)V
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcyh;->c(Ljava/lang/String;[B)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 394
    return-void
.end method

.method public static f(Ldgb;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 401
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "enable_corpus_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p0}, Ldgb;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static g(Ldgb;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 445
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "override_whitelist_for_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p0}, Ldgb;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static he(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1032
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "-"

    const-string v2, "_"

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "last_hotword_model_downloaded_url"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private i(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 385
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcyh;->c(Ljava/lang/String;J)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 386
    return-void
.end method

.method private j(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 378
    return-void
.end method

.method private n(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcyh;->l(Ljava/lang/String;I)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 382
    return-void
.end method


# virtual methods
.method public final CS()Z
    .locals 3

    .prologue
    .line 948
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "contact_upload_for_communication_actions_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final J(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 531
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "corpora_name_source_stats_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lckf;->M(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    return-void
.end method

.method public final K(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 682
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "action_discovery_data_url_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lckf;->M(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    return-void
.end method

.method public final L(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1021
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    invoke-static {p1}, Lckf;->he(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcyh;->B(Ljava/lang/String;Ljava/lang/String;)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 1023
    return-void
.end method

.method public final NA()Ljava/lang/String;
    .locals 3

    .prologue
    .line 168
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "search_domain_scheme"

    const-string v2, "http"

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final NB()Ljava/lang/String;
    .locals 3

    .prologue
    .line 182
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "search_domain_country_code"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final NC()Ljava/lang/String;
    .locals 3

    .prologue
    .line 189
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "search_language"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ND()Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 261
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    const-string v1, "google_account"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final NE()Z
    .locals 3

    .prologue
    .line 284
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    const-string v1, "signed_out"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final NF()Z
    .locals 3

    .prologue
    .line 194
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "debug_js_injection_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final NG()Ljava/lang/String;
    .locals 3

    .prologue
    .line 207
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "debug_js_server_address"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final NH()Ljava/lang/String;
    .locals 3

    .prologue
    .line 212
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "debug_search_scheme_override"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final NI()Ljava/lang/String;
    .locals 3

    .prologue
    .line 217
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "debug_search_domain_override"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final NJ()Ljava/lang/String;
    .locals 3

    .prologue
    .line 222
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "debug_search_host_param"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final NK()Z
    .locals 3

    .prologue
    .line 245
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "debug_icing_extensive_logging_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final NL()Z
    .locals 3

    .prologue
    .line 407
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "did_user_opt_into_icing_whitelisting"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final NM()Ljava/util/Map;
    .locals 5

    .prologue
    .line 486
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "source_stats_"

    invoke-interface {v0, v1}, Lcyg;->gj(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 490
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v2

    .line 491
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 492
    invoke-interface {v2, v0}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    goto :goto_0

    .line 494
    :cond_0
    invoke-interface {v2}, Lcyh;->apply()V

    .line 496
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v2

    .line 497
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 498
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/16 v4, 0xd

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 499
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 501
    :cond_1
    return-object v2
.end method

.method public final NN()Ljava/util/Map;
    .locals 5

    .prologue
    .line 472
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "corpora_name_source_stats_"

    invoke-interface {v0, v1}, Lcyg;->gj(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 475
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v2

    .line 476
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 477
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/16 v4, 0x1a

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 478
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 480
    :cond_0
    return-object v2
.end method

.method public final NO()Z
    .locals 3

    .prologue
    .line 506
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "need_source_stats_upgrade"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final NP()V
    .locals 2

    .prologue
    .line 511
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "source_stats_"

    invoke-interface {v0, v1}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 512
    return-void
.end method

.method public final NQ()Z
    .locals 3

    .prologue
    .line 516
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "app_launch_log_migration_needed"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final NR()Ljava/lang/String;
    .locals 3

    .prologue
    .line 536
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "zero_query_web_results"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final NS()J
    .locals 4

    .prologue
    .line 566
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "last_launch"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcyg;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final NT()Ljava/lang/String;
    .locals 3

    .prologue
    .line 581
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "user_agent"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final NU()Z
    .locals 4

    .prologue
    .line 641
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "personalized_search_bool"

    iget-object v2, p0, Lckf;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final NV()Ljava/lang/String;
    .locals 3

    .prologue
    .line 651
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "safe_search"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 653
    if-eqz v0, :cond_0

    .line 654
    iget-object v0, p0, Lckf;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0110

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 656
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lckf;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0111

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final NW()[B
    .locals 3

    .prologue
    .line 591
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "web_corpora_config"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->b(Ljava/lang/String;[B)[B

    move-result-object v0

    return-object v0
.end method

.method public final NX()J
    .locals 4

    .prologue
    .line 611
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "refresh_webview_cookies_at"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcyg;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final NY()Ljava/lang/String;
    .locals 3

    .prologue
    .line 621
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "webview_logged_in_account"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final NZ()Ljava/lang/String;
    .locals 3

    .prologue
    .line 631
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "webview_logged_in_domain"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Ny()Z
    .locals 3

    .prologue
    .line 161
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "use_google_com"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final Nz()Ljava/lang/String;
    .locals 3

    .prologue
    .line 175
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "search_domain"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final OA()V
    .locals 2

    .prologue
    .line 1011
    const-string v0, "update_promo_shown_count"

    invoke-virtual {p0}, Lckf;->Oz()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v0, v1}, Lckf;->n(Ljava/lang/String;I)V

    .line 1012
    return-void
.end method

.method public final OB()V
    .locals 2

    .prologue
    .line 1016
    const-string v0, "update_promo_shown_count"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lckf;->n(Ljava/lang/String;I)V

    .line 1017
    return-void
.end method

.method public final OC()Z
    .locals 3

    .prologue
    .line 1042
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "service_created"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final OD()Lcsj;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1063
    iget-object v1, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    const-string v2, "pending_hotword_model_download_info"

    invoke-interface {v1, v2, v0}, Lcyg;->b(Ljava/lang/String;[B)[B

    move-result-object v1

    .line 1064
    if-nez v1, :cond_0

    .line 1071
    :goto_0
    return-object v0

    .line 1068
    :cond_0
    :try_start_0
    invoke-static {v1}, Lcsj;->z([B)Lcsj;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1069
    :catch_0
    move-exception v1

    .line 1070
    const-string v2, "QSB.SearchSettingsImpl"

    const-string v3, "Couldn\'t load HotwordModelDownloadInfo"

    invoke-static {v2, v3, v1}, Leor;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final OE()V
    .locals 2

    .prologue
    .line 1057
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "pending_hotword_model_download_info"

    invoke-interface {v0, v1}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 1058
    return-void
.end method

.method public final OF()Z
    .locals 3

    .prologue
    .line 1084
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "remove_experiment_overrides"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final OG()[I
    .locals 2

    .prologue
    .line 1097
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "server_experiment_ids"

    invoke-interface {v0, v1}, Lcyg;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    return-object v0
.end method

.method public final OH()J
    .locals 4

    .prologue
    .line 1102
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "server_experiment_ids_timestamp"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcyg;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final Oa()V
    .locals 2

    .prologue
    .line 363
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.search.action.SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 365
    iget-object v1, p0, Lckf;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 366
    return-void
.end method

.method public final declared-synchronized Ob()Ljava/lang/String;
    .locals 3

    .prologue
    .line 662
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "install-id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 663
    if-nez v0, :cond_0

    .line 664
    invoke-direct {p0}, Lckf;->OI()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 666
    :cond_0
    monitor-exit p0

    return-object v0

    .line 662
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final Oc()I
    .locals 3

    .prologue
    .line 702
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "voice_action_discovery_instant_peek_count"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final Od()V
    .locals 2

    .prologue
    .line 707
    const-string v0, "voice_action_discovery_instant_peek_count"

    invoke-virtual {p0}, Lckf;->Oc()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v0, v1}, Lckf;->n(Ljava/lang/String;I)V

    .line 709
    return-void
.end method

.method public final Oe()Ljava/lang/String;
    .locals 3

    .prologue
    .line 713
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "hotword_usage_stats"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Of()J
    .locals 4

    .prologue
    .line 723
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "first_hotword_hint_shown_at"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcyg;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final Og()I
    .locals 3

    .prologue
    .line 321
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    const-string v1, "debug_features_level"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final Oh()I
    .locals 3

    .prologue
    .line 300
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    const-string v1, "last_run_version"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Lcyg;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final Oi()Ljava/lang/String;
    .locals 3

    .prologue
    .line 311
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    const-string v1, "last_run_system_build"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Oj()Ljjf;
    .locals 3

    .prologue
    .line 758
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    const-string v1, "gsa_config_server"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->b(Ljava/lang/String;[B)[B

    move-result-object v0

    .line 759
    if-nez v0, :cond_0

    .line 760
    new-instance v0, Ljjf;

    invoke-direct {v0}, Ljjf;-><init>()V

    .line 766
    :goto_0
    return-object v0

    .line 763
    :cond_0
    :try_start_0
    invoke-static {v0}, Ljjf;->ap([B)Ljjf;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 765
    :catch_0
    move-exception v0

    const-string v0, "QSB.SearchSettingsImpl"

    const-string v1, "Couldn\'t load default configuration."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 766
    new-instance v0, Ljjf;

    invoke-direct {v0}, Ljjf;-><init>()V

    goto :goto_0
.end method

.method public final Ok()Ljjf;
    .locals 3

    .prologue
    .line 772
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    const-string v1, "gsa_config_overrides"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->b(Ljava/lang/String;[B)[B

    move-result-object v0

    .line 773
    if-nez v0, :cond_0

    .line 774
    new-instance v0, Ljjf;

    invoke-direct {v0}, Ljjf;-><init>()V

    .line 780
    :goto_0
    return-object v0

    .line 777
    :cond_0
    :try_start_0
    invoke-static {v0}, Ljjf;->ap([B)Ljjf;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 779
    :catch_0
    move-exception v0

    const-string v0, "QSB.SearchSettingsImpl"

    const-string v1, "Couldn\'t load local configuration."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 780
    new-instance v0, Ljjf;

    invoke-direct {v0}, Ljjf;-><init>()V

    goto :goto_0
.end method

.method public final Ol()Ljava/lang/String;
    .locals 3

    .prologue
    .line 791
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    const-string v1, "gsa_config_container_etag"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Om()Lcjd;
    .locals 4
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 804
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "gsa_relationship_contact_info"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->b(Ljava/lang/String;[B)[B

    move-result-object v0

    .line 807
    if-nez v0, :cond_0

    .line 808
    new-instance v0, Lcjd;

    invoke-direct {v0}, Lcjd;-><init>()V

    .line 814
    :goto_0
    return-object v0

    .line 811
    :cond_0
    :try_start_0
    invoke-static {v0}, Lcjd;->o([B)Lcjd;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 813
    :catch_0
    move-exception v0

    const-string v0, "QSB.SearchSettingsImpl"

    const-string v1, "Couldn\'t load relationship contact mapping."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 814
    new-instance v0, Lcjd;

    invoke-direct {v0}, Lcjd;-><init>()V

    goto :goto_0
.end method

.method public final On()Ljtd;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 833
    iget-object v1, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    const-string v2, "gsa_relationship_configuration"

    invoke-interface {v1, v2, v0}, Lcyg;->b(Ljava/lang/String;[B)[B

    move-result-object v1

    .line 836
    if-nez v1, :cond_0

    .line 844
    :goto_0
    return-object v0

    .line 840
    :cond_0
    :try_start_0
    invoke-static {v1}, Ljtd;->ax([B)Ljtd;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 842
    :catch_0
    move-exception v1

    const-string v1, "QSB.SearchSettingsImpl"

    const-string v2, "Couldn\'t load relationship configuration."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final Oo()Ljta;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 862
    iget-object v1, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    const-string v2, "gsa_contact_method_alias_configuration"

    invoke-interface {v1, v2, v0}, Lcyg;->b(Ljava/lang/String;[B)[B

    move-result-object v1

    .line 865
    if-nez v1, :cond_0

    .line 873
    :goto_0
    return-object v0

    .line 869
    :cond_0
    :try_start_0
    invoke-static {v1}, Ljta;->aw([B)Ljta;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 870
    :catch_0
    move-exception v1

    .line 871
    const-string v2, "QSB.SearchSettingsImpl"

    const-string v3, "Couldn\'t load contact method configuration."

    invoke-static {v2, v3, v1}, Leor;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final Op()Lciw;
    .locals 4
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 879
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "person_shortcut_info"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->b(Ljava/lang/String;[B)[B

    move-result-object v0

    .line 880
    if-nez v0, :cond_0

    .line 881
    new-instance v0, Lciw;

    invoke-direct {v0}, Lciw;-><init>()V

    .line 887
    :goto_0
    return-object v0

    .line 884
    :cond_0
    :try_start_0
    invoke-static {v0}, Lciw;->n([B)Lciw;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 886
    :catch_0
    move-exception v0

    const-string v0, "QSB.SearchSettingsImpl"

    const-string v1, "Failed to load PersonShortcutInfo"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 887
    new-instance v0, Lciw;

    invoke-direct {v0}, Lciw;-><init>()V

    goto :goto_0
.end method

.method public final Oq()Ljava/util/Set;
    .locals 3

    .prologue
    .line 901
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "debug_gsa_config_overridable_flags"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final Or()Ljava/lang/String;
    .locals 3

    .prologue
    .line 920
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "last_query"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Os()J
    .locals 4

    .prologue
    .line 925
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "last_query_updated"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcyg;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final Ot()Ljava/lang/String;
    .locals 3

    .prologue
    .line 938
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "last_failed_query"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Ou()J
    .locals 4

    .prologue
    .line 943
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "last_failed_query_updated"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcyg;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final Ov()I
    .locals 3

    .prologue
    .line 961
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "temp_units"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Lcyg;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final Ow()Z
    .locals 3

    .prologue
    .line 980
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "gel_usage_stats"

    iget-object v2, p0, Lckf;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lesp;->az(Landroid/content/Context;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final Ox()[B
    .locals 3

    .prologue
    .line 991
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "action_discovery_suggestions"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->b(Ljava/lang/String;[B)[B

    move-result-object v0

    return-object v0
.end method

.method public final Oy()J
    .locals 4

    .prologue
    .line 996
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "update_promo_last_shown_time"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcyg;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final Oz()I
    .locals 3

    .prologue
    .line 1006
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "update_promo_shown_count"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final U(J)V
    .locals 1

    .prologue
    .line 571
    const-string v0, "last_launch"

    invoke-direct {p0, v0, p1, p2}, Lckf;->i(Ljava/lang/String;J)V

    .line 572
    return-void
.end method

.method public final V(J)V
    .locals 1

    .prologue
    .line 616
    const-string v0, "refresh_webview_cookies_at"

    invoke-direct {p0, v0, p1, p2}, Lckf;->i(Ljava/lang/String;J)V

    .line 617
    return-void
.end method

.method public final W(J)V
    .locals 1

    .prologue
    .line 728
    const-string v0, "first_hotword_hint_shown_at"

    invoke-direct {p0, v0, p1, p2}, Lckf;->i(Ljava/lang/String;J)V

    .line 729
    return-void
.end method

.method public final X(J)V
    .locals 1

    .prologue
    .line 1001
    const-string v0, "update_promo_last_shown_time"

    invoke-direct {p0, v0, p1, p2}, Lckf;->i(Ljava/lang/String;J)V

    .line 1002
    return-void
.end method

.method public final a(JLjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1047
    new-instance v0, Lcsj;

    invoke-direct {v0}, Lcsj;-><init>()V

    .line 1048
    invoke-virtual {v0, p1, p2}, Lcsj;->af(J)Lcsj;

    .line 1049
    invoke-virtual {v0, p3}, Lcsj;->iP(Ljava/lang/String;)Lcsj;

    .line 1050
    invoke-virtual {v0, p4}, Lcsj;->iQ(Ljava/lang/String;)Lcsj;

    .line 1051
    iget-object v1, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    invoke-interface {v1}, Lcyg;->EH()Lcyh;

    move-result-object v1

    const-string v2, "pending_hotword_model_download_info"

    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcyh;->c(Ljava/lang/String;[B)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 1053
    return-void
.end method

.method public final a(Lciw;)V
    .locals 3
    .param p1    # Lciw;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 893
    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    .line 894
    iget-object v1, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    invoke-interface {v1}, Lcyg;->EH()Lcyh;

    move-result-object v1

    const-string v2, "person_shortcut_info"

    invoke-interface {v1, v2, v0}, Lcyh;->c(Ljava/lang/String;[B)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 897
    return-void
.end method

.method public final a(Lcjd;)V
    .locals 3

    .prologue
    .line 796
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    .line 797
    const-string v1, "gsa_relationship_contact_info"

    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcyh;->c(Ljava/lang/String;[B)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 799
    return-void
.end method

.method public final a(Ldgb;Z)V
    .locals 5

    .prologue
    .line 434
    invoke-static {p1}, Lckf;->f(Ldgb;)Ljava/lang/String;

    move-result-object v0

    .line 435
    iget-object v1, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    invoke-interface {v1}, Lcyg;->EH()Lcyh;

    move-result-object v1

    invoke-interface {v1, v0}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    move-result-object v1

    invoke-interface {v1}, Lcyh;->commit()Z

    move-result v1

    if-nez v1, :cond_0

    .line 436
    const-string v1, "QSB.SearchSettingsImpl"

    const-string v2, "Unable to setSourceEnabled %s %b"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 438
    :cond_0
    return-void
.end method

.method public final a(Ldht;)V
    .locals 2

    .prologue
    .line 422
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    invoke-static {p1}, Lckf;->f(Ldgb;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 423
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 239
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v2}, Lchr;->Kt()Lcyg;

    move-result-object v2

    invoke-interface {v2}, Lcyg;->EH()Lcyh;

    move-result-object v2

    const-string v3, "search_domain_scheme"

    invoke-interface {v2, v3, p1}, Lcyh;->B(Ljava/lang/String;Ljava/lang/String;)Lcyh;

    move-result-object v2

    const-string v3, "search_domain"

    invoke-interface {v2, v3, p2}, Lcyh;->B(Ljava/lang/String;Ljava/lang/String;)Lcyh;

    move-result-object v2

    const-string v3, "search_domain_country_code"

    invoke-interface {v2, v3, p3}, Lcyh;->B(Ljava/lang/String;Ljava/lang/String;)Lcyh;

    move-result-object v2

    const-string v3, "search_language"

    invoke-interface {v2, v3, p4}, Lcyh;->B(Ljava/lang/String;Ljava/lang/String;)Lcyh;

    move-result-object v2

    const-string v3, "search_domain_apply_time"

    invoke-interface {v2, v3, v0, v1}, Lcyh;->c(Ljava/lang/String;J)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 241
    return-void
.end method

.method public final a(Ljjf;)V
    .locals 3

    .prologue
    .line 740
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    .line 741
    const-string v1, "gsa_config_server"

    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcyh;->c(Ljava/lang/String;[B)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 742
    return-void
.end method

.method public final a(Ljta;)V
    .locals 3

    .prologue
    .line 849
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    .line 850
    if-nez p1, :cond_0

    .line 851
    const-string v1, "gsa_contact_method_alias_configuration"

    invoke-interface {v0, v1}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    .line 856
    :goto_0
    invoke-interface {v0}, Lcyh;->apply()V

    .line 857
    return-void

    .line 853
    :cond_0
    const-string v1, "gsa_contact_method_alias_configuration"

    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcyh;->c(Ljava/lang/String;[B)Lcyh;

    goto :goto_0
.end method

.method public final a(Ljtd;)V
    .locals 3

    .prologue
    .line 820
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    .line 821
    if-nez p1, :cond_0

    .line 822
    const-string v1, "gsa_relationship_configuration"

    invoke-interface {v0, v1}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    .line 827
    :goto_0
    invoke-interface {v0}, Lcyh;->apply()V

    .line 828
    return-void

    .line 824
    :cond_0
    const-string v1, "gsa_relationship_configuration"

    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcyh;->c(Ljava/lang/String;[B)Lcyh;

    goto :goto_0
.end method

.method public final a([IJ)V
    .locals 2

    .prologue
    .line 1089
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "server_experiment_ids"

    invoke-interface {v0, v1, p1}, Lcyh;->b(Ljava/lang/String;[I)Lcyh;

    move-result-object v0

    const-string v1, "server_experiment_ids_timestamp"

    invoke-interface {v0, v1, p2, p3}, Lcyh;->c(Ljava/lang/String;J)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 1093
    return-void
.end method

.method public final b(Ljjf;)V
    .locals 3

    .prologue
    .line 733
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    .line 734
    const-string v1, "gsa_config_overrides"

    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcyh;->c(Ljava/lang/String;[B)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 736
    return-void
.end method

.method public final b(Ldgb;)Z
    .locals 2

    .prologue
    .line 417
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-static {p1}, Lckf;->f(Ldgb;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcyg;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final c(Ljava/util/Set;)V
    .locals 2

    .prologue
    .line 906
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    .line 907
    const-string v1, "debug_gsa_config_overridable_flags"

    invoke-interface {v0, v1, p1}, Lcyh;->b(Ljava/lang/String;Ljava/util/Set;)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 908
    return-void
.end method

.method public final c(Ldgb;)Z
    .locals 3

    .prologue
    .line 427
    invoke-interface {p1}, Ldgb;->abP()Z

    move-result v0

    .line 428
    invoke-static {p1}, Lckf;->f(Ldgb;)Ljava/lang/String;

    move-result-object v1

    .line 429
    iget-object v2, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v2}, Lchr;->Kt()Lcyg;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final cd(Z)V
    .locals 2

    .prologue
    .line 954
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "contact_upload_for_communication_actions_enabled"

    invoke-interface {v0, v1, p1}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 957
    return-void
.end method

.method public final cn(Z)V
    .locals 1

    .prologue
    .line 334
    const-string v0, "use_google_com"

    invoke-direct {p0, v0, p1}, Lckf;->j(Ljava/lang/String;Z)V

    .line 335
    return-void
.end method

.method public final co(Z)V
    .locals 2

    .prologue
    .line 412
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "did_user_opt_into_icing_whitelisting"

    invoke-interface {v0, v1, p1}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 413
    return-void
.end method

.method public final cp(Z)V
    .locals 2

    .prologue
    .line 521
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "app_launch_log_migration_needed"

    invoke-interface {v0, v1, p1}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 522
    return-void
.end method

.method public final cq(Z)V
    .locals 3

    .prologue
    .line 973
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "gel_usage_stats"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 976
    return-void
.end method

.method public final cr(Z)V
    .locals 1

    .prologue
    .line 1037
    const-string v0, "service_created"

    invoke-direct {p0, v0, p1}, Lckf;->j(Ljava/lang/String;Z)V

    .line 1038
    return-void
.end method

.method public final cs(Z)V
    .locals 2

    .prologue
    .line 1077
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "remove_experiment_overrides"

    invoke-interface {v0, v1, p1}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 1080
    return-void
.end method

.method public final d(Ljava/lang/String;[B)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 697
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "action_discovery_data_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lckf;->e(Ljava/lang/String;[B)V

    .line 698
    return-void
.end method

.method public final d(Ldgb;)Z
    .locals 3

    .prologue
    .line 450
    invoke-static {p1}, Lckf;->g(Ldgb;)Ljava/lang/String;

    move-result-object v0

    .line 451
    iget-object v1, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final e(Landroid/view/Menu;)V
    .locals 3

    .prologue
    .line 149
    new-instance v0, Landroid/view/MenuInflater;

    iget-object v1, p0, Lckf;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    .line 150
    const v1, 0x7f130007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 151
    const v0, 0x7f1104dc

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 152
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.search.action.SEARCH_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x80000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lckf;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    .line 153
    return-void
.end method

.method public final e(Ldgb;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 456
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    invoke-static {p1}, Lckf;->g(Ldgb;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->commit()Z

    move-result v0

    .line 460
    if-nez v0, :cond_0

    .line 461
    const-string v0, "QSB.SearchSettingsImpl"

    const-string v1, "Unable to set preference %s"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Lckf;->g(Ldgb;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 463
    :cond_0
    return-void
.end method

.method public final e(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 551
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "background_task_earliest_next_run_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lckf;->i(Ljava/lang/String;J)V

    .line 552
    return-void
.end method

.method public final eY(I)I
    .locals 3

    .prologue
    .line 289
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "account_mismatch_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final eZ(I)V
    .locals 3

    .prologue
    .line 294
    invoke-virtual {p0, p1}, Lckf;->eY(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 295
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "account_mismatch_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lckf;->n(Ljava/lang/String;I)V

    .line 296
    return-void
.end method

.method public final f(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 561
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "background_task_forced_run_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lckf;->i(Ljava/lang/String;J)V

    .line 562
    return-void
.end method

.method public final fa(I)V
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "last_run_version"

    invoke-interface {v0, v1, p1}, Lcyh;->l(Ljava/lang/String;I)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 307
    return-void
.end method

.method public final fb(I)V
    .locals 2

    .prologue
    .line 327
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    .line 328
    const-string v1, "debug_features_level"

    invoke-interface {v0, v1, p1}, Lcyh;->l(Ljava/lang/String;I)Lcyh;

    .line 329
    invoke-interface {v0}, Lcyh;->apply()V

    .line 330
    return-void
.end method

.method public final fc(I)V
    .locals 2

    .prologue
    .line 966
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "temp_units"

    invoke-interface {v0, v1, p1}, Lcyh;->l(Ljava/lang/String;I)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 969
    return-void
.end method

.method public final g(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 912
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "last_query"

    invoke-interface {v0, v1, p1}, Lcyh;->B(Ljava/lang/String;Ljava/lang/String;)Lcyh;

    move-result-object v0

    const-string v1, "last_query_updated"

    invoke-interface {v0, v1, p2, p3}, Lcyh;->c(Ljava/lang/String;J)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 916
    return-void
.end method

.method public final gQ(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 255
    const-string v0, "gservices_overrides"

    invoke-direct {p0, v0, p1}, Lckf;->M(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    return-void
.end method

.method public final gR(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 541
    const-string v0, "zero_query_web_results"

    invoke-direct {p0, v0, p1}, Lckf;->M(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    return-void
.end method

.method public final gS(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 546
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "background_task_earliest_next_run_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcyg;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final gT(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 556
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "background_task_forced_run_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcyg;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final gU(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 586
    const-string v0, "user_agent"

    invoke-direct {p0, v0, p1}, Lckf;->M(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    return-void
.end method

.method public final gV(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 626
    const-string v0, "webview_logged_in_account"

    invoke-direct {p0, v0, p1}, Lckf;->M(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    return-void
.end method

.method public final gW(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 636
    const-string v0, "webview_logged_in_domain"

    invoke-direct {p0, v0, p1}, Lckf;->M(Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    return-void
.end method

.method public final gX(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 677
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "action_discovery_data_url_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final gY(Ljava/lang/String;)[B
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 687
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "action_discovery_data_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->b(Ljava/lang/String;[B)[B

    move-result-object v0

    return-object v0
.end method

.method public final gZ(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 692
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "action_discovery_data_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcyg;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final h(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 930
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "last_failed_query"

    invoke-interface {v0, v1, p1}, Lcyh;->B(Ljava/lang/String;Ljava/lang/String;)Lcyh;

    move-result-object v0

    const-string v1, "last_failed_query_updated"

    invoke-interface {v0, v1, p2, p3}, Lcyh;->c(Ljava/lang/String;J)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 934
    return-void
.end method

.method public final ha(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 718
    const-string v0, "hotword_usage_stats"

    invoke-direct {p0, v0, p1}, Lckf;->M(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    return-void
.end method

.method public final hb(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "last_run_system_build"

    invoke-interface {v0, v1, p1}, Lcyh;->B(Ljava/lang/String;Ljava/lang/String;)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 317
    return-void
.end method

.method public final hc(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 752
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    .line 753
    const-string v1, "gsa_config_container_etag"

    invoke-interface {v0, v1, p1}, Lcyh;->B(Ljava/lang/String;Ljava/lang/String;)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 754
    return-void
.end method

.method public final hd(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1028
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-static {p1}, Lckf;->he(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i(Ljava/lang/String;Z)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 266
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    .line 267
    iget-object v1, p0, Lckf;->mPrefController:Lchr;

    iget-object v1, v1, Lchr;->mGelStartupPrefs:Ldku;

    .line 269
    if-nez p1, :cond_0

    .line 270
    const-string v2, "google_account"

    invoke-interface {v0, v2}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    .line 271
    const-string v2, "signed_out"

    invoke-interface {v0, v2, p2}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    .line 272
    const-string v2, "GSAPrefs.google_account"

    invoke-virtual {v1, v2}, Ldku;->remove(Ljava/lang/String;)V

    .line 279
    :goto_0
    invoke-interface {v0}, Lcyh;->apply()V

    .line 280
    return-void

    .line 274
    :cond_0
    const-string v2, "google_account"

    invoke-interface {v0, v2, p1}, Lcyh;->B(Ljava/lang/String;Ljava/lang/String;)Lcyh;

    .line 275
    const-string v2, "signed_out"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcyh;->h(Ljava/lang/String;Z)Lcyh;

    .line 276
    const-string v2, "GSAPrefs.google_account"

    invoke-virtual {v1, v2, p1}, Ldku;->as(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final q([B)V
    .locals 1

    .prologue
    .line 596
    const-string v0, "web_corpora_config"

    invoke-direct {p0, v0, p1}, Lckf;->e(Ljava/lang/String;[B)V

    .line 597
    return-void
.end method

.method public final r([B)V
    .locals 1

    .prologue
    .line 986
    const-string v0, "action_discovery_suggestions"

    invoke-direct {p0, v0, p1}, Lckf;->e(Ljava/lang/String;[B)V

    .line 987
    return-void
.end method

.method public final registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 3

    .prologue
    .line 340
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    iget-object v1, v0, Lchr;->dK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v2

    invoke-interface {v2, p1}, Lcyg;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    iget-object v2, v0, Lchr;->mMainPrefs:Lcyg;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lchr;->mMainPrefs:Lcyg;

    invoke-interface {v0, p1}, Lcyg;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    iget-object v2, v0, Lchr;->aXm:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lchr;->aXm:Ljava/util/ArrayList;

    :cond_1
    iget-object v0, v0, Lchr;->aXm:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 3

    .prologue
    .line 346
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    iget-object v1, v0, Lchr;->dK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v2

    invoke-interface {v2, p1}, Lcyg;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    iget-object v2, v0, Lchr;->mMainPrefs:Lcyg;

    if-eqz v2, :cond_1

    iget-object v0, v0, Lchr;->mMainPrefs:Lcyg;

    invoke-interface {v0, p1}, Lcyg;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    iget-object v2, v0, Lchr;->aXm:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lchr;->aXm:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final wu()Z
    .locals 3

    .prologue
    .line 1107
    iget-object v0, p0, Lckf;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "high_contrast_mode_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
