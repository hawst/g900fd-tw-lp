.class public final Ljfx;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private efN:Ljez;

.field private efQ:I

.field private eid:Z

.field private ejb:Z

.field private ejc:Z

.field private ejd:Z

.field private eje:Z

.field private ejf:Z

.field private ejg:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 8993
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 8994
    iput v1, p0, Ljfx;->aez:I

    iput-object v2, p0, Ljfx;->efN:Ljez;

    iput v0, p0, Ljfx;->efQ:I

    iput-boolean v0, p0, Ljfx;->eid:Z

    iput-boolean v0, p0, Ljfx;->ejd:Z

    iput-boolean v0, p0, Ljfx;->eje:Z

    iput-boolean v1, p0, Ljfx;->ejb:Z

    iput-boolean v1, p0, Ljfx;->ejc:Z

    iput-boolean v0, p0, Ljfx;->ejf:Z

    iput-boolean v0, p0, Ljfx;->ejg:Z

    iput-object v2, p0, Ljfx;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljfx;->eCz:I

    .line 8995
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8819
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljfx;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljfx;->efN:Ljez;

    if-nez v0, :cond_1

    new-instance v0, Ljez;

    invoke-direct {v0}, Ljez;-><init>()V

    iput-object v0, p0, Ljfx;->efN:Ljez;

    :cond_1
    iget-object v0, p0, Ljfx;->efN:Ljez;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfx;->ejd:Z

    iget v0, p0, Ljfx;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljfx;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfx;->eje:Z

    iget v0, p0, Ljfx;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljfx;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfx;->ejb:Z

    iget v0, p0, Ljfx;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljfx;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfx;->ejc:Z

    iget v0, p0, Ljfx;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljfx;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfx;->ejf:Z

    iget v0, p0, Ljfx;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljfx;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfx;->eid:Z

    iget v0, p0, Ljfx;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfx;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljfx;->efQ:I

    iget v0, p0, Ljfx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfx;->aez:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfx;->ejg:Z

    iget v0, p0, Ljfx;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljfx;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 9016
    iget-object v0, p0, Ljfx;->efN:Ljez;

    if-eqz v0, :cond_0

    .line 9017
    const/4 v0, 0x1

    iget-object v1, p0, Ljfx;->efN:Ljez;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 9019
    :cond_0
    iget v0, p0, Ljfx;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 9020
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljfx;->ejd:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 9022
    :cond_1
    iget v0, p0, Ljfx;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    .line 9023
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljfx;->eje:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 9025
    :cond_2
    iget v0, p0, Ljfx;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_3

    .line 9026
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljfx;->ejb:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 9028
    :cond_3
    iget v0, p0, Ljfx;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_4

    .line 9029
    const/4 v0, 0x5

    iget-boolean v1, p0, Ljfx;->ejc:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 9031
    :cond_4
    iget v0, p0, Ljfx;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_5

    .line 9032
    const/4 v0, 0x6

    iget-boolean v1, p0, Ljfx;->ejf:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 9034
    :cond_5
    iget v0, p0, Ljfx;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_6

    .line 9035
    const/4 v0, 0x7

    iget-boolean v1, p0, Ljfx;->eid:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 9037
    :cond_6
    iget v0, p0, Ljfx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_7

    .line 9038
    const/16 v0, 0x8

    iget v1, p0, Ljfx;->efQ:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 9040
    :cond_7
    iget v0, p0, Ljfx;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_8

    .line 9041
    const/16 v0, 0x9

    iget-boolean v1, p0, Ljfx;->ejg:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 9043
    :cond_8
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 9044
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 9048
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 9049
    iget-object v1, p0, Ljfx;->efN:Ljez;

    if-eqz v1, :cond_0

    .line 9050
    const/4 v1, 0x1

    iget-object v2, p0, Ljfx;->efN:Ljez;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9053
    :cond_0
    iget v1, p0, Ljfx;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_1

    .line 9054
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljfx;->ejd:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 9057
    :cond_1
    iget v1, p0, Ljfx;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    .line 9058
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljfx;->eje:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 9061
    :cond_2
    iget v1, p0, Ljfx;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_3

    .line 9062
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljfx;->ejb:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 9065
    :cond_3
    iget v1, p0, Ljfx;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_4

    .line 9066
    const/4 v1, 0x5

    iget-boolean v2, p0, Ljfx;->ejc:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 9069
    :cond_4
    iget v1, p0, Ljfx;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_5

    .line 9070
    const/4 v1, 0x6

    iget-boolean v2, p0, Ljfx;->ejf:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 9073
    :cond_5
    iget v1, p0, Ljfx;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_6

    .line 9074
    const/4 v1, 0x7

    iget-boolean v2, p0, Ljfx;->eid:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 9077
    :cond_6
    iget v1, p0, Ljfx;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_7

    .line 9078
    const/16 v1, 0x8

    iget v2, p0, Ljfx;->efQ:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9081
    :cond_7
    iget v1, p0, Ljfx;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_8

    .line 9082
    const/16 v1, 0x9

    iget-boolean v2, p0, Ljfx;->ejg:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 9085
    :cond_8
    return v0
.end method
