.class public final Lffm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfds;


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private final mConfig:Lcjs;

.field private final mNotificationStore:Lffp;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lffp;Lcjs;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lffm;->mAppContext:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lffm;->mNotificationStore:Lffp;

    .line 36
    iput-object p3, p0, Lffm;->mConfig:Lcjs;

    .line 37
    return-void
.end method


# virtual methods
.method public final b(Landroid/location/Location;Landroid/location/Location;)V
    .locals 3
    .param p1    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 44
    invoke-static {}, Lenu;->auQ()V

    .line 46
    iget-object v0, p0, Lffm;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Mp()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    new-instance v0, Lffn;

    invoke-direct {v0, p2, p1}, Lffn;-><init>(Landroid/location/Location;Landroid/location/Location;)V

    .line 51
    iget-object v1, p0, Lffm;->mNotificationStore:Lffp;

    invoke-virtual {v1, v0}, Lffp;->a(Lffx;)V

    .line 53
    invoke-virtual {v0}, Lffn;->azx()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    iget-object v1, p0, Lffm;->mAppContext:Landroid/content/Context;

    iget-object v2, v0, Lffx;->cqu:Ljava/util/Set;

    iget-object v0, v0, Lffx;->cqv:Ljava/util/Set;

    invoke-static {v1, v2, v0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->a(Landroid/content/Context;Ljava/util/Set;Ljava/util/Set;)Landroid/content/Intent;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lffm;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method
