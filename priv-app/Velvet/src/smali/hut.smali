.class public final Lhut;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private bKr:Ljava/lang/CharSequence;

.field private final cOU:I

.field drs:I

.field private drt:Landroid/text/SpannableString;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mHypothesisToSuggestionSpansConverter:Lgel;

.field private final mRequestId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILgel;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lhut;->drs:I

    .line 62
    iput-object p1, p0, Lhut;->mRequestId:Ljava/lang/String;

    .line 63
    iput p2, p0, Lhut;->cOU:I

    .line 64
    iput-object p3, p0, Lhut;->mHypothesisToSuggestionSpansConverter:Lgel;

    .line 65
    return-void
.end method

.method private static f(Ljava/lang/CharSequence;I)I
    .locals 2

    .prologue
    .line 199
    const-string v0, " "

    invoke-static {p0, v0, p1}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)I

    move-result v0

    .line 200
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 201
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 203
    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public P(Ljava/lang/CharSequence;)Lhuu;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 110
    iget v2, p0, Lhut;->drs:I

    iget-object v3, p0, Lhut;->bKr:Ljava/lang/CharSequence;

    iget v1, p0, Lhut;->drs:I

    if-eqz v3, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    sub-int/2addr v2, v0

    .line 111
    iput-object p1, p0, Lhut;->bKr:Ljava/lang/CharSequence;

    .line 113
    if-nez v2, :cond_3

    .line 115
    const/4 v0, 0x0

    .line 129
    :goto_1
    return-object v0

    .line 110
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    :goto_2
    if-ge v0, v1, :cond_2

    invoke-interface {v3, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    if-ne v4, v5, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_0

    .line 119
    :cond_3
    const/16 v0, 0x4a

    invoke-static {v0}, Lege;->ht(I)V

    .line 121
    iget v0, p0, Lhut;->drs:I

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 122
    add-int/lit8 v1, v0, -0x1

    if-ltz v1, :cond_4

    add-int/lit8 v1, v0, -0x1

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    const/16 v3, 0x20

    if-eq v1, v3, :cond_4

    .line 123
    iget v0, p0, Lhut;->drs:I

    invoke-static {p1, v0}, Lhut;->f(Ljava/lang/CharSequence;I)I

    move-result v0

    .line 125
    :cond_4
    new-instance v1, Lhuu;

    iget v3, p0, Lhut;->drs:I

    sub-int/2addr v3, v2

    invoke-interface {p1, v3, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lhuu;-><init>(ILjava/lang/CharSequence;)V

    .line 127
    iput v0, p0, Lhut;->drs:I

    move-object v0, v1

    .line 129
    goto :goto_1
.end method

.method public final a(Lcom/google/android/shared/speech/Hypothesis;)Lhuu;
    .locals 3
    .param p1    # Lcom/google/android/shared/speech/Hypothesis;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 86
    iget-object v0, p0, Lhut;->mHypothesisToSuggestionSpansConverter:Lgel;

    iget-object v1, p0, Lhut;->mRequestId:Ljava/lang/String;

    iget v2, p0, Lhut;->cOU:I

    invoke-virtual {v0, v1, v2, p1}, Lgel;->a(Ljava/lang/String;ILcom/google/android/shared/speech/Hypothesis;)Landroid/text/SpannableString;

    move-result-object v0

    iput-object v0, p0, Lhut;->drt:Landroid/text/SpannableString;

    .line 88
    iget-object v0, p0, Lhut;->drt:Landroid/text/SpannableString;

    invoke-virtual {p0, v0}, Lhut;->P(Ljava/lang/CharSequence;)Lhuu;

    move-result-object v0

    .line 90
    iget v1, p0, Lhut;->drs:I

    iget-object v2, p0, Lhut;->bKr:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 93
    if-eqz v0, :cond_1

    .line 94
    iget v1, v0, Lhuu;->drv:I

    iget v2, p0, Lhut;->drs:I

    add-int/2addr v1, v2

    iget-object v0, v0, Lhuu;->dru:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    sub-int v0, v1, v0

    .line 101
    :goto_0
    new-instance v1, Lhuu;

    invoke-virtual {p0}, Lhut;->aSs()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lhuu;-><init>(ILjava/lang/CharSequence;)V

    move-object v0, v1

    .line 103
    :cond_0
    return-object v0

    .line 97
    :cond_1
    iget v0, p0, Lhut;->drs:I

    goto :goto_0
.end method

.method public final aSq()Z
    .locals 2

    .prologue
    .line 136
    iget v0, p0, Lhut;->drs:I

    iget-object v1, p0, Lhut;->bKr:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aSr()Lhuu;
    .locals 3

    .prologue
    .line 147
    iget-object v0, p0, Lhut;->bKr:Ljava/lang/CharSequence;

    iget v1, p0, Lhut;->drs:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lhut;->f(Ljava/lang/CharSequence;I)I

    move-result v0

    .line 149
    iget-object v1, p0, Lhut;->bKr:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lhut;->abc()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 151
    new-instance v0, Lhuu;

    iget v1, p0, Lhut;->drs:I

    invoke-virtual {p0}, Lhut;->aSs()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lhuu;-><init>(ILjava/lang/CharSequence;)V

    .line 152
    iget-object v1, p0, Lhut;->bKr:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    iput v1, p0, Lhut;->drs:I

    .line 157
    :goto_0
    return-object v0

    .line 155
    :cond_0
    iget-object v1, p0, Lhut;->bKr:Ljava/lang/CharSequence;

    iget v2, p0, Lhut;->drs:I

    invoke-interface {v1, v2, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    .line 156
    iput v0, p0, Lhut;->drs:I

    .line 157
    new-instance v0, Lhuu;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, Lhuu;-><init>(ILjava/lang/CharSequence;)V

    goto :goto_0
.end method

.method aSs()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Lhut;->abc()Z

    move-result v0

    if-nez v0, :cond_0

    .line 182
    iget-object v0, p0, Lhut;->bKr:Ljava/lang/CharSequence;

    .line 185
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhut;->drt:Landroid/text/SpannableString;

    goto :goto_0
.end method

.method public final abc()Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lhut;->drt:Landroid/text/SpannableString;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
