.class public final Lekp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lejh;


# instance fields
.field private cbv:Z

.field private synthetic cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;


# direct methods
.method public constructor <init>(Lcom/google/android/shared/ui/SuggestionGridLayout;)V
    .locals 1

    .prologue
    .line 1450
    iput-object p1, p0, Lekp;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1451
    const/4 v0, 0x0

    iput-boolean v0, p0, Lekp;->cbv:Z

    return-void
.end method


# virtual methods
.method public final J(F)V
    .locals 3

    .prologue
    .line 1470
    iget-object v0, p0, Lekp;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v0, v0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    instance-of v0, v0, Lejl;

    if-eqz v0, :cond_0

    .line 1471
    iget-object v0, p0, Lekp;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v0, v0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    .line 1472
    iget-object v1, p0, Lekp;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v1, v1, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    check-cast v1, Lejl;

    iget v2, v0, Lekm;->cdE:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v2, v2

    iget v0, v0, Lekm;->cdF:I

    invoke-interface {v1, p1, v2, v0}, Lejl;->b(FII)I

    .line 1475
    :cond_0
    return-void
.end method

.method public final atA()V
    .locals 2

    .prologue
    .line 1487
    iget-boolean v0, p0, Lekp;->cbv:Z

    if-eqz v0, :cond_0

    .line 1489
    iget-object v0, p0, Lekp;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->eZ(Z)V

    .line 1490
    iget-object v0, p0, Lekp;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aud()V

    .line 1491
    iget-object v0, p0, Lekp;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdk:Z

    .line 1493
    :cond_0
    return-void
.end method

.method public final atw()I
    .locals 2

    .prologue
    .line 1455
    iget-object v0, p0, Lekp;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget v0, v0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdl:I

    iget-object v1, p0, Lekp;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget v1, v1, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdj:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final atx()I
    .locals 2

    .prologue
    .line 1460
    iget-object v0, p0, Lekp;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v0, v0, Lcom/google/android/shared/ui/SuggestionGridLayout;->mScrollView:Lekf;

    invoke-interface {v0}, Lekf;->ate()I

    move-result v0

    iget-object v1, p0, Lekp;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->auf()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public final aty()I
    .locals 1

    .prologue
    .line 1465
    iget-object v0, p0, Lekp;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget v0, v0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdl:I

    return v0
.end method

.method public final atz()V
    .locals 2

    .prologue
    .line 1479
    const/4 v0, 0x1

    iput-boolean v0, p0, Lekp;->cbv:Z

    .line 1480
    iget-object v0, p0, Lekp;->cdp:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v0, v0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdg:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leko;

    .line 1481
    invoke-interface {v0}, Leko;->Cs()V

    goto :goto_0

    .line 1483
    :cond_0
    return-void
.end method
