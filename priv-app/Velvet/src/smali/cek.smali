.class public final Lcek;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static Do()Ljwv;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 51
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_2

    .line 52
    invoke-static {}, Landroid/app/ActivityManager;->isRunningInTestHarness()Z

    move-result v0

    .line 54
    :goto_0
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "forceEmulatorServerLogs"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 56
    new-instance v0, Ljwv;

    invoke-direct {v0}, Ljwv;-><init>()V

    invoke-virtual {v0, v1}, Ljwv;->jj(Z)Ljwv;

    move-result-object v0

    .line 58
    :goto_1
    return-object v0

    :cond_1
    new-instance v0, Ljwv;

    invoke-direct {v0}, Ljwv;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljwv;->jj(Z)Ljwv;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static b([BI)Ljwv;
    .locals 3

    .prologue
    .line 28
    invoke-static {}, Lcek;->Do()Ljwv;

    move-result-object v0

    .line 29
    sget-object v1, Ljwd;->eIQ:Ljsm;

    invoke-static {p0, p1}, Lcek;->c([BI)Ljwd;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljwv;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 30
    return-object v0
.end method

.method public static c([BI)Ljwd;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Ljwd;

    invoke-direct {v0}, Ljwd;-><init>()V

    .line 41
    const/4 v1, 0x0

    invoke-static {p0, v1, p1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljwd;->az([B)Ljwd;

    .line 42
    return-object v0
.end method
