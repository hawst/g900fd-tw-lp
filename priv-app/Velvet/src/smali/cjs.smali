.class public Lcjs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# static fields
.field private static aZa:Landroid/net/Uri;


# instance fields
.field private final aZb:Landroid/util/SparseArray;

.field private final aZc:Landroid/util/SparseArray;

.field private final aZd:Landroid/util/SparseArray;

.field private volatile aZe:Landroid/util/SparseArray;

.field private aZf:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mPrefController:Lchr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-string v0, "content://com.google.android.partnersetup.rlzappprovider/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcjs;->aZa:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput-object v0, p0, Lcjs;->mContext:Landroid/content/Context;

    .line 110
    iput-object v0, p0, Lcjs;->mPrefController:Lchr;

    .line 111
    iput-object v0, p0, Lcjs;->aZd:Landroid/util/SparseArray;

    .line 112
    iput-object v0, p0, Lcjs;->aZb:Landroid/util/SparseArray;

    .line 113
    iput-object v0, p0, Lcjs;->aZc:Landroid/util/SparseArray;

    .line 114
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lchr;)V
    .locals 3

    .prologue
    const/16 v2, 0x100

    const/4 v1, 0x5

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput-object p1, p0, Lcjs;->mContext:Landroid/content/Context;

    .line 97
    iput-object p2, p0, Lcjs;->mPrefController:Lchr;

    .line 98
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v2}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcjs;->aZd:Landroid/util/SparseArray;

    .line 99
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcjs;->aZb:Landroid/util/SparseArray;

    .line 100
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcjs;->aZc:Landroid/util/SparseArray;

    .line 102
    const v0, 0x7f0f0015

    const-string v1, "default_source_uris"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0f0016

    const-string v1, "default_sources"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0f0017

    const-string v1, "google_search_paths"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0f0018

    const-string v1, "google_search_logout_redirects"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0f0019

    const-string v1, "google_utility_paths"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0f001e

    const-string v1, "full_size_icon_source_suggest_uris"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c002d

    const-string v1, "source_timeout_millis"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0030

    const-string v1, "max_displayed_summons_in_results_suggest"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0031

    const-string v1, "min_web_suggestions"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0032

    const-string v1, "max_web_suggestions"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0033

    const-string v1, "max_total_suggestions"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0034

    const-string v1, "max_stat_age_hours"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0035

    const-string v1, "min_clicks_for_source_ranking"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0036

    const-string v1, "typing_update_suggestions_delay_millis"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a010b

    const-string v1, "tos_url_format"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a0113

    const-string v1, "search_domain_override"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c003b

    const-string v1, "http_cache_size"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e0013

    const-string v1, "suggest_look_ahead_enabled"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a011c

    const-string v1, "user_agent_param_format"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c003c

    const-string v1, "location_expirey_time"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c004c

    const-string v1, "mariner_background_refresh_interval_minutes"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c004d

    const-string v1, "mariner_idle_background_refresh_interval_minutes"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0f0024

    const-string v1, "clicked_result_destination_params"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a0118

    const-string v1, "clicked_ad_url_path"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a0117

    const-string v1, "clicked_result_url_path"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0f0022

    const-string v1, "click_ad_url_exception_patterns"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0f0023

    const-string v1, "click_ad_url_substitutions"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a0119

    const-string v1, "corpora_config_uri_24_plus"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a0114

    const-string v1, "register_gsa_bridge_javascript"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0039

    const-string v1, "suggestion_view_recycle_bin_size"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0056

    const-string v1, "suggest_num_visible_summons_rows"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a0116

    const-string v1, "velvetgsabridge_interface_name"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a011a

    const-string v1, "web_corpus_query_param"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0f001f

    const-string v1, "domain_whitelist"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0037

    const-string v1, "saved_configuration_expiry_seconds"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0038

    const-string v1, "saved_whitelisted_configuration_expiry_seconds"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0f0025

    const-string v1, "gws_cgi_param_query_equal_whitelist_33_plus"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0f0026

    const-string v1, "gws_cgi_param_changes_for_back_navigation"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0f0027

    const-string v1, "gws_path_whitelist_for_back_navigation"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e0014

    const-string v1, "correction_spans_enabled"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c003d

    const-string v1, "prefetch_ttl_millis"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c003e

    const-string v1, "prefetch_cache_entries"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c003f

    const-string v1, "prefetch_simultaneous_downloads"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0040

    const-string v1, "prefetch_throttle_period_millis"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a011b

    const-string v1, "google_gen_204_pattern"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e0015

    const-string v1, "personalized_search_enabled"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a011d

    const-string v1, "device_country"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a011e

    const-string v1, "gms_disable:com.google.android.ears"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a011f

    const-string v1, "s3_service_voice_actions_override"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a0120

    const-string v1, "s3_server_override"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a0121

    const-string v1, "history_api_lookup_url_pattern"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a0122

    const-string v1, "history_api_change_url_pattern"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a0123

    const-string v1, "history_api_client_param"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e0018

    const-string v1, "test_platform_logging_enabled"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e0019

    const-string v1, "debug_audio_logging_enabled"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a0125

    const-string v1, "action_discovery_data_uri"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0f0028

    const-string v1, "action_discovery_supported_locales"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c004a

    const-string v1, "abnf_compiler_num_contacts"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e001b

    const-string v1, "content_provider_global_search_enabled"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a0127

    const-string v1, "client_experiments_header"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a0128

    const-string v1, "client_experiments_param"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a0129

    const-string v1, "gservices_experiment_ids"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e001c

    const-string v1, "ssl_session_cache_enabled"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c004e

    const-string v1, "predictive_idle_user_threshold_minutes"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0050

    const-string v1, "tv_detection_timeout_millis"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0051

    const-string v1, "service_timeout_for_tv_detection_millis"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0f0029

    const-string v1, "tv_search_query_by_locale"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c004b

    const-string v1, "google_analytics_sample_rate_1e3"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a0115

    const-string v1, "remote_debug_javascript"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e001e

    const-string v1, "gms_geofencing"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e001f

    const-string v1, "hide_dogfood_indicator"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0a012a

    const-string v1, "dogfood_icon_url"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e0020

    const-string v1, "find_my_car_enabled"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0052

    const-string v1, "offline_card_cache_timeout_days"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0053

    const-string v1, "stale_activity_in_seconds"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e0022

    const-string v1, "redirect_mfe_requests"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e0023

    const-string v1, "redirect_mfe_to_gmm"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e0021

    const-string v1, "tv_force_show_what_to_watch"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e0024

    const-string v1, "quantum_now_cards"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e0025

    const-string v1, "traditional_view_time_recording"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0054

    const-string v1, "second_screen_reader_list_buffer_size"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0055

    const-string v1, "second_screen_cards_scroll_buffer_percent"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e0026

    const-string v1, "tv_cast_detection"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0048

    const-string v1, "icing_should_log_query_global_ppm"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e0011

    const-string v1, "icing_sources_enabled"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0f001b

    const-string v1, "ignored_icing_source_packages"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e0012

    const-string v1, "internal_icing_corpora_enabled"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0f001c

    const-string v1, "disabled_internal_icing_corpora"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0041

    const-string v1, "icing_apps_corpus_update_all_interval_millis"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0042

    const-string v1, "icing_contacts_corpus_update_all_interval_without_delta_millis"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0043

    const-string v1, "icing_contacts_corpus_update_all_interval_with_delta_millis"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0044

    const-string v1, "icing_contacts_provider_resync_initial_poll_delay_millis"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0045

    const-string v1, "icing_contacts_provider_resync_repoll_period_millis"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0046

    const-string v1, "icing_contacts_provider_resync_max_repoll_attempts"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0047

    const-string v1, "icing_contacts_provider_changed_delta_update_delay_millis"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0e0017

    const-string v1, "icing_app_launch_broadcast_handling_enabled"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    const v0, 0x7f0c0049

    const-string v1, "icing_launch_log_max_age_days"

    invoke-direct {p0, v0, v1}, Lcjs;->h(ILjava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcjs;->aZd:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-gt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 104
    return-void

    .line 103
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static KZ()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 117
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "device_country"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "gms_disable:com.google.android.ears"

    aput-object v2, v0, v1

    return-object v0
.end method

.method public static Lm()I
    .locals 1

    .prologue
    .line 499
    const/16 v0, 0x9

    return v0
.end method

.method public static Ln()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 503
    sget-object v0, Lcjs;->aZa:Landroid/net/Uri;

    return-object v0
.end method

.method private declared-synchronized MQ()V
    .locals 5

    .prologue
    .line 956
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcjs;->aZe:Landroid/util/SparseArray;

    if-nez v0, :cond_2

    .line 958
    iget-object v0, p0, Lcjs;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "gservices_overrides"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 960
    invoke-static {v0}, Lesp;->lq(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 964
    if-eqz v3, :cond_3

    .line 965
    const-string v0, "device_country"

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcjs;->aZf:Ljava/lang/String;

    .line 966
    new-instance v1, Landroid/util/SparseArray;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v1, v0}, Landroid/util/SparseArray;-><init>(I)V

    .line 968
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lcjs;->aZd:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 969
    iget-object v0, p0, Lcjs;->aZd:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 970
    if-eqz v0, :cond_0

    .line 971
    iget-object v4, p0, Lcjs;->aZd:Landroid/util/SparseArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v1, v4, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 968
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 977
    :goto_1
    iput-object v0, p0, Lcjs;->aZe:Landroid/util/SparseArray;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 979
    :cond_2
    monitor-exit p0

    return-void

    .line 975
    :cond_3
    :try_start_1
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 956
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized eV(I)Ljava/util/Map;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 397
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcjs;->aZc:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 398
    if-eqz v0, :cond_0

    .line 408
    :goto_0
    monitor-exit p0

    return-object v0

    .line 401
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-direct {p0, p1, v0}, Lcjs;->r(IZ)[Ljava/lang/String;

    move-result-object v3

    .line 402
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    .line 403
    array-length v2, v3

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Lifv;->gY(Z)V

    .line 404
    :goto_2
    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_2

    .line 405
    aget-object v2, v3, v1

    add-int/lit8 v4, v1, 0x1

    aget-object v4, v3, v4

    invoke-interface {v0, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 404
    add-int/lit8 v1, v1, 0x2

    goto :goto_2

    :cond_1
    move v2, v1

    .line 403
    goto :goto_1

    .line 407
    :cond_2
    iget-object v1, p0, Lcjs;->aZc:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 397
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized eW(I)Ljava/util/Set;
    .locals 2

    .prologue
    .line 413
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcjs;->aZb:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414
    if-eqz v0, :cond_0

    .line 419
    :goto_0
    monitor-exit p0

    return-object v0

    .line 417
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-direct {p0, p1, v0}, Lcjs;->r(IZ)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Liqs;->m([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    .line 418
    iget-object v1, p0, Lcjs;->aZb:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 413
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static gM(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 738
    const/16 v0, 0x2c

    const/16 v1, 0x3a

    invoke-static {v0, v1, p0}, Lesp;->a(CCLjava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 739
    const-string v1, "enabled"

    const-string v2, "0"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static gP(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 994
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 997
    :goto_0
    return-object v0

    .line 996
    :catch_0
    move-exception v0

    const-string v0, "Search.SearchConfig"

    const-string v1, "Invalid gservices int"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 997
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getBoolean(I)Z
    .locals 3

    .prologue
    .line 289
    :goto_0
    iget-object v1, p0, Lcjs;->aZe:Landroid/util/SparseArray;

    .line 290
    if-eqz v1, :cond_3

    .line 291
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 292
    if-eqz v0, :cond_4

    .line 293
    instance-of v2, v0, Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 294
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 304
    :goto_1
    return v0

    .line 296
    :cond_0
    check-cast v0, Ljava/lang/String;

    sget-object v2, Lbwj;->aMq:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_2
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    sget-object v2, Lbwj;->aMr:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_2

    :cond_2
    const-string v0, "Search.SearchConfig"

    const-string v2, "Invalid gservices boolean"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_2

    .line 300
    :cond_3
    iget-object v0, p0, Lcjs;->aZd:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 301
    invoke-direct {p0}, Lcjs;->MQ()V

    goto :goto_0

    .line 304
    :cond_4
    iget-object v0, p0, Lcjs;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    goto :goto_1
.end method

.method private getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 349
    :goto_0
    iget-object v0, p0, Lcjs;->aZe:Landroid/util/SparseArray;

    .line 350
    if-eqz v0, :cond_0

    .line 351
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 352
    if-eqz v0, :cond_1

    .line 353
    check-cast v0, Ljava/lang/String;

    .line 359
    :goto_1
    return-object v0

    .line 355
    :cond_0
    iget-object v0, p0, Lcjs;->aZd:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 356
    invoke-direct {p0}, Lcjs;->MQ()V

    goto :goto_0

    .line 359
    :cond_1
    iget-object v0, p0, Lcjs;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private h(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcjs;->aZd:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 129
    return-void
.end method

.method private r(IZ)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 378
    :goto_0
    iget-object v1, p0, Lcjs;->aZe:Landroid/util/SparseArray;

    .line 379
    if-eqz v1, :cond_3

    .line 380
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 381
    if-eqz v0, :cond_4

    .line 382
    instance-of v2, v0, [Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 383
    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    .line 393
    :goto_1
    return-object v0

    .line 385
    :cond_0
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz p2, :cond_1

    invoke-static {v0}, Lesp;->lr(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    const-string v2, ","

    invoke-static {v0, v2}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 389
    :cond_3
    iget-object v0, p0, Lcjs;->aZd:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 390
    invoke-direct {p0}, Lcjs;->MQ()V

    goto :goto_0

    .line 393
    :cond_4
    iget-object v0, p0, Lcjs;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final Ex()Z
    .locals 1

    .prologue
    .line 775
    const v0, 0x7f0e001c

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final I(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 525
    const v1, 0x7f0f0016

    invoke-direct {p0, v1}, Lcjs;->eW(I)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 535
    :cond_0
    :goto_0
    return v0

    .line 530
    :cond_1
    const v1, 0x7f0f0015

    invoke-direct {p0, v1}, Lcjs;->eW(I)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 535
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final LA()Ljava/lang/String;
    .locals 5

    .prologue
    .line 632
    const v0, 0x7f0a0114

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 633
    const v1, 0x7f0a0116

    invoke-direct {p0, v1}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 634
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v2, v0, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final LB()Ljava/lang/String;
    .locals 1

    .prologue
    .line 641
    const v0, 0x7f0a0115

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final LC()I
    .locals 1

    .prologue
    .line 645
    const v0, 0x7f0c0039

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final LD()Ljava/lang/String;
    .locals 1

    .prologue
    .line 649
    const v0, 0x7f0a0116

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final LE()Ljava/lang/String;
    .locals 1

    .prologue
    .line 653
    const v0, 0x7f0a011a

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final LF()Z
    .locals 1

    .prologue
    .line 657
    const v0, 0x7f0e000e

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final LG()Z
    .locals 1

    .prologue
    .line 661
    const v0, 0x7f0e000f

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final LH()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 665
    const v0, 0x7f0f001f

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcjs;->r(IZ)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final LI()Ljava/util/Set;
    .locals 1

    .prologue
    .line 669
    const v0, 0x7f0f0025

    invoke-direct {p0, v0}, Lcjs;->eW(I)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final LJ()Ljava/util/Set;
    .locals 1

    .prologue
    .line 673
    const v0, 0x7f0f0026

    invoke-direct {p0, v0}, Lcjs;->eW(I)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final LK()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 677
    const v0, 0x7f0f0027

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcjs;->r(IZ)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final LL()I
    .locals 1

    .prologue
    .line 681
    const v0, 0x7f0c004c

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final LM()I
    .locals 1

    .prologue
    .line 685
    const v0, 0x7f0c004d

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final LN()I
    .locals 1

    .prologue
    .line 689
    const v0, 0x7f0c004e

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final LO()I
    .locals 1

    .prologue
    .line 693
    const v0, 0x7f0c004f

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final LP()I
    .locals 1

    .prologue
    .line 697
    const v0, 0x7f0c003d

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final LQ()I
    .locals 1

    .prologue
    .line 701
    const v0, 0x7f0c003e

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final LR()I
    .locals 1

    .prologue
    .line 705
    const v0, 0x7f0c003f

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final LS()I
    .locals 1

    .prologue
    .line 709
    const v0, 0x7f0c0040

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final LT()Ljava/lang/String;
    .locals 1

    .prologue
    .line 713
    const v0, 0x7f0a011b

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final LU()Ljava/lang/String;
    .locals 1

    .prologue
    .line 717
    const v0, 0x7f0a011d

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final LV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 721
    const v0, 0x7f0a0121

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final LW()Ljava/lang/String;
    .locals 1

    .prologue
    .line 725
    const v0, 0x7f0a0122

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final LX()Ljava/lang/String;
    .locals 1

    .prologue
    .line 729
    const v0, 0x7f0a0123

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final LY()Ljava/lang/String;
    .locals 1

    .prologue
    .line 733
    const v0, 0x7f0a0124

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final LZ()Z
    .locals 1

    .prologue
    .line 743
    const v0, 0x7f0a011e

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcjs;->gM(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized La()V
    .locals 1

    .prologue
    .line 121
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcjs;->aZe:Landroid/util/SparseArray;

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcjs;->aZf:Ljava/lang/String;

    .line 123
    iget-object v0, p0, Lcjs;->aZb:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 124
    iget-object v0, p0, Lcjs;->aZc:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    monitor-exit p0

    return-void

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final Lb()Ljava/lang/String;
    .locals 2

    .prologue
    .line 433
    iget-object v0, p0, Lcjs;->aZf:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcjs;->aZf:Ljava/lang/String;

    const v1, 0x7f0f0014

    invoke-direct {p0, v1}, Lcjs;->eV(I)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final Lc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 444
    const v0, 0x7f0a0113

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Ld()I
    .locals 1

    .prologue
    .line 452
    const v0, 0x7f0c002d

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final Le()I
    .locals 1

    .prologue
    .line 456
    const v0, 0x7f0c002d

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final Lf()Ljava/lang/String;
    .locals 1

    .prologue
    .line 460
    const v0, 0x7f0a010b

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Lg()I
    .locals 1

    .prologue
    .line 464
    const v0, 0x7f0c002f

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final Lh()I
    .locals 1

    .prologue
    .line 468
    const v0, 0x7f0c0030

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final Li()I
    .locals 1

    .prologue
    .line 472
    const v0, 0x7f0c0031

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final Lj()I
    .locals 1

    .prologue
    .line 480
    const v0, 0x7f0c0033

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final Lk()J
    .locals 4

    .prologue
    .line 485
    const v0, 0x7f0c0034

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x36ee80

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public final Ll()I
    .locals 1

    .prologue
    .line 490
    const v0, 0x7f0c0035

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final Lo()I
    .locals 1

    .prologue
    .line 507
    const v0, 0x7f0c002d

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final Lp()I
    .locals 1

    .prologue
    .line 515
    const v0, 0x7f0c0036

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final Lq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 519
    const v0, 0x7f0a011c

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Lr()Z
    .locals 1

    .prologue
    .line 549
    const v0, 0x7f0e0013

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final Ls()Z
    .locals 1

    .prologue
    .line 557
    const v0, 0x7f0e0014

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final Lt()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 584
    const v0, 0x7f0f0020

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcjs;->r(IZ)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Lu()J
    .locals 2

    .prologue
    .line 588
    const v0, 0x7f0c003b

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final Lv()I
    .locals 1

    .prologue
    .line 592
    const v0, 0x7f0c003c

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final Lw()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 596
    const v0, 0x7f0f0024

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcjs;->r(IZ)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Lx()Ljava/lang/String;
    .locals 1

    .prologue
    .line 600
    const v0, 0x7f0a0117

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Ly()Ljava/lang/String;
    .locals 1

    .prologue
    .line 604
    const v0, 0x7f0a0118

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Lz()Ljava/util/List;
    .locals 5

    .prologue
    .line 619
    const v0, 0x7f0f0023

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcjs;->r(IZ)[Ljava/lang/String;

    move-result-object v1

    .line 620
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 621
    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_0

    .line 622
    aget-object v3, v1, v0

    add-int/lit8 v4, v0, 0x1

    aget-object v4, v1, v4

    invoke-static {v3, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 621
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 624
    :cond_0
    return-object v2
.end method

.method public final MA()I
    .locals 1

    .prologue
    .line 876
    const v0, 0x7f0c0055

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final MB()Z
    .locals 1

    .prologue
    .line 880
    const v0, 0x7f0e0026

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final MC()I
    .locals 1

    .prologue
    .line 886
    const v0, 0x7f0c0048

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final MD()I
    .locals 1

    .prologue
    .line 894
    const v0, 0x7f0c0041

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final ME()I
    .locals 1

    .prologue
    .line 898
    const v0, 0x7f0c0042

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final MF()I
    .locals 1

    .prologue
    .line 902
    const v0, 0x7f0c0043

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final MG()I
    .locals 1

    .prologue
    .line 906
    const v0, 0x7f0c0044

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final MH()I
    .locals 1

    .prologue
    .line 910
    const v0, 0x7f0c0045

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final MI()I
    .locals 1

    .prologue
    .line 914
    const v0, 0x7f0c0047

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final MJ()I
    .locals 1

    .prologue
    .line 918
    const v0, 0x7f0c0046

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final MK()Z
    .locals 1

    .prologue
    .line 922
    const v0, 0x7f0e0017

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final ML()Z
    .locals 1

    .prologue
    .line 927
    const v0, 0x7f0e0011

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final MM()Z
    .locals 1

    .prologue
    .line 937
    const v0, 0x7f0e0012

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final MN()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 942
    const v0, 0x7f0f001c

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcjs;->r(IZ)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final MO()I
    .locals 1

    .prologue
    .line 946
    const v0, 0x7f0c0049

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final MP()I
    .locals 2

    .prologue
    .line 950
    iget-object v0, p0, Lcjs;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0058

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method public final Ma()Z
    .locals 1

    .prologue
    .line 747
    const v0, 0x7f0e0015

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final Mb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 751
    const v0, 0x7f0a011f

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Mc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 755
    const v0, 0x7f0a0120

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Md()Z
    .locals 1

    .prologue
    .line 759
    const v0, 0x7f0e0018

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final Me()Z
    .locals 1

    .prologue
    .line 763
    const v0, 0x7f0e0019

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final Mf()Ljava/lang/String;
    .locals 1

    .prologue
    .line 767
    const v0, 0x7f0a0125

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Mg()I
    .locals 1

    .prologue
    .line 771
    const v0, 0x7f0c004a

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final Mh()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 779
    const v0, 0x7f0f0028

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcjs;->r(IZ)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Mi()Z
    .locals 4

    .prologue
    .line 784
    sget v0, Lesp;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    const v0, 0x7f0e001b

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcjs;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.GLOBAL_SEARCH"

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Mj()I
    .locals 1

    .prologue
    .line 795
    const v0, 0x7f0c0050

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final Mk()I
    .locals 1

    .prologue
    .line 803
    const v0, 0x7f0c0051

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final Ml()Ljava/util/Map;
    .locals 1

    .prologue
    .line 807
    const v0, 0x7f0f0029

    invoke-direct {p0, v0}, Lcjs;->eV(I)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final Mm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 811
    const v0, 0x7f0a0127

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Mn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 815
    const v0, 0x7f0a0128

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Mo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 819
    const v0, 0x7f0a0129

    invoke-direct {p0, v0}, Lcjs;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Mp()Z
    .locals 1

    .prologue
    .line 823
    const v0, 0x7f0e001e

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final Mq()D
    .locals 4

    .prologue
    .line 835
    const v0, 0x7f0c004b

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    .line 836
    int-to-double v0, v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public final Mr()Z
    .locals 1

    .prologue
    .line 840
    const v0, 0x7f0e0020

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final Ms()I
    .locals 1

    .prologue
    .line 844
    const v0, 0x7f0c0052

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final Mt()Z
    .locals 1

    .prologue
    .line 848
    const v0, 0x7f0e0021

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final Mu()I
    .locals 1

    .prologue
    .line 852
    const v0, 0x7f0c0053

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final Mv()Z
    .locals 1

    .prologue
    .line 856
    const v0, 0x7f0e0022

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final Mw()Z
    .locals 1

    .prologue
    .line 860
    const v0, 0x7f0e0023

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final Mx()Z
    .locals 1

    .prologue
    .line 864
    const v0, 0x7f0e0024

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final My()Z
    .locals 1

    .prologue
    .line 868
    const v0, 0x7f0e0025

    invoke-direct {p0, v0}, Lcjs;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public final Mz()I
    .locals 1

    .prologue
    .line 872
    const v0, 0x7f0c0054

    invoke-virtual {p0, v0}, Lcjs;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final a(Letj;)V
    .locals 1

    .prologue
    .line 1014
    const-string v0, "Disabled icing corpora"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 1015
    invoke-virtual {p0}, Lcjs;->MN()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Letj;->e(Ljava/lang/Iterable;)V

    .line 1016
    return-void
.end method

.method public final a(Ldgb;)Z
    .locals 2

    .prologue
    .line 539
    invoke-interface {p1}, Ldgb;->getName()Ljava/lang/String;

    move-result-object v0

    .line 541
    const v1, 0x7f0f001a

    invoke-direct {p0, v1}, Lcjs;->eW(I)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final gH(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 564
    const v0, 0x7f0f001e

    invoke-direct {p0, v0}, Lcjs;->eW(I)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final gI(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 568
    const v0, 0x7f0f0017

    invoke-direct {p0, v0}, Lcjs;->eW(I)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final gJ(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 572
    const v0, 0x7f0f0018

    invoke-direct {p0, v0}, Lcjs;->eW(I)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final gK(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 580
    const v0, 0x7f0f0019

    invoke-direct {p0, v0}, Lcjs;->eW(I)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final gL(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 608
    const v1, 0x7f0f0022

    invoke-direct {p0, v1, v0}, Lcjs;->r(IZ)[Ljava/lang/String;

    move-result-object v2

    .line 609
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 610
    invoke-virtual {p1, v4}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 612
    const/4 v0, 0x1

    .line 615
    :cond_0
    return v0

    .line 609
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final gN(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 890
    const v0, 0x7f0f001d

    invoke-direct {p0, v0}, Lcjs;->eW(I)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final gO(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 932
    const v0, 0x7f0f001b

    invoke-direct {p0, v0}, Lcjs;->eW(I)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final getInt(I)I
    .locals 3

    .prologue
    .line 319
    :goto_0
    iget-object v1, p0, Lcjs;->aZe:Landroid/util/SparseArray;

    .line 320
    if-eqz v1, :cond_1

    .line 321
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 322
    if-eqz v0, :cond_2

    .line 323
    instance-of v2, v0, Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 324
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 334
    :goto_1
    return v0

    .line 326
    :cond_0
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcjs;->gP(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 330
    :cond_1
    iget-object v0, p0, Lcjs;->aZd:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 331
    invoke-direct {p0}, Lcjs;->MQ()V

    goto :goto_0

    .line 334
    :cond_2
    iget-object v0, p0, Lcjs;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    goto :goto_1
.end method
