.class public final Lfvz;
.super Lfus;
.source "PG"


# instance fields
.field private final mFifeImageUrlUtil:Lgan;


# direct methods
.method public constructor <init>(Lizq;Lftz;Lemp;Lgan;Lgbr;)V
    .locals 6

    .prologue
    .line 34
    const/4 v5, 0x3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lfus;-><init>(Lizq;Lftz;Lemp;Lgbr;I)V

    .line 36
    iput-object p4, p0, Lfvz;->mFifeImageUrlUtil:Lgan;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lizj;)Lanh;
    .locals 7

    .prologue
    const v6, 0x7f0d015b

    const v5, 0x80008

    .line 44
    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    .line 45
    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Lanh;->cm(I)Lanh;

    .line 46
    iput-object p2, v1, Lanh;->ahu:Lizj;

    .line 47
    new-instance v0, Laoc;

    invoke-direct {v0}, Laoc;-><init>()V

    iput-object v0, v1, Lanh;->agH:Laoc;

    .line 49
    iget-object v2, p2, Lizj;->dSK:Lizr;

    .line 51
    iget-object v0, v1, Lanh;->agH:Laoc;

    invoke-virtual {v2}, Lizr;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Laoc;->bs(Ljava/lang/String;)Laoc;

    .line 54
    iget-object v0, v2, Lizr;->aeB:Ljbp;

    if-eqz v0, :cond_0

    iget-object v0, v2, Lizr;->aeB:Ljbp;

    invoke-virtual {v0}, Ljbp;->oK()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, v1, Lanh;->agH:Laoc;

    iget-object v3, v2, Lizr;->aeB:Ljbp;

    invoke-virtual {v3}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Laoc;->bt(Ljava/lang/String;)Laoc;

    .line 59
    :cond_0
    invoke-virtual {v2}, Lizr;->bdr()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    invoke-virtual {v2}, Lizr;->bdt()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 63
    invoke-virtual {v2}, Lizr;->bdq()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lizr;->bds()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v0, v3, v5}, Lesi;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 70
    :goto_0
    iget-object v3, v1, Lanh;->agH:Laoc;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Laoc;->bu(Ljava/lang/String;)Laoc;

    .line 73
    :cond_1
    iget-object v0, v2, Lizr;->aiX:Ljcn;

    if-eqz v0, :cond_2

    iget-object v0, v2, Lizr;->aiX:Ljcn;

    invoke-virtual {v0}, Ljcn;->qG()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 74
    iget-object v0, p0, Lfvz;->mFifeImageUrlUtil:Lgan;

    iget-object v3, v2, Lizr;->aiX:Ljcn;

    invoke-virtual {v0, p1, v3, v6, v6}, Lgan;->a(Landroid/content/Context;Ljcn;II)Ljava/lang/String;

    move-result-object v0

    .line 76
    iget-object v3, v1, Lanh;->agH:Laoc;

    invoke-virtual {v3, v0}, Laoc;->bx(Ljava/lang/String;)Laoc;

    .line 80
    :cond_2
    iget-object v0, v2, Lizr;->dOb:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 81
    iget-object v0, v1, Lanh;->agH:Laoc;

    const-string v3, " \u00b7 "

    iget-object v4, v2, Lizr;->dOb:[Ljava/lang/String;

    invoke-static {v3, v4}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Laoc;->bw(Ljava/lang/String;)Laoc;

    .line 85
    :cond_3
    iget-object v0, v2, Lizr;->dNE:Lixx;

    if-eqz v0, :cond_4

    iget-object v0, v2, Lizr;->dNE:Lixx;

    invoke-virtual {v0}, Lixx;->baX()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 86
    new-instance v0, Lfsa;

    const/16 v3, 0x37

    invoke-direct {v0, v3}, Lfsa;-><init>(I)V

    iget-object v2, v2, Lizr;->dNE:Lixx;

    invoke-virtual {v2}, Lixx;->getUri()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    iput-object v0, v1, Lanh;->ahs:Lani;

    .line 90
    :cond_4
    return-object v1

    .line 66
    :cond_5
    invoke-virtual {v2}, Lizr;->bdq()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {p1, v0, v5, v3, v4}, Lesi;->a(Landroid/content/Context;Ljava/lang/String;IZZ)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public final aV(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v0, v0, Lizq;->dUZ:Lizj;

    .line 97
    iget-object v1, v0, Lizj;->dSL:Lizs;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lizj;->dSL:Lizs;

    invoke-virtual {v0}, Lizs;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 100
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 101
    const v0, 0x7f0a039f

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 103
    :cond_0
    return-object v0

    .line 97
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
