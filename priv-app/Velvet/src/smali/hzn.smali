.class public final Lhzn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final dvy:Livs;

.field private dvz:Ljava/util/Map;

.field private final mGsaConfigFlags:Lchk;

.field private final mLocationOracle:Lfdr;

.field private final mLocationSettings:Lcob;

.field final mSearchUrlHelper:Lcpn;


# direct methods
.method public constructor <init>(Lcpn;Lfdr;Ljava/util/concurrent/ExecutorService;Lcob;Lchk;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lhzn;->mSearchUrlHelper:Lcpn;

    .line 56
    iput-object p2, p0, Lhzn;->mLocationOracle:Lfdr;

    .line 57
    iput-object p4, p0, Lhzn;->mLocationSettings:Lcob;

    .line 58
    iput-object p5, p0, Lhzn;->mGsaConfigFlags:Lchk;

    .line 59
    instance-of v0, p3, Livs;

    if-eqz v0, :cond_0

    check-cast p3, Livs;

    :goto_0
    iput-object p3, p0, Lhzn;->dvy:Livs;

    .line 60
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lhzn;->dvz:Ljava/util/Map;

    .line 61
    return-void

    .line 59
    :cond_0
    instance-of v0, p3, Ljava/util/concurrent/ScheduledExecutorService;

    if-eqz v0, :cond_1

    new-instance v0, Livx;

    check-cast p3, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {v0, p3}, Livx;-><init>(Ljava/util/concurrent/ScheduledExecutorService;)V

    move-object p3, v0

    goto :goto_0

    :cond_1
    new-instance v0, Livv;

    invoke-direct {v0, p3}, Livv;-><init>(Ljava/util/concurrent/ExecutorService;)V

    move-object p3, v0

    goto :goto_0
.end method

.method public static b(Landroid/util/Pair;)Ljwq;
    .locals 4

    .prologue
    .line 138
    new-instance v1, Ljwq;

    invoke-direct {v1}, Ljwq;-><init>()V

    .line 139
    new-instance v2, Ljwo;

    invoke-direct {v2}, Ljwo;-><init>()V

    .line 140
    iget-object v0, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljwo;->zs(Ljava/lang/String;)Ljwo;

    .line 141
    iget-object v0, p0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljwo;->zt(Ljava/lang/String;)Ljwo;

    .line 142
    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Ljwo;->sL(I)Ljwo;

    .line 143
    const/4 v0, 0x1

    new-array v0, v0, [Ljwo;

    const/4 v3, 0x0

    aput-object v2, v0, v3

    iput-object v0, v1, Ljwq;->eJv:[Ljwo;

    .line 144
    return-object v1
.end method


# virtual methods
.method public final a(Lhzt;J)V
    .locals 10

    .prologue
    const/16 v3, 0xd8

    .line 65
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 66
    iget-object v0, p0, Lhzn;->dvz:Ljava/util/Map;

    invoke-interface {v0, p1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    iget-object v0, p0, Lhzn;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->HI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lhzn;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->HJ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lhzn;->dvy:Livs;

    new-instance v1, Lhzo;

    const-string v2, "Get Cookies"

    const/4 v4, 0x0

    new-array v4, v4, [I

    invoke-direct {v1, p0, v2, v4, p1}, Lhzo;-><init>(Lhzn;Ljava/lang/String;[ILhzt;)V

    invoke-interface {v0, v1}, Livs;->d(Ljava/util/concurrent/Callable;)Livq;

    move-result-object v0

    .line 79
    const/16 v1, 0x106

    const/16 v2, 0x107

    const v4, 0x20008

    new-instance v5, Legl;

    sget-object v7, Leoi;->cgG:Leoi;

    invoke-virtual {v7}, Leoi;->auU()J

    move-result-wide v8

    invoke-direct {v5, p2, p3, v8, v9}, Legl;-><init>(JJ)V

    invoke-static/range {v0 .. v5}, Lege;->a(Livq;IIIILegl;)V

    .line 85
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    :cond_0
    iget-object v0, p0, Lhzn;->mLocationSettings:Lcob;

    invoke-interface {v0}, Lcob;->QL()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    new-instance v7, Lhzp;

    invoke-direct {v7, p0, p1}, Lhzp;-><init>(Lhzn;Lhzt;)V

    .line 97
    iget-object v0, p0, Lhzn;->mLocationOracle:Lfdr;

    invoke-interface {v0}, Lfdr;->ayB()Livq;

    move-result-object v0

    invoke-static {v0, v7}, Livg;->a(Livq;Lifg;)Livq;

    move-result-object v0

    .line 100
    const/16 v1, 0x102

    const/16 v2, 0x103

    const v4, 0x20009

    new-instance v5, Legl;

    sget-object v8, Leoi;->cgG:Leoi;

    invoke-virtual {v8}, Leoi;->auU()J

    move-result-wide v8

    invoke-direct {v5, p2, p3, v8, v9}, Legl;-><init>(JJ)V

    invoke-static/range {v0 .. v5}, Lege;->a(Livq;IIIILegl;)V

    .line 106
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    iget-object v0, p0, Lhzn;->mLocationOracle:Lfdr;

    invoke-interface {v0}, Lfdr;->ayC()Livq;

    move-result-object v0

    invoke-static {v0, v7}, Livg;->a(Livq;Lifg;)Livq;

    move-result-object v0

    .line 111
    const/16 v1, 0x104

    const/16 v2, 0x105

    const v4, 0x2000a

    new-instance v5, Legl;

    sget-object v7, Leoi;->cgG:Leoi;

    invoke-virtual {v7}, Leoi;->auU()J

    move-result-wide v8

    invoke-direct {v5, p2, p3, v8, v9}, Legl;-><init>(JJ)V

    invoke-static/range {v0 .. v5}, Lege;->a(Livq;IIIILegl;)V

    .line 117
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    :cond_1
    return-void
.end method

.method public final b(Lhzt;)V
    .locals 3

    .prologue
    .line 126
    iget-object v0, p0, Lhzn;->dvz:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 127
    if-eqz v0, :cond_0

    .line 128
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livq;

    .line 129
    const/4 v2, 0x1

    invoke-interface {v0, v2}, Livq;->cancel(Z)Z

    goto :goto_0

    .line 132
    :cond_0
    const-string v0, "PinholeParamsUpdateHandler"

    const-string v1, "stop called for unknown listener."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->e(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 134
    :cond_1
    return-void
.end method
