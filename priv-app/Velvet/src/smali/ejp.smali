.class public abstract Lejp;
.super Landroid/support/v4/widget/DrawerLayout;
.source "PG"

# interfaces
.implements Lji;


# instance fields
.field protected cbE:Landroid/widget/FrameLayout;

.field protected cbF:Landroid/view/View;

.field private cbG:Z

.field private cbH:Z

.field protected cbI:F

.field private cbJ:F

.field private cbK:F

.field private cbL:Ljava/util/Set;

.field private cbM:F

.field private cbN:F

.field private mTouchSlop:I

.field protected yW:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0, p1}, Landroid/support/v4/widget/DrawerLayout;-><init>(Landroid/content/Context;)V

    .line 54
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lejp;->cbL:Ljava/util/Set;

    .line 61
    invoke-direct {p0, p1}, Lejp;->aq(Landroid/content/Context;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Landroid/support/v4/widget/DrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lejp;->cbL:Ljava/util/Set;

    .line 66
    invoke-direct {p0, p1}, Lejp;->aq(Landroid/content/Context;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/widget/DrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lejp;->cbL:Ljava/util/Set;

    .line 71
    invoke-direct {p0, p1}, Lejp;->aq(Landroid/content/Context;)V

    .line 72
    return-void
.end method

.method private aq(Landroid/content/Context;)V
    .locals 8

    .prologue
    const v7, 0x800003

    const/4 v1, -0x1

    const/4 v6, 0x0

    .line 75
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lejp;->cbE:Landroid/widget/FrameLayout;

    .line 76
    new-instance v0, Ljj;

    invoke-direct {v0, v1, v1, v7}, Ljj;-><init>(III)V

    .line 81
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 82
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lejp;->mTouchSlop:I

    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d02b7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lejp;->cbI:F

    .line 85
    const/high16 v2, 0x40000000    # 2.0f

    iget v3, p0, Lejp;->cbI:F

    mul-float/2addr v2, v3

    iput v2, p0, Lejp;->cbJ:F

    .line 89
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d02b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 92
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d02b5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 94
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d02b4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    .line 97
    sub-int v2, v4, v2

    invoke-static {v0, v6, v6, v2, v6}, Leot;->a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)V

    .line 100
    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    add-int v2, v3, v4

    if-le v1, v2, :cond_0

    .line 101
    iput v3, v0, Ljj;->width:I

    .line 104
    :cond_0
    iget-object v1, p0, Lejp;->cbE:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 105
    iget-object v0, p0, Lejp;->cbE:Landroid/widget/FrameLayout;

    const v1, 0x7f0b00c9

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 106
    iget-object v0, p0, Lejp;->cbE:Landroid/widget/FrameLayout;

    new-instance v1, Lejq;

    invoke-direct {v1, p0}, Lejq;-><init>(Lejp;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 114
    iput-object p0, p0, Landroid/support/v4/widget/DrawerLayout;->is:Lji;

    .line 116
    const v0, 0x7f0a002e

    invoke-virtual {p1, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {p0}, Lge;->j(Landroid/view/View;)I

    move-result v1

    invoke-static {v7, v1}, Lfi;->getAbsoluteGravity(II)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    iput-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->iv:Ljava/lang/CharSequence;

    .line 118
    :cond_1
    :goto_0
    return-void

    .line 116
    :cond_2
    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    iput-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->iw:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method private atM()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 379
    iput-boolean v0, p0, Lejp;->cbG:Z

    .line 380
    iput-boolean v0, p0, Lejp;->cbH:Z

    .line 381
    return-void
.end method


# virtual methods
.method public final A(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lejp;->cbE:Landroid/widget/FrameLayout;

    if-ne p1, v0, :cond_0

    .line 268
    iget-object v0, p0, Lejp;->cbL:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lejr;

    .line 269
    invoke-interface {v0}, Lejr;->Co()V

    goto :goto_0

    .line 272
    :cond_0
    return-void
.end method

.method protected final K(F)Z
    .locals 6

    .prologue
    const v5, 0x800005

    const v4, 0x800003

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 357
    invoke-virtual {p0}, Lejp;->getLeft()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, p1

    iget v3, p0, Lejp;->cbJ:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    invoke-virtual {p0}, Lejp;->getLeft()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, p1

    iget v3, p0, Lejp;->cbI:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_3

    .line 359
    invoke-static {p0}, Leot;->ay(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 360
    invoke-virtual {p0, v5}, Lejp;->hS(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 375
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 360
    goto :goto_0

    .line 362
    :cond_2
    invoke-virtual {p0, v4}, Lejp;->hS(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 366
    :cond_3
    invoke-virtual {p0}, Lejp;->getRight()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v2, p1

    iget v3, p0, Lejp;->cbJ:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_5

    invoke-virtual {p0}, Lejp;->getRight()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v2, p1

    iget v3, p0, Lejp;->cbI:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_5

    .line 368
    invoke-static {p0}, Leot;->ay(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 369
    invoke-virtual {p0, v4}, Lejp;->hS(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 371
    :cond_4
    invoke-virtual {p0, v5}, Lejp;->hS(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 375
    goto :goto_0
.end method

.method public final a(Lejr;)V
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lejp;->cbL:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 319
    return-void
.end method

.method protected atD()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 192
    iget-object v0, p0, Lejp;->cbF:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lejp;->yW:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lejp;->cbE:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 194
    iget-object v0, p0, Lejp;->cbF:Landroid/view/View;

    iget-object v1, p0, Lejp;->yW:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lejp;->yW:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lejp;->yW:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v4, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 196
    :cond_0
    return-void
.end method

.method public final atE()Landroid/view/View;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lejp;->cbF:Landroid/view/View;

    if-nez v0, :cond_0

    .line 200
    invoke-virtual {p0}, Lejp;->atG()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lejp;->cbF:Landroid/view/View;

    .line 201
    invoke-virtual {p0}, Lejp;->atF()V

    .line 204
    :cond_0
    iget-object v0, p0, Lejp;->cbF:Landroid/view/View;

    return-object v0
.end method

.method public atF()V
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 208
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 211
    iget-object v1, p0, Lejp;->cbF:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 212
    iget-object v0, p0, Lejp;->cbE:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lejp;->cbF:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 213
    const v0, 0x7f02005e

    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const v1, 0x800003

    invoke-static {p0}, Lge;->j(Landroid/view/View;)I

    move-result v2

    invoke-static {v1, v2}, Lfi;->getAbsoluteGravity(II)I

    move-result v1

    and-int/lit8 v2, v1, 0x3

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    iput-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->it:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->invalidate()V

    :cond_0
    and-int/lit8 v1, v1, 0x5

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    iput-object v0, p0, Landroid/support/v4/widget/DrawerLayout;->iu:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Landroid/support/v4/widget/DrawerLayout;->invalidate()V

    .line 214
    :cond_1
    invoke-virtual {p0}, Lejp;->atD()V

    .line 215
    return-void
.end method

.method public abstract atG()Landroid/view/View;
.end method

.method public final atH()V
    .locals 4

    .prologue
    const v3, 0x800003

    .line 233
    invoke-virtual {p0, v3}, Landroid/support/v4/widget/DrawerLayout;->F(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No drawer view found with gravity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/support/v4/widget/DrawerLayout;->G(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->v(Landroid/view/View;)V

    .line 234
    return-void
.end method

.method public final atI()V
    .locals 4

    .prologue
    const v3, 0x800003

    .line 237
    invoke-virtual {p0, v3}, Landroid/support/v4/widget/DrawerLayout;->F(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No drawer view found with gravity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/support/v4/widget/DrawerLayout;->G(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->w(Landroid/view/View;)V

    .line 238
    return-void
.end method

.method public final atJ()Z
    .locals 4

    .prologue
    .line 241
    const v0, 0x800003

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->F(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Landroid/support/v4/widget/DrawerLayout;->u(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "View "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a drawer"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Ljj;

    iget-boolean v0, v0, Ljj;->iE:Z

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final atK()F
    .locals 1

    .prologue
    .line 245
    iget v0, p0, Lejp;->cbK:F

    return v0
.end method

.method public final atL()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 298
    invoke-super {p0}, Landroid/support/v4/widget/DrawerLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lejp;->yW:Landroid/graphics/Rect;

    .line 229
    invoke-virtual {p0}, Lejp;->atD()V

    .line 230
    return-void
.end method

.method public final c(Landroid/view/View;F)V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lejp;->cbE:Landroid/widget/FrameLayout;

    if-ne p1, v0, :cond_0

    .line 286
    iput p2, p0, Lejp;->cbK:F

    .line 287
    iget-object v0, p0, Lejp;->cbL:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lejr;

    .line 288
    invoke-interface {v0, p2}, Lejr;->B(F)V

    goto :goto_0

    .line 291
    :cond_0
    return-void
.end method

.method public final cN()Z
    .locals 1

    .prologue
    .line 326
    invoke-virtual {p0}, Lejp;->atJ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    invoke-virtual {p0}, Lejp;->atI()V

    .line 328
    const/4 v0, 0x1

    .line 331
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 1

    .prologue
    .line 222
    invoke-virtual {p0, p1}, Lejp;->b(Landroid/graphics/Rect;)V

    .line 224
    invoke-super {p0, p1}, Landroid/support/v4/widget/DrawerLayout;->fitSystemWindows(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public final g(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 302
    invoke-super {p0, p1}, Landroid/support/v4/widget/DrawerLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 303
    return-void
.end method

.method public final getContentView()Landroid/view/View;
    .locals 4

    .prologue
    .line 249
    invoke-virtual {p0}, Lejp;->getChildCount()I

    move-result v3

    .line 250
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 251
    invoke-virtual {p0, v2}, Lejp;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 252
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Ljj;

    iget v0, v0, Ljj;->gravity:I

    if-nez v0, :cond_0

    move-object v0, v1

    .line 258
    :goto_1
    return-object v0

    .line 250
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 258
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final hS(I)Landroid/view/View;
    .locals 6

    .prologue
    .line 341
    invoke-static {p0}, Lge;->j(Landroid/view/View;)I

    move-result v0

    invoke-static {p1, v0}, Lfi;->getAbsoluteGravity(II)I

    move-result v0

    and-int/lit8 v3, v0, 0x7

    .line 343
    invoke-virtual {p0}, Lejp;->getChildCount()I

    move-result v4

    .line 344
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    .line 345
    invoke-virtual {p0, v2}, Lejp;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 346
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Ljj;

    iget v0, v0, Ljj;->gravity:I

    .line 347
    invoke-static {p0}, Lge;->j(Landroid/view/View;)I

    move-result v5

    invoke-static {v0, v5}, Lfi;->getAbsoluteGravity(II)I

    move-result v0

    .line 349
    and-int/lit8 v0, v0, 0x7

    if-ne v0, v3, :cond_0

    move-object v0, v1

    .line 353
    :goto_1
    return-object v0

    .line 344
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 353
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 122
    invoke-super {p0}, Landroid/support/v4/widget/DrawerLayout;->onFinishInflate()V

    .line 124
    iget-object v0, p0, Lejp;->cbE:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lejp;->addView(Landroid/view/View;)V

    .line 125
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 129
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 168
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/widget/DrawerLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_1
    return v0

    .line 131
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lejp;->cbM:F

    .line 132
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lejp;->cbN:F

    .line 136
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    invoke-virtual {p0, v1}, Lejp;->K(F)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    iput-boolean v0, p0, Lejp;->cbG:Z

    goto :goto_0

    .line 142
    :pswitch_1
    iget-boolean v1, p0, Lejp;->cbG:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lejp;->cbH:Z

    if-nez v1, :cond_0

    .line 143
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lejp;->cbM:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 144
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v3, p0, Lejp;->cbN:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 146
    iget v3, p0, Lejp;->mTouchSlop:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 149
    invoke-direct {p0}, Lejp;->atM()V

    goto :goto_0

    .line 150
    :cond_1
    iget v2, p0, Lejp;->mTouchSlop:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 151
    iput-boolean v0, p0, Lejp;->cbH:Z

    goto :goto_1

    .line 163
    :pswitch_2
    invoke-direct {p0}, Lejp;->atM()V

    goto :goto_0

    .line 129
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 307
    invoke-super {p0, p1}, Landroid/support/v4/widget/DrawerLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 312
    invoke-virtual {p0}, Lejp;->atJ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lejp;->cbK:F

    .line 315
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 184
    :goto_0
    :pswitch_0
    iget-boolean v0, p0, Lejp;->cbH:Z

    if-eqz v0, :cond_0

    .line 185
    const/4 v0, 0x1

    .line 188
    :goto_1
    return v0

    .line 179
    :pswitch_1
    invoke-direct {p0}, Lejp;->atM()V

    goto :goto_0

    .line 188
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/widget/DrawerLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1

    .line 173
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final z(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lejp;->cbE:Landroid/widget/FrameLayout;

    if-ne p1, v0, :cond_0

    .line 277
    iget-object v0, p0, Lejp;->cbL:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lejr;

    .line 278
    invoke-interface {v0}, Lejr;->Cn()V

    goto :goto_0

    .line 281
    :cond_0
    return-void
.end method
