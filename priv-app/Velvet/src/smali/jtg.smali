.class public final Ljtg;
.super Ljsl;
.source "PG"


# instance fields
.field public eDo:[Ljava/lang/String;

.field public eDp:[Ljti;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 299
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 300
    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljtg;->eDo:[Ljava/lang/String;

    invoke-static {}, Ljti;->but()[Ljti;

    move-result-object v0

    iput-object v0, p0, Ljtg;->eDp:[Ljti;

    const/4 v0, 0x0

    iput-object v0, p0, Ljtg;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljtg;->eCz:I

    .line 301
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljtg;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljtg;->eDo:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljtg;->eDo:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljtg;->eDo:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljtg;->eDo:[Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljtg;->eDp:[Ljti;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljti;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljtg;->eDp:[Ljti;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Ljti;

    invoke-direct {v3}, Ljti;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljtg;->eDp:[Ljti;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Ljti;

    invoke-direct {v3}, Ljti;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljtg;->eDp:[Ljti;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 314
    iget-object v0, p0, Ljtg;->eDo:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljtg;->eDo:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 315
    :goto_0
    iget-object v2, p0, Ljtg;->eDo:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 316
    iget-object v2, p0, Ljtg;->eDo:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 317
    if-eqz v2, :cond_0

    .line 318
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 315
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 322
    :cond_1
    iget-object v0, p0, Ljtg;->eDp:[Ljti;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljtg;->eDp:[Ljti;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 323
    :goto_1
    iget-object v0, p0, Ljtg;->eDp:[Ljti;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 324
    iget-object v0, p0, Ljtg;->eDp:[Ljti;

    aget-object v0, v0, v1

    .line 325
    if-eqz v0, :cond_2

    .line 326
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 323
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 330
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 331
    return-void
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 335
    invoke-super {p0}, Ljsl;->lF()I

    move-result v4

    .line 336
    iget-object v0, p0, Ljtg;->eDo:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljtg;->eDo:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    move v2, v1

    move v3, v1

    .line 339
    :goto_0
    iget-object v5, p0, Ljtg;->eDo:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_1

    .line 340
    iget-object v5, p0, Ljtg;->eDo:[Ljava/lang/String;

    aget-object v5, v5, v0

    .line 341
    if-eqz v5, :cond_0

    .line 342
    add-int/lit8 v3, v3, 0x1

    .line 343
    invoke-static {v5}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 339
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 347
    :cond_1
    add-int v0, v4, v2

    .line 348
    mul-int/lit8 v2, v3, 0x1

    add-int/2addr v0, v2

    .line 350
    :goto_1
    iget-object v2, p0, Ljtg;->eDp:[Ljti;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljtg;->eDp:[Ljti;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 351
    :goto_2
    iget-object v2, p0, Ljtg;->eDp:[Ljti;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 352
    iget-object v2, p0, Ljtg;->eDp:[Ljti;

    aget-object v2, v2, v1

    .line 353
    if-eqz v2, :cond_2

    .line 354
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 351
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 359
    :cond_3
    return v0

    :cond_4
    move v0, v4

    goto :goto_1
.end method
