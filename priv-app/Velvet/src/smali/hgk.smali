.class final Lhgk;
.super Lepm;
.source "PG"


# instance fields
.field private synthetic dgA:Ljava/lang/Integer;

.field private synthetic dgB:Lhgj;


# direct methods
.method varargs constructor <init>(Lhgj;Ljava/lang/String;[ILjava/lang/Integer;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lhgk;->dgB:Lhgj;

    iput-object p4, p0, Lhgk;->dgA:Ljava/lang/Integer;

    invoke-direct {p0, p2, p3}, Lepm;-><init>(Ljava/lang/String;[I)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 205
    iget-object v0, p0, Lhgk;->dgB:Lhgj;

    invoke-static {v0}, Lhgj;->a(Lhgj;)V

    .line 207
    iget-object v0, p0, Lhgk;->dgB:Lhgj;

    invoke-static {v0}, Lhgj;->b(Lhgj;)Lhhu;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1, v4, v5, v4}, Lhhu;->a(IILhhv;Z)V

    .line 209
    iget-object v0, p0, Lhgk;->dgB:Lhgj;

    invoke-static {v0}, Lhgj;->b(Lhgj;)Lhhu;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AudioTester_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lhgj;->aOD()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lhhu;->nY(Ljava/lang/String;)Z

    move-result v0

    .line 210
    invoke-static {}, Lhgj;->aOE()I

    .line 211
    if-nez v0, :cond_1

    .line 212
    const-string v0, "AudioTester"

    const-string v1, "Failed to tear down audio route."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 216
    :cond_1
    iget-object v0, p0, Lhgk;->dgB:Lhgj;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lhgj;->a(Lhgj;J)J

    .line 217
    iget-object v0, p0, Lhgk;->dgB:Lhgj;

    invoke-static {v0}, Lhgj;->b(Lhgj;)Lhhu;

    move-result-object v0

    iget-object v1, p0, Lhgk;->dgA:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v4, v1, v5, v4}, Lhhu;->a(IILhhv;Z)V

    .line 218
    iget-object v0, p0, Lhgk;->dgB:Lhgj;

    invoke-static {v0}, Lhgj;->b(Lhgj;)Lhhu;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AudioTester_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lhgj;->aOD()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lhhu;->nY(Ljava/lang/String;)Z

    move-result v0

    .line 219
    invoke-static {}, Lhgj;->aOE()I

    .line 220
    if-nez v0, :cond_2

    .line 221
    const-string v0, "AudioTester"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Did not get a bluetooth route for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhgk;->dgA:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    iget-object v0, p0, Lhgk;->dgB:Lhgj;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lhgj;->a(Lhgj;J)J

    goto :goto_0

    .line 228
    :cond_2
    iget-object v0, p0, Lhgk;->dgB:Lhgj;

    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    invoke-static {v0, v1}, Lhgj;->a(Lhgj;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;

    .line 229
    iget-object v0, p0, Lhgk;->dgB:Lhgj;

    invoke-static {v0}, Lhgj;->c(Lhgj;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 230
    iget-object v0, p0, Lhgk;->dgB:Lhgj;

    invoke-static {v0}, Lhgj;->d(Lhgj;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 234
    :try_start_0
    iget-object v0, p0, Lhgk;->dgB:Lhgj;

    invoke-static {v0}, Lhgj;->c(Lhgj;)Landroid/media/MediaPlayer;

    move-result-object v0

    iget-object v1, p0, Lhgk;->dgB:Lhgj;

    invoke-static {v1}, Lhgj;->d(Lhgj;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    iget-object v2, p0, Lhgk;->dgB:Lhgj;

    invoke-static {v2}, Lhgj;->d(Lhgj;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    iget-object v4, p0, Lhgk;->dgB:Lhgj;

    invoke-static {v4}, Lhgj;->d(Lhgj;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    .line 237
    iget-object v0, p0, Lhgk;->dgB:Lhgj;

    invoke-static {v0}, Lhgj;->c(Lhgj;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    .line 238
    iget-object v0, p0, Lhgk;->dgB:Lhgj;

    invoke-static {v0}, Lhgj;->c(Lhgj;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 239
    :catch_0
    move-exception v0

    .line 241
    const-string v1, "AudioTester"

    const-string v2, "Exception when preparing audio stream: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method
