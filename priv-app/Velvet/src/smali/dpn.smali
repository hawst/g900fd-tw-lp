.class final Ldpn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcex;


# instance fields
.field private synthetic bET:Ldpk;


# direct methods
.method constructor <init>(Ldpk;)V
    .locals 0

    .prologue
    .line 477
    iput-object p1, p0, Ldpn;->bET:Ldpk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final Ds()V
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Ldpn;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Yn()V

    .line 504
    return-void
.end method

.method public final a(Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 482
    iget-object v0, p0, Ldpn;->bET:Ldpk;

    iget-object v0, v0, Ldpk;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Yo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 488
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    .line 492
    sget-object v1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 494
    const-string v2, "Referer"

    invoke-static {v1}, Lesp;->aD(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    iget-object v1, p0, Ldpn;->bET:Ldpk;

    iget-object v1, v1, Ldpk;->mSearchController:Lcjt;

    new-instance v2, Lcom/google/android/search/shared/api/UriRequest;

    invoke-direct {v2, p2, v0}, Lcom/google/android/search/shared/api/UriRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;)V

    invoke-virtual {v1, v2}, Lcjt;->a(Lcom/google/android/search/shared/api/UriRequest;)V

    .line 498
    :cond_0
    return-void
.end method
