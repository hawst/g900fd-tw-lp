.class public final Ldmq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final bCt:Ldmq;


# instance fields
.field private final bCu:Z

.field private final bCv:Z

.field private final bCw:Z

.field private final bCx:Ljava/util/Set;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final bCy:Ljava/util/Set;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Ldmq;

    invoke-direct {v0}, Ldmq;-><init>()V

    sput-object v0, Ldmq;->bCt:Ldmq;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-boolean v0, p0, Ldmq;->bCu:Z

    .line 104
    iput-boolean v0, p0, Ldmq;->bCv:Z

    .line 105
    iput-boolean v0, p0, Ldmq;->bCw:Z

    .line 106
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Ldmq;->bCx:Ljava/util/Set;

    .line 107
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Ldmq;->bCy:Ljava/util/Set;

    .line 108
    return-void
.end method

.method private constructor <init>(ZZZLjava/util/Set;Ljava/util/Set;)V
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iput-boolean p1, p0, Ldmq;->bCu:Z

    .line 113
    iput-boolean p2, p0, Ldmq;->bCv:Z

    .line 114
    iput-boolean p3, p0, Ldmq;->bCw:Z

    .line 115
    invoke-static {p4}, Lijp;->G(Ljava/util/Collection;)Lijp;

    move-result-object v0

    iput-object v0, p0, Ldmq;->bCx:Ljava/util/Set;

    .line 116
    invoke-static {p5}, Lijp;->G(Ljava/util/Collection;)Lijp;

    move-result-object v0

    iput-object v0, p0, Ldmq;->bCy:Ljava/util/Set;

    .line 117
    return-void
.end method

.method private static A(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 120
    invoke-virtual {p0}, Landroid/net/Uri;->getEncodedFragment()Ljava/lang/String;

    move-result-object v0

    .line 121
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 122
    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static e(Landroid/net/Uri;Landroid/net/Uri;)Ldmq;
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 35
    invoke-static {p0, p1}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    sget-object v0, Ldmq;->bCt:Ldmq;

    .line 46
    :goto_0
    return-object v0

    .line 38
    :cond_0
    if-nez p0, :cond_1

    .line 39
    new-instance v0, Ldmq;

    invoke-static {p1}, Ldmq;->z(Landroid/net/Uri;)Ljava/util/Set;

    move-result-object v4

    invoke-static {p1}, Ldmq;->A(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Ldmq;->z(Landroid/net/Uri;)Ljava/util/Set;

    move-result-object v5

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v5}, Ldmq;-><init>(ZZZLjava/util/Set;Ljava/util/Set;)V

    goto :goto_0

    .line 42
    :cond_1
    if-nez p1, :cond_2

    .line 43
    new-instance v0, Ldmq;

    invoke-static {p0}, Ldmq;->z(Landroid/net/Uri;)Ljava/util/Set;

    move-result-object v4

    invoke-static {p0}, Ldmq;->A(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Ldmq;->z(Landroid/net/Uri;)Ljava/util/Set;

    move-result-object v5

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v5}, Ldmq;-><init>(ZZZLjava/util/Set;Ljava/util/Set;)V

    goto :goto_0

    .line 46
    :cond_2
    new-instance v0, Ldmq;

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    move v6, v1

    :goto_1
    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    move v2, v1

    :goto_2
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    move v3, v1

    :cond_3
    invoke-static {p0, p1}, Ldmq;->f(Landroid/net/Uri;Landroid/net/Uri;)Ljava/util/Set;

    move-result-object v4

    invoke-static {p0}, Ldmq;->A(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1}, Ldmq;->A(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v1, v5}, Ldmq;->f(Landroid/net/Uri;Landroid/net/Uri;)Ljava/util/Set;

    move-result-object v5

    move v1, v6

    invoke-direct/range {v0 .. v5}, Ldmq;-><init>(ZZZLjava/util/Set;Ljava/util/Set;)V

    goto :goto_0

    :cond_4
    move v6, v3

    goto :goto_1

    :cond_5
    move v2, v3

    goto :goto_2
.end method

.method private static f(Landroid/net/Uri;Landroid/net/Uri;)Ljava/util/Set;
    .locals 6

    .prologue
    .line 83
    invoke-static {p0}, Ldmq;->z(Landroid/net/Uri;)Ljava/util/Set;

    move-result-object v1

    .line 84
    invoke-static {p1}, Ldmq;->z(Landroid/net/Uri;)Ljava/util/Set;

    move-result-object v2

    .line 86
    new-instance v3, Ljava/util/HashSet;

    invoke-static {v1, v2}, Liqs;->a(Ljava/util/Set;Ljava/util/Set;)Liqx;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 87
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 88
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 91
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 92
    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 93
    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 94
    invoke-static {v5, v0}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 99
    :cond_1
    return-object v3
.end method

.method private static z(Landroid/net/Uri;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Landroid/net/Uri;->isHierarchical()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {p0}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    .line 58
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final adv()Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Ldmq;->bCu:Z

    return v0
.end method

.method public final adw()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Ldmq;->bCv:Z

    return v0
.end method

.method public final adx()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Ldmq;->bCw:Z

    return v0
.end method

.method public final ady()Ljava/util/Set;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Ldmq;->bCx:Ljava/util/Set;

    return-object v0
.end method

.method public final adz()Ljava/util/Set;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Ldmq;->bCy:Ljava/util/Set;

    return-object v0
.end method
