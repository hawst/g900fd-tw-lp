.class public final Ljls;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private agv:I

.field public esK:Ljlq;

.field public esL:[Ljln;

.field public esM:Ljln;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 8762
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 8763
    iput v2, p0, Ljls;->aez:I

    iput-object v1, p0, Ljls;->esK:Ljlq;

    invoke-static {}, Ljln;->bpo()[Ljln;

    move-result-object v0

    iput-object v0, p0, Ljls;->esL:[Ljln;

    iput-object v1, p0, Ljls;->esM:Ljln;

    iput v2, p0, Ljls;->agv:I

    iput-object v1, p0, Ljls;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljls;->eCz:I

    .line 8764
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8710
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljls;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljls;->esK:Ljlq;

    if-nez v0, :cond_1

    new-instance v0, Ljlq;

    invoke-direct {v0}, Ljlq;-><init>()V

    iput-object v0, p0, Ljls;->esK:Ljlq;

    :cond_1
    iget-object v0, p0, Ljls;->esK:Ljlq;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljls;->agv:I

    iget v0, p0, Ljls;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljls;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljls;->esL:[Ljln;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljln;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljls;->esL:[Ljln;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljln;

    invoke-direct {v3}, Ljln;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljls;->esL:[Ljln;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljln;

    invoke-direct {v3}, Ljln;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljls;->esL:[Ljln;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljls;->esM:Ljln;

    if-nez v0, :cond_5

    new-instance v0, Ljln;

    invoke-direct {v0}, Ljln;-><init>()V

    iput-object v0, p0, Ljls;->esM:Ljln;

    :cond_5
    iget-object v0, p0, Ljls;->esM:Ljln;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 8780
    iget-object v0, p0, Ljls;->esK:Ljlq;

    if-eqz v0, :cond_0

    .line 8781
    const/4 v0, 0x1

    iget-object v1, p0, Ljls;->esK:Ljlq;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 8783
    :cond_0
    iget v0, p0, Ljls;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 8784
    const/4 v0, 0x2

    iget v1, p0, Ljls;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 8786
    :cond_1
    iget-object v0, p0, Ljls;->esL:[Ljln;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljls;->esL:[Ljln;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 8787
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljls;->esL:[Ljln;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 8788
    iget-object v1, p0, Ljls;->esL:[Ljln;

    aget-object v1, v1, v0

    .line 8789
    if-eqz v1, :cond_2

    .line 8790
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 8787
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8794
    :cond_3
    iget-object v0, p0, Ljls;->esM:Ljln;

    if-eqz v0, :cond_4

    .line 8795
    const/4 v0, 0x4

    iget-object v1, p0, Ljls;->esM:Ljln;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 8797
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 8798
    return-void
.end method

.method public final getType()I
    .locals 1

    .prologue
    .line 8746
    iget v0, p0, Ljls;->agv:I

    return v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 8802
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 8803
    iget-object v1, p0, Ljls;->esK:Ljlq;

    if-eqz v1, :cond_0

    .line 8804
    const/4 v1, 0x1

    iget-object v2, p0, Ljls;->esK:Ljlq;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8807
    :cond_0
    iget v1, p0, Ljls;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 8808
    const/4 v1, 0x2

    iget v2, p0, Ljls;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8811
    :cond_1
    iget-object v1, p0, Ljls;->esL:[Ljln;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljls;->esL:[Ljln;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 8812
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljls;->esL:[Ljln;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 8813
    iget-object v2, p0, Ljls;->esL:[Ljln;

    aget-object v2, v2, v0

    .line 8814
    if-eqz v2, :cond_2

    .line 8815
    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 8812
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 8820
    :cond_4
    iget-object v1, p0, Ljls;->esM:Ljln;

    if-eqz v1, :cond_5

    .line 8821
    const/4 v1, 0x4

    iget-object v2, p0, Ljls;->esM:Ljln;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8824
    :cond_5
    return v0
.end method

.method public final qK(I)Ljls;
    .locals 1

    .prologue
    .line 8749
    iput p1, p0, Ljls;->agv:I

    .line 8750
    iget v0, p0, Ljls;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljls;->aez:I

    .line 8751
    return-object p0
.end method
