.class final Lfie;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final bOP:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final cso:Lfif;

.field final csp:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final csq:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final csr:Ljhr;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field css:I


# direct methods
.method constructor <init>(Lfif;Ljava/lang/String;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;Ljhr;I)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljhr;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1652
    iput-object p1, p0, Lfie;->cso:Lfif;

    .line 1653
    iput-object p2, p0, Lfie;->bOP:Ljava/lang/String;

    .line 1654
    iput-object p3, p0, Lfie;->csp:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 1655
    iput-object p4, p0, Lfie;->csq:Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    .line 1656
    iput-object p5, p0, Lfie;->csr:Ljhr;

    .line 1657
    iput p6, p0, Lfie;->css:I

    .line 1658
    return-void
.end method

.method public static a(Lfif;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;)Lfie;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1670
    new-instance v0, Lfie;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v6}, Lfie;-><init>(Lfif;Ljava/lang/String;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;Ljhr;I)V

    return-object v0
.end method


# virtual methods
.method public final aAf()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1686
    iget-object v2, p0, Lfie;->cso:Lfif;

    sget-object v3, Lfif;->csB:Lfif;

    if-ne v2, v3, :cond_2

    .line 1687
    iget-object v2, p0, Lfie;->csp:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v2

    .line 1688
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljdf;->bhh()Z

    move-result v2

    if-ne v2, v1, :cond_1

    :cond_0
    move v0, v1

    .line 1696
    :cond_1
    :goto_0
    return v0

    .line 1689
    :cond_2
    iget-object v2, p0, Lfie;->cso:Lfif;

    sget-object v3, Lfif;->csv:Lfif;

    if-ne v2, v3, :cond_4

    .line 1692
    iget-object v2, p0, Lfie;->csr:Ljhr;

    invoke-virtual {v2}, Ljhr;->bme()I

    move-result v2

    if-eq v2, v1, :cond_3

    iget-object v2, p0, Lfie;->csr:Ljhr;

    iget-object v2, v2, Ljhr;->emf:[Ljdj;

    array-length v2, v2

    if-gtz v2, :cond_3

    iget-object v2, p0, Lfie;->csr:Ljhr;

    iget-object v2, v2, Ljhr;->emg:[Ljhr;

    array-length v2, v2

    if-lez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1696
    goto :goto_0
.end method
