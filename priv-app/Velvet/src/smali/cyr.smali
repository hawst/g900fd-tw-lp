.class public final Lcyr;
.super Lcyc;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private bnp:Landroid/preference/ListPreference;


# direct methods
.method public constructor <init>(Lcke;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcyc;-><init>(Lcke;)V

    .line 23
    return-void
.end method

.method private getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcyr;->bnp:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private jc(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 43
    const-string v0, "handsFreeOnly"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    const v0, 0x7f0a0875

    invoke-direct {p0, v0}, Lcyr;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 51
    :goto_0
    iget-object v1, p0, Lcyr;->bnp:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 52
    return-void

    .line 46
    :cond_0
    const-string v0, "never"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47
    const v0, 0x7f0a0876

    invoke-direct {p0, v0}, Lcyr;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 49
    :cond_1
    const v0, 0x7f0a0874

    invoke-direct {p0, v0}, Lcyr;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 1

    .prologue
    .line 27
    check-cast p1, Landroid/preference/ListPreference;

    iput-object p1, p0, Lcyr;->bnp:Landroid/preference/ListPreference;

    .line 28
    iget-object v0, p0, Lcyr;->bnp:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 29
    iget-object v0, p0, Lcyr;->bnp:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcyr;->jc(Ljava/lang/String;)V

    .line 30
    return-void
.end method

.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcyr;->jc(Ljava/lang/String;)V

    .line 35
    const/4 v0, 0x1

    return v0
.end method
