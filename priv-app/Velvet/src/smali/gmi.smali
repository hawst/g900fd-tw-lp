.class public final Lgmi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lceq;
.implements Lhzt;


# instance fields
.field private final cPf:Ljava/lang/Object;

.field private final cPg:Ljava/util/Queue;

.field private cPh:Lhzs;

.field private mClosed:Z


# direct methods
.method public constructor <init>(Lhzs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lgmi;->cPf:Ljava/lang/Object;

    .line 33
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lgmi;->cPg:Ljava/util/Queue;

    .line 34
    iput-object p1, p0, Lgmi;->cPh:Lhzs;

    .line 35
    iget-object v0, p0, Lgmi;->cPh:Lhzs;

    invoke-interface {v0, p0, p2}, Lhzs;->a(Lhzt;Ljava/lang/String;)V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgmi;->mClosed:Z

    .line 37
    return-void
.end method


# virtual methods
.method public final Dr()Ljwv;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-static {}, Lcek;->Do()Ljwv;

    move-result-object v1

    .line 42
    iget-boolean v2, p0, Lgmi;->mClosed:Z

    if-eqz v2, :cond_0

    .line 50
    :goto_0
    return-object v0

    .line 45
    :cond_0
    iget-object v2, p0, Lgmi;->cPf:Ljava/lang/Object;

    monitor-enter v2

    .line 46
    :try_start_0
    iget-object v3, p0, Lgmi;->cPg:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 47
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 49
    :cond_1
    :try_start_1
    sget-object v0, Ljwq;->eJt:Ljsm;

    iget-object v3, p0, Lgmi;->cPg:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Ljwv;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 50
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_0
.end method

.method public final synthetic aT(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 21
    check-cast p1, Ljwq;

    iget-object v1, p0, Lgmi;->cPf:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lgmi;->cPg:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgmi;->mClosed:Z

    .line 57
    iget-object v0, p0, Lgmi;->cPh:Lhzs;

    invoke-interface {v0, p0}, Lhzs;->a(Lhzt;)V

    .line 58
    return-void
.end method

.method public final yo()Z
    .locals 2

    .prologue
    .line 62
    iget-object v1, p0, Lgmi;->cPf:Ljava/lang/Object;

    monitor-enter v1

    .line 63
    :try_start_0
    iget-object v0, p0, Lgmi;->cPg:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
