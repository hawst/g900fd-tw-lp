.class final Lglh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhgm;


# instance fields
.field bxW:Ljava/util/List;

.field private final mNow:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lglh;->bxW:Ljava/util/List;

    .line 40
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lglh;->mNow:J

    .line 41
    return-void
.end method


# virtual methods
.method public final h(Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 45
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_0

    .line 47
    iget-object v1, p0, Lglh;->bxW:Ljava/util/List;

    new-instance v2, Lglf;

    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iget-wide v4, p0, Lglh;->mNow:J

    const/4 v6, 0x2

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-direct {v2, v0, v3, v4, v5}, Lglf;-><init>(Ljava/lang/String;IJ)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    :goto_0
    return-void

    .line 49
    :cond_0
    const-string v0, "GrammarContactRetriever"

    const-string v1, "Provider returned null display name."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method
