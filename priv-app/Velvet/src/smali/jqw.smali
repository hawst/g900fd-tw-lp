.class public final Ljqw;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ezD:[Ljqw;


# instance fields
.field private aez:I

.field private afW:Ljava/lang/String;

.field private afh:Ljava/lang/String;

.field private aiK:Ljava/lang/String;

.field private dHr:Ljava/lang/String;

.field private dMI:Ljava/lang/String;

.field private ezo:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1916
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1917
    iput v1, p0, Ljqw;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljqw;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljqw;->aiK:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljqw;->dHr:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljqw;->afW:Ljava/lang/String;

    iput v1, p0, Ljqw;->ezo:I

    const-string v0, ""

    iput-object v0, p0, Ljqw;->dMI:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljqw;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqw;->eCz:I

    .line 1918
    return-void
.end method

.method public static bsI()[Ljqw;
    .locals 2

    .prologue
    .line 1774
    sget-object v0, Ljqw;->ezD:[Ljqw;

    if-nez v0, :cond_1

    .line 1775
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 1777
    :try_start_0
    sget-object v0, Ljqw;->ezD:[Ljqw;

    if-nez v0, :cond_0

    .line 1778
    const/4 v0, 0x0

    new-array v0, v0, [Ljqw;

    sput-object v0, Ljqw;->ezD:[Ljqw;

    .line 1780
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1782
    :cond_1
    sget-object v0, Ljqw;->ezD:[Ljqw;

    return-object v0

    .line 1780
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 1768
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqw;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqw;->afh:Ljava/lang/String;

    iget v0, p0, Ljqw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqw;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqw;->aiK:Ljava/lang/String;

    iget v0, p0, Ljqw;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqw;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqw;->dHr:Ljava/lang/String;

    iget v0, p0, Ljqw;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljqw;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqw;->afW:Ljava/lang/String;

    iget v0, p0, Ljqw;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljqw;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljqw;->ezo:I

    iget v0, p0, Ljqw;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljqw;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqw;->dMI:Ljava/lang/String;

    iget v0, p0, Ljqw;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljqw;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x38 -> :sswitch_5
        0x42 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 1936
    iget v0, p0, Ljqw;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1937
    const/4 v0, 0x1

    iget-object v1, p0, Ljqw;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1939
    :cond_0
    iget v0, p0, Ljqw;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1940
    const/4 v0, 0x2

    iget-object v1, p0, Ljqw;->aiK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1942
    :cond_1
    iget v0, p0, Ljqw;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 1943
    const/4 v0, 0x3

    iget-object v1, p0, Ljqw;->dHr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1945
    :cond_2
    iget v0, p0, Ljqw;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 1946
    const/4 v0, 0x4

    iget-object v1, p0, Ljqw;->afW:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1948
    :cond_3
    iget v0, p0, Ljqw;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 1949
    const/4 v0, 0x7

    iget v1, p0, Ljqw;->ezo:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1951
    :cond_4
    iget v0, p0, Ljqw;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 1952
    const/16 v0, 0x8

    iget-object v1, p0, Ljqw;->dMI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1954
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1955
    return-void
.end method

.method public final bsC()Z
    .locals 1

    .prologue
    .line 1886
    iget v0, p0, Ljqw;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bsy()I
    .locals 1

    .prologue
    .line 1878
    iget v0, p0, Ljqw;->ezo:I

    return v0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1812
    iget-object v0, p0, Ljqw;->aiK:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1790
    iget-object v0, p0, Ljqw;->afh:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1897
    iget-object v0, p0, Ljqw;->dMI:Ljava/lang/String;

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1834
    iget-object v0, p0, Ljqw;->dHr:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1959
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1960
    iget v1, p0, Ljqw;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1961
    const/4 v1, 0x1

    iget-object v2, p0, Ljqw;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1964
    :cond_0
    iget v1, p0, Ljqw;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1965
    const/4 v1, 0x2

    iget-object v2, p0, Ljqw;->aiK:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1968
    :cond_1
    iget v1, p0, Ljqw;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 1969
    const/4 v1, 0x3

    iget-object v2, p0, Ljqw;->dHr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1972
    :cond_2
    iget v1, p0, Ljqw;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 1973
    const/4 v1, 0x4

    iget-object v2, p0, Ljqw;->afW:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1976
    :cond_3
    iget v1, p0, Ljqw;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 1977
    const/4 v1, 0x7

    iget v2, p0, Ljqw;->ezo:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1980
    :cond_4
    iget v1, p0, Ljqw;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 1981
    const/16 v1, 0x8

    iget-object v2, p0, Ljqw;->dMI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1984
    :cond_5
    return v0
.end method

.method public final oP()Z
    .locals 1

    .prologue
    .line 1908
    iget v0, p0, Ljqw;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final oj()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1856
    iget-object v0, p0, Ljqw;->afW:Ljava/lang/String;

    return-object v0
.end method

.method public final rD(I)Ljqw;
    .locals 1

    .prologue
    .line 1881
    iput p1, p0, Ljqw;->ezo:I

    .line 1882
    iget v0, p0, Ljqw;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljqw;->aez:I

    .line 1883
    return-object p0
.end method

.method public final ym(Ljava/lang/String;)Ljqw;
    .locals 1

    .prologue
    .line 1793
    if-nez p1, :cond_0

    .line 1794
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1796
    :cond_0
    iput-object p1, p0, Ljqw;->afh:Ljava/lang/String;

    .line 1797
    iget v0, p0, Ljqw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqw;->aez:I

    .line 1798
    return-object p0
.end method

.method public final yn(Ljava/lang/String;)Ljqw;
    .locals 1

    .prologue
    .line 1815
    if-nez p1, :cond_0

    .line 1816
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1818
    :cond_0
    iput-object p1, p0, Ljqw;->aiK:Ljava/lang/String;

    .line 1819
    iget v0, p0, Ljqw;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqw;->aez:I

    .line 1820
    return-object p0
.end method

.method public final yo(Ljava/lang/String;)Ljqw;
    .locals 1

    .prologue
    .line 1837
    if-nez p1, :cond_0

    .line 1838
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1840
    :cond_0
    iput-object p1, p0, Ljqw;->dHr:Ljava/lang/String;

    .line 1841
    iget v0, p0, Ljqw;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljqw;->aez:I

    .line 1842
    return-object p0
.end method

.method public final yp(Ljava/lang/String;)Ljqw;
    .locals 1

    .prologue
    .line 1859
    if-nez p1, :cond_0

    .line 1860
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1862
    :cond_0
    iput-object p1, p0, Ljqw;->afW:Ljava/lang/String;

    .line 1863
    iget v0, p0, Ljqw;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljqw;->aez:I

    .line 1864
    return-object p0
.end method
