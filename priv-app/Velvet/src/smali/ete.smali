.class public abstract Lete;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leml;


# instance fields
.field protected final ciu:Leps;

.field protected final civ:Ljava/util/Map;


# direct methods
.method constructor <init>(Leps;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lete;->ciu:Leps;

    .line 20
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lete;->civ:Ljava/util/Map;

    .line 21
    return-void
.end method


# virtual methods
.method public final auB()Z
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lete;->ciu:Leps;

    invoke-interface {v0}, Leps;->auB()Z

    move-result v0

    return v0
.end method

.method public final e(Lemy;)V
    .locals 3

    .prologue
    .line 25
    iget-object v0, p0, Lete;->civ:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 26
    invoke-virtual {p0, p1}, Lete;->g(Lemy;)Lemy;

    move-result-object v0

    .line 27
    iget-object v1, p0, Lete;->civ:Ljava/util/Map;

    monitor-enter v1

    .line 28
    :try_start_0
    iget-object v2, p0, Lete;->civ:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    iget-object v1, p0, Lete;->ciu:Leps;

    invoke-interface {v1, v0}, Leps;->e(Lemy;)V

    .line 31
    return-void

    .line 25
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f(Lemy;)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lete;->civ:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lemy;

    .line 53
    if-eqz v0, :cond_0

    iget-object v1, p0, Lete;->ciu:Leps;

    instance-of v1, v1, Leml;

    if-eqz v1, :cond_0

    .line 54
    iget-object v1, p0, Lete;->ciu:Leps;

    check-cast v1, Leml;

    invoke-interface {v1, v0}, Leml;->f(Lemy;)V

    .line 56
    :cond_0
    return-void
.end method

.method protected abstract g(Lemy;)Lemy;
.end method
