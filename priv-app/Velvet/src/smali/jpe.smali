.class public final Ljpe;
.super Ljsl;
.source "PG"


# static fields
.field public static final exB:Ljsm;


# instance fields
.field private aez:I

.field private ahB:I

.field private erc:Ljava/lang/String;

.field private err:I

.field private esF:Ljava/lang/String;

.field public exC:[Ljpd;

.field public exD:[Ljpd;

.field private exE:[B


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 6286
    const/16 v0, 0xb

    const-class v1, Ljpe;

    const v2, 0xe524252

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljpe;->exB:Ljsm;

    .line 6299
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6416
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 6417
    iput v1, p0, Ljpe;->aez:I

    invoke-static {}, Ljpd;->brE()[Ljpd;

    move-result-object v0

    iput-object v0, p0, Ljpe;->exC:[Ljpd;

    invoke-static {}, Ljpd;->brE()[Ljpd;

    move-result-object v0

    iput-object v0, p0, Ljpe;->exD:[Ljpd;

    const-string v0, ""

    iput-object v0, p0, Ljpe;->erc:Ljava/lang/String;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljpe;->exE:[B

    const-string v0, ""

    iput-object v0, p0, Ljpe;->esF:Ljava/lang/String;

    iput v1, p0, Ljpe;->ahB:I

    iput v1, p0, Ljpe;->err:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljpe;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljpe;->eCz:I

    .line 6418
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6279
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljpe;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljpe;->exC:[Ljpd;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljpd;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljpe;->exC:[Ljpd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljpd;

    invoke-direct {v3}, Ljpd;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljpe;->exC:[Ljpd;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljpd;

    invoke-direct {v3}, Ljpd;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljpe;->exC:[Ljpd;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljpe;->erc:Ljava/lang/String;

    iget v0, p0, Ljpe;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpe;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljpe;->ahB:I

    iget v0, p0, Ljpe;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljpe;->aez:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljpe;->exD:[Ljpd;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljpd;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljpe;->exD:[Ljpd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Ljpd;

    invoke-direct {v3}, Ljpd;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljpe;->exD:[Ljpd;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Ljpd;

    invoke-direct {v3}, Ljpd;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljpe;->exD:[Ljpd;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljpe;->err:I

    iget v0, p0, Ljpe;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljpe;->aez:I

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljpe;->esF:Ljava/lang/String;

    iget v0, p0, Ljpe;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljpe;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljpe;->exE:[B

    iget v0, p0, Ljpe;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljpe;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6437
    iget-object v0, p0, Ljpe;->exC:[Ljpd;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljpe;->exC:[Ljpd;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 6438
    :goto_0
    iget-object v2, p0, Ljpe;->exC:[Ljpd;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 6439
    iget-object v2, p0, Ljpe;->exC:[Ljpd;

    aget-object v2, v2, v0

    .line 6440
    if-eqz v2, :cond_0

    .line 6441
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 6438
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6445
    :cond_1
    iget v0, p0, Ljpe;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 6446
    const/4 v0, 0x2

    iget-object v2, p0, Ljpe;->erc:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 6448
    :cond_2
    iget v0, p0, Ljpe;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 6449
    const/4 v0, 0x3

    iget v2, p0, Ljpe;->ahB:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 6451
    :cond_3
    iget-object v0, p0, Ljpe;->exD:[Ljpd;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljpe;->exD:[Ljpd;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 6452
    :goto_1
    iget-object v0, p0, Ljpe;->exD:[Ljpd;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 6453
    iget-object v0, p0, Ljpe;->exD:[Ljpd;

    aget-object v0, v0, v1

    .line 6454
    if-eqz v0, :cond_4

    .line 6455
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 6452
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6459
    :cond_5
    iget v0, p0, Ljpe;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    .line 6460
    const/4 v0, 0x5

    iget v1, p0, Ljpe;->err:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 6462
    :cond_6
    iget v0, p0, Ljpe;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_7

    .line 6463
    const/4 v0, 0x6

    iget-object v1, p0, Ljpe;->esF:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6465
    :cond_7
    iget v0, p0, Ljpe;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_8

    .line 6466
    const/4 v0, 0x7

    iget-object v1, p0, Ljpe;->exE:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 6468
    :cond_8
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 6469
    return-void
.end method

.method public final brF()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6359
    iget-object v0, p0, Ljpe;->esF:Ljava/lang/String;

    return-object v0
.end method

.method public final brQ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6315
    iget-object v0, p0, Ljpe;->erc:Ljava/lang/String;

    return-object v0
.end method

.method public final brR()[B
    .locals 1

    .prologue
    .line 6337
    iget-object v0, p0, Ljpe;->exE:[B

    return-object v0
.end method

.method public final brS()Z
    .locals 1

    .prologue
    .line 6348
    iget v0, p0, Ljpe;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final brT()I
    .locals 1

    .prologue
    .line 6400
    iget v0, p0, Ljpe;->err:I

    return v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 6473
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 6474
    iget-object v2, p0, Ljpe;->exC:[Ljpd;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ljpe;->exC:[Ljpd;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 6475
    :goto_0
    iget-object v3, p0, Ljpe;->exC:[Ljpd;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 6476
    iget-object v3, p0, Ljpe;->exC:[Ljpd;

    aget-object v3, v3, v0

    .line 6477
    if-eqz v3, :cond_0

    .line 6478
    const/4 v4, 0x1

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 6475
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 6483
    :cond_2
    iget v2, p0, Ljpe;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    .line 6484
    const/4 v2, 0x2

    iget-object v3, p0, Ljpe;->erc:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6487
    :cond_3
    iget v2, p0, Ljpe;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_4

    .line 6488
    const/4 v2, 0x3

    iget v3, p0, Ljpe;->ahB:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 6491
    :cond_4
    iget-object v2, p0, Ljpe;->exD:[Ljpd;

    if-eqz v2, :cond_6

    iget-object v2, p0, Ljpe;->exD:[Ljpd;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 6492
    :goto_1
    iget-object v2, p0, Ljpe;->exD:[Ljpd;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 6493
    iget-object v2, p0, Ljpe;->exD:[Ljpd;

    aget-object v2, v2, v1

    .line 6494
    if-eqz v2, :cond_5

    .line 6495
    const/4 v3, 0x4

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6492
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6500
    :cond_6
    iget v1, p0, Ljpe;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_7

    .line 6501
    const/4 v1, 0x5

    iget v2, p0, Ljpe;->err:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6504
    :cond_7
    iget v1, p0, Ljpe;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_8

    .line 6505
    const/4 v1, 0x6

    iget-object v2, p0, Ljpe;->esF:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6508
    :cond_8
    iget v1, p0, Ljpe;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_9

    .line 6509
    const/4 v1, 0x7

    iget-object v2, p0, Ljpe;->exE:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 6512
    :cond_9
    return v0
.end method

.method public final oY()I
    .locals 1

    .prologue
    .line 6381
    iget v0, p0, Ljpe;->ahB:I

    return v0
.end method

.method public final rc(I)Ljpe;
    .locals 1

    .prologue
    .line 6384
    iput p1, p0, Ljpe;->ahB:I

    .line 6385
    iget v0, p0, Ljpe;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljpe;->aez:I

    .line 6386
    return-object p0
.end method

.method public final rd(I)Ljpe;
    .locals 1

    .prologue
    .line 6403
    iput p1, p0, Ljpe;->err:I

    .line 6404
    iget v0, p0, Ljpe;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljpe;->aez:I

    .line 6405
    return-object p0
.end method

.method public final ya(Ljava/lang/String;)Ljpe;
    .locals 1

    .prologue
    .line 6318
    if-nez p1, :cond_0

    .line 6319
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6321
    :cond_0
    iput-object p1, p0, Ljpe;->erc:Ljava/lang/String;

    .line 6322
    iget v0, p0, Ljpe;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpe;->aez:I

    .line 6323
    return-object p0
.end method

.method public final yb(Ljava/lang/String;)Ljpe;
    .locals 1

    .prologue
    .line 6362
    if-nez p1, :cond_0

    .line 6363
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6365
    :cond_0
    iput-object p1, p0, Ljpe;->esF:Ljava/lang/String;

    .line 6366
    iget v0, p0, Ljpe;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljpe;->aez:I

    .line 6367
    return-object p0
.end method
