.class public final Lefl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lefk;
.implements Ljava/util/concurrent/Future;


# instance fields
.field private final bYa:Livy;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Livy;

    invoke-direct {v0}, Livy;-><init>()V

    iput-object v0, p0, Lefl;->bYa:Livy;

    return-void
.end method


# virtual methods
.method public final ar(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lefl;->bYa:Livy;

    invoke-virtual {v0, p1}, Livy;->bB(Ljava/lang/Object;)Z

    .line 19
    return-void
.end method

.method public final cancel(Z)Z
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    return v0
.end method

.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lefl;->bYa:Livy;

    invoke-virtual {v0}, Livy;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lefl;->bYa:Livy;

    invoke-virtual {v0, p1, p2, p3}, Livy;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final isCancelled()Z
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lefl;->bYa:Livy;

    invoke-virtual {v0}, Livy;->isCancelled()Z

    move-result v0

    return v0
.end method

.method public final isDone()Z
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lefl;->bYa:Livy;

    invoke-virtual {v0}, Livy;->isDone()Z

    move-result v0

    return v0
.end method
