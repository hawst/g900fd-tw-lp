.class public Ldct;
.super Lddj;
.source "PG"


# instance fields
.field private So:I

.field private bsn:I

.field private bso:I

.field private bsp:I

.field private bsq:Ljava/lang/String;

.field private bsr:Lcom/google/android/search/shared/service/ClientConfig;

.field private bss:I

.field private final mEventBus:Ldda;

.field private final mGsaConfig:Lchk;


# direct methods
.method public constructor <init>(Ldda;Lchk;)V
    .locals 1

    .prologue
    .line 39
    const/16 v0, 0x100

    invoke-direct {p0, p1, v0}, Lddj;-><init>(Ldda;I)V

    .line 40
    iput-object p1, p0, Ldct;->mEventBus:Ldda;

    .line 41
    iput-object p2, p0, Ldct;->mGsaConfig:Lchk;

    .line 42
    const-string v0, ""

    iput-object v0, p0, Ldct;->bsq:Ljava/lang/String;

    .line 43
    return-void
.end method


# virtual methods
.method public final Yv()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Ldct;->bsn:I

    return v0
.end method

.method public final Yw()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Ldct;->bso:I

    return v0
.end method

.method public final Yx()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Ldct;->bsp:I

    return v0
.end method

.method public final Yy()I
    .locals 1

    .prologue
    .line 241
    iget v0, p0, Ldct;->bss:I

    return v0
.end method

.method public final a(Lddb;)V
    .locals 13

    .prologue
    const/4 v4, 0x6

    const/4 v5, 0x5

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 48
    .line 50
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lddb;->aay()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lddb;->aaz()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lddb;->aaB()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lddb;->aaD()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lddb;->aaE()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 53
    :cond_0
    iget-object v0, p0, Ldct;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqU()Z

    move-result v0

    if-eqz v0, :cond_28

    const/16 v0, 0x8

    :goto_0
    iget-object v3, p0, Ldct;->mEventBus:Ldda;

    invoke-virtual {v3}, Ldda;->aal()Ldby;

    move-result-object v3

    invoke-virtual {v3}, Ldby;->Wz()Z

    move-result v6

    if-eqz v6, :cond_1

    or-int/lit16 v0, v0, 0x100

    :cond_1
    invoke-virtual {v3}, Ldby;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_e

    invoke-virtual {v3}, Ldby;->getPrompt()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Ldct;->bsq:Ljava/lang/String;

    invoke-virtual {v3}, Ldby;->WH()Z

    move-result v6

    if-eqz v6, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    invoke-virtual {v3}, Ldby;->WF()Z

    move-result v6

    if-eqz v6, :cond_3

    or-int/lit8 v0, v0, 0x1

    :cond_3
    invoke-virtual {v3}, Ldby;->WG()Z

    move-result v3

    if-eqz v3, :cond_4

    or-int/lit8 v0, v0, 0x2

    :cond_4
    :goto_1
    iget-object v3, p0, Ldct;->mEventBus:Ldda;

    invoke-virtual {v3}, Ldda;->aak()Ldcw;

    move-result-object v3

    invoke-virtual {v3}, Ldcw;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget-object v3, p0, Ldct;->mEventBus:Ldda;

    invoke-virtual {v3}, Ldda;->aao()Ldcu;

    move-result-object v3

    invoke-virtual {v3}, Ldcu;->YZ()Z

    move-result v3

    if-eqz v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v3, p0, Ldct;->mGsaConfig:Lchk;

    invoke-virtual {v3}, Lchk;->HF()Z

    move-result v3

    if-eqz v3, :cond_7

    or-int/lit8 v0, v0, 0x20

    :cond_7
    iget-object v3, p0, Ldct;->mEventBus:Ldda;

    invoke-virtual {v3}, Ldda;->aau()Ldbh;

    move-result-object v3

    invoke-virtual {v3}, Ldbh;->Wg()Z

    move-result v3

    if-eqz v3, :cond_8

    or-int/lit8 v0, v0, 0x40

    :cond_8
    iget-object v3, p0, Ldct;->mEventBus:Ldda;

    invoke-virtual {v3}, Ldda;->aau()Ldbh;

    move-result-object v3

    invoke-virtual {v3}, Ldbh;->Wf()Z

    move-result v3

    if-eqz v3, :cond_9

    or-int/lit16 v0, v0, 0x200

    :cond_9
    iget-object v3, p0, Ldct;->mEventBus:Ldda;

    invoke-virtual {v3}, Ldda;->aao()Ldcu;

    move-result-object v3

    invoke-virtual {v3}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v3

    iget v6, p0, Ldct;->bsn:I

    if-ne v6, v0, :cond_a

    iget-object v6, p0, Ldct;->bsr:Lcom/google/android/search/shared/service/ClientConfig;

    if-eq v6, v3, :cond_f

    :cond_a
    iput-object v3, p0, Ldct;->bsr:Lcom/google/android/search/shared/service/ClientConfig;

    iput v0, p0, Ldct;->bsn:I

    move v0, v2

    :goto_2
    or-int/lit8 v6, v0, 0x0

    .line 54
    iget-object v0, p0, Ldct;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v7

    iget-object v0, p0, Ldct;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v8

    iget-object v0, p0, Ldct;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v9

    iget-object v0, p0, Ldct;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aao()Ldcu;

    move-result-object v10

    invoke-virtual {v7}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v11

    iget v3, p0, Ldct;->bso:I

    invoke-virtual {v11}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-nez v0, :cond_b

    invoke-virtual {v11}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v0

    if-nez v0, :cond_b

    invoke-virtual {v11}, Lcom/google/android/shared/search/Query;->aqo()Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_b
    move v0, v2

    :goto_3
    invoke-virtual {v11}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v12

    if-eqz v12, :cond_14

    invoke-virtual {v7}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v0

    if-eqz v0, :cond_11

    move v0, v2

    :goto_4
    iget v3, p0, Ldct;->bso:I

    if-eq v3, v0, :cond_22

    iput v0, p0, Ldct;->bso:I

    invoke-virtual {v11}, Lcom/google/android/shared/search/Query;->aqd()Z

    move-result v0

    if-eqz v0, :cond_21

    move v0, v2

    :goto_5
    iput v0, p0, Ldct;->bsp:I

    move v0, v2

    :goto_6
    or-int v3, v6, v0

    .line 55
    iget-object v0, p0, Ldct;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->HR()Z

    move-result v0

    if-eqz v0, :cond_24

    iget-object v0, p0, Ldct;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xh()Z

    move-result v4

    if-eqz v4, :cond_26

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v4

    if-eqz v4, :cond_26

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Yf()Z

    move-result v0

    if-eqz v0, :cond_23

    const/16 v0, 0x64

    :goto_7
    iget v4, p0, Ldct;->So:I

    if-eq v4, v0, :cond_25

    iput v0, p0, Ldct;->So:I

    :goto_8
    or-int v1, v3, v2

    .line 58
    :cond_c
    if-eqz v1, :cond_d

    .line 60
    invoke-virtual {p0}, Ldct;->notifyChanged()V

    .line 62
    :cond_d
    return-void

    .line 53
    :cond_e
    const-string v3, ""

    iput-object v3, p0, Ldct;->bsq:Ljava/lang/String;

    goto/16 :goto_1

    :cond_f
    move v0, v1

    goto/16 :goto_2

    :cond_10
    move v0, v1

    .line 54
    goto :goto_3

    :cond_11
    invoke-static {v11}, Lgyt;->aY(Lcom/google/android/shared/search/Query;)Lgyt;

    move-result-object v0

    sget-object v4, Lgyt;->cZA:Lgyt;

    if-ne v0, v4, :cond_12

    iget-object v4, p0, Ldct;->mEventBus:Ldda;

    invoke-virtual {v4}, Ldda;->aao()Ldcu;

    move-result-object v4

    invoke-virtual {v4}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/search/shared/service/ClientConfig;->anv()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/shared/search/SearchBoxStats;->QW()Ljava/lang/String;

    move-result-object v4

    const-string v5, "velvet"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_27

    :cond_12
    sget-object v3, Lgyt;->cZD:Lgyt;

    if-ne v0, v3, :cond_13

    move v0, v2

    goto :goto_4

    :cond_13
    move v0, v1

    goto :goto_4

    :cond_14
    invoke-virtual {v11}, Lcom/google/android/shared/search/Query;->aqs()Z

    move-result v3

    if-eqz v3, :cond_15

    move v0, v1

    goto/16 :goto_4

    :cond_15
    invoke-virtual {v7}, Lcom/google/android/search/core/state/QueryState;->Yi()Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v3

    if-eqz v3, :cond_17

    sget-object v3, Lcgg;->aVm:Lcgg;

    invoke-virtual {v3}, Lcgg;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_16

    if-eqz v0, :cond_16

    move v0, v4

    goto/16 :goto_4

    :cond_16
    move v0, v5

    goto/16 :goto_4

    :cond_17
    invoke-virtual {v10}, Ldcu;->Zf()Z

    move-result v3

    if-eqz v3, :cond_19

    invoke-virtual {v11}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-eqz v0, :cond_18

    move v0, v4

    goto/16 :goto_4

    :cond_18
    move v0, v5

    goto/16 :goto_4

    :cond_19
    invoke-virtual {v11}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v3

    if-eqz v3, :cond_1c

    invoke-virtual {v11}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1a

    invoke-virtual {v10}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/search/shared/service/ClientConfig;->amV()Z

    move-result v3

    if-eqz v3, :cond_1c

    :cond_1a
    invoke-virtual {v9}, Ldcy;->ZB()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-virtual {v10}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->anh()Z

    move-result v0

    if-eqz v0, :cond_1b

    const/4 v0, 0x7

    goto/16 :goto_4

    :cond_1b
    const/4 v0, 0x2

    goto/16 :goto_4

    :cond_1c
    invoke-virtual {v11}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v3

    if-eqz v3, :cond_1d

    invoke-virtual {v8, v11}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v3

    if-nez v3, :cond_1d

    const/4 v0, 0x3

    goto/16 :goto_4

    :cond_1d
    invoke-virtual {v11}, Lcom/google/android/shared/search/Query;->aqo()Z

    move-result v3

    if-eqz v3, :cond_1e

    invoke-virtual {v8, v11}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v3

    if-nez v3, :cond_1e

    const/4 v0, 0x4

    goto/16 :goto_4

    :cond_1e
    if-eqz v0, :cond_1f

    move v0, v4

    goto/16 :goto_4

    :cond_1f
    invoke-virtual {v7}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v0

    if-eqz v0, :cond_20

    move v0, v2

    goto/16 :goto_4

    :cond_20
    move v0, v5

    goto/16 :goto_4

    :cond_21
    move v0, v1

    goto/16 :goto_5

    :cond_22
    move v0, v1

    goto/16 :goto_6

    .line 55
    :cond_23
    iget-object v0, p0, Ldct;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-virtual {v0}, Ldcy;->getProgress()I

    move-result v0

    goto/16 :goto_7

    :cond_24
    const/4 v0, -0x1

    goto/16 :goto_7

    :cond_25
    move v2, v1

    goto/16 :goto_8

    :cond_26
    move v0, v1

    goto/16 :goto_7

    :cond_27
    move v0, v3

    goto/16 :goto_4

    :cond_28
    move v0, v1

    goto/16 :goto_0
.end method

.method public final fE(I)V
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Ldct;->bss:I

    if-eq v0, p1, :cond_0

    .line 235
    iput p1, p0, Ldct;->bss:I

    .line 236
    invoke-virtual {p0}, Ldct;->notifyChanged()V

    .line 238
    :cond_0
    return-void
.end method

.method public final getProgress()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Ldct;->So:I

    return v0
.end method

.method public final tR()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Ldct;->bsq:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 284
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SearchPlateState[Mode:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Ldct;->bso:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ldct;->bsp:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Ext:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ldct;->bsn:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " HwPrompt:\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldct;->bsq:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" Progress:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ldct;->So:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
