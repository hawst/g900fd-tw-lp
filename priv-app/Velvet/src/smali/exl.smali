.class public final Lexl;
.super Landroid/app/Dialog;
.source "PG"

# interfaces
.implements Landroid/app/FragmentManager$OnBackStackChangedListener;
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnShowListener;


# instance fields
.field private be:Z

.field private boe:I

.field private final cli:Landroid/content/DialogInterface$OnClickListener;

.field private final clj:Landroid/app/FragmentManager;

.field private final clk:Landroid/app/AlertDialog$Builder;

.field private cll:Landroid/app/AlertDialog;

.field private clm:Landroid/view/View$OnClickListener;

.field private cln:Landroid/view/View$OnClickListener;

.field private clo:Landroid/view/View$OnClickListener;

.field private clp:I

.field private clq:Landroid/content/DialogInterface$OnShowListener;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/app/FragmentManager;I)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 28
    new-instance v0, Lexm;

    invoke-direct {v0, p0}, Lexm;-><init>(Lexl;)V

    iput-object v0, p0, Lexl;->cli:Landroid/content/DialogInterface$OnClickListener;

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lexl;->clp:I

    .line 49
    iput-object p2, p0, Lexl;->clj:Landroid/app/FragmentManager;

    .line 51
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iput-object v0, p0, Lexl;->clk:Landroid/app/AlertDialog$Builder;

    .line 52
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 55
    iget-boolean v0, p0, Lexl;->be:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot set button. Dialog already created."

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 56
    iget-object v0, p0, Lexl;->clk:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lexl;->cli:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, p1, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 57
    iput-object p2, p0, Lexl;->clm:Landroid/view/View$OnClickListener;

    .line 58
    return-void

    .line 55
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(ILandroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 61
    iget-boolean v0, p0, Lexl;->be:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot set button. Dialog already created."

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 62
    iget-object v0, p0, Lexl;->clk:Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lexl;->cli:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, p1, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 63
    iput-object p2, p0, Lexl;->cln:Landroid/view/View$OnClickListener;

    .line 64
    return-void

    .line 61
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(ILandroid/view/View$OnClickListener;)V
    .locals 3

    .prologue
    .line 67
    iget-boolean v0, p0, Lexl;->be:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot set button. Dialog already created."

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 68
    iget-object v0, p0, Lexl;->clk:Landroid/app/AlertDialog$Builder;

    const/high16 v1, 0x1040000

    iget-object v2, p0, Lexl;->cli:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 69
    iput-object p2, p0, Lexl;->clo:Landroid/view/View$OnClickListener;

    .line 70
    return-void

    .line 67
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dismiss()V
    .locals 1

    .prologue
    .line 190
    iget-boolean v0, p0, Lexl;->be:Z

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lexl;->cll:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 193
    :cond_0
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 194
    return-void
.end method

.method public final findViewById(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 150
    invoke-super {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 151
    if-nez v0, :cond_0

    iget-boolean v1, p0, Lexl;->be:Z

    if-eqz v1, :cond_0

    .line 152
    iget-object v0, p0, Lexl;->cll:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 154
    :cond_0
    return-object v0
.end method

.method public final getButton(I)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lexl;->be:Z

    if-nez v0, :cond_0

    .line 87
    const/4 v0, 0x0

    .line 89
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lexl;->cll:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    goto :goto_0
.end method

.method public final hide()V
    .locals 1

    .prologue
    .line 182
    iget-boolean v0, p0, Lexl;->be:Z

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lexl;->cll:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->hide()V

    .line 185
    :cond_0
    invoke-super {p0}, Landroid/app/Dialog;->hide()V

    .line 186
    return-void
.end method

.method public final ir(I)V
    .locals 2

    .prologue
    .line 78
    iget-boolean v0, p0, Lexl;->be:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot set softInputMode. Dialog already created."

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 79
    iput p1, p0, Lexl;->clp:I

    .line 80
    return-void

    .line 78
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onBackStackChanged()V
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lexl;->clj:Landroid/app/FragmentManager;

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    .line 199
    iget v1, p0, Lexl;->boe:I

    if-ne v0, v1, :cond_0

    .line 200
    invoke-virtual {p0}, Lexl;->dismiss()V

    .line 202
    :cond_0
    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 220
    invoke-virtual {p0}, Lexl;->cancel()V

    .line 221
    return-void
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 94
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 95
    iget-boolean v0, p0, Lexl;->be:Z

    if-nez v0, :cond_1

    .line 98
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lexl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 99
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 100
    invoke-virtual {p0, v0}, Lexl;->setContentView(Landroid/view/View;)V

    .line 102
    invoke-virtual {p0}, Lexl;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setLayout(II)V

    .line 104
    iget-object v0, p0, Lexl;->clk:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lexl;->cll:Landroid/app/AlertDialog;

    .line 105
    iget v0, p0, Lexl;->clp:I

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lexl;->cll:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget v1, p0, Lexl;->clp:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 108
    :cond_0
    iget-object v0, p0, Lexl;->cll:Landroid/app/AlertDialog;

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 109
    iput-boolean v2, p0, Lexl;->be:Z

    .line 111
    :cond_1
    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 132
    iget-boolean v0, p0, Lexl;->be:Z

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lexl;->cll:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1, p2}, Landroid/app/AlertDialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 135
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lexl;->be:Z

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lexl;->cll:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1, p2}, Landroid/app/AlertDialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 144
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onShow(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lexl;->clj:Landroid/app/FragmentManager;

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    iput v0, p0, Lexl;->boe:I

    .line 210
    iget-object v0, p0, Lexl;->clj:Landroid/app/FragmentManager;

    invoke-virtual {v0, p0}, Landroid/app/FragmentManager;->addOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    .line 212
    iget-object v0, p0, Lexl;->clq:Landroid/content/DialogInterface$OnShowListener;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lexl;->clq:Landroid/content/DialogInterface$OnShowListener;

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnShowListener;->onShow(Landroid/content/DialogInterface;)V

    .line 215
    :cond_0
    return-void
.end method

.method protected final onStart()V
    .locals 0

    .prologue
    .line 115
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 116
    invoke-super {p0, p0}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 117
    return-void
.end method

.method protected final onStop()V
    .locals 1

    .prologue
    .line 126
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 127
    iget-object v0, p0, Lexl;->clj:Landroid/app/FragmentManager;

    invoke-virtual {v0, p0}, Landroid/app/FragmentManager;->removeOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    .line 128
    return-void
.end method

.method public final setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lexl;->clq:Landroid/content/DialogInterface$OnShowListener;

    .line 122
    return-void
.end method

.method public final setView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 73
    iget-boolean v0, p0, Lexl;->be:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot set view. Dialog already created."

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 74
    iget-object v0, p0, Lexl;->clk:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 75
    return-void

    .line 73
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final show()V
    .locals 2

    .prologue
    .line 159
    invoke-super {p0}, Landroid/app/Dialog;->show()V

    .line 160
    iget-object v0, p0, Lexl;->cll:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 166
    iget-object v0, p0, Lexl;->clm:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lexl;->cll:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 168
    iget-object v1, p0, Lexl;->clm:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    :cond_0
    iget-object v0, p0, Lexl;->cln:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_1

    .line 171
    iget-object v0, p0, Lexl;->cll:Landroid/app/AlertDialog;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 172
    iget-object v1, p0, Lexl;->cln:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    :cond_1
    iget-object v0, p0, Lexl;->clo:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_2

    .line 175
    iget-object v0, p0, Lexl;->cll:Landroid/app/AlertDialog;

    const/4 v1, -0x3

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 176
    iget-object v1, p0, Lexl;->clo:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    :cond_2
    return-void
.end method
