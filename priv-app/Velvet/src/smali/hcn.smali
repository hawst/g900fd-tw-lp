.class public final Lhcn;
.super Landroid/app/DialogFragment;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 559
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static c([Landroid/accounts/Account;)Lhcn;
    .locals 3

    .prologue
    .line 599
    new-instance v0, Lhcn;

    invoke-direct {v0}, Lhcn;-><init>()V

    .line 600
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 601
    const-string v2, "accounts"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 602
    invoke-virtual {v0, v1}, Lhcn;->setArguments(Landroid/os/Bundle;)V

    .line 603
    return-object v0
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 594
    invoke-virtual {p0}, Lhcn;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/tg/FirstRunActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->gi(Z)V

    .line 595
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 596
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 564
    invoke-virtual {p0}, Lhcn;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "accounts"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    .line 566
    array-length v0, v2

    new-array v3, v0, [Ljava/lang/String;

    .line 567
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_0

    .line 568
    aget-object v0, v2, v1

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v0, v3, v1

    .line 567
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 571
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lhcn;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 572
    const v1, 0x7f0a0241

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 574
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lhcn;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x1090003

    invoke-direct {v1, v4, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 577
    new-instance v3, Lhco;

    invoke-direct {v3, p0, v2}, Lhco;-><init>(Lhcn;[Landroid/os/Parcelable;)V

    .line 587
    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 589
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
