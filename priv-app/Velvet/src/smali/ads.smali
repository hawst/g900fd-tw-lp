.class public final Lads;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic Np:Landroid/content/Context;

.field private synthetic TB:Lcom/android/launcher3/WallpaperCropActivity;

.field private synthetic TD:Laif;

.field private synthetic TE:Landroid/view/View;

.field private synthetic TF:Z

.field private synthetic TG:Z

.field private synthetic TH:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/WallpaperCropActivity;Laif;Landroid/view/View;Landroid/content/Context;ZZLjava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lads;->TB:Lcom/android/launcher3/WallpaperCropActivity;

    iput-object p2, p0, Lads;->TD:Laif;

    iput-object p3, p0, Lads;->TE:Landroid/view/View;

    iput-object p4, p0, Lads;->Np:Landroid/content/Context;

    iput-boolean p5, p0, Lads;->TF:Z

    iput-boolean p6, p0, Lads;->TG:Z

    iput-object p7, p0, Lads;->TH:Ljava/lang/Runnable;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs ko()Ljava/lang/Void;
    .locals 2

    .prologue
    .line 151
    invoke-virtual {p0}, Lads;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    :try_start_0
    iget-object v0, p0, Lads;->TD:Laif;

    invoke-virtual {v0}, Laif;->lO()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 154
    :catch_0
    move-exception v0

    .line 155
    iget-object v1, p0, Lads;->TB:Lcom/android/launcher3/WallpaperCropActivity;

    invoke-virtual {v1}, Lcom/android/launcher3/WallpaperCropActivity;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 161
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lads;->cancel(Z)Z

    goto :goto_0

    .line 164
    :cond_1
    throw v0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0}, Lads;->ko()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 149
    invoke-virtual {p0}, Lads;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lads;->TE:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lads;->TD:Laif;

    iget-object v0, v0, Laif;->YQ:Laig;

    sget-object v1, Laig;->YS:Laig;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lads;->TB:Lcom/android/launcher3/WallpaperCropActivity;

    iget-object v0, v0, Lcom/android/launcher3/WallpaperCropActivity;->Ty:Lcom/android/launcher3/CropView;

    new-instance v1, Laie;

    iget-object v2, p0, Lads;->Np:Landroid/content/Context;

    iget-object v3, p0, Lads;->TD:Laif;

    invoke-direct {v1, v2, v3}, Laie;-><init>(Landroid/content/Context;Laif;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher3/CropView;->a(Laiq;Ljava/lang/Runnable;)V

    iget-object v0, p0, Lads;->TB:Lcom/android/launcher3/WallpaperCropActivity;

    iget-object v0, v0, Lcom/android/launcher3/WallpaperCropActivity;->Ty:Lcom/android/launcher3/CropView;

    iget-boolean v1, p0, Lads;->TF:Z

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CropView;->K(Z)V

    iget-boolean v0, p0, Lads;->TG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lads;->TB:Lcom/android/launcher3/WallpaperCropActivity;

    iget-object v0, v0, Lcom/android/launcher3/WallpaperCropActivity;->Ty:Lcom/android/launcher3/CropView;

    invoke-virtual {v0}, Lcom/android/launcher3/CropView;->ft()V

    :cond_0
    iget-object v0, p0, Lads;->TH:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lads;->TH:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_1
    return-void
.end method
