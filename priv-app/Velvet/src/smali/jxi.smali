.class public final Ljxi;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eKL:Ljava/lang/String;

.field private eKM:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 90
    const/4 v0, 0x0

    iput v0, p0, Ljxi;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljxi;->eKL:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljxi;->eKM:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljxi;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljxi;->eCz:I

    .line 91
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 26
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljxi;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljxi;->eKL:Ljava/lang/String;

    iget v0, p0, Ljxi;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxi;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljxi;->eKM:Ljava/lang/String;

    iget v0, p0, Ljxi;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljxi;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 105
    iget v0, p0, Ljxi;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 106
    const/4 v0, 0x1

    iget-object v1, p0, Ljxi;->eKL:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 108
    :cond_0
    iget v0, p0, Ljxi;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 109
    const/4 v0, 0x2

    iget-object v1, p0, Ljxi;->eKM:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 111
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 112
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 116
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 117
    iget v1, p0, Ljxi;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 118
    const/4 v1, 0x1

    iget-object v2, p0, Ljxi;->eKL:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 121
    :cond_0
    iget v1, p0, Ljxi;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 122
    const/4 v1, 0x2

    iget-object v2, p0, Ljxi;->eKM:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 125
    :cond_1
    return v0
.end method
