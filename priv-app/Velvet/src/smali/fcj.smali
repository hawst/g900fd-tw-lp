.class public Lfcj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private final mSignedCipherHelper:Lfdh;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfdh;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lfcj;->mAppContext:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lfcj;->mSignedCipherHelper:Lfdh;

    .line 29
    return-void
.end method


# virtual methods
.method public A(Ljava/lang/String;I)[B
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 42
    .line 45
    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lfcj;->mAppContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-direct {v0, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 46
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 47
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    .line 80
    :goto_0
    return-object v1

    .line 50
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 51
    int-to-long v2, p2

    cmp-long v0, v6, v2

    if-ltz v0, :cond_1

    .line 52
    const-string v0, "FileBytesReader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Data is too large ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bytes) to read to disk: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 55
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    .line 58
    :cond_1
    :try_start_2
    iget-object v0, p0, Lfcj;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 59
    long-to-int v0, v6

    :try_start_3
    new-array v0, v0, [B

    .line 60
    const/4 v4, 0x0

    .line 61
    long-to-int v3, v6

    .line 66
    :cond_2
    invoke-virtual {v2, v0, v4, v3}, Ljava/io/FileInputStream;->read([BII)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v5

    .line 67
    if-gtz v5, :cond_3

    .line 77
    :goto_1
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    :goto_2
    move-object v1, v0

    .line 80
    goto :goto_0

    .line 68
    :cond_3
    sub-int/2addr v3, v5

    .line 69
    add-int/2addr v4, v5

    .line 70
    if-gtz v3, :cond_2

    goto :goto_1

    .line 75
    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_3
    :try_start_4
    const-string v2, "FileBytesReader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to read file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 77
    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    move-object v0, v1

    .line 78
    goto :goto_2

    .line 77
    :catchall_0
    move-exception v0

    :goto_4
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_4

    :catchall_2
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_4

    .line 75
    :catch_1
    move-exception v0

    move-object v0, v2

    goto :goto_3
.end method

.method public final B(Ljava/lang/String;I)[B
    .locals 4

    .prologue
    .line 92
    invoke-virtual {p0, p1, p2}, Lfcj;->A(Ljava/lang/String;I)[B

    move-result-object v0

    .line 93
    if-eqz v0, :cond_0

    .line 94
    iget-object v1, p0, Lfcj;->mSignedCipherHelper:Lfdh;

    invoke-interface {v1, v0}, Lfdh;->J([B)[B

    move-result-object v0

    .line 95
    if-nez v0, :cond_0

    .line 96
    const-string v1, "FileBytesReader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to read file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " crypto failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    :cond_0
    return-object v0
.end method
