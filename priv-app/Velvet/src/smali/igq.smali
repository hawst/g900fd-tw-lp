.class public abstract Ligq;
.super Lirv;
.source "PG"


# instance fields
.field dBF:Ligs;

.field private dBG:Ljava/lang/Object;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lirv;-><init>()V

    .line 65
    sget-object v0, Ligs;->dBJ:Ligs;

    iput-object v0, p0, Ligq;->dBF:Ligs;

    .line 68
    return-void
.end method


# virtual methods
.method protected abstract aWe()Ljava/lang/Object;
.end method

.method public final hasNext()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 130
    iget-object v0, p0, Ligq;->dBF:Ligs;

    sget-object v3, Ligs;->dBL:Ligs;

    if-eq v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 131
    sget-object v0, Ligr;->dBH:[I

    iget-object v3, p0, Ligq;->dBF:Ligs;

    invoke-virtual {v3}, Ligs;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 138
    sget-object v0, Ligs;->dBL:Ligs;

    iput-object v0, p0, Ligq;->dBF:Ligs;

    invoke-virtual {p0}, Ligq;->aWe()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Ligq;->dBG:Ljava/lang/Object;

    iget-object v0, p0, Ligq;->dBF:Ligs;

    sget-object v3, Ligs;->dBK:Ligs;

    if-eq v0, v3, :cond_0

    sget-object v0, Ligs;->dBI:Ligs;

    iput-object v0, p0, Ligq;->dBF:Ligs;

    move v2, v1

    :cond_0
    :goto_1
    :pswitch_0
    return v2

    :cond_1
    move v0, v2

    .line 130
    goto :goto_0

    :pswitch_1
    move v2, v1

    .line 135
    goto :goto_1

    .line 131
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Ligq;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 156
    :cond_0
    sget-object v0, Ligs;->dBJ:Ligs;

    iput-object v0, p0, Ligq;->dBF:Ligs;

    .line 157
    iget-object v0, p0, Ligq;->dBG:Ljava/lang/Object;

    return-object v0
.end method
