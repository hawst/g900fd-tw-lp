.class public final Ljjg;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eoK:[Ljjg;


# instance fields
.field private aez:I

.field private bmI:Ljava/lang/String;

.field private bmJ:Z

.field private bmL:I

.field private bmN:Ljava/lang/String;

.field private eoL:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 128
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 129
    iput v1, p0, Ljjg;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljjg;->bmI:Ljava/lang/String;

    iput-boolean v1, p0, Ljjg;->bmJ:Z

    const-string v0, ""

    iput-object v0, p0, Ljjg;->bmN:Ljava/lang/String;

    iput v1, p0, Ljjg;->bmL:I

    iput v1, p0, Ljjg;->eoL:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljjg;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljjg;->eCz:I

    .line 130
    return-void
.end method

.method public static bnQ()[Ljjg;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Ljjg;->eoK:[Ljjg;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Ljjg;->eoK:[Ljjg;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Ljjg;

    sput-object v0, Ljjg;->eoK:[Ljjg;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Ljjg;->eoK:[Ljjg;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final TO()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Ljjg;->bmJ:Z

    return v0
.end method

.method public final TV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Ljjg;->bmN:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljjg;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljjg;->bmI:Ljava/lang/String;

    iget v0, p0, Ljjg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjg;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljjg;->bmJ:Z

    iget v0, p0, Ljjg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljjg;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljjg;->bmN:Ljava/lang/String;

    iget v0, p0, Ljjg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljjg;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljjg;->bmL:I

    iget v0, p0, Ljjg;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljjg;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljjg;->eoL:I

    iget v0, p0, Ljjg;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljjg;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 147
    iget v0, p0, Ljjg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 148
    const/4 v0, 0x1

    iget-object v1, p0, Ljjg;->bmI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 150
    :cond_0
    iget v0, p0, Ljjg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 151
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljjg;->bmJ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 153
    :cond_1
    iget v0, p0, Ljjg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 154
    const/4 v0, 0x3

    iget-object v1, p0, Ljjg;->bmN:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 156
    :cond_2
    iget v0, p0, Ljjg;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 157
    const/4 v0, 0x4

    iget v1, p0, Ljjg;->bmL:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 159
    :cond_3
    iget v0, p0, Ljjg;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 160
    const/4 v0, 0x5

    iget v1, p0, Ljjg;->eoL:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 162
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 163
    return-void
.end method

.method public final getId()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Ljjg;->eoL:I

    return v0
.end method

.method public final getIntValue()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Ljjg;->bmL:I

    return v0
.end method

.method public final in(Z)Ljjg;
    .locals 1

    .prologue
    .line 55
    iput-boolean p1, p0, Ljjg;->bmJ:Z

    .line 56
    iget v0, p0, Ljjg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljjg;->aez:I

    .line 57
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 167
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 168
    iget v1, p0, Ljjg;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 169
    const/4 v1, 0x1

    iget-object v2, p0, Ljjg;->bmI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    :cond_0
    iget v1, p0, Ljjg;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 173
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljjg;->bmJ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 176
    :cond_1
    iget v1, p0, Ljjg;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 177
    const/4 v1, 0x3

    iget-object v2, p0, Ljjg;->bmN:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    :cond_2
    iget v1, p0, Ljjg;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 181
    const/4 v1, 0x4

    iget v2, p0, Ljjg;->bmL:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 184
    :cond_3
    iget v1, p0, Ljjg;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 185
    const/4 v1, 0x5

    iget v2, p0, Ljjg;->eoL:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 188
    :cond_4
    return v0
.end method

.method public final mY()Z
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Ljjg;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final qn(I)Ljjg;
    .locals 1

    .prologue
    .line 96
    iput p1, p0, Ljjg;->bmL:I

    .line 97
    iget v0, p0, Ljjg;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljjg;->aez:I

    .line 98
    return-object p0
.end method

.method public final qo(I)Ljjg;
    .locals 1

    .prologue
    .line 115
    iput p1, p0, Ljjg;->eoL:I

    .line 116
    iget v0, p0, Ljjg;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljjg;->aez:I

    .line 117
    return-object p0
.end method

.method public final wh(Ljava/lang/String;)Ljjg;
    .locals 1

    .prologue
    .line 74
    if-nez p1, :cond_0

    .line 75
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 77
    :cond_0
    iput-object p1, p0, Ljjg;->bmN:Ljava/lang/String;

    .line 78
    iget v0, p0, Ljjg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljjg;->aez:I

    .line 79
    return-object p0
.end method
