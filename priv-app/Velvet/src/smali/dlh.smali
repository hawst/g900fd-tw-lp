.class public Ldlh;
.super Ldjz;
.source "PG"

# interfaces
.implements Lcnf;


# instance fields
.field private final bBl:Lisu;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private bBm:Z

.field private final dK:Ljava/lang/Object;


# direct methods
.method private constructor <init>(Lisu;Ljava/util/List;Lcng;Ljava/util/concurrent/ExecutorService;Legl;)V
    .locals 1
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lcng;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # Legl;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 70
    invoke-static {p2}, Lilw;->y(Ljava/lang/Iterable;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-direct {p0, v0, p3, p4, p5}, Ldjz;-><init>(Ljava/util/Queue;Lcng;Ljava/util/concurrent/ExecutorService;Legl;)V

    .line 52
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ldlh;->dK:Ljava/lang/Object;

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldlh;->bBm:Z

    .line 71
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lisu;

    iput-object v0, p0, Ldlh;->bBl:Lisu;

    .line 72
    return-void
.end method

.method synthetic constructor <init>(Lisu;Ljava/util/List;Lcng;Ljava/util/concurrent/ExecutorService;Legl;B)V
    .locals 0

    .prologue
    .line 43
    invoke-direct/range {p0 .. p5}, Ldlh;-><init>(Lisu;Ljava/util/List;Lcng;Ljava/util/concurrent/ExecutorService;Legl;)V

    return-void
.end method

.method public static a(Lcnr;Ljava/util/List;Ljava/util/concurrent/ExecutorService;Lchk;Legl;Lcoi;Lcpt;Lcpq;)Ldlh;
    .locals 7
    .param p0    # Lcnr;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Legl;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # Lcoi;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p6    # Lcpt;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p7    # Lcpq;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 87
    new-instance v0, Lcng;

    const-string v2, ""

    move-object v1, p3

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcng;-><init>(Lchk;Ljava/lang/String;Lcoi;Lcpt;Lcpq;)V

    .line 89
    new-instance v1, Ldlh;

    move-object v2, p0

    move-object v3, p1

    move-object v4, v0

    move-object v5, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Ldlh;-><init>(Lisu;Ljava/util/List;Lcng;Ljava/util/concurrent/ExecutorService;Legl;)V

    .line 91
    invoke-virtual {v1}, Ldlh;->start()V

    .line 92
    return-object v1
.end method

.method public static a(Lcnr;Ljava/util/List;Ljava/util/concurrent/ExecutorService;Lchk;Legl;Lcoi;Lcpt;Ljava/lang/String;Lcpq;)Ldlh;
    .locals 7
    .param p0    # Lcnr;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Legl;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # Lcoi;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p6    # Lcpt;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p8    # Lcpq;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 109
    new-instance v0, Lcng;

    move-object v1, p3

    move-object v2, p7

    move-object v3, p5

    move-object v4, p6

    move-object v5, p8

    invoke-direct/range {v0 .. v5}, Lcng;-><init>(Lchk;Ljava/lang/String;Lcoi;Lcpt;Lcpq;)V

    .line 111
    new-instance v1, Ldlh;

    move-object v2, p0

    move-object v3, p1

    move-object v4, v0

    move-object v5, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Ldlh;-><init>(Lisu;Ljava/util/List;Lcng;Ljava/util/concurrent/ExecutorService;Legl;)V

    .line 113
    invoke-virtual {v1}, Ldlh;->start()V

    .line 114
    return-object v1
.end method

.method private acT()Z
    .locals 2

    .prologue
    .line 173
    iget-object v1, p0, Ldlh;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 174
    :try_start_0
    iget-boolean v0, p0, Ldlh;->bBm:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 175
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private d(Lefq;)V
    .locals 4

    .prologue
    .line 234
    :try_start_0
    iget-object v0, p0, Ldlh;->bbk:Legl;

    iget-wide v0, v0, Legl;->amT:J

    iget-object v2, p0, Ldlh;->bbk:Legl;

    iget-wide v2, v2, Legl;->bUI:J

    invoke-static {p1, v0, v1, v2, v3}, Lege;->a(Lefr;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    invoke-virtual {p0, p1}, Ldlh;->c(Ljava/lang/Exception;)Z

    .line 237
    return-void

    .line 236
    :catchall_0
    move-exception v0

    invoke-virtual {p0, p1}, Ldlh;->c(Ljava/lang/Exception;)Z

    throw v0
.end method

.method private e(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 290
    invoke-direct {p0}, Ldlh;->acT()Z

    move-result v0

    if-nez v0, :cond_0

    .line 291
    const-string v0, "Search.InputStreamChunkProducer"

    const-string v1, "Exception while buffering stream"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 293
    :cond_0
    return-void
.end method


# virtual methods
.method protected final PX()V
    .locals 14

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v9, 0x1

    const/4 v3, 0x0

    const v13, 0x30008

    .line 213
    const/4 v12, 0x0

    :try_start_0
    const-string v1, "ChunkProducer stopped by another thread, before stream"

    invoke-virtual {p0, v1}, Ldlh;->ht(Ljava/lang/String;)V

    iget-object v1, p0, Ldlh;->bBl:Lisu;

    invoke-interface {v1}, Lisu;->PN()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcnr;

    move-object v12, v0

    if-nez v12, :cond_0

    new-instance v1, Lefs;

    const v2, 0x3000a

    invoke-direct {v1, v2}, Lefs;-><init>(I)V

    throw v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lefq; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catch_0
    move-exception v1

    :try_start_1
    new-instance v1, Lefs;

    const v2, 0x30001

    invoke-direct {v1, v2}, Lefs;-><init>(I)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v1

    :try_start_2
    invoke-virtual {p0, v12}, Ldlh;->a(Lcnr;)V

    throw v1
    :try_end_2
    .catch Lefq; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_7

    .line 215
    :catch_1
    move-exception v1

    .line 219
    const-string v2, "Search.InputStreamChunkProducer"

    const-string v3, "Checked exception in runReadTask()"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    invoke-direct {p0, v1}, Ldlh;->d(Lefq;)V

    .line 229
    :goto_0
    return-void

    .line 213
    :cond_0
    :try_start_3
    iget-object v11, p0, Ldjz;->bAB:Lcng;

    iget-object v2, v12, Lcnr;->mInputStream:Ljava/io/InputStream;

    if-nez v2, :cond_1

    new-instance v1, Lefs;

    const v2, 0x3000a

    invoke-direct {v1, v2}, Lefs;-><init>(I)V

    throw v1
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lefq; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_2
    move-exception v1

    :try_start_4
    invoke-direct {p0, v1}, Ldlh;->e(Ljava/lang/Exception;)V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_1
    :try_start_5
    iget v1, v12, Lcnr;->bdd:I

    const/4 v4, 0x2

    if-eq v1, v4, :cond_2

    if-eq v1, v6, :cond_2

    if-ne v1, v5, :cond_5

    :cond_2
    if-ne v1, v6, :cond_3

    move v8, v9

    :goto_1
    if-ne v1, v5, :cond_4

    :goto_2
    new-instance v1, Lcof;

    iget-object v3, v11, Lcng;->mGsaConfig:Lchk;

    invoke-virtual {v3}, Lchk;->JD()I

    move-result v3

    iget-object v4, v11, Lcng;->mExtrasConsumer:Lcoi;

    iget-object v5, v11, Lcng;->bcN:Ljava/lang/String;

    iget-object v6, v11, Lcng;->mGsaConfig:Lchk;

    invoke-virtual {v6}, Lchk;->IL()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v11, Lcng;->mGsaConfig:Lchk;

    invoke-virtual {v7}, Lchk;->JP()Ljava/lang/String;

    move-result-object v7

    iget-object v10, v11, Lcng;->mPreloadTaskHandler:Lcpt;

    iget-object v11, v11, Lcng;->mStaticContentCache:Lcpq;

    invoke-direct/range {v1 .. v11}, Lcof;-><init>(Ljava/io/InputStream;ILcoi;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcpt;Lcpq;)V

    :goto_3
    const-string v2, "ChunkProducer stopped by another thread, after stream"

    invoke-virtual {p0, v2}, Ldlh;->ht(Ljava/lang/String;)V

    invoke-interface {v1, p0}, Lcne;->a(Lcnf;)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lefq; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/net/UnknownHostException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {p0, v12}, Ldlh;->a(Lcnr;)V

    .line 214
    invoke-virtual {p0}, Ldlh;->Qd()Z
    :try_end_6
    .catch Lefq; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/Error; {:try_start_6 .. :try_end_6} :catch_7

    goto :goto_0

    .line 221
    :catch_3
    move-exception v1

    .line 222
    const-string v2, "Search.InputStreamChunkProducer"

    const-string v3, "RuntimeException in runReadTask()"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 223
    new-instance v2, Lefs;

    invoke-direct {v2, v1, v13}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    invoke-direct {p0, v2}, Ldlh;->d(Lefq;)V

    .line 224
    throw v1

    :cond_3
    move v8, v3

    .line 213
    goto :goto_1

    :cond_4
    move v9, v3

    goto :goto_2

    :cond_5
    :try_start_7
    new-instance v1, Lcoq;

    iget-object v3, v11, Lcng;->mGsaConfig:Lchk;

    invoke-virtual {v3}, Lchk;->JD()I

    move-result v3

    iget-object v4, v11, Lcng;->mExtrasConsumer:Lcoi;

    iget-object v5, v11, Lcng;->mGsaConfig:Lchk;

    invoke-virtual {v5}, Lchk;->IF()I

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcoq;-><init>(Ljava/io/InputStream;ILcoi;I)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Lefq; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/net/UnknownHostException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    :catch_4
    move-exception v1

    :try_start_8
    invoke-direct {p0, v1}, Ldlh;->e(Ljava/lang/Exception;)V

    new-instance v2, Lefs;

    const v3, 0x30005

    invoke-direct {v2, v1, v3}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v2

    :catch_5
    move-exception v1

    invoke-direct {p0, v1}, Ldlh;->e(Ljava/lang/Exception;)V

    new-instance v2, Lefs;

    const v3, 0x30007

    invoke-direct {v2, v1, v3}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v2

    :catch_6
    move-exception v1

    const-string v2, "Search.InputStreamChunkProducer"

    const-string v3, "Exception without error code for logging"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v2, Lefs;

    const v3, 0x30008

    invoke-direct {v2, v1, v3}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 225
    :catch_7
    move-exception v1

    .line 226
    const-string v2, "Search.InputStreamChunkProducer"

    const-string v3, "Error in runReadTask()"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 227
    new-instance v2, Lefs;

    invoke-direct {v2, v1, v13}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    invoke-direct {p0, v2}, Ldlh;->d(Lefq;)V

    .line 228
    throw v1
.end method

.method public final QF()Z
    .locals 1

    .prologue
    .line 306
    invoke-virtual {p0}, Ldlh;->acy()Z

    move-result v0

    return v0
.end method

.method protected a(Lcnr;)V
    .locals 1
    .param p1    # Lcnr;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 199
    if-eqz p1, :cond_0

    .line 200
    iget-object v0, p1, Lcnr;->mInputStream:Ljava/io/InputStream;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 202
    :cond_0
    return-void
.end method

.method public final a(Ldkd;)Z
    .locals 1

    .prologue
    .line 301
    invoke-virtual {p0, p1}, Ldlh;->b(Ldkd;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 326
    invoke-direct {p0}, Ldlh;->acT()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    new-instance v0, Lefs;

    const-string v1, "ChunkProducer was stopped"

    const v2, 0x3000b

    invoke-direct {v0, v1, p1, v2}, Lefs;-><init>(Ljava/lang/String;Ljava/lang/Throwable;I)V

    throw v0

    .line 330
    :cond_0
    return-void
.end method

.method public final ht(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 315
    invoke-direct {p0}, Ldlh;->acT()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 316
    new-instance v0, Lefs;

    const v1, 0x3000b

    invoke-direct {v0, p1, v1}, Lefs;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 318
    :cond_0
    return-void
.end method

.method public final start()V
    .locals 1

    .prologue
    .line 163
    invoke-direct {p0}, Ldlh;->acT()Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    invoke-super {p0}, Ldjz;->start()V

    .line 166
    :cond_0
    return-void
.end method

.method public final stop()V
    .locals 2

    .prologue
    .line 184
    iget-object v1, p0, Ldlh;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 185
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Ldlh;->bBm:Z

    .line 186
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    invoke-virtual {p0}, Ldlh;->acw()V

    .line 188
    return-void

    .line 186
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
