.class public final Ljap;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ajB:Ljava/lang/String;

.field private dJq:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20043
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 20044
    const/4 v0, 0x0

    iput v0, p0, Ljap;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljap;->ajB:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Ljap;->dJq:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljap;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljap;->eCz:I

    .line 20045
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 19980
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljap;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljap;->ajB:Ljava/lang/String;

    iget v0, p0, Ljap;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljap;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljap;->dJq:I

    iget v0, p0, Ljap;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljap;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 20059
    iget v0, p0, Ljap;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 20060
    const/4 v0, 0x1

    iget-object v1, p0, Ljap;->ajB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 20062
    :cond_0
    iget v0, p0, Ljap;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 20063
    const/4 v0, 0x2

    iget v1, p0, Ljap;->dJq:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 20065
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 20066
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 20070
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 20071
    iget v1, p0, Ljap;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 20072
    const/4 v1, 0x1

    iget-object v2, p0, Ljap;->ajB:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20075
    :cond_0
    iget v1, p0, Ljap;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 20076
    const/4 v1, 0x2

    iget v2, p0, Ljap;->dJq:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 20079
    :cond_1
    return v0
.end method

.method public final ok(I)Ljap;
    .locals 1

    .prologue
    .line 20030
    const/4 v0, 0x1

    iput v0, p0, Ljap;->dJq:I

    .line 20031
    iget v0, p0, Ljap;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljap;->aez:I

    .line 20032
    return-object p0
.end method

.method public final sg(Ljava/lang/String;)Ljap;
    .locals 1

    .prologue
    .line 20008
    if-nez p1, :cond_0

    .line 20009
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 20011
    :cond_0
    iput-object p1, p0, Ljap;->ajB:Ljava/lang/String;

    .line 20012
    iget v0, p0, Ljap;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljap;->aez:I

    .line 20013
    return-object p0
.end method
