.class public final Lfyg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field cDn:Ljava/util/List;

.field final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfyg;->cDn:Ljava/util/List;

    .line 24
    iput-object p1, p0, Lfyg;->mContext:Landroid/content/Context;

    .line 25
    return-void
.end method


# virtual methods
.method public final aDR()Laow;
    .locals 3

    .prologue
    .line 88
    new-instance v1, Laow;

    invoke-direct {v1}, Laow;-><init>()V

    .line 89
    iget-object v0, p0, Lfyg;->cDn:Ljava/util/List;

    iget-object v2, p0, Lfyg;->cDn:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Laov;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laov;

    iput-object v0, v1, Laow;->aks:[Laov;

    .line 91
    return-object v1
.end method

.method public final b(ILjava/lang/String;I)Lfyg;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lfyg;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lfyg;->e(Ljava/lang/String;Ljava/lang/String;I)Lfyg;

    move-result-object v0

    return-object v0
.end method

.method e(Ljava/lang/String;Ljava/lang/String;I)Lfyg;
    .locals 3

    .prologue
    .line 44
    new-instance v1, Laov;

    invoke-direct {v1}, Laov;-><init>()V

    .line 45
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    invoke-virtual {v1, p1}, Laov;->cv(Ljava/lang/String;)Laov;

    .line 47
    iget-object v0, v1, Laov;->akp:[Ljava/lang/String;

    invoke-static {v0, p2}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Laov;->akp:[Ljava/lang/String;

    .line 52
    :goto_0
    invoke-virtual {v1, p3}, Laov;->cL(I)Laov;

    .line 53
    iget-object v0, p0, Lfyg;->cDn:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    return-object p0

    .line 49
    :cond_0
    const-string v0, ""

    invoke-virtual {v1, v0}, Laov;->cv(Ljava/lang/String;)Laov;

    .line 50
    iget-object v0, v1, Laov;->akp:[Ljava/lang/String;

    const-string v2, ""

    invoke-static {v0, v2}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Laov;->akp:[Ljava/lang/String;

    goto :goto_0
.end method

.method g(Ljava/lang/String;Ljava/util/List;I)Lfyg;
    .locals 3

    .prologue
    .line 74
    new-instance v1, Laov;

    invoke-direct {v1}, Laov;-><init>()V

    .line 75
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    invoke-virtual {v1, p1}, Laov;->cv(Ljava/lang/String;)Laov;

    .line 77
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Laov;->akp:[Ljava/lang/String;

    .line 82
    :goto_0
    invoke-virtual {v1, p3}, Laov;->cL(I)Laov;

    .line 83
    iget-object v0, p0, Lfyg;->cDn:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    return-object p0

    .line 79
    :cond_0
    const-string v0, ""

    invoke-virtual {v1, v0}, Laov;->cv(Ljava/lang/String;)Laov;

    .line 80
    iget-object v0, v1, Laov;->akp:[Ljava/lang/String;

    const-string v2, ""

    invoke-static {v0, v2}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Laov;->akp:[Ljava/lang/String;

    goto :goto_0
.end method

.method public final m(ILjava/lang/String;)Lfyg;
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lfyg;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p2, v1}, Lfyg;->e(Ljava/lang/String;Ljava/lang/String;I)Lfyg;

    move-result-object v0

    return-object v0
.end method
