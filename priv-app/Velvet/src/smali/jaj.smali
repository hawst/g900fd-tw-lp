.class public final Ljaj;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dWC:J

.field private dWD:J

.field private dWE:I

.field private dWF:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 43213
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 43214
    iput v0, p0, Ljaj;->aez:I

    iput-wide v2, p0, Ljaj;->dWC:J

    iput-wide v2, p0, Ljaj;->dWD:J

    iput v0, p0, Ljaj;->dWE:I

    const-string v0, ""

    iput-object v0, p0, Ljaj;->dWF:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljaj;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljaj;->eCz:I

    .line 43215
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 43115
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljaj;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljaj;->dWC:J

    iget v0, p0, Ljaj;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljaj;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljaj;->dWD:J

    iget v0, p0, Ljaj;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljaj;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljaj;->dWE:I

    iget v0, p0, Ljaj;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljaj;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljaj;->dWF:Ljava/lang/String;

    iget v0, p0, Ljaj;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljaj;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 43231
    iget v0, p0, Ljaj;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 43232
    const/4 v0, 0x1

    iget-wide v2, p0, Ljaj;->dWC:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 43234
    :cond_0
    iget v0, p0, Ljaj;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 43235
    const/4 v0, 0x2

    iget-wide v2, p0, Ljaj;->dWD:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 43237
    :cond_1
    iget v0, p0, Ljaj;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 43238
    const/4 v0, 0x3

    iget v1, p0, Ljaj;->dWE:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 43240
    :cond_2
    iget v0, p0, Ljaj;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 43241
    const/4 v0, 0x4

    iget-object v1, p0, Ljaj;->dWF:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 43243
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 43244
    return-void
.end method

.method public final bdX()J
    .locals 2

    .prologue
    .line 43137
    iget-wide v0, p0, Ljaj;->dWC:J

    return-wide v0
.end method

.method public final bdY()Z
    .locals 1

    .prologue
    .line 43145
    iget v0, p0, Ljaj;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bdZ()J
    .locals 2

    .prologue
    .line 43156
    iget-wide v0, p0, Ljaj;->dWD:J

    return-wide v0
.end method

.method public final bea()Z
    .locals 1

    .prologue
    .line 43164
    iget v0, p0, Ljaj;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final beb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43194
    iget-object v0, p0, Ljaj;->dWF:Ljava/lang/String;

    return-object v0
.end method

.method public final bec()Z
    .locals 1

    .prologue
    .line 43205
    iget v0, p0, Ljaj;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final cA(J)Ljaj;
    .locals 1

    .prologue
    .line 43140
    iput-wide p1, p0, Ljaj;->dWC:J

    .line 43141
    iget v0, p0, Ljaj;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljaj;->aez:I

    .line 43142
    return-object p0
.end method

.method public final cB(J)Ljaj;
    .locals 1

    .prologue
    .line 43159
    iput-wide p1, p0, Ljaj;->dWD:J

    .line 43160
    iget v0, p0, Ljaj;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljaj;->aez:I

    .line 43161
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 43248
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 43249
    iget v1, p0, Ljaj;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 43250
    const/4 v1, 0x1

    iget-wide v2, p0, Ljaj;->dWC:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 43253
    :cond_0
    iget v1, p0, Ljaj;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 43254
    const/4 v1, 0x2

    iget-wide v2, p0, Ljaj;->dWD:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 43257
    :cond_1
    iget v1, p0, Ljaj;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 43258
    const/4 v1, 0x3

    iget v2, p0, Ljaj;->dWE:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 43261
    :cond_2
    iget v1, p0, Ljaj;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 43262
    const/4 v1, 0x4

    iget-object v2, p0, Ljaj;->dWF:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43265
    :cond_3
    return v0
.end method

.method public final og(I)Ljaj;
    .locals 1

    .prologue
    .line 43178
    iput p1, p0, Ljaj;->dWE:I

    .line 43179
    iget v0, p0, Ljaj;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljaj;->aez:I

    .line 43180
    return-object p0
.end method

.method public final sc(Ljava/lang/String;)Ljaj;
    .locals 1

    .prologue
    .line 43197
    if-nez p1, :cond_0

    .line 43198
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 43200
    :cond_0
    iput-object p1, p0, Ljaj;->dWF:Ljava/lang/String;

    .line 43201
    iget v0, p0, Ljaj;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljaj;->aez:I

    .line 43202
    return-object p0
.end method
