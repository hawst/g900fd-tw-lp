.class final Lael;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# instance fields
.field private synthetic Un:Ladz;


# direct methods
.method constructor <init>(Ladz;)V
    .locals 0

    .prologue
    .line 566
    iput-object p1, p0, Lael;->Un:Ladz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 606
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 607
    const v3, 0x7f1104d1

    if-ne v1, v3, :cond_3

    .line 608
    iget-object v1, p0, Lael;->Un:Ladz;

    invoke-static {v1}, Ladz;->h(Ladz;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    .line 609
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v4, v0

    move v3, v0

    .line 611
    :goto_0
    if-ge v4, v5, :cond_0

    .line 612
    iget-object v0, p0, Lael;->Un:Ladz;

    invoke-static {v0}, Ladz;->h(Ladz;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CheckableFrameLayout;

    .line 614
    invoke-virtual {v0}, Lcom/android/launcher3/CheckableFrameLayout;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 615
    invoke-virtual {v0}, Lcom/android/launcher3/CheckableFrameLayout;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laeu;

    .line 616
    iget-object v7, p0, Lael;->Un:Ladz;

    invoke-virtual {v1, v7}, Laeu;->b(Ladz;)V

    .line 617
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 618
    iget-object v0, p0, Lael;->Un:Ladz;

    invoke-static {v0}, Ladz;->k(Ladz;)I

    move-result v0

    if-ne v4, v0, :cond_4

    move v0, v2

    .line 611
    :goto_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v3, v0

    goto :goto_0

    .line 623
    :cond_0
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 624
    iget-object v4, p0, Lael;->Un:Ladz;

    invoke-static {v4}, Ladz;->h(Ladz;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    goto :goto_2

    .line 626
    :cond_1
    if-eqz v3, :cond_2

    .line 627
    iget-object v0, p0, Lael;->Un:Ladz;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Ladz;->a(Ladz;I)I

    .line 628
    iget-object v0, p0, Lael;->Un:Ladz;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ladz;->b(Ladz;Landroid/view/View;)Landroid/view/View;

    .line 629
    iget-object v0, p0, Lael;->Un:Ladz;

    invoke-virtual {v0, v2}, Ladz;->aq(Z)V

    .line 631
    :cond_2
    iget-object v0, p0, Lael;->Un:Ladz;

    invoke-static {v0}, Ladz;->j(Ladz;)V

    .line 632
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    .line 635
    :goto_3
    return v2

    :cond_3
    move v2, v0

    goto :goto_3

    :cond_4
    move v0, v3

    goto :goto_1
.end method

.method public final onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 571
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 572
    const v1, 0x7f130001

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 573
    const/4 v0, 0x1

    return v0
.end method

.method public final onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 642
    iget-object v0, p0, Lael;->Un:Ladz;

    invoke-static {v0}, Ladz;->h(Ladz;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    move v1, v2

    .line 643
    :goto_0
    if-ge v1, v3, :cond_0

    .line 644
    iget-object v0, p0, Lael;->Un:Ladz;

    invoke-static {v0}, Ladz;->h(Ladz;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CheckableFrameLayout;

    .line 645
    invoke-virtual {v0, v2}, Lcom/android/launcher3/CheckableFrameLayout;->setChecked(Z)V

    .line 643
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 647
    :cond_0
    iget-object v0, p0, Lael;->Un:Ladz;

    invoke-static {v0}, Ladz;->m(Ladz;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 648
    iget-object v0, p0, Lael;->Un:Ladz;

    invoke-static {v0}, Ladz;->m(Ladz;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 650
    :cond_1
    iget-object v0, p0, Lael;->Un:Ladz;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ladz;->a(Ladz;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 651
    return-void
.end method

.method public final onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 592
    iget-object v0, p0, Lael;->Un:Ladz;

    invoke-static {v0}, Ladz;->h(Ladz;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    move v2, v3

    move v1, v3

    :goto_0
    if-ge v2, v4, :cond_0

    iget-object v0, p0, Lael;->Un:Ladz;

    invoke-static {v0}, Ladz;->h(Ladz;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CheckableFrameLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CheckableFrameLayout;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    add-int/lit8 v0, v1, 0x1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 593
    :cond_0
    if-nez v1, :cond_1

    .line 594
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    .line 599
    :goto_2
    return v6

    .line 597
    :cond_1
    iget-object v0, p0, Lael;->Un:Ladz;

    invoke-virtual {v0}, Ladz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f10000e

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-virtual {v0, v2, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method
