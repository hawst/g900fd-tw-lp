.class public final Lgct;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcdz;


# instance fields
.field private final mGsaConfigFlags:Lchk;

.field private final mNetworkInformation:Lgno;


# direct methods
.method public constructor <init>(Lchk;Lgno;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lgct;->mGsaConfigFlags:Lchk;

    .line 21
    iput-object p2, p0, Lgct;->mNetworkInformation:Lgno;

    .line 22
    return-void
.end method


# virtual methods
.method public final Dj()Lcdy;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 26
    iget-object v0, p0, Lgct;->mNetworkInformation:Lgno;

    invoke-virtual {v0}, Lgno;->aHX()Ljava/lang/String;

    move-result-object v1

    .line 28
    const-string v0, "CELL_2G"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Lgct;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Hu()[I

    move-result-object v0

    .line 40
    :goto_0
    const-string v2, "NetTimeoutPolicyFactory"

    const-string v3, "Creating TimeoutPolicy for network=%s, [%d, %d, %d]"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v6

    aget v1, v0, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v7

    aget v1, v0, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v8

    const/4 v1, 0x3

    aget v5, v0, v8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 42
    new-instance v1, Lhhl;

    aget v2, v0, v6

    aget v3, v0, v7

    aget v0, v0, v8

    invoke-direct {v1, v2, v3, v0}, Lhhl;-><init>(III)V

    return-object v1

    .line 30
    :cond_0
    const-string v0, "CELL_3G"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31
    iget-object v0, p0, Lgct;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Hv()[I

    move-result-object v0

    goto :goto_0

    .line 32
    :cond_1
    const-string v0, "CELL_4G"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 33
    iget-object v0, p0, Lgct;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Hw()[I

    move-result-object v0

    goto :goto_0

    .line 34
    :cond_2
    const-string v0, "WIFI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 35
    iget-object v0, p0, Lgct;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Hx()[I

    move-result-object v0

    goto :goto_0

    .line 37
    :cond_3
    iget-object v0, p0, Lgct;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->Hy()[I

    move-result-object v0

    goto :goto_0
.end method
