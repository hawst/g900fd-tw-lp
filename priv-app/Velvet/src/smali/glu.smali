.class public final Lglu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lglx;


# instance fields
.field private final cOS:Lglx;

.field public zE:Z


# direct methods
.method public constructor <init>(Lglx;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lglu;->zE:Z

    .line 28
    iput-object p1, p0, Lglu;->cOS:Lglx;

    .line 29
    return-void
.end method


# virtual methods
.method public final CZ()V
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lglu;->zE:Z

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lglu;->cOS:Lglx;

    invoke-interface {v0}, Lglx;->CZ()V

    .line 101
    :cond_0
    return-void
.end method

.method public final Nr()V
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lglu;->zE:Z

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lglu;->cOS:Lglx;

    invoke-interface {v0}, Lglx;->Nr()V

    .line 164
    :cond_0
    return-void
.end method

.method public final Ns()V
    .locals 1

    .prologue
    .line 177
    iget-boolean v0, p0, Lglu;->zE:Z

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lglu;->cOS:Lglx;

    invoke-interface {v0}, Lglx;->Ns()V

    .line 180
    :cond_0
    return-void
.end method

.method public final Nv()V
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lglu;->zE:Z

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lglu;->cOS:Lglx;

    invoke-interface {v0}, Lglx;->Nv()V

    .line 155
    :cond_0
    return-void
.end method

.method public final Nw()V
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lglu;->zE:Z

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lglu;->cOS:Lglx;

    invoke-interface {v0}, Lglx;->Nw()V

    .line 146
    :cond_0
    return-void
.end method

.method public final a(Lieb;)V
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lglu;->zE:Z

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lglu;->cOS:Lglx;

    invoke-interface {v0, p1}, Lglx;->a(Lieb;)V

    .line 92
    :cond_0
    return-void
.end method

.method public final a(Ljps;)V
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lglu;->zE:Z

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lglu;->cOS:Lglx;

    invoke-interface {v0, p1}, Lglx;->a(Ljps;)V

    .line 56
    :cond_0
    return-void
.end method

.method public final a(Ljvv;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lglu;->zE:Z

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lglu;->cOS:Lglx;

    invoke-interface {v0, p1, p2}, Lglx;->a(Ljvv;Ljava/lang/String;)V

    .line 110
    :cond_0
    return-void
.end method

.method public final a(Ljxb;)V
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lglu;->zE:Z

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lglu;->cOS:Lglx;

    invoke-interface {v0, p1}, Lglx;->a(Ljxb;)V

    .line 74
    :cond_0
    return-void
.end method

.method public final aEM()V
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lglu;->zE:Z

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lglu;->cOS:Lglx;

    invoke-interface {v0}, Lglx;->aEM()V

    .line 119
    :cond_0
    return-void
.end method

.method public final au(J)V
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lglu;->zE:Z

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lglu;->cOS:Lglx;

    invoke-interface {v0, p1, p2}, Lglx;->au(J)V

    .line 128
    :cond_0
    return-void
.end method

.method public final b(Ljwp;)V
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lglu;->zE:Z

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lglu;->cOS:Lglx;

    invoke-interface {v0, p1}, Lglx;->b(Ljwp;)V

    .line 65
    :cond_0
    return-void
.end method

.method public final c(Lcom/google/android/shared/speech/HotwordResult;)V
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lglu;->zE:Z

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lglu;->cOS:Lglx;

    invoke-interface {v0, p1}, Lglx;->c(Lcom/google/android/shared/speech/HotwordResult;)V

    .line 83
    :cond_0
    return-void
.end method

.method public final c(Leiq;)V
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lglu;->zE:Z

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lglu;->cOS:Lglx;

    invoke-interface {v0, p1}, Lglx;->c(Leiq;)V

    .line 38
    :cond_0
    return-void
.end method

.method public final d(Ljyl;)V
    .locals 1

    .prologue
    .line 168
    iget-boolean v0, p0, Lglu;->zE:Z

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lglu;->cOS:Lglx;

    invoke-interface {v0, p1}, Lglx;->d(Ljyl;)V

    .line 173
    :cond_0
    return-void
.end method

.method public final invalidate()V
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x0

    iput-boolean v0, p0, Lglu;->zE:Z

    .line 184
    return-void
.end method

.method public final onEndOfSpeech()V
    .locals 1

    .prologue
    .line 132
    iget-boolean v0, p0, Lglu;->zE:Z

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lglu;->cOS:Lglx;

    invoke-interface {v0}, Lglx;->onEndOfSpeech()V

    .line 137
    :cond_0
    return-void
.end method

.method public final v([B)V
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lglu;->zE:Z

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lglu;->cOS:Lglx;

    invoke-interface {v0, p1}, Lglx;->v([B)V

    .line 47
    :cond_0
    return-void
.end method
