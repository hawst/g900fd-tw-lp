.class public Lhkj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhkr;


# instance fields
.field private djd:Z

.field private dje:Z

.field private final mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

.field private mIntentApiParams:Lhwg;

.field private final mRecognizerController:Lhwh;

.field private final mSettings:Lhym;

.field private final mSoundManager:Lhik;

.field final mUi:Lhkq;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/intentapi/IntentApiActivity;Lhkq;Lhym;Lhik;Lhwh;Ljava/util/concurrent/Executor;)V
    .locals 8

    .prologue
    .line 73
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lhkj;-><init>(Lcom/google/android/voicesearch/intentapi/IntentApiActivity;Lhkq;Lhym;Lhik;Lhwh;Ljava/util/concurrent/Executor;B)V

    .line 74
    return-void
.end method

.method public constructor <init>(Lcom/google/android/voicesearch/intentapi/IntentApiActivity;Lhkq;Lhym;Lhik;Lhwh;Ljava/util/concurrent/Executor;B)V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhkj;->dje:Z

    .line 86
    iput-object p1, p0, Lhkj;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    .line 87
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkq;

    iput-object v0, p0, Lhkj;->mUi:Lhkq;

    .line 88
    iput-object p3, p0, Lhkj;->mSettings:Lhym;

    .line 89
    iput-object p4, p0, Lhkj;->mSoundManager:Lhik;

    .line 90
    iput-object p5, p0, Lhkj;->mRecognizerController:Lhwh;

    .line 91
    iget-object v0, p0, Lhkj;->mUi:Lhkq;

    invoke-interface {v0, p0}, Lhkq;->a(Lhkr;)V

    .line 94
    return-void
.end method

.method static synthetic a(Lhkj;)Z
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lhkj;->aQq()Z

    move-result v0

    return v0
.end method

.method private aQq()Z
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lhkj;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->JW()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhkj;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->JX()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aQt()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 405
    iput-boolean v3, p0, Lhkj;->djd:Z

    .line 406
    iget-object v0, p0, Lhkj;->mUi:Lhkq;

    const v1, 0x7f0a082c

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v2, v3}, Lhkq;->b(IIZZ)V

    .line 407
    iget-boolean v0, p0, Lhkj;->dje:Z

    if-nez v0, :cond_0

    .line 408
    iget-object v0, p0, Lhkj;->mSoundManager:Lhik;

    invoke-virtual {v0}, Lhik;->aPF()V

    .line 410
    :cond_0
    return-void
.end method

.method private gx(Z)V
    .locals 4

    .prologue
    .line 226
    if-eqz p1, :cond_0

    .line 227
    iget-object v0, p0, Lhkj;->mUi:Lhkq;

    const v1, 0x7f0a083d

    invoke-interface {v0, v1}, Lhkq;->lk(I)V

    .line 234
    :goto_0
    new-instance v1, Lhkk;

    invoke-direct {v1, p0}, Lhkk;-><init>(Lhkj;)V

    .line 289
    const/4 v0, 0x0

    .line 290
    if-eqz p1, :cond_2

    .line 291
    iget-object v0, p0, Lhkj;->mRecognizerController:Lhwh;

    iget-object v2, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v2}, Lhwg;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lhwh;->b(Lglx;Ljava/lang/String;)Z

    move-result v0

    .line 297
    :goto_1
    if-eqz v0, :cond_3

    .line 298
    iget-object v0, p0, Lhkj;->mUi:Lhkq;

    invoke-interface {v0}, Lhkq;->aQx()V

    .line 302
    :goto_2
    return-void

    .line 228
    :cond_0
    iget-object v0, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v0}, Lhwg;->getPrompt()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 229
    iget-object v0, p0, Lhkj;->mUi:Lhkq;

    iget-object v1, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v1}, Lhwg;->getPrompt()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lhkq;->ob(Ljava/lang/String;)V

    goto :goto_0

    .line 231
    :cond_1
    iget-object v0, p0, Lhkj;->mUi:Lhkq;

    const v1, 0x7f0a0025

    invoke-interface {v0, v1}, Lhkq;->lk(I)V

    goto :goto_0

    .line 294
    :cond_2
    iget-object v2, p0, Lhkj;->mRecognizerController:Lhwh;

    iget-object v3, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v3}, Lhwg;->getCallingPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lhwh;->a(Lglx;Ljava/lang/String;)V

    goto :goto_1

    .line 300
    :cond_3
    iget-object v0, p0, Lhkj;->mUi:Lhkq;

    invoke-interface {v0}, Lhkq;->aQv()V

    goto :goto_2
.end method


# virtual methods
.method protected J(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 186
    iget-object v0, p0, Lhkj;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    const v1, 0x7f0a0861

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lhwg;)V
    .locals 1

    .prologue
    .line 97
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhwg;

    iput-object v0, p0, Lhkj;->mIntentApiParams:Lhwg;

    .line 98
    return-void
.end method

.method final a(Ljvw;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, -0x1

    const/4 v2, 0x0

    .line 327
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 329
    iget-object v0, p1, Ljvw;->eIk:[Ljvs;

    array-length v0, v0

    .line 330
    if-gtz v0, :cond_0

    .line 331
    invoke-direct {p0}, Lhkj;->aQt()V

    .line 382
    :goto_0
    return-void

    .line 334
    :cond_0
    iput-boolean v8, p0, Lhkj;->dje:Z

    .line 335
    iget-object v1, p0, Lhkj;->mSoundManager:Lhik;

    invoke-virtual {v1}, Lhik;->aPD()V

    .line 337
    iget-object v1, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v1}, Lhwg;->aTg()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v1}, Lhwg;->aTg()I

    move-result v1

    if-ge v1, v0, :cond_1

    .line 339
    iget-object v0, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v0}, Lhwg;->aTg()I

    move-result v0

    .line 342
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 343
    new-array v5, v0, [F

    move v1, v2

    .line 344
    :goto_1
    if-ge v1, v0, :cond_2

    .line 346
    iget-object v6, p1, Ljvw;->eIk:[Ljvs;

    aget-object v6, v6, v1

    invoke-virtual {v6}, Ljvs;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 347
    iget-object v6, p1, Ljvw;->eIk:[Ljvs;

    aget-object v6, v6, v1

    invoke-virtual {v6}, Ljvs;->bty()F

    move-result v6

    aput v6, v5, v1

    .line 344
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 350
    :cond_2
    const-string v0, "android.speech.extra.RESULTS"

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 351
    const-string v0, "android.speech.extra.CONFIDENCE_SCORES"

    invoke-virtual {v3, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[F)Landroid/content/Intent;

    .line 352
    const-string v1, "query"

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 353
    if-eqz p2, :cond_3

    .line 354
    const-string v0, "android.speech.extra.LANGUAGE_RESULTS"

    invoke-virtual {v3, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 357
    :cond_3
    iget-object v0, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v0}, Lhwg;->aTf()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 358
    iget-object v0, p0, Lhkj;->mRecognizerController:Lhwh;

    invoke-virtual {v0}, Lhwh;->aFy()Lgfc;

    move-result-object v0

    iget-object v1, p0, Lhkj;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-static {v1, v0}, Lcom/google/android/speech/audio/AudioProvider;->a(Landroid/content/Context;Lgfc;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {v3, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 361
    :cond_4
    iget-object v0, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v0}, Lhwg;->aTd()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 362
    const-string v1, "IntentApiController"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, "Recognition results: ["

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    :cond_5
    iget-object v0, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v0}, Lhwg;->yy()Landroid/app/PendingIntent;

    move-result-object v0

    if-nez v0, :cond_6

    .line 366
    iget-object v0, p0, Lhkj;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v0, v7, v3}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->a(ILandroid/content/Intent;)V

    .line 367
    iget-object v0, p0, Lhkj;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->finish()V

    goto/16 :goto_0

    .line 371
    :cond_6
    iget-object v0, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v0}, Lhwg;->aTe()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 372
    iget-object v0, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v0}, Lhwg;->aTe()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 376
    :cond_7
    :try_start_0
    iget-object v0, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v0}, Lhwg;->yy()Landroid/app/PendingIntent;

    move-result-object v0

    iget-object v1, p0, Lhkj;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381
    iget-object v0, p0, Lhkj;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->finish()V

    goto/16 :goto_0

    .line 378
    :catch_0
    move-exception v0

    .line 379
    :try_start_1
    const-string v1, "IntentApiController"

    const-string v2, "Not possible to start pending intent"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 381
    iget-object v0, p0, Lhkj;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->finish()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lhkj;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v1}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->finish()V

    throw v0
.end method

.method public final aQr()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 168
    iget-object v0, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v0}, Lhwg;->getLanguage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lhkj;->mSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aER()Ljze;

    move-result-object v2

    invoke-static {v2, v0}, Lgnq;->h(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 169
    :goto_0
    invoke-direct {p0}, Lhkj;->aQq()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v2}, Lhwg;->aTh()[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v1, p0, Lhkj;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aER()Ljze;

    move-result-object v1

    invoke-static {v1, v2}, Lgnq;->c(Ljze;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 172
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    if-nez v1, :cond_4

    .line 173
    :cond_1
    iget-object v0, p0, Lhkj;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v1

    .line 174
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    .line 175
    const-string v2, "IntentApiController"

    const-string v3, "Falling back to default language with no additional languages"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 178
    :goto_2
    iget-object v2, p0, Lhkj;->mRecognizerController:Lhwh;

    invoke-virtual {v2, v1}, Lhwh;->oq(Ljava/lang/String;)V

    .line 179
    iget-object v2, p0, Lhkj;->mRecognizerController:Lhwh;

    iput-object v0, v2, Lhwh;->cQc:Ljava/util/List;

    .line 181
    invoke-virtual {p0, v1, v0}, Lhkj;->g(Ljava/lang/String;Ljava/util/List;)V

    .line 182
    return-void

    :cond_2
    move-object v0, v1

    .line 168
    goto :goto_0

    .line 169
    :cond_3
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v1

    goto :goto_1

    :cond_4
    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_2
.end method

.method final aQs()V
    .locals 1

    .prologue
    .line 388
    iget-boolean v0, p0, Lhkj;->djd:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lhkj;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 390
    invoke-direct {p0}, Lhkj;->aQt()V

    .line 394
    :cond_0
    :goto_0
    return-void

    .line 391
    :cond_1
    iget-boolean v0, p0, Lhkj;->dje:Z

    if-nez v0, :cond_0

    .line 392
    iget-object v0, p0, Lhkj;->mSoundManager:Lhik;

    invoke-virtual {v0}, Lhik;->aPF()V

    goto :goto_0
.end method

.method final aQu()V
    .locals 1

    .prologue
    .line 414
    iget-boolean v0, p0, Lhkj;->dje:Z

    if-nez v0, :cond_0

    .line 415
    iget-object v0, p0, Lhkj;->mSoundManager:Lhik;

    invoke-virtual {v0}, Lhik;->aPF()V

    .line 417
    :cond_0
    iget-object v0, p0, Lhkj;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->finish()V

    .line 418
    return-void
.end method

.method public final afc()V
    .locals 2

    .prologue
    .line 442
    iget-object v0, p0, Lhkj;->mRecognizerController:Lhwh;

    iget-boolean v1, v0, Lhwh;->diX:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x11

    invoke-static {v1}, Lege;->ht(I)V

    iget-object v1, v0, Lhwh;->mRecognizer:Lgdb;

    iget-object v0, v0, Lhwh;->bbe:Ljava/lang/String;

    invoke-interface {v1, v0}, Lgdb;->mr(Ljava/lang/String;)V

    .line 443
    :cond_0
    return-void
.end method

.method public final afd()V
    .locals 1

    .prologue
    .line 447
    const/16 v0, 0x12

    invoke-static {v0}, Lege;->ht(I)V

    .line 449
    invoke-virtual {p0}, Lhkj;->cancel()V

    .line 450
    return-void
.end method

.method public final cancel()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lhkj;->mRecognizerController:Lhwh;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhwh;->ct(Z)V

    .line 103
    return-void
.end method

.method public g(Ljava/lang/String;Ljava/util/List;)V
    .locals 3

    .prologue
    .line 194
    invoke-direct {p0}, Lhkj;->aQq()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    iget-object v0, p0, Lhkj;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    invoke-static {v0, p1}, Lgnq;->b(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 198
    iget-object v1, p0, Lhkj;->mUi:Lhkq;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lhkj;->J(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lhkq;->oa(Ljava/lang/String;)V

    .line 210
    :goto_0
    return-void

    .line 201
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lhkj;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aER()Ljze;

    move-result-object v1

    invoke-static {v0, v1}, Lgnq;->a(Ljava/lang/String;Ljze;)Ljava/lang/String;

    move-result-object v0

    .line 203
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Lhkj;->mUi:Lhkq;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lhkq;->oa(Ljava/lang/String;)V

    goto :goto_0

    .line 206
    :cond_1
    iget-object v0, p0, Lhkj;->mUi:Lhkq;

    iget-object v1, p0, Lhkj;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aER()Ljze;

    move-result-object v1

    invoke-static {v1, p1}, Lgnq;->b(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lhkq;->oa(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final gy(Z)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 431
    if-eqz p1, :cond_0

    .line 432
    iput-boolean v0, p0, Lhkj;->djd:Z

    .line 433
    invoke-direct {p0, v0}, Lhkj;->gx(Z)V

    .line 438
    :goto_0
    return-void

    .line 435
    :cond_0
    iput-boolean v0, p0, Lhkj;->djd:Z

    .line 436
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhkj;->gx(Z)V

    goto :goto_0
.end method

.method final l(Leiq;)V
    .locals 5

    .prologue
    .line 306
    invoke-static {p1}, Leno;->e(Leiq;)I

    move-result v0

    .line 307
    invoke-static {p1}, Leno;->i(Leiq;)I

    move-result v1

    .line 308
    invoke-static {p1}, Leno;->g(Leiq;)Z

    move-result v2

    .line 309
    invoke-static {p1}, Leno;->j(Leiq;)Z

    move-result v3

    .line 311
    const/4 v4, 0x1

    iput-boolean v4, p0, Lhkj;->djd:Z

    .line 312
    iget-object v4, p0, Lhkj;->mUi:Lhkq;

    invoke-interface {v4, v0, v1, v2, v3}, Lhkq;->b(IIZZ)V

    .line 313
    iget-boolean v0, p0, Lhkj;->dje:Z

    if-nez v0, :cond_0

    .line 314
    iget-object v0, p0, Lhkj;->mSoundManager:Lhik;

    invoke-virtual {v0}, Lhik;->aPH()V

    .line 316
    :cond_0
    return-void
.end method

.method public final lj(I)V
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lhkj;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->lw(I)V

    .line 426
    iget-object v0, p0, Lhkj;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->finish()V

    .line 427
    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 106
    iget-boolean v2, p0, Lhkj;->djd:Z

    if-eqz v2, :cond_0

    .line 119
    :goto_0
    return-void

    .line 110
    :cond_0
    iget-object v2, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v2}, Lhwg;->aTf()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v2}, Lhwg;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lhkj;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v3}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "android.permission.RECORD_AUDIO"

    invoke-virtual {v3, v4, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_1

    const-string v0, "IntentApiController"

    const-string v2, "Must have android.permission.RECORD_AUDIO to record audio"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :cond_1
    if-nez v0, :cond_3

    .line 111
    iget-object v0, p0, Lhkj;->mActivity:Lcom/google/android/voicesearch/intentapi/IntentApiActivity;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->finish()V

    goto :goto_0

    :cond_2
    move v2, v1

    .line 110
    goto :goto_1

    .line 115
    :cond_3
    invoke-virtual {p0}, Lhkj;->aQr()V

    .line 116
    iget-object v0, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v0}, Lhwg;->aTj()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v2, p0, Lhkj;->mRecognizerController:Lhwh;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, v2, Lhwh;->cPu:Z

    .line 117
    :goto_2
    iget-object v0, p0, Lhkj;->mRecognizerController:Lhwh;

    iget-object v2, p0, Lhkj;->mIntentApiParams:Lhwg;

    invoke-virtual {v2}, Lhwg;->aTi()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, v0, Lhwh;->dtf:Landroid/net/Uri;

    .line 118
    invoke-direct {p0, v1}, Lhkj;->gx(Z)V

    goto :goto_0

    .line 116
    :cond_4
    iget-object v0, p0, Lhkj;->mRecognizerController:Lhwh;

    iget-object v2, p0, Lhkj;->mSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aHP()Z

    move-result v2

    iput-boolean v2, v0, Lhwh;->cPu:Z

    goto :goto_2
.end method
