.class public final Litv;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dHA:I

.field private dHB:Ljava/lang/String;

.field private dHC:Ljava/lang/String;

.field private dHz:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3249
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3250
    iput v0, p0, Litv;->aez:I

    iput v0, p0, Litv;->dHz:I

    iput v0, p0, Litv;->dHA:I

    const-string v0, ""

    iput-object v0, p0, Litv;->dHB:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Litv;->dHC:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Litv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Litv;->eCz:I

    .line 3251
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 3148
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Litv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litv;->dHz:I

    iget v0, p0, Litv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Litv;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litv;->dHA:I

    iget v0, p0, Litv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Litv;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Litv;->dHB:Ljava/lang/String;

    iget v0, p0, Litv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Litv;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Litv;->dHC:Ljava/lang/String;

    iget v0, p0, Litv;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Litv;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 3267
    iget v0, p0, Litv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3268
    const/4 v0, 0x1

    iget v1, p0, Litv;->dHz:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 3270
    :cond_0
    iget v0, p0, Litv;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 3271
    const/4 v0, 0x2

    iget v1, p0, Litv;->dHA:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 3273
    :cond_1
    iget v0, p0, Litv;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 3274
    const/4 v0, 0x3

    iget-object v1, p0, Litv;->dHB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3276
    :cond_2
    iget v0, p0, Litv;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 3277
    const/4 v0, 0x4

    iget-object v1, p0, Litv;->dHC:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3279
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3280
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 3284
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3285
    iget v1, p0, Litv;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3286
    const/4 v1, 0x1

    iget v2, p0, Litv;->dHz:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3289
    :cond_0
    iget v1, p0, Litv;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 3290
    const/4 v1, 0x2

    iget v2, p0, Litv;->dHA:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3293
    :cond_1
    iget v1, p0, Litv;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 3294
    const/4 v1, 0x3

    iget-object v2, p0, Litv;->dHB:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3297
    :cond_2
    iget v1, p0, Litv;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 3298
    const/4 v1, 0x4

    iget-object v2, p0, Litv;->dHC:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3301
    :cond_3
    return v0
.end method

.method public final mM(I)Litv;
    .locals 1

    .prologue
    .line 3173
    iput p1, p0, Litv;->dHz:I

    .line 3174
    iget v0, p0, Litv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Litv;->aez:I

    .line 3175
    return-object p0
.end method

.method public final mN(I)Litv;
    .locals 1

    .prologue
    .line 3192
    iput p1, p0, Litv;->dHA:I

    .line 3193
    iget v0, p0, Litv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Litv;->aez:I

    .line 3194
    return-object p0
.end method

.method public final pR(Ljava/lang/String;)Litv;
    .locals 1

    .prologue
    .line 3211
    if-nez p1, :cond_0

    .line 3212
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3214
    :cond_0
    iput-object p1, p0, Litv;->dHB:Ljava/lang/String;

    .line 3215
    iget v0, p0, Litv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Litv;->aez:I

    .line 3216
    return-object p0
.end method

.method public final pS(Ljava/lang/String;)Litv;
    .locals 1

    .prologue
    .line 3233
    if-nez p1, :cond_0

    .line 3234
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3236
    :cond_0
    iput-object p1, p0, Litv;->dHC:Ljava/lang/String;

    .line 3237
    iget v0, p0, Litv;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Litv;->aez:I

    .line 3238
    return-object p0
.end method
