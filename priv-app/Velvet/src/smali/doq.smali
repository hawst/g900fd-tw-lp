.class final Ldoq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhuo;


# instance fields
.field private synthetic bEm:Ldoj;


# direct methods
.method constructor <init>(Ldoj;)V
    .locals 0

    .prologue
    .line 396
    iput-object p1, p0, Ldoq;->bEm:Ldoj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final Nv()V
    .locals 4

    .prologue
    .line 571
    const-string v0, "HotwordWorker"

    const-string v1, "onMusicDetected"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x4

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 572
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v0}, Ldby;->Nv()V

    .line 573
    return-void
.end method

.method public final b(Lcom/google/android/shared/speech/HotwordResult;)V
    .locals 11

    .prologue
    .line 399
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v10

    .line 403
    const-string v0, "HotwordWorker"

    const-string v1, "#onHotwordDetected"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x4

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 410
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aTR()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v0}, Ldby;->isActive()Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_0

    .line 412
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldoj;->dH(Z)V

    .line 413
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    invoke-virtual {v0}, Ldoj;->aeh()V

    .line 416
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/shared/speech/HotwordResult;->asQ()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    invoke-virtual {v10}, Lcom/google/android/search/shared/service/ClientConfig;->anf()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 419
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v10}, Lcom/google/android/search/shared/service/ClientConfig;->anf()Z

    move-result v1

    if-nez v1, :cond_2

    .line 420
    const-string v0, "HotwordWorker"

    const-string v1, "Client doesn\'t want to handle hotword in DUMP_UTTERANCE mode"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 567
    :goto_1
    return-void

    .line 416
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 423
    :cond_2
    if-eqz v0, :cond_4

    .line 425
    iget-object v1, p0, Ldoq;->bEm:Ldoj;

    iget-object v1, v1, Ldoj;->mContext:Landroid/content/Context;

    iget-object v2, p0, Ldoq;->bEm:Ldoj;

    iget-object v2, v2, Ldoj;->bEc:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-virtual {p1}, Lcom/google/android/shared/speech/HotwordResult;->asV()[B

    move-result-object v3

    iget-object v4, p0, Ldoq;->bEm:Ldoj;

    iget-object v4, v4, Ldoj;->mContext:Landroid/content/Context;

    iget-object v5, p0, Ldoq;->bEm:Ldoj;

    iget-object v5, v5, Ldoj;->mSearchSettings:Lcke;

    invoke-interface {v5}, Lcke;->ND()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/io/File;

    const-string v7, "sid"

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ".pcm"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v6, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lgep;->a(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;[BLjava/lang/String;)V

    .line 435
    :cond_3
    :goto_2
    invoke-virtual {v10}, Lcom/google/android/search/shared/service/ClientConfig;->ang()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 442
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v0, p1}, Ldby;->b(Lcom/google/android/shared/speech/HotwordResult;)V

    goto :goto_1

    .line 428
    :cond_4
    iget-object v1, p0, Ldoq;->bEm:Ldoj;

    iget-object v1, v1, Ldoj;->mVoiceSettings:Lhym;

    invoke-virtual {v1}, Lhym;->Me()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 429
    iget-object v1, p0, Ldoq;->bEm:Ldoj;

    iget-object v1, v1, Ldoj;->mContext:Landroid/content/Context;

    iget-object v2, p0, Ldoq;->bEm:Ldoj;

    iget-object v2, v2, Ldoj;->bEc:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-virtual {p1}, Lcom/google/android/shared/speech/HotwordResult;->asV()[B

    move-result-object v3

    iget-object v4, p0, Ldoq;->bEm:Ldoj;

    iget-object v4, v4, Ldoj;->mContext:Landroid/content/Context;

    const-string v5, "-hotword"

    invoke-static {v4, v5}, Lgep;->q(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lgep;->a(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;[BLjava/lang/String;)V

    goto :goto_2

    .line 447
    :cond_5
    iget-object v1, p0, Ldoq;->bEm:Ldoj;

    iget-object v1, v1, Ldoj;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->getMode()I

    move-result v1

    .line 448
    if-nez v1, :cond_6

    if-eqz v0, :cond_7

    .line 454
    :cond_6
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v0, p1}, Ldby;->b(Lcom/google/android/shared/speech/HotwordResult;)V

    goto/16 :goto_1

    .line 459
    :cond_7
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v0}, Ldby;->WO()Lent;

    move-result-object v0

    invoke-virtual {v0}, Lent;->auL()V

    .line 460
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mVoiceSettings:Lhym;

    iget-object v1, p0, Ldoq;->bEm:Ldoj;

    iget-object v1, v1, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v1}, Ldby;->WO()Lent;

    move-result-object v1

    invoke-virtual {v1}, Lent;->auO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhym;->ha(Ljava/lang/String;)V

    .line 463
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aTS()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_8

    .line 465
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mVoiceSettings:Lhym;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lhym;->lA(I)V

    .line 470
    :cond_8
    invoke-virtual {v10}, Lcom/google/android/search/shared/service/ClientConfig;->amT()Z

    move-result v1

    .line 471
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mScreenStateHelper:Ldmh;

    invoke-virtual {v0}, Ldmh;->adq()I

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_3
    iget-object v2, p0, Ldoq;->bEm:Ldoj;

    iget-object v2, v2, Ldoj;->mScreenStateHelper:Ldmh;

    invoke-virtual {v2}, Ldmh;->isScreenOn()Z

    move-result v2

    iget-object v3, p0, Ldoq;->bEm:Ldoj;

    iget-object v3, v3, Ldoj;->mServiceState:Ldcu;

    invoke-virtual {v3}, Ldcu;->Zc()Z

    iget-object v3, p0, Ldoq;->bEm:Ldoj;

    iget-object v3, v3, Ldoj;->mServiceState:Ldcu;

    invoke-virtual {v3}, Ldcu;->YZ()Z

    move-result v3

    iget-object v4, p0, Ldoq;->bEm:Ldoj;

    iget-object v4, v4, Ldoj;->mServiceState:Ldcu;

    invoke-virtual {v4}, Ldcu;->YA()Z

    move-result v4

    invoke-static {v0, v2, v3, v4}, Ldmu;->a(ZZZZ)Lcom/google/android/shared/search/Query;

    move-result-object v9

    .line 477
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mScreenStateHelper:Ldmh;

    invoke-virtual {v0}, Ldmh;->adq()I

    move-result v2

    .line 480
    const/16 v0, 0x41

    .line 481
    invoke-virtual {p1}, Lcom/google/android/shared/speech/HotwordResult;->asQ()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_d

    .line 482
    invoke-virtual {p1}, Lcom/google/android/shared/speech/HotwordResult;->asR()Z

    move-result v0

    if-nez v0, :cond_c

    .line 484
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget v1, v0, Ldoj;->bEl:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Ldoj;->bEl:I

    .line 504
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget v0, v0, Ldoj;->bEl:I

    iget-object v1, p0, Ldoq;->bEm:Ldoj;

    iget-object v1, v1, Ldoj;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->Jh()I

    move-result v1

    if-lt v0, v1, :cond_9

    .line 505
    invoke-virtual {v10}, Lcom/google/android/search/shared/service/ClientConfig;->ans()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 507
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mVoiceSettings:Lhym;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lhym;->lB(I)V

    .line 509
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    const/4 v1, 0x0

    iput v1, v0, Ldoj;->bEl:I

    .line 516
    :cond_9
    :goto_4
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    const/16 v1, 0x12f

    invoke-virtual {v0, v1, v9, p1}, Ldoj;->a(ILcom/google/android/shared/search/Query;Lcom/google/android/shared/speech/HotwordResult;)V

    .line 518
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v0, p1}, Ldby;->b(Lcom/google/android/shared/speech/HotwordResult;)V

    goto/16 :goto_1

    .line 471
    :cond_a
    const/4 v0, 0x0

    goto :goto_3

    .line 511
    :cond_b
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mContext:Landroid/content/Context;

    const v1, 0x7f0a0951

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_4

    .line 521
    :cond_c
    const/16 v0, 0x12d

    .line 526
    :cond_d
    iget-object v3, p0, Ldoq;->bEm:Ldoj;

    iget-object v3, v3, Ldoj;->bEd:Ldlu;

    const/4 v4, 0x0

    iget-object v5, p0, Ldoq;->bEm:Ldoj;

    iget-object v5, v5, Ldoj;->mVoiceSettings:Lhym;

    invoke-virtual {v3, v4, v5, v2}, Ldlu;->a(ILhym;I)Z

    move-result v2

    if-nez v2, :cond_e

    .line 530
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    const/16 v1, 0x12e

    invoke-virtual {v0, v1, v9, p1}, Ldoj;->a(ILcom/google/android/shared/search/Query;Lcom/google/android/shared/speech/HotwordResult;)V

    .line 533
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v0, p1}, Ldby;->b(Lcom/google/android/shared/speech/HotwordResult;)V

    goto/16 :goto_1

    .line 537
    :cond_e
    iget-object v2, p0, Ldoq;->bEm:Ldoj;

    invoke-virtual {v2, v0, v9, p1}, Ldoj;->a(ILcom/google/android/shared/search/Query;Lcom/google/android/shared/speech/HotwordResult;)V

    .line 540
    invoke-virtual {p1}, Lcom/google/android/shared/speech/HotwordResult;->asV()[B

    move-result-object v0

    if-eqz v0, :cond_f

    .line 541
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aPh()Lgfb;

    move-result-object v0

    invoke-virtual {v9}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lgfc;

    invoke-virtual {p1}, Lcom/google/android/shared/speech/HotwordResult;->getSampleRate()I

    move-result v4

    invoke-virtual {p1}, Lcom/google/android/shared/speech/HotwordResult;->asV()[B

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lgfc;-><init>(I[B)V

    invoke-interface {v0, v2, v3}, Lgfb;->a(Ljava/lang/String;Lgfc;)V

    .line 547
    :cond_f
    if-eqz v1, :cond_11

    .line 548
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mAssistContextHelper:Lckx;

    invoke-virtual {v0}, Lckx;->Pe()Ljava/lang/String;

    move-result-object v0

    .line 549
    const/4 v8, 0x0

    .line 550
    if-eqz v0, :cond_10

    .line 552
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 553
    const-string v1, "android.intent.extra.ASSIST_PACKAGE"

    invoke-virtual {v8, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    :cond_10
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v1, v0, Ldoj;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    const-wide/16 v2, 0x64

    const-wide/16 v4, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/search/core/state/QueryState;->a(JJLandroid/os/Bundle;ILandroid/os/Bundle;)V

    .line 561
    :cond_11
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->ant()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 562
    invoke-virtual {v9}, Lcom/google/android/shared/search/Query;->aoY()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 565
    :goto_5
    iget-object v1, p0, Ldoq;->bEm:Ldoj;

    iget-object v1, v1, Ldoj;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v10}, Lcom/google/android/search/shared/service/ClientConfig;->anv()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/search/core/state/QueryState;->y(Lcom/google/android/shared/search/Query;)V

    .line 566
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v0, p1}, Ldby;->b(Lcom/google/android/shared/speech/HotwordResult;)V

    goto/16 :goto_1

    :cond_12
    move-object v0, v9

    goto :goto_5
.end method

.method public final dd(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 584
    const-string v0, "HotwordWorker"

    const-string v1, "#onError(%b)"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 585
    if-eqz p1, :cond_0

    .line 586
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v0, v5}, Ldby;->dd(Z)V

    .line 591
    :goto_0
    return-void

    .line 588
    :cond_0
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mUiThread:Leqo;

    iget-object v1, p0, Ldoq;->bEm:Ldoj;

    iget-object v1, v1, Ldoj;->bEf:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 589
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mUiThread:Leqo;

    iget-object v1, p0, Ldoq;->bEm:Ldoj;

    iget-object v1, v1, Ldoj;->bEf:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-interface {v0, v1, v2, v3}, Leqo;->a(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method public final onReady()V
    .locals 4

    .prologue
    .line 577
    const-string v0, "HotwordWorker"

    const-string v1, "onReady"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x4

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 578
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mUiThread:Leqo;

    iget-object v1, p0, Ldoq;->bEm:Ldoj;

    iget-object v1, v1, Ldoj;->bEf:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 579
    iget-object v0, p0, Ldoq;->bEm:Ldoj;

    iget-object v0, v0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v0}, Ldby;->onReady()V

    .line 580
    return-void
.end method
