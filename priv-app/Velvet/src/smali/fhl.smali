.class final Lfhl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private synthetic crG:Ljava/util/List;

.field private synthetic crH:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

.field private synthetic crI:Lfhj;


# direct methods
.method constructor <init>(Lfhj;Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lfhl;->crI:Lfhj;

    iput-object p2, p0, Lfhl;->crG:Ljava/util/List;

    iput-object p3, p0, Lfhl;->crH:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 99
    iget-object v0, p0, Lfhl;->crI:Lfhj;

    invoke-virtual {v0}, Lfhj;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lfhs;

    if-eqz v0, :cond_2

    .line 100
    check-cast p1, Landroid/app/AlertDialog;

    .line 101
    invoke-virtual {p1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v3

    .line 104
    new-instance v4, Ljdf;

    invoke-direct {v4}, Ljdf;-><init>()V

    move v1, v2

    .line 105
    :goto_0
    iget-object v0, p0, Lfhl;->crG:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 106
    invoke-virtual {v3, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    iget-object v5, v4, Ljdf;->ebG:[I

    iget-object v0, p0, Lfhl;->crG:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpa;

    iget-object v0, v0, Lfpa;->cyl:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v5, v0}, Leqh;->b([II)[I

    move-result-object v0

    iput-object v0, v4, Ljdf;->ebG:[I

    .line 105
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 112
    :cond_1
    iget-object v0, p0, Lfhl;->crI:Lfhj;

    invoke-virtual {v0}, Lfhj;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lfhs;

    iget-object v1, p0, Lfhl;->crH:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-virtual {v0, v1, v4}, Lfhs;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljdf;)V

    .line 115
    iget-object v0, p0, Lfhl;->crI:Lfhj;

    invoke-virtual {v0}, Lfhj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0a045c

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 118
    :cond_2
    return-void
.end method
