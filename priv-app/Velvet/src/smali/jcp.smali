.class public final Ljcp;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private bmI:Ljava/lang/String;

.field private dHr:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48297
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 48298
    const/4 v0, 0x0

    iput v0, p0, Ljcp;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljcp;->bmI:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljcp;->dHr:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljcp;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljcp;->eCz:I

    .line 48299
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 48234
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljcp;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcp;->bmI:Ljava/lang/String;

    iget v0, p0, Ljcp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcp;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcp;->dHr:Ljava/lang/String;

    iget v0, p0, Ljcp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljcp;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 48313
    iget v0, p0, Ljcp;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 48314
    const/4 v0, 0x1

    iget-object v1, p0, Ljcp;->bmI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 48316
    :cond_0
    iget v0, p0, Ljcp;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 48317
    const/4 v0, 0x2

    iget-object v1, p0, Ljcp;->dHr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 48319
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 48320
    return-void
.end method

.method public final getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48256
    iget-object v0, p0, Ljcp;->bmI:Ljava/lang/String;

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48278
    iget-object v0, p0, Ljcp;->dHr:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 48324
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 48325
    iget v1, p0, Ljcp;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 48326
    const/4 v1, 0x1

    iget-object v2, p0, Ljcp;->bmI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 48329
    :cond_0
    iget v1, p0, Ljcp;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 48330
    const/4 v1, 0x2

    iget-object v2, p0, Ljcp;->dHr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 48333
    :cond_1
    return v0
.end method

.method public final tm(Ljava/lang/String;)Ljcp;
    .locals 1

    .prologue
    .line 48259
    if-nez p1, :cond_0

    .line 48260
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48262
    :cond_0
    iput-object p1, p0, Ljcp;->bmI:Ljava/lang/String;

    .line 48263
    iget v0, p0, Ljcp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcp;->aez:I

    .line 48264
    return-object p0
.end method

.method public final tn(Ljava/lang/String;)Ljcp;
    .locals 1

    .prologue
    .line 48281
    if-nez p1, :cond_0

    .line 48282
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48284
    :cond_0
    iput-object p1, p0, Ljcp;->dHr:Ljava/lang/String;

    .line 48285
    iget v0, p0, Ljcp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljcp;->aez:I

    .line 48286
    return-object p0
.end method
