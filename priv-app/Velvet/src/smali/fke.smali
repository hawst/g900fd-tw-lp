.class public Lfke;
.super Lfkb;
.source "PG"


# instance fields
.field private mActivityHelper:Lfzw;

.field protected final mClock:Lemp;

.field protected final mFifeImageUrlUtil:Lgan;

.field protected final mIntentUtils:Leom;

.field protected final mPhotoWithAttributionDecorator:Lgbd;

.field protected final mPlaceDataHelper:Lgbe;

.field protected final mStringEvaluator:Lgbr;


# direct methods
.method public constructor <init>(Lemp;Lgah;Landroid/net/wifi/WifiManager;Lfzw;Lerk;Landroid/content/pm/PackageManager;Landroid/content/ContentResolver;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Lfkb;-><init>()V

    .line 50
    new-instance v0, Leon;

    invoke-direct {v0}, Leon;-><init>()V

    iput-object v0, p0, Lfke;->mIntentUtils:Leom;

    .line 51
    iput-object p1, p0, Lfke;->mClock:Lemp;

    .line 52
    iput-object p4, p0, Lfke;->mActivityHelper:Lfzw;

    .line 55
    new-instance v0, Lgan;

    invoke-direct {v0}, Lgan;-><init>()V

    iput-object v0, p0, Lfke;->mFifeImageUrlUtil:Lgan;

    .line 59
    new-instance v0, Lgbe;

    iget-object v1, p0, Lfke;->mFifeImageUrlUtil:Lgan;

    invoke-direct {v0, v1}, Lgbe;-><init>(Lgan;)V

    iput-object v0, p0, Lfke;->mPlaceDataHelper:Lgbe;

    .line 60
    new-instance v0, Lgbd;

    iget-object v1, p0, Lfke;->mFifeImageUrlUtil:Lgan;

    invoke-direct {v0, v1}, Lgbd;-><init>(Lgan;)V

    iput-object v0, p0, Lfke;->mPhotoWithAttributionDecorator:Lgbd;

    .line 61
    new-instance v0, Lgbr;

    invoke-direct {v0, p1}, Lgbr;-><init>(Lemp;)V

    iput-object v0, p0, Lfke;->mStringEvaluator:Lgbr;

    .line 62
    return-void
.end method


# virtual methods
.method protected synthetic b(Lizj;Ljal;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1, p2, p3}, Lfke;->d(Lizj;Ljal;I)Lfkd;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c(ILizj;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1, p2}, Lfke;->d(ILizj;)Lfkd;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic c(ILizq;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1, p2}, Lfke;->d(ILizq;)Lfkd;

    move-result-object v0

    return-object v0
.end method

.method protected d(ILizj;)Lfkd;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 66
    sparse-switch p1, :sswitch_data_0

    .line 84
    :cond_0
    :goto_0
    return-object v0

    .line 68
    :sswitch_0
    new-instance v0, Lfjy;

    iget-object v1, p0, Lfke;->mActivityHelper:Lfzw;

    iget-object v2, p0, Lfke;->mStringEvaluator:Lgbr;

    invoke-direct {v0, p2, v1, v2}, Lfjy;-><init>(Lizj;Lfzw;Lgbr;)V

    goto :goto_0

    .line 71
    :sswitch_1
    iget-object v1, p2, Lizj;->dSK:Lizr;

    invoke-virtual {v1}, Lizr;->getType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 72
    new-instance v0, Lfkf;

    iget-object v1, p0, Lfke;->mActivityHelper:Lfzw;

    iget-object v2, p0, Lfke;->mPhotoWithAttributionDecorator:Lgbd;

    invoke-direct {v0, p2, v1, v2}, Lfkf;-><init>(Lizj;Lfzw;Lgbd;)V

    goto :goto_0

    .line 78
    :sswitch_2
    new-instance v0, Lfkt;

    iget-object v1, p0, Lfke;->mActivityHelper:Lfzw;

    invoke-direct {v0, p2, v1}, Lfkt;-><init>(Lizj;Lfzw;)V

    goto :goto_0

    .line 81
    :sswitch_3
    new-instance v0, Lfku;

    iget-object v1, p0, Lfke;->mActivityHelper:Lfzw;

    invoke-direct {v0, p2, v1}, Lfku;-><init>(Lizj;Lfzw;)V

    goto :goto_0

    .line 66
    :sswitch_data_0
    .sparse-switch
        0x1c -> :sswitch_1
        0x43 -> :sswitch_3
        0x79 -> :sswitch_0
        0x7e -> :sswitch_2
    .end sparse-switch
.end method

.method protected d(ILizq;)Lfkd;
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return-object v0
.end method

.method protected d(Lizj;Ljal;I)Lfkd;
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return-object v0
.end method
