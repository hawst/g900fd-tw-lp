.class public Lhym;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgdo;


# instance fields
.field private final bgq:Lchk;

.field private duR:Lhyc;

.field private mContext:Landroid/content/Context;

.field private final mGStaticConfiguration:Lhye;

.field private final mHttpHelper:Ldkx;

.field private final mPrefController:Lchr;

.field private final mSearchConfig:Lcjs;

.field private final mSearchSettings:Lcke;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lchr;Lcke;Lcjs;Lchk;Ldkx;Ljava/util/concurrent/ExecutorService;)V
    .locals 10

    .prologue
    .line 266
    new-instance v8, Lhye;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v2, Lchw;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v2, v3}, Lchw;-><init>(Landroid/content/ContentResolver;)V

    move-object/from16 v0, p7

    invoke-direct {v8, p2, v1, v0, v2}, Lhye;-><init>(Lchr;Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;Lchw;)V

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Lhym;-><init>(Lchr;Landroid/content/Context;Lcke;Lcjs;Lchk;Ldkx;Lhye;Ljava/util/concurrent/ExecutorService;)V

    .line 277
    return-void
.end method

.method public constructor <init>(Lchr;Landroid/content/Context;Lcke;Lcjs;Lchk;Ldkx;Lhye;Ljava/util/concurrent/ExecutorService;)V
    .locals 2

    .prologue
    .line 287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 260
    const/4 v0, 0x0

    iput-object v0, p0, Lhym;->duR:Lhyc;

    .line 288
    iput-object p1, p0, Lhym;->mPrefController:Lchr;

    .line 289
    iput-object p2, p0, Lhym;->mContext:Landroid/content/Context;

    .line 290
    iput-object p3, p0, Lhym;->mSearchSettings:Lcke;

    .line 291
    iput-object p4, p0, Lhym;->mSearchConfig:Lcjs;

    .line 292
    iput-object p5, p0, Lhym;->bgq:Lchk;

    .line 293
    iput-object p6, p0, Lhym;->mHttpHelper:Ldkx;

    .line 296
    new-instance v0, Lcry;

    invoke-direct {v0, p2, p0}, Lcry;-><init>(Landroid/content/Context;Lhym;)V

    .line 297
    iput-object p7, p0, Lhym;->mGStaticConfiguration:Lhye;

    .line 298
    iget-object v0, p0, Lhym;->mGStaticConfiguration:Lhye;

    new-instance v1, Lhyn;

    invoke-direct {v1, p0}, Lhyn;-><init>(Lhym;)V

    invoke-virtual {v0, v1}, Lhye;->a(Lhyo;)V

    .line 306
    return-void
.end method

.method private Wy()Z
    .locals 2

    .prologue
    .line 611
    iget-object v0, p0, Lhym;->bgq:Lchk;

    invoke-virtual {v0}, Lchk;->getHotwordConfigMap()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsi;

    .line 612
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcsi;->SE()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aUf()Lhyc;
    .locals 2

    .prologue
    .line 830
    iget-object v0, p0, Lhym;->duR:Lhyc;

    if-nez v0, :cond_0

    .line 831
    new-instance v0, Lhyc;

    iget-object v1, p0, Lhym;->bgq:Lchk;

    invoke-direct {v0, p0, v1}, Lhyc;-><init>(Lhym;Lchk;)V

    iput-object v0, p0, Lhym;->duR:Lhyc;

    .line 832
    iget-object v0, p0, Lhym;->duR:Lhyc;

    invoke-virtual {v0}, Lhyc;->aTw()V

    .line 834
    :cond_0
    iget-object v0, p0, Lhym;->duR:Lhyc;

    return-object v0
.end method

.method private n(Ljze;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 346
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lgnq;->a(Ljava/lang/String;Ljze;)Ljava/lang/String;

    move-result-object v0

    .line 348
    iget-object v1, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "spoken-language-bcp-47"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "spoken-language-default"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 353
    return-object v0
.end method


# virtual methods
.method public final BU()Z
    .locals 3

    .prologue
    .line 699
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "hands_free_auto_driving_car_mode_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final BV()Z
    .locals 3

    .prologue
    .line 703
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "hands_free_auto_nav_car_mode_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized D(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 380
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    .line 381
    const-string v1, "spoken-language-bcp-47"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 382
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "spoken-language-bcp-47"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "spoken-language-default"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 387
    :cond_0
    monitor-exit p0

    return-void

    .line 380
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final Hq()Z
    .locals 1

    .prologue
    .line 1208
    iget-object v0, p0, Lhym;->bgq:Lchk;

    invoke-virtual {v0}, Lchk;->Hq()Z

    move-result v0

    return v0
.end method

.method public final Hr()Z
    .locals 1

    .prologue
    .line 1218
    iget-object v0, p0, Lhym;->bgq:Lchk;

    invoke-virtual {v0}, Lchk;->Hr()Z

    move-result v0

    return v0
.end method

.method public final Hs()I
    .locals 1

    .prologue
    .line 1213
    iget-object v0, p0, Lhym;->bgq:Lchk;

    invoke-virtual {v0}, Lchk;->Hs()I

    move-result v0

    return v0
.end method

.method public final IW()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1193
    iget-object v0, p0, Lhym;->bgq:Lchk;

    invoke-virtual {v0}, Lchk;->IW()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final JV()I
    .locals 1

    .prologue
    .line 1043
    iget-object v0, p0, Lhym;->bgq:Lchk;

    invoke-virtual {v0}, Lchk;->JV()I

    move-result v0

    return v0
.end method

.method public final JW()Z
    .locals 1

    .prologue
    .line 1047
    iget-object v0, p0, Lhym;->bgq:Lchk;

    invoke-virtual {v0}, Lchk;->JW()Z

    move-result v0

    return v0
.end method

.method public final JX()Z
    .locals 1

    .prologue
    .line 1051
    iget-object v0, p0, Lhym;->bgq:Lchk;

    invoke-virtual {v0}, Lchk;->JX()Z

    move-result v0

    return v0
.end method

.method public final M(Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 756
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "adaptive_tts_%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 757
    return-void
.end method

.method public final Mc()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1030
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "debugS3Server"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Me()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1122
    iget-object v1, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    const-string v2, "audioLoggingEnabled"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lhym;->mSearchConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->Me()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final Oe()Ljava/lang/String;
    .locals 1

    .prologue
    .line 632
    iget-object v0, p0, Lhym;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->Oe()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Of()J
    .locals 2

    .prologue
    .line 642
    iget-object v0, p0, Lhym;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->Of()J

    move-result-wide v0

    return-wide v0
.end method

.method public final W(J)V
    .locals 1

    .prologue
    .line 647
    iget-object v0, p0, Lhym;->mSearchSettings:Lcke;

    invoke-interface {v0, p1, p2}, Lcke;->W(J)V

    .line 648
    return-void
.end method

.method public final aER()Ljze;
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lhym;->mGStaticConfiguration:Lhye;

    invoke-virtual {v0}, Lhye;->aTB()Ljze;

    move-result-object v0

    return-object v0
.end method

.method public final aFa()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1110
    iget-object v1, p0, Lhym;->bgq:Lchk;

    invoke-virtual {v1}, Lchk;->Ga()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1111
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lhym;->aUt()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final aFb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1117
    iget-object v0, p0, Lhym;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->Ob()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized aFc()Ljava/lang/String;
    .locals 3

    .prologue
    .line 362
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "spoken-language-bcp-47"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 363
    if-nez v0, :cond_0

    .line 364
    iget-object v0, p0, Lhym;->mGStaticConfiguration:Lhye;

    invoke-virtual {v0}, Lhye;->aTB()Ljze;

    move-result-object v0

    invoke-direct {p0, v0}, Lhym;->n(Ljze;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 366
    :cond_0
    monitor-exit p0

    return-object v0

    .line 362
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final aFd()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1128
    iget-object v2, p0, Lhym;->mSearchConfig:Lcjs;

    invoke-virtual {v2}, Lcjs;->LZ()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lhym;->aER()Ljze;

    move-result-object v2

    iget-object v3, v2, Ljze;->eNu:Ljzz;

    if-eqz v3, :cond_4

    iget-object v2, v2, Ljze;->eNu:Ljzz;

    iget-object v3, v2, Ljzz;->eOH:[Ljava/lang/String;

    array-length v4, v3

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    :cond_0
    invoke-virtual {p0}, Lhym;->aER()Ljze;

    move-result-object v2

    iget-object v2, v2, Ljze;->eNp:Ljzf;

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    return v0

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    move v2, v0

    goto :goto_1
.end method

.method public final aFe()Ljava/util/List;
    .locals 5

    .prologue
    .line 1164
    invoke-virtual {p0}, Lhym;->aUp()Ljava/util/List;

    move-result-object v1

    .line 1165
    iget-object v0, p0, Lhym;->bgq:Lchk;

    invoke-virtual {v0}, Lchk;->Ft()[I

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 1166
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1165
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1168
    :cond_0
    return-object v1
.end method

.method public final aFf()Z
    .locals 2

    .prologue
    .line 1068
    const-string v0, "networkOnly"

    invoke-virtual {p0}, Lhym;->aUl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final aFg()Z
    .locals 2

    .prologue
    .line 1074
    const-string v0, "embeddedOnly"

    invoke-virtual {p0}, Lhym;->aUl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final aFh()Z
    .locals 3

    .prologue
    .line 1039
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "debugS3Logging"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aFi()Z
    .locals 3

    .prologue
    .line 546
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "alwaysOnHotwordSettingPref"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aHP()Z
    .locals 3

    .prologue
    .line 486
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "profanityFilter"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aTA()Ljze;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 460
    iget-object v0, p0, Lhym;->mGStaticConfiguration:Lhye;

    invoke-virtual {v0}, Lhye;->aTA()Ljze;

    move-result-object v0

    return-object v0
.end method

.method public final aTC()Ljze;
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lhym;->mGStaticConfiguration:Lhye;

    invoke-virtual {v0}, Lhye;->aTC()Ljze;

    move-result-object v0

    return-object v0
.end method

.method public final aTE()Z
    .locals 3

    .prologue
    .line 338
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "audio-history-last"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized aTF()Z
    .locals 2

    .prologue
    .line 357
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "spoken-language-bcp-47"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final aTG()Ljava/util/Locale;
    .locals 2

    .prologue
    .line 370
    iget-object v0, p0, Lhym;->mGStaticConfiguration:Lhye;

    invoke-virtual {v0}, Lhye;->aTB()Ljze;

    move-result-object v0

    invoke-virtual {p0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgnq;->l(Ljze;Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public final aTH()Z
    .locals 3

    .prologue
    .line 375
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "spoken-language-default"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized aTI()Ljava/util/List;
    .locals 3

    .prologue
    .line 390
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhym;->bgq:Lchk;

    invoke-virtual {v0}, Lchk;->JW()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v1

    .line 392
    const-string v0, "additional-spoken-language-bcp-47"

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 393
    if-nez v0, :cond_1

    .line 394
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 395
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "additional-spoken-language-bcp-47"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    move-object v1, v0

    .line 397
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 399
    :goto_1
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 390
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public final aTJ()Ljava/lang/String;
    .locals 3

    .prologue
    .line 435
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "authTokenTypeRefreshed"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aTK()V
    .locals 2

    .prologue
    .line 480
    invoke-static {}, Lenu;->auQ()V

    .line 481
    iget-object v0, p0, Lhym;->mGStaticConfiguration:Lhye;

    iget-object v1, p0, Lhym;->mHttpHelper:Ldkx;

    invoke-virtual {v0, v1}, Lhye;->a(Ldkx;)V

    .line 482
    return-void
.end method

.method public final aTL()Z
    .locals 4

    .prologue
    .line 490
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "ttsMode"

    iget-object v2, p0, Lhym;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0877

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "handsFreeOnly"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final aTM()Z
    .locals 4

    .prologue
    .line 495
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "ttsMode"

    iget-object v2, p0, Lhym;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0877

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "never"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final aTN()Z
    .locals 3

    .prologue
    .line 504
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "bluetoothHeadset"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aTO()Z
    .locals 3

    .prologue
    .line 508
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "preferBluetoothSpeaker"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aTP()Z
    .locals 3

    .prologue
    .line 512
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "hotwordDetector"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aTQ()Z
    .locals 3

    .prologue
    .line 516
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "hotword_from_lock_screen"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aTR()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 621
    iget-object v1, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    const-string v2, "supportsAlwaysOn"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhym;->bgq:Lchk;

    invoke-virtual {v1}, Lchk;->FE()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final aTS()I
    .locals 3

    .prologue
    .line 656
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "hands_free_hotword_onboarding_notification_state"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final aTT()I
    .locals 3

    .prologue
    .line 676
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "hands_free_hotword_retraining_notification_state"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final aTU()Z
    .locals 3

    .prologue
    .line 691
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "hands_free_read_notifications_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aTV()Z
    .locals 3

    .prologue
    .line 695
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "headset_notifications_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aTW()I
    .locals 3

    .prologue
    .line 707
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "wired_headset_onboarding_notification_count"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final aTX()Z
    .locals 3

    .prologue
    .line 718
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "lockscreen_search_bluetooth"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aTY()Z
    .locals 3

    .prologue
    .line 722
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "lockscreen_search_headset"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aTZ()Z
    .locals 3

    .prologue
    .line 726
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "eyes_free_onboarding_headset_flow_shown"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aTz()V
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lhym;->mGStaticConfiguration:Lhye;

    invoke-virtual {v0}, Lhye;->aTz()V

    .line 432
    return-void
.end method

.method public final aUa()Z
    .locals 3

    .prologue
    .line 730
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "hands_free_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aUb()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 734
    sget-object v1, Lcgg;->aVt:Lcgg;

    invoke-virtual {v1}, Lcgg;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcgg;->aVv:Lcgg;

    invoke-virtual {v1}, Lcgg;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    const-string v2, "eyes_free_intro_tts_flow_started"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final aUc()V
    .locals 3

    .prologue
    .line 739
    sget-object v0, Lcgg;->aVt:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcgg;->aVv:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 740
    :cond_0
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "eyes_free_intro_tts_flow_started"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 742
    :cond_1
    return-void
.end method

.method public final aUd()Ljava/lang/String;
    .locals 3

    .prologue
    .line 786
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "car_mode_devices"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aUe()Ljava/lang/String;
    .locals 3

    .prologue
    .line 808
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "current-bluetooth-device"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aUg()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 957
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 960
    const-string v1, "hands_free_enabled"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 963
    const-string v1, "eyes_free_intro_tts_flow_started"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 966
    const-string v1, "hands_free_read_notifications_enabled"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 967
    const-string v1, "headset_notifications_enabled"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 970
    const-string v1, "hands_free_auto_driving_car_mode_enabled"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 971
    const-string v1, "hands_free_auto_nav_car_mode_enabled"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 973
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 974
    return-void
.end method

.method public final aUh()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 986
    invoke-virtual {p0}, Lhym;->aUg()V

    .line 988
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 991
    const-string v1, "eyes_free_onboarding_headset_flow_shown"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 992
    const-string v1, "wired_headset_onboarding_notification_count"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 995
    const-string v1, "lockscreen_search_bluetooth"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 996
    const-string v1, "lockscreen_search_headset"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 999
    iget-object v1, p0, Lhym;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 1000
    const-string v2, "com.google.android.googlequicksearchbox"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->clearPackagePreferredActivities(Ljava/lang/String;)V

    .line 1003
    const-string v1, "contact_upload_for_communication_actions_enabled"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1006
    invoke-direct {p0}, Lhym;->aUf()Lhyc;

    move-result-object v1

    .line 1007
    invoke-virtual {v1}, Lhyc;->aTy()V

    .line 1009
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1010
    return-void
.end method

.method public final aUi()Z
    .locals 3

    .prologue
    .line 1017
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "gmm_context_active"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aUj()V
    .locals 3

    .prologue
    .line 1026
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "gmm_context_active"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1027
    return-void
.end method

.method public final aUk()V
    .locals 2

    .prologue
    .line 1059
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "debugS3Server"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1060
    return-void
.end method

.method public final aUl()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1063
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "debugRecognitionEngineRestrict"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aUm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1091
    iget-object v0, p0, Lhym;->mGStaticConfiguration:Lhye;

    invoke-virtual {v0}, Lhye;->getTimestamp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aUn()J
    .locals 2

    .prologue
    .line 1095
    iget-object v0, p0, Lhym;->mGStaticConfiguration:Lhye;

    invoke-virtual {v0}, Lhye;->aTB()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->eNg:Lkaa;

    invoke-virtual {v0}, Lkaa;->bxB()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final aUo()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1102
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "s3SandboxOverride"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aUp()Ljava/util/List;
    .locals 6

    .prologue
    .line 1147
    const/4 v0, 0x1

    invoke-static {v0}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 1148
    invoke-virtual {p0}, Lhym;->aER()Ljze;

    move-result-object v0

    iget-object v0, v0, Ljze;->auq:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1149
    iget-object v0, p0, Lhym;->mSearchConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Mo()Ljava/lang/String;

    move-result-object v0

    .line 1150
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1151
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 1152
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1153
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1151
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1156
    :cond_1
    iget-object v0, p0, Lhym;->bgq:Lchk;

    invoke-virtual {v0}, Lchk;->GQ()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1157
    const-string v0, "chrome_prerender"

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1159
    :cond_2
    return-object v1
.end method

.method public final aUq()Z
    .locals 3

    .prologue
    .line 1172
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "hasEverUsedVoiceSearch"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aUr()V
    .locals 3

    .prologue
    .line 1176
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hasEverUsedVoiceSearch"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1177
    return-void
.end method

.method public final aUs()I
    .locals 3

    .prologue
    .line 1180
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "languagePacksAutoUpdate"

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final aUt()I
    .locals 3

    .prologue
    .line 1197
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "pref-voice-personalization-status"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final aUu()Z
    .locals 3

    .prologue
    .line 1230
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "consumed-ime-languages-for-multilang"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final b(Lhyo;)V
    .locals 1

    .prologue
    .line 1083
    iget-object v0, p0, Lhym;->mGStaticConfiguration:Lhye;

    invoke-virtual {v0, p1}, Lhye;->a(Lhyo;)V

    .line 1084
    return-void
.end method

.method public final b(Ljava/lang/String;III)V
    .locals 1

    .prologue
    .line 902
    invoke-direct {p0}, Lhym;->aUf()Lhyc;

    move-result-object v0

    .line 903
    invoke-virtual {v0, p1, p2, p3, p4}, Lhyc;->a(Ljava/lang/String;III)V

    .line 904
    invoke-virtual {v0}, Lhyc;->aTx()V

    .line 905
    return-void
.end method

.method public final declared-synchronized b(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 405
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v2}, Lchr;->Kt()Lcyg;

    move-result-object v3

    .line 410
    const-string v2, "spoken-language-bcp-47"

    const/4 v4, 0x0

    invoke-interface {v3, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v0

    .line 414
    :goto_0
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 415
    const-string v5, "additional-spoken-language-bcp-47"

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v5

    .line 417
    invoke-interface {v5, v4}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 421
    :goto_1
    if-nez v2, :cond_0

    if-eqz v0, :cond_1

    .line 422
    :cond_0
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "spoken-language-bcp-47"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "additional-spoken-language-bcp-47"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "spoken-language-default"

    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 428
    :cond_1
    monitor-exit p0

    return-void

    .line 405
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v2, v1

    goto :goto_0
.end method

.method public final fJ(Z)V
    .locals 2

    .prologue
    .line 627
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "supportsAlwaysOn"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 628
    return-void
.end method

.method public final gM(Z)V
    .locals 4

    .prologue
    .line 331
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "audio-history-last"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "audio-history-last-timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 335
    return-void
.end method

.method public final gN(Z)V
    .locals 2

    .prologue
    .line 554
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "alwaysOnHotwordSettingPref"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 555
    return-void
.end method

.method public final gO(Z)V
    .locals 2

    .prologue
    .line 1055
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "debugS3Logging"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1056
    return-void
.end method

.method public final gP(Z)V
    .locals 2

    .prologue
    .line 1226
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "consumed-ime-languages-for-multilang"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1227
    return-void
.end method

.method public final getHotwordConfigMap()Ljava/util/Map;
    .locals 1

    .prologue
    .line 1079
    iget-object v0, p0, Lhym;->bgq:Lchk;

    invoke-virtual {v0}, Lchk;->getHotwordConfigMap()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final ha(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 637
    iget-object v0, p0, Lhym;->mSearchSettings:Lcke;

    invoke-interface {v0, p1}, Lcke;->ha(Ljava/lang/String;)V

    .line 638
    return-void
.end method

.method public final declared-synchronized i(Ljava/lang/String;[B)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 586
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 605
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 590
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lhym;->Wy()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 594
    if-nez p2, :cond_2

    .line 595
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "speaker_model_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 586
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 597
    :cond_2
    const/4 v1, 0x0

    :try_start_2
    invoke-static {p2, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 598
    iget-object v2, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v2}, Lchr;->Kt()Lcyg;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "speaker_model_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 602
    iget-object v1, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "voiceEverywhereEnabled"

    invoke-virtual {p0}, Lhym;->aTR()Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v0, 0x1

    :cond_3
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final iG(Ljava/lang/String;)[B
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 566
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 579
    :cond_0
    :goto_0
    return-object v0

    .line 570
    :cond_1
    invoke-direct {p0}, Lhym;->Wy()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 574
    iget-object v1, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "speaker_model_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 576
    if-eqz v1, :cond_0

    .line 579
    const/4 v0, 0x0

    invoke-static {v1, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    goto :goto_0
.end method

.method public final l(Ljze;)V
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lhym;->mGStaticConfiguration:Lhye;

    invoke-virtual {v0, p1}, Lhye;->l(Ljze;)V

    .line 470
    return-void
.end method

.method public final lA(I)V
    .locals 2

    .prologue
    .line 666
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hands_free_hotword_onboarding_notification_state"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 668
    return-void
.end method

.method public final lB(I)V
    .locals 2

    .prologue
    .line 686
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hands_free_hotword_retraining_notification_state"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 688
    return-void
.end method

.method public final lC(I)V
    .locals 2

    .prologue
    .line 711
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "wired_headset_onboarding_notification_count"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 712
    return-void
.end method

.method public final lD(I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1185
    if-eqz p1, :cond_0

    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 1188
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "languagePacksAutoUpdate"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1189
    return-void

    .line 1185
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final lE(I)V
    .locals 2

    .prologue
    .line 1201
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "pref-voice-personalization-status"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1204
    return-void
.end method

.method final m(Ljze;)V
    .locals 4

    .prologue
    .line 314
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    .line 315
    const-string v1, "spoken-language-bcp-47"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 316
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    .line 317
    if-eqz v1, :cond_1

    invoke-static {p1, v1}, Lgnq;->a(Ljze;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 319
    invoke-static {v2, p1}, Lgnq;->a(Ljava/lang/String;Ljze;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 322
    const-string v2, "spoken-language-default"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eq v1, v2, :cond_0

    .line 323
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "spoken-language-default"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 326
    :cond_1
    invoke-direct {p0, p1}, Lhym;->n(Ljze;)Ljava/lang/String;

    goto :goto_0
.end method

.method public final oC(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 526
    iget-object v1, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v1}, Lchr;->Kt()Lcyg;

    move-result-object v1

    const-string v2, "voiceEverywhereEnabled"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lhym;->oD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final oD(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 531
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lhym;->Wy()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "speaker_model_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final oE(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 748
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "adaptive_tts_%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final oF(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 763
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "last_adaptive_tts_time_%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final oG(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 795
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "car_mode_devices"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 796
    return-void
.end method

.method public final oH(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 800
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "current-bluetooth-device"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 801
    return-void
.end method

.method public final oI(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 813
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const-string v1, "current-bluetooth-device"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 815
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 825
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "current-bluetooth-device"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 826
    return-void
.end method

.method public final oJ(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 844
    invoke-direct {p0}, Lhym;->aUf()Lhyc;

    move-result-object v0

    .line 845
    invoke-virtual {v0, p1}, Lhyc;->oA(Ljava/lang/String;)Lhyd;

    move-result-object v0

    .line 846
    if-eqz v0, :cond_0

    iget v0, v0, Lhyd;->AUDIO:I

    .line 847
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x64

    goto :goto_0
.end method

.method public final oK(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 858
    invoke-direct {p0}, Lhym;->aUf()Lhyc;

    move-result-object v0

    .line 859
    invoke-virtual {v0, p1}, Lhyc;->oA(Ljava/lang/String;)Lhyd;

    move-result-object v0

    .line 860
    if-eqz v0, :cond_0

    iget v0, v0, Lhyd;->duA:I

    .line 861
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final oL(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 870
    invoke-direct {p0}, Lhym;->aUf()Lhyc;

    move-result-object v0

    .line 871
    invoke-virtual {v0, p1}, Lhyc;->oA(Ljava/lang/String;)Lhyd;

    move-result-object v0

    .line 872
    if-eqz v0, :cond_0

    iget v0, v0, Lhyd;->duB:I

    .line 873
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final oM(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 922
    invoke-direct {p0}, Lhym;->aUf()Lhyc;

    move-result-object v0

    .line 923
    invoke-virtual {v0, p1}, Lhyc;->oy(Ljava/lang/String;)V

    .line 924
    invoke-virtual {v0}, Lhyc;->aTx()V

    .line 925
    return-void
.end method

.method public final oN(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 932
    invoke-direct {p0}, Lhym;->aUf()Lhyc;

    move-result-object v0

    .line 933
    invoke-virtual {v0, p1}, Lhyc;->oz(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final oO(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 951
    invoke-direct {p0}, Lhym;->aUf()Lhyc;

    move-result-object v0

    .line 952
    invoke-virtual {v0, p1}, Lhyc;->oA(Ljava/lang/String;)Lhyd;

    move-result-object v0

    .line 953
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final oP(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1034
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "debugS3Server"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1035
    return-void
.end method

.method public final ow(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 913
    invoke-direct {p0}, Lhym;->aUf()Lhyc;

    move-result-object v0

    .line 914
    invoke-virtual {v0, p1}, Lhyc;->ow(Ljava/lang/String;)V

    .line 915
    invoke-virtual {v0}, Lhyc;->aTx()V

    .line 916
    return-void
.end method

.method public final ox(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 940
    invoke-direct {p0}, Lhym;->aUf()Lhyc;

    move-result-object v0

    .line 941
    invoke-virtual {v0, p1}, Lhyc;->ox(Ljava/lang/String;)V

    .line 942
    invoke-virtual {v0}, Lhyc;->aTx()V

    .line 943
    return-void
.end method

.method public final t(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 775
    iget-object v0, p0, Lhym;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_adaptive_tts_time_%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 778
    return-void
.end method
