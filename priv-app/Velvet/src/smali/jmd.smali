.class public final Ljmd;
.super Ljsl;
.source "PG"


# static fields
.field public static final etp:Ljsm;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private ajk:Ljava/lang/String;

.field private etq:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 7659
    const/16 v0, 0xb

    const-class v1, Ljmd;

    const v2, 0xc2144fa

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljmd;->etp:Ljsm;

    .line 7665
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7738
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 7739
    const/4 v0, 0x0

    iput v0, p0, Ljmd;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljmd;->ajk:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljmd;->etq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljmd;->agq:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljmd;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljmd;->eCz:I

    .line 7740
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 7652
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljmd;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmd;->ajk:Ljava/lang/String;

    iget v0, p0, Ljmd;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmd;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmd;->etq:Ljava/lang/String;

    iget v0, p0, Ljmd;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmd;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmd;->agq:Ljava/lang/String;

    iget v0, p0, Ljmd;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljmd;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 7755
    iget v0, p0, Ljmd;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 7756
    const/4 v0, 0x1

    iget-object v1, p0, Ljmd;->ajk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 7758
    :cond_0
    iget v0, p0, Ljmd;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 7759
    const/4 v0, 0x2

    iget-object v1, p0, Ljmd;->etq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 7761
    :cond_1
    iget v0, p0, Ljmd;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 7762
    const/4 v0, 0x3

    iget-object v1, p0, Ljmd;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 7764
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 7765
    return-void
.end method

.method public final bpC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7697
    iget-object v0, p0, Ljmd;->etq:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7719
    iget-object v0, p0, Ljmd;->agq:Ljava/lang/String;

    return-object v0
.end method

.method public final getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7675
    iget-object v0, p0, Ljmd;->ajk:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 7769
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 7770
    iget v1, p0, Ljmd;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 7771
    const/4 v1, 0x1

    iget-object v2, p0, Ljmd;->ajk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7774
    :cond_0
    iget v1, p0, Ljmd;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 7775
    const/4 v1, 0x2

    iget-object v2, p0, Ljmd;->etq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7778
    :cond_1
    iget v1, p0, Ljmd;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 7779
    const/4 v1, 0x3

    iget-object v2, p0, Ljmd;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7782
    :cond_2
    return v0
.end method

.method public final wM(Ljava/lang/String;)Ljmd;
    .locals 1

    .prologue
    .line 7678
    if-nez p1, :cond_0

    .line 7679
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7681
    :cond_0
    iput-object p1, p0, Ljmd;->ajk:Ljava/lang/String;

    .line 7682
    iget v0, p0, Ljmd;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmd;->aez:I

    .line 7683
    return-object p0
.end method

.method public final wN(Ljava/lang/String;)Ljmd;
    .locals 1

    .prologue
    .line 7700
    if-nez p1, :cond_0

    .line 7701
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7703
    :cond_0
    iput-object p1, p0, Ljmd;->etq:Ljava/lang/String;

    .line 7704
    iget v0, p0, Ljmd;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmd;->aez:I

    .line 7705
    return-object p0
.end method

.method public final wO(Ljava/lang/String;)Ljmd;
    .locals 1

    .prologue
    .line 7722
    if-nez p1, :cond_0

    .line 7723
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7725
    :cond_0
    iput-object p1, p0, Ljmd;->agq:Ljava/lang/String;

    .line 7726
    iget v0, p0, Ljmd;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljmd;->aez:I

    .line 7727
    return-object p0
.end method
