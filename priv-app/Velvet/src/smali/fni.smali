.class final Lfni;
.super Lenp;
.source "PG"


# instance fields
.field private synthetic cwS:Lfnh;

.field private final dz:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lfnh;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 206
    iput-object p1, p0, Lfni;->cwS:Lfnh;

    .line 207
    const-string v0, "ImageLoader"

    const/4 v1, 0x0

    new-array v1, v1, [I

    invoke-direct {p0, v0, p2, p3, v1}, Lenp;-><init>(Ljava/lang/String;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;[I)V

    .line 208
    iput-object p4, p0, Lfni;->dz:Landroid/net/Uri;

    .line 209
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 203
    iget-object v0, p0, Lfni;->cwS:Lfnh;

    iget-object v0, v0, Lfnh;->mNowRemoteClient:Lfml;

    iget-object v1, p0, Lfni;->dz:Landroid/net/Uri;

    iget-object v2, p0, Lfni;->cwS:Lfnh;

    iget-boolean v2, v2, Lfnh;->cwP:Z

    invoke-virtual {v0, v1, v2}, Lfml;->e(Landroid/net/Uri;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lfni;->cwS:Lfnh;

    iget-object v2, v2, Lfnh;->mResources:Landroid/content/res/Resources;

    invoke-direct {v0, v2, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 203
    check-cast p1, Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lfni;->cwS:Lfnh;

    iget-object v2, p0, Lfni;->dz:Landroid/net/Uri;

    iget-object v3, v1, Lfnh;->cwQ:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v0, v1, Lfnh;->cwR:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnj;

    if-eqz v0, :cond_1

    iget-object v4, v0, Lfnj;->dK:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, v0, Lfnj;->cwT:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lemy;

    invoke-interface {v0, p1}, Lemy;->aj(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    iget-object v0, v1, Lfnh;->cwR:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return-void
.end method
