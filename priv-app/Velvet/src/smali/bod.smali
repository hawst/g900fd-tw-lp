.class final Lbod;
.super Lbnk;


# instance fields
.field private final aGA:Lbhh;

.field private synthetic aGz:Lbnv;


# direct methods
.method public constructor <init>(Lbnv;Lbhh;)V
    .locals 0

    iput-object p1, p0, Lbod;->aGz:Lbnv;

    invoke-direct {p0}, Lbnk;-><init>()V

    iput-object p2, p0, Lbod;->aGA:Lbhh;

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V
    .locals 5

    const/4 v0, 0x3

    const-string v1, "PeopleService"

    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Avatar callback: status="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " resolution="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pfd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v0, 0x0

    invoke-static {p1, v0, p2}, Lbnv;->b(ILjava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    iget-object v1, p0, Lbod;->aGz:Lbnv;

    new-instance v2, Lbnx;

    iget-object v3, p0, Lbod;->aGz:Lbnv;

    iget-object v4, p0, Lbod;->aGA:Lbhh;

    invoke-direct {v2, v3, v4, v0, p3}, Lbnx;-><init>(Lbnv;Lbhh;Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;)V

    invoke-virtual {v1, v2}, Lbnv;->b(Lbit;)V

    return-void
.end method
