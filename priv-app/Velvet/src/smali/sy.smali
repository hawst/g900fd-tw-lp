.class public final Lsy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public AY:I

.field public AZ:I

.field public Ba:Landroid/view/View;

.field public Bb:I

.field public Bc:I

.field public Bd:J


# direct methods
.method public constructor <init>(Landroid/view/View;Lwq;)V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 3249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3242
    iput v0, p0, Lsy;->Bb:I

    .line 3243
    iput v0, p0, Lsy;->Bc:I

    .line 3250
    iput-object p1, p0, Lsy;->Ba:Landroid/view/View;

    .line 3251
    iget v0, p2, Lwq;->Bb:I

    iput v0, p0, Lsy;->Bb:I

    .line 3252
    iget v0, p2, Lwq;->Bc:I

    iput v0, p0, Lsy;->Bc:I

    .line 3253
    iget v0, p2, Lwq;->AY:I

    iput v0, p0, Lsy;->AY:I

    .line 3254
    iget v0, p2, Lwq;->AZ:I

    iput v0, p0, Lsy;->AZ:I

    .line 3255
    iget-wide v0, p2, Lwq;->Bd:J

    iput-wide v0, p0, Lsy;->Bd:J

    .line 3256
    iget-wide v0, p2, Lwq;->JA:J

    .line 3257
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3261
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "Cell[view="

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lsy;->Ba:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lsy;->Bb:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lsy;->Bc:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lsy;->Ba:Landroid/view/View;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method
