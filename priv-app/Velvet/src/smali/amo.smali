.class public final Lamo;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private aff:J

.field private afg:J

.field private afh:Ljava/lang/String;

.field private afi:J

.field private afj:J

.field private afk:Ljava/lang/String;

.field private afl:Z

.field private afm:I

.field private afn:I

.field private afo:I

.field private afp:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 255
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 256
    iput v1, p0, Lamo;->aez:I

    iput-wide v2, p0, Lamo;->aff:J

    iput-wide v2, p0, Lamo;->afg:J

    const-string v0, ""

    iput-object v0, p0, Lamo;->afh:Ljava/lang/String;

    iput-wide v2, p0, Lamo;->afi:J

    iput-wide v2, p0, Lamo;->afj:J

    const-string v0, ""

    iput-object v0, p0, Lamo;->afk:Ljava/lang/String;

    iput-boolean v1, p0, Lamo;->afl:Z

    iput v1, p0, Lamo;->afm:I

    iput v1, p0, Lamo;->afn:I

    iput v1, p0, Lamo;->afo:I

    iput-wide v2, p0, Lamo;->afp:J

    const/4 v0, 0x0

    iput-object v0, p0, Lamo;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lamo;->eCz:I

    .line 257
    return-void
.end method


# virtual methods
.method public final V(Ljava/lang/String;)Lamo;
    .locals 1

    .prologue
    .line 84
    if-nez p1, :cond_0

    .line 85
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 87
    :cond_0
    iput-object p1, p0, Lamo;->afh:Ljava/lang/String;

    .line 88
    iget v0, p0, Lamo;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lamo;->aez:I

    .line 89
    return-object p0
.end method

.method public final W(Ljava/lang/String;)Lamo;
    .locals 1

    .prologue
    .line 144
    if-nez p1, :cond_0

    .line 145
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 147
    :cond_0
    iput-object p1, p0, Lamo;->afk:Ljava/lang/String;

    .line 148
    iget v0, p0, Lamo;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lamo;->aez:I

    .line 149
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lamo;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lamo;->aff:J

    iget v0, p0, Lamo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamo;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lamo;->afg:J

    iget v0, p0, Lamo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamo;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lamo;->afh:Ljava/lang/String;

    iget v0, p0, Lamo;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lamo;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lamo;->afi:J

    iget v0, p0, Lamo;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lamo;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lamo;->afj:J

    iget v0, p0, Lamo;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lamo;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lamo;->afk:Ljava/lang/String;

    iget v0, p0, Lamo;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lamo;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lamo;->afm:I

    iget v0, p0, Lamo;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lamo;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lamo;->afn:I

    iget v0, p0, Lamo;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lamo;->aez:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lamo;->afl:Z

    iget v0, p0, Lamo;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lamo;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_0

    :pswitch_1
    iput v0, p0, Lamo;->afo:I

    iget v0, p0, Lamo;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lamo;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lamo;->afp:J

    iget v0, p0, Lamo;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lamo;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 280
    iget v0, p0, Lamo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 281
    const/4 v0, 0x1

    iget-wide v2, p0, Lamo;->aff:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->g(IJ)V

    .line 283
    :cond_0
    iget v0, p0, Lamo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 284
    const/4 v0, 0x2

    iget-wide v2, p0, Lamo;->afg:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 286
    :cond_1
    iget v0, p0, Lamo;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 287
    const/4 v0, 0x3

    iget-object v1, p0, Lamo;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 289
    :cond_2
    iget v0, p0, Lamo;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 290
    const/4 v0, 0x4

    iget-wide v2, p0, Lamo;->afi:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 292
    :cond_3
    iget v0, p0, Lamo;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 293
    const/4 v0, 0x5

    iget-wide v2, p0, Lamo;->afj:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 295
    :cond_4
    iget v0, p0, Lamo;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 296
    const/4 v0, 0x6

    iget-object v1, p0, Lamo;->afk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 298
    :cond_5
    iget v0, p0, Lamo;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_6

    .line 299
    const/4 v0, 0x7

    iget v1, p0, Lamo;->afm:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 301
    :cond_6
    iget v0, p0, Lamo;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_7

    .line 302
    const/16 v0, 0x8

    iget v1, p0, Lamo;->afn:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bs(II)V

    .line 304
    :cond_7
    iget v0, p0, Lamo;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_8

    .line 305
    const/16 v0, 0x9

    iget-boolean v1, p0, Lamo;->afl:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 307
    :cond_8
    iget v0, p0, Lamo;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_9

    .line 308
    const/16 v0, 0xa

    iget v1, p0, Lamo;->afo:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 310
    :cond_9
    iget v0, p0, Lamo;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_a

    .line 311
    const/16 v0, 0xb

    iget-wide v2, p0, Lamo;->afp:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 313
    :cond_a
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 314
    return-void
.end method

.method public final aJ(Z)Lamo;
    .locals 1

    .prologue
    .line 166
    iput-boolean p1, p0, Lamo;->afl:Z

    .line 167
    iget v0, p0, Lamo;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lamo;->aez:I

    .line 168
    return-object p0
.end method

.method public final cb(I)Lamo;
    .locals 1

    .prologue
    .line 185
    iput p1, p0, Lamo;->afm:I

    .line 186
    iget v0, p0, Lamo;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lamo;->aez:I

    .line 187
    return-object p0
.end method

.method public final cc(I)Lamo;
    .locals 1

    .prologue
    .line 204
    iput p1, p0, Lamo;->afn:I

    .line 205
    iget v0, p0, Lamo;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lamo;->aez:I

    .line 206
    return-object p0
.end method

.method public final cd(I)Lamo;
    .locals 1

    .prologue
    .line 223
    iput p1, p0, Lamo;->afo:I

    .line 224
    iget v0, p0, Lamo;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lamo;->aez:I

    .line 225
    return-object p0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lamo;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 318
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 319
    iget v1, p0, Lamo;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 320
    const/4 v1, 0x1

    iget-wide v2, p0, Lamo;->aff:J

    invoke-static {v1, v2, v3}, Ljsj;->j(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 323
    :cond_0
    iget v1, p0, Lamo;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 324
    const/4 v1, 0x2

    iget-wide v2, p0, Lamo;->afg:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 327
    :cond_1
    iget v1, p0, Lamo;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 328
    const/4 v1, 0x3

    iget-object v2, p0, Lamo;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 331
    :cond_2
    iget v1, p0, Lamo;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 332
    const/4 v1, 0x4

    iget-wide v2, p0, Lamo;->afi:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 335
    :cond_3
    iget v1, p0, Lamo;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 336
    const/4 v1, 0x5

    iget-wide v2, p0, Lamo;->afj:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 339
    :cond_4
    iget v1, p0, Lamo;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 340
    const/4 v1, 0x6

    iget-object v2, p0, Lamo;->afk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 343
    :cond_5
    iget v1, p0, Lamo;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_6

    .line 344
    const/4 v1, 0x7

    iget v2, p0, Lamo;->afm:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 347
    :cond_6
    iget v1, p0, Lamo;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_7

    .line 348
    const/16 v1, 0x8

    iget v2, p0, Lamo;->afn:I

    invoke-static {v1, v2}, Ljsj;->bw(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 351
    :cond_7
    iget v1, p0, Lamo;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_8

    .line 352
    const/16 v1, 0x9

    iget-boolean v2, p0, Lamo;->afl:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 355
    :cond_8
    iget v1, p0, Lamo;->aez:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_9

    .line 356
    const/16 v1, 0xa

    iget v2, p0, Lamo;->afo:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 359
    :cond_9
    iget v1, p0, Lamo;->aez:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_a

    .line 360
    const/16 v1, 0xb

    iget-wide v2, p0, Lamo;->afp:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 363
    :cond_a
    return v0
.end method

.method public final nh()J
    .locals 2

    .prologue
    .line 43
    iget-wide v0, p0, Lamo;->aff:J

    return-wide v0
.end method

.method public final ni()Z
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lamo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nj()J
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lamo;->afg:J

    return-wide v0
.end method

.method public final nk()J
    .locals 2

    .prologue
    .line 103
    iget-wide v0, p0, Lamo;->afi:J

    return-wide v0
.end method

.method public final nl()Z
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lamo;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nm()J
    .locals 2

    .prologue
    .line 122
    iget-wide v0, p0, Lamo;->afj:J

    return-wide v0
.end method

.method public final nn()Z
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lamo;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final no()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lamo;->afk:Ljava/lang/String;

    return-object v0
.end method

.method public final np()Z
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lamo;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nq()Z
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lamo;->afl:Z

    return v0
.end method

.method public final nr()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lamo;->afn:I

    return v0
.end method

.method public final ns()I
    .locals 1

    .prologue
    .line 220
    iget v0, p0, Lamo;->afo:I

    return v0
.end method

.method public final nt()J
    .locals 2

    .prologue
    .line 239
    iget-wide v0, p0, Lamo;->afp:J

    return-wide v0
.end method

.method public final r(J)Lamo;
    .locals 1

    .prologue
    .line 46
    iput-wide p1, p0, Lamo;->aff:J

    .line 47
    iget v0, p0, Lamo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamo;->aez:I

    .line 48
    return-object p0
.end method

.method public final s(J)Lamo;
    .locals 1

    .prologue
    .line 65
    iput-wide p1, p0, Lamo;->afg:J

    .line 66
    iget v0, p0, Lamo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamo;->aez:I

    .line 67
    return-object p0
.end method

.method public final t(J)Lamo;
    .locals 1

    .prologue
    .line 106
    iput-wide p1, p0, Lamo;->afi:J

    .line 107
    iget v0, p0, Lamo;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lamo;->aez:I

    .line 108
    return-object p0
.end method

.method public final u(J)Lamo;
    .locals 1

    .prologue
    .line 125
    iput-wide p1, p0, Lamo;->afj:J

    .line 126
    iget v0, p0, Lamo;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lamo;->aez:I

    .line 127
    return-object p0
.end method

.method public final v(J)Lamo;
    .locals 1

    .prologue
    .line 242
    iput-wide p1, p0, Lamo;->afp:J

    .line 243
    iget v0, p0, Lamo;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lamo;->aez:I

    .line 244
    return-object p0
.end method
