.class final Lcub;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final biy:Z


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    sget v0, Lesp;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcub;->biy:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcub;->mContentResolver:Landroid/content/ContentResolver;

    .line 48
    return-void
.end method

.method private a(Landroid/content/res/Resources;Ljava/lang/String;)Landroid/util/Pair;
    .locals 9

    .prologue
    const-wide/16 v6, -0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 77
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "directory"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 81
    iget-object v0, p0, Lcub;->mContentResolver:Landroid/content/ContentResolver;

    sget-boolean v2, Lcub;->biy:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v8

    const-string v3, "contact_last_updated_timestamp"

    aput-object v3, v2, v5

    :goto_0
    move-object v3, p2

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 87
    if-nez v4, :cond_1

    .line 88
    const-string v0, "IcingCPHelper"

    const-string v1, "Could not query ContactsProvider; disabled? Wiping local DB."

    new-array v2, v8, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 89
    new-instance v0, Landroid/util/Pair;

    new-instance v1, Lcui;

    invoke-direct {v1}, Lcui;-><init>()V

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 114
    :goto_1
    return-object v0

    .line 81
    :cond_0
    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v8

    goto :goto_0

    .line 94
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 97
    :try_start_0
    const-string v0, "_id"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-wide v2, v6

    .line 98
    :goto_2
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 99
    invoke-interface {v4, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 100
    sget-boolean v0, Lcub;->biy:Z

    if-eqz v0, :cond_4

    .line 101
    const-string v0, "contact_last_updated_timestamp"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :goto_3
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 104
    :goto_4
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-wide v2, v0

    .line 105
    goto :goto_2

    .line 101
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_3

    .line 107
    :cond_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 112
    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 114
    new-instance v0, Landroid/util/Pair;

    new-instance v1, Lcuc;

    iget-object v4, p0, Lcub;->mContentResolver:Landroid/content/ContentResolver;

    invoke-direct {v1, v4, p1, v5}, Lcuc;-><init>(Landroid/content/ContentResolver;Landroid/content/res/Resources;Ljava/util/List;)V

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 107
    :catchall_0
    move-exception v0

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_4
    move-wide v0, v2

    goto :goto_4
.end method

.method static synthetic i(Landroid/database/Cursor;)J
    .locals 2

    .prologue
    .line 36
    const-wide/16 v0, 0x0

    return-wide v0
.end method


# virtual methods
.method final a(JLjava/util/Set;)J
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 128
    sget-boolean v0, Lcub;->biy:Z

    const-string v1, "Delta API not supported"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 129
    const-string v3, "contact_deleted_timestamp>?"

    .line 130
    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    .line 135
    :try_start_0
    iget-object v0, p0, Lcub;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/ContactsContract$DeletedContacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "contact_id"

    aput-object v7, v2, v5

    const/4 v5, 0x1

    const-string v7, "contact_deleted_timestamp"

    aput-object v7, v2, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 145
    if-nez v1, :cond_1

    .line 146
    :try_start_1
    const-string v0, "IcingCPHelper"

    const-string v2, "Could not fetch deleted contacts - no contacts provider present?"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x4

    invoke-static {v4, v0, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 147
    if-eqz v1, :cond_0

    .line 164
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 167
    :cond_0
    :goto_0
    return-wide p1

    .line 150
    :cond_1
    :try_start_2
    const-string v0, "contact_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 152
    const-string v2, "contact_deleted_timestamp"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 154
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 157
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {p3, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 159
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {p1, p2, v4, v5}, Ljava/lang/Math;->max(JJ)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-wide p1

    goto :goto_1

    .line 163
    :cond_2
    if-eqz v1, :cond_0

    .line 164
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_3

    .line 164
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 163
    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method public final a(Landroid/content/res/Resources;J)Landroid/util/Pair;
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 68
    sget-boolean v0, Lcub;->biy:Z

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "contact_last_updated_timestamp>"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 71
    invoke-direct {p0, p1, v0}, Lcub;->a(Landroid/content/res/Resources;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/res/Resources;Ljava/util/Collection;)Lcts;
    .locals 1

    .prologue
    .line 61
    const-string v0, "_id"

    invoke-static {v0, p2}, Lcto;->a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 62
    invoke-direct {p0, p1, v0}, Lcub;->a(Landroid/content/res/Resources;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcts;

    return-object v0
.end method

.method public final d(Landroid/content/res/Resources;)Landroid/util/Pair;
    .locals 1

    .prologue
    .line 55
    const-string v0, ""

    invoke-direct {p0, p1, v0}, Lcub;->a(Landroid/content/res/Resources;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method
