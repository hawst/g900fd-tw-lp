.class final Lipy;
.super Liqa;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private dCD:Liqa;


# direct methods
.method constructor <init>(Liqa;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Liqa;-><init>()V

    .line 31
    iput-object p1, p0, Lipy;->dCD:Liqa;

    .line 32
    return-void
.end method


# virtual methods
.method public final aYf()Liqa;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lipy;->dCD:Liqa;

    invoke-virtual {v0}, Liqa;->aYf()Liqa;

    move-result-object v0

    invoke-virtual {v0}, Liqa;->aYg()Liqa;

    move-result-object v0

    return-object v0
.end method

.method public final aYg()Liqa;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lipy;->dCD:Liqa;

    invoke-virtual {v0}, Liqa;->aYg()Liqa;

    move-result-object v0

    return-object v0
.end method

.method public final aYh()Liqa;
    .locals 0

    .prologue
    .line 58
    return-object p0
.end method

.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 35
    if-ne p1, p2, :cond_0

    .line 36
    const/4 v0, 0x0

    .line 44
    :goto_0
    return v0

    .line 38
    :cond_0
    if-nez p1, :cond_1

    .line 39
    const/4 v0, 0x1

    goto :goto_0

    .line 41
    :cond_1
    if-nez p2, :cond_2

    .line 42
    const/4 v0, -0x1

    goto :goto_0

    .line 44
    :cond_2
    iget-object v0, p0, Lipy;->dCD:Liqa;

    invoke-virtual {v0, p1, p2}, Liqa;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 62
    if-ne p1, p0, :cond_0

    .line 63
    const/4 v0, 0x1

    .line 69
    :goto_0
    return v0

    .line 65
    :cond_0
    instance-of v0, p1, Lipy;

    if-eqz v0, :cond_1

    .line 66
    check-cast p1, Lipy;

    .line 67
    iget-object v0, p0, Lipy;->dCD:Liqa;

    iget-object v1, p1, Lipy;->dCD:Liqa;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 69
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lipy;->dCD:Liqa;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, -0x36e88db8    # -620324.5f

    xor-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lipy;->dCD:Liqa;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".nullsLast()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
