.class final Ldjj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldiq;


# instance fields
.field private final Yq:Landroid/content/pm/ActivityInfo;

.field private final aVO:Z

.field private final bAa:Landroid/app/SearchableInfo;

.field private final bAb:Ldjo;

.field private final bAc:Ljava/lang/String;

.field private final bAd:Landroid/net/Uri;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bAe:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final buX:Z

.field private hO:Z

.field private final mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/pm/ActivityInfo;Lcjs;Landroid/app/SearchableInfo;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ldjo;)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p4, p0, Ldjj;->mName:Ljava/lang/String;

    .line 69
    iput-object p5, p0, Ldjj;->bAc:Ljava/lang/String;

    .line 70
    iput-object p3, p0, Ldjj;->bAa:Landroid/app/SearchableInfo;

    .line 71
    iput-object p6, p0, Ldjj;->bAd:Landroid/net/Uri;

    .line 72
    iget-object v0, p0, Ldjj;->bAd:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Ldjj;->bAe:Ljava/lang/String;

    .line 73
    iput-object p1, p0, Ldjj;->Yq:Landroid/content/pm/ActivityInfo;

    .line 74
    iput-object p7, p0, Ldjj;->bAb:Ldjo;

    .line 75
    iget-object v0, p0, Ldjj;->bAe:Ljava/lang/String;

    iget-object v1, p0, Ldjj;->mName:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Lcjs;->I(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldjj;->aVO:Z

    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldjj;->hO:Z

    .line 77
    iget-object v0, p0, Ldjj;->bAe:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcjs;->gH(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldjj;->buX:Z

    .line 78
    return-void

    .line 72
    :cond_0
    iget-object v0, p0, Ldjj;->bAd:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/app/SearchableInfo;Ljava/lang/String;ILandroid/os/CancellationSignal;)Landroid/util/Pair;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 193
    iget-object v0, p0, Ldjj;->bAd:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 228
    :goto_0
    return-object v7

    .line 194
    :cond_0
    iget-object v0, p0, Ldjj;->bAd:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 197
    invoke-virtual {p2}, Landroid/app/SearchableInfo;->getSuggestSelection()Ljava/lang/String;

    move-result-object v3

    .line 200
    if-eqz v3, :cond_1

    .line 201
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p3, v4, v1

    .line 206
    :goto_1
    const-string v1, "limit"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 208
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 210
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireUnstableContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    .line 220
    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v6, p5

    :try_start_0
    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 228
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v7

    goto :goto_0

    .line 203
    :cond_1
    invoke-virtual {v0, p3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-object v4, v7

    goto :goto_1

    .line 221
    :catch_0
    move-exception v1

    .line 222
    invoke-virtual {p5}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 223
    throw v1

    .line 225
    :cond_2
    invoke-static {v7, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v7

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/shared/search/Query;ILandroid/os/CancellationSignal;)Ldef;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 130
    .line 131
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v3

    .line 133
    :try_start_0
    iget-object v2, p0, Ldjj;->bAa:Landroid/app/SearchableInfo;

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Ldjj;->a(Landroid/content/Context;Landroid/app/SearchableInfo;Ljava/lang/String;ILandroid/os/CancellationSignal;)Landroid/util/Pair;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 134
    :try_start_1
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    .line 135
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 137
    :cond_0
    new-instance v1, Ldiz;

    iget-object v4, p0, Ldjj;->bAb:Ldjo;

    if-eqz p0, :cond_1

    const-string v5, "content://browser/bookmarks/search_suggest_query"

    invoke-interface {p0}, Ldiq;->ach()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, v4, Ldjo;->mGsaConfig:Lchk;

    invoke-virtual {v5}, Lchk;->Ir()I

    move-result v5

    new-instance v6, Ldih;

    iget-object v7, v4, Ldjo;->mContext:Landroid/content/Context;

    iget-object v4, v4, Ldjo;->mConfig:Lcjs;

    invoke-direct {v6, v7, v4, v5}, Ldih;-><init>(Landroid/content/Context;Lcjs;I)V

    :cond_1
    :goto_0
    invoke-direct {v1, p0, p2, v0, v6}, Ldiz;-><init>(Ldiq;Lcom/google/android/shared/search/Query;Landroid/database/Cursor;Ldjn;)V

    invoke-virtual {v1, p3}, Ldiz;->fU(I)Ldef;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 145
    if-eqz v2, :cond_2

    .line 146
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/io/Closeable;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 147
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v0, :cond_2

    .line 148
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/content/ContentProviderClient;

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_2
    move-object v0, v1

    :goto_1
    return-object v0

    .line 137
    :cond_3
    :try_start_2
    invoke-interface {p0}, Ldiq;->abQ()Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v6, Ldin;->byS:Ldin;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 140
    :catch_0
    move-exception v0

    move-object v2, v6

    .line 141
    :goto_2
    :try_start_3
    const-string v1, "QSB.SearchableSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ldjj;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 142
    new-instance v1, Ldei;

    iget-object v0, p0, Ldjj;->mName:Ljava/lang/String;

    invoke-direct {v1, v0, p2}, Ldei;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 145
    if-eqz v2, :cond_4

    .line 146
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/io/Closeable;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 147
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v0, :cond_4

    .line 148
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/content/ContentProviderClient;

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_4
    move-object v0, v1

    goto :goto_1

    .line 145
    :catchall_0
    move-exception v0

    move-object v1, v0

    move-object v2, v6

    :goto_3
    if-eqz v2, :cond_5

    .line 146
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/io/Closeable;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 147
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v0, :cond_5

    .line 148
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/content/ContentProviderClient;

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_5
    throw v1

    .line 145
    :catchall_1
    move-exception v0

    move-object v1, v0

    goto :goto_3

    .line 140
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public final a(Letj;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 259
    const-string v0, "SearchableSource"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 260
    const-string v0, "name"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Ldjj;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 261
    const-string v0, "canonical name"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Ldjj;->bAc:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 262
    return-void
.end method

.method public final abJ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Ldjj;->bAa:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSuggestIntentAction()Ljava/lang/String;

    move-result-object v0

    .line 239
    if-eqz v0, :cond_0

    .line 240
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "android.intent.action.SEARCH"

    goto :goto_0
.end method

.method public final abK()Ljava/lang/String;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Ldjj;->bAa:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSuggestIntentData()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final abL()I
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Ldjj;->Yq:Landroid/content/pm/ActivityInfo;

    iget v0, v0, Landroid/content/pm/ActivityInfo;->labelRes:I

    return v0
.end method

.method public final abM()I
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Ldjj;->bAa:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSettingsDescriptionId()I

    move-result v0

    return v0
.end method

.method public final abN()I
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Ldjj;->Yq:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v0}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    move-result v0

    return v0
.end method

.method public final abO()Z
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Ldjj;->buX:Z

    return v0
.end method

.method public final abP()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Ldjj;->aVO:Z

    return v0
.end method

.method public final abQ()Z
    .locals 2

    .prologue
    .line 266
    const-string v0, "com.android.contacts"

    iget-object v1, p0, Ldjj;->bAa:Landroid/app/SearchableInfo;

    invoke-virtual {v1}, Landroid/app/SearchableInfo;->getSuggestAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final ach()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Ldjj;->bAe:Ljava/lang/String;

    return-object v0
.end method

.method public final aci()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Ldjj;->bAa:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method public final acj()I
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Ldjj;->bAa:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSuggestThreshold()I

    move-result v0

    return v0
.end method

.method public final ack()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Ldjj;->bAa:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSuggestPackage()Ljava/lang/String;

    move-result-object v0

    .line 104
    if-eqz v0, :cond_0

    .line 108
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldjj;->bAa:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 272
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final getApplicationInfo()Landroid/content/pm/ApplicationInfo;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Ldjj;->Yq:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    return-object v0
.end method

.method public final getCanonicalName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Ldjj;->bAc:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Ldjj;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Ldjj;->Yq:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 166
    iget-boolean v0, p0, Ldjj;->hO:Z

    return v0
.end method

.method public final queryAfterZeroResults()Z
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Ldjj;->bAa:Landroid/app/SearchableInfo;

    invoke-virtual {v0}, Landroid/app/SearchableInfo;->queryAfterZeroResults()Z

    move-result v0

    return v0
.end method

.method public final setEnabled(Z)V
    .locals 0

    .prologue
    .line 171
    iput-boolean p1, p0, Ldjj;->hO:Z

    .line 172
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 254
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SearchableSource[name="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldjj;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", canonicalName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldjj;->bAc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
