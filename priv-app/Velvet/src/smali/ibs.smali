.class public final Libs;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final dkw:Lglm;

.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Lglm;Landroid/content/pm/PackageManager;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Libs;->dkw:Lglm;

    .line 48
    iput-object p2, p0, Libs;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 49
    return-void
.end method

.method private static a(Libu;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Libu;->dxs:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 188
    const-string v0, "android.intent.extra.EMAIL"

    iget-object v1, p0, Libu;->dxs:[Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 190
    :cond_0
    iget-object v0, p0, Libu;->dxt:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 197
    const-string v0, "android.intent.extra.SUBJECT"

    iget-object v1, p0, Libu;->dxt:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199
    :cond_1
    iget-object v0, p0, Libu;->dxu:Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    .line 200
    const-string v0, "android.intent.extra.TEXT"

    iget-object v1, p0, Libu;->dxu:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 202
    :cond_2
    iget-object v0, p0, Libu;->dxv:Landroid/net/Uri;

    if-eqz v0, :cond_3

    .line 203
    const-string v0, "android.intent.extra.STREAM"

    iget-object v1, p0, Libu;->dxv:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 205
    :cond_3
    return-void
.end method

.method public static aUJ()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 175
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 176
    const-string v1, "mailto"

    const-string v2, ""

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 177
    return-object v0
.end method

.method private b(Libu;ZLjava/lang/String;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 138
    invoke-virtual {p0, p2, p4}, Libs;->K(ZZ)Landroid/content/Intent;

    move-result-object v0

    .line 140
    const-string v1, "com.google.android.gm.extra.ACCOUNT"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    const-string v1, "account"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    invoke-static {p1, v0}, Libs;->a(Libu;Landroid/content/Intent;)V

    .line 143
    return-object v0
.end method


# virtual methods
.method public K(ZZ)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 102
    new-instance v2, Landroid/content/Intent;

    if-eqz p1, :cond_1

    const-string v0, "com.google.android.gm.action.AUTO_SEND"

    :goto_0
    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 104
    if-eqz p2, :cond_4

    .line 105
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "text/plain"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "com.google.android.voicesearch.SELF_NOTE"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "com.google.android.gm"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v3, p0, Libs;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v3, v0, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_3

    .line 108
    const-string v0, "com.google.android.gm"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 124
    :cond_0
    :goto_2
    const-string v0, "text/plain"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 125
    return-object v2

    .line 102
    :cond_1
    const-string v0, "android.intent.action.SEND"

    goto :goto_0

    :cond_2
    move v0, v1

    .line 105
    goto :goto_1

    .line 109
    :cond_3
    if-nez p1, :cond_0

    .line 116
    const-string v0, "com.google.android.voicesearch.SELF_NOTE"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    .line 121
    :cond_4
    const-string v0, "com.google.android.gm"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2
.end method

.method public final a(Libu;ZLjava/lang/String;Z)[Landroid/content/Intent;
    .locals 2

    .prologue
    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 92
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    iget-object v1, p1, Libu;->dxs:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 93
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1, p3, p4}, Libs;->b(Libu;ZLjava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, p3, p4}, Libs;->b(Libu;ZLjava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    invoke-static {}, Libs;->aUJ()Landroid/content/Intent;

    move-result-object v1

    invoke-static {p1, v1}, Libs;->a(Libu;Landroid/content/Intent;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Landroid/content/Intent;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/Intent;

    return-object v0
.end method
