.class public Lbch;
.super Ljava/lang/Object;


# instance fields
.field private final awS:Lbci;

.field private final awT:I

.field private synthetic awU:Lcom/google/android/gms/appdatasearch/SearchResults;


# direct methods
.method constructor <init>(Lcom/google/android/gms/appdatasearch/SearchResults;ILbci;)V
    .locals 0

    iput-object p1, p0, Lbch;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lbch;->awS:Lbci;

    iput p2, p0, Lbch;->awT:I

    return-void
.end method

.method private xh()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lbch;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/SearchResults;->awP:[Ljava/lang/String;

    iget-object v1, p0, Lbch;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v1, v1, Lcom/google/android/gms/appdatasearch/SearchResults;->awO:[I

    iget v2, p0, Lbch;->awT:I

    aget v1, v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method


# virtual methods
.method public final eQ(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lbch;->awS:Lbci;

    invoke-static {v0}, Lbci;->b(Lbci;)[Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lbch;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v1, v1, Lcom/google/android/gms/appdatasearch/SearchResults;->awO:[I

    iget v3, p0, Lbch;->awT:I

    aget v1, v1, v3

    aget-object v0, v0, v1

    if-nez v0, :cond_4

    iget-object v0, p0, Lbch;->awS:Lbci;

    invoke-static {v0}, Lbci;->b(Lbci;)[Ljava/util/Map;

    move-result-object v1

    iget-object v0, p0, Lbch;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/SearchResults;->awO:[I

    iget v3, p0, Lbch;->awT:I

    aget v3, v0, v3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    aput-object v0, v1, v3

    move-object v1, v0

    :goto_0
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbck;

    if-nez v0, :cond_1

    iget-object v0, p0, Lbch;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/SearchResults;->awL:[Landroid/os/Bundle;

    iget-object v3, p0, Lbch;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/SearchResults;->awO:[I

    iget v4, p0, Lbch;->awT:I

    aget v3, v3, v4

    aget-object v0, v0, v3

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v3

    iget-object v0, p0, Lbch;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/SearchResults;->awM:[Landroid/os/Bundle;

    iget-object v4, p0, Lbch;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v4, v4, Lcom/google/android/gms/appdatasearch/SearchResults;->awO:[I

    iget v5, p0, Lbch;->awT:I

    aget v4, v4, v5

    aget-object v0, v0, v4

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v4

    if-eqz v3, :cond_0

    if-nez v4, :cond_2

    :cond_0
    move-object v0, v2

    :cond_1
    :goto_1
    if-nez v0, :cond_3

    move-object v0, v2

    :goto_2
    return-object v0

    :cond_2
    new-instance v0, Lbck;

    invoke-direct {v0, v3, v4}, Lbck;-><init>([I[B)V

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    iget v1, p0, Lbch;->awT:I

    invoke-virtual {v0, v1}, Lbck;->dA(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public final getPackageName()Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lbch;->xh()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x2d

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getUri()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lbch;->awS:Lbci;

    invoke-static {v0}, Lbci;->a(Lbci;)Lbck;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lbch;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/SearchResults;->awI:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbch;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/SearchResults;->awJ:[B

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lbch;->awS:Lbci;

    new-instance v1, Lbck;

    iget-object v2, p0, Lbch;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v2, v2, Lcom/google/android/gms/appdatasearch/SearchResults;->awI:[I

    iget-object v3, p0, Lbch;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/SearchResults;->awJ:[B

    invoke-direct {v1, v2, v3}, Lbck;-><init>([I[B)V

    invoke-static {v0, v1}, Lbci;->a(Lbci;Lbck;)Lbck;

    :cond_2
    iget-object v0, p0, Lbch;->awS:Lbci;

    invoke-static {v0}, Lbci;->a(Lbci;)Lbck;

    move-result-object v0

    iget v1, p0, Lbch;->awT:I

    invoke-virtual {v0, v1}, Lbck;->dA(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final xg()Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lbch;->xh()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2d

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final xi()D
    .locals 2

    iget-object v0, p0, Lbch;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/SearchResults;->awR:[D

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbch;->awU:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/SearchResults;->awR:[D

    iget v1, p0, Lbch;->awT:I

    aget-wide v0, v0, v1

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
