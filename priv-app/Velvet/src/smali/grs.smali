.class public abstract Lgrs;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final cUv:[Ljava/lang/String;


# instance fields
.field final cNN:Z

.field private final mGsaConfigFlags:Lchk;

.field private mResources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "%1$s"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "%2$s"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "%3$s"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "%4$s"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "%5$s"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "%6$s"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "%7$s"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "%8$s"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "%9$s"

    aput-object v2, v0, v1

    sput-object v0, Lgrs;->cUv:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lchk;Z)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lgrs;->mResources:Landroid/content/res/Resources;

    .line 52
    iput-object p2, p0, Lgrs;->mGsaConfigFlags:Lchk;

    .line 53
    iput-boolean p3, p0, Lgrs;->cNN:Z

    .line 54
    return-void
.end method

.method public static a(Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/Object;Ljava/lang/Class;)Landroid/util/Pair;
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v0, 0x0

    .line 341
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 344
    const-string v1, "%s"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v6, :cond_0

    .line 345
    const-string v1, "%s"

    const-string v2, "%1$s"

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    :cond_0
    move v1, v0

    move v2, v0

    move v3, v0

    .line 350
    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_5

    .line 351
    invoke-static {v1}, Lgrs;->kz(I)Ljava/lang/String;

    move-result-object v5

    .line 352
    invoke-virtual {p0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v6, :cond_2

    .line 354
    aget-object v0, p1, v1

    if-nez v0, :cond_1

    .line 356
    add-int/lit8 v3, v3, 0x1

    .line 350
    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 361
    :cond_2
    aget-object v0, p1, v1

    if-eqz v0, :cond_3

    .line 363
    aget-object v0, p1, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v5, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 368
    :cond_3
    add-int/lit8 v0, v3, 0x1

    aget-object v3, p2, v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 369
    if-eq v2, v1, :cond_4

    .line 370
    invoke-static {v2}, Lgrs;->kz(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v5, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 372
    :cond_4
    add-int/lit8 v2, v2, 0x1

    move v3, v0

    goto :goto_1

    .line 376
    :cond_5
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {p3, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 377
    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method protected static a(Ljrc;I)Ljqg;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 84
    iget-object v2, p0, Ljrc;->ezX:[Ljqg;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 85
    invoke-virtual {v0}, Ljqg;->ajF()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 89
    :goto_1
    return-object v0

    .line 84
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 89
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected static a(Ljqb;Ljqg;)Ljqh;
    .locals 2

    .prologue
    .line 73
    new-instance v0, Ljqh;

    invoke-direct {v0}, Ljqh;-><init>()V

    invoke-virtual {p1}, Ljqg;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljqh;->rt(I)Ljqh;

    move-result-object v1

    .line 75
    iget-object v0, p0, Ljqb;->eye:[Ljqh;

    invoke-static {v0, v1}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljqh;

    iput-object v0, p0, Ljqb;->eye:[Ljqh;

    .line 76
    return-object v1
.end method

.method protected static a(Ljqg;I)Ljqu;
    .locals 1

    .prologue
    .line 290
    invoke-static {p0}, Lgrs;->c(Ljqg;)Ljqu;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljqu;->rC(I)Ljqu;

    move-result-object v0

    return-object v0
.end method

.method private varargs a(I[Ljqu;)Ljqv;
    .locals 2

    .prologue
    .line 206
    new-instance v0, Ljqv;

    invoke-direct {v0}, Ljqv;-><init>()V

    iget-object v1, p0, Lgrs;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljqv;->yl(Ljava/lang/String;)Ljqv;

    move-result-object v0

    .line 209
    iput-object p2, v0, Ljqv;->ezC:[Ljqu;

    .line 210
    return-object v0
.end method

.method private static a(Ljava/lang/String;[Ljava/lang/Object;[Ljqu;)Ljqv;
    .locals 3

    .prologue
    .line 321
    const-class v0, Ljqu;

    invoke-static {p0, p1, p2, v0}, Lgrs;->a(Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/Object;Ljava/lang/Class;)Landroid/util/Pair;

    move-result-object v1

    .line 325
    new-instance v2, Ljqv;

    invoke-direct {v2}, Ljqv;-><init>()V

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljqv;->yl(Ljava/lang/String;)Ljqv;

    move-result-object v2

    .line 327
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, [Ljqu;

    iput-object v0, v2, Ljqv;->ezC:[Ljqu;

    .line 328
    return-object v2
.end method

.method private aKg()[Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/16 v9, 0xa

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 297
    new-array v3, v9, [Ljava/lang/String;

    .line 301
    new-array v4, v8, [Ljava/lang/Object;

    aput-object v10, v4, v2

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    aput-object v0, v4, v7

    move v1, v2

    .line 302
    :goto_0
    if-ge v1, v9, :cond_0

    .line 303
    iget-object v0, p0, Lgrs;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f0a0719

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v5, v8, [Ljava/lang/Object;

    add-int/lit8 v6, v1, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    aput-object v10, v5, v7

    const-class v6, Ljava/lang/Object;

    invoke-static {v0, v5, v4, v6}, Lgrs;->a(Ljava/lang/String;[Ljava/lang/Object;[Ljava/lang/Object;Ljava/lang/Class;)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    aput-object v0, v3, v1

    .line 302
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 309
    :cond_0
    return-object v3
.end method

.method protected static c(Ljqg;)Ljqu;
    .locals 2

    .prologue
    .line 279
    new-instance v0, Ljqu;

    invoke-direct {v0}, Ljqu;-><init>()V

    invoke-virtual {p0}, Ljqg;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljqu;->rB(I)Ljqu;

    move-result-object v0

    return-object v0
.end method

.method private ky(I)[I
    .locals 3

    .prologue
    .line 175
    if-gez p1, :cond_0

    .line 176
    iget-object v0, p0, Lgrs;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->IQ()I

    move-result v0

    .line 180
    :goto_0
    new-array v2, v0, [I

    .line 181
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_1

    .line 182
    aput v1, v2, v1

    .line 181
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 178
    :cond_0
    iget-object v0, p0, Lgrs;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->IQ()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 184
    :cond_1
    return-object v2
.end method

.method private static kz(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 386
    sget-object v0, Lgrs;->cUv:[Ljava/lang/String;

    array-length v0, v0

    if-ge p0, v0, :cond_0

    sget-object v0, Lgrs;->cUv:[Ljava/lang/String;

    aget-object v0, v0, p0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "%"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v1, p0, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "$s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Ljqg;ZLgtl;Lgra;I)Ljqj;
    .locals 9

    .prologue
    .line 100
    new-instance v1, Ljqj;

    invoke-direct {v1}, Ljqj;-><init>()V

    .line 102
    sget-object v0, Lgqu;->cTA:Lgrn;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lgrn;->nx(Ljava/lang/String;)Lgtl;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v3, 0x1

    new-array v3, v3, [Ljqu;

    const/4 v4, 0x0

    const/16 v5, 0x8

    invoke-static {p1, v5}, Lgrs;->a(Ljqg;I)Ljqu;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v0, p2, v2, v3}, Lgrs;->a(Lgtl;ZZ[Ljqu;)[Ljrg;

    move-result-object v0

    iput-object v0, v1, Ljqj;->eyM:[Ljrg;

    .line 107
    sget-object v0, Lgqu;->cTB:Lgrn;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lgrn;->nx(Ljava/lang/String;)Lgtl;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v3, 0x1

    new-array v3, v3, [Ljqu;

    const/4 v4, 0x0

    const/16 v5, 0x8

    invoke-static {p1, v5}, Lgrs;->a(Ljqg;I)Ljqu;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v0, p2, v2, v3}, Lgrs;->a(Lgtl;ZZ[Ljqu;)[Ljrg;

    move-result-object v0

    iput-object v0, v1, Ljqj;->eyN:[Ljrg;

    .line 112
    sget-object v0, Lgqu;->cTC:Lgqw;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lgqw;->aS(Ljava/lang/String;Ljava/lang/String;)Lgtl;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v3, 0x2

    new-array v3, v3, [Ljqu;

    const/4 v4, 0x0

    const/16 v5, 0x8

    invoke-static {p1, v5}, Lgrs;->a(Ljqg;I)Ljqu;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x4

    invoke-static {p1, v5}, Lgrs;->a(Ljqg;I)Ljqu;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v0, p2, v2, v3}, Lgrs;->a(Lgtl;ZZ[Ljqu;)[Ljrg;

    move-result-object v0

    iput-object v0, v1, Ljqj;->eyO:[Ljrg;

    .line 119
    sget-object v0, Lgqu;->cTD:Lgqw;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lgqw;->aS(Ljava/lang/String;Ljava/lang/String;)Lgtl;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v3, 0x2

    new-array v3, v3, [Ljqu;

    const/4 v4, 0x0

    const/16 v5, 0x8

    invoke-static {p1, v5}, Lgrs;->a(Ljqg;I)Ljqu;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x4

    invoke-static {p1, v5}, Lgrs;->a(Ljqg;I)Ljqu;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v0, p2, v2, v3}, Lgrs;->a(Lgtl;ZZ[Ljqu;)[Ljrg;

    move-result-object v0

    iput-object v0, v1, Ljqj;->eyP:[Ljrg;

    .line 125
    const/4 v0, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljqu;

    invoke-virtual {p0, p3, p2, v0, v2}, Lgrs;->a(Lgtl;ZZ[Ljqu;)[Ljrg;

    move-result-object v0

    iput-object v0, v1, Ljqj;->eyL:[Ljrg;

    .line 127
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lgrs;->a(Ljqg;I)Ljqu;

    move-result-object v0

    .line 128
    iget-object v2, p0, Lgrs;->mResources:Landroid/content/res/Resources;

    invoke-static {v2}, Lerr;->g(Landroid/content/res/Resources;)Ljra;

    move-result-object v2

    iput-object v2, v0, Ljqu;->ezy:Ljra;

    .line 129
    const/4 v2, 0x3

    invoke-direct {p0, v2}, Lgrs;->ky(I)[I

    move-result-object v2

    iput-object v2, v0, Ljqu;->ezx:[I

    .line 130
    const/4 v2, 0x4

    invoke-static {p1, v2}, Lgrs;->a(Ljqg;I)Ljqu;

    move-result-object v2

    .line 131
    iget-object v3, p0, Lgrs;->mResources:Landroid/content/res/Resources;

    invoke-static {v3}, Lerr;->g(Landroid/content/res/Resources;)Ljra;

    move-result-object v3

    iput-object v3, v2, Ljqu;->ezy:Ljra;

    .line 132
    const/4 v3, -0x1

    invoke-direct {p0, v3}, Lgrs;->ky(I)[I

    move-result-object v3

    iput-object v3, v2, Ljqu;->ezx:[I

    .line 133
    iget-object v3, v2, Ljqu;->ezy:Ljra;

    invoke-direct {p0}, Lgrs;->aKg()[Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Ljra;->ezR:[Ljava/lang/String;

    .line 135
    iget-object v3, p4, Lgra;->cTY:Lgrl;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lgrl;->a(Ljava/lang/String;Ljava/util/List;Lchk;)Lgtl;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x3

    new-array v5, v5, [Ljqu;

    const/4 v6, 0x0

    const/16 v7, 0x9

    invoke-static {p1, v7}, Lgrs;->a(Ljqg;I)Ljqu;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v0, v5, v6

    const/4 v0, 0x2

    aput-object v2, v5, v0

    invoke-virtual {p0, v3, p2, v4, v5}, Lgrs;->a(Lgtl;ZZ[Ljqu;)[Ljrg;

    move-result-object v0

    iput-object v0, v1, Ljqj;->eyQ:[Ljrg;

    .line 140
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lgrs;->a(Ljqg;I)Ljqu;

    move-result-object v0

    .line 141
    iget-object v2, p0, Lgrs;->mResources:Landroid/content/res/Resources;

    invoke-static {v2}, Lerr;->g(Landroid/content/res/Resources;)Ljra;

    move-result-object v2

    iput-object v2, v0, Ljqu;->ezy:Ljra;

    .line 142
    const/4 v2, -0x1

    invoke-direct {p0, v2}, Lgrs;->ky(I)[I

    move-result-object v2

    iput-object v2, v0, Ljqu;->ezx:[I

    .line 143
    if-eqz p2, :cond_0

    .line 144
    iget-object v2, v0, Ljqu;->ezy:Ljra;

    invoke-direct {p0}, Lgrs;->aKg()[Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Ljra;->ezR:[Ljava/lang/String;

    .line 146
    :cond_0
    iget-object v2, p4, Lgra;->cUa:Lgrc;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lgrc;->b(Ljava/util/List;Lchk;)Lgtl;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x1

    new-array v4, v4, [Ljqu;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {p0, v2, p2, v3, v4}, Lgrs;->a(Lgtl;ZZ[Ljqu;)[Ljrg;

    move-result-object v0

    iput-object v0, v1, Ljqj;->eyS:[Ljrg;

    .line 150
    const/4 v0, 0x5

    invoke-static {p1, v0}, Lgrs;->a(Ljqg;I)Ljqu;

    move-result-object v0

    .line 151
    iget-object v2, p0, Lgrs;->mResources:Landroid/content/res/Resources;

    invoke-static {v2}, Lerr;->g(Landroid/content/res/Resources;)Ljra;

    move-result-object v2

    iput-object v2, v0, Ljqu;->ezy:Ljra;

    .line 152
    const/4 v2, -0x1

    invoke-direct {p0, v2}, Lgrs;->ky(I)[I

    move-result-object v2

    iput-object v2, v0, Ljqu;->ezx:[I

    .line 153
    iget-object v2, p4, Lgra;->cTZ:Lgre;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lgre;->b(Ljava/util/List;Lchk;)Lgtl;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x1

    new-array v4, v4, [Ljqu;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {p0, v2, p2, v3, v4}, Lgrs;->a(Lgtl;ZZ[Ljqu;)[Ljrg;

    move-result-object v0

    iput-object v0, v1, Ljqj;->eyR:[Ljrg;

    .line 159
    const/4 v0, 0x1

    new-array v2, v0, [Ljrg;

    const/4 v3, 0x0

    const v0, 0x7f0a05f4

    const v4, 0x7f0a05f6

    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljqu;

    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-static {p1, v8}, Lgrs;->a(Ljqg;I)Ljqu;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p0, v0, v4, v5, v6}, Lgrs;->a(IIZ[Ljqu;)Ljrg;

    move-result-object v4

    if-eqz p2, :cond_1

    const/4 v0, 0x2

    :goto_0
    invoke-virtual {v4, v0}, Ljrg;->rL(I)Ljrg;

    move-result-object v0

    aput-object v0, v2, v3

    iput-object v2, v1, Ljqj;->eyT:[Ljrg;

    .line 166
    return-object v1

    .line 159
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final varargs a(IIZ[Ljqu;)Ljrg;
    .locals 2

    .prologue
    .line 194
    new-instance v0, Ljrg;

    invoke-direct {v0}, Ljrg;-><init>()V

    .line 195
    if-eqz p1, :cond_0

    .line 196
    invoke-direct {p0, p1, p4}, Lgrs;->a(I[Ljqu;)Ljqv;

    move-result-object v1

    iput-object v1, v0, Ljrg;->eAn:Ljqv;

    .line 198
    :cond_0
    if-eqz p2, :cond_2

    iget-boolean v1, p0, Lgrs;->cNN:Z

    if-nez v1, :cond_1

    if-eqz p3, :cond_2

    .line 199
    :cond_1
    invoke-direct {p0, p2, p4}, Lgrs;->a(I[Ljqu;)Ljqv;

    move-result-object v1

    iput-object v1, v0, Ljrg;->eAo:Ljqv;

    .line 201
    :cond_2
    return-object v0
.end method

.method protected final varargs a(Lgtl;ZZ[Ljqu;)[Ljrg;
    .locals 11

    .prologue
    .line 222
    invoke-interface {p1}, Lgtl;->aKd()Lgtm;

    move-result-object v0

    iget-object v1, p0, Lgrs;->mResources:Landroid/content/res/Resources;

    invoke-interface {p1, v1}, Lgtl;->i(Landroid/content/res/Resources;)[Ljava/lang/Object;

    move-result-object v4

    iget-object v1, v0, Lgtm;->cUY:[Lgto;

    array-length v1, v1

    new-array v5, v1, [Ljrg;

    iget-object v1, v0, Lgtm;->cUY:[Lgto;

    iget-object v2, v0, Lgtm;->cUY:[Lgto;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    iget v6, v1, Lgto;->cVd:I

    const/4 v1, 0x0

    iget-object v7, v0, Lgtm;->cUY:[Lgto;

    array-length v8, v7

    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    if-ge v1, v8, :cond_7

    aget-object v3, v7, v1

    new-instance v9, Ljrg;

    invoke-direct {v9}, Ljrg;-><init>()V

    iget-object v0, p0, Lgrs;->mResources:Landroid/content/res/Resources;

    iget v10, v3, Lgto;->cVa:I

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4, p4}, Lgrs;->a(Ljava/lang/String;[Ljava/lang/Object;[Ljqu;)Ljqv;

    move-result-object v0

    iput-object v0, v9, Ljrg;->eAn:Ljqv;

    iget-boolean v0, p0, Lgrs;->cNN:Z

    if-eqz v0, :cond_0

    iget-object v10, p0, Lgrs;->mResources:Landroid/content/res/Resources;

    if-eqz p2, :cond_4

    iget v0, v3, Lgto;->cVc:I

    :goto_1
    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4, p4}, Lgrs;->a(Ljava/lang/String;[Ljava/lang/Object;[Ljqu;)Ljqv;

    move-result-object v0

    iput-object v0, v9, Ljrg;->eAo:Ljqv;

    :cond_0
    iget v0, v3, Lgto;->cVd:I

    if-eqz v0, :cond_1

    iget v0, v3, Lgto;->cVd:I

    invoke-virtual {v9, v0}, Ljrg;->rM(I)Ljrg;

    :cond_1
    iget v0, v3, Lgto;->cVe:I

    if-eqz v0, :cond_2

    iget v0, v3, Lgto;->cVe:I

    invoke-virtual {v9, v0}, Ljrg;->rN(I)Ljrg;

    :cond_2
    iget-boolean v0, p0, Lgrs;->cNN:Z

    if-eqz v0, :cond_3

    iget v0, v3, Lgto;->cVd:I

    if-ne v0, v6, :cond_6

    :cond_3
    if-eqz p2, :cond_5

    const/4 v0, 0x2

    :goto_2
    invoke-virtual {v9, v0}, Ljrg;->rL(I)Ljrg;

    :goto_3
    add-int/lit8 v3, v2, 0x1

    aput-object v9, v5, v2

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v2, v3

    goto :goto_0

    :cond_4
    iget v0, v3, Lgto;->cVb:I

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Ljrg;->rL(I)Ljrg;

    goto :goto_3

    :cond_7
    return-object v5
.end method
