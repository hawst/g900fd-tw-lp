.class final Lhsd;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private final GS:Landroid/view/LayoutInflater;

.field private synthetic dpO:Lhrz;


# direct methods
.method constructor <init>(Lhrz;)V
    .locals 1

    .prologue
    .line 150
    iput-object p1, p0, Lhsd;->dpO:Lhrz;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 151
    invoke-virtual {p1}, Lhrz;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lhsd;->GS:Landroid/view/LayoutInflater;

    .line 152
    return-void
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x0

    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lhsd;->dpO:Lhrz;

    iget-object v0, v0, Lhrz;->dpM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lhsd;->dpO:Lhrz;

    iget-object v0, v0, Lhrz;->dpM:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 166
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 182
    if-eqz p2, :cond_0

    .line 183
    check-cast p2, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;

    .line 188
    :goto_0
    const v0, 0x7f110238

    invoke-virtual {p2, v0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 189
    if-nez p1, :cond_1

    .line 190
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 195
    :goto_1
    iget-object v0, p0, Lhsd;->dpO:Lhrz;

    iget-object v0, v0, Lhrz;->dpM:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 196
    iget-object v1, p0, Lhsd;->dpO:Lhrz;

    iget-object v1, v1, Lhrz;->doQ:Lhrk;

    invoke-virtual {v1}, Lhrk;->aGp()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljzp;

    .line 197
    iget-object v2, p0, Lhsd;->dpO:Lhrz;

    iget-object v2, v2, Lhrz;->doQ:Lhrk;

    invoke-virtual {v2}, Lhrk;->aRK()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljzp;

    .line 198
    if-eq v0, v1, :cond_2

    .line 200
    :goto_2
    iget-object v2, p0, Lhsd;->dpO:Lhrz;

    iget-object v2, v2, Lhrz;->doQ:Lhrk;

    iget-object v3, p0, Lhsd;->dpO:Lhrz;

    iget-object v3, v3, Lhrz;->mConfiguration:Ljze;

    invoke-virtual {p2, v2, v3, v1, v0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->a(Lhrk;Ljze;Ljzp;Ljzp;)V

    .line 201
    return-object p2

    .line 185
    :cond_0
    iget-object v0, p0, Lhsd;->GS:Landroid/view/LayoutInflater;

    const v1, 0x7f0400b0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;

    move-object p2, v0

    goto :goto_0

    .line 192
    :cond_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 198
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x0

    return v0
.end method
