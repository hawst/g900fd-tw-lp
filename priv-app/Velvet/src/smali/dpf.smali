.class final Ldpf;
.super Lgly;
.source "PG"


# instance fields
.field private final bEC:Lgnp;

.field private bED:Z

.field private synthetic bEE:Ldpe;

.field private final bbd:Lenw;

.field private final mQuery:Lcom/google/android/shared/search/Query;


# direct methods
.method public constructor <init>(Ldpe;Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 184
    iput-object p1, p0, Ldpf;->bEE:Ldpe;

    invoke-direct {p0}, Lgly;-><init>()V

    .line 185
    iput-object p2, p0, Ldpf;->mQuery:Lcom/google/android/shared/search/Query;

    .line 186
    new-instance v0, Lenw;

    invoke-direct {v0}, Lenw;-><init>()V

    iput-object v0, p0, Ldpf;->bbd:Lenw;

    .line 187
    new-instance v0, Lgnp;

    invoke-direct {v0}, Lgnp;-><init>()V

    iput-object v0, p0, Ldpf;->bEC:Lgnp;

    .line 188
    return-void
.end method

.method private a([Ljvs;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 285
    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 286
    array-length v0, p1

    new-array v0, v0, [Ljava/lang/String;

    .line 287
    array-length v2, p1

    new-array v2, v2, [F

    .line 288
    :goto_1
    array-length v3, p1

    if-ge v1, v3, :cond_1

    .line 289
    aget-object v3, p1, v1

    invoke-virtual {v3}, Ljvs;->getText()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    .line 290
    aget-object v3, p1, v1

    invoke-virtual {v3}, Ljvs;->bty()F

    move-result v3

    aput v3, v2, v1

    .line 288
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 285
    goto :goto_0

    .line 292
    :cond_1
    iget-object v1, p0, Ldpf;->bEE:Ldpe;

    iget-object v1, v1, Ldpe;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aaq()Ldbi;

    move-result-object v1

    iget-object v3, p0, Ldpf;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v3, v0, v2}, Ldbi;->a(Lcom/google/android/shared/search/Query;[Ljava/lang/String;[F)V

    .line 293
    return-void
.end method

.method private aeo()V
    .locals 6

    .prologue
    .line 296
    new-instance v0, Lein;

    invoke-direct {v0}, Lein;-><init>()V

    .line 297
    iget-object v1, p0, Ldpf;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->AF()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    invoke-static {v0, v2, v3, v4, v5}, Lege;->a(Lefr;JJ)V

    .line 298
    invoke-direct {p0, v0}, Ldpf;->d(Leiq;)V

    .line 299
    return-void
.end method

.method private d(Leiq;)V
    .locals 8

    .prologue
    .line 302
    iget-object v0, p0, Ldpf;->bEE:Ldpe;

    iget-object v0, v0, Ldpe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v6

    iget-object v7, p0, Ldpf;->mQuery:Lcom/google/android/shared/search/Query;

    new-instance v0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;

    iget-object v1, p0, Ldpf;->mQuery:Lcom/google/android/shared/search/Query;

    iget-object v2, p0, Ldpf;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;-><init>(Lcom/google/android/shared/search/Query;Leiq;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v6, v7, v0}, Lcom/google/android/search/core/state/QueryState;->b(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/errors/SearchError;)V

    .line 305
    return-void
.end method


# virtual methods
.method public final CZ()V
    .locals 2

    .prologue
    .line 309
    iget-object v0, p0, Ldpf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 310
    iget-boolean v0, p0, Ldpf;->bED:Z

    if-nez v0, :cond_0

    .line 311
    invoke-direct {p0}, Ldpf;->aeo()V

    .line 313
    :cond_0
    iget-object v0, p0, Ldpf;->bEE:Ldpe;

    iget-object v0, v0, Ldpe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iget-object v1, p0, Ldpf;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->U(Lcom/google/android/shared/search/Query;)V

    .line 314
    return-void
.end method

.method public final Nr()V
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Ldpf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 202
    iget-object v0, p0, Ldpf;->bEE:Ldpe;

    iget-object v0, v0, Ldpe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ldct;->fE(I)V

    .line 203
    return-void
.end method

.method public final Nw()V
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Ldpf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 223
    invoke-direct {p0}, Ldpf;->aeo()V

    .line 224
    iget-object v0, p0, Ldpf;->bEE:Ldpe;

    iget-object v0, v0, Ldpe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iget-object v1, p0, Ldpf;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->U(Lcom/google/android/shared/search/Query;)V

    .line 225
    return-void
.end method

.method public final a(Ljvv;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 239
    iget-object v1, p0, Ldpf;->bbd:Lenw;

    invoke-virtual {v1}, Lenw;->auS()Lenw;

    .line 241
    invoke-virtual {p1}, Ljvv;->getEventType()I

    move-result v1

    if-nez v1, :cond_0

    .line 242
    iget-object v1, p0, Ldpf;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-static {v1}, Ldpe;->aN(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 243
    iget-object v0, p1, Ljvv;->eIm:Ljvw;

    if-eqz v0, :cond_0

    iget-object v0, p1, Ljvv;->eIm:Ljvw;

    iget-object v1, v0, Ljvw;->eIk:[Ljvs;

    array-length v1, v1

    if-lez v1, :cond_0

    iget-object v0, v0, Ljvw;->eIk:[Ljvs;

    invoke-direct {p0, v0}, Ldpf;->a([Ljvs;)V

    .line 251
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljvv;->getEventType()I

    move-result v0

    if-ne v0, v3, :cond_1

    iget-object v0, p1, Ljvv;->eIo:Ljvw;

    if-eqz v0, :cond_1

    iget-object v0, p1, Ljvv;->eIo:Ljvw;

    iget-object v0, v0, Ljvw;->eIk:[Ljvs;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 254
    iput-boolean v3, p0, Ldpf;->bED:Z

    iget-object v0, p1, Ljvv;->eIo:Ljvw;

    iget-object v0, v0, Ljvw;->eIk:[Ljvs;

    invoke-direct {p0, v0}, Ldpf;->a([Ljvs;)V

    .line 256
    :cond_1
    return-void

    .line 244
    :cond_2
    iget-object v1, p0, Ldpf;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "android.speech.extra.PARTIAL_RESULTS"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :cond_3
    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Ldpf;->bEC:Lgnp;

    invoke-virtual {v0, p1}, Lgnp;->g(Ljvv;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, p0, Ldpf;->bEE:Ldpe;

    iget-object v0, v0, Ldpe;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->Ne()Ldyl;

    move-result-object v2

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v0, v1}, Ldyl;->x(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method final aen()Z
    .locals 1

    .prologue
    .line 195
    iget-boolean v0, p0, Ldpf;->bED:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ldpf;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqE()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final au(J)V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Ldpf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 209
    iget-object v0, p0, Ldpf;->bEE:Ldpe;

    iget-object v0, v0, Ldpe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ldct;->fE(I)V

    .line 210
    return-void
.end method

.method public final c(Leiq;)V
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Ldpf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 231
    invoke-direct {p0, p1}, Ldpf;->d(Leiq;)V

    .line 232
    iget-object v0, p0, Ldpf;->bEE:Ldpe;

    iget-object v0, v0, Ldpe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iget-object v1, p0, Ldpf;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->U(Lcom/google/android/shared/search/Query;)V

    .line 233
    return-void
.end method

.method public final onEndOfSpeech()V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Ldpf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 216
    iget-object v0, p0, Ldpf;->bEE:Ldpe;

    iget-object v0, v0, Ldpe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ldct;->fE(I)V

    .line 217
    return-void
.end method

.method final zK()Ljava/lang/String;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Ldpf;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
