.class public final Lcpx;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "HandlerLeak"
    }
.end annotation


# static fields
.field private static final bfR:Ljava/util/Map;

.field private static final bfS:Lcqc;


# instance fields
.field private final bfT:Lalf;

.field final bfU:Lamb;

.field private final bfV:Lcqb;

.field private bfW:J

.field private bfX:J

.field private bfY:Ljava/lang/String;

.field private final mAppContext:Landroid/content/Context;

.field final mClock:Lemp;

.field private final mPrefController:Lchr;

.field private final mRandom:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 104
    const-string v0, "NOTIFICATION_CLICK"

    const/16 v1, 0x17

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "NOTIFICATION_ACTION_PRESS"

    const/16 v3, 0x18

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "WIDGET_PRESS"

    const/16 v5, 0x1b

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lijm;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lijm;

    move-result-object v0

    sput-object v0, Lcpx;->bfR:Ljava/util/Map;

    .line 111
    new-instance v0, Lcpy;

    invoke-direct {v0}, Lcpy;-><init>()V

    sput-object v0, Lcpx;->bfS:Lcqc;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lemp;Lchr;Lcjs;)V
    .locals 8

    .prologue
    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    const-wide/16 v4, 0x0

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    iput-object p2, p0, Lcpx;->mClock:Lemp;

    .line 148
    new-instance v0, Lcqb;

    invoke-direct {v0, p0}, Lcqb;-><init>(Lcpx;)V

    iput-object v0, p0, Lcpx;->bfV:Lcqb;

    .line 149
    invoke-static {p1}, Lalf;->H(Landroid/content/Context;)Lalf;

    move-result-object v0

    iput-object v0, p0, Lcpx;->bfT:Lalf;

    .line 150
    iput-object p1, p0, Lcpx;->mAppContext:Landroid/content/Context;

    .line 151
    iput-object p3, p0, Lcpx;->mPrefController:Lchr;

    .line 152
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcpx;->mRandom:Ljava/util/Random;

    .line 159
    iget-object v0, p0, Lcpx;->bfT:Lalf;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v6, "mThread"

    invoke-virtual {v1, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v6, "getQueue"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v1, v6, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/LinkedBlockingQueue;

    new-instance v1, Lcpz;

    invoke-direct {v1}, Lcpz;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :goto_0
    invoke-static {}, Lakm;->mr()Lakm;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lakm;->bZ(I)V

    .line 165
    iget-object v0, p0, Lcpx;->bfT:Lalf;

    const-string v1, "UA-25271179-3"

    invoke-virtual {v0, v1}, Lalf;->C(Ljava/lang/String;)Lamb;

    move-result-object v0

    iput-object v0, p0, Lcpx;->bfU:Lamb;

    .line 167
    invoke-virtual {p4}, Lcjs;->Mq()D

    move-result-wide v0

    .line 170
    cmpg-double v6, v0, v4

    if-gez v6, :cond_0

    move-wide v0, v4

    .line 171
    :cond_0
    cmpl-double v4, v0, v2

    if-lez v4, :cond_1

    move-wide v0, v2

    .line 172
    :cond_1
    iget-object v2, p0, Lcpx;->bfU:Lamb;

    invoke-virtual {v2, v0, v1}, Lamb;->a(D)V

    .line 174
    return-void

    .line 159
    :catch_0
    move-exception v0

    const-string v1, "QSB.UserInteractionLogger"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Failed to lower GA thread priority: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static Sg()V
    .locals 1

    .prologue
    .line 183
    invoke-static {}, Lakm;->mr()Lakm;

    move-result-object v0

    invoke-virtual {v0}, Lakm;->mp()V

    .line 184
    return-void
.end method

.method private static Sh()Lfcr;
    .locals 1

    .prologue
    .line 355
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->Sh()Lfcr;

    move-result-object v0

    return-object v0
.end method

.method private Si()V
    .locals 5

    .prologue
    .line 480
    iget-object v0, p0, Lcpx;->bfU:Lamb;

    const-string v1, ""

    const-string v2, ""

    const-string v3, ""

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lamb;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 481
    return-void
.end method

.method private Sk()Z
    .locals 4

    .prologue
    .line 529
    iget-wide v0, p0, Lcpx;->bfX:J

    iget-wide v2, p0, Lcpx;->bfW:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcpx;)V
    .locals 4

    .prologue
    .line 64
    iget-object v0, p0, Lcpx;->bfV:Lcqb;

    const/4 v1, 0x1

    const-wide/32 v2, 0x493e0

    invoke-virtual {v0, v1, v2, v3}, Lcqb;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method static synthetic a(Lcpx;J)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 64
    iget-object v0, p0, Lcpx;->bfV:Lcqb;

    invoke-virtual {v0, v1}, Lcqb;->removeMessages(I)V

    invoke-direct {p0}, Lcpx;->Sk()Z

    move-result v0

    if-nez v0, :cond_0

    iput-wide p1, p0, Lcpx;->bfW:J

    iget-object v0, p0, Lcpx;->bfU:Lamb;

    invoke-virtual {v0, v1}, Lamb;->aC(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcpx;->bfY:Ljava/lang/String;

    invoke-direct {p0}, Lcpx;->Si()V

    :cond_0
    return-void
.end method

.method private static d(Lizj;I)Liwk;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 346
    iget-object v2, p0, Lizj;->dUo:[Liwk;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 347
    invoke-virtual {v0}, Liwk;->getType()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 351
    :goto_1
    return-object v0

    .line 346
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 351
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method final Sj()V
    .locals 2

    .prologue
    .line 508
    iget-object v0, p0, Lcpx;->bfV:Lcqb;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcqb;->removeMessages(I)V

    .line 511
    invoke-direct {p0}, Lcpx;->Sk()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 512
    iget-object v0, p0, Lcpx;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcpx;->bfX:J

    .line 516
    const/4 v0, 0x0

    iput-object v0, p0, Lcpx;->bfY:Ljava/lang/String;

    .line 519
    invoke-direct {p0}, Lcpx;->Si()V

    .line 521
    :cond_0
    return-void
.end method

.method public final W(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 218
    iget-object v0, p0, Lcpx;->bfU:Lamb;

    const-string v1, "UI_ACTION"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, p2, v2}, Lamb;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 219
    return-void
.end method

.method public final X(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 441
    iget-object v0, p0, Lcpx;->bfU:Lamb;

    const-string v1, "INTERNAL"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, p2, v2}, Lamb;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 442
    return-void
.end method

.method public final Y(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 461
    iget-object v0, p0, Lcpx;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    .line 462
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "uil_limiter_internal_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 464
    iget-object v2, p0, Lcpx;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    div-long/2addr v2, v4

    .line 465
    const-wide/16 v4, 0x0

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 466
    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 467
    const/4 v4, 0x0

    invoke-virtual {p0, p1, v4}, Lcpx;->X(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 473
    :cond_0
    return-void
.end method

.method public final a(Landroid/app/Activity;J)V
    .locals 4

    .prologue
    .line 552
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 553
    const-string v1, "UserInteractionLoggerSession"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 555
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 556
    new-instance v1, Lcqa;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p2, p3, v2}, Lcqa;-><init>(Lcpx;JB)V

    const-string v2, "UserInteractionLoggerSession"

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 557
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 559
    :cond_0
    return-void
.end method

.method public final a(Lizj;ILixx;)V
    .locals 4
    .param p3    # Lixx;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 298
    invoke-static {p1, p2}, Lcpx;->d(Lizj;I)Liwk;

    move-result-object v0

    .line 299
    if-nez v0, :cond_0

    .line 313
    :goto_0
    return-void

    .line 303
    :cond_0
    sget-object v1, Lcgg;->aVG:Lcgg;

    invoke-virtual {v1}, Lcgg;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 304
    iget-object v1, p0, Lcpx;->mAppContext:Landroid/content/Context;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Logged: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 307
    :cond_1
    invoke-static {}, Lcpx;->Sh()Lfcr;

    move-result-object v1

    .line 308
    if-eqz p3, :cond_2

    .line 309
    invoke-interface {v1, p1, v0, p3}, Lfcr;->a(Lizj;Liwk;Lixx;)V

    goto :goto_0

    .line 311
    :cond_2
    invoke-interface {v1, p1, v0}, Lfcr;->e(Lizj;Liwk;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lfey;)V
    .locals 3

    .prologue
    .line 268
    invoke-interface {p2}, Lfey;->ayU()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcpx;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    invoke-interface {p2}, Lfey;->ayP()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 270
    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v2}, Lcpx;->a(Ljava/lang/String;Lizj;Lixx;)V

    goto :goto_0

    .line 272
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lfkd;)V
    .locals 2

    .prologue
    .line 250
    invoke-interface {p2}, Lfkd;->ayU()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcpx;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    invoke-interface {p2}, Lfkd;->getEntry()Lizj;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcpx;->a(Ljava/lang/String;Lizj;Lixx;)V

    .line 252
    return-void
.end method

.method public final a(Ljava/lang/String;Lizj;Lixx;)V
    .locals 2
    .param p3    # Lixx;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 284
    sget-object v0, Lcpx;->bfR:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 285
    if-eqz v0, :cond_0

    .line 286
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, p2, v0, v1}, Lcpx;->a(Lizj;ILixx;)V

    .line 288
    :cond_0
    return-void
.end method

.method public final b(Lizj;I)V
    .locals 2

    .prologue
    .line 322
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcpx;->d(Lizj;I)Liwk;

    move-result-object v0

    .line 323
    if-nez v0, :cond_0

    .line 327
    :goto_0
    return-void

    .line 326
    :cond_0
    invoke-static {}, Lcpx;->Sh()Lfcr;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lfcr;->b(Lizj;Liwk;)V

    goto :goto_0
.end method

.method public final c(Lizj;I)Z
    .locals 2

    .prologue
    .line 337
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcpx;->d(Lizj;I)Liwk;

    move-result-object v0

    .line 338
    if-nez v0, :cond_0

    .line 339
    const/4 v0, 0x0

    .line 341
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcpx;->Sh()Lfcr;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lfcr;->c(Lizj;Liwk;)Z

    move-result v0

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 235
    iget-object v0, p0, Lcpx;->bfU:Lamb;

    const-string v1, "UI_ACTION"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, p1, p2, v2}, Lamb;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 236
    return-void
.end method

.method public final io(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 195
    if-nez p1, :cond_1

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    iget-object v0, p0, Lcpx;->bfY:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 198
    iput-object p1, p0, Lcpx;->bfY:Ljava/lang/String;

    .line 199
    iget-object v0, p0, Lcpx;->bfU:Lamb;

    invoke-virtual {v0, p1}, Lamb;->J(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final ip(Ljava/lang/String;)Lcqc;
    .locals 6

    .prologue
    .line 401
    iget-object v0, p0, Lcpx;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    .line 402
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "uil_limiter_timing_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 404
    iget-object v2, p0, Lcpx;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    .line 405
    const-wide/16 v4, 0x0

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 406
    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 408
    const-wide/32 v4, 0x3dcc500

    add-long/2addr v2, v4

    iget-object v4, p0, Lcpx;->mRandom:Ljava/util/Random;

    const v5, 0x2932e00

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 410
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 415
    new-instance v0, Lcqd;

    invoke-direct {v0, p0, p1}, Lcqd;-><init>(Lcpx;Ljava/lang/String;)V

    .line 422
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcpx;->bfS:Lcqc;

    goto :goto_0
.end method
