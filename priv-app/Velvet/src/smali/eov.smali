.class public final Leov;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cgM:[Leox;

.field private final cgN:[Leox;

.field private final cgO:[[I

.field private final cgP:[[I


# direct methods
.method public constructor <init>([Leox;[Leox;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    array-length v4, p1

    .line 40
    array-length v5, p2

    .line 41
    add-int/lit8 v0, v4, 0x1

    add-int/lit8 v1, v5, 0x1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    .line 42
    add-int/lit8 v1, v4, 0x1

    add-int/lit8 v2, v5, 0x1

    filled-new-array {v1, v2}, [I

    move-result-object v1

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[I

    .line 43
    aget-object v2, v0, v7

    const/4 v6, 0x3

    aput v6, v2, v7

    .line 44
    aget-object v2, v1, v7

    aput v7, v2, v7

    move v2, v3

    .line 45
    :goto_0
    if-gt v2, v4, :cond_0

    .line 46
    aget-object v6, v0, v2

    aput v7, v6, v7

    .line 47
    aget-object v6, v1, v2

    aput v2, v6, v7

    .line 45
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    move v2, v3

    .line 49
    :goto_1
    if-gt v2, v5, :cond_1

    .line 50
    aget-object v4, v0, v7

    aput v3, v4, v2

    .line 51
    aget-object v4, v1, v7

    aput v2, v4, v2

    .line 49
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 53
    :cond_1
    iput-object v0, p0, Leov;->cgO:[[I

    .line 54
    iput-object v1, p0, Leov;->cgP:[[I

    .line 55
    iput-object p1, p0, Leov;->cgM:[Leox;

    .line 56
    iput-object p2, p0, Leov;->cgN:[Leox;

    .line 57
    return-void
.end method


# virtual methods
.method public final auZ()I
    .locals 14

    .prologue
    .line 65
    iget-object v5, p0, Leov;->cgM:[Leox;

    .line 66
    iget-object v6, p0, Leov;->cgN:[Leox;

    .line 67
    array-length v7, v5

    .line 68
    array-length v8, v6

    .line 69
    iget-object v9, p0, Leov;->cgP:[[I

    .line 70
    iget-object v10, p0, Leov;->cgO:[[I

    .line 71
    const/4 v0, 0x1

    move v4, v0

    :goto_0
    if-gt v4, v7, :cond_4

    .line 72
    add-int/lit8 v0, v4, -0x1

    aget-object v11, v5, v0

    .line 73
    const/4 v0, 0x1

    move v3, v0

    :goto_1
    if-gt v3, v8, :cond_3

    .line 74
    add-int/lit8 v0, v3, -0x1

    aget-object v0, v6, v0

    .line 75
    invoke-virtual {v11, v0}, Leox;->a(Leox;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 77
    :goto_2
    add-int/lit8 v1, v4, -0x1

    aget-object v1, v9, v1

    aget v1, v1, v3

    add-int/lit8 v2, v1, 0x1

    .line 78
    const/4 v1, 0x0

    .line 80
    aget-object v12, v9, v4

    add-int/lit8 v13, v3, -0x1

    aget v12, v12, v13

    .line 81
    add-int/lit8 v13, v12, 0x1

    if-ge v13, v2, :cond_0

    .line 82
    add-int/lit8 v2, v12, 0x1

    .line 83
    const/4 v1, 0x1

    .line 86
    :cond_0
    add-int/lit8 v12, v4, -0x1

    aget-object v12, v9, v12

    add-int/lit8 v13, v3, -0x1

    aget v12, v12, v13

    .line 87
    add-int v13, v12, v0

    if-ge v13, v2, :cond_5

    .line 88
    add-int v2, v12, v0

    .line 89
    if-nez v0, :cond_2

    const/4 v0, 0x3

    .line 91
    :goto_3
    aget-object v1, v9, v4

    aput v2, v1, v3

    .line 92
    aget-object v1, v10, v4

    aput v0, v1, v3

    .line 73
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 75
    :cond_1
    const/4 v0, 0x1

    goto :goto_2

    .line 89
    :cond_2
    const/4 v0, 0x2

    goto :goto_3

    .line 71
    :cond_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 95
    :cond_4
    aget-object v0, v9, v7

    aget v0, v0, v8

    return v0

    :cond_5
    move v0, v1

    goto :goto_3
.end method

.method public final ava()[Leow;
    .locals 6

    .prologue
    .line 107
    iget-object v0, p0, Leov;->cgN:[Leox;

    array-length v1, v0

    .line 108
    new-array v2, v1, [Leow;

    .line 110
    iget-object v0, p0, Leov;->cgM:[Leox;

    array-length v0, v0

    .line 111
    iget-object v3, p0, Leov;->cgO:[[I

    .line 112
    :goto_0
    if-lez v1, :cond_0

    .line 113
    aget-object v4, v3, v0

    aget v4, v4, v1

    .line 114
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 116
    :pswitch_0
    add-int/lit8 v0, v0, -0x1

    .line 117
    goto :goto_0

    .line 119
    :pswitch_1
    add-int/lit8 v1, v1, -0x1

    .line 120
    new-instance v5, Leow;

    invoke-direct {v5, v4, v0}, Leow;-><init>(II)V

    aput-object v5, v2, v1

    goto :goto_0

    .line 124
    :pswitch_2
    add-int/lit8 v1, v1, -0x1

    .line 125
    add-int/lit8 v0, v0, -0x1

    .line 126
    new-instance v5, Leow;

    invoke-direct {v5, v4, v0}, Leow;-><init>(II)V

    aput-object v5, v2, v1

    goto :goto_0

    .line 131
    :cond_0
    return-object v2

    .line 114
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
