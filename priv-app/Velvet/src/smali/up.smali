.class public final Lup;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ltz;


# instance fields
.field private FZ:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const/4 v0, 0x0

    iput v0, p0, Lup;->FZ:I

    .line 74
    check-cast p1, Lcom/android/launcher3/Launcher;

    .line 75
    invoke-virtual {p1}, Lcom/android/launcher3/Launcher;->hx()Lty;

    move-result-object v0

    invoke-virtual {v0, p0}, Lty;->a(Ltz;)V

    .line 76
    return-void
.end method


# virtual methods
.method public final a(Lui;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 94
    iget v0, p0, Lup;->FZ:I

    if-eqz v0, :cond_0

    .line 95
    const-string v0, "DropTarget"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDragEnter: Drag contract violated: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lup;->FZ:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :cond_0
    return-void
.end method

.method public final eK()V
    .locals 3

    .prologue
    .line 101
    iget v0, p0, Lup;->FZ:I

    if-eqz v0, :cond_0

    .line 102
    const-string v0, "DropTarget"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDragExit: Drag contract violated: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lup;->FZ:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :cond_0
    return-void
.end method

.method public final fe()V
    .locals 3

    .prologue
    .line 79
    iget v0, p0, Lup;->FZ:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lup;->FZ:I

    .line 80
    iget v0, p0, Lup;->FZ:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 81
    const-string v0, "DropTarget"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDragEnter: Drag contract violated: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lup;->FZ:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :cond_0
    return-void
.end method

.method public final ff()V
    .locals 3

    .prologue
    .line 86
    iget v0, p0, Lup;->FZ:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lup;->FZ:I

    .line 87
    iget v0, p0, Lup;->FZ:I

    if-eqz v0, :cond_0

    .line 88
    const-string v0, "DropTarget"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDragExit: Drag contract violated: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lup;->FZ:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :cond_0
    return-void
.end method
