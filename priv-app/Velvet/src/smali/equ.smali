.class public Lequ;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private volatile bJI:I

.field private volatile chA:Leqv;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, -0x1

    iput v0, p0, Lequ;->bJI:I

    .line 19
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Leqv;)V
    .locals 1

    .prologue
    .line 50
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lequ;->chA:Leqv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    monitor-exit p0

    return-void

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final afQ()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lequ;->bJI:I

    return v0
.end method

.method public final declared-synchronized b(Leqv;)V
    .locals 1

    .prologue
    .line 54
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lequ;->chA:Leqv;

    if-ne v0, p1, :cond_0

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lequ;->chA:Leqv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    :cond_0
    monitor-exit p0

    return-void

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final gp(I)V
    .locals 2

    .prologue
    .line 24
    if-ltz p1, :cond_0

    const/16 v0, 0x64

    if-le p1, v0, :cond_1

    :cond_0
    const/4 v0, -0x1

    if-ne p1, v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 26
    iput p1, p0, Lequ;->bJI:I

    .line 27
    iget-object v0, p0, Lequ;->chA:Leqv;

    if-eqz v0, :cond_2

    iget v1, p0, Lequ;->bJI:I

    invoke-interface {v0, v1}, Leqv;->fw(I)V

    .line 28
    :cond_2
    return-void

    .line 24
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final reset()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lequ;->gp(I)V

    .line 36
    return-void
.end method
