.class public final Lcnj;
.super Lepk;
.source "PG"


# instance fields
.field private mContactAccountLogger:Lcnh;

.field private final mGsaPreferenceController:Lchr;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lchr;Lcfo;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lchr;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lcfo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 37
    const-string v0, "ContactAccountLoggerTask"

    const/4 v1, 0x0

    new-array v1, v1, [I

    invoke-direct {p0, v0, v1}, Lepk;-><init>(Ljava/lang/String;[I)V

    .line 38
    iput-object p2, p0, Lcnj;->mGsaPreferenceController:Lchr;

    .line 39
    new-instance v0, Lcnh;

    invoke-virtual {p3}, Lcfo;->DL()Lcrh;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcnh;-><init>(Landroid/content/Context;Lcrh;)V

    iput-object v0, p0, Lcnj;->mContactAccountLogger:Lcnh;

    .line 41
    return-void
.end method

.method public static a(Lcfo;)Z
    .locals 5
    .param p0    # Lcfo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x4

    const/4 v0, 0x0

    .line 60
    invoke-virtual {p0}, Lcfo;->BL()Lchk;

    move-result-object v1

    .line 61
    invoke-virtual {v1}, Lchk;->FQ()Z

    move-result v1

    if-nez v1, :cond_0

    .line 62
    const-string v1, "ContactAccountLoggerTask"

    const-string v2, "canRun() : Disabled"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 72
    :goto_0
    return v0

    .line 66
    :cond_0
    invoke-virtual {p0}, Lcfo;->BK()Lcke;

    move-result-object v1

    .line 67
    invoke-interface {v1}, Lcke;->CS()Z

    move-result v1

    if-nez v1, :cond_1

    .line 68
    const-string v1, "ContactAccountLoggerTask"

    const-string v2, "canRun() : Opted Out"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 72
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lchr;Lcfo;J)Z
    .locals 10
    .param p0    # Lchr;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Lcfo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 54
    invoke-static {p1}, Lcnj;->a(Lcfo;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lchr;->Kt()Lcyg;

    move-result-object v2

    const-string v3, "KEY_CONTACT_ACCOUNT_LOGGER_TIMESTAMP"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    sub-long v2, p2, v2

    invoke-virtual {p1}, Lcfo;->BL()Lchk;

    move-result-object v4

    invoke-virtual {v4}, Lchk;->FR()I

    move-result v4

    int-to-long v4, v4

    const-wide/16 v6, 0x18

    mul-long/2addr v4, v6

    const-wide/32 v6, 0x36ee80

    mul-long/2addr v4, v6

    const-string v6, "ContactAccountLoggerTask"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "isAfterMinInterval() : MsSinceLastRun = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " : MinIntervalMs = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v8, v1, [Ljava/lang/Object;

    const/4 v9, 0x4

    invoke-static {v9, v6, v7, v8}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcnh;)Lcnj;
    .locals 0
    .param p1    # Lcnh;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 46
    iput-object p1, p0, Lcnj;->mContactAccountLogger:Lcnh;

    .line 47
    return-object p0
.end method

.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcnj;->ub()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final ub()Ljava/lang/Void;
    .locals 4

    .prologue
    .line 93
    const-string v0, "ContactAccountLoggerTask"

    const-string v1, "call()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x4

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 94
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 95
    iget-object v2, p0, Lcnj;->mContactAccountLogger:Lcnh;

    invoke-virtual {v2, v0, v1}, Lcnh;->aa(J)Z

    .line 98
    iget-object v2, p0, Lcnj;->mGsaPreferenceController:Lchr;

    invoke-virtual {v2}, Lchr;->Kt()Lcyg;

    move-result-object v2

    invoke-interface {v2}, Lcyg;->EH()Lcyh;

    move-result-object v2

    const-string v3, "KEY_CONTACT_ACCOUNT_LOGGER_TIMESTAMP"

    invoke-interface {v2, v3, v0, v1}, Lcyh;->c(Ljava/lang/String;J)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 101
    const/4 v0, 0x0

    return-object v0
.end method
