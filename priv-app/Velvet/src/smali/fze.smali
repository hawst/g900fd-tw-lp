.class public final Lfze;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final cDC:Ljava/util/List;

.field final mActivityHelper:Lfzw;

.field final mCardContainer:Lfmt;

.field final mClock:Lemp;

.field final mContext:Landroid/content/Context;

.field private final mEntry:Lizj;

.field private final mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Lfzw;Lizj;Lemp;)V
    .locals 6

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lfze;->mContext:Landroid/content/Context;

    .line 71
    iput-object p2, p0, Lfze;->mCardContainer:Lfmt;

    .line 72
    iput-object p3, p0, Lfze;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 73
    iput-object p4, p0, Lfze;->mActivityHelper:Lfzw;

    .line 74
    iput-object p5, p0, Lfze;->mEntry:Lizj;

    .line 75
    iput-object p6, p0, Lfze;->mClock:Lemp;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfze;->cDC:Ljava/util/List;

    .line 77
    iget-object v0, p0, Lfze;->mEntry:Lizj;

    iget-object v1, v0, Lizj;->dUo:[Liwk;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 78
    invoke-virtual {v3}, Liwk;->getType()I

    move-result v4

    const/16 v5, 0xd8

    if-ne v4, v5, :cond_0

    .line 79
    iget-object v4, p0, Lfze;->cDC:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 82
    :cond_1
    return-void
.end method

.method private static a(Landroid/view/View;Lapg;)Landroid/text/SpannableStringBuilder;
    .locals 4

    .prologue
    .line 288
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 289
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lapg;->pb()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lapg;->sK()Z

    move-result v1

    if-nez v1, :cond_1

    .line 301
    :cond_0
    :goto_0
    return-object v0

    .line 294
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lapg;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lapg;->sJ()I

    move-result v3

    invoke-static {v1, v2, v3}, Lgab;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 296
    invoke-virtual {p1}, Lapg;->hasText()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 298
    const-string v1, "  "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 299
    invoke-virtual {p1}, Lapg;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method private a(Landroid/widget/LinearLayout;Lapi;Z)Z
    .locals 12

    .prologue
    .line 309
    iget-object v0, p0, Lfze;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04013b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 311
    iget-object v0, p0, Lfze;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 313
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 314
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 315
    iget-object v0, p0, Lfze;->mContext:Landroid/content/Context;

    invoke-static {v0}, Leot;->au(Landroid/content/Context;)I

    move-result v0

    iget-object v2, p0, Lfze;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0d0136

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x41f00000    # 30.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sub-int v4, v0, v1

    .line 319
    invoke-virtual {p2}, Lapi;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 320
    const v0, 0x7f110359

    const-string v1, "http://maps.gstatic.com/mapfiles/transit/iw2/8/walk.png"

    invoke-direct {p0, v3, v0, v1}, Lfze;->d(Landroid/view/View;ILjava/lang/String;)V

    .line 330
    :cond_0
    if-nez p3, :cond_1

    .line 331
    const v0, 0x7f11035b

    const-string v1, "http://maps.gstatic.com/tactile/directions/cards/arrow-right-2x.png"

    invoke-direct {p0, v3, v0, v1}, Lfze;->d(Landroid/view/View;ILjava/lang/String;)V

    .line 337
    :cond_1
    invoke-virtual {p1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 338
    invoke-static {p1}, Lfze;->bt(Landroid/view/View;)I

    move-result v0

    if-gt v0, v4, :cond_6

    .line 339
    const/4 v0, 0x1

    .line 347
    :goto_0
    return v0

    .line 322
    :cond_2
    invoke-virtual {p2}, Lapi;->pt()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    const v0, 0x7f110359

    invoke-virtual {p2}, Lapi;->ps()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v3, v0, v1}, Lfze;->d(Landroid/view/View;ILjava/lang/String;)V

    .line 324
    const v0, 0x7f11035a

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 325
    iget-object v5, p2, Lapi;->all:[Laph;

    array-length v6, v5

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_0

    aget-object v7, v5, v2

    .line 326
    iget-object v1, p0, Lfze;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v8, 0x7f040138

    const/4 v9, 0x0

    invoke-virtual {v1, v8, v0, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v8, p0, Lfze;->mContext:Landroid/content/Context;

    invoke-static {v8}, Leot;->au(Landroid/content/Context;)I

    move-result v8

    int-to-double v8, v8

    const-wide v10, 0x3fd51eb851eb851fL    # 0.33

    mul-double/2addr v8, v10

    invoke-virtual {v7}, Laph;->sO()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-virtual {v7}, Laph;->sN()I

    move-result v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setBackgroundColor(I)V

    :cond_3
    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 325
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 326
    :cond_4
    invoke-virtual {v7}, Laph;->oK()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-virtual {v7}, Laph;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v7}, Laph;->hasBackgroundColor()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-virtual {v7}, Laph;->hasForegroundColor()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-virtual {v7}, Laph;->getBackgroundColor()I

    move-result v10

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setBackgroundColor(I)V

    invoke-virtual {v7}, Laph;->getForegroundColor()I

    move-result v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_5
    invoke-static {v1}, Lfze;->bt(Landroid/view/View;)I

    move-result v7

    int-to-double v10, v7

    cmpl-double v7, v10, v8

    if-lez v7, :cond_3

    const-string v7, ""

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 341
    :cond_6
    invoke-virtual {p1, v3}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 343
    iget-object v0, p0, Lfze;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040138

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 345
    const-string v1, "\u2026"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 346
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 347
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public static bA(Landroid/view/View;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 245
    const v0, 0x7f11031a

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 246
    const v0, 0x7f11034d

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 247
    const v0, 0x7f11034e

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 248
    const v0, 0x7f11034f

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 249
    const v0, 0x7f11028a

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 250
    const v0, 0x7f110350

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 251
    const v0, 0x7f110351

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 252
    return-void
.end method

.method private static bt(Landroid/view/View;)I
    .locals 1

    .prologue
    const/4 v0, -0x2

    .line 373
    invoke-virtual {p0, v0, v0}, Landroid/view/View;->measure(II)V

    .line 374
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    return v0
.end method

.method private d(Landroid/view/View;ILjava/lang/String;)V
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 378
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 379
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    .line 380
    iget-object v1, p0, Lfze;->mCardContainer:Lfmt;

    invoke-interface {v1}, Lfmt;->aAD()Lfml;

    move-result-object v1

    iget-object v1, v1, Lfml;->cvY:Lfnh;

    invoke-virtual {v0, p3, v1}, Lcom/google/android/search/shared/ui/WebImageView;->a(Ljava/lang/String;Lesm;)V

    .line 381
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/WebImageView;->setVisibility(I)V

    .line 383
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lapc;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 191
    invoke-static {p1}, Lfze;->bA(Landroid/view/View;)V

    .line 193
    invoke-virtual {p2}, Lapc;->rX()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    const v0, 0x7f11031a

    invoke-virtual {p2}, Lapc;->rW()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 197
    :cond_0
    iget-object v0, p2, Lapc;->akH:[Lapi;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 198
    const v0, 0x7f11034d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 199
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 200
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    move v1, v2

    .line 201
    :goto_0
    iget-object v3, p2, Lapc;->akH:[Lapi;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 202
    iget-object v3, p2, Lapc;->akH:[Lapi;

    aget-object v5, v3, v1

    iget-object v3, p2, Lapc;->akH:[Lapi;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_1

    move v3, v4

    :goto_1
    invoke-direct {p0, v0, v5, v3}, Lfze;->a(Landroid/widget/LinearLayout;Lapi;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 204
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v3, v2

    .line 202
    goto :goto_1

    .line 209
    :cond_2
    invoke-virtual {p2}, Lapc;->sb()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 210
    const v0, 0x7f11034e

    iget-object v1, p0, Lfze;->mContext:Landroid/content/Context;

    const v3, 0x7f0a01bf

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, ""

    aput-object v6, v5, v2

    const-string v2, ""

    aput-object v2, v5, v4

    invoke-virtual {v1, v3, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 213
    const v0, 0x7f11034f

    invoke-virtual {p2}, Lapc;->sa()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 215
    invoke-virtual {p2}, Lapc;->rZ()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 216
    const v0, 0x7f11028a

    invoke-virtual {p2}, Lapc;->rY()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lfze;->d(Landroid/view/View;ILjava/lang/String;)V

    .line 218
    :cond_3
    invoke-virtual {p2}, Lapc;->sd()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 219
    const v0, 0x7f110350

    invoke-virtual {p2}, Lapc;->sc()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 223
    :cond_4
    invoke-virtual {p2}, Lapc;->sf()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 224
    invoke-virtual {p2}, Lapc;->se()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 227
    :cond_5
    new-instance v0, Lfzg;

    iget-object v2, p0, Lfze;->mCardContainer:Lfmt;

    iget-object v3, p0, Lfze;->mEntry:Lizj;

    const/16 v4, 0x3a

    move-object v1, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lfzg;-><init>(Lfze;Lfmt;Lizj;ILapc;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 237
    iget-object v0, p2, Lapc;->akM:Lapg;

    invoke-static {p1, v0}, Lfze;->a(Landroid/view/View;Lapg;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 238
    const v1, 0x7f110351

    invoke-static {p1, v1, v0}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 239
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;Lapj;Z)V
    .locals 11

    .prologue
    .line 93
    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 94
    invoke-virtual {p2}, Lapj;->sQ()Z

    move-result v9

    .line 95
    if-nez p3, :cond_a

    iget-object v0, p2, Lapj;->alm:[Lapf;

    array-length v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    move v8, v0

    .line 96
    :goto_0
    if-eqz v8, :cond_0

    .line 99
    iget-object v0, p2, Lapj;->alm:[Lapf;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lapf;->sz()Lapf;

    .line 101
    :cond_0
    const/4 v5, 0x0

    :goto_1
    iget-object v0, p2, Lapj;->alm:[Lapf;

    array-length v0, v0

    if-ge v5, v0, :cond_10

    .line 102
    iget-object v0, p2, Lapj;->alm:[Lapf;

    aget-object v7, v0, v5

    iget-object v0, p0, Lfze;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04013a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    if-eqz v8, :cond_b

    invoke-virtual {v10}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d018e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    move v1, v0

    :goto_2
    if-eqz v9, :cond_c

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {v10}, Landroid/view/View;->getPaddingEnd()I

    move-result v2

    invoke-virtual {v10}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {v10, v1, v0, v2, v3}, Landroid/view/View;->setPaddingRelative(IIII)V

    invoke-virtual {v7}, Lapf;->sw()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f110354

    invoke-virtual {v7}, Lapf;->sv()Ljava/lang/String;

    move-result-object v1

    invoke-static {v10, v0, v1}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    :cond_1
    invoke-virtual {v7}, Lapf;->sy()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f110355

    invoke-virtual {v7}, Lapf;->sx()Ljava/lang/String;

    move-result-object v1

    invoke-static {v10, v0, v1}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    :cond_2
    invoke-virtual {v7}, Lapf;->sB()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f110357

    invoke-virtual {v7}, Lapf;->sA()Ljava/lang/String;

    move-result-object v1

    invoke-static {v10, v0, v1}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    :cond_3
    invoke-virtual {v7}, Lapf;->se()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v7}, Lapf;->se()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_4
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v7}, Lapf;->sD()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v7}, Lapf;->sC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_5
    invoke-virtual {v7}, Lapf;->sF()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_6

    const-string v1, "  "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_6
    invoke-virtual {v10}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    const/4 v2, 0x1

    const/high16 v3, 0x41400000    # 12.0f

    invoke-static {v2, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    invoke-virtual {v10}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02023c

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    const/4 v4, 0x1

    invoke-static {v2, v3, v1, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/text/style/ImageSpan;

    invoke-virtual {v10}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v2, v3, v1, v4}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;I)V

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x11

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v7}, Lapf;->sE()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_7
    invoke-virtual {v7}, Lapf;->sH()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_8

    const-string v1, "  "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_8
    invoke-virtual {v7}, Lapf;->sG()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_9
    const v1, 0x7f110358

    invoke-static {v10, v1, v0}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    iget-object v0, v7, Lapf;->akH:[Lapi;

    array-length v0, v0

    if-lez v0, :cond_e

    const v0, 0x7f110356

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    const/4 v1, 0x0

    :goto_4
    iget-object v2, v7, Lapf;->akH:[Lapi;

    array-length v2, v2

    if-ge v1, v2, :cond_e

    iget-object v2, v7, Lapf;->akH:[Lapi;

    aget-object v3, v2, v1

    iget-object v2, v7, Lapf;->akH:[Lapi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_d

    const/4 v2, 0x1

    :goto_5
    invoke-direct {p0, v0, v3, v2}, Lfze;->a(Landroid/widget/LinearLayout;Lapi;Z)Z

    move-result v2

    if-eqz v2, :cond_e

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 95
    :cond_a
    const/4 v0, 0x0

    move v8, v0

    goto/16 :goto_0

    .line 102
    :cond_b
    invoke-virtual {v10}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0136

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    move v1, v0

    goto/16 :goto_2

    :cond_c
    invoke-virtual {v10}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0139

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    goto/16 :goto_3

    :cond_d
    const/4 v2, 0x0

    goto :goto_5

    :cond_e
    iget-object v0, v7, Lapf;->akM:Lapg;

    invoke-static {v10, v0}, Lfze;->a(Landroid/view/View;Lapg;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    const v1, 0x7f110351

    invoke-static {v10, v1, v0}, Lgab;->b(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    invoke-virtual {v7}, Lapf;->sI()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lfze;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v10, v0}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_f
    new-instance v0, Lfzf;

    iget-object v2, p0, Lfze;->mCardContainer:Lfmt;

    iget-object v3, p0, Lfze;->mEntry:Lizj;

    const/16 v4, 0x3a

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v7}, Lfzf;-><init>(Lfze;Lfmt;Lizj;IILandroid/view/ViewGroup;Lapf;)V

    invoke-virtual {v10, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 101
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 104
    :cond_10
    return-void
.end method
