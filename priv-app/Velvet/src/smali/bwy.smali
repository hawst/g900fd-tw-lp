.class public final Lbwy;
.super Landroid/app/Activity;
.source "PG"


# direct methods
.method public static em(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    packed-switch p0, :pswitch_data_0

    .line 72
    const-string v0, "unknown/auto"

    :goto_0
    return-object v0

    .line 64
    :pswitch_0
    const-string v0, "(HFOA onStop) home/back/nonRevivalAction"

    goto :goto_0

    .line 66
    :pswitch_1
    const-string v0, "notification"

    goto :goto_0

    .line 68
    :pswitch_2
    const-string v0, "1-sec delay"

    goto :goto_0

    .line 70
    :pswitch_3
    const-string v0, "voice"

    goto :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    new-instance v0, Lbyl;

    invoke-direct {v0}, Lbyl;-><init>()V

    .line 51
    invoke-static {}, Lbyl;->BW()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lbyl;->BT()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.search.core.action.EXIT_MANUAL_CAR_MODE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 53
    const-class v1, Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 54
    const-string v1, "exit-point"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 56
    invoke-virtual {p0, v0}, Lbwy;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 58
    :cond_0
    invoke-virtual {p0}, Lbwy;->finish()V

    .line 59
    return-void
.end method
