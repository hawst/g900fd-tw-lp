.class public final Lfld;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cuJ:Lfkz;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final jr:Landroid/view/ViewGroup;

.field private final mActivity:Landroid/app/Activity;

.field private mCardContainer:Lfmt;

.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field private final mStringEvaluator:Lgbr;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lfmt;Lgbr;Lfkz;)V
    .locals 0
    .param p6    # Lfkz;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lfld;->mActivity:Landroid/app/Activity;

    .line 75
    iput-object p2, p0, Lfld;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 76
    iput-object p3, p0, Lfld;->jr:Landroid/view/ViewGroup;

    .line 77
    iput-object p4, p0, Lfld;->mCardContainer:Lfmt;

    .line 78
    iput-object p5, p0, Lfld;->mStringEvaluator:Lgbr;

    .line 79
    iput-object p6, p0, Lfld;->cuJ:Lfkz;

    .line 80
    return-void
.end method

.method private a(Landroid/view/View;Lfkd;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 129
    const v0, 0x7f11000a

    invoke-virtual {p1, v0, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 130
    const v0, 0x7f110002

    invoke-interface {p2}, Lfkd;->getEntry()Lizj;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 134
    invoke-interface {p2}, Lfkd;->aAw()Lizj;

    move-result-object v0

    .line 135
    if-eqz v0, :cond_0

    .line 136
    const v1, 0x7f110003

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 139
    :cond_0
    invoke-interface {p2}, Lfkd;->aAw()Lizj;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lfld;->mCardContainer:Lfmt;

    const/4 v2, 0x2

    new-array v2, v2, [Lizj;

    invoke-interface {p2}, Lfkd;->getEntry()Lizj;

    move-result-object v3

    aput-object v3, v2, v4

    aput-object v0, v2, v5

    invoke-interface {v1, p1, v2}, Lfmt;->a(Landroid/view/View;[Lizj;)V

    .line 140
    :goto_0
    return-void

    .line 139
    :cond_1
    iget-object v0, p0, Lfld;->mCardContainer:Lfmt;

    new-array v1, v5, [Lizj;

    invoke-interface {p2}, Lfkd;->getEntry()Lizj;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-interface {v0, p1, v1}, Lfmt;->a(Landroid/view/View;[Lizj;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lfkd;)Landroid/view/View;
    .locals 4

    .prologue
    .line 100
    iget-object v0, p0, Lfld;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lfld;->mCardContainer:Lfmt;

    iget-object v2, p0, Lfld;->mLayoutInflater:Landroid/view/LayoutInflater;

    iget-object v3, p0, Lfld;->jr:Landroid/view/ViewGroup;

    invoke-interface {p1, v0, v1, v2, v3}, Lfkd;->a(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 101
    iget-object v1, p0, Lfld;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lfld;->mCardContainer:Lfmt;

    invoke-interface {p1, v1, v2, v0}, Lfkd;->a(Landroid/app/Activity;Lfmt;Landroid/view/View;)V

    .line 102
    iget-object v1, p0, Lfld;->mCardContainer:Lfmt;

    invoke-interface {p1, v1, v0}, Lfkd;->a(Lfmt;Landroid/view/View;)V

    .line 103
    iget-object v1, p0, Lfld;->mStringEvaluator:Lgbr;

    invoke-interface {p1, v0, v1}, Lfkd;->a(Landroid/view/View;Lgbr;)V

    .line 104
    invoke-direct {p0, v0, p1}, Lfld;->a(Landroid/view/View;Lfkd;)V

    .line 105
    return-object v0
.end method

.method public final a(Lfkd;Landroid/view/View;)Landroid/view/View;
    .locals 10
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x0

    .line 170
    iget-object v0, p0, Lfld;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lfld;->mCardContainer:Lfmt;

    invoke-interface {p1, v0, v1}, Lfkd;->a(Landroid/app/Activity;Lfmt;)Lfry;

    move-result-object v0

    .line 171
    if-eqz v0, :cond_0

    move-object v2, v0

    .line 173
    :goto_0
    if-nez v2, :cond_4

    move-object v0, v6

    .line 189
    :goto_1
    return-object v0

    .line 171
    :cond_0
    iget-object v7, p0, Lfld;->mCardContainer:Lfmt;

    invoke-interface {p1}, Lfkd;->getEntry()Lizj;

    move-result-object v8

    invoke-virtual {v8}, Lizj;->bda()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v2, v6

    goto :goto_0

    :cond_1
    const/16 v0, 0x19

    new-array v1, v9, [I

    invoke-static {v8, v0, v1}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v3

    const/16 v0, 0x1a

    new-array v1, v9, [I

    invoke-static {v8, v0, v1}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v2

    if-eqz v3, :cond_2

    if-nez v2, :cond_3

    :cond_2
    move-object v2, v6

    goto :goto_0

    :cond_3
    new-instance v0, Lfry;

    invoke-virtual {v8}, Lizj;->bcZ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Liwk;->aZo()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Liwk;->aZo()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lfle;

    invoke-direct {v4, p0, v7, v8}, Lfle;-><init>(Lfld;Lfmt;Lizj;)V

    new-instance v5, Lflf;

    invoke-direct {v5, p0, v7, v8}, Lflf;-><init>(Lfld;Lfmt;Lizj;)V

    invoke-direct/range {v0 .. v5}, Lfry;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lfrz;Lfrz;)V

    move-object v2, v0

    goto :goto_0

    .line 174
    :cond_4
    iget-object v0, p0, Lfld;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040036

    iget-object v3, p0, Lfld;->jr:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 176
    const v0, 0x7f1100f9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 177
    iget-object v3, v2, Lfry;->bLS:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 180
    const v0, 0x7f1100fb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 181
    iget-object v3, v2, Lfry;->cBp:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 182
    new-instance v3, Lflg;

    iget-object v4, p0, Lfld;->mActivity:Landroid/app/Activity;

    const/4 v5, 0x1

    invoke-direct {v3, v4, v2, v5, v1}, Lflg;-><init>(Landroid/content/Context;Lfry;ILandroid/view/View;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    const v0, 0x7f1100fc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 186
    iget-object v3, v2, Lfry;->cBq:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 187
    new-instance v3, Lflg;

    iget-object v4, p0, Lfld;->mActivity:Landroid/app/Activity;

    const/4 v5, 0x2

    invoke-direct {v3, v4, v2, v5, v1}, Lflg;-><init>(Landroid/content/Context;Lfry;ILandroid/view/View;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, v1

    .line 189
    goto/16 :goto_1
.end method

.method public final a(Lfkd;Lfpu;)Landroid/view/View;
    .locals 4
    .param p2    # Lfpu;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 89
    invoke-virtual {p0, p1}, Lfld;->c(Lfkd;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lfld;->jr:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400a1

    iget-object v2, p0, Lfld;->jr:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/training/IcebreakerView;

    invoke-interface {p1}, Lfkd;->aAx()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfld;->mCardContainer:Lfmt;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->a(Lfmt;Lfkd;Lfpu;)V

    :goto_0
    invoke-direct {p0, v0, p1}, Lfld;->a(Landroid/view/View;Lfkd;)V

    .line 92
    :goto_1
    return-object v0

    .line 90
    :cond_0
    iget-object v1, p0, Lfld;->mCardContainer:Lfmt;

    new-instance v2, Lfqh;

    iget-object v3, p0, Lfld;->jr:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lfqh;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, p1, v2, p2}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->a(Lfmt;Lfkd;Lfqh;Lfpu;)V

    goto :goto_0

    .line 92
    :cond_1
    invoke-virtual {p0, p1}, Lfld;->a(Lfkd;)Landroid/view/View;

    move-result-object v0

    goto :goto_1
.end method

.method public final b(Lfkd;)Landroid/view/View;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 148
    invoke-static {p1}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->f(Lfkd;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfld;->cuJ:Lfkz;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lfld;->jr:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040038

    iget-object v2, p0, Lfld;->jr:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 151
    const v0, 0x7f1100fe

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 153
    invoke-virtual {v0}, Landroid/widget/ImageButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201a3

    const v4, 0x7f0b0134

    invoke-static {v2, v3, v4}, Lgai;->b(Landroid/content/res/Resources;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 156
    new-instance v2, Lfqu;

    iget-object v3, p0, Lfld;->cuJ:Lfkz;

    iget-object v4, p0, Lfld;->mCardContainer:Lfmt;

    invoke-direct {v2, v3, v4, p1}, Lfqu;-><init>(Lfkz;Lfmt;Lfkd;)V

    .line 158
    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, v1

    .line 161
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final c(Lfkd;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 197
    invoke-interface {p1}, Lfkd;->getEntry()Lizj;

    move-result-object v2

    .line 198
    iget-object v3, v2, Lizj;->dSa:Ljdj;

    if-nez v3, :cond_2

    .line 199
    invoke-virtual {v2}, Lizj;->getType()I

    move-result v2

    const/16 v3, 0x43

    if-eq v2, v3, :cond_0

    invoke-interface {p1}, Lfkd;->aAx()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 208
    :cond_1
    :goto_0
    return v0

    .line 204
    :cond_2
    iget-object v2, v2, Lizj;->dSa:Ljdj;

    iget-object v3, p0, Lfld;->mCardContainer:Lfmt;

    invoke-interface {v3}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBQ()Ljava/util/Collection;

    move-result-object v3

    invoke-static {v2, v3}, Lfqe;->a(Ljdj;Ljava/util/Collection;)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    move-result-object v2

    .line 208
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v2

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0
.end method
