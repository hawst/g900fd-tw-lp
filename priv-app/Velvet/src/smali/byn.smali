.class public final Lbyn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public aOT:Z

.field public aOU:Lbyv;

.field public final abz:Landroid/content/ServiceConnection;

.field public final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbyn;->aOT:Z

    .line 26
    iput-object p1, p0, Lbyn;->mContext:Landroid/content/Context;

    .line 27
    new-instance v0, Lbyo;

    invoke-direct {v0, p0}, Lbyo;-><init>(Lbyn;)V

    iput-object v0, p0, Lbyn;->abz:Landroid/content/ServiceConnection;

    .line 28
    return-void
.end method


# virtual methods
.method public final a(ZLbyy;)V
    .locals 4

    .prologue
    .line 52
    iget-boolean v0, p0, Lbyn;->aOT:Z

    if-nez v0, :cond_0

    const-string v0, "HotwordBenchmarkServiceClient"

    const-string v1, "Hotword benchmark service is not yet bound"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 54
    :cond_0
    :try_start_0
    iget-object v0, p0, Lbyn;->aOU:Lbyv;

    invoke-interface {v0, p1, p2}, Lbyv;->a(ZLbyy;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :goto_0
    return-void

    .line 55
    :catch_0
    move-exception v0

    .line 56
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
