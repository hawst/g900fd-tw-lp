.class public final Lczt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final bou:[B

.field public final bov:I

.field public final bow:Lczr;


# direct methods
.method public constructor <init>(Lczr;[BI)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lczt;->bow:Lczr;

    .line 31
    iput-object p2, p0, Lczt;->bou:[B

    .line 32
    iput p3, p0, Lczt;->bov:I

    .line 33
    return-void
.end method

.method public static a(Ljava/lang/String;[BLemp;)Lczt;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x5

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 67
    invoke-static {p1}, Lczw;->D([B)I

    move-result v4

    .line 68
    if-nez v4, :cond_0

    .line 69
    const-string v0, "Sdch.Dictionary"

    const-string v1, "Invalid header for dictionary: %s"

    new-array v2, v8, [Ljava/lang/Object;

    aput-object p0, v2, v6

    invoke-static {v7, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v3

    .line 134
    :goto_0
    return-object v0

    .line 73
    :cond_0
    add-int/lit8 v0, v4, 0x2

    array-length v1, p1

    if-lt v0, v1, :cond_1

    .line 74
    const-string v0, "Sdch.Dictionary"

    const-string v1, "Empty dictionary: %s"

    new-array v2, v8, [Ljava/lang/Object;

    aput-object p0, v2, v6

    invoke-static {v7, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v3

    .line 75
    goto :goto_0

    .line 78
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, v6, v4}, Ljava/lang/String;-><init>([BII)V

    invoke-static {v0}, Lczw;->jn(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    .line 81
    const-string v0, "domain"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 82
    const-string v1, "path"

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 83
    const-string v5, "format-version"

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 85
    if-nez v0, :cond_2

    .line 86
    const-string v0, "Sdch.Dictionary"

    const-string v1, "Dictionary missing Domain header"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v7, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v3

    .line 87
    goto :goto_0

    .line 90
    :cond_2
    if-nez v1, :cond_3

    .line 91
    const-string v0, "Sdch.Dictionary"

    const-string v1, "Dictionary missing path header"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v7, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v3

    .line 92
    goto :goto_0

    .line 97
    :cond_3
    if-eqz v2, :cond_4

    const-string v5, "1.0"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 98
    const-string v0, "Sdch.Dictionary"

    const-string v1, "Unsupported dictionary format: %s"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v2, v4, v6

    invoke-static {v7, v0, v1, v4}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v3

    .line 99
    goto :goto_0

    .line 102
    :cond_4
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 106
    invoke-virtual {v5}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 107
    const-string v1, "Sdch.Dictionary"

    const-string v2, "URL: %s cannot set :%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v6

    aput-object v0, v4, v8

    invoke-static {v7, v1, v2, v4}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v3

    .line 108
    goto/16 :goto_0

    .line 111
    :cond_5
    invoke-static {p1}, Lczw;->C([B)Landroid/util/Pair;

    move-result-object v5

    .line 112
    if-nez v5, :cond_6

    move-object v0, v3

    .line 115
    goto/16 :goto_0

    .line 118
    :cond_6
    new-instance v6, Lczr;

    invoke-direct {v6}, Lczr;-><init>()V

    .line 119
    iget-object v3, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iput-object v3, v6, Lczr;->boo:Ljava/lang/String;

    .line 120
    iget-object v3, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iput-object v3, v6, Lczr;->bop:Ljava/lang/String;

    .line 121
    iput-object v0, v6, Lczr;->boq:Ljava/lang/String;

    .line 122
    iput-object v1, v6, Lczr;->acp:Ljava/lang/String;

    .line 124
    if-eqz v2, :cond_7

    .line 125
    invoke-virtual {v6, v2}, Lczr;->jl(Ljava/lang/String;)Lczr;

    .line 132
    :goto_1
    invoke-interface {p2}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, v6, Lczr;->bor:J

    .line 134
    new-instance v0, Lczt;

    add-int/lit8 v1, v4, 0x2

    invoke-direct {v0, v6, p1, v1}, Lczt;-><init>(Lczr;[BI)V

    goto/16 :goto_0

    .line 127
    :cond_7
    const-string v0, "1.0"

    invoke-virtual {v6, v0}, Lczr;->jl(Ljava/lang/String;)Lczr;

    goto :goto_1
.end method

.method public static h(Ljava/io/InputStream;)Lczt;
    .locals 4

    .prologue
    .line 44
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 47
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    .line 48
    new-array v1, v1, [B

    .line 49
    invoke-virtual {v0, v1}, Ljava/io/DataInputStream;->readFully([B)V

    .line 50
    invoke-static {v1}, Lczr;->B([B)Lczr;

    move-result-object v1

    .line 53
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    .line 54
    new-array v2, v2, [B

    .line 55
    invoke-virtual {v0, v2}, Ljava/io/DataInputStream;->readFully([B)V

    .line 59
    new-instance v0, Lczt;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lczt;-><init>(Lczr;[BI)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/io/OutputStream;)V
    .locals 4

    .prologue
    .line 162
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, p1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 165
    iget-object v1, p0, Lczt;->bow:Lczr;

    invoke-virtual {v1}, Lczr;->buf()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 166
    iget-object v1, p0, Lczt;->bow:Lczr;

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->write([B)V

    .line 169
    invoke-virtual {p0}, Lczt;->getLength()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 170
    iget-object v1, p0, Lczt;->bou:[B

    iget v2, p0, Lczt;->bov:I

    invoke-virtual {p0}, Lczt;->getLength()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/DataOutputStream;->write([BII)V

    .line 171
    return-void
.end method

.method public final getLength()I
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lczt;->bou:[B

    array-length v0, v0

    iget v1, p0, Lczt;->bov:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 175
    const-string v0, "C:%s,S:%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lczt;->bow:Lczr;

    iget-object v3, v3, Lczr;->boo:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lczt;->bow:Lczr;

    iget-object v3, v3, Lczr;->bop:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
