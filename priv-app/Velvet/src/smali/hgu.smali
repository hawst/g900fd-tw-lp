.class public final Lhgu;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private synthetic dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;


# direct methods
.method public constructor <init>(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)V
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Lhgu;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 314
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 315
    const-string v1, "com.google.android.googlequicksearchbox.CHANGE_VOICESEARCH_LANGUAGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 316
    const-string v0, "language"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 321
    iget-object v1, p0, Lhgu;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v1}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->e(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 322
    iget-object v1, p0, Lhgu;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v1, v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->a(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;Ljava/lang/String;)V

    .line 334
    :cond_0
    :goto_0
    return-void

    .line 324
    :cond_1
    const-string v1, "com.google.android.googlequicksearchbox.interactor.RESTART_RECOGNITION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lhgu;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->k(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lhgu;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-static {v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->i(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Z

    .line 331
    iget-object v0, p0, Lhgu;->dgP:Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    const-string v1, "delayStartByMilliseconds"

    const-wide/16 v2, 0x0

    invoke-virtual {p2, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->a(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;J)V

    goto :goto_0
.end method
