.class public final Ljti;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eDs:[Ljti;


# instance fields
.field public eDt:Ljth;

.field public eDu:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 171
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 172
    iput-object v1, p0, Ljti;->eDt:Ljth;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljti;->eDu:[Ljava/lang/String;

    iput-object v1, p0, Ljti;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljti;->eCz:I

    .line 173
    return-void
.end method

.method public static but()[Ljti;
    .locals 2

    .prologue
    .line 154
    sget-object v0, Ljti;->eDs:[Ljti;

    if-nez v0, :cond_1

    .line 155
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 157
    :try_start_0
    sget-object v0, Ljti;->eDs:[Ljti;

    if-nez v0, :cond_0

    .line 158
    const/4 v0, 0x0

    new-array v0, v0, [Ljti;

    sput-object v0, Ljti;->eDs:[Ljti;

    .line 160
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    :cond_1
    sget-object v0, Ljti;->eDs:[Ljti;

    return-object v0

    .line 160
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 148
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljti;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljti;->eDt:Ljth;

    if-nez v0, :cond_1

    new-instance v0, Ljth;

    invoke-direct {v0}, Ljth;-><init>()V

    iput-object v0, p0, Ljti;->eDt:Ljth;

    :cond_1
    iget-object v0, p0, Ljti;->eDt:Ljth;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljti;->eDu:[Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljti;->eDu:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljti;->eDu:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljti;->eDu:[Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 186
    iget-object v0, p0, Ljti;->eDt:Ljth;

    if-eqz v0, :cond_0

    .line 187
    const/4 v0, 0x1

    iget-object v1, p0, Ljti;->eDt:Ljth;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 189
    :cond_0
    iget-object v0, p0, Ljti;->eDu:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljti;->eDu:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 190
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljti;->eDu:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 191
    iget-object v1, p0, Ljti;->eDu:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 192
    if-eqz v1, :cond_1

    .line 193
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 190
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 197
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 198
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 202
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 203
    iget-object v2, p0, Ljti;->eDt:Ljth;

    if-eqz v2, :cond_0

    .line 204
    const/4 v2, 0x1

    iget-object v3, p0, Ljti;->eDt:Ljth;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 207
    :cond_0
    iget-object v2, p0, Ljti;->eDu:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljti;->eDu:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    move v3, v1

    .line 210
    :goto_0
    iget-object v4, p0, Ljti;->eDu:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 211
    iget-object v4, p0, Ljti;->eDu:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 212
    if-eqz v4, :cond_1

    .line 213
    add-int/lit8 v3, v3, 0x1

    .line 214
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 210
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 218
    :cond_2
    add-int/2addr v0, v2

    .line 219
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 221
    :cond_3
    return v0
.end method
