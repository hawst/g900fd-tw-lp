.class public final Lfzd;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 32
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 13

    .prologue
    .line 41
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    check-cast v0, Landroid/widget/TableLayout;

    .line 42
    invoke-virtual {v0}, Landroid/widget/TableLayout;->removeAllViews()V

    .line 44
    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->agY:Lape;

    iget-object v2, v1, Lape;->akX:[Lapd;

    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x5

    if-ge v1, v3, :cond_6

    array-length v3, v2

    if-ge v1, v3, :cond_6

    aget-object v3, v2, v1

    invoke-virtual {v3}, Lapd;->sl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x2

    if-le v3, v4, :cond_5

    const/4 v1, 0x0

    move v2, v1

    .line 45
    :goto_1
    const/4 v3, 0x0

    .line 46
    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->agY:Lape;

    iget-object v5, v1, Lape;->akX:[Lapd;

    array-length v6, v5

    const/4 v1, 0x0

    move v4, v1

    move v1, v3

    :goto_2
    if-ge v4, v6, :cond_7

    aget-object v7, v5, v4

    .line 47
    invoke-virtual {p0}, Lfzd;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v8, 0x7f0401a8

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 48
    invoke-virtual {v7}, Lapd;->sk()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 49
    const v1, 0x7f110476

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/ui/WebImageView;

    .line 50
    invoke-virtual {v7}, Lapd;->sj()Ljava/lang/String;

    move-result-object v3

    iget-object v9, p0, Lfzd;->mCardContainer:Lfmt;

    invoke-interface {v9}, Lfmt;->aAD()Lfml;

    move-result-object v9

    iget-object v9, v9, Lfml;->cvY:Lfnh;

    invoke-virtual {v1, v3, v9}, Lcom/google/android/search/shared/ui/WebImageView;->a(Ljava/lang/String;Lesm;)V

    .line 52
    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v3}, Lcom/google/android/search/shared/ui/WebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 53
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/search/shared/ui/WebImageView;->setVisibility(I)V

    .line 54
    const/4 v1, 0x1

    move v3, v1

    .line 57
    :goto_3
    invoke-virtual {v7}, Lapd;->so()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v7}, Lapd;->sn()I

    move-result v1

    const/4 v9, -0x1

    if-eq v1, v9, :cond_0

    .line 58
    const v1, 0x7f110477

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 59
    invoke-virtual {v7}, Lapd;->sn()I

    move-result v9

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 60
    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 63
    :cond_0
    const v1, 0x7f110457

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 64
    invoke-virtual {v7}, Lapd;->sl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    if-eqz v2, :cond_1

    .line 68
    new-instance v9, Landroid/widget/TableRow$LayoutParams;

    const/4 v10, -0x2

    const/4 v11, -0x2

    const v12, 0x3d23d70a    # 0.04f

    invoke-direct {v9, v10, v11, v12}, Landroid/widget/TableRow$LayoutParams;-><init>(IIF)V

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    :cond_1
    invoke-virtual {v7}, Lapd;->sq()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 73
    const v1, 0x7f110314

    invoke-virtual {v7}, Lapd;->sp()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v1, v9}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 74
    invoke-virtual {v7}, Lapd;->st()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v7}, Lapd;->ss()I

    move-result v1

    const/4 v9, -0x1

    if-eq v1, v9, :cond_2

    .line 75
    const v1, 0x7f110314

    invoke-virtual {v7}, Lapd;->ss()I

    move-result v9

    invoke-static {v8, v1, v9}, Lgab;->i(Landroid/view/View;II)Landroid/widget/TextView;

    .line 78
    :cond_2
    invoke-virtual {v7}, Lapd;->sr()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 79
    const v1, 0x7f110479

    invoke-virtual {v7}, Lapd;->sr()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v1, v9}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 85
    :cond_3
    invoke-virtual {v7}, Lapd;->sm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 86
    const v1, 0x7f110478

    invoke-virtual {v7}, Lapd;->sm()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v1, v7}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 89
    :cond_4
    invoke-virtual {v0, v8}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 46
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v3

    goto/16 :goto_2

    .line 44
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_6
    const/4 v1, 0x1

    move v2, v1

    goto/16 :goto_1

    .line 92
    :cond_7
    if-nez v1, :cond_8

    .line 93
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TableLayout;->setColumnCollapsed(IZ)V

    .line 96
    :cond_8
    return-void

    :cond_9
    move v3, v1

    goto/16 :goto_3
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 36
    const v0, 0x7f040137

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
