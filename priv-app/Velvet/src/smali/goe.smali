.class public abstract Lgoe;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final cRf:Landroid/content/ContentProvider;


# direct methods
.method public constructor <init>(Landroid/content/ContentProvider;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lgoe;->cRf:Landroid/content/ContentProvider;

    .line 20
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 36
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 2

    .prologue
    .line 67
    array-length v0, p2

    .line 68
    const/4 v1, 0x0

    if-ge v1, v0, :cond_0

    .line 69
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 68
    :cond_0
    return v0
.end method

.method public abstract vE()Ljava/lang/String;
.end method
