.class public final Leiu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final caq:Lemy;

.field private car:Landroid/net/Uri;

.field cas:Landroid/net/Uri;

.field private final mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lemy;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Leiu;->mView:Landroid/view/View;

    .line 41
    iput-object p2, p0, Leiu;->caq:Lemy;

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Leiu;->mView:Landroid/view/View;

    .line 31
    new-instance v0, Leiv;

    invoke-direct {v0, p0, p1}, Leiv;-><init>(Leiu;Landroid/widget/ImageView;)V

    iput-object v0, p0, Leiu;->caq:Lemy;

    .line 37
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;Landroid/net/Uri;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 104
    iput-object p2, p0, Leiu;->car:Landroid/net/Uri;

    .line 108
    iget-object v0, p0, Leiu;->caq:Lemy;

    invoke-interface {v0, p1}, Lemy;->aj(Ljava/lang/Object;)Z

    .line 110
    if-nez p1, :cond_0

    .line 111
    iget-object v0, p0, Leiu;->mView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 123
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v0, p0, Leiu;->mView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 120
    invoke-virtual {p1, v1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 121
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    goto :goto_0
.end method


# virtual methods
.method final a(Landroid/graphics/drawable/Drawable;Landroid/net/Uri;Ljava/lang/String;Lesm;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 92
    if-eqz p1, :cond_0

    .line 93
    invoke-direct {p0, p1, p2}, Leiu;->a(Landroid/graphics/drawable/Drawable;Landroid/net/Uri;)V

    .line 101
    :goto_0
    return-void

    .line 94
    :cond_0
    if-eqz p3, :cond_1

    .line 95
    iget-object v0, p0, Leiu;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0202c3

    invoke-static {v0, v1}, Lesp;->y(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 97
    invoke-virtual {p0, p3, v0, p4}, Leiu;->a(Ljava/lang/String;Ljava/lang/String;Lesm;)V

    goto :goto_0

    .line 99
    :cond_1
    invoke-direct {p0, v0, v0}, Leiu;->a(Landroid/graphics/drawable/Drawable;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lesm;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 46
    if-eqz p1, :cond_2

    .line 47
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 48
    iput-object v1, p0, Leiu;->cas:Landroid/net/Uri;

    .line 49
    iget-object v0, p0, Leiu;->cas:Landroid/net/Uri;

    iget-object v2, p0, Leiu;->car:Landroid/net/Uri;

    invoke-static {v0, v2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    invoke-interface {p3, v1}, Lesm;->D(Landroid/net/Uri;)Leml;

    move-result-object v0

    .line 52
    invoke-interface {v0}, Leps;->auB()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 54
    invoke-interface {v0}, Leps;->auC()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0, v1, p2, p3}, Leiu;->a(Landroid/graphics/drawable/Drawable;Landroid/net/Uri;Ljava/lang/String;Lesm;)V

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    invoke-direct {p0, v3, v3}, Leiu;->a(Landroid/graphics/drawable/Drawable;Landroid/net/Uri;)V

    .line 59
    new-instance v2, Leiw;

    invoke-direct {v2, p0, v1, p2, p3}, Leiw;-><init>(Leiu;Landroid/net/Uri;Ljava/lang/String;Lesm;)V

    invoke-interface {v0, v2}, Leps;->e(Lemy;)V

    goto :goto_0

    .line 76
    :cond_2
    iput-object v3, p0, Leiu;->cas:Landroid/net/Uri;

    .line 77
    invoke-virtual {p0, v3, v3, p2, p3}, Leiu;->a(Landroid/graphics/drawable/Drawable;Landroid/net/Uri;Ljava/lang/String;Lesm;)V

    goto :goto_0
.end method

.method public final j(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 83
    iput-object v0, p0, Leiu;->cas:Landroid/net/Uri;

    .line 84
    invoke-direct {p0, p1, v0}, Leiu;->a(Landroid/graphics/drawable/Drawable;Landroid/net/Uri;)V

    .line 85
    return-void
.end method
