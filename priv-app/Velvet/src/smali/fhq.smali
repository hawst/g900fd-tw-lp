.class final Lfhq;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic crV:Lfhp;


# direct methods
.method constructor <init>(Lfhp;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lfhq;->crV:Lfhp;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 141
    iget-object v0, p0, Lfhq;->crV:Lfhp;

    iget-object v0, v0, Lfhp;->mTrainingQuestionManager:Lfdn;

    invoke-interface {v0}, Lfdn;->ayu()Ljava/lang/Iterable;

    move-result-object v0

    new-instance v1, Ljhs;

    invoke-direct {v1}, Ljhs;-><init>()V

    iget-object v2, p0, Lfhq;->crV:Lfhp;

    iget-object v2, v2, Lfhp;->mTrainingQuestionManager:Lfdn;

    invoke-interface {v2}, Lfdn;->ayv()Ljhu;

    move-result-object v2

    iput-object v2, v1, Ljhs;->emi:Ljhu;

    const/16 v2, 0x9

    invoke-static {v2}, Lfjw;->iQ(I)Ljed;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljed;->hK(Z)Ljed;

    move-result-object v2

    invoke-static {}, Lcxg;->Tx()Lizm;

    move-result-object v3

    iput-object v3, v2, Ljed;->edr:Lizm;

    iput-object v1, v2, Ljed;->edC:Ljhs;

    new-instance v1, Ljhp;

    invoke-direct {v1}, Ljhp;-><init>()V

    iput-object v1, v2, Ljed;->edD:Ljhp;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lfqm;->k(Ljava/lang/Iterable;)Liwl;

    move-result-object v1

    iput-object v1, v2, Ljed;->eds:Liwl;

    :cond_0
    iget-object v1, p0, Lfhq;->crV:Lfhp;

    iget-object v1, v1, Lfhp;->mNetworkClient:Lfcx;

    invoke-interface {v1, v2}, Lfcx;->c(Ljed;)Ljeh;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, v1, Ljeh;->eep:Ljhq;

    if-nez v2, :cond_3

    :cond_1
    iget-object v0, p0, Lfhq;->crV:Lfhp;

    iput-boolean v4, v0, Lfhp;->crS:Z

    :cond_2
    :goto_0
    iget-object v0, p0, Lfhq;->crV:Lfhp;

    const/4 v2, 0x0

    iput-object v2, v0, Lfhp;->crR:Lfhq;

    return-object v1

    :cond_3
    iget-object v2, v1, Ljeh;->eeo:Ljht;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lfhq;->crV:Lfhp;

    iget-object v2, v2, Lfhp;->mTrainingQuestionManager:Lfdn;

    iget-object v3, v1, Ljeh;->eeo:Ljht;

    invoke-interface {v2, v3, v0}, Lfdn;->a(Ljht;Ljava/lang/Iterable;)V

    :goto_1
    iget-object v0, p0, Lfhq;->crV:Lfhp;

    iget-object v2, v1, Ljeh;->eep:Ljhq;

    iput-object v2, v0, Lfhp;->crT:Ljhq;

    iget-object v0, v1, Ljeh;->aeC:Lizn;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfhq;->crV:Lfhp;

    iget-object v2, v1, Ljeh;->aeC:Lizn;

    invoke-static {v2}, Lfhp;->e(Lizn;)I

    move-result v2

    iput v2, v0, Lfhp;->crU:I

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lfhq;->crV:Lfhp;

    iget-object v2, v2, Lfhp;->mTrainingQuestionManager:Lfdn;

    invoke-interface {v2, v0}, Lfdn;->i(Ljava/lang/Iterable;)V

    goto :goto_1
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Lfhq;->crV:Lfhp;

    invoke-virtual {v0}, Lfhp;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lfhr;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lfhq;->crV:Lfhp;

    iget-boolean v1, v1, Lfhp;->crS:Z

    if-eqz v1, :cond_1

    invoke-interface {v0}, Lfhr;->onError()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lfhq;->crV:Lfhp;

    iget-object v1, v1, Lfhp;->crT:Ljhq;

    iget-object v2, p0, Lfhq;->crV:Lfhp;

    iget v2, v2, Lfhp;->crU:I

    invoke-interface {v0, v1, v2}, Lfhr;->a(Ljhq;I)V

    goto :goto_0
.end method
