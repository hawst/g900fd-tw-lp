.class public final Ljnm;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile euX:[Ljnm;


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field private ail:I

.field private dIj:I

.field private ecs:I

.field public euY:Ljno;

.field public euZ:Ljno;

.field private eva:Z

.field public evb:Ljnn;

.field private evc:Ljava/lang/String;

.field private evd:Ljava/lang/String;

.field private eve:Ljava/lang/String;

.field private evf:Ljnt;

.field public evg:Ljnu;

.field private evh:Ljpk;

.field private evi:Ljnx;

.field private evj:Ljrx;

.field private evk:Ljnv;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1587
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1588
    iput v2, p0, Ljnm;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljnm;->afh:Ljava/lang/String;

    iput-object v1, p0, Ljnm;->euY:Ljno;

    iput-object v1, p0, Ljnm;->euZ:Ljno;

    iput-boolean v2, p0, Ljnm;->eva:Z

    iput v2, p0, Ljnm;->dIj:I

    iput v2, p0, Ljnm;->ail:I

    iput-object v1, p0, Ljnm;->evb:Ljnn;

    iput v2, p0, Ljnm;->ecs:I

    const-string v0, ""

    iput-object v0, p0, Ljnm;->evc:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljnm;->evd:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljnm;->eve:Ljava/lang/String;

    iput-object v1, p0, Ljnm;->evf:Ljnt;

    iput-object v1, p0, Ljnm;->evg:Ljnu;

    iput-object v1, p0, Ljnm;->evh:Ljpk;

    iput-object v1, p0, Ljnm;->evi:Ljnx;

    iput-object v1, p0, Ljnm;->evj:Ljrx;

    iput-object v1, p0, Ljnm;->evk:Ljnv;

    iput-object v1, p0, Ljnm;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljnm;->eCz:I

    .line 1589
    return-void
.end method

.method public static bqI()[Ljnm;
    .locals 2

    .prologue
    .line 1383
    sget-object v0, Ljnm;->euX:[Ljnm;

    if-nez v0, :cond_1

    .line 1384
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 1386
    :try_start_0
    sget-object v0, Ljnm;->euX:[Ljnm;

    if-nez v0, :cond_0

    .line 1387
    const/4 v0, 0x0

    new-array v0, v0, [Ljnm;

    sput-object v0, Ljnm;->euX:[Ljnm;

    .line 1389
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1391
    :cond_1
    sget-object v0, Ljnm;->euX:[Ljnm;

    return-object v0

    .line 1389
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 1354
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljnm;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnm;->afh:Ljava/lang/String;

    iget v0, p0, Ljnm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljnm;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljnm;->euY:Ljno;

    if-nez v0, :cond_1

    new-instance v0, Ljno;

    invoke-direct {v0}, Ljno;-><init>()V

    iput-object v0, p0, Ljnm;->euY:Ljno;

    :cond_1
    iget-object v0, p0, Ljnm;->euY:Ljno;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljnm;->euZ:Ljno;

    if-nez v0, :cond_2

    new-instance v0, Ljno;

    invoke-direct {v0}, Ljno;-><init>()V

    iput-object v0, p0, Ljnm;->euZ:Ljno;

    :cond_2
    iget-object v0, p0, Ljnm;->euZ:Ljno;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljnm;->eva:Z

    iget v0, p0, Ljnm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljnm;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljnm;->ail:I

    iget v0, p0, Ljnm;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljnm;->aez:I

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ljnm;->evb:Ljnn;

    if-nez v0, :cond_3

    new-instance v0, Ljnn;

    invoke-direct {v0}, Ljnn;-><init>()V

    iput-object v0, p0, Ljnm;->evb:Ljnn;

    :cond_3
    iget-object v0, p0, Ljnm;->evb:Ljnn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljnm;->ecs:I

    iget v0, p0, Ljnm;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljnm;->aez:I

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Ljnm;->evf:Ljnt;

    if-nez v0, :cond_4

    new-instance v0, Ljnt;

    invoke-direct {v0}, Ljnt;-><init>()V

    iput-object v0, p0, Ljnm;->evf:Ljnt;

    :cond_4
    iget-object v0, p0, Ljnm;->evf:Ljnt;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ljnm;->evg:Ljnu;

    if-nez v0, :cond_5

    new-instance v0, Ljnu;

    invoke-direct {v0}, Ljnu;-><init>()V

    iput-object v0, p0, Ljnm;->evg:Ljnu;

    :cond_5
    iget-object v0, p0, Ljnm;->evg:Ljnu;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ljnm;->evh:Ljpk;

    if-nez v0, :cond_6

    new-instance v0, Ljpk;

    invoke-direct {v0}, Ljpk;-><init>()V

    iput-object v0, p0, Ljnm;->evh:Ljpk;

    :cond_6
    iget-object v0, p0, Ljnm;->evh:Ljpk;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Ljnm;->evi:Ljnx;

    if-nez v0, :cond_7

    new-instance v0, Ljnx;

    invoke-direct {v0}, Ljnx;-><init>()V

    iput-object v0, p0, Ljnm;->evi:Ljnx;

    :cond_7
    iget-object v0, p0, Ljnm;->evi:Ljnx;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Ljnm;->evj:Ljrx;

    if-nez v0, :cond_8

    new-instance v0, Ljrx;

    invoke-direct {v0}, Ljrx;-><init>()V

    iput-object v0, p0, Ljnm;->evj:Ljrx;

    :cond_8
    iget-object v0, p0, Ljnm;->evj:Ljrx;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto/16 :goto_0

    :pswitch_2
    iput v0, p0, Ljnm;->dIj:I

    iget v0, p0, Ljnm;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljnm;->aez:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnm;->evc:Ljava/lang/String;

    iget v0, p0, Ljnm;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljnm;->aez:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnm;->evd:Ljava/lang/String;

    iget v0, p0, Ljnm;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljnm;->aez:I

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Ljnm;->evk:Ljnv;

    if-nez v0, :cond_9

    new-instance v0, Ljnv;

    invoke-direct {v0}, Ljnv;-><init>()V

    iput-object v0, p0, Ljnm;->evk:Ljnv;

    :cond_9
    iget-object v0, p0, Ljnm;->evk:Ljnv;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnm;->eve:Ljava/lang/String;

    iget v0, p0, Ljnm;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljnm;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x70 -> :sswitch_d
        0x7a -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 1618
    iget v0, p0, Ljnm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1619
    const/4 v0, 0x1

    iget-object v1, p0, Ljnm;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1621
    :cond_0
    iget-object v0, p0, Ljnm;->euY:Ljno;

    if-eqz v0, :cond_1

    .line 1622
    const/4 v0, 0x2

    iget-object v1, p0, Ljnm;->euY:Ljno;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1624
    :cond_1
    iget-object v0, p0, Ljnm;->euZ:Ljno;

    if-eqz v0, :cond_2

    .line 1625
    const/4 v0, 0x3

    iget-object v1, p0, Ljnm;->euZ:Ljno;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1627
    :cond_2
    iget v0, p0, Ljnm;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 1628
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljnm;->eva:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 1630
    :cond_3
    iget v0, p0, Ljnm;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 1631
    const/4 v0, 0x5

    iget v1, p0, Ljnm;->ail:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1633
    :cond_4
    iget-object v0, p0, Ljnm;->evb:Ljnn;

    if-eqz v0, :cond_5

    .line 1634
    const/4 v0, 0x6

    iget-object v1, p0, Ljnm;->evb:Ljnn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1636
    :cond_5
    iget v0, p0, Ljnm;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    .line 1637
    const/4 v0, 0x7

    iget v1, p0, Ljnm;->ecs:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1639
    :cond_6
    iget-object v0, p0, Ljnm;->evf:Ljnt;

    if-eqz v0, :cond_7

    .line 1640
    const/16 v0, 0x8

    iget-object v1, p0, Ljnm;->evf:Ljnt;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1642
    :cond_7
    iget-object v0, p0, Ljnm;->evg:Ljnu;

    if-eqz v0, :cond_8

    .line 1643
    const/16 v0, 0xa

    iget-object v1, p0, Ljnm;->evg:Ljnu;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1645
    :cond_8
    iget-object v0, p0, Ljnm;->evh:Ljpk;

    if-eqz v0, :cond_9

    .line 1646
    const/16 v0, 0xb

    iget-object v1, p0, Ljnm;->evh:Ljpk;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1648
    :cond_9
    iget-object v0, p0, Ljnm;->evi:Ljnx;

    if-eqz v0, :cond_a

    .line 1649
    const/16 v0, 0xc

    iget-object v1, p0, Ljnm;->evi:Ljnx;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1651
    :cond_a
    iget-object v0, p0, Ljnm;->evj:Ljrx;

    if-eqz v0, :cond_b

    .line 1652
    const/16 v0, 0xd

    iget-object v1, p0, Ljnm;->evj:Ljrx;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1654
    :cond_b
    iget v0, p0, Ljnm;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_c

    .line 1655
    const/16 v0, 0xe

    iget v1, p0, Ljnm;->dIj:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1657
    :cond_c
    iget v0, p0, Ljnm;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_d

    .line 1658
    const/16 v0, 0xf

    iget-object v1, p0, Ljnm;->evc:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1660
    :cond_d
    iget v0, p0, Ljnm;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_e

    .line 1661
    const/16 v0, 0x10

    iget-object v1, p0, Ljnm;->evd:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1663
    :cond_e
    iget-object v0, p0, Ljnm;->evk:Ljnv;

    if-eqz v0, :cond_f

    .line 1664
    const/16 v0, 0x11

    iget-object v1, p0, Ljnm;->evk:Ljnv;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1666
    :cond_f
    iget v0, p0, Ljnm;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_10

    .line 1667
    const/16 v0, 0x12

    iget-object v1, p0, Ljnm;->eve:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1669
    :cond_10
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1670
    return-void
.end method

.method public final bqJ()Z
    .locals 1

    .prologue
    .line 1427
    iget-boolean v0, p0, Ljnm;->eva:Z

    return v0
.end method

.method public final bqK()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1506
    iget-object v0, p0, Ljnm;->evc:Ljava/lang/String;

    return-object v0
.end method

.method public final bqL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1528
    iget-object v0, p0, Ljnm;->evd:Ljava/lang/String;

    return-object v0
.end method

.method public final bqM()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1550
    iget-object v0, p0, Ljnm;->eve:Ljava/lang/String;

    return-object v0
.end method

.method public final getEventType()I
    .locals 1

    .prologue
    .line 1446
    iget v0, p0, Ljnm;->dIj:I

    return v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1399
    iget-object v0, p0, Ljnm;->afh:Ljava/lang/String;

    return-object v0
.end method

.method public final iM(Z)Ljnm;
    .locals 1

    .prologue
    .line 1430
    iput-boolean p1, p0, Ljnm;->eva:Z

    .line 1431
    iget v0, p0, Ljnm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljnm;->aez:I

    .line 1432
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1674
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1675
    iget v1, p0, Ljnm;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1676
    const/4 v1, 0x1

    iget-object v2, p0, Ljnm;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1679
    :cond_0
    iget-object v1, p0, Ljnm;->euY:Ljno;

    if-eqz v1, :cond_1

    .line 1680
    const/4 v1, 0x2

    iget-object v2, p0, Ljnm;->euY:Ljno;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1683
    :cond_1
    iget-object v1, p0, Ljnm;->euZ:Ljno;

    if-eqz v1, :cond_2

    .line 1684
    const/4 v1, 0x3

    iget-object v2, p0, Ljnm;->euZ:Ljno;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1687
    :cond_2
    iget v1, p0, Ljnm;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_3

    .line 1688
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljnm;->eva:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1691
    :cond_3
    iget v1, p0, Ljnm;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 1692
    const/4 v1, 0x5

    iget v2, p0, Ljnm;->ail:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1695
    :cond_4
    iget-object v1, p0, Ljnm;->evb:Ljnn;

    if-eqz v1, :cond_5

    .line 1696
    const/4 v1, 0x6

    iget-object v2, p0, Ljnm;->evb:Ljnn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1699
    :cond_5
    iget v1, p0, Ljnm;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_6

    .line 1700
    const/4 v1, 0x7

    iget v2, p0, Ljnm;->ecs:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1703
    :cond_6
    iget-object v1, p0, Ljnm;->evf:Ljnt;

    if-eqz v1, :cond_7

    .line 1704
    const/16 v1, 0x8

    iget-object v2, p0, Ljnm;->evf:Ljnt;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1707
    :cond_7
    iget-object v1, p0, Ljnm;->evg:Ljnu;

    if-eqz v1, :cond_8

    .line 1708
    const/16 v1, 0xa

    iget-object v2, p0, Ljnm;->evg:Ljnu;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1711
    :cond_8
    iget-object v1, p0, Ljnm;->evh:Ljpk;

    if-eqz v1, :cond_9

    .line 1712
    const/16 v1, 0xb

    iget-object v2, p0, Ljnm;->evh:Ljpk;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1715
    :cond_9
    iget-object v1, p0, Ljnm;->evi:Ljnx;

    if-eqz v1, :cond_a

    .line 1716
    const/16 v1, 0xc

    iget-object v2, p0, Ljnm;->evi:Ljnx;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1719
    :cond_a
    iget-object v1, p0, Ljnm;->evj:Ljrx;

    if-eqz v1, :cond_b

    .line 1720
    const/16 v1, 0xd

    iget-object v2, p0, Ljnm;->evj:Ljrx;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1723
    :cond_b
    iget v1, p0, Ljnm;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_c

    .line 1724
    const/16 v1, 0xe

    iget v2, p0, Ljnm;->dIj:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1727
    :cond_c
    iget v1, p0, Ljnm;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_d

    .line 1728
    const/16 v1, 0xf

    iget-object v2, p0, Ljnm;->evc:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1731
    :cond_d
    iget v1, p0, Ljnm;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_e

    .line 1732
    const/16 v1, 0x10

    iget-object v2, p0, Ljnm;->evd:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1735
    :cond_e
    iget-object v1, p0, Ljnm;->evk:Ljnv;

    if-eqz v1, :cond_f

    .line 1736
    const/16 v1, 0x11

    iget-object v2, p0, Ljnm;->evk:Ljnv;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1739
    :cond_f
    iget v1, p0, Ljnm;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_10

    .line 1740
    const/16 v1, 0x12

    iget-object v2, p0, Ljnm;->eve:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1743
    :cond_10
    return v0
.end method

.method public final qS(I)Ljnm;
    .locals 1

    .prologue
    .line 1449
    iput p1, p0, Ljnm;->dIj:I

    .line 1450
    iget v0, p0, Ljnm;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljnm;->aez:I

    .line 1451
    return-object p0
.end method

.method public final xm(Ljava/lang/String;)Ljnm;
    .locals 1

    .prologue
    .line 1402
    if-nez p1, :cond_0

    .line 1403
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1405
    :cond_0
    iput-object p1, p0, Ljnm;->afh:Ljava/lang/String;

    .line 1406
    iget v0, p0, Ljnm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljnm;->aez:I

    .line 1407
    return-object p0
.end method

.method public final xn(Ljava/lang/String;)Ljnm;
    .locals 1

    .prologue
    .line 1509
    if-nez p1, :cond_0

    .line 1510
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1512
    :cond_0
    iput-object p1, p0, Ljnm;->evc:Ljava/lang/String;

    .line 1513
    iget v0, p0, Ljnm;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljnm;->aez:I

    .line 1514
    return-object p0
.end method

.method public final xo(Ljava/lang/String;)Ljnm;
    .locals 1

    .prologue
    .line 1531
    if-nez p1, :cond_0

    .line 1532
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1534
    :cond_0
    iput-object p1, p0, Ljnm;->evd:Ljava/lang/String;

    .line 1535
    iget v0, p0, Ljnm;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljnm;->aez:I

    .line 1536
    return-object p0
.end method

.method public final xp(Ljava/lang/String;)Ljnm;
    .locals 1

    .prologue
    .line 1553
    if-nez p1, :cond_0

    .line 1554
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1556
    :cond_0
    iput-object p1, p0, Ljnm;->eve:Ljava/lang/String;

    .line 1557
    iget v0, p0, Ljnm;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljnm;->aez:I

    .line 1558
    return-object p0
.end method
