.class public Lhrx;
.super Landroid/app/Fragment;
.source "PG"


# instance fields
.field private mSettings:Lhym;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lhrx;)Lhym;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lhrx;->mSettings:Lhym;

    return-object v0
.end method

.method private a(Landroid/widget/RadioGroup;III)V
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p1, p2}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 52
    if-ne p3, p4, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 53
    new-instance v1, Lhry;

    invoke-direct {v1, p0, p4}, Lhry;-><init>(Lhrx;I)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    return-void

    .line 52
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 27
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    .line 29
    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    iput-object v0, p0, Lhrx;->mSettings:Lhym;

    .line 30
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 35
    const v0, 0x7f0401ba

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 38
    iget-object v1, p0, Lhrx;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aUs()I

    move-result v1

    .line 39
    const v2, 0x7f110495

    invoke-direct {p0, v0, v2, v1, v3}, Lhrx;->a(Landroid/widget/RadioGroup;III)V

    .line 41
    const v2, 0x7f110496

    const/4 v3, 0x1

    invoke-direct {p0, v0, v2, v1, v3}, Lhrx;->a(Landroid/widget/RadioGroup;III)V

    .line 43
    const v2, 0x7f110497

    const/4 v3, 0x2

    invoke-direct {p0, v0, v2, v1, v3}, Lhrx;->a(Landroid/widget/RadioGroup;III)V

    .line 46
    return-object v0
.end method
