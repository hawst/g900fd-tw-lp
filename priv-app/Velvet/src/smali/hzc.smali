.class public final Lhzc;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aUA:I

.field private final aYZ:Lciu;

.field private final bDB:Ligi;

.field private final bwj:Ldgm;

.field final cUE:Lgru;

.field private final cXk:Libv;

.field final duT:Licp;

.field private final dva:Z

.field private final dvb:Lbye;

.field private final mActionDiscoveryData:Lgpc;

.field private final mAdaptiveTtsHelper:Lbwo;

.field final mContactLabelConverter:Ldyv;

.field final mContactLookup:Lghy;

.field final mContentResolver:Landroid/content/ContentResolver;

.field final mContext:Landroid/content/Context;

.field private final mDeviceCapabilityManager:Lenm;

.field final mDiscourseContextSupplier:Ligi;

.field private final mGooglePlayServicesHelper:Lcha;

.field final mGsaConfigFlags:Lchk;

.field private final mLoginHelper:Lcrh;

.field private final mMessageManager:Lbxy;

.field final mMessageSearchTtsUtil:Lgtj;

.field private final mNowOptInSettings:Lcin;

.field final mPersonShortcutManager:Lciy;

.field final mRelationshipManager:Lcjg;

.field private final mScreenStateHelper:Ldmh;

.field private final mSearchSettings:Lcke;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldmh;Ldyv;Lcha;Lcjg;Lciy;Lcke;Lchk;Ligi;Lghy;Landroid/content/ContentResolver;Lenm;ZLigi;Lcrh;Lbxy;Libv;Lgpc;Licp;ILgru;Lgtj;Lcin;Lciu;Lbwo;Lbye;Ldgm;)V
    .locals 2
    .param p2    # Ldmh;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Ldyv;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Lcha;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # Lcjg;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p6    # Lciy;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    iput-object p2, p0, Lhzc;->mScreenStateHelper:Ldmh;

    .line 198
    iput-object p3, p0, Lhzc;->mContactLabelConverter:Ldyv;

    .line 199
    iput-object p4, p0, Lhzc;->mGooglePlayServicesHelper:Lcha;

    .line 200
    iput-object p9, p0, Lhzc;->bDB:Ligi;

    .line 201
    invoke-static {p10}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lghy;

    iput-object v1, p0, Lhzc;->mContactLookup:Lghy;

    .line 202
    invoke-static {p11}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ContentResolver;

    iput-object v1, p0, Lhzc;->mContentResolver:Landroid/content/ContentResolver;

    .line 203
    iput-object p1, p0, Lhzc;->mContext:Landroid/content/Context;

    .line 204
    iput-object p5, p0, Lhzc;->mRelationshipManager:Lcjg;

    .line 205
    iput-object p6, p0, Lhzc;->mPersonShortcutManager:Lciy;

    .line 206
    iput-object p7, p0, Lhzc;->mSearchSettings:Lcke;

    .line 207
    iput-object p8, p0, Lhzc;->mGsaConfigFlags:Lchk;

    .line 208
    invoke-static {p12}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lenm;

    iput-object v1, p0, Lhzc;->mDeviceCapabilityManager:Lenm;

    .line 209
    iput-boolean p13, p0, Lhzc;->dva:Z

    .line 210
    invoke-static/range {p14 .. p14}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ligi;

    iput-object v1, p0, Lhzc;->mDiscourseContextSupplier:Ligi;

    .line 211
    invoke-static/range {p15 .. p15}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcrh;

    iput-object v1, p0, Lhzc;->mLoginHelper:Lcrh;

    .line 212
    move-object/from16 v0, p16

    iput-object v0, p0, Lhzc;->mMessageManager:Lbxy;

    .line 213
    move-object/from16 v0, p17

    iput-object v0, p0, Lhzc;->cXk:Libv;

    .line 214
    move-object/from16 v0, p18

    iput-object v0, p0, Lhzc;->mActionDiscoveryData:Lgpc;

    .line 215
    invoke-static/range {p19 .. p19}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Licp;

    iput-object v1, p0, Lhzc;->duT:Licp;

    .line 216
    move/from16 v0, p20

    iput v0, p0, Lhzc;->aUA:I

    .line 217
    move-object/from16 v0, p21

    iput-object v0, p0, Lhzc;->cUE:Lgru;

    .line 218
    move-object/from16 v0, p22

    iput-object v0, p0, Lhzc;->mMessageSearchTtsUtil:Lgtj;

    .line 219
    move-object/from16 v0, p23

    iput-object v0, p0, Lhzc;->mNowOptInSettings:Lcin;

    .line 220
    move-object/from16 v0, p24

    iput-object v0, p0, Lhzc;->aYZ:Lciu;

    .line 221
    move-object/from16 v0, p25

    iput-object v0, p0, Lhzc;->mAdaptiveTtsHelper:Lbwo;

    .line 222
    move-object/from16 v0, p26

    iput-object v0, p0, Lhzc;->dvb:Lbye;

    .line 223
    move-object/from16 v0, p27

    iput-object v0, p0, Lhzc;->bwj:Ldgm;

    .line 224
    return-void
.end method

.method private static a(Ljrc;Lcom/google/android/search/shared/actions/modular/ModularAction;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1014
    iget-object v0, p0, Ljrc;->ezY:[Ljrl;

    array-length v0, v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 1016
    iget-object v4, p0, Ljrc;->ezY:[Ljrl;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_3

    aget-object v6, v4, v2

    .line 1017
    iget-object v0, v6, Ljrl;->eAH:[Ljqs;

    array-length v0, v0

    if-lez v0, :cond_1

    iget-object v0, v6, Ljrl;->eAH:[Ljqs;

    aget-object v0, v0, v1

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->aji()Ljrl;

    move-result-object v7

    iget-object v7, v7, Ljrl;->ezJ:[I

    invoke-static {v0}, Ldwc;->b(Ljqs;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqv;

    invoke-static {p1, v0, v7}, Ldxd;->a(Ldxb;Ljqv;[I)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 1020
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1016
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1017
    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    .line 1024
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Ljrc;->ezY:[Ljrl;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 1026
    const-class v0, Ljrl;

    invoke-static {v3, v0}, Likm;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljrl;

    iput-object v0, p0, Ljrc;->ezY:[Ljrl;

    .line 1028
    :cond_4
    return-void
.end method

.method private aUx()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1414
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lhzc;->mContext:Landroid/content/Context;

    const-class v2, Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1415
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1416
    const-string v1, "source"

    const-string v2, "ADD_REMINDER"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1418
    return-object v0
.end method

.method private static aUy()Lcom/google/android/search/shared/actions/PuntAction;
    .locals 3

    .prologue
    .line 1462
    new-instance v0, Lcom/google/android/search/shared/actions/PuntAction;

    const v1, 0x7f0a064b

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/search/shared/actions/PuntAction;-><init>(IZ)V

    return-object v0
.end method


# virtual methods
.method final a(Lcom/google/android/shared/search/Query;Ljmr;Z)Lcom/google/android/search/shared/actions/ShowContactInformationAction;
    .locals 12
    .param p1    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljmr;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1362
    invoke-virtual {p2}, Ljmr;->bqr()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1363
    const/4 v0, 0x1

    move v11, v0

    .line 1372
    :goto_0
    const/4 v1, 0x0

    .line 1373
    iget-object v0, p2, Ljmr;->etx:Ljoq;

    if-eqz v0, :cond_5

    .line 1374
    iget-object v1, p0, Lhzc;->mContactLabelConverter:Ldyv;

    iget-object v2, p0, Lhzc;->mRelationshipManager:Lcjg;

    iget-object v3, p0, Lhzc;->mPersonShortcutManager:Lciy;

    iget-object v4, p0, Lhzc;->mGsaConfigFlags:Lchk;

    iget-object v5, p0, Lhzc;->mContactLookup:Lghy;

    sget-object v6, Ldzb;->bRt:Ldzb;

    iget-object v7, p2, Ljmr;->etx:Ljoq;

    iget-object v8, p0, Lhzc;->mDiscourseContextSupplier:Ligi;

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object v0, p1

    invoke-static/range {v0 .. v10}, Lico;->a(Lcom/google/android/shared/search/Query;Ldyv;Lcjg;Lciy;Lchk;Lghy;Ldzb;Ljoq;Ligi;Lifw;Z)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    .line 1391
    :cond_0
    :goto_1
    if-nez p3, :cond_6

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ajO()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1392
    :cond_1
    const/4 v0, 0x0

    .line 1397
    :goto_2
    return-object v0

    .line 1364
    :cond_2
    invoke-virtual {p2}, Ljmr;->bqs()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1365
    const/4 v0, 0x2

    move v11, v0

    goto :goto_0

    .line 1366
    :cond_3
    invoke-virtual {p2}, Ljmr;->bqt()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1367
    const/4 v0, 0x3

    move v11, v0

    goto :goto_0

    .line 1369
    :cond_4
    const/4 v0, 0x0

    move v11, v0

    goto :goto_0

    .line 1379
    :cond_5
    iget-object v0, p2, Ljmr;->etw:[Ljks;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 1380
    iget-object v0, p0, Lhzc;->mContactLookup:Lghy;

    sget-object v1, Ldzb;->bRt:Ldzb;

    iget-object v2, p2, Ljmr;->etw:[Ljks;

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Lghy;->a(Lcom/google/android/shared/search/Query;Ldzb;[Ljks;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1383
    new-instance v1, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    iget-object v2, p2, Ljmr;->etw:[Ljks;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-static {v2}, Lico;->a(Ljks;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Ldzb;->bRt:Ldzb;

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/google/android/search/shared/contact/PersonDisambiguation;-><init>(Ljava/lang/String;Ljava/util/List;ZLdzb;)V

    goto :goto_1

    .line 1395
    :cond_6
    iget-object v0, p2, Ljmr;->etw:[Ljks;

    invoke-static {v0}, Lico;->a([Ljks;)Ljava/lang/String;

    move-result-object v3

    .line 1397
    new-instance v0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v2, v11

    invoke-direct/range {v0 .. v7}, Lcom/google/android/search/shared/actions/ShowContactInformationAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;ILjava/lang/String;ZLjava/util/List;Ljava/util/List;Ljava/util/List;)V

    goto :goto_2
.end method

.method public final a(Ljkt;Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/actions/VoiceAction;
    .locals 13
    .param p1    # Ljkt;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x0

    const/4 v10, 0x1

    .line 840
    iget-object v0, p0, Lhzc;->mDeviceCapabilityManager:Lenm;

    invoke-interface {v0}, Lenm;->auH()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 841
    sget-object v0, Ljmg;->etv:Ljsm;

    invoke-virtual {p1, v0}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Ljmg;

    .line 842
    iget-object v0, p1, Ljkt;->eqn:Ljku;

    if-eqz v0, :cond_1

    iget-object v0, p1, Ljkt;->eqn:Ljku;

    invoke-virtual {v0}, Ljku;->oY()I

    move-result v0

    move v12, v0

    .line 846
    :goto_0
    const/16 v0, 0x9

    if-ne v12, v0, :cond_2

    .line 847
    new-instance v9, Lcom/google/android/search/shared/actions/LocalResultsAction;

    iget-object v0, v11, Ljmg;->etx:Ljoq;

    iget-object v0, v0, Ljoq;->ewR:Ljpe;

    invoke-direct {v9, v0, v10}, Lcom/google/android/search/shared/actions/LocalResultsAction;-><init>(Ljpe;Z)V

    .line 898
    :cond_0
    :goto_1
    return-object v9

    .line 842
    :cond_1
    const/4 v0, -0x1

    move v12, v0

    goto :goto_0

    .line 852
    :cond_2
    iget-object v0, v11, Ljmg;->etx:Ljoq;

    if-eqz v0, :cond_c

    .line 853
    iget-object v0, v11, Ljmg;->etx:Ljoq;

    invoke-virtual {v0}, Ljoq;->bru()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhzc;->mMessageManager:Lbxy;

    invoke-virtual {v0}, Lbxy;->BD()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 855
    iget-object v0, p0, Lhzc;->mMessageManager:Lbxy;

    invoke-virtual {v0}, Lbxy;->BD()Ljava/lang/String;

    move-result-object v0

    .line 856
    sget-object v1, Ldzb;->bRq:Ldzb;

    iget-object v2, p0, Lhzc;->dvb:Lbye;

    new-instance v2, Lgia;

    iget-object v3, p0, Lhzc;->mContentResolver:Landroid/content/ContentResolver;

    invoke-direct {v2, v3}, Lgia;-><init>(Landroid/content/ContentResolver;)V

    invoke-static {v0, v2}, Lbye;->a(Ljava/lang/String;Lgia;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lico;->a(Ljava/lang/String;Ldzb;Ljava/lang/String;)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    .line 861
    iget-object v1, p0, Lhzc;->mMessageManager:Lbxy;

    invoke-virtual {v1}, Lbxy;->BC()V

    move-object v1, v0

    .line 872
    :goto_2
    const/16 v0, 0xa

    if-eq v12, v0, :cond_3

    const/16 v0, 0x1c

    if-ne v12, v0, :cond_5

    .line 874
    :cond_3
    new-instance v9, Lcom/google/android/search/shared/actions/PhoneCallAction;

    invoke-direct {v9, v1}, Lcom/google/android/search/shared/actions/PhoneCallAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    goto :goto_1

    .line 863
    :cond_4
    iget-object v1, p0, Lhzc;->mContactLabelConverter:Ldyv;

    iget-object v2, p0, Lhzc;->mRelationshipManager:Lcjg;

    iget-object v3, p0, Lhzc;->mPersonShortcutManager:Lciy;

    iget-object v4, p0, Lhzc;->mGsaConfigFlags:Lchk;

    iget-object v5, p0, Lhzc;->mContactLookup:Lghy;

    sget-object v6, Ldzb;->bRq:Ldzb;

    iget-object v7, v11, Ljmg;->etx:Ljoq;

    iget-object v8, p0, Lhzc;->mDiscourseContextSupplier:Ligi;

    move-object v0, p2

    invoke-static/range {v0 .. v10}, Lico;->a(Lcom/google/android/shared/search/Query;Ldyv;Lcjg;Lciy;Lchk;Lghy;Ldzb;Ljoq;Ligi;Lifw;Z)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 879
    :cond_5
    const/16 v0, 0x2a

    if-ne v12, v0, :cond_a

    .line 881
    const/16 v0, 0x9c

    invoke-static {v0}, Lege;->ht(I)V

    .line 884
    iget-object v0, p1, Ljkt;->eqn:Ljku;

    if-eqz v0, :cond_6

    iget-object v0, p1, Ljkt;->eqn:Ljku;

    invoke-virtual {v0}, Ljku;->boA()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_6
    move v0, v10

    .line 886
    :goto_3
    if-nez v0, :cond_7

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ajO()Z

    move-result v0

    if-nez v0, :cond_9

    .line 887
    :cond_7
    new-instance v9, Lcom/google/android/search/shared/actions/PhoneCallAction;

    invoke-direct {v9, v1}, Lcom/google/android/search/shared/actions/PhoneCallAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    goto/16 :goto_1

    .line 884
    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    .line 888
    :cond_9
    iget-object v0, v11, Ljmg;->etx:Ljoq;

    if-eqz v0, :cond_0

    iget-object v0, v11, Ljmg;->etx:Ljoq;

    iget-object v0, v0, Ljoq;->ewR:Ljpe;

    if-eqz v0, :cond_0

    .line 890
    new-instance v9, Lcom/google/android/search/shared/actions/LocalResultsAction;

    iget-object v0, v11, Ljmg;->etx:Ljoq;

    iget-object v0, v0, Ljoq;->ewR:Ljpe;

    invoke-direct {v9, v0, v10}, Lcom/google/android/search/shared/actions/LocalResultsAction;-><init>(Ljpe;Z)V

    goto/16 :goto_1

    .line 896
    :cond_a
    new-instance v9, Lcom/google/android/search/shared/actions/PhoneCallAction;

    invoke-direct {v9, v1}, Lcom/google/android/search/shared/actions/PhoneCallAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    goto/16 :goto_1

    .line 898
    :cond_b
    invoke-static {}, Lhzc;->aUy()Lcom/google/android/search/shared/actions/PuntAction;

    move-result-object v9

    goto/16 :goto_1

    :cond_c
    move-object v1, v9

    goto :goto_2
.end method

.method public final a(Ljmo;Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/actions/VoiceAction;
    .locals 11

    .prologue
    const/4 v9, 0x0

    const/4 v7, 0x0

    .line 810
    iget-object v0, p0, Lhzc;->mDeviceCapabilityManager:Lenm;

    invoke-interface {v0}, Lenm;->auH()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 812
    iget-object v0, p1, Ljmo;->euf:[Ljoq;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 813
    iget-object v0, p1, Ljmo;->euf:[Ljoq;

    aget-object v0, v0, v7

    invoke-virtual {v0}, Ljoq;->bru()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhzc;->mMessageManager:Lbxy;

    invoke-virtual {v0}, Lbxy;->BD()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 816
    iget-object v0, p0, Lhzc;->mAdaptiveTtsHelper:Lbwo;

    const-string v1, "reply"

    invoke-virtual {v0, v1}, Lbwo;->fK(Ljava/lang/String;)V

    .line 817
    iget-object v0, p0, Lhzc;->mMessageManager:Lbxy;

    invoke-virtual {v0}, Lbxy;->BD()Ljava/lang/String;

    move-result-object v0

    .line 818
    sget-object v1, Ldzb;->bRq:Ldzb;

    iget-object v2, p0, Lhzc;->dvb:Lbye;

    new-instance v2, Lgia;

    iget-object v3, p0, Lhzc;->mContentResolver:Landroid/content/ContentResolver;

    invoke-direct {v2, v3}, Lgia;-><init>(Landroid/content/ContentResolver;)V

    invoke-static {v0, v2}, Lbye;->a(Ljava/lang/String;Lgia;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lico;->a(Ljava/lang/String;Ldzb;Ljava/lang/String;)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v9

    .line 822
    iget-object v0, p0, Lhzc;->mMessageManager:Lbxy;

    invoke-virtual {v0}, Lbxy;->BC()V

    .line 833
    :cond_0
    :goto_0
    new-instance v0, Lcom/google/android/search/shared/actions/SmsAction;

    invoke-virtual {p1}, Ljmo;->getMessageBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v2

    invoke-direct {v0, v9, v1, v2}, Lcom/google/android/search/shared/actions/SmsAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ljava/lang/String;Z)V

    .line 835
    :goto_1
    return-object v0

    .line 825
    :cond_1
    iget-object v1, p0, Lhzc;->mContactLabelConverter:Ldyv;

    iget-object v2, p0, Lhzc;->mRelationshipManager:Lcjg;

    iget-object v3, p0, Lhzc;->mPersonShortcutManager:Lciy;

    iget-object v4, p0, Lhzc;->mGsaConfigFlags:Lchk;

    iget-object v5, p0, Lhzc;->mContactLookup:Lghy;

    sget-object v6, Ldzb;->bRq:Ldzb;

    iget-object v0, p1, Ljmo;->euf:[Ljoq;

    aget-object v7, v0, v7

    iget-object v8, p0, Lhzc;->mDiscourseContextSupplier:Ligi;

    const/4 v10, 0x1

    move-object v0, p2

    invoke-static/range {v0 .. v10}, Lico;->a(Lcom/google/android/shared/search/Query;Ldyv;Lcjg;Lciy;Lchk;Lghy;Ldzb;Ljoq;Ligi;Lifw;Z)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v9

    goto :goto_0

    .line 835
    :cond_2
    invoke-static {}, Lhzc;->aUy()Lcom/google/android/search/shared/actions/PuntAction;

    move-result-object v0

    goto :goto_1
.end method

.method final a(Lcom/google/android/shared/search/Query;ILjrc;Z)Lcom/google/android/search/shared/actions/modular/ModularAction;
    .locals 19
    .param p1    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Ljrc;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 933
    move-object/from16 v0, p3

    iget-object v2, v0, Ljrc;->ezX:[Ljqg;

    array-length v2, v2

    invoke-static {v2}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v15

    .line 934
    move-object/from16 v0, p3

    iget-object v0, v0, Ljrc;->ezX:[Ljqg;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    const/4 v2, 0x0

    move v14, v2

    :goto_0
    move/from16 v0, v17

    if-ge v14, v0, :cond_1f

    aget-object v18, v16, v14

    .line 936
    sget-object v2, Ljrh;->eAt:Ljsm;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 938
    new-instance v2, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;

    move-object/from16 v0, v18

    invoke-direct {v2, v0}, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;-><init>(Ljqg;)V

    .line 979
    :goto_1
    if-nez p4, :cond_1e

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajO()Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 989
    const/4 v2, 0x0

    .line 1001
    :goto_2
    return-object v2

    .line 939
    :cond_0
    sget-object v2, Ljqi;->eyH:Ljsm;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 942
    invoke-virtual/range {v18 .. v18}, Ljqg;->getId()I

    move-result v3

    move-object/from16 v0, p3

    iget-object v4, v0, Ljrc;->ezY:[Ljrl;

    array-length v5, v4

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v5, :cond_6

    aget-object v6, v4, v2

    invoke-static {v6, v3}, Ldxv;->a(Ljrl;I)Ldzb;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 944
    :goto_4
    move-object/from16 v0, p3

    iget-object v3, v0, Ljrc;->ezY:[Ljrl;

    sget-object v2, Ljqi;->eyH:Ljsm;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    move-object v13, v2

    check-cast v13, Ljqi;

    const/4 v12, 0x1

    array-length v2, v3

    if-lez v2, :cond_1

    const/4 v2, 0x0

    aget-object v2, v3, v2

    iget-object v2, v2, Ljrl;->eAH:[Ljqs;

    array-length v2, v2

    if-lez v2, :cond_1

    const/4 v2, 0x0

    aget-object v2, v3, v2

    iget-object v2, v2, Ljrl;->eAH:[Ljqs;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    sget-object v3, Ljre;->eAh:Ljsm;

    invoke-virtual {v2, v3}, Ljqs;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljre;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljre;->btc()I

    move-result v2

    invoke-virtual/range {v18 .. v18}, Ljqg;->getId()I

    move-result v3

    if-ne v2, v3, :cond_1

    const/4 v12, 0x0

    :cond_1
    const/4 v2, 0x0

    if-eqz v13, :cond_7

    iget-object v3, v13, Ljqi;->eyI:Ljoq;

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lhzc;->mContactLabelConverter:Ldyv;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhzc;->mRelationshipManager:Lcjg;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhzc;->mPersonShortcutManager:Lciy;

    move-object/from16 v0, p0

    iget-object v6, v0, Lhzc;->mGsaConfigFlags:Lchk;

    move-object/from16 v0, p0

    iget-object v7, v0, Lhzc;->mContactLookup:Lghy;

    iget-object v9, v13, Ljqi;->eyI:Ljoq;

    move-object/from16 v0, p0

    iget-object v10, v0, Lhzc;->mDiscourseContextSupplier:Ligi;

    const/4 v11, 0x0

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v12}, Lico;->a(Lcom/google/android/shared/search/Query;Ldyv;Lcjg;Lciy;Lchk;Lghy;Ldzb;Ljoq;Ligi;Lifw;Z)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v3, v13, Ljqi;->eyI:Ljoq;

    invoke-virtual {v3}, Ljoq;->oK()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Ljoq;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->kE(Ljava/lang/String;)V

    :cond_2
    iget-object v3, v13, Ljqi;->eyI:Ljoq;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, Lhzc;->a(Ljoq;Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    :cond_3
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ajO()Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_4
    iget-object v3, v13, Ljqi;->eyI:Ljoq;

    iget-object v3, v3, Ljoq;->ewR:Ljpe;

    if-eqz v3, :cond_7

    new-instance v2, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;

    iget-object v3, v13, Ljqi;->eyI:Ljoq;

    iget-object v3, v3, Ljoq;->ewR:Ljpe;

    move-object/from16 v0, v18

    invoke-direct {v2, v0, v3}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;-><init>(Ljqg;Ljpe;)V

    goto/16 :goto_1

    .line 942
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    :cond_6
    const/4 v8, 0x0

    goto/16 :goto_4

    .line 944
    :cond_7
    new-instance v3, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    move-object/from16 v0, v18

    invoke-direct {v3, v0, v2}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;-><init>(Ljqg;Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    invoke-virtual {v3, v12}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->ev(Z)V

    move-object v2, v3

    goto/16 :goto_1

    .line 945
    :cond_8
    sget-object v2, Ljqq;->ezg:Ljsm;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_13

    .line 947
    sget-object v2, Ljqq;->ezg:Ljsm;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljqq;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v6, v2, Ljqq;->ezj:[Ljod;

    array-length v7, v6

    const/4 v3, 0x0

    move v4, v3

    :goto_5
    if-ge v4, v7, :cond_a

    aget-object v3, v6, v4

    sget-object v8, Ljoe;->ewe:Ljsm;

    invoke-virtual {v3, v8}, Ljod;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljoe;

    if-eqz v3, :cond_9

    invoke-virtual {v3}, Ljoe;->bhT()Z

    move-result v8

    if-eqz v8, :cond_9

    iget-object v8, v3, Ljoe;->ewg:[Ljof;

    if-eqz v8, :cond_9

    iget-object v8, v3, Ljoe;->ewg:[Ljof;

    array-length v8, v8

    if-eqz v8, :cond_9

    move-object/from16 v0, p0

    iget-object v8, v0, Lhzc;->bwj:Ldgm;

    invoke-static {v8, v3}, Licc;->a(Ldgm;Ljoe;)Licd;

    move-result-object v3

    if-eqz v3, :cond_9

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_5

    :cond_a
    invoke-virtual {v2}, Ljqq;->bsw()Z

    move-result v11

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v13, v3

    check-cast v13, Licd;

    invoke-virtual {v13}, Licd;->aUK()Z

    move-result v3

    if-eqz v3, :cond_b

    iget v6, v13, Licd;->dxL:I

    sget-object v3, Ljqq;->ezg:Ljsm;

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljqq;

    iget-object v3, v3, Ljqq;->ezh:[Ljqw;

    array-length v3, v3

    if-ge v6, v3, :cond_b

    iget-object v2, v2, Ljqq;->ezh:[Ljqw;

    iget v3, v13, Licd;->dxL:I

    aget-object v6, v2, v3

    new-instance v2, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;

    invoke-virtual {v6}, Ljqw;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Ljqw;->getDescription()Ljava/lang/String;

    move-result-object v4

    if-eqz v11, :cond_c

    iget-object v5, v13, Licd;->aqX:Ljava/lang/String;

    :goto_6
    invoke-virtual {v6}, Ljqw;->bsC()Z

    move-result v7

    if-eqz v7, :cond_d

    invoke-virtual {v6}, Ljqw;->bsy()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    :goto_7
    iget-object v7, v13, Licd;->mData:Ljava/lang/String;

    iget-object v8, v13, Licd;->bse:Ljava/lang/String;

    const/4 v9, 0x1

    const/4 v10, 0x1

    iget v12, v13, Licd;->dxK:I

    invoke-direct/range {v2 .. v12}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ZZZI)V

    new-instance v3, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;

    move-object/from16 v0, v18

    invoke-direct {v3, v0, v2}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;-><init>(Ljqg;Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;)V

    iget-object v2, v13, Licd;->dxJ:Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->kp(Ljava/lang/String;)V

    move-object v2, v3

    goto/16 :goto_1

    :cond_c
    invoke-virtual {v6}, Ljqw;->oj()Ljava/lang/String;

    move-result-object v5

    goto :goto_6

    :cond_d
    const/4 v6, 0x0

    goto :goto_7

    :cond_e
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v13, v2

    check-cast v13, Licd;

    invoke-virtual {v13}, Licd;->aUK()Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, v13, Licd;->dxH:Ljava/lang/String;

    if-nez v2, :cond_10

    const-string v3, ""

    :goto_8
    iget-object v2, v13, Licd;->dxI:Ljava/lang/String;

    if-nez v2, :cond_11

    const-string v4, ""

    :goto_9
    new-instance v2, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;

    iget-object v5, v13, Licd;->aqX:Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, v13, Licd;->mData:Ljava/lang/String;

    iget-object v8, v13, Licd;->bse:Ljava/lang/String;

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x1

    iget v12, v13, Licd;->dxK:I

    invoke-direct/range {v2 .. v12}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ZZZI)V

    new-instance v3, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;

    move-object/from16 v0, v18

    invoke-direct {v3, v0, v2}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;-><init>(Ljqg;Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;)V

    iget-object v2, v13, Licd;->dxJ:Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->kp(Ljava/lang/String;)V

    move-object v2, v3

    goto/16 :goto_1

    :cond_10
    iget-object v3, v13, Licd;->dxH:Ljava/lang/String;

    goto :goto_8

    :cond_11
    iget-object v4, v13, Licd;->dxI:Ljava/lang/String;

    goto :goto_9

    :cond_12
    new-instance v2, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;

    move-object/from16 v0, v18

    invoke-direct {v2, v0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;-><init>(Ljqg;)V

    goto/16 :goto_1

    .line 948
    :cond_13
    sget-object v2, Ljrb;->ezS:Ljsm;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_14

    .line 950
    new-instance v3, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;

    sget-object v2, Ljrb;->ezS:Ljsm;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljrb;

    iget-object v2, v2, Ljrb;->ezT:Ljpe;

    move-object/from16 v0, v18

    invoke-direct {v3, v0, v2}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;-><init>(Ljqg;Ljpe;)V

    move-object v2, v3

    goto/16 :goto_1

    .line 953
    :cond_14
    sget-object v2, Ljqo;->ezd:Ljsm;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_17

    .line 955
    sget-object v2, Ljqo;->ezd:Ljsm;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljqo;

    new-instance v4, Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;

    invoke-virtual {v2}, Ljqo;->baX()Z

    move-result v3

    if-eqz v3, :cond_15

    invoke-virtual {v2}, Ljqo;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    :goto_a
    invoke-virtual {v2}, Ljqo;->oP()Z

    move-result v5

    if-eqz v5, :cond_16

    invoke-virtual {v2}, Ljqo;->getType()I

    move-result v2

    :goto_b
    move-object/from16 v0, v18

    invoke-direct {v4, v0, v3, v2}, Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;-><init>(Ljqg;Landroid/net/Uri;I)V

    move-object v2, v4

    goto/16 :goto_1

    :cond_15
    const/4 v3, 0x0

    goto :goto_a

    :cond_16
    const/4 v2, 0x0

    goto :goto_b

    .line 956
    :cond_17
    sget-object v2, Ljqk;->eyU:Ljsm;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_18

    .line 958
    new-instance v2, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    move-object/from16 v0, v18

    invoke-direct {v2, v0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;-><init>(Ljqg;)V

    goto/16 :goto_1

    .line 959
    :cond_18
    sget-object v2, Ljrj;->eAA:Ljsm;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_19

    .line 961
    new-instance v2, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    move-object/from16 v0, v18

    invoke-direct {v2, v0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;-><init>(Ljqg;)V

    goto/16 :goto_1

    .line 962
    :cond_19
    sget-object v2, Ljri;->eAv:Ljsm;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1a

    .line 964
    new-instance v2, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;

    move-object/from16 v0, v18

    invoke-direct {v2, v0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;-><init>(Ljqg;)V

    goto/16 :goto_1

    .line 965
    :cond_1a
    sget-object v2, Ljrd;->eAe:Ljsm;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1b

    .line 967
    new-instance v2, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;

    move-object/from16 v0, v18

    invoke-direct {v2, v0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;-><init>(Ljqg;)V

    goto/16 :goto_1

    .line 968
    :cond_1b
    sget-object v2, Ljqx;->ezE:Ljsm;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1c

    .line 970
    new-instance v2, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    move-object/from16 v0, v18

    invoke-direct {v2, v0}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;-><init>(Ljqg;)V

    goto/16 :goto_1

    .line 971
    :cond_1c
    sget-object v2, Ljqn;->eza:Ljsm;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1d

    .line 973
    new-instance v2, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;

    move-object/from16 v0, v18

    invoke-direct {v2, v0}, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;-><init>(Ljqg;)V

    goto/16 :goto_1

    .line 976
    :cond_1d
    const-string v2, "ActionV2Processor"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown argument with id "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljqg;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 934
    :goto_c
    add-int/lit8 v2, v14, 0x1

    move v14, v2

    goto/16 :goto_0

    .line 991
    :cond_1e
    invoke-interface {v15, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 995
    :cond_1f
    new-instance v2, Lcom/google/android/search/shared/actions/modular/ModularAction;

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-direct {v2, v0, v15, v1}, Lcom/google/android/search/shared/actions/modular/ModularAction;-><init>(Ljrc;Ljava/util/List;I)V

    .line 998
    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lhzc;->a(Ljrc;Lcom/google/android/search/shared/actions/modular/ModularAction;)V

    .line 999
    invoke-static {v2}, Ldxv;->d(Lcom/google/android/search/shared/actions/modular/ModularAction;)V

    goto/16 :goto_2
.end method

.method final a(Ljkt;Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 1

    .prologue
    .line 667
    if-nez p2, :cond_1

    .line 679
    :cond_0
    :goto_0
    return-void

    .line 671
    :cond_1
    iget-object v0, p1, Ljkt;->eqo:Ljlm;

    if-eqz v0, :cond_0

    .line 672
    iget-object v0, p1, Ljkt;->eqo:Ljlm;

    invoke-virtual {v0}, Ljlm;->bph()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 673
    invoke-interface {p2}, Lcom/google/android/search/shared/actions/VoiceAction;->agl()Z

    .line 674
    iget-object v0, p0, Lhzc;->mPersonShortcutManager:Lciy;

    invoke-virtual {v0, p2}, Lciy;->d(Lcom/google/android/search/shared/actions/VoiceAction;)V

    goto :goto_0
.end method

.method final a(Ljli;Lefk;)V
    .locals 2

    .prologue
    .line 1481
    if-eqz p1, :cond_0

    .line 1482
    new-instance v0, Lhzg;

    invoke-direct {v0, p0, p1, p2}, Lhzg;-><init>(Lhzc;Ljli;Lefk;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lhzg;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1498
    :cond_0
    return-void
.end method

.method public final a(Ljoq;Lcom/google/android/search/shared/contact/PersonDisambiguation;)V
    .locals 4
    .param p1    # Ljoq;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/search/shared/contact/PersonDisambiguation;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1163
    invoke-virtual {p1}, Ljoq;->brx()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-virtual {p2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1172
    :cond_0
    :goto_0
    return-void

    .line 1167
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 1168
    invoke-virtual {p2}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v1

    .line 1169
    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amt()Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v2

    .line 1170
    iget-object v3, p0, Lhzc;->aYZ:Lciu;

    invoke-virtual {v3, v0, v2}, Lciu;->a(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Relationship;)V

    .line 1171
    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amw()V

    goto :goto_0
.end method

.method public final a(Ljkt;Lcom/google/android/shared/search/Query;Lefk;)Z
    .locals 19

    .prologue
    .line 236
    sget-object v4, Ljrc;->ezW:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljrc;

    if-eqz v4, :cond_4

    iget-object v5, v4, Ljrc;->eAd:[Ljrf;

    array-length v6, v5

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v6, :cond_4

    aget-object v7, v5, v4

    invoke-virtual {v7}, Ljrf;->getType()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lhzc;->mLoginHelper:Lcrh;

    invoke-virtual {v8}, Lcrh;->Su()Z

    move-result v8

    if-nez v8, :cond_0

    new-instance v4, Lcom/google/android/search/shared/actions/PuntAction;

    iget-object v5, v7, Ljrf;->eAl:Ljrg;

    iget-object v5, v5, Ljrg;->eAn:Ljqv;

    invoke-virtual {v5}, Ljqv;->TV()Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0a088f

    const/4 v7, 0x0

    invoke-direct/range {p0 .. p0}, Lhzc;->aUx()Landroid/content/Intent;

    move-result-object v8

    const/4 v9, 0x1

    invoke-direct/range {v4 .. v9}, Lcom/google/android/search/shared/actions/PuntAction;-><init>(Ljava/lang/CharSequence;ILjava/lang/String;Landroid/content/Intent;Z)V

    :goto_1
    if-eqz v4, :cond_5

    const/4 v5, 0x1

    new-array v5, v5, [Lcom/google/android/search/shared/actions/VoiceAction;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    invoke-static {v5}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Lefk;->ar(Ljava/lang/Object;)V

    const/4 v4, 0x1

    :goto_2
    if-nez v4, :cond_3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v6

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v7

    const/4 v4, 0x0

    sget-object v5, Ljmq;->eul:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_14

    sget-object v4, Ljmq;->eul:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljmq;

    invoke-virtual {v4}, Ljmq;->bqn()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Ljmq;->bqq()Z

    move-result v6

    if-eqz v6, :cond_12

    const/4 v5, 0x0

    invoke-virtual {v4}, Ljmq;->bqp()Z

    move-result v6

    if-eqz v6, :cond_3c

    invoke-virtual {v4}, Ljmq;->bqo()I

    move-result v4

    :goto_3
    new-instance v5, Lcom/google/android/search/shared/actions/SetTimerAction;

    invoke-direct {v5, v4}, Lcom/google/android/search/shared/actions/SetTimerAction;-><init>(I)V

    move-object v4, v5

    :cond_1
    :goto_4
    if-nez v4, :cond_2e

    const/4 v4, 0x0

    :goto_5
    if-nez v4, :cond_3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v12

    sget-object v4, Ljmg;->etv:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_2f

    const/4 v7, 0x1

    :goto_6
    sget-object v4, Ljmo;->eud:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_30

    const/4 v9, 0x1

    :goto_7
    sget-object v4, Ljlh;->ers:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_31

    const/4 v10, 0x1

    :goto_8
    sget-object v4, Ljmr;->eur:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_32

    const/4 v14, 0x1

    :goto_9
    sget-object v4, Ljkx;->eqH:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_33

    const/4 v11, 0x1

    :goto_a
    sget-object v4, Ljmh;->ety:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_34

    const/4 v15, 0x1

    :goto_b
    sget-object v4, Ljrc;->ezW:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_35

    const/16 v16, 0x1

    :goto_c
    sget-object v4, Ljmb;->eti:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_36

    sget-object v4, Lcgg;->aVA:Lcgg;

    invoke-virtual {v4}, Lcgg;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_36

    const/4 v13, 0x1

    :goto_d
    if-nez v7, :cond_2

    if-nez v9, :cond_2

    if-nez v10, :cond_2

    if-nez v14, :cond_2

    if-nez v11, :cond_2

    if-nez v15, :cond_2

    if-nez v16, :cond_2

    if-eqz v13, :cond_37

    :cond_2
    const/4 v4, 0x1

    :goto_e
    if-eqz v4, :cond_38

    new-instance v4, Lhzd;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v8, p2

    move-object/from16 v17, p3

    invoke-direct/range {v4 .. v17}, Lhzd;-><init>(Lhzc;Ljkt;ZLcom/google/android/shared/search/Query;ZZZZZZZZLefk;)V

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Void;

    invoke-virtual {v4, v5}, Lhzd;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v4, 0x1

    :goto_f
    if-nez v4, :cond_3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v4

    if-eqz v4, :cond_3a

    new-instance v4, Lcom/google/android/search/shared/actions/PuntAction;

    const-string v5, ""

    const/4 v6, 0x1

    invoke-direct {v4, v5, v6}, Lcom/google/android/search/shared/actions/PuntAction;-><init>(Ljava/lang/CharSequence;Z)V

    const/4 v5, 0x1

    new-array v5, v5, [Lcom/google/android/search/shared/actions/VoiceAction;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    invoke-static {v5}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Lefk;->ar(Ljava/lang/Object;)V

    const/4 v4, 0x1

    :goto_10
    if-eqz v4, :cond_3b

    :cond_3
    const/4 v4, 0x1

    :goto_11
    return v4

    :cond_4
    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_5
    sget-object v4, Ljmg;->etv:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_7

    const/4 v4, 0x1

    :goto_12
    sget-object v5, Ljmo;->eud:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_8

    const/4 v5, 0x1

    :goto_13
    sget-object v6, Ljlh;->ers:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_9

    const/4 v6, 0x1

    :goto_14
    sget-object v7, Ljmr;->eur:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_a

    const/4 v7, 0x1

    :goto_15
    if-nez v4, :cond_6

    if-nez v5, :cond_6

    if-nez v6, :cond_6

    if-nez v7, :cond_6

    sget-object v4, Ljrc;->ezW:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljrc;

    if-eqz v4, :cond_c

    iget-object v5, v4, Ljrc;->ezX:[Ljqg;

    array-length v6, v5

    const/4 v4, 0x0

    :goto_16
    if-ge v4, v6, :cond_c

    aget-object v7, v5, v4

    sget-object v8, Ljqi;->eyH:Ljsm;

    invoke-virtual {v7, v8}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_b

    const/4 v4, 0x1

    :goto_17
    if-eqz v4, :cond_d

    :cond_6
    const/4 v4, 0x1

    :goto_18
    if-nez v4, :cond_e

    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_7
    const/4 v4, 0x0

    goto :goto_12

    :cond_8
    const/4 v5, 0x0

    goto :goto_13

    :cond_9
    const/4 v6, 0x0

    goto :goto_14

    :cond_a
    const/4 v7, 0x0

    goto :goto_15

    :cond_b
    add-int/lit8 v4, v4, 0x1

    goto :goto_16

    :cond_c
    const/4 v4, 0x0

    goto :goto_17

    :cond_d
    const/4 v4, 0x0

    goto :goto_18

    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lhzc;->mGooglePlayServicesHelper:Lcha;

    invoke-virtual {v4}, Lcha;->Fi()Z

    move-result v4

    if-nez v4, :cond_f

    const/4 v4, 0x1

    new-array v10, v4, [Lcom/google/android/search/shared/actions/VoiceAction;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lhzc;->mGooglePlayServicesHelper:Lcha;

    invoke-virtual {v4}, Lcha;->Fj()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    const-string v4, "ActionV2Processor"

    const-string v5, "createGooglePlayServicesPuntAction() : Broken, cause unknown."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    const v5, 0x7f0a0721

    const/4 v6, 0x0

    const/4 v8, 0x0

    :goto_19
    new-instance v4, Lcom/google/android/search/shared/actions/PuntAction;

    const/4 v7, 0x0

    const/4 v9, 0x1

    invoke-direct/range {v4 .. v9}, Lcom/google/android/search/shared/actions/PuntAction;-><init>(IIILandroid/content/Intent;Z)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lhzc;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Landroid/content/Intent;

    const/4 v7, 0x0

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lepd;->a(Landroid/content/pm/PackageManager;[Landroid/content/Intent;)Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/search/shared/actions/PuntAction;->a(Lcom/google/android/shared/util/MatchingAppInfo;)V

    const/16 v5, 0x53

    invoke-virtual {v4, v5}, Lcom/google/android/search/shared/actions/PuntAction;->gz(I)V

    aput-object v4, v10, v11

    invoke-static {v10}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Lefk;->ar(Ljava/lang/Object;)V

    const/4 v4, 0x1

    goto/16 :goto_2

    :pswitch_1
    const-string v4, "ActionV2Processor"

    const-string v5, "createGooglePlayServicesPuntAction() : Old Version"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    const v5, 0x7f0a071e

    const v6, 0x7f0a071d

    move-object/from16 v0, p0

    iget-object v4, v0, Lhzc;->mGooglePlayServicesHelper:Lcha;

    const/4 v7, 0x2

    invoke-virtual {v4, v7}, Lcha;->eB(I)Landroid/content/Intent;

    move-result-object v8

    goto :goto_19

    :pswitch_2
    const-string v4, "ActionV2Processor"

    const-string v5, "createGooglePlayServicesPuntAction() : Disabled"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    const v5, 0x7f0a0720

    const v6, 0x7f0a071f

    move-object/from16 v0, p0

    iget-object v4, v0, Lhzc;->mGooglePlayServicesHelper:Lcha;

    const/4 v7, 0x3

    invoke-virtual {v4, v7}, Lcha;->eB(I)Landroid/content/Intent;

    move-result-object v8

    goto :goto_19

    :cond_f
    invoke-virtual/range {p0 .. p0}, Lhzc;->aUv()Z

    invoke-virtual/range {p0 .. p0}, Lhzc;->aUw()Z

    move-result v4

    if-eqz v4, :cond_11

    move-object/from16 v0, p0

    iget-object v4, v0, Lhzc;->mScreenStateHelper:Ldmh;

    invoke-virtual {v4}, Ldmh;->adq()I

    move-result v4

    sget v5, Ldmh;->bCo:I

    if-ne v4, v5, :cond_10

    new-instance v4, Lcom/google/android/search/shared/actions/PuntAction;

    const-string v5, "Please unlock your phone and try again."

    const/4 v6, 0x1

    invoke-direct {v4, v5, v6}, Lcom/google/android/search/shared/actions/PuntAction;-><init>(Ljava/lang/CharSequence;Z)V

    const/4 v5, 0x1

    new-array v5, v5, [Lcom/google/android/search/shared/actions/VoiceAction;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    invoke-static {v5}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Lefk;->ar(Ljava/lang/Object;)V

    const/4 v4, 0x1

    goto/16 :goto_2

    :cond_10
    const/4 v4, 0x1

    new-array v4, v4, [Lcom/google/android/search/shared/actions/VoiceAction;

    const/4 v5, 0x0

    new-instance v6, Lcom/google/android/search/shared/actions/ContactOptInAction;

    const/4 v7, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/google/android/search/shared/actions/ContactOptInAction;-><init>(ZLjava/lang/String;)V

    aput-object v6, v4, v5

    invoke-static {v4}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Lefk;->ar(Ljava/lang/Object;)V

    const/4 v4, 0x1

    goto/16 :goto_2

    :cond_11
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_12
    iget-object v6, v4, Ljmq;->erP:Ljkh;

    if-eqz v6, :cond_13

    iget-object v6, v4, Ljmq;->erP:Ljkh;

    invoke-virtual {v6}, Ljkh;->bil()Z

    move-result v6

    if-eqz v6, :cond_13

    iget-object v6, v4, Ljmq;->erP:Ljkh;

    invoke-virtual {v6}, Ljkh;->getHour()I

    move-result v6

    iget-object v4, v4, Ljmq;->erP:Ljkh;

    invoke-virtual {v4}, Ljkh;->getMinute()I

    move-result v7

    new-instance v4, Lcom/google/android/search/shared/actions/SetAlarmAction;

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/search/shared/actions/SetAlarmAction;-><init>(Ljava/lang/String;II)V

    goto/16 :goto_4

    :cond_13
    new-instance v4, Lcom/google/android/search/shared/actions/SetAlarmAction;

    invoke-direct {v4, v5}, Lcom/google/android/search/shared/actions/SetAlarmAction;-><init>(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_14
    sget-object v5, Ljmp;->eui:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_16

    sget-object v4, Ljmp;->eui:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljmp;

    new-instance v5, Lcom/google/android/search/shared/actions/SelfNoteAction;

    invoke-virtual {v4}, Ljmp;->getBody()Ljava/lang/String;

    move-result-object v8

    if-eqz v6, :cond_15

    move-object/from16 v0, p0

    iget-object v4, v0, Lhzc;->bDB:Ligi;

    invoke-interface {v4}, Ligi;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Future;

    :goto_1a
    invoke-direct {v5, v8, v4, v7}, Lcom/google/android/search/shared/actions/SelfNoteAction;-><init>(Ljava/lang/String;Ljava/util/concurrent/Future;Z)V

    move-object v4, v5

    goto/16 :goto_4

    :cond_15
    const/4 v4, 0x0

    goto :goto_1a

    :cond_16
    sget-object v5, Ljkv;->equ:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_22

    sget-object v4, Ljkv;->equ:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljkv;

    iget-object v5, v4, Ljkv;->eqv:Ljnp;

    if-eqz v5, :cond_21

    iget-object v7, v4, Ljkv;->eqv:Ljnp;

    iget-object v4, v7, Ljnp;->euY:Ljno;

    if-eqz v4, :cond_18

    iget-object v4, v7, Ljnp;->euY:Ljno;

    invoke-virtual {v4}, Ljno;->bqQ()Z

    move-result v4

    if-nez v4, :cond_18

    const/4 v12, 0x1

    :goto_1b
    iget-object v4, v7, Ljnp;->euY:Ljno;

    if-eqz v4, :cond_19

    iget-object v4, v7, Ljnp;->euY:Ljno;

    invoke-virtual {v4}, Ljno;->bqP()Z

    move-result v4

    if-nez v4, :cond_19

    const/4 v13, 0x1

    :goto_1c
    iget-object v4, v7, Ljnp;->euY:Ljno;

    if-eqz v4, :cond_1a

    iget-object v4, v7, Ljnp;->euY:Ljno;

    invoke-virtual {v4}, Ljno;->Pu()J

    move-result-wide v10

    :goto_1d
    iget-object v4, v7, Ljnp;->euZ:Ljno;

    if-eqz v4, :cond_1b

    iget-object v4, v7, Ljnp;->euZ:Ljno;

    invoke-virtual {v4}, Ljno;->Pu()J

    move-result-wide v14

    :goto_1e
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, v7, Ljnp;->evr:[Ljnq;

    array-length v6, v5

    const/4 v4, 0x0

    :goto_1f
    if-ge v4, v6, :cond_1c

    aget-object v9, v5, v4

    invoke-virtual {v9}, Ljnq;->baV()Z

    move-result v16

    if-eqz v16, :cond_17

    invoke-virtual {v9}, Ljnq;->getDisplayName()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_17
    add-int/lit8 v4, v4, 0x1

    goto :goto_1f

    :cond_18
    const/4 v12, 0x0

    goto :goto_1b

    :cond_19
    const/4 v13, 0x0

    goto :goto_1c

    :cond_1a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Libp;->bM(J)J

    move-result-wide v10

    goto :goto_1d

    :cond_1b
    invoke-static {v10, v11}, Libp;->bN(J)J

    move-result-wide v14

    goto :goto_1e

    :cond_1c
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, v7, Ljnp;->evt:[Ljnr;

    array-length v4, v4

    if-nez v4, :cond_1e

    sget-object v4, Lcom/google/android/search/shared/util/Reminder;->bWy:Lcom/google/android/search/shared/util/Reminder;

    invoke-interface {v9, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1d
    new-instance v5, Lcom/google/android/search/shared/actions/AddEventAction;

    invoke-virtual {v7}, Ljnp;->pO()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, Ljnp;->getLocation()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v5 .. v15}, Lcom/google/android/search/shared/actions/AddEventAction;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;JZZJ)V

    :goto_20
    move-object v4, v5

    goto/16 :goto_4

    :cond_1e
    iget-object v5, v7, Ljnp;->evt:[Ljnr;

    array-length v6, v5

    const/4 v4, 0x0

    :goto_21
    if-ge v4, v6, :cond_1d

    aget-object v16, v5, v4

    new-instance v17, Leee;

    invoke-direct/range {v17 .. v17}, Leee;-><init>()V

    invoke-virtual/range {v16 .. v16}, Ljnr;->bqT()Z

    move-result v18

    if-eqz v18, :cond_1f

    invoke-virtual/range {v16 .. v16}, Ljnr;->getMinutes()I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Leee;->bWz:I

    :cond_1f
    invoke-virtual/range {v16 .. v16}, Ljnr;->bqU()Z

    move-result v18

    if-eqz v18, :cond_20

    invoke-virtual/range {v16 .. v16}, Ljnr;->getMethod()I

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, v17

    iput v0, v1, Leee;->bWA:I

    :cond_20
    new-instance v16, Lcom/google/android/search/shared/util/Reminder;

    move-object/from16 v0, v17

    iget v0, v0, Leee;->bWz:I

    move/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, Leee;->bWA:I

    move/from16 v17, v0

    move-object/from16 v0, v16

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/search/shared/util/Reminder;-><init>(II)V

    move-object/from16 v0, v16

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_21

    :cond_21
    const/4 v5, 0x0

    goto :goto_20

    :cond_22
    sget-object v5, Ljmw;->euD:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_25

    sget-object v4, Ljmw;->euD:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljmw;

    invoke-virtual {v4}, Ljmw;->bqx()Z

    move-result v5

    if-eqz v5, :cond_23

    invoke-virtual {v4}, Ljmw;->bqw()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    const/4 v5, 0x0

    move-object v6, v5

    :goto_22
    if-eqz v6, :cond_24

    new-instance v5, Lcom/google/android/search/shared/actions/SocialUpdateAction;

    invoke-virtual {v4}, Ljmw;->pB()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4, v6}, Lcom/google/android/search/shared/actions/SocialUpdateAction;-><init>(Ljava/lang/String;Ldvl;)V

    move-object v4, v5

    goto/16 :goto_4

    :pswitch_3
    sget-object v5, Ldvl;->bNA:Ldvl;

    move-object v6, v5

    goto :goto_22

    :pswitch_4
    sget-object v5, Ldvl;->bNB:Ldvl;

    move-object v6, v5

    goto :goto_22

    :cond_23
    sget-object v5, Ldvl;->bNA:Ldvl;

    move-object v6, v5

    goto :goto_22

    :cond_24
    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_25
    sget-object v5, Ljkw;->eqw:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_28

    sget-object v4, Ljkw;->eqw:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljkw;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhzc;->mLoginHelper:Lcrh;

    invoke-virtual {v5}, Lcrh;->Su()Z

    move-result v5

    if-eqz v5, :cond_26

    move-object/from16 v0, p0

    iget-object v5, v0, Lhzc;->mGsaConfigFlags:Lchk;

    move-object/from16 v0, p0

    iget-object v6, v0, Lhzc;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Lchk;->GL()Lcom/google/android/search/shared/actions/RemindersConfigFlags;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/search/shared/actions/SetReminderAction;->a(Ljkw;Lcom/google/android/search/shared/actions/RemindersConfigFlags;)Lcom/google/android/search/shared/actions/SetReminderAction;

    move-result-object v4

    goto/16 :goto_4

    :cond_26
    move-object/from16 v0, p0

    iget-object v4, v0, Lhzc;->mNowOptInSettings:Lcin;

    invoke-interface {v4}, Lcin;->KF()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_27

    new-instance v4, Lcom/google/android/search/shared/actions/PuntAction;

    const v5, 0x7f0a088e

    const v6, 0x7f0a088f

    const/4 v7, 0x0

    invoke-direct/range {p0 .. p0}, Lhzc;->aUx()Landroid/content/Intent;

    move-result-object v8

    const/4 v9, 0x1

    invoke-direct/range {v4 .. v9}, Lcom/google/android/search/shared/actions/PuntAction;-><init>(IIILandroid/content/Intent;Z)V

    goto/16 :goto_4

    :cond_27
    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_28
    sget-object v5, Ljmv;->euA:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_2a

    sget-object v4, Ljmv;->euA:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljmv;

    invoke-virtual {v4}, Ljmv;->bqv()Z

    move-result v5

    if-nez v5, :cond_29

    const-string v4, "ActionV2Processor"

    const-string v5, "Unsupported action without explanation"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_29
    new-instance v5, Lcom/google/android/search/shared/actions/PuntAction;

    invoke-virtual {v4}, Ljmv;->bqu()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    invoke-direct {v5, v4, v6}, Lcom/google/android/search/shared/actions/PuntAction;-><init>(Ljava/lang/CharSequence;Z)V

    move-object v4, v5

    goto/16 :goto_4

    :cond_2a
    sget-object v5, Ljmn;->euc:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_2d

    move-object/from16 v0, p0

    iget-object v4, v0, Lhzc;->mMessageManager:Lbxy;

    invoke-virtual {v4}, Lbxy;->Bx()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2c

    move-object/from16 v0, p0

    iget-object v5, v0, Lhzc;->mAdaptiveTtsHelper:Lbwo;

    const-string v6, "read-message"

    invoke-virtual {v5, v6}, Lbwo;->fK(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v6, v0, Lhzc;->mContext:Landroid/content/Context;

    const v7, 0x7f0a09c3

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lhzc;->mAdaptiveTtsHelper:Lbwo;

    const-string v6, "reply"

    invoke-virtual {v4, v6}, Lbwo;->fJ(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2b

    const-string v4, " "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhzc;->mContext:Landroid/content/Context;

    const v6, 0x7f0a0975

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lhzc;->mContext:Landroid/content/Context;

    const v10, 0x7f0a09c4

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v4, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2b
    move-object/from16 v0, p0

    iget-object v4, v0, Lhzc;->mMessageManager:Lbxy;

    invoke-virtual {v4}, Lbxy;->BB()V

    new-instance v4, Lcom/google/android/search/shared/actions/ReadNotificationAction;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/search/shared/actions/ReadNotificationAction;-><init>(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_2c
    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_2d
    sget-object v5, Ljlt;->esN:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_1

    sget-object v4, Ljlt;->esN:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljlt;

    new-instance v5, Lcom/google/android/search/shared/actions/MediaControlAction;

    invoke-virtual {v4}, Ljlt;->getAction()I

    move-result v6

    invoke-virtual {v4}, Ljlt;->bpu()I

    move-result v4

    invoke-direct {v5, v6, v4}, Lcom/google/android/search/shared/actions/MediaControlAction;-><init>(II)V

    move-object v4, v5

    goto/16 :goto_4

    :cond_2e
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v4}, Lhzc;->a(Ljkt;Lcom/google/android/search/shared/actions/VoiceAction;)V

    const/4 v5, 0x1

    new-array v5, v5, [Lcom/google/android/search/shared/actions/VoiceAction;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    invoke-static {v5}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Lefk;->ar(Ljava/lang/Object;)V

    const/4 v4, 0x1

    goto/16 :goto_5

    :cond_2f
    const/4 v7, 0x0

    goto/16 :goto_6

    :cond_30
    const/4 v9, 0x0

    goto/16 :goto_7

    :cond_31
    const/4 v10, 0x0

    goto/16 :goto_8

    :cond_32
    const/4 v14, 0x0

    goto/16 :goto_9

    :cond_33
    const/4 v11, 0x0

    goto/16 :goto_a

    :cond_34
    const/4 v15, 0x0

    goto/16 :goto_b

    :cond_35
    const/16 v16, 0x0

    goto/16 :goto_c

    :cond_36
    const/4 v13, 0x0

    goto/16 :goto_d

    :cond_37
    const/4 v4, 0x0

    goto/16 :goto_e

    :cond_38
    sget-object v4, Ljli;->erx:Ljsm;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_39

    new-instance v4, Lhze;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v4, v0, v1, v2}, Lhze;-><init>(Lhzc;Ljkt;Lefk;)V

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Void;

    invoke-virtual {v4, v5}, Lhze;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v4, 0x1

    goto/16 :goto_f

    :cond_39
    const/4 v4, 0x0

    goto/16 :goto_f

    :cond_3a
    const/4 v4, 0x0

    goto/16 :goto_10

    :cond_3b
    const/4 v4, 0x0

    goto/16 :goto_11

    :cond_3c
    move v4, v5

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method final aUv()Z
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lhzc;->mNowOptInSettings:Lcin;

    invoke-interface {v0}, Lcin;->KF()I

    .line 403
    const/4 v0, 0x0

    return v0
.end method

.method final aUw()Z
    .locals 2

    .prologue
    .line 410
    iget-object v0, p0, Lhzc;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->CS()Z

    move-result v0

    .line 411
    iget-object v1, p0, Lhzc;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->FI()I

    move-result v1

    .line 416
    if-nez v0, :cond_0

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b(Ljli;)Ljava/util/List;
    .locals 4

    .prologue
    .line 1237
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1238
    iget-boolean v1, p0, Lhzc;->dva:Z

    if-eqz v1, :cond_0

    .line 1239
    iget-object v1, p0, Lhzc;->cXk:Libv;

    iget-object v2, p0, Lhzc;->mDeviceCapabilityManager:Lenm;

    iget v3, p0, Lhzc;->aUA:I

    invoke-static {p1, v0}, Ldkw;->a(Ljli;Ljava/util/List;)V

    invoke-static {p1, v1, v2, v3, v0}, Ldkw;->a(Ljli;Libv;Lenm;ILjava/util/List;)V

    .line 1242
    :cond_0
    return-object v0
.end method

.method public final e(Lefk;)V
    .locals 2

    .prologue
    .line 1466
    iget-object v0, p0, Lhzc;->mActionDiscoveryData:Lgpc;

    invoke-virtual {v0}, Lgpc;->auB()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1467
    iget-object v0, p0, Lhzc;->mActionDiscoveryData:Lgpc;

    invoke-virtual {v0}, Lgpc;->aIX()Ljli;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lhzc;->a(Ljli;Lefk;)V

    .line 1477
    :goto_0
    return-void

    .line 1469
    :cond_0
    iget-object v0, p0, Lhzc;->mActionDiscoveryData:Lgpc;

    new-instance v1, Lhzf;

    invoke-direct {v1, p0, p1}, Lhzf;-><init>(Lhzc;Lefk;)V

    invoke-virtual {v0, v1}, Lgpc;->e(Lemy;)V

    goto :goto_0
.end method
