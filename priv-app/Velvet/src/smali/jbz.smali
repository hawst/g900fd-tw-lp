.class public final Ljbz;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public dMF:[Liyg;

.field public dMM:Ljcn;

.field private dMY:I

.field private dOA:J

.field private dOs:Ljava/lang/String;

.field public dOz:Ljbg;

.field public dZN:Ljbw;

.field public dZO:Ljbp;

.field private dZP:J

.field private dZQ:Ljava/lang/String;

.field private dZt:I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 62520
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 62521
    iput v2, p0, Ljbz;->aez:I

    iput-object v1, p0, Ljbz;->dZN:Ljbw;

    iput-object v1, p0, Ljbz;->dZO:Ljbp;

    iput-wide v4, p0, Ljbz;->dZP:J

    invoke-static {}, Liyg;->bbv()[Liyg;

    move-result-object v0

    iput-object v0, p0, Ljbz;->dMF:[Liyg;

    iput-object v1, p0, Ljbz;->dOz:Ljbg;

    iput v2, p0, Ljbz;->dMY:I

    iput v2, p0, Ljbz;->dZt:I

    const-string v0, ""

    iput-object v0, p0, Ljbz;->dOs:Ljava/lang/String;

    iput-object v1, p0, Ljbz;->dMM:Ljcn;

    const-string v0, ""

    iput-object v0, p0, Ljbz;->dZQ:Ljava/lang/String;

    iput-wide v4, p0, Ljbz;->dOA:J

    iput-object v1, p0, Ljbz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbz;->eCz:I

    .line 62522
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 62366
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljbz;->dZN:Ljbw;

    if-nez v0, :cond_1

    new-instance v0, Ljbw;

    invoke-direct {v0}, Ljbw;-><init>()V

    iput-object v0, p0, Ljbz;->dZN:Ljbw;

    :cond_1
    iget-object v0, p0, Ljbz;->dZN:Ljbw;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljbz;->dZO:Ljbp;

    if-nez v0, :cond_2

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljbz;->dZO:Ljbp;

    :cond_2
    iget-object v0, p0, Ljbz;->dZO:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljbz;->dZP:J

    iget v0, p0, Ljbz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbz;->aez:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljbz;->dMF:[Liyg;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Liyg;

    if-eqz v0, :cond_3

    iget-object v3, p0, Ljbz;->dMF:[Liyg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Liyg;

    invoke-direct {v3}, Liyg;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ljbz;->dMF:[Liyg;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Liyg;

    invoke-direct {v3}, Liyg;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljbz;->dMF:[Liyg;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljbz;->dOz:Ljbg;

    if-nez v0, :cond_6

    new-instance v0, Ljbg;

    invoke-direct {v0}, Ljbg;-><init>()V

    iput-object v0, p0, Ljbz;->dOz:Ljbg;

    :cond_6
    iget-object v0, p0, Ljbz;->dOz:Ljbg;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljbz;->dZt:I

    iget v0, p0, Ljbz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbz;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbz;->dOs:Ljava/lang/String;

    iget v0, p0, Ljbz;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljbz;->aez:I

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ljbz;->dMM:Ljcn;

    if-nez v0, :cond_7

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljbz;->dMM:Ljcn;

    :cond_7
    iget-object v0, p0, Ljbz;->dMM:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbz;->dZQ:Ljava/lang/String;

    iget v0, p0, Ljbz;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljbz;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljbz;->dOA:J

    iget v0, p0, Ljbz;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljbz;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljbz;->dMY:I

    iget v0, p0, Ljbz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbz;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 62545
    iget-object v0, p0, Ljbz;->dZN:Ljbw;

    if-eqz v0, :cond_0

    .line 62546
    const/4 v0, 0x1

    iget-object v1, p0, Ljbz;->dZN:Ljbw;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 62548
    :cond_0
    iget-object v0, p0, Ljbz;->dZO:Ljbp;

    if-eqz v0, :cond_1

    .line 62549
    const/4 v0, 0x2

    iget-object v1, p0, Ljbz;->dZO:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 62551
    :cond_1
    iget v0, p0, Ljbz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 62552
    const/4 v0, 0x3

    iget-wide v2, p0, Ljbz;->dZP:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 62554
    :cond_2
    iget-object v0, p0, Ljbz;->dMF:[Liyg;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljbz;->dMF:[Liyg;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 62555
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljbz;->dMF:[Liyg;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 62556
    iget-object v1, p0, Ljbz;->dMF:[Liyg;

    aget-object v1, v1, v0

    .line 62557
    if-eqz v1, :cond_3

    .line 62558
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 62555
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 62562
    :cond_4
    iget-object v0, p0, Ljbz;->dOz:Ljbg;

    if-eqz v0, :cond_5

    .line 62563
    const/4 v0, 0x5

    iget-object v1, p0, Ljbz;->dOz:Ljbg;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 62565
    :cond_5
    iget v0, p0, Ljbz;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    .line 62566
    const/4 v0, 0x6

    iget v1, p0, Ljbz;->dZt:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 62568
    :cond_6
    iget v0, p0, Ljbz;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_7

    .line 62569
    const/4 v0, 0x7

    iget-object v1, p0, Ljbz;->dOs:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 62571
    :cond_7
    iget-object v0, p0, Ljbz;->dMM:Ljcn;

    if-eqz v0, :cond_8

    .line 62572
    const/16 v0, 0x8

    iget-object v1, p0, Ljbz;->dMM:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 62574
    :cond_8
    iget v0, p0, Ljbz;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_9

    .line 62575
    const/16 v0, 0x9

    iget-object v1, p0, Ljbz;->dZQ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 62577
    :cond_9
    iget v0, p0, Ljbz;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_a

    .line 62578
    const/16 v0, 0xa

    iget-wide v2, p0, Ljbz;->dOA:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 62580
    :cond_a
    iget v0, p0, Ljbz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_b

    .line 62581
    const/16 v0, 0xb

    iget v1, p0, Ljbz;->dMY:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 62583
    :cond_b
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 62584
    return-void
.end method

.method public final baL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62457
    iget-object v0, p0, Ljbz;->dOs:Ljava/lang/String;

    return-object v0
.end method

.method public final baQ()J
    .locals 2

    .prologue
    .line 62504
    iget-wide v0, p0, Ljbz;->dOA:J

    return-wide v0
.end method

.method public final bfC()I
    .locals 1

    .prologue
    .line 62438
    iget v0, p0, Ljbz;->dZt:I

    return v0
.end method

.method public final bfD()Z
    .locals 1

    .prologue
    .line 62446
    iget v0, p0, Ljbz;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bfR()J
    .locals 2

    .prologue
    .line 62394
    iget-wide v0, p0, Ljbz;->dZP:J

    return-wide v0
.end method

.method public final bfS()Z
    .locals 1

    .prologue
    .line 62402
    iget v0, p0, Ljbz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final cL(J)Ljbz;
    .locals 1

    .prologue
    .line 62397
    iput-wide p1, p0, Ljbz;->dZP:J

    .line 62398
    iget v0, p0, Ljbz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbz;->aez:I

    .line 62399
    return-object p0
.end method

.method public final cM(J)Ljbz;
    .locals 1

    .prologue
    .line 62507
    iput-wide p1, p0, Ljbz;->dOA:J

    .line 62508
    iget v0, p0, Ljbz;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljbz;->aez:I

    .line 62509
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 62588
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 62589
    iget-object v1, p0, Ljbz;->dZN:Ljbw;

    if-eqz v1, :cond_0

    .line 62590
    const/4 v1, 0x1

    iget-object v2, p0, Ljbz;->dZN:Ljbw;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62593
    :cond_0
    iget-object v1, p0, Ljbz;->dZO:Ljbp;

    if-eqz v1, :cond_1

    .line 62594
    const/4 v1, 0x2

    iget-object v2, p0, Ljbz;->dZO:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62597
    :cond_1
    iget v1, p0, Ljbz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    .line 62598
    const/4 v1, 0x3

    iget-wide v2, p0, Ljbz;->dZP:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 62601
    :cond_2
    iget-object v1, p0, Ljbz;->dMF:[Liyg;

    if-eqz v1, :cond_5

    iget-object v1, p0, Ljbz;->dMF:[Liyg;

    array-length v1, v1

    if-lez v1, :cond_5

    .line 62602
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljbz;->dMF:[Liyg;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 62603
    iget-object v2, p0, Ljbz;->dMF:[Liyg;

    aget-object v2, v2, v0

    .line 62604
    if-eqz v2, :cond_3

    .line 62605
    const/4 v3, 0x4

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 62602
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 62610
    :cond_5
    iget-object v1, p0, Ljbz;->dOz:Ljbg;

    if-eqz v1, :cond_6

    .line 62611
    const/4 v1, 0x5

    iget-object v2, p0, Ljbz;->dOz:Ljbg;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62614
    :cond_6
    iget v1, p0, Ljbz;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_7

    .line 62615
    const/4 v1, 0x6

    iget v2, p0, Ljbz;->dZt:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 62618
    :cond_7
    iget v1, p0, Ljbz;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_8

    .line 62619
    const/4 v1, 0x7

    iget-object v2, p0, Ljbz;->dOs:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62622
    :cond_8
    iget-object v1, p0, Ljbz;->dMM:Ljcn;

    if-eqz v1, :cond_9

    .line 62623
    const/16 v1, 0x8

    iget-object v2, p0, Ljbz;->dMM:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62626
    :cond_9
    iget v1, p0, Ljbz;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_a

    .line 62627
    const/16 v1, 0x9

    iget-object v2, p0, Ljbz;->dZQ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62630
    :cond_a
    iget v1, p0, Ljbz;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_b

    .line 62631
    const/16 v1, 0xa

    iget-wide v2, p0, Ljbz;->dOA:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 62634
    :cond_b
    iget v1, p0, Ljbz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_c

    .line 62635
    const/16 v1, 0xb

    iget v2, p0, Ljbz;->dMY:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 62638
    :cond_c
    return v0
.end method

.method public final oA(I)Ljbz;
    .locals 1

    .prologue
    .line 62441
    const/16 v0, 0x49

    iput v0, p0, Ljbz;->dZt:I

    .line 62442
    iget v0, p0, Ljbz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbz;->aez:I

    .line 62443
    return-object p0
.end method

.method public final sM(Ljava/lang/String;)Ljbz;
    .locals 1

    .prologue
    .line 62460
    if-nez p1, :cond_0

    .line 62461
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 62463
    :cond_0
    iput-object p1, p0, Ljbz;->dOs:Ljava/lang/String;

    .line 62464
    iget v0, p0, Ljbz;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljbz;->aez:I

    .line 62465
    return-object p0
.end method
