.class public final Lags;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# instance fields
.field private Xu:Lagx;


# direct methods
.method public constructor <init>(F)V
    .locals 2

    .prologue
    .line 1943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1944
    new-instance v0, Lagx;

    const v1, 0x3eb33333    # 0.35f

    invoke-direct {v0, v1}, Lagx;-><init>(F)V

    iput-object v0, p0, Lags;->Xu:Lagx;

    .line 1945
    return-void
.end method


# virtual methods
.method public final getInterpolation(F)F
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 1947
    iget-object v0, p0, Lags;->Xu:Lagx;

    sub-float v1, v2, p1

    invoke-virtual {v0, v1}, Lagx;->getInterpolation(F)F

    move-result v0

    sub-float v0, v2, v0

    return v0
.end method
