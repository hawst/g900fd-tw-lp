.class public final Lhgn;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static T(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 131
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 132
    const-string v1, "android.intent.action.WEB_SEARCH"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.google.android.googlequicksearchbox.GOOGLE_SEARCH"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.google.android.googlequicksearchbox.INTERNAL_GOOGLE_SEARCH"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lhgn;->U(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lhgn;->W(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static U(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 153
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.search.action.GLOBAL_SEARCH"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public static V(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 159
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google.android.googlequicksearchbox.TEXT_ASSIST"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.VOICE_ASSIST"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    sget v2, Lesp;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_2

    const-string v2, "android.intent.action.ASSIST"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "android.intent.extra.ASSIST_INPUT_HINT_KEYBOARD"

    invoke-virtual {p0, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public static W(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 175
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.googlequicksearchbox.TEXT_ASSIST"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lhgn;->ae(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static X(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 188
    invoke-virtual {p0}, Landroid/content/Intent;->getFlags()I

    move-result v0

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static Y(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 192
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 193
    const-string v1, "android.intent.action.SEARCH_LONG_PRESS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.speech.action.WEB_SEARCH"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.VOICE_ASSIST"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.google.android.googlequicksearchbox.VOICE_SEARCH_RECORDED_AUDIO"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static Z(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    .line 209
    const-string v0, "app_data"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 210
    if-eqz v0, :cond_0

    const-string v1, "source"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "bvra"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/shared/search/Query;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 417
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqA()Z

    move-result v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lhgn;->a(Landroid/content/Context;ZZ)Landroid/content/Intent;

    move-result-object v0

    .line 418
    const-string v1, "velvet-query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 419
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;JZ)Landroid/content/Intent;
    .locals 5
    .param p2    # Lcom/google/android/velvet/ActionData;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 373
    const/4 v0, 0x0

    invoke-static {p0, p5, v0}, Lhgn;->a(Landroid/content/Context;ZZ)Landroid/content/Intent;

    move-result-object v0

    .line 374
    invoke-static {v0, p1, p2, p3, p4}, Lhgn;->a(Landroid/content/Intent;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;J)V

    .line 375
    sget v1, Lesp;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 377
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "search"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 379
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;Z)Landroid/content/Intent;
    .locals 4
    .param p2    # Lcom/google/android/velvet/ActionData;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 390
    invoke-static {p0, p3, p3}, Lhgn;->a(Landroid/content/Context;ZZ)Landroid/content/Intent;

    move-result-object v0

    .line 391
    const-wide/16 v2, 0x64

    invoke-static {v0, p1, p2, v2, v3}, Lhgn;->a(Landroid/content/Intent;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;J)V

    .line 393
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 476
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 477
    const-string v1, "com.google.android.search.queryentry.QueryEntryActivity"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 478
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 479
    invoke-static {v0, p1}, Lhgn;->f(Landroid/content/Intent;Ljava/lang/String;)V

    .line 480
    if-eqz p3, :cond_0

    .line 481
    const-string v1, "START_QEA_BACKGROUND_OPAQUE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 483
    :cond_0
    return-object v0
.end method

.method private static a(Landroid/content/Context;ZZ)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 350
    if-eqz p1, :cond_1

    .line 351
    if-eqz p2, :cond_0

    .line 352
    const-string v0, "com.google.android.search.queryentry.QueryEntryActivity"

    invoke-static {p0, v0}, Lhgn;->s(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 362
    :goto_0
    return-object v0

    .line 355
    :cond_0
    const-string v0, "com.google.android.velvet.ui.VelvetAssistantActivity"

    invoke-static {p0, v0}, Lhgn;->s(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 360
    :cond_1
    const-string v0, "com.google.android.velvet.ui.VelvetActivity"

    invoke-static {p0, v0}, Lhgn;->s(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 361
    const-string v1, "scrim_transition_to_solid"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;ZZLjava/lang/String;)V
    .locals 5

    .prologue
    const/high16 v4, 0x10000000

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 519
    sget v0, Lesp;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_3

    .line 521
    if-eqz p2, :cond_2

    .line 522
    const-string v0, "com.android.chrome.append_task"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 523
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v0

    const v1, -0x10000001

    and-int/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 531
    :goto_0
    invoke-static {p0, p1}, Lhgn;->i(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535
    const-string v0, "trusted_application_code_extra"

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-static {p0, v3, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 537
    if-eqz p3, :cond_0

    .line 538
    const-string v0, "com.google.chrome.transition_type"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 543
    :cond_0
    if-eqz p4, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 545
    const-string v0, "com.google.android.googlequicksearchbox.extra.intent_to_assist_package"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 552
    :cond_1
    :goto_1
    return-void

    .line 525
    :cond_2
    const-string v0, "com.android.chrome.preserve_task"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 526
    invoke-virtual {p1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 549
    :cond_3
    invoke-virtual {p1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_1
.end method

.method public static a(Landroid/content/Intent;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;J)V
    .locals 3

    .prologue
    .line 438
    const/high16 v0, 0x10000

    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 439
    const/high16 v0, 0x10000000

    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 440
    const/high16 v0, 0x4000000

    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 441
    sget v0, Lesp;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 445
    const/high16 v0, 0x80000

    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 446
    const/high16 v0, 0x8000000

    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 447
    const/16 v0, 0x2000

    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 449
    :cond_0
    const-string v0, "handover-backup-query"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 450
    const-string v0, "handover-session-id"

    invoke-virtual {p0, v0, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 451
    if-eqz p2, :cond_1

    .line 452
    const-string v0, "handover-backup-action"

    invoke-virtual {p0, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 454
    :cond_1
    return-void
.end method

.method public static aOF()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 803
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.googlequicksearchbox"

    const-string v2, "com.google.android.search.core.service.SearchService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.search.core.action.REFRESH_SERVICE_STATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static aV(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 721
    if-eqz p0, :cond_4

    .line 722
    const-string v2, "com.google.android.apps.gmm.dev"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.google.android.apps.gmm"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.google.android.apps.gmm.fishfood"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.google.android.apps.maps"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    .line 723
    const/4 v0, 0x3

    .line 737
    :cond_1
    :goto_1
    return v0

    :cond_2
    move v2, v1

    .line 722
    goto :goto_0

    .line 725
    :cond_3
    invoke-static {p0}, Lhgn;->nP(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 726
    const/4 v0, 0x4

    goto :goto_1

    .line 729
    :cond_4
    if-eqz p1, :cond_5

    .line 730
    const-string v2, "gel"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 733
    const-string v0, "velvet"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 734
    const/4 v0, 0x2

    goto :goto_1

    :cond_5
    move v0, v1

    .line 737
    goto :goto_1
.end method

.method public static aa(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 232
    const-string v0, "handover-session-id"

    invoke-virtual {p0, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static ab(Landroid/content/Intent;)Lcom/google/android/shared/search/Query;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 268
    const-string v0, "velvet-query"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    return-object v0
.end method

.method public static ac(Landroid/content/Intent;)Lcom/google/android/shared/search/Query;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 286
    const-string v0, "handover-backup-query"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    return-object v0
.end method

.method public static ad(Landroid/content/Intent;)Lcom/google/android/shared/search/Query;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 741
    .line 743
    invoke-static {p0}, Lhgn;->T(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 744
    invoke-static {p0}, Lhgn;->ae(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 745
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {p0}, Lhgn;->af(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ljava/lang/CharSequence;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/CharSequence;

    .line 746
    :goto_0
    if-nez v0, :cond_10

    .line 754
    const-string v0, ""

    move-object v1, v0

    .line 756
    :goto_1
    const-string v0, "PREDICTIVE_WIDGET"

    const-string v4, "source"

    invoke-virtual {p0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 757
    const-string v0, "now-search-options"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/client/NowSearchOptions;

    .line 758
    sget-object v5, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    if-nez v0, :cond_3

    move-object v4, v3

    :goto_2
    if-nez v0, :cond_4

    :goto_3
    if-nez v0, :cond_5

    move v0, v2

    :goto_4
    invoke-virtual {v5, v1, v4, v3, v0}, Lcom/google/android/shared/search/Query;->a(Ljava/lang/CharSequence;Landroid/location/Location;Ljava/lang/String;Z)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 767
    :goto_5
    if-eqz v0, :cond_0

    .line 789
    :cond_0
    :goto_6
    if-nez v0, :cond_1

    .line 790
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    sget-object v1, Lgyt;->cZD:Lgyt;

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/shared/search/Query;->a(Ljava/io/Serializable;Landroid/os/Bundle;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 793
    :cond_1
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Lhgn;->ae(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->kW(Ljava/lang/String;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 795
    const-string v1, "android-search-app"

    invoke-static {p0, v1}, Lhgn;->e(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "velvet"

    invoke-static {v2, v1}, Lcom/google/android/shared/search/SearchBoxStats;->aA(Ljava/lang/String;Ljava/lang/String;)Lehj;

    move-result-object v1

    invoke-virtual {v1}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0

    :cond_2
    move-object v0, v3

    .line 745
    goto :goto_0

    .line 758
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/client/NowSearchOptions;->aqW()Landroid/location/Location;

    move-result-object v4

    goto :goto_2

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/client/NowSearchOptions;->aqX()Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :cond_5
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/client/NowSearchOptions;->aqr()Z

    move-result v0

    goto :goto_4

    .line 763
    :cond_6
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    const-string v3, "select_query"

    invoke-virtual {p0, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/shared/search/Query;->c(Ljava/lang/CharSequence;Z)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_5

    .line 774
    :cond_7
    invoke-static {p0}, Lhgn;->Y(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {p0}, Lhgn;->X(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 775
    const-string v0, "com.google.android.googlequicksearchbox.VOICE_SEARCH_RECORDED_AUDIO"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "com.google.android.googlequicksearchbox.RecordedVoiceSearchActivity"

    invoke-virtual {p0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 776
    :cond_8
    if-eqz v3, :cond_9

    .line 777
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v3}, Lcom/google/android/shared/search/Query;->F(Landroid/net/Uri;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto/16 :goto_6

    .line 778
    :cond_9
    invoke-static {p0}, Lhgn;->Z(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 779
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v2}, Lcom/google/android/shared/search/Query;->eQ(Z)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto/16 :goto_6

    .line 781
    :cond_a
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aph()Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto/16 :goto_6

    .line 783
    :cond_b
    const-string v0, "com.google.android.googlequicksearchbox.MUSIC_SEARCH"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-static {p0}, Lhgn;->X(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 784
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apd()Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto/16 :goto_6

    .line 785
    :cond_c
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MAIN"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.ASSIST"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.googlequicksearchbox.GOOGLE_ICON"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_d
    const/4 v2, 0x1

    :cond_e
    if-eqz v2, :cond_f

    invoke-static {p0}, Lhgn;->V(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 786
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    sget-object v1, Lgyt;->cZB:Lgyt;

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/shared/search/Query;->a(Ljava/io/Serializable;Landroid/os/Bundle;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto/16 :goto_6

    :cond_f
    move-object v0, v3

    goto/16 :goto_6

    :cond_10
    move-object v1, v0

    goto/16 :goto_1

    :cond_11
    move-object v0, v3

    goto/16 :goto_5
.end method

.method public static ae(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 799
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "android.intent.extra.ASSIST_PACKAGE"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static ae(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 810
    invoke-static {p0}, Lhgn;->af(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static af(Landroid/content/Intent;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 823
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 824
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.google.android.googlequicksearchbox.TEXT_ASSIST"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 825
    :cond_0
    const-string v0, "android.intent.extra.TEXT"

    .line 827
    :goto_0
    return-object v0

    :cond_1
    const-string v0, "query"

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/shared/search/Query;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 430
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqA()Z

    move-result v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lhgn;->a(Landroid/content/Context;ZZ)Landroid/content/Intent;

    move-result-object v0

    .line 431
    const-string v1, "velvet-query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 432
    const-string v1, "commit-query"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 433
    return-object v0
.end method

.method public static bI(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 662
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 664
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 665
    const-string v3, "android.intent.category.HOME"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 666
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/high16 v4, 0x10000

    invoke-virtual {v3, v2, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 669
    if-eqz v2, :cond_0

    iget-object v3, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-nez v3, :cond_2

    .line 672
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unable to resolve home activity: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 675
    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v3, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "com.google.android.launcher.GEL"

    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static e(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 298
    const-string v0, "app_data"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 299
    const/4 v0, 0x0

    .line 300
    if-eqz v1, :cond_0

    .line 301
    const-string v0, "source"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 303
    :cond_0
    if-nez v0, :cond_1

    .line 306
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "android-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move-object p1, v0

    goto :goto_0
.end method

.method public static f(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 457
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 458
    const-string v1, "source"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    const-string v1, "app_data"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 460
    return-void
.end method

.method public static g(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 561
    invoke-static {p1}, Lhgn;->nP(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 562
    invoke-virtual {p0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 566
    :goto_0
    return-void

    .line 564
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static i(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 686
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/high16 v1, 0x10000

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 688
    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-nez v1, :cond_1

    .line 689
    :cond_0
    const/4 v0, 0x0

    .line 692
    :goto_0
    return v0

    :cond_1
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-static {v0}, Lhgn;->nP(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static nP(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 696
    const-string v0, "com.android.chrome"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.chrome.beta"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.chrome.canary"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.google.android.apps.chrome_dev"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.google.android.apps.chrome.document"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 343
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 344
    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, p0, p1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 345
    return-object v0
.end method

.method public static t(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 487
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.search.action.GLOBAL_SEARCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 490
    const-class v1, Lcom/google/android/googlequicksearchbox/SearchActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 491
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 492
    invoke-static {v0, p1}, Lhgn;->f(Landroid/content/Intent;Ljava/lang/String;)V

    .line 493
    return-object v0
.end method

.method public static u(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 509
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.ASSIST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 512
    const-class v1, Lcom/google/android/googlequicksearchbox/SearchActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 513
    const-string v1, "source"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 514
    return-object v0
.end method
