.class final Lhbb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Livf;


# instance fields
.field private final aWr:Livq;

.field private bf:Z

.field private synthetic dbv:Lhax;

.field private final dbw:Lcom/google/android/velvet/presenter/inappwebpage/Request;


# direct methods
.method public constructor <init>(Lhax;Lcom/google/android/velvet/presenter/inappwebpage/Request;Livq;)V
    .locals 2

    .prologue
    .line 395
    iput-object p1, p0, Lhbb;->dbv:Lhax;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhbb;->bf:Z

    .line 396
    iput-object p2, p0, Lhbb;->dbw:Lcom/google/android/velvet/presenter/inappwebpage/Request;

    .line 397
    iput-object p3, p0, Lhbb;->aWr:Livq;

    .line 398
    iget-object v0, p0, Lhbb;->aWr:Livq;

    iget-object v1, p1, Lhax;->atG:Leqo;

    invoke-static {v0, p0, v1}, Livg;->a(Livq;Livf;Ljava/util/concurrent/Executor;)V

    .line 399
    return-void
.end method


# virtual methods
.method public final synthetic az(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 390
    check-cast p1, Lhao;

    iget-boolean v0, p0, Lhbb;->bf:Z

    if-nez v0, :cond_0

    iget-object v1, p0, Lhbb;->dbv:Lhax;

    iget-object v2, p0, Lhbb;->dbw:Lcom/google/android/velvet/presenter/inappwebpage/Request;

    iget-object v0, v1, Lhax;->dbq:Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;

    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;->aNo()Lcom/google/android/velvet/presenter/inappwebpage/Request;

    move-result-object v0

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, v1, Lhax;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->aNN()Lhfp;

    move-result-object v0

    iget-object v3, v1, Lhax;->dbo:Lhbt;

    invoke-virtual {v3, v0, v2, p1}, Lhbt;->a(Lhfp;Lcom/google/android/velvet/presenter/inappwebpage/Request;Lhao;)Lhbq;

    move-result-object v0

    new-instance v2, Lhbd;

    iget v3, v1, Lhax;->dbr:I

    invoke-direct {v2, v1, v3, v0}, Lhbd;-><init>(Lhax;ILhbq;)V

    new-instance v3, Lhbe;

    iget-object v4, v1, Lhax;->atG:Leqo;

    invoke-direct {v3, v1, v4, v2}, Lhbe;-><init>(Lhax;Ljava/util/concurrent/Executor;Lhbx;)V

    invoke-virtual {v0, v3}, Lhbq;->a(Lhbx;)V

    iget-object v1, v1, Lhax;->dbn:Lhav;

    invoke-virtual {v1, v0}, Lhav;->a(Lhbq;)V

    invoke-virtual {v0}, Lhbq;->aNq()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 415
    iget-boolean v0, p0, Lhbb;->bf:Z

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 416
    const-string v0, "Velvet.InAppWebPagePresenter"

    const-string v1, "Unexpected exception from UriRequestMaker"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 417
    iget-object v0, p0, Lhbb;->dbv:Lhax;

    invoke-virtual {v0}, Lhax;->aNc()V

    .line 419
    :cond_0
    return-void
.end method

.method public final stop()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 402
    iput-boolean v1, p0, Lhbb;->bf:Z

    .line 403
    iget-object v0, p0, Lhbb;->aWr:Livq;

    invoke-interface {v0, v1}, Livq;->cancel(Z)Z

    .line 404
    return-void
.end method
