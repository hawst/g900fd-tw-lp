.class public final Lgcu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgda;


# instance fields
.field private bqM:Z

.field private final cFv:Z

.field private final cGh:Ljava/util/List;

.field private final cGi:Ljava/util/List;

.field private cGj:Lgco;

.field private final mRecognitionDispatcher:Lgil;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Lgil;Z)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lgcu;->cGh:Ljava/util/List;

    .line 33
    iput-object p2, p0, Lgcu;->cGi:Ljava/util/List;

    .line 34
    iput-object p3, p0, Lgcu;->mRecognitionDispatcher:Lgil;

    .line 35
    iput-boolean p4, p0, Lgcu;->cFv:Z

    .line 36
    return-void
.end method

.method private kf(I)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 113
    iget-object v0, p0, Lgcu;->cGh:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgcu;->cGh:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_1

    .line 114
    iget-object v0, p0, Lgcu;->cGh:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 115
    iget-object v0, p0, Lgcu;->cGh:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 118
    :goto_0
    return v0

    .line 115
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 117
    :cond_1
    iget-object v0, p0, Lgcu;->cGh:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 118
    const/4 v0, 0x2

    goto :goto_0
.end method


# virtual methods
.method public final a(Lgco;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lgcu;->cGj:Lgco;

    .line 125
    return-void
.end method

.method public final a(Lggg;)V
    .locals 0

    .prologue
    .line 138
    return-void
.end method

.method public final aEK()V
    .locals 2

    .prologue
    .line 80
    iget-boolean v0, p0, Lgcu;->bqM:Z

    if-nez v0, :cond_0

    .line 81
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgcu;->bqM:Z

    .line 82
    iget-boolean v0, p0, Lgcu;->cFv:Z

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lgcu;->mRecognitionDispatcher:Lgil;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lgil;->km(I)V

    .line 90
    :cond_0
    return-void
.end method

.method public final invalidate()V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method public final jX(I)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 40
    iget-object v0, p0, Lgcu;->cGh:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    const/4 v0, 0x2

    .line 48
    :goto_0
    return v0

    .line 43
    :cond_0
    iget-object v0, p0, Lgcu;->cGh:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_1

    move v0, v1

    .line 45
    goto :goto_0

    .line 48
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final jY(I)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 54
    iget-object v0, p0, Lgcu;->cGi:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    const/4 v0, 0x2

    .line 62
    :goto_0
    return v0

    .line 57
    :cond_0
    iget-object v0, p0, Lgcu;->cGi:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_1

    move v0, v1

    .line 59
    goto :goto_0

    .line 62
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final jZ(I)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    iget-boolean v0, p0, Lgcu;->bqM:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgcu;->cGi:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgcu;->cGi:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_1

    :cond_0
    move v0, v1

    .line 74
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final ka(I)I
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lgcu;->kf(I)I

    move-result v0

    return v0
.end method

.method public final kb(I)I
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lgcu;->cGi:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 102
    invoke-direct {p0, p1}, Lgcu;->kf(I)I

    move-result v0

    return v0
.end method

.method public final ke(I)V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lgcu;->cGi:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgcu;->cGi:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 107
    iget-object v0, p0, Lgcu;->cGj:Lgco;

    invoke-interface {v0}, Lgco;->aES()V

    .line 109
    :cond_0
    iget-object v0, p0, Lgcu;->cGi:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 110
    return-void
.end method

.method public final onEndOfSpeech()V
    .locals 0

    .prologue
    .line 130
    return-void
.end method
