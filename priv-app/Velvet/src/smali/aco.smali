.class public final Laco;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lacf;


# instance fields
.field private QA:I

.field private Qz:I

.field private Rt:I

.field private Ru:I

.field private Rv:Lacq;

.field private zG:I

.field private zH:I

.field private zM:I

.field private zN:I

.field private zO:I

.field private zP:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Laco;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 51
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Laco;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0, p1, p2, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    invoke-virtual {p0, v0}, Laco;->setAlwaysDrawnWithCacheEnabled(Z)V

    .line 60
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 61
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    .line 62
    iget v1, v0, Ltu;->DL:I

    iput v1, p0, Laco;->zG:I

    iput v1, p0, Laco;->Rt:I

    .line 63
    iget v1, v0, Ltu;->DM:I

    iput v1, p0, Laco;->zH:I

    iput v1, p0, Laco;->Ru:I

    .line 64
    iget v1, v0, Ltu;->Dh:F

    float-to-int v1, v1

    iput v1, p0, Laco;->Qz:I

    .line 65
    iget v0, v0, Ltu;->Dg:F

    float-to-int v0, v0

    iput v0, p0, Laco;->QA:I

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Laco;->zP:I

    iput v0, p0, Laco;->zO:I

    iput v0, p0, Laco;->zN:I

    iput v0, p0, Laco;->zM:I

    .line 68
    new-instance v0, Lacq;

    invoke-direct {v0, p1}, Lacq;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Laco;->Rv:Lacq;

    .line 69
    iget-object v0, p0, Laco;->Rv:Lacq;

    iget v1, p0, Laco;->zG:I

    iget v2, p0, Laco;->zH:I

    iput v1, v0, Lacq;->zG:I

    iput v2, v0, Lacq;->zH:I

    invoke-virtual {v0}, Lacq;->requestLayout()V

    .line 70
    iget-object v0, p0, Laco;->Rv:Lacq;

    iget v1, p0, Laco;->zO:I

    iget v2, p0, Laco;->zP:I

    invoke-virtual {v0, v1, v2}, Lacq;->Q(II)V

    .line 72
    iget-object v0, p0, Laco;->Rv:Lacq;

    invoke-virtual {p0, v0}, Laco;->addView(Landroid/view/View;)V

    .line 73
    return-void
.end method


# virtual methods
.method public final bE(I)I
    .locals 1

    .prologue
    .line 356
    iget v0, p0, Laco;->zG:I

    mul-int/2addr v0, p1

    return v0
.end method

.method public final bF(I)I
    .locals 1

    .prologue
    .line 364
    iget v0, p0, Laco;->zH:I

    mul-int/2addr v0, p1

    return v0
.end method

.method public final cancelLongPress()V
    .locals 3

    .prologue
    .line 85
    invoke-super {p0}, Landroid/view/ViewGroup;->cancelLongPress()V

    .line 88
    invoke-virtual {p0}, Laco;->getChildCount()I

    move-result v1

    .line 89
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 90
    invoke-virtual {p0, v0}, Laco;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 91
    invoke-virtual {v2}, Landroid/view/View;->cancelLongPress()V

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    :cond_0
    return-void
.end method

.method protected final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 374
    instance-of v0, p1, Lacp;

    return v0
.end method

.method public final eb()V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Laco;->Rv:Lacq;

    invoke-virtual {v0}, Lacq;->removeAllViews()V

    .line 119
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Laco;->setLayerType(ILandroid/graphics/Paint;)V

    .line 120
    return-void
.end method

.method public final ec()I
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Laco;->Rv:Lacq;

    invoke-virtual {v0}, Lacq;->getChildCount()I

    move-result v0

    return v0
.end method

.method public final generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 369
    new-instance v0, Lacp;

    invoke-virtual {p0}, Laco;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lacp;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected final generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 379
    new-instance v0, Lacp;

    invoke-direct {v0, p1}, Lacp;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected final onLayout(ZIIII)V
    .locals 8

    .prologue
    .line 238
    invoke-virtual {p0}, Laco;->getChildCount()I

    move-result v1

    .line 239
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 240
    invoke-virtual {p0, v0}, Laco;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 241
    invoke-virtual {p0}, Laco;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Laco;->getPaddingTop()I

    move-result v4

    sub-int v5, p4, p2

    invoke-virtual {p0}, Laco;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    sub-int v6, p5, p3

    invoke-virtual {p0}, Laco;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 239
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 244
    :cond_0
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 11

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 165
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 166
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 168
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 169
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 171
    if-eqz v4, :cond_0

    if-nez v0, :cond_1

    .line 172
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "CellLayout cannot have UNSPECIFIED dimensions"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :cond_1
    iget v0, p0, Laco;->Qz:I

    add-int/lit8 v0, v0, -0x1

    .line 176
    iget v5, p0, Laco;->QA:I

    add-int/lit8 v5, v5, -0x1

    .line 178
    iget v6, p0, Laco;->zM:I

    if-ltz v6, :cond_2

    iget v6, p0, Laco;->zN:I

    if-gez v6, :cond_5

    .line 179
    :cond_2
    invoke-virtual {p0}, Laco;->getPaddingLeft()I

    move-result v6

    sub-int v6, v3, v6

    invoke-virtual {p0}, Laco;->getPaddingRight()I

    move-result v7

    sub-int/2addr v6, v7

    .line 180
    invoke-virtual {p0}, Laco;->getPaddingTop()I

    move-result v7

    sub-int v7, v2, v7

    invoke-virtual {p0}, Laco;->getPaddingBottom()I

    move-result v8

    sub-int/2addr v7, v8

    .line 181
    iget v8, p0, Laco;->Qz:I

    iget v9, p0, Laco;->Rt:I

    mul-int/2addr v8, v9

    sub-int/2addr v6, v8

    .line 182
    iget v8, p0, Laco;->QA:I

    iget v9, p0, Laco;->Ru:I

    mul-int/2addr v8, v9

    sub-int/2addr v7, v8

    .line 183
    if-lez v0, :cond_3

    div-int v0, v6, v0

    :goto_0
    iput v0, p0, Laco;->zO:I

    .line 184
    if-lez v5, :cond_4

    div-int v0, v7, v5

    :goto_1
    iput v0, p0, Laco;->zP:I

    .line 186
    iget-object v0, p0, Laco;->Rv:Lacq;

    iget v5, p0, Laco;->zO:I

    iget v6, p0, Laco;->zP:I

    invoke-virtual {v0, v5, v6}, Lacq;->Q(II)V

    .line 195
    :goto_2
    const/high16 v0, -0x80000000

    if-ne v4, v0, :cond_7

    .line 196
    invoke-virtual {p0}, Laco;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Laco;->getPaddingRight()I

    move-result v2

    add-int/2addr v0, v2

    iget v2, p0, Laco;->Qz:I

    iget v3, p0, Laco;->zG:I

    mul-int/2addr v2, v3

    add-int/2addr v0, v2

    iget v2, p0, Laco;->Qz:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Laco;->zO:I

    mul-int/2addr v2, v3

    add-int/2addr v2, v0

    .line 198
    invoke-virtual {p0}, Laco;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Laco;->getPaddingBottom()I

    move-result v3

    add-int/2addr v0, v3

    iget v3, p0, Laco;->QA:I

    iget v4, p0, Laco;->zH:I

    mul-int/2addr v3, v4

    add-int/2addr v0, v3

    iget v3, p0, Laco;->QA:I

    add-int/lit8 v3, v3, -0x1

    iget v4, p0, Laco;->zP:I

    mul-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 200
    invoke-virtual {p0, v2, v0}, Laco;->setMeasuredDimension(II)V

    .line 203
    :goto_3
    invoke-virtual {p0}, Laco;->getChildCount()I

    move-result v3

    .line 204
    :goto_4
    if-ge v1, v3, :cond_6

    .line 205
    invoke-virtual {p0, v1}, Laco;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 206
    invoke-virtual {p0}, Laco;->getPaddingLeft()I

    move-result v5

    sub-int v5, v2, v5

    invoke-virtual {p0}, Laco;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-static {v5, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 209
    invoke-virtual {p0}, Laco;->getPaddingTop()I

    move-result v6

    sub-int v6, v0, v6

    invoke-virtual {p0}, Laco;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-static {v6, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 212
    invoke-virtual {v4, v5, v6}, Landroid/view/View;->measure(II)V

    .line 204
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    move v0, v1

    .line 183
    goto :goto_0

    :cond_4
    move v0, v1

    .line 184
    goto :goto_1

    .line 188
    :cond_5
    iget v0, p0, Laco;->zM:I

    iput v0, p0, Laco;->zO:I

    .line 189
    iget v0, p0, Laco;->zN:I

    iput v0, p0, Laco;->zP:I

    goto :goto_2

    .line 215
    :cond_6
    invoke-virtual {p0, v2, v0}, Laco;->setMeasuredDimension(II)V

    .line 216
    return-void

    :cond_7
    move v0, v2

    move v2, v3

    goto :goto_3
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 248
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 249
    iget-object v1, p0, Laco;->Rv:Lacq;

    invoke-virtual {v1}, Lacq;->getChildCount()I

    move-result v1

    .line 250
    if-lez v1, :cond_2

    .line 252
    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Laco;->Rv:Lacq;

    invoke-virtual {v2, v1}, Lacq;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 253
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    .line 254
    iget-object v2, p0, Laco;->Rv:Lacq;

    invoke-virtual {v2}, Lacq;->getChildCount()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Laco;->Qz:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 255
    iget v3, p0, Laco;->QA:I

    if-ge v2, v3, :cond_0

    .line 257
    iget v2, p0, Laco;->zH:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 259
    :cond_0
    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    .line 261
    :cond_2
    :goto_0
    return v0

    .line 259
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final setChildrenDrawingCacheEnabled(Z)V
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Laco;->Rv:Lacq;

    invoke-virtual {v0, p1}, Lacq;->setChildrenDrawingCacheEnabled(Z)V

    .line 271
    return-void
.end method
