.class final Lbny;
.super Lbit;

# interfaces
.implements Lbso;


# instance fields
.field private final aAw:Lcom/google/android/gms/common/api/Status;

.field private final aGw:Lbun;


# direct methods
.method public constructor <init>(Lbnv;Lbhh;Lcom/google/android/gms/common/api/Status;Lbun;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbit;-><init>(Lbir;Ljava/lang/Object;)V

    iput-object p3, p0, Lbny;->aAw:Lcom/google/android/gms/common/api/Status;

    iput-object p4, p0, Lbny;->aGw:Lbun;

    return-void
.end method


# virtual methods
.method protected final synthetic an(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lbhh;

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Lbhh;->al(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final release()V
    .locals 1

    iget-object v0, p0, Lbny;->aGw:Lbun;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbny;->aGw:Lbun;

    invoke-virtual {v0}, Lbun;->close()V

    :cond_0
    return-void
.end method

.method public final wZ()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lbny;->aAw:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method protected final yU()V
    .locals 0

    invoke-virtual {p0}, Lbny;->release()V

    return-void
.end method

.method public final zY()Lbun;
    .locals 1

    iget-object v0, p0, Lbny;->aGw:Lbun;

    return-object v0
.end method
