.class public final Lars;
.super Larc;
.source "PG"


# instance fields
.field private anx:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Larc;-><init>()V

    .line 95
    return-void
.end method

.method static synthetic a(Lars;)I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lars;->anx:I

    return v0
.end method

.method public static bt(Z)Lars;
    .locals 3

    .prologue
    .line 38
    new-instance v0, Lars;

    invoke-direct {v0}, Lars;-><init>()V

    .line 40
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 41
    const-string v2, "always_on_hotword"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 42
    invoke-virtual {v0, v1}, Lars;->setArguments(Landroid/os/Bundle;)V

    .line 44
    return-object v0
.end method


# virtual methods
.method public final onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 90
    invoke-super {p0, p1}, Larc;->onAttach(Landroid/app/Activity;)V

    .line 91
    const/16 v0, 0x12b

    invoke-virtual {p0, v0}, Lars;->db(I)V

    .line 93
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 50
    const v0, 0x7f04006c

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 52
    invoke-virtual {p0}, Lars;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lars;->anx:I

    .line 54
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->am:Landroid/os/Bundle;

    const-string v1, "always_on_hotword"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 56
    const v0, 0x7f110191

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 57
    const v3, 0x7f0a0709

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {}, Lars;->tR()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p0, v3, v4}, Lars;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    const v0, 0x7f110192

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 60
    if-eqz v1, :cond_0

    const v1, 0x7f0a070b

    :goto_0
    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {}, Lars;->tR()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0, v1, v3}, Lars;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    const v0, 0x7f0a070d

    invoke-virtual {p0, v0}, Lars;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 66
    const v0, 0x7f0a070e

    invoke-virtual {p0, v0}, Lars;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 67
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 68
    new-instance v4, Laru;

    invoke-direct {v4, p0, v6}, Laru;-><init>(Lars;B)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v5, 0x21

    invoke-interface {v3, v4, v6, v0, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 71
    const v0, 0x7f110193

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 72
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/CharSequence;

    aput-object v1, v4, v6

    const-string v1, " "

    aput-object v1, v4, v7

    const/4 v1, 0x2

    aput-object v3, v4, v1

    invoke-static {v4}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 74
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setClickable(Z)V

    .line 76
    const v0, 0x7f110194

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lart;

    invoke-direct {v1, p0}, Lart;-><init>(Lars;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    return-object v2

    .line 60
    :cond_0
    const v1, 0x7f0a070a

    goto :goto_0
.end method
