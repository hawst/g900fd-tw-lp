.class public final Ljfy;
.super Ljsl;
.source "PG"


# instance fields
.field private ejh:[Ljfz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15810
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 15811
    invoke-static {}, Ljfz;->bjN()[Ljfz;

    move-result-object v0

    iput-object v0, p0, Ljfy;->ejh:[Ljfz;

    const/4 v0, 0x0

    iput-object v0, p0, Ljfy;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljfy;->eCz:I

    .line 15812
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 15650
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljfy;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljfy;->ejh:[Ljfz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljfz;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljfy;->ejh:[Ljfz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljfz;

    invoke-direct {v3}, Ljfz;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljfy;->ejh:[Ljfz;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljfz;

    invoke-direct {v3}, Ljfz;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljfy;->ejh:[Ljfz;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 15824
    iget-object v0, p0, Ljfy;->ejh:[Ljfz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljfy;->ejh:[Ljfz;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 15825
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljfy;->ejh:[Ljfz;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 15826
    iget-object v1, p0, Ljfy;->ejh:[Ljfz;

    aget-object v1, v1, v0

    .line 15827
    if-eqz v1, :cond_0

    .line 15828
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 15825
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15832
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 15833
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 15837
    invoke-super {p0}, Ljsl;->lF()I

    move-result v1

    .line 15838
    iget-object v0, p0, Ljfy;->ejh:[Ljfz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljfy;->ejh:[Ljfz;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 15839
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ljfy;->ejh:[Ljfz;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 15840
    iget-object v2, p0, Ljfy;->ejh:[Ljfz;

    aget-object v2, v2, v0

    .line 15841
    if-eqz v2, :cond_0

    .line 15842
    const/4 v3, 0x1

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 15839
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15847
    :cond_1
    return v1
.end method
