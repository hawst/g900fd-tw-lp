.class public final Ljtn;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dHy:I

.field public eDA:[Ljtm;

.field private eDB:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 225
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 226
    iput v1, p0, Ljtn;->aez:I

    invoke-static {}, Ljtm;->bux()[Ljtm;

    move-result-object v0

    iput-object v0, p0, Ljtn;->eDA:[Ljtm;

    iput v1, p0, Ljtn;->eDB:I

    iput v1, p0, Ljtn;->dHy:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljtn;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljtn;->eCz:I

    .line 227
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 181
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljtn;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljtn;->eDA:[Ljtm;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljtm;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljtn;->eDA:[Ljtm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljtm;

    invoke-direct {v3}, Ljtm;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljtn;->eDA:[Ljtm;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljtm;

    invoke-direct {v3}, Ljtm;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljtn;->eDA:[Ljtm;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljtn;->eDB:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljtn;->dHy:I

    iget v0, p0, Ljtn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljtn;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 242
    iget-object v0, p0, Ljtn;->eDA:[Ljtm;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljtn;->eDA:[Ljtm;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 243
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljtn;->eDA:[Ljtm;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 244
    iget-object v1, p0, Ljtn;->eDA:[Ljtm;

    aget-object v1, v1, v0

    .line 245
    if-eqz v1, :cond_0

    .line 246
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 243
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 250
    :cond_1
    const/4 v0, 0x2

    iget v1, p0, Ljtn;->eDB:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 251
    iget v0, p0, Ljtn;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 252
    const/4 v0, 0x3

    iget v1, p0, Ljtn;->dHy:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 254
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 255
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 259
    invoke-super {p0}, Ljsl;->lF()I

    move-result v1

    .line 260
    iget-object v0, p0, Ljtn;->eDA:[Ljtm;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljtn;->eDA:[Ljtm;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 261
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ljtn;->eDA:[Ljtm;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 262
    iget-object v2, p0, Ljtn;->eDA:[Ljtm;

    aget-object v2, v2, v0

    .line 263
    if-eqz v2, :cond_0

    .line 264
    const/4 v3, 0x1

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 261
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 269
    :cond_1
    const/4 v0, 0x2

    iget v2, p0, Ljtn;->eDB:I

    invoke-static {v0, v2}, Ljsj;->bv(II)I

    move-result v0

    add-int/2addr v0, v1

    .line 271
    iget v1, p0, Ljtn;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    .line 272
    const/4 v1, 0x3

    iget v2, p0, Ljtn;->dHy:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 275
    :cond_2
    return v0
.end method
