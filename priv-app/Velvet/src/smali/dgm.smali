.class public Ldgm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# static fields
.field static final bwY:J


# instance fields
.field public final auh:Lbhi;

.field final bwZ:Ldgy;

.field private final bxa:Lemm;

.field private final bxb:Ljava/lang/Runnable;

.field private final bxc:Ljava/lang/Runnable;

.field private final bxd:Lbvh;

.field final bxe:Ljava/util/concurrent/atomic/AtomicInteger;

.field final bxf:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 49
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ldgm;->bwY:J

    return-void
.end method

.method constructor <init>(Lbhi;Lbbt;ZZZ)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 76
    sget-object v0, Lbut;->aLn:Lbvh;

    new-instance v1, Ldgy;

    new-instance v2, Ldgz;

    new-instance v3, Ldha;

    const/4 v4, 0x1

    invoke-direct {v3, p2, v4}, Ldha;-><init>(Lbbt;Z)V

    invoke-direct {v2, v3, p5}, Ldgz;-><init>(Ldha;Z)V

    invoke-direct {v1, v2, v5}, Ldgy;-><init>(Ldgz;Z)V

    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "IcingConnectionThread"

    invoke-direct {v2, v3, v5}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    new-instance v3, Leod;

    new-instance v4, Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v3, v4}, Leod;-><init>(Landroid/os/Handler;)V

    invoke-direct {p0, v0, p1, v1, v3}, Ldgm;-><init>(Lbvh;Lbhi;Ldgy;Lemm;)V

    .line 83
    return-void
.end method

.method public constructor <init>(Lbvh;Lbhi;Ldgy;Lemm;)V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object p1, p0, Ldgm;->bxd:Lbvh;

    .line 90
    iput-object p2, p0, Ldgm;->auh:Lbhi;

    .line 92
    iput-object p4, p0, Ldgm;->bxa:Lemm;

    .line 93
    iput-object p3, p0, Ldgm;->bwZ:Ldgy;

    .line 95
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Ldgm;->bxe:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 96
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Ldgm;->bxf:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 97
    new-instance v0, Ldgn;

    invoke-direct {v0, p0}, Ldgn;-><init>(Ldgm;)V

    iput-object v0, p0, Ldgm;->bxb:Ljava/lang/Runnable;

    .line 104
    new-instance v0, Ldgq;

    invoke-direct {v0, p0}, Ldgq;-><init>(Ldgm;)V

    iput-object v0, p0, Ldgm;->bxc:Ljava/lang/Runnable;

    .line 112
    return-void
.end method

.method static synthetic a(Ldgm;)Lbvh;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ldgm;->bxd:Lbvh;

    return-object v0
.end method

.method private abW()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Ldgm;->bxe:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 145
    iget-object v0, p0, Ldgm;->bxa:Lemm;

    iget-object v1, p0, Ldgm;->bxb:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 146
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 8

    .prologue
    .line 191
    new-instance v6, Landroid/os/ConditionVariable;

    invoke-direct {v6}, Landroid/os/ConditionVariable;-><init>()V

    .line 192
    const/4 v0, 0x1

    new-array v2, v0, [Lcom/google/android/gms/appdatasearch/SearchResults;

    .line 194
    invoke-direct {p0}, Ldgm;->abW()V

    .line 195
    iget-object v7, p0, Ldgm;->bxa:Lemm;

    new-instance v0, Ldgt;

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Ldgt;-><init>(Ldgm;[Lcom/google/android/gms/appdatasearch/SearchResults;Ljava/lang/String;ILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;Landroid/os/ConditionVariable;)V

    invoke-interface {v7, v0}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 202
    iget-object v0, p0, Ldgm;->bxa:Lemm;

    iget-object v1, p0, Ldgm;->bxc:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 203
    invoke-virtual {v6}, Landroid/os/ConditionVariable;->block()V

    .line 204
    const/4 v0, 0x0

    aget-object v0, v2, v0

    return-object v0
.end method

.method public final a(Letj;)V
    .locals 4

    .prologue
    .line 404
    const-string v0, "IcingConnection state"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 405
    const-string v0, "mPendingConnection"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Ldgm;->bxe:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 406
    const-string v0, "mWaitingForQueries"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Ldgm;->bxf:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 409
    const-string v0, "isConnected"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Ldgm;->bwZ:Ldgy;

    invoke-virtual {v1}, Ldgy;->isConnected()Z

    move-result v1

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 410
    return-void
.end method

.method public final a(Ljava/lang/String;ILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;ZLemy;)V
    .locals 9

    .prologue
    .line 156
    iget-object v0, p0, Ldgm;->bxf:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    .line 158
    if-eqz v2, :cond_0

    .line 162
    invoke-direct {p0}, Ldgm;->abW()V

    .line 164
    :cond_0
    iget-object v8, p0, Ldgm;->bxa:Lemm;

    new-instance v0, Ldgs;

    move-object v1, p0

    move v3, p4

    move-object v4, p1

    move v5, p2

    move-object v6, p3

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Ldgs;-><init>(Ldgm;ZZLjava/lang/String;ILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;Lemy;)V

    invoke-interface {v8, v0}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 181
    if-eqz v2, :cond_1

    .line 182
    iget-object v0, p0, Ldgm;->bxa:Lemm;

    iget-object v1, p0, Ldgm;->bxc:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 184
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;ZLjava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 308
    invoke-direct {p0}, Ldgm;->abW()V

    .line 309
    iget-object v0, p0, Ldgm;->bxa:Lemm;

    new-instance v1, Ldgo;

    invoke-direct {v1, p0, p1, p2, p3}, Ldgo;-><init>(Ldgm;Ljava/lang/String;ZLjava/lang/Runnable;)V

    invoke-interface {v0, v1}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 318
    iget-object v0, p0, Ldgm;->bxa:Lemm;

    iget-object v1, p0, Ldgm;->bxc:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 319
    return-void
.end method

.method public final abU()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 127
    iget-object v0, p0, Ldgm;->bxf:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "already waiting for queries"

    invoke-static {v0, v2}, Lifv;->d(ZLjava/lang/Object;)V

    .line 129
    iget-object v0, p0, Ldgm;->bxf:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 130
    invoke-direct {p0}, Ldgm;->abW()V

    .line 131
    return-void

    .line 127
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final abV()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Ldgm;->bxf:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    const-string v1, "wasn\'t waiting for queries"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 139
    iget-object v0, p0, Ldgm;->bxf:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 140
    iget-object v0, p0, Ldgm;->bxa:Lemm;

    iget-object v1, p0, Ldgm;->bxc:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 141
    return-void
.end method

.method public final b([Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/DocumentResults;
    .locals 8

    .prologue
    .line 213
    invoke-static {}, Lenu;->auQ()V

    .line 215
    new-instance v6, Landroid/os/ConditionVariable;

    invoke-direct {v6}, Landroid/os/ConditionVariable;-><init>()V

    .line 216
    const/4 v0, 0x1

    new-array v2, v0, [Lcom/google/android/gms/appdatasearch/DocumentResults;

    .line 218
    invoke-direct {p0}, Ldgm;->abW()V

    .line 219
    iget-object v7, p0, Ldgm;->bxa:Lemm;

    new-instance v0, Ldgu;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Ldgu;-><init>(Ldgm;[Lcom/google/android/gms/appdatasearch/DocumentResults;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/appdatasearch/QuerySpecification;Landroid/os/ConditionVariable;)V

    invoke-interface {v7, v0}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 226
    iget-object v0, p0, Ldgm;->bxa:Lemm;

    iget-object v1, p0, Ldgm;->bxc:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 227
    invoke-virtual {v6}, Landroid/os/ConditionVariable;->block()V

    .line 228
    const/4 v0, 0x0

    aget-object v0, v2, v0

    return-object v0
.end method

.method public final b([Ljava/lang/String;Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;)Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;
    .locals 7

    .prologue
    .line 259
    invoke-static {}, Lenu;->auQ()V

    .line 261
    new-instance v5, Landroid/os/ConditionVariable;

    invoke-direct {v5}, Landroid/os/ConditionVariable;-><init>()V

    .line 262
    const/4 v0, 0x1

    new-array v2, v0, [Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;

    .line 263
    invoke-direct {p0}, Ldgm;->abW()V

    .line 264
    iget-object v6, p0, Ldgm;->bxa:Lemm;

    new-instance v0, Ldgw;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Ldgw;-><init>(Ldgm;[Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;[Ljava/lang/String;Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;Landroid/os/ConditionVariable;)V

    invoke-interface {v6, v0}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 271
    iget-object v0, p0, Ldgm;->bxa:Lemm;

    iget-object v1, p0, Ldgm;->bxc:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 272
    invoke-virtual {v5}, Landroid/os/ConditionVariable;->block()V

    .line 273
    const/4 v0, 0x0

    aget-object v0, v2, v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 10

    .prologue
    .line 237
    new-instance v8, Landroid/os/ConditionVariable;

    invoke-direct {v8}, Landroid/os/ConditionVariable;-><init>()V

    .line 238
    const/4 v0, 0x1

    new-array v2, v0, [Lcom/google/android/gms/appdatasearch/SearchResults;

    .line 240
    invoke-direct {p0}, Ldgm;->abW()V

    .line 241
    iget-object v9, p0, Ldgm;->bxa:Lemm;

    new-instance v0, Ldgv;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v8}, Ldgv;-><init>(Ldgm;[Lcom/google/android/gms/appdatasearch/SearchResults;Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;Landroid/os/ConditionVariable;)V

    invoke-interface {v9, v0}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 248
    iget-object v0, p0, Ldgm;->bxa:Lemm;

    iget-object v1, p0, Ldgm;->bxc:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 249
    invoke-virtual {v8}, Landroid/os/ConditionVariable;->block()V

    .line 250
    const/4 v0, 0x0

    aget-object v0, v2, v0

    return-object v0
.end method

.method public final b(Ljjf;Z)V
    .locals 3

    .prologue
    .line 329
    invoke-static {}, Lenu;->auQ()V

    .line 331
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    .line 332
    invoke-direct {p0}, Ldgm;->abW()V

    .line 333
    iget-object v1, p0, Ldgm;->bxa:Lemm;

    new-instance v2, Ldgp;

    invoke-direct {v2, p0, p1, p2, v0}, Ldgp;-><init>(Ldgm;Ljjf;ZLandroid/os/ConditionVariable;)V

    invoke-interface {v1, v2}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 341
    iget-object v1, p0, Ldgm;->bxa:Lemm;

    iget-object v2, p0, Ldgm;->bxc:Ljava/lang/Runnable;

    invoke-interface {v1, v2}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 342
    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 343
    return-void
.end method

.method public final d(Lemy;)V
    .locals 2

    .prologue
    .line 284
    iget-object v0, p0, Ldgm;->bxa:Lemm;

    new-instance v1, Ldgx;

    invoke-direct {v1, p0, p1}, Ldgx;-><init>(Ldgm;Lemy;)V

    invoke-interface {v0, v1}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 304
    return-void
.end method

.method public final du(Z)V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Ldgm;->bxa:Lemm;

    new-instance v1, Ldgr;

    invoke-direct {v1, p0, p1}, Ldgr;-><init>(Ldgm;Z)V

    invoke-interface {v0, v1}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 121
    return-void
.end method
