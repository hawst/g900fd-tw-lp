.class public final Lacz;
.super Lyx;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static RU:Landroid/content/res/Resources$Theme;


# instance fields
.field private final RV:Landroid/graphics/Rect;

.field private RW:Landroid/view/View;

.field private RX:Landroid/view/View$OnClickListener;

.field private final RY:Lyy;

.field private final RZ:I

.field private final Sa:Landroid/content/Intent;

.field private Sb:Landroid/graphics/Bitmap;

.field private Sc:Lcom/android/launcher3/PreloadIconDrawable;

.field private Sd:Landroid/graphics/drawable/Drawable;

.field private Se:Landroid/graphics/drawable/Drawable;

.field private Sf:Z

.field private final Sg:Landroid/text/TextPaint;

.field private Sh:Landroid/text/Layout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lyy;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 57
    invoke-direct {p0, p1}, Lyx;-><init>(Landroid/content/Context;)V

    .line 38
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lacz;->RV:Landroid/graphics/Rect;

    .line 58
    iput-object p2, p0, Lacz;->RY:Lyy;

    .line 59
    iget v0, p2, Lyy;->Mq:I

    iput v0, p0, Lacz;->RZ:I

    .line 60
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p2, Lyy;->Mp:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lacz;->Sa:Landroid/content/Intent;

    .line 62
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lacz;->Sg:Landroid/text/TextPaint;

    .line 63
    iget-object v0, p0, Lacz;->Sg:Landroid/text/TextPaint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 64
    iget-object v0, p0, Lacz;->Sg:Landroid/text/TextPaint;

    invoke-static {}, Lacz;->gl()Ltu;

    move-result-object v1

    iget v1, v1, Ltu;->DJ:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lacz;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v3, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 66
    const v0, 0x7f02029d

    invoke-virtual {p0, v0}, Lacz;->setBackgroundResource(I)V

    .line 67
    invoke-virtual {p0, v3}, Lacz;->setWillNotDraw(Z)V

    .line 68
    return-void
.end method

.method private static gl()Ltu;
    .locals 1

    .prologue
    .line 239
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lwi;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 104
    iget-object v0, p0, Lacz;->Sa:Landroid/content/Intent;

    iget-object v1, p0, Lacz;->RY:Lyy;

    iget-object v1, v1, Lyy;->Jl:Lahz;

    invoke-virtual {p1, v0, v1}, Lwi;->a(Landroid/content/Intent;Lahz;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lacz;->Sb:Landroid/graphics/Bitmap;

    if-ne v1, v0, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    iput-object v0, p0, Lacz;->Sb:Landroid/graphics/Bitmap;

    .line 109
    iget-object v0, p0, Lacz;->Sc:Lcom/android/launcher3/PreloadIconDrawable;

    if-eqz v0, :cond_2

    .line 110
    iget-object v0, p0, Lacz;->Sc:Lcom/android/launcher3/PreloadIconDrawable;

    invoke-virtual {v0, v2}, Lcom/android/launcher3/PreloadIconDrawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 111
    iput-object v2, p0, Lacz;->Sc:Lcom/android/launcher3/PreloadIconDrawable;

    .line 113
    :cond_2
    iget-object v0, p0, Lacz;->Sb:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 116
    invoke-virtual {p0}, Lacz;->jW()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 117
    invoke-virtual {p0}, Lacz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020217

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lacz;->Sd:Landroid/graphics/drawable/Drawable;

    .line 118
    new-instance v0, Lus;

    iget-object v1, p0, Lacz;->Sb:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Lus;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lacz;->Se:Landroid/graphics/drawable/Drawable;

    .line 130
    :goto_1
    iput-boolean v3, p0, Lacz;->Sf:Z

    goto :goto_0

    .line 120
    :cond_3
    sget-object v0, Lacz;->RU:Landroid/content/res/Resources$Theme;

    if-nez v0, :cond_4

    .line 121
    invoke-virtual {p0}, Lacz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 122
    sput-object v0, Lacz;->RU:Landroid/content/res/Resources$Theme;

    const v1, 0x7f090041

    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 125
    :cond_4
    iget-object v0, p0, Lacz;->Sb:Landroid/graphics/Bitmap;

    invoke-static {v0}, Ladp;->i(Landroid/graphics/Bitmap;)Lus;

    move-result-object v0

    .line 126
    new-instance v1, Lcom/android/launcher3/PreloadIconDrawable;

    sget-object v2, Lacz;->RU:Landroid/content/res/Resources$Theme;

    invoke-direct {v1, v0, v2}, Lcom/android/launcher3/PreloadIconDrawable;-><init>(Landroid/graphics/drawable/Drawable;Landroid/content/res/Resources$Theme;)V

    iput-object v1, p0, Lacz;->Sc:Lcom/android/launcher3/PreloadIconDrawable;

    .line 127
    iget-object v0, p0, Lacz;->Sc:Lcom/android/launcher3/PreloadIconDrawable;

    invoke-virtual {v0, p0}, Lcom/android/launcher3/PreloadIconDrawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 128
    invoke-virtual {p0}, Lacz;->jV()V

    goto :goto_1
.end method

.method protected final getDefaultView()Landroid/view/View;
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lacz;->RW:Landroid/view/View;

    if-nez v0, :cond_0

    .line 79
    iget-object v0, p0, Lacz;->GS:Landroid/view/LayoutInflater;

    const v1, 0x7f040022

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lacz;->RW:Landroid/view/View;

    .line 80
    iget-object v0, p0, Lacz;->RW:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    invoke-virtual {p0}, Lacz;->jV()V

    .line 83
    :cond_0
    iget-object v0, p0, Lacz;->RW:Landroid/view/View;

    return-object v0
.end method

.method public final in()Z
    .locals 2

    .prologue
    .line 94
    iget v0, p0, Lacz;->RZ:I

    iget-object v1, p0, Lacz;->RY:Lyy;

    iget v1, v1, Lyy;->Mq:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final jV()V
    .locals 3

    .prologue
    .line 140
    iget-object v0, p0, Lacz;->Sc:Lcom/android/launcher3/PreloadIconDrawable;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lacz;->Sc:Lcom/android/launcher3/PreloadIconDrawable;

    iget-object v1, p0, Lacz;->RY:Lyy;

    iget v1, v1, Lyy;->Mr:I

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/PreloadIconDrawable;->setLevel(I)Z

    .line 143
    :cond_0
    return-void
.end method

.method public final jW()Z
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lacz;->RY:Lyy;

    iget v0, v0, Lyy;->Mq:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    iget-object v0, p0, Lacz;->RY:Lyy;

    iget v0, v0, Lyy;->Mq:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lacz;->RX:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lacz;->RX:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 152
    :cond_0
    return-void
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 16

    .prologue
    .line 161
    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->Sc:Lcom/android/launcher3/PreloadIconDrawable;

    if-eqz v1, :cond_2

    .line 162
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lacz;->Sf:Z

    if-eqz v1, :cond_0

    .line 163
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v1

    iget-object v1, v1, Lyu;->Mk:Lur;

    invoke-virtual {v1}, Lur;->gl()Ltu;

    move-result-object v1

    iget v1, v1, Ltu;->DI:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lacz;->Sc:Lcom/android/launcher3/PreloadIconDrawable;

    invoke-virtual {v2}, Lcom/android/launcher3/PreloadIconDrawable;->jX()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 165
    invoke-virtual/range {p0 .. p0}, Lacz;->getWidth()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lacz;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual/range {p0 .. p0}, Lacz;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual/range {p0 .. p0}, Lacz;->getHeight()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lacz;->getPaddingTop()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual/range {p0 .. p0}, Lacz;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 169
    move-object/from16 v0, p0

    iget-object v2, v0, Lacz;->RV:Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 170
    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->RV:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v2, v0, Lacz;->Sc:Lcom/android/launcher3/PreloadIconDrawable;

    invoke-virtual {v2}, Lcom/android/launcher3/PreloadIconDrawable;->jX()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lacz;->Sc:Lcom/android/launcher3/PreloadIconDrawable;

    invoke-virtual {v3}, Lcom/android/launcher3/PreloadIconDrawable;->jX()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->inset(II)V

    .line 171
    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->RV:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Lacz;->getWidth()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lacz;->RV:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    invoke-virtual/range {p0 .. p0}, Lacz;->getHeight()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lacz;->RV:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 172
    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->Sc:Lcom/android/launcher3/PreloadIconDrawable;

    move-object/from16 v0, p0

    iget-object v2, v0, Lacz;->RV:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Lcom/android/launcher3/PreloadIconDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 173
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lacz;->Sf:Z

    .line 176
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->Sc:Lcom/android/launcher3/PreloadIconDrawable;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/android/launcher3/PreloadIconDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 236
    :cond_1
    :goto_0
    return-void

    .line 177
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->Sd:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->Se:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 178
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lacz;->Sf:Z

    if-eqz v1, :cond_3

    .line 179
    invoke-static {}, Lacz;->gl()Ltu;

    move-result-object v9

    .line 180
    iget v10, v9, Ltu;->DI:I

    .line 181
    invoke-virtual/range {p0 .. p0}, Lacz;->getPaddingTop()I

    move-result v11

    .line 182
    invoke-virtual/range {p0 .. p0}, Lacz;->getPaddingBottom()I

    move-result v12

    .line 183
    invoke-virtual/range {p0 .. p0}, Lacz;->getPaddingLeft()I

    move-result v13

    .line 184
    invoke-virtual/range {p0 .. p0}, Lacz;->getPaddingRight()I

    move-result v14

    .line 186
    invoke-virtual/range {p0 .. p0}, Lacz;->getWidth()I

    move-result v1

    sub-int/2addr v1, v13

    sub-int v4, v1, v14

    .line 187
    invoke-virtual/range {p0 .. p0}, Lacz;->getHeight()I

    move-result v1

    sub-int/2addr v1, v11

    sub-int v15, v1, v12

    .line 190
    new-instance v1, Landroid/text/StaticLayout;

    invoke-virtual/range {p0 .. p0}, Lacz;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00bb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lacz;->Sg:Landroid/text/TextPaint;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-direct/range {v1 .. v8}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lacz;->Sh:Landroid/text/Layout;

    .line 193
    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->Sh:Landroid/text/Layout;

    invoke-virtual {v1}, Landroid/text/Layout;->getLineCount()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 195
    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->Sh:Landroid/text/Layout;

    invoke-virtual {v1}, Landroid/text/Layout;->getHeight()I

    move-result v1

    sub-int v1, v15, v1

    invoke-static {v4, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v10, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 197
    move-object/from16 v0, p0

    iget-object v2, v0, Lacz;->RV:Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 198
    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->RV:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Lacz;->getWidth()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lacz;->RV:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    invoke-virtual/range {p0 .. p0}, Lacz;->getHeight()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lacz;->RV:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lacz;->Sh:Landroid/text/Layout;

    invoke-virtual {v4}, Landroid/text/Layout;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    iget v4, v9, Ltu;->DK:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 202
    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->Se:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v2, v0, Lacz;->RV:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 205
    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->RV:Landroid/graphics/Rect;

    iput v13, v1, Landroid/graphics/Rect;->left:I

    .line 206
    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->RV:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v2, v0, Lacz;->RV:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget v3, v9, Ltu;->DK:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 222
    :goto_1
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lacz;->Sf:Z

    .line 225
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->Sh:Landroid/text/Layout;

    if-nez v1, :cond_5

    .line 226
    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->Sd:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 227
    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->Se:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 209
    :cond_4
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lacz;->Sh:Landroid/text/Layout;

    .line 210
    invoke-virtual/range {p0 .. p0}, Lacz;->getWidth()I

    move-result v1

    sub-int/2addr v1, v13

    sub-int/2addr v1, v14

    invoke-virtual/range {p0 .. p0}, Lacz;->getHeight()I

    move-result v2

    sub-int/2addr v2, v11

    sub-int/2addr v2, v12

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v10, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 213
    move-object/from16 v0, p0

    iget-object v2, v0, Lacz;->RV:Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 214
    move-object/from16 v0, p0

    iget-object v2, v0, Lacz;->RV:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Lacz;->getWidth()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lacz;->RV:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    invoke-virtual/range {p0 .. p0}, Lacz;->getHeight()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lacz;->RV:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 215
    move-object/from16 v0, p0

    iget-object v2, v0, Lacz;->Sd:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lacz;->RV:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 217
    div-int/lit8 v1, v1, 0x2

    move-object/from16 v0, p0

    iget-object v2, v0, Lacz;->RV:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v11

    move-object/from16 v0, p0

    iget-object v3, v0, Lacz;->RV:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v13

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 219
    move-object/from16 v0, p0

    iget-object v2, v0, Lacz;->Se:Landroid/graphics/drawable/Drawable;

    add-int v3, v13, v1

    add-int/2addr v1, v11

    invoke-virtual {v2, v13, v11, v3, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto/16 :goto_1

    .line 229
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 230
    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->RV:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lacz;->RV:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 231
    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->Sh:Landroid/text/Layout;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 232
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 233
    move-object/from16 v0, p0

    iget-object v1, v0, Lacz;->Se:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method

.method protected final onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 99
    invoke-super {p0, p1, p2, p3, p4}, Lyx;->onSizeChanged(IIII)V

    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lacz;->Sf:Z

    .line 101
    return-void
.end method

.method public final setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lacz;->RX:Landroid/view/View$OnClickListener;

    .line 89
    return-void
.end method

.method public final updateAppWidgetSize(Landroid/os/Bundle;IIII)V
    .locals 0

    .prologue
    .line 74
    return-void
.end method

.method protected final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lacz;->Sc:Lcom/android/launcher3/PreloadIconDrawable;

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Lyx;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
