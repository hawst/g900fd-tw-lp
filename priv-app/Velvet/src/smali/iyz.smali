.class public final Liyz;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dRs:[Liyz;


# instance fields
.field private aez:I

.field private agv:I

.field private dRt:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30268
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 30269
    iput v1, p0, Liyz;->aez:I

    const/4 v0, 0x4

    iput v0, p0, Liyz;->agv:I

    iput v1, p0, Liyz;->dRt:I

    const/4 v0, 0x0

    iput-object v0, p0, Liyz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Liyz;->eCz:I

    .line 30270
    return-void
.end method

.method public static bcC()[Liyz;
    .locals 2

    .prologue
    .line 30217
    sget-object v0, Liyz;->dRs:[Liyz;

    if-nez v0, :cond_1

    .line 30218
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 30220
    :try_start_0
    sget-object v0, Liyz;->dRs:[Liyz;

    if-nez v0, :cond_0

    .line 30221
    const/4 v0, 0x0

    new-array v0, v0, [Liyz;

    sput-object v0, Liyz;->dRs:[Liyz;

    .line 30223
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30225
    :cond_1
    sget-object v0, Liyz;->dRs:[Liyz;

    return-object v0

    .line 30223
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final Ad()I
    .locals 1

    .prologue
    .line 30252
    iget v0, p0, Liyz;->dRt:I

    return v0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 30202
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liyz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Liyz;->agv:I

    iget v0, p0, Liyz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liyz;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Liyz;->dRt:I

    iget v0, p0, Liyz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liyz;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 30284
    iget v0, p0, Liyz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 30285
    const/4 v0, 0x1

    iget v1, p0, Liyz;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 30287
    :cond_0
    iget v0, p0, Liyz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 30288
    const/4 v0, 0x2

    iget v1, p0, Liyz;->dRt:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 30290
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 30291
    return-void
.end method

.method public final getType()I
    .locals 1

    .prologue
    .line 30233
    iget v0, p0, Liyz;->agv:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 30295
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 30296
    iget v1, p0, Liyz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 30297
    const/4 v1, 0x1

    iget v2, p0, Liyz;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 30300
    :cond_0
    iget v1, p0, Liyz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 30301
    const/4 v1, 0x2

    iget v2, p0, Liyz;->dRt:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 30304
    :cond_1
    return v0
.end method

.method public final nQ(I)Liyz;
    .locals 1

    .prologue
    .line 30236
    iput p1, p0, Liyz;->agv:I

    .line 30237
    iget v0, p0, Liyz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liyz;->aez:I

    .line 30238
    return-object p0
.end method

.method public final nR(I)Liyz;
    .locals 1

    .prologue
    .line 30255
    iput p1, p0, Liyz;->dRt:I

    .line 30256
    iget v0, p0, Liyz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liyz;->aez:I

    .line 30257
    return-object p0
.end method
