.class public final Lfgj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lffk;


# instance fields
.field private final cqQ:Ljdw;

.field cqR:Z


# direct methods
.method public constructor <init>(Ljdw;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfgj;->cqR:Z

    .line 27
    iput-object p1, p0, Lfgj;->cqQ:Ljdw;

    .line 28
    return-void
.end method


# virtual methods
.method public final aK(Landroid/content/Context;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 43
    iget-object v1, p0, Lfgj;->cqQ:Ljdw;

    iget-object v0, v1, Ljdw;->ahD:Lixx;

    if-eqz v0, :cond_2

    iget-object v0, v1, Ljdw;->ahD:Lixx;

    invoke-virtual {v0}, Lixx;->baX()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Ljdw;->ahD:Lixx;

    invoke-virtual {v1}, Lixx;->getUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 44
    :goto_0
    if-nez v0, :cond_0

    iget-object v1, p0, Lfgj;->cqQ:Ljdw;

    invoke-virtual {v1}, Ljdw;->bhT()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iget-object v1, p0, Lfgj;->cqQ:Ljdw;

    invoke-virtual {v1}, Ljdw;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->x(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-static {p1, v0}, Lhgn;->b(Landroid/content/Context;Lcom/google/android/shared/search/Query;)Landroid/content/Intent;

    move-result-object v0

    .line 48
    :cond_0
    iget-boolean v1, p0, Lfgj;->cqR:Z

    if-eqz v1, :cond_1

    .line 49
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 51
    :cond_1
    return-object v0

    .line 43
    :cond_2
    iget-object v0, v1, Ljdw;->dSf:Lixr;

    if-eqz v0, :cond_4

    iget-object v0, v1, Ljdw;->dSf:Lixr;

    invoke-virtual {v0}, Lixr;->pb()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v1, v1, Ljdw;->dSf:Lixr;

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.INSERT"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v2, "title"

    invoke-virtual {v1}, Lixr;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, v1, Lixr;->dOo:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    const-string v2, ","

    invoke-static {v2}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v2

    invoke-virtual {v2}, Lifj;->aVV()Lifj;

    move-result-object v2

    const-string v3, "android.intent.extra.EMAIL"

    iget-object v1, v1, Lixr;->dOo:[Ljava/lang/String;

    invoke-virtual {v2, v1}, Lifj;->b([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_3
    const-string v1, "com.google.android.shared.util.SimpleIntentStarter.ERROR_TOAST_ID"

    const v2, 0x7f0a0085

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    :cond_4
    iget-object v0, v1, Ljdw;->dME:Lixn;

    if-eqz v0, :cond_5

    iget-object v0, v1, Ljdw;->dME:Lixn;

    invoke-virtual {v0}, Lixn;->bau()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.CALL"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "tel:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Ljdw;->dME:Lixn;

    invoke-virtual {v1}, Lixn;->Ar()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final aL(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 73
    iget-object v0, p0, Lfgj;->cqQ:Ljdw;

    invoke-virtual {v0}, Ljdw;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 93
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid action type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lfgj;->cqQ:Ljdw;

    invoke-virtual {v2}, Ljdw;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :sswitch_0
    const v0, 0x7f0a090a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 91
    :cond_0
    :goto_0
    return-object v0

    .line 77
    :sswitch_1
    const v0, 0x7f0a090c

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 79
    :sswitch_2
    const v0, 0x7f0a090d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 81
    :sswitch_3
    const v0, 0x7f0a090e

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 83
    :sswitch_4
    const/4 v0, 0x0

    iget-object v1, p0, Lfgj;->cqQ:Ljdw;

    iget-object v1, v1, Ljdw;->ahD:Lixx;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lfgj;->cqQ:Ljdw;

    iget-object v1, v1, Ljdw;->ahD:Lixx;

    invoke-virtual {v1}, Lixx;->baX()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lfgj;->cqQ:Ljdw;

    iget-object v0, v0, Ljdw;->ahD:Lixx;

    invoke-virtual {v0}, Lixx;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 84
    :cond_1
    iget-object v1, p0, Lfgj;->cqQ:Ljdw;

    iget-object v1, v1, Ljdw;->ahD:Lixx;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lfgj;->cqQ:Ljdw;

    iget-object v1, v1, Ljdw;->ahD:Lixx;

    invoke-virtual {v1}, Lixx;->rn()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 86
    iget-object v0, p0, Lfgj;->cqQ:Ljdw;

    iget-object v0, v0, Ljdw;->ahD:Lixx;

    invoke-virtual {v0}, Lixx;->getLabel()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 87
    :cond_2
    if-nez v0, :cond_0

    .line 90
    const-string v0, "ReminderSmartAction"

    const-string v1, "Received a URL action without a label or valid host."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    const v0, 0x7f0a0143

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 73
    nop

    :sswitch_data_0
    .sparse-switch
        0x94 -> :sswitch_0
        0xab -> :sswitch_2
        0xac -> :sswitch_1
        0xad -> :sswitch_4
        0xae -> :sswitch_3
    .end sparse-switch
.end method

.method public final aif()I
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lfgj;->cqQ:Ljdw;

    invoke-virtual {v0}, Ljdw;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 68
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid action type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lfgj;->cqQ:Ljdw;

    invoke-virtual {v2}, Ljdw;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :sswitch_0
    const v0, 0x7f02009d

    .line 66
    :goto_0
    return v0

    .line 60
    :sswitch_1
    const v0, 0x7f0200d8

    goto :goto_0

    .line 62
    :sswitch_2
    const v0, 0x7f0200a6

    goto :goto_0

    .line 64
    :sswitch_3
    const v0, 0x7f0200aa

    goto :goto_0

    .line 66
    :sswitch_4
    const v0, 0x7f0200db

    goto :goto_0

    .line 56
    :sswitch_data_0
    .sparse-switch
        0x94 -> :sswitch_0
        0xab -> :sswitch_2
        0xac -> :sswitch_1
        0xad -> :sswitch_4
        0xae -> :sswitch_3
    .end sparse-switch
.end method

.method public final aza()Ljava/lang/String;
    .locals 2

    .prologue
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "REMINDER_SMART_ACTION_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lfgj;->cqQ:Ljdw;

    invoke-virtual {v1}, Ljdw;->getType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final azb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    const-string v0, "activity"

    return-object v0
.end method

.method public final azc()Z
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x1

    return v0
.end method

.method public final isActive()Z
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x1

    return v0
.end method
