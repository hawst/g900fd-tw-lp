.class public final Ljzh;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eNx:[Ljzh;


# instance fields
.field private aez:I

.field private afb:Ljava/lang/String;

.field private eNA:[I

.field private eNB:Ljava/lang/String;

.field private eNC:Z

.field private eNy:Ljava/lang/String;

.field public eNz:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3341
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3342
    iput v1, p0, Ljzh;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljzh;->afb:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljzh;->eNy:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljzh;->eNz:[Ljava/lang/String;

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljzh;->eNA:[I

    const-string v0, ""

    iput-object v0, p0, Ljzh;->eNB:Ljava/lang/String;

    iput-boolean v1, p0, Ljzh;->eNC:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljzh;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljzh;->eCz:I

    .line 3343
    return-void
.end method

.method public static bwP()[Ljzh;
    .locals 2

    .prologue
    .line 3237
    sget-object v0, Ljzh;->eNx:[Ljzh;

    if-nez v0, :cond_1

    .line 3238
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 3240
    :try_start_0
    sget-object v0, Ljzh;->eNx:[Ljzh;

    if-nez v0, :cond_0

    .line 3241
    const/4 v0, 0x0

    new-array v0, v0, [Ljzh;

    sput-object v0, Ljzh;->eNx:[Ljzh;

    .line 3243
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3245
    :cond_1
    sget-object v0, Ljzh;->eNx:[Ljzh;

    return-object v0

    .line 3243
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final Ad(Ljava/lang/String;)Ljzh;
    .locals 1

    .prologue
    .line 3256
    if-nez p1, :cond_0

    .line 3257
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3259
    :cond_0
    iput-object p1, p0, Ljzh;->afb:Ljava/lang/String;

    .line 3260
    iget v0, p0, Ljzh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzh;->aez:I

    .line 3261
    return-object p0
.end method

.method public final Ae(Ljava/lang/String;)Ljzh;
    .locals 1

    .prologue
    .line 3278
    if-nez p1, :cond_0

    .line 3279
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3281
    :cond_0
    iput-object p1, p0, Ljzh;->eNy:Ljava/lang/String;

    .line 3282
    iget v0, p0, Ljzh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzh;->aez:I

    .line 3283
    return-object p0
.end method

.method public final Af(Ljava/lang/String;)Ljzh;
    .locals 1

    .prologue
    .line 3306
    if-nez p1, :cond_0

    .line 3307
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3309
    :cond_0
    iput-object p1, p0, Ljzh;->eNB:Ljava/lang/String;

    .line 3310
    iget v0, p0, Ljzh;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljzh;->aez:I

    .line 3311
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 3225
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljzh;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljzh;->afb:Ljava/lang/String;

    iget v0, p0, Ljzh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzh;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljzh;->eNy:Ljava/lang/String;

    iget v0, p0, Ljzh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzh;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljzh;->eNz:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljzh;->eNz:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljzh;->eNz:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljzh;->eNz:[Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x28

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v1

    move v2, v1

    :goto_3
    if-ge v3, v4, :cond_5

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Ljsi;->btN()I

    :cond_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v2

    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_3

    :pswitch_0
    add-int/lit8 v0, v2, 0x1

    aput v6, v5, v2

    goto :goto_4

    :cond_5
    if-eqz v2, :cond_0

    iget-object v0, p0, Ljzh;->eNA:[I

    if-nez v0, :cond_6

    move v0, v1

    :goto_5
    if-nez v0, :cond_7

    array-length v3, v5

    if-ne v2, v3, :cond_7

    iput-object v5, p0, Ljzh;->eNA:[I

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Ljzh;->eNA:[I

    array-length v0, v0

    goto :goto_5

    :cond_7
    add-int v3, v0, v2

    new-array v3, v3, [I

    if-eqz v0, :cond_8

    iget-object v4, p0, Ljzh;->eNA:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    invoke-static {v5, v1, v3, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Ljzh;->eNA:[I

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_6
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_9

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_6

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    if-eqz v0, :cond_d

    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljzh;->eNA:[I

    if-nez v2, :cond_b

    move v2, v1

    :goto_7
    add-int/2addr v0, v2

    new-array v4, v0, [I

    if-eqz v2, :cond_a

    iget-object v0, p0, Ljzh;->eNA:[I

    invoke-static {v0, v1, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    :goto_8
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v0

    if-lez v0, :cond_c

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_8

    :pswitch_2
    add-int/lit8 v0, v2, 0x1

    aput v5, v4, v2

    move v2, v0

    goto :goto_8

    :cond_b
    iget-object v2, p0, Ljzh;->eNA:[I

    array-length v2, v2

    goto :goto_7

    :cond_c
    iput-object v4, p0, Ljzh;->eNA:[I

    :cond_d
    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljzh;->eNB:Ljava/lang/String;

    iget v0, p0, Ljzh;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljzh;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljzh;->eNC:Z

    iget v0, p0, Ljzh;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljzh;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x28 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3361
    iget v0, p0, Ljzh;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3362
    const/4 v0, 0x1

    iget-object v2, p0, Ljzh;->afb:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 3364
    :cond_0
    iget v0, p0, Ljzh;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 3365
    const/4 v0, 0x2

    iget-object v2, p0, Ljzh;->eNy:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 3367
    :cond_1
    iget-object v0, p0, Ljzh;->eNz:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljzh;->eNz:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 3368
    :goto_0
    iget-object v2, p0, Ljzh;->eNz:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 3369
    iget-object v2, p0, Ljzh;->eNz:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 3370
    if-eqz v2, :cond_2

    .line 3371
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 3368
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3375
    :cond_3
    iget-object v0, p0, Ljzh;->eNA:[I

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljzh;->eNA:[I

    array-length v0, v0

    if-lez v0, :cond_4

    .line 3376
    :goto_1
    iget-object v0, p0, Ljzh;->eNA:[I

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 3377
    const/4 v0, 0x5

    iget-object v2, p0, Ljzh;->eNA:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 3376
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3380
    :cond_4
    iget v0, p0, Ljzh;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    .line 3381
    const/4 v0, 0x6

    iget-object v1, p0, Ljzh;->eNB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3383
    :cond_5
    iget v0, p0, Ljzh;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_6

    .line 3384
    const/4 v0, 0x7

    iget-boolean v1, p0, Ljzh;->eNC:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 3386
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3387
    return-void
.end method

.method public final bwQ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3275
    iget-object v0, p0, Ljzh;->eNy:Ljava/lang/String;

    return-object v0
.end method

.method public final bwR()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3303
    iget-object v0, p0, Ljzh;->eNB:Ljava/lang/String;

    return-object v0
.end method

.method public final bwS()Z
    .locals 1

    .prologue
    .line 3314
    iget v0, p0, Ljzh;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bwT()Z
    .locals 1

    .prologue
    .line 3325
    iget-boolean v0, p0, Ljzh;->eNC:Z

    return v0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3253
    iget-object v0, p0, Ljzh;->afb:Ljava/lang/String;

    return-object v0
.end method

.method public final jE(Z)Ljzh;
    .locals 1

    .prologue
    .line 3328
    iput-boolean p1, p0, Ljzh;->eNC:Z

    .line 3329
    iget v0, p0, Ljzh;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljzh;->aez:I

    .line 3330
    return-object p0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 3391
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3392
    iget v1, p0, Ljzh;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3393
    const/4 v1, 0x1

    iget-object v3, p0, Ljzh;->afb:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3396
    :cond_0
    iget v1, p0, Ljzh;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 3397
    const/4 v1, 0x2

    iget-object v3, p0, Ljzh;->eNy:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3400
    :cond_1
    iget-object v1, p0, Ljzh;->eNz:[Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljzh;->eNz:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_4

    move v1, v2

    move v3, v2

    move v4, v2

    .line 3403
    :goto_0
    iget-object v5, p0, Ljzh;->eNz:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_3

    .line 3404
    iget-object v5, p0, Ljzh;->eNz:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 3405
    if-eqz v5, :cond_2

    .line 3406
    add-int/lit8 v4, v4, 0x1

    .line 3407
    invoke-static {v5}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 3403
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3411
    :cond_3
    add-int/2addr v0, v3

    .line 3412
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 3414
    :cond_4
    iget-object v1, p0, Ljzh;->eNA:[I

    if-eqz v1, :cond_6

    iget-object v1, p0, Ljzh;->eNA:[I

    array-length v1, v1

    if-lez v1, :cond_6

    move v1, v2

    .line 3416
    :goto_1
    iget-object v3, p0, Ljzh;->eNA:[I

    array-length v3, v3

    if-ge v2, v3, :cond_5

    .line 3417
    iget-object v3, p0, Ljzh;->eNA:[I

    aget v3, v3, v2

    .line 3418
    invoke-static {v3}, Ljsj;->sa(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 3416
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3421
    :cond_5
    add-int/2addr v0, v1

    .line 3422
    iget-object v1, p0, Ljzh;->eNA:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3424
    :cond_6
    iget v1, p0, Ljzh;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_7

    .line 3425
    const/4 v1, 0x6

    iget-object v2, p0, Ljzh;->eNB:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3428
    :cond_7
    iget v1, p0, Ljzh;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_8

    .line 3429
    const/4 v1, 0x7

    iget-boolean v2, p0, Ljzh;->eNC:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3432
    :cond_8
    return v0
.end method
