.class public final Lcrr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final beu:Ljava/util/concurrent/Executor;

.field final bhl:Ljava/util/concurrent/Executor;

.field private mConfig:Lcjs;

.field final mGsaConfig:Lchk;

.field final mHttpHelper:Ldkx;

.field final mLoginHelper:Lcrh;

.field final mUrlHelper:Lcpn;


# direct methods
.method public constructor <init>(Ldkx;Lcpn;Lcrh;Lcjs;Lchk;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lcrr;->mHttpHelper:Ldkx;

    .line 67
    iput-object p2, p0, Lcrr;->mUrlHelper:Lcpn;

    .line 68
    iput-object p3, p0, Lcrr;->mLoginHelper:Lcrh;

    .line 69
    iput-object p4, p0, Lcrr;->mConfig:Lcjs;

    .line 70
    iput-object p5, p0, Lcrr;->mGsaConfig:Lchk;

    .line 71
    iput-object p6, p0, Lcrr;->beu:Ljava/util/concurrent/Executor;

    .line 72
    iput-object p7, p0, Lcrr;->bhl:Ljava/util/concurrent/Executor;

    .line 73
    return-void
.end method

.method static iD(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 125
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 126
    const-string v1, "history_recording_enabled"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 127
    :catch_0
    move-exception v0

    .line 128
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Invalid response"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;ZLemy;)V
    .locals 2

    .prologue
    .line 134
    new-instance v0, Lcrt;

    invoke-direct {v0, p0, p1, p2, p3}, Lcrt;-><init>(Lcrr;Landroid/accounts/Account;ZLemy;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcrt;->a([Ljava/lang/Object;)Lenp;

    .line 148
    return-void
.end method

.method public final a(Landroid/accounts/Account;ZZLemy;)V
    .locals 6

    .prologue
    .line 191
    new-instance v0, Lcrv;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcrv;-><init>(Lcrr;Landroid/accounts/Account;ZZLemy;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcrv;->a([Ljava/lang/Object;)Lenp;

    .line 206
    return-void
.end method

.method public final b(Landroid/accounts/Account;ZZ)Z
    .locals 3

    .prologue
    .line 156
    invoke-static {}, Lenu;->auQ()V

    .line 157
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 159
    :try_start_0
    const-string v0, "enable_history_recording"

    invoke-virtual {v1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 160
    const-string v2, "client"

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcrr;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->LY()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :goto_1
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 167
    new-instance v1, Lcru;

    invoke-direct {v1, p0, v0}, Lcru;-><init>(Lcrr;Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Lcru;->r(Landroid/accounts/Account;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 181
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_2
    return v0

    .line 160
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcrr;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->LX()Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    .line 162
    :catch_0
    move-exception v0

    .line 164
    invoke-static {v0}, Ligm;->g(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    goto :goto_1

    .line 181
    :cond_1
    const/4 v0, 0x0

    goto :goto_2
.end method
