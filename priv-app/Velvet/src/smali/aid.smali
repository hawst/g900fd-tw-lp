.class public final Laid;
.super Laic;
.source "PG"


# instance fields
.field private final Yp:Landroid/content/pm/PackageManager;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Laic;-><init>(Landroid/content/Context;)V

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Laid;->Yp:Landroid/content/pm/PackageManager;

    .line 36
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;Lahz;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Laid;->Yp:Landroid/content/pm/PackageManager;

    invoke-virtual {p2}, Lahz;->getUser()Landroid/os/UserHandle;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getUserBadgedIcon(Landroid/graphics/drawable/Drawable;Landroid/os/UserHandle;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;Lahz;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 59
    if-nez p2, :cond_0

    .line 62
    :goto_0
    return-object p1

    :cond_0
    iget-object v0, p0, Laid;->Yp:Landroid/content/pm/PackageManager;

    invoke-virtual {p2}, Lahz;->getUser()Landroid/os/UserHandle;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getUserBadgedLabel(Ljava/lang/CharSequence;Landroid/os/UserHandle;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0
.end method

.method public final getUserProfiles()Ljava/util/List;
    .locals 3

    .prologue
    .line 40
    iget-object v0, p0, Laid;->Yo:Landroid/os/UserManager;

    invoke-virtual {v0}, Landroid/os/UserManager;->getUserProfiles()Ljava/util/List;

    move-result-object v0

    .line 41
    if-nez v0, :cond_0

    .line 42
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    .line 49
    :goto_0
    return-object v0

    .line 44
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 46
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserHandle;

    .line 47
    invoke-static {v0}, Lahz;->a(Landroid/os/UserHandle;)Lahz;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 49
    goto :goto_0
.end method
