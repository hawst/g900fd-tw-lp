.class public final Lbl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laz;


# instance fields
.field public cP:Ljava/util/ArrayList;

.field public cZ:I

.field private da:Landroid/app/PendingIntent;

.field private db:Ljava/util/ArrayList;

.field public dc:Landroid/graphics/Bitmap;

.field private dd:I

.field private de:I

.field private df:I

.field private dg:I

.field private dh:I

.field private di:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2256
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbl;->cP:Ljava/util/ArrayList;

    .line 2257
    const/4 v0, 0x1

    iput v0, p0, Lbl;->cZ:I

    .line 2259
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbl;->db:Ljava/util/ArrayList;

    .line 2262
    const v0, 0x800005

    iput v0, p0, Lbl;->de:I

    .line 2263
    const/4 v0, -0x1

    iput v0, p0, Lbl;->df:I

    .line 2264
    const/4 v0, 0x0

    iput v0, p0, Lbl;->dg:I

    .line 2266
    const/16 v0, 0x50

    iput v0, p0, Lbl;->di:I

    .line 2273
    return-void
.end method


# virtual methods
.method public final a(Lay;)Lay;
    .locals 5

    .prologue
    .line 2315
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2317
    iget-object v0, p0, Lbl;->cP:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2318
    const-string v2, "actions"

    invoke-static {}, Lat;->af()Lbb;

    move-result-object v3

    iget-object v0, p0, Lbl;->cP:Ljava/util/ArrayList;

    iget-object v4, p0, Lbl;->cP:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Lau;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lau;

    invoke-interface {v3, v0}, Lbb;->a([Lau;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2322
    :cond_0
    iget v0, p0, Lbl;->cZ:I

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    .line 2323
    const-string v0, "flags"

    iget v2, p0, Lbl;->cZ:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2325
    :cond_1
    iget-object v0, p0, Lbl;->da:Landroid/app/PendingIntent;

    if-eqz v0, :cond_2

    .line 2326
    const-string v0, "displayIntent"

    iget-object v2, p0, Lbl;->da:Landroid/app/PendingIntent;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2328
    :cond_2
    iget-object v0, p0, Lbl;->db:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2329
    const-string v2, "pages"

    iget-object v0, p0, Lbl;->db:Ljava/util/ArrayList;

    iget-object v3, p0, Lbl;->db:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Landroid/app/Notification;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 2332
    :cond_3
    iget-object v0, p0, Lbl;->dc:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 2333
    const-string v0, "background"

    iget-object v2, p0, Lbl;->dc:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2335
    :cond_4
    iget v0, p0, Lbl;->dd:I

    if-eqz v0, :cond_5

    .line 2336
    const-string v0, "contentIcon"

    iget v2, p0, Lbl;->dd:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2338
    :cond_5
    iget v0, p0, Lbl;->de:I

    const v2, 0x800005

    if-eq v0, v2, :cond_6

    .line 2339
    const-string v0, "contentIconGravity"

    iget v2, p0, Lbl;->de:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2341
    :cond_6
    iget v0, p0, Lbl;->df:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_7

    .line 2342
    const-string v0, "contentActionIndex"

    iget v2, p0, Lbl;->df:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2345
    :cond_7
    iget v0, p0, Lbl;->dg:I

    if-eqz v0, :cond_8

    .line 2346
    const-string v0, "customSizePreset"

    iget v2, p0, Lbl;->dg:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2348
    :cond_8
    iget v0, p0, Lbl;->dh:I

    if-eqz v0, :cond_9

    .line 2349
    const-string v0, "customContentHeight"

    iget v2, p0, Lbl;->dh:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2351
    :cond_9
    iget v0, p0, Lbl;->di:I

    const/16 v2, 0x50

    if-eq v0, v2, :cond_a

    .line 2352
    const-string v0, "gravity"

    iget v2, p0, Lbl;->di:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2355
    :cond_a
    iget-object v0, p1, Lay;->cF:Landroid/os/Bundle;

    if-nez v0, :cond_b

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p1, Lay;->cF:Landroid/os/Bundle;

    :cond_b
    iget-object v0, p1, Lay;->cF:Landroid/os/Bundle;

    const-string v2, "android.wearable.EXTENSIONS"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2356
    return-object p1
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2173
    new-instance v0, Lbl;

    invoke-direct {v0}, Lbl;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lbl;->cP:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lbl;->cP:Ljava/util/ArrayList;

    iget v1, p0, Lbl;->cZ:I

    iput v1, v0, Lbl;->cZ:I

    iget-object v1, p0, Lbl;->da:Landroid/app/PendingIntent;

    iput-object v1, v0, Lbl;->da:Landroid/app/PendingIntent;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lbl;->db:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lbl;->db:Ljava/util/ArrayList;

    iget-object v1, p0, Lbl;->dc:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lbl;->dc:Landroid/graphics/Bitmap;

    iget v1, p0, Lbl;->dd:I

    iput v1, v0, Lbl;->dd:I

    iget v1, p0, Lbl;->de:I

    iput v1, v0, Lbl;->de:I

    iget v1, p0, Lbl;->df:I

    iput v1, v0, Lbl;->df:I

    iget v1, p0, Lbl;->dg:I

    iput v1, v0, Lbl;->dg:I

    iget v1, p0, Lbl;->dh:I

    iput v1, v0, Lbl;->dh:I

    iget v1, p0, Lbl;->di:I

    iput v1, v0, Lbl;->di:I

    return-object v0
.end method
