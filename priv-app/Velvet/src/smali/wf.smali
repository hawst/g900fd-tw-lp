.class public final Lwf;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final IK:Landroid/graphics/Rect;

.field private static IW:Lwf;


# instance fields
.field private final IL:Landroid/graphics/Canvas;

.field private final IM:Landroid/graphics/Paint;

.field private final IO:Landroid/graphics/Paint;

.field private final IP:Landroid/graphics/Paint;

.field private final IQ:Landroid/graphics/BlurMaskFilter;

.field private final IR:Landroid/graphics/BlurMaskFilter;

.field private final IS:Landroid/graphics/BlurMaskFilter;

.field private final IT:Landroid/graphics/BlurMaskFilter;

.field private final IU:I

.field public final IV:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lwf;->IK:Landroid/graphics/Rect;

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    .prologue
    const/high16 v6, 0x40800000    # 4.0f

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x1

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    iput-object v0, p0, Lwf;->IL:Landroid/graphics/Canvas;

    .line 35
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lwf;->IM:Landroid/graphics/Paint;

    .line 36
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lwf;->IO:Landroid/graphics/Paint;

    .line 37
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lwf;->IP:Landroid/graphics/Paint;

    .line 54
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    iget v0, v0, Lyu;->Me:F

    .line 56
    new-instance v1, Landroid/graphics/BlurMaskFilter;

    mul-float v2, v0, v5

    sget-object v3, Landroid/graphics/BlurMaskFilter$Blur;->OUTER:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v1, v2, v3}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    iput-object v1, p0, Lwf;->IQ:Landroid/graphics/BlurMaskFilter;

    .line 57
    new-instance v1, Landroid/graphics/BlurMaskFilter;

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v2, v0

    sget-object v3, Landroid/graphics/BlurMaskFilter$Blur;->OUTER:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v1, v2, v3}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    iput-object v1, p0, Lwf;->IR:Landroid/graphics/BlurMaskFilter;

    .line 58
    new-instance v1, Landroid/graphics/BlurMaskFilter;

    mul-float v2, v0, v5

    sget-object v3, Landroid/graphics/BlurMaskFilter$Blur;->NORMAL:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v1, v2, v3}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    iput-object v1, p0, Lwf;->IS:Landroid/graphics/BlurMaskFilter;

    .line 60
    new-instance v1, Landroid/graphics/BlurMaskFilter;

    mul-float v2, v0, v6

    sget-object v3, Landroid/graphics/BlurMaskFilter$Blur;->NORMAL:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v1, v2, v3}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    iput-object v1, p0, Lwf;->IT:Landroid/graphics/BlurMaskFilter;

    .line 61
    mul-float v1, v0, v5

    float-to-int v1, v1

    iput v1, p0, Lwf;->IU:I

    .line 62
    mul-float/2addr v0, v6

    float-to-int v0, v0

    iput v0, p0, Lwf;->IV:I

    .line 64
    iget-object v0, p0, Lwf;->IM:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 65
    iget-object v0, p0, Lwf;->IM:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 66
    iget-object v0, p0, Lwf;->IO:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 67
    iget-object v0, p0, Lwf;->IO:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 68
    iget-object v0, p0, Lwf;->IP:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 69
    iget-object v0, p0, Lwf;->IP:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 70
    iget-object v0, p0, Lwf;->IP:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 71
    return-void
.end method

.method public static k(Landroid/content/Context;)Lwf;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lwf;->IW:Lwf;

    if-nez v0, :cond_0

    .line 75
    new-instance v0, Lwf;

    invoke-direct {v0}, Lwf;-><init>()V

    sput-object v0, Lwf;->IW:Lwf;

    .line 77
    :cond_0
    sget-object v0, Lwf;->IW:Lwf;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/android/launcher3/BubbleTextView;)Landroid/graphics/Bitmap;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 156
    invoke-virtual {p1}, Lcom/android/launcher3/BubbleTextView;->getWidth()I

    move-result v0

    iget v1, p0, Lwf;->IV:I

    add-int/2addr v0, v1

    iget v1, p0, Lwf;->IV:I

    add-int/2addr v0, v1

    invoke-virtual {p1}, Lcom/android/launcher3/BubbleTextView;->getHeight()I

    move-result v1

    iget v2, p0, Lwf;->IV:I

    add-int/2addr v1, v2

    iget v2, p0, Lwf;->IV:I

    add-int/2addr v1, v2

    iget v2, p0, Lwf;->IU:I

    add-int/2addr v1, v2

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 161
    iget-object v1, p0, Lwf;->IL:Landroid/graphics/Canvas;

    invoke-virtual {v1, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 163
    sget-object v1, Lwf;->IK:Landroid/graphics/Rect;

    .line 164
    sget-object v2, Lwf;->IK:Landroid/graphics/Rect;

    invoke-virtual {p1, v2}, Lcom/android/launcher3/BubbleTextView;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 166
    invoke-virtual {p1}, Lcom/android/launcher3/BubbleTextView;->getExtendedPaddingTop()I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    invoke-virtual {p1}, Lcom/android/launcher3/BubbleTextView;->getLayout()Landroid/text/Layout;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/text/Layout;->getLineTop(I)I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 172
    iget-object v2, p0, Lwf;->IL:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->save()I

    .line 173
    iget-object v2, p0, Lwf;->IL:Landroid/graphics/Canvas;

    invoke-virtual {p1}, Lcom/android/launcher3/BubbleTextView;->getScaleX()F

    move-result v3

    invoke-virtual {p1}, Lcom/android/launcher3/BubbleTextView;->getScaleY()F

    move-result v4

    invoke-virtual {p1}, Lcom/android/launcher3/BubbleTextView;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    iget v6, p0, Lwf;->IV:I

    add-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {p1}, Lcom/android/launcher3/BubbleTextView;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    iget v7, p0, Lwf;->IV:I

    add-int/2addr v6, v7

    int-to-float v6, v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 176
    iget-object v2, p0, Lwf;->IL:Landroid/graphics/Canvas;

    invoke-virtual {p1}, Lcom/android/launcher3/BubbleTextView;->getScrollX()I

    move-result v3

    neg-int v3, v3

    iget v4, p0, Lwf;->IV:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {p1}, Lcom/android/launcher3/BubbleTextView;->getScrollY()I

    move-result v4

    neg-int v4, v4

    iget v5, p0, Lwf;->IV:I

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 178
    iget-object v2, p0, Lwf;->IL:Landroid/graphics/Canvas;

    sget-object v3, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {v2, v1, v3}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 179
    iget-object v1, p0, Lwf;->IL:Landroid/graphics/Canvas;

    invoke-virtual {p1, v1}, Lcom/android/launcher3/BubbleTextView;->draw(Landroid/graphics/Canvas;)V

    .line 180
    iget-object v1, p0, Lwf;->IL:Landroid/graphics/Canvas;

    invoke-virtual {v1}, Landroid/graphics/Canvas;->restore()V

    .line 182
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 183
    iget-object v2, p0, Lwf;->IO:Landroid/graphics/Paint;

    iget-object v3, p0, Lwf;->IT:Landroid/graphics/BlurMaskFilter;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 184
    iget-object v2, p0, Lwf;->IO:Landroid/graphics/Paint;

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Bitmap;->extractAlpha(Landroid/graphics/Paint;[I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 186
    iget-object v3, p0, Lwf;->IL:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Landroid/graphics/Canvas;->save()I

    .line 187
    iget-object v3, p0, Lwf;->IL:Landroid/graphics/Canvas;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v9, v4}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 188
    iget-object v3, p0, Lwf;->IL:Landroid/graphics/Canvas;

    aget v4, v1, v9

    int-to-float v4, v4

    const/4 v5, 0x1

    aget v1, v1, v5

    int-to-float v1, v1

    invoke-virtual {v3, v4, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 190
    iget-object v1, p0, Lwf;->IM:Landroid/graphics/Paint;

    const/high16 v3, -0x1000000

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 191
    iget-object v1, p0, Lwf;->IM:Landroid/graphics/Paint;

    const/16 v3, 0x1e

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 192
    iget-object v1, p0, Lwf;->IL:Landroid/graphics/Canvas;

    iget-object v3, p0, Lwf;->IM:Landroid/graphics/Paint;

    invoke-virtual {v1, v2, v8, v8, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 194
    iget-object v1, p0, Lwf;->IM:Landroid/graphics/Paint;

    const/16 v3, 0x3c

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 195
    iget-object v1, p0, Lwf;->IL:Landroid/graphics/Canvas;

    iget v3, p0, Lwf;->IU:I

    int-to-float v3, v3

    iget-object v4, p0, Lwf;->IM:Landroid/graphics/Paint;

    invoke-virtual {v1, v2, v8, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 196
    iget-object v1, p0, Lwf;->IL:Landroid/graphics/Canvas;

    invoke-virtual {v1}, Landroid/graphics/Canvas;->restore()V

    .line 198
    iget-object v1, p0, Lwf;->IL:Landroid/graphics/Canvas;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 199
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 201
    return-object v0
.end method

.method public final a(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V
    .locals 6

    .prologue
    .line 86
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lwf;->a(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IIZ)V

    .line 87
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IIZ)V
    .locals 14

    .prologue
    .line 93
    if-eqz p5, :cond_2

    .line 94
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    mul-int/2addr v1, v2

    new-array v2, v1, [I

    .line 95
    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    move-object v1, p1

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 97
    const/4 v1, 0x0

    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 98
    aget v3, v2, v1

    ushr-int/lit8 v3, v3, 0x18

    .line 99
    const/16 v4, 0xbc

    if-ge v3, v4, :cond_0

    .line 100
    const/4 v3, 0x0

    aput v3, v2, v1

    .line 97
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 103
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    move-object v1, p1

    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 106
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->extractAlpha()Landroid/graphics/Bitmap;

    move-result-object v7

    .line 109
    iget-object v1, p0, Lwf;->IO:Landroid/graphics/Paint;

    iget-object v2, p0, Lwf;->IQ:Landroid/graphics/BlurMaskFilter;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 110
    const/4 v1, 0x2

    new-array v8, v1, [I

    .line 111
    iget-object v1, p0, Lwf;->IO:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v8}, Landroid/graphics/Bitmap;->extractAlpha(Landroid/graphics/Paint;[I)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 113
    iget-object v1, p0, Lwf;->IO:Landroid/graphics/Paint;

    iget-object v2, p0, Lwf;->IR:Landroid/graphics/BlurMaskFilter;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 114
    const/4 v1, 0x2

    new-array v10, v1, [I

    .line 115
    iget-object v1, p0, Lwf;->IO:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v10}, Landroid/graphics/Bitmap;->extractAlpha(Landroid/graphics/Paint;[I)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 118
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 119
    const/high16 v1, -0x1000000

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_OUT:Landroid/graphics/PorterDuff$Mode;

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 120
    iget-object v1, p0, Lwf;->IO:Landroid/graphics/Paint;

    iget-object v2, p0, Lwf;->IS:Landroid/graphics/BlurMaskFilter;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 121
    const/4 v1, 0x2

    new-array v12, v1, [I

    .line 122
    iget-object v1, p0, Lwf;->IO:Landroid/graphics/Paint;

    invoke-virtual {v7, v1, v12}, Landroid/graphics/Bitmap;->extractAlpha(Landroid/graphics/Paint;[I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 125
    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 126
    const/4 v1, 0x0

    aget v1, v12, v1

    neg-int v1, v1

    int-to-float v1, v1

    const/4 v2, 0x1

    aget v2, v12, v2

    neg-int v2, v2

    int-to-float v2, v2

    iget-object v3, p0, Lwf;->IP:Landroid/graphics/Paint;

    move-object/from16 v0, p2

    invoke-virtual {v0, v7, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 128
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x0

    aget v1, v12, v1

    neg-int v1, v1

    int-to-float v4, v1

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v5, v1

    iget-object v6, p0, Lwf;->IP:Landroid/graphics/Paint;

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 130
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v4, v1

    const/4 v1, 0x1

    aget v1, v12, v1

    neg-int v1, v1

    int-to-float v5, v1

    iget-object v6, p0, Lwf;->IP:Landroid/graphics/Paint;

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 134
    move-object/from16 v0, p2

    invoke-virtual {v0, p1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 135
    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 136
    iget-object v1, p0, Lwf;->IM:Landroid/graphics/Paint;

    move/from16 v0, p3

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 137
    const/4 v1, 0x0

    aget v1, v12, v1

    int-to-float v1, v1

    const/4 v2, 0x1

    aget v2, v12, v2

    int-to-float v2, v2

    iget-object v3, p0, Lwf;->IM:Landroid/graphics/Paint;

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 139
    const/4 v1, 0x0

    aget v1, v8, v1

    int-to-float v1, v1

    const/4 v2, 0x1

    aget v2, v8, v2

    int-to-float v2, v2

    iget-object v3, p0, Lwf;->IM:Landroid/graphics/Paint;

    move-object/from16 v0, p2

    invoke-virtual {v0, v9, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 143
    iget-object v1, p0, Lwf;->IM:Landroid/graphics/Paint;

    move/from16 v0, p4

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 144
    const/4 v1, 0x0

    aget v1, v10, v1

    int-to-float v1, v1

    const/4 v2, 0x1

    aget v2, v10, v2

    int-to-float v2, v2

    iget-object v3, p0, Lwf;->IM:Landroid/graphics/Paint;

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 148
    const/4 v1, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 149
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->recycle()V

    .line 150
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V

    .line 151
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    .line 152
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 153
    return-void
.end method
