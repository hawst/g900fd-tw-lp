.class public final Lcit;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcis;


# instance fields
.field private final mGsaConfigFlags:Lchk;

.field private final mHttpHelper:Ldkx;


# direct methods
.method public constructor <init>(Lchk;Ldkx;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcit;->mGsaConfigFlags:Lchk;

    .line 21
    iput-object p2, p0, Lcit;->mHttpHelper:Ldkx;

    .line 22
    return-void
.end method


# virtual methods
.method public final E(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcit;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->Gb()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/lookup?type=contact&id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 31
    new-instance v1, Ldlb;

    invoke-direct {v1, v0}, Ldlb;-><init>(Ljava/lang/String;)V

    .line 32
    const-string v0, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "OAuth "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ldlb;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    const/4 v0, 0x0

    .line 36
    :try_start_0
    iget-object v2, p0, Lcit;->mHttpHelper:Ldkx;

    const/16 v3, 0x8

    invoke-virtual {v2, v1, v3}, Ldkx;->b(Ldlb;I)Ljava/lang/String;
    :try_end_0
    .catch Left; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 44
    :goto_0
    return-object v0

    .line 37
    :catch_0
    move-exception v1

    .line 38
    invoke-virtual {v1}, Left;->getErrorCode()I

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcit;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->Gb()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/merge?container=contact"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 62
    new-instance v1, Ldld;

    invoke-direct {v1, v0}, Ldld;-><init>(Ljava/lang/String;)V

    .line 63
    const-string v0, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "OAuth "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ldld;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v0, "Content-Type"

    const-string v2, "application/json"

    invoke-virtual {v1, v0, v2}, Ldld;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-virtual {v1, p3}, Ldld;->jO(Ljava/lang/String;)V

    .line 67
    const/4 v0, 0x0

    .line 69
    :try_start_0
    iget-object v2, p0, Lcit;->mHttpHelper:Ldkx;

    const/16 v3, 0x8

    invoke-virtual {v2, v1, v3}, Ldkx;->a(Ldld;I)Ljava/lang/String;
    :try_end_0
    .catch Left; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 77
    :goto_0
    return-object v0

    .line 70
    :catch_0
    move-exception v1

    .line 71
    invoke-virtual {v1}, Left;->getErrorCode()I

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method
