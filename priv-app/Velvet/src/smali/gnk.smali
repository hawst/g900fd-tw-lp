.class public final Lgnk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private Oq:I

.field public aTr:Ljava/lang/String;

.field public aTs:Ljava/lang/String;

.field public anC:I

.field public aok:Z

.field public axT:Ljava/lang/String;

.field public bhQ:F

.field public bhR:[F

.field public bhS:[F

.field public bhT:[F

.field public cFP:Z

.field public cFx:Z

.field public cOM:Lgjl;

.field private cPA:I

.field public cPY:Lgmx;

.field private cPZ:Z

.field public cPu:Z

.field public cPw:Ljtp;

.field public cQa:Z

.field public cQb:Ljava/lang/String;

.field public cQc:Ljava/util/List;

.field public cQd:Ljava/lang/String;

.field public cQe:Lgjo;

.field public cQf:Z

.field public cQg:Z

.field private cQh:Z

.field public cQi:Z

.field public cQj:Z

.field private cQm:I

.field private cQn:I

.field public mRequestId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 439
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400
    const/4 v0, 0x2

    iput v0, p0, Lgnk;->Oq:I

    .line 402
    iput-boolean v2, p0, Lgnk;->cPZ:Z

    .line 403
    iput-boolean v1, p0, Lgnk;->cQa:Z

    .line 404
    iput-boolean v2, p0, Lgnk;->cFP:Z

    .line 405
    const-string v0, "en-US"

    iput-object v0, p0, Lgnk;->cQb:Ljava/lang/String;

    .line 406
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    iput-object v0, p0, Lgnk;->cQc:Ljava/util/List;

    .line 409
    sget-object v0, Lgjl;->cMB:Lgjl;

    iput-object v0, p0, Lgnk;->cOM:Lgjl;

    .line 410
    sget-object v0, Lgjo;->cML:Lgjo;

    iput-object v0, p0, Lgnk;->cQe:Lgjo;

    .line 411
    iput-boolean v1, p0, Lgnk;->cQf:Z

    .line 412
    iput v1, p0, Lgnk;->anC:I

    .line 413
    iput v3, p0, Lgnk;->bhQ:F

    .line 414
    new-array v0, v2, [F

    aput v3, v0, v1

    iput-object v0, p0, Lgnk;->bhR:[F

    .line 415
    new-array v0, v1, [F

    iput-object v0, p0, Lgnk;->bhS:[F

    .line 416
    new-array v0, v1, [F

    iput-object v0, p0, Lgnk;->bhT:[F

    .line 418
    iput-boolean v2, p0, Lgnk;->cQg:Z

    .line 419
    iput-boolean v2, p0, Lgnk;->cQh:Z

    .line 420
    iput-boolean v2, p0, Lgnk;->cPu:Z

    .line 421
    iput-boolean v2, p0, Lgnk;->cQi:Z

    .line 422
    const/4 v0, 0x5

    iput v0, p0, Lgnk;->cPA:I

    .line 431
    iput-boolean v1, p0, Lgnk;->cQj:Z

    .line 434
    const/16 v0, 0x2710

    iput v0, p0, Lgnk;->cQm:I

    .line 435
    const v0, 0xea60

    iput v0, p0, Lgnk;->cQn:I

    .line 437
    iput-boolean v1, p0, Lgnk;->aok:Z

    .line 441
    return-void
.end method


# virtual methods
.method public final aHW()Lgnj;
    .locals 35

    .prologue
    .line 444
    move-object/from16 v0, p0

    iget-object v2, v0, Lgnk;->cPY:Lgmx;

    if-nez v2, :cond_0

    .line 445
    new-instance v2, Lgmy;

    invoke-direct {v2}, Lgmy;-><init>()V

    invoke-virtual {v2}, Lgmy;->aHB()Lgmx;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lgnk;->cPY:Lgmx;

    .line 447
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lgnk;->aTs:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 448
    move-object/from16 v0, p0

    iget v2, v0, Lgnk;->Oq:I

    packed-switch v2, :pswitch_data_0

    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown mode "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_0
    const-string v2, "intent-api"

    :goto_0
    move-object/from16 v0, p0

    iput-object v2, v0, Lgnk;->aTs:Ljava/lang/String;

    .line 450
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lgnk;->aTr:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 451
    move-object/from16 v0, p0

    iget-object v2, v0, Lgnk;->cQc:Ljava/util/List;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lgnk;->cQc:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    .line 453
    :goto_1
    move-object/from16 v0, p0

    iget v3, v0, Lgnk;->Oq:I

    packed-switch v3, :pswitch_data_1

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown mode "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 448
    :pswitch_1
    const-string v2, "service-api"

    goto :goto_0

    :pswitch_2
    const-string v2, "voice-search"

    goto :goto_0

    :pswitch_3
    const-string v2, "voice-ime"

    goto :goto_0

    :pswitch_4
    const-string v2, "hands-free"

    goto :goto_0

    :pswitch_5
    const-string v2, "now-tv"

    goto :goto_0

    .line 451
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 453
    :pswitch_6
    if-eqz v2, :cond_5

    const-string v2, "multi-recognizer"

    :goto_2
    move-object/from16 v0, p0

    iput-object v2, v0, Lgnk;->aTr:Ljava/lang/String;

    .line 455
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lgnk;->mRequestId:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 456
    sget-object v2, Leoi;->cgG:Leoi;

    invoke-virtual {v2}, Leoi;->auU()J

    move-result-wide v2

    invoke-static {v2, v3}, Leoi;->toString(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lgnk;->mRequestId:Ljava/lang/String;

    .line 458
    :cond_4
    new-instance v2, Lgnj;

    move-object/from16 v0, p0

    iget v3, v0, Lgnk;->Oq:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lgnk;->cPY:Lgmx;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lgnk;->cPZ:Z

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lgnk;->cQa:Z

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lgnk;->cFP:Z

    move-object/from16 v0, p0

    iget-object v8, v0, Lgnk;->cQb:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lgnk;->cQc:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v10, v0, Lgnk;->cQd:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lgnk;->cOM:Lgjl;

    move-object/from16 v0, p0

    iget-object v12, v0, Lgnk;->cQe:Lgjo;

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lgnk;->cQf:Z

    move-object/from16 v0, p0

    iget v14, v0, Lgnk;->anC:I

    move-object/from16 v0, p0

    iget v15, v0, Lgnk;->bhQ:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lgnk;->bhR:[F

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgnk;->bhS:[F

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgnk;->bhT:[F

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lgnk;->cQg:Z

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lgnk;->cQh:Z

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lgnk;->cPu:Z

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lgnk;->cQi:Z

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lgnk;->cPA:I

    move/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgnk;->cPw:Ljtp;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgnk;->aTs:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgnk;->mRequestId:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgnk;->aTr:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lgnk;->cQj:Z

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lgnk;->cFx:Z

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lgnk;->cQm:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lgnk;->cQn:I

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lgnk;->aok:Z

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgnk;->axT:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-direct/range {v2 .. v34}, Lgnj;-><init>(ILgmx;ZZZLjava/lang/String;Ljava/util/List;Ljava/lang/String;Lgjl;Lgjo;ZIF[F[F[FZZZZILandroid/location/Location;Ljtp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIIZLjava/lang/String;)V

    return-object v2

    .line 453
    :cond_5
    const-string v2, "recognizer"

    goto/16 :goto_2

    :pswitch_7
    const-string v2, "voicesearch"

    goto/16 :goto_2

    :pswitch_8
    if-eqz v2, :cond_6

    const-string v2, "multi-voicesearch-web"

    goto/16 :goto_2

    :cond_6
    const-string v2, "voicesearch-web"

    goto/16 :goto_2

    :pswitch_9
    if-eqz v2, :cond_7

    const-string v2, "multi-recognizer"

    goto/16 :goto_2

    :cond_7
    const-string v2, "recognizer"

    goto/16 :goto_2

    :pswitch_a
    const-string v2, "recognizer"

    goto/16 :goto_2

    :pswitch_b
    const-string v2, "sound-search"

    goto/16 :goto_2

    :pswitch_c
    const-string v2, "sound-search-tv"

    goto/16 :goto_2

    :pswitch_d
    const-string v2, "voicesearch-clockwork"

    goto/16 :goto_2

    .line 448
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_2
        :pswitch_5
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 453
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_6
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_7
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_a
        :pswitch_d
    .end packed-switch
.end method

.method public final kt(I)Lgnk;
    .locals 2

    .prologue
    .line 476
    if-ltz p1, :cond_0

    const/16 v0, 0x9

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Unsupported mode"

    invoke-static {v0, v1}, Lifv;->c(ZLjava/lang/Object;)V

    .line 477
    iput p1, p0, Lgnk;->Oq:I

    .line 478
    return-object p0

    .line 476
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
