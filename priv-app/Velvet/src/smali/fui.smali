.class public final Lfui;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 24
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 4

    .prologue
    .line 34
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    .line 35
    iget-object v1, p0, Lfro;->cBc:Lanh;

    .line 37
    iget-object v1, v1, Lanh;->agJ:Lapr;

    .line 38
    const v2, 0x7f1101a9

    invoke-virtual {v1}, Lapr;->tk()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 39
    const v2, 0x7f110080

    invoke-virtual {v1}, Lapr;->tl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 40
    const v2, 0x7f1102f0

    invoke-virtual {v1}, Lapr;->tm()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 42
    iget-object v2, v1, Lapr;->agx:Laor;

    if-eqz v2, :cond_0

    .line 43
    iget-object v1, v1, Lapr;->agx:Laor;

    .line 44
    const v2, 0x7f1101a8

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 46
    new-instance v2, Lfox;

    invoke-direct {v2}, Lfox;-><init>()V

    .line 47
    iget-object v3, v1, Laor;->ajY:Ljbp;

    iput-object v3, v2, Lfox;->mSource:Ljbp;

    iget-object v3, v1, Laor;->ajZ:Ljal;

    iput-object v3, v2, Lfox;->mFrequentPlaceEntry:Ljal;

    invoke-virtual {v1}, Laor;->rg()Z

    move-result v3

    iput-boolean v3, v2, Lfox;->cxZ:Z

    invoke-virtual {v1}, Laor;->rh()Z

    move-result v3

    iput-boolean v3, v2, Lfox;->cya:Z

    invoke-virtual {v1}, Laor;->ri()Z

    move-result v1

    iput-boolean v1, v2, Lfox;->cyb:Z

    const/4 v1, 0x1

    iput-boolean v1, v2, Lfox;->cyc:Z

    .line 54
    iget-object v1, p0, Lfui;->mCardContainer:Lfmt;

    invoke-interface {v1}, Lfmt;->aAD()Lfml;

    move-result-object v1

    iget-object v1, v1, Lfml;->cwa:Lgbo;

    invoke-virtual {v2}, Lfox;->aCh()Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lgbo;->a(Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;Landroid/widget/ImageView;)V

    .line 56
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 58
    :cond_0
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 28
    const v0, 0x7f04011f

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
