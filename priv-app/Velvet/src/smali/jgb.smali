.class public final Ljgb;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dRq:J

.field private ejn:I

.field public ejo:Lixz;

.field public ejp:Ljed;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 755
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 756
    iput v3, p0, Ljgb;->aez:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljgb;->dRq:J

    iput v3, p0, Ljgb;->ejn:I

    iput-object v2, p0, Ljgb;->ejo:Lixz;

    iput-object v2, p0, Ljgb;->ejp:Ljed;

    iput-object v2, p0, Ljgb;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljgb;->eCz:I

    .line 757
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 692
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljgb;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljgb;->dRq:J

    iget v0, p0, Ljgb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljgb;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljgb;->ejn:I

    iget v0, p0, Ljgb;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljgb;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljgb;->ejo:Lixz;

    if-nez v0, :cond_1

    new-instance v0, Lixz;

    invoke-direct {v0}, Lixz;-><init>()V

    iput-object v0, p0, Ljgb;->ejo:Lixz;

    :cond_1
    iget-object v0, p0, Ljgb;->ejo:Lixz;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljgb;->ejp:Ljed;

    if-nez v0, :cond_2

    new-instance v0, Ljed;

    invoke-direct {v0}, Ljed;-><init>()V

    iput-object v0, p0, Ljgb;->ejp:Ljed;

    :cond_2
    iget-object v0, p0, Ljgb;->ejp:Ljed;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 773
    iget v0, p0, Ljgb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 774
    const/4 v0, 0x1

    iget-wide v2, p0, Ljgb;->dRq:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 776
    :cond_0
    iget v0, p0, Ljgb;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 777
    const/4 v0, 0x2

    iget v1, p0, Ljgb;->ejn:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 779
    :cond_1
    iget-object v0, p0, Ljgb;->ejo:Lixz;

    if-eqz v0, :cond_2

    .line 780
    const/4 v0, 0x3

    iget-object v1, p0, Ljgb;->ejo:Lixz;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 782
    :cond_2
    iget-object v0, p0, Ljgb;->ejp:Ljed;

    if-eqz v0, :cond_3

    .line 783
    const/4 v0, 0x4

    iget-object v1, p0, Ljgb;->ejp:Ljed;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 785
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 786
    return-void
.end method

.method public final bcB()J
    .locals 2

    .prologue
    .line 714
    iget-wide v0, p0, Ljgb;->dRq:J

    return-wide v0
.end method

.method public final df(J)Ljgb;
    .locals 1

    .prologue
    .line 717
    iput-wide p1, p0, Ljgb;->dRq:J

    .line 718
    iget v0, p0, Ljgb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljgb;->aez:I

    .line 719
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 790
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 791
    iget v1, p0, Ljgb;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 792
    const/4 v1, 0x1

    iget-wide v2, p0, Ljgb;->dRq:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 795
    :cond_0
    iget v1, p0, Ljgb;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 796
    const/4 v1, 0x2

    iget v2, p0, Ljgb;->ejn:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 799
    :cond_1
    iget-object v1, p0, Ljgb;->ejo:Lixz;

    if-eqz v1, :cond_2

    .line 800
    const/4 v1, 0x3

    iget-object v2, p0, Ljgb;->ejo:Lixz;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 803
    :cond_2
    iget-object v1, p0, Ljgb;->ejp:Ljed;

    if-eqz v1, :cond_3

    .line 804
    const/4 v1, 0x4

    iget-object v2, p0, Ljgb;->ejp:Ljed;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 807
    :cond_3
    return v0
.end method
