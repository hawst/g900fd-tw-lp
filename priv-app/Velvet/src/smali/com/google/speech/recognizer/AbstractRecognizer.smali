.class public abstract Lcom/google/speech/recognizer/AbstractRecognizer;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private callbacks:Ljava/util/List;

.field private nativeObj:J

.field private reader:Ljava/io/InputStream;

.field private rm:Lcom/google/speech/recognizer/ResourceManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/speech/recognizer/AbstractRecognizer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/speech/recognizer/AbstractRecognizer;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/speech/recognizer/AbstractRecognizer;->callbacks:Ljava/util/List;

    .line 36
    invoke-direct {p0}, Lcom/google/speech/recognizer/AbstractRecognizer;->nativeConstruct()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/speech/recognizer/AbstractRecognizer;->nativeObj:J

    .line 37
    return-void
.end method

.method private native nativeCancel(J)I
.end method

.method private native nativeConstruct()J
.end method

.method private native nativeDelete(J)V
.end method

.method protected static native nativeInit()V
.end method

.method private native nativeInitFromFile(JJLjava/lang/String;)I
.end method

.method private native nativeInitFromProto(JJ[B)I
.end method

.method private native nativeRun(J[B)[B
.end method

.method private validate()V
    .locals 4

    .prologue
    .line 158
    iget-wide v0, p0, Lcom/google/speech/recognizer/AbstractRecognizer;->nativeObj:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 159
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "recognizer is not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :cond_0
    return-void
.end method


# virtual methods
.method public addCallback(Ljvj;)I
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/speech/recognizer/AbstractRecognizer;->callbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    const/4 v0, 0x0

    return v0
.end method

.method public cancel()I
    .locals 2

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/google/speech/recognizer/AbstractRecognizer;->validate()V

    .line 112
    iget-wide v0, p0, Lcom/google/speech/recognizer/AbstractRecognizer;->nativeObj:J

    invoke-direct {p0, v0, v1}, Lcom/google/speech/recognizer/AbstractRecognizer;->nativeCancel(J)I

    move-result v0

    return v0
.end method

.method public declared-synchronized delete()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 52
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/speech/recognizer/AbstractRecognizer;->nativeObj:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 53
    iget-wide v0, p0, Lcom/google/speech/recognizer/AbstractRecognizer;->nativeObj:J

    invoke-direct {p0, v0, v1}, Lcom/google/speech/recognizer/AbstractRecognizer;->nativeDelete(J)V

    .line 54
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/recognizer/AbstractRecognizer;->nativeObj:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    :cond_0
    monitor-exit p0

    return-void

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/speech/recognizer/AbstractRecognizer;->delete()V

    .line 43
    return-void
.end method

.method protected handleAudioLevelEvent([B)V
    .locals 3

    .prologue
    .line 146
    new-instance v1, Ljvq;

    invoke-direct {v1}, Ljvq;-><init>()V

    .line 147
    invoke-static {v1, p1}, Ljsr;->c(Ljsr;[B)Ljsr;

    .line 148
    iget-object v0, p0, Lcom/google/speech/recognizer/AbstractRecognizer;->callbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljvj;

    .line 149
    invoke-interface {v0, v1}, Ljvj;->a(Ljvq;)V

    goto :goto_0

    .line 151
    :cond_0
    return-void
.end method

.method protected handleEndpointerEvent([B)V
    .locals 3

    .prologue
    .line 137
    new-instance v1, Ljvr;

    invoke-direct {v1}, Ljvr;-><init>()V

    .line 138
    invoke-static {v1, p1}, Ljsr;->c(Ljsr;[B)Ljsr;

    .line 139
    iget-object v0, p0, Lcom/google/speech/recognizer/AbstractRecognizer;->callbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljvj;

    .line 140
    invoke-interface {v0, v1}, Ljvj;->a(Ljvr;)V

    goto :goto_0

    .line 142
    :cond_0
    return-void
.end method

.method protected handleRecognitionEvent([B)V
    .locals 3

    .prologue
    .line 128
    new-instance v1, Ljvv;

    invoke-direct {v1}, Ljvv;-><init>()V

    .line 129
    invoke-static {v1, p1}, Ljsr;->c(Ljsr;[B)Ljsr;

    .line 130
    iget-object v0, p0, Lcom/google/speech/recognizer/AbstractRecognizer;->callbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljvj;

    .line 131
    invoke-interface {v0, v1}, Ljvj;->b(Ljvv;)V

    goto :goto_0

    .line 133
    :cond_0
    return-void
.end method

.method public initFromFile(Ljava/lang/String;Lcom/google/speech/recognizer/ResourceManager;)I
    .locals 7

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/speech/recognizer/AbstractRecognizer;->validate()V

    .line 64
    iput-object p2, p0, Lcom/google/speech/recognizer/AbstractRecognizer;->rm:Lcom/google/speech/recognizer/ResourceManager;

    .line 65
    iget-wide v2, p0, Lcom/google/speech/recognizer/AbstractRecognizer;->nativeObj:J

    invoke-virtual {p2}, Lcom/google/speech/recognizer/ResourceManager;->bva()J

    move-result-wide v4

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/speech/recognizer/AbstractRecognizer;->nativeInitFromFile(JJLjava/lang/String;)I

    move-result v0

    .line 66
    return v0
.end method

.method public initFromProto([BLcom/google/speech/recognizer/ResourceManager;)I
    .locals 7

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/google/speech/recognizer/AbstractRecognizer;->validate()V

    .line 75
    iput-object p2, p0, Lcom/google/speech/recognizer/AbstractRecognizer;->rm:Lcom/google/speech/recognizer/ResourceManager;

    .line 76
    iget-wide v2, p0, Lcom/google/speech/recognizer/AbstractRecognizer;->nativeObj:J

    invoke-virtual {p2}, Lcom/google/speech/recognizer/ResourceManager;->bva()J

    move-result-wide v4

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/speech/recognizer/AbstractRecognizer;->nativeInitFromProto(JJ[B)I

    move-result v0

    .line 77
    return v0
.end method

.method public read([B)I
    .locals 2

    .prologue
    .line 117
    array-length v0, p1

    if-nez v0, :cond_0

    .line 118
    new-instance v0, Ljava/io/IOException;

    const-string v1, "illegal zero length buffer"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/google/speech/recognizer/AbstractRecognizer;->reader:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I

    move-result v0

    .line 121
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 122
    const/4 v0, 0x0

    .line 123
    :cond_1
    return v0
.end method

.method public run(Ljwb;)Ljvn;
    .locals 3

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/speech/recognizer/AbstractRecognizer;->validate()V

    .line 98
    iget-wide v0, p0, Lcom/google/speech/recognizer/AbstractRecognizer;->nativeObj:J

    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/speech/recognizer/AbstractRecognizer;->nativeRun(J[B)[B

    move-result-object v0

    .line 100
    :try_start_0
    new-instance v1, Ljvn;

    invoke-direct {v1}, Ljvn;-><init>()V

    invoke-static {v1, v0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Ljvn;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    :goto_0
    return-object v0

    .line 102
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/speech/recognizer/AbstractRecognizer;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "bad protocol buffer from recognizer jni"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 103
    new-instance v0, Ljvn;

    invoke-direct {v0}, Ljvn;-><init>()V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljvn;->sA(I)Ljvn;

    move-result-object v0

    goto :goto_0
.end method

.method public setAudioReader(Ljava/io/InputStream;)I
    .locals 1

    .prologue
    .line 83
    iput-object p1, p0, Lcom/google/speech/recognizer/AbstractRecognizer;->reader:Ljava/io/InputStream;

    .line 84
    const/4 v0, 0x0

    return v0
.end method
