.class public Lcom/google/speech/grammar/pumpkin/ActionFrameManager;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private eFa:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {}, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->nativeCreate()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->eFa:J

    .line 22
    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->eFa:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Couldn\'t create action_frame_manager from the provided config"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_0
    return-void
.end method

.method private declared-synchronized delete()V
    .locals 2

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->eFa:J

    invoke-static {v0, v1}, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->nativeDelete(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    monitor-exit p0

    return-void

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static native nativeCreate()J
.end method

.method private static native nativeDelete(J)V
.end method

.method private static native nativeLoadActionFrame(J[B)J
.end method


# virtual methods
.method public final S([B)Lcom/google/speech/grammar/pumpkin/ActionFrame;
    .locals 4

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->eFa:J

    invoke-static {v0, v1, p1}, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->nativeLoadActionFrame(J[B)J

    move-result-wide v0

    .line 39
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 40
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Couldn\'t create action_frame from the provided ActionSetConfig"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_0
    new-instance v2, Lcom/google/speech/grammar/pumpkin/ActionFrame;

    invoke-direct {v2, v0, v1}, Lcom/google/speech/grammar/pumpkin/ActionFrame;-><init>(J)V

    .line 44
    return-object v2
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/google/speech/grammar/pumpkin/ActionFrameManager;->delete()V

    .line 75
    return-void
.end method
