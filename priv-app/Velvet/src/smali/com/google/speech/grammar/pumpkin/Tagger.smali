.class public Lcom/google/speech/grammar/pumpkin/Tagger;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private eFF:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/google/speech/grammar/pumpkin/Tagger;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/speech/grammar/pumpkin/Tagger;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>([B)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, Lcom/google/speech/grammar/pumpkin/Tagger;->nativeConstruct([B)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/speech/grammar/pumpkin/Tagger;->eFF:J

    .line 31
    return-void
.end method

.method private declared-synchronized delete()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 67
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/Tagger;->eFF:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 68
    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/Tagger;->eFF:J

    invoke-static {v0, v1}, Lcom/google/speech/grammar/pumpkin/Tagger;->nativeDelete(J)V

    .line 69
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/speech/grammar/pumpkin/Tagger;->eFF:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    :cond_0
    monitor-exit p0

    return-void

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static native nativeConstruct([B)J
.end method

.method private static native nativeDelete(J)V
.end method

.method private static native nativeTag(JJJLjava/lang/String;)[B
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/speech/grammar/pumpkin/ActionFrame;Lcom/google/speech/grammar/pumpkin/UserValidators;)Ljup;
    .locals 7

    .prologue
    .line 42
    if-nez p2, :cond_0

    .line 43
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Passed null ActionFrame to the Pumpkin Tagger"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    if-nez p3, :cond_1

    .line 46
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Passed null UserValidators to the Pumpkin Tagger"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_1
    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/Tagger;->eFF:J

    invoke-virtual {p2}, Lcom/google/speech/grammar/pumpkin/ActionFrame;->buH()J

    move-result-wide v2

    invoke-virtual {p3}, Lcom/google/speech/grammar/pumpkin/UserValidators;->getNativeUserValidators()J

    move-result-wide v4

    move-object v6, p1

    invoke-static/range {v0 .. v6}, Lcom/google/speech/grammar/pumpkin/Tagger;->nativeTag(JJJLjava/lang/String;)[B

    move-result-object v0

    .line 55
    :try_start_0
    invoke-static {v0}, Ljup;->ay([B)Ljup;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 59
    :goto_0
    return-object v0

    .line 57
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/speech/grammar/pumpkin/Tagger;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "Couldn\'t parse PumpkinTaggerResults proto from JNI"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 59
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/google/speech/grammar/pumpkin/Tagger;->delete()V

    .line 77
    return-void
.end method
