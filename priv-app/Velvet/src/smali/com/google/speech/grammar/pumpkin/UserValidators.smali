.class public Lcom/google/speech/grammar/pumpkin/UserValidators;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private javaValidators:Ljava/util/Map;

.field private nativeUserValidators:J


# direct methods
.method public constructor <init>(Ljui;)V
    .locals 1

    .prologue
    .line 33
    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/speech/grammar/pumpkin/UserValidators;-><init>([B)V

    .line 34
    return-void
.end method

.method public constructor <init>([B)V
    .locals 4

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-direct {p0, p1}, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeCreate([B)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeUserValidators:J

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/speech/grammar/pumpkin/UserValidators;->javaValidators:Ljava/util/Map;

    .line 26
    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeUserValidators:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 27
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Couldn\'t create UserValidator native object from the provided config"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_0
    return-void
.end method

.method private declared-synchronized delete()V
    .locals 2

    .prologue
    .line 107
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeUserValidators:J

    invoke-direct {p0, v0, v1}, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeDelete(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    monitor-exit p0

    return-void

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private native nativeAddJavaValidator(JLjava/lang/String;)V
.end method

.method private native nativeAddMapBasedValidator(JLjava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
.end method

.method private native nativeAddUserValidator(JLjava/lang/String;[Ljava/lang/String;)V
.end method

.method private native nativeCreate([B)J
.end method

.method private native nativeDelete(J)V
.end method

.method private native nativeSetContacts(J[Ljava/lang/String;)V
.end method


# virtual methods
.method public declared-synchronized addUserValidator(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 76
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeUserValidators:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeAddUserValidator(JLjava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    monitor-exit p0

    return-void

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized addUserValidatorFromMap(Ljava/lang/String;Ljava/util/Map;)V
    .locals 7

    .prologue
    .line 85
    monitor-enter p0

    :try_start_0
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v0

    new-array v5, v0, [Ljava/lang/String;

    .line 86
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v0

    new-array v6, v0, [Ljava/lang/String;

    .line 87
    const/4 v0, 0x0

    .line 88
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 89
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v2

    .line 90
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v6, v2

    .line 91
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 92
    goto :goto_0

    .line 93
    :cond_0
    iget-wide v2, p0, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeUserValidators:J

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeAddMapBasedValidator(JLjava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    monitor-exit p0

    return-void

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized addValidator(Ljava/lang/String;Ljuq;)V
    .locals 2

    .prologue
    .line 37
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeUserValidators:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeAddJavaValidator(JLjava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/UserValidators;->javaValidators:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    monitor-exit p0

    return-void

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public canonicalize(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/UserValidators;->javaValidators:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuq;

    .line 52
    if-nez v0, :cond_0

    .line 53
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Java validator should exist at this point."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_0
    invoke-virtual {v0, p2}, Ljuq;->mZ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/google/speech/grammar/pumpkin/UserValidators;->delete()V

    .line 100
    return-void
.end method

.method public getNativeUserValidators()J
    .locals 2

    .prologue
    .line 111
    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeUserValidators:J

    return-wide v0
.end method

.method public getPosterior(Ljava/lang/String;Ljava/lang/String;)F
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/speech/grammar/pumpkin/UserValidators;->javaValidators:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuq;

    .line 44
    if-nez v0, :cond_0

    .line 45
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Java validator should exist at this point."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_0
    invoke-virtual {v0, p2}, Ljuq;->mY(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public declared-synchronized setContacts([Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 67
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeUserValidators:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/speech/grammar/pumpkin/UserValidators;->nativeSetContacts(J[Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    monitor-exit p0

    return-void

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
