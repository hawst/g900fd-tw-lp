.class public Lcom/google/android/e300/VerificationService;
.super Landroid/app/IntentService;
.source "PG"


# instance fields
.field private anC:I

.field private anD:Ljava/lang/String;

.field private anL:I

.field private anM:Ljava/lang/String;

.field private anl:Larv;

.field private mAsyncServices:Lema;

.field private mGsaConfigFlags:Lchk;

.field private mSearchSettings:Lcke;

.field private mSpeechSettings:Lhym;

.field private mVss:Lhhq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 81
    const-class v0, Lcom/google/android/e300/VerificationService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 82
    return-void
.end method

.method public static I(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 91
    const-string v0, "sid"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/e300/VerificationService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "sample_rate_hz"

    const/16 v3, 0x3e80

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "speaker_mode"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "audio_directory"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 96
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    .prologue
    .line 121
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 122
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    iput-object v0, p0, Lcom/google/android/e300/VerificationService;->mAsyncServices:Lema;

    .line 123
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/e300/VerificationService;->mVss:Lhhq;

    .line 124
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/e300/VerificationService;->mSearchSettings:Lcke;

    .line 125
    iget-object v0, p0, Lcom/google/android/e300/VerificationService;->mVss:Lhhq;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    iput-object v0, p0, Lcom/google/android/e300/VerificationService;->mSpeechSettings:Lhym;

    .line 126
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/e300/VerificationService;->mGsaConfigFlags:Lchk;

    .line 127
    iget-object v0, p0, Lcom/google/android/e300/VerificationService;->mVss:Lhhq;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/e300/VerificationService;->anD:Ljava/lang/String;

    .line 128
    return-void
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 11

    .prologue
    const/4 v2, 0x2

    const/4 v9, 0x0

    .line 132
    const-string v0, "sample_rate_hz"

    const/16 v1, 0x3e80

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/e300/VerificationService;->anL:I

    .line 133
    const-string v0, "speaker_mode"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/e300/VerificationService;->anC:I

    .line 134
    const-string v0, "audio_directory"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/e300/VerificationService;->anM:Ljava/lang/String;

    .line 136
    iget-object v0, p0, Lcom/google/android/e300/VerificationService;->anM:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/e300/VerificationService;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->HO()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/e300/VerificationService;->mSpeechSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aTE()Z

    move-result v0

    if-nez v0, :cond_1

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    iget v0, p0, Lcom/google/android/e300/VerificationService;->anC:I

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/e300/VerificationService;->mSpeechSettings:Lhym;

    iget-object v1, p0, Lcom/google/android/e300/VerificationService;->mSearchSettings:Lcke;

    invoke-interface {v1}, Lcke;->ND()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhym;->oD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    :cond_2
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 152
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/e300/VerificationService;->anM:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 153
    array-length v2, v1

    move v0, v9

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 154
    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    const-string v5, ".pcm"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 157
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-interface {v10, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :cond_3
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 159
    :catch_0
    move-exception v3

    const-string v3, "VerificationService"

    const-string v4, "File not found while running VerificationService"

    new-array v5, v9, [Ljava/lang/Object;

    const/4 v6, 0x5

    invoke-static {v6, v3, v4, v5}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_2

    .line 164
    :cond_4
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    new-instance v0, Larv;

    invoke-virtual {p0}, Lcom/google/android/e300/VerificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/e300/VerificationService;->mVss:Lhhq;

    iget-object v3, p0, Lcom/google/android/e300/VerificationService;->mAsyncServices:Lema;

    iget-object v4, p0, Lcom/google/android/e300/VerificationService;->mGsaConfigFlags:Lchk;

    iget-object v5, p0, Lcom/google/android/e300/VerificationService;->mSearchSettings:Lcke;

    invoke-interface {v5}, Lcke;->ND()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/e300/VerificationService;->anD:Ljava/lang/String;

    iget v7, p0, Lcom/google/android/e300/VerificationService;->anL:I

    iget v8, p0, Lcom/google/android/e300/VerificationService;->anC:I

    invoke-direct/range {v0 .. v8}, Larv;-><init>(Landroid/content/Context;Lhhq;Lema;Lchk;Ljava/lang/String;Ljava/lang/String;II)V

    iput-object v0, p0, Lcom/google/android/e300/VerificationService;->anl:Larv;

    .line 172
    iget-object v0, p0, Lcom/google/android/e300/VerificationService;->anl:Larv;

    new-instance v1, Lasb;

    invoke-direct {v1, p0, v9}, Lasb;-><init>(Lcom/google/android/e300/VerificationService;B)V

    invoke-virtual {v0, v10, v1}, Larv;->a(Ljava/util/List;Lary;)V

    goto :goto_0
.end method
