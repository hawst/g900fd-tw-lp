.class public Lcom/google/android/setupwizard/SetupWizardNavBar;
.super Landroid/app/Fragment;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field private bXK:I

.field private bXL:Landroid/view/ViewGroup;

.field private bXM:Landroid/widget/Button;

.field private bXN:Landroid/widget/Button;

.field private bXO:Lefh;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 33
    const/16 v0, 0x1202

    iput v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXK:I

    .line 48
    return-void
.end method


# virtual methods
.method public final aom()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXN:Landroid/widget/Button;

    return-object v0
.end method

.method public final aon()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXM:Landroid/widget/Button;

    return-object v0
.end method

.method public final eP(Z)V
    .locals 2

    .prologue
    .line 96
    iget v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXK:I

    and-int/lit16 v0, v0, -0x1203

    iput v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXK:I

    iget-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXL:Landroid/view/ViewGroup;

    iget v1, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXK:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setSystemUiVisibility(I)V

    .line 97
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 53
    check-cast p1, Lefh;

    iput-object p1, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXO:Lefh;

    .line 54
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXN:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 139
    iget-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXO:Lefh;

    invoke-interface {v0}, Lefh;->aoo()V

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXM:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXO:Lefh;

    invoke-interface {v0}, Lefh;->aop()V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x3

    const/4 v8, 0x2

    const/4 v2, 0x0

    .line 59
    new-instance v3, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-array v5, v6, [I

    fill-array-data v5, :array_0

    invoke-virtual {v0, v5}, Landroid/app/Activity;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v5

    invoke-virtual {v5, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    if-nez v0, :cond_0

    new-array v0, v6, [F

    new-array v6, v6, [F

    invoke-virtual {v5, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v7

    invoke-static {v7, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    invoke-virtual {v5, v8, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v7

    invoke-static {v7, v6}, Landroid/graphics/Color;->colorToHSV(I[F)V

    aget v0, v0, v8

    aget v6, v6, v8

    cmpl-float v0, v0, v6

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    const v0, 0x7f090096

    :cond_0
    :goto_1
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    invoke-direct {v3, v4, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 60
    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 61
    const v1, 0x7f040169

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXL:Landroid/view/ViewGroup;

    .line 63
    iget-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXL:Landroid/view/ViewGroup;

    const v1, 0x7f1103ee

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXM:Landroid/widget/Button;

    .line 64
    iget-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXL:Landroid/view/ViewGroup;

    const v1, 0x7f1103ed

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXN:Landroid/widget/Button;

    .line 65
    iget-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXM:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXN:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXL:Landroid/view/ViewGroup;

    return-object v0

    :cond_1
    move v0, v2

    .line 59
    goto :goto_0

    :cond_2
    const v0, 0x7f090097

    goto :goto_1

    nop

    :array_0
    .array-data 4
        0x7f0100e5
        0x1010030
        0x1010031
    .end array-data
.end method

.method public onPreDraw()Z
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXL:Landroid/view/ViewGroup;

    iget v1, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXK:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setSystemUiVisibility(I)V

    .line 87
    const/4 v0, 0x1

    return v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 72
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXO:Lefh;

    invoke-interface {v0, p0}, Lefh;->a(Lcom/google/android/setupwizard/SetupWizardNavBar;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXL:Landroid/view/ViewGroup;

    iget v1, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXK:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setSystemUiVisibility(I)V

    .line 78
    iget-object v0, p0, Lcom/google/android/setupwizard/SetupWizardNavBar;->bXL:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 79
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 80
    return-void
.end method
