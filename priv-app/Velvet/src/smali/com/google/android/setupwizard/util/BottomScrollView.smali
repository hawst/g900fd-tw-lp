.class public Lcom/google/android/setupwizard/util/BottomScrollView;
.super Landroid/widget/ScrollView;
.source "PG"


# instance fields
.field private bXP:Lefi;

.field private bXQ:I

.field private bXR:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->bXR:Z

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->bXR:Z

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->bXR:Z

    .line 32
    return-void
.end method

.method private aoq()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->bXP:Lefi;

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/BottomScrollView;->getScrollY()I

    move-result v0

    iget v1, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->bXQ:I

    if-lt v0, v1, :cond_1

    .line 62
    iget-object v0, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->bXP:Lefi;

    invoke-interface {v0}, Lefi;->aor()V

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->bXR:Z

    if-nez v0, :cond_0

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->bXR:Z

    .line 65
    iget-object v0, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->bXP:Lefi;

    invoke-interface {v0}, Lefi;->aos()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lefi;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->bXP:Lefi;

    .line 36
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-super/range {p0 .. p5}, Landroid/widget/ScrollView;->onLayout(ZIIII)V

    .line 41
    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/util/BottomScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 42
    if-eqz v0, :cond_0

    .line 43
    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/util/BottomScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    sub-int/2addr v0, p5

    add-int/2addr v0, p3

    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/BottomScrollView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/setupwizard/util/BottomScrollView;->bXQ:I

    .line 46
    :cond_0
    sub-int v0, p5, p3

    if-lez v0, :cond_1

    .line 47
    invoke-direct {p0}, Lcom/google/android/setupwizard/util/BottomScrollView;->aoq()V

    .line 49
    :cond_1
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 54
    if-eq p4, p2, :cond_0

    .line 55
    invoke-direct {p0}, Lcom/google/android/setupwizard/util/BottomScrollView;->aoq()V

    .line 57
    :cond_0
    return-void
.end method
