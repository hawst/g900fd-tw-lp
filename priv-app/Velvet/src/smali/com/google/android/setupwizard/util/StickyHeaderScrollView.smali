.class public Lcom/google/android/setupwizard/util/StickyHeaderScrollView;
.super Lcom/google/android/setupwizard/util/BottomScrollView;
.source "PG"


# instance fields
.field private arQ:I

.field private bXX:Landroid/view/View;

.field private bXY:Landroid/view/View;

.field private bXZ:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/setupwizard/util/BottomScrollView;-><init>(Landroid/content/Context;)V

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->arQ:I

    .line 34
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXZ:Landroid/graphics/RectF;

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/google/android/setupwizard/util/BottomScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->arQ:I

    .line 34
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXZ:Landroid/graphics/RectF;

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/setupwizard/util/BottomScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->arQ:I

    .line 34
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXZ:Landroid/graphics/RectF;

    .line 46
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 73
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/util/BottomScrollView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXX:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 76
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 78
    iget-object v0, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXY:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXY:Landroid/view/View;

    .line 80
    :goto_0
    iget-object v1, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXY:Landroid/view/View;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXX:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    .line 82
    :goto_1
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->getScrollY()I

    move-result v5

    sub-int/2addr v4, v5

    .line 83
    add-int v5, v4, v1

    iget v6, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->arQ:I

    if-lt v5, v6, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v5

    if-nez v5, :cond_4

    .line 85
    :cond_0
    iget-object v5, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXZ:Landroid/graphics/RectF;

    neg-int v6, v1

    iget v7, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->arQ:I

    add-int/2addr v6, v7

    int-to-float v6, v6

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v8

    sub-int v1, v8, v1

    iget v8, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->arQ:I

    add-int/2addr v1, v8

    int-to-float v1, v1

    invoke-virtual {v5, v9, v6, v7, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 87
    neg-int v1, v4

    int-to-float v1, v1

    iget-object v4, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXZ:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v4

    invoke-virtual {p1, v9, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 88
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-virtual {p1, v2, v2, v1, v4}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 89
    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 93
    :goto_2
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 95
    :cond_1
    return-void

    .line 78
    :cond_2
    iget-object v0, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXX:Landroid/view/View;

    goto :goto_0

    :cond_3
    move v1, v2

    .line 80
    goto :goto_1

    .line 91
    :cond_4
    iget-object v0, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXZ:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->setEmpty()V

    goto :goto_2
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXZ:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXZ:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    neg-float v0, v0

    iget-object v1, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXZ:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    neg-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 65
    iget-object v0, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXY:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 67
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/setupwizard/util/BottomScrollView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 1

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->getFitsSystemWindows()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iput v0, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->arQ:I

    .line 103
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 50
    invoke-super/range {p0 .. p5}, Lcom/google/android/setupwizard/util/BottomScrollView;->onLayout(ZIIII)V

    .line 51
    iget-object v0, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXX:Landroid/view/View;

    if-nez v0, :cond_0

    .line 52
    const-string v0, "sticky"

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXX:Landroid/view/View;

    const-string v0, "stickyContainer"

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/setupwizard/util/StickyHeaderScrollView;->bXY:Landroid/view/View;

    .line 54
    :cond_0
    return-void
.end method
