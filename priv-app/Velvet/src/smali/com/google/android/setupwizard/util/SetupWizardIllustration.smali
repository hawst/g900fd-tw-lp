.class public Lcom/google/android/setupwizard/util/SetupWizardIllustration;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private OX:F

.field private OY:F

.field private bXS:F

.field private bXT:Landroid/graphics/drawable/Drawable;

.field private final bXU:Landroid/graphics/Rect;

.field private final bXV:Landroid/graphics/Rect;

.field private bXW:F

.field private zx:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 49
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXU:Landroid/graphics/Rect;

    .line 35
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXV:Landroid/graphics/Rect;

    .line 36
    iput v3, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->OX:F

    .line 37
    iput v3, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->OY:F

    .line 38
    iput v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXW:F

    .line 50
    if-eqz p2, :cond_0

    .line 51
    sget-object v0, Lbwe;->aMc:[I

    invoke-virtual {p1, p2, v0, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 53
    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXW:F

    .line 54
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 57
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x41000000    # 8.0f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXS:F

    .line 58
    invoke-virtual {p0, v2}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->setWillNotDraw(Z)V

    .line 59
    return-void
.end method

.method private a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;Z)V
    .locals 2

    .prologue
    .line 164
    const/16 v0, 0x37

    invoke-static {v0, p2, p3, p4, p5}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 165
    invoke-virtual {p5}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    int-to-float v1, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->OX:F

    .line 166
    invoke-virtual {p5}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    int-to-float v1, p3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->OY:F

    .line 167
    return-void
.end method

.method public static d(Landroid/view/View;II)V
    .locals 5

    .prologue
    .line 170
    const v0, 0x7f1103f8

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;

    .line 172
    if-eqz v0, :cond_0

    .line 173
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 174
    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 176
    :cond_0
    const v0, 0x7f1103fc

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;

    .line 178
    if-eqz v0, :cond_2

    .line 179
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0202c0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/LayerDrawable;

    .line 181
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 182
    instance-of v2, v3, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_1

    move-object v2, v3

    .line 183
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    const v4, 0x800033

    invoke-virtual {v2, v4}, Landroid/graphics/drawable/BitmapDrawable;->setGravity(I)V

    .line 185
    :cond_1
    const v2, 0x7f1104cf

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 186
    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 188
    :cond_2
    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v4, -0x40800000    # -1.0f

    const/4 v3, 0x0

    .line 130
    invoke-static {p0}, Leot;->ay(Landroid/view/View;)Z

    move-result v0

    .line 131
    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->zx:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 133
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 134
    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXV:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v3, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 136
    iget v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->OX:F

    iget v2, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->OY:F

    invoke-virtual {p1, v1, v2, v3, v3}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 137
    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->scale(FF)V

    .line 139
    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->zx:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v1, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 141
    :cond_0
    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->zx:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 142
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 144
    :cond_1
    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXT:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    .line 145
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 146
    if-eqz v0, :cond_2

    .line 147
    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->scale(FF)V

    .line 148
    iget-object v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXV:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 151
    :cond_2
    iget-object v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXT:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 152
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 154
    :cond_3
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 155
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    const/16 v1, 0x37

    const/4 v9, 0x0

    .line 99
    sub-int v7, p4, p2

    .line 100
    sub-int v8, p5, p3

    .line 101
    iget-object v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXT:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXT:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    .line 103
    iget-object v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXT:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 104
    invoke-static {p0}, Leot;->ay(Landroid/view/View;)Z

    move-result v6

    .line 106
    iget-object v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXU:Landroid/graphics/Rect;

    invoke-virtual {v0, v9, v9, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 107
    iget-object v4, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXU:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXV:Landroid/graphics/Rect;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;Z)V

    .line 109
    iget v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXW:F

    const/4 v4, 0x0

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_0

    .line 112
    int-to-float v0, v2

    iget v2, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->OX:F

    mul-float/2addr v0, v2

    float-to-int v2, v0

    .line 113
    int-to-float v0, v3

    iget v3, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->OX:F

    mul-float/2addr v0, v3

    float-to-int v3, v0

    .line 114
    iget-object v4, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXU:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXV:Landroid/graphics/Rect;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->a(IIILandroid/graphics/Rect;Landroid/graphics/Rect;Z)V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXT:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXV:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->zx:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 122
    iget-object v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->zx:Landroid/graphics/drawable/Drawable;

    int-to-float v1, v7

    iget v2, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->OX:F

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    iget-object v2, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXV:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int v2, v8, v2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->OY:F

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-virtual {v0, v9, v9, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 125
    :cond_2
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 126
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 85
    iget v0, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXW:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 86
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 87
    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXW:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 88
    int-to-float v1, v0

    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXS:F

    rem-float/2addr v0, v2

    sub-float v0, v1, v0

    float-to-int v0, v0

    .line 89
    invoke-virtual {p0, v3, v0, v3, v3}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->setPadding(IIII)V

    .line 91
    :cond_0
    sget v0, Lesp;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 92
    sget-object v0, Landroid/view/ViewOutlineProvider;->BOUNDS:Landroid/view/ViewOutlineProvider;

    invoke-virtual {p0, v0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 94
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 95
    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->zx:Landroid/graphics/drawable/Drawable;

    .line 68
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->invalidate()V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->requestLayout()V

    .line 70
    return-void
.end method

.method public setForeground(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->bXT:Landroid/graphics/drawable/Drawable;

    .line 79
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->invalidate()V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->requestLayout()V

    .line 81
    return-void
.end method
