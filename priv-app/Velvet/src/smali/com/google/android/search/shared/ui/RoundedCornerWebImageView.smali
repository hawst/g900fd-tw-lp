.class public Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;
.super Lcom/google/android/search/shared/ui/WebImageView;
.source "PG"


# instance fields
.field private bVq:Landroid/graphics/drawable/shapes/Shape;

.field private bVr:Landroid/graphics/BitmapShader;

.field private bVs:F

.field private bVt:Z

.field private ob:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 40
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/search/shared/ui/WebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    iput-boolean v3, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVt:Z

    .line 49
    if-eqz p2, :cond_0

    .line 50
    sget-object v0, Lbwe;->aMa:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 52
    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVs:F

    .line 53
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    .line 54
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 56
    iget v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVs:F

    invoke-direct {p0, v0, v1}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->a(FI)V

    .line 61
    :goto_0
    return-void

    .line 58
    :cond_0
    iput-object v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVq:Landroid/graphics/drawable/shapes/Shape;

    .line 59
    iput-object v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->ob:Landroid/graphics/Paint;

    goto :goto_0
.end method

.method private a(FI)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 65
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_4

    if-lez p2, :cond_4

    .line 66
    const/16 v0, 0x8

    new-array v0, v0, [F

    .line 67
    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_0

    .line 68
    const/4 v1, 0x0

    aput p1, v0, v1

    .line 69
    aput p1, v0, v3

    .line 71
    :cond_0
    and-int/lit8 v1, p2, 0x2

    if-eqz v1, :cond_1

    .line 72
    const/4 v1, 0x2

    aput p1, v0, v1

    .line 73
    const/4 v1, 0x3

    aput p1, v0, v1

    .line 75
    :cond_1
    and-int/lit8 v1, p2, 0x4

    if-eqz v1, :cond_2

    .line 76
    const/4 v1, 0x4

    aput p1, v0, v1

    .line 77
    const/4 v1, 0x5

    aput p1, v0, v1

    .line 79
    :cond_2
    and-int/lit8 v1, p2, 0x8

    if-eqz v1, :cond_3

    .line 80
    const/4 v1, 0x6

    aput p1, v0, v1

    .line 81
    const/4 v1, 0x7

    aput p1, v0, v1

    .line 83
    :cond_3
    new-instance v1, Landroid/graphics/drawable/shapes/RoundRectShape;

    invoke-direct {v1, v0, v2, v2}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    iput-object v1, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVq:Landroid/graphics/drawable/shapes/Shape;

    .line 84
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->ob:Landroid/graphics/Paint;

    .line 85
    iget-object v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 90
    :goto_0
    return-void

    .line 87
    :cond_4
    iput-object v2, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVq:Landroid/graphics/drawable/shapes/Shape;

    .line 88
    iput-object v2, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->ob:Landroid/graphics/Paint;

    goto :goto_0
.end method

.method private anD()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x3f000000    # 0.5f

    .line 175
    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 176
    if-eqz v1, :cond_0

    instance-of v0, v1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVr:Landroid/graphics/BitmapShader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->ob:Landroid/graphics/Paint;

    if-nez v0, :cond_2

    .line 181
    :cond_0
    iput-boolean v8, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVt:Z

    .line 213
    :cond_1
    :goto_0
    return-void

    .line 186
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVt:Z

    .line 196
    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    move-result-object v2

    .line 197
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    if-eq v2, v0, :cond_3

    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    if-ne v2, v0, :cond_1

    .line 198
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->getPaddingLeft()I

    move-result v3

    sub-int v3, v0, v3

    .line 199
    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->getPaddingTop()I

    move-result v4

    sub-int/2addr v0, v4

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->getPaddingBottom()I

    move-result v4

    sub-int v4, v0, v4

    .line 200
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 201
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 202
    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    if-ne v2, v5, :cond_4

    .line 203
    int-to-float v2, v3

    int-to-float v5, v0

    div-float/2addr v2, v5

    int-to-float v5, v4

    int-to-float v6, v1

    div-float/2addr v5, v6

    invoke-static {v2, v5}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 206
    int-to-float v0, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v7

    float-to-int v0, v0

    .line 207
    int-to-float v1, v1

    mul-float/2addr v1, v2

    add-float/2addr v1, v7

    float-to-int v1, v1

    .line 209
    :cond_4
    if-lt v0, v3, :cond_5

    if-ge v1, v4, :cond_1

    .line 210
    :cond_5
    iput-boolean v8, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVt:Z

    goto :goto_0
.end method


# virtual methods
.method public final c(FFFF)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 108
    cmpl-float v0, p1, v1

    if-nez v0, :cond_1

    cmpl-float v0, p2, v1

    if-nez v0, :cond_1

    cmpl-float v0, p3, v1

    if-nez v0, :cond_1

    cmpl-float v0, p4, v1

    if-nez v0, :cond_1

    .line 112
    iput-object v2, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVq:Landroid/graphics/drawable/shapes/Shape;

    .line 113
    iput-object v2, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->ob:Landroid/graphics/Paint;

    .line 114
    iput-object v2, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVr:Landroid/graphics/BitmapShader;

    .line 139
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->invalidate()V

    .line 140
    return-void

    .line 116
    :cond_1
    const/16 v0, 0x8

    new-array v0, v0, [F

    .line 117
    const/4 v1, 0x0

    aput p1, v0, v1

    .line 118
    aput p1, v0, v3

    .line 119
    const/4 v1, 0x2

    aput p2, v0, v1

    .line 120
    const/4 v1, 0x3

    aput p2, v0, v1

    .line 121
    const/4 v1, 0x4

    aput p3, v0, v1

    .line 122
    const/4 v1, 0x5

    aput p3, v0, v1

    .line 123
    const/4 v1, 0x6

    aput p4, v0, v1

    .line 124
    const/4 v1, 0x7

    aput p4, v0, v1

    .line 125
    new-instance v1, Landroid/graphics/drawable/shapes/RoundRectShape;

    invoke-direct {v1, v0, v2, v2}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    iput-object v1, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVq:Landroid/graphics/drawable/shapes/Shape;

    .line 128
    iget-object v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->ob:Landroid/graphics/Paint;

    if-nez v0, :cond_2

    .line 129
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->ob:Landroid/graphics/Paint;

    .line 130
    iget-object v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 132
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v0, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 133
    new-instance v1, Landroid/graphics/BitmapShader;

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    sget-object v2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v3, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v1, v0, v2, v3}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    iput-object v1, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVr:Landroid/graphics/BitmapShader;

    goto :goto_0
.end method

.method public final hi(I)V
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVs:F

    invoke-direct {p0, v0, p1}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->a(FI)V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->invalidate()V

    .line 102
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 218
    if-eqz v0, :cond_0

    instance-of v0, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVr:Landroid/graphics/BitmapShader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->ob:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVt:Z

    if-nez v0, :cond_1

    .line 223
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/search/shared/ui/WebImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 233
    :goto_0
    return-void

    .line 227
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVr:Landroid/graphics/BitmapShader;

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/BitmapShader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 228
    iget-object v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->ob:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVr:Landroid/graphics/BitmapShader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 231
    iget-object v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVq:Landroid/graphics/drawable/shapes/Shape;

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/shapes/Shape;->resize(FF)V

    .line 232
    iget-object v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVq:Landroid/graphics/drawable/shapes/Shape;

    iget-object v1, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1}, Landroid/graphics/drawable/shapes/Shape;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method protected setFrame(IIII)Z
    .locals 1

    .prologue
    .line 157
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/search/shared/ui/WebImageView;->setFrame(IIII)Z

    move-result v0

    .line 158
    invoke-direct {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->anD()V

    .line 159
    return v0
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 144
    invoke-super {p0, p1}, Lcom/google/android/search/shared/ui/WebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 147
    iget-object v0, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVq:Landroid/graphics/drawable/shapes/Shape;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v0, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 148
    new-instance v1, Landroid/graphics/BitmapShader;

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    sget-object v2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v3, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v1, v0, v2, v3}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    iput-object v1, p0, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->bVr:Landroid/graphics/BitmapShader;

    .line 152
    :cond_0
    invoke-direct {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->anD()V

    .line 153
    return-void
.end method

.method public setScaleType(Landroid/widget/ImageView$ScaleType;)V
    .locals 0

    .prologue
    .line 164
    invoke-super {p0, p1}, Lcom/google/android/search/shared/ui/WebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 165
    invoke-direct {p0}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->anD()V

    .line 166
    return-void
.end method
