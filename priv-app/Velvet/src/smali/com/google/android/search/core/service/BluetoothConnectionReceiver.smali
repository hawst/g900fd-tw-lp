.class public Lcom/google/android/search/core/service/BluetoothConnectionReceiver;
.super Lhjm;
.source "PG"


# instance fields
.field private final mClassifier:Lhiz;

.field private final mSettings:Lhym;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Lhjm;-><init>()V

    .line 37
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lhhq;->aOY()Lhiz;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/core/service/BluetoothConnectionReceiver;->mClassifier:Lhiz;

    .line 39
    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    iput-object v0, p0, Lcom/google/android/search/core/service/BluetoothConnectionReceiver;->mSettings:Lhym;

    .line 40
    return-void
.end method

.method constructor <init>(Lhiz;Lhym;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lhjm;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/search/core/service/BluetoothConnectionReceiver;->mClassifier:Lhiz;

    .line 45
    iput-object p2, p0, Lcom/google/android/search/core/service/BluetoothConnectionReceiver;->mSettings:Lhym;

    .line 46
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Landroid/content/Intent;Lhji;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 51
    const-string v0, "BTConnectionReceiver"

    const-string v1, "onReceive(context, %s, %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v4

    invoke-virtual {p3}, Lhji;->toDebugString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x4

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/search/core/service/BluetoothConnectionReceiver;->mSettings:Lhym;

    invoke-virtual {p3}, Lhji;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhym;->oO(Ljava/lang/String;)Z

    move-result v0

    .line 56
    iget-object v1, p0, Lcom/google/android/search/core/service/BluetoothConnectionReceiver;->mClassifier:Lhiz;

    invoke-virtual {v1, p3}, Lhiz;->b(Lhji;)Z

    move-result v1

    .line 60
    if-nez v0, :cond_1

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/google/android/search/core/service/BluetoothConnectionReceiver;->mClassifier:Lhiz;

    invoke-static {p3}, Lhiz;->a(Lhji;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 67
    const-string v2, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 70
    sget-object v0, Lcgg;->aVt:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcgg;->aVv:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.search.core.action.BTSTARTUP"

    const-class v3, Lcom/google/android/search/core/service/SearchService;

    invoke-direct {v0, v2, v6, p1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 75
    const-string v2, "com.google.android.voicesearch.bluetooth.BVRA_SUPPORTED"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 77
    const-string v1, "bt-device-address"

    invoke-virtual {p3}, Lhji;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 80
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 82
    :cond_3
    const-string v1, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 86
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.search.core.action.BTSTOP"

    const-class v2, Lcom/google/android/search/core/service/SearchService;

    invoke-direct {v0, v1, v6, p1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 89
    const-string v1, "bt-device-address"

    invoke-virtual {p3}, Lhji;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 93
    :cond_4
    const-string v0, "BTConnectionReceiver"

    const-string v1, "Unexpected intent received by BluetoothConnectionReceiver: %s"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p2, v2, v4

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method
