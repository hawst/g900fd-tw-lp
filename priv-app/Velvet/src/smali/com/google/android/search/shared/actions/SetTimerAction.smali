.class public Lcom/google/android/search/shared/actions/SetTimerAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bNo:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Ldve;

    invoke-direct {v0}, Ldve;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/SetTimerAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 16
    iput p1, p0, Lcom/google/android/search/shared/actions/SetTimerAction;->bNo:I

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 21
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/SetTimerAction;->bNo:I

    .line 22
    return-void
.end method


# virtual methods
.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/SetTimerAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final agb()Ldtw;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Ldtw;->bLq:Ldtw;

    return-object v0
.end method

.method public final aiD()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/search/shared/actions/SetTimerAction;->bNo:I

    return v0
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/google/android/search/shared/actions/SetTimerAction;->bNo:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 48
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 49
    iget v0, p0, Lcom/google/android/search/shared/actions/SetTimerAction;->bNo:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 50
    return-void
.end method
