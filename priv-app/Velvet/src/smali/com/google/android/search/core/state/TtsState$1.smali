.class public Lcom/google/android/search/core/state/TtsState$1;
.super Lcom/google/android/shared/util/BitFlags;
.source "PG"


# instance fields
.field private synthetic btf:Ldcw;


# direct methods
.method public constructor <init>(Ldcw;Ljava/lang/Class;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/search/core/state/TtsState$1;->btf:Ldcw;

    invoke-direct {p0, p2}, Lcom/google/android/shared/util/BitFlags;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected final onChanged()V
    .locals 4

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/search/core/state/TtsState$1;->btf:Ldcw;

    invoke-static {v0}, Ldcw;->a(Ldcw;)Lcom/google/android/shared/util/BitFlags;

    move-result-object v0

    const-wide/16 v2, 0x60

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/core/state/TtsState$1;->btf:Ldcw;

    invoke-static {v0}, Ldcw;->a(Ldcw;)Lcom/google/android/shared/util/BitFlags;

    move-result-object v0

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/TtsState$1;->btf:Ldcw;

    invoke-static {v0}, Ldcw;->b(Ldcw;)[B

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/search/core/state/TtsState$1;->btf:Ldcw;

    invoke-static {v0}, Ldcw;->a(Ldcw;)Lcom/google/android/shared/util/BitFlags;

    move-result-object v0

    const-wide/16 v2, 0x86

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/search/core/state/TtsState$1;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 81
    return-void

    .line 76
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
