.class public Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;
.super Landroid/content/ContentProvider;
.source "PG"


# instance fields
.field private axG:Landroid/content/UriMatcher;

.field protected bkJ:J

.field protected bkK:Ldmb;

.field protected mClock:Lemp;

.field protected mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private DC()Lemp;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->mClock:Lemp;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->mClock:Lemp;

    .line 226
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DC()Lemp;

    move-result-object v0

    goto :goto_0
.end method

.method protected static a(Ljava/io/File;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 241
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_2

    .line 242
    :cond_0
    const/4 v0, -0x1

    .line 254
    :cond_1
    return v0

    .line 246
    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 247
    if-eqz v2, :cond_1

    .line 248
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 249
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "networkimageloader_"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 250
    add-int/lit8 v0, v0, 0x1

    .line 248
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected static a(Ljava/io/File;J)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 203
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_2

    .line 204
    :cond_0
    const/4 v0, -0x1

    .line 218
    :cond_1
    return v0

    .line 208
    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 209
    if-eqz v2, :cond_1

    .line 210
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 211
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    cmp-long v5, v6, p1

    if-gez v5, :cond_3

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "networkimageloader_"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 213
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 214
    add-int/lit8 v0, v0, 0x1

    .line 210
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private x(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;
    .locals 14

    .prologue
    const-wide/32 v12, 0x2935510

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 122
    iget-object v1, p0, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->mContext:Landroid/content/Context;

    .line 123
    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    .line 124
    const-string v4, "url"

    invoke-virtual {p1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 125
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 126
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 127
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "networkimageloader_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    const/16 v7, 0x8

    invoke-static {v5, v7}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 131
    invoke-direct {p0}, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->DC()Lemp;

    move-result-object v6

    invoke-interface {v6}, Lemp;->uptimeMillis()J

    move-result-wide v6

    .line 132
    invoke-direct {p0}, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->DC()Lemp;

    move-result-object v8

    invoke-interface {v8}, Lemp;->currentTimeMillis()J

    move-result-wide v8

    .line 133
    iget-wide v10, p0, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->bkJ:J

    sub-long v10, v6, v10

    cmp-long v10, v10, v12

    if-lez v10, :cond_0

    .line 134
    sub-long v10, v8, v12

    invoke-static {v1, v10, v11}, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->a(Ljava/io/File;J)I

    .line 135
    iput-wide v6, p0, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->bkJ:J

    .line 138
    :cond_0
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 141
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    const-wide/32 v10, 0x2932e00

    sub-long/2addr v8, v10

    invoke-virtual {v6}, Ljava/io/File;->lastModified()J

    move-result-wide v10

    cmp-long v1, v10, v8

    if-gez v1, :cond_2

    move v1, v2

    :goto_0
    if-nez v1, :cond_3

    .line 144
    const/high16 v1, 0x10000000

    :try_start_0
    invoke-static {v6, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 177
    :cond_1
    :goto_1
    return-object v0

    :cond_2
    move v1, v3

    .line 141
    goto :goto_0

    .line 145
    :catch_0
    move-exception v1

    .line 146
    const-string v4, "NetworkImageLoaderContentProvider"

    const-string v6, "Failed to open file %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v5, v2, v3

    invoke-static {v4, v1, v6, v2}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 151
    :cond_3
    iget-object v1, p0, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->bkK:Ldmb;

    if-nez v1, :cond_4

    .line 152
    new-instance v1, Ldmb;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v7

    invoke-virtual {v7}, Lgql;->SC()Lcfo;

    move-result-object v7

    invoke-virtual {v7}, Lcfo;->DG()Ldkx;

    move-result-object v7

    const/16 v8, 0x12

    invoke-direct {v1, v7, v8}, Ldmb;-><init>(Ldkx;I)V

    iput-object v1, p0, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->bkK:Ldmb;

    .line 158
    :cond_4
    iget-object v1, p0, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->bkK:Ldmb;

    invoke-virtual {v1, v4}, Ldmb;->y(Landroid/net/Uri;)[B

    move-result-object v1

    .line 160
    if-eqz v1, :cond_1

    .line 166
    :try_start_1
    invoke-static {v6}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Leop;

    const/4 v7, 0x0

    invoke-direct {v4, v6, v7}, Leop;-><init>(Ljava/io/File;Z)V

    invoke-static {v1, v4}, Leoo;->a([BLisv;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 174
    const/high16 v1, 0x10000000

    :try_start_2
    invoke-static {v6, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    goto :goto_1

    .line 167
    :catch_1
    move-exception v1

    .line 168
    const-string v4, "NetworkImageLoaderContentProvider"

    const-string v5, "Failed to write bytes to file %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v6, v2, v3

    invoke-static {v4, v1, v5, v2}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 169
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 175
    :catch_2
    move-exception v1

    .line 176
    const-string v4, "NetworkImageLoaderContentProvider"

    const-string v6, "Failed to open file %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v5, v2, v3

    invoke-static {v4, v1, v6, v2}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 95
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->axG:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 84
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 82
    :pswitch_0
    const-string v0, "vnd.android.cursor.item/fetchCode"

    goto :goto_0

    .line 80
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 90
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onCreate()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 62
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->axG:Landroid/content/UriMatcher;

    .line 63
    iget-object v0, p0, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->axG:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.googlequicksearchbox.NetworkImageLoaderContentProvider"

    const-string v2, "icon"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 65
    iput-object v4, p0, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->bkK:Ldmb;

    .line 66
    iput-object v4, p0, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->mClock:Lemp;

    .line 67
    invoke-virtual {p0}, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->mContext:Landroid/content/Context;

    .line 68
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->bkJ:J

    .line 69
    return v3
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 3

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->axG:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 107
    invoke-direct {p0, p1}, Lcom/google/android/search/core/imageloader/NetworkImageLoaderContentProvider;->x(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0

    .line 109
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 75
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 100
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
