.class public Lcom/google/android/search/shared/actions/errors/SoundSearchUnavailableError;
.super Lcom/google/android/search/shared/actions/errors/SearchError;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Ldvq;

    invoke-direct {v0}, Ldvq;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/errors/SoundSearchUnavailableError;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/errors/SearchError;-><init>(Landroid/os/Parcel;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/google/android/shared/search/Query;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/errors/SearchError;-><init>(Lcom/google/android/shared/search/Query;)V

    .line 16
    return-void
.end method


# virtual methods
.method public final aiT()I
    .locals 1

    .prologue
    .line 24
    const v0, 0x7f0a065a

    return v0
.end method

.method public final aiX()I
    .locals 1

    .prologue
    .line 29
    const v0, 0x7f02016a

    return v0
.end method

.method public final aiZ()Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method
