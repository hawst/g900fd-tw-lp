.class public Lcom/google/android/search/shared/actions/ActionExecutionState;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private bLg:B

.field private bLh:Z

.field private bLi:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 245
    new-instance v0, Ldtv;

    invoke-direct {v0}, Ldtv;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/ActionExecutionState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x1

    iput-byte v0, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLg:B

    .line 25
    iput-boolean v1, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLh:Z

    .line 26
    iput-boolean v1, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLi:Z

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-byte v1, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLg:B

    .line 25
    iput-boolean v2, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLh:Z

    .line 26
    iput-boolean v2, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLi:Z

    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLg:B

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLh:Z

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLi:Z

    .line 36
    return-void

    :cond_0
    move v0, v2

    .line 34
    goto :goto_0

    :cond_1
    move v1, v2

    .line 35
    goto :goto_1
.end method

.method private c(B)Z
    .locals 1

    .prologue
    .line 109
    iget-byte v0, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLg:B

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    .line 110
    :goto_0
    iput-byte p1, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLg:B

    .line 111
    return v0

    .line 109
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljrl;)Ljqt;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p1, Ljrl;->eAJ:Ljqt;

    .line 187
    :goto_0
    return-object v0

    .line 180
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p1, Ljrl;->eAL:Ljqt;

    goto :goto_0

    .line 182
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agI()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 183
    iget-object v0, p1, Ljrl;->eAM:Ljqt;

    goto :goto_0

    .line 184
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agJ()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 185
    iget-object v0, p1, Ljrl;->eAK:Ljqt;

    goto :goto_0

    .line 187
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final agA()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->c(B)Z

    move-result v0

    return v0
.end method

.method public final agB()Z
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->c(B)Z

    move-result v0

    return v0
.end method

.method public final agC()Z
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLh:Z

    .line 74
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->c(B)Z

    move-result v0

    return v0
.end method

.method public final agD()Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLh:Z

    .line 83
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->c(B)Z

    move-result v0

    return v0
.end method

.method public final agE()Z
    .locals 1

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->yo()Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    const/4 v0, 0x0

    .line 94
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->c(B)Z

    move-result v0

    goto :goto_0
.end method

.method public final agF()Z
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agG()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agH()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLh:Z

    .line 105
    :cond_1
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->c(B)Z

    move-result v0

    return v0
.end method

.method public final agG()Z
    .locals 2

    .prologue
    .line 115
    iget-byte v0, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLg:B

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final agH()Z
    .locals 2

    .prologue
    .line 127
    iget-byte v0, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLg:B

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final agI()Z
    .locals 2

    .prologue
    .line 144
    iget-byte v0, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLg:B

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final agJ()Z
    .locals 2

    .prologue
    .line 158
    iget-byte v0, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLg:B

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final agK()Z
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLi:Z

    return v0
.end method

.method public final agd()Z
    .locals 1

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLh:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    return v0
.end method

.method public final ek(Z)Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->c(B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    iput-boolean p1, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLi:Z

    .line 55
    const/4 v0, 0x1

    .line 57
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 217
    instance-of v1, p1, Lcom/google/android/search/shared/actions/ActionExecutionState;

    if-nez v1, :cond_1

    .line 223
    :cond_0
    :goto_0
    return v0

    .line 221
    :cond_1
    check-cast p1, Lcom/google/android/search/shared/actions/ActionExecutionState;

    .line 223
    iget-byte v1, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLg:B

    iget-byte v2, p1, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLg:B

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLi:Z

    iget-boolean v2, p1, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLi:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLh:Z

    iget-boolean v2, p1, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLh:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 230
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-byte v2, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLg:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLi:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLh:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final isCanceled()Z
    .locals 2

    .prologue
    .line 151
    iget-byte v0, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLg:B

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isDone()Z
    .locals 2

    .prologue
    .line 134
    iget-byte v0, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLg:B

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 192
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ActionExecutionState["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 193
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->yo()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 194
    const-string v1, "ready"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLi:Z

    if-eqz v1, :cond_6

    .line 208
    const-string v1, ", auto-executed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    :cond_0
    :goto_1
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 195
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->isCanceled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 196
    const-string v1, "canceled"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 197
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->isDone()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 198
    const-string v1, "done"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 199
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agI()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 200
    const-string v1, "uncertain result"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 201
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agJ()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 202
    const-string v1, "execution error"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 204
    :cond_5
    const-string v1, "unknown"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 209
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLh:Z

    if-eqz v1, :cond_0

    .line 210
    const-string v1, ", executed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 240
    iget-byte v0, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLg:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 241
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLh:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 242
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLi:Z

    if-eqz v0, :cond_1

    :goto_1
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 243
    return-void

    :cond_0
    move v0, v2

    .line 241
    goto :goto_0

    :cond_1
    move v1, v2

    .line 242
    goto :goto_1
.end method

.method public final yo()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 123
    iget-byte v1, p0, Lcom/google/android/search/shared/actions/ActionExecutionState;->bLg:B

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
