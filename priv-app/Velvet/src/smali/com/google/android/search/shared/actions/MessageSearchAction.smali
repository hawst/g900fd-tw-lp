.class public Lcom/google/android/search/shared/actions/MessageSearchAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final aoW:Ljava/util/List;

.field private final bMo:Ljava/lang/String;

.field private final bMp:I

.field private final bMq:Z

.field private final bMr:Ljmb;

.field private final bus:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 131
    new-instance v0, Ldul;

    invoke-direct {v0}, Ldul;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/MessageSearchAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->bus:I

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->bMo:Ljava/lang/String;

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->bMp:I

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->bMq:Z

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->aoW:Ljava/util/List;

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->bMr:Ljmb;

    .line 80
    return-void

    .line 76
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/util/List;ILjava/lang/String;IZLjmb;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->aoW:Ljava/util/List;

    .line 62
    iput p2, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->bus:I

    .line 63
    iput-object p3, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->bMo:Ljava/lang/String;

    .line 64
    iput p4, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->bMp:I

    .line 65
    iput-boolean p5, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->bMq:Z

    .line 66
    iput-object p6, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->bMr:Ljmb;

    .line 67
    new-instance v0, Lepf;

    invoke-direct {v0}, Lepf;-><init>()V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 68
    invoke-static {p1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 69
    return-void
.end method


# virtual methods
.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 120
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/MessageSearchAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final agW()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->bus:I

    return v0
.end method

.method public final ahC()Ljava/util/List;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->aoW:Ljava/util/List;

    return-object v0
.end method

.method public final ahD()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->bMo:Ljava/lang/String;

    return-object v0
.end method

.method public final ahE()Z
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->bMq:Z

    return v0
.end method

.method public final ahF()Ljmb;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->bMr:Ljmb;

    return-object v0
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 89
    iget v0, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->bus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 90
    iget-object v0, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->bMo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 91
    iget v0, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->bMp:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 92
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/MessageSearchAction;->bMq:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 94
    return-void

    .line 92
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
