.class public Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;
.super Lcom/google/android/search/shared/actions/utils/Disambiguation;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Ldwy;

    invoke-direct {v0}, Ldwy;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/android/search/shared/actions/utils/Disambiguation;-><init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V

    .line 22
    return-void
.end method

.method public varargs constructor <init>([Ljpd;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 17
    array-length v0, p1

    if-lez v0, :cond_0

    aget-object v0, p1, v2

    invoke-virtual {v0}, Ljpd;->getQuery()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;->b([Ljpd;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/search/shared/actions/utils/Disambiguation;-><init>(Ljava/lang/String;Ljava/util/List;Z)V

    .line 18
    return-void

    .line 17
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static varargs b([Ljpd;)Ljava/util/List;
    .locals 5

    .prologue
    .line 29
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 31
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 32
    new-instance v4, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;

    invoke-direct {v4, v3}, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;-><init>(Ljpd;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 34
    :cond_0
    return-object v1
.end method

.method public static x(Ljava/util/List;)[Ljpd;
    .locals 3

    .prologue
    .line 41
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljpd;

    .line 42
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 43
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;->aky()Ljpd;

    move-result-object v0

    aput-object v0, v2, v1

    .line 42
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 45
    :cond_0
    return-object v2
.end method
