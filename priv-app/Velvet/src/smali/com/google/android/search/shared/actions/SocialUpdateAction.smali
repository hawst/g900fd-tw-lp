.class public Lcom/google/android/search/shared/actions/SocialUpdateAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bMP:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bNz:Ldvl;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Ldvk;

    invoke-direct {v0}, Ldvk;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/SocialUpdateAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SocialUpdateAction;->bMP:Ljava/lang/String;

    .line 63
    invoke-static {}, Ldvl;->values()[Ldvl;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SocialUpdateAction;->bNz:Ldvl;

    .line 64
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ldvl;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ldvl;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 56
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvl;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SocialUpdateAction;->bNz:Ldvl;

    .line 57
    iput-object p1, p0, Lcom/google/android/search/shared/actions/SocialUpdateAction;->bMP:Ljava/lang/String;

    .line 58
    return-void
.end method


# virtual methods
.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 81
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/SocialUpdateAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final aiN()Ldvl;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SocialUpdateAction;->bNz:Ldvl;

    return-object v0
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return v0
.end method

.method public final getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SocialUpdateAction;->bMP:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 86
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 87
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SocialUpdateAction;->bMP:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SocialUpdateAction;->bNz:Ldvl;

    invoke-virtual {v0}, Ldvl;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 89
    return-void
.end method
