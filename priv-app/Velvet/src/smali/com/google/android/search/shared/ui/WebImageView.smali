.class public Lcom/google/android/search/shared/ui/WebImageView;
.super Landroid/widget/ImageView;
.source "PG"


# instance fields
.field private bMy:Landroid/net/Uri;

.field private bVA:Lemy;

.field private bVB:Ledl;

.field bVC:Z

.field private bVD:Z

.field private final bVE:D

.field private final bVF:I

.field private final bVG:I

.field private final bVH:Z

.field private bVz:Leml;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/ui/WebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/shared/ui/WebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    sget-object v0, Lbwe;->aMh:[I

    invoke-virtual {p1, p2, v0, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 47
    const/4 v1, 0x0

    invoke-virtual {v0, v5, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    float-to-double v2, v1

    iput-wide v2, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVE:D

    .line 48
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVF:I

    .line 52
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 54
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/WebImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_0

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0081

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/search/shared/ui/WebImageView;->setBackgroundColor(I)V

    .line 56
    iput-boolean v5, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVD:Z

    .line 59
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {v0, v4, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVG:I

    .line 60
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVH:Z

    .line 62
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 63
    return-void
.end method

.method private anH()V
    .locals 1

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVD:Z

    if-eqz v0, :cond_0

    .line 178
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/ui/WebImageView;->setBackgroundColor(I)V

    .line 180
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lesm;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 121
    iput-object p1, p0, Lcom/google/android/search/shared/ui/WebImageView;->bMy:Landroid/net/Uri;

    .line 125
    iget-object v1, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVz:Leml;

    if-eqz v1, :cond_0

    .line 126
    iget-object v1, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVz:Leml;

    iget-object v2, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVA:Lemy;

    invoke-interface {v1, v2}, Leml;->f(Lemy;)V

    .line 127
    iput-object v0, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVz:Leml;

    .line 132
    :cond_0
    if-eqz p1, :cond_2

    .line 133
    invoke-interface {p2, p1}, Lesm;->D(Landroid/net/Uri;)Leml;

    move-result-object v1

    .line 134
    invoke-interface {v1}, Leml;->auB()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 135
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVC:Z

    .line 136
    invoke-interface {v1}, Leml;->auC()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 137
    iget-object v1, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVB:Ledl;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 138
    iget-object v1, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVB:Ledl;

    invoke-interface {v1, v0}, Ledl;->f(Landroid/graphics/drawable/Drawable;)V

    .line 140
    :cond_1
    invoke-direct {p0}, Lcom/google/android/search/shared/ui/WebImageView;->anH()V

    .line 152
    :cond_2
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/ui/WebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 153
    return-void

    .line 142
    :cond_3
    iget-object v2, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVA:Lemy;

    if-nez v2, :cond_4

    .line 143
    new-instance v2, Ledk;

    invoke-direct {v2, p0}, Ledk;-><init>(Lcom/google/android/search/shared/ui/WebImageView;)V

    iput-object v2, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVA:Lemy;

    .line 145
    :cond_4
    iget-object v2, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVA:Lemy;

    invoke-interface {v1, v2}, Leml;->e(Lemy;)V

    .line 146
    iput-object v1, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVz:Leml;

    goto :goto_0
.end method

.method public final a(Ledl;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVB:Ledl;

    .line 92
    return-void
.end method

.method public final a(Ljava/lang/String;Lesm;)V
    .locals 1

    .prologue
    .line 114
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/search/shared/ui/WebImageView;->a(Landroid/net/Uri;Lesm;)V

    .line 115
    return-void
.end method

.method public final aF(II)V
    .locals 0

    .prologue
    .line 75
    invoke-virtual {p0, p1, p2}, Lcom/google/android/search/shared/ui/WebImageView;->setMeasuredDimension(II)V

    .line 76
    return-void
.end method

.method public final anG()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/search/shared/ui/WebImageView;->bMy:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/ui/WebImageView;->bMy:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 273
    iget v1, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVG:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 274
    invoke-super {p0}, Landroid/widget/ImageView;->drawableStateChanged()V

    .line 294
    :goto_0
    return-void

    .line 278
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/WebImageView;->getDrawableState()[I

    move-result-object v2

    .line 280
    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget v4, v2, v1

    .line 281
    const v5, 0x10100a7

    if-eq v4, v5, :cond_1

    const v5, 0x101009c

    if-ne v4, v5, :cond_3

    .line 282
    :cond_1
    const/4 v0, 0x1

    .line 287
    :cond_2
    if-eqz v0, :cond_4

    .line 288
    iget v0, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVG:I

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/ui/WebImageView;->setColorFilter(I)V

    goto :goto_0

    .line 280
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 292
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/ui/WebImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method

.method public final f(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 156
    invoke-static {}, Lenu;->auR()V

    .line 157
    if-nez p1, :cond_2

    .line 158
    iget-boolean v0, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVH:Z

    if-eqz v0, :cond_0

    .line 159
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/ui/WebImageView;->setVisibility(I)V

    .line 165
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVz:Leml;

    .line 166
    iget-object v0, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVB:Ledl;

    if-eqz v0, :cond_1

    .line 167
    iget-object v0, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVB:Ledl;

    invoke-interface {v0, p1}, Ledl;->f(Landroid/graphics/drawable/Drawable;)V

    .line 169
    :cond_1
    return-void

    .line 162
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/ui/WebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 163
    invoke-direct {p0}, Lcom/google/android/search/shared/ui/WebImageView;->anH()V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVz:Leml;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVz:Leml;

    iget-object v1, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVA:Lemy;

    invoke-interface {v0, v1}, Leml;->f(Lemy;)V

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVz:Leml;

    .line 102
    :cond_0
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 103
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    .line 80
    iget-wide v0, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVE:D

    invoke-static {v0, v1, p1}, Ledj;->a(DI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    iget-wide v2, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVE:D

    iget v4, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVF:I

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/WebImageView;->getSuggestedMinimumWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/WebImageView;->getMaxHeight()I

    move-result v8

    move-object v1, p0

    move v5, p1

    move v6, p2

    invoke-static/range {v1 .. v8}, Ledj;->a(Lcom/google/android/search/shared/ui/WebImageView;DIIIII)V

    .line 88
    :goto_0
    return-void

    .line 86
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    goto :goto_0
.end method
