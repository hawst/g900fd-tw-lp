.class public Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field public Oq:I

.field public Qa:F

.field private final bSP:Landroid/graphics/Rect;

.field private final bSQ:Ljava/lang/String;

.field public bSR:Z

.field public bSS:F

.field public bST:Landroid/graphics/Paint;

.field public bSU:I

.field public bSV:I

.field public bSW:I

.field private bSX:I

.field public bSY:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

.field public bSZ:Landroid/animation/ValueAnimator;

.field private bTa:Landroid/view/TouchDelegate;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const v4, 0x7f0d00cd

    .line 61
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->Oq:I

    .line 63
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSP:Landroid/graphics/Rect;

    .line 65
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->setWillNotDraw(Z)V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0041

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSQ:Ljava/lang/String;

    .line 70
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bST:Landroid/graphics/Paint;

    .line 71
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bST:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b012e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 72
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bST:Landroid/graphics/Paint;

    const/16 v1, 0x99

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 73
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bST:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFlags(I)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSU:I

    .line 75
    invoke-virtual {p0}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00a7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSX:I

    .line 77
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSZ:Landroid/animation/ValueAnimator;

    .line 78
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSZ:Landroid/animation/ValueAnimator;

    sget-object v1, Ldqs;->bGc:Ldqs;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSZ:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 80
    invoke-virtual {p0}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSV:I

    .line 82
    invoke-virtual {p0}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSW:I

    .line 84
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSZ:Landroid/animation/ValueAnimator;

    new-instance v1, Leay;

    invoke-direct {v1, p0}, Leay;-><init>(Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 96
    return-void

    .line 77
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private gZ(I)V
    .locals 1

    .prologue
    .line 164
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bTa:Landroid/view/TouchDelegate;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 165
    return-void

    .line 164
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final gY(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 143
    iget v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->Oq:I

    if-ne v0, p1, :cond_0

    .line 155
    :goto_0
    return-void

    .line 147
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->gZ(I)V

    .line 148
    if-nez p1, :cond_1

    const/4 v0, 0x1

    .line 149
    :goto_1
    if-eqz v0, :cond_2

    .line 150
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSY:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSQ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 154
    :goto_2
    iput p1, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->Oq:I

    goto :goto_0

    :cond_1
    move v0, v1

    .line 148
    goto :goto_1

    .line 152
    :cond_2
    iput-boolean v1, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSR:Z

    goto :goto_2
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSP:Landroid/graphics/Rect;

    const/4 v1, 0x0

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 201
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 202
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 169
    iget v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->Oq:I

    invoke-static {v0}, Ldtd;->gv(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSP:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 173
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSP:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 176
    iget v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->Oq:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSR:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSZ:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 178
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSP:Landroid/graphics/Rect;

    iget v1, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSX:I

    neg-int v1, v1

    iget v2, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSX:I

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->inset(II)V

    .line 179
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSP:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 180
    iget v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->Qa:F

    iget v1, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSS:F

    iget v2, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSU:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bST:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 181
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSP:Landroid/graphics/Rect;

    iget v1, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSX:I

    iget v2, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSX:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->inset(II)V

    .line 182
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 100
    const v0, 0x7f1103c5

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 101
    const v0, 0x7f1103be

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    iput-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSY:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    .line 102
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSY:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    iget-object v2, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSQ:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 103
    new-instance v0, Leaz;

    invoke-direct {v0, p0}, Leaz;-><init>(Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;)V

    .line 104
    invoke-virtual {v1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 105
    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSY:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v1, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 106
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 110
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 115
    iget v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->Oq:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bTa:Landroid/view/TouchDelegate;

    if-nez v0, :cond_0

    .line 116
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSY:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v1}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSY:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v2}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getHeight()I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 117
    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSY:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 118
    iput p3, v0, Landroid/graphics/Rect;->top:I

    .line 119
    iput p5, v0, Landroid/graphics/Rect;->bottom:I

    .line 120
    invoke-static {p0}, Leot;->ay(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 121
    iput p2, v0, Landroid/graphics/Rect;->left:I

    .line 126
    :goto_0
    new-instance v1, Landroid/view/TouchDelegate;

    iget-object v2, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSY:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-direct {v1, v0, v2}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bTa:Landroid/view/TouchDelegate;

    .line 128
    iget v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->Oq:I

    invoke-direct {p0, v0}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->gZ(I)V

    .line 130
    :cond_0
    return-void

    .line 123
    :cond_1
    iput p4, v0, Landroid/graphics/Rect;->right:I

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 188
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 191
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSP:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 192
    if-eqz v0, :cond_0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->getMeasuredHeight()I

    move-result v1

    if-eq v1, v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->Oq:I

    if-nez v0, :cond_1

    .line 194
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->bSP:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/search/shared/overlay/OverlaySearchPlateContainer;->getMeasuredHeight()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 196
    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 138
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 139
    const/4 v0, 0x1

    return v0
.end method
