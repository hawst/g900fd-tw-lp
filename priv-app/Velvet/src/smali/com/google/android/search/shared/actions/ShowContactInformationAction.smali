.class public Lcom/google/android/search/shared/actions/ShowContactInformationAction;
.super Lcom/google/android/search/shared/actions/CommunicationAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private bNp:Lcom/google/android/search/shared/contact/Contact;

.field private final bNq:Ljava/lang/String;

.field private final bNr:I

.field private bNs:Z

.field private bNt:Ljava/util/List;

.field private bNu:Ljava/util/List;

.field private bNv:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 171
    new-instance v0, Ldvf;

    invoke-direct {v0}, Ldvf;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 66
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/CommunicationAction;-><init>(Landroid/os/Parcel;)V

    .line 67
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNr:I

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNq:Ljava/lang/String;

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNs:Z

    .line 72
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNt:Ljava/util/List;

    .line 73
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNu:Ljava/util/List;

    .line 74
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNv:Ljava/util/List;

    .line 75
    return-void

    .line 71
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;ILjava/lang/String;ZLjava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/CommunicationAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    .line 51
    if-eq p2, v0, :cond_0

    const/4 v1, 0x2

    if-eq p2, v1, :cond_0

    const/4 v1, 0x3

    if-eq p2, v1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 55
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    iput p2, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNr:I

    .line 57
    iput-object p3, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNq:Ljava/lang/String;

    .line 58
    iput-boolean p4, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNs:Z

    .line 59
    iput-object p5, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNt:Ljava/util/List;

    .line 60
    iput-object p6, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNu:Ljava/util/List;

    .line 61
    iput-object p7, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNv:Ljava/util/List;

    .line 62
    return-void

    .line 51
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final Wb()Z
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bLV:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bLV:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amv()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 91
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/ShowContactInformationAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final afZ()Ldzb;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Ldzb;->bRt:Ldzb;

    return-object v0
.end method

.method public final agc()I
    .locals 1

    .prologue
    .line 101
    const/16 v0, 0x21

    return v0
.end method

.method public final aiE()Lcom/google/android/search/shared/contact/Contact;
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNp:Lcom/google/android/search/shared/contact/Contact;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNp:Lcom/google/android/search/shared/contact/Contact;

    .line 112
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNp:Lcom/google/android/search/shared/contact/Contact;

    .line 113
    return-object v0
.end method

.method public final aiF()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNr:I

    return v0
.end method

.method public final aiG()Ljava/util/List;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNt:Ljava/util/List;

    return-object v0
.end method

.method public final aiH()Ljava/util/List;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNu:Ljava/util/List;

    return-object v0
.end method

.method public final aiI()Ljava/util/List;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNv:Ljava/util/List;

    return-object v0
.end method

.method public final aiJ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNq:Ljava/lang/String;

    return-object v0
.end method

.method public final aiK()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNs:Z

    return v0
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method public final eo(Z)V
    .locals 0

    .prologue
    .line 157
    iput-boolean p1, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNs:Z

    .line 158
    return-void
.end method

.method public final g(Lcom/google/android/search/shared/contact/Contact;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNp:Lcom/google/android/search/shared/contact/Contact;

    .line 118
    return-void
.end method

.method public final i(Lcom/google/android/search/shared/contact/PersonDisambiguation;)Lcom/google/android/search/shared/actions/CommunicationAction;
    .locals 8

    .prologue
    .line 79
    new-instance v0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;

    iget v2, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNr:I

    iget-object v3, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNq:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNs:Z

    iget-object v5, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNt:Ljava/util/List;

    iget-object v6, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNu:Ljava/util/List;

    iget-object v7, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNv:Ljava/util/List;

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/search/shared/actions/ShowContactInformationAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;ILjava/lang/String;ZLjava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public final s(Ljava/util/List;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNt:Ljava/util/List;

    .line 130
    return-void
.end method

.method public final t(Ljava/util/List;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNu:Ljava/util/List;

    .line 138
    return-void
.end method

.method public final u(Ljava/util/List;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNv:Ljava/util/List;

    .line 146
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 162
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/CommunicationAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 163
    iget v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNr:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 164
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNq:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 165
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNs:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 166
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNt:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 167
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNu:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 168
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ShowContactInformationAction;->bNv:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 169
    return-void

    .line 165
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
