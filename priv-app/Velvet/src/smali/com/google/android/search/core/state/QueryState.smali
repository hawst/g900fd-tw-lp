.class public Lcom/google/android/search/core/state/QueryState;
.super Lddj;
.source "PG"


# instance fields
.field private brA:Z

.field private final brB:Ljava/util/List;

.field private brC:Lcom/google/android/search/core/state/QueryState$BackStackEntry;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private brD:Z

.field private final brE:Ldcm;

.field public brF:Lcmq;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private brG:Z

.field private brH:Z

.field private final brI:Ldcn;

.field private brJ:Lcmq;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private brK:Z

.field private brL:Z

.field private final brM:Ldcm;

.field private brN:Z

.field private brO:Z

.field private brP:I

.field private brQ:Z

.field private brR:[I

.field private brS:J

.field private brT:Z

.field private brU:Z

.field private brV:Z

.field private brW:Z

.field private final brt:Ljava/util/Observer;

.field private final bru:Landroid/util/LongSparseArray;

.field private brv:Lcom/google/android/shared/search/Query;

.field private brw:Lcom/google/android/shared/search/Query;

.field private brx:Z

.field private bry:Z

.field private brz:Lcom/google/android/shared/search/Query;

.field private mCommittedQuery:Lcom/google/android/shared/search/Query;

.field private final mEventBus:Ldda;

.field private final mGsaConfig:Lchk;

.field private mQuery:Lcom/google/android/shared/search/Query;

.field private final mSearchBoxLogging:Lcpd;

.field private final mUrlHelper:Lcpn;


# direct methods
.method public constructor <init>(Ldda;Lcpn;Lcpd;Lchk;)V
    .locals 3

    .prologue
    .line 273
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lddj;-><init>(Ldda;I)V

    .line 162
    new-instance v0, Ldch;

    invoke-direct {v0, p0}, Ldch;-><init>(Lcom/google/android/search/core/state/QueryState;)V

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brt:Ljava/util/Observer;

    .line 200
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brv:Lcom/google/android/shared/search/Query;

    .line 206
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brz:Lcom/google/android/shared/search/Query;

    .line 209
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    .line 275
    iput-object p1, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    .line 276
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    sget-object v1, Lgyt;->cZA:Lgyt;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/shared/search/Query;->a(Ljava/io/Serializable;Landroid/os/Bundle;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->app()Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 277
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apg()Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    .line 278
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brw:Lcom/google/android/shared/search/Query;

    .line 279
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->bru:Landroid/util/LongSparseArray;

    .line 280
    new-instance v0, Ldcn;

    invoke-direct {v0}, Ldcn;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    .line 281
    new-instance v0, Ldcm;

    const-string v1, "network"

    invoke-direct {v0, v1}, Ldcm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    .line 282
    iput-object p2, p0, Lcom/google/android/search/core/state/QueryState;->mUrlHelper:Lcpn;

    .line 283
    iput-object p3, p0, Lcom/google/android/search/core/state/QueryState;->mSearchBoxLogging:Lcpd;

    .line 284
    new-instance v0, Ldcm;

    const-string v1, "webapp"

    invoke-direct {v0, v1}, Ldcm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brM:Ldcm;

    .line 285
    iput-object p4, p0, Lcom/google/android/search/core/state/QueryState;->mGsaConfig:Lchk;

    .line 286
    return-void
.end method

.method private O(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 1162
    invoke-direct {p0, p1}, Lcom/google/android/search/core/state/QueryState;->Y(Lcom/google/android/shared/search/Query;)V

    .line 1163
    invoke-static {p1}, Lcom/google/android/search/core/state/QueryState;->Q(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1164
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apg()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apw()Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    .line 1168
    :goto_0
    return-void

    .line 1166
    :cond_0
    iput-object p1, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    goto :goto_0
.end method

.method private P(Lcom/google/android/shared/search/Query;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1174
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1175
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    if-eq p1, v1, :cond_0

    .line 1177
    iput-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brx:Z

    .line 1178
    iput-object p1, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    .line 1179
    const/4 v0, 0x1

    .line 1181
    :cond_0
    return v0
.end method

.method private static Q(Lcom/google/android/shared/search/Query;)Z
    .locals 1

    .prologue
    .line 1245
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lgyt;->aY(Lcom/google/android/shared/search/Query;)Lgyt;

    move-result-object v0

    invoke-virtual {v0}, Lgyt;->aLJ()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private WX()V
    .locals 2

    .prologue
    .line 293
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    invoke-direct {p0, v0, v1}, Lcom/google/android/search/core/state/QueryState;->a(ZLjava/util/List;)V

    .line 294
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brw:Lcom/google/android/shared/search/Query;

    .line 295
    return-void
.end method

.method private XM()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1791
    sget-object v1, Lcgg;->aVn:Lcgg;

    invoke-virtual {v1}, Lcgg;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1793
    iget-boolean v1, p0, Lcom/google/android/search/core/state/QueryState;->brU:Z

    if-eqz v1, :cond_0

    .line 1795
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aaj()Ldbd;

    move-result-object v1

    .line 1796
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v2}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Ldbd;->yo()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Ldbd;->VK()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1817
    :cond_0
    :goto_0
    return v0

    .line 1810
    :cond_1
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1817
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private XN()Z
    .locals 2

    .prologue
    .line 1826
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->XM()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1827
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    .line 1828
    const/4 v0, 0x1

    .line 1830
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private XQ()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1948
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqA()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    if-ne v1, v2, :cond_1

    .line 1951
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v1}, Lcmq;->Qt()Lcms;

    move-result-object v1

    .line 1952
    if-eqz v1, :cond_1

    .line 1958
    iget-object v2, v1, Lcms;->bcz:Ljava/lang/Boolean;

    if-nez v2, :cond_0

    .line 1961
    iget-object v1, p0, Lddj;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aaj()Ldbd;

    move-result-object v1

    .line 1962
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqV()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v2}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/velvet/ActionData;->aIK()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1966
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoY()Lcom/google/android/shared/search/Query;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 1975
    :goto_0
    return v0

    .line 1969
    :cond_0
    iget-object v1, v1, Lcms;->bcz:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->XM()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1970
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoY()Lcom/google/android/shared/search/Query;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    goto :goto_0

    .line 1975
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private XR()Z
    .locals 3

    .prologue
    .line 2127
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    .line 2130
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brw:Lcom/google/android/shared/search/Query;

    sget-object v2, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aak()Ldcw;

    move-result-object v1

    invoke-virtual {v1}, Ldcw;->isDone()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ldbd;->yo()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2139
    :cond_0
    const/4 v0, 0x0

    .line 2153
    :goto_0
    return v0

    .line 2141
    :cond_1
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 2142
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brw:Lcom/google/android/shared/search/Query;

    .line 2144
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->WX()V

    .line 2146
    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->Y(Lcom/google/android/shared/search/Query;)V

    .line 2151
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->aU(Lcom/google/android/shared/search/Query;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    .line 2153
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private Xd()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 831
    iget-boolean v2, p0, Lcom/google/android/search/core/state/QueryState;->brU:Z

    if-eqz v2, :cond_1

    .line 856
    :cond_0
    :goto_0
    return v0

    .line 835
    :cond_1
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    sget-object v3, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    if-eq v2, v3, :cond_0

    .line 839
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqC()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqB()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 841
    iput-boolean v1, p0, Lcom/google/android/search/core/state/QueryState;->brU:Z

    move v0, v1

    .line 842
    goto :goto_0

    .line 844
    :cond_2
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aaj()Ldbd;

    move-result-object v1

    .line 845
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v2}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ldbd;->yo()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 849
    invoke-virtual {v1}, Ldbd;->VK()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 851
    iget-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brU:Z

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqC()Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brU:Z

    .line 856
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brU:Z

    goto :goto_0

    .line 854
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brU:Z

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqB()Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brU:Z

    goto :goto_1
.end method

.method private Xo()Z
    .locals 1

    .prologue
    .line 1192
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1193
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->Xp()V

    .line 1194
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->Xt()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1195
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    .line 1197
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apg()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apw()Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    .line 1198
    const/4 v0, 0x1

    .line 1200
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Xp()V
    .locals 1

    .prologue
    .line 1208
    iget-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brV:Z

    if-eqz v0, :cond_0

    .line 1209
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brV:Z

    .line 1210
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brW:Z

    .line 1212
    :cond_0
    return-void
.end method

.method private Xr()Z
    .locals 1

    .prologue
    .line 1221
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqU()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqU()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Xt()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1249
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-static {v1}, Lcom/google/android/search/core/state/QueryState;->Q(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1263
    :cond_0
    :goto_0
    return v0

    .line 1253
    :cond_1
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->Xr()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1258
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apX()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1263
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private Xv()V
    .locals 2

    .prologue
    .line 1276
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->Xt()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1277
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    .line 1281
    :goto_0
    return-void

    .line 1279
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->aT(Lcom/google/android/shared/search/Query;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    goto :goto_0
.end method

.method private Y(Lcom/google/android/shared/search/Query;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2220
    const-wide/16 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->AF()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->AF()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->AF()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 2222
    :cond_0
    const-string v0, "QueryState"

    const-string v1, "Commit without new request id. This shouldn\'t happen."

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 2225
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apY()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2226
    const/16 v0, 0xa0

    invoke-direct {p0, p1, v0, v5}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;IZ)V

    .line 2251
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brv:Lcom/google/android/shared/search/Query;

    .line 2252
    iput-object p1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 2254
    return-void

    .line 2227
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2228
    const/16 v0, 0xa1

    invoke-direct {p0, p1, v0, v5}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;IZ)V

    goto :goto_0

    .line 2229
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2230
    const/16 v0, 0xad

    invoke-direct {p0, p1, v0, v5}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;IZ)V

    goto :goto_0

    .line 2231
    :cond_5
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqo()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2232
    const/16 v0, 0x10b

    invoke-direct {p0, p1, v0, v5}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;IZ)V

    goto :goto_0

    .line 2233
    :cond_6
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2234
    invoke-static {p1}, Lgyt;->aY(Lcom/google/android/shared/search/Query;)Lgyt;

    move-result-object v0

    .line 2235
    sget-object v1, Lgyt;->cZA:Lgyt;

    if-eq v0, v1, :cond_2

    .line 2237
    invoke-virtual {v0}, Lgyt;->aLI()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2238
    const/16 v0, 0x10c

    invoke-direct {p0, p1, v0, v4}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;IZ)V

    goto :goto_0

    .line 2240
    :cond_7
    invoke-virtual {v0}, Lgyt;->aLJ()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2241
    const/16 v0, 0x10e

    invoke-direct {p0, p1, v0, v4}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;IZ)V

    goto :goto_0

    .line 2244
    :cond_8
    const/16 v0, 0x110

    invoke-direct {p0, p1, v0, v4}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;IZ)V

    goto :goto_0

    .line 2248
    :cond_9
    const/16 v0, 0x111

    invoke-direct {p0, p1, v0, v4}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;IZ)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/shared/search/Query;IZ)V
    .locals 3

    .prologue
    .line 2257
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aao()Ldcu;

    move-result-object v0

    .line 2258
    invoke-static {p2}, Lege;->hs(I)Litu;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v1

    invoke-virtual {v0}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/service/ClientConfig;->anv()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/shared/search/SearchBoxStats;->QW()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lege;->kQ(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Litu;->mH(I)Litu;

    move-result-object v1

    .line 2262
    if-eqz p3, :cond_0

    .line 2263
    invoke-virtual {v0}, Ldcu;->YW()I

    move-result v0

    .line 2264
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2265
    invoke-virtual {v1, v0}, Litu;->mG(I)Litu;

    .line 2268
    :cond_0
    invoke-static {v1}, Lege;->a(Litu;)V

    .line 2269
    return-void
.end method

.method private static a(Letj;Ljava/lang/String;Lcmq;)V
    .locals 3
    .param p2    # Lcmq;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3069
    if-eqz p2, :cond_0

    .line 3070
    invoke-virtual {p0}, Letj;->avJ()Letj;

    move-result-object v0

    .line 3071
    invoke-virtual {v0, p1}, Letj;->lt(Ljava/lang/String;)V

    .line 3072
    invoke-virtual {v0, p2}, Letj;->b(Leti;)V

    .line 3076
    :goto_0
    return-void

    .line 3074
    :cond_0
    invoke-virtual {p0, p1}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    goto :goto_0
.end method

.method private a(ZLjava/util/List;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 298
    const/4 v0, 0x1

    .line 299
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aaj()Ldbd;

    move-result-object v3

    .line 301
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqo()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 304
    :cond_0
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v3, v2}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v3}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/ActionData;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 314
    :cond_2
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->Xr()Z

    move-result v2

    if-eqz v2, :cond_3

    if-nez p1, :cond_3

    move v0, v1

    .line 318
    :cond_3
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqs()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqp()Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    move v0, v1

    .line 323
    :cond_5
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 334
    :cond_6
    if-eqz v0, :cond_8

    .line 336
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 340
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsd:Lcmq;

    .line 343
    :cond_7
    new-instance v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v3}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v2

    invoke-virtual {v3}, Ldbd;->VP()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    iget-object v5, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v4, v5}, Ldda;->au(Lcom/google/android/shared/search/Query;)Landroid/os/Bundle;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/search/core/state/QueryState$BackStackEntry;-><init>(Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;Ljava/util/List;Landroid/os/Bundle;Lcmq;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 348
    :cond_8
    return-void
.end method

.method public static synthetic a(Lcom/google/android/search/core/state/QueryState;Lcom/google/android/search/core/state/QueryState$BackStackEntry;Z)Z
    .locals 2

    .prologue
    .line 134
    iget-object v0, p1, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsb:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsb:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsb:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/VoiceAction;

    :goto_0
    iget-object v1, p1, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->query:Lcom/google/android/shared/search/Query;

    invoke-direct {p0, v1, v0, p2}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/VoiceAction;Z)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/VoiceAction;Z)Z
    .locals 2
    .param p1    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/search/shared/actions/VoiceAction;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 623
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqA()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 626
    if-eqz p2, :cond_2

    if-eqz p3, :cond_0

    invoke-interface {p2}, Lcom/google/android/search/shared/actions/VoiceAction;->ago()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-interface {p2}, Lcom/google/android/search/shared/actions/VoiceAction;->agd()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p2}, Lcom/google/android/search/shared/actions/VoiceAction;->age()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aaj()Ldbd;

    move-result-object v1

    invoke-virtual {v1, p2}, Ldbd;->s(Lcom/google/android/search/shared/actions/VoiceAction;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 633
    :cond_1
    :goto_0
    return v0

    .line 626
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;I)Z
    .locals 2

    .prologue
    .line 725
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 726
    const/16 v0, 0xcb

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 728
    iput-object p2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 729
    iput-object p2, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    .line 730
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/state/QueryState;->c(Lcmq;)V

    .line 731
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 732
    const/4 v0, 0x1

    .line 734
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/shared/search/Query;ZZ)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2527
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v1, p1}, Ldcm;->ak(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2528
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2529
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aan()Ldcy;

    move-result-object v1

    .line 2530
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aao()Ldcu;

    move-result-object v2

    invoke-virtual {v2}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v2

    .line 2539
    invoke-virtual {v1}, Ldcy;->ZB()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Ldcy;->ZA()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/search/shared/service/ClientConfig;->anj()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2543
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->XT()V

    .line 2544
    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->di(Z)Z

    .line 2558
    :goto_0
    if-eqz p2, :cond_4

    :goto_1
    iput-object p1, p0, Lcom/google/android/search/core/state/QueryState;->brz:Lcom/google/android/shared/search/Query;

    .line 2561
    :goto_2
    return v0

    .line 2548
    :cond_1
    if-eqz p3, :cond_2

    .line 2549
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v1}, Ldcm;->Yr()V

    goto :goto_0

    .line 2551
    :cond_2
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v1}, Ldcm;->Yq()V

    goto :goto_0

    .line 2556
    :cond_3
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v1}, Ldcm;->reset()V

    goto :goto_0

    .line 2558
    :cond_4
    sget-object p1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    goto :goto_1

    .line 2561
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private a(Lifw;)Z
    .locals 3

    .prologue
    .line 705
    const/4 v1, 0x0

    .line 706
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 707
    :goto_0
    if-lez v0, :cond_0

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Lifw;->apply(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 708
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 709
    const/4 v1, 0x1

    .line 710
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 712
    :cond_0
    return v1
.end method

.method private c(Lcom/google/android/search/shared/actions/errors/SearchError;)V
    .locals 2

    .prologue
    .line 1732
    const/16 v0, 0xca

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 1735
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/errors/SearchError;->aja()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->ard()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1736
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->Xa()V

    .line 1745
    :goto_0
    return-void

    .line 1742
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v0, p1}, Ldcm;->d(Lcom/google/android/search/shared/actions/errors/SearchError;)V

    .line 1743
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    goto :goto_0
.end method

.method private c(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V
    .locals 6

    .prologue
    .line 2359
    const-string v0, "QueryState"

    const-string v1, "%s: not current commit, new=%s, WebView=%s, committed=%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iget-object v4, v4, Ldcm;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 2363
    return-void
.end method

.method private d(Lcom/google/android/shared/search/Query;Z)Z
    .locals 2

    .prologue
    .line 2436
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mUrlHelper:Lcpn;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, p1, v1}, Lcpn;->b(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apS()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apS()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2438
    const/4 v0, 0x0

    .line 2449
    :goto_0
    return v0

    .line 2440
    :cond_0
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->WX()V

    .line 2443
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apq()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->Y(Lcom/google/android/shared/search/Query;)V

    .line 2444
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->P(Lcom/google/android/shared/search/Query;)Z

    .line 2445
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/state/QueryState;->c(Lcmq;)V

    .line 2446
    if-nez p2, :cond_1

    .line 2447
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 2449
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private dh(Z)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 578
    .line 580
    if-eqz p1, :cond_2

    .line 581
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    .line 582
    iget-object v4, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v4}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    .line 585
    :goto_0
    iget-object v4, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-direct {p0, v4, v0, v3}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/VoiceAction;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 595
    :goto_1
    return v0

    :cond_0
    move-object v0, v1

    .line 582
    goto :goto_0

    :cond_1
    move v2, v3

    .line 591
    :cond_2
    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    move-object v0, v1

    .line 595
    :goto_2
    new-instance v1, Ldci;

    invoke-direct {v1, p0, v0}, Ldci;-><init>(Lcom/google/android/search/core/state/QueryState;Lcom/google/android/search/core/state/QueryState$BackStackEntry;)V

    invoke-direct {p0, v1}, Lcom/google/android/search/core/state/QueryState;->a(Lifw;)Z

    move-result v0

    or-int/2addr v0, v2

    goto :goto_1

    .line 591
    :cond_4
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    goto :goto_2
.end method

.method private di(Z)Z
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 866
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v0}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v0

    :goto_0
    invoke-virtual {v1}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->aIP()Z

    move-result v0

    if-nez v0, :cond_2

    .line 868
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 869
    if-gez v0, :cond_4

    .line 874
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0, v3, v7}, Ldda;->c(Landroid/os/Bundle;I)V

    .line 936
    :goto_2
    return v7

    :cond_1
    move-object v0, v3

    .line 866
    goto :goto_0

    :cond_2
    invoke-interface {v1}, Lcom/google/android/search/shared/actions/VoiceAction;->agz()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ldcj;

    invoke-direct {v0, p0}, Ldcj;-><init>(Lcom/google/android/search/core/state/QueryState;)V

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->a(Lifw;)Z

    goto :goto_1

    :cond_3
    new-instance v0, Ldck;

    invoke-direct {v0, p0}, Ldck;-><init>(Lcom/google/android/search/core/state/QueryState;)V

    goto :goto_3

    .line 878
    :cond_4
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    .line 889
    new-instance v0, Legm;

    const/4 v1, -0x1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v8, v3

    move-object v9, v3

    invoke-direct/range {v0 .. v9}, Legm;-><init>(IZLjava/lang/String;Ljava/lang/Integer;Legb;Legk;ZLjava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lege;->a(Legm;)V

    .line 891
    if-eqz p1, :cond_b

    iget-object v0, v10, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->query:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aps()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 897
    :goto_4
    iget-boolean v1, p0, Lcom/google/android/search/core/state/QueryState;->brU:Z

    if-eqz v1, :cond_5

    .line 898
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoY()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoZ()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 901
    :cond_5
    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->O(Lcom/google/android/shared/search/Query;)V

    .line 902
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brM:Ldcm;

    invoke-virtual {v1}, Ldcm;->reset()V

    .line 903
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iput-boolean v2, v1, Ldcm;->bsi:Z

    .line 904
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v1}, Ldcm;->reset()V

    .line 905
    invoke-static {v10}, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->a(Lcom/google/android/search/core/state/QueryState$BackStackEntry;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 906
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aam()Ldcg;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    iget-object v5, v10, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsa:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v1, v4, v5}, Ldcg;->a(Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;)V

    .line 907
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqo()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 910
    :cond_6
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v1, v0}, Ldcm;->af(Lcom/google/android/shared/search/Query;)V

    .line 911
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v1, v10, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsa:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0, v1}, Ldcm;->aA(Ljava/lang/Object;)V

    .line 922
    :cond_7
    iget-object v0, v10, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsd:Lcmq;

    if-eqz v0, :cond_8

    iget-object v0, v10, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsd:Lcmq;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    if-ne v0, v1, :cond_8

    iget-object v0, v10, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsd:Lcmq;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iget-object v1, v10, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsd:Lcmq;

    invoke-virtual {v1}, Lcmq;->Qi()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldcn;->ai(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 926
    :cond_8
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    invoke-virtual {v0}, Ldcn;->reset()V

    .line 929
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->XO()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 930
    iput-object v3, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    .line 932
    :cond_9
    invoke-virtual {p0, v3}, Lcom/google/android/search/core/state/QueryState;->c(Lcmq;)V

    .line 934
    :cond_a
    iput-object v10, p0, Lcom/google/android/search/core/state/QueryState;->brC:Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    move v7, v2

    .line 936
    goto/16 :goto_2

    .line 891
    :cond_b
    iget-object v0, v10, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->query:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apr()Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_4
.end method

.method public static fD(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 3018
    packed-switch p0, :pswitch_data_0

    .line 3026
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "INVALID("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 3019
    :pswitch_0
    const-string v0, "IDLE"

    goto :goto_0

    .line 3020
    :pswitch_1
    const-string v0, "COMMITTED"

    goto :goto_0

    .line 3021
    :pswitch_2
    const-string v0, "LOADING"

    goto :goto_0

    .line 3022
    :pswitch_3
    const-string v0, "LOADED"

    goto :goto_0

    .line 3023
    :pswitch_4
    const-string v0, "ERROR"

    goto :goto_0

    .line 3024
    :pswitch_5
    const-string v0, "PAUSED_BY_TIMEOUT"

    goto :goto_0

    .line 3025
    :pswitch_6
    const-string v0, "PAUSED_BY_DEMAND"

    goto :goto_0

    .line 3018
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final I(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 739
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apK()Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 740
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 741
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->app()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->Y(Lcom/google/android/shared/search/Query;)V

    .line 742
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    .line 743
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/state/QueryState;->c(Lcmq;)V

    .line 744
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 746
    :cond_0
    return-void
.end method

.method public final J(Lcom/google/android/shared/search/Query;)Landroid/os/Bundle;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 941
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brC:Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brC:Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    iget-object v0, v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->query:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, p1}, Lcom/google/android/shared/search/Query;->aW(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 943
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brC:Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    iget-object v0, v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsc:Landroid/os/Bundle;

    .line 945
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final K(Lcom/google/android/shared/search/Query;)Z
    .locals 4

    .prologue
    .line 1039
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final L(Lcom/google/android/shared/search/Query;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1061
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 1062
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1063
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brz:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    .line 1064
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brz:Lcom/google/android/shared/search/Query;

    .line 1073
    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 1061
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1069
    goto :goto_1
.end method

.method public final M(Lcom/google/android/shared/search/Query;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1079
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 1080
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    .line 1081
    iget-object v0, v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->query:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    .line 1085
    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 1079
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1085
    goto :goto_1
.end method

.method public final N(Lcom/google/android/shared/search/Query;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1108
    iget-boolean v2, p0, Lcom/google/android/search/core/state/QueryState;->bry:Z

    if-eqz v2, :cond_2

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 1109
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 1111
    :cond_1
    return-void

    .line 1108
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aaj()Ldbd;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2, v3}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-nez v2, :cond_0

    iput-boolean v1, p0, Lcom/google/android/search/core/state/QueryState;->bry:Z

    move v0, v1

    goto :goto_0
.end method

.method public final Qk()[I
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2628
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brR:[I

    return-object v0
.end method

.method public final Qs()Lcom/google/android/velvet/ActionData;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1634
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->ZB()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1639
    sget-object v0, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    .line 1694
    :cond_0
    :goto_0
    return-object v0

    .line 1642
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aql()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1644
    sget-object v0, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    goto :goto_0

    .line 1647
    :cond_2
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brC:Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brC:Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    iget-object v0, v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->query:Lcom/google/android/shared/search/Query;

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brC:Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    invoke-static {v0}, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->a(Lcom/google/android/search/core/state/QueryState$BackStackEntry;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1650
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brC:Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    iget-object v0, v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsa:Lcom/google/android/velvet/ActionData;

    goto :goto_0

    .line 1653
    :cond_3
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aam()Ldcg;

    move-result-object v6

    .line 1654
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v3, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v3}, Ldcm;->ao(Lcom/google/android/shared/search/Query;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ActionData;

    .line 1655
    iget-object v3, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v6, v3}, Ldcg;->H(Lcom/google/android/shared/search/Query;)Lcom/google/android/velvet/ActionData;

    move-result-object v3

    .line 1656
    iget-object v4, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v5, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v4, v5}, Ldcm;->ag(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v4

    if-eqz v4, :cond_4

    move v5, v1

    .line 1664
    :goto_1
    const/4 v4, 0x0

    .line 1669
    if-eqz v3, :cond_5

    sget-object v7, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    if-eq v3, v7, :cond_5

    move-object v0, v3

    .line 1672
    goto :goto_0

    :cond_4
    move v5, v2

    .line 1656
    goto :goto_1

    .line 1673
    :cond_5
    iget-object v7, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v6, v7}, Ldcg;->F(Lcom/google/android/shared/search/Query;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1675
    if-eqz v3, :cond_9

    .line 1677
    if-eqz v0, :cond_7

    .line 1678
    sget-object v4, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    if-ne v3, v4, :cond_6

    :goto_2
    invoke-static {v1}, Lifv;->gY(Z)V

    goto :goto_0

    :cond_6
    move v1, v2

    goto :goto_2

    .line 1682
    :cond_7
    if-eqz v5, :cond_9

    .line 1684
    sget-object v0, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    goto :goto_0

    .line 1686
    :cond_8
    if-nez v0, :cond_0

    .line 1689
    if-eqz v5, :cond_9

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v6, v0}, Ldcg;->E(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1691
    sget-object v0, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    goto :goto_0

    :cond_9
    move-object v0, v4

    goto :goto_0
.end method

.method public final Qv()Ljyl;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1500
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v0}, Lcmq;->Qv()Ljyl;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final R(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 1381
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1382
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apG()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1383
    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->P(Lcom/google/android/shared/search/Query;)Z

    .line 1384
    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 1387
    :cond_0
    return-void
.end method

.method public final S(Lcom/google/android/shared/search/Query;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1440
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/search/core/state/QueryState;->bry:Z

    if-eqz v1, :cond_0

    .line 1441
    iput-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->bry:Z

    .line 1442
    const/4 v0, 0x1

    .line 1444
    :cond_0
    return v0
.end method

.method public final T(Lcom/google/android/shared/search/Query;)Z
    .locals 1

    .prologue
    .line 1522
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v0}, Lcmq;->Qt()Lcms;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v0}, Lcmq;->Qt()Lcms;

    move-result-object v0

    iget-object v0, v0, Lcms;->bcy:Ljyw;

    invoke-virtual {v0}, Ljyw;->bwB()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final U(Lcom/google/android/shared/search/Query;)V
    .locals 2

    .prologue
    .line 1567
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1568
    const/16 v0, 0xc4

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 1572
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v0}, Ldcm;->hasError()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1573
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1574
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    sget-object v1, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0, v1}, Ldcm;->aA(Ljava/lang/Object;)V

    .line 1576
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    const/4 v1, 0x1

    iput-boolean v1, v0, Ldcm;->bsi:Z

    .line 1577
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 1580
    :cond_1
    return-void
.end method

.method public final UF()V
    .locals 1

    .prologue
    .line 1185
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->Xo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1186
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 1188
    :cond_0
    return-void
.end method

.method public final V(Lcom/google/android/shared/search/Query;)Z
    .locals 1

    .prologue
    .line 1584
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v0, p1}, Ldcm;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-boolean v0, v0, Ldcm;->bsi:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final W(Lcom/google/android/shared/search/Query;)Z
    .locals 1

    .prologue
    .line 1594
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v0, p1}, Ldcm;->al(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->X(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final WW()Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    return-object v0
.end method

.method public final WY()J
    .locals 2

    .prologue
    .line 376
    iget-wide v0, p0, Lcom/google/android/search/core/state/QueryState;->brS:J

    return-wide v0
.end method

.method public final WZ()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 543
    iget-boolean v1, p0, Lcom/google/android/search/core/state/QueryState;->brT:Z

    if-eqz v1, :cond_0

    .line 544
    iput-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brT:Z

    .line 545
    const/4 v0, 0x1

    .line 547
    :cond_0
    return v0
.end method

.method public final X(Lcom/google/android/shared/search/Query;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1611
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v0, p1}, Ldcm;->am(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1617
    :cond_0
    :goto_0
    return v1

    .line 1614
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->Yi()Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v0

    .line 1615
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/errors/SearchError;->ajb()I

    move-result v0

    const/4 v3, 0x7

    if-ne v0, v3, :cond_3

    move v0, v1

    .line 1617
    :goto_1
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez v0, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move v0, v2

    .line 1615
    goto :goto_1
.end method

.method public final XA()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1344
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v1, v1, Ldcm;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {p0, v1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1345
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v2}, Ldcm;->ak(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1354
    :cond_0
    :goto_0
    return v0

    .line 1350
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final XB()Z
    .locals 1

    .prologue
    .line 1360
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v0, v0, Ldcm;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v0}, Ldcm;->hasError()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final XC()Z
    .locals 2

    .prologue
    .line 1391
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    .line 1392
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqo()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final XD()Lcom/google/android/shared/search/Query;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1399
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->XJ()Z

    move-result v0

    .line 1404
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aar()Lddk;

    move-result-object v0

    invoke-virtual {v0}, Lddk;->aaT()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brM:Ldcm;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ldcm;->ah(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v0, v0, Ldcm;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1407
    const/16 v0, 0xbc

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 1411
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ldcm;->af(Lcom/google/android/shared/search/Query;)V

    .line 1413
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 1416
    :goto_1
    return-object v0

    .line 1404
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1416
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final XE()Lcom/google/android/shared/search/Query;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1421
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aar()Lddk;

    move-result-object v0

    invoke-virtual {v0}, Lddk;->aaT()Z

    move-result v0

    .line 1427
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brM:Ldcm;

    iget-object v0, v0, Ldcm;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1428
    const/16 v0, 0x160

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 1431
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brM:Ldcm;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ldcm;->af(Lcom/google/android/shared/search/Query;)V

    .line 1433
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 1436
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final XF()Landroid/util/Pair;
    .locals 7
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1454
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    invoke-virtual {v0}, Lcmq;->Qr()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brA:Z

    if-eqz v0, :cond_3

    .line 1457
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    invoke-virtual {v0}, Lcmq;->OO()Ldyo;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyo;

    .line 1458
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    invoke-virtual {v1}, Lcmq;->Qi()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-static {v1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/shared/search/Query;

    .line 1461
    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqx()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1462
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v2}, Lcom/google/android/shared/search/Query;->aV(Lcom/google/android/shared/search/Query;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    .line 1466
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->ara()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1467
    const-string v2, "QueryState"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "srpQuery must have submission time: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x5

    invoke-static {v5, v2, v3, v4}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1470
    :cond_1
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    invoke-virtual {v2, v1}, Ldcn;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1471
    const/16 v2, 0xc5

    invoke-static {v2}, Lege;->hs(I)Litu;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v2

    invoke-static {v2}, Lege;->a(Litu;)V

    .line 1475
    iget-boolean v2, p0, Lcom/google/android/search/core/state/QueryState;->brK:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/search/core/state/QueryState;->brL:Z

    if-nez v2, :cond_2

    .line 1478
    const/16 v2, 0x148

    invoke-static {v2}, Lege;->ht(I)V

    .line 1481
    :cond_2
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    invoke-virtual {v2, v1}, Ldcn;->af(Lcom/google/android/shared/search/Query;)V

    .line 1484
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    invoke-virtual {v2}, Lcmq;->Qm()Z

    move-result v2

    iput-boolean v2, v1, Ldcn;->bsl:Z

    .line 1487
    iput-boolean v6, p0, Lcom/google/android/search/core/state/QueryState;->brV:Z

    .line 1488
    iput-boolean v6, p0, Lcom/google/android/search/core/state/QueryState;->brW:Z

    .line 1491
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    invoke-virtual {v1}, Lcmq;->Ql()Lcnb;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 1495
    :goto_0
    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final XG()Lcmq;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1504
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v0}, Lcmq;->Qr()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v1}, Lcmq;->Qi()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldcn;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1506
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    .line 1509
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final XH()Z
    .locals 2

    .prologue
    .line 1513
    const/4 v0, 0x0

    .line 1514
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/search/core/state/QueryState;->brD:Z

    if-nez v1, :cond_0

    .line 1515
    const/4 v0, 0x1

    .line 1517
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/search/core/state/QueryState;->brD:Z

    .line 1518
    return v0
.end method

.method public final XI()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 1535
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aao()Ldcu;

    move-result-object v0

    invoke-virtual {v0}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->amZ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1536
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    .line 1537
    iget-boolean v1, p0, Lcom/google/android/search/core/state/QueryState;->brN:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v1

    sget-object v2, Lcom/google/android/velvet/ActionData;->cRR:Lcom/google/android/velvet/ActionData;

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Ldbd;->yo()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ldbd;->VK()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1542
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brN:Z

    .line 1543
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 1546
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final XJ()Z
    .locals 2

    .prologue
    .line 1621
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aar()Lddk;

    move-result-object v0

    .line 1622
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apK()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqp()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqT()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/search/core/state/QueryState;->brG:Z

    if-eqz v1, :cond_3

    :cond_1
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/search/core/state/QueryState;->brH:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/search/core/state/QueryState;->brA:Z

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqU()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aql()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lddk;->aaU()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final XK()Lcom/google/android/velvet/ActionData;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1700
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aam()Ldcg;

    .line 1701
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ldcm;->G(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1703
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v1

    .line 1704
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v2}, Ldcm;->ao(Lcom/google/android/shared/search/Query;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ActionData;

    .line 1705
    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    if-eq v0, v1, :cond_0

    .line 1709
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final XL()Z
    .locals 3

    .prologue
    .line 1714
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v0

    .line 1715
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aam()Ldcg;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v2}, Ldcg;->H(Lcom/google/android/shared/search/Query;)Lcom/google/android/velvet/ActionData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ActionData;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ldcm;->ao(Lcom/google/android/shared/search/Query;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/google/android/velvet/ActionData;->cRR:Lcom/google/android/velvet/ActionData;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqA()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final XO()Z
    .locals 2

    .prologue
    .line 1835
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final XP()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 1843
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v0}, Lcmq;->isFailed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v2}, Ldcm;->ah(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1845
    new-instance v0, Lcom/google/android/search/shared/actions/errors/WebSearchConnectionError;

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v2}, Lcmq;->Qi()Lcom/google/android/shared/search/Query;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v3}, Lcmq;->Qo()Lefq;

    move-result-object v3

    const-string v4, "search result error"

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/search/shared/actions/errors/WebSearchConnectionError;-><init>(Lcom/google/android/shared/search/Query;Lefr;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->c(Lcom/google/android/search/shared/actions/errors/SearchError;)V

    .line 1932
    :goto_0
    return v1

    .line 1853
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v0}, Lcmq;->Qt()Lcms;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1854
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v0}, Lcmq;->Qi()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1855
    invoke-virtual {p0, v0}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1856
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->afX()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1857
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1859
    const/16 v2, 0xc0

    invoke-static {v2}, Lege;->hs(I)Litu;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v2

    invoke-static {v2}, Lege;->a(Litu;)V

    .line 1862
    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    .line 1865
    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 1881
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->XQ()Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 1883
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->Xd()Z

    move-result v2

    or-int/2addr v0, v2

    .line 1889
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->ZB()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v2}, Lcmq;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v3, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2, v3}, Ldcm;->ah(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1891
    const/16 v0, 0xc1

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 1894
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v2}, Lcmq;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldcm;->aA(Ljava/lang/Object;)V

    move v0, v1

    .line 1899
    :cond_3
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iget-object v2, v2, Ldcm;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {p0, v2}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v2}, Lcmq;->Qr()Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    .line 1903
    :cond_4
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aao()Ldcu;

    move-result-object v2

    invoke-virtual {v2}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/service/ClientConfig;->anm()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v2}, Lcmq;->Qv()Ljyl;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v3, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2, v3}, Ldcm;->ah(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1906
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    sget-object v2, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0, v2}, Ldcm;->aA(Ljava/lang/Object;)V

    move v0, v1

    .line 1911
    :cond_5
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v2}, Lcmq;->getProgress()I

    move-result v2

    .line 1912
    iget v3, p0, Lcom/google/android/search/core/state/QueryState;->brP:I

    if-eq v3, v2, :cond_6

    .line 1913
    iput v2, p0, Lcom/google/android/search/core/state/QueryState;->brP:I

    move v0, v1

    .line 1916
    :cond_6
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v2}, Lcmq;->Qj()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1917
    iget-boolean v2, p0, Lcom/google/android/search/core/state/QueryState;->brQ:Z

    if-nez v2, :cond_7

    .line 1918
    iput-boolean v1, p0, Lcom/google/android/search/core/state/QueryState;->brQ:Z

    move v0, v1

    .line 1923
    :cond_7
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v2}, Lcmq;->Qk()[I

    move-result-object v2

    .line 1924
    iget-object v3, p0, Lcom/google/android/search/core/state/QueryState;->brR:[I

    if-eq v3, v2, :cond_8

    .line 1925
    iput-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brR:[I

    move v0, v1

    .line 1929
    :cond_8
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->XN()Z

    move-result v2

    or-int/2addr v0, v2

    .line 1930
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    iget-object v3, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    if-ne v2, v3, :cond_a

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iget-boolean v2, v2, Ldcn;->bsl:Z

    if-nez v2, :cond_a

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iget-object v3, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v3}, Lcmq;->Qi()Lcom/google/android/shared/search/Query;

    move-result-object v3

    invoke-virtual {v2, v3}, Ldcn;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v2}, Lcmq;->Qm()Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    invoke-virtual {v2}, Ldcn;->reset()V

    :goto_2
    or-int/2addr v1, v0

    .line 1932
    goto/16 :goto_0

    .line 1869
    :cond_9
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->apO()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apF()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    if-eq v2, v0, :cond_2

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqc()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1873
    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    .line 1874
    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    goto/16 :goto_1

    .line 1930
    :cond_a
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final XS()Landroid/net/Uri;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2157
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2158
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lesn;->lp(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2161
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final XT()V
    .locals 3

    .prologue
    .line 2494
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    if-eqz v0, :cond_1

    .line 2495
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v1

    .line 2496
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2498
    const/4 v0, 0x0

    .line 2499
    invoke-virtual {v1}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2500
    invoke-virtual {v1}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->oY()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2502
    :cond_0
    const/16 v1, 0x9f

    invoke-static {v1}, Lege;->hs(I)Litu;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 2507
    :cond_1
    return-void
.end method

.method public final XU()Z
    .locals 2

    .prologue
    .line 2581
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ldcm;->al(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    return v0
.end method

.method public final XV()Z
    .locals 2

    .prologue
    .line 2586
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ldcm;->an(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    return v0
.end method

.method public final XW()Z
    .locals 1

    .prologue
    .line 2590
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apK()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final XX()Lcom/google/android/shared/search/Query;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2594
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iget-boolean v1, v0, Ldcm;->bsg:Z

    const/4 v2, 0x0

    iput-boolean v2, v0, Ldcm;->bsg:Z

    if-eqz v1, :cond_0

    .line 2595
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iget-object v0, v0, Ldcm;->bqv:Lcom/google/android/shared/search/Query;

    .line 2597
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final XY()Lcom/google/android/shared/search/Query;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2601
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iget-boolean v1, v0, Ldcm;->bsh:Z

    const/4 v2, 0x0

    iput-boolean v2, v0, Ldcm;->bsh:Z

    if-eqz v1, :cond_0

    .line 2602
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iget-object v0, v0, Ldcm;->bqv:Lcom/google/android/shared/search/Query;

    .line 2604
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final XZ()Ljava/lang/Boolean;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2613
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    if-eqz v1, :cond_1

    .line 2614
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    invoke-virtual {v1}, Lcmq;->Qj()Z

    move-result v1

    .line 2615
    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    invoke-virtual {v2}, Lcmq;->isComplete()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2619
    :cond_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2624
    :cond_1
    return-object v0
.end method

.method protected final Xa()V
    .locals 4

    .prologue
    .line 717
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v0

    if-nez v0, :cond_0

    .line 718
    const-string v0, "QueryState"

    const-string v1, "SRP Auth failure for search type without SRP."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 720
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apH()Lcom/google/android/shared/search/Query;

    move-result-object v1

    const/16 v2, 0xcb

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;I)Z

    .line 722
    return-void
.end method

.method public final Xb()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 792
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->Xr()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->Xs()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v1

    .line 794
    :goto_0
    if-eqz v2, :cond_0

    .line 796
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->Xv()V

    .line 798
    :cond_0
    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqA()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-static {v2}, Lgyt;->aY(Lcom/google/android/shared/search/Query;)Lgyt;

    move-result-object v2

    invoke-virtual {v2}, Lgyt;->aLJ()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 806
    :cond_1
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aaj()Ldbd;

    move-result-object v2

    invoke-virtual {v2}, Ldbd;->VT()V

    .line 808
    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->di(Z)Z

    move-result v2

    .line 809
    if-nez v2, :cond_3

    .line 815
    :goto_1
    return v0

    :cond_2
    move v2, v0

    .line 792
    goto :goto_0

    .line 814
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    move v0, v1

    .line 815
    goto :goto_1
.end method

.method public final Xc()Z
    .locals 1

    .prologue
    .line 824
    iget-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brU:Z

    return v0
.end method

.method public final Xe()Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 949
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    return-object v0
.end method

.method public final Xf()Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 953
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brv:Lcom/google/android/shared/search/Query;

    return-object v0
.end method

.method public final Xg()Z
    .locals 2

    .prologue
    .line 1031
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->getWebViewQuery()Lcom/google/android/shared/search/Query;

    move-result-object v0

    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iget-boolean v0, v0, Ldcn;->bsk:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Xh()Z
    .locals 1

    .prologue
    .line 1035
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apK()Z

    move-result v0

    return v0
.end method

.method public final Xi()V
    .locals 1

    .prologue
    .line 1043
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brK:Z

    .line 1044
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 1045
    return-void
.end method

.method public final Xj()Z
    .locals 1

    .prologue
    .line 1048
    iget-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brK:Z

    return v0
.end method

.method public final Xk()V
    .locals 1

    .prologue
    .line 1052
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brL:Z

    .line 1053
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 1054
    return-void
.end method

.method public final Xl()Z
    .locals 1

    .prologue
    .line 1057
    iget-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brL:Z

    return v0
.end method

.method protected final Xm()Z
    .locals 1

    .prologue
    .line 1090
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brH:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Xn()V
    .locals 2

    .prologue
    .line 1141
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->Xm()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1142
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apu()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1143
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1144
    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    .line 1146
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->Y(Lcom/google/android/shared/search/Query;)V

    .line 1147
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brw:Lcom/google/android/shared/search/Query;

    .line 1148
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/state/QueryState;->c(Lcmq;)V

    .line 1149
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 1151
    :cond_1
    return-void
.end method

.method public final Xq()Z
    .locals 2

    .prologue
    .line 1215
    iget-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brW:Z

    .line 1216
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/search/core/state/QueryState;->brW:Z

    .line 1217
    return v0
.end method

.method protected final Xs()Z
    .locals 2

    .prologue
    .line 1234
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    .line 1235
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apK()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqU()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ldbd;->yo()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ldbd;->VK()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->Yi()Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-virtual {v0}, Ldcy;->ZM()Lhha;

    move-result-object v0

    invoke-virtual {v0}, Lhha;->aOJ()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Xu()V
    .locals 1

    .prologue
    .line 1268
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1269
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->Xv()V

    .line 1270
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 1272
    :cond_0
    return-void
.end method

.method public final Xw()Z
    .locals 1

    .prologue
    .line 1284
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Xx()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1292
    iget-boolean v1, p0, Lcom/google/android/search/core/state/QueryState;->brO:Z

    if-nez v1, :cond_0

    .line 1293
    iput-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brO:Z

    .line 1296
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Xy()Lcom/google/android/shared/search/Query;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1305
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqt()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1306
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 1307
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/search/core/state/QueryState;->di(Z)Z

    .line 1310
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Xz()V
    .locals 2

    .prologue
    .line 1334
    iget-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brH:Z

    if-nez v0, :cond_0

    .line 1335
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brH:Z

    .line 1336
    const/16 v0, 0xb6

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 1339
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 1341
    :cond_0
    return-void
.end method

.method public final Ya()Z
    .locals 2

    .prologue
    .line 2635
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcmq;->Qi()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldcn;->aj(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Yb()Z
    .locals 1

    .prologue
    .line 2639
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iget-boolean v0, v0, Ldcn;->bsj:Z

    return v0
.end method

.method public final Yc()Z
    .locals 1

    .prologue
    .line 2644
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    invoke-virtual {v0}, Ldcn;->hasError()Z

    move-result v0

    return v0
.end method

.method public final Yd()Z
    .locals 1

    .prologue
    .line 2649
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v0}, Ldcm;->hasError()Z

    move-result v0

    return v0
.end method

.method public final Ye()Z
    .locals 2

    .prologue
    .line 2654
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ldcm;->ah(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    return v0
.end method

.method public final Yf()Z
    .locals 2

    .prologue
    .line 2659
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    invoke-virtual {v1}, Lcmq;->Qi()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldcn;->ah(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Yg()I
    .locals 1

    .prologue
    .line 2666
    iget v0, p0, Lcom/google/android/search/core/state/QueryState;->brP:I

    return v0
.end method

.method public final Yh()Z
    .locals 1

    .prologue
    .line 2680
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apO()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqo()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqp()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Yi()Lcom/google/android/search/shared/actions/errors/SearchError;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2698
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aam()Ldcg;

    move-result-object v2

    .line 2699
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2, v1}, Ldcg;->H(Lcom/google/android/shared/search/Query;)Lcom/google/android/velvet/ActionData;

    move-result-object v3

    .line 2700
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    iget-object v4, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v4}, Ldcm;->ag(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v1

    .line 2703
    if-eqz v3, :cond_1

    sget-object v4, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    if-eq v3, v4, :cond_1

    .line 2732
    :cond_0
    :goto_0
    return-object v0

    .line 2707
    :cond_1
    iget-object v4, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2, v4}, Ldcg;->F(Lcom/google/android/shared/search/Query;)Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz v3, :cond_0

    .line 2711
    :cond_2
    iget-object v4, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2, v4}, Ldcg;->E(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-eqz v2, :cond_3

    if-nez v3, :cond_3

    if-nez v1, :cond_0

    .line 2716
    :cond_3
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqe()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2723
    :cond_4
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iget-object v3, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2, v3}, Ldcn;->ag(Lcom/google/android/shared/search/Query;)Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v2

    .line 2724
    if-eqz v1, :cond_5

    move-object v0, v1

    .line 2726
    goto :goto_0

    .line 2727
    :cond_5
    if-eqz v2, :cond_0

    move-object v0, v2

    .line 2729
    goto :goto_0
.end method

.method public final Yj()Z
    .locals 1

    .prologue
    .line 2738
    iget-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brA:Z

    return v0
.end method

.method public final Yk()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2742
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    instance-of v0, v1, Ljava/util/Collection;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v2

    :goto_0
    check-cast v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->query:Lcom/google/android/shared/search/Query;

    .line 2743
    :cond_0
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 2742
    :cond_1
    instance-of v0, v1, Ljava/util/List;

    if-eqz v0, :cond_2

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_2
    instance-of v0, v1, Ljava/util/SortedSet;

    if-eqz v0, :cond_3

    check-cast v1, Ljava/util/SortedSet;

    invoke-interface {v1}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, v2}, Likr;->b(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 2743
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final Yl()Z
    .locals 1

    .prologue
    .line 2747
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final Ym()V
    .locals 1

    .prologue
    .line 2751
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brx:Z

    .line 2752
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 2753
    return-void
.end method

.method public final Yn()V
    .locals 1

    .prologue
    .line 2756
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brx:Z

    .line 2757
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 2758
    return-void
.end method

.method public final Yo()Z
    .locals 1

    .prologue
    .line 2761
    iget-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brx:Z

    return v0
.end method

.method protected final Yp()Landroid/util/LongSparseArray;
    .locals 1

    .prologue
    .line 3128
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->bru:Landroid/util/LongSparseArray;

    return-object v0
.end method

.method public final Z(Lcom/google/android/shared/search/Query;)V
    .locals 2

    .prologue
    .line 2299
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    invoke-virtual {v0, p1}, Ldcn;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2301
    const/16 v0, 0xc6

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 2304
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    invoke-virtual {v0}, Ldcn;->Ys()V

    .line 2305
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 2309
    :goto_0
    return-void

    .line 2307
    :cond_0
    const-string v0, "resultsPageStart"

    invoke-direct {p0, v0, p1}, Lcom/google/android/search/core/state/QueryState;->c(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    goto :goto_0
.end method

.method public final a(JJLandroid/os/Bundle;ILandroid/os/Bundle;)V
    .locals 5
    .param p5    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 410
    and-int/lit8 v0, p6, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 414
    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v0, p3, v2

    if-eqz v0, :cond_0

    if-nez p5, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 415
    iget-wide v2, p0, Lcom/google/android/search/core/state/QueryState;->brS:J

    cmp-long v0, p1, v2

    if-nez v0, :cond_4

    const-wide/16 v2, 0x0

    cmp-long v0, p3, v2

    if-nez v0, :cond_4

    .line 416
    if-eqz v1, :cond_1

    .line 420
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->dh(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 421
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->di(Z)Z

    .line 470
    :cond_1
    :goto_2
    return-void

    .line 410
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 414
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 429
    :cond_4
    iget-wide v0, p0, Lcom/google/android/search/core/state/QueryState;->brS:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    iget-wide v0, p0, Lcom/google/android/search/core/state/QueryState;->brS:J

    cmp-long v0, v0, p3

    if-eqz v0, :cond_5

    iget-wide v0, p0, Lcom/google/android/search/core/state/QueryState;->brS:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_5

    .line 437
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->bru:Landroid/util/LongSparseArray;

    iget-wide v2, p0, Lcom/google/android/search/core/state/QueryState;->brS:J

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aaf()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 439
    :cond_5
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->bru:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 440
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->bru:Landroid/util/LongSparseArray;

    invoke-virtual {v1, p1, p2}, Landroid/util/LongSparseArray;->remove(J)V

    .line 441
    const/4 v1, 0x1

    .line 442
    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-eqz v2, :cond_9

    .line 443
    iget-wide v2, p0, Lcom/google/android/search/core/state/QueryState;->brS:J

    cmp-long v0, v2, p3

    if-nez v0, :cond_7

    .line 445
    const/4 v0, 0x0

    .line 446
    const/4 v1, 0x0

    .line 456
    :goto_3
    iput-wide p1, p0, Lcom/google/android/search/core/state/QueryState;->brS:J

    .line 457
    if-eqz v1, :cond_a

    .line 460
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0, v1, p6}, Ldda;->d(Landroid/os/Bundle;I)V

    .line 469
    :cond_6
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    goto :goto_2

    .line 447
    :cond_7
    const-wide/16 v2, 0x1

    cmp-long v0, p3, v2

    if-nez v0, :cond_8

    .line 449
    const/4 v0, 0x0

    move v4, v1

    move-object v1, v0

    move v0, v4

    goto :goto_3

    .line 452
    :cond_8
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->bru:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p3, p4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 453
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->bru:Landroid/util/LongSparseArray;

    invoke-virtual {v2, p3, p4}, Landroid/util/LongSparseArray;->remove(J)V

    :cond_9
    move v4, v1

    move-object v1, v0

    move v0, v4

    goto :goto_3

    .line 461
    :cond_a
    if-eqz p5, :cond_b

    const-string v1, "velvet:query_state:back_stack"

    invoke-virtual {p5, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "velvet:query_state:query"

    invoke-virtual {p5, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    const/4 v1, 0x1

    :goto_5
    if-eqz v1, :cond_c

    .line 463
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0, p5, p6}, Ldda;->d(Landroid/os/Bundle;I)V

    goto :goto_4

    .line 461
    :cond_b
    const/4 v1, 0x0

    goto :goto_5

    .line 464
    :cond_c
    if-eqz v0, :cond_6

    .line 466
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0, p7, p6}, Ldda;->c(Landroid/os/Bundle;I)V

    goto :goto_4
.end method

.method public final a(JLandroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 532
    iget-wide v0, p0, Lcom/google/android/search/core/state/QueryState;->brS:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 533
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaf()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 540
    :cond_0
    :goto_0
    return-void

    .line 535
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->bru:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 536
    if-eqz v0, :cond_0

    .line 537
    invoke-virtual {p3, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected final a(Landroid/os/Bundle;I)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 486
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brw:Lcom/google/android/shared/search/Query;

    .line 487
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 488
    const-string v0, "velvet:query_state:back_stack"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v4

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v0, v4, v3

    .line 489
    iget-object v6, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    check-cast v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 488
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 491
    :cond_0
    and-int/lit8 v0, p2, 0x1

    if-ne v0, v1, :cond_3

    move v0, v1

    .line 493
    :goto_1
    if-eqz v0, :cond_4

    invoke-direct {p0, v2}, Lcom/google/android/search/core/state/QueryState;->dh(Z)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 494
    :goto_2
    invoke-direct {p0, v2}, Lcom/google/android/search/core/state/QueryState;->di(Z)Z

    .line 495
    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brv:Lcom/google/android/shared/search/Query;

    .line 496
    if-nez v0, :cond_2

    .line 498
    const-string v0, "velvet:query_state:query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apz()Lcom/google/android/shared/search/Query;

    move-result-object v0

    :cond_1
    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    .line 500
    :cond_2
    const-string v0, "velvet:query_state:is_session_solidified"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brU:Z

    .line 501
    iput-boolean v2, p0, Lcom/google/android/search/core/state/QueryState;->brV:Z

    .line 502
    iput-boolean v2, p0, Lcom/google/android/search/core/state/QueryState;->brW:Z

    .line 504
    return-void

    :cond_3
    move v0, v2

    .line 491
    goto :goto_1

    :cond_4
    move v0, v2

    .line 493
    goto :goto_2
.end method

.method public final a(Lcom/google/android/shared/search/Query;Lcmq;)V
    .locals 1

    .prologue
    .line 1752
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1753
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    if-eq p2, v0, :cond_1

    .line 1754
    invoke-virtual {p0, p2}, Lcom/google/android/search/core/state/QueryState;->c(Lcmq;)V

    .line 1755
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 1760
    :cond_0
    :goto_0
    return-void

    .line 1756
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->XP()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1757
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/errors/SearchError;)V
    .locals 2

    .prologue
    .line 975
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brM:Ldcm;

    invoke-virtual {v0, p1}, Ldcm;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 976
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brM:Ldcm;

    invoke-virtual {v0, p2}, Ldcm;->d(Lcom/google/android/search/shared/actions/errors/SearchError;)V

    .line 977
    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/errors/SearchError;->getErrorCode()I

    move-result v0

    const v1, 0x90002

    if-ne v0, v1, :cond_0

    .line 978
    const/16 v0, 0x171

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 982
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 984
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/shared/search/Query;Lehm;)V
    .locals 2

    .prologue
    .line 1003
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1004
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brM:Ldcm;

    invoke-virtual {v0, p1}, Ldcm;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1005
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brM:Ldcm;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldcm;->aA(Ljava/lang/Object;)V

    .line 1006
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, p2}, Lcom/google/android/shared/search/Query;->a(Lehm;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 1007
    iget-object v0, p0, Lddj;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aar()Lddk;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lddk;->aw(Lcom/google/android/shared/search/Query;)V

    .line 1008
    invoke-virtual {p2}, Lehm;->asN()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x16f

    .line 1011
    :goto_0
    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 1013
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 1015
    :cond_0
    return-void

    .line 1008
    :cond_1
    const/16 v0, 0x170

    goto :goto_0
.end method

.method public final a(Lcom/google/android/shared/search/Query;Ljava/lang/CharSequence;Lijj;)V
    .locals 2
    .param p3    # Lijj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1366
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1367
    const/16 v0, 0xac

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 1371
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apF()Z

    move-result v1

    invoke-virtual {v0, p2, p3, v1}, Lcom/google/android/shared/search/Query;->a(Ljava/lang/CharSequence;Lijj;Z)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1373
    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->P(Lcom/google/android/shared/search/Query;)Z

    .line 1374
    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 1375
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 1377
    :cond_0
    return-void
.end method

.method protected final a(Lddb;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2006
    .line 2008
    invoke-virtual {p1}, Lddb;->aax()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2009
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    .line 2011
    invoke-virtual {v0}, Ldbd;->yo()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2012
    invoke-virtual {v0}, Ldbd;->VL()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 2013
    if-eqz v0, :cond_1

    .line 2017
    const/16 v1, 0xdf

    invoke-static {v1}, Lege;->hs(I)Litu;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v1

    invoke-static {v1}, Lege;->a(Litu;)V

    .line 2022
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/search/core/state/QueryState;->g(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)V

    .line 2091
    :cond_0
    :goto_0
    return-void

    .line 2028
    :cond_1
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->XQ()Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 2029
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->XN()Z

    move-result v2

    or-int/2addr v2, v0

    .line 2030
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->Xs()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->Xo()Z

    move-result v0

    :goto_1
    or-int/2addr v0, v2

    .line 2031
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->Xd()Z

    move-result v2

    or-int/2addr v0, v2

    .line 2035
    :goto_2
    invoke-virtual {p1}, Lddb;->aaz()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aak()Ldcw;

    move-result-object v2

    invoke-virtual {v2}, Ldcw;->isDone()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2043
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v2}, Lcmq;->Qt()Lcms;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v2}, Lcmq;->Qt()Lcms;

    move-result-object v2

    invoke-virtual {v2}, Lcms;->QC()Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v3

    :goto_3
    if-eqz v2, :cond_4

    .line 2049
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqz()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/shared/search/Query;->v(ZZ)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/state/QueryState;->y(Lcom/google/android/shared/search/Query;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2030
    goto :goto_1

    :cond_3
    move v2, v1

    .line 2043
    goto :goto_3

    .line 2053
    :cond_4
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->XR()Z

    move-result v2

    or-int/2addr v0, v2

    .line 2056
    :cond_5
    invoke-virtual {p1}, Lddb;->aay()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2057
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aan()Ldcy;

    move-result-object v2

    .line 2058
    invoke-virtual {v2}, Ldcy;->Zx()Z

    move-result v4

    if-nez v4, :cond_6

    invoke-virtual {v2}, Ldcy;->Zy()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2062
    :cond_6
    sget-object v4, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v4, p0, Lcom/google/android/search/core/state/QueryState;->brw:Lcom/google/android/shared/search/Query;

    .line 2071
    invoke-virtual {v2}, Ldcy;->Zx()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aak()Ldcw;

    move-result-object v2

    invoke-virtual {v2}, Ldcw;->Zv()Z

    move-result v2

    if-nez v2, :cond_7

    .line 2073
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-direct {p0, v2, v3, v1}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;ZZ)Z

    move-result v2

    or-int/2addr v0, v2

    .line 2081
    :cond_7
    invoke-virtual {p1}, Lddb;->aaE()Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aau()Ldbh;

    move-result-object v2

    invoke-virtual {v2}, Ldbh;->Wi()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2082
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-direct {p0, v2, v3, v1}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;ZZ)Z

    move-result v1

    or-int/2addr v0, v1

    .line 2085
    :cond_8
    if-eqz v0, :cond_0

    .line 2089
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto/16 :goto_2
.end method

.method public final a(Letj;)V
    .locals 4

    .prologue
    const v2, 0x7f0a0728

    .line 3032
    const-string v0, "QueryState"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 3033
    const-string v0, "mQuery"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 3034
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    if-eqz v0, :cond_1

    .line 3035
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apY()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3036
    invoke-virtual {p1, v2}, Letj;->in(I)Letn;

    move-result-object v0

    invoke-virtual {v0}, Letn;->avS()Letn;

    move-result-object v0

    const v1, 0x7f0a072a

    invoke-virtual {v0, v1}, Letn;->io(I)V

    .line 3044
    :cond_0
    :goto_0
    const v0, 0x7f0a072b

    invoke-virtual {p1, v0}, Letj;->in(I)Letn;

    move-result-object v0

    invoke-virtual {v0}, Letn;->avS()Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 3048
    :cond_1
    const-string v0, "mCommittedQuery"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 3049
    const-string v0, "mLastCommittedQuery"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 3050
    const-string v0, "Solid"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/core/state/QueryState;->brU:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 3051
    const-string v0, "mCurrentClientSessionId"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/search/core/state/QueryState;->brS:J

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 3052
    const-string v0, "mPendingFollowOnQuery"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brw:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 3053
    const-string v0, "mCurrentSearchResult"

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-static {p1, v0, v1}, Lcom/google/android/search/core/state/QueryState;->a(Letj;Ljava/lang/String;Lcmq;)V

    .line 3054
    const-string v0, "mWebviewSearchResult"

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    invoke-static {p1, v0, v1}, Lcom/google/android/search/core/state/QueryState;->a(Letj;Ljava/lang/String;Lcmq;)V

    .line 3055
    const-string v0, "mWebviewState"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 3056
    const-string v0, "mWebAppLoadState"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brM:Ldcm;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 3057
    const-string v0, "mNetworkActionState"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 3058
    const-string v0, "mResolvingAdClickUrl"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/core/state/QueryState;->brx:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 3059
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v2

    .line 3060
    const-string v0, "Backstack"

    invoke-virtual {v2, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 3061
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_3

    .line 3062
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    .line 3063
    iget-object v3, v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->query:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2, v3}, Letj;->b(Leti;)V

    .line 3064
    iget-object v0, v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsa:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v2, v0}, Letj;->b(Leti;)V

    .line 3061
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 3039
    :cond_2
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3040
    invoke-virtual {p1, v2}, Letj;->in(I)Letn;

    move-result-object v0

    invoke-virtual {v0}, Letn;->avS()Letn;

    move-result-object v0

    const v1, 0x7f0a0729

    invoke-virtual {v0, v1}, Letn;->io(I)V

    goto/16 :goto_0

    .line 3066
    :cond_3
    return-void
.end method

.method public final aa(Lcom/google/android/shared/search/Query;)V
    .locals 2

    .prologue
    .line 2322
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    invoke-virtual {v0, p1}, Ldcn;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2324
    const/16 v0, 0xc7

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 2327
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldcn;->aA(Ljava/lang/Object;)V

    .line 2328
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->XR()Z

    .line 2329
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 2333
    :goto_0
    return-void

    .line 2331
    :cond_0
    const-string v0, "resultsPageEnd"

    invoke-direct {p0, v0, p1}, Lcom/google/android/search/core/state/QueryState;->c(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    goto :goto_0
.end method

.method public final ab(Lcom/google/android/shared/search/Query;)Z
    .locals 2

    .prologue
    .line 2429
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aar()Lddk;

    move-result-object v0

    invoke-virtual {v0}, Lddk;->aaM()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apI()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 2431
    :goto_0
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/search/core/state/QueryState;->d(Lcom/google/android/shared/search/Query;Z)Z

    move-result v0

    return v0

    .line 2429
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apx()Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_0
.end method

.method public final ac(Lcom/google/android/shared/search/Query;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2460
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    invoke-virtual {v0, p1}, Ldcn;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 2462
    :goto_0
    invoke-direct {p0, v1}, Lcom/google/android/search/core/state/QueryState;->di(Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2463
    const/4 v0, 0x1

    goto :goto_0

    .line 2465
    :cond_0
    if-eqz v0, :cond_1

    .line 2466
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 2469
    :cond_1
    return-void
.end method

.method public final ad(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 2474
    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2475
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 2477
    :cond_0
    return-void
.end method

.method public final ae(Lcom/google/android/shared/search/Query;)V
    .locals 4

    .prologue
    .line 2566
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqA()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2567
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoY()Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 2568
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->Xd()Z

    .line 2569
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 2571
    :cond_0
    return-void
.end method

.method protected final b(Landroid/os/Bundle;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 508
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brw:Lcom/google/android/shared/search/Query;

    .line 509
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    sget-object v1, Lgyt;->cZA:Lgyt;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/shared/search/Query;->a(Ljava/io/Serializable;Landroid/os/Bundle;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->app()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->Y(Lcom/google/android/shared/search/Query;)V

    .line 510
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brv:Lcom/google/android/shared/search/Query;

    .line 512
    invoke-virtual {p0, v3}, Lcom/google/android/search/core/state/QueryState;->c(Lcmq;)V

    .line 513
    iput-boolean v2, p0, Lcom/google/android/search/core/state/QueryState;->brD:Z

    .line 514
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->P(Lcom/google/android/shared/search/Query;)Z

    .line 515
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 516
    iput-boolean v2, p0, Lcom/google/android/search/core/state/QueryState;->brx:Z

    .line 517
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brT:Z

    .line 518
    iput-boolean v2, p0, Lcom/google/android/search/core/state/QueryState;->brU:Z

    .line 519
    iput-object v3, p0, Lcom/google/android/search/core/state/QueryState;->brC:Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    .line 524
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Kq()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brV:Z

    .line 525
    return-void
.end method

.method public final b(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/errors/SearchError;)V
    .locals 1

    .prologue
    .line 1724
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1725
    invoke-direct {p0, p2}, Lcom/google/android/search/core/state/QueryState;->c(Lcom/google/android/search/shared/actions/errors/SearchError;)V

    .line 1726
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 1728
    :cond_0
    return-void
.end method

.method final c(Lcmq;)V
    .locals 2

    .prologue
    .line 1770
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    if-eqz v0, :cond_0

    .line 1771
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brt:Ljava/util/Observer;

    invoke-virtual {v0, v1}, Lcmq;->deleteObserver(Ljava/util/Observer;)V

    .line 1773
    :cond_0
    iput-object p1, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    .line 1774
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->XN()Z

    .line 1775
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brR:[I

    .line 1776
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    if-eqz v0, :cond_1

    .line 1778
    const/16 v0, 0xbd

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 1781
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brt:Ljava/util/Observer;

    invoke-virtual {v0, v1}, Lcmq;->addObserver(Ljava/util/Observer;)V

    .line 1783
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->XP()Z

    .line 1788
    :goto_0
    return-void

    .line 1786
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/search/core/state/QueryState;->brP:I

    goto :goto_0
.end method

.method public final c(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/errors/SearchError;)V
    .locals 2

    .prologue
    .line 2336
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    invoke-virtual {v0, p1}, Ldcn;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2339
    const/16 v0, 0xca

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 2342
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    invoke-virtual {v0, p2}, Ldcn;->d(Lcom/google/android/search/shared/actions/errors/SearchError;)V

    .line 2343
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 2347
    :goto_0
    return-void

    .line 2345
    :cond_0
    const-string v0, "resultsPageError"

    invoke-direct {p0, v0, p1}, Lcom/google/android/search/core/state/QueryState;->c(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;)V
    .locals 1

    .prologue
    .line 1555
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v0}, Ldcm;->hasError()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1556
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v0, p2}, Ldcm;->aA(Ljava/lang/Object;)V

    .line 1557
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 1559
    :cond_0
    return-void
.end method

.method protected final commit()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 759
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-virtual {v0}, Ldcy;->aac()Z

    move-result v0

    if-nez v0, :cond_0

    .line 760
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoY()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoZ()Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    .line 763
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->P(Lcom/google/android/shared/search/Query;)Z

    .line 765
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apP()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 767
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->WX()V

    .line 769
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqQ()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v0

    .line 770
    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mSearchBoxLogging:Lcpd;

    iget-object v3, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    iget-object v4, p0, Lcom/google/android/search/core/state/QueryState;->mEventBus:Ldda;

    invoke-virtual {v4}, Ldda;->aao()Ldcu;

    move-result-object v4

    invoke-virtual {v4}, Ldcu;->YP()Ljava/lang/String;

    move-result-object v4

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/shared/search/SearchBoxStats;->QW()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v4, v0}, Lhgn;->aV(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v3, v0}, Lcpd;->a(Lcom/google/android/shared/search/Query;I)Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    .line 775
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->app()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->O(Lcom/google/android/shared/search/Query;)V

    .line 776
    invoke-virtual {p0, v1}, Lcom/google/android/search/core/state/QueryState;->c(Lcmq;)V

    .line 777
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->Xp()V

    .line 778
    iput-boolean v5, p0, Lcom/google/android/search/core/state/QueryState;->brN:Z

    .line 779
    iput-boolean v5, p0, Lcom/google/android/search/core/state/QueryState;->brO:Z

    .line 780
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->Xd()Z

    .line 786
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 789
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    .line 770
    goto :goto_0
.end method

.method public final d(Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;)V
    .locals 2

    .prologue
    .line 3095
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3097
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3099
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apz()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 3103
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->WX()V

    .line 3104
    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->P(Lcom/google/android/shared/search/Query;)Z

    .line 3105
    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->Y(Lcom/google/android/shared/search/Query;)V

    .line 3106
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/search/core/state/QueryState;->c(Lcmq;)V

    .line 3107
    if-eqz p2, :cond_0

    .line 3108
    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v1, v0}, Ldcm;->af(Lcom/google/android/shared/search/Query;)V

    .line 3109
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v0, p2}, Ldcm;->aA(Ljava/lang/Object;)V

    .line 3112
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brV:Z

    .line 3113
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 3115
    :cond_1
    return-void
.end method

.method public final dj(Z)V
    .locals 1

    .prologue
    .line 1315
    iget-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brG:Z

    if-eq p1, v0, :cond_0

    .line 1316
    iput-boolean p1, p0, Lcom/google/android/search/core/state/QueryState;->brG:Z

    .line 1317
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 1319
    :cond_0
    return-void
.end method

.method public final dk(Z)V
    .locals 2

    .prologue
    .line 1322
    iget-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brA:Z

    if-eq v0, p1, :cond_0

    .line 1323
    iput-boolean p1, p0, Lcom/google/android/search/core/state/QueryState;->brA:Z

    .line 1324
    iget-boolean v0, p0, Lcom/google/android/search/core/state/QueryState;->brA:Z

    if-eqz v0, :cond_1

    const/16 v0, 0xb7

    .line 1327
    :goto_0
    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 1329
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 1331
    :cond_0
    return-void

    .line 1324
    :cond_1
    const/16 v0, 0xb8

    goto :goto_0
.end method

.method public final dl(Z)V
    .locals 1

    .prologue
    .line 2312
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iget-boolean v0, v0, Ldcn;->bsj:Z

    if-eq p1, v0, :cond_0

    .line 2314
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iput-boolean p1, v0, Ldcn;->bsj:Z

    .line 2315
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 2319
    :cond_0
    return-void
.end method

.method public final e(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)V
    .locals 2

    .prologue
    .line 556
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->apJ()Lcom/google/android/shared/search/Query;

    move-result-object v0

    const/16 v1, 0xcb

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;I)Z

    move-result v0

    .line 559
    if-eqz v0, :cond_0

    .line 560
    const/16 v0, 0x174

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 564
    :cond_0
    return-void
.end method

.method public final e(Lcom/google/android/shared/search/Query;Z)V
    .locals 1

    .prologue
    .line 2488
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2489
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 2491
    :cond_0
    return-void
.end method

.method public final f(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)V
    .locals 2

    .prologue
    .line 1018
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1019
    iput-object p2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 1020
    iget-object v0, p0, Lddj;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aar()Lddk;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lddk;->aA(Lcom/google/android/shared/search/Query;)V

    .line 1021
    const/16 v0, 0x176

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 1025
    :cond_0
    return-void
.end method

.method public final g(Lcom/google/android/velvet/ActionData;)Ljava/util/List;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 3119
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brC:Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brC:Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    iget-object v0, v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsa:Lcom/google/android/velvet/ActionData;

    invoke-virtual {p1, v0}, Lcom/google/android/velvet/ActionData;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3121
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brC:Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    iget-object v0, v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsb:Ljava/util/List;

    .line 3123
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)V
    .locals 2

    .prologue
    .line 2173
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2174
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aoQ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2179
    const/16 v0, 0xe0

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 2182
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->app()Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brw:Lcom/google/android/shared/search/Query;

    .line 2184
    invoke-direct {p0}, Lcom/google/android/search/core/state/QueryState;->XR()Z

    .line 2206
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 2208
    :cond_0
    return-void

    .line 2187
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->apX()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2188
    const/16 v0, 0xe1

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 2193
    iput-object p2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 2200
    :goto_1
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    .line 2201
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brw:Lcom/google/android/shared/search/Query;

    .line 2203
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/state/QueryState;->c(Lcmq;)V

    goto :goto_0

    .line 2195
    :cond_2
    const/16 v0, 0xe2

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 2198
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->app()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/search/core/state/QueryState;->Y(Lcom/google/android/shared/search/Query;)V

    goto :goto_1
.end method

.method public final getWebViewQuery()Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 970
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    iget-object v0, v0, Ldcm;->bqv:Lcom/google/android/shared/search/Query;

    return-object v0
.end method

.method public final h(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2374
    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->arb()J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->arb()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p2, v1}, Lcom/google/android/search/core/state/QueryState;->d(Lcom/google/android/shared/search/Query;Z)Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_4

    .line 2375
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apN()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2376
    iget-object v0, p0, Lddj;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aar()Lddk;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v2}, Lddk;->aA(Lcom/google/android/shared/search/Query;)V

    .line 2377
    const/16 v0, 0x175

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 2386
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    move v0, v1

    .line 2389
    :goto_2
    return v0

    .line 2374
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqf()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_3
    if-ltz v3, :cond_2

    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    iget-object v0, v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->query:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    invoke-direct {p0, p2, v1}, Lcom/google/android/search/core/state/QueryState;->d(Lcom/google/android/shared/search/Query;Z)Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqf()Z

    move-result v0

    if-eqz v0, :cond_2

    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_3

    :cond_2
    move v0, v2

    goto :goto_0

    .line 2381
    :cond_3
    iget-object v0, p0, Lddj;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aar()Lddk;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v2}, Lddk;->ax(Lcom/google/android/shared/search/Query;)V

    .line 2382
    const/16 v0, 0x16e

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    goto :goto_1

    :cond_4
    move v0, v2

    .line 2389
    goto :goto_2
.end method

.method protected final s(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 476
    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/search/core/state/QueryState;->a(ZLjava/util/List;)V

    .line 478
    const-string v1, "velvet:query_state:query"

    iget-object v2, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 479
    const-string v1, "velvet:query_state:back_stack"

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/search/core/state/QueryState$BackStackEntry;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 481
    const-string v0, "velvet:query_state:is_session_solidified"

    iget-boolean v1, p0, Lcom/google/android/search/core/state/QueryState;->brU:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 482
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2766
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "QS[\n\t\tQ:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\t\tCQ:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\t\tAS:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brE:Ldcm;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\t\tWVS:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brI:Ldcn;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\t\tBS:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brB:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\t\tSR:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brF:Lcmq;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\t\tWSR:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState;->brJ:Lcmq;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final x(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 1155
    invoke-direct {p0, p1}, Lcom/google/android/search/core/state/QueryState;->P(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1157
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->notifyChanged()V

    .line 1159
    :cond_0
    return-void
.end method

.method public final y(Lcom/google/android/shared/search/Query;)V
    .locals 0

    .prologue
    .line 750
    invoke-direct {p0, p1}, Lcom/google/android/search/core/state/QueryState;->P(Lcom/google/android/shared/search/Query;)Z

    .line 751
    invoke-virtual {p0}, Lcom/google/android/search/core/state/QueryState;->commit()V

    .line 752
    return-void
.end method
