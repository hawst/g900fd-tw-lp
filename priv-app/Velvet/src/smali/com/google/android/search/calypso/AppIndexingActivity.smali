.class public Lcom/google/android/search/calypso/AppIndexingActivity;
.super Landroid/app/Activity;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private n(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 46
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 47
    invoke-static {v0}, Lcet;->i(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    invoke-static {p0, v0}, Lcet;->b(Landroid/content/Context;Landroid/net/Uri;)Z

    .line 50
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/calypso/AppIndexingActivity;->finish()V

    .line 51
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/android/search/calypso/AppIndexingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 32
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-direct {p0, v0}, Lcom/google/android/search/calypso/AppIndexingActivity;->n(Landroid/content/Intent;)V

    .line 35
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/search/calypso/AppIndexingActivity;->n(Landroid/content/Intent;)V

    .line 43
    return-void
.end method
