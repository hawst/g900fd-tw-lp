.class public Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;
.super Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final bnP:Ljava/util/Comparator;


# instance fields
.field private aUh:Lcxs;

.field private final bnQ:Ljava/util/Map;

.field private bnR:Landroid/preference/PreferenceCategory;

.field private bnS:Landroid/preference/SwitchPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lczi;

    invoke-direct {v0}, Lczi;-><init>()V

    sput-object v0, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->bnP:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;-><init>()V

    .line 42
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->bnQ:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method protected final Ug()I
    .locals 1

    .prologue
    .line 49
    const v0, 0x7f070018

    return v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 6

    .prologue
    .line 54
    invoke-super {p0, p1}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->onAttach(Landroid/app/Activity;)V

    .line 57
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v1

    invoke-virtual {v1}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v1

    .line 60
    if-eqz v1, :cond_0

    .line 61
    invoke-virtual {v0}, Lcfo;->DE()Lcxs;

    move-result-object v0

    .line 62
    invoke-virtual {v0, v1}, Lcxs;->v(Landroid/accounts/Account;)Lizg;

    move-result-object v0

    .line 63
    if-eqz v0, :cond_0

    iget-object v1, v0, Lizg;->dRP:Lizh;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lizg;->dRP:Lizh;

    iget-object v1, v1, Lizh;->dRQ:[Lizi;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 67
    iget-object v0, v0, Lizg;->dRP:Lizh;

    iget-object v1, v0, Lizh;->dRQ:[Lizi;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 68
    iget-object v4, p0, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->bnQ:Ljava/util/Map;

    invoke-virtual {v3}, Lizi;->bcO()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3}, Lizi;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 76
    invoke-super {p0, p1}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 78
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DE()Lcxs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->aUh:Lcxs;

    .line 81
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const v1, 0x7f0a0133

    invoke-virtual {p0, v1}, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->bnR:Landroid/preference/PreferenceCategory;

    .line 83
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->bnR:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v6}, Landroid/preference/PreferenceCategory;->setOrderingAsAdded(Z)V

    .line 86
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->aUh:Lcxs;

    invoke-virtual {v0}, Lcxs;->TH()Lcxi;

    move-result-object v0

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Lcxi;->fs(I)Lijj;

    move-result-object v0

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 88
    sget-object v1, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->bnP:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 90
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljes;

    .line 91
    new-instance v3, Lcom/google/android/search/core/preferences/WrappingSwitchPreference;

    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v3, v1}, Lcom/google/android/search/core/preferences/WrappingSwitchPreference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v6}, Lcom/google/android/search/core/preferences/WrappingSwitchPreference;->setPersistent(Z)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "api_client_"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljes;->bcO()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/google/android/search/core/preferences/WrappingSwitchPreference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljes;->bcO()I

    move-result v1

    iget-object v4, p0, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->bnQ:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->bnQ:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :goto_1
    invoke-virtual {v3, v1}, Lcom/google/android/search/core/preferences/WrappingSwitchPreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Ljes;->bjf()Z

    move-result v0

    invoke-virtual {v3, v0}, Lcom/google/android/search/core/preferences/WrappingSwitchPreference;->setChecked(Z)V

    invoke-virtual {v3, p0}, Lcom/google/android/search/core/preferences/WrappingSwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 92
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->bnR:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 91
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Client "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 95
    :cond_1
    invoke-virtual {p0, v6}, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->setHasOptionsMenu(Z)V

    .line 97
    const v0, 0x7f0a013d

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->bnS:Landroid/preference/SwitchPreference;

    .line 99
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->bnS:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 100
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->bnS:Landroid/preference/SwitchPreference;

    if-ne p1, v0, :cond_1

    .line 126
    const v0, 0x7f0a0166

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 136
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 129
    :cond_1
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "api_client_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 133
    iget-object v1, p0, Lcom/google/android/search/core/preferences/cards/InternalAPICardsSettingsFragment;->aUh:Lcxs;

    invoke-virtual {v1}, Lcxs;->TH()Lcxi;

    move-result-object v1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcxi;->s(IZ)V

    goto :goto_0
.end method
