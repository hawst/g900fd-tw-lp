.class public final Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;
.super Lbed;
.source "PG"


# static fields
.field public static final bjA:Z

.field private static final bjB:Landroid/net/Uri$Builder;

.field public static final bjC:Landroid/net/Uri;

.field public static final bjD:Landroid/net/Uri;

.field public static final bjE:Landroid/net/Uri;

.field public static final bjF:Landroid/net/Uri;

.field public static final bjG:Landroid/net/Uri;

.field private static bjH:Ljava/util/concurrent/atomic/AtomicBoolean;


# instance fields
.field private axG:Landroid/content/UriMatcher;

.field private bjI:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 89
    sget-boolean v0, Lcto;->biy:Z

    sput-boolean v0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjA:Z

    .line 95
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "com.google.android.googlequicksearchbox.icing"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 104
    sput-object v0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjB:Landroid/net/Uri$Builder;

    const-string v1, "applications"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjC:Landroid/net/Uri;

    .line 105
    sget-object v0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjB:Landroid/net/Uri$Builder;

    const-string v1, "contacts"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjD:Landroid/net/Uri;

    .line 111
    sget-object v0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjB:Landroid/net/Uri$Builder;

    const-string v1, "register_corpora"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjE:Landroid/net/Uri;

    .line 112
    sget-object v0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjB:Landroid/net/Uri$Builder;

    const-string v1, "dump"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjF:Landroid/net/Uri;

    .line 113
    sget-object v0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjB:Landroid/net/Uri$Builder;

    const-string v1, "diagnose"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjG:Landroid/net/Uri;

    .line 124
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjH:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lbed;-><init>()V

    .line 426
    return-void
.end method

.method static Ta()V
    .locals 2

    .prologue
    .line 136
    sget-object v0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjH:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 137
    return-void
.end method

.method public static Tb()Z
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x1

    return v0
.end method

.method private a(Lbem;)I
    .locals 4

    .prologue
    .line 402
    :try_start_0
    invoke-super {p0, p1}, Lbed;->h(Lbej;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 405
    :goto_0
    return v0

    .line 403
    :catch_0
    move-exception v0

    .line 404
    const-string v1, "IcingCorporaProvider"

    const-string v2, "Exception thrown from diagnoseTable"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 405
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private a(Lcjs;Lcke;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 329
    const-string v0, "IcingCorporaProvider"

    const-string v2, "Attemping to migrate usage reports."

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v4, 0x4

    invoke-static {v4, v0, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 330
    invoke-virtual {p0}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->xy()Lbdw;

    move-result-object v0

    check-cast v0, Lcun;

    invoke-virtual {v0}, Lcun;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 331
    if-nez v2, :cond_0

    .line 332
    const-string v0, "IcingCorporaProvider"

    const-string v2, "Could not get DB to migrate usage reports."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 354
    :goto_0
    return-void

    .line 336
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->xy()Lbdw;

    move-result-object v0

    check-cast v0, Lcun;

    invoke-virtual {v0, v2, p1}, Lcun;->a(Landroid/database/sqlite/SQLiteDatabase;Lcjs;)Landroid/database/Cursor;

    move-result-object v3

    .line 337
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v4, v0, [Lcom/google/android/gms/appdatasearch/UsageInfo;

    move v0, v1

    .line 340
    :goto_1
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_1

    array-length v5, v4

    if-ge v0, v5, :cond_1

    .line 341
    const/4 v5, 0x0

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 342
    const/4 v6, 0x1

    invoke-interface {v3, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 343
    new-instance v8, Lcom/google/android/gms/appdatasearch/DocumentId;

    invoke-virtual {p0}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcum;->bjr:Lcum;

    invoke-virtual {v10}, Lcum;->xF()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10, v5}, Lcom/google/android/gms/appdatasearch/DocumentId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    new-instance v5, Lcom/google/android/gms/appdatasearch/UsageInfo;

    const/4 v9, 0x0

    invoke-direct {v5, v8, v6, v7, v9}, Lcom/google/android/gms/appdatasearch/UsageInfo;-><init>(Lcom/google/android/gms/appdatasearch/DocumentId;JI)V

    aput-object v5, v4, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346
    add-int/lit8 v0, v0, 0x1

    .line 347
    goto :goto_1

    .line 349
    :cond_1
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 351
    new-instance v0, Ldif;

    invoke-virtual {p0}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3, v4}, Ldif;-><init>(Landroid/content/Context;[Lcom/google/android/gms/appdatasearch/UsageInfo;)V

    invoke-virtual {v0}, Ldif;->run()V

    .line 352
    invoke-virtual {p0}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->xy()Lbdw;

    move-result-object v0

    check-cast v0, Lcun;

    invoke-virtual {v0, v2}, Lcun;->q(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 353
    invoke-interface {p2, v1}, Lcke;->cp(Z)V

    goto :goto_0

    .line 349
    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private b([Lbem;)I
    .locals 4

    .prologue
    .line 415
    :try_start_0
    invoke-super {p0, p1}, Lbed;->a([Lbem;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 418
    :goto_0
    return v0

    .line 416
    :catch_0
    move-exception v0

    .line 417
    const-string v1, "IcingCorporaProvider"

    const-string v2, "Exception thrown from registerCorpora"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 418
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private k([Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 357
    sget-object v0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjH:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371
    :goto_0
    return v1

    .line 361
    :cond_0
    invoke-static {p1}, Lcum;->j([Ljava/lang/String;)Lijp;

    move-result-object v0

    .line 362
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 363
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcum;

    .line 364
    invoke-virtual {v0}, Lcum;->SZ()Lbem;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 369
    :cond_1
    new-array v0, v1, [Lbem;

    invoke-interface {v2, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbem;

    invoke-direct {p0, v0}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->b([Lbem;)I

    move-result v2

    .line 370
    sget-object v3, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjH:Ljava/util/concurrent/atomic/AtomicBoolean;

    if-nez v2, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v3, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    move v1, v2

    .line 371
    goto :goto_0

    :cond_2
    move v0, v1

    .line 370
    goto :goto_2
.end method


# virtual methods
.method protected final synthetic a(Lbdy;)Lbdw;
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->c(Lbdy;)Lcun;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 178
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 179
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    if-eq v0, v3, :cond_0

    .line 180
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No access to query "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for uid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->axG:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 211
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "illegal uri: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 186
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjI:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 187
    :goto_0
    if-eqz v0, :cond_2

    .line 188
    const-string v0, "IcingCorporaProvider"

    const-string v1, "Diagnose returning early - external call pending"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 189
    const/4 v0, 0x0

    .line 209
    :goto_1
    return-object v0

    :cond_1
    move v0, v2

    .line 186
    goto :goto_0

    .line 193
    :cond_2
    :try_start_0
    new-instance v1, Landroid/database/MatrixCursor;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "corpus"

    aput-object v4, v0, v3

    const/4 v3, 0x1

    const-string v4, "diagnostic"

    aput-object v4, v0, v3

    invoke-direct {v1, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 195
    invoke-static {p2}, Lcum;->j([Ljava/lang/String;)Lijp;

    move-result-object v0

    invoke-virtual {v0}, Lijp;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcum;

    .line 196
    invoke-virtual {v0}, Lcum;->SZ()Lbem;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->a(Lbem;)I

    move-result v4

    .line 197
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v0}, Lcum;->xF()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v0

    invoke-virtual {v1, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 200
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjI:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjI:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    move-object v0, v1

    .line 202
    goto :goto_1

    .line 204
    :pswitch_1
    aget-object v0, p2, v2

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 205
    new-instance v4, Ljava/io/StringWriter;

    invoke-direct {v4}, Ljava/io/StringWriter;-><init>()V

    .line 206
    new-instance v5, Ljava/io/PrintWriter;

    invoke-direct {v5, v4}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    new-array v0, v1, [Ljava/lang/Object;

    const-string v6, "InternalIcingCorporaProvider state"

    aput-object v6, v0, v2

    invoke-static {v5, v0}, Leth;->a(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    const-string v6, "  "

    invoke-virtual {p0}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->xy()Lbdw;

    move-result-object v0

    check-cast v0, Lcun;

    invoke-virtual {v0, v6, v5, v3}, Lcun;->a(Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    invoke-virtual {p0}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v6, v5}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 207
    new-instance v0, Landroid/database/MatrixCursor;

    new-array v3, v1, [Ljava/lang/String;

    const-string v5, "dump"

    aput-object v5, v3, v2

    invoke-direct {v0, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 208
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 184
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected final c(Lbdy;)Lcun;
    .locals 2

    .prologue
    .line 381
    new-instance v0, Lcun;

    invoke-virtual {p0}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcun;-><init>(Landroid/content/Context;Lbdy;)V

    return-object v0
.end method

.method public final delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 237
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 231
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 11

    .prologue
    const/4 v3, -0x1

    const/4 v10, 0x5

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 244
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 245
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    if-eq v0, v4, :cond_0

    .line 246
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No access to update "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for uid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->axG:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    .line 251
    if-ne v4, v3, :cond_1

    .line 252
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid uri: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 255
    :cond_1
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJs()Lchr;

    move-result-object v5

    .line 256
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v6

    .line 257
    invoke-virtual {v6}, Lcfo;->DD()Lcjs;

    move-result-object v0

    .line 258
    invoke-virtual {v6}, Lcfo;->BK()Lcke;

    move-result-object v7

    .line 260
    invoke-virtual {v0}, Lcjs;->MN()[Ljava/lang/String;

    move-result-object v8

    .line 262
    const/4 v9, 0x4

    if-ne v4, v9, :cond_6

    .line 263
    iget-object v3, p0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjI:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3, v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v3

    if-nez v3, :cond_2

    .line 265
    :goto_0
    if-eqz v1, :cond_3

    .line 266
    const-string v0, "IcingCorporaProvider"

    const-string v1, "Corpora registration returning early - external call pending"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v10, v0, v1, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 320
    :goto_1
    return v2

    :cond_2
    move v1, v2

    .line 263
    goto :goto_0

    .line 271
    :cond_3
    :try_start_0
    invoke-direct {p0, v8}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->k([Ljava/lang/String;)I

    move-result v1

    .line 272
    if-eqz v1, :cond_4

    .line 273
    const-string v3, "IcingCorporaProvider"

    const-string v4, "Corpora registration failed"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x5

    invoke-static {v6, v3, v4, v5}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 276
    :cond_4
    if-nez v1, :cond_5

    invoke-interface {v7}, Lcke;->NQ()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 278
    invoke-direct {p0, v0, v7}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->a(Lcjs;Lcke;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281
    :cond_5
    iget-object v0, p0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjI:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjI:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    throw v0

    .line 286
    :cond_6
    invoke-direct {p0, v8}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->k([Ljava/lang/String;)I

    move-result v0

    .line 287
    if-nez v0, :cond_8

    move v0, v1

    .line 289
    :goto_2
    if-nez v0, :cond_7

    .line 290
    const-string v0, "IcingCorporaProvider"

    const-string v7, "Corpora registration failed"

    new-array v8, v2, [Ljava/lang/Object;

    invoke-static {v10, v0, v7, v8}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 293
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->xy()Lbdw;

    move-result-object v0

    check-cast v0, Lcun;

    .line 294
    invoke-virtual {v0}, Lcun;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 296
    if-nez v7, :cond_9

    move v2, v3

    .line 297
    goto :goto_1

    :cond_8
    move v0, v2

    .line 287
    goto :goto_2

    .line 299
    :cond_9
    packed-switch v4, :pswitch_data_0

    .line 322
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unhandled match for uri "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 301
    :pswitch_1
    invoke-virtual {v0, v7, p3}, Lcun;->g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 303
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0, v7, v3, p3, p4}, Lcun;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/res/Resources;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 307
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 308
    if-lez v3, :cond_b

    move v0, v1

    .line 309
    :goto_3
    invoke-static {v5, v6, v8, v9, v0}, Lcnn;->a(Lchr;Lcfo;JZ)Lcnm;

    move-result-object v0

    .line 311
    invoke-virtual {v0}, Lcnm;->QG()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-virtual {v0}, Lcnm;->QH()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 312
    invoke-virtual {v6}, Lcfo;->DO()Lgpp;

    move-result-object v0

    const-string v1, "log_contacts_to_clearcut_after_hash_check"

    invoke-virtual {v6}, Lcfo;->BL()Lchk;

    move-result-object v2

    invoke-virtual {v2}, Lchk;->FP()I

    move-result v2

    int-to-long v4, v2

    invoke-interface {v0, v1, v4, v5}, Lgpp;->r(Ljava/lang/String;J)V

    :cond_a
    :goto_4
    move v2, v3

    .line 320
    goto/16 :goto_1

    :cond_b
    move v0, v2

    .line 308
    goto :goto_3

    .line 315
    :cond_c
    invoke-virtual {v0}, Lcnm;->QG()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 316
    invoke-virtual {v6}, Lcfo;->DO()Lgpp;

    move-result-object v0

    const-string v1, "log_contacts_to_clearcut_unconditionally"

    invoke-virtual {v6}, Lcfo;->BL()Lchk;

    move-result-object v2

    invoke-virtual {v2}, Lchk;->FP()I

    move-result v2

    int-to-long v4, v2

    invoke-interface {v0, v1, v4, v5}, Lgpp;->r(Ljava/lang/String;J)V

    goto :goto_4

    .line 299
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected final xA()Ljava/lang/String;
    .locals 1

    .prologue
    .line 376
    const-string v0, "com.google.android.googlequicksearchbox.icing"

    return-object v0
.end method

.method public final xB()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 146
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->axG:Landroid/content/UriMatcher;

    .line 147
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjI:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 148
    iget-object v0, p0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->axG:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.googlequicksearchbox.icing"

    const-string v2, "applications"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 149
    iget-object v0, p0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->axG:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.googlequicksearchbox.icing"

    const-string v2, "contacts"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 150
    iget-object v0, p0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->axG:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.googlequicksearchbox.icing"

    const-string v2, "register_corpora"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 151
    iget-object v0, p0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->axG:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.googlequicksearchbox.icing"

    const-string v2, "dump"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 152
    iget-object v0, p0, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->axG:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.googlequicksearchbox.icing"

    const-string v2, "diagnose"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 153
    return v4
.end method

.method public final xC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final xE()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    invoke-static {}, Lcuo;->xE()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final xx()Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 391
    new-instance v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    const v1, 0x7f0a0666

    const v2, 0x7f0a0667

    const v3, 0x7f030003

    const-string v4, "android.intent.action.MAIN"

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected final xz()Z
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    return v0
.end method
