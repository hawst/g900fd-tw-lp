.class public Lcom/google/android/search/shared/actions/SelfNoteAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bMT:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bMU:Ljava/util/concurrent/Future;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bMV:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    new-instance v0, Ldva;

    invoke-direct {v0}, Ldva;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/SelfNoteAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/shared/actions/SelfNoteAction;->bMT:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/SelfNoteAction;->bMV:Z

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SelfNoteAction;->bMU:Ljava/util/concurrent/Future;

    .line 41
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/concurrent/Future;Z)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/Future;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/search/shared/actions/SelfNoteAction;->bMT:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/google/android/search/shared/actions/SelfNoteAction;->bMU:Ljava/util/concurrent/Future;

    .line 33
    iput-boolean p3, p0, Lcom/google/android/search/shared/actions/SelfNoteAction;->bMV:Z

    .line 34
    return-void
.end method


# virtual methods
.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/SelfNoteAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final agb()Ldtw;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Ldtw;->bLn:Ldtw;

    return-object v0
.end method

.method public final aik()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SelfNoteAction;->bMT:Ljava/lang/String;

    return-object v0
.end method

.method public final ail()Ljava/util/concurrent/Future;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SelfNoteAction;->bMU:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SelfNoteAction;->bMT:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 69
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SelfNoteAction;->bMT:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 70
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/SelfNoteAction;->bMV:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 71
    return-void

    .line 70
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
