.class public Lcom/google/android/search/shared/actions/MediaControlAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bMm:I

.field private final bMn:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 151
    new-instance v0, Lduk;

    invoke-direct {v0}, Lduk;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/MediaControlAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 41
    iput p1, p0, Lcom/google/android/search/shared/actions/MediaControlAction;->bMm:I

    .line 42
    iput p2, p0, Lcom/google/android/search/shared/actions/MediaControlAction;->bMn:I

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/MediaControlAction;->bMm:I

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/MediaControlAction;->bMn:I

    .line 50
    return-void
.end method

.method private static actionToString(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 105
    packed-switch p0, :pswitch_data_0

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "INVALID_ACTION: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 107
    :pswitch_0
    const-string v0, "KEYCODE_MEDIA_PLAY"

    goto :goto_0

    .line 109
    :pswitch_1
    const-string v0, "KEYCODE_MEDIA_STOP"

    goto :goto_0

    .line 111
    :pswitch_2
    const-string v0, "KEYCODE_MEDIA_NEXT"

    goto :goto_0

    .line 113
    :pswitch_3
    const-string v0, "KEYCODE_MEDIA_PREVIOUS"

    goto :goto_0

    .line 115
    :pswitch_4
    const-string v0, "KEYCODE_MEDIA_PAUSE"

    goto :goto_0

    .line 117
    :pswitch_5
    const-string v0, "KEYCODE_MEDIA_PLAY"

    goto :goto_0

    .line 119
    :pswitch_6
    const-string v0, "COMMAND_COMMAND_UNKNOWN"

    goto :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 128
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/MediaControlAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Letj;)V
    .locals 4

    .prologue
    .line 145
    const-string v0, "MediaControlAction"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 146
    const-string v0, "action"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/google/android/search/shared/actions/MediaControlAction;->bMm:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/search/shared/actions/MediaControlAction;->bMm:I

    invoke-static {v2}, Lcom/google/android/search/shared/actions/MediaControlAction;->actionToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 148
    const-string v0, "media type"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget v1, p0, Lcom/google/android/search/shared/actions/MediaControlAction;->bMn:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 149
    return-void
.end method

.method public final ahB()I
    .locals 2

    .prologue
    const/16 v0, 0x7e

    .line 81
    iget v1, p0, Lcom/google/android/search/shared/actions/MediaControlAction;->bMm:I

    packed-switch v1, :pswitch_data_0

    .line 97
    const/4 v0, -0x1

    :goto_0
    :pswitch_0
    return v0

    .line 85
    :pswitch_1
    const/16 v0, 0x56

    goto :goto_0

    .line 87
    :pswitch_2
    const/16 v0, 0x57

    goto :goto_0

    .line 89
    :pswitch_3
    const/16 v0, 0x58

    goto :goto_0

    .line 91
    :pswitch_4
    const/16 v0, 0x7f

    goto :goto_0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/google/android/search/shared/actions/MediaControlAction;->bMn:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/search/shared/actions/MediaControlAction;->bMm:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MediaControlAction: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 134
    const-string v1, "Action = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    iget v1, p0, Lcom/google/android/search/shared/actions/MediaControlAction;->bMm:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 136
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    iget v1, p0, Lcom/google/android/search/shared/actions/MediaControlAction;->bMm:I

    invoke-static {v1}, Lcom/google/android/search/shared/actions/MediaControlAction;->actionToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    iget v1, p0, Lcom/google/android/search/shared/actions/MediaControlAction;->bMn:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 140
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 54
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 55
    iget v0, p0, Lcom/google/android/search/shared/actions/MediaControlAction;->bMm:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 56
    iget v0, p0, Lcom/google/android/search/shared/actions/MediaControlAction;->bMn:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 57
    return-void
.end method
