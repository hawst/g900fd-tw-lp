.class public Lcom/google/android/search/searchplate/ClearOrVoiceButton;
.super Landroid/widget/ImageView;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private bGg:Landroid/graphics/drawable/Drawable;

.field private bGh:Landroid/graphics/drawable/Drawable;

.field private bGi:Landroid/graphics/drawable/Drawable;

.field private bGj:Landroid/graphics/drawable/Drawable;

.field private bGk:F

.field private bGl:F

.field private bGm:Landroid/animation/ValueAnimator;

.field private bGn:F

.field private bGo:F

.field private bGp:Z

.field private bGq:Landroid/view/View$OnClickListener;

.field private bGr:Landroid/view/View$OnClickListener;

.field private bGs:Z

.field private bGt:Z

.field private nR:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    iput-boolean v2, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGt:Z

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 53
    const v1, 0x7f020105

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGg:Landroid/graphics/drawable/Drawable;

    .line 54
    const v1, 0x7f02018f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGi:Landroid/graphics/drawable/Drawable;

    .line 55
    const v1, 0x7f02019d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGj:Landroid/graphics/drawable/Drawable;

    .line 57
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGj:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGh:Landroid/graphics/drawable/Drawable;

    .line 58
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGh:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0041

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGs:Z

    .line 63
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGk:F

    .line 65
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGl:F

    .line 68
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->nR:Landroid/graphics/Paint;

    .line 69
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->nR:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->nR:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFlags(I)V

    .line 71
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->nR:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0018

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 72
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->nR:Landroid/graphics/Paint;

    const/16 v1, 0x4d

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 74
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGm:Landroid/animation/ValueAnimator;

    .line 75
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGm:Landroid/animation/ValueAnimator;

    sget-object v1, Ldqs;->bGc:Ldqs;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 76
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGm:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 77
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGm:Landroid/animation/ValueAnimator;

    new-instance v1, Ldqu;

    invoke-direct {v1, p0}, Ldqu;-><init>(Lcom/google/android/search/searchplate/ClearOrVoiceButton;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 83
    invoke-virtual {p0, p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    return-void

    .line 74
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final F(F)V
    .locals 3

    .prologue
    const/high16 v1, 0x44160000    # 600.0f

    .line 170
    const/high16 v0, 0x44480000    # 800.0f

    mul-float/2addr v0, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    div-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGk:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGn:F

    .line 173
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGn:F

    iget v2, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGl:F

    mul-float/2addr v2, p1

    sub-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGo:F

    .line 175
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->nR:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGo:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 176
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->invalidate()V

    .line 177
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGq:Landroid/view/View$OnClickListener;

    .line 121
    return-void
.end method

.method public final aeR()V
    .locals 2

    .prologue
    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGt:Z

    .line 97
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 98
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a002c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 99
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGs:Z

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->dM(Z)V

    .line 100
    return-void
.end method

.method public final b(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGr:Landroid/view/View$OnClickListener;

    .line 125
    return-void
.end method

.method public final dM(Z)V
    .locals 2

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGs:Z

    .line 104
    if-eqz p1, :cond_1

    .line 105
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGm:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGm:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 109
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a002c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 117
    :goto_0
    return-void

    .line 110
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGt:Z

    if-eqz v0, :cond_2

    .line 111
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGh:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 112
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0041

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 115
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGs:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGq:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_1

    .line 89
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGq:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGr:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGr:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 153
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 154
    iget v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGo:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 157
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getPaddingLeft()I

    move-result v0

    .line 158
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getPaddingTop()I

    move-result v1

    .line 159
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getWidth()I

    move-result v2

    sub-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getHeight()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGn:F

    iget-object v3, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->nR:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 163
    :cond_0
    return-void
.end method

.method public final q(ZZ)V
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGp:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGp:Z

    iget-boolean v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGp:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGi:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGh:Landroid/graphics/drawable/Drawable;

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGs:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGh:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 146
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGs:Z

    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    .line 147
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGm:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 149
    :cond_1
    return-void

    .line 143
    :cond_2
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGj:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->bGh:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method
