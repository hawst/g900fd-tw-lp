.class public Lcom/google/android/search/core/google/GoogleSuggestionProvider;
.super Landroid/content/ContentProvider;
.source "PG"


# instance fields
.field private bde:Lcnt;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 63
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/search/core/google/GoogleSuggestionProvider;->bde:Lcnt;

    const-string v0, "vnd.android.cursor.dir/vnd.android.search.suggest"

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 52
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onCreate()Z
    .locals 3

    .prologue
    .line 34
    new-instance v0, Lcnt;

    invoke-virtual {p0}, Lcom/google/android/search/core/google/GoogleSuggestionProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcnt;-><init>(Landroid/content/Context;Lgql;)V

    iput-object v0, p0, Lcom/google/android/search/core/google/GoogleSuggestionProvider;->bde:Lcnt;

    .line 36
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/search/core/google/GoogleSuggestionProvider;->bde:Lcnt;

    invoke-virtual {v0, p1}, Lcnt;->k(Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 58
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
