.class public Lcom/google/android/search/shared/contact/RelationshipStatus;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private aYM:Lcom/google/android/search/shared/contact/Relationship;

.field private bSw:Z

.field private bSx:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 169
    new-instance v0, Leaj;

    invoke-direct {v0}, Leaj;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/contact/RelationshipStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSw:Z

    .line 27
    iput-boolean v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSx:Z

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-class v0, Lcom/google/android/search/shared/contact/Relationship;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Relationship;

    iput-object v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSw:Z

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSx:Z

    .line 40
    return-void

    :cond_0
    move v0, v2

    .line 38
    goto :goto_0

    :cond_1
    move v1, v2

    .line 39
    goto :goto_1
.end method

.method public constructor <init>(Lcom/google/android/search/shared/contact/RelationshipStatus;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iget-object v0, p1, Lcom/google/android/search/shared/contact/RelationshipStatus;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    iput-object v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    .line 32
    iget-boolean v0, p1, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSw:Z

    iput-boolean v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSw:Z

    .line 33
    iget-boolean v0, p1, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSx:Z

    iput-boolean v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSx:Z

    .line 34
    return-void
.end method

.method public static a(Lcom/google/android/search/shared/contact/RelationshipStatus;Lcom/google/android/search/shared/contact/RelationshipStatus;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 193
    if-eqz p0, :cond_0

    if-nez p1, :cond_5

    .line 194
    :cond_0
    if-nez p0, :cond_2

    move v3, v0

    :goto_0
    if-nez p1, :cond_3

    move v2, v0

    :goto_1
    if-ne v3, v2, :cond_4

    .line 200
    :cond_1
    :goto_2
    return v0

    :cond_2
    move v3, v1

    .line 194
    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    .line 196
    :cond_5
    iget-object v2, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    if-eqz v2, :cond_6

    iget-object v2, p1, Lcom/google/android/search/shared/contact/RelationshipStatus;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    if-nez v2, :cond_7

    .line 198
    :cond_6
    iget-object v2, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    iget-object v3, p1, Lcom/google/android/search/shared/contact/RelationshipStatus;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    if-eq v2, v3, :cond_1

    move v0, v1

    goto :goto_2

    .line 200
    :cond_7
    iget-object v2, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    invoke-virtual {v2, p1}, Lcom/google/android/search/shared/contact/Relationship;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-boolean v2, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSw:Z

    iget-boolean v3, p1, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSw:Z

    if-ne v2, v3, :cond_8

    iget-boolean v2, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSx:Z

    iget-boolean v3, p1, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSx:Z

    if-eq v2, v3, :cond_1

    :cond_8
    move v0, v1

    goto :goto_2
.end method

.method private a([Ljrg;[Ljrg;)[Ljrg;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Relationship;->amr()Z

    move-result v0

    if-eqz v0, :cond_0

    array-length v0, p1

    if-eqz v0, :cond_0

    .line 153
    :goto_0
    return-object p1

    :cond_0
    move-object p1, p2

    goto :goto_0
.end method


# virtual methods
.method public final a(ZZLjqj;)[Ljrg;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amv()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    if-eqz p1, :cond_0

    .line 129
    iget-object v0, p3, Ljqj;->eyN:[Ljrg;

    iget-object v1, p3, Ljqj;->eyM:[Ljrg;

    invoke-direct {p0, v0, v1}, Lcom/google/android/search/shared/contact/RelationshipStatus;->a([Ljrg;[Ljrg;)[Ljrg;

    move-result-object v0

    .line 140
    :goto_0
    return-object v0

    .line 132
    :cond_0
    if-eqz p2, :cond_1

    .line 133
    iget-object v0, p3, Ljqj;->eyP:[Ljrg;

    iget-object v1, p3, Ljqj;->eyO:[Ljrg;

    invoke-direct {p0, v0, v1}, Lcom/google/android/search/shared/contact/RelationshipStatus;->a([Ljrg;[Ljrg;)[Ljrg;

    move-result-object v0

    goto :goto_0

    .line 140
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ama()Z
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final amt()Lcom/google/android/search/shared/contact/Relationship;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    return-object v0
.end method

.method public final amu()V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSw:Z

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSx:Z

    .line 73
    return-void
.end method

.method public final amv()Z
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSw:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSx:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final amw()V
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSw:Z

    if-eqz v0, :cond_0

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSx:Z

    .line 94
    :cond_0
    return-void
.end method

.method public final amx()Z
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSw:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSx:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x0

    return v0
.end method

.method public final f(Lcom/google/android/search/shared/contact/Relationship;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    .line 48
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[ Relationship = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : ShouldProcessRelationship = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSw:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : IsRelationshipOperationComplete = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSx:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 164
    iget-object v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 165
    iget-boolean v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSw:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 166
    iget-boolean v0, p0, Lcom/google/android/search/shared/contact/RelationshipStatus;->bSx:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 167
    return-void

    :cond_0
    move v0, v2

    .line 165
    goto :goto_0

    :cond_1
    move v1, v2

    .line 166
    goto :goto_1
.end method
