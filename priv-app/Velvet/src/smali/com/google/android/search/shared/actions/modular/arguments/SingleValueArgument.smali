.class public abstract Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;
.super Lcom/google/android/search/shared/actions/modular/arguments/Argument;
.source "PG"


# instance fields
.field public wi:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;Ljava/lang/Object;)V

    .line 30
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V

    .line 34
    invoke-virtual {p0, p2}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->setValue(Ljava/lang/Object;)V

    .line 35
    return-void
.end method

.method protected constructor <init>(Ljqg;Ljava/lang/Object;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;-><init>(Ljqg;)V

    .line 25
    invoke-virtual {p0, p2}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->setValue(Ljava/lang/Object;)V

    .line 26
    return-void
.end method


# virtual methods
.method public Pd()Z
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected f(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z
    .locals 3
    .param p1    # Lcom/google/android/search/shared/actions/modular/arguments/Argument;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 75
    instance-of v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;

    if-nez v0, :cond_0

    move v0, v1

    .line 79
    :goto_0
    return v0

    :cond_0
    move-object v0, p1

    .line 78
    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;

    .line 79
    invoke-super {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->f(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    iget-object v0, v0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    invoke-static {v2, v0}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final getValue()Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    return-object v0
.end method

.method protected final h(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z
    .locals 4
    .param p1    # Lcom/google/android/search/shared/actions/modular/arguments/Argument;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 87
    instance-of v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;

    if-nez v0, :cond_0

    move v0, v2

    .line 97
    :goto_0
    return v0

    .line 90
    :cond_0
    check-cast p1, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;

    .line 91
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    .line 92
    iget-object v1, p1, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    .line 93
    instance-of v3, v1, Ljsr;

    if-nez v3, :cond_1

    if-nez v1, :cond_2

    .line 95
    :cond_1
    check-cast v0, Ljsr;

    check-cast v1, Ljsr;

    invoke-static {v0, v1}, Leqh;->c(Ljsr;Ljsr;)Z

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 97
    goto :goto_0
.end method

.method public setValue(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->Pd()Z

    move-result v1

    .line 42
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->ajH()I

    move-result v2

    .line 43
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    invoke-static {v0, p1}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 44
    :goto_0
    iput-object p1, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    .line 45
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->Pd()Z

    move-result v3

    .line 46
    if-eq v1, v3, :cond_2

    .line 47
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->ajJ()V

    .line 53
    :cond_0
    :goto_1
    return-void

    .line 43
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 48
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->ajH()I

    move-result v1

    if-eq v2, v1, :cond_3

    .line 49
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->ajK()V

    goto :goto_1

    .line 50
    :cond_3
    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->ajL()V

    goto :goto_1
.end method
