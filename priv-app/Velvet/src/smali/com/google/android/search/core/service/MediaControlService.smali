.class public Lcom/google/android/search/core/service/MediaControlService;
.super Landroid/service/notification/NotificationListenerService;
.source "PG"

# interfaces
.implements Landroid/media/RemoteController$OnClientUpdateListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field private aOj:Landroid/media/RemoteController;

.field private bqa:Landroid/content/BroadcastReceiver;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/service/notification/NotificationListenerService;-><init>()V

    .line 85
    new-instance v0, Ldat;

    invoke-direct {v0, p0}, Ldat;-><init>(Lcom/google/android/search/core/service/MediaControlService;)V

    iput-object v0, p0, Lcom/google/android/search/core/service/MediaControlService;->bqa:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/search/core/service/MediaControlService;I)V
    .locals 3

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/search/core/service/MediaControlService;->aOj:Landroid/media/RemoteController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/service/MediaControlService;->aOj:Landroid/media/RemoteController;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    iget-object v0, p0, Lcom/google/android/search/core/service/MediaControlService;->aOj:Landroid/media/RemoteController;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/media/RemoteController;->sendMediaKeyEvent(Landroid/view/KeyEvent;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public onClientChange(Z)V
    .locals 2

    .prologue
    .line 140
    new-instance v0, Landroid/content/Intent;

    const-string v1, "onClientChange"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 141
    const-string v1, "clearing"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 142
    invoke-static {p0}, Lcn;->a(Landroid/content/Context;)Lcn;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcn;->a(Landroid/content/Intent;)Z

    .line 143
    return-void
.end method

.method public onClientMetadataUpdate(Landroid/media/RemoteController$MetadataEditor;)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 169
    new-instance v0, Landroid/content/Intent;

    const-string v1, "onClientMetadataUpdate"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 170
    new-instance v1, Lcom/google/android/search/core/service/SongData;

    const/16 v2, 0xd

    invoke-virtual {p1, v2, v6}, Landroid/media/RemoteController$MetadataEditor;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x7

    invoke-virtual {p1, v3, v6}, Landroid/media/RemoteController$MetadataEditor;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {p1, v4, v6}, Landroid/media/RemoteController$MetadataEditor;->getString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    invoke-virtual {p1, v5, v6}, Landroid/media/RemoteController$MetadataEditor;->getBitmap(ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v5

    const/16 v6, 0x9

    const-wide/16 v8, -0x1

    invoke-virtual {p1, v6, v8, v9}, Landroid/media/RemoteController$MetadataEditor;->getLong(IJ)J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/search/core/service/SongData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;J)V

    .line 176
    const-string v2, "metaData"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 177
    invoke-static {p0}, Lcn;->a(Landroid/content/Context;)Lcn;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcn;->a(Landroid/content/Intent;)Z

    .line 178
    return-void
.end method

.method public onClientPlaybackStateUpdate(I)V
    .locals 2

    .prologue
    .line 147
    new-instance v0, Landroid/content/Intent;

    const-string v1, "onClientPlaybackStateUpdate"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 148
    const-string v1, "state"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 149
    invoke-static {p0}, Lcn;->a(Landroid/content/Context;)Lcn;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcn;->a(Landroid/content/Intent;)Z

    .line 150
    return-void
.end method

.method public onClientPlaybackStateUpdate(IJJF)V
    .locals 2

    .prologue
    .line 155
    new-instance v0, Landroid/content/Intent;

    const-string v1, "onClientPlaybackStateUpdateKnown"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 156
    const-string v1, "state"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 157
    invoke-static {p0}, Lcn;->a(Landroid/content/Context;)Lcn;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcn;->a(Landroid/content/Intent;)Z

    .line 158
    return-void
.end method

.method public onClientTransportControlUpdate(I)V
    .locals 2

    .prologue
    .line 162
    new-instance v0, Landroid/content/Intent;

    const-string v1, "onClientTransportControlUpdate"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 163
    const-string v1, "state"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 164
    invoke-static {p0}, Lcn;->a(Landroid/content/Context;)Lcn;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcn;->a(Landroid/content/Intent;)Z

    .line 165
    return-void
.end method

.method public onCreate()V
    .locals 5

    .prologue
    const/16 v4, 0x280

    .line 94
    invoke-virtual {p0}, Lcom/google/android/search/core/service/MediaControlService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/service/MediaControlService;->mContext:Landroid/content/Context;

    .line 95
    new-instance v0, Landroid/media/RemoteController;

    iget-object v1, p0, Lcom/google/android/search/core/service/MediaControlService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Landroid/media/RemoteController;-><init>(Landroid/content/Context;Landroid/media/RemoteController$OnClientUpdateListener;)V

    iput-object v0, p0, Lcom/google/android/search/core/service/MediaControlService;->aOj:Landroid/media/RemoteController;

    .line 96
    iget-object v0, p0, Lcom/google/android/search/core/service/MediaControlService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcn;->a(Landroid/content/Context;)Lcn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/service/MediaControlService;->bqa:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "mediaKey"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcn;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/search/core/service/MediaControlService;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/search/core/service/MediaControlService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v0, p0, Lcom/google/android/search/core/service/MediaControlService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/search/core/service/MediaControlService;->aOj:Landroid/media/RemoteController;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->registerRemoteController(Landroid/media/RemoteController;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/service/MediaControlService;->aOj:Landroid/media/RemoteController;

    invoke-virtual {v0, v4, v4}, Landroid/media/RemoteController;->setArtworkConfiguration(II)Z

    .line 99
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/search/core/service/MediaControlService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/search/core/service/MediaControlService;->aOj:Landroid/media/RemoteController;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterRemoteController(Landroid/media/RemoteController;)V

    .line 104
    return-void
.end method

.method public onNotificationPosted(Landroid/service/notification/StatusBarNotification;)V
    .locals 0

    .prologue
    .line 183
    return-void
.end method

.method public onNotificationRemoved(Landroid/service/notification/StatusBarNotification;)V
    .locals 0

    .prologue
    .line 188
    return-void
.end method
