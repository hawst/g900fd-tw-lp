.class public Lcom/google/android/search/shared/actions/utils/CardDecision;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

.field public static final bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;


# instance fields
.field private final bOm:I

.field private final bOn:I

.field private final bQi:Ljava/lang/String;

.field private final bQj:Ljava/lang/String;

.field private final bQk:Z

.field private final bQl:Z

.field private final bQm:Z

.field private final bQn:Z

.field private final bQo:Z

.field private final bQp:J

.field private final bQq:Z


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const-wide/16 v10, 0x0

    const/4 v13, -0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 31
    new-instance v1, Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-object v3, v2

    move v5, v4

    move v6, v4

    move v7, v4

    move v8, v4

    move v9, v4

    move v12, v4

    invoke-direct/range {v1 .. v13}, Lcom/google/android/search/shared/actions/utils/CardDecision;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZZZZJII)V

    sput-object v1, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

    .line 38
    new-instance v1, Lcom/google/android/search/shared/actions/utils/CardDecision;

    const/4 v7, 0x1

    move-object v3, v2

    move v5, v4

    move v6, v4

    move v8, v4

    move v9, v4

    move v12, v4

    invoke-direct/range {v1 .. v13}, Lcom/google/android/search/shared/actions/utils/CardDecision;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZZZZJII)V

    sput-object v1, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    .line 389
    new-instance v0, Ldxz;

    invoke-direct {v0}, Ldxz;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 14

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v4

    if-ne v4, v0, :cond_0

    move v4, v0

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v5

    if-ne v5, v0, :cond_1

    move v5, v0

    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v6

    if-ne v6, v0, :cond_2

    move v6, v0

    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v7

    if-ne v7, v0, :cond_3

    move v7, v0

    :goto_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v8

    if-ne v8, v0, :cond_4

    move v8, v0

    :goto_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v9

    if-ne v9, v0, :cond_5

    move v9, v0

    :goto_5
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v10

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v12

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v13

    move-object v1, p0

    invoke-direct/range {v1 .. v13}, Lcom/google/android/search/shared/actions/utils/CardDecision;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZZZZJII)V

    .line 102
    return-void

    :cond_0
    move v4, v1

    .line 91
    goto :goto_0

    :cond_1
    move v5, v1

    goto :goto_1

    :cond_2
    move v6, v1

    goto :goto_2

    :cond_3
    move v7, v1

    goto :goto_3

    :cond_4
    move v8, v1

    goto :goto_4

    :cond_5
    move v9, v1

    goto :goto_5
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZZZZZJII)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQi:Ljava/lang/String;

    .line 78
    iput-object p2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQj:Ljava/lang/String;

    .line 79
    iput-boolean p3, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQk:Z

    .line 80
    iput-boolean p4, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQl:Z

    .line 81
    iput-boolean p5, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQm:Z

    .line 82
    iput-boolean p6, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQq:Z

    .line 83
    iput-boolean p7, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQn:Z

    .line 84
    iput-boolean p8, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQo:Z

    .line 85
    iput-wide p9, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQp:J

    .line 86
    iput p11, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bOm:I

    .line 87
    iput p12, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bOn:I

    .line 88
    return-void
.end method

.method public static alo()Ldya;
    .locals 1

    .prologue
    .line 172
    new-instance v0, Ldya;

    invoke-direct {v0}, Ldya;-><init>()V

    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;I)Ldya;
    .locals 1

    .prologue
    .line 186
    new-instance v0, Ldya;

    invoke-direct {v0}, Ldya;-><init>()V

    invoke-virtual {v0, p0, p2}, Ldya;->t(Ljava/lang/String;I)Ldya;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ldya;->u(Ljava/lang/String;I)Ldya;

    move-result-object v0

    return-object v0
.end method

.method public static t(Ljava/lang/String;I)Ldya;
    .locals 1

    .prologue
    .line 179
    new-instance v0, Ldya;

    invoke-direct {v0}, Ldya;-><init>()V

    invoke-virtual {v0, p0, p1}, Ldya;->t(Ljava/lang/String;I)Ldya;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final Zv()Z
    .locals 1

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQl:Z

    return v0
.end method

.method public final alc()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQi:Ljava/lang/String;

    return-object v0
.end method

.method public final ald()Z
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQi:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ale()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQj:Ljava/lang/String;

    return-object v0
.end method

.method public final alf()Z
    .locals 1

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQk:Z

    return v0
.end method

.method public final alg()Z
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQn:Z

    return v0
.end method

.method public final alh()Z
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQo:Z

    return v0
.end method

.method public final ali()J
    .locals 2

    .prologue
    .line 145
    iget-wide v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQp:J

    return-wide v0
.end method

.method public final alj()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQq:Z

    return v0
.end method

.method public final alk()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bOm:I

    return v0
.end method

.method public final all()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bOn:I

    return v0
.end method

.method public final alm()Z
    .locals 2

    .prologue
    .line 161
    iget v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bOn:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aln()Z
    .locals 1

    .prologue
    .line 168
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQm:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 371
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 208
    if-ne p0, p1, :cond_1

    .line 227
    :cond_0
    :goto_0
    return v0

    .line 209
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 211
    :cond_3
    check-cast p1, Lcom/google/android/search/shared/actions/utils/CardDecision;

    .line 213
    iget-wide v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQp:J

    iget-wide v4, p1, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQp:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQq:Z

    iget-boolean v3, p1, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQq:Z

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQk:Z

    iget-boolean v3, p1, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQk:Z

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bOn:I

    iget v3, p1, Lcom/google/android/search/shared/actions/utils/CardDecision;->bOn:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bOm:I

    iget v3, p1, Lcom/google/android/search/shared/actions/utils/CardDecision;->bOm:I

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQn:Z

    iget-boolean v3, p1, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQn:Z

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQo:Z

    iget-boolean v3, p1, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQo:Z

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQm:Z

    iget-boolean v3, p1, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQm:Z

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQl:Z

    iget-boolean v3, p1, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQl:Z

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQi:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQi:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQj:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQj:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    .line 224
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 232
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQi:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQj:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQk:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQl:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQm:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQn:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQo:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQp:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQq:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bOm:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bOn:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 192
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CardDecision[OverrideNetworkPrompt: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQq:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", StartFollowOnVoiceSearch: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQm:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ShouldAutoExecute: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ShouldCancel: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQo:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", PlayTts: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQk:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", DisplayPrompt: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", VocalizedPrompt: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", PromptedField: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bOm:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", PromptedArgumentId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bOn:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", CountdownDuration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQp:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 376
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQi:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 377
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQj:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 378
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQk:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 379
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQl:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 380
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQm:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 381
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQq:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 382
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQn:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 383
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQo:Z

    if-eqz v0, :cond_5

    :goto_5
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 384
    iget-wide v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQp:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 385
    iget v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bOm:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 386
    iget v0, p0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bOn:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 387
    return-void

    :cond_0
    move v0, v2

    .line 378
    goto :goto_0

    :cond_1
    move v0, v2

    .line 379
    goto :goto_1

    :cond_2
    move v0, v2

    .line 380
    goto :goto_2

    :cond_3
    move v0, v2

    .line 381
    goto :goto_3

    :cond_4
    move v0, v2

    .line 382
    goto :goto_4

    :cond_5
    move v1, v2

    .line 383
    goto :goto_5
.end method
