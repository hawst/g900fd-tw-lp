.class public abstract Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;
.super Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;
.source "PG"


# direct methods
.method protected constructor <init>(Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;Ljava/lang/Object;)V

    .line 35
    return-void
.end method

.method protected constructor <init>(Ljqg;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;-><init>(Ljqg;Ljava/lang/Object;)V

    .line 31
    return-void
.end method


# virtual methods
.method public abstract a(Ljkh;)J
.end method

.method public abstract a(Ljrk;)V
.end method

.method public abstract ajA()[Ljrk;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ajB()Z
.end method

.method public abstract ajx()Ljrk;
.end method

.method public abstract ajy()J
.end method

.method public abstract ajz()Z
.end method

.method public b(Ljqu;Landroid/content/res/Resources;)Ldws;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 87
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 88
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->ajy()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 89
    invoke-virtual {p1}, Ljqu;->bsE()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 103
    sget-object v0, Ldws;->bOO:Ldws;

    :goto_0
    return-object v0

    .line 91
    :sswitch_0
    new-instance v0, Ldws;

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->ajy()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ldws;-><init>(J)V

    goto :goto_0

    .line 93
    :sswitch_1
    new-instance v0, Ldws;

    const/16 v1, 0xb

    invoke-virtual {v2, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-direct {v0, v1}, Ldws;-><init>(I)V

    goto :goto_0

    .line 95
    :sswitch_2
    new-instance v0, Ldws;

    const/16 v1, 0xc

    invoke-virtual {v2, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-direct {v0, v1}, Ldws;-><init>(I)V

    goto :goto_0

    .line 97
    :sswitch_3
    iget-object v3, p1, Ljqu;->ezz:Ljqm;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljqm;->getFormat()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 98
    :cond_0
    :goto_1
    if-nez v1, :cond_1

    .line 99
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_0

    .line 97
    :pswitch_0
    const/4 v0, 0x3

    :goto_2
    invoke-virtual {v3}, Ljqm;->bsv()I

    move-result v3

    packed-switch v3, :pswitch_data_1

    goto :goto_1

    :pswitch_1
    invoke-static {v0}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_2

    :pswitch_3
    const/4 v0, 0x1

    goto :goto_2

    :pswitch_4
    const/4 v0, 0x0

    goto :goto_2

    :pswitch_5
    invoke-static {v0}, Ljava/text/DateFormat;->getTimeInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :pswitch_6
    invoke-static {v0, v0}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 101
    :cond_1
    new-instance v0, Ldws;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 89
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x11 -> :sswitch_2
        0x12 -> :sswitch_0
        0x19 -> :sswitch_3
    .end sparse-switch

    .line 97
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public gI(I)Z
    .locals 1

    .prologue
    .line 79
    const/16 v0, 0x10

    if-eq p1, v0, :cond_0

    const/16 v0, 0x11

    if-eq p1, v0, :cond_0

    const/16 v0, 0x12

    if-eq p1, v0, :cond_0

    const/16 v0, 0x19

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
