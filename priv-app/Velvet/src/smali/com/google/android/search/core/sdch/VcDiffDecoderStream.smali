.class public Lcom/google/android/search/core/sdch/VcDiffDecoderStream;
.super Ljava/io/InputStream;
.source "PG"


# instance fields
.field private final boJ:[B

.field private boK:Z

.field private boL:J

.field private mClosed:Z

.field private final mDelegate:Ljava/io/InputStream;

.field private final mSdchManager:Lczz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-string v0, "vcdecoder_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lczz;I)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->mDelegate:Ljava/io/InputStream;

    .line 41
    iput-object p2, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->mSdchManager:Lczz;

    .line 42
    new-array v0, p3, [B

    iput-object v0, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->boJ:[B

    .line 43
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->boL:J

    .line 44
    return-void
.end method

.method private e([BII)I
    .locals 9

    .prologue
    .line 115
    iget-wide v2, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->boL:J

    iget-object v7, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->boJ:[B

    const/4 v8, 0x0

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v8}, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->nativeDecode(J[BII[BI)I

    move-result v0

    .line 116
    :goto_0
    if-nez v0, :cond_0

    .line 120
    :try_start_0
    iget-object v1, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->mDelegate:Ljava/io/InputStream;

    iget-object v2, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->boJ:[B

    invoke-virtual {v1, v2}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 126
    if-ltz v8, :cond_0

    .line 128
    iget-wide v2, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->boL:J

    iget-object v7, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->boJ:[B

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v8}, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->nativeDecode(J[BII[BI)I

    move-result v0

    goto :goto_0

    .line 121
    :catch_0
    move-exception v0

    .line 122
    invoke-virtual {p0}, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->close()V

    .line 124
    throw v0

    .line 132
    :cond_0
    if-gez v0, :cond_1

    .line 133
    invoke-virtual {p0}, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->close()V

    .line 134
    const/16 v1, 0x98

    invoke-static {v1}, Lege;->ht(I)V

    .line 136
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error decoding stream, error code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 137
    :cond_1
    if-nez v0, :cond_2

    .line 138
    invoke-virtual {p0}, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->close()V

    .line 141
    const/16 v0, 0x97

    invoke-static {v0}, Lege;->ht(I)V

    .line 143
    const/4 v0, -0x1

    .line 145
    :cond_2
    return v0
.end method

.method private native nativeCleanup(J)V
.end method

.method private native nativeDecode(J[BII[BI)I
.end method

.method private native nativeStart([BII)J
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 150
    iget-wide v0, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->boL:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->boL:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->nativeCleanup(J)V

    iput-wide v2, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->boL:J

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->mDelegate:Ljava/io/InputStream;

    invoke-static {v0}, Lisq;->b(Ljava/io/Closeable;)V

    .line 152
    invoke-super {p0}, Ljava/io/InputStream;->close()V

    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->mClosed:Z

    .line 154
    return-void
.end method

.method protected jo(Ljava/lang/String;)Lczt;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->mSdchManager:Lczz;

    invoke-virtual {v0, p1}, Lczz;->jo(Ljava/lang/String;)Lczt;

    move-result-object v0

    return-object v0
.end method

.method public read()I
    .locals 2

    .prologue
    .line 165
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Single byte reads not supported."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public read([BII)I
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/16 v8, 0x8

    const/4 v1, 0x0

    .line 99
    if-nez p3, :cond_0

    .line 110
    :goto_0
    return v1

    .line 103
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->mClosed:Z

    if-eqz v2, :cond_1

    .line 104
    const/4 v1, -0x1

    goto :goto_0

    .line 107
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->boK:Z

    if-nez v2, :cond_7

    iput-boolean v0, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->boK:Z

    const/4 v3, 0x0

    const/16 v2, 0x9

    :try_start_0
    new-array v4, v2, [B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    iget-object v2, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->mDelegate:Ljava/io/InputStream;

    invoke-static {v2, v4}, Liso;->a(Ljava/io/InputStream;[B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    new-instance v2, Ljava/lang/String;

    const/4 v5, 0x0

    const/16 v6, 0x8

    sget-object v7, Lesp;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v2, v4, v5, v6, v7}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    const/16 v3, 0x8

    :try_start_3
    aget-byte v3, v4, v3

    if-nez v3, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eq v3, v8, :cond_4

    :cond_2
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Invalid dictionary identifier: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->close()V

    const/16 v2, 0x96

    invoke-static {v2}, Lege;->hs(I)Litu;

    move-result-object v2

    if-eqz v1, :cond_3

    invoke-virtual {v2, v1}, Litu;->pP(Ljava/lang/String;)Litu;

    :cond_3
    invoke-static {v2}, Lege;->a(Litu;)V

    throw v0

    :catch_1
    move-exception v0

    :try_start_4
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Unable to read dictionary identifier"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception v0

    move-object v1, v3

    goto :goto_1

    :cond_4
    :try_start_5
    invoke-virtual {p0, v2}, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->jo(Ljava/lang/String;)Lczt;

    move-result-object v3

    if-nez v3, :cond_5

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Response encoded with invalid dictionary : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iget-object v4, v3, Lczt;->bou:[B

    iget v5, v3, Lczt;->bov:I

    invoke-virtual {v3}, Lczt;->getLength()I

    move-result v3

    invoke-direct {p0, v4, v5, v3}, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->nativeStart([BII)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->boL:J

    iget-wide v4, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->boL:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_6

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Could not initialize streaming decoder"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    const/16 v3, 0x81

    invoke-static {v3}, Lege;->hs(I)Litu;

    move-result-object v3

    invoke-virtual {v3, v2}, Litu;->pP(Ljava/lang/String;)Litu;

    move-result-object v3

    invoke-static {v3}, Lege;->a(Litu;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 108
    :cond_7
    iget-wide v2, p0, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->boL:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    :goto_2
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 110
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/search/core/sdch/VcDiffDecoderStream;->e([BII)I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    .line 108
    goto :goto_2
.end method
