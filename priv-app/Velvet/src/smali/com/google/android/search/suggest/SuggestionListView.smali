.class public Lcom/google/android/search/suggest/SuggestionListView;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lelk;


# static fields
.field private static final bXb:Landroid/animation/TimeInterpolator;


# instance fields
.field private aof:Landroid/widget/TextView;

.field private bTm:Leld;

.field private bTt:Lelj;

.field private bWQ:Lesm;

.field private bWZ:Leeo;

.field private final bXc:Leex;

.field private bXd:Leey;

.field private bXe:Lela;

.field private final bXf:Landroid/view/View;

.field private bXg:Leet;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bXh:Z

.field private bXi:Z

.field private bXj:I

.field private bXk:Landroid/view/View;

.field private bXl:Landroid/view/View;

.field private bXm:Landroid/widget/TextView;

.field private bXn:I

.field private bXo:I

.field private bXp:Z

.field private bXq:Z

.field private bXr:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    sget-object v0, Ldqx;->bGB:Ldqx;

    sput-object v0, Lcom/google/android/search/suggest/SuggestionListView;->bXb:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/suggest/SuggestionListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/suggest/SuggestionListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 112
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    .line 115
    const/4 v4, 0x0

    sget-object v0, Lbwe;->aMf:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/search/suggest/SuggestionListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILandroid/view/View;Landroid/content/res/TypedArray;)V

    .line 117
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILandroid/view/View;Landroid/content/res/TypedArray;)V
    .locals 3
    .param p4    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 131
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    new-instance v0, Leex;

    invoke-direct {v0}, Leex;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXc:Leex;

    .line 92
    iput v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    .line 104
    sget-object v0, Lelj;->ceI:Lelj;

    iput-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bTt:Lelj;

    .line 133
    if-nez p4, :cond_0

    move-object p4, p0

    :cond_0
    iput-object p4, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXf:Landroid/view/View;

    .line 134
    invoke-virtual {p5, v2, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    .line 135
    invoke-virtual {p5, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXi:Z

    .line 136
    const/4 v0, 0x2

    invoke-virtual {p5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXh:Z

    .line 137
    invoke-virtual {p5}, Landroid/content/res/TypedArray;->recycle()V

    .line 138
    return-void
.end method

.method private a(Lcom/google/android/shared/search/Suggestion;Landroid/view/View;Ljava/lang/String;ZZZ)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 491
    move-object v0, p2

    check-cast v0, Leev;

    .line 492
    if-nez v0, :cond_5

    .line 493
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXd:Leey;

    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p0}, Leey;->a(Landroid/content/Context;Lcom/google/android/shared/search/Suggestion;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 494
    check-cast v0, Leev;

    .line 495
    iget-object v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bWQ:Lesm;

    iget-object v4, p0, Lcom/google/android/search/suggest/SuggestionListView;->bTm:Leld;

    invoke-interface {v0, v3, v4}, Leev;->a(Lesm;Leld;)V

    move-object v3, v0

    move-object p2, v1

    .line 498
    :goto_0
    if-eqz p5, :cond_4

    .line 499
    const/4 v0, 0x1

    .line 501
    :goto_1
    if-eqz p6, :cond_3

    .line 502
    or-int/lit8 v0, v0, 0x2

    move v1, v0

    .line 504
    :goto_2
    if-eqz p4, :cond_2

    move v0, v2

    :goto_3
    invoke-interface {v3, v0, v1}, Leev;->aG(II)V

    .line 507
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bWZ:Leeo;

    invoke-interface {v3, p1, p3, v0}, Leev;->a(Lcom/google/android/shared/search/Suggestion;Ljava/lang/String;Leeo;)Z

    .line 508
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXc:Leex;

    invoke-interface {v3, v0}, Leev;->a(Leew;)V

    .line 511
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->isEnabled()Z

    move-result v0

    invoke-interface {v3, v0}, Leev;->setEnabled(Z)V

    .line 514
    iget-boolean v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXh:Z

    if-eqz v0, :cond_1

    .line 515
    invoke-interface {v3, v2}, Leev;->hn(I)V

    .line 518
    :cond_1
    return-object p2

    .line 504
    :cond_2
    const/4 v0, 0x2

    goto :goto_3

    :cond_3
    move v1, v0

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move-object v3, v0

    goto :goto_0
.end method

.method private aD(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXd:Leey;

    check-cast p1, Leev;

    invoke-interface {p1}, Leev;->aod()Lcom/google/android/shared/search/Suggestion;

    move-result-object v0

    invoke-static {v0}, Leey;->p(Lcom/google/android/shared/search/Suggestion;)I

    move-result v0

    return v0
.end method

.method private aH(II)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 662
    add-int/lit8 v1, p2, -0x1

    if-ne p1, v1, :cond_1

    .line 667
    :cond_0
    :goto_0
    return v0

    .line 666
    :cond_1
    iget v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    sub-int v1, p1, v1

    add-int/lit8 v1, v1, 0x1

    .line 667
    iget v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    rem-int/2addr v1, v2

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aof()V
    .locals 12

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 365
    :goto_0
    iget-object v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isFocusable()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    .line 368
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getChildCount()I

    move-result v2

    iget v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    sub-int v8, v2, v3

    .line 369
    iget v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    div-int v2, v8, v2

    .line 370
    iget v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    rem-int v3, v8, v3

    if-eqz v3, :cond_0

    .line 371
    add-int/lit8 v2, v2, 0x1

    .line 374
    :cond_0
    if-eqz v8, :cond_c

    .line 375
    if-eqz v0, :cond_1

    .line 376
    add-int/lit8 v3, v8, -0x1

    add-int/lit8 v4, v2, -0x1

    iget v5, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    mul-int/2addr v4, v5

    iget v5, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    add-int/lit8 v5, v5, -0x1

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 378
    iget v4, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    add-int/2addr v3, v4

    invoke-static {p0, v3}, Leln;->e(Landroid/view/ViewGroup;I)I

    move-result v3

    .line 380
    iget v4, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    invoke-static {p0, v4}, Leln;->e(Landroid/view/ViewGroup;I)I

    move-result v4

    .line 381
    iget-object v5, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    const/4 v6, 0x2

    new-array v6, v6, [I

    const/4 v7, 0x0

    iget-object v9, p0, Lcom/google/android/search/suggest/SuggestionListView;->bTt:Lelj;

    iget v9, v9, Lelj;->ceD:I

    aput v9, v6, v7

    const/4 v7, 0x1

    iget-object v9, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getId()I

    move-result v9

    aput v9, v6, v7

    invoke-static {v6}, Leln;->o([I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 382
    iget-object v5, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-virtual {v5, v3}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 383
    iget-object v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    const/4 v5, 0x2

    new-array v5, v5, [I

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/search/suggest/SuggestionListView;->bTt:Lelj;

    iget v7, v7, Lelj;->ceF:I

    aput v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getId()I

    move-result v7

    aput v7, v5, v6

    invoke-static {v5}, Leln;->o([I)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 385
    iget-object v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    const/4 v5, 0x2

    new-array v5, v5, [I

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/search/suggest/SuggestionListView;->bTt:Lelj;

    iget v7, v7, Lelj;->ceG:I

    aput v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getId()I

    move-result v7

    aput v7, v5, v6

    invoke-static {v5}, Leln;->o([I)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 387
    iget-object v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setNextFocusForwardId(I)V

    .line 390
    :cond_1
    if-eqz v1, :cond_2

    .line 391
    add-int/lit8 v3, v8, -0x1

    add-int/lit8 v2, v2, -0x1

    iget v4, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    mul-int/2addr v2, v4

    iget v4, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    add-int/lit8 v4, v4, -0x1

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 393
    iget v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    add-int/2addr v2, v3

    invoke-static {p0, v2}, Leln;->e(Landroid/view/ViewGroup;I)I

    move-result v2

    .line 395
    iget-object v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 396
    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    const/4 v3, 0x2

    new-array v3, v3, [I

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/search/suggest/SuggestionListView;->bTt:Lelj;

    iget v5, v5, Lelj;->ceE:I

    aput v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getId()I

    move-result v5

    aput v5, v3, v4

    invoke-static {v3}, Leln;->o([I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 398
    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    const/4 v3, 0x2

    new-array v3, v3, [I

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/search/suggest/SuggestionListView;->bTt:Lelj;

    iget v5, v5, Lelj;->ceF:I

    aput v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getId()I

    move-result v5

    aput v5, v3, v4

    invoke-static {v3}, Leln;->o([I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 400
    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    const/4 v3, 0x2

    new-array v3, v3, [I

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/search/suggest/SuggestionListView;->bTt:Lelj;

    iget v5, v5, Lelj;->ceG:I

    aput v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getId()I

    move-result v5

    aput v5, v3, v4

    invoke-static {v3}, Leln;->o([I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 402
    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    const/4 v3, 0x2

    new-array v3, v3, [I

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/search/suggest/SuggestionListView;->bTt:Lelj;

    iget v5, v5, Lelj;->ceH:I

    aput v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getId()I

    move-result v5

    aput v5, v3, v4

    invoke-static {v3}, Leln;->o([I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setNextFocusForwardId(I)V

    .line 406
    :cond_2
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    move v7, v0

    .line 407
    :goto_2
    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    .line 408
    :goto_3
    const/4 v1, 0x0

    move v4, v1

    :goto_4
    if-ge v4, v8, :cond_c

    .line 409
    iget v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    add-int/2addr v1, v4

    invoke-virtual {p0, v1}, Lcom/google/android/search/suggest/SuggestionListView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 410
    iget v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    sub-int v1, v4, v1

    if-ltz v1, :cond_7

    iget v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    add-int/2addr v1, v4

    iget v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    sub-int/2addr v1, v2

    invoke-static {p0, v1}, Leln;->e(Landroid/view/ViewGroup;I)I

    move-result v1

    .line 413
    :goto_5
    iget v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    add-int/2addr v2, v4

    if-ge v2, v8, :cond_8

    iget v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    add-int/2addr v2, v4

    iget v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    add-int/2addr v2, v3

    invoke-static {p0, v2}, Leln;->e(Landroid/view/ViewGroup;I)I

    move-result v2

    .line 416
    :goto_6
    iget v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    rem-int v3, v4, v3

    if-eqz v3, :cond_9

    iget v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    invoke-static {p0, v3}, Leln;->e(Landroid/view/ViewGroup;I)I

    move-result v3

    move v6, v3

    .line 419
    :goto_7
    add-int/lit8 v3, v4, 0x1

    if-ge v3, v8, :cond_a

    add-int/lit8 v3, v4, 0x1

    iget v5, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    rem-int/2addr v3, v5

    if-eqz v3, :cond_a

    iget v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    invoke-static {p0, v3}, Leln;->e(Landroid/view/ViewGroup;I)I

    move-result v3

    move v5, v3

    .line 422
    :goto_8
    add-int/lit8 v3, v4, 0x1

    if-ge v3, v8, :cond_b

    iget v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    invoke-static {p0, v3}, Leln;->e(Landroid/view/ViewGroup;I)I

    move-result v3

    .line 426
    :goto_9
    const/4 v10, 0x4

    new-array v10, v10, [I

    const/4 v11, 0x0

    aput v1, v10, v11

    const/4 v1, 0x1

    aput v7, v10, v1

    const/4 v1, 0x2

    iget-object v11, p0, Lcom/google/android/search/suggest/SuggestionListView;->bTt:Lelj;

    iget v11, v11, Lelj;->ceD:I

    aput v11, v10, v1

    const/4 v1, 0x3

    invoke-virtual {v9}, Landroid/view/View;->getId()I

    move-result v11

    aput v11, v10, v1

    invoke-static {v10}, Leln;->o([I)I

    move-result v1

    invoke-virtual {v9, v1}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 428
    const/4 v1, 0x4

    new-array v1, v1, [I

    const/4 v10, 0x0

    aput v2, v1, v10

    const/4 v2, 0x1

    aput v0, v1, v2

    const/4 v2, 0x2

    iget-object v10, p0, Lcom/google/android/search/suggest/SuggestionListView;->bTt:Lelj;

    iget v10, v10, Lelj;->ceE:I

    aput v10, v1, v2

    const/4 v2, 0x3

    invoke-virtual {v9}, Landroid/view/View;->getId()I

    move-result v10

    aput v10, v1, v2

    invoke-static {v1}, Leln;->o([I)I

    move-result v1

    invoke-virtual {v9, v1}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 430
    const/4 v1, 0x3

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput v6, v1, v2

    const/4 v2, 0x1

    iget-object v6, p0, Lcom/google/android/search/suggest/SuggestionListView;->bTt:Lelj;

    iget v6, v6, Lelj;->ceF:I

    aput v6, v1, v2

    const/4 v2, 0x2

    invoke-virtual {v9}, Landroid/view/View;->getId()I

    move-result v6

    aput v6, v1, v2

    invoke-static {v1}, Leln;->o([I)I

    move-result v1

    invoke-virtual {v9, v1}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 431
    const/4 v1, 0x3

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput v5, v1, v2

    const/4 v2, 0x1

    iget-object v5, p0, Lcom/google/android/search/suggest/SuggestionListView;->bTt:Lelj;

    iget v5, v5, Lelj;->ceG:I

    aput v5, v1, v2

    const/4 v2, 0x2

    invoke-virtual {v9}, Landroid/view/View;->getId()I

    move-result v5

    aput v5, v1, v2

    invoke-static {v1}, Leln;->o([I)I

    move-result v1

    invoke-virtual {v9, v1}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 432
    const/4 v1, 0x4

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput v3, v1, v2

    const/4 v2, 0x1

    aput v0, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bTt:Lelj;

    iget v3, v3, Lelj;->ceH:I

    aput v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {v9}, Landroid/view/View;->getId()I

    move-result v3

    aput v3, v1, v2

    invoke-static {v1}, Leln;->o([I)I

    move-result v1

    invoke-virtual {v9, v1}, Landroid/view/View;->setNextFocusForwardId(I)V

    .line 408
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto/16 :goto_4

    .line 362
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 365
    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 406
    :cond_5
    const/4 v0, -0x1

    move v7, v0

    goto/16 :goto_2

    .line 407
    :cond_6
    const/4 v0, -0x1

    goto/16 :goto_3

    .line 410
    :cond_7
    const/4 v1, -0x1

    goto/16 :goto_5

    .line 413
    :cond_8
    const/4 v2, -0x1

    goto/16 :goto_6

    .line 416
    :cond_9
    const/4 v3, -0x1

    move v6, v3

    goto/16 :goto_7

    .line 419
    :cond_a
    const/4 v3, -0x1

    move v5, v3

    goto/16 :goto_8

    .line 422
    :cond_b
    const/4 v3, -0x1

    goto/16 :goto_9

    .line 436
    :cond_c
    return-void
.end method

.method private aog()V
    .locals 3

    .prologue
    .line 531
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getChildCount()I

    move-result v0

    iget v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    sub-int/2addr v0, v1

    .line 532
    if-lez v0, :cond_2

    .line 533
    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    div-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    add-int/2addr v0, v1

    move v1, v0

    .line 535
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 536
    invoke-virtual {p0, v1}, Lcom/google/android/search/suggest/SuggestionListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 537
    iget-boolean v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXh:Z

    if-eqz v2, :cond_0

    .line 538
    check-cast v0, Leev;

    iget-boolean v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXi:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    :goto_1
    invoke-interface {v0, v2}, Leev;->hn(I)V

    .line 535
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 538
    :cond_1
    const/16 v2, 0x8

    goto :goto_1

    .line 543
    :cond_2
    return-void
.end method

.method private b(Landroid/view/View;II)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 647
    int-to-long v2, p2

    const-wide/16 v4, 0x28

    mul-long/2addr v2, v4

    .line 648
    iget-boolean v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXq:Z

    if-eqz v0, :cond_0

    neg-int v0, p3

    int-to-float v0, v0

    const v4, 0x3ea3d70a    # 0.32f

    mul-float/2addr v0, v4

    .line 651
    :goto_0
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 652
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 653
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v4, 0xa0

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v4, 0x50

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lcom/google/android/search/suggest/SuggestionListView;->bXb:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    .line 659
    return-void

    :cond_0
    move v0, v1

    .line 648
    goto :goto_0
.end method

.method private static c(Landroid/view/View;II)V
    .locals 2

    .prologue
    .line 672
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, p2

    invoke-virtual {p0, p1, p2, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 673
    return-void
.end method

.method private hq(I)V
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXf:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 440
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXg:Leet;

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXg:Leet;

    invoke-interface {v0}, Leet;->aoh()V

    .line 443
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Leen;)V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXc:Leex;

    invoke-virtual {v0, p1}, Leex;->a(Leen;)V

    .line 164
    return-void
.end method

.method public final a(Leet;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXg:Leet;

    .line 168
    return-void
.end method

.method public final a(Leey;Leeo;Lesm;Lela;Leld;)V
    .locals 1

    .prologue
    .line 153
    iput-object p1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXd:Leey;

    .line 154
    iput-object p2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bWZ:Leeo;

    .line 155
    iput-object p3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bWQ:Lesm;

    .line 156
    iput-object p4, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXe:Lela;

    .line 157
    iput-object p5, p0, Lcom/google/android/search/suggest/SuggestionListView;->bTm:Leld;

    .line 158
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXd:Leey;

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXj:I

    .line 159
    return-void
.end method

.method public final a(Lelj;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bTt:Lelj;

    .line 301
    invoke-direct {p0}, Lcom/google/android/search/suggest/SuggestionListView;->aof()V

    .line 302
    return-void
.end method

.method public final d(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    :cond_0
    return-void
.end method

.method public final e(Ljava/lang/String;Ljava/util/List;I)V
    .locals 14

    .prologue
    .line 216
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v1

    move/from16 v0, p3

    if-gt v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lifv;->gY(Z)V

    .line 219
    if-nez p3, :cond_1

    .line 220
    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lcom/google/android/search/suggest/SuggestionListView;->hq(I)V

    .line 273
    :goto_1
    return-void

    .line 216
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 226
    :cond_1
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v7

    const/4 v1, 0x1

    if-gt v7, v1, :cond_5

    const/4 v1, 0x0

    move v8, v1

    .line 228
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getChildCount()I

    move-result v10

    .line 229
    const/4 v1, 0x0

    move v9, v1

    :goto_3
    move/from16 v0, p3

    if-ge v9, v0, :cond_15

    .line 230
    iget-object v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_f

    :cond_2
    if-nez v9, :cond_f

    const/4 v6, 0x1

    .line 232
    :goto_4
    iget-object v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_10

    :cond_3
    add-int/lit8 v1, p3, -0x1

    if-ne v9, v1, :cond_10

    const/4 v7, 0x1

    .line 234
    :goto_5
    if-eqz v8, :cond_4

    if-nez v9, :cond_11

    :cond_4
    const/4 v5, 0x1

    .line 236
    :goto_6
    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/shared/search/Suggestion;

    .line 237
    iget-object v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXd:Leey;

    invoke-static {v2}, Leey;->p(Lcom/google/android/shared/search/Suggestion;)I

    move-result v1

    .line 239
    iget v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    add-int v11, v3, v9

    .line 240
    if-ge v11, v10, :cond_12

    invoke-virtual {p0, v11}, Lcom/google/android/search/suggest/SuggestionListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 242
    :goto_7
    if-eqz v3, :cond_13

    invoke-direct {p0, v3}, Lcom/google/android/search/suggest/SuggestionListView;->aD(Landroid/view/View;)I

    move-result v4

    if-ne v4, v1, :cond_13

    move-object v1, p0

    move-object v4, p1

    .line 244
    invoke-direct/range {v1 .. v7}, Lcom/google/android/search/suggest/SuggestionListView;->a(Lcom/google/android/shared/search/Suggestion;Landroid/view/View;Ljava/lang/String;ZZZ)Landroid/view/View;

    .line 229
    :goto_8
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    goto :goto_3

    .line 226
    :cond_5
    const/4 v2, 0x0

    const/4 v1, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/shared/search/Suggestion;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/google/android/shared/search/Suggestion;->ast()Z

    move-result v2

    if-nez v2, :cond_8

    invoke-virtual {v1}, Lcom/google/android/shared/search/Suggestion;->asb()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    :cond_6
    const/4 v1, 0x1

    move v6, v1

    :goto_9
    if-ge v6, v7, :cond_e

    move-object/from16 v0, p2

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/shared/search/Suggestion;

    if-eqz v1, :cond_d

    if-eqz v2, :cond_9

    invoke-virtual {v1}, Lcom/google/android/shared/search/Suggestion;->asb()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_9

    const/4 v3, 0x1

    move v5, v3

    :goto_a
    if-nez v2, :cond_a

    const/4 v3, 0x1

    :goto_b
    invoke-virtual {v1}, Lcom/google/android/shared/search/Suggestion;->asb()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_b

    const/4 v4, 0x1

    :goto_c
    if-eq v3, v4, :cond_c

    const/4 v3, 0x1

    :goto_d
    if-nez v3, :cond_7

    if-eqz v5, :cond_d

    invoke-virtual {v1}, Lcom/google/android/shared/search/Suggestion;->asb()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    :cond_7
    const/4 v1, 0x0

    move v8, v1

    goto/16 :goto_2

    :cond_8
    const/4 v1, 0x0

    move v8, v1

    goto/16 :goto_2

    :cond_9
    const/4 v3, 0x0

    move v5, v3

    goto :goto_a

    :cond_a
    const/4 v3, 0x0

    goto :goto_b

    :cond_b
    const/4 v4, 0x0

    goto :goto_c

    :cond_c
    const/4 v3, 0x0

    goto :goto_d

    :cond_d
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_9

    :cond_e
    const/4 v1, 0x1

    move v8, v1

    goto/16 :goto_2

    .line 230
    :cond_f
    const/4 v6, 0x0

    goto/16 :goto_4

    .line 232
    :cond_10
    const/4 v7, 0x0

    goto/16 :goto_5

    .line 234
    :cond_11
    const/4 v5, 0x0

    goto/16 :goto_6

    .line 240
    :cond_12
    const/4 v3, 0x0

    goto :goto_7

    .line 247
    :cond_13
    if-eqz v3, :cond_14

    .line 250
    invoke-virtual {p0, v11}, Lcom/google/android/search/suggest/SuggestionListView;->removeViewAt(I)V

    .line 251
    iget-object v4, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXe:Lela;

    invoke-direct {p0, v3}, Lcom/google/android/search/suggest/SuggestionListView;->aD(Landroid/view/View;)I

    move-result v12

    iget v13, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXj:I

    invoke-virtual {v4, v3, v12, v13}, Lela;->g(Landroid/view/View;II)V

    .line 256
    :cond_14
    iget-object v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXe:Lela;

    invoke-virtual {v3, v1}, Lela;->ie(I)Landroid/view/View;

    move-result-object v3

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/search/suggest/SuggestionListView;->a(Lcom/google/android/shared/search/Suggestion;Landroid/view/View;Ljava/lang/String;ZZZ)Landroid/view/View;

    move-result-object v1

    .line 258
    invoke-virtual {p0, v1, v11}, Lcom/google/android/search/suggest/SuggestionListView;->addView(Landroid/view/View;I)V

    goto/16 :goto_8

    .line 262
    :cond_15
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getChildCount()I

    move-result v1

    iget v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    :goto_e
    move/from16 v0, p3

    if-lt v1, v0, :cond_16

    iget v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    add-int/2addr v2, v1

    invoke-virtual {p0, v2}, Lcom/google/android/search/suggest/SuggestionListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0, v2}, Lcom/google/android/search/suggest/SuggestionListView;->removeViewAt(I)V

    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXe:Lela;

    invoke-direct {p0, v3}, Lcom/google/android/search/suggest/SuggestionListView;->aD(Landroid/view/View;)I

    move-result v4

    iget v5, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXj:I

    invoke-virtual {v2, v3, v4, v5}, Lela;->g(Landroid/view/View;II)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_e

    .line 263
    :cond_16
    invoke-direct {p0}, Lcom/google/android/search/suggest/SuggestionListView;->aog()V

    .line 264
    invoke-direct {p0}, Lcom/google/android/search/suggest/SuggestionListView;->aof()V

    .line 267
    iget-object v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXf:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_17

    iget-boolean v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXp:Z

    if-eqz v1, :cond_17

    .line 268
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXr:Z

    .line 272
    :cond_17
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/search/suggest/SuggestionListView;->hq(I)V

    goto/16 :goto_1
.end method

.method public final eO(Z)V
    .locals 1

    .prologue
    .line 523
    iget-boolean v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXi:Z

    if-eq v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXh:Z

    if-eqz v0, :cond_0

    .line 524
    iput-boolean p1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXi:Z

    .line 525
    invoke-direct {p0}, Lcom/google/android/search/suggest/SuggestionListView;->aog()V

    .line 527
    :cond_0
    return-void
.end method

.method public final hb(I)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 306
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 309
    :goto_0
    iget-object v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->isFocusable()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 312
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getChildCount()I

    move-result v2

    iget v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    sub-int v3, v2, v3

    .line 313
    iget v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    div-int v2, v3, v2

    .line 314
    iget v4, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    rem-int v4, v3, v4

    if-eqz v4, :cond_0

    .line 315
    add-int/lit8 v2, v2, 0x1

    .line 319
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    if-nez v3, :cond_4

    .line 320
    :cond_1
    const/4 v0, -0x1

    .line 355
    :goto_2
    return v0

    :cond_2
    move v0, v2

    .line 306
    goto :goto_0

    :cond_3
    move v1, v2

    .line 309
    goto :goto_1

    .line 322
    :cond_4
    sparse-switch p1, :sswitch_data_0

    .line 357
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 324
    :sswitch_0
    if-eqz v0, :cond_5

    .line 325
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    goto :goto_2

    .line 327
    :cond_5
    iget v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    invoke-static {p0, v0}, Leln;->e(Landroid/view/ViewGroup;I)I

    move-result v0

    goto :goto_2

    .line 330
    :sswitch_1
    if-eqz v0, :cond_6

    .line 331
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    goto :goto_2

    .line 334
    :cond_6
    add-int/lit8 v0, v3, -0x1

    iget v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    add-int/lit8 v1, v1, -0x1

    div-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 335
    iget v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    add-int/2addr v0, v1

    invoke-static {p0, v0}, Leln;->e(Landroid/view/ViewGroup;I)I

    move-result v0

    goto :goto_2

    .line 339
    :sswitch_2
    if-eqz v1, :cond_7

    .line 340
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    goto :goto_2

    .line 343
    :cond_7
    add-int/lit8 v0, v3, -0x1

    add-int/lit8 v1, v2, -0x1

    iget v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    add-int/lit8 v2, v2, -0x1

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 345
    iget v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    add-int/2addr v0, v1

    invoke-static {p0, v0}, Leln;->e(Landroid/view/ViewGroup;I)I

    move-result v0

    goto :goto_2

    .line 349
    :sswitch_3
    add-int/lit8 v0, v3, -0x1

    add-int/lit8 v1, v2, -0x1

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 351
    iget v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    add-int/2addr v0, v1

    invoke-static {p0, v0}, Leln;->e(Landroid/view/ViewGroup;I)I

    move-result v0

    goto :goto_2

    .line 354
    :sswitch_4
    add-int/lit8 v0, v3, -0x1

    add-int/lit8 v1, v2, -0x1

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    mul-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 355
    iget v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    add-int/2addr v0, v1

    invoke-static {p0, v0}, Leln;->e(Landroid/view/ViewGroup;I)I

    move-result v0

    goto/16 :goto_2

    .line 322
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x11 -> :sswitch_3
        0x21 -> :sswitch_2
        0x42 -> :sswitch_4
        0x82 -> :sswitch_1
    .end sparse-switch
.end method

.method public final ho(I)V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 187
    invoke-direct {p0}, Lcom/google/android/search/suggest/SuggestionListView;->aog()V

    .line 189
    :cond_0
    return-void
.end method

.method public final hp(I)V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXm:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXm:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 206
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 142
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 143
    const v0, 0x7f1103b6

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/SuggestionListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    .line 144
    const v0, 0x7f1103bb

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/SuggestionListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    .line 145
    const v0, 0x7f1103b7

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/SuggestionListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->aof:Landroid/widget/TextView;

    .line 146
    const v0, 0x7f110419

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/SuggestionListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXm:Landroid/widget/TextView;

    .line 147
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getChildCount()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    .line 148
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v1, 0x0

    .line 605
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getPaddingLeft()I

    move-result v3

    .line 606
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getPaddingTop()I

    move-result v0

    .line 607
    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eq v2, v8, :cond_0

    .line 608
    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-static {v2, v3, v0}, Lcom/google/android/search/suggest/SuggestionListView;->c(Landroid/view/View;II)V

    .line 609
    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 612
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getChildCount()I

    move-result v5

    .line 614
    iget v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    move v4, v2

    move v2, v0

    move v0, v1

    :goto_0
    if-ge v4, v5, :cond_3

    .line 615
    invoke-virtual {p0, v4}, Lcom/google/android/search/suggest/SuggestionListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 616
    invoke-static {v6, v3, v2}, Lcom/google/android/search/suggest/SuggestionListView;->c(Landroid/view/View;II)V

    .line 617
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 620
    iget-boolean v7, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXr:Z

    if-eqz v7, :cond_1

    .line 621
    iget v7, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    sub-int v7, v4, v7

    invoke-direct {p0, v6, v7, v0}, Lcom/google/android/search/suggest/SuggestionListView;->b(Landroid/view/View;II)V

    .line 624
    :cond_1
    invoke-direct {p0, v4, v5}, Lcom/google/android/search/suggest/SuggestionListView;->aH(II)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 626
    add-int/2addr v0, v2

    .line 628
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getPaddingLeft()I

    move-result v2

    move v3, v2

    move v2, v0

    move v0, v1

    .line 614
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 630
    :cond_2
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v3, v6

    goto :goto_1

    .line 634
    :cond_3
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v8, :cond_4

    .line 635
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-static {v0, v3, v2}, Lcom/google/android/search/suggest/SuggestionListView;->c(Landroid/view/View;II)V

    .line 636
    iget-boolean v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXr:Z

    if-eqz v0, :cond_4

    .line 638
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getChildCount()I

    move-result v2

    iget v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/search/suggest/SuggestionListView;->b(Landroid/view/View;II)V

    .line 643
    :cond_4
    iput-boolean v1, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXr:Z

    .line 644
    return-void
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 547
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 548
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 549
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 550
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 552
    if-ne v0, v10, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 554
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getPaddingTop()I

    move-result v0

    .line 557
    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eq v2, v11, :cond_0

    .line 558
    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-virtual {p0, v2, p1, p2}, Lcom/google/android/search/suggest/SuggestionListView;->measureChild(Landroid/view/View;II)V

    .line 559
    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXk:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 563
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getChildCount()I

    move-result v7

    .line 567
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getPaddingRight()I

    move-result v4

    add-int/2addr v2, v4

    .line 568
    sub-int v4, v6, v2

    iget v8, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXn:I

    div-int/2addr v4, v8

    add-int/2addr v2, v4

    invoke-static {v2, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 573
    iget v2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXo:I

    move v4, v2

    move v2, v0

    move v0, v1

    :goto_1
    if-ge v4, v7, :cond_3

    .line 574
    invoke-virtual {p0, v4}, Lcom/google/android/search/suggest/SuggestionListView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 575
    invoke-virtual {p0, v9, v8, p2}, Lcom/google/android/search/suggest/SuggestionListView;->measureChild(Landroid/view/View;II)V

    .line 576
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    invoke-static {v9, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 578
    invoke-direct {p0, v4, v7}, Lcom/google/android/search/suggest/SuggestionListView;->aH(II)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 580
    add-int/2addr v0, v2

    move v2, v0

    move v0, v1

    .line 573
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 552
    goto :goto_0

    .line 586
    :cond_3
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v11, :cond_4

    .line 587
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/search/suggest/SuggestionListView;->measureChild(Landroid/view/View;II)V

    .line 588
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXl:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v2, v0

    .line 591
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionListView;->getPaddingBottom()I

    move-result v0

    add-int/2addr v0, v2

    .line 593
    if-ne v5, v10, :cond_6

    move v0, v3

    .line 599
    :cond_5
    :goto_2
    invoke-virtual {p0, v6, v0}, Lcom/google/android/search/suggest/SuggestionListView;->setMeasuredDimension(II)V

    .line 600
    return-void

    .line 595
    :cond_6
    const/high16 v1, -0x80000000

    if-ne v5, v1, :cond_5

    .line 596
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_2
.end method

.method public final s(ZZ)V
    .locals 1

    .prologue
    .line 450
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXp:Z

    .line 451
    iput-boolean p2, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXq:Z

    .line 452
    iget-boolean v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXr:Z

    and-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->bXr:Z

    .line 453
    return-void
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->aof:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionListView;->aof:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    :cond_0
    return-void
.end method
