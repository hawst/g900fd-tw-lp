.class public Lcom/google/android/search/core/google/GoogleSearch;
.super Landroid/app/Activity;
.source "PG"


# static fields
.field private static DBG:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/search/core/google/GoogleSearch;->DBG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Intent;Landroid/content/Context;ZLandroid/location/Location;Lemp;Lcpn;Lcpw;Ljava/lang/String;)Landroid/content/Intent;
    .locals 8

    .prologue
    .line 103
    const-string v0, "query"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 104
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    const-string v0, "QSB.GoogleSearch"

    const-string v1, "Got search intent with no query."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    const/4 v0, 0x0

    .line 157
    :goto_0
    return-object v0

    .line 113
    :cond_0
    const-string v0, "com.android.browser.application_id"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 114
    if-nez v0, :cond_1

    .line 115
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 118
    :cond_1
    const-string v2, "from_self"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 119
    const-string v3, "new_search"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 120
    const-string v4, "query_submit_ts"

    invoke-interface {p4}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {p0, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 124
    const-string v6, "unknown"

    invoke-static {p0, v6}, Lhgn;->e(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 125
    invoke-virtual {p5}, Lcpn;->Rx()Lcpo;

    move-result-object v7

    invoke-virtual {v7}, Lcpo;->RV()Lcpo;

    move-result-object v7

    invoke-virtual {v7, v1}, Lcpo;->hZ(Ljava/lang/String;)Lcpo;

    move-result-object v1

    invoke-virtual {v1}, Lcpo;->RK()Lcpo;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcpo;->ie(Ljava/lang/String;)Lcpo;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Lcpo;->ac(J)Lcpo;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, p2, p3, v4}, Lcpo;->a(ZLandroid/location/Location;Landroid/location/Location;)Lcpo;

    move-result-object v1

    .line 134
    if-eqz v2, :cond_2

    .line 137
    invoke-virtual {v1, p7}, Lcpo;->ib(Ljava/lang/String;)Lcpo;

    .line 140
    :cond_2
    invoke-virtual {v1, p6}, Lcpo;->a(Lcpw;)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v2

    .line 141
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-virtual {v2}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 143
    const-string v4, "com.android.browser.application_id"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    if-eqz v3, :cond_3

    .line 145
    const-string v0, "create_new_tab"

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 148
    :cond_3
    if-eqz p2, :cond_4

    if-eqz p3, :cond_4

    .line 150
    invoke-virtual {v2}, Lcom/google/android/search/shared/api/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v0

    .line 151
    invoke-static {v0}, Lesp;->o(Ljava/util/Map;)Landroid/os/Bundle;

    move-result-object v0

    .line 152
    const-string v2, "com.android.browser.headers"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 156
    :cond_4
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object v0, v1

    .line 157
    goto/16 :goto_0
.end method

.method private a(Landroid/app/PendingIntent;Landroid/content/Intent;)Z
    .locals 3

    .prologue
    .line 238
    const/4 v0, -0x1

    :try_start_0
    invoke-virtual {p1, p0, v0, p2}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    const/4 v0, 0x1

    .line 242
    :goto_0
    return v0

    .line 241
    :catch_0
    move-exception v0

    const-string v0, "QSB.GoogleSearch"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Pending intent cancelled: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static a(Landroid/app/PendingIntent;Landroid/content/pm/PackageManager;)Z
    .locals 4
    .param p0    # Landroid/app/PendingIntent;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Landroid/content/pm/PackageManager;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 170
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    if-nez p0, :cond_1

    .line 182
    :cond_0
    :goto_0
    return v0

    .line 175
    :cond_1
    invoke-virtual {p0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v2

    .line 176
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    .line 177
    goto :goto_0

    .line 180
    :cond_2
    const-string v3, "android.permission.ACCESS_FINE_LOCATION"

    invoke-virtual {p1, v3, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private q(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 229
    :try_start_0
    const-string v0, "QSB.GoogleSearch"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Launching intent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/google/GoogleSearch;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    :goto_0
    return-void

    .line 232
    :catch_0
    move-exception v0

    const-string v0, "QSB.GoogleSearch"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No activity found to handle: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 72
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/search/core/google/GoogleSearch;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 75
    const-string v1, "QSB.GoogleSearch"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Got intent: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 78
    const-string v4, "android.intent.action.WEB_SEARCH"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "android.intent.action.SEARCH"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 79
    :cond_0
    const-string v1, "web_search_pendingintent"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Landroid/app/PendingIntent;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DP()Lcob;

    move-result-object v1

    invoke-interface {v1}, Lcob;->QL()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/search/core/google/GoogleSearch;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v8, v1}, Lcom/google/android/search/core/google/GoogleSearch;->a(Landroid/app/PendingIntent;Landroid/content/pm/PackageManager;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :cond_1
    if-eqz v2, :cond_2

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    invoke-virtual {v1}, Lgql;->El()Lfdr;

    move-result-object v1

    invoke-interface {v1}, Lfdr;->ayy()Landroid/location/Location;

    move-result-object v3

    :cond_2
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DC()Lemp;

    move-result-object v4

    invoke-virtual {v1}, Lcfo;->DI()Lcpn;

    move-result-object v5

    invoke-virtual {v1}, Lcfo;->DM()Lcpw;

    move-result-object v6

    invoke-virtual {v1}, Lcfo;->DH()Lcoy;

    move-result-object v1

    invoke-virtual {v1}, Lcoy;->Rf()Ljava/lang/String;

    move-result-object v7

    move-object v1, p0

    invoke-static/range {v0 .. v7}, Lcom/google/android/search/core/google/GoogleSearch;->a(Landroid/content/Intent;Landroid/content/Context;ZLandroid/location/Location;Lemp;Lcpn;Lcpw;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    if-eqz v8, :cond_3

    invoke-direct {p0, v8, v1}, Lcom/google/android/search/core/google/GoogleSearch;->a(Landroid/app/PendingIntent;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_4

    :cond_3
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.googlequicksearchbox.GOOGLE_SEARCH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "query"

    const-string v3, "query"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "com.google.android.googlequicksearchbox"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "web-search-intent"

    invoke-static {v0, v2}, Lhgn;->e(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lhgn;->f(Landroid/content/Intent;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/google/android/search/core/google/GoogleSearch;->q(Landroid/content/Intent;)V

    .line 86
    :cond_4
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/search/core/google/GoogleSearch;->finish()V

    .line 90
    return-void

    .line 80
    :cond_5
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 81
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_6

    const-string v0, "QSB.GoogleSearch"

    const-string v1, "Got ACTION_VIEW with no data."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_6
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v0, "intent_extra_data_key"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, v1}, Lcom/google/android/search/core/google/GoogleSearch;->q(Landroid/content/Intent;)V

    goto :goto_0

    .line 83
    :cond_7
    const-string v1, "QSB.GoogleSearch"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled intent: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
