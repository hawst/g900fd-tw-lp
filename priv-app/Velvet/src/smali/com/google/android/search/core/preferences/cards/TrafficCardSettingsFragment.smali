.class public Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;
.super Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private aUh:Lcxs;

.field private boa:Lcom/google/android/search/core/preferences/WrappingSwitchPreference;

.field private mIntentStarter:Leoj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;-><init>()V

    return-void
.end method

.method private fu(I)V
    .locals 2

    .prologue
    .line 124
    invoke-virtual {p0, p1}, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 125
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 126
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 127
    if-eqz v0, :cond_0

    .line 128
    instance-of v1, v0, Lcom/google/android/search/core/preferences/WrappingPreference;

    if-eqz v1, :cond_1

    .line 129
    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_0
.end method


# virtual methods
.method protected final Ug()I
    .locals 1

    .prologue
    .line 38
    const v0, 0x7f070024

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const v2, 0x7f0a013c

    .line 43
    invoke-super {p0, p1}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 45
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DE()Lcxs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->aUh:Lcxs;

    .line 48
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0, v2}, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/core/preferences/WrappingSwitchPreference;

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->boa:Lcom/google/android/search/core/preferences/WrappingSwitchPreference;

    .line 51
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 52
    instance-of v1, v0, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    if-eqz v1, :cond_0

    .line 53
    check-cast v0, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->wr()Leoj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->mIntentStarter:Leoj;

    .line 54
    const v0, 0x7f0a0138

    invoke-direct {p0, v0}, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->fu(I)V

    .line 55
    const v0, 0x7f0a0137

    invoke-direct {p0, v0}, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->fu(I)V

    .line 56
    invoke-direct {p0, v2}, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->fu(I)V

    .line 58
    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 106
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 108
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 110
    const v3, 0x7f0a013c

    invoke-virtual {p0, v3}, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 112
    iget-object v3, p0, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->aUh:Lcxs;

    invoke-virtual {v3}, Lcxs;->TH()Lcxi;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcxi;->cL(Z)V

    .line 113
    if-eqz v0, :cond_0

    const v0, 0x7f0a016c

    .line 116
    :goto_0
    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 120
    :goto_1
    return v0

    .line 113
    :cond_0
    const v0, 0x7f0a016d

    goto :goto_0

    .line 120
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 69
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 70
    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 71
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 73
    const v5, 0x7f0a0138

    invoke-virtual {p0, v5}, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 74
    const v0, 0x7f0a0335

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 75
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 76
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 77
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->mIntentStarter:Leoj;

    new-array v4, v1, [Landroid/content/Intent;

    aput-object v3, v4, v2

    invoke-interface {v0, v4}, Leoj;->b([Landroid/content/Intent;)Z

    move v0, v1

    .line 101
    :goto_0
    return v0

    .line 81
    :cond_0
    const v5, 0x7f0a0137

    invoke-virtual {p0, v5}, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 82
    invoke-static {}, Lgaq;->aEa()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v5, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    if-ne v6, v1, :cond_1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Landroid/content/pm/PackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    move v2, v1

    :cond_1
    if-eqz v2, :cond_2

    .line 83
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 84
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DL()Lcrh;

    move-result-object v2

    .line 85
    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->aus()Leqo;

    move-result-object v0

    .line 86
    invoke-virtual {v2}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v2

    .line 88
    new-instance v3, Lgar;

    invoke-direct {v3, v4, v0}, Lgar;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->mIntentStarter:Leoj;

    invoke-static {v2}, Lgaq;->J(Landroid/accounts/Account;)Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v0, v2, v3}, Leoj;->a(Landroid/content/Intent;Leol;)Z

    :goto_1
    move v0, v1

    .line 98
    goto :goto_0

    .line 94
    :cond_2
    const v0, 0x7f0a016b

    invoke-static {v3, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_3
    move v0, v2

    .line 101
    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->onResume()V

    .line 63
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->boa:Lcom/google/android/search/core/preferences/WrappingSwitchPreference;

    iget-object v1, p0, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;->aUh:Lcxs;

    invoke-virtual {v1}, Lcxs;->TH()Lcxi;

    move-result-object v1

    invoke-virtual {v1}, Lcxi;->TC()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/preferences/WrappingSwitchPreference;->setChecked(Z)V

    .line 65
    return-void
.end method
