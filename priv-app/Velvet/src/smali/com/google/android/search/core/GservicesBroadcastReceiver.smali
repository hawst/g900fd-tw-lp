.class public Lcom/google/android/search/core/GservicesBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 32
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 33
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    .line 35
    const-string v2, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 36
    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DO()Lgpp;

    move-result-object v0

    const-string v1, "update_gservices_config"

    invoke-interface {v0, v1}, Lgpp;->nw(Ljava/lang/String;)V

    .line 41
    :cond_0
    :goto_0
    return-void

    .line 38
    :cond_1
    const-string v2, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {v1}, Lgql;->aJq()Lhhq;

    move-result-object v0

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v3

    invoke-static {v3, v1}, Lgnq;->d(Ljze;Ljava/lang/String;)Ljzh;

    move-result-object v1

    invoke-virtual {v0}, Lhym;->aTH()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lhym;->JW()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lhym;->aTI()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v3, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-static {v3}, Lgnq;->an(Ljava/util/List;)V

    invoke-virtual {v0, v1, v3, v4}, Lhym;->b(Ljava/lang/String;Ljava/util/List;Z)V

    :goto_1
    invoke-static {v1}, Lcwy;->iY(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_2
    invoke-static {p1}, Lbwz;->ae(Landroid/content/Context;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0, v1, v4}, Lhym;->D(Ljava/lang/String;Z)V

    goto :goto_1
.end method
