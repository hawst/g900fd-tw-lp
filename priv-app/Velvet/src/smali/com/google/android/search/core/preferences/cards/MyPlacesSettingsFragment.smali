.class public Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;
.super Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Lefk;


# instance fields
.field private atJ:Landroid/widget/ProgressBar;

.field private bnT:Lizj;

.field private bnU:Landroid/preference/Preference;

.field private bnV:Lizj;

.field private bnW:Landroid/preference/Preference;

.field private bnX:Ldkp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;-><init>()V

    return-void
.end method

.method private TM()V
    .locals 3

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnT:Lizj;

    if-eqz v0, :cond_0

    .line 176
    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnU:Landroid/preference/Preference;

    .line 177
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnU:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnT:Lizj;

    const v2, 0x7f0a0082

    invoke-direct {p0, v1, v2}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->e(Lizj;I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnT:Lizj;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->a(Landroid/preference/Preference;Ljava/lang/String;Lizj;)V

    .line 179
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnU:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 180
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnU:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnV:Lizj;

    if-eqz v0, :cond_1

    .line 184
    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnW:Landroid/preference/Preference;

    .line 186
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnW:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnV:Lizj;

    const v2, 0x7f0a0172

    invoke-direct {p0, v1, v2}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->e(Lizj;I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnV:Lizj;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->a(Landroid/preference/Preference;Ljava/lang/String;Lizj;)V

    .line 188
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnW:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 189
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnW:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 191
    :cond_1
    return-void
.end method

.method private Ui()V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->atJ:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->atJ:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->atJ:Landroid/widget/ProgressBar;

    .line 172
    :cond_0
    return-void
.end method

.method private a(ILizj;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 226
    const/16 v0, 0x11

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/16 v2, 0x12

    aput v2, v1, v3

    invoke-static {p2, v0, v1}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v0

    .line 227
    invoke-static {p2, v0, p1, p3}, Lewx;->a(Lizj;Liwk;ILjava/lang/String;)Lewx;

    move-result-object v0

    .line 229
    invoke-virtual {v0, p0, v3}, Lewx;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 230
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "address_picker_tag"

    invoke-virtual {v0, v1, v2}, Lewx;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 231
    return-void
.end method

.method private a(Landroid/preference/Preference;Ljava/lang/String;Lizj;)V
    .locals 2

    .prologue
    .line 194
    invoke-virtual {p1, p2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 195
    invoke-static {p3}, Lgbf;->J(Lizj;)Ljava/lang/String;

    move-result-object v0

    .line 196
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 197
    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 202
    :goto_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    invoke-virtual {p1, p2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 205
    :cond_0
    return-void

    .line 199
    :cond_1
    const v0, 0x7f0a031a

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private static b(Landroid/os/Bundle;Ljava/lang/String;)Lizj;
    .locals 2

    .prologue
    .line 114
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    :try_start_0
    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v0, v1}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Lizj;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 281
    invoke-static {p1}, Lgbf;->K(Lizj;)Ljava/lang/String;

    move-result-object v0

    .line 282
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 283
    invoke-virtual {p0, p2}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 285
    :cond_0
    return-object v0
.end method


# virtual methods
.method protected final Ug()I
    .locals 1

    .prologue
    .line 148
    const v0, 0x7f07001b

    return v0
.end method

.method public final a(Lizj;Liwk;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 234
    invoke-static {p1}, Lgbf;->J(Lizj;)Ljava/lang/String;

    move-result-object v0

    .line 235
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p1, Lizj;->dSc:Ljal;

    iget-object v2, v2, Ljal;->dWL:Ljak;

    invoke-static {v1, v2}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v1

    .line 238
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0, p3}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {v1, p4}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 240
    :cond_1
    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    .line 241
    invoke-virtual {v0, p3}, Ljbp;->su(Ljava/lang/String;)Ljbp;

    .line 243
    invoke-virtual {p2}, Liwk;->getType()I

    move-result v0

    const/16 v1, 0x11

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnU:Landroid/preference/Preference;

    if-eqz v0, :cond_6

    .line 244
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnT:Lizj;

    iget-object v0, v0, Lizj;->dSc:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    iget-object v0, v0, Ljak;->aeB:Ljbp;

    invoke-virtual {v0, p3}, Ljbp;->su(Ljava/lang/String;)Ljbp;

    .line 248
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnU:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnT:Lizj;

    const v2, 0x7f0a0082

    invoke-direct {p0, v1, v2}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->e(Lizj;I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnT:Lizj;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->a(Landroid/preference/Preference;Ljava/lang/String;Lizj;)V

    .line 256
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "TRAINING_CLOSET_FETCHER"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lfhp;

    if-eqz v0, :cond_5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnT:Lizj;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnT:Lizj;

    invoke-static {v2}, Lgbf;->J(Lizj;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v1, 0x1

    :cond_3
    iget-object v2, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnV:Lizj;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnV:Lizj;

    invoke-static {v2}, Lgbf;->J(Lizj;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    add-int/lit8 v1, v1, 0x1

    :cond_4
    iput v1, v0, Lfhp;->crU:I

    .line 258
    :cond_5
    return-void

    .line 250
    :cond_6
    invoke-virtual {p2}, Liwk;->getType()I

    move-result v0

    const/16 v1, 0x12

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnW:Landroid/preference/Preference;

    if-eqz v0, :cond_2

    .line 251
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnV:Lizj;

    iget-object v0, v0, Lizj;->dSc:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    iget-object v0, v0, Ljak;->aeB:Ljbp;

    invoke-virtual {v0, p3}, Ljbp;->su(Ljava/lang/String;)Ljbp;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljbp;->st(Ljava/lang/String;)Ljbp;

    .line 253
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnW:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnV:Lizj;

    invoke-direct {p0, v0, p4, v1}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->a(Landroid/preference/Preference;Ljava/lang/String;Lizj;)V

    goto :goto_0
.end method

.method public final synthetic ar(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 37
    check-cast p1, Landroid/util/Pair;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnX:Ldkp;

    invoke-direct {p0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->Ui()V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "Error getting locations"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lizj;

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnT:Lizj;

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lizj;

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnV:Lizj;

    invoke-direct {p0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->TM()V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->getView()Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 84
    new-instance v1, Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->atJ:Landroid/widget/ProgressBar;

    .line 85
    iget-object v1, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->atJ:Landroid/widget/ProgressBar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 86
    iget-object v1, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->atJ:Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 89
    :cond_0
    if-eqz p1, :cond_1

    .line 90
    invoke-direct {p0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->Ui()V

    .line 91
    const-string v0, "HOME_ENTRY"

    invoke-static {p1, v0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->b(Landroid/os/Bundle;Ljava/lang/String;)Lizj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnT:Lizj;

    const-string v0, "WORK_ENTRY"

    invoke-static {p1, v0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->b(Landroid/os/Bundle;Ljava/lang/String;)Lizj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnV:Lizj;

    .line 92
    invoke-direct {p0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->TM()V

    .line 94
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 60
    invoke-super {p0, p1}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 66
    if-eqz p1, :cond_0

    const-string v0, "HOME_ENTRY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "WORK_ENTRY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 69
    :cond_0
    new-instance v0, Ldkp;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    invoke-virtual {v1}, Lgql;->aJr()Lfdb;

    move-result-object v1

    invoke-virtual {v1}, Lfdb;->axI()Lfcx;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Ldkp;-><init>(Lfcx;Lefk;)V

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnX:Ldkp;

    .line 72
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnX:Ldkp;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldkp;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 74
    :cond_1
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnX:Ldkp;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnX:Ldkp;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldkp;->cancel(Z)Z

    .line 143
    :cond_0
    invoke-super {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->onDestroy()V

    .line 144
    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 214
    iget-object v1, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnU:Landroid/preference/Preference;

    if-ne p1, v1, :cond_0

    .line 215
    const v1, 0x7f0a0316

    iget-object v2, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnT:Lizj;

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->a(ILizj;Ljava/lang/String;)V

    .line 222
    :goto_0
    return v0

    .line 217
    :cond_0
    iget-object v1, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnW:Landroid/preference/Preference;

    if-ne p1, v1, :cond_1

    .line 218
    const v1, 0x7f0a0318

    iget-object v2, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnV:Lizj;

    iget-object v3, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnV:Lizj;

    const v4, 0x7f0a0172

    invoke-direct {p0, v3, v4}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->e(Lizj;I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->a(ILizj;Ljava/lang/String;)V

    goto :goto_0

    .line 222
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 126
    invoke-super {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->onResume()V

    .line 131
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;

    if-eqz v0, :cond_0

    .line 132
    const v0, 0x7f0a0459

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 133
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->lO(Ljava/lang/String;)V

    .line 135
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 98
    invoke-super {p0, p1}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnT:Lizj;

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "HOME_ENTRY"

    iget-object v1, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnT:Lizj;

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnV:Lizj;

    if-eqz v0, :cond_1

    .line 104
    const-string v0, "WORK_ENTRY"

    iget-object v1, p0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;->bnV:Lizj;

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 106
    :cond_1
    return-void
.end method
