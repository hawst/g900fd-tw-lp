.class public Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private bPb:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bPc:Landroid/text/Spanned;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bPd:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bPe:Landroid/text/Spanned;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bPf:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bPg:Ljava/lang/Integer;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bPh:Z

.field private final bPi:Z

.field private final bPj:Z

.field private final bPk:I

.field private bse:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final mValue:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 438
    new-instance v0, Ldxa;

    invoke-direct {v0}, Ldxa;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ZZZI)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 340
    iput-object p1, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPb:Ljava/lang/String;

    .line 341
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPb:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 342
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPb:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPc:Landroid/text/Spanned;

    .line 346
    :goto_0
    iput-object p2, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPd:Ljava/lang/String;

    .line 347
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPd:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 348
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPd:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPe:Landroid/text/Spanned;

    .line 352
    :goto_1
    iput-object p3, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPf:Ljava/lang/String;

    .line 353
    iput-object p4, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPg:Ljava/lang/Integer;

    .line 354
    iput-object p5, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->mValue:Ljava/lang/String;

    .line 355
    iput-object p6, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bse:Ljava/lang/String;

    .line 356
    iput-boolean p7, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPh:Z

    .line 357
    iput-boolean p8, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPi:Z

    .line 358
    iput-boolean p9, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPj:Z

    .line 359
    iput p10, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPk:I

    .line 360
    return-void

    .line 344
    :cond_0
    iput-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPc:Landroid/text/Spanned;

    goto :goto_0

    .line 350
    :cond_1
    iput-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPe:Landroid/text/Spanned;

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;)Z
    .locals 1

    .prologue
    .line 312
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPj:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;)Ljqw;
    .locals 2

    .prologue
    .line 312
    new-instance v0, Ljqw;

    invoke-direct {v0}, Ljqw;-><init>()V

    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPb:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPb:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljqw;->ym(Ljava/lang/String;)Ljqw;

    :cond_0
    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPd:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPd:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljqw;->yn(Ljava/lang/String;)Ljqw;

    :cond_1
    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPf:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPf:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljqw;->yp(Ljava/lang/String;)Ljqw;

    :cond_2
    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPg:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPg:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljqw;->rD(I)Ljqw;

    :cond_3
    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->mValue:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->mValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljqw;->yo(Ljava/lang/String;)Ljqw;

    :cond_4
    return-object v0
.end method


# virtual methods
.method public final aki()Ljava/lang/Integer;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPg:Ljava/lang/Integer;

    return-object v0
.end method

.method public final akj()Z
    .locals 1

    .prologue
    .line 383
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPh:Z

    return v0
.end method

.method public final akk()Z
    .locals 1

    .prologue
    .line 387
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPi:Z

    return v0
.end method

.method public final akl()I
    .locals 1

    .prologue
    .line 391
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPk:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 420
    const/4 v0, 0x0

    return v0
.end method

.method public final getDescription()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPe:Landroid/text/Spanned;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPc:Landroid/text/Spanned;

    return-object v0
.end method

.method public final getType()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bse:Ljava/lang/String;

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public final oj()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPf:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 425
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPb:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 426
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPd:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 427
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPf:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 428
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPg:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 429
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->mValue:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 430
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bse:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 431
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPh:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 432
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPi:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 433
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPj:Z

    if-eqz v0, :cond_2

    :goto_2
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 434
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->bPk:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 435
    return-void

    :cond_0
    move v0, v2

    .line 431
    goto :goto_0

    :cond_1
    move v0, v2

    .line 432
    goto :goto_1

    :cond_2
    move v1, v2

    .line 433
    goto :goto_2
.end method
