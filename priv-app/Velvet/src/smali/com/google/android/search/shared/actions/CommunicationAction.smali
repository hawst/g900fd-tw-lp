.class public abstract Lcom/google/android/search/shared/actions/CommunicationAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# instance fields
.field protected bLV:Lcom/google/android/search/shared/contact/PersonDisambiguation;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bLW:Ldyc;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 30
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/CommunicationAction;->bLV:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/search/shared/actions/CommunicationAction;->bLV:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    .line 26
    return-void
.end method


# virtual methods
.method public final a(Ldyc;)V
    .locals 2

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/search/shared/actions/CommunicationAction;->bLW:Ldyc;

    .line 48
    iget-object v0, p0, Lcom/google/android/search/shared/actions/CommunicationAction;->bLV:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/google/android/search/shared/actions/CommunicationAction;->bLV:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    iget-object v1, p0, Lcom/google/android/search/shared/actions/CommunicationAction;->bLW:Ldyc;

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->b(Ldyc;)V

    .line 51
    :cond_0
    return-void
.end method

.method public abstract afZ()Ldzb;
.end method

.method public final agy()Ldxl;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Ldud;

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/CommunicationAction;->afZ()Ldzb;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ldud;-><init>(Lcom/google/android/search/shared/actions/CommunicationAction;Ldzb;)V

    return-object v0
.end method

.method public final ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/search/shared/actions/CommunicationAction;->bLV:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    return-object v0
.end method

.method public canExecute()Z
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/search/shared/actions/CommunicationAction;->bLV:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/CommunicationAction;->bLV:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/CommunicationAction;->bLV:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amv()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 76
    :goto_0
    return v0

    .line 71
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract i(Lcom/google/android/search/shared/contact/PersonDisambiguation;)Lcom/google/android/search/shared/actions/CommunicationAction;
.end method

.method public final j(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V
    .locals 2

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/search/shared/actions/CommunicationAction;->bLV:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    .line 63
    iget-object v0, p0, Lcom/google/android/search/shared/actions/CommunicationAction;->bLW:Ldyc;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/google/android/search/shared/actions/CommunicationAction;->bLV:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    iget-object v1, p0, Lcom/google/android/search/shared/actions/CommunicationAction;->bLW:Ldyc;

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->b(Ldyc;)V

    .line 65
    iget-object v0, p0, Lcom/google/android/search/shared/actions/CommunicationAction;->bLW:Ldyc;

    invoke-interface {v0, p1}, Ldyc;->a(Lcom/google/android/search/shared/actions/utils/Disambiguation;)V

    .line 67
    :cond_0
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 81
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 82
    iget-object v0, p0, Lcom/google/android/search/shared/actions/CommunicationAction;->bLV:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 83
    return-void
.end method
