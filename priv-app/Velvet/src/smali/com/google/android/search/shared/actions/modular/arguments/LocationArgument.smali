.class public Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;
.super Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bMi:Ljpe;

.field private final bPw:Ljpe;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bPx:Ljpx;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 217
    new-instance v0, Ldxh;

    invoke-direct {v0}, Ldxh;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljqg;Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;-><init>(Ljqg;Lcom/google/android/search/shared/actions/utils/Disambiguation;)V

    .line 48
    sget-object v0, Ljrb;->ezS:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljrb;

    .line 50
    iget-object v1, v0, Ljrb;->ezT:Ljpe;

    if-eqz v1, :cond_0

    iget-object v1, v0, Ljrb;->ezT:Ljpe;

    :goto_0
    iput-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->bMi:Ljpe;

    .line 52
    iget-object v1, v0, Ljrb;->ezV:Ljpx;

    iput-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->bPx:Ljpx;

    .line 53
    iget-object v0, v0, Ljrb;->ezU:Ljpe;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->bPw:Ljpe;

    .line 54
    return-void

    .line 50
    :cond_0
    new-instance v1, Ljpe;

    invoke-direct {v1}, Ljpe;-><init>()V

    goto :goto_0
.end method

.method public constructor <init>(Ljqg;Ljpe;)V
    .locals 3
    .param p2    # Ljpe;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 36
    if-eqz p2, :cond_0

    iget-object v0, p2, Ljpe;->exC:[Ljpd;

    if-nez v0, :cond_2

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;-><init>(Ljqg;Lcom/google/android/search/shared/actions/utils/Disambiguation;)V

    .line 37
    if-eqz p2, :cond_3

    :goto_1
    iput-object p2, p0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->bMi:Ljpe;

    .line 38
    sget-object v0, Ljrb;->ezS:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljrb;

    .line 40
    if-eqz v0, :cond_4

    iget-object v2, v0, Ljrb;->ezV:Ljpx;

    :goto_2
    iput-object v2, p0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->bPx:Ljpx;

    .line 41
    if-eqz v0, :cond_1

    iget-object v1, v0, Ljrb;->ezU:Ljpe;

    :cond_1
    iput-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->bPw:Ljpe;

    .line 42
    return-void

    .line 36
    :cond_2
    iget-object v2, p2, Ljpe;->exC:[Ljpd;

    new-instance v0, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;

    invoke-direct {v0, v2}, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;-><init>([Ljpd;)V

    goto :goto_0

    .line 37
    :cond_3
    new-instance p2, Ljpe;

    invoke-direct {p2}, Ljpe;-><init>()V

    goto :goto_1

    :cond_4
    move-object v2, v1

    .line 40
    goto :goto_2
.end method

.method private akv()Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;
    .locals 3

    .prologue
    .line 208
    new-instance v1, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->ajP()Ljqg;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->ahw()Ljpe;

    move-result-object v0

    invoke-static {v0}, Leqh;->f(Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Ljpe;

    invoke-direct {v1, v2, v0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;-><init>(Ljqg;Ljpe;)V

    return-object v1
.end method


# virtual methods
.method public final a(Ldwu;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 203
    invoke-interface {p1, p0}, Ldwu;->a(Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldwp;)Z
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->aiz()Ljpd;

    move-result-object v0

    .line 77
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljpd;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Ljpd;->esI:Ljne;

    if-eqz v0, :cond_0

    .line 80
    invoke-interface {p1, p0}, Ldwp;->g(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V

    .line 81
    const/4 v0, 0x0

    .line 83
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ahw()Ljpe;
    .locals 4

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;

    .line 113
    iget-object v2, p0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->bMi:Ljpe;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;->isCompleted()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljpd;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;->aky()Ljpd;

    move-result-object v0

    aput-object v0, v1, v3

    move-object v0, v1

    :goto_0
    iput-object v0, v2, Ljpe;->exC:[Ljpd;

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->bMi:Ljpe;

    return-object v0

    .line 113
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;->alu()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;->x(Ljava/util/List;)[Ljpd;

    move-result-object v0

    goto :goto_0
.end method

.method public final aiz()Ljpd;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;->isCompleted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;->aky()Ljpd;

    move-result-object v0

    .line 93
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic ajE()Lcom/google/android/search/shared/actions/modular/arguments/Argument;
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->akv()Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;

    move-result-object v0

    return-object v0
.end method

.method public final ajP()Ljqg;
    .locals 3

    .prologue
    .line 166
    invoke-super {p0}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->ajP()Ljqg;

    move-result-object v0

    .line 167
    new-instance v1, Ljrb;

    invoke-direct {v1}, Ljrb;-><init>()V

    .line 168
    sget-object v2, Ljrb;->ezS:Ljsm;

    invoke-virtual {v0, v2, v1}, Ljqg;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 169
    iget-object v2, p0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->bPx:Ljpx;

    iput-object v2, v1, Ljrb;->ezV:Ljpx;

    .line 170
    iget-object v2, p0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->bPw:Ljpe;

    iput-object v2, v1, Ljrb;->ezU:Ljpe;

    .line 171
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->Pd()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 172
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->ahw()Ljpe;

    move-result-object v2

    iput-object v2, v1, Ljrb;->ezT:Ljpe;

    .line 175
    :cond_0
    return-object v0
.end method

.method public final aks()Z
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->bPw:Ljpe;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->bPw:Ljpe;

    iget-object v0, v0, Ljpe;->exC:[Ljpd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->bPw:Ljpe;

    iget-object v0, v0, Ljpe;->exC:[Ljpd;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final akt()Ljpe;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->bPw:Ljpe;

    return-object v0
.end method

.method public final aku()Ljpx;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->bPx:Ljpx;

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->akv()Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljpd;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 100
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->Pd()Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;

    new-array v1, v3, [Ljpd;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-direct {v0, v1}, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;-><init>([Ljpd;)V

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->b(Lcom/google/android/search/shared/actions/utils/Disambiguation;)V

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;

    new-instance v1, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;

    invoke-direct {v1, p1}, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;-><init>(Ljpd;)V

    invoke-virtual {v0, v1, v3}, Lcom/google/android/search/shared/actions/modular/arguments/EcoutezLocalResultDisambiguation;->a(Landroid/os/Parcelable;Z)V

    .line 104
    return-void
.end method

.method public final gI(I)Z
    .locals 1

    .prologue
    .line 159
    const/16 v0, 0xa

    if-eq p1, v0, :cond_0

    const/16 v0, 0xb

    if-eq p1, v0, :cond_0

    const/16 v0, 0x16

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final h(Ljava/lang/Object;I)Ldws;
    .locals 2

    .prologue
    .line 139
    check-cast p1, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;->aky()Ljpd;

    move-result-object v1

    .line 140
    sparse-switch p2, :sswitch_data_0

    .line 154
    sget-object v0, Ldws;->bOO:Ldws;

    :goto_0
    return-object v0

    .line 142
    :sswitch_0
    invoke-virtual {v1}, Ljpd;->brG()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ldws;

    invoke-virtual {v1}, Ljpd;->brF()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_0

    .line 146
    :sswitch_1
    invoke-virtual {v1}, Ljpd;->bau()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ldws;

    invoke-virtual {v1}, Ljpd;->Ar()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_0

    .line 150
    :sswitch_2
    invoke-virtual {v1}, Ljpd;->pb()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ldws;

    invoke-virtual {v1}, Ljpd;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_0

    .line 140
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0xb -> :sswitch_1
        0x16 -> :sswitch_2
    .end sparse-switch
.end method

.method public final r(ZZ)Ljqg;
    .locals 6

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->akv()Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->ajP()Ljqg;

    move-result-object v1

    .line 183
    if-nez p2, :cond_1

    .line 184
    sget-object v0, Ljrb;->ezS:Ljsm;

    invoke-virtual {v1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljrb;

    .line 186
    const/4 v2, 0x0

    iput-object v2, v0, Ljrb;->ezU:Ljpe;

    .line 187
    iget-object v2, v0, Ljrb;->ezT:Ljpe;

    if-eqz v2, :cond_1

    .line 188
    iget-object v0, v0, Ljrb;->ezT:Ljpe;

    iget-object v2, v0, Ljpe;->exC:[Ljpd;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 189
    iget-object v5, v4, Ljpd;->esI:Ljne;

    if-eqz v5, :cond_0

    .line 191
    iget-object v5, v4, Ljpd;->esI:Ljne;

    .line 192
    invoke-virtual {v4}, Ljpd;->brP()Ljpd;

    .line 193
    iput-object v5, v4, Ljpd;->esI:Ljne;

    .line 188
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 198
    :cond_1
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->ajP()Ljqg;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 214
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 215
    return-void
.end method
