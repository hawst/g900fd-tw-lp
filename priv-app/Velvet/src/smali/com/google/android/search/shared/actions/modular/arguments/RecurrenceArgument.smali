.class public Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;
.super Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bPE:I

.field private final bPF:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 208
    new-instance v0, Ldxn;

    invoke-direct {v0}, Ldxn;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;)V

    .line 58
    iget v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->bPE:I

    iput v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->bPE:I

    .line 59
    iget v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->bPF:I

    iput v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->bPF:I

    .line 60
    return-void
.end method

.method public constructor <init>(Ljqg;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 44
    sget-object v0, Ljrd;->eAe:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljrd;

    iget-object v0, v0, Ljrd;->eqD:Lier;

    invoke-static {v0}, Ldxx;->a(Lier;)Laiw;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;-><init>(Ljqg;Ljava/lang/Object;)V

    .line 47
    sget-object v0, Ljrd;->eAe:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljrd;

    .line 50
    invoke-virtual {v0}, Ljrd;->bsZ()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljrd;->bsY()I

    move-result v1

    :goto_0
    iput v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->bPE:I

    .line 52
    invoke-virtual {v0}, Ljrd;->btb()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljrd;->bta()I

    move-result v2

    :cond_0
    iput v2, p0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->bPF:I

    .line 54
    return-void

    :cond_1
    move v1, v2

    .line 50
    goto :goto_0
.end method

.method private akG()Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;
    .locals 1

    .prologue
    .line 116
    new-instance v0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;

    invoke-direct {v0, p0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ldwu;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 99
    invoke-interface {p1, p0}, Ldwu;->a(Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final ajC()Z
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic ajE()Lcom/google/android/search/shared/actions/modular/arguments/Argument;
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->akG()Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;

    move-result-object v0

    return-object v0
.end method

.method public final ajP()Ljqg;
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 187
    invoke-super {p0}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->ajP()Ljqg;

    move-result-object v1

    .line 188
    new-instance v2, Ljrd;

    invoke-direct {v2}, Ljrd;-><init>()V

    .line 190
    sget-object v0, Ljrd;->eAe:Ljsm;

    invoke-virtual {v1, v0, v2}, Ljqg;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 192
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Laiw;

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->ajy()J

    move-result-wide v4

    const/4 v3, 0x0

    invoke-static {v0, v4, v5, v3}, Ldxx;->a(Laiw;JLdyf;)Lier;

    move-result-object v0

    iput-object v0, v2, Ljrd;->eqD:Lier;

    .line 194
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->bPE:I

    if-eq v0, v6, :cond_0

    .line 195
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->bPE:I

    invoke-virtual {v2, v0}, Ljrd;->rG(I)Ljrd;

    .line 197
    :cond_0
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->bPF:I

    if-eq v0, v6, :cond_1

    .line 198
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->bPF:I

    invoke-virtual {v2, v0}, Ljrd;->rH(I)Ljrd;

    .line 200
    :cond_1
    return-object v1
.end method

.method public final ajy()J
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 164
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->akJ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->akK()Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    move-result-object v0

    .line 166
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akS()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->ajy()J

    move-result-wide v0

    .line 182
    :goto_0
    return-wide v0

    .line 171
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->akH()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->akI()Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    move-result-object v0

    .line 173
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->getCalendar()Ljava/util/Calendar;

    move-result-object v0

    .line 179
    :goto_1
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 180
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 181
    const/16 v1, 0xb

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 182
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    goto :goto_0

    .line 176
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    goto :goto_1
.end method

.method protected final akF()V
    .locals 2

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->akH()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->akI()Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    move-result-object v0

    new-instance v1, Ldxm;

    invoke-direct {v1, p0}, Ldxm;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->a(Ldwn;)V

    .line 90
    :cond_0
    return-void
.end method

.method protected akH()Z
    .locals 2

    .prologue
    .line 129
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->bPE:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;

    iget v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->bPE:I

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected akI()Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;
    .locals 2

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->akH()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 136
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;

    iget v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->bPE:I

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    return-object v0
.end method

.method protected akJ()Z
    .locals 2

    .prologue
    .line 144
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->bPF:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;

    iget v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->bPF:I

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected akK()Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;
    .locals 2

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->akJ()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 151
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;

    iget v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->bPF:I

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    return-object v0
.end method

.method public final b(Ljqu;Landroid/content/res/Resources;)Ldws;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Ldws;->bOO:Ldws;

    return-object v0
.end method

.method public final c(Lcom/google/android/search/shared/actions/modular/ModularAction;)V
    .locals 0

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->c(Lcom/google/android/search/shared/actions/modular/ModularAction;)V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->akF()V

    .line 66
    return-void
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->akG()Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;

    move-result-object v0

    return-object v0
.end method

.method public final f(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z
    .locals 1
    .param p1    # Lcom/google/android/search/shared/actions/modular/arguments/Argument;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 121
    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->h(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z

    move-result v0

    return v0
.end method

.method public final gI(I)Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->ajP()Ljqg;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 206
    return-void
.end method
