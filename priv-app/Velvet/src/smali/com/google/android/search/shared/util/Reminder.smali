.class public Lcom/google/android/search/shared/util/Reminder;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final bWy:Lcom/google/android/search/shared/util/Reminder;


# instance fields
.field private final bWA:I

.field private final bWz:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 16
    new-instance v0, Lcom/google/android/search/shared/util/Reminder;

    const/16 v1, 0xf

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/search/shared/util/Reminder;-><init>(II)V

    sput-object v0, Lcom/google/android/search/shared/util/Reminder;->bWy:Lcom/google/android/search/shared/util/Reminder;

    .line 66
    new-instance v0, Leed;

    invoke-direct {v0}, Leed;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/util/Reminder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput p1, p0, Lcom/google/android/search/shared/util/Reminder;->bWz:I

    .line 24
    iput p2, p0, Lcom/google/android/search/shared/util/Reminder;->bWA:I

    .line 25
    return-void
.end method


# virtual methods
.method public final aoa()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/google/android/search/shared/util/Reminder;->bWz:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method

.method public final getMethod()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/google/android/search/shared/util/Reminder;->bWA:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/google/android/search/shared/util/Reminder;->bWz:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 63
    iget v0, p0, Lcom/google/android/search/shared/util/Reminder;->bWA:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 64
    return-void
.end method
