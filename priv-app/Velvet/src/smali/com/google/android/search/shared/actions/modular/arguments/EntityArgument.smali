.class public Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;
.super Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bOZ:Ljpw;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bPa:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 458
    new-instance v0, Ldwz;

    invoke-direct {v0}, Ldwz;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;)V
    .locals 2

    .prologue
    .line 73
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    iget-object v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    invoke-direct {v1, v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;-><init>(Lcom/google/android/search/shared/actions/utils/Disambiguation;)V

    move-object v0, v1

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;Lcom/google/android/search/shared/actions/utils/Disambiguation;)V

    .line 74
    iget-object v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->bOZ:Ljpw;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->bOZ:Ljpw;

    .line 75
    iget-object v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->bPa:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->bPa:Ljava/lang/String;

    .line 76
    return-void

    .line 73
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljqg;)V
    .locals 1

    .prologue
    .line 51
    invoke-static {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->b(Ljqg;)Lcom/google/android/search/shared/actions/utils/Disambiguation;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;-><init>(Ljqg;Lcom/google/android/search/shared/actions/utils/Disambiguation;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Ljqg;Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 65
    new-instance v1, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    sget-object v0, Ljqq;->ezg:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqq;

    invoke-virtual {v0}, Ljqq;->getQuery()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;

    aput-object p2, v2, v3

    invoke-static {v2}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/search/shared/actions/utils/Disambiguation;-><init>(Ljava/lang/String;Ljava/util/List;Z)V

    invoke-direct {p0, p1, v1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;-><init>(Ljqg;Lcom/google/android/search/shared/actions/utils/Disambiguation;)V

    .line 70
    return-void
.end method

.method public constructor <init>(Ljqg;Lcom/google/android/search/shared/actions/utils/Disambiguation;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;-><init>(Ljqg;Lcom/google/android/search/shared/actions/utils/Disambiguation;)V

    .line 56
    sget-object v0, Ljqq;->ezg:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqq;

    iget-object v0, v0, Ljqq;->ezi:Ljpw;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->bOZ:Ljpw;

    .line 58
    return-void
.end method

.method private akg()Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;
    .locals 1

    .prologue
    .line 240
    new-instance v0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;

    invoke-direct {v0, p0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;)V

    return-object v0
.end method

.method private static b(Ljqg;)Lcom/google/android/search/shared/actions/utils/Disambiguation;
    .locals 17

    .prologue
    .line 291
    sget-object v1, Ljqq;->ezg:Ljsm;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 292
    const/4 v1, 0x0

    .line 309
    :goto_0
    return-object v1

    .line 294
    :cond_0
    sget-object v1, Ljqq;->ezg:Ljsm;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Ljqq;

    .line 297
    iget-object v1, v12, Ljqq;->ezh:[Ljqw;

    array-length v1, v1

    invoke-static {v1}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v14

    .line 298
    iget-object v15, v12, Ljqq;->ezh:[Ljqw;

    array-length v0, v15

    move/from16 v16, v0

    const/4 v1, 0x0

    move v13, v1

    :goto_1
    move/from16 v0, v16

    if-ge v13, v0, :cond_3

    aget-object v7, v15, v13

    .line 299
    new-instance v1, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;

    invoke-virtual {v7}, Ljqw;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Ljqw;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, Ljqw;->oj()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7}, Ljqw;->bsC()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v7}, Ljqw;->bsy()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    :goto_2
    invoke-virtual {v7}, Ljqw;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, Ljqw;->oP()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v7}, Ljqw;->getType()Ljava/lang/String;

    move-result-object v7

    :goto_3
    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v1 .. v11}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ZZZI)V

    invoke-interface {v14, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 298
    add-int/lit8 v1, v13, 0x1

    move v13, v1

    goto :goto_1

    .line 299
    :cond_1
    const/4 v5, 0x0

    goto :goto_2

    :cond_2
    invoke-virtual {v12}, Ljqq;->getType()Ljava/lang/String;

    move-result-object v7

    goto :goto_3

    .line 309
    :cond_3
    new-instance v1, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    invoke-virtual {v12}, Ljqq;->getQuery()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v14, v3}, Lcom/google/android/search/shared/actions/utils/Disambiguation;-><init>(Ljava/lang/String;Ljava/util/List;Z)V

    goto :goto_0
.end method

.method private et(Z)Ljqg;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 178
    invoke-super {p0}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->ajP()Ljqg;

    move-result-object v3

    .line 180
    new-instance v4, Ljqq;

    invoke-direct {v4}, Ljqq;-><init>()V

    .line 182
    sget-object v0, Ljqq;->ezg:Ljsm;

    invoke-virtual {v3, v0, v4}, Ljqg;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 183
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->bOZ:Ljpw;

    iput-object v0, v4, Ljqq;->ezi:Ljpw;

    .line 185
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->Pd()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v3

    .line 212
    :goto_0
    return-object v0

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    .line 190
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 191
    if-eqz v2, :cond_2

    .line 192
    invoke-virtual {v4, v2}, Ljqq;->yj(Ljava/lang/String;)Ljqq;

    .line 195
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->isCompleted()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 200
    const/4 v2, 0x1

    new-array v2, v2, [Ljqw;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;

    invoke-static {v0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->c(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;)Ljqw;

    move-result-object v0

    aput-object v0, v2, v1

    iput-object v2, v4, Ljqq;->ezh:[Ljqw;

    :cond_3
    move-object v0, v3

    .line 212
    goto :goto_0

    .line 204
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alu()Ljava/util/List;

    move-result-object v2

    .line 205
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljqw;

    iput-object v2, v4, Ljqq;->ezh:[Ljqw;

    .line 207
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alu()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;

    .line 208
    iget-object v6, v4, Ljqq;->ezh:[Ljqw;

    add-int/lit8 v2, v1, 0x1

    invoke-static {v0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->c(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;)Ljqw;

    move-result-object v0

    aput-object v0, v6, v1

    move v1, v2

    .line 209
    goto :goto_1
.end method


# virtual methods
.method public final a(Ldwu;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 235
    invoke-interface {p1, p0}, Ldwu;->b(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Legu;)V
    .locals 2

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->akh()Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;

    move-result-object v0

    .line 272
    if-eqz v0, :cond_0

    .line 273
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->akj()Z

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->akk()Z

    move-result v0

    invoke-virtual {p1, v1, v0}, Legu;->u(ZZ)V

    .line 275
    :cond_0
    return-void
.end method

.method public final a(Ljng;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 90
    sget-object v0, Ljnh;->euO:Ljsm;

    invoke-virtual {p1, v0}, Ljng;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljnh;

    .line 92
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljnh;->bqF()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move v0, v1

    .line 108
    :goto_0
    return v0

    .line 95
    :cond_1
    invoke-virtual {v0}, Ljnh;->bqE()I

    move-result v0

    .line 96
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->akh()Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;

    move-result-object v3

    .line 97
    if-nez v3, :cond_2

    move v0, v1

    .line 98
    goto :goto_0

    .line 100
    :cond_2
    const/4 v4, 0x2

    if-ne v0, v4, :cond_3

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->akj()Z

    move-result v4

    if-nez v4, :cond_3

    move v0, v1

    .line 102
    goto :goto_0

    .line 104
    :cond_3
    if-ne v0, v2, :cond_4

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->akk()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 106
    goto :goto_0

    :cond_4
    move v0, v2

    .line 108
    goto :goto_0
.end method

.method public final a(Ljqh;)[Ljrg;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->ajC()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ljqp;->eze:Ljsm;

    invoke-virtual {p1, v0}, Ljqh;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 222
    :cond_0
    const/4 v0, 0x0

    .line 227
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Ljqp;->eze:Ljsm;

    invoke-virtual {p1, v0}, Ljqh;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqp;

    iget-object v1, v0, Ljqp;->ezf:[Ljrg;

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alu()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_1
    invoke-static {v1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->b([Ljrg;I)[Ljrg;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final synthetic ajE()Lcom/google/android/search/shared/actions/modular/arguments/Argument;
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->akg()Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;

    move-result-object v0

    return-object v0
.end method

.method public final ajN()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->bPa:Ljava/lang/String;

    return-object v0
.end method

.method public final ajO()Z
    .locals 1

    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->ajO()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ajP()Ljqg;
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->et(Z)Ljqg;

    move-result-object v0

    return-object v0
.end method

.method public final ajk()I
    .locals 2

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->akh()Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;

    move-result-object v0

    .line 114
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->akl()I

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->akl()I

    move-result v0

    .line 117
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->ajk()I

    move-result v0

    goto :goto_0
.end method

.method public final ake()Ljpw;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->bOZ:Ljpw;

    return-object v0
.end method

.method public final akf()I
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->akh()Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;

    move-result-object v0

    .line 127
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->b(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    const/4 v0, 0x2

    .line 132
    :goto_0
    return v0

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->bOZ:Ljpw;

    if-nez v0, :cond_1

    .line 130
    const/4 v0, 0x0

    goto :goto_0

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->bOZ:Ljpw;

    invoke-virtual {v0}, Ljpw;->akf()I

    move-result v0

    goto :goto_0
.end method

.method public final akh()Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    .line 266
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->isCompleted()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->akg()Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;

    move-result-object v0

    return-object v0
.end method

.method public final gI(I)Z
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x6

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1a

    if-eq p1, v0, :cond_0

    const/16 v0, 0x18

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final h(Ljava/lang/Object;I)Ldws;
    .locals 2

    .prologue
    .line 137
    check-cast p1, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;

    .line 138
    sparse-switch p2, :sswitch_data_0

    .line 161
    sget-object v0, Ldws;->bOO:Ldws;

    :goto_0
    return-object v0

    .line 140
    :sswitch_0
    new-instance v0, Ldws;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 142
    :sswitch_1
    iget-object v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->mValue:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ldws;

    iget-object v1, p1, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->mValue:Ljava/lang/String;

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_0

    .line 149
    :sswitch_2
    iget-object v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->mValue:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 150
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_0

    .line 152
    :cond_1
    iget-object v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->mValue:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Leeh;->E(Landroid/net/Uri;)Landroid/content/ComponentName;

    move-result-object v1

    .line 154
    if-eqz v1, :cond_2

    new-instance v0, Ldws;

    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_0

    .line 158
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->getType()Ljava/lang/String;

    move-result-object v1

    .line 159
    if-nez v1, :cond_3

    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_0

    :cond_3
    new-instance v0, Ldws;

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 138
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x6 -> :sswitch_0
        0x18 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final kp(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 254
    iput-object p1, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->bPa:Ljava/lang/String;

    .line 255
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 280
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->et(Z)Ljqg;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 281
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 282
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->bPa:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 283
    return-void
.end method
