.class public Lcom/google/android/search/shared/actions/RemoveRelationshipAction;
.super Lcom/google/android/search/shared/actions/AbstractRelationshipAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lduz;

    invoke-direct {v0}, Lduz;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/RemoveRelationshipAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractRelationshipAction;-><init>(Landroid/os/Parcel;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractRelationshipAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    .line 17
    return-void
.end method


# virtual methods
.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/RemoveRelationshipAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final agc()I
    .locals 1

    .prologue
    .line 35
    const/16 v0, 0x2f

    return v0
.end method

.method public final i(Lcom/google/android/search/shared/contact/PersonDisambiguation;)Lcom/google/android/search/shared/actions/CommunicationAction;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/search/shared/actions/RemoveRelationshipAction;

    invoke-direct {v0, p1}, Lcom/google/android/search/shared/actions/RemoveRelationshipAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    return-object v0
.end method
