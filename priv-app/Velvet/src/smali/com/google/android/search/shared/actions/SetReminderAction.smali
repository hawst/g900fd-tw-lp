.class public Lcom/google/android/search/shared/actions/SetReminderAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private aav:Laiw;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final apP:Lcom/google/android/search/shared/actions/RemindersConfigFlags;

.field private bMW:Ljava/lang/String;

.field private bNc:J

.field private bNd:Ljava/lang/String;

.field private bNe:J

.field private bNf:Z

.field private bNg:Ldyf;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bNh:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bNi:Ljkt;

.field private bNj:Ljava/lang/String;

.field private bNk:I

.field private bNl:Ljpd;

.field private bNm:Ljpd;

.field private bNn:Ljpd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 595
    new-instance v0, Ldvd;

    invoke-direct {v0}, Ldvd;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/SetReminderAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 95
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 61
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNf:Z

    .line 82
    invoke-static {v1}, Lcom/google/android/search/shared/actions/SetReminderAction;->gA(I)Ljpd;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNm:Ljpd;

    .line 83
    invoke-static {v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->gA(I)Ljpd;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNn:Ljpd;

    .line 96
    new-instance v2, Lcom/google/android/search/shared/actions/RemindersConfigFlags;

    invoke-direct {v2, p1}, Lcom/google/android/search/shared/actions/RemindersConfigFlags;-><init>(Landroid/os/Parcel;)V

    iput-object v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->apP:Lcom/google/android/search/shared/actions/RemindersConfigFlags;

    .line 97
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNc:J

    .line 98
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/SetReminderAction;->aip()V

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNd:Ljava/lang/String;

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNe:J

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 104
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 105
    invoke-static {v2}, Ldyf;->gW(I)Ldyf;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/search/shared/actions/SetReminderAction;->a(Ldyf;)V

    .line 107
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    if-eqz v2, :cond_3

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNf:Z

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bMW:Ljava/lang/String;

    .line 110
    const-class v0, Ljkt;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljkt;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNi:Ljkt;

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNj:Ljava/lang/String;

    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNk:I

    .line 115
    const-class v0, Ljpd;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljpd;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNl:Ljpd;

    .line 117
    const-class v0, Ljpd;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljpd;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNm:Ljpd;

    .line 119
    const-class v0, Ljpd;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljpd;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNn:Ljpd;

    .line 122
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 123
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 125
    :try_start_0
    new-instance v1, Laiw;

    invoke-direct {v1}, Laiw;-><init>()V

    .line 126
    invoke-virtual {v1, v0}, Laiw;->parse(Ljava/lang/String;)V

    .line 127
    iput-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->aav:Laiw;
    :try_end_0
    .catch Laix; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 133
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 134
    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNh:Ljava/lang/String;

    .line 136
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 107
    goto :goto_0

    .line 128
    :catch_0
    move-exception v0

    .line 129
    const-string v1, "SetReminderAction"

    const-string v2, "Failed to parse recurrence"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public constructor <init>(Lcom/google/android/search/shared/actions/RemindersConfigFlags;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 85
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 61
    iput-boolean v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNf:Z

    .line 82
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->gA(I)Ljpd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNm:Ljpd;

    .line 83
    invoke-static {v2}, Lcom/google/android/search/shared/actions/SetReminderAction;->gA(I)Ljpd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNn:Ljpd;

    .line 86
    iput-object p1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->apP:Lcom/google/android/search/shared/actions/RemindersConfigFlags;

    .line 87
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNc:J

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bMW:Ljava/lang/String;

    .line 89
    iput v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNk:I

    .line 90
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/SetReminderAction;->aip()V

    .line 91
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/SetReminderAction;->aix()V

    .line 92
    return-void
.end method

.method public static a(Ljkw;Lcom/google/android/search/shared/actions/RemindersConfigFlags;)Lcom/google/android/search/shared/actions/SetReminderAction;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 146
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    new-instance v6, Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-direct {v6, p1}, Lcom/google/android/search/shared/actions/SetReminderAction;-><init>(Lcom/google/android/search/shared/actions/RemindersConfigFlags;)V

    .line 149
    invoke-virtual {p0}, Ljkw;->bhU()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v6, Lcom/google/android/search/shared/actions/SetReminderAction;->bNd:Ljava/lang/String;

    .line 150
    invoke-virtual {p0}, Ljkw;->getLabel()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v6, Lcom/google/android/search/shared/actions/SetReminderAction;->bMW:Ljava/lang/String;

    .line 151
    invoke-virtual {p0}, Ljkw;->aiw()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v6, Lcom/google/android/search/shared/actions/SetReminderAction;->bNj:Ljava/lang/String;

    .line 153
    iget-object v3, p0, Ljkw;->eqB:Ljkt;

    if-eqz v3, :cond_0

    .line 154
    iget-object v3, p0, Ljkw;->eqB:Ljkt;

    iput-object v3, v6, Lcom/google/android/search/shared/actions/SetReminderAction;->bNi:Ljkt;

    .line 157
    :cond_0
    iget-object v3, p0, Ljkw;->eqA:Ljls;

    if-eqz v3, :cond_1

    .line 160
    iget-object v3, p0, Ljkw;->eqA:Ljls;

    invoke-virtual {v6, v3}, Lcom/google/android/search/shared/actions/SetReminderAction;->a(Ljls;)V

    .line 163
    :cond_1
    invoke-virtual {p0}, Ljkw;->aiy()I

    move-result v3

    const/4 v7, 0x3

    if-ne v3, v7, :cond_2

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->aii()Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v0

    .line 165
    :goto_0
    packed-switch v3, :pswitch_data_0

    :goto_1
    :pswitch_0
    move v0, v3

    .line 206
    :goto_2
    iput v0, v6, Lcom/google/android/search/shared/actions/SetReminderAction;->bNk:I

    .line 208
    return-object v6

    .line 163
    :cond_2
    iget-object v3, p0, Ljkw;->eqz:Ljkr;

    if-nez v3, :cond_3

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->aih()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Ljkw;->eqD:Lier;

    if-eqz v3, :cond_4

    :cond_3
    move v3, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Ljkw;->eqA:Ljls;

    if-eqz v3, :cond_5

    iget-object v3, p0, Ljkw;->eqA:Ljls;

    iget-object v7, v3, Ljls;->esL:[Ljln;

    array-length v7, v7

    if-lez v7, :cond_5

    iget-object v3, v3, Ljls;->esL:[Ljln;

    aget-object v3, v3, v5

    iget-object v3, v3, Ljln;->esu:[Ljpd;

    array-length v3, v3

    if-lez v3, :cond_5

    move v3, v4

    goto :goto_0

    :cond_5
    move v3, v5

    goto :goto_0

    .line 167
    :pswitch_1
    invoke-virtual {v6}, Lcom/google/android/search/shared/actions/SetReminderAction;->aip()V

    .line 168
    invoke-virtual {v6}, Lcom/google/android/search/shared/actions/SetReminderAction;->aix()V

    move v0, v2

    .line 170
    goto :goto_2

    .line 173
    :pswitch_2
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->aih()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 174
    iget-object v0, p0, Ljkw;->eqD:Lier;

    invoke-static {v0}, Ldxx;->a(Lier;)Laiw;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/search/shared/actions/SetReminderAction;->aav:Laiw;

    .line 176
    iget-object v0, p0, Ljkw;->eqE:Liey;

    if-eqz v0, :cond_6

    iget-object v0, p0, Ljkw;->eqE:Liey;

    invoke-virtual {v0}, Liey;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 178
    iget-object v0, p0, Ljkw;->eqE:Liey;

    invoke-virtual {v0}, Liey;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/search/shared/actions/SetReminderAction;->bNh:Ljava/lang/String;

    .line 180
    :cond_6
    iget-wide v8, v6, Lcom/google/android/search/shared/actions/SetReminderAction;->bNc:J

    iget-object v0, p0, Ljkw;->eqz:Ljkr;

    if-eqz v0, :cond_9

    iget-object v0, p0, Ljkw;->eqz:Ljkr;

    .line 184
    :goto_3
    const-string v5, "The timeTrigger can\'t be NULL."

    invoke-static {v0, v5}, Lifv;->o(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    invoke-virtual {v0}, Ljkr;->Pu()J

    move-result-wide v8

    invoke-virtual {v0}, Ljkr;->boo()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-virtual {v0}, Ljkr;->bon()I

    move-result v1

    invoke-static {v1}, Ldyf;->gW(I)Ldyf;

    move-result-object v1

    :cond_7
    invoke-virtual {v0}, Ljkr;->bop()Z

    move-result v0

    iput-wide v8, v6, Lcom/google/android/search/shared/actions/SetReminderAction;->bNe:J

    iput-boolean v0, v6, Lcom/google/android/search/shared/actions/SetReminderAction;->bNf:Z

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v6, v1}, Lcom/google/android/search/shared/actions/SetReminderAction;->a(Ldyf;)V

    if-nez v1, :cond_8

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/16 v5, 0xc

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v6, v1, v5}, Lcom/google/android/search/shared/actions/SetReminderAction;->p(II)V

    :cond_8
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v6, v1, v2, v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->r(III)V

    .line 193
    invoke-virtual {v6}, Lcom/google/android/search/shared/actions/SetReminderAction;->aix()V

    move v0, v3

    .line 194
    goto/16 :goto_2

    .line 180
    :cond_9
    iget-object v0, p0, Ljkw;->eqD:Lier;

    if-nez v0, :cond_a

    move-object v0, v1

    goto :goto_3

    :cond_a
    iget-object v0, p0, Ljkw;->eqD:Lier;

    invoke-static {v8, v9, v0}, Ldxx;->a(JLier;)Ljkr;

    move-result-object v0

    goto :goto_3

    .line 182
    :cond_b
    iget-object v0, p0, Ljkw;->eqz:Ljkr;

    goto :goto_3

    .line 196
    :pswitch_3
    iget-object v0, p0, Ljkw;->eqA:Ljls;

    .line 197
    iget-object v0, v0, Ljls;->esL:[Ljln;

    aget-object v0, v0, v5

    iget-object v0, v0, Ljln;->esu:[Ljpd;

    aget-object v0, v0, v5

    iput-object v0, v6, Lcom/google/android/search/shared/actions/SetReminderAction;->bNl:Ljpd;

    .line 200
    invoke-virtual {v6}, Lcom/google/android/search/shared/actions/SetReminderAction;->aip()V

    move v0, v3

    .line 201
    goto/16 :goto_2

    .line 203
    :pswitch_4
    iput v0, v6, Lcom/google/android/search/shared/actions/SetReminderAction;->bNk:I

    goto/16 :goto_1

    .line 165
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private a([Ljpd;)V
    .locals 4

    .prologue
    .line 477
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    .line 478
    iget-object v3, v2, Ljpd;->esI:Ljne;

    if-eqz v3, :cond_0

    .line 479
    iget-object v3, v2, Ljpd;->esI:Ljne;

    invoke-virtual {v3}, Ljne;->bqz()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 477
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 481
    :pswitch_0
    iput-object v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNm:Ljpd;

    goto :goto_1

    .line 484
    :pswitch_1
    iput-object v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNn:Ljpd;

    goto :goto_1

    .line 489
    :cond_1
    return-void

    .line 479
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static gA(I)Ljpd;
    .locals 2

    .prologue
    .line 636
    new-instance v0, Ljpd;

    invoke-direct {v0}, Ljpd;-><init>()V

    .line 637
    new-instance v1, Ljne;

    invoke-direct {v1}, Ljne;-><init>()V

    invoke-virtual {v1, p0}, Ljne;->qP(I)Ljne;

    move-result-object v1

    iput-object v1, v0, Ljpd;->esI:Ljne;

    .line 638
    return-object v0
.end method


# virtual methods
.method public final Wb()Z
    .locals 1

    .prologue
    .line 514
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 524
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/SetReminderAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldyf;)V
    .locals 6

    .prologue
    .line 325
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/SetReminderAction;->air()Z

    move-result v0

    iget-wide v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNe:J

    iget-wide v4, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNc:J

    invoke-static {v0, v2, v3, v4, v5}, Ldxw;->a(ZJJ)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->b(Ldyf;Ljava/util/List;)V

    .line 327
    return-void
.end method

.method public final a(Letj;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 549
    const-string v0, "SetReminderAction"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 550
    const-string v0, "mRemindersConfigFlags"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->apP:Lcom/google/android/search/shared/actions/RemindersConfigFlags;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 551
    const-string v0, "mSetUpTimeMs"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNc:J

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 552
    const-string v0, "mDefaultDateTime"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNf:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 553
    const-string v0, "mOriginalTaskId"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNd:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 554
    const-string v0, "mDateTimeMs"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNe:J

    invoke-virtual {v0, v2, v3}, Letn;->bd(J)V

    .line 555
    const-string v0, "mSymbolicTime"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    invoke-virtual {v0, v1}, Letn;->e(Ljava/lang/Enum;)V

    .line 556
    const-string v0, "mRecurrence"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->aav:Laiw;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 557
    const-string v0, "mRecurrenceId"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNh:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 558
    const-string v0, "mLabel"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bMW:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 559
    const-string v0, "mEmbeddedAction"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNi:Ljkt;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 560
    const-string v0, "mConfirmationUrlPath"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNj:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 561
    const-string v0, "mTriggerType"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNk:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 562
    const-string v0, "mLocation"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNl:Ljpd;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 563
    const-string v0, "mHomeLocation"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNm:Ljpd;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 564
    const-string v0, "mWorkLocation"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNn:Ljpd;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 565
    return-void
.end method

.method public final a(Ljls;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 451
    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNm:Ljpd;

    .line 452
    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNn:Ljpd;

    .line 454
    iget-object v0, p1, Ljls;->esM:Ljln;

    if-eqz v0, :cond_0

    .line 455
    iget-object v0, p1, Ljls;->esM:Ljln;

    iget-object v0, v0, Ljln;->esu:[Ljpd;

    invoke-direct {p0, v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->a([Ljpd;)V

    .line 458
    :cond_0
    iget-object v2, p1, Ljls;->esL:[Ljln;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 459
    iget-object v5, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNm:Ljpd;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNn:Ljpd;

    if-nez v5, :cond_2

    .line 460
    :cond_1
    iget-object v4, v4, Ljln;->esu:[Ljpd;

    invoke-direct {p0, v4}, Lcom/google/android/search/shared/actions/SetReminderAction;->a([Ljpd;)V

    .line 458
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 464
    :cond_2
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNm:Ljpd;

    if-nez v0, :cond_3

    .line 465
    invoke-static {v1}, Lcom/google/android/search/shared/actions/SetReminderAction;->gA(I)Ljpd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNm:Ljpd;

    .line 467
    :cond_3
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNn:Ljpd;

    if-nez v0, :cond_4

    .line 468
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->gA(I)Ljpd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNn:Ljpd;

    .line 470
    :cond_4
    return-void
.end method

.method public final agb()Ldtw;
    .locals 1

    .prologue
    .line 519
    sget-object v0, Ldtw;->bLp:Ldtw;

    return-object v0
.end method

.method public final aiA()Ljpd;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNm:Ljpd;

    return-object v0
.end method

.method public final aiB()Ljpd;
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNn:Ljpd;

    return-object v0
.end method

.method public final aiC()Ljkw;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 651
    new-instance v0, Ljkw;

    invoke-direct {v0}, Ljkw;-><init>()V

    .line 652
    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNd:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 653
    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNd:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljkw;->wv(Ljava/lang/String;)Ljkw;

    .line 655
    :cond_0
    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNh:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 656
    new-instance v1, Liey;

    invoke-direct {v1}, Liey;-><init>()V

    iget-object v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNh:Ljava/lang/String;

    invoke-virtual {v1, v2}, Liey;->pn(Ljava/lang/String;)Liey;

    move-result-object v1

    iput-object v1, v0, Ljkw;->eqE:Liey;

    .line 658
    :cond_1
    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bMW:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 659
    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bMW:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljkw;->wt(Ljava/lang/String;)Ljkw;

    .line 662
    :cond_2
    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNj:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 663
    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljkw;->wu(Ljava/lang/String;)Ljkw;

    .line 666
    :cond_3
    iget v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNk:I

    if-ne v1, v6, :cond_a

    .line 667
    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->apP:Lcom/google/android/search/shared/actions/RemindersConfigFlags;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->aih()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->aav:Laiw;

    if-nez v1, :cond_9

    .line 668
    :cond_4
    new-instance v1, Ljkr;

    invoke-direct {v1}, Ljkr;-><init>()V

    .line 669
    iget-wide v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNe:J

    invoke-virtual {v1, v2, v3}, Ljkr;->dz(J)Ljkr;

    .line 670
    iget-object v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    if-eqz v2, :cond_5

    .line 671
    iget-object v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    iget v2, v2, Ldyf;->bQI:I

    invoke-virtual {v1, v2}, Ljkr;->qw(I)Ljkr;

    .line 673
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNf:Z

    invoke-virtual {v1, v2}, Ljkr;->ir(Z)Ljkr;

    .line 674
    iput-object v1, v0, Ljkw;->eqz:Ljkr;

    .line 682
    :cond_6
    :goto_0
    invoke-virtual {v0, v5}, Ljkw;->qz(I)Ljkw;

    .line 695
    :cond_7
    :goto_1
    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNi:Ljkt;

    if-eqz v1, :cond_8

    .line 696
    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNi:Ljkt;

    iput-object v1, v0, Ljkw;->eqB:Ljkt;

    .line 699
    :cond_8
    return-object v0

    .line 676
    :cond_9
    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->aav:Laiw;

    iget-wide v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNe:J

    iget-object v4, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    invoke-static {v1, v2, v3, v4}, Ldxx;->a(Laiw;JLdyf;)Lier;

    move-result-object v1

    .line 678
    if-eqz v1, :cond_6

    .line 679
    iput-object v1, v0, Ljkw;->eqD:Lier;

    goto :goto_0

    .line 683
    :cond_a
    iget v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNk:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_b

    .line 684
    new-instance v1, Ljls;

    invoke-direct {v1}, Ljls;-><init>()V

    .line 685
    invoke-virtual {v1, v5}, Ljls;->qK(I)Ljls;

    .line 686
    new-instance v2, Ljln;

    invoke-direct {v2}, Ljln;-><init>()V

    .line 687
    new-array v3, v6, [Ljpd;

    iget-object v4, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNl:Ljpd;

    aput-object v4, v3, v5

    iput-object v3, v2, Ljln;->esu:[Ljpd;

    .line 688
    new-array v3, v6, [Ljln;

    aput-object v2, v3, v5

    iput-object v3, v1, Ljls;->esL:[Ljln;

    .line 689
    iput-object v1, v0, Ljkw;->eqA:Ljls;

    .line 690
    invoke-virtual {v0, v6}, Ljkw;->qz(I)Ljkw;

    goto :goto_1

    .line 691
    :cond_b
    iget v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNk:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_7

    .line 692
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljkw;->qz(I)Ljkw;

    goto :goto_1
.end method

.method public final aim()Lcom/google/android/search/shared/actions/RemindersConfigFlags;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->apP:Lcom/google/android/search/shared/actions/RemindersConfigFlags;

    return-object v0
.end method

.method public final ain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNd:Ljava/lang/String;

    return-object v0
.end method

.method public final aio()J
    .locals 2

    .prologue
    .line 245
    iget-wide v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNe:J

    return-wide v0
.end method

.method public final aip()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 281
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    .line 286
    sget-object v2, Ldyf;->bQC:Ldyf;

    iget v2, v2, Ldyf;->bQH:I

    if-ge v0, v2, :cond_1

    .line 287
    sget-object v0, Ldyf;->bQC:Ldyf;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    move v0, v1

    .line 300
    :goto_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 301
    if-nez v0, :cond_0

    .line 303
    const/4 v0, 0x6

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->add(II)V

    .line 305
    :cond_0
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNe:J

    .line 306
    iput-boolean v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNf:Z

    .line 307
    return-void

    .line 288
    :cond_1
    sget-object v2, Ldyf;->bQD:Ldyf;

    iget v2, v2, Ldyf;->bQH:I

    if-ge v0, v2, :cond_2

    .line 289
    sget-object v0, Ldyf;->bQD:Ldyf;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    move v0, v1

    goto :goto_0

    .line 290
    :cond_2
    sget-object v2, Ldyf;->bQE:Ldyf;

    iget v2, v2, Ldyf;->bQH:I

    if-ge v0, v2, :cond_3

    .line 291
    sget-object v0, Ldyf;->bQE:Ldyf;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    move v0, v1

    goto :goto_0

    .line 292
    :cond_3
    sget-object v2, Ldyf;->bQF:Ldyf;

    iget v2, v2, Ldyf;->bQH:I

    if-ge v0, v2, :cond_4

    .line 293
    sget-object v0, Ldyf;->bQF:Ldyf;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    move v0, v1

    goto :goto_0

    .line 296
    :cond_4
    const/4 v0, 0x0

    .line 297
    sget-object v2, Ldyf;->bQC:Ldyf;

    iput-object v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    goto :goto_0
.end method

.method public final aiq()Ldyf;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    return-object v0
.end method

.method public final air()Z
    .locals 2

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->aav:Laiw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->aav:Laiw;

    iget v0, v0, Laiw;->ZU:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->aav:Laiw;

    iget v0, v0, Laiw;->ZU:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->aav:Laiw;

    iget v0, v0, Laiw;->ZU:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->aav:Laiw;

    iget v0, v0, Laiw;->ZU:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ais()Laiw;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->aav:Laiw;

    return-object v0
.end method

.method public final ait()Ljava/lang/String;
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNh:Ljava/lang/String;

    return-object v0
.end method

.method public final aiu()J
    .locals 2

    .prologue
    .line 359
    iget-wide v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNc:J

    return-wide v0
.end method

.method public final aiv()Ljkt;
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNi:Ljkt;

    return-object v0
.end method

.method public final aiw()Ljava/lang/String;
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNj:Ljava/lang/String;

    return-object v0
.end method

.method public final aix()V
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNm:Ljpd;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNl:Ljpd;

    .line 429
    return-void
.end method

.method public final aiy()I
    .locals 1

    .prologue
    .line 432
    iget v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNk:I

    return v0
.end method

.method public final aiz()Ljpd;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNl:Ljpd;

    return-object v0
.end method

.method public final ax(J)V
    .locals 1

    .prologue
    .line 249
    iput-wide p1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNe:J

    .line 250
    return-void
.end method

.method public final ay(J)V
    .locals 1

    .prologue
    .line 364
    iput-wide p1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNc:J

    .line 365
    return-void
.end method

.method public final b(Laiw;)V
    .locals 0

    .prologue
    .line 346
    iput-object p1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->aav:Laiw;

    .line 347
    return-void
.end method

.method public final b(Ldyf;Ljava/util/List;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 368
    if-eqz p1, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 369
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    .line 375
    :goto_0
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    invoke-interface {p2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 377
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    if-eqz v0, :cond_2

    .line 379
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    iget v0, v0, Ldyf;->bQH:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/search/shared/actions/SetReminderAction;->p(II)V

    .line 381
    :cond_2
    return-void

    .line 370
    :cond_3
    invoke-interface {p2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 371
    iput-object p1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    goto :goto_0

    .line 373
    :cond_4
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyf;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    goto :goto_0

    :cond_5
    move v0, v1

    .line 375
    goto :goto_1
.end method

.method public final b(Ljkt;)V
    .locals 0

    .prologue
    .line 412
    iput-object p1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNi:Ljkt;

    .line 413
    return-void
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 509
    const/4 v0, 0x1

    return v0
.end method

.method public final dj(I)V
    .locals 0

    .prologue
    .line 436
    iput p1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNk:I

    .line 437
    return-void
.end method

.method public final e(Ljpd;)V
    .locals 0

    .prologue
    .line 444
    iput-object p1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNl:Ljpd;

    .line 445
    return-void
.end method

.method public final em(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 404
    iput-object p1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bMW:Ljava/lang/String;

    .line 405
    return-void
.end method

.method public final f(Ljpd;)V
    .locals 0

    .prologue
    .line 496
    iput-object p1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNm:Ljpd;

    .line 497
    return-void
.end method

.method public final g(Ljpd;)V
    .locals 0

    .prologue
    .line 504
    iput-object p1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNn:Ljpd;

    .line 505
    return-void
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bMW:Ljava/lang/String;

    return-object v0
.end method

.method public final km(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNd:Ljava/lang/String;

    .line 242
    return-void
.end method

.method public final kn(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 355
    iput-object p1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNh:Ljava/lang/String;

    .line 356
    return-void
.end method

.method public final p(II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 388
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 389
    iget-wide v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNe:J

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 391
    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    .line 392
    const/16 v1, 0xc

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 393
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v4}, Ljava/util/Calendar;->set(II)V

    .line 394
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v4}, Ljava/util/Calendar;->set(II)V

    .line 396
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNe:J

    .line 397
    return-void
.end method

.method public final r(III)V
    .locals 4

    .prologue
    .line 310
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 311
    iget-wide v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNe:J

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 313
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    .line 314
    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 315
    const/4 v1, 0x5

    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->set(II)V

    .line 317
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNe:J

    .line 318
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 529
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SetReminderAction(mRemindersConfigFlags="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->apP:Lcom/google/android/search/shared/actions/RemindersConfigFlags;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSetUpTimeMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNc:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mDefaultDateTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNf:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mOriginalTaskId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNd:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mDateTimeMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNe:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSymbolicTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mRecurrence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->aav:Laiw;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mRecurrenceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNh:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLabel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bMW:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mEmbeddedAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNi:Ljkt;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", confirmationUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTriggerType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNk:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNl:Ljpd;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mHomeLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNm:Ljpd;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mWorkLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNn:Ljpd;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 569
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 570
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->apP:Lcom/google/android/search/shared/actions/RemindersConfigFlags;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->writeToParcel(Landroid/os/Parcel;I)V

    .line 571
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNd:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 573
    iget-wide v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNe:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 574
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 575
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNf:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 577
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bMW:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 578
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNi:Ljkt;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 579
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNj:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 581
    iget v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNk:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 583
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNl:Ljpd;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 584
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNm:Ljpd;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 585
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNn:Ljpd;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 587
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->aav:Laiw;

    if-eqz v0, :cond_2

    .line 588
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->aav:Laiw;

    invoke-virtual {v0}, Laiw;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 592
    :goto_2
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNh:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 593
    return-void

    .line 574
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetReminderAction;->bNg:Ldyf;

    iget v0, v0, Ldyf;->bQI:I

    goto :goto_0

    .line 575
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 590
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2
.end method
