.class public Lcom/google/android/search/shared/actions/AddRelationshipAction;
.super Lcom/google/android/search/shared/actions/AbstractRelationshipAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Ldty;

    invoke-direct {v0}, Ldty;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/AddRelationshipAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractRelationshipAction;-><init>(Landroid/os/Parcel;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractRelationshipAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    .line 18
    return-void
.end method


# virtual methods
.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/AddRelationshipAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final agc()I
    .locals 1

    .prologue
    .line 36
    const/16 v0, 0x2e

    return v0
.end method

.method public final i(Lcom/google/android/search/shared/contact/PersonDisambiguation;)Lcom/google/android/search/shared/actions/CommunicationAction;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/search/shared/actions/AddRelationshipAction;

    invoke-direct {v0, p1}, Lcom/google/android/search/shared/actions/AddRelationshipAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    return-object v0
.end method
