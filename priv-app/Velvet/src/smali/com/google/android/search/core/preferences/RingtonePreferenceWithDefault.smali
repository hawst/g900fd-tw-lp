.class public Lcom/google/android/search/core/preferences/RingtonePreferenceWithDefault;
.super Landroid/preference/RingtonePreference;
.source "PG"


# instance fields
.field private bmA:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Landroid/preference/RingtonePreference;-><init>(Landroid/content/Context;)V

    .line 22
    invoke-direct {p0}, Lcom/google/android/search/core/preferences/RingtonePreferenceWithDefault;->ed()V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/preference/RingtonePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    invoke-direct {p0}, Lcom/google/android/search/core/preferences/RingtonePreferenceWithDefault;->ed()V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/RingtonePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    invoke-direct {p0}, Lcom/google/android/search/core/preferences/RingtonePreferenceWithDefault;->ed()V

    .line 33
    return-void
.end method

.method private ed()V
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/RingtonePreferenceWithDefault;->getShowDefault()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/search/core/preferences/RingtonePreferenceWithDefault;->bmA:Z

    if-nez v0, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/RingtonePreferenceWithDefault;->getRingtoneType()I

    move-result v0

    invoke-static {v0}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v0

    .line 38
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/RingtonePreferenceWithDefault;->setDefaultValue(Ljava/lang/Object;)V

    .line 40
    :cond_0
    return-void

    .line 38
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/core/preferences/RingtonePreferenceWithDefault;->bmA:Z

    .line 46
    invoke-super {p0, p1, p2}, Landroid/preference/RingtonePreference;->onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
