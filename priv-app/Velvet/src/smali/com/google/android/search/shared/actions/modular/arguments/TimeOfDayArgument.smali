.class public Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;
.super Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bPE:I

.field private final bPQ:Ljpz;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bPR:[Ljrk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 253
    new-instance v0, Ldxu;

    invoke-direct {v0}, Ldxu;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;)V

    .line 58
    iget-object v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPR:[Ljrk;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPR:[Ljrk;

    .line 59
    iget-object v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPQ:Ljpz;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPQ:Ljpz;

    .line 60
    iget v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPE:I

    iput v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPE:I

    .line 61
    return-void
.end method

.method public constructor <init>(Ljqg;)V
    .locals 2

    .prologue
    .line 46
    sget-object v0, Ljrj;->eAA:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljrj;

    iget-object v0, v0, Ljrj;->eAB:Ljrk;

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;-><init>(Ljqg;Ljava/lang/Object;)V

    .line 48
    sget-object v0, Ljrj;->eAA:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljrj;

    .line 50
    iget-object v1, v0, Ljrj;->eAC:[Ljrk;

    iput-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPR:[Ljrk;

    .line 51
    iget-object v1, v0, Ljrj;->eAD:Ljpz;

    iput-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPQ:Ljpz;

    .line 52
    invoke-virtual {v0}, Ljrj;->bsZ()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljrj;->bsY()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPE:I

    .line 54
    return-void

    .line 52
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private akV()Z
    .locals 3

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOD:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljng;

    .line 174
    sget-object v2, Ljni;->euQ:Ljsm;

    invoke-virtual {v0, v2}, Ljng;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljni;

    .line 176
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljni;->bqG()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    const/4 v0, 0x1

    .line 181
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private akW()Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;
    .locals 1

    .prologue
    .line 230
    new-instance v0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    invoke-direct {v0, p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;)V

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;)Z
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akV()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljkh;)J
    .locals 3
    .param p1    # Ljkh;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akT()Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->getCalendar()Ljava/util/Calendar;

    move-result-object v0

    .line 151
    :goto_0
    const/16 v1, 0xd

    invoke-virtual {p1}, Ljkh;->getSecond()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 152
    const/16 v1, 0xc

    invoke-virtual {p1}, Ljkh;->getMinute()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 153
    const/16 v1, 0xb

    invoke-virtual {p1}, Ljkh;->getHour()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 154
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0

    .line 149
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ldwu;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 191
    invoke-interface {p1, p0}, Ldwu;->a(Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljrk;)V
    .locals 4

    .prologue
    .line 87
    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPR:[Ljrk;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 88
    if-ne v3, p1, :cond_0

    .line 89
    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->setValue(Ljava/lang/Object;)V

    .line 95
    :goto_1
    return-void

    .line 87
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 94
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->setValue(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final ajA()[Ljrk;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPR:[Ljrk;

    return-object v0
.end method

.method public final ajB()Z
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x1

    return v0
.end method

.method public final ajC()Z
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic ajE()Lcom/google/android/search/shared/actions/modular/arguments/Argument;
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akW()Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    move-result-object v0

    return-object v0
.end method

.method public final ajH()I
    .locals 2

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akV()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akS()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->ajy()J

    move-result-wide v0

    invoke-static {v0, v1}, Lesi;->ba(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    const/4 v0, 0x1

    .line 168
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ajP()Ljqg;
    .locals 4

    .prologue
    .line 235
    invoke-super {p0}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->ajP()Ljqg;

    move-result-object v1

    .line 236
    new-instance v2, Ljrj;

    invoke-direct {v2}, Ljrj;-><init>()V

    .line 238
    sget-object v0, Ljrj;->eAA:Ljsm;

    invoke-virtual {v1, v0, v2}, Ljqg;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 239
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljsr;

    invoke-static {v0}, Leqh;->f(Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Ljrk;

    iput-object v0, v2, Ljrj;->eAB:Ljrk;

    .line 240
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPR:[Ljrk;

    iput-object v0, v2, Ljrj;->eAC:[Ljrk;

    .line 241
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPQ:Ljpz;

    iput-object v0, v2, Ljrj;->eAD:Ljpz;

    .line 242
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPE:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 243
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPE:I

    invoke-virtual {v2, v0}, Ljrj;->rQ(I)Ljrj;

    .line 245
    :cond_0
    return-object v1
.end method

.method public final ajx()Ljrk;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljrk;

    return-object v0
.end method

.method public final ajy()J
    .locals 2

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akS()Z

    move-result v0

    const-string v1, "Cannot get time in ms for symbolic time."

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljrk;

    iget-object v0, v0, Ljrk;->eAF:Ljkh;

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->a(Ljkh;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final ajz()Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPR:[Ljrk;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected akH()Z
    .locals 2

    .prologue
    .line 120
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPE:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;

    iget v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPE:I

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final akS()Z
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljrk;

    iget-object v0, v0, Ljrk;->eAF:Ljkh;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public akT()Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;
    .locals 2

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akH()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 126
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;

    iget v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->bPE:I

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    return-object v0
.end method

.method protected final akU()Z
    .locals 1

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akH()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akT()Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljqu;Landroid/content/res/Resources;)Ldws;
    .locals 2

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akS()Z

    move-result v0

    if-nez v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->ajP()Ljqg;

    .line 199
    sget-object v0, Ldws;->bOO:Ldws;

    .line 220
    :goto_0
    return-object v0

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljrk;

    iget-object v1, v0, Ljrk;->eAF:Ljkh;

    .line 202
    invoke-virtual {p1}, Ljqu;->bsE()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 216
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akU()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 217
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->b(Ljqu;Landroid/content/res/Resources;)Ldws;

    move-result-object v0

    goto :goto_0

    .line 204
    :sswitch_0
    new-instance v0, Ldws;

    invoke-virtual {v1}, Ljkh;->getHour()I

    move-result v1

    invoke-direct {v0, v1}, Ldws;-><init>(I)V

    goto :goto_0

    .line 206
    :sswitch_1
    new-instance v0, Ldws;

    invoke-virtual {v1}, Ljkh;->getMinute()I

    move-result v1

    invoke-direct {v0, v1}, Ldws;-><init>(I)V

    goto :goto_0

    .line 209
    :sswitch_2
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akU()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, Ljqu;->ezz:Ljqm;

    if-eqz v0, :cond_1

    iget-object v0, p1, Ljqu;->ezz:Ljqm;

    invoke-virtual {v0}, Ljqm;->bsv()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 212
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->b(Ljqu;Landroid/content/res/Resources;)Ldws;

    move-result-object v0

    goto :goto_0

    .line 220
    :cond_3
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_0

    .line 202
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x11 -> :sswitch_1
        0x19 -> :sswitch_2
    .end sparse-switch
.end method

.method public final c(Lcom/google/android/search/shared/actions/modular/ModularAction;)V
    .locals 2

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->c(Lcom/google/android/search/shared/actions/modular/ModularAction;)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akH()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akT()Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    move-result-object v0

    new-instance v1, Ldxt;

    invoke-direct {v1, p0}, Ldxt;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->a(Ldwn;)V

    .line 78
    :cond_0
    return-void
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akW()Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    move-result-object v0

    return-object v0
.end method

.method public final f(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z
    .locals 1
    .param p1    # Lcom/google/android/search/shared/actions/modular/arguments/Argument;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 225
    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->h(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->ajP()Ljqg;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 251
    return-void
.end method
