.class public abstract Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/search/shared/actions/VoiceAction;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ParcelCreator"
    }
.end annotation


# static fields
.field private static final bLa:Ljava/util/Set;


# instance fields
.field private bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

.field private bLc:Z

.field private bLd:J

.field private bLe:Lcom/google/android/shared/util/MatchingAppInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 25
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v1, 0x48

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0xe

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/16 v4, 0x32

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lijp;->c(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lijp;

    move-result-object v0

    sput-object v0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLa:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLc:Z

    .line 44
    new-instance v0, Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-direct {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLc:Z

    .line 52
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 53
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/ActionExecutionState;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    .line 54
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/MatchingAppInfo;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLe:Lcom/google/android/shared/util/MatchingAppInfo;

    .line 55
    return-void
.end method


# virtual methods
.method public Wb()Z
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    return v0
.end method

.method public Wc()Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    return v0
.end method

.method public final a(ILcom/google/android/search/shared/actions/utils/CardDecision;I)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 100
    .line 101
    sget-object v1, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLa:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alg()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 102
    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->ali()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 105
    :goto_0
    const/4 v2, 0x4

    if-ne p1, v2, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v2

    .line 107
    if-eqz v2, :cond_0

    .line 108
    invoke-virtual {v2}, Lcom/google/android/shared/util/MatchingAppInfo;->avi()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 109
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 122
    :cond_0
    :goto_1
    invoke-static {p1}, Lege;->hs(I)Litu;

    move-result-object v2

    invoke-virtual {v2, p3}, Litu;->mC(I)Litu;

    move-result-object v2

    .line 123
    if-eqz v1, :cond_1

    .line 124
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Litu;->mI(I)Litu;

    .line 126
    :cond_1
    if-eqz v0, :cond_2

    .line 127
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Litu;->mJ(I)Litu;

    .line 129
    :cond_2
    invoke-static {v2}, Lege;->a(Litu;)V

    .line 130
    return-void

    .line 110
    :cond_3
    invoke-virtual {v2}, Lcom/google/android/shared/util/MatchingAppInfo;->avh()I

    move-result v0

    if-ne v0, v4, :cond_4

    .line 111
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 113
    :cond_4
    invoke-virtual {v2}, Lcom/google/android/shared/util/MatchingAppInfo;->avd()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 114
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 116
    :cond_5
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    :cond_6
    move-object v1, v0

    goto :goto_0
.end method

.method public a(Lcom/google/android/shared/util/MatchingAppInfo;)V
    .locals 0

    .prologue
    .line 258
    iput-object p1, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLe:Lcom/google/android/shared/util/MatchingAppInfo;

    .line 259
    return-void
.end method

.method public a(Legu;)V
    .locals 0

    .prologue
    .line 135
    return-void
.end method

.method public a(Letj;)V
    .locals 1

    .prologue
    .line 300
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 301
    return-void
.end method

.method public aga()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    return v0
.end method

.method public agb()Ldtw;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Ldtw;->bLj:Ldtw;

    return-object v0
.end method

.method public agc()I
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method

.method public final agd()Z
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agd()Z

    move-result v0

    return v0
.end method

.method public final age()Z
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agd()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agK()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->agf()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected agf()Z
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    return v0
.end method

.method public agg()Lcom/google/android/search/shared/actions/ActionExecutionState;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    return-object v0
.end method

.method public final agh()Z
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agA()Z

    move-result v0

    return v0
.end method

.method public final agi()Z
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agB()Z

    move-result v0

    return v0
.end method

.method public final agj()Z
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agC()Z

    move-result v0

    return v0
.end method

.method public final agk()Z
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agD()Z

    move-result v0

    return v0
.end method

.method public final agl()Z
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agE()Z

    move-result v0

    return v0
.end method

.method public final agm()Z
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agF()Z

    move-result v0

    return v0
.end method

.method public final agn()Z
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agG()Z

    move-result v0

    return v0
.end method

.method public final ago()Z
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->yo()Z

    move-result v0

    return v0
.end method

.method public final agp()Z
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agH()Z

    move-result v0

    return v0
.end method

.method public final agq()Z
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->isDone()Z

    move-result v0

    return v0
.end method

.method public final agr()Z
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agI()Z

    move-result v0

    return v0
.end method

.method public final ags()Z
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->isCanceled()Z

    move-result v0

    return v0
.end method

.method public final agt()Z
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agJ()Z

    move-result v0

    return v0
.end method

.method public agu()Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLe:Lcom/google/android/shared/util/MatchingAppInfo;

    return-object v0
.end method

.method public final agv()V
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLc:Z

    .line 269
    return-void
.end method

.method public final agw()Z
    .locals 1

    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLc:Z

    return v0
.end method

.method public final agx()J
    .locals 2

    .prologue
    .line 283
    iget-wide v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLd:J

    return-wide v0
.end method

.method public agy()Ldxl;
    .locals 1

    .prologue
    .line 288
    const/4 v0, 0x0

    return-object v0
.end method

.method public final agz()Z
    .locals 2

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    .line 294
    iget-object v1, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/ActionExecutionState;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agI()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/ActionExecutionState;->isDone()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/ActionExecutionState;->agJ()Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/shared/util/MatchingAppInfo;->avi()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aw(J)V
    .locals 1

    .prologue
    .line 278
    iput-wide p1, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLd:J

    .line 279
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    return v0
.end method

.method public final ei(Z)Z
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/actions/ActionExecutionState;->ek(Z)Z

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLb:Lcom/google/android/search/shared/actions/ActionExecutionState;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 142
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->bLe:Lcom/google/android/shared/util/MatchingAppInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 143
    return-void
.end method
