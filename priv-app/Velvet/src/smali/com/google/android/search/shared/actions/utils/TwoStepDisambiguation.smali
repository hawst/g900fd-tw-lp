.class public abstract Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;
.super Lcom/google/android/search/shared/actions/utils/Disambiguation;
.source "PG"


# instance fields
.field private bQK:Ljava/util/List;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bQL:Landroid/os/Parcelable;


# direct methods
.method public constructor <init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/google/android/search/shared/actions/utils/Disambiguation;-><init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V

    .line 56
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQK:Ljava/util/List;

    .line 57
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQL:Landroid/os/Parcelable;

    .line 58
    return-void
.end method

.method public constructor <init>(Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/utils/Disambiguation;-><init>(Lcom/google/android/search/shared/actions/utils/Disambiguation;)V

    .line 49
    iget-object v0, p1, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQK:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQK:Ljava/util/List;

    .line 50
    iget-object v0, p1, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQL:Landroid/os/Parcelable;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQL:Landroid/os/Parcelable;

    .line 51
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;-><init>(Ljava/lang/String;Ljava/util/List;ZZ)V

    .line 45
    return-void
.end method

.method private A(Ljava/util/List;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 98
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->alB()Z

    move-result v0

    if-nez v0, :cond_1

    .line 99
    :cond_0
    iput-object v2, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQK:Ljava/util/List;

    .line 100
    iput-object v2, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQL:Landroid/os/Parcelable;

    .line 109
    :goto_0
    return-void

    .line 103
    :cond_1
    iput-object p1, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQK:Ljava/util/List;

    .line 104
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQK:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 105
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQK:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-direct {p0, v0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->c(Landroid/os/Parcelable;)V

    goto :goto_0

    .line 107
    :cond_2
    iput-object v2, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQL:Landroid/os/Parcelable;

    goto :goto_0
.end method

.method private b(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->d(Landroid/os/Parcelable;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->e(Landroid/os/Parcelable;)Ljava/util/List;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->A(Ljava/util/List;)V

    .line 91
    return-void

    .line 90
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 123
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQL:Landroid/os/Parcelable;

    .line 124
    return-void
.end method


# virtual methods
.method protected final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->a(Landroid/os/Parcelable;)V

    .line 75
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->b(Landroid/os/Parcelable;)V

    .line 76
    return-void
.end method

.method public final alD()Ljava/util/List;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQK:Ljava/util/List;

    return-object v0
.end method

.method public final alE()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 157
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->alB()Z

    move-result v1

    if-nez v1, :cond_1

    .line 160
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->d(Landroid/os/Parcelable;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQK:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQK:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final alF()Landroid/os/Parcelable;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQL:Landroid/os/Parcelable;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method

.method protected final alG()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 180
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQL:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 181
    :goto_0
    iput-object v1, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQL:Landroid/os/Parcelable;

    .line 182
    iput-object v1, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQK:Ljava/util/List;

    .line 183
    return v0

    .line 180
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final alt()V
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->alB()Z

    move-result v0

    if-nez v0, :cond_1

    .line 64
    invoke-super {p0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alt()V

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQL:Landroid/os/Parcelable;

    if-nez v0, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->b(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method protected final alx()Z
    .locals 2

    .prologue
    .line 174
    invoke-super {p0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alx()Z

    move-result v0

    .line 175
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->alG()Z

    move-result v1

    or-int/2addr v0, v1

    .line 176
    return v0
.end method

.method public final aly()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 188
    iget-object v1, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQK:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQK:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v0, :cond_1

    :cond_0
    invoke-super {p0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->aly()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/os/Parcelable;Z)V
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->c(Landroid/os/Parcelable;)V

    .line 116
    if-eqz p2, :cond_0

    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQt:Z

    .line 119
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->PM()V

    .line 120
    return-void
.end method

.method public final d(Ljava/util/List;Z)V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->A(Ljava/util/List;)V

    .line 83
    if-eqz p2, :cond_0

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQt:Z

    .line 86
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->PM()V

    .line 87
    return-void
.end method

.method protected abstract d(Landroid/os/Parcelable;)Z
.end method

.method public final d(Lcom/google/android/search/shared/actions/utils/Disambiguation;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 209
    invoke-super {p0, p1}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->d(Lcom/google/android/search/shared/actions/utils/Disambiguation;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 237
    :cond_0
    :goto_0
    return v1

    .line 214
    :cond_1
    instance-of v2, p1, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;

    if-eqz v2, :cond_0

    .line 218
    check-cast p1, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;

    .line 219
    iget-object v2, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQL:Landroid/os/Parcelable;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQL:Landroid/os/Parcelable;

    iget-object v3, p1, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQL:Landroid/os/Parcelable;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v1, v0

    .line 224
    goto :goto_0

    .line 226
    :cond_2
    iget-object v2, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQK:Ljava/util/List;

    iget-object v3, p1, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQK:Ljava/util/List;

    invoke-static {v2, v3}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 235
    iget-object v2, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQL:Landroid/os/Parcelable;

    if-nez v2, :cond_3

    iget-object v2, p1, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQL:Landroid/os/Parcelable;

    if-nez v2, :cond_3

    :goto_1
    move v1, v0

    .line 237
    goto :goto_0

    :cond_3
    move v0, v1

    .line 235
    goto :goto_1
.end method

.method protected abstract e(Landroid/os/Parcelable;)Ljava/util/List;
.end method

.method public isCompleted()Z
    .locals 1

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->alB()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQL:Landroid/os/Parcelable;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->d(Landroid/os/Parcelable;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOngoing()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 143
    invoke-super {p0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->isOngoing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 149
    :cond_0
    :goto_0
    return v0

    .line 145
    :cond_1
    iget-object v2, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQL:Landroid/os/Parcelable;

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->alB()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 146
    goto :goto_0

    .line 149
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->d(Landroid/os/Parcelable;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQK:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQK:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 246
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->writeToParcel(Landroid/os/Parcel;I)V

    .line 247
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQK:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 248
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->bQL:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 249
    return-void
.end method
