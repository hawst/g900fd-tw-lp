.class public Lcom/google/android/search/shared/actions/PhoneCallAction;
.super Lcom/google/android/search/shared/actions/CommunicationAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private bMh:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    new-instance v0, Ldur;

    invoke-direct {v0}, Ldur;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/PhoneCallAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/CommunicationAction;-><init>(Landroid/os/Parcel;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/CommunicationAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    .line 23
    return-void
.end method


# virtual methods
.method public final Wb()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 56
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/PhoneCallAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Letj;)V
    .locals 3

    .prologue
    .line 66
    const-string v0, "PhoneCallAction"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 67
    const-string v0, "recipient"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/PhoneCallAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 68
    return-void
.end method

.method public final afZ()Ldzb;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Ldzb;->bRq:Ldzb;

    return-object v0
.end method

.method public final agb()Ldtw;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Ldtw;->bLl:Ldtw;

    return-object v0
.end method

.method public final agc()I
    .locals 1

    .prologue
    .line 41
    const/16 v0, 0xa

    return v0
.end method

.method public final ahy()V
    .locals 2

    .prologue
    .line 84
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/search/shared/actions/PhoneCallAction;->bMh:J

    .line 85
    return-void
.end method

.method public final ahz()Z
    .locals 4

    .prologue
    .line 89
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/search/shared/actions/PhoneCallAction;->bMh:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x7d0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i(Lcom/google/android/search/shared/contact/PersonDisambiguation;)Lcom/google/android/search/shared/actions/CommunicationAction;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/search/shared/actions/PhoneCallAction;

    invoke-direct {v0, p1}, Lcom/google/android/search/shared/actions/PhoneCallAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PhoneCallAction[recipient="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/PhoneCallAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
