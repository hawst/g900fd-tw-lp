.class public Lcom/google/android/search/shared/actions/modular/ModularAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"

# interfaces
.implements Ldvu;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final bNY:Ljpu;


# instance fields
.field private final bMN:I

.field private final bNZ:Ljava/util/List;

.field private final bOa:Landroid/util/SparseArray;

.field private final bOb:Ljrl;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final bOc:Ljqb;

.field private final bOd:Ljpu;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bOe:I

.field private bOf:Ldvv;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bOg:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    new-instance v0, Ljpu;

    invoke-direct {v0}, Ljpu;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bNY:Ljpu;

    .line 520
    new-instance v0, Ldvx;

    invoke-direct {v0}, Ldvx;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/modular/ModularAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 126
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 90
    iput-boolean v2, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOg:Z

    .line 127
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 128
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 129
    invoke-static {v3}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bNZ:Ljava/util/List;

    .line 130
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bNZ:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->v(Ljava/util/List;)Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOa:Landroid/util/SparseArray;

    .line 131
    const-class v0, Ljrl;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljrl;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOb:Ljrl;

    .line 133
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    .line 134
    invoke-virtual {v0, p0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->c(Lcom/google/android/search/shared/actions/modular/ModularAction;)V

    goto :goto_0

    .line 136
    :cond_0
    const-class v0, Ljqb;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljqb;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOc:Ljqb;

    .line 138
    const-class v0, Ljpu;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljpu;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOd:Ljpu;

    .line 139
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bMN:I

    .line 140
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOe:I

    .line 141
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOg:Z

    .line 142
    return-void

    :cond_1
    move v0, v2

    .line 141
    goto :goto_1
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljrc;Ljava/util/List;I)V
    .locals 7

    .prologue
    .line 94
    iget-object v1, p1, Ljrc;->ezY:[Ljrl;

    iget-object v2, p1, Ljrc;->eAb:Ljqb;

    iget-object v4, p1, Ljrc;->eAc:Ljpu;

    invoke-virtual {p1}, Ljrc;->bss()I

    move-result v6

    move-object v0, p0

    move-object v3, p2

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/search/shared/actions/modular/ModularAction;-><init>([Ljrl;Ljqb;Ljava/util/List;Ljpu;II)V

    .line 96
    invoke-virtual {p1}, Ljrc;->bsA()Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    .line 99
    :cond_0
    return-void
.end method

.method private constructor <init>([Ljrl;Ljqb;Ljava/util/List;Ljpu;II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 103
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 90
    iput-boolean v1, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOg:Z

    .line 104
    invoke-static {p3}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bNZ:Ljava/util/List;

    .line 105
    invoke-static {p3}, Lcom/google/android/search/shared/actions/modular/ModularAction;->v(Ljava/util/List;)Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOa:Landroid/util/SparseArray;

    .line 108
    array-length v0, p1

    if-eqz v0, :cond_0

    .line 109
    aget-object v0, p1, v1

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOb:Ljrl;

    .line 115
    :goto_0
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    .line 116
    invoke-virtual {v0, p0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->c(Lcom/google/android/search/shared/actions/modular/ModularAction;)V

    goto :goto_1

    .line 111
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    .line 112
    new-instance v0, Ljrl;

    invoke-direct {v0}, Ljrl;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOb:Ljrl;

    goto :goto_0

    .line 119
    :cond_1
    iput-object p2, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOc:Ljqb;

    .line 120
    iput-object p4, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOd:Ljpu;

    .line 121
    iput p5, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bMN:I

    .line 122
    iput p6, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOe:I

    .line 123
    return-void
.end method

.method private static v(Ljava/util/List;)Landroid/util/SparseArray;
    .locals 4

    .prologue
    .line 513
    new-instance v1, Landroid/util/SparseArray;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Landroid/util/SparseArray;-><init>(I)V

    .line 514
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    .line 515
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->getId()I

    move-result v3

    invoke-virtual {v1, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 517
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final Wb()Z
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOd:Ljpu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOd:Ljpu;

    :goto_0
    invoke-virtual {v0}, Ljpu;->brX()Z

    move-result v0

    return v0

    :cond_0
    sget-object v0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bNY:Ljpu;

    goto :goto_0
.end method

.method public final Wc()Z
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOb:Ljrl;

    invoke-virtual {v0}, Ljrl;->btp()Z

    move-result v0

    return v0
.end method

.method public final a(Ljqu;Landroid/content/res/Resources;)Ldws;
    .locals 2

    .prologue
    .line 406
    invoke-virtual {p1}, Ljqu;->bqC()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    .line 407
    if-nez v0, :cond_0

    .line 408
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    .line 409
    sget-object v0, Ldws;->bOO:Ldws;

    .line 416
    :goto_0
    return-object v0

    .line 412
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->Pd()Z

    move-result v1

    if-nez v1, :cond_1

    .line 413
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_0

    .line 416
    :cond_1
    invoke-virtual {v0, p1, p2}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->b(Ljqu;Landroid/content/res/Resources;)Ldws;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 327
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/modular/ModularAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/shared/util/MatchingAppInfo;)V
    .locals 1

    .prologue
    .line 316
    instance-of v0, p1, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 317
    invoke-super {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->a(Lcom/google/android/shared/util/MatchingAppInfo;)V

    .line 318
    return-void
.end method

.method public final a(Legu;)V
    .locals 2

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bNZ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    .line 291
    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->a(Legu;)V

    goto :goto_0

    .line 293
    :cond_0
    return-void
.end method

.method public final a(Ljqu;)Z
    .locals 2

    .prologue
    .line 421
    invoke-virtual {p1}, Ljqu;->bqC()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    .line 422
    if-nez v0, :cond_0

    .line 423
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    .line 424
    const/4 v0, 0x0

    .line 427
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Ljqu;->bsE()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->gI(I)Z

    move-result v0

    goto :goto_0
.end method

.method public final a([Ljng;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 455
    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    aget-object v3, p1, v1

    .line 456
    invoke-virtual {v3}, Ljng;->bqD()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 457
    invoke-virtual {v3}, Ljng;->bqC()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v4

    .line 460
    if-nez v4, :cond_1

    .line 461
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    invoke-virtual {v3}, Ljng;->bqC()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No argument matching ArgumentConstraint="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 469
    :cond_0
    :goto_1
    return v0

    .line 465
    :cond_1
    invoke-virtual {v4, v3}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->a(Ljng;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 455
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 469
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final a(Ljqh;Landroid/content/res/Resources;)[Ljrg;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 475
    invoke-virtual {p1}, Ljqh;->all()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    .line 476
    if-nez v0, :cond_0

    .line 477
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    move-result-object v0

    invoke-virtual {p1}, Ljqh;->all()I

    move-result v1

    invoke-virtual {v0, v1}, Ldvv;->gE(I)V

    .line 478
    const/4 v0, 0x0

    .line 481
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->a(Ljqh;)[Ljrg;

    move-result-object v0

    goto :goto_0
.end method

.method public final agb()Ldtw;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 298
    const/4 v0, 0x0

    return-object v0
.end method

.method public final agc()I
    .locals 1

    .prologue
    .line 278
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bMN:I

    return v0
.end method

.method protected final agf()Z
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOb:Ljrl;

    invoke-virtual {v0}, Ljrl;->btq()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final agg()Lcom/google/android/search/shared/actions/ActionExecutionState;
    .locals 1

    .prologue
    .line 199
    invoke-super {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->agg()Lcom/google/android/search/shared/actions/ActionExecutionState;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic agu()Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 1

    .prologue
    .line 52
    invoke-super {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;

    return-object v0
.end method

.method public final agy()Ldxl;
    .locals 3

    .prologue
    .line 370
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajn()Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    move-result-object v2

    .line 371
    if-nez v2, :cond_0

    .line 372
    const/4 v0, 0x0

    .line 378
    :goto_0
    return-object v0

    .line 374
    :cond_0
    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->akB()Ldzb;

    move-result-object v0

    .line 375
    if-nez v0, :cond_1

    .line 376
    invoke-static {p0, v2}, Ldxv;->a(Lcom/google/android/search/shared/actions/modular/ModularAction;Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;)Ldzb;

    move-result-object v0

    .line 378
    :cond_1
    new-instance v1, Ldvw;

    invoke-direct {v1, p0, v0, v2}, Ldvw;-><init>(Lcom/google/android/search/shared/actions/modular/ModularAction;Ldzb;Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final aje()Ljqb;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOc:Ljqb;

    return-object v0
.end method

.method public final ajf()Ljqs;
    .locals 1

    .prologue
    .line 497
    invoke-super {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;->ajq()Ljqs;

    move-result-object v0

    return-object v0
.end method

.method public final ajh()Ljava/util/List;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bNZ:Ljava/util/List;

    return-object v0
.end method

.method public final aji()Ljrl;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOb:Ljrl;

    return-object v0
.end method

.method public final declared-synchronized ajj()Ldvv;
    .locals 2

    .prologue
    .line 266
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOf:Ldvv;

    if-nez v0, :cond_0

    .line 267
    new-instance v0, Ldvv;

    iget v1, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bMN:I

    invoke-direct {v0, v1}, Ldvv;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOf:Ldvv;

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOf:Ldvv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 266
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final ajk()I
    .locals 1

    .prologue
    .line 285
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOe:I

    return v0
.end method

.method public final ajl()Z
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOb:Ljrl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOb:Ljrl;

    iget-object v0, v0, Ljrl;->ezI:[I

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ajm()Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 322
    invoke-super {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;

    return-object v0
.end method

.method public final ajn()Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;
    .locals 3

    .prologue
    .line 360
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bNZ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    .line 361
    instance-of v2, v0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    if-eqz v2, :cond_0

    .line 362
    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    .line 365
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic ajo()Ldwd;
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljqu;)Ldws;
    .locals 2

    .prologue
    .line 432
    invoke-super {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;

    .line 433
    invoke-virtual {p1}, Ljqu;->bsE()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 450
    :cond_0
    sget-object v0, Ldws;->bOO:Ldws;

    :goto_0
    return-object v0

    .line 435
    :sswitch_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;->avd()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 436
    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;->c(Ljqu;)Ldws;

    move-result-object v0

    goto :goto_0

    .line 438
    :cond_1
    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOb:Ljrl;

    .line 439
    invoke-virtual {v1}, Ljrl;->btu()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440
    new-instance v0, Ldws;

    invoke-virtual {v1}, Ljrl;->btt()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 445
    :sswitch_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;->avd()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 446
    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;->c(Ljqu;)Ldws;

    move-result-object v0

    goto :goto_0

    .line 433
    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_0
        0xe -> :sswitch_1
    .end sparse-switch
.end method

.method public final b(ZZZZ)Ljrc;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 231
    new-instance v2, Ljrc;

    invoke-direct {v2}, Ljrc;-><init>()V

    .line 232
    if-eqz p3, :cond_0

    .line 233
    const/4 v1, 0x1

    new-array v1, v1, [Ljrl;

    iget-object v3, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOb:Ljrl;

    aput-object v3, v1, v0

    iput-object v1, v2, Ljrc;->ezY:[Ljrl;

    .line 235
    :cond_0
    if-eqz p4, :cond_1

    .line 236
    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOc:Ljqb;

    iput-object v1, v2, Ljrc;->eAb:Ljqb;

    .line 239
    :cond_1
    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bNZ:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljqg;

    iput-object v1, v2, Ljrc;->ezX:[Ljqg;

    move v1, v0

    .line 240
    :goto_0
    iget-object v0, v2, Ljrc;->ezX:[Ljqg;

    array-length v0, v0

    if-ge v1, v0, :cond_2

    .line 241
    iget-object v3, v2, Ljrc;->ezX:[Ljqg;

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bNZ:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->r(ZZ)Ljqg;

    move-result-object v0

    aput-object v0, v3, v1

    .line 240
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 245
    :cond_2
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOd:Ljpu;

    iput-object v0, v2, Ljrc;->eAc:Ljpu;

    .line 246
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOe:I

    invoke-virtual {v2, v0}, Ljrc;->rF(I)Ljrc;

    .line 247
    return-object v2
.end method

.method public final canExecute()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 160
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOb:Ljrl;

    .line 162
    iget-object v3, v0, Ljrl;->eAH:[Ljqs;

    array-length v3, v3

    if-nez v3, :cond_0

    move v0, v1

    .line 178
    :goto_0
    return v0

    .line 167
    :cond_0
    iget-object v3, v0, Ljrl;->ezJ:[I

    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_4

    aget v5, v3, v0

    invoke-virtual {p0, v5}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v6

    if-nez v6, :cond_1

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    move-result-object v0

    invoke-virtual {v0, v5}, Ldvv;->gD(I)V

    move v0, v1

    :goto_2
    if-nez v0, :cond_5

    move v0, v1

    .line 168
    goto :goto_0

    .line 167
    :cond_1
    invoke-virtual {v6}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajG()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v6}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajC()Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    .line 172
    :cond_5
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bNZ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    .line 173
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->Pd()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajC()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    .line 174
    goto :goto_0

    :cond_7
    move v0, v2

    .line 178
    goto :goto_0
.end method

.method public final d(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z
    .locals 2

    .prologue
    .line 307
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajl()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308
    const/4 v0, 0x1

    .line 311
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOb:Ljrl;

    iget-object v0, v0, Ljrl;->ezI:[I

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->getId()I

    move-result v1

    invoke-static {v0, v1}, Lesp;->c([II)Z

    move-result v0

    goto :goto_0
.end method

.method public final e(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Lcom/google/android/search/shared/actions/modular/ModularAction;
    .locals 7
    .param p1    # Lcom/google/android/search/shared/actions/modular/arguments/Argument;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bNZ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 344
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bNZ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    .line 345
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->getId()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->getId()I

    move-result v4

    if-ne v2, v4, :cond_0

    .line 346
    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 348
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajE()Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 352
    :cond_1
    new-instance v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    const/4 v1, 0x1

    new-array v1, v1, [Ljrl;

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOb:Ljrl;

    new-instance v5, Ljrl;

    invoke-direct {v5}, Ljrl;-><init>()V

    invoke-static {v2, v5}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v2

    check-cast v2, Ljrl;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOc:Ljqb;

    new-instance v4, Ljqb;

    invoke-direct {v4}, Ljqb;-><init>()V

    invoke-static {v2, v4}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v2

    check-cast v2, Ljqb;

    iget-object v4, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOd:Ljpu;

    new-instance v5, Ljpu;

    invoke-direct {v5}, Ljpu;-><init>()V

    invoke-static {v4, v5}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v4

    check-cast v4, Ljpu;

    iget v5, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bMN:I

    iget v6, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOe:I

    invoke-direct/range {v0 .. v6}, Lcom/google/android/search/shared/actions/modular/ModularAction;-><init>([Ljrl;Ljqb;Ljava/util/List;Ljpu;II)V

    return-object v0
.end method

.method public final gB(I)I
    .locals 1

    .prologue
    .line 486
    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    .line 487
    if-nez v0, :cond_0

    .line 488
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldvv;->gE(I)V

    .line 489
    const/4 v0, 0x0

    .line 492
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajF()I

    move-result v0

    goto :goto_0
.end method

.method public final gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOa:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 502
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 503
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bNZ:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 504
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOb:Ljrl;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 505
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOc:Ljqb;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 506
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOd:Ljpu;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 507
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bMN:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 508
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOe:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 509
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;->bOg:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 510
    return-void

    .line 509
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
