.class public Lcom/google/android/search/shared/actions/OpenUrlAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bLI:Ljava/lang/String;

.field private final bMu:Ljava/lang/String;

.field private final bMv:Ljava/lang/String;

.field private final bMw:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lduo;

    invoke-direct {v0}, Lduo;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/OpenUrlAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 31
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/OpenUrlAction;->bMu:Ljava/lang/String;

    .line 32
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/OpenUrlAction;->bMv:Ljava/lang/String;

    .line 33
    const-class v0, Ljrs;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/OpenUrlAction;->bMw:Landroid/net/Uri;

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/OpenUrlAction;->bLI:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 0
    .param p3    # Landroid/net/Uri;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/search/shared/actions/OpenUrlAction;->bMu:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/google/android/search/shared/actions/OpenUrlAction;->bMv:Ljava/lang/String;

    .line 25
    iput-object p3, p0, Lcom/google/android/search/shared/actions/OpenUrlAction;->bMw:Landroid/net/Uri;

    .line 26
    iput-object p4, p0, Lcom/google/android/search/shared/actions/OpenUrlAction;->bLI:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 60
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/OpenUrlAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final ahK()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/search/shared/actions/OpenUrlAction;->bMu:Ljava/lang/String;

    return-object v0
.end method

.method public final ahL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/search/shared/actions/OpenUrlAction;->bMv:Ljava/lang/String;

    return-object v0
.end method

.method public final ahM()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/search/shared/actions/OpenUrlAction;->bMw:Landroid/net/Uri;

    return-object v0
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/search/shared/actions/OpenUrlAction;->bMv:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/search/shared/actions/OpenUrlAction;->bLI:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 66
    iget-object v0, p0, Lcom/google/android/search/shared/actions/OpenUrlAction;->bMu:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/search/shared/actions/OpenUrlAction;->bMv:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/search/shared/actions/OpenUrlAction;->bMw:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 69
    iget-object v0, p0, Lcom/google/android/search/shared/actions/OpenUrlAction;->bLI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 70
    return-void
.end method
