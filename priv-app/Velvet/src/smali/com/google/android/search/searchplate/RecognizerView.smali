.class public Lcom/google/android/search/searchplate/RecognizerView;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private aNF:Ldtr;

.field private bGp:Z

.field public bHc:Ldrw;

.field public bHd:I

.field private bHe:Z

.field private bHf:I

.field public bHg:Ldqv;

.field private bHh:Landroid/animation/ValueAnimator;

.field private bHi:Landroid/graphics/drawable/Drawable;

.field private bHj:Landroid/graphics/drawable/Drawable;

.field private bHk:Landroid/graphics/drawable/Drawable;

.field private bHl:Landroid/graphics/drawable/Drawable;

.field private bHm:Landroid/graphics/drawable/TransitionDrawable;

.field private bHn:I

.field private bHo:I

.field private bHp:Landroid/graphics/drawable/Drawable;

.field private bHq:Ldtc;

.field private bHr:Landroid/animation/ValueAnimator;

.field private bHs:Landroid/animation/AnimatorSet;

.field private bHt:Landroid/animation/ValueAnimator;

.field private bHu:Landroid/animation/ValueAnimator;

.field private bHv:Landroid/graphics/drawable/Drawable;

.field private bHw:Landroid/widget/ImageView;

.field private bHx:Landroid/widget/ProgressBar;

.field private bHy:Z

.field private bHz:Landroid/view/ViewPropertyAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/searchplate/RecognizerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 143
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/searchplate/RecognizerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 139
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const v4, 0x7f02030f

    const v3, 0x7f02030e

    const/4 v1, 0x0

    .line 99
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 101
    sget-object v0, Ldrk;->bHb:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 103
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHi:Landroid/graphics/drawable/Drawable;

    .line 104
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHj:Landroid/graphics/drawable/Drawable;

    .line 105
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHk:Landroid/graphics/drawable/Drawable;

    .line 106
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHl:Landroid/graphics/drawable/Drawable;

    .line 107
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHn:I

    .line 109
    const/4 v1, 0x5

    const v2, 0x7f020310

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHo:I

    .line 111
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHp:Landroid/graphics/drawable/Drawable;

    .line 112
    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHv:Landroid/graphics/drawable/Drawable;

    .line 114
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 116
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 117
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHi:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    .line 118
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHi:Landroid/graphics/drawable/Drawable;

    .line 120
    :cond_0
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHj:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_1

    .line 121
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHj:Landroid/graphics/drawable/Drawable;

    .line 123
    :cond_1
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHk:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_2

    .line 124
    const v1, 0x7f020311

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHk:Landroid/graphics/drawable/Drawable;

    .line 126
    :cond_2
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHl:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_3

    .line 127
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHl:Landroid/graphics/drawable/Drawable;

    .line 129
    :cond_3
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHp:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_4

    .line 130
    const v1, 0x7f02030d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHp:Landroid/graphics/drawable/Drawable;

    .line 132
    :cond_4
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHv:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_5

    .line 133
    const v1, 0x7f020193

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHv:Landroid/graphics/drawable/Drawable;

    .line 135
    :cond_5
    return-void
.end method

.method public static synthetic a(Lcom/google/android/search/searchplate/RecognizerView;)Ldtc;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHq:Ldtc;

    return-object v0
.end method

.method private aeZ()V
    .locals 3

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->aNF:Ldtr;

    if-eqz v0, :cond_0

    .line 354
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->aNF:Ldtr;

    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHs:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHr:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHd:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Ldtr;->eg(Z)V

    .line 359
    :cond_0
    return-void

    .line 354
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private afa()V
    .locals 2

    .prologue
    .line 404
    iget v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHd:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHd:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHd:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHd:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHr:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHs:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    .line 409
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHt:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 413
    :cond_1
    :goto_0
    return-void

    .line 410
    :cond_2
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHt:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 411
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHt:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method private afb()V
    .locals 7

    .prologue
    const v5, 0x7f0a0031

    const/4 v1, 0x0

    .line 416
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a003a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 417
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 420
    const/4 v0, 0x1

    .line 421
    iget v4, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHd:I

    packed-switch v4, :pswitch_data_0

    move-object v1, v2

    .line 453
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setKeepScreenOn(Z)V

    .line 454
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHw:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 455
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHw:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 456
    return-void

    .line 423
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHm:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    .line 424
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHw:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHi:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object v1, v2

    .line 425
    goto :goto_0

    .line 427
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHm:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    .line 428
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHw:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHm:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 429
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 432
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHw:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHm:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 433
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHu:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 434
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHm:Landroid/graphics/drawable/TransitionDrawable;

    const/16 v2, 0xa7

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 436
    const v1, 0x7f0a0030

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 439
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHw:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 440
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 444
    :pswitch_4
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setTranslationY(F)V

    .line 446
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHw:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHg:Ldqv;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 447
    const v0, 0x7f0a002f

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move v6, v1

    move-object v1, v0

    move v0, v6

    .line 448
    goto :goto_0

    :pswitch_5
    move v0, v1

    move-object v1, v2

    .line 450
    goto :goto_0

    .line 421
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public static synthetic b(Lcom/google/android/search/searchplate/RecognizerView;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/search/searchplate/RecognizerView;->aeZ()V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/search/searchplate/RecognizerView;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/search/searchplate/RecognizerView;->afa()V

    return-void
.end method

.method public static synthetic d(Lcom/google/android/search/searchplate/RecognizerView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHw:Landroid/widget/ImageView;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/search/searchplate/RecognizerView;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHv:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method


# virtual methods
.method public final a(Ldrw;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHc:Ldrw;

    .line 335
    return-void
.end method

.method public final a(Ldtc;)V
    .locals 0

    .prologue
    .line 338
    iput-object p1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHq:Ldtc;

    .line 339
    return-void
.end method

.method public final b(Ldtb;)V
    .locals 5

    .prologue
    .line 466
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 467
    new-instance v1, Ldsv;

    const v2, 0x7f0d001e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v3, 0x7f0d001f

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const v4, 0x7f0b001c

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {v1, v2, v3, v0}, Ldsv;-><init>(III)V

    .line 471
    invoke-virtual {p0, v1}, Lcom/google/android/search/searchplate/RecognizerView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 472
    new-instance v0, Ldtr;

    invoke-direct {v0, v1, p1}, Ldtr;-><init>(Ldsv;Ldtb;)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->aNF:Ldtr;

    .line 473
    return-void
.end method

.method public final dP(Z)V
    .locals 1

    .prologue
    .line 393
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setState(I)V

    .line 395
    if-eqz p1, :cond_1

    .line 396
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHr:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 401
    :cond_0
    :goto_0
    return-void

    .line 398
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHs:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0
.end method

.method public final dQ(Z)V
    .locals 4

    .prologue
    .line 538
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bGp:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bGp:Z

    iget-boolean v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bGp:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHn:I

    iput v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHf:I

    :goto_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHg:Ldqv;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHf:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldqv;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    invoke-direct {p0}, Lcom/google/android/search/searchplate/RecognizerView;->afb()V

    .line 539
    :cond_0
    return-void

    .line 538
    :cond_1
    iget v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHo:I

    iput v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHf:I

    goto :goto_0
.end method

.method public final dR(Z)V
    .locals 1

    .prologue
    .line 569
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHe:Z

    if-ne p1, v0, :cond_0

    .line 582
    :goto_0
    return-void

    .line 573
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/search/searchplate/RecognizerView;->setKeepScreenOn(Z)V

    .line 574
    iput-boolean p1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHe:Z

    .line 575
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHe:Z

    if-eqz v0, :cond_1

    .line 577
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/RecognizerView;->dS(Z)V

    .line 578
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHh:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 580
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHh:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    goto :goto_0
.end method

.method public final dS(Z)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x64

    .line 585
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHy:Z

    if-ne p1, v0, :cond_1

    .line 613
    :cond_0
    :goto_0
    return-void

    .line 589
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHy:Z

    .line 590
    if-eqz p1, :cond_2

    .line 591
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHx:Landroid/widget/ProgressBar;

    invoke-static {v0}, Ldtp;->aA(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Ldrn;

    invoke-direct {v1, p0}, Ldrn;-><init>(Lcom/google/android/search/searchplate/RecognizerView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHz:Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 602
    :cond_2
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHz:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_3

    .line 603
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHz:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 604
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHz:Landroid/view/ViewPropertyAnimator;

    .line 606
    :cond_3
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHw:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHg:Ldqv;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 607
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHw:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 608
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHx:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHx:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Ldtp;->p(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public onAttachedToWindow()V
    .locals 0

    .prologue
    .line 311
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 324
    invoke-direct {p0}, Lcom/google/android/search/searchplate/RecognizerView;->afb()V

    .line 325
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHt:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 330
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 331
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 546
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 547
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    .line 548
    or-int v2, v0, v1

    if-nez v2, :cond_0

    .line 549
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 556
    :goto_0
    return-void

    .line 551
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->save(I)I

    .line 552
    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 553
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 554
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 12

    .prologue
    const-wide/16 v10, 0xa7

    const-wide/16 v8, 0xe9

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x2

    .line 147
    new-instance v0, Ldrl;

    invoke-direct {v0, p0}, Ldrl;-><init>(Lcom/google/android/search/searchplate/RecognizerView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    const v0, 0x7f1103c7

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/RecognizerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHw:Landroid/widget/ImageView;

    .line 176
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHw:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 178
    const v0, 0x7f1103c6

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/RecognizerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHx:Landroid/widget/ProgressBar;

    .line 179
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 180
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHx:Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0024

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminateTintList(Landroid/content/res/ColorStateList;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHx:Landroid/widget/ProgressBar;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminateTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 185
    :cond_0
    iget v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHo:I

    iput v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHf:I

    .line 186
    new-instance v0, Ldqv;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHf:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHp:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ldqv;-><init>(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHg:Ldqv;

    .line 189
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHg:Ldqv;

    invoke-virtual {v0, v6, v6}, Ldqv;->setId(II)V

    .line 190
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHg:Ldqv;

    invoke-virtual {v0, v7, v7}, Ldqv;->setId(II)V

    .line 192
    new-instance v0, Landroid/graphics/drawable/TransitionDrawable;

    new-array v1, v5, [Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHj:Landroid/graphics/drawable/Drawable;

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHk:Landroid/graphics/drawable/Drawable;

    aput-object v2, v1, v7

    invoke-direct {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHm:Landroid/graphics/drawable/TransitionDrawable;

    .line 195
    new-array v0, v5, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHh:Landroid/animation/ValueAnimator;

    .line 196
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHh:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 197
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHh:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 198
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHh:Landroid/animation/ValueAnimator;

    new-instance v1, Ldro;

    invoke-direct {v1, p0}, Ldro;-><init>(Lcom/google/android/search/searchplate/RecognizerView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 205
    new-array v0, v5, [F

    fill-array-data v0, :array_1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHr:Landroid/animation/ValueAnimator;

    .line 206
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHr:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 207
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHr:Landroid/animation/ValueAnimator;

    sget-object v1, Ldqx;->bGB:Ldqx;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 208
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHr:Landroid/animation/ValueAnimator;

    new-instance v1, Ldrp;

    invoke-direct {v1, p0}, Ldrp;-><init>(Lcom/google/android/search/searchplate/RecognizerView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 217
    new-instance v0, Ldrq;

    invoke-direct {v0, p0}, Ldrq;-><init>(Lcom/google/android/search/searchplate/RecognizerView;)V

    .line 231
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHr:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 236
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 238
    new-array v2, v5, [F

    fill-array-data v2, :array_2

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 239
    invoke-virtual {v2, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 240
    sget-object v3, Ldqy;->bGC:Ldqy;

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 241
    new-instance v3, Ldrr;

    invoke-direct {v3, p0, v1}, Ldrr;-><init>(Lcom/google/android/search/searchplate/RecognizerView;I)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 249
    new-array v3, v5, [F

    fill-array-data v3, :array_3

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v3

    .line 250
    invoke-virtual {v3, v10, v11}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 251
    sget-object v4, Ldqy;->bGC:Ldqy;

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 252
    new-instance v4, Ldrs;

    invoke-direct {v4, p0, v1}, Ldrs;-><init>(Lcom/google/android/search/searchplate/RecognizerView;I)V

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 260
    new-array v1, v5, [F

    fill-array-data v1, :array_4

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 261
    invoke-virtual {v1, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 262
    sget-object v4, Ldqx;->bGB:Ldqx;

    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 263
    new-instance v4, Ldrt;

    invoke-direct {v4, p0}, Ldrt;-><init>(Lcom/google/android/search/searchplate/RecognizerView;)V

    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 272
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v4, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHs:Landroid/animation/AnimatorSet;

    .line 273
    iget-object v4, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHs:Landroid/animation/AnimatorSet;

    invoke-virtual {v4, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 274
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHs:Landroid/animation/AnimatorSet;

    invoke-virtual {v1, v3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 275
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHs:Landroid/animation/AnimatorSet;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 277
    new-array v0, v5, [F

    fill-array-data v0, :array_5

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHt:Landroid/animation/ValueAnimator;

    .line 278
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHt:Landroid/animation/ValueAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 279
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHt:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x488

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 280
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHt:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v5}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 281
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHt:Landroid/animation/ValueAnimator;

    new-instance v1, Ldru;

    invoke-direct {v1, p0}, Ldru;-><init>(Lcom/google/android/search/searchplate/RecognizerView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 289
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHt:Landroid/animation/ValueAnimator;

    new-instance v1, Ldrv;

    invoke-direct {v1, p0}, Ldrv;-><init>(Lcom/google/android/search/searchplate/RecognizerView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 296
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 298
    new-array v1, v5, [F

    const/4 v2, 0x0

    aput v2, v1, v6

    int-to-float v0, v0

    aput v0, v1, v7

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHu:Landroid/animation/ValueAnimator;

    .line 299
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHu:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v10, v11}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 300
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHu:Landroid/animation/ValueAnimator;

    sget-object v1, Ldqs;->bGc:Ldqs;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 301
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHu:Landroid/animation/ValueAnimator;

    new-instance v1, Ldrm;

    invoke-direct {v1, p0}, Ldrm;-><init>(Lcom/google/android/search/searchplate/RecognizerView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 307
    return-void

    .line 195
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 205
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 238
    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 249
    :array_3
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 260
    :array_4
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 277
    :array_5
    .array-data 4
        0x3f800000    # 1.0f
        0x3f733333    # 0.95f
    .end array-data
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 480
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 481
    const-class v0, Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 482
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 370
    instance-of v0, p1, Lcom/google/android/search/searchplate/RecognizerView$SavedState;

    if-nez v0, :cond_0

    .line 371
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 379
    :goto_0
    return-void

    .line 375
    :cond_0
    check-cast p1, Lcom/google/android/search/searchplate/RecognizerView$SavedState;

    .line 376
    invoke-virtual {p1}, Lcom/google/android/search/searchplate/RecognizerView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 378
    iget v0, p1, Lcom/google/android/search/searchplate/RecognizerView$SavedState;->ag:I

    iput v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHd:I

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 363
    new-instance v0, Lcom/google/android/search/searchplate/RecognizerView$SavedState;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/search/searchplate/RecognizerView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 364
    iget v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHd:I

    iput v1, v0, Lcom/google/android/search/searchplate/RecognizerView$SavedState;->ag:I

    .line 365
    return-object v0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 383
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 384
    if-ne p1, p0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHt:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 385
    invoke-direct {p0}, Lcom/google/android/search/searchplate/RecognizerView;->afa()V

    .line 387
    :cond_0
    if-ne p1, p0, :cond_1

    if-eqz p2, :cond_1

    .line 388
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/RecognizerView;->dS(Z)V

    .line 390
    :cond_1
    return-void
.end method

.method public final setState(I)V
    .locals 0

    .prologue
    .line 346
    iput p1, p0, Lcom/google/android/search/searchplate/RecognizerView;->bHd:I

    .line 347
    invoke-direct {p0}, Lcom/google/android/search/searchplate/RecognizerView;->afb()V

    .line 348
    invoke-direct {p0}, Lcom/google/android/search/searchplate/RecognizerView;->afa()V

    .line 349
    invoke-direct {p0}, Lcom/google/android/search/searchplate/RecognizerView;->aeZ()V

    .line 350
    return-void
.end method
