.class public Lcom/google/android/search/shared/ui/TravelModeSpinner;
.super Landroid/widget/Spinner;
.source "PG"


# instance fields
.field private bLG:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;I)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 39
    return-void
.end method

.method private anF()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 63
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/search/shared/ui/TravelModeSpinner;->bLG:I

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/search/shared/ui/TravelModeSpinner;->bLG:I

    if-ne v0, v1, :cond_1

    .line 65
    :cond_0
    const/4 v0, 0x0

    .line 67
    :cond_1
    return v0
.end method

.method private gx(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 71
    iget v0, p0, Lcom/google/android/search/shared/ui/TravelModeSpinner;->bLG:I

    invoke-direct {p0, v0}, Lcom/google/android/search/shared/ui/TravelModeSpinner;->hj(I)[I

    move-result-object v2

    move v0, v1

    .line 73
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 74
    aget v3, v2, v0

    if-ne v3, p1, :cond_0

    .line 79
    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/ui/TravelModeSpinner;->setSelection(I)V

    .line 80
    return-void

    .line 73
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private hj(I)[I
    .locals 2

    .prologue
    .line 92
    const/4 v0, 0x3

    if-ne v0, p1, :cond_0

    .line 93
    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/TravelModeSpinner;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    .line 96
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/TravelModeSpinner;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final aE(II)V
    .locals 5

    .prologue
    .line 42
    iput p1, p0, Lcom/google/android/search/shared/ui/TravelModeSpinner;->bLG:I

    .line 43
    invoke-direct {p0}, Lcom/google/android/search/shared/ui/TravelModeSpinner;->anF()Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/ui/TravelModeSpinner;->setVisibility(I)V

    .line 51
    :goto_0
    return-void

    .line 47
    :cond_0
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/TravelModeSpinner;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0401ae

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/TravelModeSpinner;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v4, 0x3

    if-ne v4, p1, :cond_1

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0f000c

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v2, v3, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {p0, v1}, Lcom/google/android/search/shared/ui/TravelModeSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 50
    invoke-direct {p0, p2}, Lcom/google/android/search/shared/ui/TravelModeSpinner;->gx(I)V

    goto :goto_0

    .line 47
    :cond_1
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0f000a

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final anE()I
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/search/shared/ui/TravelModeSpinner;->anF()Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    const/4 v0, 0x0

    .line 59
    :goto_0
    return v0

    .line 57
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/TravelModeSpinner;->getSelectedItemPosition()I

    move-result v0

    .line 58
    iget v1, p0, Lcom/google/android/search/shared/ui/TravelModeSpinner;->bLG:I

    invoke-direct {p0, v1}, Lcom/google/android/search/shared/ui/TravelModeSpinner;->hj(I)[I

    move-result-object v1

    .line 59
    aget v0, v1, v0

    goto :goto_0
.end method
