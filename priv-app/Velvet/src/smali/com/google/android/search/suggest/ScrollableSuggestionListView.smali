.class public Lcom/google/android/search/suggest/ScrollableSuggestionListView;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private final GR:Landroid/widget/ScrollView;

.field private final bWW:Lcom/google/android/search/suggest/SuggestionListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/suggest/ScrollableSuggestionListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/suggest/ScrollableSuggestionListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    new-instance v0, Landroid/widget/ScrollView;

    invoke-direct {v0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->GR:Landroid/widget/ScrollView;

    .line 47
    new-instance v0, Lcom/google/android/search/suggest/SuggestionListView;

    const/4 v2, 0x0

    sget-object v1, Lbwe;->aMf:[I

    invoke-virtual {p1, p2, v1, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v5

    move-object v1, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/search/suggest/SuggestionListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILandroid/view/View;Landroid/content/res/TypedArray;)V

    iput-object v0, p0, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->bWW:Lcom/google/android/search/suggest/SuggestionListView;

    .line 49
    return-void
.end method


# virtual methods
.method public final a(Leen;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->bWW:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0, p1}, Lcom/google/android/search/suggest/SuggestionListView;->a(Leen;)V

    .line 84
    return-void
.end method

.method public final a(Leey;Leeo;Lesm;Lela;Leld;)V
    .locals 6

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->bWW:Lcom/google/android/search/suggest/SuggestionListView;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/search/suggest/SuggestionListView;->a(Leey;Leeo;Lesm;Lela;Leld;)V

    .line 79
    return-void
.end method

.method public final e(Ljava/lang/String;Ljava/util/List;I)V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->bWW:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/search/suggest/SuggestionListView;->e(Ljava/lang/String;Ljava/util/List;I)V

    .line 109
    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 54
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 55
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 56
    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->removeView(Landroid/view/View;)V

    .line 57
    iget-object v1, p0, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->bWW:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v1, v0}, Lcom/google/android/search/suggest/SuggestionListView;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->bWW:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0}, Lcom/google/android/search/suggest/SuggestionListView;->onFinishInflate()V

    .line 64
    iget-object v0, p0, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->bWW:Lcom/google/android/search/suggest/SuggestionListView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v3, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/suggest/SuggestionListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->GR:Landroid/widget/ScrollView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->GR:Landroid/widget/ScrollView;

    iget-object v1, p0, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->bWW:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->GR:Landroid/widget/ScrollView;

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->addView(Landroid/view/View;)V

    .line 71
    return-void
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->bWW:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0, p1}, Lcom/google/android/search/suggest/SuggestionListView;->setTitle(Ljava/lang/CharSequence;)V

    .line 99
    return-void
.end method

.method public setVisibility(I)V
    .locals 2

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->GR:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setScrollY(I)V

    .line 127
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 128
    return-void
.end method
