.class public Lcom/google/android/search/shared/actions/modular/CheckableLinearLayout;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Landroid/widget/Checkable;


# static fields
.field private static final BY:[I


# instance fields
.field private BZ:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 17
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/search/shared/actions/modular/CheckableLinearLayout;->BY:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/CheckableLinearLayout;->BZ:Z

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/CheckableLinearLayout;->BZ:Z

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/CheckableLinearLayout;->BZ:Z

    .line 29
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/CheckableLinearLayout;->BZ:Z

    return v0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 33
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 34
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/CheckableLinearLayout;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35
    sget-object v1, Lcom/google/android/search/shared/actions/modular/CheckableLinearLayout;->BY:[I

    invoke-static {v0, v1}, Lcom/google/android/search/shared/actions/modular/CheckableLinearLayout;->mergeDrawableStates([I[I)[I

    .line 37
    :cond_0
    return-object v0
.end method

.method public setChecked(Z)V
    .locals 0

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/search/shared/actions/modular/CheckableLinearLayout;->BZ:Z

    .line 48
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/CheckableLinearLayout;->refreshDrawableState()V

    .line 49
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/CheckableLinearLayout;->BZ:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/actions/modular/CheckableLinearLayout;->setChecked(Z)V

    .line 54
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
