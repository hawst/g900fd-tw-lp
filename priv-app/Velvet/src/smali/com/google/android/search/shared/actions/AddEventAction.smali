.class public Lcom/google/android/search/shared/actions/AddEventAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bBj:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bLA:Z

.field private final bLB:J

.field private final bLv:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bLw:Ljava/util/List;

.field private final bLx:Ljava/util/List;

.field private final bLy:J

.field private final bLz:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121
    new-instance v0, Ldtx;

    invoke-direct {v0}, Ldtx;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/AddEventAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 43
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 45
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLv:Ljava/lang/String;

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bBj:Ljava/lang/String;

    .line 47
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLw:Ljava/util/List;

    .line 48
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLx:Ljava/util/List;

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLy:J

    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLz:Z

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLA:Z

    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLB:J

    .line 53
    return-void

    :cond_0
    move v0, v2

    .line 50
    goto :goto_0

    :cond_1
    move v1, v2

    .line 51
    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;JZZJ)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLv:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bBj:Ljava/lang/String;

    .line 31
    iput-object p3, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLw:Ljava/util/List;

    .line 32
    iput-object p4, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLx:Ljava/util/List;

    .line 33
    iput-wide p5, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLy:J

    .line 34
    iput-boolean p7, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLz:Z

    .line 35
    iput-boolean p8, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLA:Z

    .line 36
    iput-wide p9, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLB:J

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 105
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/AddEventAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final agL()Ljava/util/List;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLw:Ljava/util/List;

    return-object v0
.end method

.method public final agM()Ljava/util/List;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLx:Ljava/util/List;

    return-object v0
.end method

.method public final agN()J
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLy:J

    return-wide v0
.end method

.method public final agO()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLz:Z

    return v0
.end method

.method public final agP()Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLA:Z

    return v0
.end method

.method public final agQ()J
    .locals 2

    .prologue
    .line 90
    iget-wide v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLB:J

    return-wide v0
.end method

.method public final agb()Ldtw;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Ldtw;->bLk:Ldtw;

    return-object v0
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLz:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLv:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bBj:Ljava/lang/String;

    return-object v0
.end method

.method public final pO()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLv:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 110
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 111
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLv:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bBj:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLw:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 114
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLx:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 115
    iget-wide v4, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLy:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 116
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLz:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 117
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLA:Z

    if-eqz v0, :cond_1

    :goto_1
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 118
    iget-wide v0, p0, Lcom/google/android/search/shared/actions/AddEventAction;->bLB:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 119
    return-void

    :cond_0
    move v0, v2

    .line 116
    goto :goto_0

    :cond_1
    move v1, v2

    .line 117
    goto :goto_1
.end method
