.class public Lcom/google/android/search/suggest/SuggestionStripView;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lelk;


# static fields
.field private static final bXb:Landroid/animation/TimeInterpolator;


# instance fields
.field private bTm:Leld;

.field private bTt:Lelj;

.field private bWQ:Lesm;

.field private bWZ:Leeo;

.field private final bXc:Leex;

.field private bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

.field private bXt:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    sget-object v0, Ldqx;->bGB:Ldqx;

    sput-object v0, Lcom/google/android/search/suggest/SuggestionStripView;->bXb:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 40
    new-instance v0, Leex;

    invoke-direct {v0}, Leex;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXc:Leex;

    .line 48
    sget-object v0, Lelj;->ceI:Lelj;

    iput-object v0, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bTt:Lelj;

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    new-instance v0, Leex;

    invoke-direct {v0}, Leex;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXc:Leex;

    .line 48
    sget-object v0, Lelj;->ceI:Lelj;

    iput-object v0, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bTt:Lelj;

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    new-instance v0, Leex;

    invoke-direct {v0}, Leex;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXc:Leex;

    .line 48
    sget-object v0, Lelj;->ceI:Lelj;

    iput-object v0, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bTt:Lelj;

    .line 52
    return-void
.end method

.method private aof()V
    .locals 10

    .prologue
    const/4 v3, -0x1

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 178
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 179
    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    aget-object v5, v2, v0

    .line 180
    invoke-virtual {v5}, Lcom/google/android/search/suggest/SuggestionIconView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 181
    if-lez v0, :cond_1

    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    add-int/lit8 v4, v0, -0x1

    aget-object v2, v2, v4

    invoke-virtual {v2}, Lcom/google/android/search/suggest/SuggestionIconView;->getId()I

    move-result v2

    .line 183
    :goto_1
    add-int/lit8 v4, v0, 0x1

    iget-object v6, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    array-length v6, v6

    if-ge v4, v6, :cond_3

    iget-object v4, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    add-int/lit8 v6, v0, 0x1

    aget-object v4, v4, v6

    invoke-virtual {v4}, Lcom/google/android/search/suggest/SuggestionIconView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_3

    .line 184
    iget-object v4, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    add-int/lit8 v6, v0, 0x1

    aget-object v4, v4, v6

    invoke-virtual {v4}, Lcom/google/android/search/suggest/SuggestionIconView;->getId()I

    move-result v4

    .line 187
    :goto_2
    new-array v6, v9, [I

    aput v2, v6, v1

    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bTt:Lelj;

    iget v2, v2, Lelj;->ceF:I

    aput v2, v6, v7

    invoke-virtual {v5}, Lcom/google/android/search/suggest/SuggestionIconView;->getId()I

    move-result v2

    aput v2, v6, v8

    invoke-static {v6}, Leln;->o([I)I

    move-result v2

    invoke-virtual {v5, v2}, Lcom/google/android/search/suggest/SuggestionIconView;->setNextFocusLeftId(I)V

    .line 189
    new-array v2, v9, [I

    aput v4, v2, v1

    iget-object v6, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bTt:Lelj;

    iget v6, v6, Lelj;->ceG:I

    aput v6, v2, v7

    invoke-virtual {v5}, Lcom/google/android/search/suggest/SuggestionIconView;->getId()I

    move-result v6

    aput v6, v2, v8

    invoke-static {v2}, Leln;->o([I)I

    move-result v2

    invoke-virtual {v5, v2}, Lcom/google/android/search/suggest/SuggestionIconView;->setNextFocusRightId(I)V

    .line 191
    new-array v2, v8, [I

    iget-object v6, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bTt:Lelj;

    iget v6, v6, Lelj;->ceD:I

    aput v6, v2, v1

    invoke-virtual {v5}, Lcom/google/android/search/suggest/SuggestionIconView;->getId()I

    move-result v6

    aput v6, v2, v7

    invoke-static {v2}, Leln;->o([I)I

    move-result v2

    invoke-virtual {v5, v2}, Lcom/google/android/search/suggest/SuggestionIconView;->setNextFocusUpId(I)V

    .line 192
    new-array v2, v8, [I

    iget-object v6, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bTt:Lelj;

    iget v6, v6, Lelj;->ceE:I

    aput v6, v2, v1

    invoke-virtual {v5}, Lcom/google/android/search/suggest/SuggestionIconView;->getId()I

    move-result v6

    aput v6, v2, v7

    invoke-static {v2}, Leln;->o([I)I

    move-result v2

    invoke-virtual {v5, v2}, Lcom/google/android/search/suggest/SuggestionIconView;->setNextFocusDownId(I)V

    .line 193
    new-array v2, v9, [I

    aput v4, v2, v1

    iget-object v4, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bTt:Lelj;

    iget v4, v4, Lelj;->ceH:I

    aput v4, v2, v7

    invoke-virtual {v5}, Lcom/google/android/search/suggest/SuggestionIconView;->getId()I

    move-result v4

    aput v4, v2, v8

    invoke-static {v2}, Leln;->o([I)I

    move-result v2

    invoke-virtual {v5, v2}, Lcom/google/android/search/suggest/SuggestionIconView;->setNextFocusForwardId(I)V

    .line 178
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_1
    move v2, v3

    .line 181
    goto/16 :goto_1

    .line 197
    :cond_2
    return-void

    :cond_3
    move v4, v3

    goto :goto_2
.end method


# virtual methods
.method public final a(Lelj;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bTt:Lelj;

    .line 142
    invoke-direct {p0}, Lcom/google/android/search/suggest/SuggestionStripView;->aof()V

    .line 143
    return-void
.end method

.method public final a(Lesm;Leld;Leeo;)V
    .locals 6

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bWQ:Lesm;

    .line 91
    iput-object p2, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bTm:Leld;

    .line 92
    iput-object p3, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bWZ:Leeo;

    .line 93
    iget-object v1, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 94
    iget-object v4, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bWQ:Lesm;

    iget-object v5, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bTm:Leld;

    invoke-interface {v3, v4, v5}, Leev;->a(Lesm;Leld;)V

    .line 93
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 96
    :cond_0
    return-void
.end method

.method public final aoi()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 134
    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 135
    invoke-interface {v4, v1}, Leev;->setEnabled(Z)V

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 137
    :cond_0
    return-void
.end method

.method public final b(Leen;)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXc:Leex;

    invoke-virtual {v0, p1}, Leex;->a(Leen;)V

    .line 100
    return-void
.end method

.method public final f(Ljava/lang/String;Ljava/util/List;I)V
    .locals 10

    .prologue
    .line 103
    if-nez p3, :cond_0

    .line 104
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/SuggestionStripView;->setVisibility(I)V

    .line 131
    :goto_0
    return-void

    .line 108
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionStripView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 110
    :goto_1
    const/4 v0, 0x0

    move v2, v0

    :goto_2
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    array-length v0, v0

    if-ge v2, v0, :cond_4

    .line 111
    if-ge v2, p3, :cond_3

    .line 112
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    aget-object v3, v0, v2

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    iget-object v4, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bWZ:Leeo;

    invoke-virtual {v3, v0, p1, v4}, Lcom/google/android/search/suggest/SuggestionIconView;->a(Lcom/google/android/shared/search/Suggestion;Ljava/lang/String;Leeo;)Z

    .line 116
    :goto_3
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    aget-object v0, v0, v2

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/android/search/suggest/SuggestionIconView;->setEnabled(Z)V

    .line 117
    if-eqz v1, :cond_1

    .line 119
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    aget-object v0, v0, v2

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/search/suggest/SuggestionIconView;->setAlpha(F)V

    .line 120
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    aget-object v0, v0, v2

    iget v3, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXt:F

    neg-float v3, v3

    invoke-virtual {v0, v3}, Lcom/google/android/search/suggest/SuggestionIconView;->setTranslationY(F)V

    .line 121
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/google/android/search/suggest/SuggestionIconView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v4, 0xa0

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v4, 0x50

    int-to-long v6, v2

    const-wide/16 v8, 0x0

    mul-long/2addr v6, v8

    add-long/2addr v4, v6

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v3, Lcom/google/android/search/suggest/SuggestionStripView;->bXb:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    .line 110
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 108
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    .line 114
    :cond_3
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    aget-object v0, v0, v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bWZ:Leeo;

    invoke-virtual {v0, v3, p1, v4}, Lcom/google/android/search/suggest/SuggestionIconView;->a(Lcom/google/android/shared/search/Suggestion;Ljava/lang/String;Leeo;)Z

    goto :goto_3

    .line 129
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/SuggestionStripView;->setVisibility(I)V

    .line 130
    invoke-direct {p0}, Lcom/google/android/search/suggest/SuggestionStripView;->aof()V

    goto/16 :goto_0
.end method

.method public final hb(I)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 147
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionStripView;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 171
    :cond_0
    :goto_0
    return v0

    .line 151
    :cond_1
    sparse-switch p1, :sswitch_data_0

    .line 173
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 154
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/search/suggest/SuggestionIconView;->getId()I

    move-result v0

    goto :goto_0

    .line 157
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_0

    .line 158
    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/google/android/search/suggest/SuggestionIconView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    .line 159
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/search/suggest/SuggestionIconView;->getId()I

    move-result v0

    goto :goto_0

    .line 157
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 165
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    div-int/lit8 v1, v1, 0x2

    .line 166
    :goto_2
    if-ltz v1, :cond_0

    .line 167
    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/google/android/search/suggest/SuggestionIconView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_3

    .line 168
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/search/suggest/SuggestionIconView;->getId()I

    move-result v0

    goto :goto_0

    .line 166
    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 151
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x11 -> :sswitch_0
        0x21 -> :sswitch_2
        0x42 -> :sswitch_1
        0x82 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onFinishInflate()V
    .locals 5

    .prologue
    .line 64
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 65
    const v0, 0x3ea3d70a    # 0.32f

    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionStripView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d00a1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXt:F

    .line 67
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionStripView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0058

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 68
    const v0, 0x7f110418

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/SuggestionStripView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 69
    new-array v1, v1, [Lcom/google/android/search/suggest/SuggestionIconView;

    iput-object v1, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    .line 70
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionStripView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 72
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    iget-object v2, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    array-length v2, v2

    if-ge v3, v2, :cond_0

    .line 73
    const v2, 0x7f04017e

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 74
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/search/suggest/SuggestionIconView;

    .line 75
    iget-object v4, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXc:Leex;

    invoke-virtual {v2, v4}, Lcom/google/android/search/suggest/SuggestionIconView;->a(Leew;)V

    .line 76
    invoke-static {}, Leln;->generateViewId()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/android/search/suggest/SuggestionIconView;->setId(I)V

    .line 77
    iget-object v4, p0, Lcom/google/android/search/suggest/SuggestionStripView;->bXs:[Lcom/google/android/search/suggest/SuggestionIconView;

    aput-object v2, v4, v3

    .line 72
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 79
    :cond_0
    new-instance v0, Leeu;

    invoke-direct {v0, p0}, Leeu;-><init>(Lcom/google/android/search/suggest/SuggestionStripView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/SuggestionStripView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    return-void
.end method
