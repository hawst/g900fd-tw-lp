.class public Lcom/google/android/search/shared/actions/errors/SearchError;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# instance fields
.field private final bNG:I

.field private final bNH:I

.field private final bNI:Z

.field private final bNJ:I

.field private final bNK:I

.field private final bNL:Z

.field private bNM:Ljava/lang/String;

.field protected final mQuery:Lcom/google/android/shared/search/Query;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 97
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 98
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->mQuery:Lcom/google/android/shared/search/Query;

    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNM:Ljava/lang/String;

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNG:I

    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNH:I

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNI:Z

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNJ:I

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNK:I

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNL:Z

    .line 106
    return-void

    :cond_0
    move v0, v2

    .line 102
    goto :goto_0

    :cond_1
    move v1, v2

    .line 105
    goto :goto_1
.end method

.method protected constructor <init>(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/search/shared/actions/errors/SearchError;-><init>(Lcom/google/android/shared/search/Query;Ljava/lang/String;Lefr;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/google/android/shared/search/Query;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 85
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 86
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->mQuery:Lcom/google/android/shared/search/Query;

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNM:Ljava/lang/String;

    .line 88
    const/16 v0, 0xd3

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNG:I

    .line 89
    iput p2, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNH:I

    .line 90
    iput-boolean v1, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNI:Z

    .line 91
    iput v1, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNJ:I

    .line 92
    iput v1, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNK:I

    .line 93
    iput-boolean v1, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNL:Z

    .line 94
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/shared/search/Query;Lefr;)V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/search/shared/actions/errors/SearchError;-><init>(Lcom/google/android/shared/search/Query;Ljava/lang/String;Lefr;)V

    .line 52
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/shared/search/Query;Ljava/lang/String;Lefr;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 55
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->mQuery:Lcom/google/android/shared/search/Query;

    .line 56
    iput-object p2, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNM:Ljava/lang/String;

    .line 57
    if-eqz p3, :cond_2

    .line 58
    invoke-interface {p3}, Lefr;->aou()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNG:I

    .line 59
    invoke-interface {p3}, Lefr;->getErrorCode()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNH:I

    .line 62
    invoke-interface {p3}, Lefr;->aja()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p3}, Lefr;->aou()I

    move-result v0

    const/16 v2, 0xd8

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNI:Z

    .line 64
    instance-of v0, p3, Leiq;

    if-eqz v0, :cond_1

    .line 65
    check-cast p3, Leiq;

    .line 66
    invoke-static {p3}, Leno;->h(Leiq;)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNJ:I

    .line 67
    invoke-static {p3}, Leno;->i(Leiq;)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNK:I

    .line 68
    invoke-static {p3}, Leno;->j(Leiq;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNL:Z

    .line 83
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 62
    goto :goto_0

    .line 71
    :cond_1
    iput v1, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNJ:I

    .line 72
    iput v1, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNK:I

    .line 73
    iput-boolean v1, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNL:Z

    goto :goto_1

    .line 76
    :cond_2
    iput v1, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNG:I

    .line 77
    iput v1, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNH:I

    .line 78
    iput-boolean v1, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNI:Z

    .line 79
    iput v1, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNJ:I

    .line 80
    iput v1, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNK:I

    .line 81
    iput-boolean v1, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNL:Z

    goto :goto_1
.end method


# virtual methods
.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 209
    invoke-interface {p1, p0}, Ldvn;->b(Lcom/google/android/search/shared/actions/errors/SearchError;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final abj()Lcom/google/android/shared/search/Query;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->mQuery:Lcom/google/android/shared/search/Query;

    return-object v0
.end method

.method public final agu()Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 1

    .prologue
    .line 204
    invoke-static {}, Lcom/google/android/shared/util/MatchingAppInfo;->avc()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    return-object v0
.end method

.method public aiQ()Lcom/google/android/shared/search/Query;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->mQuery:Lcom/google/android/shared/search/Query;

    return-object v0
.end method

.method public final aiR()Z
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNM:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aiS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNM:Ljava/lang/String;

    return-object v0
.end method

.method public aiT()I
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    return v0
.end method

.method public aiU()I
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    return v0
.end method

.method public aiV()I
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    return v0
.end method

.method public final aiW()Ljava/lang/String;
    .locals 6
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 153
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNG:I

    iget v1, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNH:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string v3, "ErrorUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown error type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "U"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_1
    const-string v0, "H"

    goto :goto_0

    :pswitch_2
    const-string v0, "S"

    goto :goto_0

    :pswitch_3
    const-string v0, "G"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xd3
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public aiX()I
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    return v0
.end method

.method public aiY()I
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    return v0
.end method

.method public aiZ()Z
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x1

    return v0
.end method

.method public final aja()Z
    .locals 1

    .prologue
    .line 169
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNI:Z

    return v0
.end method

.method public final ajb()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNJ:I

    return v0
.end method

.method public final ajc()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNK:I

    return v0
.end method

.method public final ajd()Z
    .locals 1

    .prologue
    .line 194
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNL:Z

    return v0
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x0

    return v0
.end method

.method public final getErrorCode()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNH:I

    return v0
.end method

.method public getErrorMessage()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 214
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 215
    iget-object v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 216
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNG:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 217
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNH:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 218
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNI:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 219
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNJ:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 220
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNK:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 221
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/errors/SearchError;->bNL:Z

    if-eqz v0, :cond_1

    :goto_1
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 222
    return-void

    :cond_0
    move v0, v2

    .line 218
    goto :goto_0

    :cond_1
    move v1, v2

    .line 221
    goto :goto_1
.end method
