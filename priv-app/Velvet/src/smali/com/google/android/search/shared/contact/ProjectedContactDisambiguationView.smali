.class public Lcom/google/android/search/shared/contact/ProjectedContactDisambiguationView;
.super Lecx;
.source "PG"


# instance fields
.field private final bRd:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/contact/ProjectedContactDisambiguationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/shared/contact/ProjectedContactDisambiguationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lecx;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    new-instance v0, Leaf;

    invoke-direct {v0, p0}, Leaf;-><init>(Lcom/google/android/search/shared/contact/ProjectedContactDisambiguationView;)V

    iput-object v0, p0, Lcom/google/android/search/shared/contact/ProjectedContactDisambiguationView;->bRd:Landroid/view/View$OnClickListener;

    .line 54
    return-void
.end method


# virtual methods
.method protected final synthetic a(Landroid/os/Parcelable;Ljava/lang/Object;Z)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 24
    check-cast p1, Lcom/google/android/search/shared/contact/Person;

    check-cast p2, Ldzb;

    iget-object v0, p0, Lcom/google/android/search/shared/contact/ProjectedContactDisambiguationView;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040102

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonSelectItem;

    const v1, 0x7f0c0124

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    const v1, 0x7f110120

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c0126

    invoke-static {v1, v2}, Lehd;->r(Landroid/view/View;I)V

    const v1, 0x7f110134

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c0125

    invoke-static {v1, v2}, Lehd;->r(Landroid/view/View;I)V

    const v1, 0x7f110132

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c00e6

    invoke-static {v1, v2}, Lehd;->r(Landroid/view/View;I)V

    invoke-virtual {v0, p3}, Lcom/google/android/search/shared/contact/PersonSelectItem;->ez(Z)V

    if-nez p3, :cond_0

    sget-object v1, Ldzb;->bRt:Ldzb;

    if-eq p2, v1, :cond_0

    invoke-virtual {p1, p2}, Lcom/google/android/search/shared/contact/Person;->d(Ldzb;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1, v3}, Lcom/google/android/search/shared/contact/PersonSelectItem;->a(Lcom/google/android/search/shared/contact/Person;Ljava/util/List;Lesk;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p1, v3, v3}, Lcom/google/android/search/shared/contact/PersonSelectItem;->a(Lcom/google/android/search/shared/contact/Person;Ljava/util/List;Lesk;)V

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/search/shared/actions/utils/Disambiguation;Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 24
    check-cast p2, Ldzb;

    invoke-super {p0, p1, p2}, Lecx;->a(Lcom/google/android/search/shared/actions/utils/Disambiguation;Ljava/lang/Object;)V

    check-cast p1, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alD()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amv()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    const v5, 0x7f040045

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v1, :cond_2

    move v3, v1

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/contact/Contact;

    iget-object v2, p0, Lcom/google/android/search/shared/contact/ProjectedContactDisambiguationView;->mLayoutInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v2, v5, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/search/shared/contact/ContactDetailSelectItem;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/search/shared/contact/ContactDetailSelectItem;->c(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Contact;)V

    const v1, 0x7f0c00e9

    invoke-static {v2, v1}, Lehd;->r(Landroid/view/View;I)V

    const v1, 0x7f110123

    invoke-virtual {v2, v1}, Lcom/google/android/search/shared/contact/ContactDetailSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v7, 0x7f0c00e7

    invoke-static {v1, v7}, Lehd;->r(Landroid/view/View;I)V

    const v1, 0x7f110124

    invoke-virtual {v2, v1}, Lcom/google/android/search/shared/contact/ContactDetailSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v7, 0x7f0c00e8

    invoke-static {v1, v7}, Lehd;->r(Landroid/view/View;I)V

    invoke-virtual {p0, v2}, Lcom/google/android/search/shared/contact/ProjectedContactDisambiguationView;->addView(Landroid/view/View;)V

    if-nez v3, :cond_1

    iget-object v1, p0, Lcom/google/android/search/shared/contact/ProjectedContactDisambiguationView;->bRd:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v1}, Lcom/google/android/search/shared/contact/ContactDetailSelectItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_2
    move v3, v4

    goto :goto_0

    :cond_3
    return-void
.end method

.method public final a(Lecz;)V
    .locals 1

    .prologue
    .line 58
    instance-of v0, p1, Leag;

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 59
    invoke-super {p0, p1}, Lecx;->a(Lecz;)V

    .line 60
    return-void
.end method
