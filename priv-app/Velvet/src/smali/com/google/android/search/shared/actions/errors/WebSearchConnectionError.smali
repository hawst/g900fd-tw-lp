.class public Lcom/google/android/search/shared/actions/errors/WebSearchConnectionError;
.super Lcom/google/android/search/shared/actions/errors/SearchError;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private bMP:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Ldvs;

    invoke-direct {v0}, Ldvs;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/errors/WebSearchConnectionError;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/errors/SearchError;-><init>(Landroid/os/Parcel;)V

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/errors/WebSearchConnectionError;->bMP:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public constructor <init>(Lcom/google/android/shared/search/Query;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Left;

    invoke-direct {v0, p2, p3}, Left;-><init>(ILjava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/actions/errors/SearchError;-><init>(Lcom/google/android/shared/search/Query;Lefr;)V

    .line 24
    iput-object p3, p0, Lcom/google/android/search/shared/actions/errors/WebSearchConnectionError;->bMP:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public constructor <init>(Lcom/google/android/shared/search/Query;Lefr;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/google/android/search/shared/actions/errors/SearchError;-><init>(Lcom/google/android/shared/search/Query;Lefr;)V

    .line 29
    iput-object p3, p0, Lcom/google/android/search/shared/actions/errors/WebSearchConnectionError;->bMP:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public final aiT()I
    .locals 1

    .prologue
    .line 48
    const v0, 0x7f0a064a

    return v0
.end method

.method public final getErrorMessage()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Lcom/google/android/search/shared/actions/errors/SearchError;->getErrorMessage()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/errors/SearchError;->writeToParcel(Landroid/os/Parcel;I)V

    .line 54
    iget-object v0, p0, Lcom/google/android/search/shared/actions/errors/WebSearchConnectionError;->bMP:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 55
    return-void
.end method
