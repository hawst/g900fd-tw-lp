.class public Lcom/google/android/search/shared/actions/EmailAction;
.super Lcom/google/android/search/shared/actions/CommunicationAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private bMa:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bMb:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lduf;

    invoke-direct {v0}, Lduf;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/EmailAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/CommunicationAction;-><init>(Landroid/os/Parcel;)V

    .line 27
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/EmailAction;->bMa:Ljava/lang/String;

    .line 28
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/EmailAction;->bMb:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public constructor <init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/CommunicationAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    .line 21
    iput-object p2, p0, Lcom/google/android/search/shared/actions/EmailAction;->bMa:Ljava/lang/String;

    .line 22
    iput-object p3, p0, Lcom/google/android/search/shared/actions/EmailAction;->bMb:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public final Wb()Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method public final Wc()Z
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 88
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/EmailAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final afZ()Ldzb;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Ldzb;->bRp:Ldzb;

    return-object v0
.end method

.method public final agb()Ldtw;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Ldtw;->bLm:Ldtw;

    return-object v0
.end method

.method public final agc()I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x2

    return v0
.end method

.method public final canExecute()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64
    invoke-super {p0}, Lcom/google/android/search/shared/actions/CommunicationAction;->canExecute()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/search/shared/actions/EmailAction;->bMa:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/search/shared/actions/EmailAction;->bMb:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    :goto_1
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final getBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/search/shared/actions/EmailAction;->bMb:Ljava/lang/String;

    return-object v0
.end method

.method public final getSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/search/shared/actions/EmailAction;->bMa:Ljava/lang/String;

    return-object v0
.end method

.method public final i(Lcom/google/android/search/shared/contact/PersonDisambiguation;)Lcom/google/android/search/shared/actions/CommunicationAction;
    .locals 3

    .prologue
    .line 33
    new-instance v0, Lcom/google/android/search/shared/actions/EmailAction;

    iget-object v1, p0, Lcom/google/android/search/shared/actions/EmailAction;->bMa:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/search/shared/actions/EmailAction;->bMb:Ljava/lang/String;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/search/shared/actions/EmailAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 93
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/CommunicationAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 94
    iget-object v0, p0, Lcom/google/android/search/shared/actions/EmailAction;->bMa:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/google/android/search/shared/actions/EmailAction;->bMb:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 96
    return-void
.end method
