.class public Lcom/google/android/search/shared/contact/ContactDetailSelectItem;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private bRc:Lcom/google/android/search/shared/contact/Contact;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/contact/ContactDetailSelectItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/shared/contact/ContactDetailSelectItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    const v0, 0x7f0c00e9

    invoke-static {p0, v0}, Lehd;->r(Landroid/view/View;I)V

    .line 25
    return-void
.end method

.method private setTextViewText(ILjava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/contact/ContactDetailSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 44
    if-eqz v0, :cond_0

    .line 45
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 46
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    :cond_0
    return-void
.end method


# virtual methods
.method public final alR()Lcom/google/android/search/shared/contact/Contact;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/search/shared/contact/ContactDetailSelectItem;->bRc:Lcom/google/android/search/shared/contact/Contact;

    return-object v0
.end method

.method public final c(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Contact;)V
    .locals 3

    .prologue
    const v2, 0x7f110123

    .line 51
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    iput-object v0, p0, Lcom/google/android/search/shared/contact/ContactDetailSelectItem;->bRc:Lcom/google/android/search/shared/contact/Contact;

    .line 53
    invoke-virtual {p2}, Lcom/google/android/search/shared/contact/Contact;->hasValue()Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 54
    const v0, 0x7f110124

    invoke-virtual {p2}, Lcom/google/android/search/shared/contact/Contact;->alP()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/search/shared/contact/ContactDetailSelectItem;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 55
    invoke-virtual {p2}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v0

    .line 56
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 57
    invoke-virtual {p0, v2}, Lcom/google/android/search/shared/contact/ContactDetailSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    invoke-direct {p0, v2, v0}, Lcom/google/android/search/shared/contact/ContactDetailSelectItem;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0
.end method
