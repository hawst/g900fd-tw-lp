.class public Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;
.super Lcom/google/android/search/shared/actions/modular/arguments/Argument;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bOW:I

.field private bOX:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Ldww;

    invoke-direct {v0}, Ldww;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V

    .line 27
    iget v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->bOW:I

    iput v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->bOW:I

    .line 28
    iget-boolean v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->bOX:Z

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->bOX:Z

    .line 29
    return-void
.end method

.method public constructor <init>(Ljqg;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;-><init>(Ljqg;)V

    .line 19
    sget-object v0, Ljqn;->eza:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqn;

    invoke-virtual {v0}, Ljqn;->akb()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->bOW:I

    .line 21
    sget-object v0, Ljqn;->eza:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqn;

    invoke-virtual {v0}, Ljqn;->akc()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->bOX:Z

    .line 23
    return-void
.end method


# virtual methods
.method public final Pd()Z
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->bOW:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ldwu;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 66
    invoke-interface {p1, p0}, Ldwu;->a(Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final ajC()Z
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    return v0
.end method

.method public final ajE()Lcom/google/android/search/shared/actions/modular/arguments/Argument;
    .locals 1

    .prologue
    .line 71
    new-instance v0, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;

    invoke-direct {v0, p0}, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;)V

    return-object v0
.end method

.method public final ajP()Ljqg;
    .locals 3

    .prologue
    .line 76
    invoke-super {p0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajP()Ljqg;

    move-result-object v0

    .line 77
    new-instance v1, Ljqn;

    invoke-direct {v1}, Ljqn;-><init>()V

    .line 79
    sget-object v2, Ljqn;->eza:Ljsm;

    invoke-virtual {v0, v2, v1}, Ljqg;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 81
    iget v2, p0, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->bOW:I

    invoke-virtual {v1, v2}, Ljqn;->rw(I)Ljqn;

    .line 82
    iget-boolean v2, p0, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->bOX:Z

    invoke-virtual {v1, v2}, Ljqn;->iV(Z)Ljqn;

    .line 83
    return-object v0
.end method

.method public final akb()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->bOW:I

    return v0
.end method

.method public final akc()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->bOX:Z

    return v0
.end method

.method public final b(Ljqu;Landroid/content/res/Resources;)Ldws;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Ldws;->bOO:Ldws;

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->ajE()Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    return-object v0
.end method

.method public final es(Z)V
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->bOX:Z

    .line 46
    return-void
.end method

.method public final gI(I)Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->ajP()Ljqg;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 89
    return-void
.end method
