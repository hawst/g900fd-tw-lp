.class public Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;
.super Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bPB:Ljpv;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bPC:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 409
    new-instance v0, Ldxk;

    invoke-direct {v0}, Ldxk;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;Lcom/google/android/search/shared/contact/PersonDisambiguation;)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;Lcom/google/android/search/shared/actions/utils/Disambiguation;)V

    .line 76
    iget-object v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->bPB:Ljpv;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->bPB:Ljpv;

    .line 77
    iget-boolean v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->bPC:Z

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->bPC:Z

    .line 78
    return-void
.end method

.method public constructor <init>(Ljqg;Lcom/google/android/search/shared/contact/PersonDisambiguation;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;-><init>(Ljqg;Lcom/google/android/search/shared/actions/utils/Disambiguation;)V

    .line 67
    sget-object v0, Ljqi;->eyH:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqi;

    .line 69
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->bPB:Ljpv;

    .line 72
    return-void

    .line 69
    :cond_0
    iget-object v0, v0, Ljqi;->eyJ:Ljpv;

    goto :goto_0
.end method

.method private static a(Lcom/google/android/search/shared/contact/Contact;I)Ldws;
    .locals 2

    .prologue
    .line 318
    sparse-switch p1, :sswitch_data_0

    .line 329
    :cond_0
    sget-object v0, Ldws;->bOO:Ldws;

    :goto_0
    return-object v0

    .line 322
    :sswitch_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/Contact;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    new-instance v0, Ldws;

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 327
    :sswitch_1
    new-instance v0, Ldws;

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 318
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x5 -> :sswitch_1
        0x13 -> :sswitch_0
    .end sparse-switch
.end method

.method public static synthetic a(Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;Z)Z
    .locals 0

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->bPC:Z

    return p1
.end method

.method private static aE(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 215
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 216
    check-cast p0, Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    .line 219
    :cond_0
    return-object p0
.end method

.method private akE()Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;
    .locals 2

    .prologue
    .line 390
    new-instance v1, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->amf()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    :goto_0
    invoke-direct {v1, p0, v0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private eu(Z)Ljqg;
    .locals 4

    .prologue
    .line 344
    invoke-super {p0}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->ajP()Ljqg;

    move-result-object v1

    .line 345
    new-instance v2, Ljqi;

    invoke-direct {v2}, Ljqi;-><init>()V

    .line 346
    sget-object v0, Ljqi;->eyH:Ljsm;

    invoke-virtual {v1, v0, v2}, Ljqg;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 347
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->bPB:Ljpv;

    iput-object v0, v2, Ljqi;->eyJ:Ljpv;

    .line 349
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    const/4 v3, 0x0

    invoke-static {v3}, Ldyv;->f(Landroid/content/res/Resources;)Ldyv;

    move-result-object v3

    invoke-virtual {v0, v3, p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->a(Ldyv;Z)Ljoq;

    move-result-object v0

    iput-object v0, v2, Ljqi;->eyI:Ljoq;

    .line 353
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a(Ldwu;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 385
    invoke-interface {p1, p0}, Ldwu;->a(Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldzb;)V
    .locals 1

    .prologue
    .line 361
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->a(Ldzb;)V

    .line 364
    :cond_0
    return-void
.end method

.method public final a(Ljqh;)[Ljrg;
    .locals 7
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 237
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    .line 238
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->akA()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    sget-object v1, Ljqj;->eyK:Ljsm;

    invoke-virtual {p1, v1}, Ljqh;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_1
    move-object v0, v4

    .line 289
    :goto_0
    return-object v0

    .line 245
    :cond_2
    sget-object v1, Ljqj;->eyK:Ljsm;

    invoke-virtual {p1, v1}, Ljqh;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljqj;

    .line 247
    if-eqz v0, :cond_3

    .line 248
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alA()Z

    move-result v5

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v6

    invoke-virtual {v3, v5, v6, v1}, Lcom/google/android/search/shared/contact/RelationshipStatus;->a(ZZLjqj;)[Ljrg;

    move-result-object v3

    .line 253
    if-eqz v3, :cond_3

    move-object v0, v3

    .line 254
    goto :goto_0

    .line 257
    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ajO()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 258
    :cond_4
    iget-object v0, v1, Ljqj;->eyL:[Ljrg;

    goto :goto_0

    .line 259
    :cond_5
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alE()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 260
    iget-object v0, v1, Ljqj;->eyT:[Ljrg;

    goto :goto_0

    .line 261
    :cond_6
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v3

    if-nez v3, :cond_7

    .line 262
    iget-object v1, v1, Ljqj;->eyQ:[Ljrg;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alu()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->b([Ljrg;I)[Ljrg;

    move-result-object v0

    goto :goto_0

    .line 264
    :cond_7
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v3

    if-nez v3, :cond_d

    .line 265
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alD()Ljava/util/List;

    move-result-object v3

    .line 266
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_9

    move v0, v2

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 268
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 269
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_8
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 270
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->rn()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 271
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->aE(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 266
    :cond_9
    const/4 v0, 0x0

    goto :goto_1

    .line 275
    :cond_a
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v0

    .line 276
    if-gt v0, v2, :cond_c

    .line 277
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 278
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 279
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->alP()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 281
    :cond_b
    iget-object v0, v1, Ljqj;->eyS:[Ljrg;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->b([Ljrg;I)[Ljrg;

    move-result-object v0

    goto/16 :goto_0

    .line 284
    :cond_c
    iget-object v1, v1, Ljqj;->eyR:[Ljrg;

    invoke-static {v1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->b([Ljrg;I)[Ljrg;

    move-result-object v0

    goto/16 :goto_0

    :cond_d
    move-object v0, v4

    .line 289
    goto/16 :goto_0
.end method

.method public final ajC()Z
    .locals 1

    .prologue
    .line 82
    invoke-super {p0}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->ajC()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->akz()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->akA()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->ajO()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic ajE()Lcom/google/android/search/shared/actions/modular/arguments/Argument;
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->akE()Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    move-result-object v0

    return-object v0
.end method

.method public final ajH()I
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->akz()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    const/4 v0, 0x1

    .line 104
    :goto_0
    return v0

    .line 99
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->ajO()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 100
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->akA()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    .line 104
    :cond_2
    invoke-super {p0}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->ajH()I

    move-result v0

    goto :goto_0
.end method

.method public final ajO()Z
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ajO()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ajP()Ljqg;
    .locals 1

    .prologue
    .line 334
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->eu(Z)Ljqg;

    move-result-object v0

    return-object v0
.end method

.method public final akA()Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->bPC:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amv()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final akB()Ldzb;
    .locals 1

    .prologue
    .line 357
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->akB()Ldzb;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final akC()Ljpv;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->bPB:Ljpv;

    return-object v0
.end method

.method final akD()Z
    .locals 1

    .prologue
    .line 372
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->bPC:Z

    return v0
.end method

.method public final akz()Z
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alE()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljqu;Landroid/content/res/Resources;)Ldws;
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 123
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    .line 124
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->Pd()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 125
    invoke-virtual {p1}, Ljqu;->bsE()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 140
    :cond_0
    invoke-virtual {p1}, Ljqu;->bsE()I

    move-result v1

    const/4 v4, 0x4

    if-eq v1, v4, :cond_1

    const/16 v4, 0xd

    if-ne v1, v4, :cond_4

    :cond_1
    move v1, v3

    :goto_0
    if-eqz v1, :cond_5

    .line 141
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->b(Ljqu;Landroid/content/res/Resources;)Ldws;

    move-result-object v0

    .line 206
    :cond_2
    :goto_1
    return-object v0

    .line 127
    :pswitch_0
    new-instance v1, Ldws;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->als()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ldws;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    .line 129
    :pswitch_1
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/RelationshipStatus;->ama()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 130
    new-instance v1, Ldws;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amt()Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Relationship;->ams()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ldws;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    .line 134
    :cond_3
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_1

    :cond_4
    move v1, v2

    .line 140
    goto :goto_0

    .line 145
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->Pd()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v1

    if-nez v1, :cond_7

    .line 146
    :cond_6
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_1

    .line 149
    :cond_7
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 150
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alF()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {p1}, Ljqu;->bsE()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->a(Lcom/google/android/search/shared/contact/Contact;I)Ldws;

    move-result-object v0

    goto :goto_1

    .line 155
    :cond_8
    invoke-virtual {p1}, Ljqu;->bsE()I

    move-result v1

    .line 157
    sparse-switch v1, :sswitch_data_0

    .line 165
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_1

    .line 162
    :sswitch_0
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alD()Ljava/util/List;

    move-result-object v1

    .line 168
    if-nez v1, :cond_9

    .line 169
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_1

    .line 173
    :cond_9
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    .line 174
    iget-object v0, p1, Ljqu;->ezx:[I

    .line 175
    if-nez v4, :cond_a

    .line 176
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_1

    .line 177
    :cond_a
    array-length v5, v0

    if-ne v5, v3, :cond_e

    .line 178
    aget v0, v0, v2

    .line 179
    if-gez v0, :cond_b

    .line 180
    add-int/2addr v0, v4

    .line 182
    :cond_b
    if-ltz v0, :cond_c

    if-lt v0, v4, :cond_d

    .line 184
    :cond_c
    new-instance v0, Ldws;

    const-string v1, ""

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 186
    :cond_d
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {p1}, Ljqu;->bsE()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->a(Lcom/google/android/search/shared/contact/Contact;I)Ldws;

    move-result-object v0

    goto/16 :goto_1

    .line 190
    :cond_e
    invoke-static {v0, v1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->a([ILjava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 191
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 192
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 193
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_f
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 194
    invoke-virtual {p1}, Ljqu;->bsE()I

    move-result v4

    invoke-static {v0, v4}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->a(Lcom/google/android/search/shared/contact/Contact;I)Ldws;

    move-result-object v0

    .line 196
    invoke-virtual {v0}, Ldws;->Pd()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 199
    invoke-virtual {v0}, Ldws;->getObject()Ljava/lang/Object;

    move-result-object v0

    .line 202
    if-eqz v0, :cond_f

    invoke-static {v0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->aE(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 203
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 206
    :cond_10
    new-instance v0, Ldws;

    iget-object v1, p1, Ljqu;->ezy:Ljra;

    invoke-static {p2}, Lerr;->h(Landroid/content/res/Resources;)Z

    move-result v3

    invoke-static {v1, v2, v3}, Lerr;->a(Ljra;Ljava/util/List;Z)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 125
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 157
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x5 -> :sswitch_0
        0x13 -> :sswitch_0
    .end sparse-switch
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->akE()Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    move-result-object v0

    return-object v0
.end method

.method public final ev(Z)V
    .locals 0

    .prologue
    .line 380
    iput-boolean p1, p0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->bPC:Z

    .line 381
    return-void
.end method

.method protected final f(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z
    .locals 2
    .param p1    # Lcom/google/android/search/shared/actions/modular/arguments/Argument;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 395
    instance-of v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    if-nez v0, :cond_0

    .line 396
    const/4 v0, 0x0

    .line 399
    :goto_0
    return v0

    .line 398
    :cond_0
    check-cast p1, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    .line 399
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    iget-object v1, p1, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-static {v0, v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Lcom/google/android/search/shared/contact/PersonDisambiguation;)Z

    move-result v0

    goto :goto_0
.end method

.method public final gI(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 224
    if-eq p1, v0, :cond_0

    const/4 v1, 0x5

    if-eq p1, v1, :cond_0

    const/16 v1, 0x13

    if-eq p1, v1, :cond_0

    const/4 v1, 0x4

    if-eq p1, v1, :cond_0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/16 v1, 0x9

    if-eq p1, v1, :cond_0

    const/16 v1, 0x8

    if-eq p1, v1, :cond_0

    const/16 v1, 0xd

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final h(Ljava/lang/Object;I)Ldws;
    .locals 2

    .prologue
    .line 303
    check-cast p1, Lcom/google/android/search/shared/contact/Person;

    .line 304
    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    .line 305
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->oK()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    new-instance v0, Ldws;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    .line 314
    :goto_0
    return-object v0

    .line 308
    :cond_0
    new-instance v1, Ldws;

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->als()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lerr;->lo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ldws;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    .line 311
    :cond_1
    const/16 v0, 0xd

    if-ne p2, v0, :cond_2

    .line 312
    new-instance v0, Ldws;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Ldws;-><init>(Landroid/net/Uri;)V

    goto :goto_0

    .line 314
    :cond_2
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_0
.end method

.method public final r(ZZ)Ljqg;
    .locals 1

    .prologue
    .line 340
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->eu(Z)Ljqg;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 404
    invoke-direct {p0, v1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->eu(Z)Ljqg;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 405
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 406
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->bPC:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 407
    return-void

    :cond_0
    move v0, v1

    .line 406
    goto :goto_0
.end method
