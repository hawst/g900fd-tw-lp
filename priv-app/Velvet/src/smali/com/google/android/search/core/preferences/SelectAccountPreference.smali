.class public Lcom/google/android/search/core/preferences/SelectAccountPreference;
.super Landroid/preference/ListPreference;
.source "PG"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mNetworkClient:Lfcx;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    .line 21
    iput-object p1, p0, Lcom/google/android/search/core/preferences/SelectAccountPreference;->mContext:Landroid/content/Context;

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    iput-object p1, p0, Lcom/google/android/search/core/preferences/SelectAccountPreference;->mContext:Landroid/content/Context;

    .line 27
    return-void
.end method


# virtual methods
.method public final a(Lfcx;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/search/core/preferences/SelectAccountPreference;->mNetworkClient:Lfcx;

    .line 31
    return-void
.end method

.method public showDialog(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/search/core/preferences/SelectAccountPreference;->mNetworkClient:Lfcx;

    invoke-interface {v0}, Lfcx;->awo()Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/google/android/search/core/preferences/SelectAccountPreference;->mContext:Landroid/content/Context;

    const v1, 0x7f0a0150

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 41
    :goto_0
    return-void

    .line 39
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->showDialog(Landroid/os/Bundle;)V

    goto :goto_0
.end method
