.class public abstract Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;
.super Landroid/preference/PreferenceFragment;
.source "PG"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field private bnK:Landroid/widget/Switch;

.field private bnL:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method private Uh()V
    .locals 5

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 180
    if-nez v0, :cond_1

    .line 192
    :cond_0
    return-void

    .line 182
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)Z

    move-result v1

    .line 185
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    .line 186
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 187
    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v3

    .line 188
    invoke-virtual {v3}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->jg(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 189
    invoke-virtual {v3, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 186
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 167
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 168
    return-void
.end method


# virtual methods
.method protected final Uf()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->bnL:Landroid/os/Handler;

    new-instance v1, Lczg;

    invoke-direct {v1, p0}, Lczg;-><init>(Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 97
    invoke-direct {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->Uh()V

    .line 98
    return-void
.end method

.method protected abstract Ug()I
.end method

.method protected a(Landroid/content/SharedPreferences;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x1

    invoke-interface {p1, p2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected jf(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/4 v5, -0x2

    const/4 v4, 0x0

    .line 114
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 115
    instance-of v1, v0, Landroid/preference/PreferenceActivity;

    if-eqz v1, :cond_0

    .line 116
    new-instance v1, Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00c3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v4, v4, v2, v4}, Landroid/widget/Switch;->setPadding(IIII)V

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v6, v6}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    const/16 v3, 0x15

    invoke-direct {v2, v5, v5, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    iput-object v1, p0, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->bnK:Landroid/widget/Switch;

    .line 118
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 121
    invoke-virtual {p0, v0, p1}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)Z

    move-result v1

    .line 123
    iget-object v2, p0, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->bnK:Landroid/widget/Switch;

    invoke-virtual {v2, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 124
    iget-object v1, p0, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->bnK:Landroid/widget/Switch;

    new-instance v2, Lczh;

    invoke-direct {v2, p0, v0, p1}, Lczh;-><init>(Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;Landroid/content/SharedPreferences;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 133
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->setHasOptionsMenu(Z)V

    .line 135
    :cond_0
    return-void
.end method

.method protected jg(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-static {v0}, Lchr;->a(Landroid/preference/PreferenceManager;)V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->Ug()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->addPreferencesFromResource(I)V

    .line 47
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    .line 48
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 49
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 50
    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 52
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->bnL:Landroid/os/Handler;

    .line 53
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0

    .prologue
    .line 208
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 209
    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 57
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 58
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->bnK:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->bnK:Landroid/widget/Switch;

    invoke-virtual {v0, v3}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iput-object v3, p0, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->bnK:Landroid/widget/Switch;

    .line 59
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 61
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Lfaq;->iw(I)V

    .line 65
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 72
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 73
    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->jf(Ljava/lang/String;)V

    .line 76
    :cond_0
    invoke-direct {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->Uh()V

    .line 77
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-static {v0}, Lcxu;->l(Landroid/preference/Preference;)V

    .line 78
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->Uf()V

    .line 83
    return-void
.end method
