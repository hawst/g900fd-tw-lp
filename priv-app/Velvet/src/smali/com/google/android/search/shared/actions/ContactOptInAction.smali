.class public Lcom/google/android/search/shared/actions/ContactOptInAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private bLY:Z

.field private final bLZ:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Ldue;

    invoke-direct {v0}, Ldue;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/ContactOptInAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 28
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/ContactOptInAction;->bLY:Z

    .line 29
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/ContactOptInAction;->bLZ:Ljava/lang/String;

    .line 30
    return-void

    .line 28
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/ContactOptInAction;->bLY:Z

    .line 23
    iput-object p2, p0, Lcom/google/android/search/shared/actions/ContactOptInAction;->bLZ:Ljava/lang/String;

    .line 24
    return-void
.end method


# virtual methods
.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/ContactOptInAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final afW()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ContactOptInAction;->bLZ:Ljava/lang/String;

    return-object v0
.end method

.method public final ahp()Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/ContactOptInAction;->bLY:Z

    return v0
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    return v0
.end method

.method public final el(Z)V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/ContactOptInAction;->bLY:Z

    .line 34
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 57
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/ContactOptInAction;->bLY:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 58
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ContactOptInAction;->bLZ:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 59
    return-void

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
