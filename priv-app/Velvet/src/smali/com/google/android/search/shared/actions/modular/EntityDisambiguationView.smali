.class public Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;
.super Lecx;
.source "PG"


# instance fields
.field private bNR:Ldvt;

.field private mImageLoader:Lesm;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lecx;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/Parcelable;Ljava/lang/Object;Z)Landroid/view/View;
    .locals 6

    .prologue
    .line 15
    move-object v0, p1

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;

    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;->mLayoutInflater:Landroid/view/LayoutInflater;

    iget-object v3, p0, Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;->mImageLoader:Lesm;

    iget-object v4, p0, Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;->bNR:Ldvt;

    const/4 v5, 0x0

    move-object v2, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->a(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lesm;Ldvt;Ljpw;)Lcom/google/android/search/shared/actions/modular/EntitySelectItem;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lesm;Ldvt;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;->mImageLoader:Lesm;

    .line 35
    iput-object p2, p0, Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;->bNR:Ldvt;

    .line 36
    return-void
.end method
