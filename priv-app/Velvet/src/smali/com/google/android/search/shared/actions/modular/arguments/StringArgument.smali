.class public Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;
.super Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private bPM:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    new-instance v0, Ldxp;

    invoke-direct {v0}, Ldxp;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;)V

    .line 34
    iget v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;->bPM:I

    iput v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;->bPM:I

    .line 35
    return-void
.end method

.method public constructor <init>(Ljqg;)V
    .locals 1

    .prologue
    .line 26
    sget-object v0, Ljrh;->eAt:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljrh;

    invoke-virtual {v0}, Ljrh;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;-><init>(Ljqg;Ljava/lang/Object;)V

    .line 28
    sget-object v0, Ljrh;->eAt:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljrh;

    invoke-virtual {v0}, Ljrh;->btj()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;->bPM:I

    .line 30
    return-void
.end method

.method private akL()Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;
    .locals 1

    .prologue
    .line 86
    new-instance v0, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;

    invoke-direct {v0, p0}, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ldwu;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 81
    invoke-interface {p1, p0}, Ldwu;->a(Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final ajC()Z
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic ajE()Lcom/google/android/search/shared/actions/modular/arguments/Argument;
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;->akL()Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;

    move-result-object v0

    return-object v0
.end method

.method public final ajP()Ljqg;
    .locals 3

    .prologue
    .line 43
    invoke-super {p0}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->ajP()Ljqg;

    move-result-object v1

    .line 44
    new-instance v2, Ljrh;

    invoke-direct {v2}, Ljrh;-><init>()V

    .line 45
    sget-object v0, Ljrh;->eAt:Ljsm;

    invoke-virtual {v1, v0, v2}, Ljqg;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 47
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljrh;->yw(Ljava/lang/String;)Ljrh;

    .line 50
    :cond_0
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;->bPM:I

    invoke-virtual {v2, v0}, Ljrh;->rO(I)Ljrh;

    .line 51
    return-object v1
.end method

.method public final b(Ljqu;Landroid/content/res/Resources;)Ldws;
    .locals 2

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Ldws;

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Ldws;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;->akL()Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;

    move-result-object v0

    return-object v0
.end method

.method public final gI(I)Z
    .locals 1

    .prologue
    .line 76
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isMultiLine()Z
    .locals 2

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;->bPM:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic setValue(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;->setValue(Ljava/lang/String;)V

    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 61
    invoke-static {p1}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->setValue(Ljava/lang/Object;)V

    .line 62
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;->ajP()Ljqg;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 92
    return-void
.end method
