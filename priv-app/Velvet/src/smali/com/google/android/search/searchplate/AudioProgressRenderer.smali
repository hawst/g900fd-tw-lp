.class public Lcom/google/android/search/searchplate/AudioProgressRenderer;
.super Landroid/view/View;
.source "PG"


# instance fields
.field private anR:Z

.field private final bFP:Landroid/graphics/drawable/Drawable;

.field private final bFQ:Landroid/animation/TimeAnimator;

.field private final bFR:I

.field private final bFS:I

.field private final bFT:I

.field private final bFU:I

.field private bFV:I

.field private bFW:J

.field private bFX:[I

.field private bFY:[I

.field private bFZ:I

.field private bGa:Ldtb;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/searchplate/AudioProgressRenderer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 100
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/searchplate/AudioProgressRenderer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 107
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->anR:Z

    .line 109
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/AudioProgressRenderer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFP:Landroid/graphics/drawable/Drawable;

    .line 110
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/AudioProgressRenderer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0023

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFR:I

    .line 111
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/AudioProgressRenderer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0026

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFS:I

    .line 112
    iget v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFR:I

    iget v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFS:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFT:I

    .line 113
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/AudioProgressRenderer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0024

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFU:I

    .line 115
    const/16 v0, 0x12c

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFY:[I

    .line 117
    new-instance v0, Landroid/animation/TimeAnimator;

    invoke-direct {v0}, Landroid/animation/TimeAnimator;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFQ:Landroid/animation/TimeAnimator;

    .line 118
    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFQ:Landroid/animation/TimeAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/TimeAnimator;->setRepeatCount(I)V

    .line 119
    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFQ:Landroid/animation/TimeAnimator;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/animation/TimeAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 120
    return-void
.end method


# virtual methods
.method public final a(Ldtb;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bGa:Ldtb;

    .line 151
    return-void
.end method

.method public final aeP()V
    .locals 4

    .prologue
    .line 154
    iget-wide v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFW:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 155
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFW:J

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFQ:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFQ:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->start()V

    .line 159
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->anR:Z

    .line 160
    return-void
.end method

.method public final aeQ()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 163
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->anR:Z

    if-nez v0, :cond_0

    .line 171
    :goto_0
    return-void

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFQ:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->cancel()V

    .line 168
    iput-boolean v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->anR:Z

    .line 169
    iput v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFV:I

    .line 170
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFW:J

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 124
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 125
    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFQ:Landroid/animation/TimeAnimator;

    new-instance v1, Ldqr;

    invoke-direct {v1, p0}, Ldqr;-><init>(Lcom/google/android/search/searchplate/AudioProgressRenderer;)V

    invoke-virtual {v0, v1}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    .line 132
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFQ:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->cancel()V

    .line 137
    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFQ:Landroid/animation/TimeAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    .line 138
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 139
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    .line 318
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 319
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->anR:Z

    if-nez v0, :cond_1

    .line 329
    :cond_0
    return-void

    .line 322
    :cond_1
    iget v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFV:I

    if-nez v0, :cond_3

    .line 323
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    int-to-double v0, v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v0, v2

    iget v2, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFR:I

    iget v3, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFS:I

    add-int/2addr v2, v3

    int-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFV:I

    iget v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFV:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFV:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFX:[I

    .line 324
    :cond_2
    iget v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFV:I

    if-eqz v0, :cond_0

    .line 328
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/16 v0, 0x3a98

    iget v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFV:I

    div-int v3, v0, v1

    iget-wide v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFW:J

    sub-long v0, v4, v0

    long-to-int v2, v0

    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFY:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v1, v2, 0x32

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v6

    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bGa:Ldtb;

    if-nez v0, :cond_4

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFZ:I

    :goto_1
    if-gt v1, v6, :cond_5

    iget-object v7, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFY:[I

    aput v0, v7, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bGa:Ldtb;

    invoke-interface {v0}, Ldtb;->afQ()I

    move-result v0

    mul-int/lit8 v0, v0, 0x64

    goto :goto_0

    :cond_5
    add-int/lit8 v0, v6, 0x1

    iput v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFZ:I

    div-int v0, v2, v3

    iget v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFV:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v6

    iget-object v7, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFX:[I

    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFY:[I

    array-length v0, v0

    iget v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFV:I

    div-int v1, v0, v1

    const/4 v0, 0x0

    add-int/lit8 v2, v6, -0x1

    mul-int/2addr v2, v1

    const/4 v8, 0x0

    invoke-static {v2, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v8, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFY:[I

    array-length v8, v8

    mul-int/2addr v1, v6

    invoke-static {v8, v1}, Ljava/lang/Math;->min(II)I

    move-result v8

    move v1, v2

    :goto_2
    if-ge v1, v8, :cond_7

    iget v9, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFZ:I

    if-ge v1, v9, :cond_6

    iget-object v9, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFY:[I

    aget v9, v9, v1

    add-int/2addr v0, v9

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_7
    sub-int v1, v8, v2

    if-nez v1, :cond_9

    const/4 v0, 0x0

    :goto_3
    aput v0, v7, v6

    const/4 v0, 0x0

    :goto_4
    iget v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFV:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFX:[I

    aget v1, v1, v0

    iget-wide v6, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFW:J

    mul-int v2, v0, v3

    int-to-long v8, v2

    add-long/2addr v6, v8

    sub-long v6, v4, v6

    long-to-int v2, v6

    const/16 v6, 0x12c

    if-ge v2, v6, :cond_a

    mul-int/2addr v1, v2

    div-int/lit16 v1, v1, 0x12c

    :cond_8
    :goto_5
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/AudioProgressRenderer;->getHeight()I

    move-result v2

    iget v6, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFU:I

    iget v7, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFU:I

    sub-int v7, v2, v7

    mul-int/2addr v1, v7

    div-int/lit16 v1, v1, 0x2710

    add-int/2addr v1, v6

    iget-object v6, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFP:Landroid/graphics/drawable/Drawable;

    iget v7, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFT:I

    mul-int/2addr v7, v0

    sub-int v1, v2, v1

    iget v8, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFT:I

    mul-int/2addr v8, v0

    iget v9, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFR:I

    add-int/2addr v8, v9

    invoke-virtual {v6, v7, v1, v8, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFP:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_9
    sub-int v1, v8, v2

    div-int/2addr v0, v1

    goto :goto_3

    :cond_a
    const/16 v6, 0x14b4

    if-ge v2, v6, :cond_8

    add-int/lit16 v2, v2, -0x12c

    const-wide v6, 0x408f400000000000L    # 1000.0

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    int-to-double v10, v2

    mul-double/2addr v8, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    mul-double/2addr v8, v10

    const-wide v10, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v8, v10

    const-wide v10, 0x408f400000000000L    # 1000.0

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    const-wide v8, 0x3fe6666666666666L    # 0.7

    int-to-double v10, v2

    mul-double/2addr v8, v10

    const-wide v10, 0x408f400000000000L    # 1000.0

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->exp(D)D

    move-result-wide v8

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v2, v6

    add-int/2addr v1, v2

    goto :goto_5
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 187
    instance-of v0, p1, Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 188
    invoke-super {p0, p1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 191
    :cond_0
    check-cast p1, Landroid/os/Bundle;

    .line 192
    const-string v0, "AudioProgressRenderer.animationStartTimeMs"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFW:J

    .line 193
    const-string v0, "AudioProgressRenderer.micReadingsArray"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFY:[I

    .line 194
    const-string v0, "AudioProgressRenderer.currentMicReading"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFZ:I

    .line 195
    const-string v0, "parentState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 196
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 200
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 201
    const-string v1, "parentState"

    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 202
    const-string v1, "AudioProgressRenderer.animationStartTimeMs"

    iget-wide v2, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFW:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 203
    const-string v1, "AudioProgressRenderer.micReadingsArray"

    iget-object v2, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFY:[I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 204
    const-string v1, "AudioProgressRenderer.currentMicReading"

    iget v2, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->bFZ:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 205
    return-object v0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 143
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    .line 144
    if-eqz p2, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/AudioProgressRenderer;->aeQ()V

    .line 147
    :cond_0
    return-void
.end method
