.class public Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;
.super Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bus:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Ldwx;

    invoke-direct {v0}, Ldwx;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljqg;Landroid/net/Uri;I)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;-><init>(Ljqg;Ljava/lang/Object;)V

    .line 23
    iput p3, p0, Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;->bus:I

    .line 24
    return-void
.end method

.method private akd()Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;
    .locals 4

    .prologue
    .line 64
    new-instance v1, Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;->ajP()Ljqg;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Landroid/net/Uri;

    iget v3, p0, Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;->bus:I

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;-><init>(Ljqg;Landroid/net/Uri;I)V

    return-object v1
.end method


# virtual methods
.method public final a(Ldwu;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 46
    invoke-interface {p1, p0}, Ldwu;->a(Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final ajC()Z
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic ajE()Lcom/google/android/search/shared/actions/modular/arguments/Argument;
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;->akd()Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;

    move-result-object v0

    return-object v0
.end method

.method public final ajP()Ljqg;
    .locals 3

    .prologue
    .line 51
    invoke-super {p0}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->ajP()Ljqg;

    move-result-object v1

    .line 52
    new-instance v2, Ljqo;

    invoke-direct {v2}, Ljqo;-><init>()V

    .line 53
    sget-object v0, Ljqo;->ezd:Ljsm;

    invoke-virtual {v1, v0, v2}, Ljqg;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 55
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljqo;->yi(Ljava/lang/String;)Ljqo;

    .line 58
    :cond_0
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;->bus:I

    invoke-virtual {v2, v0}, Ljqo;->rx(I)Ljqo;

    .line 59
    return-object v1
.end method

.method public final b(Ljqu;Landroid/content/res/Resources;)Ldws;
    .locals 2

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljqu;->bsE()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    .line 34
    new-instance v1, Ldws;

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Landroid/net/Uri;

    invoke-direct {v1, v0}, Ldws;-><init>(Landroid/net/Uri;)V

    move-object v0, v1

    .line 36
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;->akd()Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;

    move-result-object v0

    return-object v0
.end method

.method public final gI(I)Z
    .locals 1

    .prologue
    .line 41
    const/16 v0, 0xc

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;->ajP()Ljqg;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 71
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;->bus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 72
    return-void

    .line 70
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
