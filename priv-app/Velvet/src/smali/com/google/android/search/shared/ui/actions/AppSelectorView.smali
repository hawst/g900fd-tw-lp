.class public Lcom/google/android/search/shared/ui/actions/AppSelectorView;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private bVJ:Ledq;

.field private bVK:Ledp;

.field private bVL:Landroid/widget/Spinner;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/ui/actions/AppSelectorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/shared/ui/actions/AppSelectorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method public static synthetic a(Lcom/google/android/search/shared/ui/actions/AppSelectorView;)Ledq;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/search/shared/ui/actions/AppSelectorView;->bVJ:Ledq;

    return-object v0
.end method


# virtual methods
.method public final a(Ledp;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/search/shared/ui/actions/AppSelectorView;->bVK:Ledp;

    .line 56
    return-void
.end method

.method public final a(Ledq;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/search/shared/ui/actions/AppSelectorView;->bVJ:Ledq;

    .line 52
    return-void
.end method

.method public final a(Ljava/util/List;I)V
    .locals 8

    .prologue
    .line 59
    iget-object v7, p0, Lcom/google/android/search/shared/ui/actions/AppSelectorView;->bVL:Landroid/widget/Spinner;

    new-instance v0, Ledn;

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/actions/AppSelectorView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/actions/AppSelectorView;->getId()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/actions/AppSelectorView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v4, "layout_inflater"

    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    iget-object v6, p0, Lcom/google/android/search/shared/ui/actions/AppSelectorView;->bVK:Ledp;

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Ledn;-><init>(Lcom/google/android/search/shared/ui/actions/AppSelectorView;Landroid/content/Context;ILjava/util/List;Landroid/view/LayoutInflater;Ledp;)V

    invoke-virtual {v7, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/search/shared/ui/actions/AppSelectorView;->bVL:Landroid/widget/Spinner;

    invoke-virtual {v0, p2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 63
    iget-object v0, p0, Lcom/google/android/search/shared/ui/actions/AppSelectorView;->bVL:Landroid/widget/Spinner;

    new-instance v1, Ledm;

    invoke-direct {v1, p0, p1}, Ledm;-><init>(Lcom/google/android/search/shared/ui/actions/AppSelectorView;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 74
    return-void
.end method

.method public final anI()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/search/shared/ui/actions/AppSelectorView;->bVL:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->performClick()Z

    .line 78
    return-void
.end method

.method public final b(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/search/shared/ui/actions/AppSelectorView;->bVL:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 82
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 47
    const v0, 0x7f1102d4

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/ui/actions/AppSelectorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/search/shared/ui/actions/AppSelectorView;->bVL:Landroid/widget/Spinner;

    .line 48
    return-void
.end method
