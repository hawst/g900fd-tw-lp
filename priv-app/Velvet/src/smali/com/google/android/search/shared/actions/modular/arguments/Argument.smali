.class public abstract Lcom/google/android/search/shared/actions/modular/arguments/Argument;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ParcelCreator"
    }
.end annotation


# instance fields
.field private final bOA:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bOB:Landroid/text/Spanned;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field bOD:Ljava/util/List;

.field private bOE:Ljava/util/List;

.field private bOF:Ljava/util/List;

.field private final bOe:I

.field private final bOz:I

.field private final cs:I


# direct methods
.method protected constructor <init>(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOD:Ljava/util/List;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOE:Ljava/util/List;

    .line 80
    iget v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->cs:I

    iput v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->cs:I

    .line 81
    iget v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOz:I

    iput v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOz:I

    .line 82
    iget v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOe:I

    iput v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOe:I

    .line 83
    iget-object v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOA:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOA:Ljava/lang/String;

    .line 84
    iget-object v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOB:Landroid/text/Spanned;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOB:Landroid/text/Spanned;

    .line 86
    return-void
.end method

.method protected constructor <init>(Ljqg;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOD:Ljava/util/List;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOE:Ljava/util/List;

    .line 64
    invoke-virtual {p1}, Ljqg;->getId()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->cs:I

    .line 65
    invoke-virtual {p1}, Ljqg;->ajF()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOz:I

    .line 67
    invoke-virtual {p1}, Ljqg;->bss()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOe:I

    .line 68
    invoke-virtual {p1}, Ljqg;->rn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {p1}, Ljqg;->getLabel()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOA:Ljava/lang/String;

    .line 72
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOA:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOB:Landroid/text/Spanned;

    .line 77
    :goto_0
    return-void

    .line 74
    :cond_0
    iput-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOB:Landroid/text/Spanned;

    .line 75
    iput-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOA:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Lcom/google/android/search/shared/actions/modular/arguments/Argument;Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 413
    if-eqz p0, :cond_0

    if-nez p1, :cond_4

    .line 414
    :cond_0
    if-nez p0, :cond_1

    move v3, v0

    :goto_0
    if-nez p1, :cond_2

    move v2, v0

    :goto_1
    if-ne v3, v2, :cond_3

    .line 416
    :goto_2
    return v0

    :cond_1
    move v3, v1

    .line 414
    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    .line 416
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->f(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z

    move-result v0

    goto :goto_2
.end method

.method private ajI()Ljava/util/List;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOF:Ljava/util/List;

    if-nez v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOE:Ljava/util/List;

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOF:Ljava/util/List;

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOF:Ljava/util/List;

    return-object v0
.end method

.method protected static b([Ljrg;I)[Ljrg;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 334
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 339
    const/4 v1, -0x1

    .line 340
    array-length v7, p0

    const/4 v0, 0x0

    move v3, v0

    move v4, v1

    move-object v1, v2

    :goto_0
    if-ge v3, v7, :cond_1

    aget-object v0, p0, v3

    .line 341
    invoke-virtual {v0}, Ljrg;->Pq()I

    move-result v5

    .line 344
    if-eq v5, v4, :cond_0

    .line 345
    if-eqz v1, :cond_0

    .line 346
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v2

    .line 354
    :cond_0
    invoke-virtual {v0}, Ljrg;->bti()I

    move-result v4

    if-gt v4, p1, :cond_3

    .line 340
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v4, v5

    move-object v1, v0

    goto :goto_0

    .line 359
    :cond_1
    if-eqz v1, :cond_2

    .line 360
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 362
    :cond_2
    const-class v0, Ljrg;

    invoke-static {v6, v0}, Likm;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljrg;

    return-object v0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public abstract Pd()Z
.end method

.method public abstract a(Ldwu;)Ljava/lang/Object;
.end method

.method public final a(Ldwn;)V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOE:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOE:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOF:Ljava/util/List;

    .line 171
    :cond_0
    return-void
.end method

.method public a(Legu;)V
    .locals 0

    .prologue
    .line 368
    return-void
.end method

.method public a(Ldwp;)Z
    .locals 1

    .prologue
    .line 378
    const/4 v0, 0x1

    return v0
.end method

.method public a(Ljng;)Z
    .locals 1

    .prologue
    .line 244
    const/4 v0, 0x0

    return v0
.end method

.method public a(Ljqh;)[Ljrg;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 315
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->Pd()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljqp;->eze:Ljsm;

    invoke-virtual {p1, v0}, Ljqh;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 318
    :cond_0
    const/4 v0, 0x0

    .line 321
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Ljqp;->eze:Ljsm;

    invoke-virtual {p1, v0}, Ljqh;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqp;

    iget-object v0, v0, Ljqp;->ezf:[Ljrg;

    goto :goto_0
.end method

.method public abstract ajC()Z
.end method

.method public abstract ajE()Lcom/google/android/search/shared/actions/modular/arguments/Argument;
.end method

.method public final ajF()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOz:I

    return v0
.end method

.method public ajG()Z
    .locals 1

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajH()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ajH()I
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x0

    return v0
.end method

.method public final ajJ()V
    .locals 2

    .prologue
    .line 184
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajI()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwn;

    .line 185
    invoke-interface {v0}, Ldwn;->ajQ()V

    goto :goto_0

    .line 187
    :cond_0
    return-void
.end method

.method public final ajK()V
    .locals 2

    .prologue
    .line 193
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajI()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwn;

    .line 194
    invoke-interface {v0}, Ldwn;->ajR()V

    goto :goto_0

    .line 196
    :cond_0
    return-void
.end method

.method public final ajL()V
    .locals 2

    .prologue
    .line 202
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajI()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwn;

    .line 203
    invoke-interface {v0}, Ldwn;->ajS()V

    goto :goto_0

    .line 205
    :cond_0
    return-void
.end method

.method public final ajM()V
    .locals 2

    .prologue
    .line 211
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajI()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwn;

    .line 212
    invoke-interface {v0}, Ldwn;->ajT()V

    goto :goto_0

    .line 214
    :cond_0
    return-void
.end method

.method public ajN()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 223
    const/4 v0, 0x0

    return-object v0
.end method

.method public ajO()Z
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    return v0
.end method

.method public ajP()Ljqg;
    .locals 2

    .prologue
    .line 280
    new-instance v0, Ljqg;

    invoke-direct {v0}, Ljqg;-><init>()V

    .line 281
    iget v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->cs:I

    invoke-virtual {v0, v1}, Ljqg;->rq(I)Ljqg;

    .line 282
    iget v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOz:I

    invoke-virtual {v0, v1}, Ljqg;->rr(I)Ljqg;

    .line 283
    iget v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOe:I

    invoke-virtual {v0, v1}, Ljqg;->rs(I)Ljqg;

    .line 284
    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOA:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 285
    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOA:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljqg;->yh(Ljava/lang/String;)Ljqg;

    .line 287
    :cond_0
    return-object v0
.end method

.method public ajk()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOe:I

    return v0
.end method

.method public abstract b(Ljqu;Landroid/content/res/Resources;)Ldws;
.end method

.method public c(Lcom/google/android/search/shared/actions/modular/ModularAction;)V
    .locals 6

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOD:Ljava/util/List;

    .line 114
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->aji()Ljrl;

    move-result-object v0

    iget-object v1, v0, Ljrl;->ezr:[Ljng;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 115
    invoke-virtual {v3}, Ljng;->bqD()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljng;->bqC()I

    move-result v4

    iget v5, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->cs:I

    if-ne v4, v5, :cond_0

    .line 116
    iget-object v4, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOD:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119
    :cond_1
    return-void
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajE()Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 391
    const/4 v0, 0x0

    return v0
.end method

.method protected f(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z
    .locals 2
    .param p1    # Lcom/google/android/search/shared/actions/modular/arguments/Argument;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 403
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOB:Landroid/text/Spanned;

    iget-object v1, p1, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOB:Landroid/text/Spanned;

    invoke-static {v0, v1}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public abstract gI(I)Z
.end method

.method public final getId()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->cs:I

    return v0
.end method

.method public final getLabel()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOB:Landroid/text/Spanned;

    return-object v0
.end method

.method public r(ZZ)Ljqg;
    .locals 1

    .prologue
    .line 300
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajP()Ljqg;

    move-result-object v0

    return-object v0
.end method
