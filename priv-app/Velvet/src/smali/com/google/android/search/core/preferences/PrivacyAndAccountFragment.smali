.class public Lcom/google/android/search/core/preferences/PrivacyAndAccountFragment;
.super Lcom/google/android/search/core/preferences/SettingsFragmentBase;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;-><init>()V

    return-void
.end method

.method public static k(Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 30
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 31
    const-class v1, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 32
    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/search/core/preferences/PrivacyAndAccountFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 33
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 34
    const-string v2, "fast_account_select_mode"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 35
    const-string v2, "skip_now_opt_in"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 36
    const-string v2, ":android:show_fragment_args"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 37
    return-object v0
.end method


# virtual methods
.method protected Tj()I
    .locals 1

    .prologue
    .line 42
    const v0, 0x7f070021

    return v0
.end method

.method protected final a(Lgql;)Lcxt;
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 47
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/PrivacyAndAccountFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    .line 48
    new-instance v0, Lcxw;

    invoke-virtual {p1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/PrivacyAndAccountFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {p1}, Lgql;->aJs()Lchr;

    move-result-object v3

    if-eqz v7, :cond_0

    const-string v4, "fast_account_select_mode"

    invoke-virtual {v7, v4, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    move v4, v5

    :goto_0
    if-eqz v7, :cond_1

    const-string v8, "skip_now_opt_in"

    invoke-virtual {v7, v8, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_1

    :goto_1
    invoke-direct/range {v0 .. v5}, Lcxw;-><init>(Lcfo;Landroid/app/Activity;Lchr;ZZ)V

    return-object v0

    :cond_0
    move v4, v6

    goto :goto_0

    :cond_1
    move v5, v6

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->onCreate(Landroid/os/Bundle;)V

    .line 59
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/PrivacyAndAccountFragment;->setHasOptionsMenu(Z)V

    .line 60
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 6

    .prologue
    .line 65
    invoke-super {p0, p1, p2}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 67
    new-instance v0, Lgpk;

    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/PrivacyAndAccountFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lgpk;-><init>(Landroid/content/Context;)V

    const-string v1, "now_settings"

    invoke-virtual {v0, v1}, Lgpk;->nv(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 68
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/PrivacyAndAccountFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "now_settings"

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v0

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v3

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v0

    invoke-virtual {v0}, Lcrh;->Su()Z

    move-result v5

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lgbk;->a(Landroid/view/Menu;Landroid/app/Activity;Ljava/lang/String;Landroid/accounts/Account;Landroid/net/Uri;Z)V

    .line 72
    return-void
.end method
