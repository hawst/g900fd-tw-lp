.class public Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;
.super Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private aUh:Lcxs;

.field private bnY:Lcom/google/android/search/core/preferences/NotificationGroupPreference;

.field private bnZ:Landroid/preference/RingtonePreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected final Ug()I
    .locals 1

    .prologue
    .line 35
    const v0, 0x7f07001c

    return v0
.end method

.method protected final a(Landroid/content/SharedPreferences;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 77
    iget-object v1, p0, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->aUh:Lcxs;

    invoke-virtual {v1}, Lcxs;->TH()Lcxi;

    move-result-object v1

    invoke-virtual {v1}, Lcxi;->TB()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final jf(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 71
    return-void
.end method

.method protected final jg(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->bnY:Lcom/google/android/search/core/preferences/NotificationGroupPreference;

    invoke-virtual {v0}, Lcom/google/android/search/core/preferences/NotificationGroupPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->bnZ:Landroid/preference/RingtonePreference;

    invoke-virtual {v0, p1, p2, p3}, Landroid/preference/RingtonePreference;->onActivityResult(IILandroid/content/Intent;)Z

    .line 84
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DE()Lcxs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->aUh:Lcxs;

    .line 45
    const v0, 0x7f0a0139

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/core/preferences/NotificationGroupPreference;

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->bnY:Lcom/google/android/search/core/preferences/NotificationGroupPreference;

    .line 47
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->bnY:Lcom/google/android/search/core/preferences/NotificationGroupPreference;

    invoke-virtual {v0, p0}, Lcom/google/android/search/core/preferences/NotificationGroupPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 49
    const v0, 0x7f0a013b

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/RingtonePreference;

    iput-object v0, p0, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->bnZ:Landroid/preference/RingtonePreference;

    .line 51
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->bnZ:Landroid/preference/RingtonePreference;

    invoke-virtual {v0, p0}, Landroid/preference/RingtonePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 52
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 65
    invoke-super {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->onDestroy()V

    .line 66
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->bnZ:Landroid/preference/RingtonePreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/RingtonePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 67
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 88
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->bnZ:Landroid/preference/RingtonePreference;

    if-ne p1, v0, :cond_2

    .line 89
    check-cast p2, Ljava/lang/String;

    .line 90
    const/4 v0, 0x0

    .line 91
    if-eqz p2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 92
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 94
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1, v0}, Lcxu;->a(Landroid/content/Context;Landroid/preference/Preference;Landroid/net/Uri;)V

    .line 105
    :cond_1
    :goto_0
    return v1

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->bnY:Lcom/google/android/search/core/preferences/NotificationGroupPreference;

    if-ne p1, v0, :cond_1

    .line 98
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 101
    :goto_1
    iget-object v2, p0, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->aUh:Lcxs;

    invoke-virtual {v2}, Lcxs;->TH()Lcxi;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcxi;->fr(I)V

    .line 102
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->Uf()V

    goto :goto_0

    .line 98
    :cond_3
    const/4 v0, 0x2

    goto :goto_1
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 56
    invoke-super {p0}, Lcom/google/android/search/core/preferences/cards/CardSettingsFragment;->onResume()V

    .line 57
    iget-object v1, p0, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->aUh:Lcxs;

    invoke-virtual {v1}, Lcxs;->TH()Lcxi;

    move-result-object v1

    invoke-virtual {v1}, Lcxi;->TB()I

    move-result v1

    .line 59
    iget-object v2, p0, Lcom/google/android/search/core/preferences/cards/NotificationsSettingsFragment;->bnY:Lcom/google/android/search/core/preferences/NotificationGroupPreference;

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/search/core/preferences/NotificationGroupPreference;->setChecked(Z)V

    .line 61
    return-void

    .line 59
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
