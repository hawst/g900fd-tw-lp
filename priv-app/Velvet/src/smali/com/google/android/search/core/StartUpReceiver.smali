.class public Lcom/google/android/search/core/StartUpReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 30
    const-string v2, "android.intent.action.MY_PACKAGE_REPLACED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 31
    const-string v3, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 35
    if-nez v3, :cond_0

    if-eqz v2, :cond_3

    .line 36
    :cond_0
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    .line 37
    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v3

    .line 38
    invoke-virtual {v3}, Lcfo;->DD()Lcjs;

    move-result-object v4

    .line 42
    invoke-virtual {v3}, Lcfo;->BK()Lcke;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Lcke;->cr(Z)V

    .line 44
    invoke-virtual {v3}, Lcfo;->BK()Lcke;

    move-result-object v5

    invoke-virtual {v3}, Lcfo;->DO()Lgpp;

    move-result-object v6

    invoke-static {p1, v5, v4, v6}, Lgqo;->a(Landroid/content/Context;Lcke;Lcjs;Lgpp;)V

    .line 46
    invoke-virtual {v3}, Lcfo;->DO()Lgpp;

    move-result-object v3

    invoke-interface {v3}, Lgpp;->aJj()V

    .line 47
    invoke-virtual {v2}, Lgql;->aJP()V

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 50
    const-string v5, "android.hardware.microphone"

    invoke-virtual {v3, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 51
    new-instance v5, Landroid/content/ComponentName;

    const-class v6, Lcom/google/android/googlequicksearchbox/VoiceSearchActivity;

    invoke-direct {v5, p1, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v3, v5, v0, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 56
    :cond_1
    new-instance v5, Landroid/content/ComponentName;

    const-class v6, Lcom/google/android/search/core/icingsync/ApplicationLaunchReceiver;

    invoke-direct {v5, p1, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v4}, Lcjs;->MK()Z

    move-result v4

    if-eqz v4, :cond_2

    move v0, v1

    :cond_2
    invoke-virtual {v3, v5, v0, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 64
    invoke-static {p1}, Lbwz;->ae(Landroid/content/Context;)V

    .line 66
    invoke-virtual {v2}, Lgql;->aJq()Lhhq;

    move-result-object v0

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    .line 68
    invoke-static {p1, v0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->a(Landroid/content/Context;Lhym;)V

    .line 72
    invoke-static {}, Lhgn;->aOF()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 74
    :cond_3
    return-void
.end method
