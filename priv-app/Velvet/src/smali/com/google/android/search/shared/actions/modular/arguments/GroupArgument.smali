.class public Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;
.super Lcom/google/android/search/shared/actions/modular/arguments/Argument;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bPn:Ldwn;

.field private final bPo:[Ljqy;

.field private bPp:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 203
    new-instance v0, Ldxf;

    invoke-direct {v0}, Ldxf;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V

    .line 21
    new-instance v0, Ldxe;

    invoke-direct {v0, p0}, Ldxe;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;)V

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPn:Ldwn;

    .line 57
    iget-object v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPo:[Ljqy;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPo:[Ljqy;

    .line 58
    iget v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPp:I

    iput v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPp:I

    .line 59
    return-void
.end method

.method public constructor <init>(Ljqg;)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;-><init>(Ljqg;)V

    .line 21
    new-instance v0, Ldxe;

    invoke-direct {v0, p0}, Ldxe;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;)V

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPn:Ldwn;

    .line 48
    sget-object v0, Ljqx;->ezE:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqx;

    .line 50
    iget-object v1, v0, Ljqx;->ezF:[Ljqy;

    iput-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPo:[Ljqy;

    .line 51
    invoke-virtual {v0}, Ljqx;->bsK()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljqx;->bsJ()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPp:I

    .line 53
    return-void

    .line 51
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private akp()Ljqy;
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPo:[Ljqy;

    iget v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPp:I

    aget-object v0, v0, v1

    return-object v0
.end method


# virtual methods
.method public final Pd()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 110
    iget-object v2, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;

    .line 111
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->akm()Z

    move-result v1

    if-nez v1, :cond_1

    .line 126
    :cond_0
    :goto_0
    return v0

    .line 116
    :cond_1
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->akp()Ljqy;

    move-result-object v1

    iget-object v3, v1, Ljqy;->ezJ:[I

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    aget v5, v3, v1

    .line 117
    invoke-virtual {v2, v5}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v6

    .line 118
    if-nez v6, :cond_2

    .line 119
    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    move-result-object v1

    invoke-virtual {v1, v5}, Ldvv;->gF(I)V

    goto :goto_0

    .line 122
    :cond_2
    invoke-virtual {v6}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->Pd()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 116
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 126
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Ldwu;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 190
    invoke-interface {p1, p0}, Ldwu;->a(Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ldwp;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 152
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->akp()Ljqy;

    move-result-object v1

    iget-object v2, v1, Ljqy;->ezI:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget v4, v2, v1

    .line 153
    iget-object v5, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v5, v4}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v4

    .line 154
    invoke-virtual {v4, p1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->a(Ldwp;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 158
    :goto_1
    return v0

    .line 152
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 158
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final ajC()Z
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x0

    return v0
.end method

.method public final ajE()Lcom/google/android/search/shared/actions/modular/arguments/Argument;
    .locals 1

    .prologue
    .line 195
    new-instance v0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    invoke-direct {v0, p0}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;)V

    return-object v0
.end method

.method public final ajG()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 131
    iget-object v2, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;

    .line 132
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->akm()Z

    move-result v1

    if-nez v1, :cond_1

    .line 147
    :cond_0
    :goto_0
    return v0

    .line 137
    :cond_1
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->akp()Ljqy;

    move-result-object v1

    iget-object v3, v1, Ljqy;->ezJ:[I

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    aget v5, v3, v1

    .line 138
    invoke-virtual {v2, v5}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v6

    .line 139
    if-nez v6, :cond_2

    .line 140
    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    move-result-object v1

    invoke-virtual {v1, v5}, Ldvv;->gF(I)V

    goto :goto_0

    .line 143
    :cond_2
    invoke-virtual {v6}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajG()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 137
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 147
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ajP()Ljqg;
    .locals 4

    .prologue
    .line 178
    invoke-super {p0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajP()Ljqg;

    move-result-object v0

    .line 179
    new-instance v1, Ljqx;

    invoke-direct {v1}, Ljqx;-><init>()V

    .line 180
    sget-object v2, Ljqx;->ezE:Ljsm;

    invoke-virtual {v0, v2, v1}, Ljqg;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 181
    iget v2, p0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPp:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 182
    iget v2, p0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPp:I

    invoke-virtual {v1, v2}, Ljqx;->rE(I)Ljqx;

    .line 184
    :cond_0
    iget-object v2, p0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPo:[Ljqy;

    iput-object v2, v1, Ljqx;->ezF:[Ljqy;

    .line 185
    return-object v0
.end method

.method public final akm()Z
    .locals 2

    .prologue
    .line 80
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPp:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final akn()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPp:I

    return v0
.end method

.method public final ako()[Ljqy;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPo:[Ljqy;

    return-object v0
.end method

.method public final b(Ljqu;Landroid/content/res/Resources;)Ldws;
    .locals 1

    .prologue
    .line 168
    sget-object v0, Ldws;->bOO:Ldws;

    return-object v0
.end method

.method public final c(Lcom/google/android/search/shared/actions/modular/ModularAction;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-super {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->c(Lcom/google/android/search/shared/actions/modular/ModularAction;)V

    .line 64
    iget-object v3, p0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPo:[Ljqy;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 65
    iget-object v5, v0, Ljqy;->ezJ:[I

    array-length v6, v5

    move v0, v1

    :goto_1
    if-ge v0, v6, :cond_0

    aget v7, v5, v0

    .line 66
    invoke-virtual {p1, v7}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPn:Ldwn;

    invoke-virtual {v7, v8}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->a(Ldwn;)V

    .line 65
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 64
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 69
    :cond_1
    return-void
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->ajE()Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    return-object v0
.end method

.method public final gI(I)Z
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    return v0
.end method

.method public final gM(I)V
    .locals 1

    .prologue
    .line 72
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPo:[Ljqy;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 73
    iput p1, p0, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->bPp:I

    .line 74
    return-void

    .line 72
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final gN(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->ajP()Ljqg;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 201
    return-void
.end method
