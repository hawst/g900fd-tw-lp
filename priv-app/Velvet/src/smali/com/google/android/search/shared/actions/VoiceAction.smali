.class public interface abstract Lcom/google/android/search/shared/actions/VoiceAction;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Leti;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ParcelCreator"
    }
.end annotation


# virtual methods
.method public abstract Wb()Z
.end method

.method public abstract Wc()Z
.end method

.method public abstract a(Ldvn;)Ljava/lang/Object;
.end method

.method public abstract a(ILcom/google/android/search/shared/actions/utils/CardDecision;I)V
.end method

.method public abstract a(Lcom/google/android/shared/util/MatchingAppInfo;)V
.end method

.method public abstract a(Legu;)V
.end method

.method public abstract aga()Z
.end method

.method public abstract agb()Ldtw;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract agc()I
.end method

.method public abstract agd()Z
.end method

.method public abstract age()Z
.end method

.method public abstract agh()Z
.end method

.method public abstract agi()Z
.end method

.method public abstract agj()Z
.end method

.method public abstract agk()Z
.end method

.method public abstract agl()Z
.end method

.method public abstract agm()Z
.end method

.method public abstract agn()Z
.end method

.method public abstract ago()Z
.end method

.method public abstract agp()Z
.end method

.method public abstract agq()Z
.end method

.method public abstract agr()Z
.end method

.method public abstract ags()Z
.end method

.method public abstract agt()Z
.end method

.method public abstract agu()Lcom/google/android/shared/util/MatchingAppInfo;
.end method

.method public abstract agv()V
.end method

.method public abstract agw()Z
.end method

.method public abstract agx()J
.end method

.method public abstract agy()Ldxl;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract agz()Z
.end method

.method public abstract aw(J)V
.end method

.method public abstract canExecute()Z
.end method

.method public abstract ei(Z)Z
.end method
