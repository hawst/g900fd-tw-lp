.class public Lcom/google/android/search/shared/actions/AgendaAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final anZ:I

.field private final bLC:Z

.field private final bLD:J

.field private final bLE:Z

.field private final bLF:I

.field private final bLG:I

.field private final bLH:Ljava/lang/String;

.field private final bLI:Ljava/lang/String;

.field private final bLJ:Ljava/lang/String;

.field private final bLK:Ljava/lang/String;

.field private bLL:Ljava/util/Map;

.field private final hV:J

.field private final mAgendaItems:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 292
    new-instance v0, Ldtz;

    invoke-direct {v0}, Ldtz;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/AgendaAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 80
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLL:Ljava/util/Map;

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 82
    invoke-static {v3}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->mAgendaItems:Ljava/util/List;

    move v0, v2

    .line 83
    :goto_0
    if-ge v0, v3, :cond_0

    .line 84
    iget-object v4, p0, Lcom/google/android/search/shared/actions/AgendaAction;->mAgendaItems:Ljava/util/List;

    const-class v5, Ljnm;

    invoke-static {p1, v5}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 86
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLC:Z

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->anZ:I

    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/search/shared/actions/AgendaAction;->hV:J

    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLD:J

    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLE:Z

    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLF:I

    .line 92
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLG:I

    .line 93
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLH:Ljava/lang/String;

    .line 94
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLI:Ljava/lang/String;

    .line 95
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLJ:Ljava/lang/String;

    .line 96
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLK:Ljava/lang/String;

    .line 97
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AgendaAction;->ahe()V

    .line 98
    return-void

    :cond_1
    move v0, v2

    .line 86
    goto :goto_1

    :cond_2
    move v1, v2

    .line 90
    goto :goto_2
.end method

.method public constructor <init>(Ljava/util/List;JJILjkx;)V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLL:Ljava/util/Map;

    .line 61
    iput-object p1, p0, Lcom/google/android/search/shared/actions/AgendaAction;->mAgendaItems:Ljava/util/List;

    .line 62
    iput-wide p2, p0, Lcom/google/android/search/shared/actions/AgendaAction;->hV:J

    .line 63
    iput-wide p4, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLD:J

    .line 64
    iput p6, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLG:I

    .line 66
    invoke-virtual {p7}, Ljkx;->boE()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLC:Z

    .line 67
    invoke-virtual {p7}, Ljkx;->boG()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->anZ:I

    .line 68
    invoke-virtual {p7}, Ljkx;->boF()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLE:Z

    .line 69
    iget-object v0, p7, Ljkx;->eqI:Ljns;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLF:I

    .line 72
    invoke-virtual {p7}, Ljkx;->agZ()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLH:Ljava/lang/String;

    .line 73
    invoke-virtual {p7}, Ljkx;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLI:Ljava/lang/String;

    .line 74
    invoke-virtual {p7}, Ljkx;->aha()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLJ:Ljava/lang/String;

    .line 75
    invoke-virtual {p7}, Ljkx;->ahb()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLK:Ljava/lang/String;

    .line 76
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AgendaAction;->ahe()V

    .line 77
    return-void

    .line 69
    :cond_0
    iget-object v0, p7, Ljkx;->eqI:Ljns;

    invoke-virtual {v0}, Ljns;->agW()I

    move-result v0

    goto :goto_0
.end method

.method private static a(Ljava/util/List;Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;)Ljava/util/List;
    .locals 10

    .prologue
    .line 388
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 389
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljnm;

    .line 390
    sget-object v1, Ljmz;->euH:Ljsm;

    invoke-virtual {v0, v1}, Ljnm;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lizj;

    .line 391
    if-eqz v1, :cond_3

    .line 392
    iget-object v4, v1, Lizj;->dSf:Lixr;

    if-eqz v4, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "title"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lixr;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Ljnm;->evg:Ljnu;

    invoke-virtual {v6}, Ljnu;->nj()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "start"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Lixr;->nk()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lixr;->qO(Ljava/lang/String;)Lixr;

    new-instance v6, Lamo;

    invoke-direct {v6}, Lamo;-><init>()V

    invoke-virtual {v4}, Lixr;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lamo;->V(Ljava/lang/String;)Lamo;

    move-result-object v6

    invoke-virtual {v4}, Lixr;->nk()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lamo;->t(J)Lamo;

    move-result-object v6

    invoke-virtual {v4}, Lixr;->nm()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lamo;->u(J)Lamo;

    move-result-object v6

    iget-object v7, v4, Lixr;->aeB:Ljbp;

    if-eqz v7, :cond_0

    iget-object v7, v4, Lixr;->aeB:Ljbp;

    invoke-virtual {v7}, Ljbp;->oK()Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, v4, Lixr;->aeB:Ljbp;

    invoke-virtual {v7}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lamo;->W(Ljava/lang/String;)Lamo;

    :cond_0
    iget-object v7, v0, Ljnm;->evg:Ljnu;

    if-eqz v7, :cond_1

    iget-object v7, v0, Ljnm;->evg:Ljnu;

    invoke-virtual {v7}, Ljnu;->bra()Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v0, v0, Ljnm;->evg:Ljnu;

    invoke-virtual {v0}, Ljnu;->nj()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lamo;->s(J)Lamo;

    move-result-object v0

    iget-object v4, v4, Lixr;->dOo:[Ljava/lang/String;

    array-length v4, v4

    invoke-virtual {v0, v4}, Lamo;->cc(I)Lamo;

    :cond_1
    new-instance v0, Lamk;

    invoke-direct {v0}, Lamk;-><init>()V

    iput-object v6, v0, Lamk;->aeQ:Lamo;

    invoke-virtual {p1, v5, v0}, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->a(Ljava/lang/String;Lamk;)V

    .line 393
    :cond_2
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 395
    :cond_3
    const-string v1, "AgendaAction"

    const-string v4, "AgendaItem\'s AgendaItemExtensions.entry is null. This should only happen in unit tests when quantum is enabled. Item is %"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x5

    invoke-static {v0, v1, v4, v5}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 400
    :cond_4
    return-object v2
.end method

.method private ahe()V
    .locals 14

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 210
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/AgendaAction;->agX()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLL:Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 245
    :cond_0
    return-void

    .line 213
    :cond_1
    invoke-static {}, Lior;->aYa()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLL:Ljava/util/Map;

    .line 214
    iget-wide v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->hV:J

    invoke-static {v0, v1}, Lelu;->aM(J)Landroid/text/format/Time;

    move-result-object v0

    invoke-static {v0}, Lelu;->a(Landroid/text/format/Time;)I

    move-result v8

    .line 215
    iget-wide v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLD:J

    invoke-static {v0, v1}, Lelu;->aM(J)Landroid/text/format/Time;

    move-result-object v0

    invoke-static {v0}, Lelu;->a(Landroid/text/format/Time;)I

    move-result v9

    .line 216
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->mAgendaItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljnm;

    .line 217
    iget-object v0, v1, Ljnm;->euY:Ljno;

    invoke-static {v0}, Lelu;->c(Ljno;)J

    move-result-wide v6

    invoke-static {v6, v7}, Lelu;->aM(J)Landroid/text/format/Time;

    move-result-object v0

    invoke-static {v0}, Lelu;->a(Landroid/text/format/Time;)I

    move-result v5

    iget-object v0, v1, Ljnm;->evg:Ljnu;

    if-eqz v0, :cond_3

    iget-object v0, v1, Ljnm;->euZ:Ljno;

    if-nez v0, :cond_5

    :cond_3
    new-instance v0, Landroid/util/Pair;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v2, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v2, v0

    .line 219
    :goto_0
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 220
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0, v9}, Ljava/lang/Math;->min(II)I

    move-result v11

    move v6, v7

    .line 222
    :goto_1
    if-gt v6, v11, :cond_2

    .line 225
    if-le v6, v7, :cond_c

    .line 226
    new-instance v0, Ljnm;

    invoke-direct {v0}, Ljnm;-><init>()V

    invoke-static {v1, v0}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Ljnm;

    move-object v5, v0

    .line 228
    :goto_2
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v12, v0, :cond_8

    move v0, v3

    :goto_3
    invoke-virtual {v5, v0}, Ljnm;->qS(I)Ljnm;

    .line 231
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLL:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v0, v12}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 232
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLL:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, v12, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    :cond_4
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLL:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v0, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 217
    :cond_5
    iget-object v0, v1, Ljnm;->euZ:Ljno;

    invoke-static {v0}, Lelu;->c(Ljno;)J

    move-result-wide v6

    invoke-static {v6, v7}, Lelu;->aM(J)Landroid/text/format/Time;

    move-result-object v2

    invoke-static {v2}, Lelu;->a(Landroid/text/format/Time;)I

    move-result v0

    iget v6, v2, Landroid/text/format/Time;->hour:I

    if-nez v6, :cond_7

    iget v6, v2, Landroid/text/format/Time;->minute:I

    if-nez v6, :cond_7

    iget v2, v2, Landroid/text/format/Time;->second:I

    if-nez v2, :cond_7

    move v2, v4

    :goto_4
    if-eqz v2, :cond_6

    add-int/lit8 v0, v0, -0x1

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_6
    new-instance v2, Landroid/util/Pair;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v2, v5, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_7
    move v2, v3

    goto :goto_4

    .line 228
    :cond_8
    if-ne v6, v12, :cond_9

    move v0, v4

    goto :goto_3

    :cond_9
    if-ne v6, v0, :cond_a

    const/4 v0, 0x3

    goto :goto_3

    :cond_a
    const/4 v0, 0x2

    goto :goto_3

    .line 240
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLE:Z

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLL:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 242
    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    goto :goto_5

    :cond_c
    move-object v5, v1

    goto/16 :goto_2
.end method

.method private static s(Ljava/lang/String;I)Lizj;
    .locals 2

    .prologue
    .line 309
    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    .line 310
    const/16 v1, 0x89

    invoke-virtual {v0, v1}, Lizj;->nV(I)Lizj;

    .line 311
    new-instance v1, Liwn;

    invoke-direct {v1}, Liwn;-><init>()V

    iput-object v1, v0, Lizj;->dUx:Liwn;

    .line 312
    iget-object v1, v0, Lizj;->dUx:Liwn;

    invoke-virtual {v1, p0}, Liwn;->pZ(Ljava/lang/String;)Liwn;

    .line 313
    iget-object v1, v0, Lizj;->dUx:Liwn;

    invoke-virtual {v1, p1}, Liwn;->ne(I)Liwn;

    .line 314
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;)Lizq;
    .locals 8

    .prologue
    .line 348
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/AgendaAction;->agX()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/AgendaAction;->ahc()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/search/shared/actions/AgendaAction;->gw(I)Ljava/util/List;

    move-result-object v3

    invoke-static {v3, p2}, Lcom/google/android/search/shared/actions/AgendaAction;->a(Ljava/util/List;Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Lizq;

    invoke-direct {v4}, Lizq;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v5, v0}, Landroid/text/format/Time;->setJulianDay(I)J

    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    const/16 v0, 0x12

    invoke-static {p1, v6, v7, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v0, v5}, Lcom/google/android/search/shared/actions/AgendaAction;->s(Ljava/lang/String;I)Lizj;

    move-result-object v0

    iput-object v0, v4, Lizq;->dUZ:Lizj;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lizj;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizj;

    iput-object v0, v4, Lizq;->dUX:[Lizj;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 352
    :goto_1
    new-instance v1, Lizq;

    invoke-direct {v1}, Lizq;-><init>()V

    .line 353
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lizq;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizq;

    iput-object v0, v1, Lizq;->dUW:[Lizq;

    .line 354
    return-object v1

    .line 348
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->mAgendaItems:Ljava/util/List;

    invoke-static {v0, p2}, Lcom/google/android/search/shared/actions/AgendaAction;->a(Ljava/util/List;Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Lizq;

    invoke-direct {v2}, Lizq;-><init>()V

    iget-object v3, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLI:Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/android/search/shared/actions/AgendaAction;->s(Ljava/lang/String;I)Lizj;

    move-result-object v3

    iput-object v3, v2, Lizq;->dUZ:Lizj;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lizj;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizj;

    iput-object v0, v2, Lizq;->dUX:[Lizj;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 289
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/AgendaAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final agR()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->anZ:I

    return v0
.end method

.method public final agS()Z
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLC:Z

    return v0
.end method

.method public final agT()J
    .locals 2

    .prologue
    .line 136
    iget-wide v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLD:J

    return-wide v0
.end method

.method public final agU()Z
    .locals 1

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLE:Z

    return v0
.end method

.method public final agV()Ljava/util/List;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->mAgendaItems:Ljava/util/List;

    return-object v0
.end method

.method public final agW()I
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLF:I

    return v0
.end method

.method public final agX()Z
    .locals 2

    .prologue
    .line 156
    iget v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLG:I

    const/16 v1, 0x29

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final agY()Z
    .locals 2

    .prologue
    .line 160
    iget v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLG:I

    const/16 v1, 0x5d

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final agZ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLH:Ljava/lang/String;

    return-object v0
.end method

.method public final aha()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLJ:Ljava/lang/String;

    return-object v0
.end method

.method public final ahb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLK:Ljava/lang/String;

    return-object v0
.end method

.method public final ahc()Ljava/util/List;
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLL:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 181
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    .line 187
    :cond_0
    :goto_0
    return-object v0

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLL:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 184
    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLE:Z

    if-eqz v1, :cond_0

    .line 185
    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    goto :goto_0
.end method

.method public final ahd()Ljnm;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 199
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/AgendaAction;->agX()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLL:Ljava/util/Map;

    if-eqz v1, :cond_2

    .line 200
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/AgendaAction;->ahc()Ljava/util/List;

    move-result-object v1

    .line 201
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 203
    :cond_0
    :goto_0
    return-object v0

    .line 201
    :cond_1
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/actions/AgendaAction;->gw(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljnm;

    goto :goto_0

    .line 203
    :cond_2
    iget-object v1, p0, Lcom/google/android/search/shared/actions/AgendaAction;->mAgendaItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->mAgendaItems:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljnm;

    goto :goto_0
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x0

    return v0
.end method

.method public final getStartTime()J
    .locals 2

    .prologue
    .line 132
    iget-wide v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->hV:J

    return-wide v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLI:Ljava/lang/String;

    return-object v0
.end method

.method public final gw(I)Ljava/util/List;
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLL:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 192
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    .line 194
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLL:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto :goto_0
.end method

.method public final oY()I
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLG:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 102
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 103
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->mAgendaItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->mAgendaItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljnm;

    .line 105
    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    goto :goto_0

    .line 107
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLC:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 108
    iget v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->anZ:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    iget-wide v4, p0, Lcom/google/android/search/shared/actions/AgendaAction;->hV:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 110
    iget-wide v4, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLD:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 111
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLE:Z

    if-eqz v0, :cond_2

    :goto_2
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 112
    iget v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLF:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 113
    iget v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLG:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 114
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLH:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLJ:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AgendaAction;->bLK:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 118
    return-void

    :cond_1
    move v0, v2

    .line 107
    goto :goto_1

    :cond_2
    move v1, v2

    .line 111
    goto :goto_2
.end method
