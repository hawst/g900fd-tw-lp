.class public Lcom/google/android/search/gel/GelSearchOverlayLayout;
.super Lecc;
.source "PG"

# interfaces
.implements Lwm;


# instance fields
.field private yW:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lecc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/gel/GelSearchOverlayLayout;->yW:Landroid/graphics/Rect;

    .line 23
    return-void
.end method


# virtual methods
.method public final aeD()I
    .locals 2

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/google/android/search/gel/GelSearchOverlayLayout;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/search/gel/GelSearchOverlayLayout;->yW:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/search/gel/GelSearchOverlayLayout;->yW:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final b(Landroid/graphics/Rect;)V
    .locals 5

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/search/gel/GelSearchOverlayLayout;->yW:Landroid/graphics/Rect;

    .line 35
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/search/gel/GelSearchOverlayLayout;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 36
    invoke-virtual {p0, v2}, Lcom/google/android/search/gel/GelSearchOverlayLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 37
    instance-of v1, v0, Lwm;

    if-eqz v1, :cond_0

    .line 38
    check-cast v0, Lwm;

    invoke-interface {v0, p1}, Lwm;->b(Landroid/graphics/Rect;)V

    .line 35
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 40
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 41
    iget v3, p1, Landroid/graphics/Rect;->top:I

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 42
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 43
    iget v3, p1, Landroid/graphics/Rect;->left:I

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 44
    iget v3, p1, Landroid/graphics/Rect;->right:I

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 50
    iget v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    and-int/lit8 v3, v3, 0x7

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 51
    iget v3, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    div-int/lit8 v3, v3, 0x2

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 52
    iget v3, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    div-int/lit8 v3, v3, 0x2

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 54
    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 57
    :cond_2
    return-void
.end method
