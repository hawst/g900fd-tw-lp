.class public Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;
.super Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bPN:I

.field private final bPO:Ljpy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 242
    new-instance v0, Ldxr;

    invoke-direct {v0}, Ldxr;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;)V

    .line 54
    iget v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->bPN:I

    iput v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->bPN:I

    .line 55
    iget-object v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->bPO:Ljpy;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->bPO:Ljpy;

    .line 56
    return-void
.end method

.method public constructor <init>(Ljqg;)V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Ljri;->eAv:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljri;

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;-><init>(Ljqg;Ljri;)V

    .line 41
    return-void
.end method

.method private constructor <init>(Ljqg;Ljri;)V
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p2}, Ljri;->bom()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljri;->Pu()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;-><init>(Ljqg;Ljava/lang/Object;)V

    .line 46
    invoke-virtual {p2}, Ljri;->btm()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Ljri;->btl()I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->bPN:I

    .line 49
    iget-object v0, p2, Ljri;->eAz:Ljpy;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->bPO:Ljpy;

    .line 50
    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 46
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private akQ()Z
    .locals 2

    .prologue
    .line 163
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->bPN:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;

    iget v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->bPN:I

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private akR()Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;
    .locals 2

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->akQ()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 179
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOC:Lcom/google/android/search/shared/actions/modular/ModularAction;

    iget v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->bPN:I

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    return-object v0
.end method

.method private akd()Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;
    .locals 1

    .prologue
    .line 216
    new-instance v0, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;

    invoke-direct {v0, p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljkh;)J
    .locals 2

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->ajy()J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Ldwu;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 189
    invoke-interface {p1, p0}, Ldwu;->a(Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljrk;)V
    .locals 5

    .prologue
    .line 103
    iget-object v0, p1, Ljrk;->eAF:Ljkh;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 104
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->akQ()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 105
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->akR()Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->ajy()J

    move-result-wide v0

    .line 106
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 107
    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 108
    const/16 v3, 0xb

    iget-object v4, p1, Ljrk;->eAF:Ljkh;

    invoke-virtual {v4}, Ljkh;->getHour()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 109
    const/16 v3, 0xc

    iget-object v4, p1, Ljrk;->eAF:Ljkh;

    invoke-virtual {v4}, Ljkh;->getMinute()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 112
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->setValue(Ljava/lang/Object;)V

    .line 113
    return-void

    .line 103
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ajA()[Ljrk;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 123
    const/4 v0, 0x0

    return-object v0
.end method

.method public final ajB()Z
    .locals 1

    .prologue
    .line 173
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->akQ()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 174
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->akR()Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->akR()Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akS()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ajC()Z
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic ajE()Lcom/google/android/search/shared/actions/modular/arguments/Argument;
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->akd()Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;

    move-result-object v0

    return-object v0
.end method

.method public final ajP()Ljqg;
    .locals 6

    .prologue
    .line 226
    invoke-super {p0}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->ajP()Ljqg;

    move-result-object v1

    .line 227
    new-instance v2, Ljri;

    invoke-direct {v2}, Ljri;-><init>()V

    .line 229
    sget-object v0, Ljri;->eAv:Ljsm;

    invoke-virtual {v1, v0, v2}, Ljqg;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 230
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljri;->dG(J)Ljri;

    .line 233
    :cond_0
    iget v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->bPN:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    .line 234
    sget-object v0, Ljri;->eAv:Ljsm;

    invoke-virtual {v1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljri;

    iget v2, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->bPN:I

    invoke-virtual {v0, v2}, Ljri;->rP(I)Ljri;

    .line 237
    :cond_1
    sget-object v0, Ljri;->eAv:Ljsm;

    invoke-virtual {v1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljri;

    iget-object v2, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->bPO:Ljpy;

    iput-object v2, v0, Ljri;->eAz:Ljpy;

    .line 239
    return-object v1
.end method

.method public final ajx()Ljrk;
    .locals 4

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->akQ()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 76
    new-instance v0, Ljrk;

    invoke-direct {v0}, Ljrk;-><init>()V

    .line 77
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 78
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->ajy()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 79
    new-instance v2, Ljkh;

    invoke-direct {v2}, Ljkh;-><init>()V

    const/16 v3, 0xb

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljkh;->qt(I)Ljkh;

    move-result-object v2

    const/16 v3, 0xc

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljkh;->qu(I)Ljkh;

    move-result-object v2

    const/16 v3, 0xd

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v2, v1}, Ljkh;->qv(I)Ljkh;

    move-result-object v1

    iput-object v1, v0, Ljrk;->eAF:Ljkh;

    .line 83
    return-object v0

    .line 75
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ajy()J
    .locals 4

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->akQ()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 89
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->akR()Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akS()Z

    move-result v1

    invoke-static {v1}, Lifv;->gY(Z)V

    .line 91
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->ajy()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    add-long/2addr v0, v2

    return-wide v0

    .line 88
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ajz()Z
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return v0
.end method

.method public final akM()J
    .locals 4

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->Pd()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 132
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final akN()J
    .locals 4

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->Pd()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 141
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final akO()J
    .locals 4

    .prologue
    .line 149
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->Pd()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 150
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final akP()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 157
    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->bPO:Ljpy;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->bPO:Ljpy;

    invoke-virtual {v1}, Ljpy;->getStyle()I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->akQ()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljqu;Landroid/content/res/Resources;)Ldws;
    .locals 6

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 195
    invoke-virtual {p1}, Ljqu;->bsE()I

    move-result v0

    const/16 v1, 0x14

    if-ne v0, v1, :cond_0

    .line 196
    new-instance v1, Ldws;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ldws;-><init>(J)V

    move-object v0, v1

    .line 205
    :goto_0
    return-object v0

    .line 197
    :cond_0
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->akQ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 198
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->akR()Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    move-result-object v0

    .line 199
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->Pd()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->akS()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->b(Ljqu;Landroid/content/res/Resources;)Ldws;

    move-result-object v0

    goto :goto_0

    .line 205
    :cond_1
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_0
.end method

.method public final c(Lcom/google/android/search/shared/actions/modular/ModularAction;)V
    .locals 2

    .prologue
    .line 60
    invoke-super {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->c(Lcom/google/android/search/shared/actions/modular/ModularAction;)V

    .line 61
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->akQ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->akR()Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;

    move-result-object v0

    new-instance v1, Ldxq;

    invoke-direct {v1, p0}, Ldxq;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/actions/modular/arguments/TimeOfDayArgument;->a(Ldwn;)V

    .line 71
    :cond_0
    return-void
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->akd()Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;

    move-result-object v0

    return-object v0
.end method

.method public final gI(I)Z
    .locals 1

    .prologue
    .line 210
    const/16 v0, 0x14

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->gI(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/TimeDurationArgument;->ajP()Ljqg;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 222
    return-void
.end method
