.class public Lcom/google/android/search/shared/actions/PuntAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bMH:Ljava/lang/CharSequence;

.field private final bMI:I

.field private final bMJ:I

.field private final bMK:I

.field private final bML:Landroid/content/Intent;

.field private final bMM:Z

.field private bMN:I

.field private final bnB:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    new-instance v0, Lduw;

    invoke-direct {v0}, Lduw;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/PuntAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/actions/PuntAction;-><init>(IZ)V

    .line 23
    return-void
.end method

.method public constructor <init>(IIILandroid/content/Intent;Z)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 31
    const/4 v8, 0x0

    move-object v0, p0

    move v2, p1

    move-object v3, v1

    move v4, p2

    move v5, p3

    move-object v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/search/shared/actions/PuntAction;-><init>(Ljava/lang/CharSequence;ILjava/lang/String;IILandroid/content/Intent;ZI)V

    .line 33
    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 26
    const/4 v4, 0x0

    move-object v0, p0

    move v1, p1

    move v3, v2

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/search/shared/actions/PuntAction;-><init>(IIILandroid/content/Intent;Z)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 58
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 59
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMH:Ljava/lang/CharSequence;

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMI:I

    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bnB:Ljava/lang/String;

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMJ:I

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMK:I

    .line 64
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bML:Landroid/content/Intent;

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMM:Z

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMN:I

    .line 67
    return-void

    .line 65
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/CharSequence;ILjava/lang/String;IILandroid/content/Intent;ZI)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMH:Ljava/lang/CharSequence;

    .line 48
    iput p2, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMI:I

    .line 49
    iput-object p3, p0, Lcom/google/android/search/shared/actions/PuntAction;->bnB:Ljava/lang/String;

    .line 50
    iput p4, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMJ:I

    .line 51
    iput p5, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMK:I

    .line 52
    iput-object p6, p0, Lcom/google/android/search/shared/actions/PuntAction;->bML:Landroid/content/Intent;

    .line 53
    iput-boolean p7, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMM:Z

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMN:I

    .line 55
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;ILjava/lang/String;Landroid/content/Intent;Z)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 37
    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move v4, p2

    move v5, v2

    move-object v6, p4

    move v7, p5

    move v8, v2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/search/shared/actions/PuntAction;-><init>(Ljava/lang/CharSequence;ILjava/lang/String;IILandroid/content/Intent;ZI)V

    .line 38
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Z)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 41
    move-object v0, p0

    move-object v1, p1

    move v4, v2

    move v5, v2

    move-object v6, v3

    move v7, p2

    move v8, v2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/search/shared/actions/PuntAction;-><init>(Ljava/lang/CharSequence;ILjava/lang/String;IILandroid/content/Intent;ZI)V

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 113
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/PuntAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final agc()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMN:I

    return v0
.end method

.method public final aic()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMH:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final aid()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMI:I

    return v0
.end method

.method public final aie()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMJ:I

    return v0
.end method

.method public final aif()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMK:I

    return v0
.end method

.method public final aig()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMM:Z

    return v0
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return v0
.end method

.method public final getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bML:Landroid/content/Intent;

    return-object v0
.end method

.method public final getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bnB:Ljava/lang/String;

    return-object v0
.end method

.method public final gz(I)V
    .locals 1

    .prologue
    .line 98
    const/16 v0, 0x53

    iput v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMN:I

    .line 99
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 118
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 119
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMH:Ljava/lang/CharSequence;

    invoke-static {v0, p1, p2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 120
    iget v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMI:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 121
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bnB:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 122
    iget v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMJ:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 123
    iget v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMK:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 124
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bML:Landroid/content/Intent;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 125
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMM:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 126
    iget v0, p0, Lcom/google/android/search/shared/actions/PuntAction;->bMN:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 127
    return-void

    .line 125
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
