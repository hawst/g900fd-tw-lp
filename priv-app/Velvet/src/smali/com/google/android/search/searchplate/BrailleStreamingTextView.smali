.class public Lcom/google/android/search/searchplate/BrailleStreamingTextView;
.super Landroid/widget/TextView;
.source "PG"

# interfaces
.implements Ldsx;


# instance fields
.field public bGf:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 50
    new-instance v0, Ldqt;

    const-class v1, Ljava/lang/Integer;

    const-string v2, "streamPosition"

    invoke-direct {v0, v1, v2}, Ldqt;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 66
    return-void
.end method

.method private p(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->bringPointIntoView(I)Z

    .line 94
    return-void
.end method


# virtual methods
.method public final gb(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->p(Ljava/lang/CharSequence;)V

    .line 106
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 70
    invoke-super {p0}, Landroid/widget/TextView;->onFinishInflate()V

    .line 72
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0202f5

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 73
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0202f6

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 75
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->reset()V

    .line 76
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 188
    invoke-super {p0, p1}, Landroid/widget/TextView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 189
    const-class v0, Ldsx;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 190
    return-void
.end method

.method public final reset()V
    .locals 1

    .prologue
    .line 80
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->bGf:I

    .line 82
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    return-void
.end method

.method public final x(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 112
    if-nez p1, :cond_0

    .line 113
    const-string p1, ""

    .line 116
    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 122
    if-eqz p2, :cond_1

    .line 123
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 124
    invoke-virtual {v0, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 128
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0017

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 129
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v3, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v1

    const/16 v4, 0x21

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 135
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    iget v2, p0, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->bGf:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->bGf:I

    .line 140
    new-instance v1, Landroid/text/SpannedString;

    invoke-direct {v1, v0}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v1}, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->p(Ljava/lang/CharSequence;)V

    .line 145
    return-void
.end method
