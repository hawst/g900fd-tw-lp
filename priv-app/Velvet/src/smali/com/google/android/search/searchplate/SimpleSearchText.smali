.class public Lcom/google/android/search/searchplate/SimpleSearchText;
.super Landroid/widget/EditText;
.source "PG"


# instance fields
.field private bJA:Ldtl;

.field private bJr:Ldtt;

.field private bJs:Ljava/lang/CharSequence;

.field private bJt:Z

.field private bJu:Z

.field private bJv:Z

.field private bJw:Ldta;

.field private bJx:Z

.field private bJy:Ljava/lang/CharSequence;

.field private bJz:Ldtg;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJt:Z

    .line 56
    new-instance v0, Ldtn;

    invoke-direct {v0}, Ldtn;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJw:Ldta;

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJx:Z

    .line 74
    new-instance v0, Ldst;

    invoke-direct {v0, p0}, Ldst;-><init>(Lcom/google/android/search/searchplate/SimpleSearchText;)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJA:Ldtl;

    .line 117
    new-instance v0, Ldtg;

    new-instance v1, Ldte;

    invoke-direct {v1, p1}, Ldte;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJA:Ldtl;

    invoke-direct {v0, v1, v2}, Ldtg;-><init>(Ldte;Ldtl;)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJz:Ldtg;

    .line 119
    return-void
.end method

.method public static synthetic a(Lcom/google/android/search/searchplate/SimpleSearchText;)Ldtt;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJr:Ldtt;

    return-object v0
.end method

.method private afI()V
    .locals 2

    .prologue
    .line 267
    const/4 v0, 0x0

    .line 268
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 269
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJx:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJy:Ljava/lang/CharSequence;

    .line 271
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getHint()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1, v0}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 272
    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->setHint(Ljava/lang/CharSequence;)V

    .line 274
    :cond_1
    return-void

    .line 269
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method private ax(II)V
    .locals 2

    .prologue
    .line 237
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJu:Z

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJw:Ldta;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v0, p1, p2, v1}, Ldta;->a(IILandroid/text/Editable;)V

    .line 240
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ldta;)V
    .locals 0

    .prologue
    .line 460
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 461
    iput-object p1, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJw:Ldta;

    .line 462
    return-void
.end method

.method public final a(Ldtt;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJr:Ldtt;

    .line 203
    return-void
.end method

.method public final afF()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 145
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_1

    .line 151
    :cond_0
    :goto_0
    return v0

    .line 148
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 151
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final afG()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 160
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJu:Z

    if-eqz v0, :cond_0

    .line 161
    iput-boolean v1, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJt:Z

    .line 162
    invoke-virtual {p0, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setSingleLine(Z)V

    .line 163
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->setTextSize(IF)V

    .line 165
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 166
    iget-object v1, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJs:Ljava/lang/CharSequence;

    invoke-virtual {p0, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setText(Ljava/lang/CharSequence;)V

    .line 167
    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->b(Landroid/text/Spanned;)V

    .line 168
    iput-boolean v2, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJt:Z

    .line 169
    iput-boolean v2, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJu:Z

    .line 171
    :cond_0
    return-void
.end method

.method public final afH()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 174
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJu:Z

    if-nez v0, :cond_0

    .line 175
    iput-boolean v3, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJt:Z

    .line 176
    invoke-virtual {p0, v2}, Lcom/google/android/search/searchplate/SimpleSearchText;->setSingleLine(Z)V

    .line 177
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->setLines(I)V

    .line 178
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->setTextSize(IF)V

    .line 180
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJs:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->setText(Ljava/lang/CharSequence;)V

    .line 181
    iput-boolean v2, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJt:Z

    .line 182
    iput-boolean v3, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJu:Z

    .line 184
    :cond_0
    return-void
.end method

.method public final afJ()Z
    .locals 1

    .prologue
    .line 420
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJv:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final afK()V
    .locals 2

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJz:Ldtg;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldtg;->b(Landroid/text/Editable;)V

    .line 451
    return-void
.end method

.method public final b(Landroid/text/Spanned;)V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJw:Ldta;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ldta;->a(Landroid/text/Spanned;Landroid/text/Editable;)V

    .line 157
    return-void
.end method

.method public final b(Ldto;)V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p1, Ldto;->bJs:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJs:Ljava/lang/CharSequence;

    .line 130
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJs:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJt:Z

    .line 132
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJs:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->setText(Ljava/lang/CharSequence;)V

    .line 133
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->setSelection(I)V

    .line 134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJt:Z

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJw:Ldta;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ldta;->a(Ldto;Landroid/text/Editable;)V

    .line 137
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getSelectionStart()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getSelectionEnd()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->ax(II)V

    .line 138
    return-void
.end method

.method public final ec(Z)V
    .locals 2

    .prologue
    const/high16 v1, 0x80000

    .line 189
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getInputType()I

    move-result v0

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 190
    :goto_0
    if-ne v0, p1, :cond_1

    .line 199
    :goto_1
    return-void

    .line 189
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 194
    :cond_1
    if-eqz p1, :cond_2

    .line 195
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getInputType()I

    move-result v0

    const v1, -0x80001

    and-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->setInputType(I)V

    goto :goto_1

    .line 197
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getInputType()I

    move-result v0

    or-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->setInputType(I)V

    goto :goto_1
.end method

.method public final ed(Z)V
    .locals 0

    .prologue
    .line 250
    iput-boolean p1, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJx:Z

    .line 251
    invoke-direct {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->afI()V

    .line 252
    return-void
.end method

.method public onBeginBatchEdit()V
    .locals 2

    .prologue
    .line 425
    invoke-super {p0}, Landroid/widget/EditText;->onBeginBatchEdit()V

    .line 426
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJz:Ldtg;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->hasSelection()Z

    move-result v1

    invoke-virtual {v0, v1}, Ldtg;->ee(Z)V

    .line 427
    return-void
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 3

    .prologue
    .line 323
    invoke-super {p0, p1}, Landroid/widget/EditText;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    .line 324
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJz:Ldtg;

    invoke-virtual {v0}, Ldtg;->afS()V

    new-instance v0, Ldra;

    invoke-direct {v0, v1, p0}, Ldra;-><init>(Landroid/view/inputmethod/InputConnection;Lcom/google/android/search/searchplate/SimpleSearchText;)V

    .line 328
    :goto_0
    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const v2, -0x40000100    # -1.9999695f

    and-int/2addr v1, v2

    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 329
    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const v2, 0x2000003

    or-int/2addr v1, v2

    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 330
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public onEndBatchEdit()V
    .locals 2

    .prologue
    .line 455
    invoke-super {p0}, Landroid/widget/EditText;->onEndBatchEdit()V

    .line 456
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJz:Ldtg;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldtg;->c(Landroid/text/Editable;)V

    .line 457
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 244
    invoke-super {p0}, Landroid/widget/EditText;->onFinishInflate()V

    .line 245
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJy:Ljava/lang/CharSequence;

    .line 246
    invoke-direct {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->afI()V

    .line 247
    return-void
.end method

.method public onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 299
    invoke-super {p0, p1, p2, p3}, Landroid/widget/EditText;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 301
    if-eqz p1, :cond_1

    .line 302
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->isInTouchMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJv:Z

    .line 303
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJr:Ldtt;

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJr:Ldtt;

    invoke-interface {v0}, Ldtt;->afk()V

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJz:Ldtg;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldtg;->ef(Z)V

    .line 308
    :cond_1
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 378
    invoke-super {p0, p1}, Landroid/widget/EditText;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 379
    const-class v0, Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 380
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    .prologue
    .line 360
    instance-of v0, p1, Lcom/google/android/search/searchplate/SimpleSearchText$SavedState;

    if-nez v0, :cond_0

    .line 361
    invoke-super {p0, p1}, Landroid/widget/EditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 371
    :goto_0
    return-void

    .line 365
    :cond_0
    check-cast p1, Lcom/google/android/search/searchplate/SimpleSearchText$SavedState;

    .line 366
    invoke-virtual {p1}, Lcom/google/android/search/searchplate/SimpleSearchText$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/EditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 367
    iget-object v0, p1, Lcom/google/android/search/searchplate/SimpleSearchText$SavedState;->bJC:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 368
    iget-object v0, p1, Lcom/google/android/search/searchplate/SimpleSearchText$SavedState;->bJC:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJs:Ljava/lang/CharSequence;

    .line 370
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJr:Ldtt;

    iget-object v1, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJw:Ldta;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v1, v2}, Ldta;->a(Landroid/text/Editable;)Ljava/lang/CharSequence;

    move-result-object v1

    iget v2, p1, Lcom/google/android/search/searchplate/SimpleSearchText$SavedState;->bJD:I

    invoke-interface {v0, v1, v2}, Ldtt;->a(Ljava/lang/CharSequence;I)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 337
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJt:Z

    .line 338
    invoke-super {p0}, Landroid/widget/EditText;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 339
    new-instance v1, Lcom/google/android/search/searchplate/SimpleSearchText$SavedState;

    invoke-direct {v1, v0}, Lcom/google/android/search/searchplate/SimpleSearchText$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 340
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJs:Ljava/lang/CharSequence;

    iput-object v0, v1, Lcom/google/android/search/searchplate/SimpleSearchText$SavedState;->bJC:Ljava/lang/CharSequence;

    .line 341
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getSelectionStart()I

    move-result v0

    iput v0, v1, Lcom/google/android/search/searchplate/SimpleSearchText$SavedState;->bJD:I

    .line 342
    iput-boolean v2, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJt:Z

    .line 349
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 350
    invoke-virtual {v1, v0, v2}, Lcom/google/android/search/searchplate/SimpleSearchText$SavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 351
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 352
    new-instance v1, Lcom/google/android/search/searchplate/SimpleSearchText$SavedState;

    invoke-direct {v1, v0}, Lcom/google/android/search/searchplate/SimpleSearchText$SavedState;-><init>(Landroid/os/Parcel;)V

    .line 353
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 355
    return-object v1
.end method

.method protected onSelectionChanged(II)V
    .locals 2

    .prologue
    .line 224
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onSelectionChanged(II)V

    .line 225
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJr:Ldtt;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJt:Z

    if-nez v0, :cond_0

    .line 226
    invoke-direct {p0, p1, p2}, Lcom/google/android/search/searchplate/SimpleSearchText;->ax(II)V

    .line 227
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJr:Ldtt;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Ldtt;->b(Ljava/lang/CharSequence;II)V

    .line 229
    :cond_0
    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    .line 207
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/EditText;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 208
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJr:Ldtt;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJt:Z

    if-nez v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJw:Ldta;

    iget-boolean v1, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJu:Z

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v0, v1, p1, v2}, Ldta;->a(ZLjava/lang/CharSequence;Landroid/text/Editable;)V

    .line 215
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJr:Ldtt;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getSelectionStart()I

    move-result v1

    invoke-interface {v0, p1, v1}, Ldtt;->a(Ljava/lang/CharSequence;I)V

    .line 219
    :cond_0
    invoke-direct {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->afI()V

    .line 220
    return-void
.end method

.method public onTextContextMenuItem(I)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 278
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTextContextMenuItem(I)Z

    move-result v2

    .line 279
    const v0, 0x1020022

    if-ne p1, v0, :cond_0

    .line 280
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    const-class v4, Landroid/text/style/URLSpan;

    invoke-interface {v0, v1, v3, v4}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5, v4}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 282
    :cond_0
    return v2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJw:Ldta;

    invoke-interface {v0, p1, p0}, Ldta;->a(Landroid/view/MotionEvent;Landroid/widget/EditText;)V

    .line 124
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final r(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJy:Ljava/lang/CharSequence;

    .line 260
    invoke-direct {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->afI()V

    .line 261
    return-void
.end method

.method public final s(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 435
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJz:Ldtg;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ldtg;->a(Ljava/lang/CharSequence;Landroid/text/Editable;)V

    .line 436
    return-void
.end method

.method public final t(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->bJz:Ldtg;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ldtg;->b(Ljava/lang/CharSequence;Landroid/text/Editable;)V

    .line 444
    return-void
.end method
