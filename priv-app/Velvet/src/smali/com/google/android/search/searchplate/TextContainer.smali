.class public Lcom/google/android/search/searchplate/TextContainer;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private Oq:I

.field private bJL:Ldsx;

.field private bJM:Landroid/view/View;

.field private bJN:Ldre;

.field private bJO:Z

.field private bJP:Landroid/animation/Animator$AnimatorListener;

.field private bJQ:Landroid/widget/RelativeLayout$LayoutParams;

.field private bJR:Landroid/widget/RelativeLayout$LayoutParams;

.field private bJS:Landroid/widget/RelativeLayout$LayoutParams;

.field private bJT:Landroid/widget/RelativeLayout$LayoutParams;

.field private bJU:I

.field private mDisplayText:Landroid/widget/TextView;

.field public mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    return-void
.end method

.method private static a(Landroid/view/View;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 194
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 197
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 204
    :goto_0
    return-void

    .line 200
    :cond_0
    invoke-virtual {p0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 202
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Landroid/view/View;ZJ)V
    .locals 4

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 178
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 181
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 190
    :goto_0
    return-void

    .line 184
    :cond_0
    if-eqz p1, :cond_1

    .line 185
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 186
    invoke-virtual {p0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 188
    :cond_1
    invoke-static {p0}, Ldtp;->aA(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method private gk(I)I
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method private static o(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 364
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 365
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 366
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 367
    return-void
.end method


# virtual methods
.method public final afL()V
    .locals 2

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mDisplayText:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 229
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mDisplayText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 230
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJL:Ldsx;

    invoke-interface {v0}, Ldsx;->reset()V

    .line 231
    return-void
.end method

.method public final afM()Z
    .locals 1

    .prologue
    .line 245
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJO:Z

    return v0
.end method

.method public afN()Ldre;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 370
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJN:Ldre;

    if-nez v0, :cond_0

    .line 371
    new-instance v0, Ldrg;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Ldrg;-><init>(Landroid/content/Context;)V

    .line 372
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 374
    invoke-virtual {p0, v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 375
    iput-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJN:Ldre;

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJN:Ldre;

    return-object v0
.end method

.method public final afO()V
    .locals 2

    .prologue
    .line 381
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJO:Z

    .line 382
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setAlpha(F)V

    .line 383
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->afN()Ldre;

    move-result-object v0

    invoke-interface {v0}, Ldre;->removeAllViews()V

    .line 384
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJP:Landroid/animation/Animator$AnimatorListener;

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJP:Landroid/animation/Animator$AnimatorListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 387
    :cond_0
    return-void
.end method

.method afP()Landroid/text/Layout;
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getLayout()Landroid/text/Layout;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/animation/Animator$AnimatorListener;)V
    .locals 0
    .param p1    # Landroid/animation/Animator$AnimatorListener;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 399
    iput-object p1, p0, Lcom/google/android/search/searchplate/TextContainer;->bJP:Landroid/animation/Animator$AnimatorListener;

    .line 400
    return-void
.end method

.method public final b(Ldto;)V
    .locals 6

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 324
    invoke-virtual {p1}, Ldto;->afW()Ljava/lang/String;

    move-result-object v1

    .line 325
    invoke-virtual {p1}, Ldto;->afX()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->afP()Landroid/text/Layout;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 329
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJO:Z

    if-eqz v0, :cond_0

    const-string v0, "TextContainer"

    const-string v1, "animateQuery was called while still animating."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/SimpleSearchText;->b(Ldto;)V

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->afO()V

    .line 333
    :goto_0
    return-void

    .line 329
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJO:Z

    new-instance v5, Ldsy;

    invoke-direct {v5, p0, p1}, Ldsy;-><init>(Lcom/google/android/search/searchplate/TextContainer;Ldto;)V

    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getTotalPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->afN()Ldre;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v2}, Lcom/google/android/search/searchplate/SimpleSearchText;->getLayout()Landroid/text/Layout;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-interface/range {v0 .. v5}, Ldre;->a(Ljava/lang/String;Landroid/text/Layout;Landroid/text/TextPaint;ILdrf;)V

    goto :goto_0

    .line 331
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/SimpleSearchText;->b(Ldto;)V

    goto :goto_0
.end method

.method public final ea(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 143
    if-eqz p1, :cond_0

    .line 144
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 148
    :goto_0
    return-void

    .line 146
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    goto :goto_0
.end method

.method public final n(Ljava/lang/CharSequence;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 209
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mDisplayText:Landroid/widget/TextView;

    invoke-static {v0, v4}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;Z)V

    .line 210
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJL:Ldsx;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ldsx;->gb(Ljava/lang/String;)V

    .line 211
    iget v0, p0, Lcom/google/android/search/searchplate/TextContainer;->Oq:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 212
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJL:Ldsx;

    invoke-interface {v0}, Ldsx;->reset()V

    .line 213
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    const-wide/16 v2, 0x32

    invoke-static {v0, v4, v2, v3}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;ZJ)V

    .line 215
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v1, 0x1

    const/4 v9, -0x2

    const/4 v8, -0x1

    const/4 v3, 0x0

    .line 82
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 83
    const v0, 0x7f1103c1

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/TextContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/searchplate/SimpleSearchText;

    iput-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    .line 84
    const v0, 0x7f1103c3

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/TextContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mDisplayText:Landroid/widget/TextView;

    .line 85
    const v0, 0x7f1103c4

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/TextContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJM:Landroid/view/View;

    .line 86
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJM:Landroid/view/View;

    check-cast v0, Ldsx;

    iput-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJL:Ldsx;

    .line 87
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v0, v2, :cond_0

    move v5, v1

    .line 88
    :goto_0
    if-eqz v5, :cond_1

    const/16 v0, 0x14

    move v4, v0

    .line 90
    :goto_1
    if-eqz v5, :cond_2

    const/16 v0, 0x10

    move v2, v0

    .line 91
    :goto_2
    if-eqz v5, :cond_3

    const/16 v0, 0x11

    .line 93
    :goto_3
    const v5, 0x7f0d000f

    invoke-direct {p0, v5}, Lcom/google/android/search/searchplate/TextContainer;->gk(I)I

    move-result v5

    .line 94
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v6, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v6, p0, Lcom/google/android/search/searchplate/TextContainer;->bJQ:Landroid/widget/RelativeLayout$LayoutParams;

    .line 98
    iget-object v6, p0, Lcom/google/android/search/searchplate/TextContainer;->bJQ:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v6, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 99
    iget-object v6, p0, Lcom/google/android/search/searchplate/TextContainer;->bJQ:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v7, 0xa

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 100
    iget-object v6, p0, Lcom/google/android/search/searchplate/TextContainer;->bJQ:Landroid/widget/RelativeLayout$LayoutParams;

    const v7, 0x7f1103c9

    invoke-virtual {v6, v2, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 101
    iget-object v6, p0, Lcom/google/android/search/searchplate/TextContainer;->bJQ:Landroid/widget/RelativeLayout$LayoutParams;

    const v7, 0x7f0d0010

    invoke-direct {p0, v7}, Lcom/google/android/search/searchplate/TextContainer;->gk(I)I

    move-result v7

    invoke-static {v6, v5, v7, v5, v3}, Ldtp;->a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)V

    .line 104
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v6, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v6, p0, Lcom/google/android/search/searchplate/TextContainer;->bJS:Landroid/widget/RelativeLayout$LayoutParams;

    .line 106
    iget-object v6, p0, Lcom/google/android/search/searchplate/TextContainer;->bJS:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v6, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 107
    iget-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->bJS:Landroid/widget/RelativeLayout$LayoutParams;

    const v6, 0x7f110376

    invoke-virtual {v4, v10, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 108
    iget-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->bJS:Landroid/widget/RelativeLayout$LayoutParams;

    const v6, 0x7f0d001d

    invoke-direct {p0, v6}, Lcom/google/android/search/searchplate/TextContainer;->gk(I)I

    move-result v6

    add-int/2addr v6, v5

    invoke-static {v4, v5, v3, v5, v6}, Ldtp;->a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)V

    .line 111
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->bJR:Landroid/widget/RelativeLayout$LayoutParams;

    .line 113
    iget-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->bJR:Landroid/widget/RelativeLayout$LayoutParams;

    const v5, 0x7f1103bf

    invoke-virtual {v4, v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 114
    iget-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->bJR:Landroid/widget/RelativeLayout$LayoutParams;

    const v5, 0x7f1103bc

    invoke-virtual {v4, v2, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 115
    iget-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->bJR:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v5, 0xf

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 116
    iget-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->bJR:Landroid/widget/RelativeLayout$LayoutParams;

    const v5, 0x7f0d0011

    invoke-direct {p0, v5}, Lcom/google/android/search/searchplate/TextContainer;->gk(I)I

    move-result v5

    const v6, 0x7f0d0012

    invoke-direct {p0, v6}, Lcom/google/android/search/searchplate/TextContainer;->gk(I)I

    move-result v6

    invoke-static {v4, v5, v3, v6, v3}, Ldtp;->a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)V

    .line 120
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d000d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-direct {v4, v8, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->bJT:Landroid/widget/RelativeLayout$LayoutParams;

    .line 123
    iget-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->bJT:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v5, 0xf

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 124
    iget-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->bJT:Landroid/widget/RelativeLayout$LayoutParams;

    const v5, 0x7f1103bc

    invoke-virtual {v4, v2, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 125
    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->bJT:Landroid/widget/RelativeLayout$LayoutParams;

    const v4, 0x7f1103bf

    invoke-virtual {v2, v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 126
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJT:Landroid/widget/RelativeLayout$LayoutParams;

    const v2, 0x7f0d0011

    invoke-direct {p0, v2}, Lcom/google/android/search/searchplate/TextContainer;->gk(I)I

    move-result v2

    const v4, 0x7f0d0012

    invoke-direct {p0, v4}, Lcom/google/android/search/searchplate/TextContainer;->gk(I)I

    move-result v4

    invoke-static {v0, v2, v3, v4, v3}, Ldtp;->a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)V

    .line 130
    const v0, 0x7f0d0013

    invoke-direct {p0, v0}, Lcom/google/android/search/searchplate/TextContainer;->gk(I)I

    move-result v0

    const v2, 0x7f0d0014

    invoke-direct {p0, v2}, Lcom/google/android/search/searchplate/TextContainer;->gk(I)I

    move-result v2

    add-int/2addr v0, v2

    const v2, 0x7f0d000f

    invoke-direct {p0, v2}, Lcom/google/android/search/searchplate/TextContainer;->gk(I)I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJU:I

    .line 134
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 135
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 136
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 137
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 138
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 139
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/animation/LayoutTransition;->setAnimateParentHierarchy(Z)V

    .line 140
    return-void

    :cond_0
    move v5, v3

    .line 87
    goto/16 :goto_0

    .line 88
    :cond_1
    const/16 v0, 0x9

    move v4, v0

    goto/16 :goto_1

    :cond_2
    move v2, v3

    .line 90
    goto/16 :goto_2

    :cond_3
    move v0, v1

    .line 91
    goto/16 :goto_3
.end method

.method public onMeasure(II)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 152
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 153
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v2, -0x2

    if-ne v0, v2, :cond_1

    .line 155
    iget v0, p0, Lcom/google/android/search/searchplate/TextContainer;->Oq:I

    if-nez v0, :cond_3

    .line 159
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 160
    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v2}, Lcom/google/android/search/searchplate/SimpleSearchText;->getMeasuredHeight()I

    move-result v2

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v2, v3

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v2

    :goto_0
    move v2, v0

    .line 164
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 165
    invoke-virtual {p0, v1}, Lcom/google/android/search/searchplate/TextContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 166
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    .line 167
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 168
    iget-object v3, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->getMeasuredHeight()I

    move-result v3

    iget v4, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v3, v4

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 164
    :goto_2
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_1

    .line 172
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0, v0, v2}, Lcom/google/android/search/searchplate/TextContainer;->setMeasuredDimension(II)V

    .line 174
    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final x(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 218
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mDisplayText:Landroid/widget/TextView;

    invoke-static {v0, v2}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;Z)V

    .line 221
    :cond_1
    iget v0, p0, Lcom/google/android/search/searchplate/TextContainer;->Oq:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_2

    .line 222
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-static {v0, v2}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;Z)V

    .line 224
    :cond_2
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJL:Ldsx;

    invoke-interface {v0, p1, p2}, Ldsx;->x(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    return-void
.end method

.method public final z(IZ)V
    .locals 9

    .prologue
    const v8, 0x800013

    const/16 v7, 0x11

    const-wide/16 v0, 0x32

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 250
    iput p1, p0, Lcom/google/android/search/searchplate/TextContainer;->Oq:I

    .line 252
    const-wide/16 v2, 0x12c

    .line 253
    packed-switch p1, :pswitch_data_0

    .line 320
    :goto_0
    return-void

    .line 256
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->mDisplayText:Landroid/widget/TextView;

    invoke-static {v2, v5, v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;ZJ)V

    .line 257
    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->bJL:Ldsx;

    invoke-interface {v2}, Ldsx;->reset()V

    .line 258
    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->bJL:Ldsx;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0d0017

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-interface {v2, v5, v3}, Ldsx;->setTextSize(IF)V

    .line 260
    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->bJL:Ldsx;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0b001e

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-interface {v2, v3}, Ldsx;->setTextColor(I)V

    .line 262
    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->bJL:Ldsx;

    const v3, 0x800033

    invoke-interface {v2, v3}, Ldsx;->setGravity(I)V

    .line 263
    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->bJM:Landroid/view/View;

    invoke-static {v2, v5, v5, v5, v5}, Ldtp;->a(Landroid/view/View;IIII)V

    .line 264
    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->bJM:Landroid/view/View;

    invoke-static {v2, v4, v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;ZJ)V

    .line 265
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-static {v0, v4}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;Z)V

    .line 266
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJQ:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/TextContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 267
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJM:Landroid/view/View;

    const/16 v1, 0x30

    invoke-static {v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->o(Landroid/view/View;I)V

    goto :goto_0

    .line 271
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->mDisplayText:Landroid/widget/TextView;

    invoke-static {v2, v5, v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;ZJ)V

    .line 272
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJM:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;Z)V

    .line 273
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-static {v0, v4}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;Z)V

    .line 274
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJS:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/TextContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 277
    :pswitch_2
    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->mDisplayText:Landroid/widget/TextView;

    invoke-static {v2, v4}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;Z)V

    .line 278
    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->bJL:Ldsx;

    invoke-interface {v2}, Ldsx;->reset()V

    .line 279
    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->bJL:Ldsx;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0d0016

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-interface {v2, v5, v3}, Ldsx;->setTextSize(IF)V

    .line 281
    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->bJL:Ldsx;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0b001d

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-interface {v2, v3}, Ldsx;->setTextColor(I)V

    .line 283
    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->bJM:Landroid/view/View;

    invoke-static {v2, v4, v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;ZJ)V

    .line 284
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJM:Landroid/view/View;

    invoke-static {v0, v5, v5, v5, v5}, Ldtp;->a(Landroid/view/View;IIII)V

    .line 285
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-static {v0, v4}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;Z)V

    .line 286
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJT:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/TextContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 287
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJM:Landroid/view/View;

    invoke-static {v0, v7}, Lcom/google/android/search/searchplate/TextContainer;->o(Landroid/view/View;I)V

    .line 288
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJL:Ldsx;

    invoke-interface {v0, v8}, Ldsx;->setGravity(I)V

    goto/16 :goto_0

    .line 291
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mDisplayText:Landroid/widget/TextView;

    invoke-static {v0, v4}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;Z)V

    .line 292
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJM:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;Z)V

    .line 293
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-static {v0, v4}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;Z)V

    .line 294
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJR:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/TextContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    :pswitch_4
    move-wide v2, v0

    .line 306
    :pswitch_5
    invoke-static {p0}, Ldtp;->ay(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/search/searchplate/TextContainer;->bJQ:Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    sub-int/2addr v0, v1

    if-nez v0, :cond_0

    move v0, v4

    :goto_1
    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJU:I

    neg-int v0, v0

    .line 307
    :goto_2
    iget-object v1, p0, Lcom/google/android/search/searchplate/TextContainer;->bJR:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0, v1}, Lcom/google/android/search/searchplate/TextContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 308
    iget-object v1, p0, Lcom/google/android/search/searchplate/TextContainer;->bJM:Landroid/view/View;

    invoke-static {v1, v5, v5, v0, v5}, Ldtp;->a(Landroid/view/View;IIII)V

    .line 309
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJL:Ldsx;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f0d0016

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-interface {v0, v5, v1}, Ldsx;->setTextSize(IF)V

    .line 311
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJL:Ldsx;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f0b001e

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-interface {v0, v1}, Ldsx;->setTextColor(I)V

    .line 313
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJM:Landroid/view/View;

    invoke-static {v0, v7}, Lcom/google/android/search/searchplate/TextContainer;->o(Landroid/view/View;I)V

    .line 314
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJL:Ldsx;

    invoke-interface {v0, v8}, Ldsx;->setGravity(I)V

    .line 315
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mDisplayText:Landroid/widget/TextView;

    invoke-static {v0, v4}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;Z)V

    .line 316
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->bJM:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;Z)V

    .line 317
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-static {v0, p2, v2, v3}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;ZJ)V

    goto/16 :goto_0

    :cond_0
    move v0, v5

    .line 306
    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getRight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/search/searchplate/TextContainer;->bJQ:Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    if-ne v1, v0, :cond_2

    move v0, v4

    goto :goto_1

    :cond_2
    move v0, v5

    goto :goto_1

    :cond_3
    move v0, v5

    goto :goto_2

    .line 253
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_5
        :pswitch_5
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
