.class public Lcom/google/android/search/shared/actions/RemindersConfigFlags;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Leti;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field protected bMQ:Z

.field protected bMR:Z

.field private bMS:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lduy;

    invoke-direct {v0}, Lduy;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    if-eqz v3, :cond_1

    move v3, v1

    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v4

    if-eqz v4, :cond_2

    :goto_2
    invoke-direct {p0, v0, v3, v1}, Lcom/google/android/search/shared/actions/RemindersConfigFlags;-><init>(ZZZ)V

    .line 44
    return-void

    :cond_0
    move v0, v2

    .line 41
    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public constructor <init>(ZZZ)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-boolean p1, p0, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->bMQ:Z

    .line 36
    iput-boolean p2, p0, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->bMR:Z

    .line 37
    iput-boolean p3, p0, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->bMS:Z

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Letj;)V
    .locals 2

    .prologue
    .line 80
    const-string v0, "ReminderConfigFlags"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 81
    const-string v0, "mRecurrenceEnabled"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->bMQ:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 82
    const-string v0, "mOccasionallyEnabled"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->bMR:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 83
    const-string v0, "mSearchHintEnabled"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->bMS:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 84
    return-void
.end method

.method public final aih()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->bMQ:Z

    return v0
.end method

.method public final aii()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->bMR:Z

    return v0
.end method

.method public final aij()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->bMS:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ReminderConfigFlags(mRecurrenceEnabled="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->bMQ:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mOccasionallyEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->bMR:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSearchHintEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->bMS:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->bMQ:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 49
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->bMR:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 50
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->bMS:Z

    if-eqz v0, :cond_2

    :goto_2
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 51
    return-void

    :cond_0
    move v0, v2

    .line 48
    goto :goto_0

    :cond_1
    move v0, v2

    .line 49
    goto :goto_1

    :cond_2
    move v1, v2

    .line 50
    goto :goto_2
.end method
