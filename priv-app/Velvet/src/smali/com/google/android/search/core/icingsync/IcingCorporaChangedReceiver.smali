.class public final Lcom/google/android/search/core/icingsync/IcingCorporaChangedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 28
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 32
    const-string v0, "MAYBE"

    .line 33
    const-string v2, "MAYBE"

    .line 34
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 37
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    .line 39
    const-string v5, "com.google.android.gms"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 41
    invoke-static {}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->Ta()V

    .line 44
    :cond_1
    const-string v5, "android.intent.extra.REPLACING"

    invoke-virtual {p2, v5, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 48
    const-string v6, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    if-nez v5, :cond_3

    :cond_2
    const/4 v3, 0x1

    .line 49
    :cond_3
    if-eqz v3, :cond_4

    move-object v0, v1

    .line 53
    :cond_4
    const-string v3, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    const-string v3, "com.android.providers.contacts"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 56
    const-string v1, "FORCE_ALL"

    :goto_0
    move-object v2, v1

    .line 90
    :cond_5
    :goto_1
    const-string v1, "MAYBE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "MAYBE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 98
    :goto_2
    return-void

    .line 58
    :cond_6
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 60
    const-string v0, "FORCE_ALL"

    .line 62
    const-string v2, "FORCE_ALL"

    goto :goto_1

    .line 63
    :cond_7
    const-string v1, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 64
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    .line 66
    const-string v3, "com.android.providers.contacts"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 67
    const-string v2, "FORCE_ALL"

    .line 68
    sget v1, Lesp;->SDK_INT:I

    const/16 v3, 0x12

    if-ge v1, v3, :cond_5

    .line 72
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DD()Lcjs;

    move-result-object v1

    .line 73
    invoke-static {p1, v1}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->b(Landroid/content/Context;Lcjs;)Z

    goto :goto_1

    .line 75
    :cond_8
    const-string v3, "com.google.android.gms"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 77
    invoke-static {}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->Ta()V

    goto :goto_1

    .line 79
    :cond_9
    const-string v0, "android.provider.Contacts.DATABASE_CREATED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 80
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DD()Lcjs;

    move-result-object v0

    .line 81
    invoke-static {p1, v0}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->b(Landroid/content/Context;Lcjs;)Z

    goto :goto_2

    .line 84
    :cond_a
    const-string v0, "IcingCorporaChangedReceiver"

    const-string v1, "BroadcastReceiver received unrecognized intent"

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_2

    .line 96
    :cond_b
    invoke-static {p1, v0, v2}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_2

    :cond_c
    move-object v1, v2

    goto/16 :goto_0
.end method
