.class public Lcom/google/android/search/shared/actions/SetAlarmAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bMW:Ljava/lang/String;

.field private final bMX:Z

.field private final bMY:I

.field private final bMZ:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    new-instance v0, Ldvc;

    invoke-direct {v0}, Ldvc;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/SetAlarmAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMW:Ljava/lang/String;

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMX:Z

    .line 35
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMX:Z

    if-eqz v0, :cond_1

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMY:I

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMZ:I

    .line 42
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 34
    goto :goto_0

    .line 39
    :cond_1
    iput v1, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMY:I

    .line 40
    iput v1, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMZ:I

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMW:Ljava/lang/String;

    .line 26
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMX:Z

    .line 27
    iput v0, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMY:I

    .line 28
    iput v0, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMZ:I

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMW:Ljava/lang/String;

    .line 19
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMX:Z

    .line 20
    iput p2, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMY:I

    .line 21
    iput p3, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMZ:I

    .line 22
    return-void
.end method


# virtual methods
.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 72
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/SetAlarmAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final agb()Ldtw;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Ldtw;->bLo:Ldtw;

    return-object v0
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMX:Z

    return v0
.end method

.method public final getHour()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMY:I

    return v0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMW:Ljava/lang/String;

    return-object v0
.end method

.method public final getMinute()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMZ:I

    return v0
.end method

.method public final sZ()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMX:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 78
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMW:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 79
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMX:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 80
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMX:Z

    if-eqz v0, :cond_0

    .line 81
    iget v0, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMY:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 82
    iget v0, p0, Lcom/google/android/search/shared/actions/SetAlarmAction;->bMZ:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 84
    :cond_0
    return-void

    .line 79
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
