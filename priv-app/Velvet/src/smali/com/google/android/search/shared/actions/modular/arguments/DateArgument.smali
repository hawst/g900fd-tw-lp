.class public Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;
.super Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bOV:[Ljql;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 186
    new-instance v0, Ldwv;

    invoke-direct {v0}, Ldwv;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;)V

    .line 42
    iget-object v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->bOV:[Ljql;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->bOV:[Ljql;

    .line 43
    return-void
.end method

.method public constructor <init>(Ljqg;)V
    .locals 1

    .prologue
    .line 32
    sget-object v0, Ljqk;->eyU:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqk;

    iget-object v0, v0, Ljqk;->eyV:Ljql;

    if-nez v0, :cond_0

    sget-object v0, Ljqk;->eyU:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqk;

    iget-object v0, v0, Ljqk;->euo:Ljkg;

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;-><init>(Ljqg;Ljava/lang/Object;)V

    .line 37
    sget-object v0, Ljqk;->eyU:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqk;

    iget-object v0, v0, Ljqk;->eyW:[Ljql;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->bOV:[Ljql;

    .line 38
    return-void

    .line 32
    :cond_0
    sget-object v0, Ljqk;->eyU:Ljsm;

    invoke-virtual {p1, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljqk;

    iget-object v0, v0, Ljqk;->eyV:Ljql;

    iget-object v0, v0, Ljql;->euo:Ljkg;

    goto :goto_0
.end method


# virtual methods
.method public final Pd()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-super {p0}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljkg;

    .line 54
    invoke-virtual {v0}, Ljkg;->bok()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljkg;->boj()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljkg;->boi()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 56
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 54
    goto :goto_0

    :cond_1
    move v0, v1

    .line 56
    goto :goto_0
.end method

.method public final a(Ldwu;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 119
    invoke-interface {p1, p0}, Ldwu;->a(Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final ajC()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic ajE()Lcom/google/android/search/shared/actions/modular/arguments/Argument;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->ajY()Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    move-result-object v0

    return-object v0
.end method

.method public final ajH()I
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 136
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->bOD:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljng;

    sget-object v4, Ljni;->euQ:Ljsm;

    invoke-virtual {v0, v4}, Ljng;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    sget-object v4, Ljni;->euQ:Ljsm;

    invoke-virtual {v0, v4}, Ljng;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljni;

    invoke-virtual {v0}, Ljni;->bqG()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->getCalendar()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Lesi;->aZ(J)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move v0, v1

    .line 139
    :goto_1
    return v0

    :cond_2
    move v0, v1

    .line 136
    goto :goto_0

    :cond_3
    move v0, v2

    .line 139
    goto :goto_1
.end method

.method public final ajP()Ljqg;
    .locals 4

    .prologue
    .line 171
    invoke-super {p0}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->ajP()Ljqg;

    move-result-object v1

    .line 172
    new-instance v2, Ljqk;

    invoke-direct {v2}, Ljqk;-><init>()V

    .line 173
    sget-object v0, Ljqk;->eyU:Ljsm;

    invoke-virtual {v1, v0, v2}, Ljqg;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 174
    new-instance v0, Ljql;

    invoke-direct {v0}, Ljql;-><init>()V

    iput-object v0, v2, Ljqk;->eyV:Ljql;

    .line 175
    iget-object v3, v2, Ljqk;->eyV:Ljql;

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljkg;

    iput-object v0, v3, Ljql;->euo:Ljkg;

    .line 176
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljkg;

    iput-object v0, v2, Ljqk;->euo:Ljkg;

    .line 177
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->bOV:[Ljql;

    iput-object v0, v2, Ljqk;->eyW:[Ljql;

    .line 178
    return-object v1
.end method

.method public final ajX()[Ljql;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->bOV:[Ljql;

    return-object v0
.end method

.method public final ajY()Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;
    .locals 1

    .prologue
    .line 166
    new-instance v0, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    invoke-direct {v0, p0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;)V

    return-object v0
.end method

.method public final b(Ljqu;Landroid/content/res/Resources;)Ldws;
    .locals 1

    .prologue
    .line 125
    sget-object v0, Ldws;->bOO:Ldws;

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->ajY()Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    move-result-object v0

    return-object v0
.end method

.method public final f(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z
    .locals 1
    .param p1    # Lcom/google/android/search/shared/actions/modular/arguments/Argument;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 161
    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->h(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z

    move-result v0

    return v0
.end method

.method public final gI(I)Z
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    return v0
.end method

.method public final getCalendar()Ljava/util/Calendar;
    .locals 4

    .prologue
    .line 99
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 100
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljkg;

    .line 104
    invoke-virtual {v0}, Ljkg;->getYear()I

    move-result v2

    invoke-virtual {v0}, Ljkg;->getMonth()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0}, Ljkg;->getDay()I

    move-result v0

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/Calendar;->set(III)V

    .line 106
    :cond_0
    return-object v1
.end method

.method public final getDayOfMonth()I
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->Pd()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 65
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljkg;

    invoke-virtual {v0}, Ljkg;->getDay()I

    move-result v0

    return v0
.end method

.method public final getMonth()I
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->Pd()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 74
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljkg;

    invoke-virtual {v0}, Ljkg;->getMonth()I

    move-result v0

    return v0
.end method

.method public final getYear()I
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->Pd()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 83
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljkg;

    invoke-virtual {v0}, Ljkg;->getYear()I

    move-result v0

    return v0
.end method

.method public final r(III)V
    .locals 1

    .prologue
    .line 114
    new-instance v0, Ljkg;

    invoke-direct {v0}, Ljkg;-><init>()V

    invoke-virtual {v0, p1}, Ljkg;->qs(I)Ljkg;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljkg;->qr(I)Ljkg;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljkg;->qq(I)Ljkg;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->setValue(Ljava/lang/Object;)V

    .line 115
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->ajP()Ljqg;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 184
    return-void
.end method
