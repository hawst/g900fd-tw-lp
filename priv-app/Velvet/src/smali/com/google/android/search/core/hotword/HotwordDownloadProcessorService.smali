.class public Lcom/google/android/search/core/hotword/HotwordDownloadProcessorService;
.super Landroid/app/IntentService;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/google/android/search/core/hotword/HotwordDownloadProcessorService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 36
    return-void
.end method


# virtual methods
.method protected BK()Lcke;
    .locals 1

    .prologue
    .line 120
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    return-object v0
.end method

.method protected SC()Lcfo;
    .locals 1

    .prologue
    .line 114
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    return-object v0
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 13

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 40
    const-string v0, "extra_download_id"

    invoke-virtual {p1, v0, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 42
    invoke-virtual {p0}, Lcom/google/android/search/core/hotword/HotwordDownloadProcessorService;->BK()Lcke;

    move-result-object v3

    .line 45
    invoke-interface {v3}, Lcke;->OD()Lcsj;

    move-result-object v6

    .line 46
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcsj;->SF()J

    move-result-wide v8

    cmp-long v0, v8, v4

    if-eqz v0, :cond_1

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    const-string v0, "download"

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/hotword/HotwordDownloadProcessorService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    .line 52
    new-instance v7, Landroid/app/DownloadManager$Query;

    invoke-direct {v7}, Landroid/app/DownloadManager$Query;-><init>()V

    new-array v8, v1, [J

    aput-wide v4, v8, v2

    invoke-virtual {v7, v8}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    move-result-object v7

    const/16 v8, 0x18

    invoke-virtual {v7, v8}, Landroid/app/DownloadManager$Query;->setFilterByStatus(I)Landroid/app/DownloadManager$Query;

    move-result-object v7

    .line 56
    invoke-virtual {v0, v7}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v7

    .line 58
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 59
    const-string v8, "status"

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 63
    const/16 v9, 0x10

    if-ne v8, v9, :cond_3

    .line 65
    new-array v1, v1, [J

    aput-wide v4, v1, v2

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->remove([J)I

    .line 66
    const/16 v0, 0x122

    invoke-static {v0}, Lege;->ht(I)V

    .line 68
    invoke-interface {v3}, Lcke;->OE()V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/search/core/hotword/HotwordDownloadProcessorService;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DO()Lgpp;

    move-result-object v0

    const-string v1, "update_hotword_models"

    invoke-interface {v0, v1, v10, v11}, Lgpp;->r(Ljava/lang/String;J)V

    .line 108
    :cond_2
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 71
    :cond_3
    const/16 v9, 0x8

    if-ne v8, v9, :cond_2

    .line 73
    const/16 v8, 0x120

    invoke-static {v8}, Lege;->ht(I)V

    .line 75
    const-string v8, "local_filename"

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 78
    :try_start_0
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0}, Lcom/google/android/search/core/hotword/HotwordDownloadProcessorService;->getFilesDir()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    .line 80
    invoke-virtual {v6}, Lcsj;->getLocale()Ljava/lang/String;

    move-result-object v10

    .line 81
    new-instance v11, Ljava/io/File;

    invoke-static {v8, v10}, Lcse;->ac(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 83
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_4

    .line 84
    invoke-virtual {v11}, Ljava/io/File;->mkdir()Z

    .line 88
    :cond_4
    new-instance v11, Ljava/io/File;

    invoke-static {v8, v10}, Lcse;->ab(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v11, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v11}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    :goto_2
    const-string v2, "Source %s and destination %s must be different"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v9, v8, v12

    const/4 v12, 0x1

    aput-object v11, v8, v12

    invoke-static {v1, v2, v8}, Lifv;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v9}, Lisr;->u(Ljava/io/File;)Lisu;

    move-result-object v1

    invoke-static {v11}, Lisr;->v(Ljava/io/File;)Lisv;

    move-result-object v2

    invoke-static {v1, v2}, Liso;->a(Lisu;Lisv;)J

    .line 92
    invoke-virtual {v6}, Lcsj;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v10, v1}, Lcke;->L(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    invoke-interface {v3}, Lcke;->OE()V

    .line 94
    const/4 v1, 0x1

    new-array v1, v1, [J

    const/4 v2, 0x0

    aput-wide v4, v1, v2

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->remove([J)I

    .line 98
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/search/core/hotword/HotwordDownloadProcessorService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.search.core.action.NEW_HOTWORD_MODEL_AVAILABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 102
    invoke-virtual {p0}, Lcom/google/android/search/core/hotword/HotwordDownloadProcessorService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 103
    :catch_0
    move-exception v0

    .line 104
    const-string v1, "HotwordDownloadService"

    const-string v2, "Unable to move file to internal storage"

    invoke-static {v1, v2, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :cond_5
    move v1, v2

    .line 88
    goto :goto_2
.end method
