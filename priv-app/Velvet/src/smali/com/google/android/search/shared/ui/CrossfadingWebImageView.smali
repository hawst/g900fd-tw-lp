.class public Lcom/google/android/search/shared/ui/CrossfadingWebImageView;
.super Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;
.source "PG"


# instance fields
.field private final bVe:I

.field private final bVf:Z

.field public bVg:Lecv;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    sget-object v0, Lbwe;->aLQ:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 38
    const/16 v1, 0x12c

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->bVe:I

    .line 41
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->bVf:Z

    .line 44
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 45
    return-void
.end method

.method public static synthetic a(Lcom/google/android/search/shared/ui/CrossfadingWebImageView;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 22
    invoke-super {p0, p1}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 58
    iget-object v0, p0, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->bVg:Lecv;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->bVg:Lecv;

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->bVg:Lecv;

    .line 65
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/search/shared/ui/WebImageView;->bVC:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->bVf:Z

    if-nez v0, :cond_1

    .line 66
    invoke-super {p0, p1}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 117
    :goto_0
    return-void

    .line 72
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 74
    :goto_1
    if-eqz v1, :cond_7

    .line 75
    instance-of v0, v1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 76
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    .line 77
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/animation/LayoutTransition;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 85
    :goto_2
    if-eqz v0, :cond_3

    .line 86
    invoke-super {p0, p1}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 82
    :cond_2
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_1

    .line 91
    :cond_3
    if-nez p1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_4

    .line 92
    invoke-super {p0, p1}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 97
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 98
    if-nez v0, :cond_6

    .line 99
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 105
    :cond_5
    :goto_3
    new-instance v1, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/graphics/drawable/Drawable;

    aput-object v0, v4, v3

    aput-object p1, v4, v2

    invoke-direct {v1, v4}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 107
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 110
    invoke-super {p0, v1}, Lcom/google/android/search/shared/ui/RoundedCornerWebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 111
    iget v0, p0, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->bVe:I

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 115
    new-instance v0, Lecv;

    invoke-direct {v0, p0, p1}, Lecv;-><init>(Lcom/google/android/search/shared/ui/CrossfadingWebImageView;Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->bVg:Lecv;

    .line 116
    iget-object v0, p0, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->bVg:Lecv;

    iget v1, p0, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->bVe:I

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 100
    :cond_6
    if-nez p1, :cond_5

    .line 101
    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {p1, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_3

    :cond_7
    move v0, v3

    goto :goto_2
.end method
