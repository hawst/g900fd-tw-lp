.class public abstract Lcom/google/android/search/core/preferences/SettingsFragmentBase;
.super Landroid/preference/PreferenceFragment;
.source "PG"


# instance fields
.field private bmF:Lcxt;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final TM()V
    .locals 2

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->Tj()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->addPreferencesFromResource(I)V

    .line 67
    iget-object v0, p0, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->bmF:Lcxt;

    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-interface {v0, v1}, Lcxt;->a(Landroid/preference/Preference;)V

    .line 68
    return-void
.end method

.method protected abstract Tj()I
.end method

.method protected abstract a(Lgql;)Lcxt;
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 37
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->a(Lgql;)Lcxt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->bmF:Lcxt;

    .line 39
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-static {v0}, Lchr;->a(Landroid/preference/PreferenceManager;)V

    .line 41
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->Tj()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->addPreferencesFromResource(I)V

    .line 43
    iget-object v0, p0, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->bmF:Lcxt;

    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-interface {v0, v1}, Lcxt;->a(Landroid/preference/PreferenceScreen;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->bmF:Lcxt;

    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-interface {v0, v1}, Lcxt;->a(Landroid/preference/Preference;)V

    .line 47
    iget-object v0, p0, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->bmF:Lcxt;

    invoke-interface {v0, p1}, Lcxt;->i(Landroid/os/Bundle;)V

    .line 49
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->setHasOptionsMenu(Z)V

    .line 50
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0

    .prologue
    .line 111
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 112
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->bmF:Lcxt;

    invoke-interface {v0}, Lcxt;->onDestroy()V

    .line 97
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    .line 98
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->bmF:Lcxt;

    invoke-interface {v0}, Lcxt;->onPause()V

    .line 85
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 86
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 79
    iget-object v0, p0, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->bmF:Lcxt;

    invoke-interface {v0}, Lcxt;->onResume()V

    .line 80
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 102
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->bmF:Lcxt;

    invoke-interface {v0, p1}, Lcxt;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 104
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onStart()V

    .line 73
    iget-object v0, p0, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->bmF:Lcxt;

    invoke-interface {v0}, Lcxt;->onStart()V

    .line 74
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->bmF:Lcxt;

    invoke-interface {v0}, Lcxt;->onStop()V

    .line 91
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onStop()V

    .line 92
    return-void
.end method
