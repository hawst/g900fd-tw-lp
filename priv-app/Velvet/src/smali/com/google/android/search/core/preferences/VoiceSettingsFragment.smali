.class public Lcom/google/android/search/core/preferences/VoiceSettingsFragment;
.super Lcom/google/android/search/core/preferences/SettingsFragmentBase;
.source "PG"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected final Tj()I
    .locals 1

    .prologue
    .line 28
    const v0, 0x7f070027

    return v0
.end method

.method protected final a(Lgql;)Lcxt;
    .locals 10

    .prologue
    .line 33
    invoke-virtual {p1}, Lgql;->aJq()Lhhq;

    move-result-object v6

    .line 34
    new-instance v0, Lcyt;

    invoke-virtual {p1}, Lgql;->SC()Lcfo;

    move-result-object v1

    iget-object v2, v6, Lhhq;->mSettings:Lhym;

    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/VoiceSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v6}, Lhhq;->aOV()Lgiw;

    move-result-object v4

    invoke-virtual {v4}, Lgiw;->aGj()Ligi;

    move-result-object v4

    invoke-static {}, Lenu;->auR()V

    iget-object v5, v6, Lhhq;->ble:Lhxr;

    if-nez v5, :cond_0

    new-instance v5, Lhxr;

    iget-object v7, v6, Lhhq;->mSettings:Lhym;

    iget-object v8, v6, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v8}, Lcfo;->DL()Lcrh;

    move-result-object v8

    iget-object v9, v6, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v9}, Lcfo;->DY()Lgno;

    move-result-object v9

    invoke-direct {v5, v7, v8, v9}, Lhxr;-><init>(Lhym;Lglm;Lgno;)V

    iput-object v5, v6, Lhhq;->ble:Lhxr;

    :cond_0
    iget-object v5, v6, Lhhq;->ble:Lhxr;

    iget-object v6, v6, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v6}, Lcfo;->BL()Lchk;

    move-result-object v6

    invoke-static {}, Lckw;->OZ()Lckw;

    move-result-object v7

    iget-object v8, p1, Lgql;->mAsyncServices:Lema;

    invoke-direct/range {v0 .. v8}, Lcyt;-><init>(Lcfo;Lhym;Landroid/app/Activity;Ligi;Lhxr;Lchk;Lckw;Lema;)V

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->onCreate(Landroid/os/Bundle;)V

    .line 48
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 6

    .prologue
    .line 74
    invoke-super {p0, p1, p2}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 77
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/VoiceSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "now_voice_settings"

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v0

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v3

    new-instance v0, Lgpk;

    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/VoiceSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v0, v4}, Lgpk;-><init>(Landroid/content/Context;)V

    const-string v4, "now_voice_settings"

    invoke-virtual {v0, v4}, Lgpk;->nv(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v0

    invoke-virtual {v0}, Lcrh;->Su()Z

    move-result v5

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lgbk;->a(Landroid/view/Menu;Landroid/app/Activity;Ljava/lang/String;Landroid/accounts/Account;Landroid/net/Uri;Z)V

    .line 82
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/VoiceSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 61
    invoke-super {p0}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->onPause()V

    .line 62
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 52
    invoke-super {p0}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->onResume()V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/VoiceSettingsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 55
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 66
    const-string v0, "spoken-language-bcp-47"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/VoiceSettingsFragment;->TM()V

    .line 70
    :cond_0
    return-void
.end method
