.class public abstract Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;
.super Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;
.source "PG"


# instance fields
.field private bOy:Ldyc;


# direct methods
.method protected constructor <init>(Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;Lcom/google/android/search/shared/actions/utils/Disambiguation;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;Ljava/lang/Object;)V

    .line 33
    return-void
.end method

.method protected constructor <init>(Ljqg;Lcom/google/android/search/shared/actions/utils/Disambiguation;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;-><init>(Ljqg;Ljava/lang/Object;)V

    .line 29
    return-void
.end method

.method protected static a([ILjava/util/List;)Ljava/util/List;
    .locals 5
    .param p0    # [I
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 116
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-object p1

    .line 119
    :cond_1
    array-length v0, p0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 120
    array-length v3, p0

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_4

    aget v1, p0, v2

    .line 122
    if-gez v1, :cond_2

    .line 123
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v1, v4

    .line 125
    :cond_2
    if-ltz v1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 130
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_4
    move-object p1, v0

    .line 134
    goto :goto_0
.end method


# virtual methods
.method public ajC()Z
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->isCompleted()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljqu;Landroid/content/res/Resources;)Ldws;
    .locals 5

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->Pd()Z

    move-result v0

    if-nez v0, :cond_1

    .line 72
    sget-object v0, Ldws;->bOO:Ldws;

    .line 110
    :cond_0
    :goto_0
    return-object v0

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    .line 76
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alu()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 77
    iget-object v1, p1, Ljqu;->ezx:[I

    .line 78
    if-nez v2, :cond_2

    .line 79
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_0

    .line 80
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alB()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 81
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    invoke-virtual {p1}, Ljqu;->bsE()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->h(Ljava/lang/Object;I)Ldws;

    move-result-object v0

    goto :goto_0

    .line 83
    :cond_3
    array-length v3, v1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_7

    .line 84
    const/4 v3, 0x0

    aget v1, v1, v3

    .line 85
    if-gez v1, :cond_4

    .line 86
    add-int/2addr v1, v2

    .line 88
    :cond_4
    if-ltz v1, :cond_5

    if-lt v1, v2, :cond_6

    .line 90
    :cond_5
    new-instance v0, Ldws;

    const-string v1, ""

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :cond_6
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alu()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Ljqu;->bsE()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->h(Ljava/lang/Object;I)Ldws;

    move-result-object v0

    goto :goto_0

    .line 96
    :cond_7
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alu()Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->a([ILjava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 97
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 98
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 99
    invoke-virtual {p1}, Ljqu;->bsE()I

    move-result v3

    invoke-virtual {p0, v0, v3}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->h(Ljava/lang/Object;I)Ldws;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Ldws;->Pd()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 105
    invoke-virtual {v0}, Ldws;->getObject()Ljava/lang/Object;

    move-result-object v0

    .line 106
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 107
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 110
    :cond_9
    new-instance v0, Ldws;

    iget-object v2, p1, Ljqu;->ezy:Ljra;

    invoke-static {p2}, Lerr;->h(Landroid/content/res/Resources;)Z

    move-result v3

    invoke-static {v2, v1, v3}, Lerr;->a(Ljra;Ljava/util/List;Z)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final b(Lcom/google/android/search/shared/actions/utils/Disambiguation;)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    .line 40
    if-ne p1, v0, :cond_1

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    if-eqz v0, :cond_2

    .line 45
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->b(Ldyc;)V

    .line 48
    :cond_2
    if-eqz p1, :cond_3

    .line 49
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->bOy:Ldyc;

    invoke-virtual {p1, v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->b(Ldyc;)V

    .line 52
    :cond_3
    invoke-super {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->setValue(Ljava/lang/Object;)V

    .line 54
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->bOy:Ldyc;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 55
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->bOy:Ldyc;

    invoke-interface {v0, p1}, Ldyc;->a(Lcom/google/android/search/shared/actions/utils/Disambiguation;)V

    goto :goto_0
.end method

.method public final b(Ldyc;)V
    .locals 1

    .prologue
    .line 156
    iput-object p1, p0, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->bOy:Ldyc;

    .line 158
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->b(Ldyc;)V

    .line 161
    :cond_0
    return-void
.end method

.method protected f(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z
    .locals 2

    .prologue
    .line 169
    instance-of v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    check-cast p1, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;

    iget-object v1, p1, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    invoke-static {v0, v1}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->a(Lcom/google/android/search/shared/actions/utils/Disambiguation;Lcom/google/android/search/shared/actions/utils/Disambiguation;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract h(Ljava/lang/Object;I)Ldws;
.end method

.method public final synthetic setValue(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->b(Lcom/google/android/search/shared/actions/utils/Disambiguation;)V

    return-void
.end method
