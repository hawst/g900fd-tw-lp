.class public Lcom/google/android/search/shared/contact/Person;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ldzk;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final bRN:Ljava/util/Comparator;


# instance fields
.field private final aYG:Ljava/util/Set;

.field private final bCq:Ljava/util/Locale;

.field private final bNt:Ljava/util/List;

.field private final bNu:Ljava/util/List;

.field private final bNv:Ljava/util/List;

.field private final bRH:Ljava/util/List;

.field private bRI:Z

.field private bRJ:Ljava/lang/String;

.field private bRK:Ljava/lang/String;

.field private final bRL:Ljava/util/Set;

.field private bRM:I

.field private final bRa:J

.field private final bjd:Ljava/lang/String;

.field private final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Ldzf;

    invoke-direct {v0}, Ldzf;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/contact/Person;->bRN:Ljava/util/Comparator;

    .line 955
    new-instance v0, Ldzh;

    invoke-direct {v0}, Ldzh;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/contact/Person;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 108
    move-object v1, p0

    move-wide v2, p1

    move-object v5, p3

    move-object v6, v4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/search/shared/contact/Person;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    .line 109
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    iput-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bCq:Ljava/util/Locale;

    .line 112
    iput-wide p1, p0, Lcom/google/android/search/shared/contact/Person;->bRa:J

    .line 113
    iput-object p3, p0, Lcom/google/android/search/shared/contact/Person;->bjd:Ljava/lang/String;

    .line 114
    iput-object p4, p0, Lcom/google/android/search/shared/contact/Person;->mName:Ljava/lang/String;

    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNt:Ljava/util/List;

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNu:Ljava/util/List;

    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNv:Ljava/util/List;

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRH:Ljava/util/List;

    .line 119
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/shared/contact/Person;->aYG:Ljava/util/Set;

    .line 120
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRL:Ljava/util/Set;

    .line 121
    invoke-virtual {p0, p5}, Lcom/google/android/search/shared/contact/Person;->m(Ljava/util/Collection;)V

    .line 122
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/search/shared/contact/Person;->bRM:I

    .line 124
    return-void
.end method

.method private constructor <init>(Ldzk;)V
    .locals 7
    .param p1    # Ldzk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 103
    invoke-interface {p1}, Ldzk;->getId()J

    move-result-wide v2

    invoke-interface {p1}, Ldzk;->alO()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, Ldzk;->getName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/search/shared/contact/Person;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    .line 104
    return-void
.end method

.method public static H(Ljava/util/List;)Lcom/google/android/search/shared/contact/Person;
    .locals 3
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 642
    new-instance v1, Lcom/google/android/search/shared/contact/Person;

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzk;

    invoke-direct {v1, v0}, Lcom/google/android/search/shared/contact/Person;-><init>(Ldzk;)V

    .line 644
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 645
    invoke-static {v1, v0}, Lcom/google/android/search/shared/contact/Person;->d(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Contact;)V

    goto :goto_0

    .line 647
    :cond_0
    return-object v1
.end method

.method public static I(Ljava/util/List;)Ljava/util/List;
    .locals 10
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 656
    invoke-static {}, Lior;->aYa()Ljava/util/LinkedHashMap;

    move-result-object v2

    .line 657
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 658
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 659
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getId()J

    move-result-wide v6

    .line 661
    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-lez v1, :cond_1

    .line 662
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 663
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/contact/Person;

    .line 673
    :goto_1
    invoke-static {v1, v0}, Lcom/google/android/search/shared/contact/Person;->d(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Contact;)V

    goto :goto_0

    .line 665
    :cond_0
    new-instance v1, Lcom/google/android/search/shared/contact/Person;

    invoke-direct {v1, v0}, Lcom/google/android/search/shared/contact/Person;-><init>(Ldzk;)V

    .line 666
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v2, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 667
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 670
    :cond_1
    new-instance v1, Lcom/google/android/search/shared/contact/Person;

    invoke-direct {v1, v0}, Lcom/google/android/search/shared/contact/Person;-><init>(Ldzk;)V

    .line 671
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 675
    :cond_2
    return-object v3
.end method

.method public static a(Ldyv;Ljava/lang/String;Ljoj;)Lcom/google/android/search/shared/contact/Person;
    .locals 19
    .param p0    # Ldyv;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljoj;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 495
    invoke-virtual/range {p2 .. p2}, Ljoj;->brm()Z

    move-result v11

    .line 503
    invoke-virtual/range {p2 .. p2}, Ljoj;->bob()Ljava/lang/String;

    move-result-object v3

    .line 504
    const-wide/16 v4, 0x0

    .line 505
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 507
    :try_start_0
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v4

    .line 517
    :cond_0
    invoke-virtual/range {p2 .. p2}, Ljoj;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    .line 518
    invoke-virtual/range {p2 .. p2}, Ljoj;->brk()Ljava/lang/String;

    move-result-object v12

    .line 519
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 520
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 521
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 522
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 524
    invoke-virtual/range {p2 .. p2}, Ljoj;->alO()Ljava/lang/String;

    move-result-object v6

    .line 526
    move-object/from16 v0, p2

    iget-object v0, v0, Ljoj;->ews:[Ljon;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v18, v0

    const/4 v2, 0x0

    move v10, v2

    :goto_0
    move/from16 v0, v18

    if-ge v10, v0, :cond_2

    aget-object v8, v17, v10

    .line 527
    iget-object v2, v8, Ljon;->ewy:Ljor;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ldyv;->a(Ljor;)Ljava/lang/String;

    move-result-object v9

    .line 528
    new-instance v2, Lcom/google/android/search/shared/contact/Contact;

    sget-object v3, Ldzb;->bRq:Ldzb;

    invoke-virtual {v8}, Ljon;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v9}, Lcom/google/android/search/shared/contact/Contact;-><init>(Ldzb;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v13, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 526
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_0

    .line 508
    :catch_0
    move-exception v2

    .line 509
    const-string v4, "Person"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ContactInformation has invalid ClientEntityId: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 510
    const/4 v2, 0x0

    .line 572
    :cond_1
    :goto_1
    return-object v2

    .line 511
    :catch_1
    move-exception v2

    .line 512
    const-string v4, "Person"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ContactInformation has invalid ClientEntityId: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 513
    const/4 v2, 0x0

    goto :goto_1

    .line 531
    :cond_2
    move-object/from16 v0, p2

    iget-object v0, v0, Ljoj;->ewt:[Ljok;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v18, v0

    const/4 v2, 0x0

    move v10, v2

    :goto_2
    move/from16 v0, v18

    if-ge v10, v0, :cond_3

    aget-object v8, v17, v10

    .line 532
    iget-object v2, v8, Ljok;->ewy:Ljor;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ldyv;->a(Ljor;)Ljava/lang/String;

    move-result-object v9

    .line 533
    new-instance v2, Lcom/google/android/search/shared/contact/Contact;

    sget-object v3, Ldzb;->bRp:Ldzb;

    invoke-virtual {v8}, Ljok;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v9}, Lcom/google/android/search/shared/contact/Contact;-><init>(Ldzb;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v14, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 531
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_2

    .line 536
    :cond_3
    move-object/from16 v0, p2

    iget-object v0, v0, Ljoj;->ewu:[Ljom;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v18, v0

    const/4 v2, 0x0

    move v10, v2

    :goto_3
    move/from16 v0, v18

    if-ge v10, v0, :cond_5

    aget-object v2, v17, v10

    .line 537
    iget-object v8, v2, Ljom;->ewD:Ljpd;

    .line 538
    if-eqz v8, :cond_4

    invoke-virtual {v8}, Ljpd;->bfb()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 539
    iget-object v2, v2, Ljom;->ewy:Ljor;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ldyv;->a(Ljor;)Ljava/lang/String;

    move-result-object v9

    .line 540
    new-instance v2, Lcom/google/android/search/shared/contact/Contact;

    sget-object v3, Ldzb;->bRr:Ldzb;

    invoke-virtual {v8}, Ljpd;->getAddress()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v9}, Lcom/google/android/search/shared/contact/Contact;-><init>(Ldzb;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v15, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 536
    :cond_4
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_3

    .line 544
    :cond_5
    move-object/from16 v0, p2

    iget-object v0, v0, Ljoj;->ewv:[Ljol;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v18, v0

    const/4 v2, 0x0

    move v10, v2

    :goto_4
    move/from16 v0, v18

    if-ge v10, v0, :cond_7

    aget-object v8, v17, v10

    .line 545
    invoke-virtual {v8}, Ljol;->rn()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v8}, Ljol;->getLabel()Ljava/lang/String;

    move-result-object v9

    .line 546
    :goto_5
    new-instance v2, Lcom/google/android/search/shared/contact/Contact;

    sget-object v3, Ldzb;->bRs:Ldzb;

    invoke-virtual {v8}, Ljol;->brp()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v9}, Lcom/google/android/search/shared/contact/Contact;-><init>(Ldzb;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 544
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_4

    .line 545
    :cond_6
    const/4 v9, 0x0

    goto :goto_5

    .line 550
    :cond_7
    new-instance v3, Lcom/google/android/search/shared/contact/Person;

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/search/shared/contact/Person;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    iput-boolean v11, v3, Lcom/google/android/search/shared/contact/Person;->bRI:Z

    invoke-virtual {v3, v13}, Lcom/google/android/search/shared/contact/Person;->D(Ljava/util/List;)Lcom/google/android/search/shared/contact/Person;

    move-result-object v2

    invoke-virtual {v2, v14}, Lcom/google/android/search/shared/contact/Person;->E(Ljava/util/List;)Lcom/google/android/search/shared/contact/Person;

    move-result-object v2

    invoke-virtual {v2, v15}, Lcom/google/android/search/shared/contact/Person;->G(Ljava/util/List;)Lcom/google/android/search/shared/contact/Person;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/google/android/search/shared/contact/Person;->F(Ljava/util/List;)Lcom/google/android/search/shared/contact/Person;

    move-result-object v2

    iput-object v12, v2, Lcom/google/android/search/shared/contact/Person;->bRK:Ljava/lang/String;

    move-object/from16 v0, p1

    iput-object v0, v2, Lcom/google/android/search/shared/contact/Person;->bRJ:Ljava/lang/String;

    .line 559
    move-object/from16 v0, p2

    iget-object v4, v0, Ljoj;->ewo:[Ljoo;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_6
    if-ge v3, v5, :cond_a

    aget-object v6, v4, v3

    .line 560
    invoke-virtual {v6}, Ljoo;->getCanonicalName()Ljava/lang/String;

    move-result-object v7

    .line 561
    invoke-virtual {v6}, Ljoo;->amp()Ljava/lang/String;

    move-result-object v6

    .line 562
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_8

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 563
    :cond_8
    const-string v8, "Person"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Received relationship with empty name: c=\""

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "\", o=\""

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\"."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    :goto_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 567
    :cond_9
    new-instance v8, Lcom/google/android/search/shared/contact/Relationship;

    invoke-direct {v8, v6, v7}, Lcom/google/android/search/shared/contact/Relationship;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Lcom/google/android/search/shared/contact/Person;->d(Lcom/google/android/search/shared/contact/Relationship;)V

    goto :goto_7

    .line 569
    :cond_a
    move-object/from16 v0, p2

    iget-object v4, v0, Ljoj;->ewp:[Ljava/lang/String;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_8
    if-ge v3, v5, :cond_1

    aget-object v6, v4, v3

    .line 570
    invoke-virtual {v2, v6}, Lcom/google/android/search/shared/contact/Person;->kB(Ljava/lang/String;)V

    .line 569
    add-int/lit8 v3, v3, 0x1

    goto :goto_8
.end method

.method public static a(Ldzb;Ljava/util/List;)Ljava/util/List;
    .locals 3
    .param p0    # Ldzb;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 445
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 446
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 447
    invoke-virtual {v0, p0}, Lcom/google/android/search/shared/contact/Person;->d(Ldzb;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 449
    :cond_0
    return-object v1
.end method

.method public static varargs a(Ljava/util/List;[Ldzl;)Ljava/util/List;
    .locals 9
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # [Ldzl;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 687
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 694
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 695
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    move v2, v3

    .line 699
    :goto_1
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_7

    .line 700
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/contact/Person;

    array-length v8, p1

    move v5, v3

    :goto_2
    if-ge v5, v8, :cond_2

    aget-object v4, p1, v5

    invoke-virtual {v4, v1, v0}, Ldzl;->a(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Person;)Lcom/google/android/search/shared/contact/Person;

    move-result-object v4

    if-eqz v4, :cond_1

    move-object v1, v4

    .line 702
    :goto_3
    if-eqz v1, :cond_3

    .line 705
    invoke-interface {v6, v2, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 706
    const/4 v1, 0x1

    .line 711
    :goto_4
    if-nez v1, :cond_0

    .line 713
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 700
    :cond_1
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    goto :goto_3

    .line 699
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 719
    :cond_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    iget-boolean v4, v0, Lcom/google/android/search/shared/contact/Person;->bRI:Z

    if-eqz v4, :cond_5

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_5
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_6
    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object v1

    :cond_7
    move v1, v3

    goto :goto_4
.end method

.method public static a(Ljava/util/List;Ljava/util/List;Leai;)V
    .locals 11
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Leai;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 841
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p2, v0}, Leai;->kI(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 843
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p2, v0}, Leai;->kK(Ljava/lang/String;)Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v6

    .line 845
    if-eqz v1, :cond_3

    .line 846
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/search/shared/contact/Person;->kD(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 847
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 848
    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 849
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 853
    :cond_3
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 854
    :cond_4
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 855
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 856
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->oK()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 857
    if-eqz v6, :cond_5

    invoke-virtual {v0, v6}, Lcom/google/android/search/shared/contact/Person;->c(Lcom/google/android/search/shared/contact/Relationship;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 862
    :cond_5
    new-array v1, v5, [Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/search/shared/contact/Person;->mName:Ljava/lang/String;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    .line 867
    iget-object v3, v0, Lcom/google/android/search/shared/contact/Person;->bRL:Ljava/util/Set;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v3

    if-eqz v3, :cond_6

    iget-object v3, v0, Lcom/google/android/search/shared/contact/Person;->bRL:Ljava/util/Set;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-eqz v3, :cond_6

    .line 868
    iget-object v0, v0, Lcom/google/android/search/shared/contact/Person;->bRL:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 869
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 874
    :cond_6
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v2

    move v3, v2

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 875
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v4, v3

    move v3, v1

    :cond_7
    :goto_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 877
    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v10}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 878
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    move v4, v5

    .line 880
    goto :goto_5

    .line 882
    :cond_8
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    move v3, v5

    .line 883
    goto :goto_5

    :cond_9
    move v1, v3

    move v3, v4

    .line 886
    goto :goto_4

    .line 888
    :cond_a
    if-nez v3, :cond_4

    if-eqz v1, :cond_4

    .line 889
    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_2

    .line 892
    :cond_b
    return-void
.end method

.method public static b(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/search/shared/contact/Person;
    .locals 4
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 811
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 812
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->oK()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 813
    iget-object v2, v0, Lcom/google/android/search/shared/contact/Person;->mName:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 817
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 818
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 822
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Contact;)V
    .locals 3
    .param p0    # Lcom/google/android/search/shared/contact/Person;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Lcom/google/android/search/shared/contact/Contact;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 453
    sget-object v0, Ldzi;->bRb:[I

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Contact;->akB()Ldzb;

    move-result-object v1

    invoke-virtual {v1}, Ldzb;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 470
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported mode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Contact;->akB()Ldzb;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 455
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNt:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 468
    :goto_0
    :pswitch_1
    return-void

    .line 458
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNu:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 461
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNv:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 464
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRH:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 453
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public static j(Lcom/google/android/search/shared/contact/Contact;)Lcom/google/android/search/shared/contact/Person;
    .locals 2
    .param p0    # Lcom/google/android/search/shared/contact/Contact;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 478
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/search/shared/contact/Contact;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/search/shared/contact/Person;->H(Ljava/util/List;)Lcom/google/android/search/shared/contact/Person;

    move-result-object v0

    return-object v0
.end method

.method public static kD(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 796
    if-eqz p0, :cond_0

    const-string v0, "\'s"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 799
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final D(Ljava/util/List;)Lcom/google/android/search/shared/contact/Person;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNt:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 302
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNt:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 303
    return-object p0
.end method

.method public final E(Ljava/util/List;)Lcom/google/android/search/shared/contact/Person;
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNu:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 326
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNu:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 327
    return-object p0
.end method

.method public final F(Ljava/util/List;)Lcom/google/android/search/shared/contact/Person;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRH:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 336
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRH:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 337
    return-object p0
.end method

.method public final G(Ljava/util/List;)Lcom/google/android/search/shared/contact/Person;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNv:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 360
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNv:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 361
    return-object p0
.end method

.method public final a(Ldyv;Lcom/google/android/search/shared/contact/Contact;)Ljoj;
    .locals 8
    .param p1    # Ldyv;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/search/shared/contact/Contact;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 578
    new-instance v3, Ljoj;

    invoke-direct {v3}, Ljoj;-><init>()V

    .line 582
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/Person;->oK()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 583
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->mName:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljoj;->xD(Ljava/lang/String;)Ljoj;

    .line 584
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/Person;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljoj;->xG(Ljava/lang/String;)Ljoj;

    .line 587
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bjd:Ljava/lang/String;

    invoke-static {v0}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 588
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bjd:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljoj;->xE(Ljava/lang/String;)Ljoj;

    .line 591
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->aYG:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 592
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->aYG:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljoo;

    iput-object v0, v3, Ljoj;->ewo:[Ljoo;

    .line 594
    const/4 v0, 0x0

    .line 595
    iget-object v1, p0, Lcom/google/android/search/shared/contact/Person;->aYG:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Relationship;

    .line 596
    iget-object v5, v3, Ljoj;->ewo:[Ljoo;

    add-int/lit8 v2, v1, 0x1

    new-instance v6, Ljoo;

    invoke-direct {v6}, Ljoo;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Relationship;->amp()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljoo;->xM(Ljava/lang/String;)Ljoo;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Relationship;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljoo;->xL(Ljava/lang/String;)Ljoo;

    move-result-object v0

    aput-object v0, v5, v1

    move v1, v2

    .line 599
    goto :goto_0

    .line 602
    :cond_2
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRL:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 603
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRL:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/search/shared/contact/Person;->bRL:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v3, Ljoj;->ewp:[Ljava/lang/String;

    .line 611
    :cond_3
    if-eqz p2, :cond_5

    .line 612
    invoke-virtual {p2, p1, v3}, Lcom/google/android/search/shared/contact/Contact;->a(Ldyv;Ljoj;)V

    .line 631
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/search/shared/contact/Person;->bRI:Z

    invoke-virtual {v3, v0}, Ljoj;->iN(Z)Ljoj;

    .line 633
    return-object v3

    .line 614
    :cond_5
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNt:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 615
    invoke-virtual {v0, p1, v3}, Lcom/google/android/search/shared/contact/Contact;->a(Ldyv;Ljoj;)V

    goto :goto_1

    .line 618
    :cond_6
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNu:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 619
    invoke-virtual {v0, p1, v3}, Lcom/google/android/search/shared/contact/Contact;->a(Ldyv;Ljoj;)V

    goto :goto_2

    .line 622
    :cond_7
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNv:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 623
    invoke-virtual {v0, p1, v3}, Lcom/google/android/search/shared/contact/Contact;->a(Ldyv;Ljoj;)V

    goto :goto_3

    .line 626
    :cond_8
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRH:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 627
    invoke-virtual {v0, p1, v3}, Lcom/google/android/search/shared/contact/Contact;->a(Ldyv;Ljoj;)V

    goto :goto_4
.end method

.method public final a(Ldzk;)Z
    .locals 2
    .param p1    # Ldzk;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 292
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ldzk;->oK()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/Person;->oK()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->mName:Ljava/lang/String;

    invoke-interface {p1}, Ldzk;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aiG()Ljava/util/List;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNt:Ljava/util/List;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    return-object v0
.end method

.method public final aiH()Ljava/util/List;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNu:Ljava/util/List;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    return-object v0
.end method

.method public final aiI()Ljava/util/List;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNv:Ljava/util/List;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    return-object v0
.end method

.method public final alO()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bjd:Ljava/lang/String;

    return-object v0
.end method

.method public final alS()Ljava/lang/String;
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->mName:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->mName:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/search/shared/contact/Person;->bCq:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final alT()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRJ:Ljava/lang/String;

    return-object v0
.end method

.method public final alU()Z
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/google/android/search/shared/contact/Person;->bRM:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final alV()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/google/android/search/shared/contact/Person;->bRM:I

    return v0
.end method

.method public final alW()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 182
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRJ:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    const/4 v0, 0x0

    .line 188
    :goto_0
    return-object v0

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRJ:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 186
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRJ:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/search/shared/contact/Person;->bCq:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 188
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/search/shared/contact/Person;->bRJ:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/search/shared/contact/Person;->bCq:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/contact/Person;->bRJ:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final alX()Z
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/google/android/search/shared/contact/Person;->bRI:Z

    return v0
.end method

.method public final alY()Ljava/util/Set;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRL:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final alZ()Ljava/lang/String;
    .locals 3

    .prologue
    .line 214
    const-string v0, ", "

    invoke-static {v0}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/Person;->amc()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/search/shared/contact/Person;->bRL:Ljava/util/Set;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v1, v2}, Likr;->b(Ljava/util/Iterator;Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lifj;->a(Ljava/util/Iterator;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ama()Z
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->aYG:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final amb()Ljava/util/Set;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->aYG:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final amc()Ljava/util/Set;
    .locals 2
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->aYG:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    new-instance v1, Ldzg;

    invoke-direct {v1, p0}, Ldzg;-><init>(Lcom/google/android/search/shared/contact/Person;)V

    invoke-static {v0, v1}, Liia;->a(Ljava/util/Collection;Lifg;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Liqs;->z(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method public final amd()Ljava/util/List;
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRH:Ljava/util/List;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    return-object v0
.end method

.method public final ame()Z
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRK:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/search/shared/contact/Relationship;)Z
    .locals 3

    .prologue
    .line 219
    const/4 v0, 0x0

    .line 220
    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/contact/Person;->c(Lcom/google/android/search/shared/contact/Relationship;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 222
    iget-object v1, p0, Lcom/google/android/search/shared/contact/Person;->mName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 224
    iget-object v1, p0, Lcom/google/android/search/shared/contact/Person;->mName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/search/shared/contact/Person;->bCq:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 225
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Relationship;->amp()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Relationship;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 228
    :cond_0
    const/4 v0, 0x1

    .line 232
    :cond_1
    return v0
.end method

.method public final c(Lcom/google/android/search/shared/contact/Relationship;)Z
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->aYG:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c(Ldzb;)Z
    .locals 4
    .param p1    # Ldzb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 397
    if-nez p1, :cond_0

    .line 398
    sget-object p1, Ldzb;->bRt:Ldzb;

    .line 400
    :cond_0
    sget-object v2, Ldzi;->bRb:[I

    invoke-virtual {p1}, Ldzb;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 410
    :cond_1
    :goto_0
    return v0

    .line 402
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/search/shared/contact/Person;->bRH:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 404
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/search/shared/contact/Person;->bNu:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 406
    :pswitch_2
    iget-object v2, p0, Lcom/google/android/search/shared/contact/Person;->bNt:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 408
    :pswitch_3
    iget-object v2, p0, Lcom/google/android/search/shared/contact/Person;->bNv:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 400
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final d(Ldzb;)Ljava/util/List;
    .locals 2
    .param p1    # Ldzb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 418
    if-nez p1, :cond_0

    .line 419
    sget-object p1, Ldzb;->bRt:Ldzb;

    .line 421
    :cond_0
    sget-object v0, Ldzi;->bRb:[I

    invoke-virtual {p1}, Ldzb;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 431
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 432
    iget-object v1, p0, Lcom/google/android/search/shared/contact/Person;->bRH:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 433
    iget-object v1, p0, Lcom/google/android/search/shared/contact/Person;->bNu:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 434
    iget-object v1, p0, Lcom/google/android/search/shared/contact/Person;->bNt:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 435
    iget-object v1, p0, Lcom/google/android/search/shared/contact/Person;->bNv:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 436
    :goto_0
    return-object v0

    .line 423
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRH:Ljava/util/List;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    goto :goto_0

    .line 425
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNu:Ljava/util/List;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    goto :goto_0

    .line 427
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNt:Ljava/util/List;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    goto :goto_0

    .line 429
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNv:Ljava/util/List;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    goto :goto_0

    .line 421
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final d(Lcom/google/android/search/shared/contact/Relationship;)V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->aYG:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->aYG:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->aYG:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 249
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 935
    const/4 v0, 0x0

    return v0
.end method

.method public final e(Lcom/google/android/search/shared/contact/Relationship;)V
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->aYG:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 260
    return-void
.end method

.method public final e(Lcom/google/android/search/shared/contact/Person;)Z
    .locals 4

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNt:Ljava/util/List;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/search/shared/contact/Contact;

    .line 308
    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Contact;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p1, Lcom/google/android/search/shared/contact/Person;->bNt:Ljava/util/List;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 312
    invoke-virtual {v1, v0}, Lcom/google/android/search/shared/contact/Contact;->h(Lcom/google/android/search/shared/contact/Contact;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 313
    const/4 v0, 0x1

    .line 317
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 921
    instance-of v0, p1, Lcom/google/android/search/shared/contact/Person;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/search/shared/contact/Person;

    iget-wide v0, p1, Lcom/google/android/search/shared/contact/Person;->bRa:J

    iget-wide v2, p0, Lcom/google/android/search/shared/contact/Person;->bRa:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ey(Z)Lcom/google/android/search/shared/contact/Person;
    .locals 0

    .prologue
    .line 197
    iput-boolean p1, p0, Lcom/google/android/search/shared/contact/Person;->bRI:Z

    .line 198
    return-object p0
.end method

.method public final f(Lcom/google/android/search/shared/contact/Person;)Z
    .locals 4

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNu:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 342
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->hasValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 343
    iget-object v1, p1, Lcom/google/android/search/shared/contact/Person;->bNu:Ljava/util/List;

    invoke-static {v1}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/contact/Contact;

    .line 346
    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/contact/Contact;->h(Lcom/google/android/search/shared/contact/Contact;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 347
    const/4 v0, 0x1

    .line 351
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g(Lcom/google/android/search/shared/contact/Person;)Z
    .locals 2
    .param p1    # Lcom/google/android/search/shared/contact/Person;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 388
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->ame()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/Person;->ame()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRK:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/search/shared/contact/Person;->bRK:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final gX(I)V
    .locals 0

    .prologue
    .line 167
    iput p1, p0, Lcom/google/android/search/shared/contact/Person;->bRM:I

    .line 168
    return-void
.end method

.method public final getId()J
    .locals 2

    .prologue
    .line 128
    iget-wide v0, p0, Lcom/google/android/search/shared/contact/Person;->bRa:J

    return-wide v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final getUri()Landroid/net/Uri;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 365
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNt:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNt:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->oK()Z

    move-result v0

    if-nez v0, :cond_0

    .line 366
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "tel"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNt:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->opaquePart(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 371
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/google/android/search/shared/contact/Person;->bRa:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final h(Lcom/google/android/search/shared/contact/Person;)Lcom/google/android/search/shared/contact/Person;
    .locals 2

    .prologue
    .line 760
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 763
    iget-boolean v0, p0, Lcom/google/android/search/shared/contact/Person;->bRI:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lcom/google/android/search/shared/contact/Person;->bRI:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/shared/contact/Person;->bRI:Z

    .line 766
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->ama()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 767
    iget-object v0, p1, Lcom/google/android/search/shared/contact/Person;->aYG:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Relationship;

    .line 768
    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/contact/Person;->d(Lcom/google/android/search/shared/contact/Relationship;)V

    goto :goto_1

    .line 763
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 772
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNt:Ljava/util/List;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 773
    iget-object v1, p1, Lcom/google/android/search/shared/contact/Person;->bNt:Ljava/util/List;

    invoke-static {v1}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 774
    invoke-static {v0}, Lcom/google/android/search/shared/contact/Contact;->C(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/contact/Person;->D(Ljava/util/List;)Lcom/google/android/search/shared/contact/Person;

    .line 776
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNu:Ljava/util/List;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 777
    iget-object v1, p1, Lcom/google/android/search/shared/contact/Person;->bNu:Ljava/util/List;

    invoke-static {v1}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 778
    invoke-static {v0}, Lcom/google/android/search/shared/contact/Contact;->C(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/contact/Person;->E(Ljava/util/List;)Lcom/google/android/search/shared/contact/Person;

    .line 780
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNv:Ljava/util/List;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 781
    iget-object v1, p1, Lcom/google/android/search/shared/contact/Person;->bNv:Ljava/util/List;

    invoke-static {v1}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 782
    invoke-static {v0}, Lcom/google/android/search/shared/contact/Contact;->C(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/contact/Person;->G(Ljava/util/List;)Lcom/google/android/search/shared/contact/Person;

    .line 784
    return-object p0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 926
    iget-wide v0, p0, Lcom/google/android/search/shared/contact/Person;->bRa:J

    long-to-int v0, v0

    return v0
.end method

.method public final kA(Ljava/lang/String;)Lcom/google/android/search/shared/contact/Person;
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/google/android/search/shared/contact/Person;->bRJ:Ljava/lang/String;

    .line 147
    return-object p0
.end method

.method public final kB(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRL:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 203
    return-void
.end method

.method public final kC(Ljava/lang/String;)Lcom/google/android/search/shared/contact/Person;
    .locals 0

    .prologue
    .line 379
    iput-object p1, p0, Lcom/google/android/search/shared/contact/Person;->bRK:Ljava/lang/String;

    .line 380
    return-object p0
.end method

.method public final l(Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRL:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 207
    return-void
.end method

.method public final m(Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 252
    if-nez p1, :cond_0

    .line 256
    :goto_0
    return-void

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->aYG:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public final n(Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->aYG:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 264
    return-void
.end method

.method public final oK()Z
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->mName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 912
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Person : ID = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/search/shared/contact/Person;->bRa:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : Name = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/contact/Person;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 940
    iget-wide v0, p0, Lcom/google/android/search/shared/contact/Person;->bRa:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 941
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bjd:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 942
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 943
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRJ:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 944
    iget-boolean v0, p0, Lcom/google/android/search/shared/contact/Person;->bRI:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 945
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNt:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 946
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNu:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 947
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bNv:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 948
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRH:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 949
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRK:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 950
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->aYG:Ljava/util/Set;

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 951
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Person;->bRL:Ljava/util/Set;

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 952
    iget v0, p0, Lcom/google/android/search/shared/contact/Person;->bRM:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 953
    return-void

    .line 944
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
