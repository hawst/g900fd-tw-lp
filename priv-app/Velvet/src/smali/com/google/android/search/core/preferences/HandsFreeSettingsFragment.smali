.class public Lcom/google/android/search/core/preferences/HandsFreeSettingsFragment;
.super Lcom/google/android/search/core/preferences/SettingsFragmentBase;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected final Tj()I
    .locals 1

    .prologue
    .line 21
    const v0, 0x7f070014

    return v0
.end method

.method protected final a(Lgql;)Lcxt;
    .locals 4

    .prologue
    .line 26
    invoke-virtual {p1}, Lgql;->aJq()Lhhq;

    move-result-object v0

    .line 27
    new-instance v1, Lcwh;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/HandsFreeSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {p1}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->BL()Lchk;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcwh;-><init>(Lhym;Landroid/app/Activity;Lchk;)V

    return-object v1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 6

    .prologue
    .line 33
    invoke-super {p0, p1, p2}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 36
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/HandsFreeSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "now_handsfree"

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v0

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v3

    new-instance v0, Lgpk;

    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/HandsFreeSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v0, v4}, Lgpk;-><init>(Landroid/content/Context;)V

    const-string v4, "now_handsfree"

    invoke-virtual {v0, v4}, Lgpk;->nv(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v0

    invoke-virtual {v0}, Lcrh;->Su()Z

    move-result v5

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lgbk;->a(Landroid/view/Menu;Landroid/app/Activity;Ljava/lang/String;Landroid/accounts/Account;Landroid/net/Uri;Z)V

    .line 41
    return-void
.end method
