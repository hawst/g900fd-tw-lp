.class public Lcom/google/android/search/shared/actions/PlayMediaAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private bMA:Z

.field private bMB:Lcom/google/android/shared/util/App;

.field private bMC:Z

.field private bMD:Lcom/google/android/shared/util/App;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bME:Ljava/util/List;

.field private bMF:Landroid/content/Intent;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bMx:Ljmh;

.field private bMy:Landroid/net/Uri;

.field private bMz:Lijj;

.field private mPlayStoreLink:Lcom/google/android/shared/util/App;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 268
    new-instance v0, Ldut;

    invoke-direct {v0}, Ldut;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/PlayMediaAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 57
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 58
    const-class v0, Ljmh;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljmh;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMx:Ljmh;

    .line 60
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMy:Landroid/net/Uri;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 62
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 63
    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMz:Lijj;

    .line 64
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/App;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMB:Lcom/google/android/shared/util/App;

    .line 65
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/App;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->mPlayStoreLink:Lcom/google/android/shared/util/App;

    .line 66
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/App;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMD:Lcom/google/android/shared/util/App;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 69
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 70
    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bME:Ljava/util/List;

    .line 71
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMF:Landroid/content/Intent;

    .line 72
    return-void
.end method

.method public constructor <init>(Ljmh;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 50
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljmh;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMx:Ljmh;

    .line 52
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bME:Ljava/util/List;

    .line 53
    return-void
.end method

.method protected constructor <init>(Ljmh;Landroid/net/Uri;Lijj;ZLcom/google/android/shared/util/App;Lcom/google/android/shared/util/App;Lcom/google/android/shared/util/App;Ljava/util/List;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMx:Ljmh;

    .line 79
    iput-object p2, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMy:Landroid/net/Uri;

    .line 80
    iput-object p3, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMz:Lijj;

    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMA:Z

    .line 82
    iput-object p5, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMB:Lcom/google/android/shared/util/App;

    .line 83
    iput-object p6, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->mPlayStoreLink:Lcom/google/android/shared/util/App;

    .line 84
    iput-object p7, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMD:Lcom/google/android/shared/util/App;

    .line 85
    iput-object p8, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bME:Ljava/util/List;

    .line 86
    iput-object p9, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMF:Landroid/content/Intent;

    .line 87
    return-void
.end method


# virtual methods
.method public final C(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMy:Landroid/net/Uri;

    .line 199
    return-void
.end method

.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 252
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/PlayMediaAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final ahO()Ljmh;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMx:Ljmh;

    return-object v0
.end method

.method public final ahP()Lcom/google/android/shared/util/App;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->mPlayStoreLink:Lcom/google/android/shared/util/App;

    return-object v0
.end method

.method public final ahQ()Ljava/util/List;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bME:Ljava/util/List;

    return-object v0
.end method

.method public final ahR()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMF:Landroid/content/Intent;

    return-object v0
.end method

.method public final ahS()Lcom/google/android/shared/util/App;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMD:Lcom/google/android/shared/util/App;

    return-object v0
.end method

.method public final ahT()Ljava/util/List;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMz:Lijj;

    return-object v0
.end method

.method public final ahU()Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMA:Z

    return v0
.end method

.method public final ahV()Z
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMC:Z

    return v0
.end method

.method public final ahW()Lcom/google/android/shared/util/App;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMB:Lcom/google/android/shared/util/App;

    return-object v0
.end method

.method public final ahX()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMy:Landroid/net/Uri;

    return-object v0
.end method

.method public final ahY()Z
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMx:Ljmh;

    iget-object v0, v0, Ljmh;->etB:Ljmk;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ahZ()Z
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMx:Ljmh;

    iget-object v0, v0, Ljmh;->etA:Ljml;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aia()Z
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMx:Ljmh;

    iget-object v0, v0, Ljmh;->etD:Ljmi;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aib()Z
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMx:Ljmh;

    iget-object v0, v0, Ljmh;->etC:Ljmj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 247
    const/4 v0, 0x1

    return v0
.end method

.method public final em(Z)V
    .locals 0

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMA:Z

    .line 145
    return-void
.end method

.method public final getMimeType()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahY()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    const-string v0, "video/movie"

    .line 228
    :goto_0
    return-object v0

    .line 222
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahZ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223
    const-string v0, "audio/music"

    goto :goto_0

    .line 224
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->aib()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 225
    const-string v0, "text/book"

    goto :goto_0

    .line 228
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h(Lcom/google/android/shared/util/App;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->mPlayStoreLink:Lcom/google/android/shared/util/App;

    .line 122
    return-void
.end method

.method public final i(Lcom/google/android/shared/util/App;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMD:Lcom/google/android/shared/util/App;

    .line 126
    return-void
.end method

.method public final isPreviewEnabled()Z
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMF:Landroid/content/Intent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j(Lcom/google/android/shared/util/App;)V
    .locals 1

    .prologue
    .line 163
    iput-object p1, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMB:Lcom/google/android/shared/util/App;

    .line 164
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMC:Z

    .line 165
    return-void
.end method

.method public final k(Lcom/google/android/shared/util/App;)V
    .locals 1

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMB:Lcom/google/android/shared/util/App;

    .line 175
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMC:Z

    .line 176
    return-void
.end method

.method public final k(Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 136
    invoke-static {p1}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMz:Lijj;

    .line 137
    return-void
.end method

.method public final r(Ljava/util/List;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bME:Ljava/util/List;

    .line 118
    return-void
.end method

.method public final uF()I
    .locals 1

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->aia()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    const/4 v0, 0x3

    .line 242
    :goto_0
    return v0

    .line 234
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->aib()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 235
    const/16 v0, 0x20

    goto :goto_0

    .line 236
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahY()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 237
    const/16 v0, 0x1f

    goto :goto_0

    .line 238
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahZ()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 239
    const/16 v0, 0x1e

    goto :goto_0

    .line 242
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMF:Landroid/content/Intent;

    .line 106
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 257
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 258
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMx:Ljmh;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 259
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMy:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 260
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMz:Lijj;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 261
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMB:Lcom/google/android/shared/util/App;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 262
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->mPlayStoreLink:Lcom/google/android/shared/util/App;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 263
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMD:Lcom/google/android/shared/util/App;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 264
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bME:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 265
    iget-object v0, p0, Lcom/google/android/search/shared/actions/PlayMediaAction;->bMF:Landroid/content/Intent;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 266
    return-void
.end method
