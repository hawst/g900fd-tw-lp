.class public Lcom/google/android/search/shared/actions/ReadNotificationAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bMP:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Ldux;

    invoke-direct {v0}, Ldux;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/ReadNotificationAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 19
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/ReadNotificationAction;->bMP:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/google/android/search/shared/actions/ReadNotificationAction;->bMP:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/ReadNotificationAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public final getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ReadNotificationAction;->bMP:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 29
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 30
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ReadNotificationAction;->bMP:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 31
    return-void
.end method
