.class public Lcom/google/android/search/shared/contact/Contact;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ldzk;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private bMW:Ljava/lang/String;

.field private final bPD:Ldzb;

.field private final bRa:J

.field private bjd:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 315
    new-instance v0, Ldyr;

    invoke-direct {v0}, Ldyr;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/contact/Contact;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ldzb;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/search/shared/contact/Contact;->bPD:Ldzb;

    .line 40
    iput-wide p2, p0, Lcom/google/android/search/shared/contact/Contact;->bRa:J

    .line 41
    iput-object p4, p0, Lcom/google/android/search/shared/contact/Contact;->bjd:Ljava/lang/String;

    .line 42
    iput-object p5, p0, Lcom/google/android/search/shared/contact/Contact;->mName:Ljava/lang/String;

    .line 43
    iput-object p6, p0, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    .line 44
    iput-object p7, p0, Lcom/google/android/search/shared/contact/Contact;->bMW:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public static C(Ljava/util/List;)Ljava/util/List;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 139
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 140
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    move v2, v3

    .line 142
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 143
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/contact/Contact;

    .line 144
    invoke-static {v1, v0}, Lcom/google/android/search/shared/contact/Contact;->a(Lcom/google/android/search/shared/contact/Contact;Lcom/google/android/search/shared/contact/Contact;)Lcom/google/android/search/shared/contact/Contact;

    move-result-object v6

    .line 145
    if-eqz v6, :cond_1

    .line 146
    const/4 v1, 0x1

    .line 147
    invoke-interface {v4, v2, v6}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 151
    :goto_2
    if-nez v1, :cond_0

    .line 152
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 142
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 155
    :cond_2
    return-object v4

    :cond_3
    move v1, v3

    goto :goto_2
.end method

.method public static a(Lcom/google/android/search/shared/contact/Contact;Lcom/google/android/search/shared/contact/Contact;)Lcom/google/android/search/shared/contact/Contact;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 169
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/search/shared/contact/Contact;->bPD:Ldzb;

    iget-object v2, p1, Lcom/google/android/search/shared/contact/Contact;->bPD:Ldzb;

    if-eq v1, v2, :cond_1

    .line 200
    :cond_0
    :goto_0
    return-object v0

    .line 173
    :cond_1
    iget-object v1, p0, Lcom/google/android/search/shared/contact/Contact;->bPD:Ldzb;

    sget-object v2, Ldzb;->bRt:Ldzb;

    if-ne v1, v2, :cond_4

    .line 175
    if-eqz p1, :cond_3

    invoke-interface {p1}, Ldzk;->oK()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/Contact;->oK()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/search/shared/contact/Contact;->mName:Ljava/lang/String;

    invoke-interface {p1}, Ldzk;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    .line 200
    :cond_2
    new-instance v0, Lcom/google/android/search/shared/contact/Contact;

    iget-object v1, p0, Lcom/google/android/search/shared/contact/Contact;->bPD:Ldzb;

    iget-wide v2, p0, Lcom/google/android/search/shared/contact/Contact;->bRa:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_5

    iget-wide v2, p0, Lcom/google/android/search/shared/contact/Contact;->bRa:J

    :goto_2
    iget-object v4, p0, Lcom/google/android/search/shared/contact/Contact;->bjd:Ljava/lang/String;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/search/shared/contact/Contact;->bjd:Ljava/lang/String;

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/Contact;->oK()Z

    move-result v5

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/google/android/search/shared/contact/Contact;->mName:Ljava/lang/String;

    :goto_4
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/Contact;->hasValue()Z

    move-result v6

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    :goto_5
    iget-object v7, p0, Lcom/google/android/search/shared/contact/Contact;->bMW:Ljava/lang/String;

    if-eqz v7, :cond_9

    iget-object v7, p0, Lcom/google/android/search/shared/contact/Contact;->bMW:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_9

    iget-object v7, p0, Lcom/google/android/search/shared/contact/Contact;->bMW:Ljava/lang/String;

    :goto_6
    invoke-direct/range {v0 .. v7}, Lcom/google/android/search/shared/contact/Contact;-><init>(Ldzb;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 175
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 187
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/contact/Contact;->h(Lcom/google/android/search/shared/contact/Contact;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 200
    :cond_5
    iget-wide v2, p1, Lcom/google/android/search/shared/contact/Contact;->bRa:J

    goto :goto_2

    :cond_6
    iget-object v4, p1, Lcom/google/android/search/shared/contact/Contact;->bjd:Ljava/lang/String;

    goto :goto_3

    :cond_7
    iget-object v5, p1, Lcom/google/android/search/shared/contact/Contact;->mName:Ljava/lang/String;

    goto :goto_4

    :cond_8
    iget-object v6, p1, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    goto :goto_5

    :cond_9
    iget-object v7, p1, Lcom/google/android/search/shared/contact/Contact;->bMW:Ljava/lang/String;

    goto :goto_6
.end method

.method public static ku(Ljava/lang/String;)Lcom/google/android/search/shared/contact/Contact;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 53
    new-instance v0, Lcom/google/android/search/shared/contact/Contact;

    sget-object v1, Ldzb;->bRq:Ldzb;

    const-wide/16 v2, 0x0

    move-object v5, v4

    move-object v6, p0

    move-object v7, v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/search/shared/contact/Contact;-><init>(Ldzb;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ldyv;Ljoj;)V
    .locals 4
    .param p1    # Ldyv;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljoj;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 226
    sget-object v0, Ldys;->bRb:[I

    iget-object v1, p0, Lcom/google/android/search/shared/contact/Contact;->bPD:Ldzb;

    invoke-virtual {v1}, Ldzb;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 247
    :goto_0
    return-void

    .line 228
    :pswitch_0
    iget-object v0, p2, Ljoj;->ews:[Ljon;

    new-instance v1, Ljon;

    invoke-direct {v1}, Ljon;-><init>()V

    iget-object v2, p0, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljon;->xK(Ljava/lang/String;)Ljon;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/search/shared/contact/Contact;->bMW:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ldyv;->kw(Ljava/lang/String;)Ljor;

    move-result-object v2

    iput-object v2, v1, Ljon;->ewy:Ljor;

    invoke-static {v0, v1}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljon;

    iput-object v0, p2, Ljoj;->ews:[Ljon;

    goto :goto_0

    .line 233
    :pswitch_1
    iget-object v0, p2, Ljoj;->ewt:[Ljok;

    new-instance v1, Ljok;

    invoke-direct {v1}, Ljok;-><init>()V

    iget-object v2, p0, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljok;->xH(Ljava/lang/String;)Ljok;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/search/shared/contact/Contact;->bMW:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ldyv;->kw(Ljava/lang/String;)Ljor;

    move-result-object v2

    iput-object v2, v1, Ljok;->ewy:Ljor;

    invoke-static {v0, v1}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljok;

    iput-object v0, p2, Ljoj;->ewt:[Ljok;

    goto :goto_0

    .line 238
    :pswitch_2
    iget-object v0, p2, Ljoj;->ewu:[Ljom;

    new-instance v1, Ljom;

    invoke-direct {v1}, Ljom;-><init>()V

    new-instance v2, Ljpd;

    invoke-direct {v2}, Ljpd;-><init>()V

    iget-object v3, p0, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljpd;->xY(Ljava/lang/String;)Ljpd;

    move-result-object v2

    iput-object v2, v1, Ljom;->ewD:Ljpd;

    iget-object v2, p0, Lcom/google/android/search/shared/contact/Contact;->bMW:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ldyv;->kw(Ljava/lang/String;)Ljor;

    move-result-object v2

    iput-object v2, v1, Ljom;->ewy:Ljor;

    invoke-static {v0, v1}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljom;

    iput-object v0, p2, Ljoj;->ewu:[Ljom;

    goto :goto_0

    .line 243
    :pswitch_3
    iget-object v0, p2, Ljoj;->ewv:[Ljol;

    new-instance v1, Ljol;

    invoke-direct {v1}, Ljol;-><init>()V

    iget-object v2, p0, Lcom/google/android/search/shared/contact/Contact;->bMW:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/search/shared/contact/Contact;->bMW:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljol;->xI(Ljava/lang/String;)Ljol;

    :cond_0
    iget-object v2, p0, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljol;->xJ(Ljava/lang/String;)Ljol;

    invoke-static {v0, v1}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljol;

    iput-object v0, p2, Ljoj;->ewv:[Ljol;

    goto/16 :goto_0

    .line 226
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final akB()Ldzb;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->bPD:Ldzb;

    return-object v0
.end method

.method public final alO()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->bjd:Ljava/lang/String;

    return-object v0
.end method

.method public final alP()Ljava/lang/String;
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->bPD:Ldzb;

    sget-object v1, Ldzb;->bRq:Ldzb;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/Contact;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    invoke-static {v0}, Lerr;->lo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 124
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    goto :goto_0
.end method

.method public final alQ()Ljava/lang/String;
    .locals 4

    .prologue
    .line 128
    new-instance v0, Landroid/text/util/Rfc822Token;

    iget-object v1, p0, Lcom/google/android/search/shared/contact/Contact;->mName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Landroid/text/util/Rfc822Token;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 285
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 290
    instance-of v1, p1, Lcom/google/android/search/shared/contact/Contact;

    if-nez v1, :cond_1

    .line 295
    :cond_0
    :goto_0
    return v0

    .line 294
    :cond_1
    check-cast p1, Lcom/google/android/search/shared/contact/Contact;

    .line 295
    iget-wide v2, p0, Lcom/google/android/search/shared/contact/Contact;->bRa:J

    iget-wide v4, p1, Lcom/google/android/search/shared/contact/Contact;->bRa:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/search/shared/contact/Contact;->mName:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/search/shared/contact/Contact;->mName:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getId()J
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/google/android/search/shared/contact/Contact;->bRa:J

    return-wide v0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->bMW:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Lcom/google/android/search/shared/contact/Contact;)Z
    .locals 2

    .prologue
    .line 111
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->bPD:Ldzb;

    iget-object v1, p1, Lcom/google/android/search/shared/contact/Contact;->bPD:Ldzb;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Contact;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/Contact;->hasValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 112
    :cond_0
    const/4 v0, 0x0

    .line 117
    :goto_0
    return v0

    .line 114
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->bPD:Ldzb;

    sget-object v1, Ldzb;->bRq:Ldzb;

    if-ne v0, v1, :cond_2

    .line 115
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/telephony/PhoneNumberUtils;->compare(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 117
    :cond_2
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hasValue()Z
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 302
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/android/search/shared/contact/Contact;->bRa:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/search/shared/contact/Contact;->mName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final kv(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/search/shared/contact/Contact;->bjd:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public final oK()Z
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->mName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final rn()Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->bMW:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/search/shared/contact/Contact;->mName:Ljava/lang/String;

    .line 81
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 212
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Contact : ID = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/search/shared/contact/Contact;->bRa:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : Key = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/contact/Contact;->bjd:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : Name = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/contact/Contact;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : Mode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/contact/Contact;->bPD:Ldzb;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : Value = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : Label = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/contact/Contact;->bMW:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->bPD:Ldzb;

    invoke-virtual {v0}, Ldzb;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 308
    iget-wide v0, p0, Lcom/google/android/search/shared/contact/Contact;->bRa:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 309
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->bjd:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 310
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 311
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->mValue:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 312
    iget-object v0, p0, Lcom/google/android/search/shared/contact/Contact;->bMW:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 313
    return-void
.end method
