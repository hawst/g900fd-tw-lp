.class public Lcom/google/android/search/shared/contact/PersonSelectItem;
.super Lduc;
.source "PG"

# interfaces
.implements Lecw;


# static fields
.field public static final bRT:Landroid/util/LruCache;

.field public static final bSj:Landroid/graphics/drawable/Drawable$ConstantState;


# instance fields
.field private aYL:Lcom/google/android/search/shared/contact/Person;

.field public bRU:Landroid/os/AsyncTask;

.field private bRV:Z

.field private bRW:Z

.field private bRX:Z

.field private bRY:Z

.field private bRZ:Z

.field private bSa:Z

.field private bSb:Lcom/google/android/search/shared/contact/Contact;

.field private bSc:Landroid/widget/TextView;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bSd:Landroid/widget/TextView;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bSe:Landroid/widget/TextView;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bSf:Landroid/widget/TextView;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bSg:Landroid/widget/ImageView;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bSh:Landroid/view/View;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bSi:Landroid/view/View;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mContentResolver:Landroid/content/ContentResolver;

.field public final mResources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 74
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRT:Landroid/util/LruCache;

    .line 437
    new-instance v0, Leaa;

    invoke-direct {v0}, Leaa;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSj:Landroid/graphics/drawable/Drawable$ConstantState;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 115
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 118
    invoke-direct {p0, p1, p2, p3}, Lduc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 120
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->mResources:Landroid/content/res/Resources;

    .line 121
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->mContentResolver:Landroid/content/ContentResolver;

    .line 122
    const v0, 0x7f0c0124

    invoke-static {p0, v0}, Lehd;->r(Landroid/view/View;I)V

    .line 125
    sget-object v0, Lbwe;->aLX:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 127
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRZ:Z

    .line 129
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSa:Z

    .line 131
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRW:Z

    .line 133
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRX:Z

    .line 135
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRY:Z

    .line 137
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 138
    return-void
.end method

.method public static a(Landroid/widget/QuickContactBadge;Landroid/graphics/drawable/Drawable$ConstantState;)V
    .locals 1

    .prologue
    .line 443
    sget-object v0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSj:Landroid/graphics/drawable/Drawable$ConstantState;

    if-ne p1, v0, :cond_0

    .line 444
    invoke-virtual {p0}, Landroid/widget/QuickContactBadge;->setImageToDefault()V

    .line 448
    :goto_0
    return-void

    .line 446
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/QuickContactBadge;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/CharSequence;ZI)V
    .locals 2
    .param p0    # Landroid/widget/TextView;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 174
    if-eqz p0, :cond_1

    .line 175
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 176
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    if-eqz p2, :cond_0

    .line 178
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 180
    :cond_0
    if-ltz p3, :cond_1

    .line 181
    invoke-static {p0, p3}, Lehd;->r(Landroid/view/View;I)V

    .line 184
    :cond_1
    return-void
.end method

.method private aml()V
    .locals 4

    .prologue
    .line 161
    const/16 v0, 0x8

    const/4 v1, 0x5

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSd:Landroid/widget/TextView;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSe:Landroid/widget/TextView;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSf:Landroid/widget/TextView;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSg:Landroid/widget/ImageView;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSc:Landroid/widget/TextView;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/search/shared/contact/PersonSelectItem;->b(I[Landroid/view/View;)V

    .line 163
    return-void
.end method

.method private amm()V
    .locals 2

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRU:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 431
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRU:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 432
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRU:Landroid/os/AsyncTask;

    .line 434
    :cond_0
    return-void
.end method

.method private static varargs b(I[Landroid/view/View;)V
    .locals 3

    .prologue
    .line 153
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    .line 154
    if-eqz v2, :cond_0

    .line 155
    invoke-virtual {v2, p0}, Landroid/view/View;->setVisibility(I)V

    .line 153
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 158
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;Lesk;)V
    .locals 9
    .param p2    # Lesk;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 214
    invoke-super {p0, p1}, Lduc;->b(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V

    .line 216
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    .line 217
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->ajH()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_3

    move v5, v4

    .line 219
    :goto_0
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alE()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    move v3, v4

    .line 221
    :goto_1
    if-eqz v3, :cond_6

    .line 222
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alE()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->amj()Z

    move-result v6

    if-nez v6, :cond_5

    :cond_1
    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p0, v1, v0, p2}, Lcom/google/android/search/shared/contact/PersonSelectItem;->a(Lcom/google/android/search/shared/contact/Person;Ljava/util/List;Lesk;)V

    .line 237
    :cond_2
    :goto_3
    if-eqz v5, :cond_9

    move v0, v2

    :goto_4
    new-array v1, v4, [Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSc:Landroid/widget/TextView;

    aput-object v4, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/search/shared/contact/PersonSelectItem;->b(I[Landroid/view/View;)V

    .line 238
    invoke-virtual {p0, v3}, Lcom/google/android/search/shared/contact/PersonSelectItem;->eA(Z)V

    .line 239
    return-void

    :cond_3
    move v5, v2

    .line 217
    goto :goto_0

    :cond_4
    move v3, v2

    .line 219
    goto :goto_1

    .line 222
    :cond_5
    new-array v6, v4, [Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alF()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    aput-object v0, v6, v2

    invoke-static {v6}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_2

    .line 228
    :cond_6
    if-eqz v5, :cond_7

    .line 229
    iget-object v1, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSc:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a08fc

    new-array v8, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->KK()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    invoke-direct {p0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->aml()V

    goto :goto_3

    .line 232
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->akA()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 233
    invoke-direct {p0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->aml()V

    goto :goto_3

    .line 234
    :cond_8
    if-eqz p2, :cond_2

    .line 235
    invoke-interface {p2}, Lesk;->run()V

    goto :goto_3

    .line 237
    :cond_9
    const/16 v0, 0x8

    goto :goto_4
.end method

.method public final a(Lcom/google/android/search/shared/contact/Person;Ljava/util/List;Lesk;)V
    .locals 10
    .param p1    # Lcom/google/android/search/shared/contact/Person;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lesk;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    const/16 v6, 0x8

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 245
    iput-object p1, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->aYL:Lcom/google/android/search/shared/contact/Person;

    .line 246
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSd:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0c0126

    invoke-static {v0, v1, v9, v2}, Lcom/google/android/search/shared/contact/PersonSelectItem;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;ZI)V

    .line 251
    if-eqz p2, :cond_f

    .line 253
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v8, :cond_4

    .line 255
    invoke-interface {p2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSb:Lcom/google/android/search/shared/contact/Contact;

    .line 256
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSb:Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->akB()Ldzb;

    move-result-object v0

    iget-boolean v0, v0, Ldzb;->bRu:Z

    if-eqz v0, :cond_f

    .line 257
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSb:Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->alP()Ljava/lang/String;

    move-result-object v0

    .line 258
    iget-object v1, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSb:Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v1

    .line 280
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRV:Z

    if-eqz v2, :cond_0

    .line 281
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->alZ()Ljava/lang/String;

    move-result-object v0

    .line 287
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->alX()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 288
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0a098b

    new-array v3, v8, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->alW()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v9

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move v2, v8

    move-object v3, v0

    .line 293
    :goto_1
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    .line 294
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->akz()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 295
    sget-object v5, Leab;->bRb:[I

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->akB()Ldzb;

    move-result-object v0

    invoke-virtual {v0}, Ldzb;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    .line 306
    :cond_1
    :goto_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 307
    new-array v0, v8, [Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSe:Landroid/widget/TextView;

    aput-object v2, v0, v9

    invoke-static {v6, v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->b(I[Landroid/view/View;)V

    .line 308
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 309
    new-array v0, v8, [Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSf:Landroid/widget/TextView;

    aput-object v1, v0, v9

    invoke-static {v6, v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->b(I[Landroid/view/View;)V

    .line 319
    :goto_3
    iget-boolean v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRW:Z

    if-eqz v0, :cond_a

    .line 320
    new-array v0, v8, [Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSh:Landroid/view/View;

    aput-object v1, v0, v9

    invoke-static {v6, v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->b(I[Landroid/view/View;)V

    .line 328
    :goto_4
    const v0, 0x7f110132

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/QuickContactBadge;

    .line 329
    if-eqz v0, :cond_c

    .line 330
    sget-object v1, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRT:Landroid/util/LruCache;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable$ConstantState;

    if-eqz v1, :cond_b

    invoke-static {v0, v1}, Lcom/google/android/search/shared/contact/PersonSelectItem;->a(Landroid/widget/QuickContactBadge;Landroid/graphics/drawable/Drawable$ConstantState;)V

    if-eqz p3, :cond_2

    invoke-interface {p3}, Ljava/lang/Runnable;->run()V

    .line 331
    :cond_2
    :goto_5
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->getId()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/QuickContactBadge;->assignContactUri(Landroid/net/Uri;)V

    .line 333
    const v1, 0x7f0c00e6

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 334
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0725

    new-array v3, v8, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/QuickContactBadge;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 351
    :cond_3
    :goto_6
    return-void

    .line 262
    :cond_4
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 263
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Contact;

    .line 264
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v0

    .line 265
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 266
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 269
    :cond_6
    const-string v0, ", "

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 273
    iget-object v1, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSf:Landroid/widget/TextView;

    if-eqz v1, :cond_7

    move-object v1, v0

    move-object v0, v4

    .line 274
    goto/16 :goto_0

    :cond_7
    move-object v1, v4

    .line 276
    goto/16 :goto_0

    .line 297
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f0a05f5

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 300
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f0a05f8

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 311
    :cond_8
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSf:Landroid/widget/TextView;

    const v2, 0x7f0c00e7

    invoke-static {v0, v1, v9, v2}, Lcom/google/android/search/shared/contact/PersonSelectItem;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;ZI)V

    goto/16 :goto_3

    .line 315
    :cond_9
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSe:Landroid/widget/TextView;

    const v1, 0x7f0c00e8

    invoke-static {v0, v3, v2, v1}, Lcom/google/android/search/shared/contact/PersonSelectItem;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;ZI)V

    .line 316
    new-array v0, v8, [Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSf:Landroid/widget/TextView;

    aput-object v1, v0, v9

    invoke-static {v6, v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->b(I[Landroid/view/View;)V

    goto/16 :goto_3

    .line 322
    :cond_a
    new-array v0, v8, [Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSh:Landroid/view/View;

    aput-object v1, v0, v9

    invoke-static {v9, v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->b(I[Landroid/view/View;)V

    goto/16 :goto_4

    .line 330
    :cond_b
    invoke-virtual {v0}, Landroid/widget/QuickContactBadge;->setImageToDefault()V

    invoke-direct {p0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->amm()V

    new-instance v1, Leac;

    invoke-direct {v1, p0, v0, p3}, Leac;-><init>(Lcom/google/android/search/shared/contact/PersonSelectItem;Landroid/widget/QuickContactBadge;Ljava/lang/Runnable;)V

    new-array v2, v8, [Ljava/lang/Long;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-virtual {v1, v2}, Leac;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRU:Landroid/os/AsyncTask;

    goto/16 :goto_5

    .line 336
    :cond_c
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSg:Landroid/widget/ImageView;

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->getId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_d

    .line 337
    new-instance v0, Ldzz;

    iget-object v2, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->mResources:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSg:Landroid/widget/ImageView;

    iget-boolean v5, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRZ:Z

    iget-boolean v6, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSa:Z

    move-object v1, p0

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Ldzz;-><init>(Lcom/google/android/search/shared/contact/PersonSelectItem;Landroid/content/res/Resources;Landroid/widget/ImageView;Landroid/view/View;ZZLesk;)V

    new-array v1, v8, [Lcom/google/android/search/shared/contact/Person;

    aput-object p1, v1, v9

    invoke-virtual {v0, v1}, Ldzz;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_6

    .line 348
    :cond_d
    if-eqz p3, :cond_3

    .line 349
    invoke-interface {p3}, Lesk;->run()V

    goto/16 :goto_6

    :cond_e
    move v2, v9

    move-object v3, v0

    goto/16 :goto_1

    :cond_f
    move-object v0, v4

    move-object v1, v4

    goto/16 :goto_0

    .line 295
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final ahk()Z
    .locals 1

    .prologue
    .line 480
    invoke-super {p0}, Lduc;->ahk()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->akA()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ahm()[Landroid/view/View;
    .locals 4

    .prologue
    const v1, 0x7f1102a7

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 355
    invoke-virtual {p0, v1}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 356
    new-array v0, v3, [Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v2

    .line 361
    :goto_0
    return-object v0

    .line 358
    :cond_0
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->ajO()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 359
    new-array v0, v3, [Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSc:Landroid/widget/TextView;

    aput-object v1, v0, v2

    goto :goto_0

    .line 361
    :cond_1
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSg:Landroid/widget/ImageView;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSd:Landroid/widget/TextView;

    aput-object v1, v0, v3

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSe:Landroid/widget/TextView;

    aput-object v2, v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic ajg()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->aYL:Lcom/google/android/search/shared/contact/Person;

    return-object v0
.end method

.method public final synthetic b(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V
    .locals 1

    .prologue
    .line 62
    check-cast p1, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->a(Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;Lesk;)V

    return-void
.end method

.method public final c(Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;)V
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->a(Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;Lesk;)V

    .line 207
    return-void
.end method

.method protected final synthetic c(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 62
    check-cast p1, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alE()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move v1, v2

    :goto_0
    iget-boolean v4, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRY:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSg:Landroid/widget/ImageView;

    if-eqz v4, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getId()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    :goto_1
    return v2

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1
.end method

.method public final eA(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 457
    const v0, 0x7f1102a8

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 458
    if-eqz v0, :cond_0

    .line 459
    if-eqz p1, :cond_2

    .line 460
    const v2, 0x7f020282

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 464
    :goto_0
    if-eqz p1, :cond_3

    const/16 v0, 0x8

    :goto_1
    const/4 v2, 0x1

    new-array v2, v2, [Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSi:Landroid/view/View;

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lcom/google/android/search/shared/contact/PersonSelectItem;->b(I[Landroid/view/View;)V

    .line 466
    :cond_0
    const v0, 0x7f1102a9

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 467
    if-eqz v0, :cond_1

    .line 468
    if-eqz p1, :cond_4

    .line 469
    const v1, 0x7f020283

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 474
    :cond_1
    :goto_2
    return-void

    .line 462
    :cond_2
    invoke-virtual {v0, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 464
    goto :goto_1

    .line 471
    :cond_4
    invoke-virtual {v0, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method public final ez(Z)V
    .locals 0

    .prologue
    .line 191
    iput-boolean p1, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRV:Z

    .line 192
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 196
    invoke-direct {p0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->amm()V

    .line 197
    invoke-super {p0}, Lduc;->onDetachedFromWindow()V

    .line 198
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 142
    invoke-super {p0}, Lduc;->onFinishInflate()V

    .line 143
    const v0, 0x7f1100a9

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSc:Landroid/widget/TextView;

    .line 144
    const v0, 0x7f110120

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSd:Landroid/widget/TextView;

    .line 145
    const v0, 0x7f110135

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSe:Landroid/widget/TextView;

    .line 146
    const v0, 0x7f110134

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSf:Landroid/widget/TextView;

    .line 147
    const v0, 0x7f1102a4

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSg:Landroid/widget/ImageView;

    .line 148
    const v0, 0x7f11010a

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSh:Landroid/view/View;

    .line 149
    const v0, 0x7f110153

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSi:Landroid/view/View;

    .line 150
    return-void
.end method

.method public final setEditable(Z)V
    .locals 0

    .prologue
    .line 369
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 3

    .prologue
    .line 373
    invoke-super {p0, p1}, Lduc;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 374
    iget-boolean v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bRX:Z

    if-nez v0, :cond_1

    .line 382
    :cond_0
    :goto_0
    return-void

    .line 378
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSd:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSd:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0133

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 380
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonSelectItem;->bSd:Landroid/widget/TextView;

    const-string v1, "sans-serif-condensed"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0
.end method
