.class public Lcom/google/android/search/searchplate/SearchPlate;
.super Landroid/widget/RelativeLayout;
.source "PG"


# instance fields
.field private final bHC:Landroid/view/inputmethod/InputMethodManager;

.field private final bHD:Ljava/util/Set;

.field private final bHE:Ljava/lang/Runnable;

.field private final bHF:Ljava/util/Set;

.field public bHG:Lcom/google/android/search/searchplate/TextContainer;

.field private bHH:Lcom/google/android/search/searchplate/RecognizerView;

.field private bHI:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

.field private bHJ:Ldsx;

.field private bHK:Landroid/view/View;

.field private bHL:Lcom/google/android/search/searchplate/AudioProgressRenderer;

.field private bHM:Landroid/widget/ImageView;

.field private bHN:Landroid/widget/ImageView;

.field private bHO:Lcom/google/android/search/searchplate/HintTextView;

.field private bHP:Z

.field private bHQ:Landroid/animation/Animator;

.field private bHR:Landroid/animation/Animator;

.field private bHS:Landroid/animation/Animator;

.field private bHT:Ldsm;

.field private bHU:Ldsm;

.field public bHV:Ldsj;

.field private bHW:Landroid/view/View;

.field public bHX:I

.field public bHY:Ldsl;

.field public bHZ:Z

.field private bHq:Ldtc;

.field private bIa:I

.field private bIb:I

.field private bIc:I

.field private bId:I

.field private bIe:Z

.field private bIf:Z

.field private bIg:Ldqp;

.field private bIh:Landroid/animation/ValueAnimator;

.field private bIi:Z

.field private bIj:Landroid/widget/ProgressBar;

.field private bIk:Landroid/widget/FrameLayout;

.field private bIl:Z

.field private bIm:I

.field private bIn:I

.field private bIo:Z

.field private bsq:Ljava/lang/String;

.field private bss:I

.field private mDisplayText:Landroid/widget/TextView;

.field private mErrorMessage:Ljava/lang/String;

.field public mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/searchplate/SearchPlate;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 216
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/searchplate/SearchPlate;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 220
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 223
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 96
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHD:Ljava/util/Set;

    .line 97
    new-instance v0, Ldry;

    invoke-direct {v0, p0}, Ldry;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHE:Ljava/lang/Runnable;

    .line 105
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHF:Ljava/util/Set;

    .line 161
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIi:Z

    .line 173
    iput v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIm:I

    .line 179
    iput v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIn:I

    .line 224
    const-string v0, "input_method"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHC:Landroid/view/inputmethod/InputMethodManager;

    .line 225
    return-void
.end method

.method static a(ZIII)I
    .locals 0

    .prologue
    .line 1011
    if-eqz p0, :cond_0

    .line 1016
    :goto_0
    return p3

    .line 1013
    :cond_0
    if-ltz p1, :cond_1

    if-gt p1, p3, :cond_1

    move p3, p1

    .line 1014
    goto :goto_0

    :cond_1
    move p3, p2

    .line 1016
    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/search/searchplate/SearchPlate;IZ)V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/searchplate/SearchPlate;->y(IZ)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/search/searchplate/SearchPlate;)Z
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIi:Z

    return v0
.end method

.method private aff()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 755
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 756
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getId()I

    move-result v1

    .line 757
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIm:I

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 759
    :goto_0
    iget v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIn:I

    if-ne v2, v3, :cond_1

    move v2, v1

    .line 761
    :goto_1
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v3, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setNextFocusLeftId(I)V

    .line 762
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v3, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setNextFocusRightId(I)V

    .line 763
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v3, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setNextFocusUpId(I)V

    .line 764
    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v1, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->setNextFocusDownId(I)V

    .line 765
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, v2}, Lcom/google/android/search/searchplate/SimpleSearchText;->setNextFocusForwardId(I)V

    .line 773
    :goto_2
    return-void

    .line 757
    :cond_0
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIm:I

    goto :goto_0

    .line 759
    :cond_1
    iget v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIn:I

    goto :goto_1

    .line 767
    :cond_2
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->setNextFocusLeftId(I)V

    .line 768
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->setNextFocusRightId(I)V

    .line 769
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->setNextFocusUpId(I)V

    .line 770
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->setNextFocusDownId(I)V

    .line 771
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->setNextFocusForwardId(I)V

    goto :goto_2
.end method

.method private afh()V
    .locals 1

    .prologue
    .line 1260
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHZ:Z

    if-nez v0, :cond_0

    .line 1261
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHE:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->post(Ljava/lang/Runnable;)Z

    .line 1262
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHZ:Z

    .line 1264
    :cond_0
    return-void
.end method

.method private afi()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x6

    .line 1275
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    if-ne v0, v3, :cond_0

    .line 1276
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->afF()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1277
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIk:Landroid/widget/FrameLayout;

    invoke-static {v0}, Lcom/google/android/search/searchplate/SearchPlate;->aw(Landroid/view/View;)Z

    .line 1282
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHI:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    if-eq v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->afF()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->dM(Z)V

    .line 1285
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIk:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 1289
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    if-eq v0, v3, :cond_2

    .line 1291
    const v0, 0x7f0d0008

    invoke-direct {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->gk(I)I

    move-result v1

    .line 1293
    :cond_2
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIk:Landroid/widget/FrameLayout;

    invoke-static {v0, v1, v1, v1, v1}, Ldtp;->a(Landroid/view/View;IIII)V

    .line 1298
    :cond_3
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->postInvalidate()V

    .line 1299
    return-void

    .line 1279
    :cond_4
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIk:Landroid/widget/FrameLayout;

    invoke-static {v0}, Lcom/google/android/search/searchplate/SearchPlate;->ax(Landroid/view/View;)V

    goto :goto_0

    :cond_5
    move v0, v1

    .line 1282
    goto :goto_1
.end method

.method public static afj()V
    .locals 0

    .prologue
    .line 1307
    return-void
.end method

.method private av(II)V
    .locals 3

    .prologue
    .line 790
    if-nez p1, :cond_0

    and-int/lit8 v0, p2, 0x4

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHT:Ldsm;

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHN:Landroid/widget/ImageView;

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Ldsm;->n(Landroid/view/View;I)V

    .line 795
    :goto_0
    return-void

    .line 793
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHT:Ldsm;

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHN:Landroid/widget/ImageView;

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Ldsm;->n(Landroid/view/View;I)V

    goto :goto_0
.end method

.method private static aw(Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1133
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1135
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1136
    const/4 v0, 0x1

    .line 1138
    :cond_0
    return v0
.end method

.method private static ax(Landroid/view/View;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1142
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1144
    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1146
    :cond_0
    return-void
.end method

.method public static synthetic b(Lcom/google/android/search/searchplate/SearchPlate;)Ldqp;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIg:Ldqp;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/search/searchplate/SearchPlate;)Ldtc;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHq:Ldtc;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/search/searchplate/SearchPlate;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHF:Ljava/util/Set;

    return-object v0
.end method

.method private dT(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 530
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_8

    .line 531
    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 532
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHD:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 539
    if-ne v3, p1, :cond_1

    iget-object v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHW:Landroid/view/View;

    if-eq v2, v4, :cond_1

    iget-object v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHG:Lcom/google/android/search/searchplate/TextContainer;

    if-eq v2, v4, :cond_1

    .line 540
    iget-object v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHN:Landroid/widget/ImageView;

    if-ne v2, v4, :cond_3

    .line 543
    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHN:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    if-eqz v3, :cond_2

    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHN:Landroid/widget/ImageView;

    invoke-static {v2}, Lcom/google/android/search/searchplate/SearchPlate;->aw(Landroid/view/View;)Z

    .line 530
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 543
    :cond_2
    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHN:Landroid/widget/ImageView;

    invoke-static {v2}, Lcom/google/android/search/searchplate/SearchPlate;->ax(Landroid/view/View;)V

    goto :goto_1

    .line 544
    :cond_3
    iget-object v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    if-ne v2, v4, :cond_6

    .line 545
    if-eqz v3, :cond_5

    .line 546
    iget v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    .line 547
    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHT:Ldsm;

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v2, v3, v1}, Ldsm;->n(Landroid/view/View;I)V

    .line 548
    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    iget-boolean v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIe:Z

    invoke-virtual {v2, v3}, Lcom/google/android/search/searchplate/RecognizerView;->dP(Z)V

    goto :goto_1

    .line 550
    :cond_4
    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHT:Ldsm;

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    const/4 v4, 0x7

    invoke-virtual {v2, v3, v4}, Ldsm;->n(Landroid/view/View;I)V

    .line 551
    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-static {v2}, Lcom/google/android/search/searchplate/SearchPlate;->aw(Landroid/view/View;)Z

    goto :goto_1

    .line 554
    :cond_5
    invoke-static {v2}, Lcom/google/android/search/searchplate/SearchPlate;->ax(Landroid/view/View;)V

    goto :goto_1

    .line 557
    :cond_6
    if-eqz v3, :cond_7

    .line 558
    invoke-static {v2}, Lcom/google/android/search/searchplate/SearchPlate;->aw(Landroid/view/View;)Z

    goto :goto_1

    .line 560
    :cond_7
    invoke-static {v2}, Lcom/google/android/search/searchplate/SearchPlate;->ax(Landroid/view/View;)V

    goto :goto_1

    .line 564
    :cond_8
    return-void
.end method

.method private dV(Z)V
    .locals 5

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 852
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIi:Z

    if-eq v0, p1, :cond_0

    .line 853
    if-eqz p1, :cond_1

    move v0, v1

    .line 857
    :goto_0
    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    invoke-virtual {v2}, Ldsl;->afA()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHN:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_2

    .line 859
    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIh:Landroid/animation/ValueAnimator;

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    sub-float/2addr v1, v0

    aput v1, v3, v4

    const/4 v1, 0x1

    aput v0, v3, v1

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 860
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIh:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 865
    :goto_1
    if-eqz p1, :cond_3

    .line 866
    const v0, 0x7f0a002d

    .line 870
    :goto_2
    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHN:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 871
    iput-boolean p1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIi:Z

    .line 873
    :cond_0
    return-void

    .line 853
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 862
    :cond_2
    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIg:Ldqp;

    invoke-virtual {v1, v0}, Ldqp;->E(F)V

    goto :goto_1

    .line 868
    :cond_3
    const v0, 0x7f0a002e

    goto :goto_2
.end method

.method public static synthetic e(Lcom/google/android/search/searchplate/SearchPlate;)I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bss:I

    return v0
.end method

.method private gk(I)I
    .locals 1

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method private gl(I)Landroid/animation/Animator;
    .locals 3

    .prologue
    .line 457
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    .line 458
    invoke-virtual {v0, p1}, Landroid/animation/LayoutTransition;->getAnimator(I)Landroid/animation/Animator;

    move-result-object v1

    .line 459
    sget-object v2, Ldqs;->bGc:Ldqs;

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 460
    invoke-virtual {v1}, Landroid/animation/Animator;->clone()Landroid/animation/Animator;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 461
    return-object v1
.end method

.method private x(IZ)V
    .locals 1

    .prologue
    .line 588
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    invoke-static {v0}, Ldtd;->gq(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Ldtd;->gq(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 590
    const/4 v0, 0x6

    invoke-direct {p0, v0, p2}, Lcom/google/android/search/searchplate/SearchPlate;->y(IZ)V

    .line 596
    :goto_0
    return-void

    .line 594
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bss:I

    goto :goto_0
.end method

.method private y(IZ)V
    .locals 6

    .prologue
    const/4 v5, 0x7

    const/4 v2, 0x2

    const/4 v4, 0x4

    const/4 v3, 0x3

    .line 899
    if-eqz p2, :cond_c

    .line 900
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bss:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    if-eq v0, v2, :cond_2

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    if-eq v0, v3, :cond_2

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    if-eq v0, v4, :cond_2

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    if-eq v0, v5, :cond_2

    .line 908
    :cond_0
    invoke-direct {p0}, Lcom/google/android/search/searchplate/SearchPlate;->afh()V

    .line 920
    :cond_1
    :goto_0
    :pswitch_0
    return-void

    .line 915
    :cond_2
    iput p1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bss:I

    .line 916
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bss:I

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v1, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setState(I)V

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHJ:Ldsx;

    invoke-interface {v0}, Ldsx;->reset()V

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mDisplayText:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mDisplayText:Landroid/widget/TextView;

    const v1, 0x7f0a0037

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_4
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    if-eq v0, v3, :cond_3

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    if-ne v0, v4, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHL:Lcom/google/android/search/searchplate/AudioProgressRenderer;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/AudioProgressRenderer;->aeQ()V

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    if-ne v0, v2, :cond_7

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHK:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mDisplayText:Landroid/widget/TextView;

    const v1, 0x7f0a003e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHG:Lcom/google/android/search/searchplate/TextContainer;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/TextContainer;->afL()V

    goto :goto_0

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHP:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mDisplayText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0034

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/search/searchplate/SearchPlate;->bsq:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mDisplayText:Landroid/widget/TextView;

    const v1, 0x7f0a0033

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_7
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    if-ne v0, v5, :cond_1

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHJ:Ldsx;

    invoke-interface {v0}, Ldsx;->reset()V

    goto :goto_0

    :pswitch_5
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    if-eq v0, v3, :cond_8

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    if-ne v0, v4, :cond_a

    :cond_8
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0030

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/RecognizerView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHL:Lcom/google/android/search/searchplate/AudioProgressRenderer;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/AudioProgressRenderer;->aeP()V

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->mDisplayText:Landroid/widget/TextView;

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    if-ne v0, v3, :cond_9

    const v0, 0x7f0a003f

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v0, v4}, Lcom/google/android/search/searchplate/RecognizerView;->setState(I)V

    goto/16 :goto_0

    :cond_9
    const v0, 0x7f0a0040

    goto :goto_2

    :cond_a
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    if-eq v0, v2, :cond_b

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    if-ne v0, v5, :cond_1

    :cond_b
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mDisplayText:Landroid/widget/TextView;

    const v1, 0x7f0a0025

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHJ:Ldsx;

    invoke-interface {v0}, Ldsx;->reset()V

    goto/16 :goto_0

    .line 918
    :cond_c
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    invoke-virtual {v0, p1}, Ldsl;->gn(I)V

    goto/16 :goto_0

    .line 916
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(IIZZ)V
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 620
    if-eqz p4, :cond_0

    .line 621
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    invoke-virtual {v0, v2}, Ldsl;->ea(Z)V

    .line 622
    invoke-direct {p0, p1, v1}, Lcom/google/android/search/searchplate/SearchPlate;->x(IZ)V

    .line 625
    :cond_0
    if-eqz p3, :cond_e

    .line 626
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    if-ne p1, v0, :cond_1

    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_10

    .line 627
    :cond_1
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_8

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIe:Z

    .line 628
    iget v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    .line 629
    iput p1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    .line 630
    sget-object v0, Ldqs;->bGc:Ldqs;

    iget-boolean v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIf:Z

    if-eqz v4, :cond_2

    invoke-static {v3}, Ldtd;->gu(I)Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-static {p1}, Ldtd;->gr(I)Z

    move-result v4

    if-eqz v4, :cond_9

    sget-object v0, Ldqz;->bGE:Ldqz;

    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHQ:Landroid/animation/Animator;

    invoke-virtual {v3, v0}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHR:Landroid/animation/Animator;

    invoke-virtual {v3, v0}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHS:Landroid/animation/Animator;

    invoke-virtual {v3, v0}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHG:Lcom/google/android/search/searchplate/TextContainer;

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    invoke-virtual {v0}, Ldsl;->afA()Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    :goto_2
    invoke-virtual {v3, p1, v0}, Lcom/google/android/search/searchplate/TextContainer;->z(IZ)V

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIa:I

    packed-switch p1, :pswitch_data_0

    move v3, v0

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v3, v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    iget v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIa:I

    if-ne v3, v4, :cond_b

    move v3, v2

    :goto_4
    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, v2}, Lcom/google/android/search/searchplate/SimpleSearchText;->setScrollX(I)V

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, v2}, Lcom/google/android/search/searchplate/SimpleSearchText;->setScrollY(I)V

    :cond_3
    invoke-static {p1}, Ldtd;->gs(I)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x7

    if-eq p1, v0, :cond_4

    if-nez p1, :cond_c

    and-int/lit8 v0, p2, 0x4

    if-eqz v0, :cond_c

    :cond_4
    invoke-direct {p0, v2}, Lcom/google/android/search/searchplate/SearchPlate;->dV(Z)V

    :cond_5
    :goto_5
    invoke-direct {p0, p1, p2}, Lcom/google/android/search/searchplate/SearchPlate;->av(II)V

    invoke-direct {p0}, Lcom/google/android/search/searchplate/SearchPlate;->afi()V

    invoke-direct {p0}, Lcom/google/android/search/searchplate/SearchPlate;->aff()V

    invoke-static {p1}, Ldtd;->gs(I)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p0, v2}, Lcom/google/android/search/searchplate/SearchPlate;->dN(Z)V

    :cond_6
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHD:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    packed-switch p1, :pswitch_data_1

    :cond_7
    :goto_6
    invoke-direct {p0, v2}, Lcom/google/android/search/searchplate/SearchPlate;->dT(Z)V

    invoke-direct {p0, v1}, Lcom/google/android/search/searchplate/SearchPlate;->dT(Z)V

    .line 631
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHF:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldsk;

    invoke-interface {v0, p1, p4}, Ldsk;->w(IZ)V

    goto :goto_7

    :cond_8
    move v0, v2

    .line 627
    goto/16 :goto_0

    .line 630
    :cond_9
    invoke-static {v3}, Ldtd;->gr(I)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {p1}, Ldtd;->gu(I)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v0, Ldqz;->bGF:Ldqz;

    goto/16 :goto_1

    :cond_a
    move v0, v2

    goto/16 :goto_2

    :pswitch_0
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIb:I

    move v3, v0

    goto/16 :goto_3

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v0, v5}, Lcom/google/android/search/searchplate/RecognizerView;->setState(I)V

    :pswitch_2
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIb:I

    move v3, v0

    goto/16 :goto_3

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v0, v5}, Lcom/google/android/search/searchplate/RecognizerView;->setState(I)V

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIc:I

    move v3, v0

    goto/16 :goto_3

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/google/android/search/searchplate/RecognizerView;->setState(I)V

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIc:I

    move v3, v0

    goto/16 :goto_3

    :pswitch_5
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIa:I

    move v3, v0

    goto/16 :goto_3

    :cond_b
    iget v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bId:I

    goto/16 :goto_4

    :cond_c
    if-ne p1, v1, :cond_5

    invoke-direct {p0, v1}, Lcom/google/android/search/searchplate/SearchPlate;->dV(Z)V

    goto :goto_5

    :pswitch_6
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->afG()V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHM:Landroid/widget/ImageView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHO:Lcom/google/android/search/searchplate/HintTextView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIk:Landroid/widget/FrameLayout;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    and-int/lit8 v3, p2, 0x4

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHN:Landroid/widget/ImageView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    :pswitch_7
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->afG()V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHN:Landroid/widget/ImageView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIk:Landroid/widget/FrameLayout;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    :pswitch_8
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->mDisplayText:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    :pswitch_9
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHJ:Ldsx;

    invoke-interface {v3}, Ldsx;->reset()V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->mErrorMessage:Ljava/lang/String;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->mDisplayText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->mErrorMessage:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_d
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    :pswitch_a
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->mDisplayText:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHL:Lcom/google/android/search/searchplate/AudioProgressRenderer;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    :pswitch_b
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->afH()V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHN:Landroid/widget/ImageView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    :pswitch_c
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->afG()V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHN:Landroid/widget/ImageView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIk:Landroid/widget/FrameLayout;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    :pswitch_d
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->afH()V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHN:Landroid/widget/ImageView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 633
    :cond_e
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    invoke-virtual {v0}, Ldsl;->afB()I

    move-result v0

    if-ne p1, v0, :cond_f

    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_10

    .line 635
    :cond_f
    invoke-direct {p0, p1, v2}, Lcom/google/android/search/searchplate/SearchPlate;->x(IZ)V

    .line 636
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    invoke-virtual {v0, p1, p2}, Ldsl;->aw(II)V

    .line 639
    :cond_10
    if-eqz p4, :cond_11

    .line 640
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->requestLayout()V

    .line 642
    :cond_11
    return-void

    .line 630
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_a
        :pswitch_c
        :pswitch_b
        :pswitch_d
        :pswitch_9
    .end packed-switch
.end method

.method public final a(ILjava/lang/String;Z)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1190
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    and-int/lit8 v0, p1, 0x10

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/search/searchplate/RecognizerView;->dR(Z)V

    .line 1192
    if-eqz p3, :cond_b

    .line 1193
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_3

    move v3, v1

    .line 1194
    :goto_1
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHP:Z

    .line 1195
    iput-object p2, p0, Lcom/google/android/search/searchplate/SearchPlate;->bsq:Ljava/lang/String;

    .line 1197
    const/16 v0, 0x8

    if-ne p1, v0, :cond_5

    .line 1199
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0a0039

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1213
    :goto_3
    iget v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 1214
    and-int/lit16 v4, p1, 0x100

    if-eqz v4, :cond_0

    .line 1215
    iget-object v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHK:Landroid/view/View;

    invoke-static {v4}, Lcom/google/android/search/searchplate/SearchPlate;->aw(Landroid/view/View;)Z

    .line 1219
    :cond_0
    iget v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    const/4 v5, 0x7

    if-eq v4, v5, :cond_1

    .line 1220
    iget-object v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v4, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->r(Ljava/lang/CharSequence;)V

    .line 1223
    :cond_1
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_8

    move v0, v1

    .line 1224
    :goto_4
    if-eqz v0, :cond_9

    iget-object v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHO:Lcom/google/android/search/searchplate/HintTextView;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0a0035

    new-array v7, v1, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/search/searchplate/SearchPlate;->bsq:Ljava/lang/String;

    aput-object v8, v7, v2

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/search/searchplate/HintTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHO:Lcom/google/android/search/searchplate/HintTextView;

    invoke-virtual {v4}, Lcom/google/android/search/searchplate/HintTextView;->aeT()V

    .line 1225
    :goto_5
    iget-object v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHI:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v4, v3, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->q(ZZ)V

    .line 1226
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v0, v3}, Lcom/google/android/search/searchplate/RecognizerView;->dQ(Z)V

    .line 1228
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    and-int/lit8 v3, p1, 0x20

    if-eqz v3, :cond_a

    :goto_6
    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->ec(Z)V

    .line 1233
    :goto_7
    return-void

    :cond_2
    move v0, v2

    .line 1190
    goto :goto_0

    :cond_3
    move v3, v2

    .line 1193
    goto :goto_1

    :cond_4
    move v0, v2

    .line 1194
    goto :goto_2

    .line 1200
    :cond_5
    if-eqz v3, :cond_7

    .line 1203
    and-int/lit16 v0, p1, 0x200

    if-eqz v0, :cond_6

    .line 1204
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0a003c

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 1207
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0a003b

    new-array v5, v1, [Ljava/lang/Object;

    aput-object p2, v5, v2

    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 1210
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0a003d

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :cond_8
    move v0, v2

    .line 1223
    goto :goto_4

    .line 1224
    :cond_9
    iget-object v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHO:Lcom/google/android/search/searchplate/HintTextView;

    const-string v5, ""

    invoke-virtual {v4, v5}, Lcom/google/android/search/searchplate/HintTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHO:Lcom/google/android/search/searchplate/HintTextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/search/searchplate/HintTextView;->setAlpha(F)V

    goto :goto_5

    :cond_a
    move v1, v2

    .line 1228
    goto :goto_6

    .line 1231
    :cond_b
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    invoke-virtual {v0, p1, p2}, Ldsl;->j(ILjava/lang/String;)V

    goto :goto_7
.end method

.method public final a(Landroid/text/Spanned;Z)V
    .locals 1

    .prologue
    .line 1062
    if-eqz p2, :cond_0

    .line 1063
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/SimpleSearchText;->b(Landroid/text/Spanned;)V

    .line 1067
    :goto_0
    return-void

    .line 1065
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    invoke-virtual {v0, p1}, Ldsl;->a(Landroid/text/Spanned;)V

    goto :goto_0
.end method

.method public final a(Ldsj;)V
    .locals 2

    .prologue
    .line 491
    iput-object p1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHV:Ldsj;

    .line 492
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/RecognizerView;->a(Ldrw;)V

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHI:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    new-instance v1, Ldsa;

    invoke-direct {v1, p0}, Ldsa;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->b(Landroid/view/View$OnClickListener;)V

    .line 493
    return-void
.end method

.method public final a(Ldsk;)V
    .locals 1
    .param p1    # Ldsk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 880
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 881
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHF:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 882
    return-void
.end method

.method public final a(Ldta;)V
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/SimpleSearchText;->a(Ldta;)V

    .line 522
    return-void
.end method

.method public final a(Ldtb;)V
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/RecognizerView;->b(Ldtb;)V

    .line 517
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHL:Lcom/google/android/search/searchplate/AudioProgressRenderer;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/AudioProgressRenderer;->a(Ldtb;)V

    .line 518
    return-void
.end method

.method public final a(Ldtc;)V
    .locals 1

    .prologue
    .line 511
    iput-object p1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHq:Ldtc;

    .line 512
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/RecognizerView;->a(Ldtc;)V

    .line 513
    return-void
.end method

.method public final a(Ldto;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1039
    if-eqz p2, :cond_6

    .line 1041
    invoke-virtual {p1}, Ldto;->afW()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1042
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    const/4 v3, 0x7

    if-ne v0, v3, :cond_0

    .line 1059
    :goto_0
    return-void

    .line 1050
    :cond_0
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    iget v0, p1, Ldto;->cZ:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->ed(Z)V

    .line 1053
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHG:Lcom/google/android/search/searchplate/TextContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/TextContainer;->b(Ldto;)V

    .line 1054
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v4

    iget v0, p1, Ldto;->cZ:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    iget v5, p1, Ldto;->bKR:I

    invoke-virtual {v3}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v6

    invoke-static {v0, v5, v6, v4}, Lcom/google/android/search/searchplate/SearchPlate;->a(ZIII)I

    move-result v0

    iget v5, p1, Ldto;->cZ:I

    and-int/lit8 v5, v5, 0x10

    if-eqz v5, :cond_5

    :goto_4
    iget v2, p1, Ldto;->bKS:I

    invoke-virtual {v3}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v5

    invoke-static {v1, v2, v5, v4}, Lcom/google/android/search/searchplate/SearchPlate;->a(ZIII)I

    move-result v1

    invoke-virtual {v3, v0, v1}, Landroid/widget/EditText;->setSelection(II)V

    .line 1055
    invoke-direct {p0}, Lcom/google/android/search/searchplate/SearchPlate;->afi()V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1050
    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    .line 1054
    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_4

    .line 1057
    :cond_6
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    invoke-virtual {v0, p1}, Ldsl;->a(Ldto;)V

    goto :goto_0
.end method

.method public final afg()Z
    .locals 1

    .prologue
    .line 885
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    invoke-virtual {v0}, Ldsl;->isRunning()Z

    move-result v0

    return v0
.end method

.method public final au(II)V
    .locals 0

    .prologue
    .line 749
    iput p1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIm:I

    .line 750
    iput p2, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIn:I

    .line 751
    invoke-direct {p0}, Lcom/google/android/search/searchplate/SearchPlate;->aff()V

    .line 752
    return-void
.end method

.method public final c(IIZ)V
    .locals 0

    .prologue
    .line 599
    invoke-virtual {p0, p1, p2, p3, p3}, Lcom/google/android/search/searchplate/SearchPlate;->a(IIZZ)V

    .line 600
    return-void
.end method

.method public final dN(Z)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-wide/16 v2, 0x64

    .line 1102
    if-eqz p1, :cond_0

    .line 1104
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_5

    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIo:Z

    if-nez v0, :cond_5

    .line 1106
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v0, v5}, Lcom/google/android/search/searchplate/RecognizerView;->dS(Z)V

    .line 1107
    iput-boolean v5, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIo:Z

    .line 1119
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIl:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIo:Z

    if-eqz v0, :cond_2

    .line 1121
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v0, v4}, Lcom/google/android/search/searchplate/RecognizerView;->dS(Z)V

    .line 1122
    iput-boolean v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIo:Z

    .line 1124
    :cond_2
    if-eqz p1, :cond_3

    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIo:Z

    if-eqz v0, :cond_4

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIl:Z

    if-eqz v0, :cond_4

    .line 1126
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIj:Landroid/widget/ProgressBar;

    invoke-static {v0, v6}, Ldtp;->p(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 1127
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHI:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-static {v0}, Ldtp;->aA(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 1128
    iput-boolean v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIl:Z

    .line 1130
    :cond_4
    return-void

    .line 1108
    :cond_5
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIl:Z

    if-nez v0, :cond_0

    .line 1110
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHI:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-static {v0, v6}, Ldtp;->p(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 1112
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIj:Landroid/widget/ProgressBar;

    invoke-static {v0}, Ldtp;->aA(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 1113
    iput-boolean v5, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIl:Z

    goto :goto_0
.end method

.method public final dU(Z)V
    .locals 0

    .prologue
    .line 652
    iput-boolean p1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIf:Z

    .line 653
    return-void
.end method

.method public final dW(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1070
    if-eqz p1, :cond_0

    .line 1071
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->requestFocus()Z

    .line 1072
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHW:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 1073
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHW:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 1074
    invoke-direct {p0}, Lcom/google/android/search/searchplate/SearchPlate;->afi()V

    .line 1075
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHC:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 1080
    :goto_0
    return-void

    .line 1078
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    invoke-virtual {v0}, Ldsl;->afz()V

    goto :goto_0
.end method

.method public final dX(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1083
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHW:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1086
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHW:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 1087
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHW:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 1088
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHW:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1091
    :cond_0
    if-eqz p1, :cond_1

    .line 1092
    invoke-direct {p0}, Lcom/google/android/search/searchplate/SearchPlate;->afi()V

    .line 1093
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHC:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1099
    :goto_0
    return-void

    .line 1097
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    invoke-virtual {v0}, Ldsl;->afy()V

    goto :goto_0
.end method

.method public final en(I)V
    .locals 1

    .prologue
    .line 889
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/searchplate/SearchPlate;->y(IZ)V

    .line 890
    return-void
.end method

.method public focusableViewAvailable(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    if-eq p1, v0, :cond_0

    .line 486
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->focusableViewAvailable(Landroid/view/View;)V

    .line 488
    :cond_0
    return-void
.end method

.method public final kg(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 579
    iput-object p1, p0, Lcom/google/android/search/searchplate/SearchPlate;->mErrorMessage:Ljava/lang/String;

    .line 580
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v1, v1}, Lcom/google/android/search/searchplate/SearchPlate;->c(IIZ)V

    .line 581
    return-void
.end method

.method public final n(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 991
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHG:Lcom/google/android/search/searchplate/TextContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/TextContainer;->n(Ljava/lang/CharSequence;)V

    .line 992
    return-void
.end method

.method public onFinishInflate()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 233
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 235
    const v0, 0x7f1101ae

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/searchplate/TextContainer;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHG:Lcom/google/android/search/searchplate/TextContainer;

    .line 236
    const v0, 0x7f1103c1

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/searchplate/SimpleSearchText;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    .line 237
    const v0, 0x7f1103be

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHI:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    .line 238
    const v0, 0x7f1103bc

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIk:Landroid/widget/FrameLayout;

    .line 239
    const v0, 0x7f11022f

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/searchplate/RecognizerView;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    .line 240
    const v0, 0x7f1103c3

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mDisplayText:Landroid/widget/TextView;

    .line 241
    const v0, 0x7f1103c4

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldsx;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHJ:Ldsx;

    .line 242
    const v0, 0x7f1103c9

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHK:Landroid/view/View;

    .line 243
    const v0, 0x7f110376

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/searchplate/AudioProgressRenderer;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHL:Lcom/google/android/search/searchplate/AudioProgressRenderer;

    .line 245
    const v0, 0x7f1103c8

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/searchplate/HintTextView;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHO:Lcom/google/android/search/searchplate/HintTextView;

    .line 246
    const v0, 0x7f1103bd

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIj:Landroid/widget/ProgressBar;

    .line 247
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 248
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIj:Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0024

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminateTintList(Landroid/content/res/ColorStateList;)V

    .line 250
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIj:Landroid/widget/ProgressBar;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminateTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 253
    :cond_0
    new-instance v0, Ldsl;

    invoke-direct {v0, p0, p0}, Ldsl;-><init>(Lcom/google/android/search/searchplate/SearchPlate;Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    .line 255
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHG:Lcom/google/android/search/searchplate/TextContainer;

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->b(Landroid/animation/Animator$AnimatorListener;)V

    .line 257
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.microphone"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 258
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHI:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->aeR()V

    .line 261
    :cond_1
    const v0, 0x7f1103c5

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHM:Landroid/widget/ImageView;

    .line 262
    const v0, 0x7f1103bf

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHN:Landroid/widget/ImageView;

    .line 264
    const v0, 0x7f1103c0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHW:Landroid/view/View;

    .line 265
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHW:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/search/searchplate/SearchPlate;->aw(Landroid/view/View;)Z

    .line 267
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    invoke-virtual {v0, v3}, Ldsl;->ea(Z)V

    .line 268
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v3, v8, v9}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 269
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v5, v8, v9}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 270
    new-instance v0, Ldsm;

    invoke-direct {v0, v3}, Ldsm;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHT:Ldsm;

    .line 271
    new-instance v0, Ldsm;

    invoke-direct {v0, v4}, Ldsm;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHU:Ldsm;

    .line 272
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    invoke-direct {p0, v0, v4}, Lcom/google/android/search/searchplate/SearchPlate;->av(II)V

    .line 273
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHU:Ldsm;

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHN:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v6}, Ldsm;->n(Landroid/view/View;I)V

    .line 274
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHT:Ldsm;

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHH:Lcom/google/android/search/searchplate/RecognizerView;

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Ldsm;->n(Landroid/view/View;I)V

    .line 275
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHT:Ldsm;

    invoke-virtual {v0, v5, v1}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 276
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHU:Ldsm;

    invoke-virtual {v0, v1, v2}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 284
    invoke-direct {p0, v4}, Lcom/google/android/search/searchplate/SearchPlate;->gl(I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHQ:Landroid/animation/Animator;

    .line 286
    invoke-direct {p0, v3}, Lcom/google/android/search/searchplate/SearchPlate;->gl(I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHR:Landroid/animation/Animator;

    .line 288
    invoke-direct {p0, v6}, Lcom/google/android/search/searchplate/SearchPlate;->gl(I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHS:Landroid/animation/Animator;

    .line 290
    const v0, 0x7f0d000c

    invoke-direct {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->gk(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIa:I

    .line 291
    const v0, 0x7f0d000a

    invoke-direct {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->gk(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIb:I

    .line 292
    const v0, 0x7f0d000b

    invoke-direct {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->gk(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIc:I

    .line 293
    const v0, 0x7f0d0029

    invoke-direct {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->gk(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bId:I

    .line 296
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHN:Landroid/widget/ImageView;

    new-instance v1, Ldsb;

    invoke-direct {v1, p0}, Ldsb;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 309
    new-instance v0, Ldqp;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0021

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Ldqp;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIg:Ldqp;

    .line 311
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHN:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIg:Ldqp;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 314
    new-array v0, v5, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIh:Landroid/animation/ValueAnimator;

    .line 315
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIh:Landroid/animation/ValueAnimator;

    new-instance v1, Ldsc;

    invoke-direct {v1, p0}, Ldsc;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 321
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIh:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 322
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIh:Landroid/animation/ValueAnimator;

    sget-object v1, Ldqs;->bGc:Ldqs;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 328
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    new-instance v1, Ldsd;

    invoke-direct {v1, p0}, Ldsd;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->a(Ldtt;)V

    .line 390
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    new-instance v1, Ldse;

    invoke-direct {v1, p0}, Ldse;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 399
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    new-instance v1, Ldsf;

    invoke-direct {v1, p0}, Ldsf;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 410
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mSearchBox:Lcom/google/android/search/searchplate/SimpleSearchText;

    new-instance v1, Ldsg;

    invoke-direct {v1, p0}, Ldsg;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 423
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHI:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    new-instance v1, Ldsh;

    invoke-direct {v1, p0}, Ldsh;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->a(Landroid/view/View$OnClickListener;)V

    .line 432
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bIk:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHI:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 434
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHK:Landroid/view/View;

    new-instance v1, Ldsi;

    invoke-direct {v1, p0}, Ldsi;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 443
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHM:Landroid/widget/ImageView;

    new-instance v1, Ldrz;

    invoke-direct {v1, p0}, Ldrz;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 454
    return-void

    .line 314
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 1240
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1241
    const-class v0, Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 1242
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 1250
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 1251
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    invoke-virtual {v0}, Ldsl;->afA()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1252
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldsl;->ea(Z)V

    .line 1254
    :cond_0
    invoke-direct {p0}, Lcom/google/android/search/searchplate/SearchPlate;->afh()V

    .line 1255
    return-void
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHW:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHW:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    move-result v0

    .line 479
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1310
    const-string v0, "search_plate:recognition_state"

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    invoke-virtual {v1}, Ldsl;->afC()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1311
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHY:Ldsl;

    invoke-virtual {v0}, Ldsl;->afB()I

    move-result v0

    .line 1312
    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 1313
    const-string v1, "search_plate:search_plate_error"

    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->mErrorMessage:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1317
    :cond_0
    const-string v1, "search_plate:search_plate_mode"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1319
    return-void
.end method

.method public final x(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1322
    const-string v0, "search_plate:search_plate_error"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1323
    const-string v0, "search_plate:search_plate_error"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->mErrorMessage:Ljava/lang/String;

    .line 1325
    :cond_0
    const-string v0, "search_plate:search_plate_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1326
    const-string v0, "search_plate:search_plate_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1327
    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/search/searchplate/SearchPlate;->c(IIZ)V

    .line 1329
    :cond_1
    const-string v0, "search_plate:recognition_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1332
    const-string v0, "search_plate:recognition_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/search/searchplate/SearchPlate;->y(IZ)V

    .line 1334
    :cond_2
    return-void
.end method

.method public final x(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 995
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->bHG:Lcom/google/android/search/searchplate/TextContainer;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/search/searchplate/TextContainer;->x(Ljava/lang/String;Ljava/lang/String;)V

    .line 996
    return-void
.end method
