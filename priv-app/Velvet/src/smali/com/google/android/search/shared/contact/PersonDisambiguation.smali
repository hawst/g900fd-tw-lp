.class public Lcom/google/android/search/shared/contact/PersonDisambiguation;
.super Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private bRO:Ldzb;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bRP:Ljava/lang/String;

.field private bRQ:Lcom/google/android/search/shared/contact/RelationshipStatus;

.field private bRR:Lcom/google/android/search/shared/contact/PersonShortcutKey;

.field private bRS:Lcom/google/android/search/shared/contact/PersonShortcut;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 222
    new-instance v0, Ldzj;

    invoke-direct {v0}, Ldzj;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;-><init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 56
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRO:Ldzb;

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRP:Ljava/lang/String;

    .line 58
    const-class v0, Lcom/google/android/search/shared/contact/Relationship;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/RelationshipStatus;

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRQ:Lcom/google/android/search/shared/contact/RelationshipStatus;

    .line 59
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonShortcutKey;

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRR:Lcom/google/android/search/shared/contact/PersonShortcutKey;

    .line 60
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonShortcut;

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRS:Lcom/google/android/search/shared/contact/PersonShortcut;

    .line 61
    return-void

    .line 56
    :cond_0
    invoke-static {v0}, Ldzb;->valueOf(Ljava/lang/String;)Ldzb;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p1, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRO:Ldzb;

    invoke-direct {p0, v0, p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;-><init>(Ldzb;Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Ldzb;Lcom/google/android/search/shared/contact/PersonDisambiguation;)V
    .locals 1
    .param p1    # Ldzb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 41
    invoke-direct {p0, p2}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;-><init>(Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;)V

    .line 42
    iput-object p1, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRO:Ldzb;

    .line 43
    iget-object v0, p2, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRP:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRP:Ljava/lang/String;

    .line 44
    iget-object v0, p2, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRQ:Lcom/google/android/search/shared/contact/RelationshipStatus;

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRQ:Lcom/google/android/search/shared/contact/RelationshipStatus;

    .line 45
    iget-object v0, p2, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRR:Lcom/google/android/search/shared/contact/PersonShortcutKey;

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRR:Lcom/google/android/search/shared/contact/PersonShortcutKey;

    .line 46
    iget-object v0, p2, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRS:Lcom/google/android/search/shared/contact/PersonShortcut;

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRS:Lcom/google/android/search/shared/contact/PersonShortcut;

    .line 47
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;ZLdzb;)V
    .locals 1
    .param p4    # Ldzb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;-><init>(Ljava/lang/String;Ljava/util/List;Z)V

    .line 33
    iput-object p4, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRO:Ldzb;

    .line 34
    new-instance v0, Lcom/google/android/search/shared/contact/RelationshipStatus;

    invoke-direct {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRQ:Lcom/google/android/search/shared/contact/RelationshipStatus;

    .line 37
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alt()V

    .line 38
    return-void
.end method

.method public static a(Lcom/google/android/search/shared/contact/PersonDisambiguation;Lcom/google/android/search/shared/contact/PersonDisambiguation;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 236
    invoke-static {p0, p1}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->a(Lcom/google/android/search/shared/actions/utils/Disambiguation;Lcom/google/android/search/shared/actions/utils/Disambiguation;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 245
    :cond_0
    :goto_0
    return v0

    .line 239
    :cond_1
    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    .line 240
    iget-object v1, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRQ:Lcom/google/android/search/shared/contact/RelationshipStatus;

    iget-object v2, p1, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRQ:Lcom/google/android/search/shared/contact/RelationshipStatus;

    invoke-static {v1, v2}, Lcom/google/android/search/shared/contact/RelationshipStatus;->a(Lcom/google/android/search/shared/contact/RelationshipStatus;Lcom/google/android/search/shared/contact/RelationshipStatus;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 245
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private i(Lcom/google/android/search/shared/contact/Person;)Ljava/util/List;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRO:Ldzb;

    invoke-virtual {p1, v0}, Lcom/google/android/search/shared/contact/Person;->d(Ldzb;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final KK()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRP:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ldyv;Z)Ljoq;
    .locals 7
    .param p1    # Ldyv;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x1

    .line 158
    if-nez p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ajO()Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    new-instance v0, Ljoq;

    invoke-direct {v0}, Ljoq;-><init>()V

    invoke-virtual {v0, v6}, Ljoq;->iO(Z)Ljoq;

    move-result-object v0

    .line 201
    :goto_0
    return-object v0

    .line 162
    :cond_0
    new-instance v3, Ljoq;

    invoke-direct {v3}, Ljoq;-><init>()V

    .line 163
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRP:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bLI:Ljava/lang/String;

    :goto_1
    invoke-virtual {v3, v0}, Ljoq;->xN(Ljava/lang/String;)Ljoq;

    .line 165
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 166
    new-array v4, v6, [Ljoj;

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isCompleted()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->amj()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alF()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/contact/Contact;

    :cond_1
    invoke-virtual {v0, p1, v1}, Lcom/google/android/search/shared/contact/Person;->a(Ldyv;Lcom/google/android/search/shared/contact/Contact;)Ljoj;

    move-result-object v0

    aput-object v0, v4, v2

    iput-object v4, v3, Ljoq;->ewL:[Ljoj;

    .line 180
    :cond_2
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRQ:Lcom/google/android/search/shared/contact/RelationshipStatus;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->ama()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 181
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRQ:Lcom/google/android/search/shared/contact/RelationshipStatus;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amv()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 182
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Ljoq;->ra(I)Ljoq;

    .line 192
    :goto_2
    invoke-virtual {v3, v6}, Ljoq;->iP(Z)Ljoq;

    .line 193
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRQ:Lcom/google/android/search/shared/contact/RelationshipStatus;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amt()Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Relationship;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljoq;->xO(Ljava/lang/String;)Ljoq;

    .line 195
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRQ:Lcom/google/android/search/shared/contact/RelationshipStatus;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amt()Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Relationship;->amp()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljoq;->xP(Ljava/lang/String;)Ljoq;

    :cond_3
    move-object v0, v3

    .line 201
    goto :goto_0

    .line 163
    :cond_4
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRP:Ljava/lang/String;

    goto :goto_1

    .line 171
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isOngoing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 172
    iget-object v4, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    .line 173
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljoj;

    iput-object v0, v3, Ljoq;->ewL:[Ljoj;

    .line 174
    :goto_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 175
    iget-object v5, v3, Ljoq;->ewL:[Ljoj;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/search/shared/contact/Person;->a(Ldyv;Lcom/google/android/search/shared/contact/Contact;)Ljoj;

    move-result-object v0

    aput-object v0, v5, v2

    .line 174
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 184
    :cond_6
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRQ:Lcom/google/android/search/shared/contact/RelationshipStatus;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amx()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 185
    const/4 v0, 0x3

    invoke-virtual {v3, v0}, Ljoq;->ra(I)Ljoq;

    goto :goto_2

    .line 188
    :cond_7
    invoke-virtual {v3, v6}, Ljoq;->ra(I)Ljoq;

    goto :goto_2
.end method

.method public final a(Lcom/google/android/search/shared/contact/RelationshipStatus;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRQ:Lcom/google/android/search/shared/contact/RelationshipStatus;

    .line 92
    return-void
.end method

.method public final a(Ldzb;)V
    .locals 1
    .param p1    # Ldzb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRO:Ldzb;

    if-ne p1, v0, :cond_0

    .line 103
    :goto_0
    return-void

    .line 98
    :cond_0
    iput-object p1, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRO:Ldzb;

    .line 100
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alG()Z

    .line 101
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alt()V

    .line 102
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->PM()V

    goto :goto_0
.end method

.method public final akB()Ldzb;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRO:Ldzb;

    return-object v0
.end method

.method public final amf()Lcom/google/android/search/shared/contact/PersonDisambiguation;
    .locals 3

    .prologue
    .line 65
    new-instance v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-direct {v0, p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    .line 66
    new-instance v1, Lcom/google/android/search/shared/contact/RelationshipStatus;

    iget-object v2, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRQ:Lcom/google/android/search/shared/contact/RelationshipStatus;

    invoke-direct {v1, v2}, Lcom/google/android/search/shared/contact/RelationshipStatus;-><init>(Lcom/google/android/search/shared/contact/RelationshipStatus;)V

    iput-object v1, v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRQ:Lcom/google/android/search/shared/contact/RelationshipStatus;

    .line 67
    return-object v0
.end method

.method public final amg()Lcom/google/android/search/shared/contact/PersonShortcutKey;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRR:Lcom/google/android/search/shared/contact/PersonShortcutKey;

    return-object v0
.end method

.method public final amh()Lcom/google/android/search/shared/contact/PersonShortcut;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRS:Lcom/google/android/search/shared/contact/PersonShortcut;

    return-object v0
.end method

.method public final ami()Lcom/google/android/search/shared/contact/RelationshipStatus;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRQ:Lcom/google/android/search/shared/contact/RelationshipStatus;

    return-object v0
.end method

.method protected final amj()Z
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRO:Ldzb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRO:Ldzb;

    sget-object v1, Ldzb;->bRt:Ldzb;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final amk()I
    .locals 4

    .prologue
    .line 140
    const/4 v0, 0x0

    .line 141
    iget-object v1, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 142
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->amj()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 143
    invoke-direct {p0, v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->i(Lcom/google/android/search/shared/contact/Person;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    .line 145
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 147
    goto :goto_0

    .line 148
    :cond_1
    return v1
.end method

.method public final b(Lcom/google/android/search/shared/contact/PersonShortcut;)V
    .locals 0
    .param p1    # Lcom/google/android/search/shared/contact/PersonShortcut;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 83
    iput-object p1, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRS:Lcom/google/android/search/shared/contact/PersonShortcut;

    .line 84
    return-void
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->amf()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lcom/google/android/search/shared/contact/PersonShortcutKey;)V
    .locals 0
    .param p1    # Lcom/google/android/search/shared/contact/PersonShortcutKey;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRR:Lcom/google/android/search/shared/contact/PersonShortcutKey;

    .line 76
    return-void
.end method

.method protected final synthetic d(Landroid/os/Parcelable;)Z
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->amj()Z

    move-result v0

    return v0
.end method

.method protected final synthetic e(Landroid/os/Parcelable;)Ljava/util/List;
    .locals 1

    .prologue
    .line 22
    check-cast p1, Lcom/google/android/search/shared/contact/Person;

    invoke-direct {p0, p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->i(Lcom/google/android/search/shared/contact/Person;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final isCompleted()Z
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRO:Ldzb;

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->isCompleted()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isOngoing()Z
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRO:Ldzb;

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->isOngoing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final kE(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRP:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : LookupName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRP:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : RelationshipStatus = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRQ:Lcom/google/android/search/shared/contact/RelationshipStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : AppliedPersonShortcut = [ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRS:Lcom/google/android/search/shared/contact/PersonShortcut;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 213
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/utils/TwoStepDisambiguation;->writeToParcel(Landroid/os/Parcel;I)V

    .line 214
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRO:Ldzb;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRP:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRQ:Lcom/google/android/search/shared/contact/RelationshipStatus;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 217
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRR:Lcom/google/android/search/shared/contact/PersonShortcutKey;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 218
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRS:Lcom/google/android/search/shared/contact/PersonShortcut;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 219
    return-void

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonDisambiguation;->bRO:Ldzb;

    invoke-virtual {v0}, Ldzb;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
