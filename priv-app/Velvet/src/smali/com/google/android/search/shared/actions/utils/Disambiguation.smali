.class public Lcom/google/android/search/shared/actions/utils/Disambiguation;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public bLI:Ljava/lang/String;

.field private bLW:Ldyc;

.field public bQr:Ljava/util/List;

.field private bQs:Landroid/os/Parcelable;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field bQt:Z

.field private bQu:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 395
    new-instance v0, Ldyb;

    invoke-direct {v0}, Ldyb;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v3

    .line 102
    if-eqz v3, :cond_0

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    .line 104
    array-length v4, v3

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 105
    iget-object v6, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_0
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQs:Landroid/os/Parcelable;

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQt:Z

    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQu:Z

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bLI:Ljava/lang/String;

    .line 112
    return-void

    :cond_1
    move v0, v2

    .line 109
    goto :goto_1

    :cond_2
    move v1, v2

    .line 110
    goto :goto_2
.end method

.method public constructor <init>(Lcom/google/android/search/shared/actions/utils/Disambiguation;)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iget-object v0, p1, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bLI:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bLI:Ljava/lang/String;

    .line 93
    iget-object v0, p1, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    .line 94
    iget-object v0, p1, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQs:Landroid/os/Parcelable;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQs:Landroid/os/Parcelable;

    .line 95
    iget-boolean v0, p1, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQt:Z

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQt:Z

    .line 96
    iget-boolean v0, p1, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQu:Z

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQu:Z

    .line 97
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 2

    .prologue
    .line 71
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/search/shared/actions/utils/Disambiguation;-><init>(Ljava/lang/String;Ljava/util/List;ZZ)V

    .line 72
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/util/List;ZZ)V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object p1, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bLI:Ljava/lang/String;

    .line 82
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    .line 83
    iput-boolean p3, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQt:Z

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQu:Z

    .line 86
    if-eqz p4, :cond_0

    .line 87
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alt()V

    .line 89
    :cond_0
    return-void
.end method

.method public static a(Lcom/google/android/search/shared/actions/utils/Disambiguation;Lcom/google/android/search/shared/actions/utils/Disambiguation;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 363
    if-eqz p0, :cond_0

    if-nez p1, :cond_4

    .line 364
    :cond_0
    if-nez p0, :cond_1

    move v3, v0

    :goto_0
    if-nez p1, :cond_2

    move v2, v0

    :goto_1
    if-ne v3, v2, :cond_3

    .line 371
    :goto_2
    return v0

    :cond_1
    move v3, v1

    .line 364
    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    .line 371
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->d(Lcom/google/android/search/shared/actions/utils/Disambiguation;)Z

    move-result v0

    goto :goto_2
.end method

.method public static c(Lcom/google/android/search/shared/actions/utils/Disambiguation;)Z
    .locals 1
    .param p0    # Lcom/google/android/search/shared/actions/utils/Disambiguation;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 299
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->isCompleted()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final PM()V
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bLW:Ldyc;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bLW:Ldyc;

    invoke-interface {v0, p0}, Ldyc;->a(Lcom/google/android/search/shared/actions/utils/Disambiguation;)V

    .line 310
    :cond_0
    return-void
.end method

.method protected a(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 212
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 216
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/os/Parcelable;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    .line 218
    :cond_0
    iput-object p1, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQs:Landroid/os/Parcelable;

    .line 219
    return-void
.end method

.method public final a(Landroid/os/Parcelable;Z)V
    .locals 1

    .prologue
    .line 200
    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->a(Landroid/os/Parcelable;)V

    .line 201
    if-eqz p2, :cond_0

    .line 202
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQt:Z

    .line 204
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->PM()V

    .line 205
    return-void
.end method

.method public final ajO()Z
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final alA()Z
    .locals 1

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->ajO()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQu:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final alB()Z
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQs:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final als()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 128
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 131
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bLI:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    array-length v7, v6

    move v4, v3

    move v1, v2

    :goto_0
    if-ge v4, v7, :cond_2

    aget-char v0, v6, v4

    .line 132
    invoke-static {v0}, Ljava/lang/Character;->isLetter(C)Z

    move-result v8

    if-nez v8, :cond_1

    .line 133
    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    move v1, v2

    .line 139
    :cond_0
    :goto_1
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 131
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 135
    :cond_1
    if-eqz v1, :cond_0

    .line 136
    invoke-static {v0}, Ljava/lang/Character;->toTitleCase(C)C

    move-result v0

    move v1, v3

    .line 137
    goto :goto_1

    .line 141
    :cond_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public alt()V
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQs:Landroid/os/Parcelable;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQu:Z

    if-nez v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->a(Landroid/os/Parcelable;)V

    .line 167
    :cond_0
    return-void
.end method

.method public final alu()Ljava/util/List;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    return-object v0
.end method

.method public final alv()Z
    .locals 1

    .prologue
    .line 181
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQu:Z

    return v0
.end method

.method public final alw()V
    .locals 1

    .prologue
    .line 226
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alx()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alt()V

    .line 228
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQt:Z

    .line 229
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->PM()V

    .line 231
    :cond_0
    return-void
.end method

.method protected alx()Z
    .locals 1

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->aly()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQs:Landroid/os/Parcelable;

    .line 236
    const/4 v0, 0x1

    .line 238
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aly()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 246
    iget-object v1, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQs:Landroid/os/Parcelable;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v0, :cond_0

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQu:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final alz()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQs:Landroid/os/Parcelable;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method

.method public final b(Ldyc;)V
    .locals 0

    .prologue
    .line 303
    iput-object p1, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bLW:Ldyc;

    .line 304
    return-void
.end method

.method public final c(Ljava/util/List;Z)V
    .locals 1

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    .line 153
    if-eqz p2, :cond_0

    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQt:Z

    .line 155
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alt()V

    .line 157
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->PM()V

    .line 158
    return-void
.end method

.method public d(Lcom/google/android/search/shared/actions/utils/Disambiguation;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 336
    iget-object v2, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQs:Landroid/os/Parcelable;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQs:Landroid/os/Parcelable;

    iget-object v3, p1, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQs:Landroid/os/Parcelable;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 355
    :cond_0
    :goto_0
    return v0

    .line 344
    :cond_1
    iget-object v2, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    iget-object v3, p1, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    invoke-static {v2, v3}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 349
    goto :goto_0

    .line 353
    :cond_2
    iget-object v2, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQs:Landroid/os/Parcelable;

    if-nez v2, :cond_3

    iget-object v2, p1, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQs:Landroid/os/Parcelable;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 380
    const/4 v0, 0x0

    return v0
.end method

.method public final ew(Z)V
    .locals 0

    .prologue
    .line 189
    iput-boolean p1, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQu:Z

    .line 190
    return-void
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bLI:Ljava/lang/String;

    return-object v0
.end method

.method public isCompleted()Z
    .locals 1

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alB()Z

    move-result v0

    return v0
.end method

.method public final isInteractive()Z
    .locals 1

    .prologue
    .line 316
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQt:Z

    return v0
.end method

.method public isOngoing()Z
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQs:Landroid/os/Parcelable;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 325
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Disambiguation : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bLI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : Candidates = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : Result = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQs:Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : Interactive = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQt:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : AreCandidatesSuggestions = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQu:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 385
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 388
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQs:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 389
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQt:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 390
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQu:Z

    if-eqz v0, :cond_2

    :goto_2
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 391
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bLI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 392
    return-void

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/search/shared/actions/utils/Disambiguation;->bQr:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Landroid/os/Parcelable;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 389
    goto :goto_1

    :cond_2
    move v1, v2

    .line 390
    goto :goto_2
.end method
