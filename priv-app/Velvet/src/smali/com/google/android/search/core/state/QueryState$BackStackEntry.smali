.class public Lcom/google/android/search/core/state/QueryState$BackStackEntry;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final bsa:Lcom/google/android/velvet/ActionData;

.field final bsb:Ljava/util/List;

.field final bsc:Landroid/os/Bundle;

.field bsd:Lcmq;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final query:Lcom/google/android/shared/search/Query;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3188
    new-instance v0, Ldcl;

    invoke-direct {v0}, Ldcl;-><init>()V

    sput-object v0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;Ljava/util/List;Landroid/os/Bundle;Lcmq;)V
    .locals 1
    .param p5    # Lcmq;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3154
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->query:Lcom/google/android/shared/search/Query;

    .line 3155
    iput-object p2, p0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsa:Lcom/google/android/velvet/ActionData;

    .line 3156
    iput-object p3, p0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsb:Ljava/util/List;

    .line 3157
    iput-object p5, p0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsd:Lcmq;

    .line 3158
    iput-object p4, p0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsc:Landroid/os/Bundle;

    .line 3159
    return-void
.end method

.method static synthetic a(Lcom/google/android/search/core/state/QueryState$BackStackEntry;)Z
    .locals 1

    .prologue
    .line 3140
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsa:Lcom/google/android/velvet/ActionData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 3176
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3171
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->query:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsa:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 3181
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->query:Lcom/google/android/shared/search/Query;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 3182
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsa:Lcom/google/android/velvet/ActionData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 3183
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsb:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 3184
    iget-object v0, p0, Lcom/google/android/search/core/state/QueryState$BackStackEntry;->bsc:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 3185
    return-void
.end method
