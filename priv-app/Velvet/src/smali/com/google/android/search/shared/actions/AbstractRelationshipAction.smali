.class public abstract Lcom/google/android/search/shared/actions/AbstractRelationshipAction;
.super Lcom/google/android/search/shared/actions/CommunicationAction;
.source "PG"


# instance fields
.field private bKZ:Z


# direct methods
.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/CommunicationAction;-><init>(Landroid/os/Parcel;)V

    .line 26
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/AbstractRelationshipAction;->bKZ:Z

    .line 27
    return-void

    .line 26
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/CommunicationAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    .line 22
    return-void
.end method


# virtual methods
.method public final Wb()Z
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractRelationshipAction;->bLV:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/AbstractRelationshipAction;->bLV:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->isOngoing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Letj;)V
    .locals 3

    .prologue
    .line 59
    const-string v0, "AbstractRelationshipAction"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 60
    const-string v0, "recipient"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/AbstractRelationshipAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 61
    return-void
.end method

.method public final afZ()Ldzb;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Ldzb;->bRt:Ldzb;

    return-object v0
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    return v0
.end method

.method public final eh(Z)V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/AbstractRelationshipAction;->bKZ:Z

    .line 41
    return-void
.end method

.method public final isCompleted()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/AbstractRelationshipAction;->bKZ:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AbstractRelationshipAction[recipient="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/AbstractRelationshipAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/CommunicationAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 66
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/AbstractRelationshipAction;->bKZ:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 67
    return-void

    .line 66
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
