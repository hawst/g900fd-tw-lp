.class public Lcom/google/android/search/shared/actions/ButtonAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# static fields
.field public static final bLT:Lcom/google/android/search/shared/actions/ButtonAction;

.field public static final bLU:Lcom/google/android/search/shared/actions/ButtonAction;


# instance fields
.field private final aD:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/android/search/shared/actions/ButtonAction;

    const-string v1, "Cancel"

    invoke-direct {v0, v1}, Lcom/google/android/search/shared/actions/ButtonAction;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/search/shared/actions/ButtonAction;->bLT:Lcom/google/android/search/shared/actions/ButtonAction;

    .line 13
    new-instance v0, Lcom/google/android/search/shared/actions/ButtonAction;

    const-string v1, "Escape"

    invoke-direct {v0, v1}, Lcom/google/android/search/shared/actions/ButtonAction;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/search/shared/actions/ButtonAction;->bLU:Lcom/google/android/search/shared/actions/ButtonAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/search/shared/actions/ButtonAction;->aD:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/ButtonAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final aga()Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return v0
.end method

.method public final agu()Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Lcom/google/android/shared/util/MatchingAppInfo;->avc()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    return-object v0
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ButtonAction["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/search/shared/actions/ButtonAction;->aD:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
