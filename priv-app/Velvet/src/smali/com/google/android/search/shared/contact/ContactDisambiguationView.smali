.class public Lcom/google/android/search/shared/contact/ContactDisambiguationView;
.super Lecx;
.source "PG"


# instance fields
.field private final bRd:Landroid/view/View$OnClickListener;

.field private bRe:Landroid/view/ViewGroup;

.field private bRf:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/search/shared/contact/ContactDisambiguationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/shared/contact/ContactDisambiguationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Lecx;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    new-instance v0, Ldyt;

    invoke-direct {v0, p0}, Ldyt;-><init>(Lcom/google/android/search/shared/contact/ContactDisambiguationView;)V

    iput-object v0, p0, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->bRd:Landroid/view/View$OnClickListener;

    .line 57
    return-void
.end method


# virtual methods
.method protected final synthetic a(Landroid/os/Parcelable;Ljava/lang/Object;Z)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 24
    check-cast p1, Lcom/google/android/search/shared/contact/Person;

    check-cast p2, Ldzb;

    iget-boolean v0, p0, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->bRf:Z

    if-nez v0, :cond_0

    const v0, 0x7f0400ea

    :goto_0
    iget-object v1, p0, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->mLayoutInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v1, v0, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonSelectItem;

    invoke-virtual {v0, v2}, Lcom/google/android/search/shared/contact/PersonSelectItem;->eA(Z)V

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->alU()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->alV()I

    move-result v1

    :goto_1
    invoke-static {v0, v1}, Lehd;->s(Landroid/view/View;I)V

    const v1, 0x7f110120

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c0126

    invoke-static {v1, v2}, Lehd;->r(Landroid/view/View;I)V

    const v1, 0x7f110134

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c0125

    invoke-static {v1, v2}, Lehd;->r(Landroid/view/View;I)V

    const v1, 0x7f110132

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/contact/PersonSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0c00e6

    invoke-static {v1, v2}, Lehd;->r(Landroid/view/View;I)V

    invoke-virtual {v0, p3}, Lcom/google/android/search/shared/contact/PersonSelectItem;->ez(Z)V

    if-nez p3, :cond_3

    sget-object v1, Ldzb;->bRt:Ldzb;

    if-eq p2, v1, :cond_3

    invoke-virtual {p1, p2}, Lcom/google/android/search/shared/contact/Person;->d(Ldzb;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1, v3}, Lcom/google/android/search/shared/contact/PersonSelectItem;->a(Lcom/google/android/search/shared/contact/Person;Ljava/util/List;Lesk;)V

    :goto_2
    return-object v0

    :cond_0
    if-eqz p3, :cond_1

    const v0, 0x7f0400e8

    goto :goto_0

    :cond_1
    const v0, 0x7f0400eb

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonSelectItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0124

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    goto :goto_1

    :cond_3
    invoke-virtual {v0, p1, v3, v3}, Lcom/google/android/search/shared/contact/PersonSelectItem;->a(Lcom/google/android/search/shared/contact/Person;Ljava/util/List;Lesk;)V

    goto :goto_2
.end method

.method public final a(Lcom/google/android/search/shared/actions/utils/Disambiguation;Ldzb;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 82
    invoke-super {p0, p1, p2}, Lecx;->a(Lcom/google/android/search/shared/actions/utils/Disambiguation;Ljava/lang/Object;)V

    .line 84
    check-cast p1, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    .line 87
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alD()Ljava/util/List;

    move-result-object v7

    .line 88
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz v7, :cond_5

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amv()Z

    move-result v0

    if-nez v0, :cond_5

    .line 91
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v1, :cond_3

    move v3, v1

    :goto_0
    iget-boolean v1, p0, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->bRf:Z

    if-eqz v1, :cond_4

    iget-object v2, p0, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->bRe:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->bRe:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->bRe:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v1, p0, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->bRe:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->bRe:Landroid/view/ViewGroup;

    invoke-virtual {p0, v1}, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->addView(Landroid/view/View;)V

    :cond_1
    const v1, 0x7f040046

    move v5, v1

    move-object v6, v2

    :goto_1
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/contact/Contact;

    iget-object v2, p0, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->mLayoutInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v2, v5, v6, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/search/shared/contact/ContactDetailSelectItem;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/search/shared/contact/ContactDetailSelectItem;->c(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Contact;)V

    const v1, 0x7f0c00e9

    invoke-static {v2, v1}, Lehd;->r(Landroid/view/View;I)V

    const v1, 0x7f110123

    invoke-virtual {v2, v1}, Lcom/google/android/search/shared/contact/ContactDetailSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v8, 0x7f0c00e7

    invoke-static {v1, v8}, Lehd;->r(Landroid/view/View;I)V

    const v1, 0x7f110124

    invoke-virtual {v2, v1}, Lcom/google/android/search/shared/contact/ContactDetailSelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v8, 0x7f0c00e8

    invoke-static {v1, v8}, Lehd;->r(Landroid/view/View;I)V

    invoke-virtual {v6, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    if-nez v3, :cond_2

    iget-object v1, p0, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->bRd:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v1}, Lcom/google/android/search/shared/contact/ContactDetailSelectItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    :cond_3
    move v3, v4

    goto :goto_0

    :cond_4
    const v1, 0x7f040045

    move v5, v1

    move-object v6, p0

    goto :goto_1

    .line 95
    :cond_5
    iget-object v0, p0, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->bRe:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 97
    :cond_6
    return-void
.end method

.method public final bridge synthetic a(Lcom/google/android/search/shared/actions/utils/Disambiguation;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p2, Ldzb;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->a(Lcom/google/android/search/shared/actions/utils/Disambiguation;Ldzb;)V

    return-void
.end method

.method public final a(Lecz;)V
    .locals 1

    .prologue
    .line 68
    instance-of v0, p1, Ldyu;

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 69
    invoke-super {p0, p1}, Lecx;->a(Lecz;)V

    .line 70
    return-void
.end method

.method public final ex(Z)V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->bRf:Z

    .line 78
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0}, Lecx;->onFinishInflate()V

    .line 63
    const v0, 0x7f110126

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->bRe:Landroid/view/ViewGroup;

    .line 64
    return-void
.end method
