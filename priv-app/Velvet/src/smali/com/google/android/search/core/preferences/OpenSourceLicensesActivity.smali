.class public Lcom/google/android/search/core/preferences/OpenSourceLicensesActivity;
.super Landroid/app/Activity;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 19
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    const v0, 0x7f04000d

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/OpenSourceLicensesActivity;->setContentView(I)V

    .line 21
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/OpenSourceLicensesActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    .line 22
    const v0, 0x7f110073

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/preferences/OpenSourceLicensesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 23
    const-string v1, "file:///android_asset/html/licenses.html"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 24
    return-void
.end method
