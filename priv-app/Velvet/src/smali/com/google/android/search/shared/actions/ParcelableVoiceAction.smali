.class public Lcom/google/android/search/shared/actions/ParcelableVoiceAction;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Ldup;

    invoke-direct {v0}, Ldup;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 0
    .param p1    # Lcom/google/android/search/shared/actions/VoiceAction;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    .line 22
    return-void
.end method


# virtual methods
.method public final ahN()Lcom/google/android/search/shared/actions/VoiceAction;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    if-nez v0, :cond_0

    .line 36
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 41
    :goto_0
    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0, p1, p2}, Lcom/google/android/search/shared/actions/VoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0
.end method
