.class public final Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;
.super Landroid/app/IntentService;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 178
    const-string v0, "UpdateCorporaService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 179
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->setIntentRedelivery(Z)V

    .line 180
    return-void
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 171
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "ACTION_UPDATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_APPLICATIONS_UPDATE_MODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY_CONTACTS_UPDATE_MODE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/util/Set;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 142
    sget-object v0, Lifi;->dBe:Lifi;

    invoke-static {p1, v0}, Liia;->a(Ljava/util/Collection;Lifg;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 144
    const-string v1, "NONE"

    const-string v2, "SPECIFIC"

    invoke-static {p0, v1, v2}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "KEY_CONTACTS_ARGS"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;IIJ)V
    .locals 5

    .prologue
    .line 111
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 112
    const-string v1, "ACTION_MAYBE_UPDATE_CONTACTS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    const-string v1, "EXTRA_ATTEMPT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 114
    const-string v1, "EXTRA_LAST_RAW_CONTACT_COUNT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 115
    const/4 v1, 0x0

    const/high16 v2, 0x50000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    add-long/2addr v2, p3

    const/4 v4, 0x2

    invoke-virtual {v0, v4, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 116
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 93
    new-array v0, v5, [Ljava/lang/Object;

    aput-object p1, v0, v3

    const-string v1, "Alarm status: "

    aput-object v1, v0, v4

    invoke-static {p2, v0}, Leth;->a(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 96
    new-array v1, v6, [Ljava/lang/Object;

    aput-object v0, v1, v3

    const-string v2, "Applications score pending: "

    aput-object v2, v1, v4

    const-string v2, "ACTION_UPDATE_APP_SCORES"

    invoke-static {p0, v2}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {p2, v1}, Leth;->a(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    .line 98
    new-array v1, v6, [Ljava/lang/Object;

    aput-object v0, v1, v3

    const-string v0, "Contacts pending: "

    aput-object v0, v1, v4

    const-string v0, "ACTION_MAYBE_UPDATE_CONTACTS"

    invoke-static {p0, v0}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v1, v5

    invoke-static {p2, v1}, Leth;->a(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    .line 100
    return-void
.end method

.method public static a(Landroid/content/SharedPreferences;Letj;)V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v4, 0x0

    .line 202
    const-string v0, "Current time"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Leth;->bc(J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 204
    const-string v0, "Last applications update"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    const-string v1, "KEY_LAST_APPLICATIONS_UPDATE"

    invoke-interface {p0, v1, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Leth;->bc(J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 207
    const-string v0, "Last applications full scores update"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    const-string v1, "applications_last_scores_update_timestamp"

    const-wide/16 v2, 0x0

    invoke-interface {p0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Leth;->bc(J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 211
    const-string v0, "Last contacts update"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    const-string v1, "KEY_LAST_CONTACTS_UPDATE"

    invoke-interface {p0, v1, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Leth;->bc(J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 214
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 266
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    .line 267
    const-string v0, "UpdateIcingCorporaService"

    const-string v1, "Updating corpora: APPS=%s, CONTACTS=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x4

    invoke-static {v4, v0, v1, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 268
    new-instance v0, Lcuy;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DD()Lcjs;

    move-result-object v1

    invoke-virtual {v2}, Lgql;->aJs()Lchr;

    move-result-object v2

    invoke-virtual {v2}, Lchr;->Kt()Lcyg;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcuy;-><init>(Lcjs;Landroid/content/SharedPreferences;Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v0}, Lcuy;->ub()Ljava/lang/Void;

    .line 275
    return-void
.end method

.method public static ai(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 130
    const-string v0, "MAYBE"

    const-string v1, "MAYBE"

    invoke-static {p0, v0, v1}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static aj(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 136
    const-string v0, "NONE"

    const-string v1, "DELTA"

    invoke-static {p0, v0, v1}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static ak(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 157
    const-string v0, "FORCE_ALL"

    const-string v1, "MAYBE"

    invoke-static {p0, v0, v1}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static al(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 163
    const-string v0, "FORCE_ALL"

    const-string v1, "FORCE_ALL"

    invoke-static {p0, v0, v1}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;Lcjs;)Z
    .locals 6

    .prologue
    const/4 v4, 0x4

    const/4 v0, 0x0

    .line 64
    invoke-static {p1}, Lcum;->c(Lcjs;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 65
    const-string v1, "UpdateIcingCorporaService"

    const-string v2, "Ignoring contacts schedule because corpus disabled"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 77
    :goto_0
    return v0

    .line 69
    :cond_0
    invoke-virtual {p1}, Lcjs;->MG()I

    move-result v1

    .line 71
    if-gez v1, :cond_1

    .line 72
    const-string v1, "UpdateIcingCorporaService"

    const-string v2, "Not scheduling contact sync"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 76
    :cond_1
    const/4 v2, -0x1

    int-to-long v4, v1

    invoke-static {p0, v0, v2, v4, v5}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->a(Landroid/content/Context;IIJ)V

    .line 77
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static f(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 103
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 104
    invoke-virtual {v1, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 105
    const/high16 v2, 0x60000000

    invoke-static {p0, v0, v1, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method protected final onHandleIntent(Landroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v9, 0x5

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 184
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 185
    const-string v1, "ACTION_UPDATE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 186
    const-string v0, "KEY_APPLICATIONS_UPDATE_MODE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "KEY_CONTACTS_UPDATE_MODE"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "KEY_CONTACTS_ARGS"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    const-string v1, "ACTION_MAYBE_UPDATE_CONTACTS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 188
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "EXTRA_LAST_RAW_CONTACT_COUNT"

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    const-string v1, "EXTRA_ATTEMPT"

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "UpdateIcingCorporaService"

    const-string v1, "Could not fetch contact count - no contacts provider present?"

    new-array v2, v8, [Ljava/lang/Object;

    const/4 v3, 0x4

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DD()Lcjs;

    move-result-object v0

    invoke-virtual {v0}, Lcjs;->MJ()I

    move-result v3

    if-eq v1, v6, :cond_3

    if-ge v7, v3, :cond_3

    add-int/lit8 v2, v7, 0x1

    invoke-virtual {v0}, Lcjs;->MH()I

    move-result v0

    if-ltz v0, :cond_0

    int-to-long v4, v0

    invoke-static {p0, v2, v1, v4, v5}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->a(Landroid/content/Context;IIJ)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_3
    if-eq v1, v6, :cond_4

    const-string v0, "UpdateIcingCorporaService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Number of contacts did not stabilize after attempt "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v9, v0, v1, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_4
    const-string v0, "MAYBE"

    const-string v1, "FORCE_ALL"

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 190
    :cond_5
    const-string v0, "UpdateIcingCorporaService"

    const-string v1, "Received unrecognized action."

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v9, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
