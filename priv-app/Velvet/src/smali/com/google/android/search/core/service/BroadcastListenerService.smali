.class public Lcom/google/android/search/core/service/BroadcastListenerService;
.super Landroid/app/Service;
.source "PG"


# static fields
.field private static final bpt:Ljava/util/Map;


# instance fields
.field private final KU:Landroid/content/BroadcastReceiver;

.field private aNV:Z

.field private bpA:Z

.field private bpB:Z

.field private bpC:Z

.field private bpD:Ljava/util/List;

.field private bpE:Lcom/google/android/gms/location/ActivityRecognitionResult;

.field private bpF:Z

.field private bpG:I

.field private bpH:Z

.field private bpI:Ljava/lang/String;

.field private bpu:Landroid/os/PowerManager;

.field public bpv:Landroid/app/KeyguardManager;

.field private bpw:Lbww;

.field private bpx:Z

.field private bpy:[Ljava/lang/String;

.field private bpz:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 65
    const-string v0, "d"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "r"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "w"

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "b"

    const/4 v7, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static/range {v0 .. v7}, Lijm;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lijm;

    move-result-object v0

    sput-object v0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpt:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 75
    new-instance v0, Ldak;

    invoke-direct {v0, p0}, Ldak;-><init>(Lcom/google/android/search/core/service/BroadcastListenerService;)V

    iput-object v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->KU:Landroid/content/BroadcastReceiver;

    .line 119
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpD:Ljava/util/List;

    .line 124
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpG:I

    return-void
.end method

.method public static varargs a(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 133
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/search/core/service/BroadcastListenerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 134
    const-string v1, "com.google.android.search.core.service.BroadcastListenerService.action.UPDATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    const-string v1, "com.google.android.search.core.service.BroadcastListenerService.extra.FILTERS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 136
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 137
    return-void
.end method

.method private l([Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 199
    .line 201
    array-length v6, p1

    move v4, v5

    move v3, v5

    move v0, v5

    :goto_0
    if-ge v4, v6, :cond_5

    aget-object v7, p1, v4

    .line 202
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 204
    :cond_0
    if-nez v0, :cond_8

    .line 207
    iget-object v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpu:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    invoke-virtual {p0, p0, v0}, Lcom/google/android/search/core/service/BroadcastListenerService;->d(Landroid/content/Context;Z)V

    move v1, v2

    .line 210
    :goto_1
    const-string v0, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 212
    iget-object v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpv:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    invoke-virtual {p0, p0, v0}, Lcom/google/android/search/core/service/BroadcastListenerService;->e(Landroid/content/Context;Z)V

    .line 215
    :cond_1
    const-string v0, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 217
    invoke-virtual {p0}, Lcom/google/android/search/core/service/BroadcastListenerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v8, "audio"

    invoke-virtual {v0, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 220
    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    invoke-virtual {p0, p0, v0}, Lcom/google/android/search/core/service/BroadcastListenerService;->g(Landroid/content/Context;Z)V

    .line 223
    :cond_2
    const-string v0, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 225
    :cond_3
    if-nez v3, :cond_6

    .line 229
    new-instance v0, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 230
    const/4 v3, 0x0

    invoke-virtual {p0, v3, v0}, Lcom/google/android/search/core/service/BroadcastListenerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    .line 231
    invoke-static {v0}, Lcom/google/android/search/core/service/BroadcastListenerService;->s(Landroid/content/Intent;)Z

    move-result v0

    invoke-virtual {p0, p0, v0}, Lcom/google/android/search/core/service/BroadcastListenerService;->f(Landroid/content/Context;Z)V

    move v0, v2

    .line 234
    :goto_2
    const-string v3, "com.google.android.velvet.location.ACTIVITY_DETECTION"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 236
    iget-object v3, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpE:Lcom/google/android/gms/location/ActivityRecognitionResult;

    if-eqz v3, :cond_4

    .line 237
    new-instance v3, Landroid/content/Intent;

    const-string v7, "com.google.android.velvet.location.ACTIVITY_DETECTION"

    invoke-direct {v3, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 239
    const-string v7, "com.google.android.location.internal.EXTRA_ACTIVITY_RESULT"

    iget-object v8, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpE:Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 241
    invoke-static {p0}, Lesp;->aA(Landroid/content/Context;)Z

    move-result v7

    invoke-virtual {p0, p0, v3, v5, v7}, Lcom/google/android/search/core/service/BroadcastListenerService;->a(Landroid/content/Context;Landroid/content/Intent;ZZ)V

    .line 201
    :cond_4
    :goto_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v0

    move v0, v1

    goto/16 :goto_0

    .line 245
    :cond_5
    return-void

    :cond_6
    move v0, v3

    goto :goto_3

    :cond_7
    move v0, v3

    goto :goto_2

    :cond_8
    move v1, v0

    move v0, v3

    goto :goto_3

    :cond_9
    move v1, v0

    goto/16 :goto_1
.end method

.method protected static s(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 471
    if-nez p0, :cond_1

    .line 475
    :cond_0
    :goto_0
    return v0

    .line 474
    :cond_1
    const-string v1, "status"

    const/4 v2, -0x1

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 475
    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected final UL()Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->KU:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method protected final UM()Z
    .locals 1

    .prologue
    .line 259
    iget-boolean v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpz:Z

    return v0
.end method

.method protected final UN()Z
    .locals 1

    .prologue
    .line 269
    iget-boolean v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpB:Z

    return v0
.end method

.method protected final UO()Z
    .locals 1

    .prologue
    .line 274
    iget-boolean v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpC:Z

    return v0
.end method

.method protected final UP()Lcom/google/android/gms/location/ActivityRecognitionResult;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpE:Lcom/google/android/gms/location/ActivityRecognitionResult;

    return-object v0
.end method

.method protected final UQ()Ljava/util/List;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpD:Ljava/util/List;

    return-object v0
.end method

.method protected final UR()Z
    .locals 1

    .prologue
    .line 289
    iget-boolean v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpF:Z

    return v0
.end method

.method protected final US()Z
    .locals 1

    .prologue
    .line 294
    iget-boolean v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->aNV:Z

    return v0
.end method

.method protected final UT()Z
    .locals 1

    .prologue
    .line 299
    iget-boolean v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpH:Z

    return v0
.end method

.method protected final UU()Ljava/lang/String;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpI:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Landroid/content/Intent;ZZ)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 381
    invoke-static {p2}, Lcom/google/android/gms/location/ActivityRecognitionResult;->h(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 382
    :try_start_0
    invoke-static {p2}, Lcom/google/android/gms/location/ActivityRecognitionResult;->i(Landroid/content/Intent;)Lcom/google/android/gms/location/ActivityRecognitionResult;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 393
    iput-object v2, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpE:Lcom/google/android/gms/location/ActivityRecognitionResult;

    .line 396
    if-eqz p3, :cond_1

    .line 397
    iget-object v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpD:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/16 v3, 0xa

    if-ne v1, v3, :cond_0

    .line 398
    iget-object v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpD:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 400
    :cond_0
    iget-object v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpD:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 404
    :cond_1
    const/4 v1, 0x0

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    .line 406
    const-string v3, "status"

    const/4 v4, -0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 407
    const/4 v3, 0x2

    if-eq v1, v3, :cond_2

    const/4 v3, 0x5

    if-ne v1, v3, :cond_5

    :cond_2
    const/4 v1, 0x1

    .line 412
    :goto_0
    if-eqz p3, :cond_6

    iget-object v3, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpw:Lbww;

    invoke-virtual {v3, v2, v1}, Lbww;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;Z)Z

    move-result v1

    .line 418
    :goto_1
    if-eqz p4, :cond_7

    .line 429
    :goto_2
    iget-boolean v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpF:Z

    if-ne v1, v0, :cond_3

    if-nez p3, :cond_4

    .line 430
    :cond_3
    iput-boolean v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpF:Z

    .line 432
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/search/core/service/SearchService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.google.android.search.core.action.ACTIVITY_DETECTED"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.search.core.driving"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 435
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 438
    :cond_4
    :goto_3
    return-void

    .line 386
    :catch_0
    move-exception v0

    .line 387
    const-string v1, "BroadcastListenerSvc"

    const-string v2, "Exception while extract result. Bailing out."

    invoke-static {v1, v2, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_5
    move v1, v0

    .line 407
    goto :goto_0

    .line 412
    :cond_6
    iget-object v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpw:Lbww;

    invoke-virtual {v1}, Lbww;->Bf()Z

    move-result v1

    goto :goto_1

    :cond_7
    move v0, v1

    goto :goto_2
.end method

.method protected final a(Lbww;)V
    .locals 0

    .prologue
    .line 309
    iput-object p1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpw:Lbww;

    .line 310
    return-void
.end method

.method public final c(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 353
    iget v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpG:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpG:I

    .line 354
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/search/core/service/SearchService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.search.core.action.SMS_RECEIVED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 356
    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 357
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 358
    return-void
.end method

.method public final d(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 443
    const-string v1, "fg"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->aNV:Z

    .line 445
    const-string v1, "nav"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpH:Z

    .line 447
    const-string v1, "mode"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpI:Ljava/lang/String;

    .line 449
    sget-object v1, Lcom/google/android/search/core/service/BroadcastListenerService;->bpt:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpI:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpt:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpI:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 461
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/search/core/service/SearchService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.google.android.search.core.action.GMM_NAV_STATE_CHANGE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.search.core.extra.GMM_IS_FOREGROUNDED"

    iget-boolean v3, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->aNV:Z

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.search.core.extra.GMM_IS_NAVIGATING"

    iget-boolean v3, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpH:Z

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.search.core.extra.GMM_TRAVEL_MODE"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 466
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 467
    return-void
.end method

.method public final d(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 335
    iput-boolean p2, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpz:Z

    .line 336
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/search/core/service/SearchService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.search.core.action.SCREEN_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.search.core.extra.IS_SCREEN_ON"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 339
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 340
    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 314
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BroadcastListenerService[Registered actions: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpy:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", SMS intent count:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpG:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", IsCharging:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpB:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", IsScreenOn:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpz:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", IsLocked:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpA:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Last activity detected:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpE:Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ===================================================\n, Last activites detected:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpD:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ===================================================\n, Driving state smoother:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpw:Lbww;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", IsDriving:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpF:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", IsWiredHeadsetPlugged:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpC:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", GmmIsForegrounded:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->aNV:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", GmmIsNavigating:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpH:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", GmmTravelMode:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 331
    return-void
.end method

.method public final e(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 344
    iput-boolean p2, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpA:Z

    .line 345
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/search/core/service/SearchService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.search.core.action.DEVICE_LOCKED_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.search.core.extra.IS_LOCKED"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 348
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 349
    return-void
.end method

.method public final f(Landroid/content/Context;Z)V
    .locals 3

    .prologue
    .line 362
    iput-boolean p2, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpB:Z

    .line 363
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/search/core/service/SearchService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.search.core.action.CHARGING_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 365
    const-string v1, "com.google.android.search.core.extra.IS_CHARGING"

    iget-boolean v2, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpB:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 366
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 367
    return-void
.end method

.method public final g(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 371
    iput-boolean p2, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpC:Z

    .line 372
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/search/core/service/SearchService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.search.core.action.WIRED_HEADSET"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.search.core.extra.WIRED_HEADSET_IS_PLUGGED"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 375
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 376
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 142
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 143
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/service/BroadcastListenerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpu:Landroid/os/PowerManager;

    .line 144
    const-string v0, "keyguard"

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/service/BroadcastListenerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpv:Landroid/app/KeyguardManager;

    .line 145
    new-instance v0, Lbww;

    invoke-direct {v0}, Lbww;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpw:Lbww;

    .line 146
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpx:Z

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->KU:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/service/BroadcastListenerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpx:Z

    .line 156
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 157
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 162
    const-string v2, "com.google.android.search.core.service.BroadcastListenerService.action.UPDATE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 163
    const-string v2, "com.google.android.search.core.service.BroadcastListenerService.extra.FILTERS"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpy:[Ljava/lang/String;

    .line 165
    iget-boolean v2, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpx:Z

    if-eqz v2, :cond_0

    .line 166
    iget-object v2, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->KU:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/google/android/search/core/service/BroadcastListenerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 167
    iput-boolean v1, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpx:Z

    .line 171
    :cond_0
    iget-object v2, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpy:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpy:[Ljava/lang/String;

    array-length v2, v2

    if-nez v2, :cond_2

    .line 172
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/search/core/service/BroadcastListenerService;->stopSelf()V

    .line 187
    :goto_0
    return v0

    .line 177
    :cond_2
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 178
    iget-object v3, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpy:[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_3

    aget-object v1, v3, v0

    .line 179
    invoke-virtual {v2, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 178
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 181
    :cond_3
    iget-object v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->KU:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/search/core/service/BroadcastListenerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 182
    iput-boolean v5, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpx:Z

    .line 183
    iget-object v0, p0, Lcom/google/android/search/core/service/BroadcastListenerService;->bpy:[Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/search/core/service/BroadcastListenerService;->l([Ljava/lang/String;)V

    .line 184
    const/4 v0, 0x3

    goto :goto_0

    .line 186
    :cond_4
    const-string v2, "BroadcastListenerSvc"

    const-string v3, "UnsupportedAction: %s"

    new-array v4, v5, [Ljava/lang/Object;

    aput-object p1, v4, v1

    invoke-static {v2, v3, v4}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method
