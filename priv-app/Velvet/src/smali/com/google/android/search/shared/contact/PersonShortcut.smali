.class public Lcom/google/android/search/shared/contact/PersonShortcut;
.super Lcom/google/android/search/shared/contact/PersonShortcutKey;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private bSp:J

.field private final bSq:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final biV:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lead;

    invoke-direct {v0}, Lead;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/contact/PersonShortcut;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/search/shared/contact/PersonShortcutKey;JJLjava/lang/String;)V
    .locals 10
    .param p1    # Lcom/google/android/search/shared/contact/PersonShortcutKey;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 52
    iget-object v1, p1, Lcom/google/android/search/shared/contact/PersonShortcutKey;->bSr:Ldzb;

    iget-object v2, p1, Lcom/google/android/search/shared/contact/PersonShortcutKey;->bRP:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/search/shared/contact/PersonShortcutKey;->bSs:Ljava/lang/String;

    move-object v0, p0

    move-wide v4, p2

    move-wide v6, p4

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/search/shared/contact/PersonShortcut;-><init>(Ldzb;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Ldzb;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V
    .locals 0
    .param p1    # Ldzb;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/search/shared/contact/PersonShortcutKey;-><init>(Ldzb;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    iput-wide p4, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->bSp:J

    .line 43
    iput-wide p6, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->biV:J

    .line 44
    iput-object p8, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->bSq:Ljava/lang/String;

    .line 45
    return-void
.end method


# virtual methods
.method public final KM()J
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->bSp:J

    return-wide v0
.end method

.method public final KN()J
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->biV:J

    return-wide v0
.end method

.method public final KO()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->bSq:Ljava/lang/String;

    return-object v0
.end method

.method public final aC(J)V
    .locals 1

    .prologue
    .line 61
    iput-wide p1, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->bSp:J

    .line 62
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 84
    if-ne p0, p1, :cond_1

    .line 90
    :cond_0
    :goto_0
    return v0

    .line 85
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 86
    :cond_3
    invoke-super {p0, p1}, Lcom/google/android/search/shared/contact/PersonShortcutKey;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    .line 88
    :cond_4
    check-cast p1, Lcom/google/android/search/shared/contact/PersonShortcut;

    .line 90
    iget-wide v2, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->biV:J

    iget-wide v4, p1, Lcom/google/android/search/shared/contact/PersonShortcut;->biV:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    iget-wide v2, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->bSp:J

    iget-wide v4, p1, Lcom/google/android/search/shared/contact/PersonShortcut;->bSp:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->bSq:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/search/shared/contact/PersonShortcut;->bSq:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 97
    invoke-super {p0}, Lcom/google/android/search/shared/contact/PersonShortcutKey;->hashCode()I

    move-result v0

    .line 98
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->bSp:J

    iget-wide v4, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->bSp:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 99
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->biV:J

    iget-wide v4, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->biV:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 100
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->bSq:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->bSq:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    .line 101
    return v0

    .line 100
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PersonShortcut : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/search/shared/contact/PersonShortcutKey;->bSr:Ldzb;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/contact/PersonShortcutKey;->bRP:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonShortcutKey;->bSs:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " => "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->biV:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->bSq:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/search/shared/contact/PersonShortcutKey;->bSs:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->bSq:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 106
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/contact/PersonShortcutKey;->writeToParcel(Landroid/os/Parcel;I)V

    .line 107
    iget-wide v0, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->bSp:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 108
    iget-wide v0, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->biV:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 109
    iget-object v0, p0, Lcom/google/android/search/shared/contact/PersonShortcut;->bSq:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 110
    return-void
.end method
