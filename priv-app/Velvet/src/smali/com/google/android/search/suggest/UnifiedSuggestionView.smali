.class public Lcom/google/android/search/suggest/UnifiedSuggestionView;
.super Landroid/widget/RelativeLayout;
.source "PG"

# interfaces
.implements Leev;


# static fields
.field private static final arU:Landroid/animation/TimeInterpolator;

.field private static final bXw:Ljava/lang/String;

.field private static final bXx:Landroid/graphics/ColorFilter;


# instance fields
.field private bTm:Leld;

.field private bWH:Landroid/widget/TextView;

.field private bWJ:Landroid/view/View;

.field private bWK:Z

.field private bWL:Z

.field private bWM:Landroid/graphics/drawable/StateListDrawable;

.field private bWN:Landroid/graphics/drawable/StateListDrawable;

.field private bWO:Landroid/graphics/drawable/StateListDrawable;

.field private bWP:Landroid/graphics/drawable/StateListDrawable;

.field private bWQ:Lesm;

.field private bWR:Lcom/google/android/shared/search/Suggestion;

.field private bWS:Leew;

.field private bWT:F

.field private final bXA:Landroid/view/View$OnLongClickListener;

.field private bXB:Landroid/view/View$OnClickListener;

.field private bXC:Landroid/widget/TextView;

.field public bXD:Landroid/widget/ImageView;

.field private bXE:Landroid/widget/ImageView;

.field private bXF:Leiu;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bXy:Landroid/view/View$OnClickListener;

.field private final bXz:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 64
    sget-object v0, Ldqs;->bGc:Ldqs;

    sput-object v0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->arU:Landroid/animation/TimeInterpolator;

    .line 68
    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXw:Ljava/lang/String;

    .line 72
    new-instance v0, Landroid/graphics/ColorMatrixColorFilter;

    new-instance v1, Landroid/graphics/ColorMatrix;

    const/16 v2, 0x14

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-direct {v1, v2}, Landroid/graphics/ColorMatrix;-><init>([F)V

    invoke-direct {v0, v1}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    sput-object v0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXx:Landroid/graphics/ColorFilter;

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x433d0000    # 189.0f
        0x0
        0x0
        0x0
        0x0
        0x433d0000    # 189.0f
        0x0
        0x0
        0x0
        0x0
        0x433d0000    # 189.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 175
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/search/suggest/UnifiedSuggestionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 176
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 172
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 165
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 82
    new-instance v0, Leez;

    invoke-direct {v0, p0}, Leez;-><init>(Lcom/google/android/search/suggest/UnifiedSuggestionView;)V

    iput-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXy:Landroid/view/View$OnClickListener;

    .line 89
    new-instance v0, Lefa;

    invoke-direct {v0, p0}, Lefa;-><init>(Lcom/google/android/search/suggest/UnifiedSuggestionView;)V

    iput-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXz:Landroid/view/View$OnClickListener;

    .line 96
    new-instance v0, Lefb;

    invoke-direct {v0, p0}, Lefb;-><init>(Lcom/google/android/search/suggest/UnifiedSuggestionView;)V

    iput-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXA:Landroid/view/View$OnLongClickListener;

    .line 113
    new-instance v0, Lefc;

    invoke-direct {v0, p0}, Lefc;-><init>(Lcom/google/android/search/suggest/UnifiedSuggestionView;)V

    iput-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXB:Landroid/view/View$OnClickListener;

    .line 166
    const/4 v0, 0x1

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {p0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWT:F

    .line 168
    return-void
.end method

.method private d([F)Landroid/graphics/drawable/StateListDrawable;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 541
    invoke-virtual {p0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0079

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 542
    new-instance v1, Landroid/graphics/drawable/shapes/RoundRectShape;

    invoke-direct {v1, p1, v2, v2}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    new-instance v2, Landroid/graphics/drawable/ShapeDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 543
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 544
    sget-object v1, Lcom/google/android/search/suggest/UnifiedSuggestionView;->PRESSED_STATE_SET:[I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 545
    sget-object v1, Lcom/google/android/search/suggest/UnifiedSuggestionView;->FOCUSED_STATE_SET:[I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 546
    sget-object v1, Landroid/util/StateSet;->WILD_CARD:[I

    invoke-virtual {p0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x106000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 548
    return-object v0
.end method

.method public static q(Lcom/google/android/shared/search/Suggestion;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 328
    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->ast()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->ash()Ljava/lang/String;

    move-result-object v0

    .line 330
    if-eqz v0, :cond_0

    .line 331
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 332
    sget-object v1, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXw:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "com.android.contacts"

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 339
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/net/Uri;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 344
    invoke-virtual {p0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-static {v0, p2, p1, v1, v2}, Landroid/provider/ContactsContract$QuickContact;->showQuickContact(Landroid/content/Context;Landroid/view/View;Landroid/net/Uri;I[Ljava/lang/String;)V

    .line 345
    return-void
.end method

.method public final a(Leew;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWS:Leew;

    .line 266
    return-void
.end method

.method public final a(Lesm;Leld;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWQ:Lesm;

    .line 208
    iput-object p2, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bTm:Leld;

    .line 209
    return-void
.end method

.method public final a(Lcom/google/android/shared/search/Suggestion;Ljava/lang/String;Leeo;)Z
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 217
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWR:Lcom/google/android/shared/search/Suggestion;

    if-eq v0, p1, :cond_6

    move v0, v3

    :goto_0
    if-eqz v0, :cond_1b

    .line 218
    iput-object p1, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWR:Lcom/google/android/shared/search/Suggestion;

    .line 220
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXy:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 221
    iget-boolean v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWK:Z

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWL:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWO:Landroid/graphics/drawable/StateListDrawable;

    if-nez v0, :cond_0

    const/16 v0, 0x8

    new-array v0, v0, [F

    iget v1, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWT:F

    aput v1, v0, v4

    iget v1, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWT:F

    aput v1, v0, v3

    iget v1, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWT:F

    aput v1, v0, v7

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWT:F

    aput v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWT:F

    aput v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWT:F

    aput v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWT:F

    aput v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWT:F

    aput v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->d([F)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWO:Landroid/graphics/drawable/StateListDrawable;

    :cond_0
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWO:Landroid/graphics/drawable/StateListDrawable;

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 224
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->isStale()Z

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asp()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asj()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0658

    new-array v6, v3, [Ljava/lang/Object;

    aput-object v0, v6, v4

    invoke-virtual {v1, v2, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f090125

    invoke-virtual {p3, v0, v1, v2, v4}, Leeo;->b(Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 225
    :cond_1
    :goto_2
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->arZ()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asa()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {v1}, Landroid/webkit/URLUtil;->guessUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    const-string v6, "/"

    invoke-static {v2, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 226
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asr()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asy()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asn()Z

    move-result v2

    if-eqz v2, :cond_f

    :cond_3
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    .line 230
    :goto_3
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->isStale()Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWH:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 231
    :cond_4
    iget-object v6, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWH:Landroid/widget/TextView;

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    :cond_5
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWH:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 236
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 237
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWH:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 238
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWH:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 240
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXC:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 241
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXC:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXC:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 251
    :goto_4
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asu()Z

    move-result v0

    if-eqz v0, :cond_11

    const v0, 0x7f020086

    :goto_5
    if-eqz v0, :cond_14

    invoke-virtual {p0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->aol()Leiu;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Leiu;->j(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXD:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXx:Landroid/graphics/ColorFilter;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    :goto_6
    invoke-static {p1}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->q(Lcom/google/android/shared/search/Suggestion;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXD:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXB:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 252
    :goto_7
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asq()Z

    move-result v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXE:Landroid/widget/ImageView;

    const v1, 0x7f02020e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXE:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a05c6

    new-array v6, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->arY()Ljava/lang/CharSequence;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-virtual {v1, v2, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXE:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXz:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move v0, v3

    :goto_8
    iget-object v2, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXE:Landroid/widget/ImageView;

    if-eqz v0, :cond_19

    sget-object v1, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXx:Landroid/graphics/ColorFilter;

    :goto_9
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    iget-object v1, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXE:Landroid/widget/ImageView;

    if-eqz v0, :cond_1a

    :goto_a
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 256
    :goto_b
    return v3

    :cond_6
    move v0, v4

    .line 217
    goto/16 :goto_0

    .line 221
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWK:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWM:Landroid/graphics/drawable/StateListDrawable;

    if-nez v0, :cond_8

    const/16 v0, 0x8

    new-array v0, v0, [F

    iget v1, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWT:F

    aput v1, v0, v4

    iget v1, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWT:F

    aput v1, v0, v3

    iget v1, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWT:F

    aput v1, v0, v7

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWT:F

    aput v2, v0, v1

    const/4 v1, 0x4

    aput v6, v0, v1

    const/4 v1, 0x5

    aput v6, v0, v1

    const/4 v1, 0x6

    aput v6, v0, v1

    const/4 v1, 0x7

    aput v6, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->d([F)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWM:Landroid/graphics/drawable/StateListDrawable;

    :cond_8
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWM:Landroid/graphics/drawable/StateListDrawable;

    goto/16 :goto_1

    :cond_9
    iget-boolean v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWL:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWN:Landroid/graphics/drawable/StateListDrawable;

    if-nez v0, :cond_a

    const/16 v0, 0x8

    new-array v0, v0, [F

    aput v6, v0, v4

    aput v6, v0, v3

    aput v6, v0, v7

    const/4 v1, 0x3

    aput v6, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWT:F

    aput v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWT:F

    aput v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWT:F

    aput v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWT:F

    aput v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->d([F)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWN:Landroid/graphics/drawable/StateListDrawable;

    :cond_a
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWN:Landroid/graphics/drawable/StateListDrawable;

    goto/16 :goto_1

    :cond_b
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWP:Landroid/graphics/drawable/StateListDrawable;

    if-nez v0, :cond_c

    invoke-direct {p0, v5}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->d([F)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWP:Landroid/graphics/drawable/StateListDrawable;

    :cond_c
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWP:Landroid/graphics/drawable/StateListDrawable;

    goto/16 :goto_1

    .line 224
    :cond_d
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asq()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->arY()Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v1, v0, Landroid/text/Spanned;

    if-nez v1, :cond_1

    const v1, 0x7f090126

    const v2, 0x7f090127

    invoke-virtual {p3, p2, v0, v1, v2}, Leeo;->a(Ljava/lang/String;Ljava/lang/CharSequence;II)Ljava/lang/CharSequence;

    move-result-object v0

    goto/16 :goto_2

    :cond_e
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->arY()Ljava/lang/CharSequence;

    move-result-object v0

    goto/16 :goto_2

    .line 226
    :cond_f
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->START:Landroid/text/TextUtils$TruncateAt;

    goto/16 :goto_3

    .line 244
    :cond_10
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWH:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 245
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWH:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 247
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXC:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 251
    :cond_11
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asq()Z

    move-result v0

    if-eqz v0, :cond_12

    const v0, 0x7f0201e4

    goto/16 :goto_5

    :cond_12
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asr()Z

    move-result v0

    if-nez v0, :cond_13

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asy()Z

    move-result v0

    if-eqz v0, :cond_1c

    :cond_13
    const v0, 0x7f02013d

    goto/16 :goto_5

    :cond_14
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asb()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_16

    :cond_15
    invoke-virtual {p0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->aol()Leiu;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asb()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asc()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWQ:Lesm;

    invoke-virtual {v0, v1, v2, v6}, Leiu;->a(Ljava/lang/String;Ljava/lang/String;Lesm;)V

    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXD:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto/16 :goto_6

    :cond_16
    invoke-virtual {p0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->aol()Leiu;

    move-result-object v0

    invoke-virtual {v0, v5}, Leiu;->j(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_6

    :cond_17
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXD:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXD:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    goto/16 :goto_7

    :cond_18
    move v0, v4

    .line 252
    goto/16 :goto_8

    :cond_19
    move-object v1, v5

    goto/16 :goto_9

    :cond_1a
    const/4 v4, 0x4

    goto/16 :goto_a

    :cond_1b
    move v3, v4

    .line 256
    goto/16 :goto_b

    :cond_1c
    move v0, v4

    goto/16 :goto_5
.end method

.method public final aG(II)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 270
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWK:Z

    .line 272
    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWL:Z

    .line 273
    return-void

    :cond_0
    move v0, v2

    .line 270
    goto :goto_0

    :cond_1
    move v1, v2

    .line 272
    goto :goto_1
.end method

.method public final aod()Lcom/google/android/shared/search/Suggestion;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWR:Lcom/google/android/shared/search/Suggestion;

    return-object v0
.end method

.method public final aoj()Leew;
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWS:Leew;

    return-object v0
.end method

.method public final aok()Z
    .locals 4

    .prologue
    .line 436
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWR:Lcom/google/android/shared/search/Suggestion;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->ast()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 438
    new-instance v0, Lefd;

    invoke-direct {v0, p0}, Lefd;-><init>(Lcom/google/android/search/suggest/UnifiedSuggestionView;)V

    .line 452
    :goto_0
    if-nez v0, :cond_2

    .line 453
    const/4 v0, 0x0

    .line 455
    :goto_1
    return v0

    .line 445
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWR:Lcom/google/android/shared/search/Suggestion;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asq()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 446
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bTm:Leld;

    invoke-virtual {p0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Leld;->i(Landroid/graphics/drawable/Drawable;)Ligi;

    move-result-object v0

    goto :goto_0

    .line 449
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bTm:Leld;

    iget-object v1, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXD:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Leld;->i(Landroid/graphics/drawable/Drawable;)Ligi;

    move-result-object v0

    goto :goto_0

    .line 455
    :cond_2
    iget-object v1, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWS:Leew;

    iget-object v2, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWR:Lcom/google/android/shared/search/Suggestion;

    iget-object v3, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXD:Landroid/widget/ImageView;

    invoke-interface {v1, v2, v3, v0}, Leew;->b(Lcom/google/android/shared/search/Suggestion;Landroid/view/View;Ligi;)Z

    move-result v0

    goto :goto_1
.end method

.method protected aol()Leiu;
    .locals 3

    .prologue
    .line 461
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXF:Leiu;

    if-nez v0, :cond_0

    .line 462
    new-instance v0, Leiu;

    iget-object v1, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXD:Landroid/widget/ImageView;

    new-instance v2, Lefe;

    invoke-direct {v2, p0}, Lefe;-><init>(Lcom/google/android/search/suggest/UnifiedSuggestionView;)V

    invoke-direct {v0, v1, v2}, Leiu;-><init>(Landroid/view/View;Lemy;)V

    iput-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXF:Leiu;

    .line 483
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXF:Leiu;

    return-object v0
.end method

.method public final hn(I)V
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWJ:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 278
    return-void
.end method

.method protected onFinishInflate()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x2

    const/4 v4, 0x1

    .line 180
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 181
    const v0, 0x7f110488

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWH:Landroid/widget/TextView;

    .line 182
    const v0, 0x7f110489

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXC:Landroid/widget/TextView;

    .line 183
    const v0, 0x7f110485

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXD:Landroid/widget/ImageView;

    .line 184
    const v0, 0x7f110486

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXE:Landroid/widget/ImageView;

    .line 185
    const v0, 0x7f1102e1

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWJ:Landroid/view/View;

    .line 188
    const v0, 0x7f110487

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 189
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    .line 190
    if-eqz v0, :cond_0

    .line 191
    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/LayoutTransition;->setDuration(J)V

    .line 192
    invoke-virtual {v0, v1, v6, v7}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 193
    invoke-virtual {v0, v4, v6, v7}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 194
    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->getAnimator(I)Landroid/animation/Animator;

    move-result-object v1

    sget-object v2, Lcom/google/android/search/suggest/UnifiedSuggestionView;->arU:Landroid/animation/TimeInterpolator;

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 195
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->getAnimator(I)Landroid/animation/Animator;

    move-result-object v1

    sget-object v2, Lcom/google/android/search/suggest/UnifiedSuggestionView;->arU:Landroid/animation/TimeInterpolator;

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 196
    invoke-virtual {v0, v4}, Landroid/animation/LayoutTransition;->getAnimator(I)Landroid/animation/Animator;

    move-result-object v1

    sget-object v2, Lcom/google/android/search/suggest/UnifiedSuggestionView;->arU:Landroid/animation/TimeInterpolator;

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 197
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->getAnimator(I)Landroid/animation/Animator;

    move-result-object v1

    sget-object v2, Lcom/google/android/search/suggest/UnifiedSuggestionView;->arU:Landroid/animation/TimeInterpolator;

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 198
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->getAnimator(I)Landroid/animation/Animator;

    move-result-object v0

    sget-object v1, Lcom/google/android/search/suggest/UnifiedSuggestionView;->arU:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bXA:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/UnifiedSuggestionView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 203
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 287
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Landroid/widget/RelativeLayout;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " suggestion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/suggest/UnifiedSuggestionView;->bWR:Lcom/google/android/shared/search/Suggestion;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
