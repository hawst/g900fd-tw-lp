.class public Lcom/google/android/search/queryentry/QueryEntryActivity;
.super Landroid/app/Activity;
.source "PG"


# static fields
.field private static final aPs:Lcom/google/android/shared/search/SearchBoxStats;

.field public static final aPt:Lcom/google/android/search/shared/service/ClientConfig;

.field private static bFD:I


# instance fields
.field private aPL:Z

.field private bFA:Lebi;

.field private bFB:Landroid/content/Intent;

.field private bFC:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 49
    const-string v0, "assistant-query-entry"

    const-string v1, "android-search-assist"

    invoke-static {v0, v1}, Lcom/google/android/shared/search/SearchBoxStats;->aA(Ljava/lang/String;Ljava/lang/String;)Lehj;

    move-result-object v0

    invoke-virtual {v0}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v0

    sput-object v0, Lcom/google/android/search/queryentry/QueryEntryActivity;->aPs:Lcom/google/android/shared/search/SearchBoxStats;

    .line 55
    new-instance v0, Lcom/google/android/search/shared/service/ClientConfig;

    const-wide/32 v2, 0x58201602

    sget-object v1, Lcom/google/android/search/queryentry/QueryEntryActivity;->aPs:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/search/shared/service/ClientConfig;-><init>(JLcom/google/android/shared/search/SearchBoxStats;)V

    sput-object v0, Lcom/google/android/search/queryentry/QueryEntryActivity;->aPt:Lcom/google/android/search/shared/service/ClientConfig;

    .line 71
    const/4 v0, 0x0

    sput v0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFD:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 75
    sget v0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFD:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFD:I

    .line 76
    return-void
.end method


# virtual methods
.method public final aeN()V
    .locals 2

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/google/android/search/queryentry/QueryEntryActivity;->finish()V

    .line 203
    const/4 v0, 0x0

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lcom/google/android/search/queryentry/QueryEntryActivity;->overridePendingTransition(II)V

    .line 204
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 243
    invoke-super {p0, p1, p2, p3, p4}, Landroid/app/Activity;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 244
    new-instance v0, Letj;

    invoke-virtual {p0}, Lcom/google/android/search/queryentry/QueryEntryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Letj;-><init>(Landroid/content/res/Resources;)V

    .line 245
    const-string v1, "QueryEntryActivity"

    invoke-virtual {v0, v1}, Letj;->lt(Ljava/lang/String;)V

    .line 246
    iget-object v1, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    invoke-virtual {v0, v1}, Letj;->b(Leti;)V

    .line 247
    invoke-virtual {v0, p3, p1}, Letj;->a(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 248
    return-void
.end method

.method public finish()V
    .locals 0

    .prologue
    .line 198
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 199
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lebi;->bW(Z)Z

    .line 192
    invoke-virtual {p0}, Lcom/google/android/search/queryentry/QueryEntryActivity;->aeN()V

    .line 193
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v6, -0x1

    const/4 v1, 0x0

    .line 80
    invoke-static {p0, v2}, Legr;->b(Landroid/app/Activity;I)V

    .line 81
    invoke-virtual {p0}, Lcom/google/android/search/queryentry/QueryEntryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/google/android/search/queryentry/QueryEntryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0, v2, v1}, Legr;->a(Landroid/app/Activity;Landroid/content/Intent;IZ)V

    .line 85
    :cond_0
    if-eqz p1, :cond_3

    const-string v0, "qea:changing_configurations"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->aPL:Z

    .line 87
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 90
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->Eq()Lebk;

    move-result-object v2

    invoke-virtual {v2, p1}, Lebk;->z(Landroid/os/Bundle;)Lebi;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    .line 93
    iget-object v2, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    new-instance v3, Ldqn;

    invoke-direct {v3, p0, v1}, Ldqn;-><init>(Landroid/app/Activity;I)V

    invoke-interface {v2, v3}, Lebi;->a(Leqp;)V

    .line 94
    iget-object v2, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    new-instance v3, Lecd;

    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->aus()Leqo;

    move-result-object v0

    invoke-static {p0}, Legr;->b(Landroid/app/Activity;)J

    move-result-wide v4

    invoke-direct {v3, v0, v4, v5}, Lecd;-><init>(Leqo;J)V

    invoke-interface {v2, v3}, Lebi;->a(Lecd;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    invoke-interface {v0}, Lebi;->aer()Landroid/view/View;

    move-result-object v0

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v2}, Lcom/google/android/search/queryentry/QueryEntryActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    new-instance v2, Ldqo;

    invoke-direct {v2, p0}, Ldqo;-><init>(Lcom/google/android/search/queryentry/QueryEntryActivity;)V

    invoke-interface {v0, v2}, Lebi;->a(Lebj;)V

    .line 107
    iget-boolean v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->aPL:Z

    if-eqz v0, :cond_4

    const-string v0, "START_QEA_BACKGROUND_OPAQUE"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFC:Z

    .line 113
    iget-boolean v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFC:Z

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    invoke-interface {v0}, Lebi;->aes()V

    .line 117
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->aPL:Z

    if-nez v0, :cond_2

    .line 118
    invoke-virtual {p0}, Lcom/google/android/search/queryentry/QueryEntryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFB:Landroid/content/Intent;

    .line 119
    iget-object v2, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFB:Landroid/content/Intent;

    invoke-static {v2}, Lhgn;->aa(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    const-string v1, "handover-session-id"

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lebi;->av(J)V

    .line 121
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v0, v1

    .line 85
    goto/16 :goto_0

    .line 107
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/search/queryentry/QueryEntryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "START_QEA_BACKGROUND_OPAQUE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_1

    .line 119
    :cond_5
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "android.intent.action.VOICE_ASSIST"

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    const-string v1, "android-search-assist"

    invoke-interface {v0, v1}, Lebi;->ke(Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    invoke-static {v2}, Lhgn;->ab(Landroid/content/Intent;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    if-nez v0, :cond_7

    invoke-static {v2}, Lhgn;->ad(Landroid/content/Intent;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    sget-object v2, Lcom/google/android/search/queryentry/QueryEntryActivity;->aPs:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-virtual {v0, v2}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    :cond_7
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apA()Lcom/google/android/shared/search/Query;

    move-result-object v0

    :cond_8
    iget-object v2, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    const-string v3, "android-search-assist"

    invoke-interface {v2, v0, v1, v3}, Lebi;->a(Lcom/google/android/shared/search/Query;ZLjava/lang/String;)V

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 183
    const/4 v0, 0x2

    invoke-static {p0, v0}, Legr;->e(Landroid/app/Activity;I)V

    .line 184
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 185
    iget-object v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    invoke-virtual {p0}, Lcom/google/android/search/queryentry/QueryEntryActivity;->isChangingConfigurations()Z

    move-result v1

    invoke-interface {v0, v1}, Lebi;->dJ(Z)V

    .line 186
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 125
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1}, Legr;->a(Landroid/app/Activity;Landroid/content/Intent;IZ)V

    .line 126
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 127
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 151
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 152
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 131
    invoke-super {p0, p1}, Landroid/app/Activity;->onPostCreate(Landroid/os/Bundle;)V

    .line 132
    iget-object v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    invoke-interface {v0, p1}, Lebi;->onPostCreate(Landroid/os/Bundle;)V

    .line 133
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 138
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 139
    iget-object v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    invoke-interface {v0}, Lebi;->onResume()V

    .line 140
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 208
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 209
    invoke-virtual {p0}, Lcom/google/android/search/queryentry/QueryEntryActivity;->isChangingConfigurations()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    const-string v0, "qea:changing_configurations"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    invoke-interface {v0, p1}, Lebi;->w(Landroid/os/Bundle;)V

    .line 213
    const-string v0, "START_QEA_BACKGROUND_OPAQUE"

    iget-boolean v1, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFC:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 214
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 167
    const/4 v0, 0x2

    invoke-static {p0, v0}, Legr;->c(Landroid/app/Activity;I)V

    .line 168
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 170
    iget-object v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFB:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    invoke-interface {v0}, Lebi;->aev()Lecq;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFB:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lecq;->B(Landroid/os/Bundle;)V

    .line 173
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFB:Landroid/content/Intent;

    .line 177
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    invoke-interface {v0}, Lebi;->aev()Lecq;

    move-result-object v0

    invoke-virtual {v0}, Lecq;->start()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x2

    invoke-static {p0, v0}, Legr;->d(Landroid/app/Activity;I)V

    .line 158
    invoke-virtual {p0}, Lcom/google/android/search/queryentry/QueryEntryActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    invoke-interface {v0}, Lebi;->aev()Lecq;

    move-result-object v0

    invoke-virtual {v0}, Lecq;->stop()V

    .line 161
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 162
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 144
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 145
    iget-object v0, p0, Lcom/google/android/search/queryentry/QueryEntryActivity;->bFA:Lebi;

    invoke-interface {v0, p1}, Lebi;->onWindowFocusChanged(Z)V

    .line 146
    return-void
.end method
