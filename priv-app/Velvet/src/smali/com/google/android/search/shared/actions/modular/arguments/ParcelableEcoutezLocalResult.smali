.class public Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private bPA:Ljpd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Ldxj;

    invoke-direct {v0}, Ldxj;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljpd;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;->bPA:Ljpd;

    .line 19
    return-void
.end method


# virtual methods
.method public final aky()Ljpd;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;->bPA:Ljpd;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 32
    instance-of v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;->bPA:Ljpd;

    check-cast p1, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;

    iget-object v1, p1, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;->bPA:Ljpd;

    invoke-static {v0, v1}, Leqh;->c(Ljsr;Ljsr;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 39
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;->bPA:Ljpd;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/ParcelableEcoutezLocalResult;->bPA:Ljpd;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 45
    return-void
.end method
