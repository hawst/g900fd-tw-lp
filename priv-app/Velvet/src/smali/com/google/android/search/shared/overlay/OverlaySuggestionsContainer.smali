.class public Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Lebd;
.implements Ledf;


# static fields
.field private static bTc:J

.field private static bTd:J

.field private static bTe:J


# instance fields
.field private final Vs:Landroid/animation/LayoutTransition;

.field private final bTf:Leda;

.field private final bTg:Leda;

.field private bTh:Lcom/google/android/search/suggest/SuggestionListView;

.field private bTi:Lcom/google/android/search/suggest/SuggestionListView;

.field private bTj:Lcom/google/android/search/suggest/SuggestionStripView;

.field private bTk:Lcom/google/android/search/shared/overlay/GetGoogleNowView;

.field private bTl:Lcom/google/android/search/suggest/ScrollableSuggestionListView;

.field private bTm:Leld;

.field private bTn:Z

.field private final bTo:Ljava/util/Map;

.field private bTp:Z

.field private bTq:Lebb;

.field private bTr:F

.field private bTs:Z

.field private bTt:Lelj;

.field private ff:I

.field private final mTouchSlop:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51
    const-wide/16 v0, 0x64

    sput-wide v0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTc:J

    .line 52
    const-wide/16 v0, 0x96

    sput-wide v0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTd:J

    .line 53
    const-wide/16 v0, 0x30

    sput-wide v0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTe:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 84
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 81
    sget-object v0, Lelj;->ceI:Lelj;

    iput-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTt:Lelj;

    .line 85
    invoke-virtual {p0, v4}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->setOrientation(I)V

    .line 86
    new-instance v0, Leda;

    invoke-direct {v0, v6, p0}, Leda;-><init>(ZLedf;)V

    iput-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTf:Leda;

    .line 87
    new-instance v0, Leda;

    invoke-direct {v0, v4, p0}, Leda;-><init>(ZLedf;)V

    iput-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTg:Leda;

    .line 90
    invoke-virtual {p0}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->Vs:Landroid/animation/LayoutTransition;

    .line 91
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->Vs:Landroid/animation/LayoutTransition;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->Vs:Landroid/animation/LayoutTransition;

    sget-wide v2, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTc:J

    invoke-virtual {v0, v2, v3}, Landroid/animation/LayoutTransition;->setDuration(J)V

    sget-wide v2, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTd:J

    invoke-virtual {v0, v5, v2, v3}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    sget-wide v2, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTd:J

    invoke-virtual {v0, v7, v2, v3}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTf:Leda;

    invoke-virtual {v0, v5, v1}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTg:Leda;

    invoke-virtual {v0, v7, v1}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    sget-wide v2, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTd:J

    invoke-virtual {v0, v4, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    sget-wide v2, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTe:J

    invoke-virtual {v0, v6, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v5, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTg:Leda;

    sget-object v1, Ldqs;->bGc:Ldqs;

    invoke-virtual {v0, v1}, Leda;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 94
    :cond_0
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTo:Ljava/util/Map;

    .line 97
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->Vs:Landroid/animation/LayoutTransition;

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->Vs:Landroid/animation/LayoutTransition;

    new-instance v1, Leba;

    invoke-direct {v1, p0}, Leba;-><init>(Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;)V

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 118
    :cond_1
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->mTouchSlop:I

    .line 119
    return-void
.end method

.method public static synthetic a(Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;Z)Z
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTp:Z

    return v0
.end method

.method private eB(Z)V
    .locals 1

    .prologue
    .line 259
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->Vs:Landroid/animation/LayoutTransition;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 260
    return-void

    .line 259
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lebb;)V
    .locals 0

    .prologue
    .line 287
    iput-object p1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTq:Lebb;

    .line 288
    return-void
.end method

.method public final a(Leen;)V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0, p1}, Lcom/google/android/search/suggest/SuggestionListView;->a(Leen;)V

    .line 311
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTi:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0, p1}, Lcom/google/android/search/suggest/SuggestionListView;->a(Leen;)V

    .line 312
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTj:Lcom/google/android/search/suggest/SuggestionStripView;

    invoke-virtual {v0, p1}, Lcom/google/android/search/suggest/SuggestionStripView;->b(Leen;)V

    .line 313
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTl:Lcom/google/android/search/suggest/ScrollableSuggestionListView;

    invoke-virtual {v0, p1}, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->a(Leen;)V

    .line 314
    return-void
.end method

.method public final a(Leey;Leeo;Lesm;Lela;Leax;)V
    .locals 6

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTm:Leld;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    iget-object v5, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTm:Leld;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/search/suggest/SuggestionListView;->a(Leey;Leeo;Lesm;Lela;Leld;)V

    .line 302
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTi:Lcom/google/android/search/suggest/SuggestionListView;

    iget-object v5, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTm:Leld;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/search/suggest/SuggestionListView;->a(Leey;Leeo;Lesm;Lela;Leld;)V

    .line 303
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTj:Lcom/google/android/search/suggest/SuggestionStripView;

    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTm:Leld;

    invoke-virtual {v0, p3, v1, p2}, Lcom/google/android/search/suggest/SuggestionStripView;->a(Lesm;Leld;Leeo;)V

    .line 304
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTk:Lcom/google/android/search/shared/overlay/GetGoogleNowView;

    invoke-virtual {v0, p5}, Lcom/google/android/search/shared/overlay/GetGoogleNowView;->a(Leax;)V

    .line 305
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTl:Lcom/google/android/search/suggest/ScrollableSuggestionListView;

    iget-object v5, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTm:Leld;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->a(Leey;Leeo;Lesm;Lela;Leld;)V

    .line 306
    return-void
.end method

.method public final a(Leld;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTm:Leld;

    .line 188
    return-void
.end method

.method public final a(Lelj;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTt:Lelj;

    .line 412
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;I)V
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/search/suggest/SuggestionListView;->e(Ljava/lang/String;Ljava/util/List;I)V

    .line 335
    return-void
.end method

.method public final aB(Landroid/view/View;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 142
    iget-boolean v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTp:Z

    if-eqz v1, :cond_0

    iput-boolean v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTp:Z

    const/4 v1, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTo:Ljava/util/Map;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTo:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public final amA()V
    .locals 2

    .prologue
    .line 374
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTk:Lcom/google/android/search/shared/overlay/GetGoogleNowView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/overlay/GetGoogleNowView;->setEnabled(Z)V

    .line 375
    return-void
.end method

.method public final amB()V
    .locals 12

    .prologue
    const/16 v11, 0x21

    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 416
    new-instance v0, Lelj;

    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTt:Lelj;

    iget v1, v1, Lelj;->ceD:I

    new-array v2, v10, [I

    iget-object v3, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTj:Lcom/google/android/search/suggest/SuggestionStripView;

    const/16 v4, 0x82

    invoke-virtual {v3, v4}, Lcom/google/android/search/suggest/SuggestionStripView;->hb(I)I

    move-result v3

    aput v3, v2, v8

    iget-object v3, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTi:Lcom/google/android/search/suggest/SuggestionListView;

    const/16 v4, 0x82

    invoke-virtual {v3, v4}, Lcom/google/android/search/suggest/SuggestionListView;->hb(I)I

    move-result v3

    aput v3, v2, v9

    iget-object v3, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTt:Lelj;

    iget v3, v3, Lelj;->ceE:I

    aput v3, v2, v7

    invoke-static {v2}, Leln;->p([I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTt:Lelj;

    iget v3, v3, Lelj;->ceF:I

    iget-object v4, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTt:Lelj;

    iget v4, v4, Lelj;->ceG:I

    new-array v5, v10, [I

    iget-object v6, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTj:Lcom/google/android/search/suggest/SuggestionStripView;

    invoke-virtual {v6, v7}, Lcom/google/android/search/suggest/SuggestionStripView;->hb(I)I

    move-result v6

    aput v6, v5, v8

    iget-object v6, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTi:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v6, v7}, Lcom/google/android/search/suggest/SuggestionListView;->hb(I)I

    move-result v6

    aput v6, v5, v9

    iget-object v6, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTt:Lelj;

    iget v6, v6, Lelj;->ceH:I

    aput v6, v5, v7

    invoke-static {v5}, Leln;->p([I)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lelj;-><init>(IIIII)V

    .line 427
    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v1, v0}, Lcom/google/android/search/suggest/SuggestionListView;->a(Lelj;)V

    .line 429
    new-instance v0, Lelj;

    new-array v1, v7, [I

    iget-object v2, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v2, v11}, Lcom/google/android/search/suggest/SuggestionListView;->hb(I)I

    move-result v2

    aput v2, v1, v8

    iget-object v2, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTt:Lelj;

    iget v2, v2, Lelj;->ceD:I

    aput v2, v1, v9

    invoke-static {v1}, Leln;->p([I)I

    move-result v1

    new-array v2, v7, [I

    iget-object v3, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTi:Lcom/google/android/search/suggest/SuggestionListView;

    const/16 v4, 0x82

    invoke-virtual {v3, v4}, Lcom/google/android/search/suggest/SuggestionListView;->hb(I)I

    move-result v3

    aput v3, v2, v8

    iget-object v3, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTt:Lelj;

    iget v3, v3, Lelj;->ceE:I

    aput v3, v2, v9

    invoke-static {v2}, Leln;->p([I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTt:Lelj;

    iget v3, v3, Lelj;->ceF:I

    iget-object v4, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTt:Lelj;

    iget v4, v4, Lelj;->ceG:I

    new-array v5, v7, [I

    iget-object v6, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTi:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v6, v7}, Lcom/google/android/search/suggest/SuggestionListView;->hb(I)I

    move-result v6

    aput v6, v5, v8

    iget-object v6, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTt:Lelj;

    iget v6, v6, Lelj;->ceH:I

    aput v6, v5, v9

    invoke-static {v5}, Leln;->p([I)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lelj;-><init>(IIIII)V

    .line 440
    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTj:Lcom/google/android/search/suggest/SuggestionStripView;

    invoke-virtual {v1, v0}, Lcom/google/android/search/suggest/SuggestionStripView;->a(Lelj;)V

    .line 442
    new-instance v0, Lelj;

    new-array v1, v10, [I

    iget-object v2, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTj:Lcom/google/android/search/suggest/SuggestionStripView;

    invoke-virtual {v2, v11}, Lcom/google/android/search/suggest/SuggestionStripView;->hb(I)I

    move-result v2

    aput v2, v1, v8

    iget-object v2, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v2, v11}, Lcom/google/android/search/suggest/SuggestionListView;->hb(I)I

    move-result v2

    aput v2, v1, v9

    iget-object v2, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTt:Lelj;

    iget v2, v2, Lelj;->ceD:I

    aput v2, v1, v7

    invoke-static {v1}, Leln;->p([I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTt:Lelj;

    iget v2, v2, Lelj;->ceE:I

    iget-object v3, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTt:Lelj;

    iget v3, v3, Lelj;->ceF:I

    iget-object v4, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTt:Lelj;

    iget v4, v4, Lelj;->ceG:I

    iget-object v5, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTt:Lelj;

    iget v5, v5, Lelj;->ceH:I

    invoke-direct/range {v0 .. v5}, Lelj;-><init>(IIIII)V

    .line 449
    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTi:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v1, v0}, Lcom/google/android/search/suggest/SuggestionListView;->a(Lelj;)V

    .line 450
    return-void
.end method

.method protected final amC()V
    .locals 1

    .prologue
    .line 454
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTp:Z

    .line 455
    return-void
.end method

.method public final amz()V
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTj:Lcom/google/android/search/suggest/SuggestionStripView;

    invoke-virtual {v0}, Lcom/google/android/search/suggest/SuggestionStripView;->aoi()V

    .line 319
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/util/List;I)V
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTi:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/search/suggest/SuggestionListView;->e(Ljava/lang/String;Ljava/util/List;I)V

    .line 340
    return-void
.end method

.method public final c(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTi:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0, p1}, Lcom/google/android/search/suggest/SuggestionListView;->d(Landroid/view/View$OnClickListener;)V

    .line 364
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/util/List;I)V
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTj:Lcom/google/android/search/suggest/SuggestionStripView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/search/suggest/SuggestionStripView;->f(Ljava/lang/String;Ljava/util/List;I)V

    .line 345
    return-void
.end method

.method public final d(Ljava/lang/String;Ljava/util/List;I)V
    .locals 3

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTl:Lcom/google/android/search/suggest/ScrollableSuggestionListView;

    invoke-virtual {p0}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a071c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->setTitle(Ljava/lang/CharSequence;)V

    .line 352
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTl:Lcom/google/android/search/suggest/ScrollableSuggestionListView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->e(Ljava/lang/String;Ljava/util/List;I)V

    .line 353
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 214
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 222
    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 227
    .line 228
    invoke-virtual {p0, p2}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->aB(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    move v1, v2

    .line 229
    :goto_0
    if-eqz v0, :cond_0

    .line 230
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v4

    float-to-int v4, v4

    add-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->ff:I

    add-int/2addr v3, v4

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 231
    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->aB(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 234
    :cond_0
    if-lez v1, :cond_1

    .line 235
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 236
    invoke-virtual {p0}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->getHeight()I

    move-result v3

    invoke-virtual {p1, v2, v1, v0, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 237
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    .line 238
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 242
    :goto_1
    return v0

    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    goto :goto_1
.end method

.method public final eC(Z)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 323
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0, v1}, Lcom/google/android/search/suggest/SuggestionListView;->setVisibility(I)V

    .line 324
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTi:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0, v1}, Lcom/google/android/search/suggest/SuggestionListView;->setVisibility(I)V

    .line 325
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTj:Lcom/google/android/search/suggest/SuggestionStripView;

    invoke-virtual {v0, v1}, Lcom/google/android/search/suggest/SuggestionStripView;->setVisibility(I)V

    .line 326
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTk:Lcom/google/android/search/shared/overlay/GetGoogleNowView;

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/overlay/GetGoogleNowView;->setVisibility(I)V

    .line 327
    if-eqz p1, :cond_0

    .line 328
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTl:Lcom/google/android/search/suggest/ScrollableSuggestionListView;

    invoke-virtual {v0, v1}, Lcom/google/android/search/suggest/ScrollableSuggestionListView;->setVisibility(I)V

    .line 330
    :cond_0
    return-void
.end method

.method public final eD(Z)V
    .locals 2

    .prologue
    .line 357
    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTi:Lcom/google/android/search/suggest/SuggestionListView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/search/suggest/SuggestionListView;->ho(I)V

    .line 358
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTi:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0, p1}, Lcom/google/android/search/suggest/SuggestionListView;->eO(Z)V

    .line 359
    return-void

    .line 357
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final eE(Z)V
    .locals 1

    .prologue
    .line 368
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->eB(Z)V

    .line 369
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTn:Z

    .line 370
    return-void

    .line 369
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final eF(Z)V
    .locals 2

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTk:Lcom/google/android/search/shared/overlay/GetGoogleNowView;

    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/overlay/GetGoogleNowView;->setEnabled(Z)V

    .line 385
    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTk:Lcom/google/android/search/shared/overlay/GetGoogleNowView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/search/shared/overlay/GetGoogleNowView;->setVisibility(I)V

    .line 386
    return-void

    .line 385
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final ha(I)V
    .locals 0

    .prologue
    .line 194
    iput p1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->ff:I

    .line 195
    return-void
.end method

.method public final hb(I)I
    .locals 5

    .prologue
    const/4 v0, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 390
    sparse-switch p1, :sswitch_data_0

    .line 405
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 395
    :sswitch_0
    new-array v0, v0, [I

    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v1, p1}, Lcom/google/android/search/suggest/SuggestionListView;->hb(I)I

    move-result v1

    aput v1, v0, v2

    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTj:Lcom/google/android/search/suggest/SuggestionStripView;

    invoke-virtual {v1, p1}, Lcom/google/android/search/suggest/SuggestionStripView;->hb(I)I

    move-result v1

    aput v1, v0, v3

    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTi:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v1, p1}, Lcom/google/android/search/suggest/SuggestionListView;->hb(I)I

    move-result v1

    aput v1, v0, v4

    invoke-static {v0}, Leln;->p([I)I

    move-result v0

    .line 400
    :goto_0
    return v0

    :sswitch_1
    new-array v0, v0, [I

    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTi:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v1, p1}, Lcom/google/android/search/suggest/SuggestionListView;->hb(I)I

    move-result v1

    aput v1, v0, v2

    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTj:Lcom/google/android/search/suggest/SuggestionStripView;

    invoke-virtual {v1, p1}, Lcom/google/android/search/suggest/SuggestionStripView;->hb(I)I

    move-result v1

    aput v1, v0, v3

    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v1, p1}, Lcom/google/android/search/suggest/SuggestionListView;->hb(I)I

    move-result v1

    aput v1, v0, v4

    invoke-static {v0}, Leln;->p([I)I

    move-result v0

    goto :goto_0

    .line 390
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x11 -> :sswitch_0
        0x21 -> :sswitch_1
        0x42 -> :sswitch_0
        0x82 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 148
    const v0, 0x7f1103b8

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/suggest/SuggestionListView;

    iput-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    .line 149
    const v0, 0x7f1103ba

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/suggest/SuggestionListView;

    iput-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTi:Lcom/google/android/search/suggest/SuggestionListView;

    .line 151
    const v0, 0x7f1103b9

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/suggest/SuggestionStripView;

    iput-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTj:Lcom/google/android/search/suggest/SuggestionStripView;

    .line 152
    const v0, 0x7f1101ef

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/overlay/GetGoogleNowView;

    iput-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTk:Lcom/google/android/search/shared/overlay/GetGoogleNowView;

    .line 153
    const v0, 0x7f1103b5

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/suggest/ScrollableSuggestionListView;

    iput-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTl:Lcom/google/android/search/suggest/ScrollableSuggestionListView;

    .line 156
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/search/suggest/SuggestionListView;->s(ZZ)V

    .line 157
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTi:Lcom/google/android/search/suggest/SuggestionListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/google/android/search/suggest/SuggestionListView;->s(ZZ)V

    .line 159
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTf:Leda;

    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0, v1}, Leda;->aC(Landroid/view/View;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTg:Leda;

    iget-object v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0, v1}, Leda;->aC(Landroid/view/View;)V

    .line 161
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 268
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 270
    if-nez v0, :cond_1

    .line 271
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTr:F

    .line 272
    iput-boolean v2, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTs:Z

    .line 283
    :cond_0
    :goto_0
    return v2

    .line 273
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTs:Z

    if-nez v0, :cond_0

    .line 274
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTr:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->mTouchSlop:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 276
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTs:Z

    .line 277
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTq:Lebb;

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTq:Lebb;

    invoke-interface {v0}, Lebb;->amD()V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 250
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 252
    iget-boolean v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTn:Z

    if-eqz v0, :cond_0

    .line 253
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->bTn:Z

    .line 254
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/search/shared/overlay/OverlaySuggestionsContainer;->eB(Z)V

    .line 256
    :cond_0
    return-void
.end method
