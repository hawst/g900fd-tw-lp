.class public Lcom/google/android/search/shared/service/ClientConfig;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Leti;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final bUT:Lcom/google/android/search/shared/service/ClientConfig;


# instance fields
.field private final bUU:Lcom/google/android/shared/search/SearchBoxStats;

.field private final bqt:Lcom/google/android/shared/util/BitFlags;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 177
    new-instance v0, Lcom/google/android/search/shared/service/ClientConfig;

    const-wide/32 v2, 0x880c001

    const-string v1, "default"

    const-string v4, "android-search-app"

    invoke-static {v1, v4}, Lcom/google/android/shared/search/SearchBoxStats;->aA(Ljava/lang/String;Ljava/lang/String;)Lehj;

    move-result-object v1

    invoke-virtual {v1}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/search/shared/service/ClientConfig;-><init>(JLcom/google/android/shared/search/SearchBoxStats;)V

    sput-object v0, Lcom/google/android/search/shared/service/ClientConfig;->bUT:Lcom/google/android/search/shared/service/ClientConfig;

    .line 209
    new-instance v0, Lecf;

    invoke-direct {v0}, Lecf;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/service/ClientConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLcom/google/android/shared/search/SearchBoxStats;)V
    .locals 3

    .prologue
    .line 198
    new-instance v0, Lcom/google/android/shared/util/BitFlags;

    const-class v1, Lcom/google/android/search/shared/service/ClientConfig;

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/shared/util/BitFlags;-><init>(Ljava/lang/Class;J)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/search/shared/service/ClientConfig;-><init>(Lcom/google/android/shared/util/BitFlags;Lcom/google/android/shared/search/SearchBoxStats;)V

    .line 199
    return-void
.end method

.method private constructor <init>(Lcom/google/android/shared/util/BitFlags;Lcom/google/android/shared/search/SearchBoxStats;)V
    .locals 0

    .prologue
    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202
    iput-object p1, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    .line 203
    iput-object p2, p0, Lcom/google/android/search/shared/service/ClientConfig;->bUU:Lcom/google/android/shared/search/SearchBoxStats;

    .line 204
    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/shared/util/BitFlags;Lcom/google/android/shared/search/SearchBoxStats;B)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/google/android/search/shared/service/ClientConfig;-><init>(Lcom/google/android/shared/util/BitFlags;Lcom/google/android/shared/search/SearchBoxStats;)V

    return-void
.end method


# virtual methods
.method public final CR()Z
    .locals 4

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x1000000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final YA()Z
    .locals 4

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x800000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final a(Letj;)V
    .locals 3

    .prologue
    .line 395
    const-string v0, "ClientConfig"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 396
    const-string v0, "flags"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v1}, Lcom/google/android/shared/util/BitFlags;->auA()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 397
    const-string v0, "client stats"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/service/ClientConfig;->bUU:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 398
    return-void
.end method

.method public final amS()Z
    .locals 4

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x20

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final amT()Z
    .locals 4

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final amU()Z
    .locals 4

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final amV()Z
    .locals 4

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final amW()Z
    .locals 4

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x8

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final amX()Z
    .locals 4

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x8000000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final amY()Z
    .locals 4

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x10

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final amZ()Z
    .locals 4

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x40

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final ana()Z
    .locals 4

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x80

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final anb()Z
    .locals 4

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x1000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final anc()Z
    .locals 4

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x200

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final and()Z
    .locals 4

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x600

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final ane()Z
    .locals 4

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x40000000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final anf()Z
    .locals 4

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x80000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final ang()Z
    .locals 4

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x80000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x20000000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final anh()Z
    .locals 4

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x2000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final ani()Z
    .locals 4

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x4000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final anj()Z
    .locals 4

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x8000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final ank()Z
    .locals 4

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x10000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final anl()Z
    .locals 4

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x20000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final anm()Z
    .locals 4

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x40000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final ann()Z
    .locals 4

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x100000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final ano()Z
    .locals 4

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x200000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final anp()Z
    .locals 4

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x400000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final anq()Z
    .locals 4

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x2000000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final anr()Z
    .locals 4

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x4000000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final ans()Z
    .locals 4

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x10000000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final ant()Z
    .locals 4

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide v2, 0x80000000L

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final anu()Z
    .locals 4

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide v2, 0x100000000L

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final anv()Lcom/google/android/shared/search/SearchBoxStats;
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bUU:Lcom/google/android/shared/search/SearchBoxStats;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 379
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 390
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ClientConfig[mFlags="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v1}, Lcom/google/android/shared/util/BitFlags;->auA()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mClientStats="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/shared/service/ClientConfig;->bUU:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 384
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 385
    iget-object v0, p0, Lcom/google/android/search/shared/service/ClientConfig;->bUU:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 386
    return-void
.end method
