.class public Lcom/google/android/search/core/service/SearchService;
.super Landroid/app/Service;
.source "PG"


# instance fields
.field private aMD:Leqo;

.field private aZo:Ldah;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bqc:Landroid/util/LongSparseArray;

.field private final bqd:Ldax;

.field private final bqe:Ljava/lang/Runnable;

.field private final bqf:Ljava/lang/Runnable;

.field public bqg:Ldao;

.field private bqh:Lecg;

.field private bqi:Ldah;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 184
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    .line 187
    new-instance v0, Ldax;

    invoke-direct {v0, p0}, Ldax;-><init>(Lcom/google/android/search/core/service/SearchService;)V

    iput-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqd:Ldax;

    .line 189
    new-instance v0, Ldau;

    const-string v1, "update active client"

    invoke-direct {v0, p0, v1}, Ldau;-><init>(Lcom/google/android/search/core/service/SearchService;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqe:Ljava/lang/Runnable;

    .line 197
    new-instance v0, Ldav;

    const-string v1, "set service started"

    invoke-direct {v0, p0, v1}, Ldav;-><init>(Lcom/google/android/search/core/service/SearchService;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqf:Ljava/lang/Runnable;

    .line 754
    return-void
.end method

.method private Vg()V
    .locals 3

    .prologue
    .line 457
    iget-object v1, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    monitor-enter v1

    .line 458
    :try_start_0
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->aZo:Ldah;

    iget-object v2, p0, Lcom/google/android/search/core/service/SearchService;->bqi:Ldah;

    if-eq v0, v2, :cond_0

    .line 459
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->aMD:Leqo;

    invoke-interface {v0}, Leqo;->Du()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 460
    invoke-virtual {p0}, Lcom/google/android/search/core/service/SearchService;->Vh()V

    .line 465
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 462
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->aMD:Leqo;

    iget-object v2, p0, Lcom/google/android/search/core/service/SearchService;->bqe:Ljava/lang/Runnable;

    invoke-interface {v0, v2}, Leqo;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 465
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private Vj()V
    .locals 2

    .prologue
    .line 518
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->aZo:Ldah;

    if-nez v0, :cond_0

    .line 519
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 520
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldah;

    invoke-direct {p0, v0}, Lcom/google/android/search/core/service/SearchService;->d(Ldah;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 525
    :cond_0
    return-void

    .line 519
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private a(Ldah;ZZ)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 407
    iget-object v3, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    monitor-enter v3

    .line 408
    if-eqz p1, :cond_5

    .line 409
    :try_start_0
    iget-object v2, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {p1}, Ldah;->getId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v2

    if-eq v2, p1, :cond_2

    .line 410
    const-string v2, "SearchService"

    const-string v4, "Ignoring already detached client"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x4

    invoke-static {v6, v2, v4, v5}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 413
    iget-object v2, p0, Lcom/google/android/search/core/service/SearchService;->aZo:Ldah;

    if-eq v2, p1, :cond_0

    move v2, v0

    :goto_0
    invoke-static {v2}, Lifv;->gY(Z)V

    .line 414
    iget-object v2, p0, Lcom/google/android/search/core/service/SearchService;->bqi:Ldah;

    if-eq v2, p1, :cond_1

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 415
    monitor-exit v3

    .line 440
    :goto_2
    return-void

    :cond_0
    move v2, v1

    .line 413
    goto :goto_0

    :cond_1
    move v0, v1

    .line 414
    goto :goto_1

    .line 417
    :cond_2
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->aZo:Ldah;

    if-ne p1, v0, :cond_3

    .line 422
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/core/service/SearchService;->aZo:Ldah;

    .line 423
    invoke-direct {p0}, Lcom/google/android/search/core/service/SearchService;->Vg()V

    .line 425
    invoke-virtual {p1}, Ldah;->Ux()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 426
    invoke-virtual {p0}, Lcom/google/android/search/core/service/SearchService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->aMD:Leqo;

    iget-object v1, p0, Lcom/google/android/search/core/service/SearchService;->bqf:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 429
    :cond_3
    if-eqz p2, :cond_4

    .line 431
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {p1}, Ldah;->getId()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Landroid/util/LongSparseArray;->delete(J)V

    .line 432
    invoke-direct {p0, p1}, Lcom/google/android/search/core/service/SearchService;->f(Ldah;)V

    .line 434
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/search/core/service/SearchService;->Vi()V

    .line 435
    if-eqz p3, :cond_5

    .line 437
    invoke-direct {p0}, Lcom/google/android/search/core/service/SearchService;->Vj()V

    .line 440
    :cond_5
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method private d(Ldah;)Z
    .locals 4

    .prologue
    .line 368
    iget-object v1, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    monitor-enter v1

    .line 369
    :try_start_0
    invoke-virtual {p1}, Ldah;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {p1}, Ldah;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_1

    .line 370
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->aZo:Ldah;

    if-eq p1, v0, :cond_0

    .line 372
    iput-object p1, p0, Lcom/google/android/search/core/service/SearchService;->aZo:Ldah;

    .line 373
    invoke-direct {p0}, Lcom/google/android/search/core/service/SearchService;->Vg()V

    .line 377
    :cond_0
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 380
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 382
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private f(Ldah;)V
    .locals 3

    .prologue
    .line 479
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->aMD:Leqo;

    invoke-interface {v0}, Leqo;->Du()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480
    invoke-virtual {p1}, Ldah;->ca()V

    .line 489
    :goto_0
    return-void

    .line 482
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->aMD:Leqo;

    new-instance v1, Ldaw;

    const-string v2, "dispose AttachedClient"

    invoke-direct {v1, p0, v2, p1}, Ldaw;-><init>(Lcom/google/android/search/core/service/SearchService;Ljava/lang/String;Ldah;)V

    invoke-interface {v0, v1}, Leqo;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public final UH()Lcjt;
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-virtual {v0}, Ldao;->UH()Lcjt;

    move-result-object v0

    return-object v0
.end method

.method public final Vh()V
    .locals 3

    .prologue
    .line 470
    iget-object v1, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    monitor-enter v1

    .line 471
    :try_start_0
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->aZo:Ldah;

    iget-object v2, p0, Lcom/google/android/search/core/service/SearchService;->bqi:Ldah;

    if-eq v0, v2, :cond_0

    .line 472
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->aZo:Ldah;

    iput-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqi:Ldah;

    .line 473
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    iget-object v2, p0, Lcom/google/android/search/core/service/SearchService;->aZo:Ldah;

    invoke-virtual {v0, v2}, Ldao;->a(Ldah;)V

    .line 475
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final Vi()V
    .locals 3

    .prologue
    .line 503
    const/4 v0, 0x0

    move v1, v0

    .line 504
    :goto_0
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 505
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldah;

    .line 506
    invoke-virtual {v0}, Ldah;->UC()Z

    move-result v2

    if-nez v2, :cond_0

    .line 507
    invoke-direct {p0, v0}, Lcom/google/android/search/core/service/SearchService;->f(Ldah;)V

    .line 508
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->removeAt(I)V

    goto :goto_0

    .line 512
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 514
    goto :goto_0

    .line 515
    :cond_1
    return-void
.end method

.method protected final Vk()Ldax;
    .locals 1

    .prologue
    .line 733
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqd:Ldax;

    return-object v0
.end method

.method protected final Vl()Landroid/util/LongSparseArray;
    .locals 1

    .prologue
    .line 738
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    return-object v0
.end method

.method protected final Vm()Ldah;
    .locals 1

    .prologue
    .line 743
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->aZo:Ldah;

    return-object v0
.end method

.method public final a(JLecm;Lcom/google/android/search/shared/service/ClientConfig;)Lecg;
    .locals 9

    .prologue
    .line 283
    iget-object v8, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    monitor-enter v8

    .line 285
    :try_start_0
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldah;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/search/core/service/SearchService;->a(Ldah;ZZ)V

    .line 286
    new-instance v1, Ldah;

    iget-object v5, p0, Lcom/google/android/search/core/service/SearchService;->bqh:Lecg;

    move-wide v2, p1

    move-object v4, p0

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v7}, Ldah;-><init>(JLcom/google/android/search/core/service/SearchService;Lecg;Lecm;Lcom/google/android/search/shared/service/ClientConfig;)V

    .line 289
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2, v1}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 290
    invoke-virtual {p0}, Lcom/google/android/search/core/service/SearchService;->Vi()V

    .line 292
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v1

    .line 293
    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0
.end method

.method public final a(JJLandroid/os/Bundle;Landroid/os/Bundle;I)V
    .locals 9
    .param p5    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 299
    iget-object v7, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    monitor-enter v7

    .line 300
    :try_start_0
    iget-object v1, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {v1, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldah;

    .line 301
    iget-object v2, p0, Lcom/google/android/search/core/service/SearchService;->aZo:Ldah;

    if-ne v1, v2, :cond_0

    .line 302
    const-string v2, "SearchService"

    const-string v3, "Attempting to re-start already active client."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x5

    invoke-static {v5, v2, v3, v4}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 304
    :cond_0
    if-eqz v1, :cond_2

    move-wide v2, p3

    move-object v4, p5

    move-object v5, p6

    move/from16 v6, p7

    .line 305
    invoke-virtual/range {v1 .. v6}, Ldah;->a(JLandroid/os/Bundle;Landroid/os/Bundle;I)V

    .line 306
    iget-object v2, p0, Lcom/google/android/search/core/service/SearchService;->aZo:Ldah;

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Ldah;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/service/ClientConfig;->amT()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 307
    invoke-direct {p0, v1}, Lcom/google/android/search/core/service/SearchService;->d(Ldah;)Z

    move-result v0

    const-string v1, "Can\'t activate newly-started client"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 310
    :cond_2
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 306
    :cond_3
    and-int/lit8 v2, p7, 0x2

    if-nez v2, :cond_1

    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 310
    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method protected final a(Ldao;Lecg;Leqo;)V
    .locals 0

    .prologue
    .line 749
    iput-object p1, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    .line 750
    iput-object p2, p0, Lcom/google/android/search/core/service/SearchService;->bqh:Lecg;

    .line 751
    iput-object p3, p0, Lcom/google/android/search/core/service/SearchService;->aMD:Leqo;

    .line 752
    return-void
.end method

.method public final aj(J)V
    .locals 5

    .prologue
    .line 348
    iget-object v1, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    monitor-enter v1

    .line 349
    :try_start_0
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldah;

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/search/core/service/SearchService;->a(Ldah;ZZ)V

    .line 350
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c(JZ)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 334
    iget-object v2, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    monitor-enter v2

    .line 335
    :try_start_0
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldah;

    .line 336
    if-eqz v0, :cond_2

    .line 337
    invoke-virtual {v0}, Ldah;->Uu()V

    .line 338
    if-eqz p3, :cond_0

    .line 339
    invoke-virtual {v0}, Ldah;->Uv()V

    .line 341
    :cond_0
    const/4 v3, 0x0

    if-nez p3, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-direct {p0, v0, v3, v1}, Lcom/google/android/search/core/service/SearchService;->a(Ldah;ZZ)V

    .line 343
    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final c(Ldah;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 358
    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/search/core/service/SearchService;->a(Ldah;ZZ)V

    .line 359
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 709
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 710
    new-instance v2, Letj;

    invoke-virtual {p0}, Lcom/google/android/search/core/service/SearchService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {v2, v0}, Letj;-><init>(Landroid/content/res/Resources;)V

    .line 711
    const-string v0, "Attached Clients"

    invoke-virtual {v2, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {v1}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {v0, v4, v5}, Letn;->be(J)V

    .line 713
    invoke-virtual {v2}, Letj;->avJ()Letj;

    move-result-object v3

    .line 714
    iget-object v4, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    monitor-enter v4

    .line 715
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 716
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldah;

    .line 717
    iget-object v5, p0, Lcom/google/android/search/core/service/SearchService;->aZo:Ldah;

    if-ne v0, v5, :cond_0

    .line 718
    const-string v5, "Active:"

    invoke-virtual {v3, v5}, Letj;->lv(Ljava/lang/String;)V

    .line 720
    :cond_0
    invoke-virtual {v3, v0}, Letj;->b(Leti;)V

    .line 715
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 722
    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 723
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-virtual {v2, v0}, Letj;->b(Leti;)V

    .line 724
    invoke-virtual {v2, p2}, Letj;->c(Ljava/io/PrintWriter;)V

    .line 725
    return-void

    .line 722
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0
.end method

.method public final e(Ldah;)Z
    .locals 2

    .prologue
    .line 394
    iget-object v1, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    monitor-enter v1

    .line 395
    :try_start_0
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->aZo:Ldah;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqi:Ldah;

    if-ne p1, v0, :cond_0

    .line 397
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 400
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 402
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqd:Ldax;

    return-object v0
.end method

.method public onCreate()V
    .locals 7

    .prologue
    .line 225
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 226
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    .line 227
    iget-object v0, v2, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->aus()Leqo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/service/SearchService;->aMD:Leqo;

    .line 228
    new-instance v0, Ldao;

    invoke-virtual {p0}, Lcom/google/android/search/core/service/SearchService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v3, Ldaz;

    invoke-virtual {p0}, Lcom/google/android/search/core/service/SearchService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2}, Lgql;->aJq()Lhhq;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5}, Ldaz;-><init>(Lcom/google/android/search/core/service/SearchService;Landroid/content/Context;Lhhq;)V

    new-instance v4, Lenh;

    invoke-direct {v4, p0}, Lenh;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lgql;->aJU()Lgpu;

    move-result-object v5

    invoke-virtual {v5}, Lgpu;->aJM()Ldmh;

    move-result-object v5

    new-instance v6, Lbyl;

    invoke-direct {v6}, Lbyl;-><init>()V

    invoke-direct/range {v0 .. v6}, Ldao;-><init>(Landroid/content/Context;Lgql;Ldaz;Leqp;Ldmh;Lbyl;)V

    iput-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    .line 233
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-virtual {v0}, Ldao;->create()V

    .line 234
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->aMD:Leqo;

    const-class v1, Lecg;

    iget-object v2, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-static {v0, v1, v2}, Lers;->a(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lecg;

    iput-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqh:Lecg;

    .line 246
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-virtual {v0}, Ldao;->Vb()V

    .line 247
    return-void
.end method

.method public onDestroy()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 680
    iget-object v2, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    monitor-enter v2

    .line 681
    :try_start_0
    iget-object v1, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {v1}, Landroid/util/LongSparseArray;->size()I

    move-result v3

    move v1, v0

    .line 682
    :goto_0
    if-ge v1, v3, :cond_0

    .line 683
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldah;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, v0, v4, v5}, Lcom/google/android/search/core/service/SearchService;->a(Ldah;ZZ)V

    .line 682
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 685
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqc:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    .line 686
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 687
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->aMD:Leqo;

    iget-object v1, p0, Lcom/google/android/search/core/service/SearchService;->bqf:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 688
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->aMD:Leqo;

    iget-object v1, p0, Lcom/google/android/search/core/service/SearchService;->bqe:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 689
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-virtual {v0}, Ldao;->destroy()V

    .line 696
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 697
    return-void

    .line 686
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/google/android/search/core/service/SearchService;->Vi()V

    .line 259
    invoke-direct {p0}, Lcom/google/android/search/core/service/SearchService;->Vj()V

    .line 260
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 9

    .prologue
    const/16 v5, 0x171

    const/4 v7, -0x1

    const/4 v0, 0x1

    const/4 v6, 0x2

    const/4 v1, 0x0

    .line 538
    if-eqz p1, :cond_19

    .line 541
    const-string v2, "com.google.android.search.core.action.BTSTARTUP"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 547
    const-string v2, "com.google.android.voicesearch.bluetooth.BVRA_SUPPORTED"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 549
    const-string v2, "bt-device-address"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 551
    invoke-virtual {p0}, Lcom/google/android/search/core/service/SearchService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0, v2}, Lbyh;->a(Landroid/content/Context;ZLjava/lang/String;)V

    .line 552
    iget-object v3, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-virtual {v3, v0, v1, v2}, Ldao;->a(ZZLjava/lang/String;)V

    move v0, v6

    .line 652
    :goto_0
    return v0

    .line 554
    :cond_0
    const-string v2, "com.google.android.search.core.action.BTSTOP"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 555
    const-string v0, "bt-device-address"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 557
    invoke-virtual {p0}, Lcom/google/android/search/core/service/SearchService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1, v0}, Lbyh;->a(Landroid/content/Context;ZLjava/lang/String;)V

    .line 558
    iget-object v2, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-virtual {v2, v1, v1, v0}, Ldao;->a(ZZLjava/lang/String;)V

    move v0, v6

    .line 560
    goto :goto_0

    .line 561
    :cond_1
    const-string v2, "com.google.android.search.core.action.WIRED_HEADSET"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 562
    const-string v2, "com.google.android.search.core.extra.WIRED_HEADSET_IS_PLUGGED"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 564
    new-instance v2, Lbyl;

    invoke-direct {v2}, Lbyl;-><init>()V

    const/4 v3, 0x0

    new-instance v4, Landroid/content/IntentFilter;

    const-string v8, "android.intent.action.HEADSET_PLUG"

    invoke-direct {v4, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3, v4}, Lcom/google/android/search/core/service/SearchService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v3

    if-eqz v3, :cond_4

    const-string v4, "microphone"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v0, :cond_4

    :goto_1
    if-eqz v0, :cond_3

    if-eqz v7, :cond_5

    invoke-virtual {v2}, Lbyl;->BR()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lbyl;->BW()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lbyl;->BJ()Lhym;

    move-result-object v0

    invoke-static {}, Lbyl;->BJ()Lhym;

    move-result-object v1

    invoke-virtual {v1}, Lhym;->aTW()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lhym;->lC(I)V

    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lhdn;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v2, 0x7f0a06dc

    const v3, 0x7f0a06dd

    const v4, 0x7f02015e

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lbyh;->a(Landroid/content/Context;Landroid/content/Intent;IIII)V

    .line 565
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-virtual {v0, v7}, Ldao;->cT(Z)V

    move v0, v6

    .line 566
    goto/16 :goto_0

    :cond_4
    move v0, v1

    .line 564
    goto :goto_1

    :cond_5
    invoke-static {p0, v5}, Lbyh;->d(Landroid/content/Context;I)V

    goto :goto_2

    .line 567
    :cond_6
    const-string v2, "com.google.android.search.core.action.CHARGING_CHANGE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 568
    const-string v0, "com.google.android.search.core.extra.IS_CHARGING"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 569
    iget-object v1, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-virtual {v1, v0}, Ldao;->cV(Z)V

    move v0, v6

    .line 570
    goto/16 :goto_0

    .line 571
    :cond_7
    const-string v2, "com.google.android.search.core.action.SCREEN_STATE_CHANGE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 572
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    const-string v2, "com.google.android.search.core.extra.IS_SCREEN_ON"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Ldao;->cW(Z)V

    move v0, v6

    .line 573
    goto/16 :goto_0

    .line 574
    :cond_8
    const-string v2, "com.google.android.search.core.action.DEVICE_LOCKED_STATE_CHANGE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 575
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    const-string v2, "com.google.android.search.core.extra.IS_LOCKED"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Ldao;->cX(Z)V

    move v0, v6

    .line 576
    goto/16 :goto_0

    .line 577
    :cond_9
    const-string v2, "com.google.android.search.core.action.VOICE_SEARCH_HANDS_FREE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 578
    iget-object v2, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    const-string v3, "com.google.android.search.core.extra.IS_WIRED"

    invoke-virtual {p1, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v2, v1}, Ldao;->cY(Z)V

    goto/16 :goto_0

    .line 580
    :cond_a
    const-string v2, "com.google.android.search.core.action.NOTIFICATION"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 581
    iget-object v2, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    new-instance v3, Lbwp;

    invoke-direct {v3, p1}, Lbwp;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v2, v3, v1, v1}, Ldao;->a(Lbwp;ZZ)V

    goto/16 :goto_0

    .line 584
    :cond_b
    const-string v2, "com.google.android.search.core.action.REFRESH_SERVICE_STATE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 585
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-virtual {v0}, Ldao;->Vb()V

    move v0, v6

    .line 586
    goto/16 :goto_0

    .line 587
    :cond_c
    const-string v2, "com.google.android.search.core.action.SMS_RECEIVED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 588
    invoke-static {p1}, Lbym;->o(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    .line 589
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/util/Pair;

    .line 590
    iget-object v3, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Ldao;->ag(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_d
    move v0, v6

    .line 592
    goto/16 :goto_0

    .line 593
    :cond_e
    const-string v2, "com.google.android.search.core.action.NOTIFICATION_DISMISSED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 596
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    const-string v1, "com.google.android.search.core.extra.NOTIFICATION_FLAG"

    invoke-virtual {p1, v1, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Ldao;->eX(I)V

    move v0, v6

    .line 598
    goto/16 :goto_0

    .line 599
    :cond_f
    const-string v2, "com.google.android.search.core.action.NEW_HOTWORD_MODEL_AVAILABLE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 600
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-virtual {v0}, Ldao;->Nc()V

    move v0, v6

    .line 601
    goto/16 :goto_0

    .line 602
    :cond_10
    const-string v2, "com.google.android.search.core.action.HOTWORD_TRIGGERED_ON_DSP"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 603
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-virtual {v0}, Ldao;->UH()Lcjt;

    move-result-object v0

    const-string v2, "com.google.android.search.core.extra.HOTWORD_BYTES"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    const-string v3, "com.google.android.search.core.extra.HOTWORD_VERIFICATION_MODE"

    invoke-virtual {p1, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "com.google.android.search.core.extra.HOTWORD_SAMPLING_RATE"

    invoke-virtual {p1, v4, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v4, "com.google.android.search.core.extra.HOTWORD_CAPTURE_SESSION"

    invoke-virtual {p1, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v0, v2, v3, v1, v4}, Lcjt;->a([BIII)V

    move v0, v6

    .line 608
    goto/16 :goto_0

    .line 615
    :cond_11
    const-string v2, "com.google.android.search.core.action.ACTIVITY_DETECTED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 616
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    const-string v2, "com.google.android.search.core.driving"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Ldao;->cU(Z)V

    move v0, v6

    .line 617
    goto/16 :goto_0

    .line 618
    :cond_12
    const-string v2, "com.google.android.search.core.action.ENTER_MANUAL_CAR_MODE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 619
    const-string v2, "entry-point"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 622
    iget-object v2, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-virtual {v2, v0, v1}, Ldao;->c(ZI)V

    move v0, v6

    .line 623
    goto/16 :goto_0

    .line 624
    :cond_13
    const-string v0, "com.google.android.search.core.action.EXIT_MANUAL_CAR_MODE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 625
    const-string v0, "exit-point"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 627
    iget-object v2, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-virtual {v2, v1, v0}, Ldao;->c(ZI)V

    move v0, v6

    .line 628
    goto/16 :goto_0

    .line 629
    :cond_14
    const-string v0, "com.google.android.search.core.action.OVERLAY_REVIVE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 630
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-virtual {v0}, Ldao;->UZ()V

    move v0, v6

    .line 631
    goto/16 :goto_0

    .line 632
    :cond_15
    const-string v0, "com.google.android.search.core.action.CLEAR_MANUAL_CAR_MODE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 633
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-virtual {v0}, Ldao;->Va()V

    move v0, v6

    .line 634
    goto/16 :goto_0

    .line 635
    :cond_16
    const-string v0, "com.google.android.search.core.action.HANDS_FREE_OVERLAY_STARTED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 636
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-virtual {v0}, Ldao;->Ve()V

    move v0, v6

    .line 637
    goto/16 :goto_0

    .line 638
    :cond_17
    const-string v0, "com.google.android.search.core.action.HANDS_FREE_OVERLAY_STOPPED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 639
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    const-string v2, "com.google.android.search.core.extra.IS_LOSING_WINDOW_FOCUS"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    const-string v3, "com.google.android.search.core.extra.HANDS_FREE_OVERLAY_ACTION_EXECUTION"

    invoke-virtual {p1, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "com.google.android.search.core.extra.IS_CHANGING_CONFIGURATIONS"

    invoke-virtual {p1, v4, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v2, v3, v1}, Ldao;->b(ZIZ)V

    move v0, v6

    .line 644
    goto/16 :goto_0

    .line 645
    :cond_18
    const-string v0, "com.google.android.search.core.action.GMM_NAV_STATE_CHANGE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 646
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    const-string v2, "com.google.android.search.core.extra.GMM_IS_FOREGROUNDED"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    const-string v3, "com.google.android.search.core.extra.GMM_IS_NAVIGATING"

    invoke-virtual {p1, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    const-string v4, "com.google.android.search.core.extra.GMM_TRAVEL_MODE"

    invoke-virtual {p1, v4, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v2, v3, v1}, Ldao;->a(ZZI)V

    move v0, v6

    .line 650
    goto/16 :goto_0

    .line 652
    :cond_19
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    goto/16 :goto_0
.end method

.method public onTrimMemory(I)V
    .locals 1

    .prologue
    .line 701
    invoke-super {p0, p1}, Landroid/app/Service;->onTrimMemory(I)V

    .line 702
    invoke-static {p1}, Ldmn;->gc(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 703
    iget-object v0, p0, Lcom/google/android/search/core/service/SearchService;->bqg:Ldao;

    invoke-virtual {v0}, Ldao;->eA()V

    .line 705
    :cond_0
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 265
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    .line 266
    const/4 v0, 0x1

    return v0
.end method
