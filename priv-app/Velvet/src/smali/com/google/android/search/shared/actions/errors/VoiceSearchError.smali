.class public Lcom/google/android/search/shared/actions/errors/VoiceSearchError;
.super Lcom/google/android/search/shared/actions/errors/SearchError;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bMI:I

.field private final bNN:I

.field private final bNO:I

.field private final bNP:I

.field private final bNQ:Z

.field private final mRequestId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 118
    new-instance v0, Ldvr;

    invoke-direct {v0}, Ldvr;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/errors/SearchError;-><init>(Landroid/os/Parcel;)V

    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bMI:I

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bNO:I

    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bNP:I

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bNQ:Z

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bNN:I

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->mRequestId:Ljava/lang/String;

    .line 65
    return-void
.end method

.method private constructor <init>(Lcom/google/android/shared/search/Query;IIIZILjava/lang/String;ZLjava/lang/String;Lefr;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p9, p10}, Lcom/google/android/search/shared/actions/errors/SearchError;-><init>(Lcom/google/android/shared/search/Query;Ljava/lang/String;Lefr;)V

    .line 49
    iput p2, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bMI:I

    .line 50
    iput p3, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bNO:I

    .line 51
    iput p4, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bNP:I

    .line 52
    if-eqz p7, :cond_0

    if-eqz p5, :cond_0

    if-eqz p8, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bNQ:Z

    .line 53
    iput p6, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bNN:I

    .line 54
    iput-object p7, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->mRequestId:Ljava/lang/String;

    .line 55
    return-void

    .line 52
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/shared/search/Query;Leiq;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 11
    .param p5    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 32
    invoke-static {p2}, Leno;->e(Leiq;)I

    move-result v2

    invoke-static {p2}, Leno;->k(Leiq;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v3, 0x7f0a0949

    :goto_0
    invoke-static {p2}, Leno;->k(Leiq;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v4, 0x7f0a094a

    :goto_1
    invoke-static {p2}, Leno;->g(Leiq;)Z

    move-result v5

    invoke-static {p2}, Leno;->f(Leiq;)I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-object v7, p3

    move v8, p4

    move-object/from16 v9, p5

    move-object v10, p2

    invoke-direct/range {v0 .. v10}, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;-><init>(Lcom/google/android/shared/search/Query;IIIZILjava/lang/String;ZLjava/lang/String;Lefr;)V

    .line 42
    return-void

    .line 32
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final aiQ()Lcom/google/android/shared/search/Query;
    .locals 2
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bNQ:Z

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->mQuery:Lcom/google/android/shared/search/Query;

    iget-object v1, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->mRequestId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->kX(Ljava/lang/String;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 88
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiQ()Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_0
.end method

.method public final aiT()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bMI:I

    return v0
.end method

.method public final aiU()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bNO:I

    return v0
.end method

.method public final aiV()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bNP:I

    return v0
.end method

.method public final aiX()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bNN:I

    return v0
.end method

.method public final aiY()I
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bNQ:Z

    if-eqz v0, :cond_0

    .line 98
    const v0, 0x7f0a0647

    .line 100
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiY()I

    move-result v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 110
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/errors/SearchError;->writeToParcel(Landroid/os/Parcel;I)V

    .line 111
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bMI:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 112
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bNO:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 113
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bNP:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 114
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->bNN:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 115
    iget-object v0, p0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;->mRequestId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 116
    return-void
.end method
