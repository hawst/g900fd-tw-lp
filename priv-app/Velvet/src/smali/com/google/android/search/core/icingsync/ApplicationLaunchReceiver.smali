.class public final Lcom/google/android/search/core/icingsync/ApplicationLaunchReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/pm/PackageManager;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 30
    new-instance v2, Landroid/content/ComponentName;

    const-class v0, Lcom/google/android/search/core/icingsync/ApplicationLaunchReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p1, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    if-eqz p2, :cond_0

    move v0, v1

    .line 36
    :goto_0
    invoke-virtual {p0, v2, v0, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 38
    return-void

    .line 32
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private static r(Landroid/content/Intent;)Landroid/content/ComponentName;
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 86
    const-string v1, "com.android.launcher3.action.LAUNCH"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 87
    const-string v1, "Icing.ApplicationLaunchReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Received unrecognized intent: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v5, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 105
    :goto_0
    return-object v0

    .line 91
    :cond_0
    const-string v1, "intent"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 94
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 95
    const-string v1, "Icing.ApplicationLaunchReceiver"

    const-string v2, "Received empty intent string."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v5, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 100
    :cond_1
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v1, v2}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    .line 102
    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 103
    :catch_0
    move-exception v1

    .line 104
    const-string v2, "Icing.ApplicationLaunchReceiver"

    const-string v3, "Received invalid intent extra"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 44
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lcfo;->DD()Lcjs;

    move-result-object v1

    .line 46
    invoke-virtual {v1}, Lcjs;->MK()Z

    move-result v1

    .line 47
    if-nez v1, :cond_1

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/google/android/search/core/icingsync/ApplicationLaunchReceiver;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;Z)V

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    invoke-static {p2}, Lcom/google/android/search/core/icingsync/ApplicationLaunchReceiver;->r(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v1

    .line 54
    if-nez v1, :cond_2

    .line 55
    const-string v0, "Icing.ApplicationLaunchReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring invalid intent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 60
    :cond_2
    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->Gh()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    invoke-static {v1}, Lcsx;->c(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v0

    .line 63
    new-instance v1, Lcom/google/android/gms/appdatasearch/DocumentId;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcum;->bjr:Lcum;

    invoke-virtual {v3}, Lcum;->xF()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/gms/appdatasearch/DocumentId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 66
    new-instance v5, Lcom/google/android/gms/appdatasearch/UsageInfo;

    invoke-direct {v5, v1, v2, v3, v4}, Lcom/google/android/gms/appdatasearch/UsageInfo;-><init>(Lcom/google/android/gms/appdatasearch/DocumentId;JI)V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/search/core/icingsync/ApplicationLaunchReceiver;->goAsync()Landroid/content/BroadcastReceiver$PendingResult;

    move-result-object v6

    .line 69
    new-instance v0, Lcsw;

    const-string v2, "Icing async BC handling"

    new-array v3, v4, [I

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lcsw;-><init>(Lcom/google/android/search/core/icingsync/ApplicationLaunchReceiver;Ljava/lang/String;[ILandroid/content/Context;Lcom/google/android/gms/appdatasearch/UsageInfo;Landroid/content/BroadcastReceiver$PendingResult;)V

    .line 81
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    iget-object v1, v1, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
