.class public Lcom/google/android/search/shared/actions/HelpAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bMd:Ljava/lang/String;

.field private final bMe:Ljava/util/List;

.field private final bMf:Ljava/util/Map;

.field private final bMg:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 177
    new-instance v0, Ldui;

    invoke-direct {v0}, Ldui;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/HelpAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 78
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMd:Ljava/lang/String;

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMg:Ljava/lang/String;

    .line 83
    iput-object v3, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMe:Ljava/util/List;

    .line 84
    iput-object v3, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMf:Ljava/util/Map;

    .line 111
    :cond_0
    return-void

    .line 86
    :cond_1
    iput-object v3, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMg:Ljava/lang/String;

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 88
    invoke-static {v3}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMe:Ljava/util/List;

    move v2, v1

    .line 91
    :goto_0
    if-ge v2, v3, :cond_2

    .line 92
    new-instance v4, Ljlk;

    invoke-direct {v4}, Ljlk;-><init>()V

    .line 94
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    invoke-static {v4, v0}, Ljsr;->c(Ljsr;[B)Ljsr;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :goto_1
    iget-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMe:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 95
    :catch_0
    move-exception v0

    .line 96
    invoke-virtual {v0}, Ljsq;->printStackTrace()V

    goto :goto_1

    .line 102
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 103
    invoke-static {v2}, Lior;->mk(I)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMf:Ljava/util/Map;

    move v0, v1

    .line 104
    :goto_2
    if-ge v0, v2, :cond_0

    .line 105
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 107
    sget-object v4, Lcom/google/android/search/shared/actions/utils/ExampleContact;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v1, v4}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 108
    iget-object v4, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMf:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 70
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 71
    iput-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMe:Ljava/util/List;

    .line 72
    iput-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMf:Ljava/util/Map;

    .line 73
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMd:Ljava/lang/String;

    .line 74
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMg:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/Map;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 57
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 58
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMe:Ljava/util/List;

    .line 59
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMf:Ljava/util/Map;

    .line 60
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMd:Ljava/lang/String;

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMg:Ljava/lang/String;

    .line 64
    iget-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMe:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 65
    iget-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMf:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 66
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_2
    invoke-static {v0}, Lifv;->gY(Z)V

    goto :goto_1

    :cond_0
    move v0, v2

    .line 64
    goto :goto_0

    :cond_1
    move v0, v2

    .line 66
    goto :goto_2

    .line 68
    :cond_2
    return-void
.end method

.method public static a(Ljlk;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 195
    iget-object v2, p0, Ljlk;->erN:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    .line 196
    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    .line 197
    const/4 v0, 0x1

    .line 202
    :cond_0
    :goto_1
    return v0

    .line 198
    :cond_1
    const/4 v5, 0x4

    if-ne v4, v5, :cond_2

    .line 199
    const/4 v0, 0x2

    goto :goto_1

    .line 195
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 148
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/HelpAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final agu()Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 1

    .prologue
    .line 143
    invoke-static {}, Lcom/google/android/shared/util/MatchingAppInfo;->avc()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    return-object v0
.end method

.method public final ahr()Z
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMe:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ahs()Ljava/util/List;
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/HelpAction;->ahr()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 119
    iget-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMe:Ljava/util/List;

    return-object v0

    .line 118
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aht()Ljava/util/Map;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/HelpAction;->ahr()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 124
    iget-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMf:Ljava/util/Map;

    return-object v0

    .line 123
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ahu()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMd:Ljava/lang/String;

    return-object v0
.end method

.method public final ahv()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/HelpAction;->ahr()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 133
    iget-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMg:Ljava/lang/String;

    return-object v0
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 153
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 154
    iget-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMd:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMe:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 157
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 160
    iget-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMe:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 161
    iget-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMe:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljlk;

    .line 162
    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_0

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMf:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 167
    iget-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMf:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 168
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 169
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto :goto_1

    .line 172
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 173
    iget-object v0, p0, Lcom/google/android/search/shared/actions/HelpAction;->bMg:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 175
    :cond_2
    return-void
.end method
