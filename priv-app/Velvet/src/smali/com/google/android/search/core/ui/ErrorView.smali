.class public Lcom/google/android/search/core/ui/ErrorView;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field public bAn:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method


# virtual methods
.method public final d(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/errors/SearchError;)V
    .locals 4

    .prologue
    .line 48
    const v0, 0x7f1101b3

    invoke-virtual {p0, v0}, Lcom/google/android/search/core/ui/ErrorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 50
    const v1, 0x7f1101b2

    invoke-virtual {p0, v1}, Lcom/google/android/search/core/ui/ErrorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 51
    const v2, 0x7f1101b4

    invoke-virtual {p0, v2}, Lcom/google/android/search/core/ui/ErrorView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 52
    new-instance v3, Ldjq;

    invoke-direct {v3, p0}, Ldjq;-><init>(Lcom/google/android/search/core/ui/ErrorView;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    if-nez p2, :cond_0

    .line 63
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    :goto_0
    return-void

    .line 65
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiT()I

    move-result v3

    .line 66
    if-nez v3, :cond_1

    .line 67
    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/errors/SearchError;->getErrorMessage()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    :goto_1
    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiX()I

    move-result v0

    if-eqz v0, :cond_2

    .line 72
    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiX()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 80
    :goto_2
    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiZ()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 81
    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiY()I

    move-result v0

    if-eqz v0, :cond_4

    .line 82
    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiY()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 69
    :cond_1
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 73
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v3, " Frown Clown "

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 76
    const v0, 0x7f020005

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 78
    :cond_3
    const v0, 0x7f020077

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 84
    :cond_4
    const v0, 0x7f0a0646

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 87
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public final k(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/search/core/ui/ErrorView;->bAn:Ljava/lang/Runnable;

    .line 45
    return-void
.end method
