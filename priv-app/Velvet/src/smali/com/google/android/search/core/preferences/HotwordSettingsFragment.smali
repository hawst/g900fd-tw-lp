.class public Lcom/google/android/search/core/preferences/HotwordSettingsFragment;
.super Lcom/google/android/search/core/preferences/SettingsFragmentBase;
.source "PG"


# instance fields
.field private mIntentStarter:Leoj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected final Tj()I
    .locals 1

    .prologue
    .line 33
    const v0, 0x7f070015

    return v0
.end method

.method protected final a(Lgql;)Lcxt;
    .locals 12

    .prologue
    .line 38
    invoke-virtual {p1}, Lgql;->aJq()Lhhq;

    move-result-object v10

    .line 40
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/HotwordSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 41
    instance-of v1, v0, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    if-eqz v1, :cond_0

    .line 42
    check-cast v0, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->wr()Leoj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/core/preferences/HotwordSettingsFragment;->mIntentStarter:Leoj;

    .line 45
    :cond_0
    iget-object v0, v10, Lhhq;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    .line 46
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/HotwordSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, v10, Lhhq;->mSettings:Lhym;

    iget-object v2, p0, Lcom/google/android/search/core/preferences/HotwordSettingsFragment;->mIntentStarter:Leoj;

    invoke-static {v0, v1, v2}, Lcss;->a(Landroid/content/Context;Lhym;Leoj;)Lcsq;

    move-result-object v9

    .line 49
    new-instance v0, Lcwm;

    iget-object v1, v10, Lhhq;->mSettings:Lhym;

    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/HotwordSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v10}, Lhhq;->ue()Lcse;

    move-result-object v3

    invoke-virtual {p1}, Lgql;->aJs()Lchr;

    move-result-object v4

    iget-object v5, v10, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->BL()Lchk;

    move-result-object v5

    invoke-virtual {p1}, Lgql;->SC()Lcfo;

    move-result-object v6

    invoke-virtual {v6}, Lcfo;->BK()Lcke;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/search/core/preferences/HotwordSettingsFragment;->mIntentStarter:Leoj;

    new-instance v8, Lcry;

    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/HotwordSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    iget-object v10, v10, Lhhq;->mSettings:Lhym;

    invoke-direct {v8, v11, v10}, Lcry;-><init>(Landroid/content/Context;Lhym;)V

    invoke-direct/range {v0 .. v9}, Lcwm;-><init>(Lhym;Landroid/content/Context;Lcse;Lchr;Lchk;Lcke;Leoj;Lcry;Lcsq;)V

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 6

    .prologue
    .line 58
    invoke-super {p0, p1, p2}, Lcom/google/android/search/core/preferences/SettingsFragmentBase;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/HotwordSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "now_voice_settings"

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v0

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v3

    new-instance v0, Lgpk;

    invoke-virtual {p0}, Lcom/google/android/search/core/preferences/HotwordSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v0, v4}, Lgpk;-><init>(Landroid/content/Context;)V

    const-string v4, "now_voice_settings"

    invoke-virtual {v0, v4}, Lgpk;->nv(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v0

    invoke-virtual {v0}, Lcrh;->Su()Z

    move-result v5

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lgbk;->a(Landroid/view/Menu;Landroid/app/Activity;Ljava/lang/String;Landroid/accounts/Account;Landroid/net/Uri;Z)V

    .line 66
    return-void
.end method
