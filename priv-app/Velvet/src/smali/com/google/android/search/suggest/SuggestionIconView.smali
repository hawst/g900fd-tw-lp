.class public Lcom/google/android/search/suggest/SuggestionIconView;
.super Landroid/widget/TextView;
.source "PG"

# interfaces
.implements Leev;


# instance fields
.field public bTm:Leld;

.field private bWG:Leiu;

.field private bWQ:Lesm;

.field public bWR:Lcom/google/android/shared/search/Suggestion;
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field public bWS:Leew;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Leew;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/google/android/search/suggest/SuggestionIconView;->bWS:Leew;

    .line 119
    return-void
.end method

.method public final a(Lesm;Leld;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/search/suggest/SuggestionIconView;->bWQ:Lesm;

    .line 94
    iput-object p2, p0, Lcom/google/android/search/suggest/SuggestionIconView;->bTm:Leld;

    .line 95
    return-void
.end method

.method public final a(Lcom/google/android/shared/search/Suggestion;Ljava/lang/String;Leeo;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 100
    iget-object v1, p0, Lcom/google/android/search/suggest/SuggestionIconView;->bWR:Lcom/google/android/shared/search/Suggestion;

    if-eq v1, p1, :cond_0

    .line 101
    iput-object p1, p0, Lcom/google/android/search/suggest/SuggestionIconView;->bWR:Lcom/google/android/shared/search/Suggestion;

    .line 102
    iget-object v1, p0, Lcom/google/android/search/suggest/SuggestionIconView;->bWR:Lcom/google/android/shared/search/Suggestion;

    if-eqz v1, :cond_1

    .line 103
    iget-object v1, p0, Lcom/google/android/search/suggest/SuggestionIconView;->bWR:Lcom/google/android/shared/search/Suggestion;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Suggestion;->arY()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/search/suggest/SuggestionIconView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v1, p0, Lcom/google/android/search/suggest/SuggestionIconView;->bWG:Leiu;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asb()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asc()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/search/suggest/SuggestionIconView;->bWQ:Lesm;

    invoke-virtual {v1, v2, v3, v4}, Leiu;->a(Ljava/lang/String;Ljava/lang/String;Lesm;)V

    .line 107
    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/SuggestionIconView;->setVisibility(I)V

    .line 111
    :goto_0
    const/4 v0, 0x1

    .line 113
    :cond_0
    return v0

    .line 109
    :cond_1
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/SuggestionIconView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final aG(II)V
    .locals 0

    .prologue
    .line 123
    return-void
.end method

.method public final aod()Lcom/google/android/shared/search/Suggestion;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/search/suggest/SuggestionIconView;->bWR:Lcom/google/android/shared/search/Suggestion;

    return-object v0
.end method

.method public final hn(I)V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0}, Landroid/widget/TextView;->onFinishInflate()V

    .line 51
    new-instance v0, Leeq;

    invoke-direct {v0, p0}, Leeq;-><init>(Lcom/google/android/search/suggest/SuggestionIconView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/SuggestionIconView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    new-instance v0, Leer;

    invoke-direct {v0, p0}, Leer;-><init>(Lcom/google/android/search/suggest/SuggestionIconView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/SuggestionIconView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 71
    new-instance v0, Leiu;

    new-instance v1, Lees;

    invoke-direct {v1, p0}, Lees;-><init>(Lcom/google/android/search/suggest/SuggestionIconView;)V

    invoke-direct {v0, p0, v1}, Leiu;-><init>(Landroid/view/View;Lemy;)V

    iput-object v0, p0, Lcom/google/android/search/suggest/SuggestionIconView;->bWG:Leiu;

    .line 78
    return-void
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 81
    if-eqz p1, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionIconView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 83
    invoke-virtual {p1, v3, v3, v0, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 85
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/suggest/SuggestionIconView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00a2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 87
    invoke-virtual {p0, v2, p1, v2, v2}, Lcom/google/android/search/suggest/SuggestionIconView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 88
    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/SuggestionIconView;->setCompoundDrawablePadding(I)V

    .line 89
    return-void
.end method
