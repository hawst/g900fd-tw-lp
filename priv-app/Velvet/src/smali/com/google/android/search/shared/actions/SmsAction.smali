.class public Lcom/google/android/search/shared/actions/SmsAction;
.super Lcom/google/android/search/shared/actions/CommunicationAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private bMb:Ljava/lang/String;

.field private bNx:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    new-instance v0, Ldvh;

    invoke-direct {v0}, Ldvh;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/SmsAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/CommunicationAction;-><init>(Landroid/os/Parcel;)V

    .line 25
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/SmsAction;->bMb:Ljava/lang/String;

    .line 26
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/SmsAction;->bNx:Z

    .line 27
    return-void

    .line 26
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/CommunicationAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    .line 19
    iput-object p2, p0, Lcom/google/android/search/shared/actions/SmsAction;->bMb:Ljava/lang/String;

    .line 20
    iput-boolean p3, p0, Lcom/google/android/search/shared/actions/SmsAction;->bNx:Z

    .line 21
    return-void
.end method


# virtual methods
.method public final Wb()Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x1

    return v0
.end method

.method public final Wc()Z
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 78
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/SmsAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final afZ()Ldzb;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Ldzb;->bRq:Ldzb;

    return-object v0
.end method

.method public final agb()Ldtw;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Ldtw;->bLr:Ldtw;

    return-object v0
.end method

.method public final agc()I
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x1

    return v0
.end method

.method public final aiL()Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SmsAction;->bMb:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aiM()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/SmsAction;->bNx:Z

    return v0
.end method

.method public final canExecute()Z
    .locals 1

    .prologue
    .line 59
    invoke-super {p0}, Lcom/google/android/search/shared/actions/CommunicationAction;->canExecute()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/SmsAction;->aiL()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SmsAction;->bMb:Ljava/lang/String;

    return-object v0
.end method

.method public final i(Lcom/google/android/search/shared/contact/PersonDisambiguation;)Lcom/google/android/search/shared/actions/CommunicationAction;
    .locals 3

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/search/shared/actions/SmsAction;

    iget-object v1, p0, Lcom/google/android/search/shared/actions/SmsAction;->bMb:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/search/shared/actions/SmsAction;->bNx:Z

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/search/shared/actions/SmsAction;-><init>(Lcom/google/android/search/shared/contact/PersonDisambiguation;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 83
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/CommunicationAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 84
    iget-object v0, p0, Lcom/google/android/search/shared/actions/SmsAction;->bMb:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 85
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/SmsAction;->bNx:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 86
    return-void

    .line 85
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
