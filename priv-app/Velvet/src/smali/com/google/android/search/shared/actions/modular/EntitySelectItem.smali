.class public Lcom/google/android/search/shared/actions/modular/EntitySelectItem;
.super Lduc;
.source "PG"

# interfaces
.implements Lecw;


# instance fields
.field private bNR:Ldvt;

.field private final bNS:Z

.field private bNT:Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;

.field private bNU:Landroid/widget/TextView;

.field private bNV:Landroid/widget/TextView;

.field private bNW:Lcom/google/android/search/shared/ui/WebImageView;

.field private bNX:Landroid/view/View;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private mImageLoader:Lesm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0, p1, p2}, Lduc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    sget-object v0, Lbwe;->aLT:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 57
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNS:Z

    .line 58
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 59
    return-void
.end method

.method public static a(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lesm;Ldvt;Ljpw;)Lcom/google/android/search/shared/actions/modular/EntitySelectItem;
    .locals 6

    .prologue
    const v0, 0x7f040079

    .line 161
    if-eqz p5, :cond_0

    invoke-virtual {p5}, Ljpw;->brZ()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p5}, Ljpw;->getSize()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;

    .line 163
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lesm;

    iput-object v1, v0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->mImageLoader:Lesm;

    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldvt;

    iput-object v1, v0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNR:Ldvt;

    .line 164
    invoke-direct {v0, p0}, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->a(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;)V

    .line 165
    const v1, 0x7f1101ad

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0117

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-static {v1}, Leot;->bd(Landroid/view/View;)I

    move-result v5

    invoke-static {v1, v2, v3, v5, v4}, Leot;->a(Landroid/view/View;IIII)V

    .line 166
    :cond_1
    return-object v0

    .line 161
    :pswitch_0
    const v0, 0x7f04007a

    goto :goto_0

    :pswitch_1
    const v0, 0x7f04007b

    goto :goto_0

    :pswitch_2
    const v0, 0x7f04007c

    goto :goto_0

    :cond_2
    invoke-virtual {p5}, Ljpw;->brY()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p5}, Ljpw;->akf()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    :pswitch_3
    const v0, 0x7f040078

    goto :goto_0

    :pswitch_4
    const v0, 0x7f040075

    goto :goto_0

    :pswitch_5
    const v0, 0x7f040077

    goto :goto_0

    :pswitch_6
    const v0, 0x7f040076

    goto :goto_0

    :pswitch_7
    const v0, 0x7f040073

    goto :goto_0

    :pswitch_8
    const v0, 0x7f040074

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 125
    if-eqz p0, :cond_0

    .line 126
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 127
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    :cond_0
    return-void

    .line 126
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 71
    iput-object p1, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNT:Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;

    .line 72
    if-eqz p1, :cond_6

    .line 73
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNU:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNV:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->getDescription()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNW:Lcom/google/android/search/shared/ui/WebImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNR:Ldvt;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->mImageLoader:Lesm;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    const-string v3, "#setImageLoaders should be called before #setEntityWebImageView."

    invoke-static {v0, v3}, Lifv;->d(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->aki()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNR:Ldvt;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v3, v0, v1}, Ldvt;->A(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNW:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-virtual {v3, v0}, Lcom/google/android/search/shared/ui/WebImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 76
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNX:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 78
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNX:Landroid/view/View;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->getDescription()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v1, v2

    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 84
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v0, v1

    .line 75
    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;->oj()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNW:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->mImageLoader:Lesm;

    invoke-virtual {v3, v0, v4}, Lcom/google/android/search/shared/ui/WebImageView;->a(Landroid/net/Uri;Lesm;)V

    goto :goto_1

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNS:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNW:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-virtual {v0, v2}, Lcom/google/android/search/shared/ui/WebImageView;->setVisibility(I)V

    goto :goto_1

    .line 82
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->ahm()[Landroid/view/View;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->a(I[Landroid/view/View;)V

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;)V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0, p1}, Lduc;->b(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V

    .line 65
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->isCompleted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;

    invoke-direct {p0, v0}, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->a(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;)V

    .line 68
    :cond_0
    return-void
.end method

.method protected final ahm()[Landroid/view/View;
    .locals 3

    .prologue
    .line 230
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNU:Landroid/widget/TextView;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNV:Landroid/widget/TextView;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNW:Lcom/google/android/search/shared/ui/WebImageView;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic ajg()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNT:Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument$Entity;

    return-object v0
.end method

.method public final synthetic b(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V
    .locals 0

    .prologue
    .line 38
    check-cast p1, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;

    invoke-virtual {p0, p1}, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->a(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;)V

    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 216
    invoke-super {p0}, Lduc;->onFinishInflate()V

    .line 217
    const v0, 0x7f1101a9

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNU:Landroid/widget/TextView;

    .line 218
    const v0, 0x7f1101aa

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNV:Landroid/widget/TextView;

    .line 219
    const v0, 0x7f1101a8

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNW:Lcom/google/android/search/shared/ui/WebImageView;

    .line 220
    const v0, 0x7f1101ae

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->bNX:Landroid/view/View;

    .line 221
    return-void
.end method

.method public final setEditable(Z)V
    .locals 0

    .prologue
    .line 226
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 3

    .prologue
    .line 116
    invoke-super {p0, p1}, Lduc;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    const v0, 0x7f1101a9

    invoke-virtual {p0, v0}, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 118
    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/EntitySelectItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0133

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 120
    const-string v1, "sans-serif-condensed"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 122
    :cond_0
    return-void
.end method
