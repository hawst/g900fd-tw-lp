.class public Lcom/google/android/search/shared/actions/errors/SoundSearchError;
.super Lcom/google/android/search/shared/actions/errors/SearchError;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bMI:I

.field private final bNN:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    new-instance v0, Ldvp;

    invoke-direct {v0}, Ldvp;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/errors/SoundSearchError;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/errors/SearchError;-><init>(Landroid/os/Parcel;)V

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/SoundSearchError;->bMI:I

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/SoundSearchError;->bNN:I

    .line 39
    return-void
.end method

.method public constructor <init>(Lcom/google/android/shared/search/Query;Leis;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/errors/SearchError;-><init>(Lcom/google/android/shared/search/Query;)V

    .line 20
    invoke-virtual {p2}, Leis;->atd()Leiq;

    move-result-object v0

    instance-of v0, v0, Leil;

    if-eqz v0, :cond_0

    .line 21
    const v0, 0x7f0a0659

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/SoundSearchError;->bMI:I

    .line 22
    const v0, 0x7f02021d

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/SoundSearchError;->bNN:I

    .line 27
    :goto_0
    return-void

    .line 24
    :cond_0
    invoke-virtual {p2}, Leis;->atd()Leiq;

    move-result-object v0

    invoke-static {v0}, Leno;->e(Leiq;)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/SoundSearchError;->bMI:I

    .line 25
    invoke-virtual {p2}, Leis;->atd()Leiq;

    move-result-object v0

    invoke-static {v0}, Leno;->f(Leiq;)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/shared/actions/errors/SoundSearchError;->bNN:I

    goto :goto_0
.end method


# virtual methods
.method public final aiT()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/SoundSearchError;->bMI:I

    return v0
.end method

.method public final aiX()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/SoundSearchError;->bNN:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/errors/SearchError;->writeToParcel(Landroid/os/Parcel;I)V

    .line 54
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/SoundSearchError;->bMI:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 55
    iget v0, p0, Lcom/google/android/search/shared/actions/errors/SoundSearchError;->bNN:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 56
    return-void
.end method
