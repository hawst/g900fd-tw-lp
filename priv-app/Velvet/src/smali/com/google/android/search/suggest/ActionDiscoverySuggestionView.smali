.class public Lcom/google/android/search/suggest/ActionDiscoverySuggestionView;
.super Leej;
.source "PG"


# instance fields
.field private bWG:Leiu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Leej;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/shared/search/Suggestion;Ljava/lang/String;Leeo;)Z
    .locals 5

    .prologue
    .line 27
    invoke-super {p0, p1, p2, p3}, Leej;->a(Lcom/google/android/shared/search/Suggestion;Ljava/lang/String;Leeo;)Z

    move-result v1

    .line 28
    if-eqz v1, :cond_0

    .line 29
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asb()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "content"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "com.google.android.googlequicksearchbox.NetworkImageLoaderContentProvider"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "icon"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "url"

    invoke-virtual {v2, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    const v0, 0x7f11005e

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/ActionDiscoverySuggestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v3, Leiu;

    invoke-direct {v3, v0}, Leiu;-><init>(Landroid/widget/ImageView;)V

    iput-object v3, p0, Lcom/google/android/search/suggest/ActionDiscoverySuggestionView;->bWG:Leiu;

    iget-object v0, p0, Lcom/google/android/search/suggest/ActionDiscoverySuggestionView;->bWG:Leiu;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Leej;->bWQ:Lesm;

    invoke-virtual {v0, v2, v3, v4}, Leiu;->a(Ljava/lang/String;Ljava/lang/String;Lesm;)V

    .line 30
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->arY()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/suggest/ActionDiscoverySuggestionView;->w(Ljava/lang/CharSequence;)V

    .line 32
    :cond_0
    return v1
.end method
