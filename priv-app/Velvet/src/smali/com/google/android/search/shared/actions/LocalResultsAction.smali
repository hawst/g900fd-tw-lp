.class public Lcom/google/android/search/shared/actions/LocalResultsAction;
.super Lcom/google/android/search/shared/actions/AbstractVoiceAction;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private bMh:J

.field private final bMi:Ljpe;

.field private final bMj:Z

.field private bMk:Ljpd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lduj;

    invoke-direct {v0}, Lduj;-><init>()V

    sput-object v0, Lcom/google/android/search/shared/actions/LocalResultsAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>(Landroid/os/Parcel;)V

    .line 44
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 46
    const-class v2, Ljpe;

    invoke-virtual {v0, v2}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljpe;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_0
    iput-object v0, p0, Lcom/google/android/search/shared/actions/LocalResultsAction;->bMi:Ljpe;

    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/search/shared/actions/LocalResultsAction;->bMj:Z

    .line 53
    return-void

    .line 48
    :catch_0
    move-exception v0

    new-instance v0, Ljpe;

    invoke-direct {v0}, Ljpe;-><init>()V

    .line 49
    const-string v2, "LocalResultsAction"

    const-string v3, "Couldn\'t restore local results"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 52
    :cond_0
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public constructor <init>(Ljpe;Z)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/search/shared/actions/LocalResultsAction;->bMi:Ljpe;

    .line 37
    iput-boolean p2, p0, Lcom/google/android/search/shared/actions/LocalResultsAction;->bMj:Z

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Ldvn;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 87
    invoke-interface {p1, p0}, Ldvn;->a(Lcom/google/android/search/shared/actions/LocalResultsAction;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final ahA()Z
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/search/shared/actions/LocalResultsAction;->bMi:Ljpe;

    invoke-virtual {v0}, Ljpe;->oY()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ahw()Ljpe;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/search/shared/actions/LocalResultsAction;->bMi:Ljpe;

    return-object v0
.end method

.method public final ahx()Ljpd;
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/search/shared/actions/LocalResultsAction;->bMk:Ljpd;

    .line 69
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/search/shared/actions/LocalResultsAction;->bMk:Ljpd;

    .line 70
    return-object v0
.end method

.method public final ahy()V
    .locals 2

    .prologue
    .line 99
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/search/shared/actions/LocalResultsAction;->bMh:J

    .line 100
    return-void
.end method

.method public final ahz()Z
    .locals 4

    .prologue
    .line 104
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/search/shared/actions/LocalResultsAction;->bMh:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x7d0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final canExecute()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 81
    iget-object v1, p0, Lcom/google/android/search/shared/actions/LocalResultsAction;->bMi:Ljpe;

    iget-object v1, v1, Ljpe;->exC:[Ljpd;

    array-length v1, v1

    if-ne v1, v0, :cond_1

    iget-boolean v1, p0, Lcom/google/android/search/shared/actions/LocalResultsAction;->bMj:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/LocalResultsAction;->ahA()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljpd;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/search/shared/actions/LocalResultsAction;->bMk:Ljpd;

    .line 75
    return-void
.end method

.method public final gx(I)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/search/shared/actions/LocalResultsAction;->bMi:Ljpe;

    invoke-virtual {v0, p1}, Ljpe;->rd(I)Ljpe;

    .line 65
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 92
    invoke-super {p0, p1, p2}, Lcom/google/android/search/shared/actions/AbstractVoiceAction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 93
    iget-object v0, p0, Lcom/google/android/search/shared/actions/LocalResultsAction;->bMi:Ljpe;

    invoke-static {v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 94
    iget-boolean v0, p0, Lcom/google/android/search/shared/actions/LocalResultsAction;->bMj:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 95
    return-void

    .line 94
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
