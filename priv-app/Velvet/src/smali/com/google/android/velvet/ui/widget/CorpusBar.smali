.class public Lcom/google/android/velvet/ui/widget/CorpusBar;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private final RX:Landroid/view/View$OnClickListener;

.field public ddH:Lgxe;

.field private dfY:Lcfy;

.field private dfZ:I

.field public dga:Landroid/view/View;

.field private dgb:Z

.field private dgc:Landroid/view/View;

.field private final iA:Landroid/graphics/Rect;

.field private mImageLoader:Lesm;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 50
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->iA:Landroid/graphics/Rect;

    .line 52
    new-instance v0, Lhgc;

    invoke-direct {v0, p0}, Lhgc;-><init>(Lcom/google/android/velvet/ui/widget/CorpusBar;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->RX:Landroid/view/View$OnClickListener;

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->iA:Landroid/graphics/Rect;

    .line 52
    new-instance v0, Lhgc;

    invoke-direct {v0, p0}, Lhgc;-><init>(Lcom/google/android/velvet/ui/widget/CorpusBar;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->RX:Landroid/view/View$OnClickListener;

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->iA:Landroid/graphics/Rect;

    .line 52
    new-instance v0, Lhgc;

    invoke-direct {v0, p0}, Lhgc;-><init>(Lcom/google/android/velvet/ui/widget/CorpusBar;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->RX:Landroid/view/View$OnClickListener;

    .line 73
    return-void
.end method

.method private aOu()V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 209
    move v0, v1

    move v2, v1

    .line 210
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getChildCount()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 211
    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 212
    iget-object v5, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dga:Landroid/view/View;

    if-ne v4, v5, :cond_1

    .line 213
    iget-boolean v2, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dgb:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_1
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 214
    const/4 v2, 0x1

    .line 210
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    .line 213
    goto :goto_1

    .line 216
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dgb:Z

    if-nez v5, :cond_2

    iget v5, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dfZ:I

    if-lt v0, v5, :cond_2

    if-nez v2, :cond_2

    .line 217
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 219
    :cond_2
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 223
    :cond_3
    return-void
.end method


# virtual methods
.method public final aLs()V
    .locals 3

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 125
    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 126
    iget-object v2, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dga:Landroid/view/View;

    if-eq v1, v2, :cond_0

    .line 127
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 131
    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ui/widget/CorpusBar;->removeView(Landroid/view/View;)V

    .line 124
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 134
    :cond_1
    return-void
.end method

.method public final aOt()V
    .locals 1

    .prologue
    .line 203
    iget v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dfZ:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dgb:Z

    .line 204
    invoke-direct {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->aOu()V

    .line 205
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->c(Lcfy;)V

    .line 206
    return-void

    .line 203
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aOv()V
    .locals 2

    .prologue
    .line 226
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dgb:Z

    .line 227
    invoke-direct {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->aOu()V

    .line 230
    iget v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dfZ:I

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 231
    iget v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dfZ:I

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 233
    :cond_0
    return-void
.end method

.method public final b(Lcfy;)V
    .locals 4

    .prologue
    .line 150
    iget v0, p1, Lcfy;->aUM:I

    if-nez v0, :cond_0

    .line 200
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->ddH:Lgxe;

    invoke-virtual {v0}, Lgxe;->aJU()Lgpu;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->ddH:Lgxe;

    invoke-static {v0, p0, p1}, Lgpu;->a(Lgxe;Landroid/view/ViewGroup;Lcfy;)Landroid/view/View;

    move-result-object v3

    .line 155
    invoke-virtual {v3, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 157
    const v0, 0x7f110139

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 158
    iget-object v1, p1, Lcfy;->aUK:Landroid/net/Uri;

    .line 159
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 160
    iget-object v2, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mImageLoader:Lesm;

    invoke-interface {v2, v1}, Lesm;->D(Landroid/net/Uri;)Leml;

    move-result-object v1

    .line 161
    invoke-interface {v1}, Leml;->auB()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 162
    invoke-interface {v1}, Leml;->auC()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 173
    :cond_1
    :goto_1
    const v0, 0x7f11013a

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 174
    iget-object v1, p1, Lcfy;->aUL:Landroid/net/Uri;

    .line 175
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 177
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lesp;->c(Landroid/content/Context;Landroid/net/Uri;)Landroid/util/Pair;

    move-result-object v2

    .line 178
    iget-object v1, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/content/res/Resources;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 187
    :cond_2
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->RX:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dga:Landroid/view/View;

    if-nez v0, :cond_5

    .line 190
    invoke-virtual {p0, v3}, Lcom/google/android/velvet/ui/widget/CorpusBar;->addView(Landroid/view/View;)V

    .line 196
    :goto_2
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dfY:Lcfy;

    invoke-virtual {p1, v0}, Lcfy;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 197
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/view/View;->setSelected(Z)V

    .line 199
    :cond_3
    invoke-direct {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->aOu()V

    goto :goto_0

    .line 164
    :cond_4
    new-instance v2, Lhgd;

    invoke-direct {v2, p0, v0}, Lhgd;-><init>(Lcom/google/android/velvet/ui/widget/CorpusBar;Landroid/widget/ImageView;)V

    invoke-interface {v1, v2}, Leml;->e(Lemy;)V

    goto :goto_1

    .line 179
    :catch_0
    move-exception v0

    .line 180
    const-string v1, "Velvet.CorpusBar"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t get name for corpus "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 182
    :catch_1
    move-exception v0

    .line 183
    const-string v1, "Velvet.CorpusBar"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t get name for corpus "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 193
    :cond_5
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dga:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 194
    invoke-virtual {p0, v3, v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->addView(Landroid/view/View;I)V

    goto :goto_2
.end method

.method public final bJ(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 253
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    iput-object p1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dgc:Landroid/view/View;

    .line 266
    :goto_0
    return-void

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->iA:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 257
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->iA:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 259
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    .line 260
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 262
    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->iA:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->iA:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v0

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 263
    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->iA:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->iA:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 264
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->iA:Landroid/graphics/Rect;

    invoke-interface {v0, p0, v1, v4}, Landroid/view/ViewParent;->requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z

    goto :goto_0
.end method

.method public final c(Lcfy;)V
    .locals 3
    .param p1    # Lcfy;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 97
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dfY:Lcfy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dfY:Lcfy;

    invoke-virtual {v0, p1}, Lcfy;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dfY:Lcfy;

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dfY:Lcfy;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_1

    .line 101
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 104
    :cond_1
    iput-object p1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dfY:Lcfy;

    .line 105
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dfY:Lcfy;

    if-eqz v0, :cond_3

    .line 106
    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/widget/CorpusBar;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 107
    if-eqz v0, :cond_3

    .line 108
    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 110
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_2

    .line 111
    iput-boolean v2, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dgb:Z

    .line 112
    invoke-direct {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->aOu()V

    .line 114
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->bJ(Landroid/view/View;)V

    .line 120
    :cond_3
    :goto_0
    return-void

    .line 118
    :cond_4
    iput-object p1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dfY:Lcfy;

    goto :goto_0
.end method

.method public final c(Lgxe;)V
    .locals 2

    .prologue
    .line 86
    iput-object p1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->ddH:Lgxe;

    .line 87
    iget v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dfZ:I

    if-lez v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->ddH:Lgxe;

    invoke-virtual {v0}, Lgxe;->aJU()Lgpu;

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->ddH:Lgxe;

    invoke-static {v0, p0}, Lgpu;->a(Lgxe;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dga:Landroid/view/View;

    .line 90
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dga:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->RX:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dga:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->addView(Landroid/view/View;)V

    .line 93
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 77
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 78
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lgql;->anM()Lesm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->mImageLoader:Lesm;

    .line 80
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c003a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dfZ:I

    .line 82
    iget v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dfZ:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dgb:Z

    .line 83
    return-void

    .line 82
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 237
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 238
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dgc:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dgc:Landroid/view/View;

    .line 240
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/velvet/ui/widget/CorpusBar;->dgc:Landroid/view/View;

    .line 241
    new-instance v1, Lhge;

    const-string v2, "Scroll parent"

    invoke-direct {v1, p0, v2, v0}, Lhge;-><init>(Lcom/google/android/velvet/ui/widget/CorpusBar;Ljava/lang/String;Landroid/view/View;)V

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ui/widget/CorpusBar;->post(Ljava/lang/Runnable;)Z

    .line 250
    :cond_0
    return-void
.end method
