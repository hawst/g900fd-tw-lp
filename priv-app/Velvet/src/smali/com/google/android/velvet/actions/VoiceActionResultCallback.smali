.class public Lcom/google/android/velvet/actions/VoiceActionResultCallback;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Leol;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private cKK:Lggh;

.field private final cWb:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 169
    new-instance v0, Lgts;

    invoke-direct {v0}, Lgts;-><init>()V

    sput-object v0, Lcom/google/android/velvet/actions/VoiceActionResultCallback;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput p1, p0, Lcom/google/android/velvet/actions/VoiceActionResultCallback;->cWb:I

    .line 46
    return-void
.end method

.method public static synthetic a(Lcom/google/android/velvet/actions/VoiceActionResultCallback;Landroid/content/Context;Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 3

    .prologue
    .line 35
    instance-of v0, p1, Lcom/google/android/velvet/ui/VelvetActivity;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/velvet/ui/VelvetActivity;

    invoke-virtual {p1, p2}, Lcom/google/android/velvet/ui/VelvetActivity;->i(Lcom/google/android/search/shared/actions/VoiceAction;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "ActivityResultHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not a supported Activity: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->e(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static synthetic a(Ldxl;Lcom/google/android/search/shared/contact/Person;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 35
    invoke-virtual {p0}, Ldxl;->aho()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, v4}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->a(Landroid/os/Parcelable;Z)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/search/shared/contact/Person;

    aput-object p1, v2, v4

    invoke-static {v2}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Ldxl;->bPD:Ldzb;

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/google/android/search/shared/contact/PersonDisambiguation;-><init>(Ljava/lang/String;Ljava/util/List;ZLdzb;)V

    invoke-virtual {p0, v0}, Ldxl;->k(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    goto :goto_0
.end method

.method public static aKr()Leol;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Lcom/google/android/velvet/actions/VoiceActionResultCallback;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/velvet/actions/VoiceActionResultCallback;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public final a(Lggh;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/google/android/velvet/actions/VoiceActionResultCallback;->cKK:Lggh;

    .line 185
    return-void
.end method

.method public final a(ILandroid/content/Intent;Landroid/content/Context;)Z
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x0

    .line 65
    const/4 v0, -0x1

    if-ne p1, v0, :cond_2

    .line 66
    instance-of v0, p3, Lcom/google/android/velvet/ui/VelvetActivity;

    if-eqz v0, :cond_0

    move-object v0, p3

    check-cast v0, Lcom/google/android/velvet/ui/VelvetActivity;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/VelvetActivity;->Pm()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v8

    .line 67
    :goto_0
    if-nez v8, :cond_1

    move v0, v9

    .line 89
    :goto_1
    return v0

    .line 66
    :cond_0
    const-string v0, "ActivityResultHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not a supported Activity: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->e(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v8, v6

    goto :goto_0

    .line 70
    :cond_1
    iget v0, p0, Lcom/google/android/velvet/actions/VoiceActionResultCallback;->cWb:I

    packed-switch v0, :pswitch_data_0

    .line 86
    const-string v0, "ActivityResultHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t handle result: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/velvet/actions/VoiceActionResultCallback;->cWb:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :cond_2
    :goto_2
    const/4 v0, 0x1

    goto :goto_1

    .line 73
    :pswitch_0
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 76
    invoke-interface {v8}, Lcom/google/android/search/shared/actions/VoiceAction;->agy()Ldxl;

    move-result-object v6

    .line 78
    :cond_3
    if-eqz v6, :cond_5

    .line 79
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/velvet/actions/VoiceActionResultCallback;->cKK:Lggh;

    if-nez v0, :cond_4

    new-instance v0, Lggh;

    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    invoke-virtual {v2}, Lgql;->aJV()Lcjg;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lggh;-><init>(Landroid/content/ContentResolver;Lcjg;)V

    iput-object v0, p0, Lcom/google/android/velvet/actions/VoiceActionResultCallback;->cKK:Lggh;

    :cond_4
    iget-object v3, p0, Lcom/google/android/velvet/actions/VoiceActionResultCallback;->cKK:Lggh;

    new-instance v1, Lgtr;

    move-object v2, p0

    move-object v7, p3

    invoke-direct/range {v1 .. v8}, Lgtr;-><init>(Lcom/google/android/velvet/actions/VoiceActionResultCallback;Lggh;JLdxl;Landroid/content/Context;Lcom/google/android/search/shared/actions/VoiceAction;)V

    new-array v0, v9, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lgtr;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_2

    .line 81
    :cond_5
    const-string v0, "ActivityResultHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t handle pick contact result: data = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", action="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 70
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected final aKs()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/google/android/velvet/actions/VoiceActionResultCallback;->cWb:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/google/android/velvet/actions/VoiceActionResultCallback;->cWb:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 167
    return-void
.end method
