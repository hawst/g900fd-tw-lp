.class public Lcom/google/android/velvet/ActionData;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Leti;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static cRP:Ljava/util/concurrent/atomic/AtomicLong;

.field public static final cRQ:Lcom/google/android/velvet/ActionData;

.field public static final cRR:Lcom/google/android/velvet/ActionData;


# instance fields
.field private final bLG:I

.field private final bLc:Z

.field private final bRa:J

.field private final baD:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bcN:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final btg:Lhha;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final cRS:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cRT:Lcom/google/android/speech/embedded/TaggerResult;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cRU:Lieb;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cRV:Leiq;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cRW:Ljyw;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cRX:I

.field private final mPeanut:Ljrp;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 17

    .prologue
    .line 54
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    sput-object v0, Lcom/google/android/velvet/ActionData;->cRP:Ljava/util/concurrent/atomic/AtomicLong;

    .line 57
    new-instance v1, Lcom/google/android/velvet/ActionData;

    sget-object v0, Lcom/google/android/velvet/ActionData;->cRP:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v2

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    sget-object v12, Lhha;->dgR:Lhha;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-direct/range {v1 .. v16}, Lcom/google/android/velvet/ActionData;-><init>(JZLjrp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/speech/embedded/TaggerResult;Lieb;Leiq;Ljyw;Lhha;Ljava/lang/String;ZII)V

    sput-object v1, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    .line 62
    new-instance v1, Lcom/google/android/velvet/ActionData;

    sget-object v0, Lcom/google/android/velvet/ActionData;->cRP:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v2

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    sget-object v12, Lhha;->dgR:Lhha;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-direct/range {v1 .. v16}, Lcom/google/android/velvet/ActionData;-><init>(JZLjrp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/speech/embedded/TaggerResult;Lieb;Leiq;Ljyw;Lhha;Ljava/lang/String;ZII)V

    sput-object v1, Lcom/google/android/velvet/ActionData;->cRR:Lcom/google/android/velvet/ActionData;

    .line 182
    new-instance v0, Lgpb;

    invoke-direct {v0}, Lgpb;-><init>()V

    sput-object v0, Lcom/google/android/velvet/ActionData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JZLjrp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/speech/embedded/TaggerResult;Lieb;Leiq;Ljyw;Lhha;Ljava/lang/String;ZII)V
    .locals 3

    .prologue
    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243
    const/4 v1, 0x0

    .line 244
    if-eqz p3, :cond_0

    const/4 v1, 0x1

    .line 245
    :cond_0
    if-nez p4, :cond_1

    if-eqz p7, :cond_2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 246
    :cond_2
    if-eqz p5, :cond_3

    if-eqz p6, :cond_3

    add-int/lit8 v1, v1, 0x1

    .line 247
    :cond_3
    if-eqz p8, :cond_4

    add-int/lit8 v1, v1, 0x1

    .line 248
    :cond_4
    if-eqz p9, :cond_5

    add-int/lit8 v1, v1, 0x1

    .line 249
    :cond_5
    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lifv;->gX(Z)V

    .line 252
    iput-wide p1, p0, Lcom/google/android/velvet/ActionData;->bRa:J

    .line 253
    iput-object p4, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    .line 254
    iput-object p5, p0, Lcom/google/android/velvet/ActionData;->bcN:Ljava/lang/String;

    .line 255
    iput-object p6, p0, Lcom/google/android/velvet/ActionData;->cRS:Ljava/lang/String;

    .line 256
    iput-object p7, p0, Lcom/google/android/velvet/ActionData;->cRT:Lcom/google/android/speech/embedded/TaggerResult;

    .line 257
    iput-object p8, p0, Lcom/google/android/velvet/ActionData;->cRU:Lieb;

    .line 258
    iput-object p9, p0, Lcom/google/android/velvet/ActionData;->cRV:Leiq;

    .line 259
    iput-object p10, p0, Lcom/google/android/velvet/ActionData;->cRW:Ljyw;

    .line 260
    iput-object p11, p0, Lcom/google/android/velvet/ActionData;->btg:Lhha;

    .line 261
    iput-object p12, p0, Lcom/google/android/velvet/ActionData;->baD:Ljava/lang/String;

    .line 262
    move/from16 v0, p13

    iput-boolean v0, p0, Lcom/google/android/velvet/ActionData;->bLc:Z

    .line 263
    move/from16 v0, p14

    iput v0, p0, Lcom/google/android/velvet/ActionData;->bLG:I

    .line 264
    move/from16 v0, p15

    iput v0, p0, Lcom/google/android/velvet/ActionData;->cRX:I

    .line 265
    return-void

    .line 249
    :cond_6
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static a(Ljkt;Lcom/google/android/speech/embedded/TaggerResult;Lieb;Lcky;)I
    .locals 3
    .param p0    # Ljkt;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/google/android/speech/embedded/TaggerResult;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lieb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcky;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 419
    if-eqz p0, :cond_2

    .line 420
    iget-object v1, p0, Ljkt;->eqn:Ljku;

    if-eqz v1, :cond_1

    .line 421
    iget-object v1, p0, Ljkt;->eqn:Ljku;

    .line 422
    invoke-virtual {v1}, Ljku;->bbg()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 423
    invoke-virtual {v1}, Ljku;->oY()I

    move-result v0

    .line 436
    :cond_0
    :goto_0
    return v0

    .line 428
    :cond_1
    const v1, 0xa23c05

    invoke-static {v1}, Lhwt;->lx(I)V

    goto :goto_0

    .line 430
    :cond_2
    if-eqz p1, :cond_3

    .line 431
    invoke-static {p3, p1}, Licv;->a(Lcky;Lcom/google/android/speech/embedded/TaggerResult;)I

    move-result v0

    goto :goto_0

    .line 432
    :cond_3
    if-eqz p2, :cond_0

    .line 434
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public static a(Lcom/google/android/speech/embedded/TaggerResult;Ljrp;Lcky;)Lcom/google/android/velvet/ActionData;
    .locals 19
    .param p1    # Ljrp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcky;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 115
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v2, v0, v3, v1}, Lcom/google/android/velvet/ActionData;->a(Ljkt;Lcom/google/android/speech/embedded/TaggerResult;Lieb;Lcky;)I

    move-result v17

    .line 116
    new-instance v3, Lcom/google/android/velvet/ActionData;

    sget-object v2, Lcom/google/android/velvet/ActionData;->cRP:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v4

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {}, Lhha;->aOK()Lhha;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v18, 0x0

    move-object/from16 v7, p1

    move-object/from16 v10, p0

    invoke-direct/range {v3 .. v18}, Lcom/google/android/velvet/ActionData;-><init>(JZLjrp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/speech/embedded/TaggerResult;Lieb;Leiq;Ljyw;Lhha;Ljava/lang/String;ZII)V

    return-object v3
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljyw;)Lcom/google/android/velvet/ActionData;
    .locals 17
    .param p0    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljyw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 108
    new-instance v1, Lcom/google/android/velvet/ActionData;

    sget-object v0, Lcom/google/android/velvet/ActionData;->cRP:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    sget-object v12, Lhha;->dgR:Lhha;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v11, p2

    invoke-direct/range {v1 .. v16}, Lcom/google/android/velvet/ActionData;-><init>(JZLjrp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/speech/embedded/TaggerResult;Lieb;Leiq;Ljyw;Lhha;Ljava/lang/String;ZII)V

    return-object v1
.end method

.method public static a(Ljrp;Ljyw;Ljava/lang/String;Z)Lcom/google/android/velvet/ActionData;
    .locals 19
    .param p0    # Ljrp;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ljyw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 99
    move-object/from16 v0, p0

    iget-object v2, v0, Ljrp;->eBt:[Ljkt;

    array-length v2, v2

    if-lez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Ljrp;->eBt:[Ljkt;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    :goto_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/velvet/ActionData;->a(Ljkt;Lcom/google/android/speech/embedded/TaggerResult;Lieb;Lcky;)I

    move-result v17

    .line 101
    new-instance v3, Lcom/google/android/velvet/ActionData;

    sget-object v2, Lcom/google/android/velvet/ActionData;->cRP:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v4

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static/range {p0 .. p0}, Lhha;->c(Ljrp;)Lhha;

    move-result-object v14

    const/16 v18, 0x0

    move-object/from16 v7, p0

    move-object/from16 v13, p1

    move-object/from16 v15, p2

    move/from16 v16, p3

    invoke-direct/range {v3 .. v18}, Lcom/google/android/velvet/ActionData;-><init>(JZLjrp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/speech/embedded/TaggerResult;Lieb;Leiq;Ljyw;Lhha;Ljava/lang/String;ZII)V

    return-object v3

    .line 99
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/os/Parcel;Ljsr;)V
    .locals 1
    .param p0    # Landroid/os/Parcel;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ljsr;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 642
    if-nez p1, :cond_0

    .line 643
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 648
    :goto_0
    return-void

    .line 645
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 646
    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_0
.end method

.method public static c(Lieb;)Lcom/google/android/velvet/ActionData;
    .locals 19

    .prologue
    .line 122
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-static {v2, v3, v0, v4}, Lcom/google/android/velvet/ActionData;->a(Ljkt;Lcom/google/android/speech/embedded/TaggerResult;Lieb;Lcky;)I

    move-result v17

    .line 123
    new-instance v3, Lcom/google/android/velvet/ActionData;

    sget-object v2, Lcom/google/android/velvet/ActionData;->cRP:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    sget-object v14, Lhha;->dgT:Lhha;

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v18, 0x0

    move-object/from16 v11, p0

    invoke-direct/range {v3 .. v18}, Lcom/google/android/velvet/ActionData;-><init>(JZLjrp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/speech/embedded/TaggerResult;Lieb;Leiq;Ljyw;Lhha;Ljava/lang/String;ZII)V

    return-object v3
.end method

.method public static c(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;
    .locals 3
    .param p0    # Landroid/os/Parcel;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Class;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 657
    :try_start_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readByte()B

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 658
    invoke-virtual {p0}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    .line 659
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;

    .line 660
    invoke-static {v0, v1}, Ljsr;->c(Ljsr;[B)Ljsr;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_2

    .line 670
    :goto_0
    return-object v0

    .line 663
    :catch_0
    move-exception v0

    .line 664
    const-string v1, "Velvet.ActionData"

    const-string v2, "Error reading proto"

    invoke-static {v1, v2, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 670
    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 665
    :catch_1
    move-exception v0

    .line 666
    const-string v1, "Velvet.ActionData"

    const-string v2, "Error reading proto"

    invoke-static {v1, v2, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 667
    :catch_2
    move-exception v0

    .line 668
    const-string v1, "Velvet.ActionData"

    const-string v2, "Error reading proto"

    invoke-static {v1, v2, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method


# virtual methods
.method public final Pq()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 502
    iget v0, p0, Lcom/google/android/velvet/ActionData;->cRX:I

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ActionData;->ku(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 503
    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v0

    .line 504
    sget-object v2, Ljlf;->erl:Ljsm;

    invoke-virtual {v0, v2}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 505
    sget-object v2, Ljlf;->erl:Ljsm;

    invoke-virtual {v0, v2}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljlf;

    .line 506
    invoke-virtual {v0}, Ljlf;->boQ()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 507
    invoke-virtual {v0}, Ljlf;->Pq()I

    move-result v0

    .line 513
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final ZM()Lhha;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 517
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->btg:Lhha;

    return-object v0
.end method

.method public final Zv()Z
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    iget-object v0, v0, Ljrp;->eBl:Ljrr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    iget-object v0, v0, Ljrp;->eBl:Ljrr;

    invoke-virtual {v0}, Ljrr;->btC()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Letj;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 675
    const-string v3, "ActionData"

    invoke-virtual {p1, v3}, Letj;->lt(Ljava/lang/String;)V

    .line 676
    const-string v3, "id"

    invoke-virtual {p1, v3}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v3

    iget-wide v4, p0, Lcom/google/android/velvet/ActionData;->bRa:J

    invoke-virtual {v3, v4, v5}, Letn;->be(J)V

    .line 677
    sget-object v3, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    if-ne p0, v3, :cond_0

    .line 678
    const-string v0, "NONE"

    invoke-virtual {p1, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 705
    :goto_0
    return-void

    .line 681
    :cond_0
    sget-object v3, Lcom/google/android/velvet/ActionData;->cRR:Lcom/google/android/velvet/ActionData;

    if-ne p0, v3, :cond_1

    .line 682
    const-string v0, "ANSWER_IN_SRP"

    invoke-virtual {p1, v0}, Letj;->lv(Ljava/lang/String;)V

    goto :goto_0

    .line 685
    :cond_1
    iget-object v3, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    if-nez v3, :cond_2

    .line 686
    const-string v3, "peanut"

    invoke-virtual {p1, v3}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v3

    invoke-virtual {v3}, Letn;->avR()V

    .line 690
    :goto_1
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v3

    .line 691
    const-string v4, "HTML"

    invoke-virtual {v3, v4}, Letj;->lt(Ljava/lang/String;)V

    .line 692
    iget-object v4, p0, Lcom/google/android/velvet/ActionData;->cRS:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 693
    const-string v4, "base URL"

    invoke-virtual {v3, v4}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/velvet/ActionData;->bcN:Ljava/lang/String;

    invoke-virtual {v4, v5, v1}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 694
    const-string v4, "length"

    invoke-virtual {v3, v4}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/ActionData;->cRS:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Letn;->be(J)V

    .line 698
    :goto_2
    const-string v3, "pumpkin"

    invoke-virtual {p1, v3}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/ActionData;->cRT:Lcom/google/android/speech/embedded/TaggerResult;

    if-nez v4, :cond_4

    :goto_3
    invoke-virtual {v3, v0, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 700
    const-string v0, "ears response present"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRU:Lieb;

    if-eqz v0, :cond_5

    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Letn;->ff(Z)V

    .line 701
    const-string v0, "metadata present"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/velvet/ActionData;->cRW:Ljyw;

    if-eqz v3, :cond_6

    :goto_5
    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 702
    const-string v0, "effect on web results"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/ActionData;->btg:Lhha;

    invoke-virtual {v0, v1}, Letn;->f(Ljava/lang/Enum;)V

    .line 703
    const-string v0, "stream parsing"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/velvet/ActionData;->bLc:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 704
    const-string v0, "number of timeouts"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/ActionData;->cRX:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    goto/16 :goto_0

    .line 688
    :cond_2
    const-string v3, "peanut"

    invoke-virtual {p1, v3}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    iget-object v4, v4, Ljrp;->eBt:[Ljkt;

    array-length v4, v4

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Letn;->be(J)V

    goto/16 :goto_1

    .line 696
    :cond_3
    invoke-virtual {v3, v0}, Letj;->lv(Ljava/lang/String;)V

    goto :goto_2

    .line 698
    :cond_4
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRT:Lcom/google/android/speech/embedded/TaggerResult;

    invoke-virtual {v0}, Lcom/google/android/speech/embedded/TaggerResult;->aGX()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_5
    move v0, v2

    .line 700
    goto :goto_4

    :cond_6
    move v1, v2

    .line 701
    goto :goto_5
.end method

.method public final aGY()Litp;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRT:Lcom/google/android/speech/embedded/TaggerResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRT:Lcom/google/android/speech/embedded/TaggerResult;

    invoke-virtual {v0}, Lcom/google/android/speech/embedded/TaggerResult;->aGY()Litp;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aIA()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/google/android/velvet/ActionData;->cRX:I

    return v0
.end method

.method public final aIB()Ljyw;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRW:Ljyw;

    return-object v0
.end method

.method public final aIC()Z
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aID()Z
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRS:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aIE()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRS:Ljava/lang/String;

    return-object v0
.end method

.method public final aIF()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->bcN:Ljava/lang/String;

    return-object v0
.end method

.method public final aIG()Lcom/google/android/speech/embedded/TaggerResult;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRT:Lcom/google/android/speech/embedded/TaggerResult;

    return-object v0
.end method

.method public final aIH()Leiq;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRV:Leiq;

    return-object v0
.end method

.method public final aII()Lieb;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRU:Lieb;

    return-object v0
.end method

.method public final aIJ()Z
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRW:Ljyw;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aIK()Z
    .locals 1

    .prologue
    .line 345
    sget-object v0, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aIL()I
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRT:Lcom/google/android/speech/embedded/TaggerResult;

    if-eqz v0, :cond_0

    .line 350
    const/16 v0, 0x5d

    .line 352
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x5e

    goto :goto_0
.end method

.method public final aIM()J
    .locals 2

    .prologue
    .line 373
    invoke-virtual {p0}, Lcom/google/android/velvet/ActionData;->alg()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v0

    iget-object v0, v0, Ljkt;->eqo:Ljlm;

    invoke-virtual {v0}, Ljlm;->bpd()I

    move-result v0

    int-to-long v0, v0

    .line 376
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final aIN()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 384
    iget-object v2, p0, Lcom/google/android/velvet/ActionData;->cRT:Lcom/google/android/speech/embedded/TaggerResult;

    if-eqz v2, :cond_1

    .line 399
    :cond_0
    :goto_0
    return v0

    .line 388
    :cond_1
    iget-object v2, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    if-nez v2, :cond_2

    move v0, v1

    .line 390
    goto :goto_0

    .line 393
    :cond_2
    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ActionData;->ku(I)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v2

    .line 394
    :goto_1
    if-eqz v2, :cond_4

    iget-object v3, v2, Ljkt;->eqo:Ljlm;

    if-eqz v3, :cond_4

    iget-object v3, v2, Ljkt;->eqo:Ljlm;

    invoke-virtual {v3}, Ljlm;->bpm()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 396
    iget-object v0, v2, Ljkt;->eqo:Ljlm;

    invoke-virtual {v0}, Ljlm;->bpl()Z

    move-result v0

    goto :goto_0

    .line 393
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 399
    :cond_4
    iget-object v2, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    iget-object v2, v2, Ljrp;->eBl:Ljrr;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    iget-object v2, v2, Ljrp;->eBl:Ljrr;

    invoke-virtual {v2}, Ljrr;->bpl()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final aIO()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 452
    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ActionData;->ku(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v1

    iget-object v1, v1, Ljkt;->eqo:Ljlm;

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v1

    iget-object v1, v1, Ljkt;->eqo:Ljlm;

    invoke-virtual {v1}, Ljlm;->bpg()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final aIP()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 460
    iget v2, p0, Lcom/google/android/velvet/ActionData;->cRX:I

    if-lez v2, :cond_1

    .line 471
    :cond_0
    :goto_0
    return v0

    .line 463
    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ActionData;->ku(I)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v2

    iget-object v2, v2, Ljkt;->eqo:Ljlm;

    if-eqz v2, :cond_2

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v2

    iget-object v2, v2, Ljkt;->eqo:Ljlm;

    invoke-virtual {v2}, Ljlm;->bpf()Z

    move-result v2

    if-nez v2, :cond_0

    .line 467
    :cond_2
    iget-object v2, p0, Lcom/google/android/velvet/ActionData;->cRT:Lcom/google/android/speech/embedded/TaggerResult;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/velvet/ActionData;->cRT:Lcom/google/android/speech/embedded/TaggerResult;

    invoke-virtual {v2}, Lcom/google/android/speech/embedded/TaggerResult;->aGX()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Licv;->pd(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    .line 471
    goto :goto_0
.end method

.method public final aIQ()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    iget-object v0, v0, Ljrp;->eBl:Ljrr;

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    iget-object v0, v0, Ljrp;->eBl:Ljrr;

    invoke-virtual {v0}, Ljrr;->btB()Ljava/lang/String;

    move-result-object v0

    .line 478
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aIR()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 492
    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ActionData;->ku(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v0

    invoke-virtual {v0}, Ljkt;->bov()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final aIS()Z
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRW:Ljyw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRW:Ljyw;

    iget-object v0, v0, Ljyw;->eMy:Ljyx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRW:Ljyw;

    iget-object v0, v0, Ljyw;->eMy:Ljyx;

    invoke-virtual {v0}, Ljyx;->bwL()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aIT()Z
    .locals 1

    .prologue
    .line 531
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRT:Lcom/google/android/speech/embedded/TaggerResult;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aIU()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 538
    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ActionData;->ku(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 543
    :cond_0
    :goto_0
    return v0

    .line 542
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v1

    .line 543
    iget-object v2, v1, Ljkt;->eqo:Ljlm;

    if-eqz v2, :cond_0

    iget-object v1, v1, Ljkt;->eqo:Ljlm;

    invoke-virtual {v1}, Ljlm;->bpn()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final aIV()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 551
    invoke-virtual {p0}, Lcom/google/android/velvet/ActionData;->aIU()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v0

    iget-object v0, v0, Ljkt;->eqo:Ljlm;

    invoke-virtual {v0}, Ljlm;->aIV()I

    move-result v0

    :cond_0
    return v0
.end method

.method public final aIW()Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 558
    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ActionData;->ku(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 559
    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v0

    .line 560
    iget-object v1, v0, Ljkt;->eqn:Ljku;

    if-eqz v1, :cond_0

    iget-object v1, v0, Ljkt;->eqn:Ljku;

    invoke-virtual {v1}, Ljku;->boB()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 561
    iget-object v0, v0, Ljkt;->eqn:Ljku;

    invoke-virtual {v0}, Ljku;->aIW()Ljava/lang/String;

    move-result-object v0

    .line 564
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aIz()Lcom/google/android/velvet/ActionData;
    .locals 19

    .prologue
    .line 137
    sget-object v2, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    move-object/from16 v0, p0

    if-ne v0, v2, :cond_0

    .line 140
    :goto_0
    return-object p0

    :cond_0
    new-instance v3, Lcom/google/android/velvet/ActionData;

    sget-object v2, Lcom/google/android/velvet/ActionData;->cRP:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v4

    sget-object v2, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    move-object/from16 v0, p0

    if-ne v0, v2, :cond_1

    const/4 v6, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/velvet/ActionData;->bcN:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/velvet/ActionData;->cRS:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/velvet/ActionData;->cRT:Lcom/google/android/speech/embedded/TaggerResult;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/velvet/ActionData;->cRU:Lieb;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/velvet/ActionData;->cRV:Leiq;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/ActionData;->cRW:Ljyw;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/velvet/ActionData;->btg:Lhha;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/velvet/ActionData;->baD:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/velvet/ActionData;->bLc:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/velvet/ActionData;->bLG:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/velvet/ActionData;->cRX:I

    add-int/lit8 v18, v2, 0x1

    invoke-direct/range {v3 .. v18}, Lcom/google/android/velvet/ActionData;-><init>(JZLjrp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/speech/embedded/TaggerResult;Lieb;Leiq;Ljyw;Lhha;Ljava/lang/String;ZII)V

    move-object/from16 p0, v3

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public final agw()Z
    .locals 1

    .prologue
    .line 275
    iget-boolean v0, p0, Lcom/google/android/velvet/ActionData;->bLc:Z

    return v0
.end method

.method public final alg()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 361
    iget-boolean v1, p0, Lcom/google/android/velvet/ActionData;->bLc:Z

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ActionData;->ku(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 362
    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ActionData;->kv(I)Ljkt;

    move-result-object v1

    .line 363
    iget-object v2, v1, Ljkt;->eqo:Ljlm;

    if-eqz v2, :cond_0

    iget-object v2, v1, Ljkt;->eqo:Ljlm;

    invoke-virtual {v2}, Ljlm;->bpb()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, v1, Ljkt;->eqo:Ljlm;

    invoke-virtual {v1}, Ljlm;->bpe()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 366
    :cond_0
    return v0
.end method

.method public final aoM()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->baD:Ljava/lang/String;

    return-object v0
.end method

.method public final aoP()Z
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRT:Lcom/google/android/speech/embedded/TaggerResult;

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRS:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRU:Lieb;

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 569
    instance-of v0, p1, Lcom/google/android/velvet/ActionData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/velvet/ActionData;

    iget-wide v0, p1, Lcom/google/android/velvet/ActionData;->bRa:J

    iget-wide v2, p0, Lcom/google/android/velvet/ActionData;->bRa:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getId()J
    .locals 2

    .prologue
    .line 581
    iget-wide v0, p0, Lcom/google/android/velvet/ActionData;->bRa:J

    return-wide v0
.end method

.method public final getPeanut()Ljrp;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 586
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/android/velvet/ActionData;->bRa:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRS:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRT:Lcom/google/android/speech/embedded/TaggerResult;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRU:Lieb;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRV:Leiq;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ku(I)Z
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    iget-object v0, v0, Ljrp;->eBt:[Ljkt;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final kv(I)Ljkt;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 291
    invoke-virtual {p0}, Lcom/google/android/velvet/ActionData;->aIC()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    iget-object v0, v0, Ljrp;->eBt:[Ljkt;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final oY()I
    .locals 1

    .prologue
    .line 404
    iget v0, p0, Lcom/google/android/velvet/ActionData;->bLG:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 591
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ActionData{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 592
    const-string v1, "id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/velvet/ActionData;->bRa:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 593
    sget-object v1, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    if-ne p0, v1, :cond_3

    .line 594
    const-string v1, " NONE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 622
    :cond_0
    :goto_0
    const-string v1, " FX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/ActionData;->btg:Lhha;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 623
    iget-boolean v1, p0, Lcom/google/android/velvet/ActionData;->bLc:Z

    if-eqz v1, :cond_1

    .line 624
    const-string v1, " STREAM PARSING"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 626
    :cond_1
    iget v1, p0, Lcom/google/android/velvet/ActionData;->cRX:I

    if-lez v1, :cond_2

    .line 627
    const-string v1, " timeouts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/velvet/ActionData;->cRX:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 629
    :cond_2
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 630
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 595
    :cond_3
    sget-object v1, Lcom/google/android/velvet/ActionData;->cRR:Lcom/google/android/velvet/ActionData;

    if-ne p0, v1, :cond_4

    .line 596
    const-string v1, " ANSWER_IN_SRP"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 598
    :cond_4
    iget-object v1, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    if-eqz v1, :cond_5

    .line 599
    const-string v1, " PEANUT("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 603
    const-string v1, "ActionV2Count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    iget-object v2, v2, Ljrp;->eBt:[Ljkt;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 605
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 607
    :cond_5
    iget-object v1, p0, Lcom/google/android/velvet/ActionData;->cRS:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 608
    const-string v1, " HTML("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 609
    const-string v1, "mBaseUrl"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/ActionData;->bcN:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 610
    const-string v1, "HTML length: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/ActionData;->cRS:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 612
    :cond_6
    iget-object v1, p0, Lcom/google/android/velvet/ActionData;->cRT:Lcom/google/android/speech/embedded/TaggerResult;

    if-eqz v1, :cond_7

    .line 613
    const-string v1, " PUMPKIN("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/ActionData;->cRT:Lcom/google/android/speech/embedded/TaggerResult;

    invoke-virtual {v2}, Lcom/google/android/speech/embedded/TaggerResult;->aGX()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 615
    :cond_7
    iget-object v1, p0, Lcom/google/android/velvet/ActionData;->cRU:Lieb;

    if-eqz v1, :cond_8

    .line 616
    const-string v1, " EARS"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 618
    :cond_8
    iget-object v1, p0, Lcom/google/android/velvet/ActionData;->cRW:Ljyw;

    if-eqz v1, :cond_0

    .line 619
    const-string v1, " METADATA"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 168
    iget-wide v0, p0, Lcom/google/android/velvet/ActionData;->bRa:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 169
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->mPeanut:Ljrp;

    invoke-static {p1, v0}, Lcom/google/android/velvet/ActionData;->a(Landroid/os/Parcel;Ljsr;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->bcN:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRS:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRT:Lcom/google/android/speech/embedded/TaggerResult;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 173
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRU:Lieb;

    invoke-static {p1, v0}, Lcom/google/android/velvet/ActionData;->a(Landroid/os/Parcel;Ljsr;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRV:Leiq;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->cRW:Ljyw;

    invoke-static {p1, v0}, Lcom/google/android/velvet/ActionData;->a(Landroid/os/Parcel;Ljsr;)V

    .line 176
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->btg:Lhha;

    invoke-virtual {v0}, Lhha;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 177
    iget-object v0, p0, Lcom/google/android/velvet/ActionData;->baD:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 178
    iget v0, p0, Lcom/google/android/velvet/ActionData;->bLG:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 179
    iget v0, p0, Lcom/google/android/velvet/ActionData;->cRX:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 180
    return-void
.end method
