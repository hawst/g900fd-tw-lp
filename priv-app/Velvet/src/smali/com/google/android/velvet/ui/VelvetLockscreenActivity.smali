.class public Lcom/google/android/velvet/ui/VelvetLockscreenActivity;
.super Lcom/google/android/velvet/ui/VelvetActivity;
.source "PG"


# instance fields
.field private dfk:Lhfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetActivity;-><init>()V

    .line 57
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/VelvetActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    sget-object v0, Lcgg;->aVs:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    const-string v0, "VelLockscreenActivity"

    const-string v1, "Velvet launched on lockscreen without the feature"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->e(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 40
    :goto_0
    return-void

    .line 35
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 36
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 37
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 38
    new-instance v1, Lhfo;

    invoke-direct {v1, p0}, Lhfo;-><init>(Lcom/google/android/velvet/ui/VelvetLockscreenActivity;)V

    iput-object v1, p0, Lcom/google/android/velvet/ui/VelvetLockscreenActivity;->dfk:Lhfo;

    .line 39
    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetLockscreenActivity;->dfk:Lhfo;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/velvet/ui/VelvetLockscreenActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 52
    invoke-super {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->onDestroy()V

    .line 54
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetLockscreenActivity;->dfk:Lhfo;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetLockscreenActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 55
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->onStart()V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x280000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 47
    return-void
.end method
