.class public Lcom/google/android/velvet/ui/VelvetActivity;
.super Lhff;
.source "PG"

# interfaces
.implements Lhac;


# static fields
.field private static final DEFAULT_STATS:Lcom/google/android/shared/search/SearchBoxStats;

.field private static bFD:I


# instance fields
.field private asG:Lcom/google/android/shared/ui/CoScrollContainer;

.field private final deJ:I

.field private final deK:Ljava/lang/Runnable;

.field private deL:Lhgp;

.field private deM:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

.field private deN:Lcom/google/android/velvet/ui/VelvetMainContainer;

.field private deO:Lcom/google/android/velvet/ui/ContextHeaderView;

.field private deP:Lcom/google/android/velvet/ui/MainContentView;

.field private deQ:Lcom/google/android/velvet/ui/MainContentView;

.field private deR:Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;

.field private deS:Landroid/view/View;

.field private deT:Lcom/google/android/velvet/ui/FooterView;

.field private deU:Lejv;

.field private deV:Lejv;

.field private deW:Lejv;

.field private deX:Leju;

.field private deY:Leju;

.field private deZ:I

.field private dfa:Z

.field private dfb:Z

.field private dfc:Z

.field private dfd:J

.field private mFactory:Lgpu;

.field public mPresenter:Lgyz;

.field private mUiThread:Lemm;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 110
    const-string v0, "velvet"

    const-string v1, "android-search-app"

    invoke-static {v0, v1}, Lcom/google/android/shared/search/SearchBoxStats;->aA(Ljava/lang/String;Ljava/lang/String;)Lehj;

    move-result-object v0

    invoke-virtual {v0}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v0

    sput-object v0, Lcom/google/android/velvet/ui/VelvetActivity;->DEFAULT_STATS:Lcom/google/android/shared/search/SearchBoxStats;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;-><init>(I)V

    .line 156
    return-void
.end method

.method protected constructor <init>(I)V
    .locals 2

    .prologue
    .line 158
    invoke-direct {p0}, Lhff;-><init>()V

    .line 113
    new-instance v0, Lhfg;

    const-string v1, "Window focus changed"

    invoke-direct {v0, p0, v1}, Lhfg;-><init>(Lcom/google/android/velvet/ui/VelvetActivity;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deK:Ljava/lang/Runnable;

    .line 159
    iput p1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deJ:I

    .line 160
    sget v0, Lcom/google/android/velvet/ui/VelvetActivity;->bFD:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/velvet/ui/VelvetActivity;->bFD:I

    .line 161
    return-void
.end method

.method private static C(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 1
    .param p0    # Landroid/content/Intent;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 502
    if-eqz p0, :cond_0

    .line 503
    invoke-static {p0}, Lesp;->C(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object p0

    .line 504
    if-nez p0, :cond_0

    .line 505
    const v0, 0xd97062

    invoke-static {v0}, Lhwt;->lx(I)V

    .line 508
    :cond_0
    return-object p0
.end method

.method private aOh()V
    .locals 4

    .prologue
    .line 567
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->dfc:Z

    if-eqz v0, :cond_0

    .line 568
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deY:Leju;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Leju;->L(F)V

    .line 573
    :goto_0
    return-void

    .line 570
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deY:Leju;

    iget v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deZ:I

    iget v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deZ:I

    iget-object v3, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deS:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Leju;->aP(II)V

    goto :goto_0
.end method

.method private aOi()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 588
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->dfa:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->dfb:Z

    if-eqz v0, :cond_0

    .line 589
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deX:Leju;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deO:Lcom/google/android/velvet/ui/ContextHeaderView;

    invoke-virtual {v1}, Lcom/google/android/velvet/ui/ContextHeaderView;->getBottom()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deO:Lcom/google/android/velvet/ui/ContextHeaderView;

    invoke-virtual {v2}, Lcom/google/android/velvet/ui/ContextHeaderView;->getBottom()I

    move-result v2

    add-int/lit8 v2, v2, 0x32

    const v3, 0x3f70a3d7    # 0.94f

    invoke-virtual {v0, v1, v2, v3, v4}, Leju;->a(IIFF)V

    .line 595
    :goto_0
    return-void

    .line 593
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deX:Leju;

    invoke-virtual {v0, v4}, Leju;->L(F)V

    goto :goto_0
.end method

.method private aOj()I
    .locals 4

    .prologue
    .line 769
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "status_bar_height"

    const-string v2, "dimen"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 770
    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private kR(I)I
    .locals 2

    .prologue
    .line 512
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 513
    int-to-float v1, p1

    div-float v0, v1, v0

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final G(ZZ)V
    .locals 5

    .prologue
    const/16 v4, 0x12c

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 663
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->dfa:Z

    if-eq p1, v0, :cond_1

    .line 664
    iput-boolean p1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->dfa:Z

    .line 665
    if-nez p2, :cond_0

    .line 666
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deW:Lejv;

    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->aOj()I

    move-result v1

    invoke-virtual {v0, v1}, Lejv;->hU(I)V

    .line 667
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deW:Lejv;

    invoke-virtual {v0, v3}, Lejv;->eW(Z)V

    .line 668
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deQ:Lcom/google/android/velvet/ui/MainContentView;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/MainContentView;->aNX()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 669
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deW:Lejv;

    invoke-virtual {v0, v4}, Lejv;->hT(I)V

    .line 670
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deV:Lejv;

    invoke-virtual {v0, v4}, Lejv;->hT(I)V

    .line 673
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deN:Lcom/google/android/velvet/ui/VelvetMainContainer;

    iget-boolean v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->dfa:Z

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/VelvetMainContainer;->go(Z)V

    .line 674
    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deW:Lejv;

    if-eqz p1, :cond_2

    const/4 v0, 0x2

    :goto_0
    invoke-virtual {v1, v0, p2, v3}, Lejv;->d(IZZ)V

    .line 676
    invoke-virtual {p0, p2}, Lcom/google/android/velvet/ui/VelvetActivity;->gn(Z)V

    .line 677
    if-nez p2, :cond_1

    .line 678
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deW:Lejv;

    invoke-virtual {v0, v2}, Lejv;->hT(I)V

    .line 679
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deV:Lejv;

    invoke-virtual {v0, v2}, Lejv;->hT(I)V

    .line 680
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deW:Lejv;

    invoke-virtual {v0, v2}, Lejv;->hU(I)V

    .line 681
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deW:Lejv;

    invoke-virtual {v0, v2}, Lejv;->eW(Z)V

    .line 684
    :cond_1
    return-void

    .line 674
    :cond_2
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public final I(IZ)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 700
    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deN:Lcom/google/android/velvet/ui/VelvetMainContainer;

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    invoke-virtual {v2, v1, v0}, Lcom/google/android/velvet/ui/VelvetMainContainer;->H(ZZ)V

    .line 703
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deU:Lejv;

    invoke-virtual {v0, p1, p2, v1}, Lejv;->d(IZZ)V

    .line 704
    return-void

    .line 700
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Pm()Lcom/google/android/search/shared/actions/VoiceAction;
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lgyz;

    invoke-virtual {v0}, Lgyz;->Pm()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/shared/search/Suggestion;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 634
    invoke-static {p0, p1, p2}, Ledg;->a(Landroid/content/Context;Lcom/google/android/shared/search/Suggestion;Ljava/lang/Runnable;)V

    .line 635
    return-void
.end method

.method public final a(Letj;)V
    .locals 2

    .prologue
    .line 782
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lgyz;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 784
    const-string v0, "VelvetActivity state"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 786
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deP:Lcom/google/android/velvet/ui/MainContentView;

    if-eqz v0, :cond_0

    .line 787
    const-string v0, "Current front content:"

    invoke-virtual {p1, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 788
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deP:Lcom/google/android/velvet/ui/MainContentView;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 790
    :cond_0
    const-string v0, "Current back content:"

    invoke-virtual {p1, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 791
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deQ:Lcom/google/android/velvet/ui/MainContentView;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 792
    const-string v0, "mFooterHider"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deU:Lejv;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 793
    const-string v0, "mSearchPlateHider"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deV:Lejv;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 794
    const-string v0, "mContextHeaderHider"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deW:Lejv;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 801
    return-void
.end method

.method public final aLV()I
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deR:Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public final aMA()V
    .locals 1

    .prologue
    .line 382
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->setIntent(Landroid/content/Intent;)V

    .line 383
    return-void
.end method

.method public final aMB()V
    .locals 1

    .prologue
    .line 708
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deV:Lejv;

    invoke-virtual {v0}, Lejv;->show()V

    .line 709
    return-void
.end method

.method public final aMC()V
    .locals 1

    .prologue
    .line 713
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deV:Lejv;

    invoke-virtual {v0}, Lejv;->hide()V

    .line 714
    return-void
.end method

.method public final aMD()V
    .locals 1

    .prologue
    .line 718
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deU:Lejv;

    invoke-virtual {v0}, Lejv;->show()V

    .line 719
    return-void
.end method

.method public final aME()V
    .locals 1

    .prologue
    .line 723
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deU:Lejv;

    invoke-virtual {v0}, Lejv;->hide()V

    .line 724
    return-void
.end method

.method public final aMF()V
    .locals 4

    .prologue
    .line 742
    const v0, 0x7f11048b

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x43b40000    # 360.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->rotationBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 743
    return-void
.end method

.method public final aMG()Lcom/google/android/velvet/ui/MainContentView;
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deP:Lcom/google/android/velvet/ui/MainContentView;

    if-nez v0, :cond_0

    .line 317
    const v0, 0x7f11048d

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 318
    const v1, 0x7f110261

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/MainContentView;

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deP:Lcom/google/android/velvet/ui/MainContentView;

    .line 319
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deP:Lcom/google/android/velvet/ui/MainContentView;

    invoke-virtual {v0, p0}, Lcom/google/android/velvet/ui/MainContentView;->a(Lcom/google/android/velvet/ui/VelvetActivity;)V

    .line 320
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deP:Lcom/google/android/velvet/ui/MainContentView;

    new-instance v1, Lhfm;

    invoke-direct {v1, p0}, Lhfm;-><init>(Lcom/google/android/velvet/ui/VelvetActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/MainContentView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deP:Lcom/google/android/velvet/ui/MainContentView;

    return-object v0
.end method

.method public final aMH()Lcom/google/android/velvet/ui/MainContentView;
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deQ:Lcom/google/android/velvet/ui/MainContentView;

    return-object v0
.end method

.method public final aMI()Lgzz;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deR:Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;

    return-object v0
.end method

.method public final aMJ()Lgxb;
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deO:Lcom/google/android/velvet/ui/ContextHeaderView;

    return-object v0
.end method

.method public final aMK()Lgxh;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deT:Lcom/google/android/velvet/ui/FooterView;

    return-object v0
.end method

.method public final aML()V
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deN:Lcom/google/android/velvet/ui/VelvetMainContainer;

    invoke-static {v0}, Lesp;->be(Landroid/view/View;)V

    .line 540
    return-void
.end method

.method public final aMM()Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deM:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    return-object v0
.end method

.method public final aMz()J
    .locals 2

    .prologue
    .line 340
    iget-wide v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->dfd:J

    return-wide v0
.end method

.method protected aOf()J
    .locals 2

    .prologue
    .line 303
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method protected aOg()Z
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x1

    return v0
.end method

.method public final atC()Lcom/google/android/shared/ui/CoScrollContainer;
    .locals 1

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    return-object v0
.end method

.method public final aun()V
    .locals 1

    .prologue
    .line 658
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deL:Lhgp;

    invoke-virtual {v0}, Lhgp;->aun()V

    .line 659
    return-void
.end method

.method public bE(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 753
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_0

    .line 754
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/CoScrollContainer;->atk()Lejd;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 756
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/shared/ui/CoScrollContainer;->addView(Landroid/view/View;)V

    .line 757
    return-void
.end method

.method public final bF(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 761
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lejd;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lejd;->hM(I)V

    .line 763
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/shared/ui/CoScrollContainer;->removeView(Landroid/view/View;)V

    .line 764
    invoke-virtual {p1, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 765
    invoke-virtual {p1, v2}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 766
    return-void
.end method

.method public final be(II)V
    .locals 5

    .prologue
    .line 517
    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/VelvetActivity;->kR(I)I

    move-result v0

    .line 518
    invoke-direct {p0, p2}, Lcom/google/android/velvet/ui/VelvetActivity;->kR(I)I

    move-result v1

    .line 519
    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mUiThread:Lemm;

    new-instance v3, Lhfn;

    const-string v4, "Process browser dimensions"

    invoke-direct {v3, p0, v4, v0, v1}, Lhfn;-><init>(Lcom/google/android/velvet/ui/VelvetActivity;Ljava/lang/String;II)V

    invoke-interface {v2, v3}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 525
    return-void
.end method

.method public final d(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 737
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deM:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->d(Landroid/view/View$OnTouchListener;)V

    .line 738
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 805
    invoke-super {p0, p1, p2, p3, p4}, Lhff;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 806
    new-instance v0, Letj;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Letj;-><init>(Landroid/content/res/Resources;)V

    .line 807
    invoke-virtual {v0, p0}, Letj;->b(Leti;)V

    .line 808
    invoke-virtual {v0, p3, p1}, Letj;->a(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 809
    return-void
.end method

.method public final e(IZZ)V
    .locals 1

    .prologue
    .line 689
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deV:Lejv;

    invoke-virtual {v0, p1, p2, p3}, Lejv;->d(IZZ)V

    .line 692
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->dfc:Z

    .line 693
    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->aOh()V

    .line 694
    return-void

    .line 692
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public finalize()V
    .locals 1

    .prologue
    .line 370
    sget v0, Lcom/google/android/velvet/ui/VelvetActivity;->bFD:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/google/android/velvet/ui/VelvetActivity;->bFD:I

    .line 371
    return-void
.end method

.method public finish()V
    .locals 0

    .prologue
    .line 468
    invoke-super {p0}, Lhff;->finish()V

    .line 469
    return-void
.end method

.method public final getActivity()Landroid/app/Activity;
    .locals 0

    .prologue
    .line 732
    return-object p0
.end method

.method public final gf(Z)V
    .locals 1

    .prologue
    .line 580
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->dfb:Z

    if-eq v0, p1, :cond_0

    .line 581
    iput-boolean p1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->dfb:Z

    .line 582
    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->aOi()V

    .line 584
    :cond_0
    return-void
.end method

.method public gg(Z)V
    .locals 0

    .prologue
    .line 749
    return-void
.end method

.method public final gn(Z)V
    .locals 2

    .prologue
    .line 560
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->dfa:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->aOj()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deZ:I

    .line 561
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deV:Lejv;

    iget v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deZ:I

    invoke-virtual {v0, v1, p1}, Lejv;->C(IZ)V

    .line 562
    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->aOi()V

    .line 563
    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->aOh()V

    .line 564
    return-void

    .line 560
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lgyz;

    invoke-virtual {v0, p1}, Lgyz;->i(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 489
    return-void
.end method

.method public final kQ(I)V
    .locals 2

    .prologue
    .line 480
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 481
    return-void
.end method

.method public final nJ(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 775
    new-instance v0, Lcku;

    invoke-direct {v0}, Lcku;-><init>()V

    .line 776
    invoke-virtual {v0, p1}, Lcku;->setText(Ljava/lang/String;)V

    .line 777
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "debug_dialog"

    invoke-virtual {v0, v1, v2}, Lcku;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 778
    return-void
.end method

.method public final nK(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 813
    sget v0, Lesp;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 814
    new-instance v0, Landroid/app/ActivityManager$TaskDescription;

    invoke-direct {v0, p1}, Landroid/app/ActivityManager$TaskDescription;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->setTaskDescription(Landroid/app/ActivityManager$TaskDescription;)V

    .line 816
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 651
    invoke-super {p0, p1, p2, p3}, Lhff;->onActivityResult(IILandroid/content/Intent;)V

    .line 652
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lgyz;

    invoke-virtual {v0}, Lgyz;->aMg()V

    .line 653
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deL:Lhgp;

    invoke-virtual {v0, p1, p2, p3}, Lhgp;->a(IILandroid/content/Intent;)V

    .line 654
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 0

    .prologue
    .line 456
    invoke-super {p0}, Lhff;->onAttachedToWindow()V

    .line 457
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 166
    if-eqz p1, :cond_0

    invoke-static {p1}, Lesp;->K(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object p1

    if-nez p1, :cond_0

    const v0, 0xd97062

    invoke-static {v0}, Lhwt;->lx(I)V

    .line 167
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/velvet/ui/VelvetActivity;->C(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 169
    iget v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deJ:I

    invoke-static {p0, v1}, Legr;->b(Landroid/app/Activity;I)V

    .line 170
    if-eqz v0, :cond_1

    .line 171
    iget v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deJ:I

    invoke-static {p0, v0, v1, v9}, Legr;->a(Landroid/app/Activity;Landroid/content/Intent;IZ)V

    .line 174
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->dfd:J

    .line 180
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    .line 181
    invoke-virtual {v1}, Lgql;->aJU()Lgpu;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mFactory:Lgpu;

    .line 183
    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mFactory:Lgpu;

    const-wide/32 v4, 0x1020223c

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->aOf()J

    move-result-wide v6

    or-long/2addr v4, v6

    new-instance v3, Lcom/google/android/search/shared/service/ClientConfig;

    sget-object v6, Lcom/google/android/velvet/ui/VelvetActivity;->DEFAULT_STATS:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/search/shared/service/ClientConfig;-><init>(JLcom/google/android/shared/search/SearchBoxStats;)V

    invoke-virtual {v2, p0, v3}, Lgpu;->a(Lhac;Lcom/google/android/search/shared/service/ClientConfig;)Lgyz;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lgyz;

    .line 187
    new-instance v2, Lhgp;

    const/16 v3, 0x64

    invoke-direct {v2, p0, v3}, Lhgp;-><init>(Landroid/app/Activity;I)V

    iput-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deL:Lhgp;

    .line 188
    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deL:Lhgp;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->aOg()Z

    move-result v3

    invoke-virtual {v2, v3}, Lhgp;->fd(Z)V

    .line 190
    invoke-super {p0, p1}, Lhff;->onCreate(Landroid/os/Bundle;)V

    .line 191
    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deL:Lhgp;

    invoke-virtual {v2, p1}, Lhgp;->x(Landroid/os/Bundle;)V

    .line 193
    iget-object v1, v1, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->aus()Leqo;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mUiThread:Lemm;

    .line 195
    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lgyz;

    invoke-virtual {v1, p1, v0}, Lgyz;->a(Landroid/os/Bundle;Landroid/content/Intent;)V

    .line 197
    const v0, 0x7f0401b7

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->setContentView(I)V

    .line 199
    const v0, 0x7f11048b

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deM:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    .line 200
    const v0, 0x7f11048c

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/VelvetMainContainer;

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deN:Lcom/google/android/velvet/ui/VelvetMainContainer;

    .line 201
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deN:Lcom/google/android/velvet/ui/VelvetMainContainer;

    new-instance v1, Lhfh;

    invoke-direct {v1, p0}, Lhfh;-><init>(Lcom/google/android/velvet/ui/VelvetActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/VelvetMainContainer;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 208
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deM:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    new-instance v1, Lhfi;

    invoke-direct {v1, p0}, Lhfi;-><init>(Lcom/google/android/velvet/ui/VelvetActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->a(Landroid/view/View$OnKeyListener;)V

    .line 214
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deL:Lhgp;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deM:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    invoke-virtual {v0, v1}, Lhgp;->a(Lcom/google/android/velvet/ui/VelvetTopLevelContainer;)V

    .line 216
    const v0, 0x7f11025c

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/ui/CoScrollContainer;

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    .line 218
    const v0, 0x7f110136

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/ContextHeaderView;

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deO:Lcom/google/android/velvet/ui/ContextHeaderView;

    .line 219
    new-instance v0, Lejv;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deO:Lcom/google/android/velvet/ui/ContextHeaderView;

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-direct {v0, v1, v2, v8}, Lejv;-><init>(Landroid/view/View;Lekf;Z)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deW:Lejv;

    .line 220
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deW:Lejv;

    invoke-virtual {v0, v10, v9, v8}, Lejv;->d(IZZ)V

    .line 221
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deO:Lcom/google/android/velvet/ui/ContextHeaderView;

    new-instance v1, Lhfj;

    invoke-direct {v1, p0}, Lhfj;-><init>(Lcom/google/android/velvet/ui/VelvetActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/ContextHeaderView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 229
    const v0, 0x7f11025d

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/MainContentView;

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deQ:Lcom/google/android/velvet/ui/MainContentView;

    .line 230
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deQ:Lcom/google/android/velvet/ui/MainContentView;

    invoke-virtual {v0, p0}, Lcom/google/android/velvet/ui/MainContentView;->a(Lcom/google/android/velvet/ui/VelvetActivity;)V

    .line 232
    const v0, 0x7f11048f

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deR:Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;

    .line 233
    new-instance v0, Lejv;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deR:Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-direct {v0, v1, v2, v8}, Lejv;-><init>(Landroid/view/View;Lekf;Z)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deV:Lejv;

    .line 234
    const v0, 0x7f1103b3

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 235
    new-instance v1, Leju;

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-direct {v1, v0, v2}, Leju;-><init>(Landroid/view/View;Lekf;)V

    iput-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deX:Leju;

    .line 236
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deX:Leju;

    iput-boolean v8, v0, Leju;->ccb:Z

    .line 237
    const v0, 0x7f1101e3

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/FooterView;

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deT:Lcom/google/android/velvet/ui/FooterView;

    .line 238
    new-instance v0, Lejv;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deT:Lcom/google/android/velvet/ui/FooterView;

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-direct {v0, v1, v2, v9}, Lejv;-><init>(Landroid/view/View;Lekf;Z)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deU:Lejv;

    .line 239
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deU:Lejv;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deV:Lejv;

    invoke-virtual {v0, v1}, Lejv;->a(Lejv;)V

    .line 240
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deU:Lejv;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deV:Lejv;

    invoke-virtual {v0, v1}, Lejv;->b(Lejv;)V

    .line 241
    const v0, 0x7f110490

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deS:Landroid/view/View;

    .line 242
    new-instance v0, Leju;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deS:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetActivity;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-direct {v0, v1, v2}, Leju;-><init>(Landroid/view/View;Lekf;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deY:Leju;

    .line 244
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    new-instance v1, Lhfk;

    invoke-direct {v1, p0}, Lhfk;-><init>(Lcom/google/android/velvet/ui/VelvetActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/CoScrollContainer;->c(Landroid/view/View$OnTouchListener;)V

    .line 260
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deM:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    new-instance v1, Lhfl;

    invoke-direct {v1, p0}, Lhfl;-><init>(Lcom/google/android/velvet/ui/VelvetActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->a(Lejr;)V

    .line 275
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lgyz;

    invoke-virtual {v0}, Lgyz;->aLW()V

    .line 277
    invoke-virtual {p0, v10}, Lcom/google/android/velvet/ui/VelvetActivity;->setVolumeControlStream(I)V

    .line 295
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lgyz;

    invoke-virtual {v0, p1}, Lgyz;->f(Landroid/view/Menu;)V

    .line 629
    invoke-super {p0, p1}, Lhff;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 446
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mUiThread:Lemm;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deK:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lemm;->i(Ljava/lang/Runnable;)V

    .line 447
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lgyz;

    invoke-virtual {v0}, Lgyz;->onDestroy()V

    .line 449
    iget v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deJ:I

    invoke-static {p0, v0}, Legr;->e(Landroid/app/Activity;I)V

    .line 450
    invoke-super {p0}, Lhff;->onDestroy()V

    .line 451
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 462
    invoke-super {p0}, Lhff;->onDetachedFromWindow()V

    .line 463
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lgyz;

    invoke-virtual {v0, p1, p2}, Lgyz;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 600
    if-nez v0, :cond_0

    .line 601
    invoke-super {p0, p1, p2}, Lhff;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 603
    :cond_0
    return v0
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 435
    iget v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deJ:I

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1}, Legr;->a(Landroid/app/Activity;Landroid/content/Intent;IZ)V

    .line 438
    invoke-super {p0, p1}, Lhff;->onNewIntent(Landroid/content/Intent;)V

    .line 439
    invoke-static {p1}, Lcom/google/android/velvet/ui/VelvetActivity;->C(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 440
    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lgyz;

    invoke-virtual {v1, v0}, Lgyz;->onNewIntent(Landroid/content/Intent;)V

    .line 441
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 407
    .line 408
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lgyz;

    invoke-virtual {v0}, Lgyz;->onPause()V

    .line 409
    invoke-super {p0}, Lhff;->onPause()V

    .line 410
    return-void
.end method

.method public onRestart()V
    .locals 0

    .prologue
    .line 430
    invoke-super {p0}, Lhff;->onRestart()V

    .line 431
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 398
    invoke-super {p0}, Lhff;->onResume()V

    .line 399
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lgyz;

    invoke-virtual {v0}, Lgyz;->onResume()V

    .line 400
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deM:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->aOm()V

    .line 401
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 375
    invoke-super {p0, p1}, Lhff;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 376
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lgyz;

    invoke-virtual {v0, p1}, Lgyz;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 377
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deL:Lhgp;

    invoke-virtual {v0, p1}, Lhgp;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 378
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 388
    iget v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deJ:I

    invoke-static {p0, v0}, Legr;->c(Landroid/app/Activity;I)V

    .line 389
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deM:Lcom/google/android/velvet/ui/VelvetTopLevelContainer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/velvet/ui/VelvetTopLevelContainer;->gp(Z)V

    .line 390
    invoke-super {p0}, Lhff;->onStart()V

    .line 391
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lgyz;

    invoke-virtual {v0}, Lgyz;->onStart()V

    .line 392
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lgyz;

    invoke-virtual {v0}, Lgyz;->onStop()V

    .line 418
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deQ:Lcom/google/android/velvet/ui/MainContentView;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/MainContentView;->aNV()V

    .line 419
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deP:Lcom/google/android/velvet/ui/MainContentView;

    if-eqz v0, :cond_0

    .line 420
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deP:Lcom/google/android/velvet/ui/MainContentView;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/MainContentView;->aNV()V

    .line 423
    :cond_0
    iget v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deJ:I

    invoke-static {p0, v0}, Legr;->d(Landroid/app/Activity;I)V

    .line 424
    invoke-super {p0}, Lhff;->onStop()V

    .line 425
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    .prologue
    .line 544
    invoke-super {p0, p1}, Lhff;->onWindowFocusChanged(Z)V

    .line 546
    if-eqz p1, :cond_0

    .line 550
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mUiThread:Lemm;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deK:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lemm;->execute(Ljava/lang/Runnable;)V

    .line 555
    :goto_0
    return-void

    .line 552
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mUiThread:Lemm;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deK:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lemm;->i(Ljava/lang/Runnable;)V

    .line 553
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->mPresenter:Lgyz;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lgyz;->onWindowFocusChanged(Z)V

    goto :goto_0
.end method

.method public final wr()Leoj;
    .locals 1

    .prologue
    .line 646
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetActivity;->deL:Lhgp;

    return-object v0
.end method
