.class public Lcom/google/android/velvet/location/VehicleParkReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private final aUk:Lgql;

.field private final cjP:Lfdo;

.field private final mAsyncServices:Lema;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/location/VehicleParkReceiver;-><init>(Lgql;)V

    .line 33
    return-void
.end method

.method protected constructor <init>(Lgql;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/velvet/location/VehicleParkReceiver;->aUk:Lgql;

    .line 38
    invoke-virtual {p1}, Lgql;->aJr()Lfdb;

    move-result-object v0

    iget-object v0, v0, Lfdb;->cjP:Lfdo;

    iput-object v0, p0, Lcom/google/android/velvet/location/VehicleParkReceiver;->cjP:Lfdo;

    .line 39
    iget-object v0, p1, Lgql;->mAsyncServices:Lema;

    iput-object v0, p0, Lcom/google/android/velvet/location/VehicleParkReceiver;->mAsyncServices:Lema;

    .line 40
    return-void
.end method

.method public static synthetic a(Lcom/google/android/velvet/location/VehicleParkReceiver;)Lfdo;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/velvet/location/VehicleParkReceiver;->cjP:Lfdo;

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/velvet/location/VehicleParkReceiver;->aUk:Lgql;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->ayn()Lfer;

    move-result-object v1

    .line 49
    if-nez v1, :cond_1

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    invoke-static {p2}, Lcom/google/android/gms/location/ActivityRecognitionResult;->h(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 56
    :try_start_0
    invoke-static {p2}, Lcom/google/android/gms/location/ActivityRecognitionResult;->i(Landroid/content/Intent;)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v0

    .line 58
    iget-object v2, p0, Lcom/google/android/velvet/location/VehicleParkReceiver;->mAsyncServices:Lema;

    new-instance v3, Lguu;

    invoke-direct {v3, p0, v1, v0}, Lguu;-><init>(Lcom/google/android/velvet/location/VehicleParkReceiver;Lfer;Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    invoke-virtual {v2, v3}, Lema;->a(Leri;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 60
    :catch_0
    move-exception v0

    .line 61
    const-string v1, "VehicleParkReceiver"

    const-string v2, "Exception while extract result."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 67
    :cond_2
    const-string v0, "com.google.android.sidekick.main.location.GPS_TIMEOUT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 68
    invoke-virtual {v1}, Lfer;->ayO()V

    .line 71
    :cond_3
    const-string v0, "com.google.android.velvet.location.GMS_CORE_HIGH_POWER_LOCATION"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    const-string v0, "com.google.android.location.LOCATION"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 73
    if-eqz v0, :cond_4

    instance-of v2, v0, Landroid/location/Location;

    if-nez v2, :cond_5

    .line 74
    :cond_4
    const-string v1, "VehicleParkReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Received bad location: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 77
    :cond_5
    check-cast v0, Landroid/location/Location;

    .line 79
    invoke-virtual {v1, v0}, Lfer;->i(Landroid/location/Location;)V

    goto :goto_0
.end method
