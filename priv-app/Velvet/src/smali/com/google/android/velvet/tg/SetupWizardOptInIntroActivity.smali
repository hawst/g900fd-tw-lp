.class public Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;
.super Landroid/app/Activity;
.source "PG"

# interfaces
.implements Lefh;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field private as:Z

.field private dcP:Z

.field private dcQ:Lhct;

.field private dcR:J

.field public mAccount:Landroid/accounts/Account;

.field public mAccountConfigurationHelper:Lhcb;

.field private mClock:Lemp;

.field public mLoginHelper:Lcrh;

.field private mTaskRunner:Lerk;

.field private mUserInteractionLogger:Lcpx;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 229
    return-void
.end method

.method public static synthetic a(Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->as:Z

    return v0
.end method

.method public static synthetic b(Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;)Lhct;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->dcQ:Lhct;

    return-object v0
.end method

.method private i(Lesk;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x7d0

    .line 197
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->dcR:J

    sub-long/2addr v0, v2

    .line 198
    cmp-long v2, v0, v4

    if-ltz v2, :cond_0

    .line 199
    invoke-interface {p1}, Lesk;->run()V

    .line 203
    :goto_0
    return-void

    .line 201
    :cond_0
    iget-object v2, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mTaskRunner:Lerk;

    sub-long v0, v4, v0

    invoke-interface {v2, p1, v0, v1}, Lerk;->a(Lesk;J)V

    goto :goto_0
.end method

.method public static kL(I)V
    .locals 1

    .prologue
    const/4 v0, 0x2

    .line 146
    invoke-static {v0, p0, v0}, Lhcd;->z(III)V

    .line 150
    return-void
.end method


# virtual methods
.method public final L(Landroid/accounts/Account;)V
    .locals 2

    .prologue
    .line 161
    new-instance v0, Lhcr;

    const-string v1, "Start opt-in"

    invoke-direct {v0, p0, v1, p1}, Lhcr;-><init>(Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;Ljava/lang/String;Landroid/accounts/Account;)V

    invoke-direct {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->i(Lesk;)V

    .line 175
    return-void
.end method

.method public final a(Lcom/google/android/setupwizard/SetupWizardNavBar;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 274
    invoke-virtual {p1}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aon()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 275
    iget-boolean v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->dcP:Z

    if-eqz v0, :cond_0

    .line 276
    invoke-virtual {p1}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aom()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 278
    :cond_0
    return-void
.end method

.method public final aoo()V
    .locals 0

    .prologue
    .line 282
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->onBackPressed()V

    .line 283
    return-void
.end method

.method public final aop()V
    .locals 0

    .prologue
    .line 287
    return-void
.end method

.method public final kO(I)V
    .locals 3

    .prologue
    .line 182
    new-instance v0, Lhcs;

    const-string v1, "Finish activity"

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lhcs;-><init>(Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;Ljava/lang/String;I)V

    invoke-direct {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->i(Lesk;)V

    .line 189
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 207
    packed-switch p1, :pswitch_data_0

    .line 214
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 216
    :goto_0
    return-void

    .line 209
    :pswitch_0
    invoke-virtual {p0, p2}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->setResult(I)V

    .line 210
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->finish()V

    goto :goto_0

    .line 207
    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->dcP:Z

    if-nez v0, :cond_0

    .line 221
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->setResult(I)V

    .line 222
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->finish()V

    .line 225
    const/high16 v0, 0x10a0000

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->overridePendingTransition(II)V

    .line 227
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    .line 76
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DC()Lemp;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mClock:Lemp;

    .line 80
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DR()Lcpx;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mUserInteractionLogger:Lcpx;

    .line 81
    iget-object v1, v0, Lgql;->mAsyncServices:Lema;

    iput-object v1, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mTaskRunner:Lerk;

    .line 82
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DL()Lcrh;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mLoginHelper:Lcrh;

    .line 83
    new-instance v1, Lhcb;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->En()Leue;

    move-result-object v2

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DF()Lcin;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lhcb;-><init>(Leue;Lcin;)V

    iput-object v1, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mAccountConfigurationHelper:Lhcb;

    .line 87
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "noBack"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->dcP:Z

    .line 89
    if-eqz p1, :cond_0

    .line 90
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mAccount:Landroid/accounts/Account;

    .line 93
    :cond_0
    const v0, 0x7f04016c

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->setContentView(I)V

    .line 94
    const v0, 0x7f1101a9

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 95
    const v1, 0x7f0a0489

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 97
    const v0, 0x7f1103f9

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 98
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04016b

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 101
    const/high16 v0, 0x10a0000

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->overridePendingTransition(II)V

    .line 102
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 132
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->as:Z

    .line 135
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->dcQ:Lhct;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->dcQ:Lhct;

    invoke-virtual {v0}, Lhct;->cancel()V

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->dcQ:Lhct;

    .line 139
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 115
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->as:Z

    .line 118
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mUserInteractionLogger:Lcpx;

    const-string v1, "SETUP_WIZARD_INTRO"

    invoke-virtual {v0, v1}, Lcpx;->io(Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->dcR:J

    .line 121
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->L(Landroid/accounts/Account;)V

    .line 128
    :goto_0
    return-void

    .line 126
    :cond_0
    new-instance v0, Lhct;

    iget-object v1, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mTaskRunner:Lerk;

    invoke-direct {v0, p0, v1}, Lhct;-><init>(Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;Lerk;)V

    iput-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->dcQ:Lhct;

    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->dcQ:Lhct;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lhct;->a([Ljava/lang/Object;)Lenp;

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 106
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    .line 109
    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/velvet/tg/SetupWizardOptInIntroActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 111
    :cond_0
    return-void
.end method
