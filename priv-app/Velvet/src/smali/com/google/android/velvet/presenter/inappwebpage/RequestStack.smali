.class public final Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Iterable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final dbP:Ljava/util/ArrayDeque;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lhbp;

    invoke-direct {v0}, Lhbp;-><init>()V

    sput-object v0, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;->dbP:Ljava/util/ArrayDeque;

    .line 102
    return-void
.end method


# virtual methods
.method public final a(Landroid/util/Printer;)V
    .locals 3

    .prologue
    .line 94
    new-instance v1, Leqg;

    invoke-direct {v1, p1}, Leqg;-><init>(Landroid/util/Printer;)V

    .line 95
    const-string v0, "RequestStack:"

    invoke-virtual {v1, v0}, Leqg;->println(Ljava/lang/String;)V

    .line 96
    const-string v0, "  "

    invoke-virtual {v1, v0}, Leqg;->ll(Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;->dbP:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/presenter/inappwebpage/Request;

    .line 98
    invoke-virtual {v0}, Lcom/google/android/velvet/presenter/inappwebpage/Request;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Leqg;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 100
    :cond_0
    return-void
.end method

.method public final aNo()Lcom/google/android/velvet/presenter/inappwebpage/Request;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;->dbP:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/presenter/inappwebpage/Request;

    return-object v0
.end method

.method public final aNp()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;->dbP:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    .line 56
    return-void
.end method

.method public final c(Lcom/google/android/velvet/presenter/inappwebpage/Request;)V
    .locals 1

    .prologue
    .line 45
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    iget-object v0, p0, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;->dbP:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->addFirst(Ljava/lang/Object;)V

    .line 47
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;->dbP:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;->dbP:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 77
    invoke-static {p0}, Lifo;->bc(Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "mDeque"

    iget-object v2, p0, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;->dbP:Ljava/util/ArrayDeque;

    invoke-virtual {v0, v1, v2}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    invoke-virtual {v0}, Lifp;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/velvet/presenter/inappwebpage/RequestStack;->dbP:Ljava/util/ArrayDeque;

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 90
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 91
    return-void
.end method
