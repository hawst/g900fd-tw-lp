.class public Lcom/google/android/velvet/ui/ContextHeaderView;
.super Lcom/google/android/search/shared/ui/CrossfadingWebImageView;
.source "PG"

# interfaces
.implements Lgxb;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;-><init>(Landroid/content/Context;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method


# virtual methods
.method public final b(Landroid/graphics/drawable/Drawable;ZLandroid/view/View$OnClickListener;)V
    .locals 2
    .param p3    # Landroid/view/View$OnClickListener;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/ContextHeaderView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 50
    if-eqz p2, :cond_0

    .line 51
    invoke-virtual {p0, p3}, Lcom/google/android/velvet/ui/ContextHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/ContextHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a064e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/ContextHeaderView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 60
    :goto_0
    return-void

    .line 56
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ui/ContextHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/ContextHeaderView;->setClickable(Z)V

    .line 58
    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ui/ContextHeaderView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    .line 34
    invoke-super {p0}, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->onFinishInflate()V

    .line 38
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/ContextHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Leot;->av(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    .line 39
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/ContextHeaderView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 40
    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v2, v0, :cond_0

    .line 41
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 42
    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ui/ContextHeaderView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 44
    :cond_0
    return-void
.end method
