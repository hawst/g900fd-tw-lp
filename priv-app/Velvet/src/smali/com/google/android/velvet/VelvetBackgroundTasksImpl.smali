.class public final Lcom/google/android/velvet/VelvetBackgroundTasksImpl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgpp;


# instance fields
.field private final cSB:Ljava/lang/Runnable;

.field private final cSC:Ljava/util/Map;

.field final cSD:Ljava/util/Map;

.field private cSE:Z

.field public final dK:Ljava/lang/Object;

.field private final mAlarmHelper:Ldjx;

.field private final mAppContext:Landroid/content/Context;

.field private final mBgExecutor:Ljava/util/concurrent/Executor;

.field public final mClock:Lemp;

.field private final mConfig:Lcjs;

.field private final mFactory:Lgpu;

.field private final mGsaConfig:Lchk;

.field private final mPendingIntentFactory:Lfda;

.field public final mSettings:Lcke;


# direct methods
.method public constructor <init>(Lemp;Lcjs;Lchk;Lcke;Ljava/util/concurrent/Executor;Lgpu;Ldjx;Lfda;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Lgpq;

    const-string v1, "schedule"

    const/4 v2, 0x0

    new-array v2, v2, [I

    invoke-direct {v0, p0, v1, v2}, Lgpq;-><init>(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSB:Ljava/lang/Runnable;

    .line 94
    iput-object p1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mClock:Lemp;

    .line 95
    iput-object p2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mConfig:Lcjs;

    .line 96
    iput-object p3, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mGsaConfig:Lchk;

    .line 97
    iput-object p4, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcke;

    .line 98
    iput-object p5, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mBgExecutor:Ljava/util/concurrent/Executor;

    .line 99
    iput-object p6, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mFactory:Lgpu;

    .line 100
    iput-object p7, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mAlarmHelper:Ldjx;

    .line 101
    iput-object p8, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mPendingIntentFactory:Lfda;

    .line 102
    iput-object p9, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mAppContext:Landroid/content/Context;

    .line 103
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->dK:Ljava/lang/Object;

    .line 104
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSC:Ljava/util/Map;

    .line 105
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSD:Ljava/util/Map;

    .line 106
    invoke-direct {p0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->aJm()V

    .line 107
    return-void
.end method

.method private aJm()V
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 387
    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Lt()[Ljava/lang/String;

    move-result-object v5

    .line 388
    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Ig()[Ljava/lang/String;

    move-result-object v6

    .line 389
    array-length v7, v5

    move v4, v3

    :goto_0
    if-ge v4, v7, :cond_4

    aget-object v8, v5, v4

    .line 390
    const-wide/16 v0, 0x0

    move v2, v3

    .line 392
    :goto_1
    :try_start_0
    array-length v9, v6

    add-int/lit8 v9, v9, -0x1

    if-ge v2, v9, :cond_3

    .line 393
    aget-object v9, v6, v2

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 394
    add-int/lit8 v0, v2, 0x1

    aget-object v0, v6, v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v9, 0x2

    if-ge v1, v9, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/NumberFormatException;

    invoke-direct {v0}, Ljava/lang/NumberFormatException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 400
    :catch_0
    move-exception v0

    .line 401
    const-string v1, "Velvet.VelvetBackgroundTasksImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v9, "Exception parsing min period of "

    invoke-direct {v2, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, ": "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 389
    :goto_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 394
    :cond_1
    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Ljava/lang/NumberFormatException;

    invoke-direct {v0}, Ljava/lang/NumberFormatException;-><init>()V

    throw v0

    :sswitch_0
    const-wide/32 v0, 0xea60

    mul-long/2addr v0, v10

    .line 392
    :cond_2
    :goto_3
    add-int/lit8 v2, v2, 0x2

    goto :goto_1

    .line 394
    :sswitch_1
    const-wide/32 v0, 0x36ee80

    mul-long/2addr v0, v10

    goto :goto_3

    :sswitch_2
    const-wide/32 v0, 0x5265c00

    mul-long/2addr v0, v10

    goto :goto_3

    .line 397
    :cond_3
    new-instance v2, Lgps;

    invoke-direct {v2, p0, v8, v0, v1}, Lgps;-><init>(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;Ljava/lang/String;J)V

    .line 399
    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSC:Ljava/util/Map;

    invoke-interface {v0, v8, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 404
    :cond_4
    return-void

    .line 394
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_2
        0x68 -> :sswitch_1
        0x6d -> :sswitch_0
    .end sparse-switch
.end method

.method private static m(JJ)J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 471
    cmp-long v0, p0, v2

    if-nez v0, :cond_0

    .line 476
    :goto_0
    return-wide p2

    .line 473
    :cond_0
    cmp-long v0, p2, v2

    if-nez v0, :cond_1

    move-wide p2, p0

    .line 474
    goto :goto_0

    .line 476
    :cond_1
    invoke-static {p0, p1, p2, p3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p2

    goto :goto_0
.end method

.method private s(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 132
    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 133
    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSC:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgps;

    iput-wide p2, v0, Lgps;->cSI:J

    iget-object v2, v0, Lgps;->cSF:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    iget-object v2, v2, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcke;

    iget-object v0, v0, Lgps;->aWp:Ljava/lang/String;

    invoke-interface {v2, v0, p2, p3}, Lcke;->f(Ljava/lang/String;J)V

    .line 134
    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mBgExecutor:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSB:Ljava/lang/Runnable;

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 135
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final a(Ljava/util/Map;Ljava/util/List;)Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 259
    const/4 v0, 0x0

    .line 260
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 261
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 262
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/FutureTask;

    .line 263
    iget-object v6, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->dK:Ljava/lang/Object;

    monitor-enter v6

    .line 265
    :try_start_0
    iget-object v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSD:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/FutureTask;

    .line 266
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    if-eqz v2, :cond_3

    if-eq v2, v0, :cond_3

    .line 269
    invoke-virtual {v0, v4}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    .line 270
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v4

    :goto_1
    move v3, v0

    .line 273
    goto :goto_0

    .line 266
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 274
    :cond_0
    if-eqz v3, :cond_2

    .line 275
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 276
    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 278
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 281
    :cond_2
    return v3

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method public final aJi()V
    .locals 4

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcke;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcke;->U(J)V

    .line 113
    return-void
.end method

.method public final aJj()V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mBgExecutor:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSB:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 119
    return-void
.end method

.method public final aJk()Z
    .locals 18

    .prologue
    .line 157
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->dK:Ljava/lang/Object;

    monitor-enter v9

    .line 159
    const-wide/16 v6, 0x0

    .line 160
    const-wide/16 v4, 0x0

    .line 161
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v10

    .line 164
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSC:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 165
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 166
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgps;

    .line 168
    iget-wide v14, v2, Lgps;->cSH:J

    const-wide/16 v16, 0x0

    cmp-long v8, v14, v16

    if-eqz v8, :cond_1

    iget-wide v14, v2, Lgps;->cSH:J

    cmp-long v8, v14, v10

    if-lez v8, :cond_2

    :cond_1
    iget-wide v14, v2, Lgps;->cSI:J

    const-wide/16 v16, 0x0

    cmp-long v8, v14, v16

    if-eqz v8, :cond_5

    iget-wide v14, v2, Lgps;->cSI:J

    cmp-long v8, v14, v10

    if-gtz v8, :cond_5

    :cond_2
    const/4 v8, 0x1

    :goto_1
    if-eqz v8, :cond_7

    .line 171
    iget-wide v14, v2, Lgps;->cSI:J

    const-wide/16 v16, -0x1

    cmp-long v8, v14, v16

    if-eqz v8, :cond_3

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSD:Ljava/util/Map;

    invoke-interface {v8, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_d

    .line 176
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mFactory:Lgpu;

    iget-wide v14, v2, Lgps;->cSI:J

    const-wide/16 v16, 0x0

    cmp-long v8, v14, v16

    if-eqz v8, :cond_6

    iget-wide v14, v2, Lgps;->cSI:J

    cmp-long v8, v14, v10

    if-gtz v8, :cond_6

    const/4 v8, 0x1

    :goto_2
    invoke-virtual {v13, v3, v8}, Lgpu;->A(Ljava/lang/String;Z)Ljava/util/concurrent/Callable;

    move-result-object v8

    .line 178
    if-eqz v8, :cond_0

    .line 179
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSD:Ljava/util/Map;

    new-instance v14, Lgpr;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v3, v8}, Lgpr;-><init>(Lcom/google/android/velvet/VelvetBackgroundTasksImpl;Ljava/lang/String;Ljava/util/concurrent/Callable;)V

    invoke-interface {v13, v3, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    iget-wide v14, v2, Lgps;->cSG:J

    const-wide/16 v16, 0x0

    cmp-long v3, v14, v16

    if-eqz v3, :cond_4

    iget-wide v14, v2, Lgps;->cSG:J

    add-long/2addr v14, v10

    iput-wide v14, v2, Lgps;->cSH:J

    :cond_4
    const-wide/16 v14, 0x0

    iput-wide v14, v2, Lgps;->cSI:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 226
    :catchall_0
    move-exception v2

    monitor-exit v9

    throw v2

    .line 168
    :cond_5
    const/4 v8, 0x0

    goto :goto_1

    .line 176
    :cond_6
    const/4 v8, 0x0

    goto :goto_2

    .line 191
    :cond_7
    :try_start_1
    iget-wide v14, v2, Lgps;->cSH:J

    invoke-static {v6, v7, v14, v15}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->m(JJ)J

    move-result-wide v6

    .line 192
    iget-wide v2, v2, Lgps;->cSI:J

    invoke-static {v4, v5, v2, v3}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->m(JJ)J

    move-result-wide v2

    move-wide v4, v6

    :goto_3
    move-wide v6, v4

    move-wide v4, v2

    .line 194
    goto/16 :goto_0

    .line 198
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSD:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    .line 199
    :goto_4
    if-eqz v2, :cond_c

    .line 206
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSE:Z

    if-eqz v3, :cond_b

    .line 208
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->dK:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    .line 225
    :cond_9
    :goto_5
    monitor-exit v9

    return v2

    .line 198
    :cond_a
    const/4 v2, 0x0

    goto :goto_4

    .line 211
    :cond_b
    new-instance v3, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mAppContext:Landroid/content/Context;

    const-class v5, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$Service;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mAppContext:Landroid/content/Context;

    invoke-virtual {v4, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_5

    .line 216
    :cond_c
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11, v6, v7}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->l(JJ)J

    move-result-wide v6

    invoke-static {v6, v7, v4, v5}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->m(JJ)J

    move-result-wide v4

    .line 217
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_9

    .line 219
    new-instance v3, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mAppContext:Landroid/content/Context;

    const-class v7, Lcom/google/android/velvet/VelvetBackgroundTasksImpl$Service;

    invoke-direct {v3, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mPendingIntentFactory:Lfda;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-interface {v6, v7, v3, v8}, Lfda;->b(ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mAlarmHelper:Ldjx;

    invoke-virtual {v6, v3}, Ldjx;->cancel(Landroid/app/PendingIntent;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mAlarmHelper:Ldjx;

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v4, v5, v3}, Ldjx;->a(IJLandroid/app/PendingIntent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    :cond_d
    move-wide v2, v4

    move-wide v4, v6

    goto :goto_3
.end method

.method final aJl()V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 343
    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 344
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSE:Z

    if-nez v0, :cond_2

    move v0, v2

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 345
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSE:Z

    .line 346
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 347
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v4

    .line 349
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 354
    :cond_0
    :goto_1
    iget-object v6, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->dK:Ljava/lang/Object;

    monitor-enter v6

    :try_start_1
    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSD:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/FutureTask;

    invoke-interface {v4, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mBgExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_2
    move v0, v3

    .line 344
    goto :goto_0

    .line 346
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 354
    :cond_3
    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 358
    invoke-virtual {p0, v4, v5}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->a(Ljava/util/Map;Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 361
    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->dK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSD:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSD:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->isDone()Z

    move-result v0

    if-eqz v0, :cond_4

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 369
    :goto_3
    invoke-virtual {p0, v4, v5}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->b(Ljava/util/Map;Ljava/util/List;)V

    .line 373
    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 374
    :try_start_4
    invoke-virtual {p0}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->aJk()Z

    move-result v0

    if-nez v0, :cond_8

    .line 375
    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_7

    move v0, v2

    :goto_4
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 378
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSE:Z

    .line 379
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    return-void

    .line 361
    :cond_5
    :try_start_5
    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->dK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_6
    :goto_5
    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto :goto_3

    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_7
    move v0, v3

    .line 375
    goto :goto_4

    .line 381
    :cond_8
    monitor-exit v1

    goto/16 :goto_1

    :catchall_3
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_5
.end method

.method protected final b(Ljava/util/Map;Ljava/util/List;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 315
    iget-object v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->dK:Ljava/lang/Object;

    monitor-enter v2

    .line 316
    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSD:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 317
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 318
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/FutureTask;

    .line 319
    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 333
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 324
    :cond_1
    :try_start_1
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 325
    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSD:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x1

    :goto_2
    invoke-static {v1}, Lifv;->gY(Z)V

    .line 327
    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSD:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/FutureTask;

    invoke-virtual {v1}, Ljava/util/concurrent/FutureTask;->isDone()Z

    move-result v1

    invoke-static {v1}, Lifv;->gY(Z)V

    .line 328
    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSD:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    iget-object v1, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->cSC:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgps;

    iget-object v1, v0, Lgps;->cSF:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    iget-object v1, v1, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcke;

    iget-object v4, v0, Lgps;->aWp:Ljava/lang/String;

    iget-wide v6, v0, Lgps;->cSH:J

    invoke-interface {v1, v4, v6, v7}, Lcke;->e(Ljava/lang/String;J)V

    iget-wide v4, v0, Lgps;->cSI:J

    cmp-long v1, v4, v8

    if-nez v1, :cond_2

    iget-object v1, v0, Lgps;->cSF:Lcom/google/android/velvet/VelvetBackgroundTasksImpl;

    iget-object v1, v1, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcke;

    iget-object v0, v0, Lgps;->aWp:Ljava/lang/String;

    const-wide/16 v4, 0x0

    invoke-interface {v1, v0, v4, v5}, Lcke;->f(Ljava/lang/String;J)V

    goto :goto_1

    .line 325
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 332
    :cond_4
    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 333
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method protected final l(JJ)J
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    .line 454
    cmp-long v0, p3, v6

    if-nez v0, :cond_0

    .line 467
    :goto_0
    return-wide p3

    .line 457
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mGsaConfig:Lchk;

    invoke-virtual {v0}, Lchk;->Ih()J

    move-result-wide v0

    .line 458
    iget-object v2, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mSettings:Lcke;

    invoke-interface {v2}, Lcke;->NS()J

    move-result-wide v2

    sub-long v2, p1, v2

    const-wide/32 v4, 0x5265c00

    div-long/2addr v2, v4

    .line 460
    iget-object v4, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mGsaConfig:Lchk;

    invoke-virtual {v4}, Lchk;->Ik()I

    move-result v4

    int-to-long v4, v4

    sub-long/2addr v2, v4

    .line 462
    cmp-long v4, v2, v6

    if-lez v4, :cond_1

    .line 463
    iget-object v4, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mGsaConfig:Lchk;

    invoke-virtual {v4}, Lchk;->Ii()J

    move-result-wide v4

    .line 464
    iget-object v6, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mGsaConfig:Lchk;

    invoke-virtual {v6}, Lchk;->Ij()J

    move-result-wide v6

    .line 465
    mul-long/2addr v2, v2

    mul-long/2addr v2, v6

    add-long/2addr v0, v2

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 467
    :cond_1
    add-long/2addr v0, p1

    invoke-static {p3, p4, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p3

    goto :goto_0
.end method

.method public final nw(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 128
    const-wide/16 v0, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->s(Ljava/lang/String;J)V

    .line 129
    return-void
.end method

.method public final r(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    add-long/2addr v0, p2

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/velvet/VelvetBackgroundTasksImpl;->s(Ljava/lang/String;J)V

    .line 124
    return-void
.end method
