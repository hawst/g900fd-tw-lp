.class public Lcom/google/android/velvet/ui/FooterView;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Lgxh;


# instance fields
.field private ddC:Landroid/view/ViewStub;

.field private ddD:Landroid/view/View;

.field private ddE:Landroid/view/View;

.field private ddF:Lcom/google/android/velvet/ui/widget/CorpusBar;

.field private ddG:Landroid/view/View;

.field private ddH:Lgxe;

.field private ddI:Z

.field private ddJ:I

.field private ddK:Lepp;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 49
    new-instance v0, Lhdw;

    const-string v1, "Scroll corpus bar on rotation"

    invoke-direct {v0, p0, v1}, Lhdw;-><init>(Lcom/google/android/velvet/ui/FooterView;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddK:Lepp;

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    new-instance v0, Lhdw;

    const-string v1, "Scroll corpus bar on rotation"

    invoke-direct {v0, p0, v1}, Lhdw;-><init>(Lcom/google/android/velvet/ui/FooterView;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddK:Lepp;

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    new-instance v0, Lhdw;

    const-string v1, "Scroll corpus bar on rotation"

    invoke-direct {v0, p0, v1}, Lhdw;-><init>(Lcom/google/android/velvet/ui/FooterView;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddK:Lepp;

    .line 67
    return-void
.end method

.method public static synthetic a(Lcom/google/android/velvet/ui/FooterView;)I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddJ:I

    return v0
.end method

.method public static synthetic a(Lcom/google/android/velvet/ui/FooterView;I)I
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddJ:I

    return v0
.end method

.method public static synthetic b(Lcom/google/android/velvet/ui/FooterView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddE:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final Z(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 158
    const-string v0, "scroll_pos"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddJ:I

    .line 159
    return-void
.end method

.method public final aLq()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddD:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aLr()V
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/FooterView;->aLq()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 123
    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddF:Lcom/google/android/velvet/ui/widget/CorpusBar;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->aOt()V

    .line 124
    return-void
.end method

.method public final aLs()V
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/FooterView;->aLq()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 141
    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddF:Lcom/google/android/velvet/ui/widget/CorpusBar;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/widget/CorpusBar;->aLs()V

    .line 142
    return-void
.end method

.method public final aa(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddE:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddE:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v0

    .line 165
    if-lez v0, :cond_0

    .line 166
    const-string v1, "scroll_pos"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 169
    :cond_0
    return-void
.end method

.method public final b(Lcfy;)V
    .locals 1

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/FooterView;->aLq()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 135
    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddF:Lcom/google/android/velvet/ui/widget/CorpusBar;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/widget/CorpusBar;->b(Lcfy;)V

    .line 136
    return-void
.end method

.method public final c(Lcfy;)V
    .locals 1
    .param p1    # Lcfy;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/FooterView;->aLq()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 129
    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddF:Lcom/google/android/velvet/ui/widget/CorpusBar;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/widget/CorpusBar;->c(Lcfy;)V

    .line 130
    return-void
.end method

.method public final c(Lgxe;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/velvet/ui/FooterView;->ddH:Lgxe;

    .line 72
    return-void
.end method

.method public final fW(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 110
    if-eqz p1, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/FooterView;->aLq()Z

    move-result v0

    if-nez v0, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/FooterView;->aLq()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddC:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddD:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddD:Landroid/view/View;

    const v2, 0x7f110137

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddE:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddD:Landroid/view/View;

    const v2, 0x7f110138

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/widget/CorpusBar;

    iput-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddF:Lcom/google/android/velvet/ui/widget/CorpusBar;

    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddF:Lcom/google/android/velvet/ui/widget/CorpusBar;

    iget-object v2, p0, Lcom/google/android/velvet/ui/FooterView;->ddH:Lgxe;

    invoke-virtual {v0, v2}, Lcom/google/android/velvet/ui/widget/CorpusBar;->c(Lgxe;)V

    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddH:Lgxe;

    invoke-virtual {v0}, Lgxe;->aLp()V

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddG:Landroid/view/View;

    if-eqz p1, :cond_3

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 117
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddI:Z

    iget-object v1, p0, Lcom/google/android/velvet/ui/FooterView;->ddD:Landroid/view/View;

    if-eq p1, v0, :cond_1

    if-eqz v1, :cond_1

    if-eqz p1, :cond_4

    invoke-static {v1}, Lelv;->aA(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    :cond_1
    :goto_2
    iput-boolean p1, p0, Lcom/google/android/velvet/ui/FooterView;->ddI:Z

    .line 118
    return-void

    :cond_2
    move v0, v1

    .line 112
    goto :goto_0

    .line 116
    :cond_3
    const/16 v1, 0x8

    goto :goto_1

    .line 117
    :cond_4
    const/4 v0, 0x4

    invoke-static {v1, v0}, Lelv;->p(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    goto :goto_2
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 84
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 85
    const v0, 0x7f1101e5

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/FooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddC:Landroid/view/ViewStub;

    .line 87
    const v0, 0x7f1101e4

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/FooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddG:Landroid/view/View;

    .line 88
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 76
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 77
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/FooterView;->aLq()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddJ:I

    if-lez v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/google/android/velvet/ui/FooterView;->ddK:Lepp;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/FooterView;->post(Ljava/lang/Runnable;)Z

    .line 80
    :cond_0
    return-void
.end method
