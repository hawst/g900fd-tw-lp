.class public Lcom/google/android/velvet/ui/VelvetAssistantActivity;
.super Lcom/google/android/velvet/ui/VelvetActivity;
.source "PG"


# instance fields
.field private dfh:Landroid/view/View;

.field private dfi:Z

.field private dfj:Ldqq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 36
    invoke-direct {p0, v0}, Lcom/google/android/velvet/ui/VelvetActivity;-><init>(I)V

    .line 32
    iput-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->dfi:Z

    .line 37
    return-void
.end method

.method private aOk()V
    .locals 3

    .prologue
    .line 133
    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->dfj:Ldqq;

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->dfi:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Ldqq;->gi(I)V

    .line 134
    const v0, 0x7f040156

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 135
    if-eqz v1, :cond_0

    .line 136
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->dfi:Z

    if-eqz v0, :cond_2

    const v0, 0x106000d

    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 139
    :cond_0
    return-void

    .line 133
    :cond_1
    const/16 v0, 0xff

    goto :goto_0

    .line 136
    :cond_2
    const v0, 0x7f0b0074

    goto :goto_1
.end method


# virtual methods
.method protected final aOf()J
    .locals 2

    .prologue
    .line 41
    const-wide/32 v0, 0x8000000

    return-wide v0
.end method

.method protected final aOg()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method

.method public final bE(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/VelvetActivity;->bE(Landroid/view/View;)V

    .line 98
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->dfi:Z

    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 101
    :cond_0
    return-void
.end method

.method public finish()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 106
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->dfi:Z

    if-eqz v0, :cond_0

    sget v0, Lesp;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->finishAndRemoveTask()V

    .line 111
    invoke-virtual {p0, v2, v2}, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->overridePendingTransition(II)V

    .line 116
    :goto_0
    return-void

    .line 114
    :cond_0
    invoke-super {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->finish()V

    goto :goto_0
.end method

.method public final gg(Z)V
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->dfi:Z

    if-eq v0, p1, :cond_1

    const/4 v0, 0x1

    .line 79
    :goto_0
    iput-boolean p1, p0, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->dfi:Z

    .line 80
    if-eqz v0, :cond_0

    .line 81
    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->aOk()V

    .line 83
    :cond_0
    return-void

    .line 78
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/VelvetActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const v0, 0x7f11048b

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->dfh:Landroid/view/View;

    .line 55
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 57
    new-instance v1, Ldqq;

    const v2, 0x7f0b0075

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const v3, 0x7f0b0074

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    const v4, 0x7f0d000b

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-direct {v1, v2, v3, v0}, Ldqq;-><init>(III)V

    iput-object v1, p0, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->dfj:Ldqq;

    .line 61
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->dfj:Ldqq;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Ldqq;->setAlpha(I)V

    .line 63
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->dfh:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->dfj:Ldqq;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 64
    if-eqz p1, :cond_0

    const-string v0, "VelvetAssistantActivity.Transparent"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->dfi:Z

    .line 66
    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->aOk()V

    .line 67
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 88
    invoke-super {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->onDestroy()V

    .line 89
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 121
    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/VelvetActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 122
    const-string v0, "VelvetAssistantActivity.Transparent"

    iget-boolean v1, p0, Lcom/google/android/velvet/ui/VelvetAssistantActivity;->dfi:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 123
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 72
    invoke-super {p0}, Lcom/google/android/velvet/ui/VelvetActivity;->onStart()V

    .line 73
    return-void
.end method
