.class public Lcom/google/android/velvet/tg/SetupWizardOptInActivity;
.super Lhcd;
.source "PG"

# interfaces
.implements Lefh;
.implements Lefi;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field private atJ:Landroid/widget/ProgressBar;

.field private dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

.field private dcE:Lcom/google/android/setupwizard/util/BottomScrollView;

.field private dcG:Z

.field private dcN:Landroid/widget/RadioButton;

.field private dcO:Landroid/widget/RadioButton;

.field private dcP:Z

.field private mAccount:Landroid/accounts/Account;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 66
    const-string v0, "SETUP_WIZARD"

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lhcd;-><init>(Ljava/lang/String;I)V

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcG:Z

    .line 67
    return-void
.end method

.method public static a(Landroid/app/Activity;Landroid/content/Intent;)Z
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 113
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 116
    const-string v0, "theme"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 117
    const-string v1, "material_light"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    const v0, 0x7f090087

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setTheme(I)V

    .line 119
    const/4 v0, 0x0

    .line 122
    :goto_0
    return v0

    .line 121
    :cond_0
    const v0, 0x7f090086

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setTheme(I)V

    .line 122
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/setupwizard/SetupWizardNavBar;)V
    .locals 2

    .prologue
    .line 175
    iput-object p1, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    .line 176
    iget-boolean v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcP:Z

    if-eqz v0, :cond_0

    .line 177
    invoke-virtual {p1}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aom()Landroid/widget/Button;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 179
    :cond_0
    return-void
.end method

.method protected final aAW()V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->atJ:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 143
    return-void
.end method

.method protected final aNB()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->K(Landroid/accounts/Account;)V

    .line 138
    return-void
.end method

.method protected final aNx()I
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x2

    return v0
.end method

.method public final aoo()V
    .locals 1

    .prologue
    .line 183
    iget-boolean v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcP:Z

    if-eqz v0, :cond_0

    .line 188
    :goto_0
    return-void

    .line 186
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->setResult(I)V

    .line 187
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->finish()V

    goto :goto_0
.end method

.method public final aop()V
    .locals 2

    .prologue
    .line 192
    iget-boolean v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcG:Z

    if-eqz v0, :cond_1

    .line 193
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcE:Lcom/google/android/setupwizard/util/BottomScrollView;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/BottomScrollView;->pageScroll(I)Z

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 195
    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aon()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 196
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcN:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 197
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->aNA()V

    goto :goto_0

    .line 198
    :cond_2
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcO:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->aNC()V

    goto :goto_0
.end method

.method public final aor()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 206
    iget-boolean v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcG:Z

    if-nez v0, :cond_0

    .line 215
    :goto_0
    return-void

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aon()Landroid/widget/Button;

    move-result-object v0

    .line 211
    const v1, 0x7f0202b8

    invoke-virtual {v0, v2, v2, v1, v2}, Landroid/widget/Button;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    .line 213
    const v1, 0x7f0a02f5

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 214
    iput-boolean v2, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcG:Z

    goto :goto_0
.end method

.method public final aos()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 219
    iget-boolean v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcG:Z

    if-eqz v0, :cond_0

    .line 228
    :goto_0
    return-void

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aon()Landroid/widget/Button;

    move-result-object v0

    .line 224
    const v1, 0x7f0202c2

    invoke-virtual {v0, v2, v2, v1, v2}, Landroid/widget/Button;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    .line 226
    const v1, 0x7f0a0235

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 227
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcG:Z

    goto :goto_0
.end method

.method protected final gi(Z)V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aon()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 233
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcN:Landroid/widget/RadioButton;

    invoke-virtual {v0, p1}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 234
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcO:Landroid/widget/RadioButton;

    invoke-virtual {v0, p1}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 235
    return-void
.end method

.method protected final kM(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 147
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    :goto_0
    return-void

    .line 152
    :cond_0
    iget v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dct:I

    if-ne p1, v0, :cond_1

    .line 155
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->atJ:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 156
    invoke-virtual {p0, v2}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->gi(Z)V

    .line 157
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aon()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 158
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0147

    invoke-virtual {p0, v1}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 162
    :cond_1
    if-nez p1, :cond_2

    const/16 v0, 0xb

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->setResult(I)V

    .line 164
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->finish()V

    goto :goto_0

    .line 162
    :cond_2
    const/16 v0, 0xc

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcP:Z

    if-eqz v0, :cond_0

    .line 133
    :goto_0
    return-void

    .line 131
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->setResult(I)V

    .line 132
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->finish()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    .line 73
    invoke-super {p0, p1}, Lhcd;->onCreate(Landroid/os/Bundle;)V

    .line 75
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "optin_account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->mAccount:Landroid/accounts/Account;

    .line 76
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->mAccount:Landroid/accounts/Account;

    if-nez v0, :cond_0

    .line 77
    const-string v0, "SetupWizardOptInActivity"

    const-string v1, "Missing account extra"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    iget v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcu:I

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->kM(I)V

    .line 103
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "noBack"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcP:Z

    .line 85
    const v0, 0x7f04016c

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->setContentView(I)V

    .line 86
    const v0, 0x7f1101a9

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 87
    const v1, 0x7f0a02d2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 89
    const v0, 0x7f1103f9

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 90
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04016a

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 92
    const v0, 0x7f11025f

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->atJ:Landroid/widget/ProgressBar;

    .line 93
    const v0, 0x7f1101d8

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcN:Landroid/widget/RadioButton;

    .line 94
    const v0, 0x7f1101d7

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcO:Landroid/widget/RadioButton;

    .line 95
    const v0, 0x7f1103f7

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupwizard/util/BottomScrollView;

    iput-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcE:Lcom/google/android/setupwizard/util/BottomScrollView;

    .line 96
    iget-object v0, p0, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->dcE:Lcom/google/android/setupwizard/util/BottomScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/setupwizard/util/BottomScrollView;->a(Lefi;)V

    .line 98
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0202be

    const v2, 0x7f0202bf

    invoke-static {v0, v1, v2}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->d(Landroid/view/View;II)V

    .line 102
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/SetupWizardOptInActivity;->aNy()V

    goto :goto_0
.end method
