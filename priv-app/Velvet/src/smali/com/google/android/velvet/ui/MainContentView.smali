.class public Lcom/google/android/velvet/ui/MainContentView;
.super Lcom/google/android/shared/ui/ChildPaddingLayout;
.source "PG"

# interfaces
.implements Lejm;
.implements Leti;


# instance fields
.field private aMD:Leqo;

.field private final cxc:Lekg;

.field public cxe:Z

.field private final dej:Ljava/lang/Runnable;

.field private final dek:Ljava/lang/Runnable;

.field private final del:Landroid/animation/LayoutTransition$TransitionListener;

.field private final dem:Landroid/view/View$OnLayoutChangeListener;

.field private final den:Landroid/view/View$OnAttachStateChangeListener;

.field private final deo:Ljava/util/List;

.field public dep:Lgxi;

.field private deq:Landroid/animation/LayoutTransition;

.field private der:Lcom/google/android/shared/ui/CoScrollContainer;

.field public des:Z

.field private det:Z

.field public deu:Z

.field public dev:I

.field public dew:Z

.field private dex:Lcom/google/android/velvet/ui/VelvetActivity;

.field private mCardsView:Lcom/google/android/shared/ui/SuggestionGridLayout;

.field private mClock:Lemp;

.field private mProgressBar:Lcom/google/android/sidekick/shared/ui/NowProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/velvet/ui/MainContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 233
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 236
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/velvet/ui/MainContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 237
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 240
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/shared/ui/ChildPaddingLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 93
    new-instance v0, Lhep;

    invoke-direct {v0, p0}, Lhep;-><init>(Lcom/google/android/velvet/ui/MainContentView;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->cxc:Lekg;

    .line 132
    new-instance v0, Lheq;

    const-string v1, "Scroll view"

    invoke-direct {v0, p0, v1}, Lheq;-><init>(Lcom/google/android/velvet/ui/MainContentView;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->dej:Ljava/lang/Runnable;

    .line 142
    new-instance v0, Lher;

    const-string v1, "Commit transactions"

    invoke-direct {v0, p0, v1}, Lher;-><init>(Lcom/google/android/velvet/ui/MainContentView;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->dek:Ljava/lang/Runnable;

    .line 151
    new-instance v0, Lhes;

    invoke-direct {v0, p0}, Lhes;-><init>(Lcom/google/android/velvet/ui/MainContentView;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->del:Landroid/animation/LayoutTransition$TransitionListener;

    .line 182
    new-instance v0, Lhet;

    invoke-direct {v0, p0}, Lhet;-><init>(Lcom/google/android/velvet/ui/MainContentView;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->dem:Landroid/view/View$OnLayoutChangeListener;

    .line 191
    new-instance v0, Lheu;

    invoke-direct {v0, p0}, Lheu;-><init>(Lcom/google/android/velvet/ui/MainContentView;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->den:Landroid/view/View$OnAttachStateChangeListener;

    .line 241
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->deo:Ljava/util/List;

    .line 242
    return-void
.end method

.method private gl(Z)Z
    .locals 1

    .prologue
    .line 431
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/MainContentView;->deu:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentView;->aNY()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->der:Lcom/google/android/shared/ui/CoScrollContainer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->der:Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/CoScrollContainer;->atf()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->der:Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/CoScrollContainer;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected DC()Lemp;
    .locals 1

    .prologue
    .line 283
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    iget-object v0, v0, Lgql;->mClock:Lemp;

    return-object v0
.end method

.method final a(Lcom/google/android/velvet/ui/VelvetActivity;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/google/android/velvet/ui/MainContentView;->dex:Lcom/google/android/velvet/ui/VelvetActivity;

    .line 248
    return-void
.end method

.method public a(Lesj;)V
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->deo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 418
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentView;->aNW()V

    .line 419
    return-void
.end method

.method public final a(Letj;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 567
    const-string v0, "MainContentView State"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 568
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->dep:Lgxi;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 569
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v3

    .line 570
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PendingUiTransactions ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentView;->deo:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Letj;->lt(Ljava/lang/String;)V

    move v1, v2

    .line 573
    :goto_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->deo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 576
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v4

    .line 577
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->deo:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lesj;

    .line 578
    instance-of v5, v0, Leti;

    if-eqz v5, :cond_0

    .line 579
    check-cast v0, Leti;

    invoke-virtual {v4, v0}, Letn;->b(Leti;)V

    .line 573
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 581
    :cond_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x1

    invoke-virtual {v4, v0, v5}, Letn;->d(Ljava/lang/CharSequence;Z)V

    goto :goto_1

    .line 585
    :cond_1
    const-string v0, "mLayoutTransition"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentView;->mCardsView:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 587
    return-void
.end method

.method public final aAU()Z
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->dep:Lgxi;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aKQ()F
    .locals 1

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lejd;

    .line 318
    invoke-virtual {v0}, Lejd;->atr()F

    move-result v0

    return v0
.end method

.method public final aL(II)V
    .locals 3

    .prologue
    .line 543
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->mCardsView:Lcom/google/android/shared/ui/SuggestionGridLayout;

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->mCardsView:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentView;->mCardsView:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/velvet/ui/MainContentView;->mCardsView:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v2}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getPaddingRight()I

    move-result v2

    invoke-virtual {v0, v1, p1, v2, p2}, Lcom/google/android/shared/ui/SuggestionGridLayout;->setPadding(IIII)V

    .line 547
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->mProgressBar:Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    if-eqz v0, :cond_1

    .line 548
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->mProgressBar:Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 550
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d00ad

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int v1, p1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 552
    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentView;->mProgressBar:Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    invoke-virtual {v1, v0}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 554
    :cond_1
    return-void
.end method

.method public aLi()Z
    .locals 2

    .prologue
    .line 379
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentView;->getId()I

    move-result v0

    const v1, 0x7f11025d

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aLu()Lcom/google/android/sidekick/shared/ui/NowProgressBar;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->mProgressBar:Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    return-object v0
.end method

.method protected aNU()Leqo;
    .locals 1

    .prologue
    .line 287
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->aus()Leqo;

    move-result-object v0

    return-object v0
.end method

.method final aNV()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 437
    invoke-static {}, Lenu;->auR()V

    .line 438
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->mCardsView:Lcom/google/android/shared/ui/SuggestionGridLayout;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/MainContentView;->deu:Z

    if-nez v0, :cond_1

    .line 455
    :cond_0
    return-void

    .line 441
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/MainContentView;->des:Z

    if-eqz v0, :cond_2

    .line 442
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->aMD:Leqo;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentView;->dek:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 443
    iput-boolean v2, p0, Lcom/google/android/velvet/ui/MainContentView;->des:Z

    .line 446
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->deo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->deo:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lesj;

    move v1, v2

    .line 450
    :goto_1
    if-nez v1, :cond_3

    .line 451
    invoke-virtual {v0}, Lesj;->avD()Z

    move-result v1

    goto :goto_1

    .line 453
    :cond_3
    invoke-virtual {v0, p0}, Lesj;->a(Lejm;)V

    goto :goto_0
.end method

.method public final aNW()V
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 459
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v0

    const-wide/16 v4, 0x5

    add-long v6, v0, v4

    .line 460
    invoke-static {}, Lenu;->auR()V

    .line 461
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->mCardsView:Lcom/google/android/shared/ui/SuggestionGridLayout;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/MainContentView;->deu:Z

    if-nez v0, :cond_1

    .line 508
    :cond_0
    :goto_0
    return-void

    .line 464
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/MainContentView;->des:Z

    if-eqz v0, :cond_2

    .line 465
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->aMD:Leqo;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentView;->dek:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 466
    iput-boolean v3, p0, Lcom/google/android/velvet/ui/MainContentView;->des:Z

    .line 468
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/MainContentView;->det:Z

    if-nez v0, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/velvet/ui/MainContentView;->gl(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 469
    iput-boolean v2, p0, Lcom/google/android/velvet/ui/MainContentView;->det:Z

    move v1, v2

    .line 471
    :goto_1
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->deo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 472
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->deo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lesj;

    move v4, v3

    .line 475
    :cond_3
    if-nez v4, :cond_4

    .line 476
    invoke-virtual {v0}, Lesj;->avD()Z

    move-result v4

    .line 477
    iget-object v5, p0, Lcom/google/android/velvet/ui/MainContentView;->mClock:Lemp;

    invoke-interface {v5}, Lemp;->uptimeMillis()J

    move-result-wide v8

    cmp-long v5, v8, v6

    if-ltz v5, :cond_3

    move v1, v3

    .line 480
    :cond_4
    if-eqz v1, :cond_9

    .line 484
    invoke-virtual {v0, p0}, Lesj;->a(Lejm;)V

    .line 485
    const/4 v0, 0x0

    .line 488
    invoke-direct {p0, v3}, Lcom/google/android/velvet/ui/MainContentView;->gl(Z)Z

    move-result v4

    if-nez v4, :cond_6

    .line 489
    iget-object v4, p0, Lcom/google/android/velvet/ui/MainContentView;->mClock:Lemp;

    invoke-interface {v4}, Lemp;->uptimeMillis()J

    move-result-wide v4

    cmp-long v4, v4, v6

    if-ltz v4, :cond_8

    move-object v1, v0

    move v0, v3

    .line 494
    :goto_2
    if-nez v0, :cond_7

    .line 495
    if-eqz v1, :cond_5

    .line 497
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->deo:Ljava/util/List;

    invoke-interface {v0, v3, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 499
    :cond_5
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->deo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 500
    iput-boolean v2, p0, Lcom/google/android/velvet/ui/MainContentView;->des:Z

    .line 501
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->aMD:Leqo;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentView;->dek:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 506
    :cond_6
    iput-boolean v3, p0, Lcom/google/android/velvet/ui/MainContentView;->det:Z

    goto :goto_0

    :cond_7
    move v1, v0

    .line 505
    goto :goto_1

    :cond_8
    move-object v10, v0

    move v0, v1

    move-object v1, v10

    goto :goto_2

    :cond_9
    move-object v10, v0

    move v0, v1

    move-object v1, v10

    goto :goto_2
.end method

.method public final aNX()Z
    .locals 1

    .prologue
    .line 521
    iget v0, p0, Lcom/google/android/velvet/ui/MainContentView;->dev:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aNY()Z
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->mCardsView:Lcom/google/android/shared/ui/SuggestionGridLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->mCardsView:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    .line 527
    :goto_0
    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    return v0

    .line 526
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 527
    :cond_1
    invoke-virtual {v0}, Landroid/animation/LayoutTransition;->isRunning()Z

    move-result v0

    goto :goto_1
.end method

.method protected final aNZ()V
    .locals 1

    .prologue
    .line 532
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/MainContentView;->gk(Z)V

    .line 533
    return-void
.end method

.method public final aOa()V
    .locals 1

    .prologue
    .line 537
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentView;->aNY()Z

    move-result v0

    if-nez v0, :cond_0

    .line 538
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/MainContentView;->gk(Z)V

    .line 540
    :cond_0
    return-void
.end method

.method public atB()Lcom/google/android/shared/ui/SuggestionGridLayout;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->mCardsView:Lcom/google/android/shared/ui/SuggestionGridLayout;

    return-object v0
.end method

.method public atC()Lcom/google/android/shared/ui/CoScrollContainer;
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->der:Lcom/google/android/shared/ui/CoScrollContainer;

    if-nez v0, :cond_0

    .line 351
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/ui/CoScrollContainer;

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->der:Lcom/google/android/shared/ui/CoScrollContainer;

    .line 352
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->der:Lcom/google/android/shared/ui/CoScrollContainer;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentView;->cxc:Lekg;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/CoScrollContainer;->a(Lekg;)V

    .line 353
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->der:Lcom/google/android/shared/ui/CoScrollContainer;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentView;->dem:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/CoScrollContainer;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->der:Lcom/google/android/shared/ui/CoScrollContainer;

    return-object v0
.end method

.method public final b(Lgxi;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lcom/google/android/velvet/ui/MainContentView;->dep:Lgxi;

    .line 252
    return-void
.end method

.method public final bI(Landroid/view/View;)F
    .locals 3

    .prologue
    .line 401
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentView;->atC()Lcom/google/android/shared/ui/CoScrollContainer;

    move-result-object v0

    .line 402
    invoke-interface {v0, p1}, Lekf;->aH(Landroid/view/View;)I

    move-result v1

    .line 403
    if-gez v1, :cond_0

    .line 404
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 406
    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, Lekf;->getScrollY()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-interface {v0}, Lekf;->ate()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    goto :goto_0
.end method

.method public final cZ(I)V
    .locals 2

    .prologue
    .line 412
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 413
    return-void
.end method

.method public final eU(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 330
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->dex:Lcom/google/android/velvet/ui/VelvetActivity;

    if-eqz v0, :cond_0

    .line 331
    if-eqz p1, :cond_1

    .line 332
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->dex:Lcom/google/android/velvet/ui/VelvetActivity;

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/velvet/ui/VelvetActivity;->e(IZZ)V

    .line 341
    :cond_0
    :goto_0
    return-void

    .line 334
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentView;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->dex:Lcom/google/android/velvet/ui/VelvetActivity;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentView;->dep:Lgxi;

    invoke-virtual {v1}, Lgxi;->aLH()Lgyz;

    move-result-object v1

    invoke-virtual {v1}, Lgyz;->aMn()I

    move-result v1

    invoke-virtual {v0, v1, v3, v3}, Lcom/google/android/velvet/ui/VelvetActivity;->e(IZZ)V

    goto :goto_0
.end method

.method public final eV(Z)V
    .locals 3

    .prologue
    .line 392
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentView;->atC()Lcom/google/android/shared/ui/CoScrollContainer;

    move-result-object v1

    .line 393
    if-eqz v1, :cond_0

    .line 394
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0077

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 398
    :cond_0
    return-void

    .line 394
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final gj(Z)V
    .locals 2

    .prologue
    .line 360
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentView;->atC()Lcom/google/android/shared/ui/CoScrollContainer;

    move-result-object v0

    .line 361
    if-eqz v0, :cond_0

    .line 362
    if-eqz p1, :cond_1

    .line 363
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    .line 364
    invoke-static {v0}, Lelv;->aA(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    .line 372
    :cond_0
    :goto_0
    return-void

    .line 367
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 368
    invoke-static {v0}, Lelv;->ba(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public final gk(Z)V
    .locals 2

    .prologue
    .line 423
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/MainContentView;->des:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/MainContentView;->det:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/velvet/ui/MainContentView;->gl(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 425
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/MainContentView;->des:Z

    .line 426
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->aMD:Leqo;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentView;->dek:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 428
    :cond_0
    return-void
.end method

.method public final gm(Z)V
    .locals 4

    .prologue
    .line 558
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/MainContentView;->dew:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/MainContentView;->cxe:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    .line 559
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/MainContentView;->dew:Z

    .line 560
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->aMD:Leqo;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentView;->dej:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-interface {v0, v1, v2, v3}, Leqo;->a(Ljava/lang/Runnable;J)V

    .line 562
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/velvet/ui/MainContentView;->cxe:Z

    .line 563
    return-void
.end method

.method public final hQ(I)V
    .locals 2

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lejd;

    .line 303
    if-lez p1, :cond_0

    .line 304
    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1}, Lejd;->aN(II)V

    .line 308
    :goto_0
    return-void

    .line 306
    :cond_0
    invoke-virtual {v0}, Lejd;->atp()V

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 261
    invoke-super {p0}, Lcom/google/android/shared/ui/ChildPaddingLayout;->onFinishInflate()V

    .line 262
    const v0, 0x7f11025e

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/MainContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/ui/SuggestionGridLayout;

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->mCardsView:Lcom/google/android/shared/ui/SuggestionGridLayout;

    .line 263
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->mCardsView:Lcom/google/android/shared/ui/SuggestionGridLayout;

    new-instance v1, Lhev;

    invoke-direct {v1, p0}, Lhev;-><init>(Lcom/google/android/velvet/ui/MainContentView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->a(Lekq;)V

    .line 271
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->mCardsView:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->deq:Landroid/animation/LayoutTransition;

    .line 272
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->deq:Landroid/animation/LayoutTransition;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->setAnimateParentHierarchy(Z)V

    .line 273
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->deq:Landroid/animation/LayoutTransition;

    iget-object v1, p0, Lcom/google/android/velvet/ui/MainContentView;->del:Landroid/animation/LayoutTransition$TransitionListener;

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 274
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->den:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/MainContentView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 275
    const v0, 0x7f11025f

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/MainContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->mProgressBar:Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    .line 277
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentView;->DC()Lemp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->mClock:Lemp;

    .line 278
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentView;->aNU()Leqo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->aMD:Leqo;

    .line 279
    iget-object v0, p0, Lcom/google/android/velvet/ui/MainContentView;->deo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 280
    return-void
.end method

.method public final wj()Lekf;
    .locals 1

    .prologue
    .line 345
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/MainContentView;->atC()Lcom/google/android/shared/ui/CoScrollContainer;

    move-result-object v0

    return-object v0
.end method
