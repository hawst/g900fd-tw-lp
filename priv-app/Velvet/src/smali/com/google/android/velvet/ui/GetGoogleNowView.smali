.class public Lcom/google/android/velvet/ui/GetGoogleNowView;
.super Landroid/widget/LinearLayout;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Lcin;)V
    .locals 4

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/GetGoogleNowView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 82
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 83
    const v2, 0x10008000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 84
    const-string v2, "source"

    const-string v3, "GET_NOW_PROMO"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    invoke-interface {p1}, Lcin;->KA()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 87
    const-string v2, "skip_to_end"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 90
    :cond_0
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 91
    return-void
.end method

.method public final b(Lcin;)V
    .locals 1

    .prologue
    .line 95
    invoke-interface {p1}, Lcin;->KD()V

    .line 96
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/GetGoogleNowView;->setVisibility(I)V

    .line 97
    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    .line 41
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 43
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lcfo;->DF()Lcin;

    move-result-object v1

    .line 46
    invoke-virtual {v0}, Lcfo;->DR()Lcpx;

    move-result-object v2

    .line 48
    const-string v0, "GET_GOOGLE_NOW_BUTTON"

    invoke-virtual {v2, v0}, Lcpx;->io(Ljava/lang/String;)V

    .line 50
    invoke-interface {v1}, Lcin;->KC()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/GetGoogleNowView;->setVisibility(I)V

    .line 77
    :goto_0
    return-void

    .line 54
    :cond_0
    const v0, 0x7f1101f0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/GetGoogleNowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 55
    new-instance v3, Lhdx;

    invoke-direct {v3, p0, v2, v1}, Lhdx;-><init>(Lcom/google/android/velvet/ui/GetGoogleNowView;Lcpx;Lcin;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    const v0, 0x7f1101f1

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/GetGoogleNowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 67
    new-instance v3, Lhdy;

    invoke-direct {v3, p0, v2, v1}, Lhdy;-><init>(Lcom/google/android/velvet/ui/GetGoogleNowView;Lcpx;Lcin;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
