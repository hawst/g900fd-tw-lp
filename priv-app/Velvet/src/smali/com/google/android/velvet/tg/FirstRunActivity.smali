.class public Lcom/google/android/velvet/tg/FirstRunActivity;
.super Lhcd;
.source "PG"

# interfaces
.implements Lefh;
.implements Lefi;


# instance fields
.field private atJ:Landroid/widget/ProgressBar;

.field private dcA:Z

.field public dcB:Ljava/lang/String;

.field private dcC:Z

.field private dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

.field private dcE:Lcom/google/android/setupwizard/util/BottomScrollView;

.field private dcF:Z

.field private dcG:Z

.field private dcH:Z

.field public dcI:Ljava/util/List;

.field private dcz:Z

.field public mAccountConfigurationHelper:Lhcb;

.field private mAccountManager:Landroid/accounts/AccountManager;

.field private mBackgroundTasks:Lgpp;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 122
    const-string v0, "FIRST_RUN"

    invoke-direct {p0, v0, v2}, Lhcd;-><init>(Ljava/lang/String;I)V

    .line 85
    iput-boolean v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcC:Z

    .line 90
    iput-boolean v2, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcF:Z

    .line 92
    iput-boolean v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcG:Z

    .line 123
    return-void
.end method

.method public static synthetic a(Lcom/google/android/velvet/tg/FirstRunActivity;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 61
    if-eqz p1, :cond_0

    const-string v0, "authAccount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->gi(Z)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNF()V

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->refresh()V

    new-instance v0, Lhcp;

    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mLoginHelper:Lcrh;

    invoke-virtual {v1}, Lcrh;->Ss()[Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mTaskRunner:Lerk;

    invoke-direct {v0, p0, v1, v2}, Lhcp;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;[Landroid/accounts/Account;Lerk;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lhcp;->a([Ljava/lang/Object;)Lenp;

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/velvet/tg/FirstRunActivity;)Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcC:Z

    return v0
.end method

.method private aNE()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 202
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "skip_to_end"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcz:Z

    .line 203
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "skip_launch_velvet"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcA:Z

    .line 205
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "gel_onboard_mode"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcH:Z

    .line 207
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcB:Ljava/lang/String;

    .line 208
    return-void
.end method

.method private aNF()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 230
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcF:Z

    .line 232
    const v0, 0x7f04016b

    invoke-direct {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->kN(I)V

    .line 234
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aom()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 236
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aon()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcE:Lcom/google/android/setupwizard/util/BottomScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/BottomScrollView;->a(Lefi;)V

    .line 240
    return-void
.end method

.method private aNG()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 246
    iput-boolean v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcF:Z

    .line 248
    const-string v0, "OPTIN"

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->nM(Ljava/lang/String;)V

    .line 249
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->kL(I)V

    .line 251
    const v0, 0x7f04016a

    invoke-direct {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->kN(I)V

    .line 253
    const v0, 0x7f11025f

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->atJ:Landroid/widget/ProgressBar;

    .line 255
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aom()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 257
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aon()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 260
    :cond_0
    const v0, 0x7f1103f5

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 261
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 263
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNy()V

    .line 265
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcE:Lcom/google/android/setupwizard/util/BottomScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/setupwizard/util/BottomScrollView;->a(Lefi;)V

    .line 266
    return-void
.end method

.method public static synthetic b(Lcom/google/android/velvet/tg/FirstRunActivity;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNG()V

    return-void
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 111
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 112
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 113
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    const-string v1, "skip_to_end"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 115
    const-string v1, "skip_launch_velvet"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 116
    const-string v1, "source"

    const-string v2, "SELECT_ACCOUNT"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 118
    return-object v0
.end method

.method private kN(I)V
    .locals 2

    .prologue
    .line 193
    const v0, 0x7f1103f9

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 194
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 195
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 196
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/setupwizard/SetupWizardNavBar;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x0

    .line 628
    iput-object p1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    .line 631
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/SetupWizardNavBar;->eP(Z)V

    .line 634
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aon()Landroid/widget/Button;

    move-result-object v0

    invoke-static {v0, v1, v1, v1, v1}, Leot;->a(Landroid/widget/TextView;IIII)V

    .line 636
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aom()Landroid/widget/Button;

    move-result-object v0

    invoke-static {v0, v1, v1, v1, v1}, Leot;->a(Landroid/widget/TextView;IIII)V

    .line 638
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aom()Landroid/widget/Button;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcH:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0a02f1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 640
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aon()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNz()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 641
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aon()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00d8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 644
    iget-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcF:Z

    if-eqz v0, :cond_0

    .line 646
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aom()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 647
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aon()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 649
    :cond_0
    return-void

    .line 638
    :cond_1
    const v0, 0x7f0a02f0

    goto :goto_0
.end method

.method protected final a(Lgpp;Lhcb;Landroid/accounts/AccountManager;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mBackgroundTasks:Lgpp;

    .line 142
    iput-object p2, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAccountConfigurationHelper:Lhcb;

    .line 143
    iput-object p3, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAccountManager:Landroid/accounts/AccountManager;

    .line 144
    return-void
.end method

.method protected final aAW()V
    .locals 2

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->atJ:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 363
    return-void
.end method

.method protected final aNB()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 308
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 311
    iput-boolean v5, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcC:Z

    .line 312
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v1, "com.google"

    new-instance v6, Lhcm;

    invoke-direct {v6, p0}, Lhcm;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;)V

    move-object v3, v2

    move-object v4, v2

    move-object v5, p0

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 358
    :goto_0
    return-void

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcB:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 331
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcB:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->nO(Ljava/lang/String;)Lhcc;

    move-result-object v0

    .line 332
    if-eqz v0, :cond_1

    .line 333
    iget-object v0, v0, Lhcc;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->K(Landroid/accounts/Account;)V

    goto :goto_0

    .line 340
    :cond_1
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhcc;

    iget-boolean v4, v0, Lhcc;->aPI:Z

    if-eqz v4, :cond_2

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 341
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_4

    .line 342
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhcc;

    iget-object v0, v0, Lhcc;->mAccount:Landroid/accounts/Account;

    .line 343
    iget-object v3, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mLoginHelper:Lcrh;

    invoke-virtual {v3}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 344
    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->K(Landroid/accounts/Account;)V

    goto :goto_0

    .line 351
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [Landroid/accounts/Account;

    .line 352
    :goto_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 353
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhcc;

    iget-object v0, v0, Lhcc;->mAccount:Landroid/accounts/Account;

    aput-object v0, v3, v1

    .line 352
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 355
    :cond_5
    invoke-static {v3}, Lhcn;->c([Landroid/accounts/Account;)Lhcn;

    move-result-object v0

    .line 357
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "selectAccount"

    invoke-virtual {v0, v1, v2}, Lhcn;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final aNH()V
    .locals 2

    .prologue
    .line 419
    const v0, 0x7f0a0147

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 420
    return-void
.end method

.method public final aNI()Z
    .locals 2

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhcc;

    .line 446
    iget-boolean v0, v0, Lhcc;->aPI:Z

    if-eqz v0, :cond_0

    .line 447
    const/4 v0, 0x1

    .line 450
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final aNx()I
    .locals 2

    .prologue
    .line 609
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 610
    const-string v1, "source"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 611
    const-string v1, "SETTINGS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 612
    const/4 v0, 0x3

    .line 623
    :goto_0
    return v0

    .line 613
    :cond_0
    const-string v1, "SELECT_ACCOUNT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 614
    const/4 v0, 0x4

    goto :goto_0

    .line 615
    :cond_1
    const-string v1, "GET_NOW_PROMO"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 616
    const/4 v0, 0x5

    goto :goto_0

    .line 617
    :cond_2
    const-string v1, "ADD_REMINDER"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 618
    const/4 v0, 0x6

    goto :goto_0

    .line 619
    :cond_3
    const-string v1, "COMMUNICATION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 620
    const/4 v0, 0x7

    goto :goto_0

    .line 623
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected final aNz()Ljava/lang/String;
    .locals 2

    .prologue
    .line 270
    iget-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcH:Z

    if-eqz v0, :cond_0

    .line 271
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a02ec

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 273
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lhcd;->aNz()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final aoo()V
    .locals 0

    .prologue
    .line 654
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNC()V

    .line 655
    return-void
.end method

.method public final aop()V
    .locals 2

    .prologue
    .line 660
    iget-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcG:Z

    if-eqz v0, :cond_0

    .line 661
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcE:Lcom/google/android/setupwizard/util/BottomScrollView;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/BottomScrollView;->pageScroll(I)Z

    .line 665
    :goto_0
    return-void

    .line 663
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNA()V

    goto :goto_0
.end method

.method public final aor()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 669
    iget-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcG:Z

    if-nez v0, :cond_0

    .line 686
    :goto_0
    return-void

    .line 674
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aon()Landroid/widget/Button;

    move-result-object v0

    .line 677
    invoke-static {v0, v1, v1, v1, v1}, Leot;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 680
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNz()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 681
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00d8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 682
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 683
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aom()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 685
    iput-boolean v3, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcG:Z

    goto :goto_0
.end method

.method public final aos()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 690
    iget-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcG:Z

    if-eqz v0, :cond_0

    .line 718
    :goto_0
    return-void

    .line 694
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aom()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 696
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aon()Landroid/widget/Button;

    move-result-object v0

    .line 699
    const v1, 0x7f0a0235

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 700
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00e0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 701
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 703
    sget v2, Lesp;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_1

    .line 704
    const v1, 0x7f0202c2

    invoke-static {v0, v4, v4, v1, v4}, Leot;->a(Landroid/widget/TextView;IIII)V

    .line 715
    :goto_1
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 717
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcG:Z

    goto :goto_0

    .line 709
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020137

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 710
    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v1, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 711
    invoke-static {v0, v5, v5, v2, v5}, Leot;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method protected final awa()V
    .locals 5

    .prologue
    .line 127
    invoke-super {p0}, Lhcd;->awa()V

    .line 128
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DO()Lgpp;

    move-result-object v1

    new-instance v2, Lhcb;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->En()Leue;

    move-result-object v3

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v4

    invoke-virtual {v4}, Lcfo;->DF()Lcin;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lhcb;-><init>(Leue;Lcin;)V

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DB()Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->a(Lgpp;Lhcb;Landroid/accounts/AccountManager;)V

    .line 135
    return-void
.end method

.method public final gi(Z)V
    .locals 1

    .prologue
    .line 722
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    if-eqz v0, :cond_0

    .line 723
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aom()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 724
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcD:Lcom/google/android/setupwizard/SetupWizardNavBar;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/SetupWizardNavBar;->aon()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 726
    :cond_0
    return-void
.end method

.method public final kM(I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 384
    iget-boolean v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcz:Z

    if-nez v1, :cond_0

    .line 385
    iget-object v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mBackgroundTasks:Lgpp;

    const-string v2, "refresh_search_domain_and_cookies"

    invoke-interface {v1, v2}, Lgpp;->nw(Ljava/lang/String;)V

    .line 390
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcH:Z

    if-eqz v1, :cond_4

    .line 391
    iget v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dct:I

    if-ne p1, v1, :cond_1

    .line 392
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNH()V

    .line 394
    :cond_1
    const v1, 0x7f050006

    invoke-virtual {p0, v0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->overridePendingTransition(II)V

    .line 411
    :cond_2
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 412
    const-string v2, "opted_in_result"

    if-nez p1, :cond_3

    const/4 v0, 0x1

    :cond_3
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 413
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->setResult(ILandroid/content/Intent;)V

    .line 414
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->finish()V

    .line 415
    return-void

    .line 395
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcA:Z

    if-nez v1, :cond_2

    .line 398
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/googlequicksearchbox/SearchActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 400
    if-nez p1, :cond_6

    .line 401
    const-string v2, "android.intent.action.ASSIST"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 405
    :cond_5
    :goto_1
    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 406
    const-string v2, "source"

    const-string v3, "FIRST_RUN"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 408
    invoke-virtual {p0, v1}, Lcom/google/android/velvet/tg/FirstRunActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 402
    :cond_6
    iget v2, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dct:I

    if-ne p1, v2, :cond_5

    .line 403
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNH()V

    goto :goto_1
.end method

.method public final nO(Ljava/lang/String;)Lhcc;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhcc;

    .line 425
    iget-object v2, v0, Lhcc;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 429
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 295
    iget-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcH:Z

    if-nez v0, :cond_0

    .line 297
    iget-boolean v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcz:Z

    if-eqz v0, :cond_1

    .line 300
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNC()V

    .line 304
    :cond_0
    :goto_0
    return-void

    .line 302
    :cond_1
    invoke-super {p0}, Lhcd;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 148
    invoke-super {p0, p1}, Lhcd;->onCreate(Landroid/os/Bundle;)V

    .line 150
    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNE()V

    .line 153
    invoke-virtual {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->awo()Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    iget v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dct:I

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->kM(I)V

    .line 190
    :goto_0
    return-void

    .line 158
    :cond_0
    if-eqz p1, :cond_1

    .line 159
    const-string v0, "enabled"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v3

    .line 160
    const-string v0, "accounts"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v4

    .line 161
    if-eqz v3, :cond_1

    if-eqz v4, :cond_1

    array-length v0, v3

    array-length v1, v4

    if-ne v0, v1, :cond_1

    .line 164
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcI:Ljava/util/List;

    move v1, v2

    .line 165
    :goto_1
    array-length v0, v3

    if-ge v1, v0, :cond_1

    .line 166
    aget-object v0, v4, v1

    check-cast v0, Landroid/accounts/Account;

    .line 167
    iget-object v5, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcI:Ljava/util/List;

    new-instance v6, Lhcc;

    aget-boolean v7, v3, v1

    invoke-direct {v6, v0, v7}, Lhcc;-><init>(Landroid/accounts/Account;Z)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 172
    :cond_1
    const v0, 0x7f04016c

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->setContentView(I)V

    .line 174
    const v0, 0x7f1101a9

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 175
    iget-boolean v1, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcH:Z

    if-eqz v1, :cond_2

    const v1, 0x7f0a02d3

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 177
    const v0, 0x7f1103f7

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupwizard/util/BottomScrollView;

    iput-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcE:Lcom/google/android/setupwizard/util/BottomScrollView;

    .line 179
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/tg/FirstRunActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0202be

    const v3, 0x7f0202bf

    invoke-static {v0, v1, v3}, Lcom/google/android/setupwizard/util/SetupWizardIllustration;->d(Landroid/view/View;II)V

    .line 183
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcI:Ljava/util/List;

    if-nez v0, :cond_3

    .line 184
    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNF()V

    .line 185
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Ss()[Landroid/accounts/Account;

    move-result-object v0

    .line 186
    new-instance v1, Lhcp;

    iget-object v3, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->mTaskRunner:Lerk;

    invoke-direct {v1, p0, v0, v3}, Lhcp;-><init>(Lcom/google/android/velvet/tg/FirstRunActivity;[Landroid/accounts/Account;Lerk;)V

    new-array v0, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lhcp;->a([Ljava/lang/Object;)Lenp;

    goto/16 :goto_0

    .line 175
    :cond_2
    const v1, 0x7f0a02d2

    goto :goto_2

    .line 188
    :cond_3
    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNG()V

    goto/16 :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0, p1}, Lcom/google/android/velvet/tg/FirstRunActivity;->setIntent(Landroid/content/Intent;)V

    .line 217
    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNE()V

    .line 221
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcI:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 222
    invoke-direct {p0}, Lcom/google/android/velvet/tg/FirstRunActivity;->aNG()V

    .line 224
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 278
    invoke-super {p0, p1}, Lhcd;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 280
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcI:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 281
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Z

    .line 282
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [Landroid/accounts/Account;

    .line 283
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/google/android/velvet/tg/FirstRunActivity;->dcI:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhcc;

    .line 285
    iget-boolean v4, v0, Lhcc;->aPI:Z

    aput-boolean v4, v2, v1

    .line 286
    iget-object v0, v0, Lhcc;->mAccount:Landroid/accounts/Account;

    aput-object v0, v3, v1

    .line 283
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 288
    :cond_0
    const-string v0, "enabled"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 289
    const-string v0, "accounts"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 291
    :cond_1
    return-void
.end method
