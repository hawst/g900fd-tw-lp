.class public Lcom/google/android/velvet/ui/VelvetMainContainer;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private asG:Lcom/google/android/shared/ui/CoScrollContainer;

.field private dfm:Landroid/view/View;

.field private dfn:Landroid/view/View;

.field private dfo:Landroid/view/View;

.field private dfp:I

.field private dfq:I

.field private dfr:I

.field private dfs:Z

.field private dft:Z

.field private dfu:Z

.field private dfv:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    return-void
.end method

.method private A(III)V
    .locals 1

    .prologue
    .line 91
    invoke-virtual {p0, p1}, Lcom/google/android/velvet/ui/VelvetMainContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ui/MainContentView;

    .line 92
    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {v0, p2, p3}, Lcom/google/android/velvet/ui/MainContentView;->aL(II)V

    .line 95
    :cond_0
    return-void
.end method

.method private aOl()V
    .locals 3

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfs:Z

    if-eqz v0, :cond_3

    .line 107
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfm:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget v1, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfq:I

    add-int/2addr v0, v1

    .line 111
    :goto_0
    const/4 v1, 0x0

    .line 112
    iget-boolean v2, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfu:Z

    if-eqz v2, :cond_0

    .line 113
    iget v1, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfp:I

    add-int/lit8 v1, v1, 0x0

    .line 115
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dft:Z

    if-eqz v2, :cond_1

    .line 116
    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfo:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    .line 118
    :cond_1
    const v2, 0x7f110261

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/velvet/ui/VelvetMainContainer;->A(III)V

    .line 119
    const v2, 0x7f11025d

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/velvet/ui/VelvetMainContainer;->A(III)V

    .line 120
    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    if-eqz v2, :cond_2

    .line 121
    iget-object v2, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/shared/ui/CoScrollContainer;->aL(II)V

    .line 123
    :cond_2
    return-void

    .line 109
    :cond_3
    iget v0, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfv:I

    goto :goto_0
.end method


# virtual methods
.method public final H(ZZ)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dft:Z

    .line 57
    iput-boolean p2, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfu:Z

    .line 58
    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetMainContainer;->aOl()V

    .line 59
    return-void
.end method

.method public final go(Z)V
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfs:Z

    .line 51
    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetMainContainer;->aOl()V

    .line 52
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 63
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 64
    const v0, 0x7f110136

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetMainContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfm:Landroid/view/View;

    .line 65
    const v0, 0x7f11048f

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetMainContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfn:Landroid/view/View;

    .line 66
    const v0, 0x7f1101e3

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetMainContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfo:Landroid/view/View;

    .line 67
    const v0, 0x7f11025c

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetMainContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/ui/CoScrollContainer;

    iput-object v0, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    .line 68
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfp:I

    .line 70
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfq:I

    .line 72
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfr:I

    .line 74
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 130
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 131
    const-class v0, Lcom/google/android/velvet/ui/VelvetMainContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 132
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 80
    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfm:Landroid/view/View;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/velvet/ui/VelvetMainContainer;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 81
    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfn:Landroid/view/View;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/velvet/ui/VelvetMainContainer;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 82
    iget-object v1, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfo:Landroid/view/View;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/velvet/ui/VelvetMainContainer;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 84
    iget-object v0, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfn:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget v1, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfr:I

    if-gt v0, v1, :cond_0

    iput v0, p0, Lcom/google/android/velvet/ui/VelvetMainContainer;->dfv:I

    .line 85
    :cond_0
    invoke-direct {p0}, Lcom/google/android/velvet/ui/VelvetMainContainer;->aOl()V

    .line 87
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 88
    return-void
.end method
