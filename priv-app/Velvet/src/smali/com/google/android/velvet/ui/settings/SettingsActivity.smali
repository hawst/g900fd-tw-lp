.class public Lcom/google/android/velvet/ui/settings/SettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "PG"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcwl;


# instance fields
.field private aUh:Lcxs;

.field private dfA:Lcxn;

.field private dfB:Lchr;

.field private dfC:Lhfr;

.field protected dfD:Z

.field private dfz:Landroid/preference/PreferenceActivity$Header;

.field private mIntentStarter:Lelp;

.field private mLoginHelper:Lcrh;

.field private mNowOptInSettings:Lcin;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 530
    return-void
.end method

.method private a(Landroid/preference/PreferenceActivity$Header;Landroid/accounts/Account;ILcin;Lcxs;)Z
    .locals 10
    .param p2    # Landroid/accounts/Account;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 356
    iget-wide v6, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v8, 0x7f1104c3

    cmp-long v0, v6, v8

    if-nez v0, :cond_2

    move v5, v2

    .line 361
    :goto_0
    if-nez p3, :cond_3

    .line 362
    iget-wide v4, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v6, 0x7f1104c8

    cmp-long v0, v4, v6

    if-eqz v0, :cond_1

    .line 363
    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->extras:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 364
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p1, Landroid/preference/PreferenceActivity$Header;->extras:Landroid/os/Bundle;

    .line 366
    :cond_0
    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->extras:Landroid/os/Bundle;

    const-string v3, "LOADING_KEY"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 367
    const v0, 0x7f0a05c5

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    .line 460
    :cond_1
    :goto_1
    return v1

    :cond_2
    move v5, v1

    .line 356
    goto :goto_0

    .line 371
    :cond_3
    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->extras:Landroid/os/Bundle;

    if-eqz v0, :cond_4

    .line 372
    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->extras:Landroid/os/Bundle;

    const-string v3, "LOADING_KEY"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 375
    :cond_4
    if-eqz p2, :cond_6

    invoke-interface {p4, p2}, Lcin;->k(Landroid/accounts/Account;)Liyl;

    move-result-object v0

    .line 380
    :goto_2
    if-eqz p2, :cond_7

    invoke-interface {p4, p2}, Lcin;->e(Landroid/accounts/Account;)Z

    move-result v3

    if-eqz v3, :cond_7

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->dfD:Z

    if-eqz v0, :cond_7

    move v3, v2

    .line 389
    :goto_3
    if-eq p3, v2, :cond_5

    if-nez p2, :cond_8

    :cond_5
    move v0, v2

    .line 392
    :goto_4
    if-eqz v5, :cond_e

    .line 394
    if-nez p2, :cond_9

    iget-object v5, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mLoginHelper:Lcrh;

    invoke-virtual {v5}, Lcrh;->Ss()[Landroid/accounts/Account;

    move-result-object v5

    array-length v5, v5

    if-lez v5, :cond_9

    .line 404
    const v0, 0x7f0a03cd

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    .line 405
    const-class v0, Lcom/google/android/search/core/preferences/PrivacyAndAccountFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    .line 406
    iput-object v4, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    goto :goto_1

    :cond_6
    move-object v0, v4

    .line 375
    goto :goto_2

    :cond_7
    move v3, v1

    .line 380
    goto :goto_3

    :cond_8
    move v0, v1

    .line 389
    goto :goto_4

    .line 408
    :cond_9
    if-nez v3, :cond_1

    .line 411
    if-nez v3, :cond_a

    if-eqz v0, :cond_a

    .line 413
    invoke-direct {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->aOo()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    .line 414
    iput-object v4, p1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    goto :goto_1

    .line 423
    :cond_a
    const/4 v0, 0x5

    if-ne p3, v0, :cond_b

    .line 424
    const v0, 0x7f0a03cb

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    move v0, v1

    .line 440
    :goto_5
    iput-object v3, p1, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    iput-object v4, p1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    iput-object v4, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    if-eqz v0, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p1, Landroid/preference/PreferenceActivity$Header;->extras:Landroid/os/Bundle;

    iget-object v0, p1, Landroid/preference/PreferenceActivity$Header;->extras:Landroid/os/Bundle;

    const-string v3, "notSelectableAndContainsLink"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 426
    :cond_b
    const/4 v0, 0x4

    if-ne p3, v0, :cond_c

    .line 427
    const v0, 0x7f0a03ca

    new-array v3, v2, [Ljava/lang/Object;

    const-string v5, "http://support.google.com/websearch/answer/2938260"

    aput-object v5, v3, v1

    invoke-virtual {p0, v0, v3}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    move-object v3, v0

    move v0, v2

    .line 430
    goto :goto_5

    .line 431
    :cond_c
    const/4 v0, 0x6

    if-ne p3, v0, :cond_d

    .line 432
    const v0, 0x7f0a03cc

    new-array v3, v2, [Ljava/lang/Object;

    const-string v5, "https://support.google.com/accounts/answer/3118687"

    aput-object v5, v3, v1

    invoke-virtual {p0, v0, v3}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    move-object v3, v0

    move v0, v2

    .line 435
    goto :goto_5

    .line 439
    :cond_d
    const v0, 0x7f0a0147

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    move v0, v1

    goto :goto_5

    .line 448
    :cond_e
    if-nez v3, :cond_f

    move v1, v2

    .line 449
    goto/16 :goto_1

    .line 452
    :cond_f
    invoke-virtual {p5}, Lcxs;->TG()Z

    move-result v0

    if-nez v0, :cond_1

    .line 456
    iput-object v4, p1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    goto/16 :goto_1
.end method

.method private aOo()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 480
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 481
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 482
    const-string v1, "source"

    const-string v2, "SETTINGS"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 483
    return-object v0
.end method

.method private aOp()Lchr;
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->dfB:Lchr;

    if-nez v0, :cond_0

    .line 499
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 500
    invoke-virtual {v0}, Lgql;->aJs()Lchr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->dfB:Lchr;

    .line 502
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->dfB:Lchr;

    return-object v0
.end method


# virtual methods
.method public final Tq()Lcxn;
    .locals 1

    .prologue
    .line 744
    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->dfA:Lcxn;

    return-object v0
.end method

.method protected final a(Lcrh;Lcxs;Lcin;Lcxn;Lelp;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mLoginHelper:Lcrh;

    .line 164
    iput-object p2, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->aUh:Lcxs;

    .line 165
    iput-object p3, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mNowOptInSettings:Lcin;

    .line 166
    iput-object p4, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->dfA:Lcxn;

    .line 167
    iput-object p5, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mIntentStarter:Lelp;

    .line 168
    return-void
.end method

.method public final aOn()Landroid/widget/Switch;
    .locals 8
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getListView()Landroid/widget/ListView;

    move-result-object v2

    .line 147
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 148
    invoke-virtual {v2, v1}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 149
    iget-wide v4, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v6, 0x7f1104c3

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    .line 150
    invoke-virtual {v2, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1102dd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    .line 153
    :goto_1
    return-object v0

    .line 147
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 153
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final f(Ljava/util/List;Z)V
    .locals 10

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v2

    .line 273
    const/4 v3, 0x3

    .line 274
    if-eqz v2, :cond_0

    .line 275
    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mNowOptInSettings:Lcin;

    invoke-interface {v0, v2}, Lcin;->i(Landroid/accounts/Account;)I

    move-result v3

    .line 278
    :cond_0
    if-nez v3, :cond_1

    .line 279
    invoke-direct {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->aOp()Lchr;

    move-result-object v0

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 282
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 283
    :cond_2
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 284
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceActivity$Header;

    .line 286
    iget-wide v4, v1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v8, 0x7f1104c3

    cmp-long v0, v4, v8

    if-eqz v0, :cond_3

    iget-wide v4, v1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v8, 0x7f1104c7

    cmp-long v0, v4, v8

    if-nez v0, :cond_4

    .line 288
    :cond_3
    iget-object v4, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mNowOptInSettings:Lcin;

    iget-object v5, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->aUh:Lcxs;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->a(Landroid/preference/PreferenceActivity$Header;Landroid/accounts/Account;ILcin;Lcxs;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 290
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 294
    :cond_4
    invoke-static {}, Lckw;->OZ()Lckw;

    move-result-object v0

    invoke-virtual {v0}, Lckw;->Pa()Z

    move-result v7

    .line 295
    iget-wide v4, v1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v8, 0x7f1104ca

    cmp-long v0, v4, v8

    if-nez v0, :cond_5

    .line 296
    if-nez v7, :cond_5

    .line 297
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 301
    :cond_5
    iget-wide v4, v1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v8, 0x7f1104cb

    cmp-long v0, v4, v8

    if-nez v0, :cond_6

    .line 303
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 307
    :cond_6
    iget-wide v4, v1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v8, 0x7f1104c8

    cmp-long v0, v4, v8

    if-eqz v0, :cond_7

    iget-wide v4, v1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v8, 0x7f1104c9

    cmp-long v0, v4, v8

    if-nez v0, :cond_9

    .line 309
    :cond_7
    if-eqz v7, :cond_8

    iget-object v4, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mNowOptInSettings:Lcin;

    iget-object v5, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->aUh:Lcxs;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->a(Landroid/preference/PreferenceActivity$Header;Landroid/accounts/Account;ILcin;Lcxs;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 311
    :cond_8
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 315
    :cond_9
    iget-wide v4, v1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v8, 0x7f1104cc

    cmp-long v0, v4, v8

    if-nez v0, :cond_a

    .line 316
    if-nez v7, :cond_a

    .line 317
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_0

    .line 321
    :cond_a
    iget-wide v4, v1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v8, 0x7f1104c5

    cmp-long v0, v4, v8

    if-nez v0, :cond_b

    .line 322
    if-nez p2, :cond_b

    .line 323
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_0

    .line 329
    :cond_b
    iget-object v0, v1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->dfz:Landroid/preference/PreferenceActivity$Header;

    if-nez v0, :cond_2

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 330
    iput-object v1, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->dfz:Landroid/preference/PreferenceActivity$Header;

    goto/16 :goto_0

    .line 333
    :cond_c
    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->dfC:Lhfr;

    if-eqz v0, :cond_d

    .line 335
    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->dfC:Lhfr;

    invoke-virtual {v0}, Lhfr;->clear()V

    .line 336
    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->dfC:Lhfr;

    invoke-virtual {v0, p1}, Lhfr;->addAll(Ljava/util/Collection;)V

    .line 338
    :cond_d
    return-void
.end method

.method public getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->aOp()Lchr;

    const-string v0, "SearchSettings"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 215
    invoke-direct {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->aOp()Lchr;

    move-result-object v0

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    .line 218
    :goto_1
    return-object v0

    .line 214
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 218
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    goto :goto_1
.end method

.method protected isValidFragment(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x1

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 208
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 209
    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mIntentStarter:Lelp;

    invoke-virtual {v0, p1, p2, p3}, Lelp;->a(IILandroid/content/Intent;)V

    .line 210
    return-void
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 249
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 250
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v1

    .line 251
    invoke-virtual {v1}, Lcfo;->DR()Lcpx;

    move-result-object v0

    const-string v2, "SETTINGS"

    invoke-virtual {v0, v2}, Lcpx;->io(Ljava/lang/String;)V

    .line 252
    const v0, 0x7f07001f

    invoke-virtual {p0, v0, p1}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->loadHeadersFromResource(ILjava/util/List;)V

    .line 253
    invoke-virtual {v1}, Lcfo;->DD()Lcjs;

    move-result-object v0

    invoke-virtual {v0}, Lcjs;->Mi()Z

    move-result v0

    .line 255
    if-nez v0, :cond_0

    .line 256
    invoke-virtual {v1}, Lcfo;->Em()Lcha;

    move-result-object v0

    invoke-virtual {v0}, Lcha;->Fi()Z

    move-result v0

    .line 262
    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->aUh:Lcxs;

    invoke-virtual {v1}, Lcxs;->TH()Lcxi;

    move-result-object v1

    invoke-virtual {v1}, Lcxi;->Ty()Ljel;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->dfD:Z

    .line 265
    invoke-virtual {p0, p1, v0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->f(Ljava/util/List;Z)V

    .line 266
    return-void

    .line 262
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 109
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v7

    .line 111
    invoke-static {}, Lckw;->OZ()Lckw;

    move-result-object v0

    .line 112
    invoke-virtual {v0}, Lckw;->Pd()Z

    move-result v1

    if-nez v1, :cond_0

    .line 113
    invoke-virtual {v7}, Lcfo;->BK()Lcke;

    move-result-object v1

    invoke-virtual {v0, v1}, Lckw;->a(Lcke;)V

    .line 116
    :cond_0
    invoke-virtual {v7}, Lcfo;->DF()Lcin;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mNowOptInSettings:Lcin;

    .line 117
    new-instance v0, Lcxn;

    iget-object v1, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mNowOptInSettings:Lcin;

    invoke-virtual {v7}, Lcfo;->DL()Lcrh;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->aOo()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v7}, Lcfo;->DR()Lcpx;

    move-result-object v5

    new-instance v6, Lhfq;

    invoke-direct {v6, p0}, Lhfq;-><init>(Lcom/google/android/velvet/ui/settings/SettingsActivity;)V

    move-object v4, p0

    invoke-direct/range {v0 .. v6}, Lcxn;-><init>(Lcin;Lcrh;Landroid/content/Intent;Landroid/app/Activity;Lcpx;Ligi;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->dfA:Lcxn;

    .line 130
    invoke-virtual {v7}, Lcfo;->DL()Lcrh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mLoginHelper:Lcrh;

    .line 131
    invoke-virtual {v7}, Lcfo;->DE()Lcxs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->aUh:Lcxs;

    .line 132
    new-instance v0, Ldlv;

    const/16 v1, 0x64

    invoke-direct {v0, p0, v1}, Ldlv;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mIntentStarter:Lelp;

    .line 134
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 135
    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mIntentStarter:Lelp;

    invoke-virtual {v0, p1}, Lelp;->x(Landroid/os/Bundle;)V

    .line 137
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Lhhq;->aOV()Lgiw;

    move-result-object v0

    invoke-virtual {v0}, Lgiw;->aGh()Lgix;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Lgix;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 140
    invoke-virtual {v0}, Lgix;->cV()V

    .line 142
    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6

    .prologue
    .line 757
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 758
    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mNowOptInSettings:Lcin;

    invoke-interface {v0}, Lcin;->Kz()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "now_settings"

    .line 761
    :goto_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v3

    new-instance v0, Lgpk;

    invoke-direct {v0, p0}, Lgpk;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Lgpk;->nv(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mNowOptInSettings:Lcin;

    invoke-interface {v0}, Lcin;->Kz()Z

    move-result v5

    move-object v0, p1

    move-object v1, p0

    invoke-static/range {v0 .. v5}, Lgbk;->a(Landroid/view/Menu;Landroid/app/Activity;Ljava/lang/String;Landroid/accounts/Account;Landroid/net/Uri;Z)V

    .line 764
    const/4 v0, 0x1

    return v0

    .line 758
    :cond_0
    const-string v2, "disabled_settings"

    goto :goto_0
.end method

.method public onGetInitialHeader()Landroid/preference/PreferenceActivity$Header;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->dfz:Landroid/preference/PreferenceActivity$Header;

    return-object v0
.end method

.method public onIsMultiPane()Z
    .locals 2

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 488
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 490
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->finish()V

    .line 491
    const/4 v0, 0x1

    .line 494
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 178
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 179
    invoke-direct {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->aOp()Lchr;

    move-result-object v0

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    .line 180
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 185
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    .line 186
    invoke-virtual {v0}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v0

    .line 187
    const/16 v1, 0x23

    invoke-virtual {v0, v1}, Lfaq;->ix(I)V

    .line 188
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 192
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 193
    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->dfA:Lcxn;

    invoke-virtual {v0}, Lcxn;->TD()V

    .line 194
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 198
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mIntentStarter:Lelp;

    invoke-virtual {v0, p1}, Lelp;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 200
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 749
    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mNowOptInSettings:Lcin;

    invoke-interface {v0, p2}, Lcin;->gx(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 750
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/settings/SettingsActivity;->invalidateHeaders()V

    .line 751
    invoke-interface {p1, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 753
    :cond_0
    return-void
.end method

.method public setListAdapter(Landroid/widget/ListAdapter;)V
    .locals 4

    .prologue
    .line 507
    if-nez p1, :cond_0

    .line 508
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/preference/PreferenceActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 520
    :goto_0
    return-void

    .line 512
    :cond_0
    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    .line 513
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 514
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_1

    .line 515
    invoke-interface {p1, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 514
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 517
    :cond_1
    new-instance v0, Lhfr;

    iget-object v1, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->dfA:Lcxn;

    invoke-direct {v0, p0, v1, v3}, Lhfr;-><init>(Landroid/content/Context;Lcxn;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->dfC:Lhfr;

    .line 518
    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->dfC:Lhfr;

    invoke-super {p0, v0}, Landroid/preference/PreferenceActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method public final wr()Leoj;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/velvet/ui/settings/SettingsActivity;->mIntentStarter:Lelp;

    return-object v0
.end method
