.class public final Lcom/google/android/velvet/presenter/inappwebpage/Request;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final dbO:Lijp;

.field private final dz:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lhbo;

    invoke-direct {v0}, Lhbo;-><init>()V

    sput-object v0, Lcom/google/android/velvet/presenter/inappwebpage/Request;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Lijp;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/inappwebpage/Request;->dz:Landroid/net/Uri;

    .line 28
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lijp;

    iput-object v0, p0, Lcom/google/android/velvet/presenter/inappwebpage/Request;->dbO:Lijp;

    .line 29
    return-void
.end method

.method public static R(Landroid/content/Intent;)Lcom/google/android/velvet/presenter/inappwebpage/Request;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 32
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 33
    new-instance v0, Lcom/google/android/velvet/presenter/inappwebpage/Request;

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-static {}, Lijp;->aXb()Lijp;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/velvet/presenter/inappwebpage/Request;-><init>(Landroid/net/Uri;Lijp;)V

    .line 35
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final aNn()Lijp;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/velvet/presenter/inappwebpage/Request;->dbO:Lijp;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 65
    instance-of v1, p1, Lcom/google/android/velvet/presenter/inappwebpage/Request;

    if-eqz v1, :cond_0

    .line 66
    check-cast p1, Lcom/google/android/velvet/presenter/inappwebpage/Request;

    .line 67
    iget-object v1, p0, Lcom/google/android/velvet/presenter/inappwebpage/Request;->dz:Landroid/net/Uri;

    iget-object v2, p1, Lcom/google/android/velvet/presenter/inappwebpage/Request;->dz:Landroid/net/Uri;

    invoke-static {v1, v2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/velvet/presenter/inappwebpage/Request;->dbO:Lijp;

    iget-object v2, p1, Lcom/google/android/velvet/presenter/inappwebpage/Request;->dbO:Lijp;

    invoke-static {v1, v2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 70
    :cond_0
    return v0
.end method

.method public final getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/velvet/presenter/inappwebpage/Request;->dz:Landroid/net/Uri;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 75
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/velvet/presenter/inappwebpage/Request;->dz:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/velvet/presenter/inappwebpage/Request;->dbO:Lijp;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 57
    invoke-static {p0}, Lifo;->bc(Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "mUri"

    iget-object v2, p0, Lcom/google/android/velvet/presenter/inappwebpage/Request;->dz:Landroid/net/Uri;

    invoke-static {v2}, Lcpn;->m(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "mInitialInAppUriPatterns"

    iget-object v2, p0, Lcom/google/android/velvet/presenter/inappwebpage/Request;->dbO:Lijp;

    invoke-virtual {v0, v1, v2}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    invoke-virtual {v0}, Lifp;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/velvet/presenter/inappwebpage/Request;->dz:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 81
    iget-object v0, p0, Lcom/google/android/velvet/presenter/inappwebpage/Request;->dbO:Lijp;

    invoke-virtual {v0}, Lijp;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 82
    iget-object v0, p0, Lcom/google/android/velvet/presenter/inappwebpage/Request;->dbO:Lijp;

    invoke-virtual {v0}, Lijp;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 83
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 85
    :cond_0
    return-void
.end method
