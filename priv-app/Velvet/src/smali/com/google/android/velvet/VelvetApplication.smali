.class public Lcom/google/android/velvet/VelvetApplication;
.super Landroid/app/Application;
.source "PG"

# interfaces
.implements Lgpd;
.implements Lgpe;


# static fields
.field private static cSA:Lcom/google/android/velvet/VelvetApplication;

.field private static cSz:Landroid/content/pm/PackageInfo;


# instance fields
.field private final cSy:Ljava/util/Set;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 63
    const-class v1, Lcom/google/android/velvet/VelvetApplication;

    monitor-enter v1

    .line 66
    :try_start_0
    sget-object v0, Lcom/google/android/velvet/VelvetApplication;->cSA:Lcom/google/android/velvet/VelvetApplication;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 67
    sput-object p0, Lcom/google/android/velvet/VelvetApplication;->cSA:Lcom/google/android/velvet/VelvetApplication;

    .line 68
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    invoke-static {p0}, Lgqq;->a(Lcom/google/android/velvet/VelvetApplication;)V

    .line 73
    invoke-static {}, Lege;->ed()V

    .line 74
    invoke-static {}, Legr;->aoD()V

    .line 76
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->cSy:Ljava/util/Set;

    .line 77
    return-void

    .line 66
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static aJe()Lcom/google/android/velvet/VelvetApplication;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/google/android/velvet/VelvetApplication;->cSA:Lcom/google/android/velvet/VelvetApplication;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/google/android/velvet/VelvetApplication;->cSA:Lcom/google/android/velvet/VelvetApplication;

    return-object v0
.end method

.method private static declared-synchronized aJf()Landroid/content/pm/PackageInfo;
    .locals 4

    .prologue
    .line 164
    const-class v1, Lcom/google/android/velvet/VelvetApplication;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/velvet/VelvetApplication;->cSz:Landroid/content/pm/PackageInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 166
    :try_start_1
    sget-object v0, Lcom/google/android/velvet/VelvetApplication;->cSA:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v0}, Lcom/google/android/velvet/VelvetApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 167
    sget-object v2, Lcom/google/android/velvet/VelvetApplication;->cSA:Lcom/google/android/velvet/VelvetApplication;

    invoke-virtual {v2}, Lcom/google/android/velvet/VelvetApplication;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 168
    sput-object v0, Lcom/google/android/velvet/VelvetApplication;->cSz:Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175
    :cond_0
    :try_start_2
    sget-object v0, Lcom/google/android/velvet/VelvetApplication;->cSz:Landroid/content/pm/PackageInfo;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v1

    return-object v0

    .line 169
    :catch_0
    move-exception v0

    .line 172
    :try_start_3
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static aJg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->aJf()Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static aJh()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->aJf()Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 188
    if-nez v0, :cond_0

    .line 189
    const-string v0, "4.0.28.eclipse"

    .line 191
    :cond_0
    return-object v0
.end method

.method public static xT()I
    .locals 1

    .prologue
    .line 183
    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->aJf()Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    return v0
.end method


# virtual methods
.method public final a(Lgpe;)V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->cSy:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 142
    return-void
.end method

.method public final axE()V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->cSy:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgpe;

    .line 152
    invoke-interface {v0}, Lgpe;->axE()V

    goto :goto_0

    .line 154
    :cond_0
    return-void
.end method

.method public final onActivityStop()V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/velvet/VelvetApplication;->cSy:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgpe;

    .line 159
    invoke-interface {v0}, Lgpe;->onActivityStop()V

    goto :goto_0

    .line 161
    :cond_0
    return-void
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 99
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 100
    const-string v0, "com.google.android.googlequicksearchbox:search"

    invoke-static {p0}, Lesp;->aw(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 106
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 0

    .prologue
    .line 111
    invoke-super {p0, p1}, Landroid/app/Application;->onTrimMemory(I)V

    .line 112
    invoke-static {p1}, Lgql;->kw(I)V

    .line 113
    return-void
.end method

.method public startActivities([Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 129
    invoke-static {}, Lepy;->avr()Lepy;

    move-result-object v0

    invoke-virtual {v0}, Lepy;->avs()V

    .line 130
    invoke-super {p0, p1}, Landroid/app/Application;->startActivities([Landroid/content/Intent;)V

    .line 131
    return-void
.end method

.method public startActivities([Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 135
    invoke-static {}, Lepy;->avr()Lepy;

    move-result-object v0

    invoke-virtual {v0}, Lepy;->avs()V

    .line 136
    invoke-super {p0, p1, p2}, Landroid/app/Application;->startActivities([Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 137
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 123
    invoke-static {}, Lepy;->avr()Lepy;

    move-result-object v0

    invoke-virtual {v0}, Lepy;->avs()V

    .line 124
    invoke-super {p0, p1}, Landroid/app/Application;->startActivity(Landroid/content/Intent;)V

    .line 125
    return-void
.end method

.method public startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 117
    invoke-static {}, Lepy;->avr()Lepy;

    move-result-object v0

    invoke-virtual {v0}, Lepy;->avs()V

    .line 118
    invoke-super {p0, p1, p2}, Landroid/app/Application;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 119
    return-void
.end method
