.class public Lcom/google/android/velvet/location/LocationReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private final aUk:Lgql;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/velvet/location/LocationReceiver;-><init>(Lgql;)V

    .line 28
    return-void
.end method

.method protected constructor <init>(Lgql;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/velvet/location/LocationReceiver;->aUk:Lgql;

    .line 33
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 37
    const-string v0, "com.google.android.velvet.location.GMS_CORE_LOCATION"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    :goto_0
    return-void

    .line 41
    :cond_0
    const-string v0, "com.google.android.location.LOCATION"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 42
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/location/Location;

    if-nez v1, :cond_2

    .line 43
    :cond_1
    const-string v1, "LocationReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Received bad location: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 47
    :cond_2
    check-cast v0, Landroid/location/Location;

    .line 52
    iget-object v1, p0, Lcom/google/android/velvet/location/LocationReceiver;->aUk:Lgql;

    invoke-virtual {v1}, Lgql;->El()Lfdr;

    move-result-object v1

    invoke-interface {v1, v0}, Lfdr;->d(Landroid/location/Location;)V

    goto :goto_0
.end method
