.class public Lcom/google/android/velvet/ui/InAppWebPageActivity;
.super Landroid/app/Activity;
.source "PG"


# instance fields
.field private aUn:Lckg;

.field public dbL:Lhax;

.field public ddW:Landroid/widget/FrameLayout;

.field private ddX:Landroid/view/View;

.field private ddY:Landroid/widget/TextView;

.field private ddZ:Landroid/view/View;

.field private dea:I

.field private mIntentStarter:Lelp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 244
    return-void
.end method


# virtual methods
.method public final a(Lhfp;)V
    .locals 4

    .prologue
    .line 175
    iget-object v0, p1, Lhfp;->dfy:Landroid/webkit/WebView;

    .line 176
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setAlpha(F)V

    .line 177
    iget-object v1, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddW:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 178
    invoke-virtual {v0}, Landroid/webkit/WebView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->dea:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 179
    return-void
.end method

.method public final aMY()V
    .locals 4

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddZ:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 192
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddZ:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 193
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddZ:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->dea:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 194
    return-void
.end method

.method public final aNN()Lhfp;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    .line 162
    new-instance v0, Lcom/google/android/velvet/ui/widget/TextScalingWebview;

    invoke-direct {v0, p0}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;-><init>(Landroid/content/Context;)V

    .line 164
    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    .line 165
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 167
    iget-object v2, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->aUn:Lckg;

    invoke-virtual {v2, v0}, Lckg;->a(Landroid/webkit/WebView;)V

    .line 168
    iget-object v2, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->aUn:Lckg;

    invoke-virtual {v2}, Lckg;->OJ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 169
    invoke-static {}, Letc;->avI()V

    .line 170
    new-instance v1, Lhfp;

    invoke-direct {v1, v0}, Lhfp;-><init>(Landroid/webkit/WebView;)V

    return-object v1
.end method

.method public final aNO()V
    .locals 4

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddZ:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 199
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddZ:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->dea:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddZ:Landroid/view/View;

    new-instance v2, Lhec;

    invoke-direct {v2, p0, v1}, Lhec;-><init>(Lcom/google/android/velvet/ui/InAppWebPageActivity;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 201
    return-void
.end method

.method public final aNP()V
    .locals 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddX:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 214
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddX:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 215
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddX:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 216
    return-void
.end method

.method public final b(Lhfp;)V
    .locals 4

    .prologue
    .line 183
    iget-object v0, p1, Lhfp;->dfy:Landroid/webkit/WebView;

    .line 184
    invoke-virtual {v0}, Landroid/webkit/WebView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 185
    invoke-virtual {v0}, Landroid/webkit/WebView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget v2, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->dea:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lhed;

    invoke-direct {v2, p0, v0}, Lhed;-><init>(Lcom/google/android/velvet/ui/InAppWebPageActivity;Landroid/webkit/WebView;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 187
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 240
    invoke-super {p0, p1, p2, p3, p4}, Landroid/app/Activity;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 241
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->dbL:Lhax;

    new-instance v1, Landroid/util/PrintWriterPrinter;

    invoke-direct {v1, p3}, Landroid/util/PrintWriterPrinter;-><init>(Ljava/io/PrintWriter;)V

    invoke-virtual {v0, p1, v1}, Lhax;->a(Ljava/lang/String;Landroid/util/Printer;)V

    .line 242
    return-void
.end method

.method public final ex(I)V
    .locals 4

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddX:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 206
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddY:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 207
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddX:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 208
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddX:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->dea:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 209
    return-void
.end method

.method public final kP(I)V
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 220
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mIntentStarter:Lelp;

    invoke-virtual {v0, p1, p2, p3}, Lelp;->a(IILandroid/content/Intent;)V

    .line 158
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->dbL:Lhax;

    invoke-virtual {v0}, Lhax;->goBack()V

    .line 225
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    new-instance v0, Ldlv;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ldlv;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mIntentStarter:Lelp;

    .line 65
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->dea:I

    .line 67
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    .line 68
    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DS()Lckg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->aUn:Lckg;

    .line 70
    const v0, 0x7f0400aa

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->setContentView(I)V

    .line 72
    const v0, 0x7f110220

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddW:Landroid/widget/FrameLayout;

    .line 73
    const v0, 0x7f110221

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddX:Landroid/view/View;

    .line 74
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddX:Landroid/view/View;

    const v2, 0x7f1101b3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddY:Landroid/widget/TextView;

    .line 75
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddX:Landroid/view/View;

    const v2, 0x7f1101b4

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 76
    const v2, 0x7f0a0646

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 77
    new-instance v2, Lhee;

    invoke-direct {v2, p0}, Lhee;-><init>(Lcom/google/android/velvet/ui/InAppWebPageActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    const v0, 0x7f110222

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddZ:Landroid/view/View;

    .line 81
    invoke-virtual {v1}, Lgql;->aJU()Lgpu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->mIntentStarter:Lelp;

    invoke-virtual {v0, p0, v1}, Lgpu;->a(Lcom/google/android/velvet/ui/InAppWebPageActivity;Leoj;)Lhax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->dbL:Lhax;

    .line 83
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 85
    if-eqz p1, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->dbL:Lhax;

    invoke-virtual {v0, p1}, Lhax;->ad(Landroid/os/Bundle;)V

    .line 88
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->dbL:Lhax;

    invoke-virtual {v0, p1}, Lhax;->h(Landroid/view/Menu;)V

    .line 145
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->dbL:Lhax;

    invoke-virtual {v0}, Lhax;->destroy()V

    .line 132
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 133
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 229
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 231
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->dbL:Lhax;

    iget-object v0, v0, Lhax;->dbk:Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->finish()V

    .line 232
    const/4 v0, 0x1

    .line 234
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddW:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v2

    .line 110
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 111
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddW:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 112
    invoke-virtual {v0}, Landroid/webkit/WebView;->onPause()V

    .line 110
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 114
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 115
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->dbL:Lhax;

    invoke-virtual {v0, p1}, Lhax;->g(Landroid/view/Menu;)V

    .line 151
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 120
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 121
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddW:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v2

    .line 122
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->ddW:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 124
    invoke-virtual {v0}, Landroid/webkit/WebView;->onResume()V

    .line 122
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 126
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->dbL:Lhax;

    invoke-virtual {v0, p1}, Lhax;->ac(Landroid/os/Bundle;)V

    .line 139
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 140
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 93
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 95
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 96
    if-nez v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->dbL:Lhax;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhax;->a(Lcom/google/android/velvet/presenter/inappwebpage/Request;)V

    .line 104
    :goto_0
    return-void

    .line 99
    :cond_0
    const-string v1, "full_screen"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 100
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/InAppWebPageActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->hide()V

    .line 102
    :cond_1
    iget-object v1, p0, Lcom/google/android/velvet/ui/InAppWebPageActivity;->dbL:Lhax;

    invoke-static {v0}, Lcom/google/android/velvet/presenter/inappwebpage/Request;->R(Landroid/content/Intent;)Lcom/google/android/velvet/presenter/inappwebpage/Request;

    move-result-object v0

    invoke-virtual {v1, v0}, Lhax;->a(Lcom/google/android/velvet/presenter/inappwebpage/Request;)V

    goto :goto_0
.end method
