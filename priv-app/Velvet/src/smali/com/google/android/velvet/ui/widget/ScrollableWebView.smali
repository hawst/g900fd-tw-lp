.class public Lcom/google/android/velvet/ui/widget/ScrollableWebView;
.super Lcom/google/android/velvet/ui/widget/TextScalingWebview;
.source "PG"

# interfaces
.implements Lejf;


# instance fields
.field private bB:Z

.field private dgn:Lejd;

.field private dgo:Lhgg;

.field private dgp:Z

.field private dgq:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 65
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 74
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->setOverScrollMode(I)V

    .line 75
    return-void
.end method


# virtual methods
.method public final ats()I
    .locals 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getContentHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getScale()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public computeScroll()V
    .locals 0

    .prologue
    .line 235
    return-void
.end method

.method public destroy()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 152
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "We MUST have been detached from the UI at this point."

    invoke-static {v0, v2}, Lifv;->d(ZLjava/lang/Object;)V

    .line 153
    iput-boolean v1, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->bB:Z

    .line 154
    invoke-super {p0}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->destroy()V

    .line 155
    return-void

    .line 152
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDrawingRect(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 177
    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 179
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getTranslationY()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 180
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->dgn:Lejd;

    invoke-virtual {v0, p1}, Lejd;->f(Landroid/graphics/Rect;)V

    .line 181
    return-void
.end method

.method public final hN(I)V
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getScrollX()I

    move-result v0

    invoke-super {p0, v0, p1}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->scrollTo(II)V

    .line 141
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    .line 252
    invoke-super {p0}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->onAttachedToWindow()V

    .line 253
    const/4 v1, 0x0

    .line 255
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 256
    :goto_0
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_2

    .line 257
    instance-of v2, v0, Lhgh;

    if-eqz v2, :cond_1

    .line 258
    check-cast v0, Lhgh;

    .line 264
    :goto_1
    if-eqz v0, :cond_0

    .line 265
    new-instance v1, Lhgg;

    invoke-direct {v1, v0}, Lhgg;-><init>(Lhgh;)V

    iput-object v1, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->dgo:Lhgg;

    .line 268
    :cond_0
    return-void

    .line 261
    :cond_1
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 272
    invoke-super {p0}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->onDetachedFromWindow()V

    .line 273
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->dgo:Lhgg;

    .line 274
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v0

    and-int/lit16 v0, v0, 0x2002

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    .line 227
    const/4 v0, 0x0

    .line 229
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onScrollChanged(IIII)V
    .locals 1

    .prologue
    .line 191
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->onScrollChanged(IIII)V

    .line 192
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->dgq:Z

    .line 193
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 207
    iget-boolean v1, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->dgq:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getScrollY()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getScrollX()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getScrollY()I

    move-result v4

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->onScrollChanged(IIII)V

    .line 209
    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->dgo:Lhgg;

    if-eqz v1, :cond_1

    .line 210
    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->dgo:Lhgg;

    invoke-virtual {v1, p1}, Lhgg;->v(Landroid/view/MotionEvent;)V

    .line 213
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 214
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 217
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->isFocused()Z

    move-result v0

    .line 80
    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->onWindowFocusChanged(Z)V

    .line 85
    iget-boolean v1, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->dgp:Z

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->requestFocus()Z

    .line 88
    :cond_0
    if-nez p1, :cond_1

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->dgp:Z

    .line 89
    return-void

    .line 88
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected overScrollBy(IIIIIIIIZ)Z
    .locals 10

    .prologue
    .line 100
    const/4 v0, 0x0

    .line 101
    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->dgo:Lhgg;

    if-eqz v1, :cond_0

    .line 102
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->dgo:Lhgg;

    invoke-virtual {v0, p1, p2}, Lhgg;->bf(II)Z

    move-result v0

    .line 103
    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->dgn:Lejd;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 105
    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->dgn:Lejd;

    invoke-virtual {v1}, Lejd;->atj()V

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->dgn:Lejd;

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    .line 119
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->dgn:Lejd;

    invoke-virtual {v0, p2}, Lejd;->hK(I)I

    move-result v2

    :goto_0
    move-object v0, p0

    move v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    .line 124
    invoke-super/range {v0 .. v9}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->overScrollBy(IIIIIIIIZ)Z

    move-result v0

    return v0

    .line 121
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public scrollTo(II)V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->dgn:Lejd;

    invoke-virtual {v0, p2}, Lejd;->hL(I)I

    move-result v0

    .line 147
    invoke-super {p0, p1, v0}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->scrollTo(II)V

    .line 148
    return-void
.end method

.method public setFocusable(Z)V
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->bB:Z

    if-nez v0, :cond_0

    .line 160
    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->setFocusable(Z)V

    .line 162
    :cond_0
    return-void
.end method

.method public setFocusableInTouchMode(Z)V
    .locals 1

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->bB:Z

    if-nez v0, :cond_0

    .line 167
    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->setFocusableInTouchMode(Z)V

    .line 169
    :cond_0
    return-void
.end method

.method public setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 131
    instance-of v0, p1, Lejd;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 132
    check-cast v0, Lejd;

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->dgn:Lejd;

    .line 134
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 135
    return-void
.end method

.method public setTranslationY(F)V
    .locals 1

    .prologue
    .line 185
    invoke-super {p0, p1}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->setTranslationY(F)V

    .line 186
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->dgq:Z

    .line 187
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 246
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/google/android/velvet/ui/widget/TextScalingWebview;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{translation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getTranslationY()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";scroll="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getScrollY()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/ScrollableWebView;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
