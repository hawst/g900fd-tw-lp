.class public Lcom/google/android/velvet/ui/VelvetIntentDispatcher;
.super Landroid/app/Activity;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 26
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetIntentDispatcher;->getIntent()Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 30
    if-nez v0, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetIntentDispatcher;->finish()V

    .line 48
    :goto_0
    return-void

    .line 34
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 35
    const-string v1, "com.google.android.googlequicksearchbox.MY_REMINDERS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 36
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/sidekick/main/RemindersListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetIntentDispatcher;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 47
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetIntentDispatcher;->finish()V

    goto :goto_0

    .line 37
    :cond_2
    :try_start_2
    const-string v1, "com.google.android.googlequicksearchbox.TRAINING_CLOSET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 38
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetIntentDispatcher;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetIntentDispatcher;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v0, "com.google.android.googlequicksearchbox.EXTRA_TRAINING_CLOSET_QUESTION"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    if-eqz v0, :cond_3

    const-string v3, "com.google.android.apps.sidekick.training.EXTRA_TARGET_QUESTION"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_3
    const-string v0, "com.google.android.apps.sidekick.training.EXTRA_TARGET_ATTRIBUTE"

    const-string v3, "com.google.android.googlequicksearchbox.EXTRA_TRAINING_CLOSET_ATTRIBUTE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_4
    invoke-virtual {p0, v1}, Lcom/google/android/velvet/ui/VelvetIntentDispatcher;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 47
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/VelvetIntentDispatcher;->finish()V

    throw v0

    .line 39
    :cond_5
    :try_start_3
    const-string v1, "com.google.android.googlequicksearchbox.MY_PLACES"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 40
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.sidekick.training.EXTRA_SHOW_PLACES"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetIntentDispatcher;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 41
    :cond_6
    const-string v1, "com.google.android.googlequicksearchbox.TRAFFIC_CARD_SETTINGS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 42
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/search/core/preferences/cards/TrafficCardSettingsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetIntentDispatcher;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 43
    :cond_7
    const-string v1, "com.google.android.handsfree.ENTER_CAR_MODE_VIA_MENU"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lbwx;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "entry-point"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/VelvetIntentDispatcher;->startActivity(Landroid/content/Intent;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1
.end method
