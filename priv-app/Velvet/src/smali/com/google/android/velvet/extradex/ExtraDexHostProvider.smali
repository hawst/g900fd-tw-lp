.class public Lcom/google/android/velvet/extradex/ExtraDexHostProvider;
.super Lgog;
.source "PG"


# instance fields
.field private final cWk:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lgog;-><init>()V

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/velvet/extradex/ExtraDexHostProvider;->cWk:Ljava/util/HashMap;

    .line 48
    return-void
.end method


# virtual methods
.method protected final no(Ljava/lang/String;)Lgoe;
    .locals 3

    .prologue
    .line 32
    iget-object v1, p0, Lcom/google/android/velvet/extradex/ExtraDexHostProvider;->cWk:Ljava/util/HashMap;

    monitor-enter v1

    .line 33
    :try_start_0
    iget-object v0, p0, Lcom/google/android/velvet/extradex/ExtraDexHostProvider;->cWk:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgtv;

    .line 34
    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lgtv;

    invoke-direct {v0, p0, p1}, Lgtv;-><init>(Lcom/google/android/velvet/extradex/ExtraDexHostProvider;Ljava/lang/String;)V

    .line 37
    iget-object v2, p0, Lcom/google/android/velvet/extradex/ExtraDexHostProvider;->cWk:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    :cond_0
    :try_start_1
    iget-object v0, v0, Lgtv;->aWr:Livq;

    invoke-static {v0}, Lgtx;->c(Livq;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgoe;
    :try_end_1
    .catch Lgos; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1

    return-object v0

    .line 41
    :catch_0
    move-exception v0

    .line 43
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
