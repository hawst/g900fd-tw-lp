.class public Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lgzz;


# instance fields
.field private Oq:I

.field private bHK:Landroid/view/View;

.field private bIS:Lcom/google/android/search/searchplate/SearchPlate;

.field private bss:I

.field private dgr:Ldsp;

.field private dgs:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->Oq:I

    .line 60
    return-void
.end method


# virtual methods
.method public final UK()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SearchPlate;->dW(Z)V

    .line 185
    return-void
.end method

.method public final a(ILjava/lang/String;Z)V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/search/searchplate/SearchPlate;->a(ILjava/lang/String;Z)V

    .line 170
    return-void
.end method

.method public final a(Lcom/google/android/search/shared/actions/ParcelableVoiceAction;)V
    .locals 2

    .prologue
    .line 211
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->ahN()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/errors/SearchError;

    .line 212
    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiT()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 213
    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v1, v0}, Lcom/google/android/search/searchplate/SearchPlate;->kg(Ljava/lang/String;)V

    .line 214
    return-void
.end method

.method public final aLD()V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SearchPlate;->dX(Z)V

    .line 180
    return-void
.end method

.method public final aX(Lcom/google/android/shared/search/Query;)V
    .locals 3

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-static {p1}, Leef;->aR(Lcom/google/android/shared/search/Query;)Ldto;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldto;Z)V

    .line 165
    return-void
.end method

.method public final b(Landroid/text/Spanned;)V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/search/searchplate/SearchPlate;->a(Landroid/text/Spanned;Z)V

    .line 175
    return-void
.end method

.method public final b(Lequ;)V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-static {p1}, Leef;->a(Lequ;)Ldtb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldtb;)V

    .line 115
    return-void
.end method

.method public final b(Lgyd;)V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {p1}, Lgyd;->aLE()Ldsj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldsj;)V

    .line 120
    return-void
.end method

.method public final c(IIZ)V
    .locals 1

    .prologue
    .line 126
    iput p1, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->Oq:I

    .line 128
    if-nez p1, :cond_0

    .line 129
    or-int/lit8 p2, p2, 0x4

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/search/searchplate/SearchPlate;->c(IIZ)V

    .line 133
    return-void
.end method

.method public final dN(Z)V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/SearchPlate;->dN(Z)V

    .line 207
    return-void
.end method

.method public final en(I)V
    .locals 1

    .prologue
    .line 146
    iput p1, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bss:I

    .line 147
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/SearchPlate;->en(I)V

    .line 148
    return-void
.end method

.method public final fv(I)V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    .line 219
    return-void
.end method

.method public final kK(I)V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->dgr:Ldsp;

    iget-object v0, v0, Ldsp;->bIO:Ldss;

    invoke-virtual {v0, p1}, Ldss;->setMode(I)V

    .line 141
    return-void
.end method

.method public final n(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/SearchPlate;->n(Ljava/lang/CharSequence;)V

    .line 196
    return-void
.end method

.method public onFinishInflate()V
    .locals 4

    .prologue
    .line 88
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 89
    const v0, 0x7f110491

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->dgs:Landroid/view/View;

    .line 90
    const v0, 0x7f1103b3

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/searchplate/SearchPlate;

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    .line 92
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    new-instance v1, Ledi;

    invoke-virtual {p0}, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Ledi;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldta;)V

    .line 93
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    const v1, 0x7f1103c9

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bHK:Landroid/view/View;

    .line 95
    new-instance v0, Ldsp;

    iget-object v1, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v2, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->dgs:Landroid/view/View;

    invoke-direct {v0, v1, v2}, Ldsp;-><init>(Lcom/google/android/search/searchplate/SearchPlate;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->dgr:Ldsp;

    .line 97
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    const/4 v1, 0x0

    const/4 v2, 0x6

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/search/searchplate/SearchPlate;->c(IIZ)V

    .line 104
    new-instance v0, Lhgi;

    invoke-direct {v0, p0}, Lhgi;-><init>(Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;)V

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 110
    return-void
.end method

.method public final w(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 64
    const-string v0, "SearchPlateFragment.mode"

    iget v1, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->Oq:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 65
    const-string v0, "SearchPlateFragment.recognitionState"

    iget v1, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bss:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 66
    const-string v1, "SearchPlateFragment.musicActionVisible"

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bHK:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bHK:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 68
    return-void

    .line 66
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 72
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    const-string v0, "SearchPlateFragment.musicActionVisible"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bHK:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 77
    :cond_0
    const-string v0, "SearchPlateFragment.mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    const-string v0, "SearchPlateFragment.mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v2, v1}, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->c(IIZ)V

    .line 81
    :cond_1
    const-string v0, "SearchPlateFragment.recognitionState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 82
    const-string v0, "SearchPlateFragment.recognitionState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->en(I)V

    .line 84
    :cond_2
    return-void
.end method

.method public final x(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/search/searchplate/SearchPlate;->x(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    return-void
.end method

.method public final z(Lcom/google/android/shared/search/Query;)V
    .locals 3

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/velvet/ui/widget/VelvetSearchPlate;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-static {p1}, Leef;->aR(Lcom/google/android/shared/search/Query;)Ldto;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldto;Z)V

    .line 160
    return-void
.end method
