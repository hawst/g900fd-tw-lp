.class public Lcom/google/android/speech/alternates/SuggestionSpanBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.speech.NOTIFY_TEXT_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 51
    const-class v1, Lcom/google/android/speech/alternates/SuggestionSpanBroadcastReceiver;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 52
    const-string v1, "com.google.android.speech.REQUEST_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    const-string v1, "com.google.android.speech.SEGMENT_ID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 54
    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 63
    const-string v0, "android.text.style.SUGGESTION_PICKED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "before"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 66
    const-string v1, "after"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 69
    const-string v2, "hashcode"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 76
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v3

    invoke-virtual {v3}, Lgql;->aJq()Lhhq;

    move-result-object v3

    .line 78
    invoke-virtual {v3}, Lhhq;->aPe()Lgma;

    move-result-object v3

    invoke-virtual {v3, v2, v0, v1}, Lgma;->c(ILjava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-static {p1}, Lcom/google/android/voicesearch/logger/EventLoggerService;->bQ(Landroid/content/Context;)V

    .line 83
    :cond_0
    const-string v0, "com.google.android.speech.NOTIFY_TEXT_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    const-string v0, "com.google.android.speech.REQUEST_ID"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 85
    if-nez v1, :cond_2

    .line 86
    const-string v0, "SuggestionSpanBroadcastReceiver"

    const-string v1, "Missing request id"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 123
    :cond_1
    :goto_0
    return-void

    .line 90
    :cond_2
    const-string v0, "com.google.android.speech.SEGMENT_ID"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 91
    if-ne v2, v5, :cond_3

    .line 92
    const-string v0, "SuggestionSpanBroadcastReceiver"

    const-string v1, "Missing segment id"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 96
    :cond_3
    const-string v0, "android.text.style.EXTRA_TEXT_CHANGED_TYPE"

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 97
    if-ne v0, v5, :cond_4

    .line 98
    const-string v0, "SuggestionSpanBroadcastReceiver"

    const-string v1, "Missing changedType"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 103
    :cond_4
    packed-switch v0, :pswitch_data_0

    .line 113
    const-string v1, "SuggestionSpanBroadcastReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown changedType "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 105
    :pswitch_0
    const/16 v0, 0x22

    .line 117
    :goto_1
    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    .line 119
    new-instance v1, Liuf;

    invoke-direct {v1}, Liuf;-><init>()V

    invoke-virtual {v1, v2}, Liuf;->na(I)Liuf;

    move-result-object v1

    iput-object v1, v0, Litu;->dID:Liuf;

    .line 121
    invoke-static {v0}, Lege;->a(Litu;)V

    goto :goto_0

    .line 109
    :pswitch_1
    const/16 v0, 0x10

    .line 110
    goto :goto_1

    .line 103
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
