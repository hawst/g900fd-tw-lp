.class public Lcom/google/android/speech/grammar/GrammarCompilationService;
.super Landroid/app/IntentService;
.source "PG"


# static fields
.field private static cOB:Z


# instance fields
.field private final cOC:Ljava/security/MessageDigest;

.field private mGreco3Container:Lgiw;

.field private mOfflineActionsManager:Lgkg;

.field private mSearchConfig:Lcjs;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 156
    const-string v0, ""

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 159
    :try_start_0
    const-string v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 165
    :goto_0
    iput-object v0, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->cOC:Ljava/security/MessageDigest;

    .line 166
    return-void

    .line 161
    :catch_0
    move-exception v0

    const-string v0, "GrammarCompilationService"

    const-string v1, "MD5 message digests not supported."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 162
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lgjl;)V
    .locals 1

    .prologue
    .line 87
    invoke-static {p0, p1, p2}, Lcom/google/android/speech/grammar/GrammarCompilationService;->b(Landroid/content/Context;Ljava/lang/String;Lgjl;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 88
    return-void
.end method

.method public static declared-synchronized a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Lgjl;J)V
    .locals 8

    .prologue
    .line 125
    const-class v7, Lcom/google/android/speech/grammar/GrammarCompilationService;

    monitor-enter v7

    :try_start_0
    sget-boolean v0, Lcom/google/android/speech/grammar/GrammarCompilationService;->cOB:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/speech/grammar/GrammarCompilationService;->nh(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 148
    :cond_0
    :goto_0
    monitor-exit v7

    return-void

    .line 129
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-static {p1, p2, p3}, Lcom/google/android/speech/grammar/GrammarCompilationService;->b(Landroid/content/Context;Ljava/lang/String;Lgjl;)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x10000000

    invoke-static {p1, v0, v1, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 130
    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 133
    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 138
    const/4 v1, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x1b7740

    add-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v4, p4, v4

    if-lez v4, :cond_2

    move-wide v4, p4

    :goto_1
    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 147
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/speech/grammar/GrammarCompilationService;->cOB:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 125
    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0

    .line 138
    :cond_2
    const-wide/32 v4, 0x240c8400

    goto :goto_1
.end method

.method public static declared-synchronized aHd()Z
    .locals 2

    .prologue
    .line 151
    const-class v0, Lcom/google/android/speech/grammar/GrammarCompilationService;

    monitor-enter v0

    :try_start_0
    sget-boolean v1, Lcom/google/android/speech/grammar/GrammarCompilationService;->cOB:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Lgjl;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 318
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/speech/grammar/GrammarCompilationService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 319
    const-string v1, "compilation_locale"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 320
    const-string v1, "grammar_type"

    invoke-virtual {p2}, Lgjl;->aGv()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 321
    return-object v0
.end method

.method private c(Ljava/lang/String;Lgjl;)Z
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 202
    const-string v0, "GrammarCompilationService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Compiling grammar for: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", type="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 204
    iget-object v0, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mGreco3Container:Lgiw;

    invoke-virtual {v0}, Lgiw;->aGi()Lgjq;

    move-result-object v5

    .line 205
    new-instance v6, Lglk;

    iget-object v0, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mGreco3Container:Lgiw;

    invoke-virtual {v0}, Lgiw;->aGh()Lgix;

    move-result-object v0

    new-instance v4, Lglg;

    invoke-virtual {p0}, Lcom/google/android/speech/grammar/GrammarCompilationService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mSearchConfig:Lcjs;

    invoke-direct {v4, v7, v8}, Lglg;-><init>(Landroid/content/ContentResolver;Lcjs;)V

    new-instance v7, Lgll;

    invoke-virtual {p0}, Lcom/google/android/speech/grammar/GrammarCompilationService;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/speech/grammar/GrammarCompilationService;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lgll;-><init>(Landroid/content/res/Resources;Ljava/lang/String;)V

    invoke-direct {v6, v0, v4, v7, p2}, Lglk;-><init>(Lgix;Lglg;Lgll;Lgjl;)V

    .line 209
    iget-object v0, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mGreco3Container:Lgiw;

    invoke-virtual {v0}, Lgiw;->aGh()Lgix;

    move-result-object v7

    .line 210
    invoke-virtual {v7, v2}, Lgix;->fK(Z)V

    .line 212
    sget-object v0, Lgjo;->cMO:Lgjo;

    invoke-virtual {v7, p1, v0}, Lgix;->a(Ljava/lang/String;Lgjo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    const-string v0, "GrammarCompilationService"

    const-string v1, "No grammar compilation resources, aborting."

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v0, v2

    .line 278
    :goto_0
    return v0

    .line 217
    :cond_0
    invoke-virtual {v7, p1}, Lgix;->mM(Ljava/lang/String;)Lgkd;

    move-result-object v8

    .line 219
    invoke-virtual {v5, p2}, Lgjq;->a(Lgjl;)Ljava/lang/String;

    move-result-object v0

    .line 223
    if-eqz v0, :cond_8

    .line 225
    invoke-virtual {v8, p2, v0}, Lgkd;->c(Lgjl;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/speech/grammar/GrammarCompilationService;->ni(Ljava/lang/String;)[B

    move-result-object v0

    .line 230
    :goto_1
    iget-object v4, v6, Lglk;->mGreco3DataManager:Lgix;

    invoke-virtual {v4}, Lgix;->isInitialized()Z

    move-result v4

    invoke-static {v4}, Lifv;->gY(Z)V

    iget-object v4, v6, Lglk;->mGreco3DataManager:Lgix;

    invoke-virtual {v4, p1}, Lgix;->mM(Ljava/lang/String;)Lgkd;

    move-result-object v4

    if-nez v4, :cond_1

    const-string v4, "HandsFreeGrammarCompiler"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Grammar compilation failed, no resources for locale :"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-array v10, v2, [Ljava/lang/Object;

    invoke-static {v4, v9, v10}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v4, v1

    .line 231
    :goto_2
    if-nez v4, :cond_2

    move v0, v2

    .line 233
    goto :goto_0

    .line 230
    :cond_1
    invoke-virtual {v6, v4}, Lglk;->a(Lgkd;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 236
    :cond_2
    iget-object v9, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->cOC:Ljava/security/MessageDigest;

    if-nez v9, :cond_3

    .line 242
    :goto_3
    if-eqz v1, :cond_4

    invoke-static {v1, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v3

    .line 243
    goto :goto_0

    .line 236
    :cond_3
    iget-object v1, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->cOC:Ljava/security/MessageDigest;

    invoke-virtual {v1}, Ljava/security/MessageDigest;->reset()V

    const-string v1, "UTF-8"

    invoke-static {v1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    iget-object v9, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->cOC:Ljava/security/MessageDigest;

    invoke-virtual {v9, v1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    goto :goto_3

    .line 246
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v9, "v"

    invoke-direct {v0, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 249
    invoke-virtual {v7, p2, p1}, Lgix;->a(Lgjl;Ljava/lang/String;)Ljava/io/File;

    move-result-object v9

    .line 250
    invoke-virtual {v7, p2, p1, v0}, Lgix;->a(Lgjl;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v10

    .line 252
    if-eqz v10, :cond_5

    if-nez v9, :cond_6

    .line 253
    :cond_5
    const-string v0, "GrammarCompilationService"

    const-string v1, "Unable to create output directories: dir is null"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v0, v2

    .line 254
    goto/16 :goto_0

    .line 258
    :cond_6
    invoke-virtual {v6, v4, p1, v10, v9}, Lglk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/io/File;)Z

    move-result v4

    if-nez v4, :cond_7

    move v0, v2

    .line 259
    goto/16 :goto_0

    .line 264
    :cond_7
    :try_start_0
    new-instance v4, Ljava/io/File;

    const-string v6, "digest"

    invoke-direct {v4, v10, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v1, v4}, Lisr;->a([BLjava/io/File;)V

    .line 265
    invoke-virtual {v8}, Lgkd;->aGC()Ljzp;

    move-result-object v1

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    new-instance v4, Ljava/io/File;

    const-string v6, "metadata"

    invoke-direct {v4, v10, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v1, v4}, Lisr;->a([BLjava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    invoke-virtual {v5, p2, v0}, Lgjq;->b(Lgjl;Ljava/lang/String;)V

    .line 276
    invoke-virtual {v7, v3}, Lgix;->fK(Z)V

    move v0, v3

    .line 278
    goto/16 :goto_0

    .line 267
    :catch_0
    move-exception v0

    .line 268
    const-string v1, "GrammarCompilationService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error writing compiled digest/metadata :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v0, v2

    .line 269
    goto/16 :goto_0

    :cond_8
    move-object v0, v1

    goto/16 :goto_1
.end method

.method private static nh(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 95
    if-nez p0, :cond_1

    .line 118
    :cond_0
    :goto_0
    return v0

    .line 98
    :cond_1
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 105
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_2

    .line 106
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid revisionId:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", negative."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :catch_0
    move-exception v0

    .line 102
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid revisionId:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 114
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v6, 0x2932e000

    sub-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 118
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static ni(Ljava/lang/String;)[B
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 297
    if-eqz p0, :cond_0

    .line 299
    :try_start_0
    new-instance v1, Ljava/io/File;

    const-string v2, "digest"

    invoke-direct {v1, p0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lisr;->e(Ljava/io/File;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 305
    :cond_0
    :goto_0
    return-object v0

    .line 301
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public onCreate()V
    .locals 2

    .prologue
    .line 170
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 172
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    .line 173
    invoke-virtual {v0}, Lhhq;->aOV()Lgiw;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mGreco3Container:Lgiw;

    .line 174
    iget-object v1, v0, Lhhq;->mSettings:Lhym;

    .line 175
    invoke-virtual {v0}, Lhhq;->aPk()Lgkg;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mOfflineActionsManager:Lgkg;

    .line 176
    iget-object v0, v0, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DD()Lcjs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mSearchConfig:Lcjs;

    .line 177
    return-void
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 181
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 182
    invoke-static {}, Lgju;->aGA()V

    .line 184
    const-string v0, "compilation_locale"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 185
    const-string v1, "grammar_type"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lgjl;->mT(Ljava/lang/String;)Lgjl;

    move-result-object v2

    .line 188
    iget-object v1, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mOfflineActionsManager:Lgkg;

    invoke-virtual {v1}, Lgkg;->aGT()V

    .line 189
    const/4 v1, 0x0

    .line 191
    const/16 v3, 0x14d

    :try_start_0
    invoke-static {v3}, Lege;->ht(I)V

    .line 193
    invoke-direct {p0, v0, v2}, Lcom/google/android/speech/grammar/GrammarCompilationService;->c(Ljava/lang/String;Lgjl;)Z

    move-result v1

    .line 194
    const/16 v0, 0x14e

    invoke-static {v0}, Lege;->ht(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    iget-object v0, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mOfflineActionsManager:Lgkg;

    invoke-virtual {v0, v2, v1}, Lgkg;->a(Lgjl;Z)V

    .line 198
    return-void

    .line 197
    :catchall_0
    move-exception v0

    iget-object v3, p0, Lcom/google/android/speech/grammar/GrammarCompilationService;->mOfflineActionsManager:Lgkg;

    invoke-virtual {v3, v2, v1}, Lgkg;->a(Lgjl;Z)V

    throw v0
.end method
