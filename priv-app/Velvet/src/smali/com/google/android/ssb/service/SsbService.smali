.class public Lcom/google/android/ssb/service/SsbService;
.super Landroid/app/Service;
.source "PG"


# static fields
.field private static final cQS:Lgoa;


# instance fields
.field private final cQT:Landroid/os/Messenger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lgoa;

    invoke-direct {v0}, Lgoa;-><init>()V

    sput-object v0, Lcom/google/android/ssb/service/SsbService;->cQS:Lgoa;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 40
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lgnz;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lgnz;-><init>(Lcom/google/android/ssb/service/SsbService;B)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/ssb/service/SsbService;->cQT:Landroid/os/Messenger;

    .line 42
    return-void
.end method

.method public static a(Landroid/content/pm/PackageManager;ILandroid/os/Message;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 129
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    .line 130
    const-string v0, "ssb_service:ssb_package_name"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 131
    invoke-virtual {p0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v6

    .line 133
    if-eqz v6, :cond_4

    array-length v0, v6

    if-lez v0, :cond_4

    .line 134
    array-length v7, v6

    move v3, v2

    move v0, v2

    :goto_0
    if-ge v3, v7, :cond_1

    aget-object v8, v6, v3

    .line 135
    invoke-static {v5, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    move v0, v1

    .line 134
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 141
    :cond_1
    array-length v3, v6

    if-ne v3, v1, :cond_3

    .line 142
    const-string v0, "ssb_service:ssb_package_name"

    aget-object v3, v6, v2

    invoke-virtual {v4, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :goto_1
    if-eqz v1, :cond_2

    .line 147
    const-string v0, "ssb_service:ssb_package_name"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 148
    invoke-static {p0, v0}, Lbgt;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    .line 150
    const-string v1, "ssb_service:ssb_package_is_google"

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 158
    :goto_2
    return-void

    .line 154
    :cond_2
    const-string v0, "SsbService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Potential package name unavailable: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 155
    const-string v0, "ssb_service:ssb_package_name"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 156
    const-string v0, "ssb_service:ssb_package_is_google"

    invoke-virtual {v4, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_2

    :cond_3
    move v1, v0

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method public static synthetic aIm()Lgoa;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/ssb/service/SsbService;->cQS:Lgoa;

    return-object v0
.end method

.method public static fN(Z)V
    .locals 1

    .prologue
    .line 118
    invoke-static {}, Lenu;->auR()V

    .line 119
    sget-object v0, Lcom/google/android/ssb/service/SsbService;->cQS:Lgoa;

    invoke-virtual {v0, p0}, Lgoa;->fN(Z)V

    .line 120
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 106
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 107
    new-instance v0, Letj;

    invoke-virtual {p0}, Lcom/google/android/ssb/service/SsbService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Letj;-><init>(Landroid/content/res/Resources;)V

    .line 108
    const-string v1, "SsbService"

    invoke-virtual {v0, v1}, Letj;->lt(Ljava/lang/String;)V

    .line 109
    sget-object v1, Lcom/google/android/ssb/service/SsbService;->cQS:Lgoa;

    invoke-virtual {v0, v1}, Letj;->b(Leti;)V

    .line 110
    invoke-virtual {v0, p2}, Letj;->c(Ljava/io/PrintWriter;)V

    .line 111
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/ssb/service/SsbService;->cQT:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 79
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 80
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    .line 81
    sget-object v1, Lcom/google/android/ssb/service/SsbService;->cQS:Lgoa;

    invoke-virtual {v0}, Lcfo;->Eq()Lebk;

    move-result-object v2

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v3

    invoke-virtual {v0}, Lcfo;->DC()Lemp;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lgoa;->a(Lebk;Lcke;Lemp;)V

    .line 83
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/google/android/ssb/service/SsbService;->cQS:Lgoa;

    invoke-virtual {v0}, Lgoa;->destroy()V

    .line 101
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 102
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 94
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
