.class public Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;
.super Landroid/widget/TextView;
.source "PG"


# instance fields
.field private cAa:Z

.field private cAb:Landroid/text/SpannableStringBuilder;

.field private cAc:F

.field private cAd:F

.field private czZ:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 58
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAc:F

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAd:F

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAc:F

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAd:F

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAc:F

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAd:F

    .line 71
    return-void
.end method

.method private L(Ljava/lang/CharSequence;)Landroid/text/Layout;
    .locals 8

    .prologue
    .line 162
    new-instance v0, Landroid/text/StaticLayout;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v1, v3

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->getPaddingRight()I

    move-result v3

    sub-int v3, v1, v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iget v5, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAc:F

    iget v6, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAd:F

    const/4 v7, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    return-object v0
.end method

.method private static a(Landroid/text/SpannableStringBuilder;I)Landroid/text/SpannableStringBuilder;
    .locals 4

    .prologue
    .line 150
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 151
    sub-int v1, v0, p1

    invoke-virtual {p0, v1, v0}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    .line 152
    sub-int/2addr v0, p1

    .line 154
    :goto_0
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_0

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_0

    .line 155
    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    .line 156
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 158
    :cond_0
    return-object v1
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v3, -0x1

    const/4 v5, 0x0

    .line 97
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->czZ:Z

    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->getPaddingRight()I

    move-result v2

    add-int/2addr v2, v0

    if-ge v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->getId()I

    move-result v0

    if-ne v0, v3, :cond_1

    const-string v0, "NO_ID"

    :goto_0
    const-string v3, "EllipsizingTextView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Inconsistent layout state for TextView(id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "). textLength="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAb:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", width="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", padding="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    :cond_0
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 101
    return-void

    .line 98
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->getMaxLines()I

    move-result v1

    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAb:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAb:Landroid/text/SpannableStringBuilder;

    if-eq v1, v3, :cond_4

    invoke-direct {p0, v2}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->L(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/text/Layout;->getLineCount()I

    move-result v4

    if-le v4, v1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0a0421

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v0

    invoke-virtual {v2, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->L(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    if-le v2, v1, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAb:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sub-int v0, v1, v0

    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAb:Landroid/text/SpannableStringBuilder;

    invoke-static {v1, v0}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->a(Landroid/text/SpannableStringBuilder;I)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAa:Z

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-boolean v5, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAa:Z

    :cond_5
    iput-boolean v5, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->czZ:Z

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    iput-boolean v5, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAa:Z

    throw v0
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 89
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAa:Z

    if-nez v0, :cond_0

    .line 90
    invoke-static {p1}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAb:Landroid/text/SpannableStringBuilder;

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->czZ:Z

    .line 93
    :cond_0
    return-void
.end method

.method public setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
    .locals 0

    .prologue
    .line 171
    return-void
.end method

.method public setLineSpacing(FF)V
    .locals 0

    .prologue
    .line 81
    iput p1, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAd:F

    .line 82
    iput p2, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->cAc:F

    .line 83
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setLineSpacing(FF)V

    .line 84
    return-void
.end method

.method public setMaxLines(I)V
    .locals 1

    .prologue
    .line 75
    invoke-super {p0, p1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/ui/EllipsizingTextView;->czZ:Z

    .line 77
    return-void
.end method
