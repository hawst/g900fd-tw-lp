.class public Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;
.super Landroid/view/View;
.source "PG"


# instance fields
.field private cAp:I

.field private cAq:I

.field private final cAr:I

.field private final cAs:I

.field private final cAt:Landroid/graphics/Paint;

.field private final cAu:Landroid/graphics/Paint;

.field private final pi:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 20
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAp:I

    .line 25
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAt:Landroid/graphics/Paint;

    .line 26
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAu:Landroid/graphics/Paint;

    .line 30
    iput v1, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAr:I

    .line 31
    iput v1, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAs:I

    .line 32
    iput v1, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->pi:I

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    iput v5, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAp:I

    .line 25
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAt:Landroid/graphics/Paint;

    .line 26
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAu:Landroid/graphics/Paint;

    .line 41
    sget-object v2, Lbwe;->aLV:[I

    const v3, 0x7f090111

    invoke-virtual {p1, p2, v2, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 44
    invoke-virtual {v2, v0, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAr:I

    .line 45
    invoke-virtual {v2, v1, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAs:I

    .line 47
    iget-object v3, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAt:Landroid/graphics/Paint;

    const/4 v4, 0x2

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 49
    iget-object v3, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAt:Landroid/graphics/Paint;

    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 50
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->pi:I

    .line 52
    iget v3, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->pi:I

    iget v4, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAr:I

    if-lt v3, v4, :cond_0

    move v0, v1

    :cond_0
    const-string v3, "Selected dot radius assumed >= radius of unselected for measurement and layout."

    invoke-static {v0, v3}, Lifv;->d(ZLjava/lang/Object;)V

    .line 54
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAu:Landroid/graphics/Paint;

    const/4 v3, 0x4

    const/high16 v4, -0x1000000

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 56
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAu:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 57
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 58
    return-void
.end method

.method private aCU()I
    .locals 3

    .prologue
    .line 89
    iget v0, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAr:I

    mul-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAq:I

    add-int/lit8 v1, v1, -0x1

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->pi:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAs:I

    iget v2, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAq:I

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method private static aY(II)I
    .locals 1

    .prologue
    .line 78
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 84
    :goto_0
    return p1

    .line 80
    :sswitch_0
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    goto :goto_0

    .line 82
    :sswitch_1
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto :goto_0

    .line 78
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final jp(I)V
    .locals 0

    .prologue
    .line 61
    iput p1, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAp:I

    .line 62
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->invalidate()V

    .line 63
    return-void
.end method

.method public final jq(I)V
    .locals 0

    .prologue
    .line 66
    iput p1, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAq:I

    .line 67
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->requestLayout()V

    .line 68
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 98
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->getWidth()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->aCU()I

    move-result v2

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->pi:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 102
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->aCU()I

    move-result v6

    move v0, v1

    move v2, v1

    .line 104
    :goto_0
    iget v3, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAq:I

    if-ge v0, v3, :cond_4

    .line 105
    iget v3, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAp:I

    if-ne v0, v3, :cond_1

    const/4 v3, 0x1

    move v5, v3

    .line 106
    :goto_1
    if-eqz v5, :cond_2

    iget v3, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->pi:I

    .line 108
    :goto_2
    add-int v4, v2, v3

    .line 110
    invoke-static {p0}, Leot;->ay(Landroid/view/View;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 111
    sub-int v4, v6, v4

    .line 113
    :cond_0
    int-to-float v7, v4

    iget v4, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->pi:I

    int-to-float v8, v4

    int-to-float v9, v3

    if-eqz v5, :cond_3

    iget-object v4, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAu:Landroid/graphics/Paint;

    :goto_3
    invoke-virtual {p1, v7, v8, v9, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 118
    mul-int/lit8 v3, v3, 0x2

    iget v4, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAs:I

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v5, v1

    .line 105
    goto :goto_1

    .line 106
    :cond_2
    iget v3, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAr:I

    goto :goto_2

    .line 113
    :cond_3
    iget-object v4, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->cAt:Landroid/graphics/Paint;

    goto :goto_3

    .line 120
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 121
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->aCU()I

    move-result v0

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->aY(II)I

    move-result v0

    iget v1, p0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->pi:I

    mul-int/lit8 v1, v1, 0x2

    invoke-static {p2, v1}, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->aY(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->setMeasuredDimension(II)V

    .line 75
    return-void
.end method
