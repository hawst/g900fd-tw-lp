.class public Lcom/google/android/sidekick/gel/GelNavigationDrawerLayout;
.super Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;
.source "PG"

# interfaces
.implements Lwm;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;-><init>(Landroid/content/Context;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method


# virtual methods
.method protected final atF()V
    .locals 4

    .prologue
    .line 43
    invoke-super {p0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->atF()V

    .line 46
    sget v0, Lesp;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/google/android/sidekick/gel/GelNavigationDrawerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d02af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 50
    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/sidekick/gel/GelNavigationDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 51
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 53
    const/16 v0, 0x50

    iput v0, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 54
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 55
    const v0, 0x7f02025b

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 56
    iget-object v0, p0, Lcom/google/android/sidekick/gel/GelNavigationDrawerLayout;->cbE:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 58
    :cond_0
    return-void
.end method

.method public final b(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->b(Landroid/graphics/Rect;)V

    .line 64
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/sidekick/gel/GelNavigationDrawerLayout;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 65
    invoke-virtual {p0, v1}, Lcom/google/android/sidekick/gel/GelNavigationDrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 66
    instance-of v2, v0, Lwm;

    if-eqz v2, :cond_0

    .line 67
    check-cast v0, Lwm;

    invoke-interface {v0, p1}, Lwm;->b(Landroid/graphics/Rect;)V

    .line 64
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 70
    :cond_1
    return-void
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Landroid/support/v4/widget/DrawerLayout$SavedState;

    sget-object v1, Landroid/view/View$BaseSavedState;->EMPTY_STATE:Landroid/view/AbsSavedState;

    invoke-direct {v0, v1}, Landroid/support/v4/widget/DrawerLayout$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 38
    invoke-super {p0, v0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 39
    return-void
.end method
