.class public Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bpu:Landroid/os/PowerManager;

.field private final cpM:Lerp;

.field private final mAlarmHelper:Ldjx;

.field private final mClock:Lemp;

.field private final mContext:Landroid/content/Context;

.field private final mGmsLocationReportingHelper:Leue;

.field private final mLoginHelper:Lcrh;

.field private final mMainPreferencesSupplier:Ligi;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 69
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Success"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Package or certificate invalid"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Different user signed in to GMM"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "No user signed in to GMM"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Unsupported version"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Invalid request"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Server error: failed to opt-in"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Cannot log into non-primary account"

    aput-object v2, v0, v1

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ligi;Lemp;Ldjx;Lcrh;Landroid/os/PowerManager;Leue;Lerp;)V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mContext:Landroid/content/Context;

    .line 102
    iput-object p2, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mMainPreferencesSupplier:Ligi;

    .line 103
    iput-object p3, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mClock:Lemp;

    .line 104
    iput-object p4, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mAlarmHelper:Ldjx;

    .line 105
    iput-object p5, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mLoginHelper:Lcrh;

    .line 106
    iput-object p6, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->bpu:Landroid/os/PowerManager;

    .line 107
    iput-object p7, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mGmsLocationReportingHelper:Leue;

    .line 108
    iput-object p8, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->cpM:Lerp;

    .line 109
    return-void
.end method


# virtual methods
.method public final H(Landroid/accounts/Account;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 179
    const-string v0, "gmm_location_reporting_opt_in_handler"

    invoke-virtual {p0, v5, v0}, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->k(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 181
    const-wide/16 v2, 0x3afc

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 183
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->ayM()Landroid/app/PendingIntent;

    move-result-object v1

    .line 184
    new-instance v2, Lfen;

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3, v0, v1, p0}, Lfen;-><init>(Landroid/os/Looper;Landroid/os/PowerManager$WakeLock;Landroid/app/PendingIntent;Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;)V

    .line 187
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.apps.maps.googlenav.friend.internal.OPT_IN"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 188
    const-string v3, "com.google.android.apps.maps"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 189
    const/high16 v3, 0x10000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 190
    const-string v3, "account"

    invoke-virtual {v0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 191
    const-string v3, "messenger"

    new-instance v4, Landroid/os/Messenger;

    invoke-direct {v4, v2}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 192
    const-string v3, "sender"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 193
    const-string v1, "version"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 196
    :try_start_0
    iget-object v1, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    :goto_0
    const/4 v0, -0x1

    const-wide/16 v4, 0x3a98

    invoke-virtual {v2, v0, v4, v5}, Lfen;->sendEmptyMessageDelayed(IJ)Z

    .line 203
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final I(Landroid/accounts/Account;)V
    .locals 4

    .prologue
    .line 211
    const/4 v0, 0x1

    const-string v1, "gms_location_reporting_opt_in_handler"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->k(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 213
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 214
    const-wide/16 v2, 0x3afc

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 216
    iget-object v1, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mGmsLocationReportingHelper:Leue;

    invoke-virtual {v1, p1}, Leue;->B(Landroid/accounts/Account;)Lcgs;

    move-result-object v1

    .line 218
    new-instance v2, Lfem;

    invoke-direct {v2, p0, v0}, Lfem;-><init>(Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;Landroid/os/PowerManager$WakeLock;)V

    iget-object v0, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->cpM:Lerp;

    const-string v3, "handleReportingOptInResult"

    invoke-virtual {v1, v2, v0, v3}, Lcgs;->a(Lemy;Lerp;Ljava/lang/String;)V

    .line 234
    return-void
.end method

.method public final ayI()V
    .locals 7

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    .line 119
    iget-object v0, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mMainPreferencesSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 120
    const-string v1, "location_opt_in_start_time"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 121
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 123
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->ayL()Landroid/app/PendingIntent;

    move-result-object v6

    .line 124
    iget-object v0, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mAlarmHelper:Ldjx;

    const/4 v1, 0x0

    const-wide/32 v4, 0x36ee80

    invoke-virtual/range {v0 .. v6}, Ldjx;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 126
    return-void
.end method

.method protected final ayJ()V
    .locals 4

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mMainPreferencesSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    const-string v1, "location_opt_in_start_time"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 138
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->ayK()V

    .line 169
    :goto_1
    return-void

    .line 135
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    .line 143
    if-nez v0, :cond_2

    .line 144
    const-string v0, "LocationReportingOptInHelper"

    const-string v1, "No account to opt-in"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 148
    :cond_2
    iget-object v1, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mGmsLocationReportingHelper:Leue;

    iget-object v2, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mLoginHelper:Lcrh;

    invoke-virtual {v2}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v1, v2}, Leue;->C(Landroid/accounts/Account;)Lcgs;

    move-result-object v1

    .line 151
    new-instance v2, Lfel;

    invoke-direct {v2, p0, v0}, Lfel;-><init>(Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;Landroid/accounts/Account;)V

    iget-object v0, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->cpM:Lerp;

    const-string v3, "handleReportingStateForOptIn"

    invoke-virtual {v1, v2, v0, v3}, Lcgs;->a(Lemy;Lerp;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final ayK()V
    .locals 2

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->ayL()Landroid/app/PendingIntent;

    move-result-object v0

    .line 257
    iget-object v1, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mAlarmHelper:Ldjx;

    invoke-virtual {v1, v0}, Ldjx;->cancel(Landroid/app/PendingIntent;)V

    .line 258
    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 260
    iget-object v0, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mMainPreferencesSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 261
    const-string v1, "location_opt_in_start_time"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 262
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 263
    return-void
.end method

.method protected ayL()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 276
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mContext:Landroid/content/Context;

    const-class v2, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper$LocationReportingOptInRetryReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 277
    iget-object v1, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mContext:Landroid/content/Context;

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 278
    return-object v0
.end method

.method protected ayM()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "identity"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method protected getLooper()Landroid/os/Looper;
    .locals 1

    .prologue
    .line 268
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    .line 269
    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    return-object v0
.end method

.method protected k(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;->bpu:Landroid/os/PowerManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    return-object v0
.end method
