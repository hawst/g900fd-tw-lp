.class public Lcom/google/android/sidekick/main/remoteservice/CurrentAccountActivity;
.super Landroid/app/Activity;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 28
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v1

    .line 34
    invoke-virtual {v0}, Lcfo;->DF()Lcin;

    move-result-object v2

    .line 36
    invoke-virtual {v1}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    .line 37
    invoke-interface {v2, v0}, Lcin;->e(Landroid/accounts/Account;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 39
    const/4 v0, 0x0

    .line 43
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 44
    const-string v2, "account"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 45
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/sidekick/main/remoteservice/CurrentAccountActivity;->setResult(ILandroid/content/Intent;)V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/remoteservice/CurrentAccountActivity;->finish()V

    .line 47
    return-void
.end method
