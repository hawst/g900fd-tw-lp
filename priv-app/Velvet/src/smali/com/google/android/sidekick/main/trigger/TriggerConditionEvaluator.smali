.class public final Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final csY:Lfiu;

.field private final csZ:J

.field private final cta:Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;

.field private final mLocation:Landroid/location/Location;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/location/Location;JLcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;)V
    .locals 2
    .param p1    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Lfiu;

    invoke-direct {v0, p1}, Lfiu;-><init>(Landroid/location/Location;)V

    iput-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    .line 68
    iput-object p1, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->mLocation:Landroid/location/Location;

    .line 69
    iput-wide p2, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csZ:J

    .line 70
    iput-object p4, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->cta:Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;

    .line 74
    return-void
.end method

.method public static a(Ljie;Landroid/location/Location;J)Z
    .locals 3
    .param p1    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 104
    const-wide/16 v0, 0xa

    new-instance v2, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;

    invoke-direct {v2}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;-><init>()V

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->b(Landroid/location/Location;JLcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;)Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->c(Ljie;)Z

    move-result v0

    return v0
.end method

.method private a(Ljie;Ljbp;IZ)Z
    .locals 6
    .param p1    # Ljie;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 270
    new-instance v2, Landroid/location/Location;

    const-string v3, ""

    invoke-direct {v2, v3}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljbp;->mR()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/location/Location;->setLatitude(D)V

    invoke-virtual {p2}, Ljbp;->mS()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/location/Location;->setLongitude(D)V

    iget-object v3, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->mLocation:Landroid/location/Location;

    invoke-virtual {v3, v2}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v2

    invoke-virtual {p2}, Ljbp;->bfc()D

    move-result-wide v4

    double-to-float v3, v4

    sub-float v3, v2, v3

    .line 271
    const/4 v2, 0x0

    cmpg-float v2, v3, v2

    if-gtz v2, :cond_3

    move v2, v1

    .line 272
    :goto_0
    if-nez p4, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 283
    :cond_1
    if-ne p3, v1, :cond_5

    .line 284
    if-nez v2, :cond_2

    .line 285
    iget-object v1, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    iget-object v1, v1, Lfiu;->ctj:Ljava/lang/Float;

    if-nez v1, :cond_4

    .line 286
    iget-object v1, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v1, Lfiu;->ctj:Ljava/lang/Float;

    .line 292
    :goto_1
    if-eqz p1, :cond_2

    .line 293
    iget-object v1, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    iget-object v1, v1, Lfiu;->ctk:Ljava/util/List;

    new-instance v2, Lfit;

    invoke-direct {v2, p1, p2, p3, v3}, Lfit;-><init>(Ljie;Ljbp;IF)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 316
    :cond_2
    :goto_2
    return v0

    :cond_3
    move v2, v0

    .line 271
    goto :goto_0

    .line 288
    :cond_4
    iget-object v1, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    iget-object v2, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    iget-object v2, v2, Lfiu;->ctj:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v1, Lfiu;->ctj:Ljava/lang/Float;

    goto :goto_1

    .line 299
    :cond_5
    neg-float v1, v3

    .line 300
    if-eqz v2, :cond_2

    .line 301
    iget-object v2, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    iget-object v2, v2, Lfiu;->ctj:Ljava/lang/Float;

    if-nez v2, :cond_6

    .line 302
    iget-object v2, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v2, Lfiu;->ctj:Ljava/lang/Float;

    .line 308
    :goto_3
    if-eqz p1, :cond_2

    .line 309
    iget-object v1, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    iget-object v1, v1, Lfiu;->ctk:Ljava/util/List;

    new-instance v2, Lfit;

    invoke-direct {v2, p1, p2, p3, v3}, Lfit;-><init>(Ljie;Ljbp;IF)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 304
    :cond_6
    iget-object v2, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    iget-object v4, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    iget-object v4, v4, Lfiu;->ctj:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-static {v4, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v2, Lfiu;->ctj:Ljava/lang/Float;

    goto :goto_3
.end method

.method public static b(Landroid/location/Location;JLcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;)Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;
    .locals 1
    .param p0    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 83
    new-instance v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;-><init>(Landroid/location/Location;JLcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;)V

    return-object v0
.end method

.method public static b(Ljbp;)Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v7, 0x10

    const/4 v6, 0x0

    .line 238
    invoke-virtual {p0}, Ljbp;->oK()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 239
    invoke-virtual {p0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    .line 246
    :goto_0
    invoke-static {}, Lish;->aYu()Lisf;

    move-result-object v1

    invoke-interface {v1, v0}, Lisf;->R(Ljava/lang/CharSequence;)Lisb;

    move-result-object v1

    invoke-virtual {v1}, Lisb;->aYr()J

    move-result-wide v2

    .line 247
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v7, :cond_0

    .line 248
    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 251
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x3a

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 240
    :cond_1
    invoke-virtual {p0}, Ljbp;->bar()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 241
    invoke-virtual {p0}, Ljbp;->baq()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 243
    :cond_2
    const-string v0, "%.4f,%.4f"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljbp;->mR()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v6

    const/4 v2, 0x1

    invoke-virtual {p0}, Ljbp;->mS()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private bm(J)V
    .locals 5

    .prologue
    .line 349
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    iget-object v0, v0, Lfiu;->cti:Ljava/lang/Long;

    if-nez v0, :cond_1

    .line 351
    iget-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lfiu;->cti:Ljava/lang/Long;

    .line 357
    :cond_0
    :goto_0
    return-void

    .line 353
    :cond_1
    iget-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    iget-object v1, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    iget-object v1, v1, Lfiu;->cti:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lfiu;->cti:Ljava/lang/Long;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljie;I)Z
    .locals 22

    .prologue
    .line 143
    move-object/from16 v0, p1

    iget-object v9, v0, Ljie;->end:[I

    .line 147
    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    move/from16 v0, p2

    if-ne v0, v7, :cond_7

    invoke-virtual/range {p1 .. p1}, Ljie;->bkv()Z

    move-result v7

    if-eqz v7, :cond_0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csZ:J

    invoke-virtual/range {p1 .. p1}, Ljie;->bmV()J

    move-result-wide v10

    cmp-long v6, v6, v10

    if-ltz v6, :cond_6

    const/4 v6, 0x1

    :goto_0
    if-nez v6, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljie;->bmV()J

    move-result-wide v4

    :cond_0
    :goto_1
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->bm(J)V

    .line 150
    const/16 v4, 0xb

    invoke-static {v9, v4}, Lius;->d([II)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->cta:Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;

    iget-object v4, v4, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;->ctc:Lifq;

    invoke-virtual {v4}, Lifq;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 152
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->cta:Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;

    iget-object v4, v4, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;->ctc:Lifq;

    invoke-virtual {v4}, Lifq;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 153
    move-object/from16 v0, p1

    iget-object v7, v0, Ljie;->enh:[Liyz;

    array-length v8, v7

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v8, :cond_1

    aget-object v10, v7, v4

    .line 154
    invoke-virtual {v10}, Liyz;->getType()I

    move-result v10

    if-ne v10, v5, :cond_9

    .line 155
    const/4 v6, 0x1

    .line 161
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->mLocation:Landroid/location/Location;

    if-eqz v4, :cond_19

    .line 162
    const/4 v7, 0x0

    .line 163
    const/4 v4, 0x0

    .line 166
    const/4 v5, 0x7

    invoke-static {v9, v5}, Lius;->d([II)Z

    move-result v5

    .line 167
    const/16 v8, 0xa

    invoke-static {v9, v8}, Lius;->d([II)Z

    move-result v10

    .line 168
    if-nez v5, :cond_2

    if-eqz v10, :cond_1a

    .line 169
    :cond_2
    const/4 v5, 0x1

    .line 170
    move-object/from16 v0, p1

    iget-object v11, v0, Ljie;->eaW:[Ljbp;

    array-length v12, v11

    const/4 v4, 0x0

    move v8, v4

    :goto_3
    if-ge v8, v12, :cond_c

    aget-object v13, v11, v8

    .line 171
    invoke-static {v13}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->b(Ljbp;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->cta:Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;

    iget-object v4, v4, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;->ctb:Lijj;

    invoke-virtual {v4}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    :goto_4
    if-eqz v4, :cond_b

    .line 172
    const/4 v4, 0x1

    move/from16 v0, p2

    if-ne v0, v4, :cond_4

    .line 173
    const/4 v7, 0x1

    .line 178
    :cond_4
    :goto_5
    if-eqz v10, :cond_5

    .line 179
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    const/4 v13, 0x1

    iput-boolean v13, v4, Lfiu;->ctg:Z

    .line 170
    :cond_5
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_3

    .line 147
    :cond_6
    const/4 v6, 0x0

    goto/16 :goto_0

    :cond_7
    invoke-virtual/range {p1 .. p1}, Ljie;->bmX()Z

    move-result v7

    if-eqz v7, :cond_0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csZ:J

    invoke-virtual/range {p1 .. p1}, Ljie;->bmW()J

    move-result-wide v10

    cmp-long v6, v6, v10

    if-ltz v6, :cond_8

    const/4 v6, 0x1

    :goto_6
    if-nez v6, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljie;->bmW()J

    move-result-wide v4

    goto/16 :goto_1

    :cond_8
    const/4 v6, 0x0

    goto :goto_6

    .line 153
    :cond_9
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    .line 171
    :cond_a
    const/4 v4, 0x0

    goto :goto_4

    .line 176
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v1, v13, v2, v7}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->a(Ljie;Ljbp;IZ)Z

    move-result v7

    goto :goto_5

    :cond_c
    move v4, v5

    move v5, v7

    .line 185
    :goto_7
    const/16 v7, 0x8

    invoke-static {v9, v7}, Lius;->d([II)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 186
    const/4 v7, 0x1

    .line 187
    new-instance v10, Ljbp;

    invoke-direct {v10}, Ljbp;-><init>()V

    .line 188
    move-object/from16 v0, p1

    iget-object v11, v0, Ljie;->ene:[Lize;

    array-length v12, v11

    const/4 v4, 0x0

    move v8, v4

    :goto_8
    if-ge v8, v12, :cond_f

    aget-object v4, v11, v8

    .line 189
    iget-object v13, v4, Lize;->dRL:[Lizf;

    array-length v14, v13

    const/4 v4, 0x0

    move v9, v4

    :goto_9
    if-ge v9, v14, :cond_e

    aget-object v15, v13, v9

    .line 190
    iget-object v0, v15, Lizf;->dRO:[I

    move-object/from16 v16, v0

    .line 191
    const/4 v4, 0x0

    :goto_a
    add-int/lit8 v17, v4, 0x1

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_d

    .line 192
    aget v17, v16, v4

    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide v20, 0x3e7ad7f29abcaf48L    # 1.0E-7

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    invoke-virtual {v10, v0, v1}, Ljbp;->n(D)Ljbp;

    move-result-object v17

    add-int/lit8 v18, v4, 0x1

    aget v18, v16, v18

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide v20, 0x3e7ad7f29abcaf48L    # 1.0E-7

    mul-double v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Ljbp;->o(D)Ljbp;

    move-result-object v17

    invoke-virtual {v15}, Lizf;->bcM()I

    move-result v18

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-virtual/range {v17 .. v19}, Ljbp;->p(D)Ljbp;

    .line 195
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, p2

    invoke-direct {v0, v1, v10, v2, v5}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->a(Ljie;Ljbp;IZ)Z

    move-result v5

    .line 191
    add-int/lit8 v4, v4, 0x2

    goto :goto_a

    .line 189
    :cond_d
    add-int/lit8 v4, v9, 0x1

    move v9, v4

    goto :goto_9

    .line 188
    :cond_e
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_8

    :cond_f
    move v4, v7

    .line 201
    :cond_10
    if-eqz v4, :cond_19

    .line 203
    const/4 v4, 0x1

    move/from16 v0, p2

    if-ne v0, v4, :cond_15

    .line 204
    if-nez v6, :cond_11

    if-eqz v5, :cond_14

    :cond_11
    const/4 v4, 0x1

    .line 212
    :goto_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    iget-object v5, v5, Lfiu;->ctj:Ljava/lang/Float;

    if-eqz v5, :cond_12

    .line 213
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->mLocation:Landroid/location/Location;

    invoke-virtual {v5}, Landroid/location/Location;->hasAccuracy()Z

    move-result v5

    if-eqz v5, :cond_18

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->mLocation:Landroid/location/Location;

    invoke-virtual {v5}, Landroid/location/Location;->getAccuracy()F

    move-result v5

    .line 216
    :goto_c
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    iget-object v6, v6, Lfiu;->ctj:Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    cmpg-float v5, v6, v5

    if-gez v5, :cond_12

    .line 217
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csZ:J

    const-wide/16 v8, 0x3c

    add-long/2addr v6, v8

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->bm(J)V

    .line 228
    :cond_12
    :goto_d
    if-eqz v4, :cond_13

    .line 229
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    iget-object v5, v5, Lfiu;->ctl:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    :cond_13
    return v4

    .line 204
    :cond_14
    const/4 v4, 0x0

    goto :goto_b

    .line 206
    :cond_15
    if-nez v6, :cond_16

    if-nez v5, :cond_17

    :cond_16
    const/4 v4, 0x1

    goto :goto_b

    :cond_17
    const/4 v4, 0x0

    goto :goto_b

    .line 213
    :cond_18
    const/high16 v5, 0x42c80000    # 100.0f

    goto :goto_c

    :cond_19
    move v4, v6

    goto :goto_d

    :cond_1a
    move v5, v7

    goto/16 :goto_7
.end method

.method public final c(Ljie;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 116
    iget-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    iput-object v1, v0, Lfiu;->cti:Ljava/lang/Long;

    iput-object v1, v0, Lfiu;->ctj:Ljava/lang/Float;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lfiu;->ctg:Z

    iget-object v0, v0, Lfiu;->ctl:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->a(Ljie;I)Z

    move-result v0

    return v0
.end method
