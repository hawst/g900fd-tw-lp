.class public Lcom/google/android/sidekick/main/actions/ActionLauncherActivity;
.super Landroid/app/Activity;
.source "PG"

# interfaces
.implements Landroid/app/FragmentManager$OnBackStackChangedListener;


# instance fields
.field private boe:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackStackChanged()V
    .locals 2

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/actions/ActionLauncherActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    .line 137
    iget v1, p0, Lcom/google/android/sidekick/main/actions/ActionLauncherActivity;->boe:I

    if-ne v0, v1, :cond_0

    .line 139
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/actions/ActionLauncherActivity;->finish()V

    .line 141
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 41
    if-nez p1, :cond_1

    .line 42
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/actions/ActionLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/actions/ActionLauncherActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    iput v0, p0, Lcom/google/android/sidekick/main/actions/ActionLauncherActivity;->boe:I

    if-eqz v4, :cond_0

    const-string v0, "action_type"

    const/4 v1, -0x1

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    move-object v0, v3

    :goto_0
    if-nez v0, :cond_4

    const-string v0, "ActionLauncherActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to create a fragment for Intent "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/actions/ActionLauncherActivity;->finish()V

    .line 44
    :cond_1
    :goto_1
    :sswitch_0
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/actions/ActionLauncherActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/FragmentManager;->addOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    .line 45
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    return-void

    .line 42
    :sswitch_1
    invoke-static {v4}, Lewl;->D(Landroid/content/Intent;)Lewl;

    move-result-object v0

    const-string v3, "delete_place_dialog"

    goto :goto_0

    :sswitch_2
    invoke-static {v4}, Lexq;->F(Landroid/content/Intent;)Lexq;

    move-result-object v0

    const-string v3, "rename_place_dialog"

    goto :goto_0

    :sswitch_3
    invoke-static {p0, v4}, Lewx;->g(Landroid/content/Context;Landroid/content/Intent;)Lewx;

    move-result-object v0

    const-string v3, "edit_home_work"

    goto :goto_0

    :sswitch_4
    invoke-static {v4}, Lexe;->E(Landroid/content/Intent;)Lexe;

    move-result-object v0

    const-string v3, "editPlaceWorkerFragment"

    goto :goto_0

    :sswitch_5
    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v0, "entry"

    const-class v1, Lizj;

    invoke-static {v2, v0, v1}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    const-string v1, "action"

    const-class v5, Liwk;

    invoke-static {v2, v1, v5}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v1

    check-cast v1, Liwk;

    const-string v5, "delete_action"

    const-class v6, Liwk;

    invoke-static {v2, v5, v6}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v2

    check-cast v2, Liwk;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lizj;->dSU:Ljdx;

    if-eqz v1, :cond_2

    invoke-static {v3, v0}, Lexf;->b(Landroid/app/Fragment;Lizj;)Lexf;

    move-result-object v0

    :goto_2
    const-string v3, "reminder_edit_or_delete"

    goto :goto_0

    :cond_2
    if-eqz v2, :cond_3

    invoke-static {v3, v0}, Lewq;->a(Landroid/app/Fragment;Lizj;)Lewq;

    move-result-object v0

    goto :goto_2

    :cond_3
    move-object v0, v3

    goto :goto_2

    :cond_4
    invoke-static {v3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/actions/ActionLauncherActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    instance-of v2, v0, Landroid/app/DialogFragment;

    if-eqz v2, :cond_5

    check-cast v0, Landroid/app/DialogFragment;

    invoke-virtual {v0, v1, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    invoke-virtual {v1, v0, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_2
        0xe -> :sswitch_1
        0x3d -> :sswitch_4
        0x3e -> :sswitch_1
        0x3f -> :sswitch_3
        0x40 -> :sswitch_2
        0x93 -> :sswitch_5
        0x94 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/actions/ActionLauncherActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/FragmentManager;->removeOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    .line 51
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 52
    return-void
.end method
