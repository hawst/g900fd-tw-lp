.class public Lcom/google/android/sidekick/main/TestLauncherActivity;
.super Landroid/app/Activity;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final cjW:Ljava/lang/Object;

.field private static final cjX:Ljava/lang/Object;

.field private static final cjY:Ljava/lang/Object;

.field private static final cjZ:Ljava/lang/Object;

.field private static final cka:Ljava/lang/Object;

.field private static final ckb:Ljava/util/regex/Pattern;


# instance fields
.field private cjR:Levm;

.field private ckc:Lfam;

.field private mAsyncServices:Lema;

.field private mGsaPreferenceController:Lchr;

.field private mNetworkClient:Lfcx;

.field public mNotificationStore:Lffp;

.field private final mRandom:Ljava/util/Random;

.field public mTrainingQuestionManager:Lfdn;

.field public mVelvetServices:Lgql;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 135
    const-string v0, "com.google.android.sidekick.main.REFRESH_TRAFFIC_ACTION"

    sput-object v0, Lcom/google/android/sidekick/main/TestLauncherActivity;->cjW:Ljava/lang/Object;

    .line 137
    const-string v0, "com.google.android.sidekick.main.NOTIFICATIONS_ACTION"

    sput-object v0, Lcom/google/android/sidekick/main/TestLauncherActivity;->cjX:Ljava/lang/Object;

    .line 139
    const-string v0, "com.google.android.sidekick.main.TRAINING_MODE_ACTION"

    sput-object v0, Lcom/google/android/sidekick/main/TestLauncherActivity;->cjY:Ljava/lang/Object;

    .line 141
    const-string v0, "com.google.android.sidekick.main.OPT_IN_ACTION"

    sput-object v0, Lcom/google/android/sidekick/main/TestLauncherActivity;->cjZ:Ljava/lang/Object;

    .line 157
    const-string v0, "com.google.android.sidekick.main.DELETE_APPLICATION_FILE"

    sput-object v0, Lcom/google/android/sidekick/main/TestLauncherActivity;->cka:Ljava/lang/Object;

    .line 182
    const-string v0, "(\\d+\\.?\\d*)m$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/sidekick/main/TestLauncherActivity;->ckb:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 116
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 184
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mRandom:Ljava/util/Random;

    .line 1163
    return-void
.end method

.method private O(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    const-wide/16 v8, -0x1

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1067
    const-string v0, "command"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1068
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1069
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->axV()Levm;

    move-result-object v5

    .line 1073
    const-string v0, "location_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1074
    const-string v0, "location_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1075
    const-string v6, ","

    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1078
    :goto_0
    const-string v6, "mock_location_time_accuracy"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v1, v2

    .line 1081
    :goto_1
    array-length v2, v0

    sub-int/2addr v2, v1

    const/4 v3, 0x4

    if-lt v2, v3, :cond_5

    .line 1082
    add-int/lit8 v2, v1, 0x1

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    add-int/lit8 v1, v2, 0x1

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    add-int/lit8 v5, v1, 0x1

    aget-object v1, v0, v1

    invoke-direct {p0, v1, v8, v9}, Lcom/google/android/sidekick/main/TestLauncherActivity;->o(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    add-int/lit8 v1, v5, 0x1

    aget-object v5, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v3, v2, v6, v5}, Lcom/google/android/sidekick/main/TestLauncherActivity;->a(FFLjava/lang/Long;Ljava/lang/Integer;)Landroid/location/Location;

    move-result-object v2

    .line 1085
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1087
    :cond_0
    const-string v6, "mock_location_time"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1090
    :goto_2
    array-length v3, v0

    sub-int/2addr v3, v2

    if-lt v3, v13, :cond_5

    .line 1091
    add-int/lit8 v3, v2, 0x1

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    add-int/lit8 v6, v3, 0x1

    aget-object v2, v0, v3

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    add-int/lit8 v2, v6, 0x1

    aget-object v6, v0, v6

    invoke-direct {p0, v6, v8, v9}, Lcom/google/android/sidekick/main/TestLauncherActivity;->o(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v5, v3, v6, v1}, Lcom/google/android/sidekick/main/TestLauncherActivity;->a(FFLjava/lang/Long;Ljava/lang/Integer;)Landroid/location/Location;

    move-result-object v3

    .line 1094
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1096
    :cond_1
    const-string v6, "mock_location"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1099
    :goto_3
    array-length v3, v0

    sub-int/2addr v3, v2

    if-lt v3, v12, :cond_5

    .line 1100
    add-int/lit8 v3, v2, 0x1

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    add-int/lit8 v2, v3, 0x1

    aget-object v3, v0, v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-static {v5, v3, v1, v1}, Lcom/google/android/sidekick/main/TestLauncherActivity;->a(FFLjava/lang/Long;Ljava/lang/Integer;)Landroid/location/Location;

    move-result-object v3

    .line 1102
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1104
    :cond_2
    const-string v0, "clear"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1119
    :cond_3
    :goto_4
    return-void

    .line 1107
    :cond_4
    const-string v0, "print"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1108
    invoke-virtual {v5}, Levm;->awd()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhm;

    .line 1109
    iget-object v3, v0, Ljhm;->aeB:Ljbp;

    .line 1110
    invoke-virtual {v3}, Ljbp;->mR()D

    move-result-wide v4

    .line 1111
    invoke-virtual {v3}, Ljbp;->mS()D

    move-result-wide v6

    .line 1112
    invoke-virtual {v0}, Ljhm;->bcB()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    .line 1113
    const-string v0, "Location queue: %f, %f, %tc"

    new-array v3, v13, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v4, 0x1

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v12

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_5

    .line 1118
    :cond_5
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    goto :goto_4

    :cond_6
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private static a(FFLjava/lang/Long;Ljava/lang/Integer;)Landroid/location/Location;
    .locals 6

    .prologue
    .line 1124
    new-instance v0, Landroid/location/Location;

    const-string v1, "fake"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 1125
    float-to-double v2, p0

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 1126
    float-to-double v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 1127
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 1128
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setTime(J)V

    .line 1130
    :cond_0
    if-eqz p3, :cond_1

    .line 1131
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    .line 1134
    :cond_1
    return-object v0
.end method

.method private awf()V
    .locals 3

    .prologue
    .line 362
    const/16 v0, 0x1c

    invoke-static {v0}, Lgal;->jP(I)Landroid/content/Intent;

    move-result-object v0

    .line 363
    const-string v1, "com.google.android.apps.sidekick.REFRESH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 364
    const-string v1, "com.google.android.apps.sidekick.TYPE"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 365
    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 366
    return-void
.end method

.method private awg()Ljava/io/File;
    .locals 3

    .prologue
    .line 477
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "entry_provider"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private awh()Ljava/io/File;
    .locals 3

    .prologue
    .line 482
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "notifications_store"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private awi()V
    .locals 2

    .prologue
    .line 525
    const-string v0, "Clearing response data"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 526
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mNetworkClient:Lfcx;

    check-cast v0, Lewi;

    .line 527
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lewi;->a(Ljeh;)V

    .line 528
    return-void
.end method

.method private c(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 10
    .param p2    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x1

    const/4 v1, -0x1

    const/4 v8, 0x0

    .line 741
    const-string v0, "schedule"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 742
    const-string v0, "entry_type"

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 743
    if-ne v0, v1, :cond_1

    .line 744
    const-string v0, "Failed: entry type wasn\'t set"

    invoke-static {p0, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 774
    :cond_0
    :goto_0
    return-void

    .line 748
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 750
    const-string v2, "com.google.android.apps.sidekick.notifications.SCHEDULE_REFRESH"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 752
    new-instance v2, Ljbj;

    invoke-direct {v2}, Ljbj;-><init>()V

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljbj;->om(I)Ljbj;

    move-result-object v2

    .line 754
    new-array v3, v9, [I

    aput v0, v3, v8

    iput-object v3, v2, Ljbj;->dYx:[I

    .line 755
    new-instance v0, Lizp;

    invoke-direct {v0}, Lizp;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x1388

    add-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v0, v4, v5}, Lizp;->cu(J)Lizp;

    move-result-object v0

    .line 757
    iput-object v2, v0, Lizp;->afA:Ljbj;

    .line 758
    new-instance v2, Lizo;

    invoke-direct {v2}, Lizo;-><init>()V

    .line 759
    new-array v3, v9, [Lizp;

    aput-object v0, v3, v8

    iput-object v3, v2, Lizo;->dUS:[Lizp;

    .line 760
    const-string v0, "com.google.android.apps.sidekick.notifications.NEXT_REFRESH"

    invoke-static {v2}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 762
    invoke-virtual {p0, v1}, Lcom/google/android/sidekick/main/TestLauncherActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 763
    :cond_2
    const-string v0, "refresh"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 764
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 766
    const-string v1, "com.google.android.apps.sidekick.notifications.REFRESH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 767
    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 768
    :cond_3
    const-string v0, "clear"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 769
    const-string v0, "Clearing notification store"

    invoke-static {p0, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 770
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mAsyncServices:Lema;

    new-instance v1, Levz;

    const-string v2, "clear"

    invoke-direct {v1, p0, v2}, Levz;-><init>(Lcom/google/android/sidekick/main/TestLauncherActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lema;->a(Leri;)V

    goto :goto_0

    .line 771
    :cond_4
    const-string v0, "dump"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 772
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mAsyncServices:Lema;

    new-instance v1, Levz;

    const-string v2, "dump"

    invoke-direct {v1, p0, v2}, Levz;-><init>(Lcom/google/android/sidekick/main/TestLauncherActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lema;->a(Leri;)V

    goto/16 :goto_0
.end method

.method private f(Ljava/io/File;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 468
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 470
    const-string v0, "File deleted"

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 474
    :goto_0
    return-void

    .line 472
    :cond_0
    const-string v0, "File does not exist"

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private g(Ljava/io/File;)Ljava/util/List;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 681
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 683
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    invoke-direct {v0, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 686
    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 687
    invoke-direct {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->lB(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 690
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    throw v0
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 698
    :catch_0
    move-exception v0

    .line 693
    const-string v2, "TestLauncherActivity"

    const-string v3, "File not found: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 694
    const-string v0, "File not found.  Check the logs."

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 699
    :goto_1
    return-object v1

    .line 690
    :cond_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 695
    :catch_1
    move-exception v0

    .line 696
    const-string v2, "TestLauncherActivity"

    const-string v3, "IO Exception: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 697
    const-string v0, "IO Exception.  Check the logs."

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method private h(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 827
    const/4 v2, 0x0

    .line 829
    :try_start_0
    new-instance v3, Ljeh;

    invoke-direct {v3}, Ljeh;-><init>()V

    .line 830
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 831
    :try_start_1
    invoke-static {v3, v1}, Leqh;->a(Ljsr;Ljava/io/InputStream;)Ljsr;

    .line 832
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mNetworkClient:Lfcx;

    check-cast v0, Lewi;

    .line 833
    invoke-virtual {v0, v3}, Lewi;->a(Ljeh;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 841
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    .line 842
    :goto_0
    return-void

    .line 834
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 835
    :goto_1
    :try_start_2
    const-string v2, "TestLauncherActivity"

    const-string v3, "File not found: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 836
    const-string v0, "File not found.  Check the logs."

    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 841
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    .line 837
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 838
    :goto_2
    :try_start_3
    const-string v2, "TestLauncherActivity"

    const-string v3, "IO Exception: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 839
    const-string v0, "IO Exception.  Check the logs."

    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 841
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    .line 837
    :catch_2
    move-exception v0

    goto :goto_2

    .line 834
    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method private lB(Ljava/lang/String;)Landroid/location/Location;
    .locals 11

    .prologue
    const/4 v0, 0x2

    const/4 v10, 0x1

    const-wide/16 v2, -0x1

    .line 875
    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 876
    array-length v1, v6

    if-ge v1, v0, :cond_0

    .line 877
    const-string v0, "TestLauncherActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected parts (lat, long[, time, accuracy_meters]) in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 879
    :cond_0
    new-instance v5, Landroid/location/Location;

    const-string v1, "fake"

    invoke-direct {v5, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 880
    const/4 v1, 0x0

    aget-object v1, v6, v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Landroid/location/Location;->setLatitude(D)V

    .line 881
    aget-object v1, v6, v10

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Landroid/location/Location;->setLongitude(D)V

    move v4, v0

    move-wide v0, v2

    .line 883
    :goto_1
    array-length v7, v6

    if-ge v4, v7, :cond_3

    .line 884
    sget-object v7, Lcom/google/android/sidekick/main/TestLauncherActivity;->ckb:Ljava/util/regex/Pattern;

    aget-object v8, v6, v4

    invoke-virtual {v7, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    .line 885
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->matches()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 886
    invoke-virtual {v7, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    invoke-virtual {v5, v7}, Landroid/location/Location;->setAccuracy(F)V

    .line 883
    :cond_1
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 887
    :cond_2
    cmp-long v7, v0, v2

    if-nez v7, :cond_1

    .line 888
    aget-object v0, v6, v4

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/sidekick/main/TestLauncherActivity;->o(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_2

    .line 891
    :cond_3
    cmp-long v2, v0, v2

    if-nez v2, :cond_4

    .line 892
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 894
    :cond_4
    invoke-virtual {v5, v0, v1}, Landroid/location/Location;->setTime(J)V

    move-object v0, v5

    .line 896
    goto :goto_0
.end method

.method private o(Ljava/lang/String;J)J
    .locals 5

    .prologue
    const-wide/16 v0, -0x1

    .line 902
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 918
    :goto_0
    return-wide v0

    .line 905
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 906
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    .line 908
    :cond_1
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 910
    :try_start_0
    invoke-virtual {v2, p1}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    .line 911
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J
    :try_end_0
    .catch Landroid/util/TimeFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    .line 912
    :catch_0
    move-exception v2

    .line 913
    const-string v3, "TestLauncherActivity"

    const-string v4, "Failed to parse time value"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 914
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to parse time value: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method


# virtual methods
.method public final K([B)Ljava/lang/Void;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 962
    if-eqz p1, :cond_0

    .line 963
    :try_start_0
    new-instance v0, Lamf;

    invoke-direct {v0}, Lamf;-><init>()V

    .line 964
    invoke-static {v0, p1}, Ljsr;->c(Ljsr;[B)Ljsr;

    .line 965
    iget-object v0, v0, Lamf;->aeC:Lizn;

    .line 967
    sget-object v1, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {v1}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 969
    new-instance v4, Ljava/io/File;

    const-string v3, "response.bin"

    invoke-direct {v4, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 970
    new-instance v5, Ljava/io/File;

    const-string v3, "config.bin"

    invoke-direct {v5, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 972
    invoke-static {v0}, Ljsr;->m(Ljsr;)[B
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 976
    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 977
    :try_start_2
    invoke-virtual {v3, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 978
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    .line 980
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 981
    :try_start_3
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    .line 982
    invoke-virtual {v0}, Lcfo;->DE()Lcxs;

    move-result-object v6

    .line 984
    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v0

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcxs;->u(Landroid/accounts/Account;)[B

    move-result-object v0

    .line 986
    invoke-virtual {v1, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 987
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 992
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljsq; {:try_start_4 .. :try_end_4} :catch_1

    .line 997
    :goto_0
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljsq; {:try_start_5 .. :try_end_5} :catch_1

    .line 1001
    :goto_1
    :try_start_6
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1002
    const-string v1, "application/octet-stream"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1004
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1005
    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1006
    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1008
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1009
    const/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1}, Lcom/google/android/sidekick/main/TestLauncherActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_6
    .catch Ljsq; {:try_start_6 .. :try_end_6} :catch_1

    .line 1015
    :cond_0
    :goto_2
    return-object v2

    .line 988
    :catch_0
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    .line 989
    :goto_3
    :try_start_7
    const-string v6, "TestLauncherActivity"

    const-string v7, "Error writing response file: "

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 992
    if-eqz v3, :cond_1

    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljsq; {:try_start_8 .. :try_end_8} :catch_1

    .line 997
    :cond_1
    :goto_4
    if-eqz v1, :cond_2

    :try_start_9
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Ljsq; {:try_start_9 .. :try_end_9} :catch_1

    .line 1001
    :cond_2
    :goto_5
    :try_start_a
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1002
    const-string v1, "application/octet-stream"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1004
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1005
    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1006
    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1008
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1009
    const/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1}, Lcom/google/android/sidekick/main/TestLauncherActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_a
    .catch Ljsq; {:try_start_a .. :try_end_a} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_2

    .line 991
    :catchall_0
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    .line 992
    :goto_6
    if-eqz v3, :cond_3

    :try_start_b
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6
    .catch Ljsq; {:try_start_b .. :try_end_b} :catch_1

    .line 997
    :cond_3
    :goto_7
    if-eqz v1, :cond_4

    :try_start_c
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7
    .catch Ljsq; {:try_start_c .. :try_end_c} :catch_1

    .line 1001
    :cond_4
    :goto_8
    :try_start_d
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1002
    const-string v3, "application/octet-stream"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1004
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1005
    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1006
    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1008
    const-string v4, "android.intent.extra.STREAM"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1009
    const/16 v3, 0x3e8

    invoke-virtual {p0, v1, v3}, Lcom/google/android/sidekick/main/TestLauncherActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1010
    throw v0
    :try_end_d
    .catch Ljsq; {:try_start_d .. :try_end_d} :catch_1

    :catch_2
    move-exception v0

    goto/16 :goto_0

    :catch_3
    move-exception v0

    goto/16 :goto_1

    :catch_4
    move-exception v0

    goto :goto_4

    :catch_5
    move-exception v0

    goto :goto_5

    :catch_6
    move-exception v3

    goto :goto_7

    :catch_7
    move-exception v1

    goto :goto_8

    .line 991
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_6

    :catchall_2
    move-exception v0

    goto :goto_6

    .line 988
    :catch_8
    move-exception v0

    move-object v1, v2

    goto/16 :goto_3

    :catch_9
    move-exception v0

    goto/16 :goto_3
.end method

.method public final a(Ljava/io/File;I)V
    .locals 2

    .prologue
    .line 670
    packed-switch p2, :pswitch_data_0

    .line 678
    :goto_0
    :pswitch_0
    return-void

    .line 672
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/sidekick/main/TestLauncherActivity;->h(Ljava/io/File;)V

    goto :goto_0

    .line 675
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/sidekick/main/TestLauncherActivity;->g(Ljava/io/File;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcgj;

    invoke-direct {v1}, Lcgj;-><init>()V

    invoke-virtual {v1, p0, v0}, Lcgj;->a(Landroid/content/Context;Ljava/util/List;)V

    goto :goto_0

    .line 670
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public browseModeResults(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 937
    new-instance v0, Ljbj;

    invoke-direct {v0}, Ljbj;-><init>()V

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Ljbj;->om(I)Ljbj;

    move-result-object v0

    .line 939
    new-instance v1, Ljcn;

    invoke-direct {v1}, Ljcn;-><init>()V

    const-string v2, "http://lh3.ggpht.com/uEKm-nqyZsYhw7AyQLhI1kpHYY3dn5n0-6DL7I49DOe1bdjhrSQSVWptTZr_"

    invoke-virtual {v1, v2}, Ljcn;->tk(Ljava/lang/String;)Ljcn;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljcn;->oH(I)Ljcn;

    move-result-object v1

    .line 942
    new-instance v2, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    invoke-direct {v2}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->a(Ljbj;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v0

    const-string v2, "Test results"

    invoke-virtual {v0, v2}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->mn(Ljava/lang/String;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->b(Ljcn;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v0

    .line 946
    invoke-static {p0, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 947
    return-void
.end method

.method public detectTv(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 922
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lfiz;->ctw:Lefw;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/velvet/extradex/ExtraDexHostActivity;->a(Landroid/content/Context;Lefw;)Landroid/content/Intent;

    move-result-object v0

    .line 924
    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 925
    return-void
.end method

.method public emailResponse(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 950
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    .line 951
    iget-object v0, v0, Lfdb;->mAsyncFileStorage:Lfbz;

    const-string v1, "entry_provider"

    new-instance v2, Levs;

    invoke-direct {v2, p0}, Levs;-><init>(Lcom/google/android/sidekick/main/TestLauncherActivity;)V

    invoke-interface {v0, v1, v2}, Lfbz;->a(Ljava/lang/String;Lifg;)V

    .line 958
    return-void
.end method

.method public myRemindersWebActivity(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 928
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/sidekick/main/RemindersListWebView;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 929
    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 930
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 1020
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    .line 1021
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mAsyncServices:Lema;

    new-instance v1, Levt;

    const-string v2, "Delete response and config files"

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-direct {v1, p0, v2, v3}, Levt;-><init>(Lcom/google/android/sidekick/main/TestLauncherActivity;Ljava/lang/String;[I)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Lema;->a(Leri;J)V

    .line 1032
    :cond_0
    return-void

    .line 1021
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 413
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 414
    const v1, 0x7f11041f

    if-ne v0, v1, :cond_1

    .line 415
    const-string v0, "Test cards loaded on next refresh"

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mNetworkClient:Lfcx;

    check-cast v0, Lewi;

    invoke-virtual {v0}, Lewi;->awp()V

    .line 465
    :cond_0
    :goto_0
    return-void

    .line 417
    :cond_1
    const v1, 0x7f11041e

    if-ne v0, v1, :cond_2

    .line 418
    new-instance v0, Levp;

    invoke-direct {v0, p0}, Levp;-><init>(Lcom/google/android/sidekick/main/TestLauncherActivity;)V

    new-array v1, v5, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Levp;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 420
    :cond_2
    const v1, 0x7f110420

    if-ne v0, v1, :cond_3

    .line 421
    const-string v0, "Loading response data"

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-static {v5}, Levv;->iq(I)Levv;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "data_response"

    invoke-virtual {v0, v1, v2}, Levv;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 423
    :cond_3
    const v1, 0x7f110421

    if-ne v0, v1, :cond_4

    .line 424
    const-string v0, "Sending Intent"

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    new-instance v0, Levx;

    invoke-direct {v0}, Levx;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "intent_picker"

    invoke-virtual {v0, v1, v2}, Levx;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 426
    :cond_4
    const v1, 0x7f110422

    if-ne v0, v1, :cond_5

    .line 427
    const-string v0, "Cleared saved location"

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJs()Lchr;

    move-result-object v0

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "lastloc"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 429
    :cond_5
    const v1, 0x7f110423

    if-ne v0, v1, :cond_6

    .line 430
    invoke-direct {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->awi()V

    goto :goto_0

    .line 432
    :cond_6
    const v1, 0x7f110424

    if-ne v0, v1, :cond_7

    .line 433
    const-string v0, "clear"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/sidekick/main/TestLauncherActivity;->c(Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 435
    :cond_7
    const v1, 0x7f110425

    if-ne v0, v1, :cond_8

    .line 436
    const-string v0, "Clearing training mode data"

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    new-instance v0, Levq;

    invoke-direct {v0, p0}, Levq;-><init>(Lcom/google/android/sidekick/main/TestLauncherActivity;)V

    new-array v1, v5, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Levq;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 438
    :cond_8
    const v1, 0x7f110426

    if-ne v0, v1, :cond_9

    .line 439
    const-string v0, "Precache test"

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-static {v2}, Levv;->iq(I)Levv;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "precache_test"

    invoke-virtual {v0, v1, v2}, Levv;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 441
    :cond_9
    const v1, 0x7f110427

    if-ne v0, v1, :cond_a

    .line 442
    invoke-direct {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->awg()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->f(Ljava/io/File;)V

    goto/16 :goto_0

    .line 444
    :cond_a
    const v1, 0x7f110428

    if-ne v0, v1, :cond_b

    .line 445
    invoke-direct {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->awh()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->f(Ljava/io/File;)V

    goto/16 :goto_0

    .line 447
    :cond_b
    const v1, 0x7f11042e

    if-ne v0, v1, :cond_c

    .line 448
    new-instance v0, Lcom/android/recurrencepicker/RecurrencePickerDialog;

    invoke-direct {v0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "recurrence"

    invoke-virtual {v0, v1, v2}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 450
    :cond_c
    const v1, 0x7f110430

    if-ne v0, v1, :cond_d

    .line 451
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->ckc:Lfam;

    check-cast v0, Lfan;

    invoke-virtual {v0, v3}, Lfan;->disableScheduledRefreshes(Z)V

    goto/16 :goto_0

    .line 452
    :cond_d
    const v1, 0x7f110431

    if-ne v0, v1, :cond_e

    .line 453
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->ckc:Lfam;

    check-cast v0, Lfan;

    invoke-virtual {v0, v5}, Lfan;->disableScheduledRefreshes(Z)V

    goto/16 :goto_0

    .line 454
    :cond_e
    const v1, 0x7f110432

    if-ne v0, v1, :cond_f

    .line 455
    invoke-static {}, Lfiw;->aAh()V

    goto/16 :goto_0

    .line 456
    :cond_f
    const v1, 0x7f110433

    if-ne v0, v1, :cond_10

    .line 457
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "vnd.google.android.hangouts/vnd.google.android.hangout_whitelist"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v0}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "participant_gaia"

    const-string v2, "108081007484222053592"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "participant_name"

    const-string v2, "Bob Doe"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "start_video"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Lcom/google/android/sidekick/main/TestLauncherActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 458
    :cond_10
    const v1, 0x7f110434

    if-ne v0, v1, :cond_11

    .line 459
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Nice hair!"

    aput-object v1, v0, v5

    const-string v1, "Hey!"

    aput-object v1, v0, v3

    const-string v1, "Business opportunity"

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mRandom:Ljava/util/Random;

    array-length v2, v0

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DL()Lcrh;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.SENDTO"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "vnd.google.android.hangouts/vnd.google.android.hangout_whitelist"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "account_name"

    invoke-virtual {v2}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "participant_gaia"

    const-string v4, "108081007484222053592"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "participant_name"

    const-string v4, "Bob Doe"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "android.intent.extra.TEXT"

    aget-object v0, v0, v1

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Lcom/google/android/sidekick/main/TestLauncherActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 460
    :cond_11
    const v1, 0x7f110435

    if-ne v0, v1, :cond_12

    .line 461
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.VEHICLE_EXIT_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 462
    :cond_12
    const v1, 0x7f110436

    if-ne v0, v1, :cond_0

    .line 463
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->axU()Lfjo;

    move-result-object v0

    invoke-virtual {v0}, Lfjo;->aAt()V

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const v7, 0x7f110427

    const/4 v6, 0x3

    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 201
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 202
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->requestWindowFeature(I)Z

    .line 203
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 204
    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 205
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mVelvetServices:Lgql;

    .line 207
    invoke-static {}, Lckw;->OZ()Lckw;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Lckw;->Pd()Z

    move-result v2

    if-nez v2, :cond_0

    .line 209
    iget-object v2, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BK()Lcke;

    move-result-object v2

    invoke-virtual {v0, v2}, Lckw;->a(Lcke;)V

    .line 212
    :cond_0
    invoke-static {}, Lckw;->OZ()Lckw;

    move-result-object v0

    invoke-virtual {v0}, Lckw;->Pb()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 213
    const v0, 0x7f040188

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->setContentView(I)V

    .line 220
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->axI()Lfcx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mNetworkClient:Lfcx;

    .line 221
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->axV()Levm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->cjR:Levm;

    .line 222
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->ayc()Lfdn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mTrainingQuestionManager:Lfdn;

    .line 224
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    iget-object v0, v0, Lfdb;->mNotificationStore:Lffp;

    iput-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mNotificationStore:Lffp;

    .line 225
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->axF()Lfam;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->ckc:Lfam;

    .line 226
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mVelvetServices:Lgql;

    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    iput-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mAsyncServices:Lema;

    .line 227
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJs()Lchr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mGsaPreferenceController:Lchr;

    .line 229
    const v0, 0x7f11041f

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f11041e

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f110420

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f110421

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f110422

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f110423

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f110424

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f110425

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f110426

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v7}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f110428

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f11042e

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f110430

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f110431

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f110432

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f110433

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f110434

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f110435

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f110436

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 231
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 232
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 233
    sget-object v3, Lcom/google/android/sidekick/main/TestLauncherActivity;->cjW:Ljava/lang/Object;

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 234
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/sidekick/main/TrafficIntentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 236
    const-string v1, "force_refresh"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 237
    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 238
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->finish()V

    .line 355
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->awg()Ljava/io/File;

    move-result-object v0

    .line 356
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    .line 357
    invoke-virtual {p0, v7}, Lcom/google/android/sidekick/main/TestLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 359
    :cond_2
    :goto_1
    return-void

    .line 215
    :cond_3
    const-string v0, "Not enabled for a non-debug build"

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 216
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->finish()V

    goto :goto_1

    .line 239
    :cond_4
    const-string v3, "com.google.android.sidekick.main.LOAD_RESPONSE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 240
    const-string v0, "response"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 241
    const-string v0, "response"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    :try_start_0
    new-instance v1, Ljeh;

    invoke-direct {v1}, Ljeh;-><init>()V

    invoke-static {v1, v0}, Ljsr;->c(Ljsr;[B)Ljsr;

    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mNetworkClient:Lfcx;

    check-cast v0, Lewi;

    invoke-virtual {v0, v1}, Lewi;->a(Ljeh;)V
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    :goto_2
    const-string v0, "refresh"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 251
    invoke-direct {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->awf()V

    .line 253
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->finish()V

    goto :goto_0

    .line 241
    :catch_0
    move-exception v0

    const-string v1, "TestLauncherActivity"

    const-string v3, "IO Exception: "

    invoke-static {v1, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v0, "Invalid protocol buffer.  Check the logs."

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 242
    :cond_6
    const-string v0, "response_file"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 243
    const-string v0, "response_file"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 244
    new-instance v3, Ljava/io/File;

    invoke-virtual {p0, v1}, Lcom/google/android/sidekick/main/TestLauncherActivity;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    const-string v4, "TestData"

    invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 245
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 246
    invoke-direct {p0, v1}, Lcom/google/android/sidekick/main/TestLauncherActivity;->h(Ljava/io/File;)V

    goto :goto_2

    .line 248
    :cond_7
    invoke-direct {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->awi()V

    goto :goto_2

    .line 254
    :cond_8
    const-string v3, "com.google.android.sidekick.main.BAD_CONNECTION"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 255
    const-string v0, "enabled"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 256
    if-eqz v1, :cond_9

    const-string v0, "Simulating bad connections"

    :goto_3
    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mNetworkClient:Lfcx;

    check-cast v0, Lewi;

    invoke-virtual {v0, v1}, Lewi;->fg(Z)V

    .line 257
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->finish()V

    goto/16 :goto_0

    .line 256
    :cond_9
    const-string v0, "Back to real connections"

    goto :goto_3

    .line 258
    :cond_a
    const-string v3, "com.google.android.sidekick.main.SET_TIME"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 259
    const-string v0, "time"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 260
    if-eqz v0, :cond_b

    .line 261
    const-wide/16 v2, -0x1

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/sidekick/main/TestLauncherActivity;->o(Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->cjR:Levm;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Lifq;->be(Ljava/lang/Object;)Lifq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "User time "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lesi;->aW(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 263
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->finish()V

    goto/16 :goto_0

    .line 264
    :cond_c
    const-string v3, "com.google.android.sidekick.main.CLEAR_TIME"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 265
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->cjR:Levm;

    invoke-static {}, Lifq;->aVX()Lifq;

    const-string v0, "Cleared user time"

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 266
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->finish()V

    goto/16 :goto_0

    .line 267
    :cond_d
    sget-object v3, Lcom/google/android/sidekick/main/TestLauncherActivity;->cjX:Ljava/lang/Object;

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 268
    const-string v0, "action"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 269
    if-eqz v0, :cond_e

    .line 270
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/sidekick/main/TestLauncherActivity;->c(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 272
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->finish()V

    goto/16 :goto_0

    .line 273
    :cond_f
    sget-object v3, Lcom/google/android/sidekick/main/TestLauncherActivity;->cjY:Ljava/lang/Object;

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 274
    const-string v0, "action"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 275
    if-eqz v0, :cond_10

    .line 276
    const-string v1, "dump"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "Dumping training mode data"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    new-instance v0, Levr;

    invoke-direct {v0, p0}, Levr;-><init>(Lcom/google/android/sidekick/main/TestLauncherActivity;)V

    new-array v1, v5, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Levr;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 278
    :cond_10
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->finish()V

    goto/16 :goto_0

    .line 276
    :cond_11
    const-string v0, "Unrecognized training mode action"

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_4

    .line 279
    :cond_12
    const-string v3, "mock_locations"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 280
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->O(Landroid/os/Bundle;)V

    .line 281
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->finish()V

    goto/16 :goto_0

    .line 282
    :cond_13
    const-string v3, "com.google.android.sidekick.main.FETCH_PENDING_ACTIONS"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 283
    new-instance v0, Levu;

    invoke-direct {v0, p0}, Levu;-><init>(Lcom/google/android/sidekick/main/TestLauncherActivity;)V

    new-array v1, v5, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Levu;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_1

    .line 285
    :cond_14
    sget-object v3, Lcom/google/android/sidekick/main/TestLauncherActivity;->cka:Ljava/lang/Object;

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 286
    const-string v0, "filename"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 287
    const-string v0, "filename"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 289
    const-string v2, "entries"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 290
    invoke-direct {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->awg()Ljava/io/File;

    move-result-object v1

    .line 295
    :cond_15
    :goto_5
    if-eqz v1, :cond_16

    .line 296
    invoke-direct {p0, v1}, Lcom/google/android/sidekick/main/TestLauncherActivity;->f(Ljava/io/File;)V

    .line 299
    :cond_16
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->finish()V

    goto/16 :goto_1

    .line 291
    :cond_17
    const-string v2, "notifications"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 292
    invoke-direct {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->awh()Ljava/io/File;

    move-result-object v1

    goto :goto_5

    .line 301
    :cond_18
    sget-object v3, Lcom/google/android/sidekick/main/TestLauncherActivity;->cjZ:Ljava/lang/Object;

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 302
    const-string v0, "account_name"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 303
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v3

    .line 306
    invoke-static {v2}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_20

    .line 307
    invoke-virtual {v3, v2}, Lcrh;->iy(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 310
    :goto_6
    if-eqz v0, :cond_19

    .line 312
    :try_start_1
    invoke-virtual {v3, v2}, Lcrh;->iz(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/accounts/AccountsException; {:try_start_1 .. :try_end_1} :catch_1

    .line 318
    :cond_19
    :goto_7
    if-nez v0, :cond_1a

    .line 319
    const-string v0, "TestLauncherActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Invalid account: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    invoke-virtual {p0, v5}, Lcom/google/android/sidekick/main/TestLauncherActivity;->setResult(I)V

    .line 321
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->finish()V

    goto/16 :goto_1

    .line 314
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_7

    .line 325
    :cond_1a
    new-instance v1, Lewa;

    invoke-direct {v1, p0}, Lewa;-><init>(Lcom/google/android/sidekick/main/TestLauncherActivity;)V

    new-array v2, v4, [Landroid/accounts/Account;

    aput-object v0, v2, v5

    invoke-virtual {v1, v2}, Lewa;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_1

    .line 327
    :cond_1b
    const-string v1, "com.google.android.sidekick.main.FETCH_EXPERIMENT_OVERRIDES"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 328
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mGsaPreferenceController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    const-string v1, "now_opted_in_experiments"

    invoke-static {}, Lijp;->aXb()Lijp;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcyg;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 330
    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 331
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 332
    const-string v2, "experimentOverrides"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 333
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/sidekick/main/TestLauncherActivity;->setResult(ILandroid/content/Intent;)V

    .line 334
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->finish()V

    goto/16 :goto_1

    .line 336
    :cond_1c
    const-string v1, "com.google.android.sidekick.main.SET_EXPERIMENT_OVERRIDES"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 337
    const-string v0, "experimentOverrides"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 338
    if-eqz v0, :cond_1d

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 339
    :cond_1d
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mGsaPreferenceController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v1, "now_opted_in_experiments"

    invoke-interface {v0, v1}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    .line 348
    :goto_8
    invoke-direct {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->awf()V

    .line 349
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->finish()V

    goto/16 :goto_1

    .line 342
    :cond_1e
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 343
    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 344
    iget-object v0, p0, Lcom/google/android/sidekick/main/TestLauncherActivity;->mGsaPreferenceController:Lchr;

    invoke-virtual {v0}, Lchr;->Ks()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    const-string v2, "now_opted_in_experiments"

    invoke-interface {v0, v2, v1}, Lcyh;->b(Ljava/lang/String;Ljava/util/Set;)Lcyh;

    move-result-object v0

    invoke-interface {v0}, Lcyh;->apply()V

    goto :goto_8

    .line 351
    :cond_1f
    const-string v1, "com.google.android.sidekick.main.LOAD_QP_CARD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 352
    const-string v0, "qp_card"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    :try_start_2
    invoke-static {v1, v0}, Ljsr;->c(Ljsr;[B)Ljsr;
    :try_end_2
    .catch Ljsq; {:try_start_2 .. :try_end_2} :catch_2

    invoke-static {v1}, Lewb;->a(Lang;)Lewb;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "qp_card"

    invoke-virtual {v0, v1, v2}, Lewb;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_2
    move-exception v0

    const-string v1, "TestLauncherActivity"

    const-string v2, "IO Exception: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v0, "Invalid protocol buffer.  Check the logs."

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_20
    move-object v0, v1

    goto/16 :goto_6
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 370
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 371
    invoke-virtual {p0, p1}, Lcom/google/android/sidekick/main/TestLauncherActivity;->setIntent(Landroid/content/Intent;)V

    .line 372
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 376
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 385
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 379
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/googlequicksearchbox/SearchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 380
    const-string v1, "android.intent.action.ASSIST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 381
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 382
    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 383
    const/4 v0, 0x1

    goto :goto_0

    .line 376
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public tvResults(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 933
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    const-string v1, "test"

    invoke-static {v0, v1}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil;->n(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/TestLauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 934
    return-void
.end method
