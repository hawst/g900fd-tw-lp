.class public Lcom/google/android/sidekick/shared/ui/NowProgressBar;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lfrg;


# static fields
.field private static final arU:Landroid/animation/TimeInterpolator;


# instance fields
.field private final cAj:Landroid/graphics/RectF;

.field private cAk:F

.field private cAl:J

.field private cAm:I

.field private cAn:Landroid/animation/ObjectAnimator;

.field private hV:J

.field private jY:I

.field private jZ:I

.field private ka:I

.field private lD:Z

.field private final ob:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Ldqs;->bGc:Ldqs;

    sput-object v0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->arU:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 38
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->ob:Landroid/graphics/Paint;

    .line 39
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAj:Landroid/graphics/RectF;

    .line 55
    invoke-direct {p0, p1}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->h(Landroid/content/Context;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->ob:Landroid/graphics/Paint;

    .line 39
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAj:Landroid/graphics/RectF;

    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->h(Landroid/content/Context;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->ob:Landroid/graphics/Paint;

    .line 39
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAj:Landroid/graphics/RectF;

    .line 65
    invoke-direct {p0, p1}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->h(Landroid/content/Context;)V

    .line 66
    return-void
.end method

.method private a(Landroid/graphics/Canvas;FFIF)V
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setColor(I)V

    .line 233
    sget-object v0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->arU:Landroid/animation/TimeInterpolator;

    invoke-interface {v0, p5}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    mul-float/2addr v0, p2

    .line 234
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->ob:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, p3, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 235
    return-void
.end method

.method private h(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 70
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b008c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->jZ:I

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b008d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->jY:I

    .line 72
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b008e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->ka:I

    .line 73
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b008f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAm:I

    .line 76
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->setLayerType(ILandroid/graphics/Paint;)V

    .line 77
    return-void
.end method


# virtual methods
.method public final aCR()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAn:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAn:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAn:Landroid/animation/ObjectAnimator;

    .line 98
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->setTriggerPercentage(F)V

    .line 99
    return-void
.end method

.method public final aCS()V
    .locals 4

    .prologue
    .line 106
    const-string v0, "triggerPercentage"

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAk:F

    aput v3, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAn:Landroid/animation/ObjectAnimator;

    .line 109
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAn:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 110
    return-void
.end method

.method public final isRunning()Z
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->lD:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->getWidth()I

    move-result v0

    .line 140
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->getHeight()I

    move-result v1

    .line 141
    div-int/lit8 v6, v0, 0x2

    .line 142
    div-int/lit8 v7, v1, 0x2

    .line 143
    iget-boolean v2, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->lD:Z

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAl:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_d

    .line 146
    :cond_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    .line 147
    iget-wide v4, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->hV:J

    sub-long v4, v2, v4

    const-wide/16 v8, 0x7d0

    rem-long/2addr v4, v8

    .line 148
    iget-wide v8, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->hV:J

    sub-long v8, v2, v8

    const-wide/16 v10, 0x7d0

    div-long/2addr v8, v10

    .line 149
    long-to-float v4, v4

    const/high16 v5, 0x41a00000    # 20.0f

    div-float v10, v4, v5

    .line 152
    iget-boolean v4, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->lD:Z

    if-nez v4, :cond_3

    .line 154
    iget-wide v4, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAl:J

    sub-long v4, v2, v4

    const-wide/16 v12, 0x3e8

    cmp-long v4, v4, v12

    if-ltz v4, :cond_2

    .line 155
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAl:J

    .line 220
    :cond_1
    :goto_0
    return-void

    .line 161
    :cond_2
    iget-wide v4, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAl:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    rem-long/2addr v2, v4

    .line 162
    long-to-float v2, v2

    const/high16 v3, 0x41200000    # 10.0f

    div-float/2addr v2, v3

    .line 163
    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    .line 164
    int-to-float v0, v0

    sget-object v3, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->arU:Landroid/animation/TimeInterpolator;

    invoke-interface {v3, v2}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v2

    mul-float/2addr v0, v2

    .line 165
    iget-object v2, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAj:Landroid/graphics/RectF;

    int-to-float v3, v6

    sub-float/2addr v3, v0

    const/4 v4, 0x0

    int-to-float v5, v6

    add-float/2addr v0, v5

    int-to-float v1, v1

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 166
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAj:Landroid/graphics/RectF;

    sget-object v1, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;Landroid/graphics/Region$Op;)Z

    .line 170
    :cond_3
    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-nez v0, :cond_9

    .line 171
    iget v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->jZ:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 190
    :goto_1
    const/4 v0, 0x0

    cmpl-float v0, v10, v0

    if-ltz v0, :cond_4

    const/high16 v0, 0x41c80000    # 25.0f

    cmpg-float v0, v10, v0

    if-gtz v0, :cond_4

    .line 191
    const/high16 v0, 0x41c80000    # 25.0f

    add-float/2addr v0, v10

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float v5, v0, v1

    .line 192
    int-to-float v2, v6

    int-to-float v3, v7

    iget v4, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->jZ:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->a(Landroid/graphics/Canvas;FFIF)V

    .line 194
    :cond_4
    const/4 v0, 0x0

    cmpl-float v0, v10, v0

    if-ltz v0, :cond_5

    const/high16 v0, 0x42480000    # 50.0f

    cmpg-float v0, v10, v0

    if-gtz v0, :cond_5

    .line 195
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, v10

    const/high16 v1, 0x42c80000    # 100.0f

    div-float v5, v0, v1

    .line 196
    int-to-float v2, v6

    int-to-float v3, v7

    iget v4, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->jY:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->a(Landroid/graphics/Canvas;FFIF)V

    .line 198
    :cond_5
    const/high16 v0, 0x41c80000    # 25.0f

    cmpl-float v0, v10, v0

    if-ltz v0, :cond_6

    const/high16 v0, 0x42960000    # 75.0f

    cmpg-float v0, v10, v0

    if-gtz v0, :cond_6

    .line 199
    const/high16 v0, 0x41c80000    # 25.0f

    sub-float v0, v10, v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float v5, v0, v1

    .line 200
    int-to-float v2, v6

    int-to-float v3, v7

    iget v4, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->ka:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->a(Landroid/graphics/Canvas;FFIF)V

    .line 202
    :cond_6
    const/high16 v0, 0x42480000    # 50.0f

    cmpl-float v0, v10, v0

    if-ltz v0, :cond_7

    const/high16 v0, 0x42c80000    # 100.0f

    cmpg-float v0, v10, v0

    if-gtz v0, :cond_7

    .line 203
    const/high16 v0, 0x42480000    # 50.0f

    sub-float v0, v10, v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float v5, v0, v1

    .line 204
    int-to-float v2, v6

    int-to-float v3, v7

    iget v4, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAm:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->a(Landroid/graphics/Canvas;FFIF)V

    .line 206
    :cond_7
    const/high16 v0, 0x42960000    # 75.0f

    cmpl-float v0, v10, v0

    if-ltz v0, :cond_8

    const/high16 v0, 0x42c80000    # 100.0f

    cmpg-float v0, v10, v0

    if-gtz v0, :cond_8

    .line 207
    const/high16 v0, 0x42960000    # 75.0f

    sub-float v0, v10, v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float v5, v0, v1

    .line 208
    int-to-float v2, v6

    int-to-float v3, v7

    iget v4, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->jZ:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->a(Landroid/graphics/Canvas;FFIF)V

    .line 212
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->postInvalidateOnAnimation()V

    goto/16 :goto_0

    .line 173
    :cond_9
    const/4 v0, 0x0

    cmpl-float v0, v10, v0

    if-ltz v0, :cond_a

    const/high16 v0, 0x41c80000    # 25.0f

    cmpg-float v0, v10, v0

    if-gez v0, :cond_a

    .line 174
    iget v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAm:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    goto/16 :goto_1

    .line 175
    :cond_a
    const/high16 v0, 0x41c80000    # 25.0f

    cmpl-float v0, v10, v0

    if-ltz v0, :cond_b

    const/high16 v0, 0x42480000    # 50.0f

    cmpg-float v0, v10, v0

    if-gez v0, :cond_b

    .line 176
    iget v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->jZ:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    goto/16 :goto_1

    .line 177
    :cond_b
    const/high16 v0, 0x42480000    # 50.0f

    cmpl-float v0, v10, v0

    if-ltz v0, :cond_c

    const/high16 v0, 0x42960000    # 75.0f

    cmpg-float v0, v10, v0

    if-gez v0, :cond_c

    .line 178
    iget v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->jY:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    goto/16 :goto_1

    .line 180
    :cond_c
    iget v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->ka:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    goto/16 :goto_1

    .line 215
    :cond_d
    iget v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAk:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAk:F

    float-to-double v0, v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1

    .line 216
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->ob:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->jZ:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 217
    int-to-float v0, v6

    int-to-float v1, v7

    int-to-float v2, v6

    iget v3, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAk:F

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->ob:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method public setTriggerPercentage(F)V
    .locals 2

    .prologue
    .line 84
    iput p1, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAk:F

    .line 85
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->hV:J

    .line 86
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->postInvalidate()V

    .line 87
    return-void
.end method

.method public final start()V
    .locals 2

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->lD:Z

    if-nez v0, :cond_0

    .line 115
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAk:F

    .line 116
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->hV:J

    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->lD:Z

    .line 118
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->postInvalidate()V

    .line 120
    :cond_0
    return-void
.end method

.method public final stop()V
    .locals 2

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->lD:Z

    if-eqz v0, :cond_0

    .line 125
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAk:F

    .line 126
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->cAl:J

    .line 127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->lD:Z

    .line 128
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->postInvalidate()V

    .line 130
    :cond_0
    return-void
.end method
