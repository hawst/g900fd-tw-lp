.class public final Lcom/google/android/sidekick/shared/util/SecondScreenUtil;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/content/Context;Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 71
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 72
    const-string v1, "com.google.android.googlequicksearchbox"

    const-string v2, "com.google.android.sidekick.main.secondscreen.SecondScreenCardsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    const-string v1, "options"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 74
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 75
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lfjx;->ctF:Lefw;

    invoke-static {v1, v2, v0}, Lefy;->a(Ljava/lang/String;Lefw;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 101
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 103
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->aEt()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 104
    const-string v1, "com.google.android.googlequicksearchbox"

    const-string v2, "com.google.android.sidekick.main.secondscreen.SecondScreenReaderActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 105
    const-string v1, "options"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 106
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 107
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lfjx;->ctG:Lefw;

    invoke-static {v1, v2, v0}, Lefy;->a(Ljava/lang/String;Lefw;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 111
    :goto_0
    return-object v0

    .line 110
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public static n(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 123
    new-instance v0, Ljbj;

    invoke-direct {v0}, Ljbj;-><init>()V

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljbj;->om(I)Ljbj;

    move-result-object v0

    .line 125
    new-instance v1, Ljbk;

    invoke-direct {v1}, Ljbk;-><init>()V

    invoke-virtual {v1, p1}, Ljbk;->sq(Ljava/lang/String;)Ljbk;

    move-result-object v1

    iput-object v1, v0, Ljbj;->dYC:Ljbk;

    .line 127
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const/16 v3, 0x41

    aput v3, v1, v2

    iput-object v1, v0, Ljbj;->dYx:[I

    .line 128
    new-instance v1, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    invoke-direct {v1}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->a(Ljbj;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v0

    const v1, 0x7f0a0447

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->mn(Ljava/lang/String;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v0

    const-string v1, "tv"

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->mq(Ljava/lang/String;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->jS(I)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
