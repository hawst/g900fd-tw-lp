.class public Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private cAQ:Landroid/view/View;

.field private cAR:Landroid/view/View;

.field private cAS:Landroid/widget/Button;

.field private cAT:Landroid/widget/Button;

.field private cAU:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 40
    const v0, 0x7f1100f8

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAQ:Landroid/view/View;

    .line 41
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAQ:Landroid/view/View;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAQ:Landroid/view/View;

    const v1, 0x7f1100f9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAU:Landroid/view/View;

    .line 43
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAU:Landroid/view/View;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    const v0, 0x7f1100fa

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAR:Landroid/view/View;

    .line 45
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAR:Landroid/view/View;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAR:Landroid/view/View;

    const v1, 0x7f1100fb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAS:Landroid/widget/Button;

    .line 47
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAS:Landroid/widget/Button;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAR:Landroid/view/View;

    const v1, 0x7f1100fc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAT:Landroid/widget/Button;

    .line 49
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAT:Landroid/widget/Button;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 67
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAQ:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 68
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAU:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAU:Landroid/view/View;

    invoke-static {v1}, Leot;->bc(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 70
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAQ:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int v5, v1, v0

    .line 72
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAS:Landroid/widget/Button;

    invoke-static {v1}, Leot;->bc(Landroid/view/View;)I

    move-result v1

    add-int/2addr v1, v0

    .line 73
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAR:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 75
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 79
    div-int/lit8 v1, v1, 0x2

    .line 80
    iget-object v3, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAS:Landroid/widget/Button;

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setMaxWidth(I)V

    .line 81
    iget-object v3, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAT:Landroid/widget/Button;

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setMaxWidth(I)V

    .line 83
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 85
    iget-object v6, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAS:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/widget/Button;->getLayout()Landroid/text/Layout;

    move-result-object v7

    if-eqz v7, :cond_1

    move v1, v2

    move v3, v2

    :goto_0
    invoke-virtual {v6}, Landroid/widget/Button;->getLineCount()I

    move-result v4

    if-ge v1, v4, :cond_2

    invoke-virtual {v7, v1}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v4

    float-to-int v4, v4

    if-le v4, v3, :cond_0

    move v3, v4

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v3, v2

    .line 86
    :cond_2
    if-lez v3, :cond_3

    .line 87
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAS:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    .line 88
    sub-int v1, v5, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 89
    iget-object v2, p0, Lcom/google/android/sidekick/shared/ui/VoiceOfGoogleLayout;->cAR:Landroid/view/View;

    invoke-static {v2}, Leot;->ay(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 90
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 95
    :cond_3
    :goto_1
    return-void

    .line 92
    :cond_4
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_1
.end method
