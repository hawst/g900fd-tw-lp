.class public Lcom/google/android/sidekick/main/training/TrainingClosetActivity;
.super Lhfa;
.source "PG"


# instance fields
.field private as:Z

.field private crP:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Lhfa;-><init>()V

    .line 64
    iput-boolean v0, p0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->crP:Z

    .line 65
    iput-boolean v0, p0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->as:Z

    return-void
.end method

.method public static synthetic a(Lcom/google/android/sidekick/main/training/TrainingClosetActivity;)Lgql;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->mVelvetServices:Lgql;

    return-object v0
.end method

.method private a(Landroid/app/Fragment;Z)V
    .locals 4

    .prologue
    const v3, 0x7f110298

    .line 300
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->isDestroyed()Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_1

    .line 301
    const-string v0, "TrainingClosetActivity"

    const-string v1, "FragmentManager destroyed -> cannot start new Fragment"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    :goto_1
    return-void

    .line 300
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 305
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 306
    if-eqz p2, :cond_2

    .line 307
    invoke-virtual {v0, v3, p1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 308
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 312
    :goto_2
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_1

    .line 310
    :cond_2
    const-string v1, "TrainingClosetActivity.BaseFragment"

    invoke-virtual {v0, v3, p1, v1}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    goto :goto_2
.end method

.method public static synthetic a(Lcom/google/android/sidekick/main/training/TrainingClosetActivity;Z)Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->crP:Z

    return v0
.end method

.method private azY()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 222
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 223
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 225
    if-eqz v1, :cond_2

    .line 226
    const-string v2, "com.google.android.apps.sidekick.training.EXTRA_SHOW_PLACES"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 227
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "TrainingClosetActivity.BaseFragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 228
    invoke-direct {p0, v4}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->fo(Z)V

    .line 245
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    const-string v2, "com.google.android.search.core.preferences.ARGUMENT_QUESTION"

    const-string v3, "com.google.android.apps.sidekick.training.EXTRA_TARGET_QUESTION"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 236
    const-string v2, "com.google.android.search.core.preferences.ARGUMENT_ATTRIBUTE"

    const-string v3, "com.google.android.apps.sidekick.training.EXTRA_TARGET_ATTRIBUTE"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 242
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "TrainingClosetActivity.BaseFragment"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 243
    invoke-direct {p0, v0, v4}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->b(Landroid/os/Bundle;Z)V

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/sidekick/main/training/TrainingClosetActivity;)Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    return-object v0
.end method

.method private b(Landroid/os/Bundle;Z)V
    .locals 1

    .prologue
    .line 293
    const-class v0, Lfhs;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p1}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v0

    .line 295
    invoke-direct {p0, v0, p2}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->a(Landroid/app/Fragment;Z)V

    .line 296
    return-void
.end method

.method private fo(Z)V
    .locals 1

    .prologue
    .line 288
    new-instance v0, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;

    invoke-direct {v0}, Lcom/google/android/search/core/preferences/cards/MyPlacesSettingsFragment;-><init>()V

    .line 289
    invoke-direct {p0, v0, p1}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->a(Landroid/app/Fragment;Z)V

    .line 290
    return-void
.end method

.method private u(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 250
    invoke-virtual {v0, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 252
    if-eqz v1, :cond_0

    .line 253
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 255
    if-eqz p2, :cond_0

    .line 256
    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 259
    :cond_0
    return-void
.end method


# virtual methods
.method public final Q(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->b(Landroid/os/Bundle;Z)V

    .line 267
    return-void
.end method

.method public final azX()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 210
    iget-boolean v0, p0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->as:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->crP:Z

    if-eqz v0, :cond_0

    .line 211
    const-string v0, "TRAINING_CLOSET_FETCHER"

    invoke-direct {p0, v0, v2}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->u(Ljava/lang/String;Z)V

    .line 213
    const-string v0, "TrainingClosetActivity.BaseFragment"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->u(Ljava/lang/String;Z)V

    .line 214
    invoke-direct {p0}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->azY()V

    .line 216
    iput-boolean v2, p0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->crP:Z

    .line 218
    :cond_0
    return-void
.end method

.method public final azZ()V
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->fo(Z)V

    .line 285
    return-void
.end method

.method public getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 317
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 318
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DE()Lcxs;

    move-result-object v0

    invoke-virtual {v0}, Lcxs;->TI()Lcyg;

    move-result-object v0

    return-object v0
.end method

.method public final lO(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 275
    if-eqz v0, :cond_0

    .line 276
    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 278
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f110298

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    .line 188
    instance-of v1, v0, Lfhs;

    if-eqz v1, :cond_0

    .line 189
    check-cast v0, Lfhs;

    invoke-virtual {v0}, Lfhs;->aAa()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    :goto_0
    return-void

    .line 193
    :cond_0
    invoke-super {p0}, Lhfa;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 69
    invoke-static {}, Lckw;->OZ()Lckw;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lckw;->Pd()Z

    move-result v1

    if-nez v1, :cond_0

    .line 71
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->BK()Lcke;

    move-result-object v1

    .line 72
    invoke-virtual {v0, v1}, Lckw;->a(Lcke;)V

    .line 75
    :cond_0
    invoke-super {p0, p1}, Lhfa;->onCreate(Landroid/os/Bundle;)V

    .line 79
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->lO(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->aCL()Lfqn;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lfqn;->jn(I)V

    .line 84
    invoke-direct {p0}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->azY()V

    .line 85
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6

    .prologue
    .line 323
    invoke-super {p0, p1}, Lhfa;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 324
    const-string v2, "now_settings"

    iget-object v0, p0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v0

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v3

    new-instance v0, Lgpk;

    invoke-direct {v0, p0}, Lgpk;-><init>(Landroid/content/Context;)V

    const-string v1, "now_settings"

    invoke-virtual {v0, v1}, Lgpk;->nv(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v0

    invoke-virtual {v0}, Lcrh;->Su()Z

    move-result v5

    move-object v0, p1

    move-object v1, p0

    invoke-static/range {v0 .. v5}, Lgbk;->a(Landroid/view/Menu;Landroid/app/Activity;Ljava/lang/String;Landroid/accounts/Account;Landroid/net/Uri;Z)V

    .line 328
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 139
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 140
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->onBackPressed()V

    .line 141
    const/4 v0, 0x1

    .line 143
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lhfa;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 148
    iput-boolean v3, p0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->as:Z

    .line 149
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    .line 150
    invoke-virtual {v0}, Lfdb;->ayc()Lfdn;

    move-result-object v1

    .line 152
    invoke-virtual {v0}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v0

    .line 155
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->isChangingConfigurations()Z

    move-result v2

    if-nez v2, :cond_0

    .line 158
    invoke-interface {v1}, Lfdn;->isDirty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 159
    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Lfaq;->iu(I)V

    .line 171
    :goto_0
    const-string v0, "TRAINING_CLOSET_FETCHER"

    invoke-direct {p0, v0, v3}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->u(Ljava/lang/String;Z)V

    .line 173
    :cond_0
    invoke-super {p0}, Lhfa;->onPause()V

    .line 174
    return-void

    .line 165
    :cond_1
    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Lfaq;->ix(I)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 178
    invoke-super {p0}, Lhfa;->onResume()V

    .line 179
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->as:Z

    .line 180
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->azX()V

    .line 181
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DF()Lcin;

    move-result-object v0

    invoke-interface {v0}, Lcin;->Kz()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->finish()V

    .line 182
    :cond_0
    return-void
.end method

.method protected final wd()I
    .locals 1

    .prologue
    .line 89
    const v0, 0x7f0400dc

    return v0
.end method

.method protected final we()Lfoh;
    .locals 2

    .prologue
    .line 94
    new-instance v0, Lfho;

    iget-object v1, p0, Lcom/google/android/sidekick/main/training/TrainingClosetActivity;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-direct {v0, p0, p0, v1}, Lfho;-><init>(Lcom/google/android/sidekick/main/training/TrainingClosetActivity;Landroid/app/Activity;Landroid/view/View;)V

    return-object v0
.end method
