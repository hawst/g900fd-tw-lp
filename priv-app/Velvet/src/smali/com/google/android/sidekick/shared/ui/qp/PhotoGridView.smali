.class public Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private cCf:[Ljbb;

.field private cCg:[Ljbb;

.field private mCardContainer:Lfmt;

.field private mEntry:Lizj;

.field private mImageLoader:Lesm;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method private static a(Ljbb;)F
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Ljbb;->dXK:Ljcn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljbb;->dXK:Ljcn;

    invoke-virtual {v0}, Ljcn;->bgH()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljbb;->dXK:Ljcn;

    invoke-virtual {v0}, Ljcn;->bgI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Ljbb;->dXK:Ljcn;

    invoke-virtual {v0}, Ljcn;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Ljbb;->dXK:Ljcn;

    invoke-virtual {v1}, Ljcn;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 266
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;)Lfmt;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->mCardContainer:Lfmt;

    return-object v0
.end method

.method private aDu()I
    .locals 3

    .prologue
    const/4 v0, 0x3

    .line 249
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->cCf:[Ljbb;

    array-length v1, v1

    if-ge v1, v0, :cond_1

    .line 250
    const/4 v0, 0x1

    .line 256
    :cond_0
    :goto_0
    return v0

    .line 251
    :cond_1
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->cCf:[Ljbb;

    array-length v1, v1

    if-lt v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->cCf:[Ljbb;

    array-length v1, v1

    const/4 v2, 0x7

    if-ge v1, v2, :cond_0

    .line 252
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private h(IIII)V
    .locals 15

    .prologue
    .line 147
    sub-int v2, p2, p1

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0177

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/2addr v2, v3

    sub-int v11, p3, v2

    .line 151
    const/4 v2, 0x0

    move v10, v2

    move/from16 v2, p1

    .line 152
    :goto_0
    move/from16 v0, p2

    if-ge v2, v0, :cond_0

    .line 153
    iget-object v3, p0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->cCf:[Ljbb;

    aget-object v3, v3, v2

    invoke-static {v3}, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->a(Ljbb;)F

    move-result v3

    add-float/2addr v3, v10

    .line 152
    add-int/lit8 v2, v2, 0x1

    move v10, v3

    goto :goto_0

    .line 157
    :cond_0
    new-instance v12, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v12, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v12, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0177

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v12, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 158
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0176

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    move/from16 v8, p1

    .line 160
    :goto_1
    move/from16 v0, p2

    if-ge v8, v0, :cond_2

    .line 161
    int-to-float v2, v11

    div-float/2addr v2, v10

    iget-object v3, p0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->cCf:[Ljbb;

    aget-object v3, v3, v8

    invoke-static {v3}, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->a(Ljbb;)F

    move-result v3

    mul-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v3, v2

    .line 165
    move/from16 v0, p4

    if-le v0, v13, :cond_1

    .line 166
    iget-object v2, p0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->cCf:[Ljbb;

    aget-object v2, v2, v8

    iget-object v2, v2, Ljbb;->dXJ:Ljcn;

    .line 170
    :goto_2
    new-instance v14, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v14, v4}, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->mImageLoader:Lesm;

    invoke-virtual {v14, v2, v4}, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->a(Landroid/net/Uri;Lesm;)V

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v14, v2}, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    move/from16 v0, p4

    invoke-direct {v2, v3, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d0177

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v14, v2}, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->cCg:[Ljbb;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    new-instance v9, Lfum;

    invoke-direct {v9, p0}, Lfum;-><init>(Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;)V

    new-instance v2, Lfun;

    iget-object v4, p0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->mCardContainer:Lfmt;

    iget-object v5, p0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->mEntry:Lizj;

    const/16 v6, 0x4f

    move-object v3, p0

    invoke-direct/range {v2 .. v9}, Lfun;-><init>(Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;Lfmt;Lizj;ILjava/util/List;ILemy;)V

    invoke-virtual {v14, v2}, Lcom/google/android/search/shared/ui/CrossfadingWebImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v12, v14}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 160
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 168
    :cond_1
    iget-object v2, p0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->cCf:[Ljbb;

    aget-object v2, v2, v8

    iget-object v2, v2, Ljbb;->dXI:Ljcn;

    goto :goto_2

    .line 172
    :cond_2
    invoke-virtual {p0, v12}, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->addView(Landroid/view/View;)V

    .line 173
    return-void
.end method


# virtual methods
.method public final a(Lesm;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->mImageLoader:Lesm;

    .line 68
    return-void
.end method

.method public final a(Lfmt;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->mCardContainer:Lfmt;

    .line 81
    return-void
.end method

.method public final a([Ljbb;)V
    .locals 2

    .prologue
    const/16 v1, 0x9

    .line 57
    iput-object p1, p0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->cCg:[Ljbb;

    .line 58
    array-length v0, p1

    if-le v0, v1, :cond_0

    .line 59
    const/4 v0, 0x0

    invoke-static {p1, v0, v1}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljbb;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->cCf:[Ljbb;

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_0
    iput-object p1, p0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->cCf:[Ljbb;

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 16

    .prologue
    .line 85
    move/from16 v0, p1

    move/from16 v1, p3

    if-eq v0, v1, :cond_c

    .line 87
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->removeAllViews()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0175

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct/range {p0 .. p0}, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->aDu()I

    move-result v3

    div-int v10, v2, v3

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->cCf:[Ljbb;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v6, v4, v3

    invoke-static {v6}, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->a(Ljbb;)F

    move-result v6

    int-to-float v7, v10

    mul-float/2addr v6, v7

    add-float/2addr v2, v6

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    move/from16 v0, p1

    int-to-float v3, v0

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    const/4 v3, 0x3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v7

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x1

    if-le v7, v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->cCf:[Ljbb;

    array-length v2, v2

    new-array v11, v2, [I

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->cCf:[Ljbb;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->cCf:[Ljbb;

    aget-object v3, v3, v2

    invoke-static {v3}, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->a(Ljbb;)F

    move-result v3

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    aput v3, v11, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    array-length v2, v11

    new-array v12, v2, [I

    const/4 v2, 0x0

    const/4 v3, 0x0

    aget v3, v11, v3

    aput v3, v12, v2

    const/4 v2, 0x1

    :goto_2
    array-length v3, v11

    if-ge v2, v3, :cond_2

    add-int/lit8 v3, v2, -0x1

    aget v3, v12, v3

    aget v4, v11, v2

    add-int/2addr v3, v4

    aput v3, v12, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    array-length v2, v11

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v3, v7, 0x1

    filled-new-array {v2, v3}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[I

    array-length v3, v11

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v4, v7, 0x1

    filled-new-array {v3, v4}, [I

    move-result-object v3

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v4, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [[I

    const/4 v4, 0x1

    :goto_3
    array-length v8, v11

    if-gt v4, v8, :cond_3

    aget-object v8, v2, v4

    const/4 v9, 0x1

    add-int/lit8 v13, v4, -0x1

    aget v13, v12, v13

    aput v13, v8, v9

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_3
    const/4 v4, 0x1

    :goto_4
    if-gt v4, v7, :cond_4

    const/4 v8, 0x1

    aget-object v8, v2, v8

    const/4 v9, 0x0

    aget v9, v11, v9

    aput v9, v8, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_4
    const/4 v4, 0x2

    :goto_5
    array-length v8, v11

    if-gt v4, v8, :cond_8

    const/4 v8, 0x2

    move v9, v8

    :goto_6
    if-gt v9, v7, :cond_7

    aget-object v8, v2, v4

    const v13, 0x7fffffff

    aput v13, v8, v9

    const/4 v8, 0x1

    :goto_7
    if-ge v8, v4, :cond_6

    aget-object v13, v2, v8

    add-int/lit8 v14, v9, -0x1

    aget v13, v13, v14

    add-int/lit8 v14, v4, -0x1

    aget v14, v12, v14

    add-int/lit8 v15, v8, -0x1

    aget v15, v12, v15

    sub-int/2addr v14, v15

    invoke-static {v13, v14}, Ljava/lang/Math;->max(II)I

    move-result v13

    aget-object v14, v2, v4

    aget v14, v14, v9

    if-ge v13, v14, :cond_5

    aget-object v14, v2, v4

    aput v13, v14, v9

    aget-object v13, v3, v4

    aput v8, v13, v9

    :cond_5
    add-int/lit8 v8, v8, 0x1

    goto :goto_7

    :cond_6
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_6

    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_8
    add-int/lit8 v2, v7, -0x1

    new-array v8, v2, [I

    array-length v2, v11

    move v4, v7

    :goto_8
    const/4 v7, 0x1

    if-le v4, v7, :cond_9

    add-int/lit8 v7, v4, -0x2

    aget-object v2, v3, v2

    aget v2, v2, v4

    aput v2, v8, v7

    add-int/lit8 v4, v4, -0x1

    goto :goto_8

    :cond_9
    array-length v7, v8

    const/4 v2, 0x0

    move v4, v2

    move v3, v6

    move v2, v5

    :goto_9
    if-ge v4, v7, :cond_b

    aget v5, v8, v4

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v2, v5, v1, v10}, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->h(IIII)V

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v5

    goto :goto_9

    :cond_a
    move v2, v5

    move v3, v6

    :cond_b
    invoke-direct/range {p0 .. p0}, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->aDu()I

    move-result v4

    if-ge v3, v4, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->cCf:[Ljbb;

    array-length v3, v3

    if-ge v2, v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->cCf:[Ljbb;

    array-length v3, v3

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v2, v3, v1, v10}, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->h(IIII)V

    .line 89
    :cond_c
    invoke-super/range {p0 .. p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    .line 90
    return-void
.end method

.method public final z(Lizj;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/sidekick/shared/ui/qp/PhotoGridView;->mEntry:Lizj;

    .line 73
    return-void
.end method
