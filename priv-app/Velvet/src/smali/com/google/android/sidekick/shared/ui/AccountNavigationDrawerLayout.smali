.class public Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;
.super Lejp;
.source "PG"


# instance fields
.field private czD:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lejp;-><init>(Landroid/content/Context;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lejp;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lejp;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method


# virtual methods
.method public final aCL()Lfqn;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->atE()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lfqn;

    return-object v0
.end method

.method protected final atD()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->yW:Landroid/graphics/Rect;

    if-eqz v0, :cond_1

    .line 50
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->cbF:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->cbE:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 52
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->cbF:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 54
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->yW:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-static {v0, v2, v1, v2, v2}, Leot;->a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)V

    .line 55
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->cbF:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->atE()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lfqn;

    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->yW:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lfqn;->b(Landroid/graphics/Rect;)V

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->czD:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 60
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->czD:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 62
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->yW:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 63
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->czD:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 66
    :cond_1
    return-void
.end method

.method public atF()V
    .locals 3

    .prologue
    .line 36
    invoke-super {p0}, Lejp;->atF()V

    .line 38
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->czD:Landroid/view/View;

    .line 39
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 41
    const/16 v1, 0x30

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 42
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->czD:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 43
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->czD:Landroid/view/View;

    const v1, 0x7f0b00cc

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 44
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->cbE:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->czD:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 45
    return-void
.end method

.method public final atG()Landroid/view/View;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Lfqn;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lfqn;-><init>(Landroid/content/Context;)V

    return-object v0
.end method
