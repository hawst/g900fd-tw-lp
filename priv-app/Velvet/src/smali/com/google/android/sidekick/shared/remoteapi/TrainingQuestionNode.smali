.class public Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private cjc:Ljava/util/List;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cym:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

.field private final cyn:[Ljdf;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cyo:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 147
    new-instance v0, Lfpb;

    invoke-direct {v0}, Lfpb;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;[Ljdf;)V
    .locals 1
    .param p2    # [Ljdf;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cjc:Ljava/util/List;

    .line 32
    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cyo:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 36
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    iput-object p1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cym:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    .line 38
    iput-object p2, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cyn:[Ljdf;

    .line 39
    return-void
.end method

.method public static x(Ljava/util/Collection;)Ljava/util/List;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 87
    invoke-static {p0}, Lilw;->y(Ljava/lang/Iterable;)Ljava/util/LinkedList;

    move-result-object v1

    .line 89
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 90
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 91
    invoke-interface {v1, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 92
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    iget-object v3, v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cjc:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 94
    iget-object v0, v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cjc:Ljava/util/List;

    invoke-interface {v1, v4, v0}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    goto :goto_0

    .line 97
    :cond_1
    return-object v2
.end method


# virtual methods
.method public final aCA()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cyo:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 107
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cyo:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    iget-object v0, v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cym:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->c(Ljdf;)Z

    move-result v0

    return v0

    .line 105
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final aCx()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cyo:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    return-object v0
.end method

.method public final aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cym:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    return-object v0
.end method

.method public final aCz()Ljava/util/List;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cjc:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cjc:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;)V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p1, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cyo:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Child should not have a parent yet"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 49
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cjc:Ljava/util/List;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cjc:Ljava/util/List;

    .line 52
    :cond_0
    iput-object p0, p1, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cyo:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 53
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cjc:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    return-void

    .line 48
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljdf;)Z
    .locals 9
    .param p1    # Ljdf;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 116
    iget-object v2, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cyn:[Ljdf;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cyn:[Ljdf;

    array-length v2, v2

    if-nez v2, :cond_1

    .line 126
    :cond_0
    :goto_0
    return v0

    .line 118
    :cond_1
    if-eqz p1, :cond_4

    .line 119
    iget-object v4, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cyn:[Ljdf;

    array-length v5, v4

    move v3, v1

    :goto_1
    if-ge v3, v5, :cond_4

    aget-object v2, v4, v3

    .line 120
    iget-object v6, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cyo:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    iget-object v6, v6, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cym:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-virtual {v6}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    const-string v2, "Sidekick_TrainingQuestionNode"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Trying to compare unsupported question type: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    :goto_2
    if-nez v2, :cond_0

    .line 119
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 120
    :pswitch_0
    invoke-virtual {v2}, Ljdf;->bhh()Z

    move-result v2

    invoke-virtual {p1}, Ljdf;->bhh()Z

    move-result v6

    if-ne v2, v6, :cond_2

    move v2, v0

    goto :goto_2

    :cond_2
    move v2, v1

    goto :goto_2

    :pswitch_1
    invoke-virtual {v2}, Ljdf;->bhj()I

    move-result v2

    invoke-virtual {p1}, Ljdf;->bhj()I

    move-result v6

    if-ne v2, v6, :cond_3

    move v2, v0

    goto :goto_2

    :cond_3
    move v2, v1

    goto :goto_2

    :cond_4
    move v0, v1

    .line 126
    goto :goto_0

    .line 120
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    return v0
.end method

.method public final isVisible()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 76
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cyo:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    if-nez v1, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCA()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cyo:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->isVisible()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 177
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cym:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 178
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cjc:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 179
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cyn:[Ljdf;

    if-eqz v1, :cond_0

    .line 180
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cyn:[Ljdf;

    array-length v1, v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 181
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->cyn:[Ljdf;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 182
    invoke-static {v3, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 181
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 185
    :cond_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 187
    :cond_1
    return-void
.end method
