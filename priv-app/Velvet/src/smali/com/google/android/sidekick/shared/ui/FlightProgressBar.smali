.class public Lcom/google/android/sidekick/shared/ui/FlightProgressBar;
.super Landroid/widget/LinearLayout;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 19
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/ui/FlightProgressBar;->ed()V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/ui/FlightProgressBar;->ed()V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/ui/FlightProgressBar;->ed()V

    .line 36
    return-void
.end method

.method private ed()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/FlightProgressBar;->setOrientation(I)V

    .line 24
    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/FlightProgressBar;->setFocusable(Z)V

    .line 25
    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/FlightProgressBar;->setFocusableInTouchMode(Z)V

    .line 26
    return-void
.end method


# virtual methods
.method public final N(F)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x2

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 39
    const v0, 0x7f11030f

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/FlightProgressBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 40
    const v1, 0x7f11003d

    invoke-virtual {p0, v1}, Lcom/google/android/sidekick/shared/ui/FlightProgressBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 41
    cmpg-float v4, p1, v2

    if-gez v4, :cond_1

    move p1, v2

    .line 43
    :cond_0
    :goto_0
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v6, v5, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 44
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    sub-float v2, v3, p1

    invoke-direct {v0, v6, v5, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 45
    return-void

    .line 41
    :cond_1
    cmpl-float v2, p1, v3

    if-lez v2, :cond_0

    move p1, v3

    goto :goto_0
.end method

.method public final jo(I)V
    .locals 5

    .prologue
    const v2, 0x7f0b00bb

    .line 49
    const/4 v0, 0x0

    .line 51
    packed-switch p1, :pswitch_data_0

    .line 61
    :pswitch_0
    const v0, 0x7f0b00bc

    move v1, v2

    .line 65
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/FlightProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    if-eqz v0, :cond_0

    :goto_1
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const v0, 0x7f11030f

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/FlightProgressBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setColorFilter(I)V

    const v0, 0x7f1101dd

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/FlightProgressBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    const v0, 0x7f11003d

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/FlightProgressBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 66
    return-void

    .line 53
    :pswitch_1
    const v1, 0x7f0b00b9

    .line 54
    goto :goto_0

    .line 57
    :pswitch_2
    const v1, 0x7f0b00b7

    .line 58
    goto :goto_0

    :cond_0
    move v0, v1

    .line 65
    goto :goto_1

    .line 51
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
