.class public Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private aos:Landroid/view/View;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cod:F

.field private final iA:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 24
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->cod:F

    .line 26
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->iA:Landroid/graphics/Rect;

    .line 30
    invoke-direct {p0}, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->ed()V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->cod:F

    .line 26
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->iA:Landroid/graphics/Rect;

    .line 35
    invoke-direct {p0}, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->ed()V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->cod:F

    .line 26
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->iA:Landroid/graphics/Rect;

    .line 40
    invoke-direct {p0}, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->ed()V

    .line 41
    return-void
.end method

.method private ed()V
    .locals 2

    .prologue
    .line 45
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->setVerticalFadingEdgeEnabled(Z)V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d02ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->setFadingEdgeLength(I)V

    .line 48
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->setWillNotDraw(Z)V

    .line 50
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->setLayerType(ILandroid/graphics/Paint;)V

    .line 51
    return-void
.end method


# virtual methods
.method public final bg(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 103
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->aos:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->aos:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->removeView(Landroid/view/View;)V

    .line 106
    :cond_0
    iput-object p1, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->aos:Landroid/view/View;

    .line 107
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->aos:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setPivotX(F)V

    .line 108
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->aos:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setPivotY(F)V

    .line 109
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->aos:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->addView(Landroid/view/View;)V

    .line 110
    return-void
.end method

.method protected computeVerticalScrollRange()I
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->aos:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->getHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->aos:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->cod:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return v0
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const v3, 0x3f666666    # 0.9f

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 71
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eq v0, v4, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 72
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eq v0, v4, :cond_1

    :goto_1
    invoke-static {v1}, Lifv;->gY(Z)V

    .line 74
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->aos:Landroid/view/View;

    if-nez v0, :cond_2

    .line 75
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 100
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 71
    goto :goto_0

    :cond_1
    move v1, v2

    .line 72
    goto :goto_1

    .line 78
    :cond_2
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->aos:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->iA:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 79
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Leot;->au(Landroid/content/Context;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->iA:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->iA:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    .line 80
    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->aos:Landroid/view/View;

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/view/View;->measure(II)V

    .line 85
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 86
    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 88
    :goto_3
    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->aos:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v3

    int-to-float v2, v0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    const v1, 0x3f4ccccd    # 0.8f

    :goto_4
    iput v1, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->cod:F

    .line 89
    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->aos:Landroid/view/View;

    iget v2, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->cod:F

    invoke-virtual {v1, v2}, Landroid/view/View;->setScaleX(F)V

    .line 90
    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->aos:Landroid/view/View;

    iget v2, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->cod:F

    invoke-virtual {v1, v2}, Landroid/view/View;->setScaleY(F)V

    .line 93
    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->aos:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->cod:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->aos:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->cod:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/sidekick/main/feedback/SingleCardFrameLayout;->setMeasuredDimension(II)V

    goto :goto_2

    .line 86
    :cond_3
    const v0, 0x7fffffff

    goto :goto_3

    :cond_4
    move v1, v3

    .line 88
    goto :goto_4
.end method
