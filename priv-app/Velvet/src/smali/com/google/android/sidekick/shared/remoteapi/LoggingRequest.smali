.class public Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public bMW:Ljava/lang/String;

.field public bus:I

.field public cxQ:Ljava/lang/String;

.field public cxR:I

.field public cxS:Lixx;

.field public cxT:Ljava/util/Map;

.field public cxU:Ljava/lang/Integer;

.field public mEntry:Lizj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 139
    new-instance v0, Lfou;

    invoke-direct {v0}, Lfou;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->readFromParcel(Landroid/os/Parcel;)V

    .line 38
    return-void
.end method

.method public static aJ(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;

    invoke-direct {v0}, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;-><init>()V

    .line 59
    const/4 v1, 0x2

    iput v1, v0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->bus:I

    .line 60
    iput-object p0, v0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxQ:Ljava/lang/String;

    .line 61
    iput-object p1, v0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->bMW:Ljava/lang/String;

    .line 62
    return-object v0
.end method

.method public static b(Lizj;ILixx;Ljava/lang/Integer;)Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;
    .locals 2
    .param p2    # Lixx;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;

    invoke-direct {v0}, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;-><init>()V

    .line 46
    const/4 v1, 0x1

    iput v1, v0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->bus:I

    .line 47
    iput-object p0, v0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->mEntry:Lizj;

    .line 48
    iput p1, v0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxR:I

    .line 49
    iput-object p2, v0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxS:Lixx;

    .line 50
    iput-object p3, v0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxU:Ljava/lang/Integer;

    .line 51
    return-object v0
.end method

.method public static c(Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;

    invoke-direct {v0}, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;-><init>()V

    .line 82
    const/4 v1, 0x4

    iput v1, v0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->bus:I

    .line 83
    iput-object p0, v0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxQ:Ljava/lang/String;

    .line 84
    iput-object p1, v0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxT:Ljava/util/Map;

    .line 85
    return-object v0
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->bus:I

    .line 120
    const-class v0, Lizj;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->mEntry:Lizj;

    .line 121
    const-class v0, Lixx;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lixx;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxS:Lixx;

    .line 122
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxQ:Ljava/lang/String;

    .line 123
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->bMW:Ljava/lang/String;

    .line 124
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxR:I

    .line 125
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 126
    if-lez v0, :cond_0

    .line 127
    invoke-static {v0}, Lior;->mk(I)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxT:Ljava/util/Map;

    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 129
    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 130
    iget-object v3, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxT:Ljava/util/Map;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 133
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxU:Ljava/lang/Integer;

    .line 134
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxU:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 135
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxU:Ljava/lang/Integer;

    .line 137
    :cond_1
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    .line 95
    iget v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->bus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 96
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->mEntry:Lizj;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxS:Lixx;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxQ:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->bMW:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 100
    iget v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxR:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 101
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxT:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxT:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 102
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 111
    :goto_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxU:Ljava/lang/Integer;

    if-nez v0, :cond_3

    .line 112
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 116
    :goto_1
    return-void

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxT:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 105
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 106
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxT:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 107
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxT:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_2

    .line 109
    :cond_2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    goto :goto_0

    .line 114
    :cond_3
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->cxU:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1
.end method
