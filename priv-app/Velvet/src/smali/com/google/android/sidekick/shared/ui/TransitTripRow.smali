.class public Lcom/google/android/sidekick/shared/ui/TransitTripRow;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field cAL:Ljava/util/Map;

.field cAM:Landroid/view/View;

.field private mClock:Lemp;

.field private mRoute:Liyg;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 66
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->ed()V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->ed()V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 76
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->ed()V

    .line 77
    return-void
.end method

.method private static a(Lfmt;Landroid/view/LayoutInflater;ZLandroid/widget/LinearLayout;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 187
    const v0, 0x7f0401ac

    invoke-virtual {p1, v0, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 188
    const v0, 0x7f110359

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    .line 189
    const-string v2, "http://maps.gstatic.com/mapfiles/transit/iw2/8/walk.png"

    invoke-interface {p0}, Lfmt;->aAD()Lfml;

    move-result-object v3

    iget-object v3, v3, Lfml;->cvY:Lfnh;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/search/shared/ui/WebImageView;->a(Ljava/lang/String;Lesm;)V

    .line 191
    invoke-virtual {v0, v4}, Lcom/google/android/search/shared/ui/WebImageView;->setVisibility(I)V

    .line 192
    if-eqz p2, :cond_0

    .line 193
    const v0, 0x7f11035b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    .line 195
    const-string v2, "http://maps.gstatic.com/tactile/directions/cards/arrow-right-2x.png"

    invoke-interface {p0}, Lfmt;->aAD()Lfml;

    move-result-object v3

    iget-object v3, v3, Lfml;->cvY:Lfnh;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/search/shared/ui/WebImageView;->a(Ljava/lang/String;Lesm;)V

    .line 197
    invoke-virtual {v0, v4}, Lcom/google/android/search/shared/ui/WebImageView;->setVisibility(I)V

    .line 199
    :cond_0
    invoke-virtual {p3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 200
    return-object v1
.end method

.method private a(Liyk;Landroid/view/LayoutInflater;Landroid/view/View;)V
    .locals 12

    .prologue
    .line 210
    const v0, 0x7f11035a

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 211
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Leot;->au(Landroid/content/Context;)I

    move-result v1

    int-to-double v2, v1

    const-wide v4, 0x3fd51eb851eb851fL    # 0.33

    mul-double/2addr v4, v2

    .line 212
    const-string v2, ""

    .line 213
    iget-object v6, p1, Liyk;->dQq:[Liyj;

    array-length v7, v6

    const/4 v1, 0x0

    move v3, v1

    :goto_0
    if-ge v3, v7, :cond_5

    aget-object v8, v6, v3

    .line 214
    invoke-virtual {v8}, Liyj;->hasBackgroundColor()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v8}, Liyj;->getBackgroundColor()I

    move-result v1

    const/4 v9, -0x1

    if-eq v1, v9, :cond_4

    .line 216
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 217
    const v1, 0x7f0401a9

    const/4 v9, 0x0

    invoke-virtual {p2, v1, v0, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 219
    const v1, 0x7f110353

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 221
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 223
    invoke-static {v1}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->bt(Landroid/view/View;)I

    move-result v1

    int-to-double v10, v1

    cmpg-double v1, v10, v4

    if-gtz v1, :cond_0

    .line 224
    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 226
    :cond_0
    const-string v2, ""

    .line 229
    :cond_1
    const v1, 0x7f0401a9

    const/4 v9, 0x0

    invoke-virtual {p2, v1, v0, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 231
    const v1, 0x7f110353

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 232
    invoke-virtual {v8}, Liyj;->getBackgroundColor()I

    move-result v10

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 233
    const/4 v10, 0x0

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 235
    invoke-virtual {v8}, Liyj;->bcg()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-virtual {v8}, Liyj;->hasForegroundColor()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 236
    invoke-virtual {v8}, Liyj;->getForegroundColor()I

    move-result v10

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 237
    invoke-virtual {v8}, Liyj;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    invoke-static {v1}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->bt(Landroid/view/View;)I

    move-result v8

    int-to-double v10, v8

    cmpl-double v8, v10, v4

    if-lez v8, :cond_2

    .line 239
    const-string v8, ""

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    :cond_2
    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 213
    :cond_3
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_0

    .line 243
    :cond_4
    invoke-virtual {v8}, Liyj;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 244
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 245
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 247
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Liyj;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 250
    :cond_5
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 251
    const v1, 0x7f0401a9

    const/4 v3, 0x0

    invoke-virtual {p2, v1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 253
    const v1, 0x7f110353

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 254
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 256
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 258
    :cond_6
    return-void

    :cond_7
    move-object v1, v2

    goto :goto_2
.end method

.method private static bt(Landroid/view/View;)I
    .locals 1

    .prologue
    const/4 v0, -0x2

    .line 394
    invoke-virtual {p0, v0, v0}, Landroid/view/View;->measure(II)V

    .line 395
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    return v0
.end method

.method private static c(Landroid/content/Context;J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 389
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 390
    new-instance v1, Ljava/util/Date;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private ed()V
    .locals 1

    .prologue
    .line 80
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->cAL:Ljava/util/Map;

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->cAM:Landroid/view/View;

    .line 82
    return-void
.end method


# virtual methods
.method public final a(Liyg;Lemp;Lfmt;Landroid/view/LayoutInflater;ZZ)V
    .locals 14

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->mRoute:Liyg;

    .line 93
    move-object/from16 v0, p2

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->mClock:Lemp;

    .line 95
    iget-object v2, p0, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->mRoute:Liyg;

    iget-object v6, v2, Liyg;->dPL:Liyh;

    .line 96
    new-instance v7, Lgca;

    iget-object v2, p0, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->mRoute:Liyg;

    iget-object v3, p0, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->mClock:Lemp;

    invoke-direct {v7, v2, v3}, Lgca;-><init>(Liyg;Lemp;)V

    .line 98
    invoke-virtual {v7}, Lgca;->aEH()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v6}, Liyh;->bbW()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 100
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v6}, Liyh;->bbV()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->c(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    .line 102
    const v2, 0x7f110354

    invoke-virtual {p0, v2}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0a01c9

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v3, v8, v9

    invoke-virtual {v4, v5, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    :cond_0
    :goto_0
    invoke-virtual {v6}, Liyh;->bbZ()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v7}, Lgca;->aEH()Z

    move-result v2

    if-nez v2, :cond_1

    .line 118
    invoke-virtual {v6}, Liyh;->bbY()I

    move-result v2

    div-int/lit8 v3, v2, 0x3c

    .line 119
    const v2, 0x7f11047a

    invoke-virtual {p0, v2}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->ju(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    :cond_1
    const v2, 0x7f110356

    invoke-virtual {p0, v2}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 125
    invoke-virtual {v6}, Liyh;->bbP()I

    move-result v3

    if-lez v3, :cond_2

    .line 126
    const/4 v3, 0x1

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-static {v0, v1, v3, v2}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->a(Lfmt;Landroid/view/LayoutInflater;ZLandroid/widget/LinearLayout;)Landroid/view/View;

    move-result-object v3

    .line 128
    iput-object v3, p0, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->cAM:Landroid/view/View;

    .line 131
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "window"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Leot;->au(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v8, 0x7f0d01b5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v3, v5

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    const/high16 v5, 0x41f00000    # 30.0f

    mul-float/2addr v4, v5

    float-to-int v4, v4

    sub-int v8, v3, v4

    const/4 v5, 0x0

    invoke-interface/range {p3 .. p3}, Lfmt;->aAD()Lfml;

    move-result-object v3

    iget-object v9, v3, Lfml;->cvY:Lfnh;

    new-instance v3, Liyg;

    invoke-direct {v3}, Liyg;-><init>()V

    iput-object v6, v3, Liyg;->dPL:Liyh;

    const/4 v3, 0x0

    move v4, v3

    :goto_1
    iget-object v3, v6, Liyh;->dQi:[Liyk;

    array-length v3, v3

    if-ge v4, v3, :cond_10

    iget-object v3, v6, Liyh;->dQi:[Liyk;

    aget-object v10, v3, v4

    iget-object v3, v10, Liyk;->dQq:[Liyj;

    array-length v3, v3

    if-lez v3, :cond_7

    iget-object v3, v10, Liyk;->dQq:[Liyj;

    const/4 v11, 0x0

    aget-object v11, v3, v11

    const v3, 0x7f0401ac

    const/4 v12, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v2, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    const v3, 0x7f110359

    invoke-virtual {v12, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/search/shared/ui/WebImageView;

    invoke-virtual {v11}, Liyj;->sk()Z

    move-result v13

    if-eqz v13, :cond_9

    invoke-virtual {v11}, Liyj;->sj()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13, v9}, Lcom/google/android/search/shared/ui/WebImageView;->a(Ljava/lang/String;Lesm;)V

    const/4 v13, 0x0

    invoke-virtual {v3, v13}, Lcom/google/android/search/shared/ui/WebImageView;->setVisibility(I)V

    :cond_3
    :goto_2
    invoke-virtual {v11}, Liyj;->sk()Z

    move-result v3

    if-nez v3, :cond_4

    move-object/from16 v0, p4

    invoke-direct {p0, v10, v0, v12}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->a(Liyk;Landroid/view/LayoutInflater;Landroid/view/View;)V

    :cond_4
    iget-object v3, v6, Liyh;->dQi:[Liyk;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-lt v4, v3, :cond_5

    invoke-virtual {v6}, Liyh;->bbR()I

    move-result v3

    if-lez v3, :cond_6

    :cond_5
    const v3, 0x7f11035b

    invoke-virtual {v12, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/search/shared/ui/WebImageView;

    const-string v11, "http://maps.gstatic.com/tactile/directions/cards/arrow-right-2x.png"

    invoke-virtual {v3, v11, v9}, Lcom/google/android/search/shared/ui/WebImageView;->a(Ljava/lang/String;Lesm;)V

    const/4 v11, 0x0

    invoke-virtual {v3, v11}, Lcom/google/android/search/shared/ui/WebImageView;->setVisibility(I)V

    :cond_6
    invoke-static {v2}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->bt(Landroid/view/View;)I

    move-result v3

    invoke-static {v12}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->bt(Landroid/view/View;)I

    move-result v11

    add-int/2addr v3, v11

    if-gt v3, v8, :cond_a

    invoke-virtual {v2, v12}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v3, p0, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->cAL:Ljava/util/Map;

    invoke-interface {v3, v10, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 105
    :cond_8
    invoke-virtual {v6}, Liyh;->bbQ()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v6}, Liyh;->bbU()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v6}, Liyh;->bbW()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v6}, Liyh;->bbT()J

    move-result-wide v4

    invoke-virtual {v6}, Liyh;->bbP()I

    move-result v3

    mul-int/lit8 v3, v3, 0x3c

    int-to-long v8, v3

    sub-long/2addr v4, v8

    invoke-static {v2, v4, v5}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->c(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    .line 111
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v6}, Liyh;->bbV()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->c(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    .line 113
    const v2, 0x7f110354

    invoke-virtual {p0, v2}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->getContext()Landroid/content/Context;

    move-result-object v5

    const v8, 0x7f0a01c8

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v3, v9, v10

    const/4 v3, 0x1

    aput-object v4, v9, v3

    invoke-virtual {v5, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 131
    :cond_9
    invoke-virtual {v11}, Liyj;->bcf()Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-virtual {v11}, Liyj;->bce()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13, v9}, Lcom/google/android/search/shared/ui/WebImageView;->a(Ljava/lang/String;Lesm;)V

    const/4 v13, 0x0

    invoke-virtual {v3, v13}, Lcom/google/android/search/shared/ui/WebImageView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_a
    const v3, 0x7f040037

    const/4 v4, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    const v3, 0x7f1100fd

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const-string v5, "\u2026"

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const/4 v3, 0x1

    .line 134
    :goto_3
    invoke-virtual {v6}, Liyh;->bbR()I

    move-result v4

    if-lez v4, :cond_b

    if-nez v3, :cond_b

    .line 135
    const/4 v3, 0x0

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-static {v0, v1, v3, v2}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->a(Lfmt;Landroid/view/LayoutInflater;ZLandroid/widget/LinearLayout;)Landroid/view/View;

    .line 138
    :cond_b
    iget-object v3, v6, Liyh;->dQj:Ljhe;

    if-eqz v3, :cond_c

    iget-object v3, v6, Liyh;->dQj:Ljhe;

    invoke-virtual {v3}, Ljhe;->bjI()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 140
    iget-object v3, v6, Liyh;->dQj:Ljhe;

    invoke-virtual {v3}, Ljhe;->bjI()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 143
    :cond_c
    if-eqz p6, :cond_e

    .line 144
    iget-object v2, v6, Liyh;->dPR:Ljbp;

    if-eqz v2, :cond_d

    iget-object v2, v6, Liyh;->dPR:Ljbp;

    invoke-virtual {v2}, Ljbp;->oK()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-virtual {v6}, Liyh;->bbU()Z

    move-result v2

    if-eqz v2, :cond_d

    const v2, 0x7f11047b

    invoke-virtual {p0, v2}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v6}, Liyh;->bbN()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    const v3, 0x7f0a01d1

    :goto_4
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v6}, Liyh;->bbT()J

    move-result-wide v10

    invoke-static {v9, v10, v11}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->c(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v8

    const/4 v8, 0x1

    iget-object v9, v6, Liyh;->dPR:Ljbp;

    invoke-virtual {v9}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v8

    invoke-virtual {v4, v3, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_d
    iget-object v2, v6, Liyh;->dQg:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_e

    const v2, 0x7f11047c

    invoke-virtual {p0, v2}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, v6, Liyh;->dQg:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 146
    :cond_e
    if-eqz p5, :cond_f

    .line 147
    iget-object v2, p0, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->mRoute:Liyg;

    if-eqz v6, :cond_f

    iget-object v2, v6, Liyh;->dQe:[Liyi;

    array-length v2, v2

    if-lez v2, :cond_f

    const v2, 0x7f11047d

    invoke-virtual {p0, v2}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    const v3, 0x7f110475

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v7, v3}, Lgca;->by(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    :cond_f
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->aCY()V

    .line 151
    return-void

    .line 144
    :pswitch_0
    const v3, 0x7f0a01cd

    goto :goto_4

    :pswitch_1
    const v3, 0x7f0a01ce

    goto :goto_4

    :pswitch_2
    const v3, 0x7f0a01cf

    goto/16 :goto_4

    :pswitch_3
    const v3, 0x7f0a01d0

    goto/16 :goto_4

    :cond_10
    move v3, v5

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final aCX()Liyg;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->mRoute:Liyg;

    return-object v0
.end method

.method public final aCY()V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    const v7, 0x3e4ccccd    # 0.2f

    .line 154
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->mRoute:Liyg;

    iget-object v1, v1, Liyg;->dPL:Liyh;

    .line 155
    if-nez v1, :cond_1

    .line 183
    :cond_0
    return-void

    .line 158
    :cond_1
    new-instance v2, Lgca;

    iget-object v3, p0, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->mRoute:Liyg;

    iget-object v4, p0, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->mClock:Lemp;

    invoke-direct {v2, v3, v4}, Lgca;-><init>(Liyg;Lemp;)V

    .line 161
    iget-object v3, p0, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->cAM:Landroid/view/View;

    if-eqz v3, :cond_2

    iget-object v3, v1, Liyh;->dQi:[Liyk;

    array-length v3, v3

    if-lez v3, :cond_2

    invoke-virtual {v2}, Lgca;->aEH()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 164
    iget-object v3, v1, Liyh;->dQi:[Liyk;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Lgca;->d(Liyk;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 165
    iget-object v3, p0, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->cAM:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setAlpha(F)V

    .line 172
    :cond_2
    :goto_0
    iget-object v3, v1, Liyh;->dQi:[Liyk;

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 173
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->cAL:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 174
    if-eqz v0, :cond_3

    invoke-virtual {v2}, Lgca;->aEH()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 176
    invoke-virtual {v2, v5}, Lgca;->c(Liyk;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 177
    invoke-virtual {v0, v7}, Landroid/view/View;->setAlpha(F)V

    .line 172
    :cond_3
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 167
    :cond_4
    iget-object v3, p0, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->cAM:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 179
    :cond_5
    invoke-virtual {v0, v8}, Landroid/view/View;->setAlpha(F)V

    goto :goto_2
.end method

.method protected final ju(I)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 266
    const/16 v0, 0x5a0

    if-lt p1, v0, :cond_0

    .line 268
    const-string v0, ""

    .line 282
    :goto_0
    return-object v0

    .line 269
    :cond_0
    const/16 v0, 0x3c

    if-lt p1, v0, :cond_2

    .line 270
    div-int/lit8 v0, p1, 0x3c

    .line 271
    rem-int/lit8 v1, p1, 0x3c

    .line 272
    if-lez v1, :cond_1

    .line 274
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a01cb

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 278
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f100029

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 282
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/TransitTripRow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f100028

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
