.class public Lcom/google/android/sidekick/main/NearbyBeaconReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 21
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 22
    if-eqz v0, :cond_0

    const-string v1, "data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 23
    const-string v1, "data"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    .line 24
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    invoke-virtual {v2}, Lgql;->aJr()Lfdb;

    move-result-object v2

    .line 25
    invoke-virtual {v2}, Lfdb;->axV()Levm;

    move-result-object v3

    .line 26
    iput-object v1, v3, Levm;->cjQ:[B

    .line 31
    const-string v1, "refresh_hint"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "refresh_hint"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    invoke-virtual {v2}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v0

    invoke-virtual {v0}, Lfaq;->invalidate()V

    .line 36
    :cond_0
    return-void
.end method
