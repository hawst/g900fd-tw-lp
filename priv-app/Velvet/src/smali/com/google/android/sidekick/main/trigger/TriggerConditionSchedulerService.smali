.class public Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService;
.super Landroid/app/IntentService;
.source "PG"


# static fields
.field private static final ctq:[Ljava/lang/String;

.field private static final ctr:Ljava/util/concurrent/ConcurrentMap;


# instance fields
.field private mClock:Lemp;

.field private mGmsLocationProvider:Lguh;

.field private mLocationOracle:Lfdr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService;->ctq:[Ljava/lang/String;

    .line 50
    invoke-static {}, Lior;->aYb()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService;->ctr:Ljava/util/concurrent/ConcurrentMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    const-string v0, "TriggerConditionSchedulerService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 154
    sget-object v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService;->ctr:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1, p2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService$Receiver;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "pi"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "a:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 158
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Intent;Landroid/location/Location;)Landroid/content/Intent;
    .locals 6

    .prologue
    .line 120
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 122
    const-string v0, "tcs_l"

    invoke-virtual {v2, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 124
    const-string v0, "tcs_t"

    iget-object v1, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 129
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "f:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 131
    const-string v0, "gms_error_code"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    invoke-static {p1}, Lbrj;->j(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    .line 133
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrh;

    .line 134
    invoke-interface {v0}, Lbrh;->zK()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 140
    :cond_0
    new-instance v1, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;

    sget-object v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService;->ctq:[Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-direct {v1, v0}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;-><init>([Ljava/lang/String;)V

    move-object v0, v1

    .line 147
    :goto_1
    const-string v1, "tcs_a"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 149
    return-object v2

    .line 141
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.apps.sidekick.VEHICLE_EXIT_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 142
    new-instance v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;-><init>(I)V

    goto :goto_1

    .line 145
    :cond_2
    new-instance v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;

    invoke-direct {v0}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;-><init>()V

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Landroid/app/PendingIntent;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 164
    sget-object v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService;->ctr:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1, p2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "pi"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "f:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 168
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onCreate()V
    .locals 3

    .prologue
    .line 63
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 65
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v1

    .line 67
    iget-object v2, v1, Lfdb;->mGmsLocationProvider:Lguh;

    iget-object v1, v1, Lfdb;->mLocationOracle:Lfdr;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DC()Lemp;

    move-result-object v0

    iput-object v2, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService;->mGmsLocationProvider:Lguh;

    iput-object v1, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService;->mLocationOracle:Lfdr;

    iput-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService;->mClock:Lemp;

    .line 69
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 82
    :try_start_0
    const-string v0, "com.google.android.location.intent.extra.triggering_location"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 83
    if-nez v0, :cond_2

    .line 84
    iget-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService;->mGmsLocationProvider:Lguh;

    invoke-virtual {v0}, Lguh;->aKB()Lcgs;

    move-result-object v0

    invoke-virtual {v0}, Lcgs;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    move-object v1, v0

    .line 86
    :goto_0
    iget-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService;->mLocationOracle:Lfdr;

    invoke-interface {v0, v1}, Lfdr;->d(Landroid/location/Location;)V

    .line 93
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "pi"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 99
    if-nez v0, :cond_0

    .line 100
    invoke-direct {p0, p1, v1}, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService;->a(Landroid/content/Intent;Landroid/location/Location;)Landroid/content/Intent;

    move-result-object v1

    .line 101
    sget-object v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService;->ctr:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 102
    const/4 v3, 0x0

    invoke-virtual {v0, p0, v3, v1}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 115
    :catch_0
    move-exception v0

    invoke-static {p1}, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService$Receiver;->b(Landroid/content/Intent;)Z

    .line 116
    :goto_2
    return-void

    .line 105
    :cond_0
    :try_start_1
    invoke-direct {p0, p1, v1}, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService;->a(Landroid/content/Intent;Landroid/location/Location;)Landroid/content/Intent;

    move-result-object v1

    .line 106
    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2, v1}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_1
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 115
    :cond_1
    invoke-static {p1}, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService$Receiver;->b(Landroid/content/Intent;)Z

    goto :goto_2

    .line 111
    :catch_1
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 115
    invoke-static {p1}, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService$Receiver;->b(Landroid/content/Intent;)Z

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-static {p1}, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService$Receiver;->b(Landroid/content/Intent;)Z

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-static {p1}, Lcom/google/android/sidekick/main/trigger/TriggerConditionSchedulerService$Receiver;->b(Landroid/content/Intent;)Z

    throw v0

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method
