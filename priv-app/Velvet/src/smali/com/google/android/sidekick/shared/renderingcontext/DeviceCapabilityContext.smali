.class public Lcom/google/android/sidekick/shared/renderingcontext/DeviceCapabilityContext;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final cyp:Ljava/lang/String;


# instance fields
.field private final cyt:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/android/sidekick/shared/renderingcontext/DeviceCapabilityContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/sidekick/shared/renderingcontext/DeviceCapabilityContext;->cyp:Ljava/lang/String;

    .line 41
    new-instance v0, Lfpf;

    invoke-direct {v0}, Lfpf;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/shared/renderingcontext/DeviceCapabilityContext;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/renderingcontext/DeviceCapabilityContext;->cyt:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public static h(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/renderingcontext/DeviceCapabilityContext;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/sidekick/shared/renderingcontext/DeviceCapabilityContext;->cyp:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->lT(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/renderingcontext/DeviceCapabilityContext;

    return-object v0
.end method


# virtual methods
.method public final auH()Z
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/sidekick/shared/renderingcontext/DeviceCapabilityContext;->cyt:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return v0
.end method

.method public final fv(Z)V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/sidekick/shared/renderingcontext/DeviceCapabilityContext;->cyt:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 25
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/sidekick/shared/renderingcontext/DeviceCapabilityContext;->cyt:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 39
    return-void

    .line 38
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
