.class public Lcom/google/android/sidekick/main/NotificationReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private cju:Ljava/util/concurrent/ExecutorService;

.field private mActivityHelper:Lfzw;

.field private mClock:Lemp;

.field private mEntryNotificationFactory:Lfjs;

.field private mLocationOracle:Lfdr;

.field private mNetworkClient:Lfcx;

.field private mNowNotificationManager:Lfga;

.field private mPrefController:Lchr;

.field private mUserInteractionLogger:Lcpx;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 193
    return-void
.end method

.method private a(Landroid/content/Intent;Ljava/util/Collection;)V
    .locals 5

    .prologue
    .line 150
    const-string v0, "notificationLogActionKey"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 152
    const-string v1, "notificationLoggingNameKey"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 155
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NOTIFICATION_ACTION_PRESS:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 156
    iget-object v0, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mUserInteractionLogger:Lcpx;

    invoke-virtual {v0, v2, v1}, Lcpx;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 158
    iget-object v3, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mUserInteractionLogger:Lcpx;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v0, v4}, Lcpx;->a(Ljava/lang/String;Lizj;Lixx;)V

    goto :goto_0

    .line 160
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lizj;Lfey;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 220
    iget-object v0, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    const v3, 0x7f0a012d

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a0130

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v3, 0x7f0a0132

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v3, Liwk;

    invoke-direct {v3}, Liwk;-><init>()V

    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Liwk;->nb(I)Liwk;

    invoke-interface {p3}, Lfey;->ayP()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    new-instance v5, Lexo;

    iget-object v6, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mNetworkClient:Lfcx;

    iget-object v7, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mClock:Lemp;

    invoke-direct {v5, v6, v0, v3, v7}, Lexo;-><init>(Lfcx;Lizj;Liwk;Lemp;)V

    new-array v0, v2, [Ljava/lang/Void;

    invoke-virtual {v5, v0}, Lexo;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mNowNotificationManager:Lfga;

    invoke-interface {p3}, Lfey;->ayZ()Lfgb;

    move-result-object v2

    invoke-interface {v0, v2}, Lfga;->a(Lfgb;)V

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 250
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v2

    .line 220
    goto :goto_1

    .line 225
    :cond_3
    iget-object v0, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mNowNotificationManager:Lfga;

    const/4 v2, 0x0

    invoke-interface {v0, p3, v2, v1}, Lfga;->a(Lfey;Landroid/app/PendingIntent;Z)Landroid/app/Notification;

    move-result-object v0

    .line 229
    if-eqz v0, :cond_1

    .line 230
    invoke-interface {p3}, Lfey;->ayW()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_4

    .line 234
    iget-object v1, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mNowNotificationManager:Lfga;

    sget-object v2, Lfgb;->cqx:Lfgb;

    invoke-interface {v1, v2}, Lfga;->a(Lfgb;)V

    .line 245
    :goto_3
    iget-object v1, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mNowNotificationManager:Lfga;

    invoke-interface {v1, v0, p3}, Lfga;->a(Landroid/app/Notification;Lfey;)V

    .line 246
    invoke-interface {p3}, Lfey;->ayR()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 247
    iget-object v0, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mNowNotificationManager:Lfga;

    invoke-interface {v0, p2}, Lfga;->p(Lizj;)V

    goto :goto_2

    .line 241
    :cond_4
    iget-object v1, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mNowNotificationManager:Lfga;

    sget-object v2, Lfgb;->cqz:Lfgb;

    invoke-interface {v1, v2}, Lfga;->a(Lfgb;)V

    goto :goto_3
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    .line 66
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mNowNotificationManager:Lfga;

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lfdb;->axO()Lfga;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mNowNotificationManager:Lfga;

    :cond_0
    iget-object v2, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mNetworkClient:Lfcx;

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lfdb;->axI()Lfcx;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mNetworkClient:Lfcx;

    :cond_1
    iget-object v2, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mClock:Lemp;

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DC()Lemp;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mClock:Lemp;

    :cond_2
    iget-object v2, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mPrefController:Lchr;

    if-nez v2, :cond_3

    invoke-virtual {v0}, Lgql;->aJs()Lchr;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mPrefController:Lchr;

    :cond_3
    iget-object v2, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mEntryNotificationFactory:Lfjs;

    if-nez v2, :cond_4

    invoke-virtual {v1}, Lfdb;->axR()Lfjs;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mEntryNotificationFactory:Lfjs;

    :cond_4
    iget-object v2, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mUserInteractionLogger:Lcpx;

    if-nez v2, :cond_5

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DR()Lcpx;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mUserInteractionLogger:Lcpx;

    :cond_5
    iget-object v2, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mLocationOracle:Lfdr;

    if-nez v2, :cond_6

    invoke-virtual {v0}, Lgql;->El()Lfdr;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mLocationOracle:Lfdr;

    :cond_6
    iget-object v2, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->cju:Ljava/util/concurrent/ExecutorService;

    if-nez v2, :cond_7

    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->auu()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->cju:Ljava/util/concurrent/ExecutorService;

    :cond_7
    iget-object v0, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mActivityHelper:Lfzw;

    if-nez v0, :cond_8

    invoke-virtual {v1}, Lfdb;->axW()Lfzw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mActivityHelper:Lfzw;

    .line 70
    :cond_8
    if-eqz p2, :cond_9

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_a

    .line 146
    :cond_9
    :goto_0
    return-void

    .line 74
    :cond_a
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 76
    const-string v1, "com.google.android.apps.sidekick.NOTIFICATION_ENTRY_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 77
    const-string v0, "notification_entry"

    invoke-static {p2, v0}, Lgbm;->b(Landroid/content/Intent;Ljava/lang/String;)Lizj;

    move-result-object v4

    const-string v0, "location_key"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    if-eqz v4, :cond_9

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mEntryNotificationFactory:Lfjs;

    invoke-interface {v0, v4}, Lfjs;->r(Lizj;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lfey;

    if-eqz v5, :cond_9

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "NotificationRefresh_wakelock"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    const-wide/16 v0, 0x1388

    invoke-virtual {v2, v0, v1}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    iget-object v6, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->cju:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Leuw;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Leuw;-><init>(Lcom/google/android/sidekick/main/NotificationReceiver;Landroid/os/PowerManager$WakeLock;Landroid/content/Context;Lizj;Lfey;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 78
    :cond_b
    const-string v1, "com.google.android.apps.sidekick.NOTIFICATION_DISMISS_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 79
    const-string v0, "notification_entries"

    invoke-static {p2, v0}, Lgbm;->d(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 81
    if-eqz v1, :cond_c

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 82
    :cond_c
    const-string v0, "NotificationReceiver"

    const-string v1, "Received notification dismiss without entries!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 86
    :cond_d
    const-string v0, "notificationIdKey"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lfgb;

    .line 89
    if-nez v0, :cond_e

    .line 90
    const-string v0, "NotificationReceiver"

    const-string v1, "Received notification dismiss without notification type!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 93
    :cond_e
    iget-object v2, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mNowNotificationManager:Lfga;

    invoke-interface {v2, v1, v0}, Lfga;->a(Ljava/util/Collection;Lfgb;)V

    .line 96
    const-string v0, "notificationDismissCallback"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 98
    if-eqz v0, :cond_9

    .line 100
    :try_start_0
    invoke-virtual {v0}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 106
    :cond_f
    const-string v1, "com.google.android.apps.sidekick.NOTIFICATION_CALLBACK_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 107
    const-string v0, "notification_entries"

    invoke-static {p2, v0}, Lgbm;->d(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 109
    if-eqz v1, :cond_10

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 110
    :cond_10
    const-string v0, "NotificationReceiver"

    const-string v1, "Received notification action press without entries!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 116
    :cond_11
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 118
    const-string v0, "notificationIdKey"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lfgb;

    .line 121
    if-nez v0, :cond_12

    .line 122
    const-string v0, "NotificationReceiver"

    const-string v1, "Received notification action press without notification type!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 125
    :cond_12
    iget-object v2, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mNowNotificationManager:Lfga;

    invoke-interface {v2, v0}, Lfga;->a(Lfgb;)V

    .line 127
    invoke-direct {p0, p2, v1}, Lcom/google/android/sidekick/main/NotificationReceiver;->a(Landroid/content/Intent;Ljava/util/Collection;)V

    .line 129
    const-string v0, "callback_type"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 131
    const-string v0, "notification_callback"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 133
    if-eqz v0, :cond_13

    if-nez v1, :cond_14

    .line 134
    :cond_13
    const-string v2, "NotificationReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received notification action press with unknown callback: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :cond_14
    const-string v2, "activity"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 139
    iget-object v1, p0, Lcom/google/android/sidekick/main/NotificationReceiver;->mActivityHelper:Lfzw;

    invoke-virtual {v1, p1, v0}, Lfzw;->h(Landroid/content/Context;Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 140
    :cond_15
    const-string v2, "broadcast"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 141
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 142
    :cond_16
    const-string v2, "service"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 143
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0
.end method
