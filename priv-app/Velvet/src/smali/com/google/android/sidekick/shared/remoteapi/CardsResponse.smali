.class public Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public bcd:I

.field public cxB:Landroid/location/Location;

.field public cxE:Lizn;

.field public cxF:J

.field public cxG:J

.field public cxH:Z

.field public cxI:Z

.field public cxJ:Z

.field public cxK:Z

.field public cxL:Ljava/lang/String;

.field public cxM:Ljava/lang/String;

.field public cxN:Landroid/content/Intent;

.field public cxO:Ljcn;

.field public cxP:Ljava/lang/String;

.field public mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lfoq;

    invoke-direct {v0}, Lfoq;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->bcd:I

    const-class v0, Lizn;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizn;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxE:Lizn;

    const-class v0, Landroid/location/Location;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxB:Landroid/location/Location;

    invoke-static {p1}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aE(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxL:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxM:Ljava/lang/String;

    const-class v0, Landroid/content/Intent;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxN:Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxF:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxG:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxH:Z

    const-class v0, Ljcn;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljcn;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxO:Ljcn;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxP:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxI:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxJ:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxK:Z

    const-class v0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "embedded-parcelable"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    .line 73
    return-void

    :cond_0
    move v0, v2

    .line 72
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method


# virtual methods
.method public final aBR()Lizn;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxE:Lizn;

    return-object v0
.end method

.method public final aBS()J
    .locals 2

    .prologue
    .line 92
    iget-wide v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxG:J

    return-wide v0
.end method

.method public final aBT()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxI:Z

    return v0
.end method

.method public final aBU()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxJ:Z

    return v0
.end method

.method public final aBV()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxK:Z

    return v0
.end method

.method public final aBW()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxL:Ljava/lang/String;

    return-object v0
.end method

.method public final aBX()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxM:Ljava/lang/String;

    return-object v0
.end method

.method public final aBY()Landroid/content/Intent;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxN:Landroid/content/Intent;

    return-object v0
.end method

.method public final aBZ()Ljcn;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxO:Ljcn;

    return-object v0
.end method

.method public final aCa()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxP:Ljava/lang/String;

    return-object v0
.end method

.method public final awY()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxH:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    return v0
.end method

.method public final getResponseCode()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->bcd:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 162
    iget v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->bcd:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 163
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxE:Lizn;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxB:Landroid/location/Location;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 172
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Landroid/os/Parcel;)V

    .line 173
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxM:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxN:Landroid/content/Intent;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 176
    iget-wide v4, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxF:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 177
    iget-wide v4, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxG:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 178
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxH:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 179
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxO:Ljcn;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxP:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 181
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxI:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 182
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 183
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxJ:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 184
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 185
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxK:Z

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 186
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "embedded-parcelable"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 187
    return-void

    :cond_0
    move v0, v2

    .line 178
    goto :goto_0

    :cond_1
    move v0, v2

    .line 181
    goto :goto_1

    :cond_2
    move v0, v2

    .line 183
    goto :goto_2

    :cond_3
    move v1, v2

    .line 185
    goto :goto_3
.end method

.method public final ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    return-object v0
.end method
