.class public Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final ctb:Lijj;

.field public final ctc:Lifq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 400
    new-instance v0, Lfis;

    invoke-direct {v0}, Lfis;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 371
    invoke-static {}, Lifq;->aVX()Lifq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;->ctc:Lifq;

    .line 372
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;->ctb:Lijj;

    .line 373
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 375
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376
    const/4 v0, 0x6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lifq;->be(Ljava/lang/Object;)Lifq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;->ctc:Lifq;

    .line 377
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;->ctb:Lijj;

    .line 378
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 414
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 415
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 416
    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;->ctb:Lijj;

    .line 418
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 419
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 420
    invoke-static {}, Lifq;->aVX()Lifq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;->ctc:Lifq;

    .line 424
    :goto_0
    return-void

    .line 422
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lifq;->be(Ljava/lang/Object;)Lifq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;->ctc:Lifq;

    goto :goto_0
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 362
    invoke-direct {p0, p1}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public varargs constructor <init>([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 380
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 381
    invoke-static {}, Lifq;->aVX()Lifq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;->ctc:Lifq;

    .line 382
    invoke-static {p1}, Lijj;->d([Ljava/lang/Object;)Lijj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;->ctb:Lijj;

    .line 383
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 387
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;->ctb:Lijj;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 393
    iget-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;->ctc:Lifq;

    invoke-virtual {v0}, Lifq;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;->ctc:Lifq;

    invoke-virtual {v0}, Lifq;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 398
    :goto_0
    return-void

    .line 396
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
