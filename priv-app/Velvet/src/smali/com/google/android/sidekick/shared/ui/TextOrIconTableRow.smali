.class public Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;
.super Landroid/widget/TableRow;
.source "PG"


# instance fields
.field private Zn:I

.field private Zo:I

.field private bml:Z

.field private cAD:I

.field private cAE:I

.field private cAF:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 29
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->cAD:I

    .line 33
    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->cAF:I

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 39
    const v1, 0x7f0d02a3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->Zn:I

    .line 41
    const v1, 0x7f0d02a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->Zo:I

    .line 43
    const v1, 0x7f0d02a5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->cAE:I

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    iput v3, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->cAD:I

    .line 33
    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->cAF:I

    .line 49
    sget-object v0, Lbwe;->aMg:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 52
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->cAD:I

    .line 54
    const/4 v2, 0x1

    const v3, 0x7f0d02a3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->Zn:I

    .line 57
    const/4 v2, 0x2

    const v3, 0x7f0d02a4

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->Zo:I

    .line 60
    const/4 v2, 0x3

    const v3, 0x7f0d02a5

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->cAE:I

    .line 64
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 65
    return-void
.end method


# virtual methods
.method public final a(Ljhg;Lesm;)V
    .locals 5

    .prologue
    const/4 v1, 0x5

    const/4 v4, 0x0

    .line 103
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 106
    iget-object v0, p1, Ljhg;->dVY:Ljcn;

    if-eqz v0, :cond_1

    .line 107
    new-instance v2, Lcom/google/android/search/shared/ui/WebImageView;

    invoke-direct {v2, v3}, Lcom/google/android/search/shared/ui/WebImageView;-><init>(Landroid/content/Context;)V

    .line 108
    iget-object v0, p1, Ljhg;->dVY:Ljcn;

    invoke-static {v0}, Lgbm;->c(Ljcn;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, p2}, Lcom/google/android/search/shared/ui/WebImageView;->a(Ljava/lang/String;Lesm;)V

    .line 111
    iget v1, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->Zn:I

    .line 112
    iget v0, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->Zo:I

    .line 131
    :goto_0
    new-instance v3, Landroid/widget/TableRow$LayoutParams;

    invoke-direct {v3, v1, v0}, Landroid/widget/TableRow$LayoutParams;-><init>(II)V

    .line 132
    invoke-virtual {p1}, Ljhg;->blF()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljhg;->blE()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iput v4, v3, Landroid/widget/TableRow$LayoutParams;->width:I

    .line 134
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, v3, Landroid/widget/TableRow$LayoutParams;->weight:F

    .line 137
    :cond_0
    iget v0, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->cAE:I

    invoke-static {v3, v4, v4, v0, v4}, Leot;->a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)V

    .line 138
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->bml:Z

    if-eqz v0, :cond_6

    const/16 v0, 0x30

    :goto_1
    iput v0, v3, Landroid/widget/TableRow$LayoutParams;->gravity:I

    .line 139
    invoke-virtual {p0, v2, v3}, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 140
    return-void

    .line 114
    :cond_1
    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 115
    invoke-virtual {p1}, Ljhg;->hasText()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Ljhg;->getText()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    invoke-virtual {v2}, Landroid/widget/TextView;->setSingleLine()V

    .line 117
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 118
    iget v0, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->cAD:I

    if-lez v0, :cond_2

    .line 119
    iget v0, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->cAD:I

    invoke-virtual {v2, v3, v0}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 121
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->bml:Z

    if-eqz v0, :cond_3

    .line 122
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 123
    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 125
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_4

    invoke-virtual {p1}, Ljhg;->blH()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Ljhg;->blG()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    :goto_3
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextAlignment(I)V

    .line 128
    :cond_4
    iget v1, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->cAF:I

    .line 129
    const/4 v0, -0x2

    goto :goto_0

    .line 115
    :cond_5
    const-string v0, ""

    goto :goto_2

    .line 125
    :pswitch_0
    const/4 v0, 0x4

    goto :goto_3

    :pswitch_1
    const/4 v0, 0x6

    goto :goto_3

    .line 138
    :cond_6
    const/16 v0, 0x10

    goto :goto_1

    :cond_7
    move v0, v1

    goto :goto_3

    .line 125
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final fz(Z)Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->bml:Z

    .line 94
    return-object p0
.end method

.method public final jr(I)Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;
    .locals 0

    .prologue
    .line 68
    iput p1, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->cAD:I

    .line 69
    return-object p0
.end method

.method public final js(I)Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;
    .locals 0

    .prologue
    .line 83
    iput p1, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->cAE:I

    .line 84
    return-object p0
.end method

.method public final jt(I)Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;
    .locals 0

    .prologue
    .line 88
    iput p1, p0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->cAF:I

    .line 89
    return-object p0
.end method
