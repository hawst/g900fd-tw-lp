.class public Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lejl;
.implements Lfqb;


# instance fields
.field private GR:Landroid/widget/ScrollView;

.field public cyC:Lfqc;

.field public cym:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

.field private czk:Landroid/view/ViewGroup;

.field private czl:Z

.field private final czm:Ljava/util/List;

.field public final czn:Ljava/util/List;

.field public mEntry:Lizj;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czm:Ljava/util/List;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czn:Ljava/util/List;

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czm:Ljava/util/List;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czn:Ljava/util/List;

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czm:Ljava/util/List;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czn:Ljava/util/List;

    .line 59
    return-void
.end method

.method private a(Ljava/lang/CharSequence;Ljdf;Ljava/lang/Integer;)V
    .locals 5
    .param p1    # Ljava/lang/CharSequence;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 161
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 162
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 165
    const v2, 0x7f0b00c8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 166
    iget-object v2, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czk:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 167
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 168
    const/4 v2, -0x1

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 169
    const v2, 0x7f0d017c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 170
    const v2, 0x7f0d017d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v4, 0x7f0d017e

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v2, v3, v1, v3}, Leot;->a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)V

    .line 177
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 178
    const v1, 0x7f0401a4

    iget-object v2, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czk:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 180
    const v1, 0x7f110198

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 181
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    if-nez p3, :cond_1

    move v1, v3

    .line 184
    :goto_0
    if-nez v1, :cond_3

    .line 185
    const v1, 0x7f02029f

    move v2, v1

    .line 187
    :goto_1
    const v1, 0x7f11005e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2}, Lfqk;->i(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 189
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    if-nez p1, :cond_2

    .line 191
    const-string v1, "RadioGroupQuestionView"

    const-string v2, "Optional label missing. Not setting a content description"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 195
    :goto_2
    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czm:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czn:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czk:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 198
    return-void

    .line 182
    :cond_1
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lfqk;->jk(I)I

    move-result v1

    goto :goto_0

    .line 193
    :cond_2
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_3
    move v2, v1

    goto :goto_1
.end method

.method private jg(I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 201
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 202
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czm:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-ne v1, p1, :cond_0

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setSelected(Z)V

    .line 201
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v3, v2

    .line 202
    goto :goto_1

    .line 204
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lfqc;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->cyC:Lfqc;

    .line 222
    return-void
.end method

.method public final aU(II)V
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lfqk;->h(Landroid/view/View;II)V

    .line 209
    return-void
.end method

.method public final b(FII)I
    .locals 3

    .prologue
    .line 249
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czl:Z

    if-eqz v0, :cond_0

    .line 250
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czk:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 251
    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czk:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    int-to-float v2, p2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 252
    mul-int/2addr p2, p3

    .line 250
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 256
    :cond_0
    return p2
.end method

.method public final b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 213
    iget-object v4, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->mEntry:Lizj;

    invoke-static {p0, p1, v4}, Lfqk;->a(Landroid/view/View;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lizj;)V

    .line 215
    iput-object p1, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->cym:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    .line 216
    iget-object v4, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czk:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v4, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czm:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    iget-object v4, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czn:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    iget-object v4, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->cym:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-virtual {v4}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 216
    :sswitch_0
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->cym:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-virtual {v4}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v4

    const v5, 0x7f0a0451

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljdf;

    invoke-direct {v6}, Ljdf;-><init>()V

    invoke-virtual {v6, v1}, Ljdf;->hH(Z)Ljdf;

    move-result-object v6

    invoke-direct {p0, v5, v6, v3}, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->a(Ljava/lang/CharSequence;Ljdf;Ljava/lang/Integer;)V

    const v5, 0x7f0a0452

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v5, Ljdf;

    invoke-direct {v5}, Ljdf;-><init>()V

    invoke-virtual {v5, v0}, Ljdf;->hH(Z)Ljdf;

    move-result-object v5

    invoke-direct {p0, v2, v5, v3}, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->a(Ljava/lang/CharSequence;Ljdf;Ljava/lang/Integer;)V

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljdf;->bhi()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v4}, Ljdf;->bhh()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->jg(I)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :sswitch_1
    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->cym:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCp()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_2

    const-string v1, "RadioGroupQuestionView"

    const-string v2, "Missing multiple choice options from question"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    move v1, v0

    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpa;

    iget-object v2, v0, Lfpa;->cyk:Ljava/lang/String;

    if-nez v2, :cond_3

    move-object v2, v3

    :goto_3
    new-instance v5, Ljdf;

    invoke-direct {v5}, Ljdf;-><init>()V

    invoke-virtual {v5, v1}, Ljdf;->oL(I)Ljdf;

    move-result-object v5

    iget v0, v0, Lfpa;->cyi:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v2, v5, v0}, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->a(Ljava/lang/CharSequence;Ljdf;Ljava/lang/Integer;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    iget-object v2, v0, Lfpa;->cyk:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->cym:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljdf;->bhk()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljdf;->bhj()I

    move-result v1

    if-ltz v1, :cond_0

    invoke-virtual {v0}, Ljdf;->bhj()I

    move-result v1

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {v0}, Ljdf;->bhj()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->jg(I)V

    goto/16 :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->cym:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v4

    if-eqz v4, :cond_5

    iget-object v1, v4, Ljdf;->ebI:Ljdg;

    if-nez v1, :cond_7

    :cond_5
    move-object v1, v3

    :goto_4
    iget-object v4, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->cym:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-virtual {v4}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v4

    iget-object v5, v4, Ljde;->ebw:[Ljdg;

    move v4, v0

    move v0, v2

    :goto_5
    array-length v6, v5

    if-ge v4, v6, :cond_8

    aget-object v6, v5, v4

    new-instance v7, Ljdf;

    invoke-direct {v7}, Ljdf;-><init>()V

    iput-object v6, v7, Ljdf;->ebI:Ljdg;

    invoke-virtual {v6}, Ljdg;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8, v7, v3}, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->a(Ljava/lang/CharSequence;Ljdf;Ljava/lang/Integer;)V

    if-eqz v1, :cond_6

    new-instance v7, Lfpr;

    invoke-direct {v7, v6}, Lfpr;-><init>(Ljdg;)V

    invoke-virtual {v1, v7}, Lfpr;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    move v0, v4

    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_7
    new-instance v1, Lfpr;

    iget-object v4, v4, Ljdf;->ebI:Ljdg;

    invoke-direct {v1, v4}, Lfpr;-><init>(Ljdg;)V

    goto :goto_4

    :cond_8
    if-eq v0, v2, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->jg(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czm:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 232
    invoke-direct {p0, v0}, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->jg(I)V

    .line 233
    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->cyC:Lfqc;

    if-eqz v1, :cond_0

    .line 234
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v2, Lfqd;

    invoke-direct {v2, p0, v0}, Lfqd;-><init>(Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;I)V

    invoke-static {v1, v2}, Lelv;->a(Landroid/content/res/Resources;Ljava/lang/Runnable;)V

    .line 242
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 64
    const v0, 0x7f11046d

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czk:Landroid/view/ViewGroup;

    .line 65
    const v0, 0x7f11046c

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->GR:Landroid/widget/ScrollView;

    .line 66
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 70
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 74
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->GR:Landroid/widget/ScrollView;

    const/4 v3, -0x1

    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->canScrollVertically(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->GR:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->canScrollVertically(I)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czl:Z

    .line 75
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->czl:Z

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {p0, v1}, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->setClipChildren(Z)V

    .line 76
    return-void

    :cond_0
    move v0, v2

    .line 74
    goto :goto_0

    :cond_1
    move v1, v2

    .line 75
    goto :goto_1
.end method

.method public final z(Lizj;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/google/android/sidekick/shared/training/RadioGroupQuestionView;->mEntry:Lizj;

    .line 227
    return-void
.end method
