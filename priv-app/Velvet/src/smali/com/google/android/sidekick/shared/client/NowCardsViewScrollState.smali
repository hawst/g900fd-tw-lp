.class public Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final cva:Z

.field private final cvb:Lizj;

.field private final cvc:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lflq;

    invoke-direct {v0}, Lflq;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cva:Z

    .line 80
    const-class v0, Lizj;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cvb:Lizj;

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cvc:I

    .line 82
    return-void

    .line 79
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lizj;I)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cva:Z

    .line 26
    iput-object p1, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cvb:Lizj;

    .line 27
    iput p2, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cvc:I

    .line 28
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cva:Z

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cvb:Lizj;

    .line 36
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cvc:I

    .line 37
    return-void
.end method


# virtual methods
.method public final aAN()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cva:Z

    return v0
.end method

.method public final aAO()Lizj;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cvb:Lizj;

    return-object v0
.end method

.method public final aAP()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cvc:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cvb:Lizj;

    if-eqz v0, :cond_0

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ScollState: scroll to entry of type ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cvb:Lizj;

    invoke-virtual {v1}, Lizj;->getType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] with offset: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cvc:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 93
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ScrollState: scrollToFirstCardAdded: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cva:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cva:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 60
    iget-object v0, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cvb:Lizj;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 61
    iget v0, p0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->cvc:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 62
    return-void

    .line 59
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
