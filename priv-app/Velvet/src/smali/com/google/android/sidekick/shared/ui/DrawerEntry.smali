.class public Lcom/google/android/sidekick/shared/ui/DrawerEntry;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private IH:Landroid/widget/ImageView;

.field private czY:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->a(Landroid/util/AttributeSet;II)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    invoke-direct {p0, p2, v0, v0}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->a(Landroid/util/AttributeSet;II)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, v0}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->a(Landroid/util/AttributeSet;II)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 40
    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->a(Landroid/util/AttributeSet;II)V

    .line 41
    return-void
.end method

.method private a(Landroid/util/AttributeSet;II)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 44
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 47
    const v1, 0x7f02005d

    invoke-virtual {p0, v1}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->setBackgroundResource(I)V

    .line 48
    invoke-virtual {p0, v2}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->setClickable(Z)V

    .line 50
    const v1, 0x7f04005d

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 51
    const v0, 0x7f11014f

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->IH:Landroid/widget/ImageView;

    .line 52
    const v0, 0x7f110150

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->czY:Landroid/widget/TextView;

    .line 54
    if-eqz p1, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lbwe;->aLS:[I

    invoke-virtual {v0, p1, v1, p2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 59
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 61
    new-instance v2, Lelm;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b013d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lelm;-><init>(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->IH:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 65
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->czY:Landroid/widget/TextView;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 70
    :cond_0
    return-void

    .line 67
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method


# virtual methods
.method public final setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/DrawerEntry;->czY:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    return-void
.end method
