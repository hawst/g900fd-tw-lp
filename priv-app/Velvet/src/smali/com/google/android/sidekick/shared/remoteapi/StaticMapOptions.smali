.class public Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final cxX:Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

.field private final cxY:Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

.field private final cxZ:Z

.field private final cya:Z

.field private final cyb:Z

.field private final cyc:Z

.field private cyd:Ljava/lang/Integer;

.field private cye:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 126
    new-instance v0, Lfow;

    invoke-direct {v0}, Lfow;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    const-class v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cxY:Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 143
    const-class v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cxX:Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cxZ:Z

    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cya:Z

    .line 147
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cyb:Z

    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cyd:Ljava/lang/Integer;

    .line 151
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cye:Ljava/lang/Integer;

    .line 154
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_5

    :goto_3
    iput-boolean v1, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cyc:Z

    .line 155
    return-void

    :cond_2
    move v0, v2

    .line 145
    goto :goto_0

    :cond_3
    move v0, v2

    .line 146
    goto :goto_1

    :cond_4
    move v0, v2

    .line 147
    goto :goto_2

    :cond_5
    move v1, v2

    .line 154
    goto :goto_3
.end method

.method public constructor <init>(Ljbp;Ljal;ZZZZLjava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1
    .param p1    # Ljbp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Integer;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Integer;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    invoke-static {p2}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cxY:Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 88
    invoke-static {p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cxX:Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 89
    iput-boolean p3, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cxZ:Z

    .line 90
    iput-boolean p4, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cya:Z

    .line 91
    iput-boolean p5, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cyb:Z

    .line 92
    iput-object p7, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cyd:Ljava/lang/Integer;

    .line 93
    iput-object p8, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cye:Ljava/lang/Integer;

    .line 94
    iput-boolean p6, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cyc:Z

    .line 95
    return-void
.end method

.method public static aCb()Lfox;
    .locals 1

    .prologue
    .line 81
    new-instance v0, Lfox;

    invoke-direct {v0}, Lfox;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final aCc()Ljbp;
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cxX:Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    const-class v1, Ljbp;

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljbp;

    return-object v0
.end method

.method public final aCd()Z
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cya:Z

    return v0
.end method

.method public final aCe()Z
    .locals 1

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cyc:Z

    return v0
.end method

.method public final aCf()Ljava/lang/Integer;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cyd:Ljava/lang/Integer;

    return-object v0
.end method

.method public final aCg()Ljava/lang/Integer;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cye:Ljava/lang/Integer;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    return v0
.end method

.method public final getFrequentPlace()Ljal;
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cxY:Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    const-class v1, Ljal;

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Ljal;

    return-object v0
.end method

.method public final rg()Z
    .locals 1

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cxZ:Z

    return v0
.end method

.method public final ri()Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cyb:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 105
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cxY:Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cxX:Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 107
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cxZ:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 108
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cya:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 109
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cyb:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 110
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cyd:Ljava/lang/Integer;

    if-nez v0, :cond_3

    .line 111
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    .line 117
    :goto_3
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cye:Ljava/lang/Integer;

    if-nez v0, :cond_4

    .line 118
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    .line 123
    :goto_4
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cyc:Z

    if-eqz v0, :cond_5

    :goto_5
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 124
    return-void

    :cond_0
    move v0, v2

    .line 107
    goto :goto_0

    :cond_1
    move v0, v2

    .line 108
    goto :goto_1

    :cond_2
    move v0, v2

    .line 109
    goto :goto_2

    .line 113
    :cond_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    .line 114
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cyd:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3

    .line 120
    :cond_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    .line 121
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;->cye:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_4

    :cond_5
    move v1, v2

    .line 123
    goto :goto_5
.end method
