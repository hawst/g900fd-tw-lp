.class public Lcom/google/android/sidekick/shared/client/NowSearchOptions;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bZx:Landroid/location/Location;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bZy:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cwn:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lfms;

    invoke-direct {v0}, Lfms;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/shared/client/NowSearchOptions;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/location/Location;Ljava/lang/String;Z)V
    .locals 0
    .param p1    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/sidekick/shared/client/NowSearchOptions;->bZx:Landroid/location/Location;

    .line 22
    iput-object p2, p0, Lcom/google/android/sidekick/shared/client/NowSearchOptions;->bZy:Ljava/lang/String;

    .line 23
    iput-boolean p3, p0, Lcom/google/android/sidekick/shared/client/NowSearchOptions;->cwn:Z

    .line 24
    return-void
.end method

.method public static a(Lixx;)Lcom/google/android/sidekick/shared/client/NowSearchOptions;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-virtual {p0}, Lixx;->qQ()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/sidekick/shared/client/NowSearchOptions;

    invoke-virtual {p0}, Lixx;->qP()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/sidekick/shared/client/NowSearchOptions;-><init>(Landroid/location/Location;Ljava/lang/String;Z)V

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public static j(Landroid/location/Location;)Lcom/google/android/sidekick/shared/client/NowSearchOptions;
    .locals 3

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/sidekick/shared/client/NowSearchOptions;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/sidekick/shared/client/NowSearchOptions;-><init>(Landroid/location/Location;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static lR(Ljava/lang/String;)Lcom/google/android/sidekick/shared/client/NowSearchOptions;
    .locals 3

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/sidekick/shared/client/NowSearchOptions;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/sidekick/shared/client/NowSearchOptions;-><init>(Landroid/location/Location;Ljava/lang/String;Z)V

    return-object v0
.end method


# virtual methods
.method public final aqW()Landroid/location/Location;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/sidekick/shared/client/NowSearchOptions;->bZx:Landroid/location/Location;

    return-object v0
.end method

.method public final aqX()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/sidekick/shared/client/NowSearchOptions;->bZy:Ljava/lang/String;

    return-object v0
.end method

.method public final aqr()Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/client/NowSearchOptions;->cwn:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 58
    iget-object v1, p0, Lcom/google/android/sidekick/shared/client/NowSearchOptions;->bZx:Landroid/location/Location;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 59
    iget-object v1, p0, Lcom/google/android/sidekick/shared/client/NowSearchOptions;->bZy:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 60
    iget-boolean v1, p0, Lcom/google/android/sidekick/shared/client/NowSearchOptions;->cwn:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 61
    return-void
.end method
