.class public Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;
.super Landroid/app/Service;
.source "PG"


# instance fields
.field public ckD:Leuc;

.field public cnj:Lfdg;

.field public coV:Lgxc;

.field public cpd:Lfdi;

.field public cqW:Lfgt;

.field public cqX:Lcin;

.field public cqY:Lesm;

.field public cqZ:Livq;

.field public cra:Lhqs;

.field public mCalendarDataProvider:Leym;

.field public mCardsPrefs:Lcxs;

.field public mClock:Lemp;

.field public mEntryProvider:Lfaq;

.field public mExecutedUserActionStore:Lfcr;

.field public mExtraDexReg:Lgtx;

.field public mFreshenRequestManager:Lfil;

.field public mGsaConfigFlags:Lchk;

.field public mImageLoader:Lesm;

.field public mLocationReportingOptInHelper:Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;

.field public mLoginHelper:Lcrh;

.field public mNetworkClient:Lfcx;

.field public mNotificationStore:Lffp;

.field public mNowOptInHelper:Leux;

.field public mReminderSmartActionUtil:Leqm;

.field public mSearchHistoryHelper:Lcrr;

.field public mSidekickInjector:Lfdb;

.field public mTrainingQuestionManager:Lfdn;

.field public mUserInteractionLogger:Lcpx;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 205
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    .prologue
    .line 191
    new-instance v0, Lfgq;

    new-instance v1, Ldmf;

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-direct {v1, v2}, Ldmf;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-direct {v0, p0, v1}, Lfgq;-><init>(Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;Ldmf;)V

    return-object v0
.end method

.method public onCreate()V
    .locals 13

    .prologue
    .line 145
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v7

    .line 146
    invoke-virtual {v7}, Lgql;->aJr()Lfdb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    .line 148
    invoke-virtual {v7}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mGsaConfigFlags:Lchk;

    .line 149
    iget-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mEntryProvider:Lfaq;

    .line 150
    iget-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->Sh()Lfcr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mExecutedUserActionStore:Lfcr;

    .line 151
    invoke-virtual {v7}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DR()Lcpx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mUserInteractionLogger:Lcpx;

    .line 152
    invoke-virtual {v7}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DF()Lcin;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cqX:Lcin;

    .line 153
    iget-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->axN()Leym;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mCalendarDataProvider:Leym;

    .line 154
    iget-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->axQ()Lfdi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cpd:Lfdi;

    .line 155
    invoke-virtual {v7}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DE()Lcxs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mCardsPrefs:Lcxs;

    .line 156
    iget-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->ayc()Lfdn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mTrainingQuestionManager:Lfdn;

    .line 157
    iget-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->ayb()Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mLocationReportingOptInHelper:Lcom/google/android/sidekick/main/location/LocationReportingOptInHelper;

    .line 158
    invoke-virtual {v7}, Lgql;->anM()Lesm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mImageLoader:Lesm;

    .line 159
    invoke-virtual {v7}, Lgql;->aJS()Lesm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cqY:Lesm;

    .line 160
    iget-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    iget-object v0, v0, Lfdb;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DV()Livq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cqZ:Livq;

    .line 161
    invoke-virtual {v7}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DC()Lemp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mClock:Lemp;

    .line 162
    iget-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->axI()Lfcx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mNetworkClient:Lfcx;

    .line 163
    new-instance v0, Lhqs;

    invoke-virtual {v7}, Lgql;->aJU()Lgpu;

    move-result-object v1

    invoke-virtual {v1}, Lgpu;->aJH()Lhlr;

    move-result-object v1

    invoke-virtual {v7}, Lgql;->aJU()Lgpu;

    move-result-object v2

    invoke-virtual {v2}, Lgpu;->aJB()Lhla;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lhqs;-><init>(Lhlr;Lhla;)V

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cra:Lhqs;

    .line 166
    iget-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->axX()Leuc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->ckD:Leuc;

    .line 167
    invoke-virtual {v7}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mLoginHelper:Lcrh;

    .line 168
    iget-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->ayh()Leux;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mNowOptInHelper:Leux;

    .line 169
    iget-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    iget-object v0, v0, Lfdb;->coV:Lgxc;

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->coV:Lgxc;

    .line 170
    invoke-virtual {v7}, Lgql;->aJR()Lcgh;

    move-result-object v0

    invoke-interface {v0}, Lcgh;->EU()Lcrr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSearchHistoryHelper:Lcrr;

    .line 171
    iget-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->axY()Lfdg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cnj:Lfdg;

    .line 172
    iget-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->aym()Leqm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mReminderSmartActionUtil:Leqm;

    .line 173
    iget-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->axH()Lfil;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mFreshenRequestManager:Lfil;

    .line 174
    iget-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    iget-object v0, v0, Lfdb;->mNotificationStore:Lffp;

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mNotificationStore:Lffp;

    .line 175
    invoke-virtual {v7}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->Ev()Lgtx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mExtraDexReg:Lgtx;

    .line 177
    new-instance v0, Lfgt;

    iget-object v1, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mEntryProvider:Lfaq;

    iget-object v2, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    invoke-virtual {v2}, Lfdb;->axT()Lfao;

    move-result-object v2

    invoke-virtual {v7}, Lgql;->El()Lfdr;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cqX:Lcin;

    invoke-virtual {v7}, Lgql;->SC()Lcfo;

    move-result-object v5

    invoke-virtual {v5}, Lcfo;->Em()Lcha;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    invoke-virtual {v6}, Lfdb;->ayf()Lezt;

    move-result-object v6

    invoke-virtual {v7}, Lgql;->SC()Lcfo;

    move-result-object v7

    invoke-virtual {v7}, Lcfo;->DI()Lcpn;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mSidekickInjector:Lfdb;

    invoke-virtual {v8}, Lfdb;->axL()Lfbh;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->coV:Lgxc;

    iget-object v10, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mClock:Lemp;

    iget-object v11, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mNetworkClient:Lfcx;

    iget-object v12, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mExecutedUserActionStore:Lfcr;

    invoke-direct/range {v0 .. v12}, Lfgt;-><init>(Lfaq;Lfao;Lfdr;Lcin;Lcha;Lezt;Lcpn;Lfbh;Lgxc;Lemp;Lfcx;Lfcr;)V

    iput-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->cqW:Lfgt;

    .line 187
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/sidekick/main/remoteservice/GoogleNowRemoteService;->mExecutedUserActionStore:Lfcr;

    invoke-interface {v0}, Lfcr;->axA()V

    .line 199
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 200
    return-void
.end method
