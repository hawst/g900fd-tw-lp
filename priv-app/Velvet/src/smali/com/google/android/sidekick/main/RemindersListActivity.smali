.class public Lcom/google/android/sidekick/main/RemindersListActivity;
.super Lhfa;
.source "PG"


# static fields
.field public static final cjA:Lifw;

.field static final cjB:Lifw;

.field static final cjC:Lifw;

.field private static final cjD:Ljava/util/Collection;

.field public static final cjz:Lifw;


# instance fields
.field private asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

.field private asG:Lcom/google/android/shared/ui/CoScrollContainer;

.field private bFk:Lfmq;

.field private cjE:Lfjs;

.field private cjF:Lflr;

.field private cjG:Z

.field private cjH:[Lizo;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cjI:Levi;

.field public cjJ:Landroid/os/AsyncTask;

.field private cjK:Landroid/content/BroadcastReceiver;

.field private mLoginHelper:Lcrh;

.field private mNetworkClient:Lfcx;

.field private mNowRemoteClient:Lfml;

.field private mProgressBar:Lcom/google/android/sidekick/shared/ui/NowProgressBar;

.field private mRunner:Lerp;

.field private mSidekickInjector:Lfdb;

.field public mUserInteractionLogger:Lcpx;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 89
    new-instance v0, Leuz;

    invoke-direct {v0}, Leuz;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjz:Lifw;

    .line 97
    new-instance v0, Leva;

    invoke-direct {v0}, Leva;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjA:Lifw;

    .line 108
    new-instance v0, Levb;

    invoke-direct {v0}, Levb;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjB:Lifw;

    .line 118
    new-instance v0, Levc;

    invoke-direct {v0}, Levc;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjC:Lifw;

    .line 158
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lijj;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lijj;

    move-result-object v0

    sput-object v0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjD:Ljava/util/Collection;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 182
    new-instance v0, Levi;

    invoke-direct {v0}, Levi;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/sidekick/main/RemindersListActivity;-><init>(Levi;)V

    .line 183
    return-void
.end method

.method public constructor <init>(Levi;)V
    .locals 1

    .prologue
    .line 186
    invoke-direct {p0}, Lhfa;-><init>()V

    .line 176
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjH:[Lizo;

    .line 383
    new-instance v0, Levg;

    invoke-direct {v0, p0}, Levg;-><init>(Lcom/google/android/sidekick/main/RemindersListActivity;)V

    iput-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjK:Landroid/content/BroadcastReceiver;

    .line 187
    iput-object p1, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjI:Levi;

    .line 188
    return-void
.end method

.method public static synthetic a(Lcom/google/android/sidekick/main/RemindersListActivity;)Lcrh;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mLoginHelper:Lcrh;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/sidekick/main/RemindersListActivity;Z)Z
    .locals 0

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjG:Z

    return p1
.end method

.method private awc()V
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DF()Lcin;

    move-result-object v0

    invoke-interface {v0}, Lcin;->Kz()Z

    move-result v0

    if-nez v0, :cond_0

    .line 530
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/RemindersListActivity;->finish()V

    .line 533
    :cond_0
    return-void
.end method

.method public static synthetic b(Lcom/google/android/sidekick/main/RemindersListActivity;)Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/sidekick/main/RemindersListActivity;)Lfdb;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mSidekickInjector:Lfdb;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/sidekick/main/RemindersListActivity;)Lfcx;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mNetworkClient:Lfcx;

    return-object v0
.end method

.method private static ip(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 339
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ENTRY_TREE_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 340
    return-object v0
.end method


# virtual methods
.method public final M(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 354
    if-eqz p1, :cond_2

    .line 355
    const-string v0, "NETWORK_ERROR_KEY"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjG:Z

    .line 358
    iget-boolean v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjG:Z

    if-eqz v0, :cond_0

    .line 359
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/RemindersListActivity;->a([Lizo;)V

    .line 381
    :goto_0
    return-void

    .line 362
    :cond_0
    const-string v0, "NUM_ENTRY_TREES"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 363
    if-lez v2, :cond_2

    .line 365
    :try_start_0
    new-array v3, v2, [Lizo;

    move v0, v1

    .line 366
    :goto_1
    if-ge v0, v2, :cond_1

    .line 367
    invoke-static {v0}, Lcom/google/android/sidekick/main/RemindersListActivity;->ip(I)Ljava/lang/String;

    move-result-object v4

    .line 368
    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v4

    invoke-static {v4}, Lizo;->ag([B)Lizo;

    move-result-object v4

    aput-object v4, v3, v0

    .line 366
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 371
    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/android/sidekick/main/RemindersListActivity;->a([Lizo;)V
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 379
    :cond_2
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mProgressBar:Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->start()V

    .line 380
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjI:Levi;

    invoke-virtual {v0, p0}, Levi;->e(Lcom/google/android/sidekick/main/RemindersListActivity;)Landroid/os/AsyncTask;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjJ:Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public final N(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 537
    invoke-direct {p0}, Lcom/google/android/sidekick/main/RemindersListActivity;->awc()V

    .line 539
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjJ:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 540
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjJ:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 541
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjJ:Landroid/os/AsyncTask;

    .line 544
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/sidekick/main/RemindersListActivity;->M(Landroid/os/Bundle;)V

    .line 545
    return-void
.end method

.method public final a([Lizo;)V
    .locals 9
    .param p1    # [Lizo;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 435
    iput-object p1, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjH:[Lizo;

    .line 436
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mProgressBar:Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/NowProgressBar;->stop()V

    .line 438
    invoke-virtual {p0, p1}, Lcom/google/android/sidekick/main/RemindersListActivity;->b([Lizo;)Ljava/util/List;

    move-result-object v1

    .line 441
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjF:Lflr;

    new-instance v2, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    invoke-direct {v2}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;-><init>()V

    const/4 v3, 0x1

    move-object v6, v5

    move v7, v4

    move v8, v4

    invoke-virtual/range {v0 .. v8}, Lflr;->a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;ZZLjava/util/List;Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;ZZ)V

    .line 443
    return-void
.end method

.method protected final b([Lizo;)Ljava/util/List;
    .locals 8
    .param p1    # [Lizo;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 456
    if-nez p1, :cond_0

    .line 457
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    .line 464
    :goto_0
    return-object v0

    .line 460
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    array-length v4, p1

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v0, p1, v3

    iget-object v5, v0, Lizo;->dUQ:Lizq;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjE:Lfjs;

    iget-object v0, v0, Lizo;->dUQ:Lizq;

    invoke-interface {v5, v0}, Lfjs;->f(Lizq;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkd;

    if-eqz v0, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 461
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_7

    .line 462
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    array-length v4, p1

    move v0, v2

    :goto_2
    if-ge v0, v4, :cond_4

    aget-object v1, p1, v0

    iget-object v5, v1, Lizo;->dUQ:Lizq;

    if-eqz v5, :cond_3

    iget-object v5, v1, Lizo;->dUQ:Lizq;

    iget-object v5, v5, Lizq;->dUX:[Lizj;

    if-eqz v5, :cond_3

    iget-object v1, v1, Lizo;->dUQ:Lizq;

    iget-object v5, v1, Lizq;->dUX:[Lizj;

    array-length v6, v5

    move v1, v2

    :goto_3
    if-ge v1, v6, :cond_3

    aget-object v7, v5, v1

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sget-object v0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjD:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    new-instance v5, Lizj;

    invoke-direct {v5}, Lizj;-><init>()V

    const/16 v1, 0x2c

    invoke-virtual {v5, v1}, Lizj;->nV(I)Lizj;

    new-instance v1, Ljau;

    invoke-direct {v1}, Ljau;-><init>()V

    iput-object v1, v5, Lizj;->dSV:Ljau;

    iget-object v6, v5, Lizj;->dSV:Ljau;

    new-instance v7, Ljhe;

    invoke-direct {v7}, Ljhe;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid card id."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const v1, 0x7f0a03f2

    invoke-virtual {p0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_5
    invoke-virtual {v7, v1}, Ljhe;->uW(Ljava/lang/String;)Ljhe;

    move-result-object v1

    iput-object v1, v6, Ljau;->dQX:Ljhe;

    new-instance v1, Lizq;

    invoke-direct {v1}, Lizq;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid card id."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const v1, 0x7f0a03f3

    invoke-virtual {p0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :pswitch_2
    const v1, 0x7f0a03f4

    invoke-virtual {p0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :pswitch_3
    sget-object v0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjC:Lifw;

    :goto_6
    invoke-static {v3, v0}, Likm;->b(Ljava/lang/Iterable;Lifw;)Ljava/lang/Iterable;

    move-result-object v0

    const-class v6, Lizj;

    invoke-static {v0, v6}, Likm;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizj;

    iput-object v0, v1, Lizq;->dUX:[Lizj;

    iput-object v5, v1, Lizq;->dUZ:Lizj;

    iget-object v0, v1, Lizq;->dUX:[Lizj;

    array-length v0, v0

    if-lez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjE:Lfjs;

    invoke-interface {v0, v1}, Lfjs;->f(Lizq;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :pswitch_4
    sget-object v0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjA:Lifw;

    goto :goto_6

    :pswitch_5
    sget-object v0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjB:Lifw;

    goto :goto_6

    :cond_6
    move-object v0, v2

    goto/16 :goto_0

    :cond_7
    move-object v0, v1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 192
    invoke-static {}, Lckw;->OZ()Lckw;

    move-result-object v0

    .line 193
    invoke-virtual {v0}, Lckw;->Pd()Z

    move-result v1

    if-nez v1, :cond_0

    .line 194
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->BK()Lcke;

    move-result-object v1

    .line 195
    invoke-virtual {v0, v1}, Lckw;->a(Lcke;)V

    .line 197
    :cond_0
    invoke-super {p0, p1}, Lhfa;->onCreate(Landroid/os/Bundle;)V

    .line 199
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/RemindersListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 200
    if-eqz v0, :cond_1

    .line 201
    const v1, 0x7f0a0587

    invoke-virtual {p0, v1}, Lcom/google/android/sidekick/main/RemindersListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->aCL()Lfqn;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lfqn;->jn(I)V

    .line 207
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v7

    .line 208
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mVelvetServices:Lgql;

    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    iput-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mRunner:Lerp;

    .line 209
    invoke-virtual {v7}, Lcfo;->DR()Lcpx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mUserInteractionLogger:Lcpx;

    .line 210
    invoke-virtual {v7}, Lcfo;->DL()Lcrh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mLoginHelper:Lcrh;

    .line 212
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mSidekickInjector:Lfdb;

    .line 213
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->axP()Lfjs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjE:Lfjs;

    .line 214
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->axI()Lfcx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mNetworkClient:Lfcx;

    .line 216
    const v0, 0x7f11025e

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/RemindersListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/ui/SuggestionGridLayout;

    iput-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    .line 217
    const v0, 0x7f110161

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/RemindersListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/ui/CoScrollContainer;

    iput-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    .line 218
    const v0, 0x7f11025f

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/RemindersListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    iput-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mProgressBar:Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    .line 219
    new-instance v0, Lfrd;

    iget-object v2, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    iget-object v3, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v4, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mProgressBar:Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    new-instance v5, Levd;

    invoke-direct {v5, p0}, Levd;-><init>(Lcom/google/android/sidekick/main/RemindersListActivity;)V

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lfrd;-><init>(Landroid/content/Context;Lekf;Lcom/google/android/shared/ui/SuggestionGridLayout;Lfrg;Lfrf;)V

    .line 228
    invoke-virtual {v0}, Lfrd;->axt()V

    .line 230
    const v0, 0x7f1103a5

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/RemindersListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 231
    new-instance v1, Leve;

    invoke-direct {v1, p0}, Leve;-><init>(Lcom/google/android/sidekick/main/RemindersListActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 255
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->ayg()Lfml;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mNowRemoteClient:Lfml;

    .line 256
    new-instance v0, Lewd;

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/RemindersListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mNowRemoteClient:Lfml;

    iget-object v3, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->aJr()Lfdb;

    move-result-object v3

    iget-object v3, v3, Lfdb;->mUndoDismissManager:Lfnn;

    iget-object v4, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mVelvetServices:Lgql;

    iget-object v4, v4, Lgql;->mAsyncServices:Lema;

    iget-object v5, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mVelvetServices:Lgql;

    invoke-virtual {v5}, Lgql;->aJq()Lhhq;

    move-result-object v5

    invoke-virtual {v5}, Lhhq;->aAs()Lequ;

    move-result-object v5

    invoke-virtual {v7}, Lcfo;->DC()Lemp;

    move-result-object v6

    invoke-virtual {v7}, Lcfo;->BK()Lcke;

    move-result-object v7

    const/4 v8, 0x1

    invoke-direct/range {v0 .. v8}, Lewd;-><init>(Landroid/content/Context;Lfml;Lfnn;Lern;Lequ;Lemp;Lcke;Z)V

    .line 266
    new-instance v1, Lflr;

    iget-object v3, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mRunner:Lerp;

    iget-object v5, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v6, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    move-object v2, p0

    move-object v4, v10

    move-object v7, v0

    move-object v8, v10

    move-object v9, v10

    invoke-direct/range {v1 .. v9}, Lflr;-><init>(Landroid/app/Activity;Lerp;Lflv;Lcom/google/android/shared/ui/SuggestionGridLayout;Lekf;Lfmt;Lgbr;Lflb;)V

    iput-object v1, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjF:Lflr;

    .line 269
    if-eqz p1, :cond_2

    .line 270
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjF:Lflr;

    invoke-virtual {v0, p1}, Lflr;->V(Landroid/os/Bundle;)V

    .line 273
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/sidekick/main/RemindersListActivity;->N(Landroid/os/Bundle;)V

    .line 274
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 425
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjJ:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjJ:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 427
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjJ:Landroid/os/AsyncTask;

    .line 430
    :cond_0
    invoke-super {p0}, Lhfa;->onDestroy()V

    .line 431
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 345
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 346
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/RemindersListActivity;->onBackPressed()V

    .line 347
    const/4 v0, 0x1

    .line 349
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lhfa;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 414
    invoke-super {p0}, Lhfa;->onResume()V

    .line 415
    invoke-direct {p0}, Lcom/google/android/sidekick/main/RemindersListActivity;->awc()V

    .line 416
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 325
    invoke-super {p0, p1}, Lhfa;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 327
    const-string v0, "NETWORK_ERROR_KEY"

    iget-boolean v1, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjG:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 328
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjH:[Lizo;

    if-eqz v0, :cond_0

    .line 329
    const-string v0, "NUM_ENTRY_TREES"

    iget-object v1, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjH:[Lizo;

    array-length v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 330
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjH:[Lizo;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 331
    invoke-static {v0}, Lcom/google/android/sidekick/main/RemindersListActivity;->ip(I)Ljava/lang/String;

    move-result-object v1

    .line 332
    iget-object v2, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjH:[Lizo;

    aget-object v2, v2, v0

    invoke-static {v2}, Lizo;->m(Ljsr;)[B

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 330
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjF:Lflr;

    invoke-virtual {v0, p1}, Lflr;->U(Landroid/os/Bundle;)V

    .line 336
    return-void
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 394
    invoke-super {p0}, Lhfa;->onStart()V

    .line 395
    invoke-static {p0}, Lcn;->a(Landroid/content/Context;)Lcn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjK:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.android.apps.sidekick.DATA_BACKEND_VERSION_STORE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcn;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 397
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->mNowRemoteClient:Lfml;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfml;->lQ(Ljava/lang/String;)Lfmq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->bFk:Lfmq;

    .line 402
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->bFk:Lfmq;

    invoke-virtual {v0}, Lfmq;->aBf()Z

    .line 403
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 407
    invoke-super {p0}, Lhfa;->onStop()V

    .line 408
    invoke-static {p0}, Lcn;->a(Landroid/content/Context;)Lcn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cjK:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcn;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 409
    iget-object v0, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->bFk:Lfmq;

    invoke-virtual {v0}, Lfmq;->release()V

    .line 410
    return-void
.end method

.method protected final wd()I
    .locals 1

    .prologue
    .line 278
    const v0, 0x7f040151

    return v0
.end method

.method protected final we()Lfoh;
    .locals 2

    .prologue
    .line 283
    new-instance v0, Levf;

    iget-object v1, p0, Lcom/google/android/sidekick/main/RemindersListActivity;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-direct {v0, p0, p0, v1}, Levf;-><init>(Lcom/google/android/sidekick/main/RemindersListActivity;Landroid/app/Activity;Landroid/view/View;)V

    return-object v0
.end method
