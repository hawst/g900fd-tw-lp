.class public Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;
.super Landroid/widget/ScrollView;
.source "PG"


# instance fields
.field private final czq:Landroid/graphics/drawable/Drawable;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private czr:Landroid/graphics/drawable/Drawable;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private czs:Z

.field private czt:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    if-eqz p2, :cond_0

    .line 35
    sget-object v0, Lbwe;->aMb:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 37
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->czq:Landroid/graphics/drawable/Drawable;

    .line 39
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->czr:Landroid/graphics/drawable/Drawable;

    .line 41
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 46
    :goto_0
    return-void

    .line 43
    :cond_0
    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->czq:Landroid/graphics/drawable/Drawable;

    .line 44
    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->czr:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private aCK()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 76
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->czq:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->canScrollVertically(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->czs:Z

    .line 77
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->czr:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->canScrollVertically(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->czt:Z

    .line 78
    return-void

    :cond_0
    move v0, v2

    .line 76
    goto :goto_0

    :cond_1
    move v1, v2

    .line 77
    goto :goto_1
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onDraw(Landroid/graphics/Canvas;)V

    .line 84
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 85
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->getScrollY()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 86
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->czs:Z

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->czq:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 89
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->czt:Z

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->czr:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 92
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 93
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 50
    invoke-super/range {p0 .. p5}, Landroid/widget/ScrollView;->onLayout(ZIIII)V

    .line 51
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->czq:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->czq:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->czq:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->czr:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 59
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->czr:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->czr:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->getHeight()I

    move-result v3

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 65
    :cond_1
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->aCK()V

    .line 66
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 0

    .prologue
    .line 70
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 72
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/training/ScrollViewWithEdgeDrawable;->aCK()V

    .line 73
    return-void
.end method
