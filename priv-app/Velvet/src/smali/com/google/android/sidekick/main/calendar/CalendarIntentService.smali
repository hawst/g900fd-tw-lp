.class public Lcom/google/android/sidekick/main/calendar/CalendarIntentService;
.super Landroid/app/IntentService;
.source "PG"


# static fields
.field private static final cmf:Ljava/util/Set;

.field private static final cmg:Landroid/content/BroadcastReceiver;


# instance fields
.field private cmh:Leyu;

.field private mSidekickInjector:Lfdb;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    .line 52
    sget-object v0, Lfzy;->cEd:Ljava/lang/String;

    sget-object v1, Lfzy;->cEe:Ljava/lang/String;

    sget-object v2, Lfzy;->cEf:Ljava/lang/String;

    sget-object v3, Lfzy;->cEg:Ljava/lang/String;

    sget-object v4, Lfzy;->cEh:Ljava/lang/String;

    sget-object v5, Lfzy;->cEi:Ljava/lang/String;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    sget-object v8, Lfzy;->cEj:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, Lijp;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Lijp;

    move-result-object v0

    sput-object v0, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->cmf:Ljava/util/Set;

    .line 62
    new-instance v0, Leyw;

    invoke-direct {v0}, Leyw;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->cmg:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 69
    const-string v0, "CalendarIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 70
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->setIntentRedelivery(Z)V

    .line 71
    return-void
.end method

.method public static H(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 331
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 332
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;IJ)V
    .locals 5

    .prologue
    .line 259
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->Ea()Ldjx;

    move-result-object v0

    .line 261
    invoke-static {p0, p1}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->i(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 263
    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-gtz v2, :cond_0

    .line 265
    invoke-virtual {v0, v1}, Ldjx;->cancel(Landroid/app/PendingIntent;)V

    .line 273
    :goto_0
    return-void

    .line 271
    :cond_0
    invoke-virtual {v0, p2, p3, p4, v1}, Ldjx;->setExact(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public static aC(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lfzy;->cEd:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->h(Landroid/content/Context;Ljava/lang/String;)V

    .line 167
    return-void
.end method

.method public static aD(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 177
    sget-object v0, Lfzy;->cEe:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->i(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 179
    sget-object v1, Lfzy;->cEf:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->i(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 181
    sget-object v2, Lfzy;->cEg:Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->i(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 183
    sget-object v3, Lfzy;->cEh:Ljava/lang/String;

    invoke-static {p0, v3}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->i(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v3

    .line 187
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v4

    invoke-virtual {v4}, Lgql;->SC()Lcfo;

    move-result-object v4

    invoke-virtual {v4}, Lcfo;->Ea()Ldjx;

    move-result-object v4

    .line 189
    invoke-virtual {v4, v0}, Ldjx;->cancel(Landroid/app/PendingIntent;)V

    .line 190
    invoke-virtual {v4, v1}, Ldjx;->cancel(Landroid/app/PendingIntent;)V

    .line 191
    invoke-virtual {v4, v2}, Ldjx;->cancel(Landroid/app/PendingIntent;)V

    .line 192
    invoke-virtual {v4, v3}, Ldjx;->cancel(Landroid/app/PendingIntent;)V

    .line 195
    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 196
    invoke-virtual {v1}, Landroid/app/PendingIntent;->cancel()V

    .line 197
    invoke-virtual {v2}, Landroid/app/PendingIntent;->cancel()V

    .line 198
    invoke-virtual {v3}, Landroid/app/PendingIntent;->cancel()V

    .line 199
    return-void
.end method

.method public static c(Lfdb;)V
    .locals 3

    .prologue
    .line 207
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 208
    const-string v1, "com.google.android.apps.sidekick.LOCATION_CHANGED_SIGNIFICANTLY"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 209
    iget-object v1, p0, Lfdb;->mLocalBroadcastManager:Lcn;

    sget-object v2, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->cmg:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lcn;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 211
    return-void
.end method

.method public static d(Lfdb;)V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lfdb;->mLocalBroadcastManager:Lcn;

    sget-object v1, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->cmg:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcn;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 219
    return-void
.end method

.method public static h(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 232
    sget-object v0, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->cmf:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    const-string v0, "CalendarIntentService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendIntentWithAction ignoring call with unexpected action: \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    :goto_0
    return-void

    .line 238
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 240
    new-instance v1, Landroid/content/Intent;

    const/4 v2, 0x0

    const-class v3, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;

    invoke-direct {v1, p1, v2, v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 241
    invoke-virtual {p0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public static i(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 285
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 286
    new-instance v1, Landroid/content/Intent;

    const/4 v2, 0x0

    const-class v3, Lcom/google/android/sidekick/main/calendar/CalendarIntentService$CalendarReceiver;

    invoke-direct {v1, p1, v2, v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 287
    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v0, v2, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static j(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 300
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 301
    new-instance v2, Landroid/content/Intent;

    const/4 v3, 0x0

    const-class v4, Lcom/google/android/sidekick/main/calendar/CalendarIntentService$CalendarReceiver;

    invoke-direct {v2, p1, v3, v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 302
    const/high16 v3, 0x20000000

    invoke-static {v1, v0, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static k(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 5

    .prologue
    .line 315
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 318
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "calendar_notification"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 319
    new-instance v2, Landroid/content/Intent;

    sget-object v3, Lfzy;->cEi:Ljava/lang/String;

    const-class v4, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;

    invoke-direct {v2, v3, v1, v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 321
    const/4 v1, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onCreate()V
    .locals 10

    .prologue
    .line 78
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v9

    .line 79
    invoke-virtual {v9}, Lgql;->aJP()V

    .line 81
    invoke-virtual {v9}, Lgql;->aJr()Lfdb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->mSidekickInjector:Lfdb;

    .line 82
    new-instance v0, Leyu;

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v2}, Lfdb;->axN()Leym;

    move-result-object v2

    invoke-virtual {v9}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->DC()Lemp;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->mSidekickInjector:Lfdb;

    iget-object v4, v4, Lfdb;->mLocationOracle:Lfdr;

    iget-object v5, p0, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v5}, Lfdb;->axI()Lfcx;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v6}, Lfdb;->axO()Lfga;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v7}, Lfdb;->axR()Lfjs;

    move-result-object v7

    invoke-virtual {v9}, Lgql;->SC()Lcfo;

    move-result-object v8

    invoke-virtual {v8}, Lcfo;->Ea()Ldjx;

    move-result-object v8

    invoke-virtual {v9}, Lgql;->SC()Lcfo;

    move-result-object v9

    invoke-virtual {v9}, Lcfo;->DE()Lcxs;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, Leyu;-><init>(Landroid/content/Context;Leym;Lemp;Lfdr;Lfcx;Lfga;Lfjs;Ldjx;Lcxs;)V

    iput-object v0, p0, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->cmh:Leyu;

    .line 94
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 95
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 152
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 153
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 100
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 101
    :cond_0
    const-string v0, "CalendarIntentService"

    const-string v1, "onHandleIntent: received unexpected null or empty Intent"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    :goto_0
    return-void

    .line 105
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 106
    sget-object v1, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->cmf:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 107
    const-string v1, "CalendarIntentService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onHandleIntent: received Intent with unexpect action: \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 124
    :cond_2
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 125
    const/4 v1, 0x1

    const-string v2, "CalendarIntentService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    .line 129
    :try_start_0
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 135
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DF()Lcin;

    move-result-object v0

    invoke-interface {v0}, Lcin;->KG()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_3

    .line 144
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    .line 141
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->cmh:Leyu;

    invoke-virtual {v0, p1}, Leyu;->G(Landroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method
