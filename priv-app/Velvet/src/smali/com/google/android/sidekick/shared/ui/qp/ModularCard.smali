.class public Lcom/google/android/sidekick/shared/ui/qp/ModularCard;
.super Lfqw;
.source "PG"


# instance fields
.field private bXh:Z

.field private cBX:Landroid/graphics/drawable/ColorDrawable;

.field private cBY:I

.field private cBZ:I

.field private cCa:Landroid/graphics/Paint;

.field private cCb:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lfqw;-><init>(Landroid/content/Context;)V

    .line 36
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->aDr()V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lfqw;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->aDr()V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lfqw;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->aDr()V

    .line 47
    return-void
.end method

.method private aDr()V
    .locals 4

    .prologue
    .line 50
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    .line 52
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 54
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 55
    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 57
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->setAnimateParentHierarchy(Z)V

    .line 58
    return-void
.end method

.method private static bx(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 262
    const v0, 0x7f110011

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-eq v0, v1, :cond_0

    instance-of v0, p0, Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private jF(I)Z
    .locals 6

    .prologue
    const v5, 0x7f110012

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 213
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getChildCount()I

    move-result v0

    .line 216
    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    move v0, v2

    .line 237
    :goto_0
    return v0

    .line 218
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 219
    add-int/lit8 v1, p1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 220
    invoke-static {v0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->bx(Landroid/view/View;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v1}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->bx(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    move v0, v2

    .line 221
    goto :goto_0

    .line 224
    :cond_2
    invoke-virtual {v0, v5}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 225
    invoke-virtual {v1, v5}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 228
    if-nez v0, :cond_3

    move v5, v3

    :goto_1
    if-nez v1, :cond_4

    move v4, v3

    :goto_2
    if-eq v5, v4, :cond_5

    move v0, v2

    .line 229
    goto :goto_0

    :cond_3
    move v5, v2

    .line 228
    goto :goto_1

    :cond_4
    move v4, v2

    goto :goto_2

    .line 233
    :cond_5
    if-eqz v0, :cond_6

    if-eqz v1, :cond_6

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v2

    .line 234
    goto :goto_0

    :cond_6
    move v0, v3

    .line 237
    goto :goto_0
.end method


# virtual methods
.method protected final aCQ()V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cCb:Landroid/view/View;

    .line 82
    return-void
.end method

.method public final aDs()V
    .locals 2

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00be

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cBY:I

    .line 65
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget v1, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cBY:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cBX:Landroid/graphics/drawable/ColorDrawable;

    .line 66
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d018d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cBZ:I

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->bXh:Z

    .line 68
    return-void
.end method

.method protected final br(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 139
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const v1, 0x7f11000d

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final bs(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cCb:Landroid/view/View;

    .line 73
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cCa:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cCa:Landroid/graphics/Paint;

    .line 76
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->invalidate()V

    .line 77
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const v12, 0x7f110012

    const/4 v2, 0x0

    const/4 v11, 0x0

    .line 87
    invoke-super {p0, p1}, Lfqw;->onDraw(Landroid/graphics/Canvas;)V

    .line 90
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->bXh:Z

    if-eqz v0, :cond_3

    .line 91
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getChildCount()I

    move-result v4

    .line 92
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getMeasuredWidth()I

    move-result v5

    .line 93
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getPaddingLeft()I

    move-result v6

    .line 94
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getPaddingRight()I

    move-result v7

    move v3, v2

    .line 95
    :goto_0
    if-ge v3, v4, :cond_3

    .line 96
    invoke-virtual {p0, v3}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v8, 0x8

    if-eq v1, v8, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->jF(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cBX:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v8

    sub-int v9, v5, v7

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    iget v10, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cBZ:I

    add-int/2addr v0, v10

    invoke-virtual {v1, v6, v8, v9, v0}, Landroid/graphics/drawable/ColorDrawable;->setBounds(IIII)V

    .line 102
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-eq v3, v0, :cond_1

    invoke-virtual {p0, v3}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    add-int/lit8 v1, v3, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v12}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v1, v12}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 103
    :goto_1
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cBX:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/ColorDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 104
    if-nez v0, :cond_2

    .line 105
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cBX:Landroid/graphics/drawable/ColorDrawable;

    iget v1, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cBY:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 106
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cBX:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ColorDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 95
    :cond_0
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 102
    goto :goto_1

    .line 109
    :cond_2
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cBX:Landroid/graphics/drawable/ColorDrawable;

    const/high16 v8, -0x4d000000

    const v9, 0xffffff

    and-int/2addr v0, v9

    or-int/2addr v0, v8

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 111
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cBX:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ColorDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2

    .line 120
    :cond_3
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cCb:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 121
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cCb:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    .line 122
    cmpl-float v1, v0, v11

    if-eqz v1, :cond_4

    .line 123
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 124
    cmpl-float v1, v0, v11

    if-lez v1, :cond_5

    .line 125
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cCb:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cCb:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v11, v1, v0, v2}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 130
    :goto_3
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cCa:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0074

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 131
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cCa:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    .line 132
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 135
    :cond_4
    return-void

    .line 127
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sub-float v0, v1, v0

    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cCb:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cCb:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    goto :goto_3
.end method

.method protected onLayout(ZIIII)V
    .locals 11

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getChildCount()I

    move-result v2

    .line 147
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getPaddingTop()I

    move-result v1

    .line 148
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getMeasuredWidth()I

    move-result v3

    .line 149
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getPaddingLeft()I

    move-result v4

    .line 150
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getPaddingRight()I

    move-result v5

    .line 151
    const/4 v0, 0x0

    move v10, v0

    move v0, v1

    move v1, v10

    :goto_0
    if-ge v1, v2, :cond_1

    .line 152
    invoke-virtual {p0, v1}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 153
    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-eq v7, v8, :cond_0

    .line 154
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    .line 155
    sub-int v8, v3, v5

    add-int v9, v0, v7

    invoke-virtual {v6, v4, v0, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 157
    add-int/2addr v0, v7

    .line 159
    iget-boolean v6, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->bXh:Z

    if-eqz v6, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->jF(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 160
    iget v6, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cBZ:I

    add-int/2addr v0, v6

    .line 151
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 164
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 170
    .line 176
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    if-nez v1, :cond_1

    .line 183
    :goto_0
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 184
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getChildCount()I

    move-result v5

    move v4, v0

    move v2, v0

    move v3, v0

    .line 185
    :goto_1
    if-ge v4, v5, :cond_2

    .line 186
    invoke-virtual {p0, v4}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 187
    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v7, 0x8

    if-eq v0, v7, :cond_4

    .line 189
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 191
    iget v7, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v8, -0x2

    if-eq v7, v8, :cond_3

    .line 192
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 196
    :goto_2
    invoke-virtual {v6, p1, v0}, Landroid/view/View;->measure(II)V

    .line 197
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v3

    .line 198
    iget-boolean v3, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->bXh:Z

    if-eqz v3, :cond_0

    invoke-direct {p0, v4}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->jF(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 199
    iget v3, p0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->cBZ:I

    add-int/2addr v0, v3

    .line 201
    :cond_0
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    move v9, v2

    move v2, v0

    move v0, v9

    .line 185
    :goto_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    move v2, v0

    goto :goto_1

    .line 179
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    goto :goto_0

    .line 205
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getPaddingLeft()I

    move-result v0

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getPaddingTop()I

    move-result v1

    add-int/2addr v1, v3

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->setMeasuredDimension(II)V

    .line 208
    return-void

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v2

    move v2, v3

    goto :goto_3
.end method
