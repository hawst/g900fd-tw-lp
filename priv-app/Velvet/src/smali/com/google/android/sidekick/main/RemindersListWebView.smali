.class public Lcom/google/android/sidekick/main/RemindersListWebView;
.super Landroid/app/Activity;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 23
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    invoke-static {}, Lckw;->OZ()Lckw;

    move-result-object v0

    invoke-virtual {v0}, Lckw;->Pb()Z

    move-result v0

    if-nez v0, :cond_0

    .line 26
    const-string v0, "Not enabled for a non-debug build"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 27
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/RemindersListWebView;->finish()V

    .line 78
    :goto_0
    return-void

    .line 31
    :cond_0
    new-instance v0, Landroid/webkit/WebView;

    invoke-direct {v0, p0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 32
    invoke-static {}, Letc;->avI()V

    .line 36
    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 37
    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 39
    new-instance v1, Levl;

    invoke-direct {v1, p0, p0}, Levl;-><init>(Lcom/google/android/sidekick/main/RemindersListWebView;Landroid/app/Activity;)V

    .line 70
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 72
    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/RemindersListWebView;->setContentView(Landroid/view/View;)V

    .line 74
    const-string v1, "<html><body><ul><li><a href=\"intent:#Intent;action=com.google.android.googlequicksearchbox.MY_REMINDERS;package=com.google.android.googlequicksearchbox;end\">Intent with action</a></li></ul></body></html>"

    .line 77
    const-string v2, "text/html"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
