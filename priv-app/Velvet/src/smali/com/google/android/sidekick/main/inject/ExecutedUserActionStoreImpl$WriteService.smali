.class public Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl$WriteService;
.super Landroid/app/IntentService;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 312
    const-string v0, "ExecutedUserActionStoreImpl_WriteService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 313
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl$WriteService;->setIntentRedelivery(Z)V

    .line 314
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 319
    if-nez p1, :cond_1

    .line 338
    :cond_0
    :goto_0
    return-void

    .line 323
    :cond_1
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->Sh()Lfcr;

    move-result-object v0

    .line 326
    instance-of v1, v0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;

    if-nez v1, :cond_2

    .line 327
    const-string v0, "ExecutedUserActionStoreImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected ExecutedUserActionStore implementation, dropping intent:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 332
    :cond_2
    check-cast v0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;

    .line 333
    const-string v1, "write"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 334
    invoke-virtual {v0, p1}, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->I(Landroid/content/Intent;)V

    goto :goto_0

    .line 335
    :cond_3
    const-string v1, "delete"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 336
    invoke-virtual {v0}, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->axD()V

    goto :goto_0
.end method
