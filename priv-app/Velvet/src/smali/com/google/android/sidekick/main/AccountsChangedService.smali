.class public Lcom/google/android/sidekick/main/AccountsChangedService;
.super Landroid/app/IntentService;
.source "PG"


# instance fields
.field private mLoginHelper:Lcrh;

.field private mNowOptInSettings:Lcin;

.field private mSearchConfig:Lcjs;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    const-string v0, "AccountsChangedService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method private avV()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 103
    iget-object v0, p0, Lcom/google/android/sidekick/main/AccountsChangedService;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v3

    .line 104
    if-nez v3, :cond_0

    move v0, v1

    .line 131
    :goto_0
    return v0

    .line 110
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/AccountsChangedService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v4, 0x0

    iget-object v5, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v4, v5}, Lbex;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Lbew; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 120
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 121
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/AccountChangeEvent;

    .line 122
    invoke-virtual {v0}, Lcom/google/android/gms/auth/AccountChangeEvent;->xO()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 123
    invoke-virtual {v0}, Lcom/google/android/gms/auth/AccountChangeEvent;->xP()Ljava/lang/String;

    move-result-object v0

    .line 124
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v4, v1

    aput-object v0, v4, v2

    .line 126
    iget-object v1, p0, Lcom/google/android/sidekick/main/AccountsChangedService;->mLoginHelper:Lcrh;

    invoke-virtual {v1, v0}, Lcrh;->iA(Ljava/lang/String;)V

    move v0, v2

    .line 127
    goto :goto_0

    .line 112
    :catch_0
    move-exception v0

    .line 113
    const-string v2, "Error getting account change events"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v0, v1

    .line 114
    goto :goto_0

    .line 115
    :catch_1
    move-exception v0

    .line 116
    const-string v2, "Error getting account change events"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v0, v1

    .line 117
    goto :goto_0

    :cond_1
    move v0, v1

    .line 131
    goto :goto_0
.end method


# virtual methods
.method public onCreate()V
    .locals 2

    .prologue
    .line 40
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 42
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lcfo;->DD()Lcjs;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/sidekick/main/AccountsChangedService;->mSearchConfig:Lcjs;

    .line 46
    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/sidekick/main/AccountsChangedService;->mLoginHelper:Lcrh;

    .line 47
    invoke-virtual {v0}, Lcfo;->DF()Lcin;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/AccountsChangedService;->mNowOptInSettings:Lcin;

    .line 48
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 53
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/sidekick/main/AccountsChangedService;->avV()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/sidekick/main/AccountsChangedService;->mLoginHelper:Lcrh;

    invoke-virtual {v1}, Lcrh;->refresh()V

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/AccountsChangedService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/sidekick/main/AccountsChangedService;->mSearchConfig:Lcjs;

    invoke-static {v1, v2}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->b(Landroid/content/Context;Lcjs;)Z

    iget-object v1, p0, Lcom/google/android/sidekick/main/AccountsChangedService;->mLoginHelper:Lcrh;

    invoke-virtual {v1}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/sidekick/main/AccountsChangedService;->mNowOptInSettings:Lcin;

    invoke-interface {v2, v1}, Lcin;->e(Landroid/accounts/Account;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->axG()Lfcl;

    move-result-object v0

    invoke-virtual {v0, v1}, Lfcl;->F(Landroid/accounts/Account;)V

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v0

    invoke-virtual {v0}, Lfaq;->invalidate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    :cond_0
    invoke-static {p1}, Lcom/google/android/sidekick/main/AccountsChangedReceiver;->b(Landroid/content/Intent;)Z

    .line 58
    return-void

    .line 57
    :catchall_0
    move-exception v0

    invoke-static {p1}, Lcom/google/android/sidekick/main/AccountsChangedReceiver;->b(Landroid/content/Intent;)Z

    throw v0
.end method
