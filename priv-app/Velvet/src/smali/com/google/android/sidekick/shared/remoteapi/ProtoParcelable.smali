.class public Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field cxV:Z

.field cxW:Ljsr;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field rQ:[B
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    new-instance v0, Lfov;

    invoke-direct {v0}, Lfov;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljsr;)V
    .locals 1
    .param p1    # Ljsr;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p1, p0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->cxW:Ljsr;

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->cxV:Z

    .line 112
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1    # [B
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object p1, p0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->rQ:[B

    .line 117
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->cxV:Z

    .line 118
    return-void
.end method

.method public static a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 159
    invoke-virtual {p0}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 160
    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a([BLjava/lang/Class;)Ljsr;

    move-result-object v0

    return-object v0
.end method

.method private static a([BLjava/lang/Class;)Ljsr;
    .locals 2
    .param p0    # [B
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 126
    if-nez p0, :cond_0

    .line 127
    const/4 v0, 0x0

    .line 132
    :goto_0
    return-object v0

    .line 131
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    goto :goto_0

    .line 133
    :catch_0
    move-exception v0

    .line 134
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 135
    :catch_1
    move-exception v0

    .line 136
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 137
    :catch_2
    move-exception v0

    .line 138
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Ljava/util/List;Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 169
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 170
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;

    .line 171
    invoke-static {v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 173
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 174
    return-void
.end method

.method public static a(Ljsr;Landroid/os/Parcel;)V
    .locals 1
    .param p0    # Ljsr;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 147
    if-eqz p0, :cond_0

    invoke-static {p0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    .line 148
    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 149
    return-void

    .line 147
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/util/List;
    .locals 3

    .prologue
    .line 181
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 182
    const-class v1, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 183
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 184
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 185
    invoke-virtual {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 187
    :cond_0
    return-object v1
.end method

.method public static i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;
    .locals 1
    .param p0    # Ljsr;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    invoke-direct {v0, p0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;-><init>(Ljsr;)V

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return v0
.end method

.method public final e(Ljava/lang/Class;)Ljsr;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->cxW:Ljsr;

    if-nez v0, :cond_1

    .line 59
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->cxV:Z

    if-eqz v0, :cond_0

    .line 61
    const/4 v0, 0x0

    .line 70
    :goto_0
    return-object v0

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->rQ:[B

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a([BLjava/lang/Class;)Ljsr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->cxW:Ljsr;

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->cxV:Z

    .line 70
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->cxW:Ljsr;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 71
    :catch_0
    move-exception v0

    .line 72
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->rQ:[B

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->cxV:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->cxW:Ljsr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->cxW:Ljsr;

    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->rQ:[B

    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->rQ:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 93
    return-void
.end method
