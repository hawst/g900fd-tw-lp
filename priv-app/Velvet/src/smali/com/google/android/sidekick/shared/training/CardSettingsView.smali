.class public Lcom/google/android/sidekick/shared/training/CardSettingsView;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lejl;
.implements Lfqc;
.implements Lhj;


# static fields
.field private static final cyR:Landroid/animation/ArgbEvaluator;

.field private static final cyS:I


# instance fields
.field private cor:Landroid/widget/ImageButton;

.field private cos:Lcom/google/android/sidekick/shared/ui/PageIndicatorView;

.field private cuA:Lfkd;

.field private cyI:Landroid/support/v4/view/ViewPager;

.field private cyJ:Landroid/view/View;

.field private cyK:Landroid/widget/ImageButton;

.field private cyL:Landroid/widget/ImageButton;

.field private cyM:Landroid/graphics/drawable/Drawable;

.field private cyN:Landroid/graphics/drawable/Drawable;

.field private cyO:Lfpp;

.field private cyP:Lfqf;

.field private cyQ:I

.field private cyT:Z

.field private cyU:Z

.field private cyV:F

.field private cyW:Landroid/view/View;

.field private cyX:Lcom/google/android/sidekick/shared/training/ClippableTextView;

.field private final iA:Landroid/graphics/Rect;

.field private mCardContainer:Lfmt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyR:Landroid/animation/ArgbEvaluator;

    .line 72
    const v0, 0x7f0b00b0

    sput v0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyS:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 68
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyQ:I

    .line 73
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->iA:Landroid/graphics/Rect;

    .line 76
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyV:F

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyQ:I

    .line 73
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->iA:Landroid/graphics/Rect;

    .line 76
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyV:F

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 68
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyQ:I

    .line 73
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->iA:Landroid/graphics/Rect;

    .line 76
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyV:F

    .line 96
    return-void
.end method

.method private aCH()V
    .locals 3

    .prologue
    .line 191
    invoke-static {}, Leot;->afY()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyI:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->aI()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 194
    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyP:Lfqf;

    invoke-virtual {v1}, Lfqf;->getCount()I

    move-result v1

    if-le v1, v0, :cond_1

    .line 195
    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyI:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->c(IZ)V

    .line 199
    :goto_1
    return-void

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyI:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->aI()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyO:Lfpp;

    invoke-interface {v0}, Lfpp;->aAI()V

    goto :goto_1
.end method

.method private b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 317
    sparse-switch p2, :sswitch_data_0

    .line 334
    invoke-static {p2}, Lfqe;->jh(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 335
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cuA:Lfkd;

    invoke-interface {v1}, Lfkd;->getEntry()Lizj;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, Lfqe;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;ILandroid/content/Context;Lizj;)V

    .line 342
    :goto_0
    return-void

    .line 320
    :sswitch_0
    iput p2, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyQ:I

    .line 321
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyO:Lfpp;

    invoke-interface {v0}, Lfpp;->aAI()V

    goto :goto_0

    .line 326
    :sswitch_1
    iput p2, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyQ:I

    .line 327
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->aCH()V

    goto :goto_0

    .line 331
    :sswitch_2
    const-string v0, "CardSettingsView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring icebreaker client action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 338
    :cond_0
    const-string v0, "CardSettingsView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized client action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 317
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x4 -> :sswitch_1
        0x9 -> :sswitch_2
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public static f(Lfkd;)Z
    .locals 1

    .prologue
    .line 185
    invoke-interface {p0}, Lfkd;->getEntry()Lizj;

    move-result-object v0

    iget-object v0, v0, Lizj;->dTp:[Ljdj;

    array-length v0, v0

    if-gtz v0, :cond_0

    invoke-interface {p0}, Lfkd;->aAA()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private jd(I)I
    .locals 1

    .prologue
    .line 252
    invoke-static {}, Leot;->afY()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyP:Lfqf;

    invoke-virtual {v0}, Lfqf;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sub-int p1, v0, p1

    :cond_0
    return p1
.end method

.method private je(I)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 268
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyP:Lfqf;

    invoke-virtual {v0}, Lfqf;->getCount()I

    move-result v0

    add-int/lit8 v4, p1, 0x1

    if-le v0, v4, :cond_1

    move v0, v1

    .line 269
    :goto_0
    iget-object v4, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyL:Landroid/widget/ImageButton;

    invoke-virtual {v4, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 270
    iget-object v4, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyL:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyN:Landroid/graphics/drawable/Drawable;

    :goto_1
    invoke-virtual {v4, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 271
    if-lez p1, :cond_3

    .line 272
    :goto_2
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyK:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 273
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyK:Landroid/widget/ImageButton;

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyM:Landroid/graphics/drawable/Drawable;

    :cond_0
    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 274
    return-void

    :cond_1
    move v0, v2

    .line 268
    goto :goto_0

    :cond_2
    move-object v0, v3

    .line 270
    goto :goto_1

    :cond_3
    move v1, v2

    .line 271
    goto :goto_2
.end method


# virtual methods
.method public final a(IFI)V
    .locals 6

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyP:Lfqf;

    iget-object v1, v0, Lfqf;->czx:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    if-ltz p1, :cond_0

    iget-object v1, v0, Lfqf;->czx:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le p1, v1, :cond_4

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyW:Landroid/view/View;

    .line 224
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyW:Landroid/view/View;

    const v1, 0x7f110257

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/training/ClippableTextView;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyX:Lcom/google/android/sidekick/shared/training/ClippableTextView;

    .line 229
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyT:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyU:Z

    if-nez v0, :cond_1

    .line 230
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyW:Landroid/view/View;

    iget v1, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyV:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 235
    :cond_1
    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cor:Landroid/widget/ImageButton;

    float-to-double v2, p2

    const-wide v4, 0x3fa999999999999aL    # 0.05

    cmpg-double v0, v2, v4

    if-ltz v0, :cond_2

    float-to-double v2, p2

    const-wide v4, 0x3fee666666666666L    # 0.95

    cmpl-double v0, v2, v4

    if-lez v0, :cond_5

    :cond_2
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 238
    if-nez p3, :cond_3

    .line 239
    invoke-direct {p0, p1}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->jd(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->je(I)V

    .line 240
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cos:Lcom/google/android/sidekick/shared/ui/PageIndicatorView;

    invoke-direct {p0, p1}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->jd(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->jp(I)V

    .line 242
    :cond_3
    return-void

    .line 223
    :cond_4
    iget-object v0, v0, Lfqf;->czx:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqg;

    iget-object v0, v0, Lfqg;->mView:Landroid/view/View;

    goto :goto_0

    .line 235
    :cond_5
    const/4 v0, 0x4

    goto :goto_1
.end method

.method public final a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;I)V
    .locals 0

    .prologue
    .line 297
    invoke-direct {p0, p1, p2}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;I)V

    .line 298
    return-void
.end method

.method public final a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lfoz;)V
    .locals 4

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->aAD()Lfml;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cuA:Lfkd;

    invoke-interface {v1}, Lfkd;->getEntry()Lizj;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v2

    iget-object v3, p2, Lfoz;->ckR:Liwk;

    invoke-virtual {v0, v1, v2, v3}, Lfml;->b(Lizj;Ljde;Liwk;)V

    .line 304
    iget-object v0, p2, Lfoz;->cyj:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p2, Lfoz;->cyj:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;I)V

    .line 311
    :goto_0
    return-void

    .line 309
    :cond_0
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->aCH()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljdf;Lizj;)V
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->aAD()Lfml;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Lfml;->b(Ljde;Ljdf;Lizj;)V

    .line 283
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyP:Lfqf;

    invoke-virtual {v0, p1, p2}, Lfqf;->d(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljdf;)V

    .line 285
    invoke-virtual {p1, p2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->b(Ljdf;)Ljava/lang/Integer;

    move-result-object v0

    .line 286
    if-eqz v0, :cond_0

    .line 289
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;I)V

    .line 293
    :goto_0
    return-void

    .line 291
    :cond_0
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->aCH()V

    goto :goto_0
.end method

.method public final a(Lfmt;Lfkd;Lfqh;Lfpp;)V
    .locals 3

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->mCardContainer:Lfmt;

    .line 133
    iput-object p2, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cuA:Lfkd;

    .line 134
    iput-object p4, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyO:Lfpp;

    .line 135
    new-instance v0, Lfqf;

    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cuA:Lfkd;

    invoke-interface {v1}, Lfkd;->getEntry()Lizj;

    move-result-object v1

    invoke-static {}, Leot;->afY()Z

    move-result v2

    invoke-direct {v0, p3, v1, p0, v2}, Lfqf;-><init>(Lfqh;Lizj;Lfqc;Z)V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyP:Lfqf;

    .line 137
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyP:Lfqf;

    new-instance v1, Lfpq;

    invoke-direct {v1, p0}, Lfpq;-><init>(Lcom/google/android/sidekick/shared/training/CardSettingsView;)V

    invoke-virtual {v0, v1}, Lfqf;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyI:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyP:Lfqf;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Lfv;)V

    .line 139
    return-void
.end method

.method public final aCG()Lesk;
    .locals 7
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 170
    iget v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyQ:I

    if-ne v0, v6, :cond_0

    .line 178
    :goto_0
    return-object v5

    .line 172
    :cond_0
    iget v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyQ:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 175
    :cond_1
    :goto_1
    new-instance v0, Lfpz;

    iget v1, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyQ:I

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->mCardContainer:Lfmt;

    iget-object v4, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cuA:Lfkd;

    invoke-direct/range {v0 .. v5}, Lfpz;-><init>(ILandroid/content/Context;Lfmt;Lfkd;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 177
    iput v6, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyQ:I

    move-object v5, v0

    .line 178
    goto :goto_0

    .line 172
    :cond_2
    invoke-static {}, Landroid/view/accessibility/AccessibilityEvent;->obtain()Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v5

    const/16 v0, 0x4000

    invoke-virtual {v5, v0}, Landroid/view/accessibility/AccessibilityEvent;->setEventType(I)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {v5, v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setEventTime(J)V

    invoke-virtual {v5, p0}, Landroid/view/accessibility/AccessibilityEvent;->setSource(Landroid/view/View;)V

    invoke-virtual {v5}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a048f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final aCI()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 259
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyI:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->aI()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->jd(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->je(I)V

    .line 260
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cos:Lcom/google/android/sidekick/shared/ui/PageIndicatorView;

    iget-object v3, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyP:Lfqf;

    invoke-virtual {v3}, Lfqf;->getCount()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->jq(I)V

    .line 261
    iget-object v3, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cos:Lcom/google/android/sidekick/shared/ui/PageIndicatorView;

    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyP:Lfqf;

    invoke-virtual {v0}, Lfqf;->getCount()I

    move-result v0

    if-le v0, v2, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->setVisibility(I)V

    .line 263
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cos:Lcom/google/android/sidekick/shared/ui/PageIndicatorView;

    iget-object v3, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyP:Lfqf;

    invoke-virtual {v3}, Lfqf;->getCount()I

    move-result v3

    const/4 v4, 0x6

    if-le v3, v4, :cond_0

    move v1, v2

    :cond_0
    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->fy(Z)V

    .line 264
    return-void

    .line 261
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final ab(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyP:Lfqf;

    invoke-virtual {v0, p1}, Lfqf;->ab(Ljava/util/List;)V

    .line 144
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyP:Lfqf;

    invoke-virtual {v0}, Lfqf;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 145
    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyI:Landroid/support/v4/view/ViewPager;

    invoke-static {}, Leot;->afY()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyP:Lfqf;

    invoke-virtual {v0}, Lfqf;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->r(I)V

    .line 148
    :cond_0
    return-void

    .line 145
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(FII)I
    .locals 7

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v3, 0x3f000000    # 0.5f

    const/4 v6, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 377
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyT:Z

    .line 378
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyU:Z

    if-nez v0, :cond_0

    cmpg-float v0, p1, v5

    if-gtz v0, :cond_0

    .line 379
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyU:Z

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cor:Landroid/widget/ImageButton;

    sub-float v1, v2, p1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setScaleX(F)V

    .line 384
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cor:Landroid/widget/ImageButton;

    sub-float v1, v2, p1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setScaleY(F)V

    .line 388
    sub-float v0, p1, v3

    mul-float/2addr v0, v4

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sub-float v0, v2, v0

    iput v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyV:F

    .line 391
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyW:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 393
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyW:Landroid/view/View;

    iget v1, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyV:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 397
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyX:Lcom/google/android/sidekick/shared/training/ClippableTextView;

    if-eqz v0, :cond_1

    .line 398
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyU:Z

    if-eqz v0, :cond_3

    .line 399
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyX:Lcom/google/android/sidekick/shared/training/ClippableTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/training/ClippableTextView;->g(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyX:Lcom/google/android/sidekick/shared/training/ClippableTextView;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyS:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/training/ClippableTextView;->setTextColor(I)V

    .line 405
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyW:Landroid/view/View;

    instance-of v0, v0, Lejl;

    if-eqz v0, :cond_2

    .line 406
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyW:Landroid/view/View;

    check-cast v0, Lejl;

    invoke-interface {v0, p1, p2, p3}, Lejl;->b(FII)I

    move-result p2

    .line 411
    :cond_2
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyJ:Landroid/view/View;

    iget v1, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyV:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 412
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyJ:Landroid/view/View;

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 414
    mul-int v0, p2, p3

    return v0

    .line 401
    :cond_3
    sub-float v0, v3, p1

    mul-float/2addr v0, v4

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyX:Lcom/google/android/sidekick/shared/training/ClippableTextView;

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/training/ClippableTextView;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-int v1, v1

    invoke-static {}, Leot;->afY()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyX:Lcom/google/android/sidekick/shared/training/ClippableTextView;

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/training/ClippableTextView;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->iA:Landroid/graphics/Rect;

    sub-int v1, v2, v1

    iget-object v4, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyX:Lcom/google/android/sidekick/shared/training/ClippableTextView;

    invoke-virtual {v4}, Lcom/google/android/sidekick/shared/training/ClippableTextView;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v3, v1, v6, v2, v4}, Landroid/graphics/Rect;->set(IIII)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyX:Lcom/google/android/sidekick/shared/training/ClippableTextView;

    iget-object v2, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->iA:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Lcom/google/android/sidekick/shared/training/ClippableTextView;->g(Landroid/graphics/Rect;)V

    const v1, 0x3f666666    # 0.9f

    sub-float/2addr v0, v1

    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v0, v1

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyS:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyX:Lcom/google/android/sidekick/shared/training/ClippableTextView;

    sget-object v4, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyR:Landroid/animation/ArgbEvaluator;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, v0, v1, v2}, Landroid/animation/ArgbEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/google/android/sidekick/shared/training/ClippableTextView;->setTextColor(I)V

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->iA:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyX:Lcom/google/android/sidekick/shared/training/ClippableTextView;

    invoke-virtual {v3}, Lcom/google/android/sidekick/shared/training/ClippableTextView;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v2, v6, v6, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cor:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1

    .line 204
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyO:Lfpp;

    invoke-interface {v0}, Lfpp;->aAI()V

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyK:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_3

    .line 206
    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyI:Landroid/support/v4/view/ViewPager;

    invoke-static {p0}, Leot;->ay(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyI:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->aI()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->r(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyI:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->aI()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 209
    :cond_3
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyL:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 210
    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyI:Landroid/support/v4/view/ViewPager;

    invoke-static {p0}, Leot;->ay(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyI:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->aI()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_2
    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->r(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyI:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->aI()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public final onClose()V
    .locals 3

    .prologue
    .line 157
    iget v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyQ:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyQ:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->aAD()Lfml;

    move-result-object v0

    invoke-virtual {v0}, Lfml;->aBa()Lfor;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0}, Lfor;->azH()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 159
    :catch_0
    move-exception v0

    const-string v1, "NowRemoteClient"

    const-string v2, "Error making sendPendingTrainingAnswers request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 6

    .prologue
    const v1, 0x7f0200ef

    const v2, 0x7f0200ee

    const v5, 0x7f0b0152

    .line 100
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 101
    const v0, 0x7f110100

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyI:Landroid/support/v4/view/ViewPager;

    .line 102
    const v0, 0x7f110102

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyJ:Landroid/view/View;

    .line 103
    const v0, 0x7f110104

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cos:Lcom/google/android/sidekick/shared/ui/PageIndicatorView;

    .line 104
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cos:Lcom/google/android/sidekick/shared/ui/PageIndicatorView;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->jp(I)V

    .line 105
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyI:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->a(Lhj;)V

    .line 106
    const v0, 0x7f110101

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cor:Landroid/widget/ImageButton;

    .line 107
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cor:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cor:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020106

    invoke-static {v3, v4, v5}, Lgai;->b(Landroid/content/res/Resources;II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 111
    const v0, 0x7f110103

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyK:Landroid/widget/ImageButton;

    .line 112
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyK:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    const v0, 0x7f110105

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyL:Landroid/widget/ImageButton;

    .line 114
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyL:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {}, Leot;->afY()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v3, v0, v5}, Lgai;->b(Landroid/content/res/Resources;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyM:Landroid/graphics/drawable/Drawable;

    .line 121
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyK:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyM:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 122
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {}, Leot;->afY()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-static {v0, v2, v5}, Lgai;->b(Landroid/content/res/Resources;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyN:Landroid/graphics/drawable/Drawable;

    .line 125
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyL:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/CardSettingsView;->cyN:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 126
    return-void

    :cond_0
    move v0, v2

    .line 118
    goto :goto_0

    :cond_1
    move v2, v1

    .line 122
    goto :goto_1
.end method

.method public final w(I)V
    .locals 0

    .prologue
    .line 247
    return-void
.end method

.method public final x(I)V
    .locals 0

    .prologue
    .line 219
    return-void
.end method
