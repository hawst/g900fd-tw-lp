.class public Lcom/google/android/sidekick/main/feedback/SurveyActivity;
.super Landroid/app/Activity;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLayoutChangeListener;
.implements Lhj;


# instance fields
.field private coe:Lizj;

.field public final cof:Ljava/util/List;

.field public cog:Lepl;

.field public coh:Lesk;

.field public coi:Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

.field private final coj:Ldz;

.field private cok:Ljava/util/ArrayList;

.field private col:Landroid/widget/TextView;

.field private com:Landroid/support/v4/view/ViewPager;

.field private con:Landroid/view/ViewGroup;

.field private coo:Landroid/widget/Button;

.field private cop:Z

.field private coq:Landroid/widget/Button;

.field private cor:Landroid/widget/ImageButton;

.field private cos:Lcom/google/android/sidekick/shared/ui/PageIndicatorView;

.field private cot:Lfmq;

.field private cou:Lfjs;

.field public cov:Lfld;

.field private mCardContainer:Lfmt;

.field private mClock:Lemp;

.field private mNonUiExecutor:Ljava/util/concurrent/ScheduledExecutorService;

.field public mRemoteClient:Lfml;

.field private mRunner:Lerp;

.field private mStringEvaluator:Lgbr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 84
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cof:Ljava/util/List;

    .line 89
    new-instance v0, Ldz;

    invoke-direct {v0}, Ldz;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coj:Ldz;

    .line 463
    return-void
.end method


# virtual methods
.method public final a(IFI)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 403
    if-nez p1, :cond_1

    const/4 v0, 0x1

    .line 404
    :goto_0
    iget-object v2, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->col:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    const/4 v1, 0x4

    :cond_0
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 405
    return-void

    :cond_1
    move v0, v1

    .line 403
    goto :goto_0
.end method

.method public final axp()V
    .locals 3

    .prologue
    .line 443
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->com:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 444
    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->com:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 445
    const v2, 0x7f110010

    invoke-virtual {v1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 446
    invoke-virtual {p0, v1}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->bh(Landroid/view/View;)V

    .line 443
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 449
    :cond_1
    return-void
.end method

.method public final bh(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 455
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d02a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 456
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->col:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 461
    return-void
.end method

.method public final ed()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 203
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coi:Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

    if-nez v0, :cond_0

    .line 205
    new-instance v0, Lfbv;

    const-string v1, "Get survey entries"

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x2

    aput v3, v2, v4

    invoke-direct {v0, p0, v1, v2}, Lfbv;-><init>(Lcom/google/android/sidekick/main/feedback/SurveyActivity;Ljava/lang/String;[I)V

    invoke-static {v0}, Lepl;->a(Lerh;)Lepl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cog:Lepl;

    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mNonUiExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    new-instance v1, Lfbw;

    const-string v2, "Survey card"

    invoke-direct {v1, p0, v2, v0}, Lfbw;-><init>(Lcom/google/android/sidekick/main/feedback/SurveyActivity;Ljava/lang/String;Lepl;)V

    iget-object v2, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mRunner:Lerp;

    invoke-interface {v2}, Lerp;->Ct()Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lepl;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 210
    :goto_0
    return-void

    .line 208
    :cond_0
    invoke-virtual {p0, v4}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->fj(Z)V

    goto :goto_0
.end method

.method public final fj(Z)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 354
    new-instance v0, Lflp;

    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coi:Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mRemoteClient:Lfml;

    invoke-direct {v0, v1, v2, v8}, Lflp;-><init>(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lfml;Z)V

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mCardContainer:Lfmt;

    .line 356
    new-instance v0, Lfld;

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->com:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mCardContainer:Lfmt;

    iget-object v5, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mStringEvaluator:Lgbr;

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lfld;-><init>(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lfmt;Lgbr;Lfkz;)V

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cov:Lfld;

    .line 360
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coi:Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->aBR()Lizn;

    move-result-object v0

    iget-object v0, v0, Lizn;->dUI:[Lizo;

    aget-object v0, v0, v7

    .line 361
    new-instance v1, Lfll;

    iget-object v2, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cou:Lfjs;

    invoke-direct {v1, v2}, Lfll;-><init>(Lfjs;)V

    .line 362
    invoke-virtual {v1, v0}, Lfll;->k(Lizo;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkd;

    .line 363
    iget-object v2, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cof:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 368
    :cond_0
    if-eqz p1, :cond_2

    .line 369
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cof:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cok:Ljava/util/ArrayList;

    move v0, v7

    .line 370
    :goto_1
    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cof:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 371
    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cok:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 370
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 373
    :cond_1
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cok:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 376
    :cond_2
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->com:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lfbx;

    invoke-direct {v1, p0, v7}, Lfbx;-><init>(Lcom/google/android/sidekick/main/feedback/SurveyActivity;B)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Lfv;)V

    .line 377
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cos:Lcom/google/android/sidekick/shared/ui/PageIndicatorView;

    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->com:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->aG()Lfv;

    move-result-object v1

    invoke-virtual {v1}, Lfv;->getCount()I

    move-result v1

    const/4 v2, 0x6

    if-le v1, v2, :cond_3

    move v7, v8

    :cond_3
    invoke-virtual {v0, v7}, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->fy(Z)V

    .line 379
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cos:Lcom/google/android/sidekick/shared/ui/PageIndicatorView;

    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->com:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->aG()Lfv;

    move-result-object v1

    invoke-virtual {v1}, Lfv;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->jq(I)V

    .line 382
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->com:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->aI()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->w(I)V

    .line 383
    return-void
.end method

.method public final iC(I)Lfkd;
    .locals 3

    .prologue
    .line 438
    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cof:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cok:Ljava/util/ArrayList;

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkd;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coq:Landroid/widget/Button;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cor:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1

    .line 322
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->finish()V

    .line 345
    :goto_0
    return-void

    .line 324
    :cond_1
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coo:Landroid/widget/Button;

    if-eq p1, v0, :cond_2

    .line 326
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->com:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->aI()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->iC(I)Lfkd;

    move-result-object v0

    invoke-interface {v0}, Lfkd;->getEntry()Lizj;

    move-result-object v1

    .line 327
    const/16 v0, 0xd2

    const/4 v2, 0x0

    new-array v2, v2, [I

    invoke-static {v1, v0, v2}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v0

    invoke-static {v0}, Leqh;->f(Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Liwk;

    .line 329
    iget-object v2, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->con:Landroid/view/ViewGroup;

    invoke-virtual {v2, p1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    .line 330
    invoke-virtual {v0, v2}, Liwk;->nc(I)Liwk;

    .line 331
    new-instance v2, Lgam;

    iget-object v3, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v2, v1, v0, v4, v5}, Lgam;-><init>(Lizj;Liwk;J)V

    invoke-virtual {v2}, Lgam;->aDZ()Lizv;

    move-result-object v0

    .line 333
    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coj:Ldz;

    iget-object v2, v0, Lizv;->entry:Lizj;

    invoke-static {v2}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ldz;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    :cond_2
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->com:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->aI()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->com:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->aG()Lfv;

    move-result-object v1

    invoke-virtual {v1}, Lfv;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 339
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->com:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->com:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->aI()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->r(I)V

    goto :goto_0

    .line 342
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 118
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 119
    iget-object v1, v0, Lgql;->mAsyncServices:Lema;

    .line 120
    iput-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mRunner:Lerp;

    .line 121
    invoke-virtual {v1}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mNonUiExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 122
    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v1

    .line 123
    invoke-virtual {v1}, Lfdb;->ayg()Lfml;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mRemoteClient:Lfml;

    .line 124
    invoke-virtual {v1}, Lfdb;->axP()Lfjs;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cou:Lfjs;

    .line 125
    new-instance v1, Lgbr;

    iget-object v2, v0, Lgql;->mClock:Lemp;

    invoke-direct {v1, v2}, Lgbr;-><init>(Lemp;)V

    iput-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mStringEvaluator:Lgbr;

    .line 126
    iget-object v0, v0, Lgql;->mClock:Lemp;

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mClock:Lemp;

    .line 128
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 129
    const-string v1, "survey_lure_entry"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    const-class v1, Lizj;

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coe:Lizj;

    .line 133
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->requestWindowFeature(I)Z

    .line 134
    const v0, 0x7f040186

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->setContentView(I)V

    .line 135
    const v0, 0x7f11041a

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->col:Landroid/widget/TextView;

    .line 136
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->col:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coe:Lizj;

    iget-object v1, v1, Lizj;->dUg:Ljhd;

    invoke-virtual {v1}, Ljhd;->blx()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->col:Landroid/widget/TextView;

    new-instance v1, Lfbt;

    invoke-direct {v1, p0}, Lfbt;-><init>(Lcom/google/android/sidekick/main/feedback/SurveyActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 144
    const v0, 0x7f11041b

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->com:Landroid/support/v4/view/ViewPager;

    .line 145
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->com:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->a(Lhj;)V

    .line 146
    const v0, 0x7f11041c

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->con:Landroid/view/ViewGroup;

    .line 147
    const v0, 0x7f11006c

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coo:Landroid/widget/Button;

    .line 148
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coo:Landroid/widget/Button;

    const v1, 0x7f0a04f8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 149
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coo:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    const v0, 0x7f11041d

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coq:Landroid/widget/Button;

    .line 151
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coq:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coq:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 153
    const v0, 0x7f110101

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cor:Landroid/widget/ImageButton;

    .line 154
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cor:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cor:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 156
    const v0, 0x7f110104

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cos:Lcom/google/android/sidekick/shared/ui/PageIndicatorView;

    .line 158
    if-eqz p1, :cond_1

    .line 161
    const-string v0, "cards-response"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coi:Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

    .line 162
    const-string v0, "recorded-answers"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    .line 163
    if-eqz v2, :cond_0

    .line 164
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 165
    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    const-class v4, Lizv;

    invoke-virtual {v0, v4}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizv;

    .line 167
    iget-object v4, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coj:Ldz;

    iget-object v5, v0, Lizv;->entry:Lizj;

    invoke-static {v5}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ldz;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 170
    :cond_0
    const-string v0, "entry-ordering"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cok:Ljava/util/ArrayList;

    .line 172
    :cond_1
    return-void
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2

    .prologue
    .line 391
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coq:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cor:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getWidth()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 392
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cos:Lcom/google/android/sidekick/shared/ui/PageIndicatorView;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 393
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 394
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 395
    return-void
.end method

.method protected onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 265
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 267
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coh:Lesk;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mRemoteClient:Lfml;

    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coh:Lesk;

    invoke-virtual {v0, v1}, Lfml;->e(Lesk;)V

    .line 269
    iput-object v3, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coh:Lesk;

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cog:Lepl;

    if-eqz v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cog:Lepl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lepl;->cancel(Z)Z

    .line 275
    iput-object v3, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cog:Lepl;

    .line 277
    :cond_1
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->aBc()V

    .line 278
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 280
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coj:Ldz;

    invoke-virtual {v0}, Ldz;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 281
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mRemoteClient:Lfml;

    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coj:Ldz;

    invoke-virtual {v1}, Ldz;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfml;->V(Ljava/util/List;)V

    .line 283
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mRemoteClient:Lfml;

    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coe:Lizj;

    invoke-virtual {v0, v1, v2}, Lfml;->a(Lizj;Z)V

    .line 284
    const v0, 0x7f0a04f9

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 287
    :cond_2
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cot:Lfmq;

    invoke-virtual {v0}, Lfmq;->release()V

    .line 288
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 176
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 178
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mRemoteClient:Lfml;

    const-string v1, "SurveyActivity"

    invoke-virtual {v0, v1}, Lfml;->lQ(Ljava/lang/String;)Lfmq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cot:Lfmq;

    .line 179
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cot:Lfmq;

    invoke-virtual {v0}, Lfmq;->aBf()Z

    .line 180
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->aBd()V

    .line 183
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mCardContainer:Lfmt;

    if-nez v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->ed()V

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    new-instance v0, Lfbu;

    const-string v1, "LoadSurvey"

    invoke-direct {v0, p0, v1}, Lfbu;-><init>(Lcom/google/android/sidekick/main/feedback/SurveyActivity;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coh:Lesk;

    .line 196
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->mRemoteClient:Lfml;

    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coh:Lesk;

    invoke-virtual {v0, v1}, Lfml;->d(Lesk;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 292
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 293
    const-string v0, "cards-response"

    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coi:Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 294
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coj:Ldz;

    invoke-virtual {v0}, Ldz;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 295
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coj:Ldz;

    invoke-virtual {v0}, Ldz;->size()I

    move-result v0

    new-array v2, v0, [Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 296
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coj:Ldz;

    invoke-virtual {v0}, Ldz;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coj:Ldz;

    invoke-virtual {v0, v1}, Ldz;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;

    invoke-static {v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v0

    aput-object v0, v2, v1

    .line 296
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 299
    :cond_0
    const-string v0, "recorded-answers"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 301
    :cond_1
    const-string v0, "entry-ordering"

    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cok:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 302
    return-void
.end method

.method public final w(I)V
    .locals 11

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 409
    if-nez p1, :cond_1

    move v6, v1

    .line 410
    :goto_0
    if-eqz v6, :cond_2

    .line 411
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->con:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 412
    iput-boolean v2, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cop:Z

    .line 413
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->con:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coo:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 431
    :cond_0
    iget-object v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coq:Landroid/widget/Button;

    if-eqz v6, :cond_7

    move v0, v2

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 432
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cor:Landroid/widget/ImageButton;

    if-eqz v6, :cond_8

    :goto_2
    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 433
    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cos:Lcom/google/android/sidekick/shared/ui/PageIndicatorView;

    invoke-virtual {v0, p1}, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->jp(I)V

    .line 434
    return-void

    :cond_1
    move v6, v2

    .line 409
    goto :goto_0

    .line 415
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cop:Z

    if-nez v0, :cond_5

    .line 416
    iget-boolean v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cop:Z

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->con:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coe:Lizj;

    iget-object v0, v0, Lizj;->dUg:Ljhd;

    iget-object v4, v0, Ljhd;->elx:[Ljava/lang/String;

    array-length v7, v4

    move v3, v2

    :goto_4
    if-ge v3, v7, :cond_4

    aget-object v8, v4, v3

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v9, 0x7f040182

    iget-object v10, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->con:Landroid/view/ViewGroup;

    invoke-virtual {v0, v9, v10, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v8, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->con:Landroid/view/ViewGroup;

    invoke-virtual {v8, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    iput-boolean v1, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->cop:Z

    .line 423
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->iC(I)Lfkd;

    move-result-object v0

    invoke-interface {v0}, Lfkd;->getEntry()Lizj;

    move-result-object v0

    invoke-static {v0}, Lgam;->H(Lizj;)Lizj;

    move-result-object v0

    .line 425
    iget-object v3, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->coj:Ldz;

    invoke-static {v0}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v0

    invoke-virtual {v3, v0}, Ldz;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizv;

    move v3, v2

    .line 426
    :goto_5
    iget-object v4, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->con:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 427
    iget-object v4, p0, Lcom/google/android/sidekick/main/feedback/SurveyActivity;->con:Landroid/view/ViewGroup;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    if-eqz v0, :cond_6

    iget-object v4, v0, Lizv;->dVr:Liwk;

    invoke-virtual {v4}, Liwk;->aZs()I

    move-result v4

    if-ne v3, v4, :cond_6

    move v4, v1

    :goto_6
    invoke-virtual {v7, v4}, Landroid/view/View;->setSelected(Z)V

    .line 426
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_6
    move v4, v2

    .line 427
    goto :goto_6

    :cond_7
    move v0, v5

    .line 431
    goto/16 :goto_1

    :cond_8
    move v5, v2

    .line 432
    goto/16 :goto_2
.end method

.method public final x(I)V
    .locals 0

    .prologue
    .line 399
    return-void
.end method
