.class public Lcom/google/android/sidekick/shared/ui/UndoDismissToast;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private bSF:I

.field private bWJ:Landroid/view/View;

.field private cAN:Landroid/view/View;

.field private cAO:Lfrj;

.field private cen:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 41
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 43
    invoke-static {}, Leot;->afY()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->cen:Z

    .line 45
    const v0, 0x7f110484

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->cAN:Landroid/view/View;

    .line 46
    const v0, 0x7f1101f8

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->bWJ:Landroid/view/View;

    .line 50
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->cAN:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 51
    const v1, 0x66cccccc

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 54
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->cAN:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 55
    new-instance v1, Lfrj;

    invoke-direct {v1, v0}, Lfrj;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object v1, p0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->cAO:Lfrj;

    .line 56
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->cAN:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->cAO:Lfrj;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d028c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->bSF:I

    .line 60
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 86
    iget-object v3, p0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->cAO:Lfrj;

    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->cen:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->cen:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->bWJ:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    :goto_1
    iget-object v4, p0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->cAN:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-virtual {v3, v0, v1, v2, v4}, Lfrj;->g(IIII)V

    .line 89
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->bWJ:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->cAN:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 68
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_1

    .line 69
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 70
    iget v1, p0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->bSF:I

    if-le v0, v1, :cond_0

    .line 71
    iget v0, p0, Lcom/google/android/sidekick/shared/ui/UndoDismissToast;->bSF:I

    .line 73
    :cond_0
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 76
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 77
    return-void
.end method
