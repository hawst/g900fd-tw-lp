.class public Lcom/google/android/sidekick/shared/ui/TextViewWithImages;
.super Landroid/widget/TextView;
.source "PG"


# instance fields
.field private final cAG:Ljava/util/Map;

.field private final dK:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/sidekick/shared/ui/TextViewWithImages;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/sidekick/shared/ui/TextViewWithImages;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/TextViewWithImages;->dK:Ljava/lang/Object;

    .line 30
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/TextViewWithImages;->cAG:Ljava/util/Map;

    .line 34
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/style/ImageSpan;Landroid/graphics/drawable/Drawable;Landroid/text/Spannable;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 89
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 90
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 91
    invoke-virtual {p2, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 92
    invoke-virtual {p1}, Landroid/text/style/ImageSpan;->getSource()Ljava/lang/String;

    move-result-object v0

    .line 94
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/TextViewWithImages;->cAG:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    iget-object v1, p0, Lcom/google/android/sidekick/shared/ui/TextViewWithImages;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 97
    :try_start_0
    invoke-interface {p3, p1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v2

    .line 98
    invoke-interface {p3, p1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v3

    .line 99
    invoke-interface {p3, p1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 101
    new-instance v4, Landroid/text/style/ImageSpan;

    invoke-direct {v4, p2, v0}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    .line 102
    const/16 v0, 0x21

    invoke-interface {p3, v4, v2, v3, v0}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 103
    invoke-virtual {p0, p3}, Lcom/google/android/sidekick/shared/ui/TextViewWithImages;->setText(Ljava/lang/CharSequence;)V

    .line 104
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/CharSequence;Lesm;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-super {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    instance-of v0, p1, Landroid/text/SpannableStringBuilder;

    if-eqz v0, :cond_1

    .line 67
    check-cast p1, Landroid/text/SpannableStringBuilder;

    .line 72
    :goto_0
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const-class v2, Landroid/text/style/ImageSpan;

    invoke-virtual {p1, v1, v0, v2}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/ImageSpan;

    array-length v3, v0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_3

    aget-object v4, v0, v2

    .line 73
    invoke-virtual {v4}, Landroid/text/style/ImageSpan;->getSource()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 74
    if-eqz v1, :cond_0

    .line 75
    invoke-interface {p2, v1}, Lesm;->D(Landroid/net/Uri;)Leml;

    move-result-object v1

    .line 76
    invoke-interface {v1}, Leml;->auB()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 77
    invoke-interface {v1}, Leml;->auC()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v4, v1, p1}, Lcom/google/android/sidekick/shared/ui/TextViewWithImages;->a(Landroid/text/style/ImageSpan;Landroid/graphics/drawable/Drawable;Landroid/text/Spannable;)V

    .line 72
    :cond_0
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 69
    :cond_1
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object p1, v0

    goto :goto_0

    .line 79
    :cond_2
    new-instance v5, Lfri;

    invoke-direct {v5, v4, p1, p0}, Lfri;-><init>(Landroid/text/style/ImageSpan;Landroid/text/Spannable;Lcom/google/android/sidekick/shared/ui/TextViewWithImages;)V

    .line 80
    invoke-interface {v1, v5}, Leml;->e(Lemy;)V

    .line 81
    iget-object v6, p0, Lcom/google/android/sidekick/shared/ui/TextViewWithImages;->cAG:Ljava/util/Map;

    invoke-virtual {v4}, Landroid/text/style/ImageSpan;->getSource()Ljava/lang/String;

    move-result-object v4

    new-instance v7, Lfrh;

    invoke-direct {v7, v1, v5}, Lfrh;-><init>(Leml;Lemy;)V

    invoke-interface {v6, v4, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 86
    :cond_3
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/TextViewWithImages;->cAG:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrh;

    .line 48
    iget-object v2, v0, Lfrh;->cAH:Leml;

    iget-object v0, v0, Lfrh;->cAI:Lemy;

    invoke-interface {v2, v0}, Leml;->f(Lemy;)V

    goto :goto_0

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/TextViewWithImages;->cAG:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 51
    invoke-super {p0}, Landroid/widget/TextView;->onDetachedFromWindow()V

    .line 52
    return-void
.end method
