.class public Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lifg;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final cyf:Ljava/util/regex/Pattern;


# instance fields
.field private final csI:Ljava/util/Map;

.field private final cyg:Ljdk;

.field private cyh:Ljde;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 139
    const-string v0, "(%+)(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyf:Ljava/util/regex/Pattern;

    .line 562
    new-instance v0, Lfoy;

    invoke-direct {v0}, Lfoy;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;Ljdk;Ljde;)V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    iput-object p1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->csI:Ljava/util/Map;

    .line 151
    iput-object p2, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    .line 152
    iput-object p3, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyh:Ljde;

    .line 153
    return-void
.end method

.method private a(Ljdl;)Lfpa;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 347
    .line 348
    invoke-virtual {p1}, Ljdl;->bhB()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 349
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->csI:Ljava/util/Map;

    invoke-virtual {p1}, Ljdl;->bhA()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 350
    if-nez v0, :cond_0

    .line 351
    const-string v3, "Sidekick_TrainingQuestion"

    const-string v4, "Dictionary missing string for key %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljdl;->bhA()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    :cond_0
    :goto_0
    new-instance v4, Lfpa;

    invoke-virtual {p1}, Ljdl;->bhC()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Ljdl;->getIcon()I

    move-result v2

    :cond_1
    invoke-virtual {p1}, Ljdl;->bhE()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Ljdl;->bhD()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :goto_1
    invoke-virtual {p1}, Ljdl;->hasValue()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p1}, Ljdl;->getValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :cond_2
    invoke-direct {v4, v0, v2, v3, v1}, Lfpa;-><init>(Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;)V

    return-object v4

    :cond_3
    move-object v3, v1

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;[Ljdg;)Ljava/lang/String;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 461
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->csI:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 462
    if-nez v0, :cond_0

    .line 463
    const-string v0, "Sidekick_TrainingQuestion"

    const-string v1, "Dictionary missing string for key %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    const/4 v0, 0x0

    .line 467
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, p2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->b(Ljava/lang/String;[Ljdg;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected static b(Ljava/lang/String;[Ljdg;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 477
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyf:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 479
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 481
    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 482
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->start()I

    move-result v4

    invoke-virtual {p0, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 486
    invoke-virtual {v2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 487
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    rem-int/lit8 v4, v4, 0x2

    if-ne v4, v7, :cond_1

    .line 488
    invoke-virtual {v2, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    .line 489
    if-ltz v4, :cond_0

    array-length v5, p1

    if-ge v4, v5, :cond_0

    .line 492
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 493
    aget-object v0, p1, v4

    invoke-virtual {v0}, Ljdg;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 505
    :goto_1
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    goto :goto_0

    .line 495
    :cond_0
    const-string v0, "Sidekick_TrainingQuestion"

    const-string v5, "Missing value for index %d of string: %s"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v1

    aput-object p0, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 502
    :cond_1
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 509
    :cond_2
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 512
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static unescape(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 517
    const-string v0, "%%"

    const-string v1, "%"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljdg;)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 160
    new-instance v2, Ljde;

    invoke-direct {v2}, Ljde;-><init>()V

    .line 162
    :try_start_0
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyh:Ljde;

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-static {v2, v1}, Ljsr;->c(Ljsr;[B)Ljsr;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    iget-object v3, v2, Ljde;->ebv:[Ljdg;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 169
    invoke-virtual {v5}, Ljdg;->getSource()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    .line 170
    iget-object v0, v2, Ljde;->ebv:[Ljdg;

    aput-object p1, v0, v1

    .line 175
    :cond_0
    new-instance v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->csI:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;-><init>(Ljava/util/Map;Ljdk;Ljde;)V

    return-object v0

    .line 163
    :catch_0
    move-exception v0

    .line 165
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 173
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 168
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a(Ljdf;)V
    .locals 2

    .prologue
    .line 300
    :try_start_0
    new-instance v0, Ljde;

    invoke-direct {v0}, Ljde;-><init>()V

    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyh:Ljde;

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-static {v0, v1}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Ljde;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyh:Ljde;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    .line 305
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyh:Ljde;

    iput-object p1, v0, Ljde;->ebz:Ljdf;

    .line 306
    return-void

    .line 301
    :catch_0
    move-exception v0

    .line 303
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final aCi()Z
    .locals 1

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v0

    .line 201
    packed-switch v0, :pswitch_data_0

    .line 208
    :pswitch_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 206
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 201
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final aCj()Ljde;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyh:Ljde;

    return-object v0
.end method

.method public final aCk()Ljdf;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyh:Ljde;

    iget-object v0, v0, Ljde;->ebz:Ljdf;

    return-object v0
.end method

.method public final aCl()Ljava/util/List;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v0}, Ljdk;->getType()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Not a multiple select question"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 243
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyh:Ljde;

    iget-object v0, v0, Ljde;->ebz:Ljdf;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCq()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_2

    .line 244
    :cond_0
    const/4 v0, 0x0

    .line 261
    :goto_1
    return-object v0

    .line 240
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 247
    :cond_2
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyh:Ljde;

    iget-object v0, v0, Ljde;->ebz:Ljdf;

    .line 248
    iget-object v1, v0, Ljdf;->ebG:[I

    array-length v1, v1

    if-nez v1, :cond_3

    .line 249
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    goto :goto_1

    .line 252
    :cond_3
    iget-object v0, v0, Ljdf;->ebG:[I

    invoke-static {v0}, Lius;->w([I)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Liqs;->z(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v2

    .line 254
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 255
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCq()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpa;

    .line 256
    iget-object v4, v0, Lfpa;->cyl:Ljava/lang/Integer;

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 257
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    move-object v0, v1

    .line 261
    goto :goto_1
.end method

.method public final aCm()Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCl()Ljava/util/List;

    move-result-object v0

    .line 272
    if-nez v0, :cond_0

    .line 273
    const/4 v0, 0x0

    .line 282
    :goto_0
    return-object v0

    .line 276
    :cond_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 277
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpa;

    .line 278
    iget-object v0, v0, Lfpa;->cyk:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 282
    :cond_1
    const-string v0, ", "

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final aCn()Ljdg;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyh:Ljde;

    iget-object v0, v0, Ljde;->ebv:[Ljdg;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyh:Ljde;

    iget-object v0, v0, Ljde;->ebv:[Ljdg;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aCo()[I
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    iget-object v0, v0, Ljdk;->ecf:[I

    return-object v0
.end method

.method public final aCp()Ljava/util/List;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    iget-object v0, v0, Ljdk;->ecm:[Ljdl;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 311
    const/4 v0, 0x0

    .line 314
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    iget-object v0, v0, Ljdk;->ecm:[Ljdl;

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, p0}, Lilw;->a(Ljava/util/List;Lifg;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final aCq()Ljava/util/List;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    iget-object v0, v0, Ljdk;->ecp:[Ljdl;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 326
    const/4 v0, 0x0

    .line 329
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    iget-object v0, v0, Ljdk;->ecp:[Ljdl;

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, p0}, Lilw;->a(Ljava/util/List;Lifg;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final aCr()Ljava/util/List;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    iget-object v0, v0, Ljdk;->eco:[Ljdl;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 335
    const/4 v0, 0x0

    .line 338
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    iget-object v0, v0, Ljdk;->eco:[Ljdl;

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, p0}, Lilw;->a(Ljava/util/List;Lifg;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final aCs()Ljava/lang/Iterable;
    .locals 7

    .prologue
    .line 363
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 364
    const/4 v0, 0x0

    .line 365
    :goto_0
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    iget-object v1, v1, Ljdk;->ecn:[Ljdl;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyh:Ljde;

    iget-object v1, v1, Ljde;->dUY:[Liwk;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 366
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    iget-object v1, v1, Ljdk;->ecn:[Ljdl;

    aget-object v1, v1, v0

    .line 367
    iget-object v3, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyh:Ljde;

    iget-object v3, v3, Ljde;->dUY:[Liwk;

    aget-object v3, v3, v0

    .line 368
    new-instance v4, Lfoz;

    invoke-virtual {v1}, Ljdl;->getIcon()I

    move-result v5

    invoke-virtual {v1}, Ljdl;->bhE()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v1}, Ljdl;->bhD()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_1
    invoke-direct {v4, v3, v5, v1}, Lfoz;-><init>(Liwk;ILjava/lang/Integer;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 365
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 368
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 371
    :cond_1
    return-object v2
.end method

.method public final aCt()Ljava/lang/String;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v0}, Ljdk;->bhs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 380
    const-string v0, "Sidekick_TrainingQuestion"

    const-string v1, "Question template %d missing question key"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v4}, Ljdk;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    const/4 v0, 0x0

    .line 385
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v0}, Ljdk;->bhr()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyh:Ljde;

    iget-object v1, v1, Ljde;->ebv:[Ljdg;

    invoke-direct {p0, v0, v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->a(Ljava/lang/String;[Ljdg;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final aCu()Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v0}, Ljdk;->bhu()Z

    move-result v0

    if-nez v0, :cond_0

    .line 397
    const/4 v0, 0x0

    .line 400
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v0}, Ljdk;->bht()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyh:Ljde;

    iget-object v1, v1, Ljde;->ebx:[Ljdg;

    invoke-direct {p0, v0, v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->a(Ljava/lang/String;[Ljdg;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final aCv()Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 410
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v0}, Ljdk;->bhw()Z

    move-result v0

    if-nez v0, :cond_0

    .line 411
    const/4 v0, 0x0

    .line 414
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->csI:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v1}, Ljdk;->bhv()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final aCw()Ljava/lang/Integer;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 453
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v0}, Ljdk;->bhy()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v0}, Ljdk;->bhx()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic as(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    check-cast p1, Ljdl;

    invoke-direct {p0, p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->a(Ljdl;)Lfpa;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljdf;)Ljava/lang/Integer;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v0}, Ljdk;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 449
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 426
    :pswitch_0
    invoke-virtual {p1}, Ljdf;->bhi()Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 427
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    iget-object v0, v0, Ljdk;->eck:Ljdm;

    if-eqz v0, :cond_0

    .line 428
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    iget-object v0, v0, Ljdk;->eck:Ljdm;

    .line 429
    invoke-virtual {p1}, Ljdf;->bhh()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 430
    invoke-virtual {v0}, Ljdm;->bhG()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 431
    invoke-virtual {v0}, Ljdm;->bhF()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 434
    :cond_1
    invoke-virtual {v0}, Ljdm;->bhI()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 435
    invoke-virtual {v0}, Ljdm;->bhH()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 441
    :pswitch_1
    invoke-virtual {p1}, Ljdf;->bhk()Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 442
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    iget-object v0, v0, Ljdk;->ecm:[Ljdl;

    invoke-virtual {p1}, Ljdf;->bhj()I

    move-result v1

    aget-object v0, v0, v1

    .line 444
    invoke-virtual {v0}, Ljdl;->bhE()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 445
    invoke-virtual {v0}, Ljdl;->bhD()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 424
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 559
    const/4 v0, 0x0

    return v0
.end method

.method public final getType()I
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 179
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v0}, Ljdk;->getType()I

    move-result v0

    .line 181
    if-ne v0, v1, :cond_0

    .line 182
    iget-object v3, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyh:Ljde;

    iget-object v4, v3, Ljde;->ebv:[Ljdg;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v6, v4, v3

    invoke-virtual {v6}, Ljdg;->getSource()I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_1

    :goto_1
    if-eqz v1, :cond_0

    .line 183
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v1}, Ljdk;->bhx()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 192
    :cond_0
    :goto_2
    return v0

    .line 182
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    .line 185
    :pswitch_0
    const/4 v0, -0x1

    goto :goto_2

    .line 187
    :pswitch_1
    const/4 v0, -0x2

    goto :goto_2

    .line 183
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final jb(I)Lfpa;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    iget-object v0, v0, Ljdk;->ecm:[Ljdl;

    aget-object v0, v0, p1

    invoke-direct {p0, v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->a(Ljdl;)Lfpa;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 588
    iget-object v2, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->csI:Ljava/util/Map;

    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v1}, Ljdk;->bhs()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v1}, Ljdk;->bhr()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v1}, Ljdk;->bhu()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v1}, Ljdk;->bht()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v1}, Ljdk;->bhw()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-virtual {v1}, Ljdk;->bhv()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    iget-object v4, v1, Ljdk;->ecm:[Ljdl;

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_3

    aget-object v6, v4, v1

    invoke-virtual {v6}, Ljdl;->bhA()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    iget-object v4, v1, Ljdk;->eco:[Ljdl;

    array-length v5, v4

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_4

    aget-object v6, v4, v1

    invoke-virtual {v6}, Ljdl;->bhA()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    iget-object v1, v1, Ljdk;->ecp:[Ljdl;

    array-length v4, v1

    :goto_2
    if-ge v0, v4, :cond_5

    aget-object v5, v1, v0

    invoke-virtual {v5}, Ljdl;->bhA()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 590
    :cond_5
    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 592
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 593
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 594
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_3

    .line 596
    :cond_6
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyg:Ljdk;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 597
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->cyh:Ljde;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 598
    return-void
.end method
