.class public Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfcr;
.implements Lgpe;


# instance fields
.field private final coO:Lamu;

.field private final coP:Ljava/util/List;

.field private final coQ:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final coR:Ljava/lang/Object;

.field private final mActivityLifecycleNotifier:Lgpd;

.field private final mClock:Lemp;

.field private final mContext:Landroid/content/Context;

.field private final mFileReader:Lfcj;

.field private final mFileWriter:Lfck;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfcj;Lfck;Lemp;Lgpd;)V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Lamu;

    invoke-direct {v0}, Lamu;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coP:Ljava/util/List;

    .line 65
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coQ:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 70
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coR:Ljava/lang/Object;

    .line 75
    iput-object p1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mContext:Landroid/content/Context;

    .line 76
    iput-object p2, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mFileReader:Lfcj;

    .line 77
    iput-object p3, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mFileWriter:Lfck;

    .line 78
    iput-object p4, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mClock:Lemp;

    .line 79
    iput-object p5, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mActivityLifecycleNotifier:Lgpd;

    .line 80
    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mActivityLifecycleNotifier:Lgpd;

    invoke-interface {v0, p0}, Lgpd;->a(Lgpe;)V

    .line 81
    return-void
.end method

.method private a(Lgam;)V
    .locals 4

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coQ:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lgam;->cEr:Ljava/lang/Boolean;

    .line 147
    :cond_0
    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    monitor-enter v1

    .line 148
    :try_start_0
    iget-object v2, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    iget-object v0, v0, Lamu;->afC:[Lizv;

    invoke-virtual {p1}, Lgam;->aDZ()Lizv;

    move-result-object v3

    invoke-static {v0, v3}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizv;

    iput-object v0, v2, Lamu;->afC:[Lizv;

    .line 150
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Lgbg;Liwk;Lizv;)Z
    .locals 2

    .prologue
    .line 434
    invoke-virtual {p1}, Liwk;->getType()I

    move-result v0

    iget-object v1, p2, Lizv;->dVr:Liwk;

    invoke-virtual {v1}, Liwk;->getType()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p2, Lizv;->entry:Lizj;

    invoke-static {v0}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v0

    invoke-static {p0, v0}, Lfbo;->a(Lgbg;Lgbg;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/Iterable;Lgbg;Liwk;)Z
    .locals 2

    .prologue
    .line 376
    monitor-enter p1

    .line 377
    :try_start_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizv;

    .line 378
    invoke-static {p2, p3, v0}, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->a(Lgbg;Liwk;Lizv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 379
    const/4 v0, 0x1

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383
    :goto_0
    return v0

    .line 382
    :cond_1
    monitor-exit p1

    .line 383
    const/4 v0, 0x0

    goto :goto_0

    .line 382
    :catchall_0
    move-exception v0

    monitor-exit p1

    throw v0
.end method


# virtual methods
.method protected final I(Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 266
    const-string v0, "actions"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    .line 267
    invoke-static {v1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    array-length v0, v1

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 272
    iget-object v2, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coR:Ljava/lang/Object;

    monitor-enter v2

    .line 273
    :try_start_0
    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mFileReader:Lfcj;

    const-string v3, "executed_user_action_log"

    const/high16 v4, 0x80000

    invoke-virtual {v0, v3, v4}, Lfcj;->B(Ljava/lang/String;I)[B

    move-result-object v0

    .line 275
    new-instance v3, Lamu;

    invoke-direct {v3}, Lamu;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    if-eqz v0, :cond_0

    :try_start_1
    array-length v4, v0

    if-eqz v4, :cond_0

    .line 278
    invoke-static {v3, v0}, Ljsr;->c(Ljsr;[B)Ljsr;
    :try_end_1
    .catch Ljsq; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 287
    :cond_0
    :try_start_2
    invoke-static {v3, v1}, Ljsr;->c(Ljsr;[B)Ljsr;
    :try_end_2
    .catch Ljsq; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 294
    :try_start_3
    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mFileWriter:Lfck;

    const-string v1, "executed_user_action_log"

    invoke-static {v3}, Ljsr;->m(Ljsr;)[B

    move-result-object v3

    const/high16 v4, 0x80000

    invoke-virtual {v0, v1, v3, v4}, Lfck;->a(Ljava/lang/String;[BI)Z

    move-result v0

    if-nez v0, :cond_1

    .line 297
    const-string v0, "ExecutedUserActionStoreImpl"

    const-string v1, "Failed to write actions"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    :cond_1
    monitor-exit v2

    :goto_1
    return-void

    .line 268
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 281
    :catch_0
    move-exception v0

    const-string v0, "ExecutedUserActionStoreImpl"

    const-string v1, "File storage contained invalid data"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 299
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 289
    :catch_1
    move-exception v0

    :try_start_4
    const-string v0, "ExecutedUserActionStoreImpl"

    const-string v1, "Received intent with invalid action data"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public final M(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 133
    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    monitor-enter v1

    .line 134
    :try_start_0
    iget-object v2, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    iget-object v0, v0, Lamu;->afC:[Lizv;

    invoke-static {v0, p1}, Leqh;->a([Ljava/lang/Object;Ljava/util/Collection;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizv;

    iput-object v0, v2, Lamu;->afC:[Lizv;

    .line 136
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final P(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coQ:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizv;

    .line 121
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lizv;->hz(Z)Lizv;

    goto :goto_0

    .line 125
    :cond_0
    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    monitor-enter v1

    .line 126
    :try_start_0
    iget-object v2, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    iget-object v0, v0, Lamu;->afC:[Lizv;

    invoke-static {v0, p1}, Leqh;->a([Ljava/lang/Object;Ljava/util/Collection;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizv;

    iput-object v0, v2, Lamu;->afC:[Lizv;

    .line 128
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lizj;Liwk;JIZ)V
    .locals 5

    .prologue
    .line 98
    new-instance v0, Lgam;

    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, p1, p2, v2, v3}, Lgam;-><init>(Lizj;Liwk;J)V

    iput p5, v0, Lgam;->cEo:I

    invoke-virtual {v0, p6}, Lgam;->fB(Z)Lgam;

    move-result-object v0

    iput-wide p3, v0, Lgam;->cEn:J

    .line 104
    invoke-direct {p0, v0}, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->a(Lgam;)V

    .line 105
    return-void
.end method

.method public final a(Lizj;Liwk;Lixx;)V
    .locals 4

    .prologue
    .line 109
    new-instance v0, Lgam;

    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, p1, p2, v2, v3}, Lgam;-><init>(Lizj;Liwk;J)V

    iput-object p3, v0, Lgam;->cxS:Lixx;

    .line 113
    invoke-direct {p0, v0}, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->a(Lgam;)V

    .line 114
    return-void
.end method

.method public final axA()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 231
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->axB()V

    .line 234
    iget-object v2, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    monitor-enter v2

    .line 235
    :try_start_0
    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    iget-object v0, v0, Lamu;->afC:[Lizv;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 236
    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    .line 237
    iget-object v3, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    invoke-virtual {v3}, Lamu;->nM()Lamu;

    .line 239
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    if-eqz v0, :cond_0

    .line 241
    new-instance v2, Landroid/content/Intent;

    const-string v3, "write"

    iget-object v4, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mContext:Landroid/content/Context;

    const-class v5, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl$WriteService;

    invoke-direct {v2, v3, v1, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 242
    const-string v1, "actions"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 243
    invoke-virtual {p0, v2}, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->startService(Landroid/content/Intent;)V

    .line 245
    :cond_0
    return-void

    .line 239
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final axB()V
    .locals 5

    .prologue
    .line 411
    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coP:Ljava/util/List;

    monitor-enter v1

    .line 412
    :try_start_0
    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coP:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    monitor-exit v1

    .line 426
    :goto_0
    return-void

    .line 416
    :cond_0
    iget-object v2, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 417
    :try_start_1
    iget-object v3, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    iget-object v0, v0, Lamu;->afC:[Lizv;

    iget-object v4, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coP:Ljava/util/List;

    invoke-static {v0, v4}, Leqh;->a([Ljava/lang/Object;Ljava/util/Collection;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizv;

    iput-object v0, v3, Lamu;->afC:[Lizv;

    .line 419
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 421
    :try_start_2
    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coP:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 422
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 423
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.now.DEFERRED_ACTIONS_COMMITTED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 424
    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 425
    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 419
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 422
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axC()Ljava/util/List;
    .locals 5

    .prologue
    .line 185
    invoke-static {}, Lenu;->auQ()V

    .line 186
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 188
    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coR:Ljava/lang/Object;

    monitor-enter v1

    .line 189
    :try_start_0
    iget-object v2, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mFileReader:Lfcj;

    const-string v3, "executed_user_action_log"

    const/high16 v4, 0x80000

    invoke-virtual {v2, v3, v4}, Lfcj;->B(Ljava/lang/String;I)[B

    move-result-object v2

    .line 191
    if-eqz v2, :cond_0

    .line 192
    new-instance v3, Lamu;

    invoke-direct {v3}, Lamu;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    :try_start_1
    invoke-static {v3, v2}, Ljsr;->c(Ljsr;[B)Ljsr;

    .line 195
    iget-object v2, v3, Lamu;->afC:[Lizv;

    invoke-static {v0, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljsq; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 200
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 203
    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    monitor-enter v1

    .line 204
    :try_start_3
    iget-object v2, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    iget-object v2, v2, Lamu;->afC:[Lizv;

    invoke-static {v0, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 205
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 208
    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coP:Ljava/util/List;

    monitor-enter v1

    .line 209
    :try_start_4
    iget-object v2, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coP:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 210
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 212
    return-object v0

    .line 197
    :catch_0
    move-exception v2

    :try_start_5
    const-string v2, "ExecutedUserActionStoreImpl"

    const-string v3, "File storage contained invalid data"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 205
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 210
    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final axD()V
    .locals 3

    .prologue
    .line 249
    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coR:Ljava/lang/Object;

    monitor-enter v1

    .line 250
    :try_start_0
    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mFileWriter:Lfck;

    const-string v2, "executed_user_action_log"

    invoke-virtual {v0, v2}, Lfck;->lH(Ljava/lang/String;)V

    .line 251
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axE()V
    .locals 0

    .prologue
    .line 256
    return-void
.end method

.method public final axy()Ljava/util/List;
    .locals 5

    .prologue
    .line 155
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 157
    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coR:Ljava/lang/Object;

    monitor-enter v1

    .line 158
    :try_start_0
    iget-object v2, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mFileReader:Lfcj;

    const-string v3, "executed_user_action_log"

    const/high16 v4, 0x80000

    invoke-virtual {v2, v3, v4}, Lfcj;->B(Ljava/lang/String;I)[B

    move-result-object v2

    .line 160
    if-eqz v2, :cond_0

    .line 161
    new-instance v3, Lamu;

    invoke-direct {v3}, Lamu;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    :try_start_1
    invoke-static {v3, v2}, Ljsr;->c(Ljsr;[B)Ljsr;

    .line 164
    iget-object v2, v3, Lamu;->afC:[Lizv;

    invoke-static {v0, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljsq; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 171
    :cond_0
    :goto_0
    :try_start_2
    iget-object v2, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mFileWriter:Lfck;

    const-string v3, "executed_user_action_log"

    invoke-virtual {v2, v3}, Lfck;->lH(Ljava/lang/String;)V

    .line 172
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 175
    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    monitor-enter v1

    .line 176
    :try_start_3
    iget-object v2, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    iget-object v2, v2, Lamu;->afC:[Lizv;

    invoke-static {v0, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 177
    iget-object v2, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    invoke-virtual {v2}, Lamu;->nM()Lamu;

    .line 178
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 180
    return-object v0

    .line 166
    :catch_0
    move-exception v2

    :try_start_4
    const-string v2, "ExecutedUserActionStoreImpl"

    const-string v3, "File storage contained invalid data"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 178
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axz()V
    .locals 5

    .prologue
    .line 217
    new-instance v0, Landroid/content/Intent;

    const-string v1, "delete"

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mContext:Landroid/content/Context;

    const-class v4, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl$WriteService;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->startService(Landroid/content/Intent;)V

    .line 219
    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coP:Ljava/util/List;

    monitor-enter v1

    .line 220
    :try_start_0
    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coP:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 221
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 223
    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    monitor-enter v1

    .line 224
    :try_start_1
    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    invoke-virtual {v0}, Lamu;->nM()Lamu;

    .line 225
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 221
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 225
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Lizj;Liwk;)V
    .locals 4

    .prologue
    .line 343
    new-instance v0, Lgam;

    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, p1, p2, v2, v3}, Lgam;-><init>(Lizj;Liwk;J)V

    invoke-virtual {v0}, Lgam;->aDZ()Lizv;

    move-result-object v0

    .line 345
    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coP:Ljava/util/List;

    monitor-enter v1

    .line 348
    :try_start_0
    iget-object v2, v0, Lizv;->entry:Lizj;

    invoke-static {v2}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v2

    .line 349
    iget-object v3, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coP:Ljava/util/List;

    invoke-direct {p0, v3, v2, p2}, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->a(Ljava/lang/Iterable;Lgbg;Liwk;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 350
    iget-object v2, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coP:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c(Lizj;Liwk;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 388
    iget-object v2, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coP:Ljava/util/List;

    monitor-enter v2

    .line 389
    :try_start_0
    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coP:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390
    monitor-exit v2

    move v0, v1

    .line 406
    :goto_0
    return v0

    .line 394
    :cond_0
    new-instance v0, Lgam;

    const-wide/16 v4, 0x0

    invoke-direct {v0, p1, p2, v4, v5}, Lgam;-><init>(Lizj;Liwk;J)V

    invoke-virtual {v0}, Lgam;->aDZ()Lizv;

    move-result-object v0

    .line 396
    iget-object v0, v0, Lizv;->entry:Lizj;

    invoke-static {v0}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v3

    .line 397
    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coP:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 398
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 399
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizv;

    .line 400
    invoke-static {v3, p2, v0}, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->a(Lgbg;Liwk;Lizv;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 401
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    .line 402
    const/4 v0, 0x1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 405
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    monitor-exit v2

    move v0, v1

    .line 406
    goto :goto_0
.end method

.method public final d(Lizj;Liwk;)Z
    .locals 4

    .prologue
    .line 367
    new-instance v0, Lgam;

    const-wide/16 v2, 0x0

    invoke-direct {v0, p1, p2, v2, v3}, Lgam;-><init>(Lizj;Liwk;J)V

    invoke-virtual {v0}, Lgam;->aDZ()Lizv;

    move-result-object v0

    .line 369
    iget-object v0, v0, Lizv;->entry:Lizj;

    invoke-static {v0}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v0

    .line 370
    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coO:Lamu;

    iget-object v1, v1, Lamu;->afC:[Lizv;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1, v0, p2}, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->a(Ljava/lang/Iterable;Lgbg;Liwk;)Z

    move-result v0

    return v0
.end method

.method public final e(Lizj;Liwk;)V
    .locals 4

    .prologue
    .line 87
    invoke-virtual {p0, p1, p2}, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->c(Lizj;Liwk;)Z

    .line 89
    new-instance v0, Lgam;

    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, p1, p2, v2, v3}, Lgam;-><init>(Lizj;Liwk;J)V

    .line 92
    invoke-direct {p0, v0}, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->a(Lgam;)V

    .line 93
    return-void
.end method

.method public final f(Lizj;Liwk;)Z
    .locals 4

    .prologue
    .line 357
    new-instance v0, Lgam;

    const-wide/16 v2, 0x0

    invoke-direct {v0, p1, p2, v2, v3}, Lgam;-><init>(Lizj;Liwk;J)V

    invoke-virtual {v0}, Lgam;->aDZ()Lizv;

    move-result-object v0

    .line 361
    iget-object v0, v0, Lizv;->entry:Lizj;

    invoke-static {v0}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v0

    .line 362
    iget-object v1, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coP:Ljava/util/List;

    invoke-direct {p0, v1, v0, p2}, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->a(Ljava/lang/Iterable;Lgbg;Liwk;)Z

    move-result v0

    return v0
.end method

.method public final fk(Z)V
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->coQ:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 441
    return-void
.end method

.method public final onActivityStop()V
    .locals 0

    .prologue
    .line 261
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->axA()V

    .line 262
    return-void
.end method

.method protected startService(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/sidekick/main/inject/ExecutedUserActionStoreImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 308
    return-void
.end method
