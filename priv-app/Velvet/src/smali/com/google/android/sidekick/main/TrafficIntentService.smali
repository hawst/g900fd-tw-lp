.class public Lcom/google/android/sidekick/main/TrafficIntentService;
.super Landroid/app/IntentService;
.source "PG"


# instance fields
.field private bpu:Landroid/os/PowerManager;

.field private mAlarmHelper:Ldjx;

.field private mClock:Lemp;

.field private mEntryProvider:Lfaq;

.field private mLocationOracle:Lfdr;

.field private mNetworkClient:Lfcx;

.field private mNowNotificationManager:Lfga;

.field private mPendingIntentFactory:Lfda;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 94
    const-string v0, "TrafficIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 95
    return-void
.end method

.method private static a(Landroid/content/Context;Lfda;Z[B)Landroid/app/PendingIntent;
    .locals 3
    .param p3    # [B
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 345
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/sidekick/main/TrafficIntentService;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 346
    if-eqz p3, :cond_0

    .line 347
    const-string v0, "event_id"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 349
    :cond_0
    const/high16 v0, 0x48000000    # 131072.0f

    .line 350
    if-nez p2, :cond_1

    .line 351
    const/high16 v0, 0x68000000

    .line 353
    :cond_1
    const/4 v2, 0x0

    invoke-interface {p1, v2, v1, v0}, Lfda;->b(ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private a(IJ[B)V
    .locals 4
    .param p4    # [B
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 329
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TrafficIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mPendingIntentFactory:Lfda;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, p4}, Lcom/google/android/sidekick/main/TrafficIntentService;->a(Landroid/content/Context;Lfda;Z[B)Landroid/app/PendingIntent;

    move-result-object v0

    .line 331
    iget-object v1, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mAlarmHelper:Ldjx;

    invoke-virtual {v1, p1, p2, p3, v0}, Ldjx;->setExact(IJLandroid/app/PendingIntent;)V

    .line 332
    return-void
.end method

.method public static a(Landroid/content/Context;Lfda;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 139
    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/sidekick/main/TrafficIntentService;->a(Landroid/content/Context;Lfda;Z[B)Landroid/app/PendingIntent;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-nez v0, :cond_1

    .line 140
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/sidekick/main/TrafficIntentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 144
    :cond_1
    return-void
.end method


# virtual methods
.method protected final a(Lfdr;Lfcx;Lfga;Landroid/os/PowerManager;Lemp;Ldjx;Lfda;Lfaq;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mLocationOracle:Lfdr;

    .line 124
    iput-object p2, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mNetworkClient:Lfcx;

    .line 125
    iput-object p3, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mNowNotificationManager:Lfga;

    .line 126
    iput-object p4, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->bpu:Landroid/os/PowerManager;

    .line 127
    iput-object p5, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mClock:Lemp;

    .line 128
    iput-object p6, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mAlarmHelper:Ldjx;

    .line 129
    iput-object p7, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mPendingIntentFactory:Lfda;

    .line 130
    iput-object p8, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mEntryProvider:Lfaq;

    .line 131
    return-void
.end method

.method public onCreate()V
    .locals 10

    .prologue
    .line 99
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 104
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v9

    .line 105
    invoke-virtual {v9}, Lgql;->aJr()Lfdb;

    move-result-object v0

    .line 107
    iget-object v1, v0, Lfdb;->mLocationOracle:Lfdr;

    invoke-virtual {v0}, Lfdb;->axI()Lfcx;

    move-result-object v2

    invoke-virtual {v0}, Lfdb;->axO()Lfga;

    move-result-object v3

    const-string v4, "power"

    invoke-virtual {p0, v4}, Lcom/google/android/sidekick/main/TrafficIntentService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/PowerManager;

    invoke-virtual {v9}, Lgql;->SC()Lcfo;

    move-result-object v5

    invoke-virtual {v5}, Lcfo;->DC()Lemp;

    move-result-object v5

    invoke-virtual {v9}, Lgql;->SC()Lcfo;

    move-result-object v6

    invoke-virtual {v6}, Lcfo;->Ea()Ldjx;

    move-result-object v6

    invoke-virtual {v9}, Lgql;->SC()Lcfo;

    move-result-object v7

    invoke-virtual {v7}, Lcfo;->Eb()Lfda;

    move-result-object v7

    invoke-virtual {v0}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v8

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/sidekick/main/TrafficIntentService;->a(Lfdr;Lfcx;Lfga;Landroid/os/PowerManager;Lemp;Ldjx;Lfda;Lfaq;)V

    .line 115
    invoke-virtual {v9}, Lgql;->aJP()V

    .line 116
    return-void
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 14

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->bpu:Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "TrafficIntentService_onHandleIntent"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v6

    .line 154
    :try_start_0
    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 158
    if-eqz p1, :cond_1

    const-string v0, "com.google.android.apps.sidekick.TrafficIntentService.SHUTDOWN_ACTION"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TrafficIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mPendingIntentFactory:Lfda;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/sidekick/main/TrafficIntentService;->a(Landroid/content/Context;Lfda;Z[B)Landroid/app/PendingIntent;

    move-result-object v0

    .line 162
    if-eqz v0, :cond_0

    .line 164
    iget-object v1, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mAlarmHelper:Ldjx;

    invoke-virtual {v1, v0}, Ldjx;->cancel(Landroid/app/PendingIntent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    :cond_0
    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 195
    :goto_0
    return-void

    .line 170
    :cond_1
    :try_start_1
    const-string v0, "event_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 171
    iget-object v1, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mLocationOracle:Lfdr;

    invoke-interface {v1}, Lfdr;->tv()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v0, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mNowNotificationManager:Lfga;

    sget-object v1, Lfgb;->cqx:Lfgb;

    invoke-interface {v0, v1}, Lfga;->a(Lfgb;)V

    const/4 v0, 0x0

    move-object v1, v0

    .line 172
    :goto_1
    if-nez v1, :cond_a

    const-wide/16 v2, 0x0

    .line 173
    :goto_2
    if-nez v1, :cond_b

    const/4 v0, 0x0

    move-object v4, v0

    .line 176
    :goto_3
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_c

    .line 178
    iget-object v0, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    .line 179
    sub-long v8, v2, v0

    const-wide/32 v10, 0x493e0

    cmp-long v5, v8, v10

    if-gez v5, :cond_e

    .line 180
    const-wide/32 v2, 0x493e0

    add-long/2addr v0, v2

    .line 182
    :goto_4
    const/4 v2, 0x0

    invoke-direct {p0, v2, v0, v1, v4}, Lcom/google/android/sidekick/main/TrafficIntentService;->a(IJ[B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 194
    :cond_2
    :goto_5
    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    .line 171
    :cond_3
    :try_start_2
    iget-object v1, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mNetworkClient:Lfcx;

    new-instance v2, Ljbj;

    invoke-direct {v2}, Ljbj;-><init>()V

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljbj;->om(I)Ljbj;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    aput v5, v3, v4

    iput-object v3, v2, Ljbj;->dYx:[I

    new-instance v3, Lizm;

    invoke-direct {v3}, Lizm;-><init>()V

    const/4 v4, 0x1

    new-array v4, v4, [Ljbj;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    iput-object v4, v3, Lizm;->dUH:[Ljbj;

    new-instance v2, Ljee;

    invoke-direct {v2}, Ljee;-><init>()V

    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Ljee;->ph(I)Ljee;

    move-result-object v2

    if-eqz v0, :cond_4

    invoke-virtual {v2, v0}, Ljee;->aj([B)Ljee;

    :cond_4
    const/16 v0, 0x11

    invoke-static {v0}, Lfjw;->iQ(I)Ljed;

    move-result-object v0

    const/4 v4, 0x1

    new-array v4, v4, [Ljee;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    iput-object v4, v0, Ljed;->edM:[Ljee;

    iput-object v3, v0, Ljed;->edr:Lizm;

    invoke-interface {v1, v0}, Lfcx;->b(Ljed;)Lfcy;

    move-result-object v7

    if-nez v7, :cond_6

    const/4 v0, 0x0

    move-object v3, v0

    :goto_6
    if-eqz v3, :cond_5

    iget-object v0, v3, Ljeh;->aeC:Lizn;

    if-nez v0, :cond_7

    :cond_5
    iget-object v0, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mNowNotificationManager:Lfga;

    sget-object v1, Lfgb;->cqx:Lfgb;

    invoke-interface {v0, v1}, Lfga;->a(Lfgb;)V

    const/4 v0, 0x0

    move-object v1, v0

    goto/16 :goto_1

    :cond_6
    iget-object v0, v7, Lfcy;->coT:Ljeh;

    move-object v3, v0

    goto :goto_6

    :cond_7
    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, v3, Ljeh;->aeC:Lizn;

    if-eqz v5, :cond_12

    iget-object v5, v3, Ljeh;->aeC:Lizn;

    iget-object v5, v5, Lizn;->dUI:[Lizo;

    array-length v5, v5

    if-lez v5, :cond_12

    iget-object v3, v3, Ljeh;->aeC:Lizn;

    iget-object v3, v3, Lizn;->dUI:[Lizo;

    const/4 v5, 0x0

    aget-object v8, v3, v5

    const/4 v3, -0x1

    invoke-virtual {v8}, Lizo;->hasError()Z

    move-result v5

    if-eqz v5, :cond_11

    invoke-virtual {v8}, Lizo;->aZt()I

    move-result v3

    move v5, v3

    :goto_7
    invoke-virtual {v8}, Lizo;->bdm()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-virtual {v8}, Lizo;->bdl()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, v0

    iget-object v1, v7, Lfcy;->cnq:[B

    :goto_8
    new-instance v7, Lewe;

    invoke-direct {v7}, Lewe;-><init>()V

    new-instance v0, Lizo;

    invoke-direct {v0}, Lizo;-><init>()V

    invoke-static {v8, v0}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lizo;

    invoke-virtual {v7, v0}, Lewe;->j(Lizo;)V

    iget-object v0, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mEntryProvider:Lfaq;

    iget-object v7, v7, Lewe;->cko:Ljava/util/List;

    invoke-virtual {v0, v7}, Lfaq;->h(Ljava/lang/Iterable;)V

    iget-object v0, v8, Lizo;->dUQ:Lizq;

    if-eqz v0, :cond_f

    const/4 v0, -0x1

    if-ne v5, v0, :cond_f

    iget-object v0, v8, Lizo;->dUQ:Lizq;

    iget-object v5, v0, Lizq;->dUX:[Lizj;

    array-length v5, v5

    if-lez v5, :cond_f

    iget-object v0, v0, Lizq;->dUX:[Lizj;

    const/4 v5, 0x0

    aget-object v0, v0, v5

    iget-object v5, v0, Lizj;->dUr:Ljcg;

    if-eqz v5, :cond_f

    iget-object v5, v0, Lizj;->dUr:Ljcg;

    invoke-virtual {v5}, Ljcg;->getType()I

    move-result v5

    const/4 v7, 0x3

    if-eq v5, v7, :cond_f

    iget-object v4, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mLocationOracle:Lfdr;

    invoke-interface {v4}, Lfdr;->ayy()Landroid/location/Location;

    move-result-object v4

    new-instance v5, Landroid/content/Intent;

    const-string v7, "com.google.android.apps.sidekick.NOTIFICATION_ENTRY_ACTION"

    invoke-direct {v5, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v7, "notification_entry"

    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    invoke-virtual {v5, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const-string v0, "location_key"

    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/TrafficIntentService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v5}, Lcom/google/android/sidekick/main/TrafficIntentService;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v0, 0x1

    move-wide v12, v2

    move v2, v0

    move-object v3, v1

    move-wide v0, v12

    :goto_9
    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mNowNotificationManager:Lfga;

    sget-object v4, Lfgb;->cqx:Lfgb;

    invoke-interface {v2, v4}, Lfga;->a(Lfgb;)V

    :cond_8
    const-wide/16 v4, 0x0

    cmp-long v2, v0, v4

    if-nez v2, :cond_9

    const/4 v0, 0x0

    move-object v1, v0

    goto/16 :goto_1

    :cond_9
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1

    .line 172
    :cond_a
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto/16 :goto_2

    .line 173
    :cond_b
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, [B

    move-object v4, v0

    goto/16 :goto_3

    .line 184
    :cond_c
    if-eqz p1, :cond_d

    const-string v0, "force_refresh"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 188
    :cond_d
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/sidekick/main/TrafficIntentService;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->elapsedRealtime()J

    move-result-wide v2

    const-wide/32 v4, 0xdbba0

    add-long/2addr v2, v4

    const/4 v1, 0x0

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/google/android/sidekick/main/TrafficIntentService;->a(IJ[B)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_5

    .line 193
    :catchall_0
    move-exception v0

    .line 194
    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0

    :cond_e
    move-wide v0, v2

    goto/16 :goto_4

    :cond_f
    move-wide v12, v2

    move v2, v4

    move-object v3, v1

    move-wide v0, v12

    goto :goto_9

    :cond_10
    move-wide v12, v0

    move-object v1, v2

    move-wide v2, v12

    goto/16 :goto_8

    :cond_11
    move v5, v3

    goto/16 :goto_7

    :cond_12
    move-object v3, v2

    move v2, v4

    goto :goto_9
.end method
