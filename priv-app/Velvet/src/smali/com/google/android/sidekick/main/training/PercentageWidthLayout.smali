.class public Lcom/google/android/sidekick/main/training/PercentageWidthLayout;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final crJ:I

.field private final crK:F

.field private final crL:I

.field private crM:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    sget-object v2, Lbwe;->aLW:[I

    invoke-virtual {p1, p2, v2, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 44
    invoke-virtual {v2, v1, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->crJ:I

    .line 46
    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->crK:F

    .line 47
    iget v3, p0, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->crK:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_0

    iget v3, p0, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->crK:F

    cmpg-float v3, v3, v5

    if-gtz v3, :cond_0

    :goto_0
    const-string v1, "childWidthPercent must be between 0 and 1"

    invoke-static {v0, v1}, Lifv;->c(ZLjava/lang/Object;)V

    .line 49
    const/4 v0, 0x2

    const v1, 0x7fffffff

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->crL:I

    .line 51
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 52
    return-void

    :cond_0
    move v0, v1

    .line 47
    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .locals 3

    .prologue
    .line 56
    iget v0, p0, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->crM:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->crM:I

    .line 57
    :goto_0
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v0, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    return-object v1

    .line 56
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 4

    .prologue
    .line 62
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    .line 64
    iget v0, p0, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->crJ:I

    if-ge p1, v0, :cond_1

    const/4 v0, -0x1

    .line 65
    :cond_0
    :goto_0
    iget v1, p0, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->crM:I

    if-eq v0, v1, :cond_3

    .line 66
    iput v0, p0, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->crM:I

    .line 67
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->getChildCount()I

    move-result v1

    .line 68
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    .line 69
    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v3, p0, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->crM:I

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 68
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 64
    :cond_1
    int-to-float v0, p1

    iget v1, p0, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->crK:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p0, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->crL:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->crL:I

    goto :goto_0

    .line 72
    :cond_2
    invoke-virtual {p0, p0}, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->post(Ljava/lang/Runnable;)Z

    .line 74
    :cond_3
    return-void
.end method

.method public run()V
    .locals 0

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/training/PercentageWidthLayout;->requestLayout()V

    .line 92
    return-void
.end method
