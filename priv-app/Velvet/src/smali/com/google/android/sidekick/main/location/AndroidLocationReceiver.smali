.class public Lcom/google/android/sidekick/main/location/AndroidLocationReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 23
    const-string v0, "com.google.android.apps.sidekick.android_location_broadcast"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    iget-object v1, v0, Lfdb;->mLocationOracle:Lfdr;

    .line 26
    const-string v0, "location"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 27
    if-eqz v0, :cond_0

    instance-of v2, v0, Landroid/location/Location;

    if-eqz v2, :cond_0

    instance-of v2, v1, Lfdu;

    if-eqz v2, :cond_0

    .line 32
    check-cast v0, Landroid/location/Location;

    invoke-interface {v1, v0}, Lfdr;->d(Landroid/location/Location;)V

    .line 35
    :cond_0
    return-void
.end method
