.class public Lcom/google/android/sidekick/shared/training/QuestionKey;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final cyh:Ljde;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lfqa;

    invoke-direct {v0}, Lfqa;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/shared/training/QuestionKey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljde;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    iput-object p1, p0, Lcom/google/android/sidekick/shared/training/QuestionKey;->cyh:Ljde;

    .line 30
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 34
    if-ne p0, p1, :cond_1

    move v1, v2

    .line 45
    :cond_0
    :goto_0
    return v1

    .line 38
    :cond_1
    instance-of v0, p1, Lcom/google/android/sidekick/shared/training/QuestionKey;

    if-eqz v0, :cond_0

    .line 42
    check-cast p1, Lcom/google/android/sidekick/shared/training/QuestionKey;

    .line 43
    iget-object v0, p1, Lcom/google/android/sidekick/shared/training/QuestionKey;->cyh:Ljde;

    .line 45
    iget-object v3, p0, Lcom/google/android/sidekick/shared/training/QuestionKey;->cyh:Ljde;

    invoke-virtual {v3}, Ljde;->bhe()[B

    move-result-object v3

    invoke-virtual {v0}, Ljde;->bhe()[B

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/sidekick/shared/training/QuestionKey;->cyh:Ljde;

    iget-object v3, v3, Ljde;->ebv:[Ljdg;

    iget-object v4, v0, Ljde;->ebv:[Ljdg;

    array-length v0, v3

    array-length v5, v4

    if-eq v0, v5, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_0

    move v1, v2

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_2
    array-length v5, v3

    if-ge v0, v5, :cond_5

    aget-object v5, v3, v0

    aget-object v6, v4, v0

    iget-object v7, v5, Ljdg;->ebK:Ljfh;

    if-nez v7, :cond_3

    iget-object v7, v5, Ljdg;->bnG:Ljfl;

    if-nez v7, :cond_3

    iget-object v7, v6, Ljdg;->ebK:Ljfh;

    if-nez v7, :cond_3

    iget-object v7, v6, Ljdg;->bnG:Ljfl;

    if-eqz v7, :cond_4

    :cond_3
    new-instance v7, Lfpr;

    invoke-direct {v7, v5}, Lfpr;-><init>(Ljdg;)V

    new-instance v5, Lfpr;

    invoke-direct {v5, v6}, Lfpr;-><init>(Ljdg;)V

    invoke-virtual {v7, v5}, Lfpr;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/QuestionKey;->cyh:Ljde;

    invoke-virtual {v0}, Ljde;->bhe()[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 91
    invoke-static {p0}, Lifo;->bc(Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "templateId"

    iget-object v2, p0, Lcom/google/android/sidekick/shared/training/QuestionKey;->cyh:Ljde;

    invoke-virtual {v2}, Ljde;->bgX()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lifp;->u(Ljava/lang/String;J)Lifp;

    move-result-object v0

    const-string v1, "fingerprint"

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/sidekick/shared/training/QuestionKey;->cyh:Ljde;

    invoke-virtual {v3}, Ljde;->bhe()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v0, v1, v2}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    invoke-virtual {v0}, Lifp;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/QuestionKey;->cyh:Ljde;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 119
    return-void
.end method
