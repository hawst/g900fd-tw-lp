.class public Lcom/google/android/sidekick/shared/training/BinaryGroupQuestionView;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Lfqb;


# instance fields
.field public cyC:Lfqc;

.field public cym:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

.field public mEntry:Lizj;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method

.method private a(IILjava/lang/String;Ljdf;Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lcom/google/android/sidekick/shared/training/BinaryGroupQuestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 62
    invoke-virtual {v0, p3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 63
    new-instance v1, Lfpn;

    invoke-direct {v1, p0, v0, p2, p4}, Lfpn;-><init>(Lcom/google/android/sidekick/shared/training/BinaryGroupQuestionView;Landroid/widget/Button;ILjdf;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    if-eqz p5, :cond_0

    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p4}, Ljdf;->bhh()Z

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSelected(Z)V

    .line 78
    return-void

    .line 77
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lfqc;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/sidekick/shared/training/BinaryGroupQuestionView;->cyC:Lfqc;

    .line 83
    return-void
.end method

.method public final aU(II)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lfqk;->h(Landroid/view/View;II)V

    .line 40
    return-void
.end method

.method public final b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;)V
    .locals 12

    .prologue
    const v2, 0x7f1100fc

    const v1, 0x7f1100fb

    .line 44
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/BinaryGroupQuestionView;->mEntry:Lizj;

    invoke-static {p0, p1, v0}, Lfqk;->a(Landroid/view/View;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lizj;)V

    .line 46
    iput-object p1, p0, Lcom/google/android/sidekick/shared/training/BinaryGroupQuestionView;->cym:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    .line 48
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/BinaryGroupQuestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 49
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/BinaryGroupQuestionView;->cym:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v0

    .line 51
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljdf;->bhi()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Ljdf;->bhh()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 53
    :goto_0
    const v0, 0x7f0a0452

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v0, Ljdf;

    invoke-direct {v0}, Ljdf;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljdf;->hH(Z)Ljdf;

    move-result-object v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/sidekick/shared/training/BinaryGroupQuestionView;->a(IILjava/lang/String;Ljdf;Ljava/lang/Boolean;)V

    .line 55
    const v0, 0x7f0a0451

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-instance v0, Ljdf;

    invoke-direct {v0}, Ljdf;-><init>()V

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljdf;->hH(Z)Ljdf;

    move-result-object v10

    move-object v6, p0

    move v7, v2

    move v8, v1

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/google/android/sidekick/shared/training/BinaryGroupQuestionView;->a(IILjava/lang/String;Ljdf;Ljava/lang/Boolean;)V

    .line 57
    return-void

    .line 51
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final z(Lizj;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/sidekick/shared/training/BinaryGroupQuestionView;->mEntry:Lizj;

    .line 88
    return-void
.end method
