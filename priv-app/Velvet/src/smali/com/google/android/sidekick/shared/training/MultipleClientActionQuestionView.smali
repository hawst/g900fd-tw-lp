.class public Lcom/google/android/sidekick/shared/training/MultipleClientActionQuestionView;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Lfqb;


# instance fields
.field public cyC:Lfqc;

.field private mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 35
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/training/MultipleClientActionQuestionView;->ed()V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/training/MultipleClientActionQuestionView;->ed()V

    .line 41
    return-void
.end method

.method private ed()V
    .locals 2

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/MultipleClientActionQuestionView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/MultipleClientActionQuestionView;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 45
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/MultipleClientActionQuestionView;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0401a0

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 46
    return-void
.end method


# virtual methods
.method public final a(Lfqc;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/sidekick/shared/training/MultipleClientActionQuestionView;->cyC:Lfqc;

    .line 51
    return-void
.end method

.method public final aU(II)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lfqk;->h(Landroid/view/View;II)V

    .line 61
    return-void
.end method

.method public final b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 65
    invoke-static {p0, p1, v6}, Lfqk;->a(Landroid/view/View;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lizj;)V

    .line 68
    const v0, 0x7f11046b

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/training/MultipleClientActionQuestionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 69
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 71
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCr()Ljava/util/List;

    move-result-object v1

    .line 72
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/MultipleClientActionQuestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0c0066

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-static {v2, v3}, Lfqk;->aV(II)I

    move-result v7

    .line 77
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v4

    move-object v3, v6

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfpa;

    .line 78
    if-eqz v3, :cond_0

    if-ne v2, v7, :cond_5

    .line 80
    :cond_0
    new-instance v2, Landroid/widget/TableRow;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/MultipleClientActionQuestionView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 81
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move v3, v4

    move-object v5, v2

    .line 84
    :goto_1
    iget-object v9, v1, Lfpa;->cyj:Ljava/lang/Integer;

    .line 85
    if-nez v9, :cond_1

    .line 86
    const-string v2, "MultipleClientActionQuestionView"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Option missing client action: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Lfpa;->cyk:Ljava/lang/String;

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    move-object v3, v5

    .line 87
    goto :goto_0

    .line 90
    :cond_1
    iget-object v2, p0, Lcom/google/android/sidekick/shared/training/MultipleClientActionQuestionView;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v10, 0x7f0401a1

    invoke-virtual {v2, v10, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 92
    iget-object v10, v1, Lfpa;->cyk:Ljava/lang/String;

    if-eqz v10, :cond_2

    .line 93
    iget-object v10, v1, Lfpa;->cyk:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 95
    :cond_2
    iget v1, v1, Lfpa;->cyi:I

    invoke-static {v1}, Lfqk;->jk(I)I

    move-result v1

    .line 96
    if-eqz v1, :cond_3

    .line 97
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/MultipleClientActionQuestionView;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10, v1}, Lfqk;->i(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 99
    invoke-static {v2, v6, v1, v6, v6}, Leot;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 103
    :cond_3
    new-instance v1, Lfpy;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-direct {v1, p0, p1, v9}, Lfpy;-><init>(Lcom/google/android/sidekick/shared/training/MultipleClientActionQuestionView;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;I)V

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    invoke-virtual {v5, v2}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 106
    add-int/lit8 v2, v3, 0x1

    move-object v3, v5

    .line 107
    goto :goto_0

    .line 108
    :cond_4
    return-void

    :cond_5
    move-object v5, v3

    move v3, v2

    goto :goto_1
.end method

.method public final z(Lizj;)V
    .locals 0

    .prologue
    .line 56
    return-void
.end method
