.class public Lcom/google/android/sidekick/main/gcm/SidekickGCMIntentService;
.super Landroid/app/IntentService;
.source "PG"


# instance fields
.field private coJ:Lcch;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    const-string v0, "638181764685"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Lcch;)V
    .locals 1

    .prologue
    .line 41
    const-string v0, "638181764685"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 42
    iput-object p1, p0, Lcom/google/android/sidekick/main/gcm/SidekickGCMIntentService;->coJ:Lcch;

    .line 43
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 55
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 56
    invoke-static {p0}, Lbjz;->X(Landroid/content/Context;)Lbjz;

    .line 60
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.google.android.c2dm.intent.RECEIVE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 62
    :cond_0
    :goto_0
    invoke-virtual {v2}, Landroid/os/Bundle;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 63
    const-string v3, "deleted_messages"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 65
    const-string v3, "gcm"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    const-string v0, "m"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_2

    :try_start_1
    new-instance v2, Ljgd;

    invoke-direct {v2}, Ljgd;-><init>()V

    invoke-static {v2, v0}, Leqh;->a(Ljsr;Ljava/lang/String;)Ljsr;

    move-result-object v0

    check-cast v0, Ljgd;

    iget-object v2, v0, Ljgd;->ejs:Ljcl;

    if-eqz v2, :cond_4

    iget-object v1, p0, Lcom/google/android/sidekick/main/gcm/SidekickGCMIntentService;->coJ:Lcch;

    if-nez v1, :cond_1

    new-instance v1, Lcch;

    invoke-direct {v1, p0}, Lcch;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/sidekick/main/gcm/SidekickGCMIntentService;->coJ:Lcch;

    :cond_1
    iget-object v1, p0, Lcom/google/android/sidekick/main/gcm/SidekickGCMIntentService;->coJ:Lcch;

    iget-object v0, v0, Ljgd;->ejs:Ljcl;

    invoke-virtual {v1, v0}, Lcch;->a(Ljcl;)V
    :try_end_1
    .catch Ljsq; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 71
    :cond_2
    :goto_1
    invoke-static {p1}, Lcom/google/android/sidekick/main/gcm/SidekickGCMBroadcastReceiver;->b(Landroid/content/Intent;)Z

    .line 72
    return-void

    .line 60
    :cond_3
    :try_start_2
    const-string v0, "message_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "gcm"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 66
    :cond_4
    :try_start_3
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v3

    invoke-virtual {v3}, Lcfo;->DL()Lcrh;

    move-result-object v4

    invoke-virtual {v4}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual {v3}, Lcfo;->DF()Lcin;

    move-result-object v3

    invoke-interface {v3, v4}, Lcin;->e(Landroid/accounts/Account;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v4}, Lfcn;->G(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljgd;->bjQ()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Ljgd;->bjP()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    :cond_5
    iget-object v3, v0, Ljgd;->ejr:Lizk;

    if-eqz v3, :cond_6

    const/4 v1, 0x1

    :cond_6
    if-eqz v1, :cond_7

    invoke-virtual {v2}, Lgql;->aJr()Lfdb;

    move-result-object v1

    invoke-virtual {v1}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v1

    iget-object v0, v0, Ljgd;->ejr:Lizk;

    invoke-virtual {v1, v0}, Lfaq;->a(Lizk;)V
    :try_end_3
    .catch Ljsq; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_4
    const-string v1, "SidekickGCMIntentService"

    const-string v2, "Bad push message received over GCM"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 71
    :catchall_0
    move-exception v0

    invoke-static {p1}, Lcom/google/android/sidekick/main/gcm/SidekickGCMBroadcastReceiver;->b(Landroid/content/Intent;)Z

    throw v0

    .line 66
    :cond_7
    :try_start_5
    invoke-virtual {v2}, Lgql;->aJr()Lfdb;

    move-result-object v1

    invoke-virtual {v1}, Lfdb;->axH()Lfil;

    move-result-object v1

    iget-object v0, v0, Ljgd;->afA:Ljbj;

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v0, v2}, Lfil;->a(Landroid/content/Context;Ljbj;Z)V

    goto :goto_1

    :cond_8
    const-string v0, "SidekickGCMIntentService"

    const-string v1, "Received a push message for another account on this phone"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljsq; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method
