.class public Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;
.super Landroid/app/IntentService;
.source "PG"


# static fields
.field private static final cqh:Ljava/util/Set;

.field private static final cqi:Lffo;


# instance fields
.field private ckD:Leuc;

.field private mAlarmHelper:Ldjx;

.field private mClock:Lemp;

.field private mEntryNotificationFactory:Lfjs;

.field private mEntryProvider:Lfaq;

.field private mNetworkClient:Lfcx;

.field private mNotificationStore:Lffp;

.field private mNowNotificationManager:Lfga;

.field private mNowOptInSettings:Lcin;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    .line 163
    const-string v0, "com.google.android.apps.sidekick.notifications.REFRESH"

    const-string v1, "com.google.android.apps.sidekick.notifications.SCHEDULE_REFRESH"

    const-string v2, "com.google.android.apps.sidekick.notifications.SHOW_NOTIFICATIONS"

    const-string v3, "com.google.android.apps.sidekick.notifications.EXPIRE_NOTIFICATIONS"

    const-string v4, "com.google.android.apps.sidekick.notifications.NOTIFICATION_DISMISS_ACTION"

    const-string v5, "com.google.android.apps.sidekick.notifications.NOTIFICATION_DELETE_ACTION"

    const/4 v6, 0x7

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "com.google.android.apps.sidekick.notifications.NOTIFICATION_TRIGGER_ACTION"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "com.google.android.apps.sidekick.notifications.NOTIFICATION_GEOFENCE_ACTION"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "com.google.android.apps.sidekick.notifications.INITIALIZE"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "com.google.android.apps.sidekick.notifications.SHUTDOWN"

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string v8, "com.google.android.apps.sidekick.notifications.REFRESH_ALL_NOTIFICATIONS"

    aput-object v8, v6, v7

    const/4 v7, 0x5

    const-string v8, "com.google.android.apps.sidekick.notifications.NOTIFICATION_ADD_ACTION"

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const-string v8, "com.google.android.apps.sidekick.notifications.RESET_ALARMS_ACTION"

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, Lijp;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Lijp;

    move-result-object v0

    sput-object v0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->cqh:Ljava/util/Set;

    .line 176
    new-instance v0, Lffo;

    invoke-direct {v0}, Lffo;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->cqi:Lffo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 190
    const-string v0, "NotificationRefreshService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 191
    return-void
.end method

.method private R(Ljava/util/List;)V
    .locals 20
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 338
    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 339
    :cond_0
    const-string v2, "NotificationRefreshService"

    const-string v3, "Skipping notification refresh, no interests to query."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    :goto_0
    return-void

    .line 343
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v10

    .line 344
    new-instance v9, Lizm;

    invoke-direct {v9}, Lizm;-><init>()V

    .line 345
    const/16 v2, 0xe

    invoke-static {v2}, Lfjw;->iQ(I)Ljed;

    move-result-object v4

    .line 347
    iput-object v9, v4, Ljed;->edr:Lizm;

    .line 349
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lffv;

    .line 350
    iget-object v3, v2, Lffv;->cqs:Ljbj;

    .line 351
    invoke-virtual {v3}, Ljbj;->nK()I

    move-result v6

    const/4 v7, 0x3

    if-ne v6, v7, :cond_2

    .line 352
    iget-object v6, v9, Lizm;->dUH:[Ljbj;

    invoke-static {v6, v3}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljbj;

    iput-object v3, v9, Lizm;->dUH:[Ljbj;

    .line 357
    :cond_2
    new-instance v3, Ljee;

    invoke-direct {v3}, Ljee;-><init>()V

    const/4 v6, 0x3

    invoke-virtual {v3, v6}, Ljee;->ph(I)Ljee;

    move-result-object v3

    .line 359
    iget-object v6, v2, Lffv;->cnq:[B

    if-eqz v6, :cond_3

    .line 360
    iget-object v2, v2, Lffv;->cnq:[B

    invoke-virtual {v3, v2}, Ljee;->aj([B)Ljee;

    .line 362
    :cond_3
    iget-object v2, v4, Ljed;->edM:[Ljee;

    invoke-static {v2, v3}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljee;

    iput-object v2, v4, Ljed;->edM:[Ljee;

    goto :goto_1

    .line 365
    :cond_4
    iget-object v2, v9, Lizm;->dUH:[Ljbj;

    array-length v2, v2

    if-nez v2, :cond_5

    .line 366
    const-string v2, "NotificationRefreshService"

    const-string v3, "Skipping notification refresh, no notification interests found"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 372
    :cond_5
    new-instance v3, Liyb;

    invoke-direct {v3}, Liyb;-><init>()V

    .line 373
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->ckD:Leuc;

    invoke-virtual {v2}, Leuc;->avW()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljbt;

    .line 378
    iget-object v6, v3, Liyb;->dMd:[Ljbt;

    invoke-static {v6, v2}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljbt;

    iput-object v2, v3, Liyb;->dMd:[Ljbt;

    goto :goto_2

    .line 384
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNetworkClient:Lfcx;

    invoke-interface {v2, v4}, Lfcx;->b(Ljed;)Lfcy;

    move-result-object v12

    .line 386
    if-nez v12, :cond_8

    const/4 v2, 0x0

    .line 390
    :goto_3
    if-eqz v2, :cond_7

    iget-object v3, v2, Ljeh;->aeC:Lizn;

    if-nez v3, :cond_9

    .line 391
    :cond_7
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->elapsedRealtime()J

    move-result-wide v4

    sget-object v3, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->cqi:Lffo;

    iget v6, v3, Lffo;->cme:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v3, Lffo;->cme:I

    iget-object v7, v3, Lffo;->cmc:[I

    array-length v7, v7

    add-int/lit8 v7, v7, -0x1

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    iget-object v7, v3, Lffo;->cmc:[I

    aget v6, v7, v6

    int-to-long v6, v6

    const-wide/32 v8, 0xea60

    mul-long/2addr v6, v8

    iget v3, v3, Lffo;->cmd:I

    int-to-long v8, v3

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    add-long/2addr v6, v8

    add-long/2addr v4, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4, v5}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->f(IJ)V

    .line 393
    const-string v2, "NotificationRefreshService"

    const-string v3, "Failed to get response for notification query from server"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 386
    :cond_8
    iget-object v2, v12, Lfcy;->coT:Ljeh;

    goto :goto_3

    .line 397
    :cond_9
    sget-object v3, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->cqi:Lffo;

    const/4 v4, -0x1

    iput v4, v3, Lffo;->cme:I

    .line 399
    iget-object v13, v2, Ljeh;->aeC:Lizn;

    .line 400
    iget-object v2, v13, Lizn;->dUI:[Lizo;

    array-length v2, v2

    iget-object v3, v9, Lizm;->dUH:[Ljbj;

    array-length v3, v3

    if-eq v2, v3, :cond_b

    .line 402
    const-string v2, "NotificationRefreshService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "got back "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v13, Lizn;->dUI:[Lizo;

    array-length v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " entry trees for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v9, Lizm;->dUH:[Ljbj;

    array-length v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " interests"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    :cond_a
    :goto_4
    invoke-direct/range {p0 .. p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azh()V

    goto/16 :goto_0

    .line 404
    :cond_b
    iget-object v2, v13, Lizn;->dUI:[Lizo;

    array-length v2, v2

    if-eqz v2, :cond_a

    .line 406
    const/4 v2, 0x0

    :goto_5
    iget-object v3, v13, Lizn;->dUI:[Lizo;

    array-length v3, v3

    if-ge v2, v3, :cond_d

    .line 407
    iget-object v3, v9, Lizm;->dUH:[Ljbj;

    aget-object v6, v3, v2

    .line 408
    iget-object v3, v13, Lizn;->dUI:[Lizo;

    aget-object v14, v3, v2

    .line 413
    invoke-virtual {v14}, Lizo;->bdm()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 417
    invoke-virtual {v14}, Lizo;->bdl()J

    move-result-wide v4

    const-wide/32 v16, 0x493e0

    add-long v16, v16, v10

    const-wide/16 v18, 0x3e8

    div-long v16, v16, v18

    move-wide/from16 v0, v16

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 420
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    const/4 v7, 0x1

    iget-object v8, v12, Lfcy;->cnq:[B

    invoke-virtual/range {v3 .. v8}, Lffp;->a(JLjbj;Z[B)V

    .line 427
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v3, v14, v6}, Lffp;->a(Lizo;Ljbj;)V

    .line 430
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    const-wide/16 v4, 0x3e8

    div-long v4, v10, v4

    invoke-virtual {v3, v4, v5, v6}, Lffp;->a(JLjbj;)V

    .line 406
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 436
    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->awG()V

    goto :goto_4
.end method

.method public static a(Landroid/content/Context;Ljava/util/Collection;Lfgb;)Landroid/app/PendingIntent;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 641
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 642
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 646
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "notification_refresh_dismiss"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p2}, Lfgb;->azC()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 648
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.apps.sidekick.notifications.NOTIFICATION_DISMISS_ACTION"

    const-class v4, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;

    invoke-direct {v2, v3, v0, p0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 650
    const-string v0, "notification_entries"

    invoke-static {v2, v0, p1}, Lgbm;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    .line 652
    const/high16 v0, 0x10000000

    invoke-static {p0, v1, v2, v0}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    .line 642
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/util/Set;Ljava/util/Set;)Landroid/content/Intent;
    .locals 2
    .param p1    # Ljava/util/Set;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/Set;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 724
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.notifications.NOTIFICATION_TRIGGER_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 725
    const-class v1, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 726
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 727
    const-string v1, "triggered_notification_entries"

    invoke-static {v0, v1, p1}, Lgbm;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    .line 729
    :cond_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 730
    const-string v1, "concluded_notification_entries"

    invoke-static {v0, v1, p2}, Lgbm;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    .line 732
    :cond_1
    return-object v0
.end method

.method public static aM(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 754
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.notifications.NOTIFICATION_GEOFENCE_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 755
    const-class v1, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 756
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private awG()V
    .locals 8

    .prologue
    .line 444
    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azg()Landroid/app/PendingIntent;

    move-result-object v0

    .line 445
    iget-object v1, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v1}, Lffp;->azt()Ljava/lang/Long;

    move-result-object v1

    .line 447
    if-nez v1, :cond_0

    .line 449
    iget-object v1, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mAlarmHelper:Ldjx;

    invoke-virtual {v1, v0}, Ldjx;->cancel(Landroid/app/PendingIntent;)V

    .line 460
    :goto_0
    return-void

    .line 456
    :cond_0
    iget-object v2, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    .line 457
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x1388

    add-long/2addr v2, v6

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 458
    iget-object v2, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mAlarmHelper:Ldjx;

    const/4 v3, 0x0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5, v0}, Ldjx;->setExact(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private azg()Landroid/app/PendingIntent;
    .locals 5

    .prologue
    .line 463
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.notifications.SHOW_NOTIFICATIONS"

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 465
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private azh()V
    .locals 4

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v0}, Lffp;->azo()Ljava/lang/Long;

    move-result-object v0

    .line 472
    if-eqz v0, :cond_0

    .line 473
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->f(IJ)V

    .line 475
    :cond_0
    return-void
.end method

.method private azi()Landroid/app/PendingIntent;
    .locals 5

    .prologue
    .line 490
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.notifications.REFRESH"

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 492
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private azj()V
    .locals 7

    .prologue
    const/16 v4, 0x2b

    const/4 v6, 0x1

    .line 498
    invoke-static {}, Lilf;->aXk()Lilf;

    move-result-object v1

    .line 499
    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v0}, Lffp;->azm()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 500
    invoke-virtual {v0}, Lizj;->getType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Liph;->q(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 504
    iget-object v3, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v3, v0}, Lffp;->l(Lizj;)V

    goto :goto_0

    .line 509
    :cond_0
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v2

    .line 517
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Liph;->bl(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 519
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    if-le v3, v6, :cond_1

    .line 520
    new-instance v3, Lffi;

    invoke-direct {v3, v0}, Lffi;-><init>(Ljava/util/Collection;)V

    .line 521
    invoke-interface {v3}, Lfey;->ayZ()Lfgb;

    move-result-object v0

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Liph;->bk(Ljava/lang/Object;)Ljava/util/Collection;

    .line 525
    :cond_1
    invoke-interface {v1}, Liph;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lizj;

    .line 526
    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mEntryNotificationFactory:Lfjs;

    invoke-interface {v0, v1}, Lfjs;->r(Lizj;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfey;

    .line 527
    if-eqz v0, :cond_3

    .line 528
    invoke-interface {v0}, Lfey;->ayZ()Lfgb;

    move-result-object v1

    .line 529
    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 530
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 536
    :cond_3
    const-string v0, "NotificationRefreshService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to get an EntryNotification for entry of type "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lizj;->getType()I

    move-result v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 543
    :cond_4
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 544
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfgb;

    .line 545
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfey;

    .line 547
    iget-object v3, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNowNotificationManager:Lfga;

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v0}, Lfey;->ayP()Ljava/util/Collection;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->a(Landroid/content/Context;Ljava/util/Collection;Lfgb;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-interface {v3, v0, v4, v6}, Lfga;->a(Lfey;Landroid/app/PendingIntent;Z)Landroid/app/Notification;

    move-result-object v3

    .line 552
    if-nez v3, :cond_5

    .line 555
    const-string v0, "NotificationRefreshService"

    const-string v1, "createNotification surprisingly returned null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 559
    :cond_5
    iget-object v4, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNowNotificationManager:Lfga;

    invoke-interface {v4, v1}, Lfga;->a(Lfgb;)V

    .line 560
    iget-object v1, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNowNotificationManager:Lfga;

    invoke-interface {v1, v3, v0}, Lfga;->a(Landroid/app/Notification;Lfey;)V

    .line 562
    invoke-interface {v0}, Lfey;->ayR()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 563
    invoke-interface {v0}, Lfey;->ayP()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 564
    iget-object v3, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNowNotificationManager:Lfga;

    invoke-interface {v3, v0}, Lfga;->p(Lizj;)V

    goto :goto_3

    .line 569
    :cond_6
    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNowNotificationManager:Lfga;

    invoke-interface {v0}, Lfga;->azA()V

    goto :goto_2

    .line 573
    :cond_7
    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azk()V

    .line 577
    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->awG()V

    .line 578
    return-void
.end method

.method private azk()V
    .locals 6

    .prologue
    .line 615
    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v0}, Lffp;->azp()Ljava/lang/Long;

    move-result-object v0

    .line 616
    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azl()Landroid/app/PendingIntent;

    move-result-object v1

    .line 618
    if-nez v0, :cond_0

    .line 622
    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mAlarmHelper:Ldjx;

    invoke-virtual {v0, v1}, Ldjx;->cancel(Landroid/app/PendingIntent;)V

    .line 630
    :goto_0
    return-void

    .line 628
    :cond_0
    iget-object v2, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mAlarmHelper:Ldjx;

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5, v1}, Ldjx;->setExact(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private azl()Landroid/app/PendingIntent;
    .locals 5

    .prologue
    .line 633
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.notifications.EXPIRE_NOTIFICATIONS"

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 635
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;Ljava/util/Collection;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 671
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 672
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 674
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.sidekick.notifications.NOTIFICATION_DELETE_ACTION"

    const/4 v2, 0x0

    const-class v3, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;

    invoke-direct {v0, v1, v2, p0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 676
    const-string v1, "notification_entries"

    invoke-static {v0, v1, p1}, Lgbm;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    .line 678
    return-object v0

    .line 672
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(IJ)V
    .locals 2

    .prologue
    .line 486
    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mAlarmHelper:Ldjx;

    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azi()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, p1, p2, p3, v1}, Ldjx;->setExact(IJLandroid/app/PendingIntent;)V

    .line 487
    return-void
.end method

.method private h(Lizj;)V
    .locals 2

    .prologue
    .line 779
    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mEntryNotificationFactory:Lfjs;

    invoke-interface {v0, p1}, Lfjs;->r(Lizj;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfey;

    .line 780
    if-eqz v0, :cond_0

    .line 784
    iget-object v1, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNowNotificationManager:Lfga;

    invoke-interface {v0}, Lfey;->ayZ()Lfgb;

    move-result-object v0

    invoke-interface {v1, v0}, Lfga;->a(Lfgb;)V

    .line 788
    :goto_0
    return-void

    .line 786
    :cond_0
    const-string v0, "NotificationRefreshService"

    const-string v1, "unable to find the notification!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private u(Ljava/util/Collection;)Ljava/util/Set;
    .locals 4

    .prologue
    .line 604
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 605
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 606
    iget-object v3, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mEntryNotificationFactory:Lfjs;

    invoke-interface {v3, v0}, Lfjs;->r(Lizj;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfey;

    .line 607
    if-eqz v0, :cond_0

    .line 608
    invoke-interface {v0}, Lfey;->ayZ()Lfgb;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 611
    :cond_1
    return-object v1
.end method


# virtual methods
.method public onCreate()V
    .locals 3

    .prologue
    .line 195
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 197
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 198
    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v1

    .line 199
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    .line 200
    invoke-virtual {v0}, Lcfo;->DC()Lemp;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mClock:Lemp;

    .line 201
    invoke-virtual {v0}, Lcfo;->DF()Lcin;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNowOptInSettings:Lcin;

    .line 203
    invoke-virtual {v1}, Lfdb;->axI()Lfcx;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNetworkClient:Lfcx;

    .line 204
    invoke-virtual {v1}, Lfdb;->axO()Lfga;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNowNotificationManager:Lfga;

    .line 205
    iget-object v2, v1, Lfdb;->mNotificationStore:Lffp;

    iput-object v2, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    .line 206
    invoke-virtual {v0}, Lcfo;->Ea()Ldjx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mAlarmHelper:Ldjx;

    .line 207
    invoke-virtual {v1}, Lfdb;->axR()Lfjs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mEntryNotificationFactory:Lfjs;

    .line 208
    invoke-virtual {v1}, Lfdb;->axX()Leuc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->ckD:Leuc;

    .line 209
    invoke-virtual {v1}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mEntryProvider:Lfaq;

    .line 210
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 216
    if-nez p1, :cond_1

    .line 304
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 221
    sget-object v0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->cqh:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNowOptInSettings:Lcin;

    invoke-interface {v0}, Lcin;->Kz()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.google.android.apps.sidekick.notifications.SHUTDOWN"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    :cond_2
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 237
    const-string v4, "NotificationRefresh_wakelock"

    invoke-virtual {v0, v5, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v7

    .line 242
    :try_start_0
    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 244
    const-string v0, "com.google.android.apps.sidekick.notifications.SCHEDULE_REFRESH"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 246
    const-string v0, "com.google.android.apps.sidekick.notifications.NEXT_REFRESH"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 247
    const-string v0, "com.google.android.apps.sidekick.notifications.NEXT_REFRESH"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 248
    new-instance v1, Lizo;

    invoke-direct {v1}, Lizo;-><init>()V

    invoke-static {v1, v0}, Leqh;->b(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Lizo;

    .line 249
    const-string v1, "com.google.android.apps.sidekick.notifications.EVENT_ID"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v6

    .line 250
    if-eqz v0, :cond_4

    .line 255
    iget-object v8, v0, Lizo;->dUS:[Lizp;

    array-length v9, v8

    move v0, v2

    :goto_1
    if-ge v0, v9, :cond_4

    aget-object v2, v8, v0

    iget-object v4, v2, Lizp;->afA:Ljbj;

    invoke-virtual {v4}, Ljbj;->nK()I

    move-result v1

    if-ne v1, v10, :cond_3

    iget-object v1, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v2}, Lizp;->bdo()J

    move-result-wide v2

    const/4 v5, 0x0

    invoke-virtual/range {v1 .. v6}, Lffp;->a(JLjbj;Z[B)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 260
    :cond_4
    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azh()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    :cond_5
    :goto_2
    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    .line 261
    :cond_6
    :try_start_1
    const-string v0, "com.google.android.apps.sidekick.notifications.REFRESH"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 262
    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v0}, Lffp;->azs()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->R(Ljava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 303
    :catchall_0
    move-exception v0

    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0

    .line 263
    :cond_7
    :try_start_2
    const-string v0, "com.google.android.apps.sidekick.notifications.SHOW_NOTIFICATIONS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 264
    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azj()V

    goto :goto_2

    .line 265
    :cond_8
    const-string v0, "com.google.android.apps.sidekick.notifications.INITIALIZE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 268
    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v0}, Lffp;->azn()V

    .line 270
    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v0}, Lffp;->azu()V

    .line 272
    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azj()V

    .line 274
    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azh()V

    .line 276
    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->awG()V

    .line 278
    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azk()V

    goto :goto_2

    .line 279
    :cond_9
    const-string v0, "com.google.android.apps.sidekick.notifications.EXPIRE_NOTIFICATIONS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 280
    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v0}, Lffp;->azq()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v1}, Lffp;->azr()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->u(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v1}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->u(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1}, Liqs;->b(Ljava/util/Set;Ljava/util/Set;)Liqx;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfgb;

    iget-object v2, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNowNotificationManager:Lfga;

    invoke-interface {v2, v0}, Lfga;->a(Lfgb;)V

    goto :goto_3

    :cond_a
    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azk()V

    goto :goto_2

    .line 281
    :cond_b
    const-string v0, "com.google.android.apps.sidekick.notifications.NOTIFICATION_DISMISS_ACTION"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 282
    const-string v0, "notification_entries"

    invoke-static {p1, v0}, Lgbm;->d(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    iget-object v2, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v2, v0}, Lffp;->n(Lizj;)V

    goto :goto_4

    .line 283
    :cond_c
    const-string v0, "com.google.android.apps.sidekick.notifications.NOTIFICATION_DELETE_ACTION"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 284
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const-string v0, "notification_entries"

    invoke-static {p1, v0}, Lgbm;->d(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_d
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    iget-object v4, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v4, v0}, Lffp;->i(Lizj;)Lizj;

    move-result-object v4

    if-eqz v4, :cond_d

    iget-object v5, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v5, v0}, Lffp;->o(Lizj;)Z

    move-result v5

    if-eqz v5, :cond_d

    invoke-direct {p0, v4}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->h(Lizj;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_e
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "actions_to_execute"

    const/4 v3, -0x1

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v1, :cond_10

    new-instance v1, Lewk;

    iget-object v3, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNetworkClient:Lfcx;

    iget-object v4, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mClock:Lemp;

    invoke-direct {v1, v3, v2, v0, v4}, Lewk;-><init>(Lfcx;Ljava/util/Collection;ILemp;)V

    const-string v0, "invalidate_after_action"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mEntryProvider:Lfaq;

    iget-object v2, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->ckD:Leuc;

    invoke-virtual {v1, v0, v2}, Lewk;->a(Lfaq;Leuc;)V

    :cond_f
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lewk;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_10
    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azk()V

    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->awG()V

    goto/16 :goto_2

    .line 285
    :cond_11
    const-string v0, "com.google.android.apps.sidekick.notifications.NOTIFICATION_ADD_ACTION"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 286
    const-string v0, "notification_entries"

    invoke-static {p1, v0}, Lgbm;->d(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    new-instance v2, Ljbj;

    invoke-direct {v2}, Ljbj;-><init>()V

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljbj;->om(I)Ljbj;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    invoke-virtual {v0}, Lizj;->getType()I

    move-result v5

    aput v5, v3, v4

    iput-object v3, v2, Ljbj;->dYx:[I

    iget-object v3, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v3, v0, v2}, Lffp;->a(Lizj;Ljbj;)V

    goto :goto_6

    :cond_12
    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->awG()V

    goto/16 :goto_2

    .line 287
    :cond_13
    const-string v0, "com.google.android.apps.sidekick.notifications.NOTIFICATION_TRIGGER_ACTION"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 288
    const-string v0, "triggered_notification_entries"

    invoke-static {p1, v0}, Lgbm;->d(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    const-string v0, "concluded_notification_entries"

    invoke-static {p1, v0}, Lgbm;->d(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_14

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    invoke-direct {p0, v0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->h(Lizj;)V

    iget-object v3, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v3, v0}, Lffp;->m(Lizj;)V

    goto :goto_7

    :cond_14
    if-eqz v1, :cond_5

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azj()V

    goto/16 :goto_2

    .line 289
    :cond_15
    const-string v0, "com.google.android.apps.sidekick.notifications.NOTIFICATION_GEOFENCE_ACTION"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 290
    invoke-static {p1}, Lbrj;->j(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "com.google.android.location.intent.extra.transition"

    const/4 v3, -0x1

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v1, :cond_17

    if-eq v0, v5, :cond_16

    const/4 v3, 0x2

    if-eq v0, v3, :cond_16

    const/4 v3, 0x4

    if-ne v0, v3, :cond_17

    :cond_16
    :goto_8
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_2

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v0, v2}, Lffp;->T(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    invoke-direct {p0, v0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->h(Lizj;)V

    goto :goto_9

    :cond_17
    move v0, v1

    goto :goto_8

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v0, v2}, Lffp;->S(Ljava/util/List;)V

    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azj()V

    goto/16 :goto_2

    .line 291
    :cond_18
    const-string v0, "com.google.android.apps.sidekick.notifications.REFRESH_ALL_NOTIFICATIONS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 292
    const-string v0, "com.google.android.apps.sidekick.notifications.EVENT_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    .line 293
    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v0}, Lffp;->azv()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljbj;

    new-instance v4, Lffv;

    invoke-direct {v4, v0, v1}, Lffv;-><init>(Ljbj;[B)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_19
    invoke-direct {p0, v2}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->R(Ljava/util/List;)V

    goto/16 :goto_2

    .line 294
    :cond_1a
    const-string v0, "com.google.android.apps.sidekick.notifications.RESET_ALARMS_ACTION"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 295
    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azk()V

    .line 296
    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->awG()V

    .line 297
    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azh()V

    goto/16 :goto_2

    .line 298
    :cond_1b
    const-string v0, "com.google.android.apps.sidekick.notifications.SHUTDOWN"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 299
    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mAlarmHelper:Ldjx;

    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azg()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldjx;->cancel(Landroid/app/PendingIntent;)V

    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mAlarmHelper:Ldjx;

    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azi()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldjx;->cancel(Landroid/app/PendingIntent;)V

    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mAlarmHelper:Ldjx;

    invoke-direct {p0}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->azl()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldjx;->cancel(Landroid/app/PendingIntent;)V

    .line 300
    iget-object v0, p0, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->mNotificationStore:Lffp;

    invoke-virtual {v0}, Lffp;->clearAll()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 290
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
