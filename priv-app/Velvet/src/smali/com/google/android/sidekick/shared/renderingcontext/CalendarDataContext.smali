.class public Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final cyp:Ljava/lang/String;


# instance fields
.field private final cyr:Ljava/util/Map;

.field private cys:J

.field private final dK:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->cyp:Ljava/lang/String;

    .line 87
    new-instance v0, Lfpe;

    invoke-direct {v0}, Lfpe;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->dK:Ljava/lang/Object;

    .line 29
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->cyr:Ljava/util/Map;

    .line 32
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->cys:J

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->dK:Ljava/lang/Object;

    .line 29
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->cyr:Ljava/util/Map;

    .line 32
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->cys:J

    .line 102
    invoke-direct {p0, p1}, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->readFromParcel(Landroid/os/Parcel;)V

    .line 103
    return-void
.end method

.method public static g(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 24
    sget-object v0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->cyp:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->lT(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;

    return-object v0
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 107
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 109
    const-class v0, Lamk;

    invoke-static {p1, v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lamk;

    .line 111
    iget-object v4, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->cyr:Ljava/util/Map;

    invoke-interface {v4, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 113
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->cys:J

    .line 114
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lamk;)V
    .locals 2

    .prologue
    .line 42
    iget-object v1, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 43
    :try_start_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->cyr:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final bn(J)V
    .locals 3

    .prologue
    .line 60
    iget-object v1, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 61
    :try_start_0
    iput-wide p1, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->cys:J

    .line 62
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return v0
.end method

.method public final lV(Ljava/lang/String;)Lamk;
    .locals 2

    .prologue
    .line 51
    iget-object v1, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 52
    :try_start_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->cyr:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamk;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final nt()J
    .locals 4

    .prologue
    .line 69
    iget-object v1, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 70
    :try_start_0
    iget-wide v2, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->cys:J

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v2

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    .line 76
    iget-object v2, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->dK:Ljava/lang/Object;

    monitor-enter v2

    .line 78
    :try_start_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->cyr:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 79
    iget-object v0, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->cyr:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 80
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 81
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 83
    :cond_0
    :try_start_1
    iget-wide v0, p0, Lcom/google/android/sidekick/shared/renderingcontext/CalendarDataContext;->cys:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 84
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
