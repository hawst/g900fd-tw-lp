.class public Lcom/google/android/sidekick/shared/ui/qp/StatusBadgeTextView;
.super Landroid/widget/TextView;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/sidekick/shared/ui/qp/StatusBadgeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 19
    const v0, 0x7f0100da

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/sidekick/shared/ui/qp/StatusBadgeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    return-void
.end method


# virtual methods
.method public setBackgroundColor(I)V
    .locals 2

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/qp/StatusBadgeTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0141

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 30
    new-instance v1, Lfxu;

    invoke-direct {v1, p1, v0}, Lfxu;-><init>(II)V

    .line 31
    const/16 v0, 0xf

    invoke-virtual {v1, v0}, Lfxu;->hi(I)V

    .line 32
    invoke-virtual {p0, v1}, Lcom/google/android/sidekick/shared/ui/qp/StatusBadgeTextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 33
    return-void
.end method
