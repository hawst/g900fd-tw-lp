.class public Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private aZF:Ljava/lang/String;

.field private bLI:Ljava/lang/String;

.field private cCs:I

.field private cEL:Ljhe;

.field private cEM:Ljhe;

.field private cEN:Ljava/lang/String;

.field private cEO:Ljava/util/List;

.field private cEP:Lizq;

.field private cEQ:Landroid/graphics/Point;

.field private cER:I

.field private cES:I

.field private cET:Landroid/graphics/Point;

.field private cEU:Z

.field private cEV:Z

.field private cEW:Z

.field private cZ:I

.field private ceJ:Ljava/lang/String;

.field private cqs:Ljbj;

.field private mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 511
    new-instance v0, Lgbj;

    invoke-direct {v0}, Lgbj;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEO:Ljava/util/List;

    .line 218
    iput-boolean v1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEU:Z

    .line 219
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEV:Z

    .line 221
    iput-boolean v1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEW:Z

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/view/View;[I)Landroid/graphics/Point;
    .locals 5

    .prologue
    .line 373
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 374
    invoke-virtual {p1, p2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 375
    const/4 v1, 0x0

    aget v1, p2, v1

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 376
    const/4 v1, 0x1

    aget v1, p2, v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 379
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "status_bar_height"

    const-string v3, "dimen"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 381
    if-lez v1, :cond_0

    .line 382
    iget v2, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int v1, v2, v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 385
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/View;Lizq;Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 6
    .param p1    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 337
    new-instance v0, Lizq;

    invoke-direct {v0}, Lizq;-><init>()V

    .line 338
    const/4 v1, 0x1

    new-array v1, v1, [Lizq;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    iput-object v1, v0, Lizq;->dUW:[Lizq;

    .line 339
    const/4 v1, 0x2

    new-array v1, v1, [I

    if-eqz p1, :cond_0

    invoke-static {p4, p1, v1}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->a(Landroid/content/Context;Landroid/view/View;[I)Landroid/graphics/Point;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEQ:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cER:I

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cES:I

    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget v2, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cER:I

    iget v4, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v4

    iget v4, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v4

    iput v2, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cER:I

    iget v2, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cES:I

    iget v4, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v4

    iget v4, v3, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v2, v4

    iput v2, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cES:I

    iget-object v2, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEQ:Landroid/graphics/Point;

    iget v4, v2, Landroid/graphics/Point;->x:I

    iget v5, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v5

    iput v4, v2, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEQ:Landroid/graphics/Point;

    iget v4, v2, Landroid/graphics/Point;->y:I

    iget v3, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Point;->y:I

    :cond_0
    if-eqz p2, :cond_1

    invoke-static {p4, p2, v1}, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->a(Landroid/content/Context;Landroid/view/View;[I)Landroid/graphics/Point;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cET:Landroid/graphics/Point;

    :cond_1
    iput-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEP:Lizq;

    iput-object p5, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    return-object p0
.end method

.method public final a(Ljbj;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cqs:Ljbj;

    .line 238
    return-object p0
.end method

.method public final a(Ljhe;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEL:Ljhe;

    .line 256
    return-object p0
.end method

.method public final aDA()I
    .locals 1

    .prologue
    .line 472
    iget v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cCs:I

    return v0
.end method

.method public final aEg()Z
    .locals 1

    .prologue
    .line 229
    iget v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cZ:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aEh()Z
    .locals 1

    .prologue
    .line 233
    iget v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cZ:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aEi()Ljbj;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cqs:Ljbj;

    return-object v0
.end method

.method public final aEj()Ljhe;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEL:Ljhe;

    return-object v0
.end method

.method public final aEk()Ljhe;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 294
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEM:Ljhe;

    return-object v0
.end method

.method public final aEl()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEN:Ljava/lang/String;

    return-object v0
.end method

.method public final aEm()Ljava/util/List;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEO:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final aEn()Lizq;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEP:Lizq;

    return-object v0
.end method

.method public final aEo()Landroid/graphics/Point;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 403
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEQ:Landroid/graphics/Point;

    return-object v0
.end method

.method public final aEp()I
    .locals 1

    .prologue
    .line 412
    iget v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cER:I

    return v0
.end method

.method public final aEq()I
    .locals 1

    .prologue
    .line 421
    iget v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cES:I

    return v0
.end method

.method public final aEr()Landroid/graphics/Point;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cET:Landroid/graphics/Point;

    return-object v0
.end method

.method public final aEs()Z
    .locals 1

    .prologue
    .line 451
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEU:Z

    return v0
.end method

.method public final aEt()Z
    .locals 1

    .prologue
    .line 463
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEV:Z

    return v0
.end method

.method public final aEu()Z
    .locals 1

    .prologue
    .line 476
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEW:Z

    return v0
.end method

.method public final ad(Ljava/util/List;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 1

    .prologue
    .line 311
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 312
    invoke-static {p1}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEO:Ljava/util/List;

    .line 313
    return-object p0

    .line 311
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljcn;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 1

    .prologue
    .line 317
    if-eqz p1, :cond_0

    .line 318
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEO:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 320
    :cond_0
    return-object p0
.end method

.method public final b(Ljhe;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEM:Ljhe;

    .line 290
    return-object p0
.end method

.method public final c(Landroid/graphics/Point;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 0

    .prologue
    .line 398
    iput-object p1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEQ:Landroid/graphics/Point;

    .line 399
    return-object p0
.end method

.method public final d(Landroid/graphics/Point;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 0

    .prologue
    .line 425
    iput-object p1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cET:Landroid/graphics/Point;

    .line 426
    return-object p0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 508
    const/4 v0, 0x0

    return v0
.end method

.method public final fE(Z)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 0

    .prologue
    .line 443
    iput-boolean p1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEU:Z

    .line 444
    return-object p0
.end method

.method public final fF(Z)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 0

    .prologue
    .line 458
    iput-boolean p1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEV:Z

    .line 459
    return-object p0
.end method

.method public final fG(Z)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 0

    .prologue
    .line 480
    iput-boolean p1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEW:Z

    .line 481
    return-object p0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->bLI:Ljava/lang/String;

    return-object v0
.end method

.method public final getUrl()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->aZF:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Lizq;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEP:Lizq;

    .line 390
    return-object p0
.end method

.method public final jS(I)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 0

    .prologue
    .line 224
    iput p1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cZ:I

    .line 225
    return-object p0
.end method

.method public final jT(I)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 0

    .prologue
    .line 407
    iput p1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cER:I

    .line 408
    return-object p0
.end method

.method public final jU(I)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 0

    .prologue
    .line 416
    iput p1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cES:I

    .line 417
    return-object p0
.end method

.method public final jV(I)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 0

    .prologue
    .line 467
    iput p1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cCs:I

    .line 468
    return-object p0
.end method

.method public final mn(Ljava/lang/String;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->bLI:Ljava/lang/String;

    .line 247
    return-object p0
.end method

.method public final mo(Ljava/lang/String;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->ceJ:Ljava/lang/String;

    .line 265
    return-object p0
.end method

.method public final mp(Ljava/lang/String;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->aZF:Ljava/lang/String;

    .line 277
    return-object p0
.end method

.method public final mq(Ljava/lang/String;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 0

    .prologue
    .line 298
    iput-object p1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEN:Ljava/lang/String;

    .line 299
    return-object p0
.end method

.method public final ol()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->ceJ:Ljava/lang/String;

    return-object v0
.end method

.method public final r(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;
    .locals 0

    .prologue
    .line 434
    iput-object p1, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    .line 435
    return-object p0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 486
    iget v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cZ:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 487
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cqs:Ljbj;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 488
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->bLI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 489
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEL:Ljhe;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 490
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->ceJ:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 491
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEM:Ljhe;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 492
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->aZF:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 493
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEN:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 494
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEO:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljava/util/List;Landroid/os/Parcel;)V

    .line 495
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEP:Lizq;

    invoke-static {v0, p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->a(Ljsr;Landroid/os/Parcel;)V

    .line 496
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEQ:Landroid/graphics/Point;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 497
    iget v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cER:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 498
    iget v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cES:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 499
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cET:Landroid/graphics/Point;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 500
    iget-object v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 501
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEU:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 502
    iget v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cCs:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 503
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/util/SecondScreenUtil$Options;->cEW:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 504
    return-void

    :cond_0
    move v0, v2

    .line 501
    goto :goto_0

    :cond_1
    move v1, v2

    .line 503
    goto :goto_1
.end method
