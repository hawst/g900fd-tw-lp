.class public Lcom/google/android/sidekick/shared/ui/PageIndicatorView;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private cAo:Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;

.field private cAp:I

.field private cAq:I

.field private czY:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method

.method private aCT()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 61
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->czY:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0519

    new-array v3, v7, [Ljava/lang/Object;

    iget v4, p0, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->cAp:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    iget v4, p0, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->cAq:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a051a

    new-array v2, v7, [Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->cAp:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget v3, p0, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->cAq:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 66
    return-void
.end method


# virtual methods
.method public final fy(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 44
    iget-object v3, p0, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->czY:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 45
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->cAo:Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->setVisibility(I)V

    .line 46
    return-void

    :cond_0
    move v0, v2

    .line 44
    goto :goto_0

    :cond_1
    move v2, v1

    .line 45
    goto :goto_1
.end method

.method public final jp(I)V
    .locals 1

    .prologue
    .line 49
    iput p1, p0, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->cAp:I

    .line 50
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->cAo:Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;

    invoke-virtual {v0, p1}, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->jp(I)V

    .line 51
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->aCT()V

    .line 52
    return-void
.end method

.method public final jq(I)V
    .locals 1

    .prologue
    .line 55
    iput p1, p0, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->cAq:I

    .line 56
    iget-object v0, p0, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->cAo:Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;

    invoke-virtual {v0, p1}, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;->jq(I)V

    .line 57
    invoke-direct {p0}, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->aCT()V

    .line 58
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 35
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 36
    const v0, 0x7f110299

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->cAo:Lcom/google/android/sidekick/shared/ui/PagerSelectionDotsView;

    .line 37
    const v0, 0x7f1100bc

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/ui/PageIndicatorView;->czY:Landroid/widget/TextView;

    .line 38
    return-void
.end method
