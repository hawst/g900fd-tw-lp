.class public Lcom/google/android/sidekick/shared/training/ClippableTextView;
.super Landroid/widget/TextView;
.source "PG"


# instance fields
.field private final cyZ:Landroid/graphics/Rect;

.field private cza:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 19
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/ClippableTextView;->cyZ:Landroid/graphics/Rect;

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/ClippableTextView;->cyZ:Landroid/graphics/Rect;

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/training/ClippableTextView;->cyZ:Landroid/graphics/Rect;

    .line 32
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/training/ClippableTextView;->cza:Z

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/ClippableTextView;->cyZ:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 60
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->draw(Landroid/graphics/Canvas;)V

    .line 61
    return-void
.end method

.method public final g(Landroid/graphics/Rect;)V
    .locals 5
    .param p1    # Landroid/graphics/Rect;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 36
    if-nez p1, :cond_1

    .line 37
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/training/ClippableTextView;->cza:Z

    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/ClippableTextView;->invalidate()V

    .line 39
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/ClippableTextView;->cyZ:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/training/ClippableTextView;->cza:Z

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/sidekick/shared/training/ClippableTextView;->cza:Z

    if-nez v0, :cond_2

    .line 43
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/ClippableTextView;->invalidate()V

    .line 44
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/ClippableTextView;->cyZ:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/sidekick/shared/training/ClippableTextView;->cza:Z

    goto :goto_0

    .line 46
    :cond_2
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/ClippableTextView;->cyZ:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/ClippableTextView;->cyZ:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->left:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/ClippableTextView;->cyZ:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/sidekick/shared/training/ClippableTextView;->cyZ:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget v3, p1, Landroid/graphics/Rect;->right:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/sidekick/shared/training/ClippableTextView;->cyZ:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/sidekick/shared/training/ClippableTextView;->invalidate(IIII)V

    .line 51
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/ClippableTextView;->cyZ:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method
