.class public Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;
.super Landroid/app/Activity;
.source "PG"


# static fields
.field public static final cjn:Ljava/util/Set;


# instance fields
.field private bLI:Ljava/lang/String;

.field public cjm:[Ljava/lang/String;

.field public volatile dz:Landroid/net/Uri;

.field protected mBgExecutor:Ljava/util/concurrent/Executor;

.field public mLoginHelper:Lcrh;

.field protected mUiExecutor:Ljava/util/concurrent/Executor;

.field mWebView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 61
    const-string v0, "mailto"

    const-string v1, "market"

    const-string v2, "tel"

    invoke-static {v0, v1, v2}, Lijp;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lijp;

    move-result-object v0

    sput-object v0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->cjn:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 209
    return-void
.end method


# virtual methods
.method public final J(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 201
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 206
    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->startActivity(Landroid/content/Intent;)V

    .line 207
    return-void
.end method

.method protected awa()V
    .locals 2

    .prologue
    .line 76
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 77
    iget-object v1, v0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->aus()Leqo;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->mUiExecutor:Ljava/util/concurrent/Executor;

    .line 78
    iget-object v1, v0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->aut()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->mBgExecutor:Ljava/util/concurrent/Executor;

    .line 79
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->mLoginHelper:Lcrh;

    .line 80
    return-void
.end method

.method public loadUrl(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 177
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 186
    :goto_0
    return-void

    .line 184
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x4

    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 66
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 67
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->requestWindowFeature(I)Z

    .line 68
    invoke-virtual {p0, v3}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->setProgressBarVisibility(Z)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->awa()V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->finish()V

    const-string v0, "GoogleServiceWebviewWrapper"

    const-string v1, "Uri required"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    :goto_0
    return-void

    .line 71
    :cond_1
    const-string v0, "webview_title"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "webview_title"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->bLI:Ljava/lang/String;

    const-string v0, "webview_url_prefixes"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->cjm:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0, v2, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v2, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->bLI:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    :cond_2
    new-instance v0, Landroid/webkit/WebView;

    invoke-direct {v0, p0, v5, v6}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    invoke-static {}, Letc;->avI()V

    iget-object v0, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    const-string v0, "enable_javascript"

    invoke-virtual {v1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string v2, "enable_zoom_controls"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    iget-object v0, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    iget-object v0, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    iget-object v0, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Leut;

    invoke-direct {v2, p0}, Leut;-><init>(Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v0, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Leur;

    invoke-direct {v2, p0}, Leur;-><init>(Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    iget-object v0, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->setContentView(Landroid/view/View;)V

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    const-string v0, "webview_service"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "webview_service"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_3
    new-instance v0, Leus;

    iget-object v2, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->mUiExecutor:Ljava/util/concurrent/Executor;

    iget-object v3, p0, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->mBgExecutor:Ljava/util/concurrent/Executor;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Leus;-><init>(Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Landroid/net/Uri;Ljava/lang/String;)V

    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Leus;->a([Ljava/lang/Object;)Lenp;

    goto/16 :goto_0

    :cond_4
    const-string v0, ""

    goto/16 :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 190
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 192
    invoke-virtual {p0}, Lcom/google/android/sidekick/main/GoogleServiceWebviewWrapper;->finish()V

    .line 193
    const/4 v0, 0x1

    .line 196
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
