.class public Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final cxD:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;


# instance fields
.field private final cxA:Landroid/os/Bundle;

.field private cxB:Landroid/location/Location;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cxC:Ljava/util/List;

.field private final dK:Ljava/lang/Object;

.field private mCurrentLocation:Landroid/location/Location;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext$1;

    invoke-direct {v0}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext$1;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxD:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    .line 194
    new-instance v0, Lfop;

    invoke-direct {v0}, Lfop;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->dK:Ljava/lang/Object;

    .line 40
    const/4 v0, 0x0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxC:Ljava/util/List;

    .line 56
    iput-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->mCurrentLocation:Landroid/location/Location;

    .line 57
    iput-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxB:Landroid/location/Location;

    .line 58
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxA:Landroid/os/Bundle;

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/location/Location;Landroid/location/Location;)V
    .locals 1
    .param p1    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->dK:Ljava/lang/Object;

    .line 40
    const/4 v0, 0x0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxC:Ljava/util/List;

    .line 63
    iput-object p1, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->mCurrentLocation:Landroid/location/Location;

    .line 64
    iput-object p2, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxB:Landroid/location/Location;

    .line 65
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxA:Landroid/os/Bundle;

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->dK:Ljava/lang/Object;

    .line 40
    const/4 v0, 0x0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxC:Ljava/util/List;

    .line 209
    const-class v0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxA:Landroid/os/Bundle;

    .line 210
    const-class v0, Landroid/location/Location;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->mCurrentLocation:Landroid/location/Location;

    .line 211
    const-class v0, Landroid/location/Location;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxB:Landroid/location/Location;

    .line 212
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxC:Ljava/util/List;

    sget-object v1, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 213
    return-void
.end method

.method static a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Landroid/os/Parcel;)V
    .locals 3
    .param p0    # Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 158
    if-nez p0, :cond_0

    .line 160
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 167
    :goto_0
    return-void

    .line 162
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 163
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 164
    :try_start_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxA:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 165
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->mCurrentLocation:Landroid/location/Location;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 166
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxB:Landroid/location/Location;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 167
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static aE(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 175
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 176
    if-eqz v0, :cond_0

    .line 177
    const-class v1, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 178
    const-class v0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;

    .line 179
    const-class v0, Landroid/location/Location;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    .line 180
    const-class v0, Landroid/location/Location;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    .line 182
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/os/Parcelable;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 127
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 128
    :try_start_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxA:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxA:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 130
    monitor-exit v1

    .line 132
    :goto_0
    return-object p2

    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxA:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aBO()Landroid/location/Location;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 72
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 73
    :try_start_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->mCurrentLocation:Landroid/location/Location;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aBP()Landroid/location/Location;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 81
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 82
    :try_start_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxB:Landroid/location/Location;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aBQ()Ljava/util/Collection;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 90
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 91
    :try_start_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxC:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    return v0
.end method

.method public final k(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 99
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 100
    :try_start_0
    iput-object p1, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->mCurrentLocation:Landroid/location/Location;

    .line 101
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final l(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 108
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 109
    :try_start_0
    iput-object p1, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxB:Landroid/location/Location;

    .line 110
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final lT(Ljava/lang/String;)Landroid/os/Parcelable;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 141
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 142
    :try_start_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxA:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final w(Ljava/util/Collection;)V
    .locals 2

    .prologue
    .line 117
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 118
    :try_start_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxC:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 119
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 186
    iget-object v1, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 187
    :try_start_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxA:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 188
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->mCurrentLocation:Landroid/location/Location;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 189
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxB:Landroid/location/Location;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 190
    iget-object v0, p0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxC:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 191
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
