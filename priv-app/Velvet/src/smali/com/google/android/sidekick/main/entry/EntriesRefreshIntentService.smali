.class public Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;
.super Lfac;
.source "PG"


# instance fields
.field private cmY:Lfja;

.field private cmZ:Lfja;

.field private cna:J

.field private cnb:Ljava/lang/String;

.field private cnc:I

.field private mAccessibilityManager:Lelo;

.field private mAsyncServices:Lema;

.field private mCastTvDetectorFactory:Livq;

.field private mConfig:Lcjs;

.field public mContentCacherFactory:Lfjr;

.field private mCoreServices:Lcfo;

.field private mEntriesRefreshScheduler:Lfak;

.field public mEntryProvider:Lfaq;

.field private mExecutedUserActionStore:Lfcr;

.field private mGmmPrecacher:Lcgj;

.field private mInteractionManager:Lfdg;

.field private mSidekickInjector:Lfdb;

.field private mTrainingQuestionManager:Lfdn;

.field private mTvDetectorFactory:Livq;

.field private mVelvetServices:Lgql;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 145
    const-string v0, "EntriesRefreshIntentService"

    invoke-direct {p0, v0}, Lfac;-><init>(Ljava/lang/String;)V

    .line 146
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfac;->cmR:Z

    .line 147
    return-void
.end method

.method private a(IILijj;Lfaf;ZZLjie;)I
    .locals 19
    .param p7    # Ljie;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 377
    invoke-static {}, Lenu;->auQ()V

    .line 378
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mEntryProvider:Lfaq;

    invoke-virtual {v2}, Lfaq;->awZ()V

    .line 379
    const/4 v2, 0x0

    .line 381
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DR()Lcpx;

    move-result-object v3

    .line 382
    const-string v4, "REFRESH"

    invoke-virtual {v3, v4}, Lcpx;->ip(Ljava/lang/String;)Lcqc;

    move-result-object v12

    .line 386
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->DE()Lcxs;

    move-result-object v4

    .line 393
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 394
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->getApplicationContext()Landroid/content/Context;

    invoke-static {v4}, Lfiy;->a(Lcxs;)Z

    move-result v6

    .line 396
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mConfig:Lcjs;

    invoke-static {v7}, Lfiy;->d(Lcjs;)Z

    move-result v7

    .line 397
    if-nez v6, :cond_0

    if-eqz v7, :cond_2e

    .line 414
    :cond_0
    if-eqz v6, :cond_2

    .line 415
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cmY:Lfja;

    if-eqz v8, :cond_1

    .line 416
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cmY:Lfja;

    invoke-interface {v8}, Lfja;->wE()V

    .line 418
    :cond_1
    new-instance v8, Lfix;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mTvDetectorFactory:Livq;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v10}, Lcfo;->DC()Lemp;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lfix;-><init>(Livq;Lemp;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cmY:Lfja;

    .line 420
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cmY:Lfja;

    new-instance v9, Lfaj;

    move-object/from16 v0, p4

    invoke-direct {v9, v0}, Lfaj;-><init>(Lfaf;)V

    invoke-interface {v8, v9}, Lfja;->a(Lfjb;)V

    .line 422
    :cond_2
    if-eqz v7, :cond_4

    .line 423
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cmZ:Lfja;

    if-eqz v8, :cond_3

    .line 424
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cmZ:Lfja;

    invoke-interface {v8}, Lfja;->wE()V

    .line 426
    :cond_3
    new-instance v8, Lfix;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCastTvDetectorFactory:Livq;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v10}, Lcfo;->DC()Lemp;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lfix;-><init>(Livq;Lemp;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cmZ:Lfja;

    .line 428
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cmZ:Lfja;

    new-instance v9, Lfaj;

    move-object/from16 v0, p4

    invoke-direct {v9, v0}, Lfaj;-><init>(Lfaf;)V

    invoke-interface {v8, v9}, Lfja;->a(Lfjb;)V

    .line 433
    :cond_4
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v8}, Lcfo;->DC()Lemp;

    move-result-object v8

    invoke-interface {v8}, Lemp;->currentTimeMillis()J

    move-result-wide v8

    .line 434
    const-string v10, "DETECT_TV"

    invoke-virtual {v3, v10}, Lcpx;->ip(Ljava/lang/String;)Lcqc;

    move-result-object v10

    .line 437
    if-eqz v6, :cond_2d

    .line 438
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cmY:Lfja;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mConfig:Lcjs;

    invoke-virtual {v3}, Lcjs;->Mj()I

    move-result v3

    int-to-long v14, v3

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const/4 v6, 0x1

    invoke-interface {v2, v14, v15, v3, v6}, Lfja;->a(JLjava/util/concurrent/TimeUnit;Z)Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 441
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mConfig:Lcjs;

    invoke-virtual {v2}, Lcjs;->Mk()I

    move-result v3

    .line 443
    :goto_0
    if-eqz v7, :cond_5

    .line 445
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->DC()Lemp;

    move-result-object v2

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v8

    .line 447
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mConfig:Lcjs;

    invoke-virtual {v2}, Lcjs;->Mj()I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v16, v0

    sub-long v6, v16, v6

    invoke-static {v14, v15, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    .line 449
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cmZ:Lfja;

    sget-object v11, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const/4 v13, 0x1

    invoke-interface {v2, v6, v7, v11, v13}, Lfja;->a(JLjava/util/concurrent/TimeUnit;Z)Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 456
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->DC()Lemp;

    move-result-object v2

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v8

    .line 458
    const-wide/16 v8, 0xc8

    cmp-long v2, v6, v8

    if-lez v2, :cond_6

    .line 459
    const-string v2, "NowTV"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Waited for "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "ms to find nearby TVs"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    :cond_6
    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "NO_DEVICES"

    :goto_1
    invoke-interface {v10, v2}, Lcqc;->iq(Ljava/lang/String;)V

    move v10, v3

    .line 470
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v2}, Lfdb;->axN()Leym;

    move-result-object v14

    .line 472
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->El()Lfdr;

    move-result-object v2

    invoke-interface {v2}, Lfdr;->ayA()Landroid/location/Location;

    move-result-object v15

    .line 473
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->DL()Lcrh;

    move-result-object v2

    .line 474
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DF()Lcin;

    move-result-object v6

    .line 475
    invoke-virtual {v2}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v7

    .line 477
    if-nez v7, :cond_9

    .line 479
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mEntryProvider:Lfaq;

    invoke-virtual {v2}, Lfaq;->invalidate()V

    .line 801
    :cond_7
    :goto_3
    return v10

    .line 462
    :cond_8
    const-string v2, "HAS_DEVICES"

    goto :goto_1

    .line 483
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mEntryProvider:Lfaq;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Lfaq;->iy(I)V

    .line 495
    const-string v2, ""

    .line 496
    invoke-virtual {v4, v7}, Lcxs;->x(Landroid/accounts/Account;)Z

    move-result v3

    if-eqz v3, :cond_2c

    .line 497
    invoke-interface {v6, v7}, Lcin;->k(Landroid/accounts/Account;)Liyl;

    move-result-object v3

    .line 498
    if-eqz v3, :cond_2c

    iget-object v8, v3, Liyl;->dQB:Ljel;

    if-eqz v8, :cond_2c

    .line 500
    iget-object v2, v3, Liyl;->dQB:Ljel;

    invoke-virtual {v2}, Ljel;->biR()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 507
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v2}, Lfdb;->axX()Leuc;

    move-result-object v2

    invoke-virtual {v2}, Leuc;->avW()Ljava/util/Collection;

    move-result-object v8

    .line 509
    new-instance v9, Liyb;

    invoke-direct {v9}, Liyb;-><init>()V

    invoke-interface {v14, v9}, Leym;->a(Liyb;)V

    if-eqz v5, :cond_a

    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v2

    new-array v2, v2, [Liza;

    invoke-interface {v5, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Liza;

    iput-object v2, v9, Liyb;->dPn:[Liza;

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mInteractionManager:Lfdg;

    invoke-virtual {v2}, Lfdg;->ayq()Z

    move-result v2

    invoke-virtual {v9, v2}, Liyb;->hm(Z)Liyb;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mEntriesRefreshScheduler:Lfak;

    invoke-virtual {v2}, Lfak;->awP()I

    move-result v2

    mul-int/lit8 v2, v2, 0x3c

    invoke-virtual {v9, v2}, Liyb;->ny(I)Liyb;

    invoke-virtual {v9, v3}, Liyb;->rk(Ljava/lang/String;)Liyb;

    invoke-interface {v8}, Ljava/util/Collection;->size()I

    move-result v2

    new-array v2, v2, [Ljbt;

    invoke-interface {v8, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljbt;

    iput-object v2, v9, Liyb;->dMd:[Ljbt;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mConfig:Lcjs;

    invoke-virtual {v2}, Lcjs;->Mt()Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Liyb;->hn(Z)Liyb;

    :cond_b
    new-instance v3, Lizm;

    invoke-direct {v3}, Lizm;-><init>()V

    iput-object v9, v3, Lizm;->dUG:Liyb;

    .line 512
    move/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->a(ILijj;)Ljava/util/Collection;

    move-result-object v2

    .line 514
    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v5

    new-array v5, v5, [Ljbj;

    invoke-interface {v2, v5}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljbj;

    iput-object v2, v3, Lizm;->dUH:[Ljbj;

    .line 517
    if-nez p2, :cond_c

    packed-switch p1, :pswitch_data_0

    const-string v2, "EntriesRefreshIntentService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "Unknown request type used: "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    :goto_5
    invoke-static/range {p2 .. p2}, Lfjw;->iQ(I)Ljed;

    move-result-object v5

    .line 519
    iput-object v3, v5, Ljed;->edr:Lizm;

    .line 523
    new-instance v3, Ljee;

    invoke-direct {v3}, Ljee;-><init>()V

    invoke-static/range {p1 .. p1}, Lflk;->iS(I)Z

    move-result v2

    if-eqz v2, :cond_19

    const/4 v2, 0x1

    :goto_6
    invoke-virtual {v3, v2}, Ljee;->ph(I)Ljee;

    move-result-object v3

    .line 529
    if-nez p7, :cond_e

    invoke-virtual/range {p3 .. p3}, Lijj;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_e

    .line 530
    invoke-virtual/range {p3 .. p3}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_d
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfin;

    .line 531
    iget-object v9, v2, Lfin;->csS:Ljie;

    if-eqz v9, :cond_d

    .line 532
    iget-object v0, v2, Lfin;->csS:Ljie;

    move-object/from16 p7, v0

    .line 537
    :cond_e
    if-eqz p7, :cond_f

    .line 538
    move-object/from16 v0, p7

    iput-object v0, v3, Ljee;->dRY:Ljie;

    .line 540
    :cond_f
    const/4 v2, 0x1

    new-array v2, v2, [Ljee;

    const/4 v8, 0x0

    aput-object v3, v2, v8

    iput-object v2, v5, Ljed;->edM:[Ljee;

    .line 542
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mTrainingQuestionManager:Lfdn;

    invoke-interface {v2}, Lfdn;->ayu()Ljava/lang/Iterable;

    move-result-object v16

    .line 544
    invoke-interface/range {v16 .. v16}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 545
    invoke-static/range {v16 .. v16}, Lfqm;->k(Ljava/lang/Iterable;)Liwl;

    move-result-object v2

    .line 547
    iput-object v2, v5, Ljed;->eds:Liwl;

    .line 550
    :cond_10
    new-instance v2, Ljhs;

    invoke-direct {v2}, Ljhs;-><init>()V

    .line 551
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mTrainingQuestionManager:Lfdn;

    invoke-interface {v3}, Lfdn;->ayv()Ljhu;

    move-result-object v3

    iput-object v3, v2, Ljhs;->emi:Ljhu;

    .line 552
    iput-object v2, v5, Ljed;->edC:Ljhs;

    .line 554
    if-eqz p5, :cond_11

    .line 556
    const/4 v2, 0x1

    invoke-virtual {v5, v2}, Ljed;->hM(Z)Ljed;

    .line 559
    :cond_11
    invoke-virtual {v4, v7}, Lcxs;->x(Landroid/accounts/Account;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 564
    invoke-virtual {v4, v7}, Lcxs;->y(Landroid/accounts/Account;)Ljgu;

    move-result-object v2

    .line 565
    if-eqz v2, :cond_12

    .line 566
    iput-object v2, v5, Ljed;->edx:Ljgu;

    .line 571
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->aJs()Lchr;

    move-result-object v2

    invoke-virtual {v2}, Lchr;->Kt()Lcyg;

    move-result-object v2

    .line 573
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a013d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 576
    if-eqz v2, :cond_13

    .line 577
    const/4 v2, 0x1

    invoke-virtual {v5, v2}, Ljed;->hL(Z)Ljed;

    .line 582
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v2}, Lfdb;->axG()Lfcl;

    move-result-object v2

    invoke-virtual {v2, v7}, Lfcl;->E(Landroid/accounts/Account;)Lfcm;

    move-result-object v3

    .line 584
    if-eqz v3, :cond_14

    .line 585
    new-instance v2, Ljdd;

    invoke-direct {v2}, Ljdd;-><init>()V

    iget-object v4, v3, Lfcm;->coI:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljdd;->tw(Ljava/lang/String;)Ljdd;

    move-result-object v2

    invoke-static {v7}, Lfcn;->G(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljdd;->tx(Ljava/lang/String;)Ljdd;

    move-result-object v2

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljdd;->ty(Ljava/lang/String;)Ljdd;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljdd;->hG(Z)Ljdd;

    move-result-object v2

    iput-object v2, v5, Ljed;->edI:Ljdd;

    .line 593
    :cond_14
    invoke-static/range {p1 .. p1}, Lflk;->iS(I)Z

    move-result v2

    invoke-virtual {v5, v2}, Ljed;->hK(Z)Ljed;

    .line 600
    invoke-static/range {p1 .. p1}, Lflk;->iV(I)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 601
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mExecutedUserActionStore:Lfcr;

    invoke-interface {v2}, Lfcr;->axB()V

    .line 604
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v2}, Lfdb;->axI()Lfcx;

    move-result-object v2

    invoke-interface {v2, v5}, Lfcx;->b(Ljed;)Lfcy;

    move-result-object v17

    .line 606
    if-eqz v17, :cond_1a

    move-object/from16 v0, v17

    iget-object v2, v0, Lfcy;->coT:Ljeh;

    move-object v13, v2

    .line 613
    :goto_7
    if-eqz v13, :cond_17

    .line 614
    if-eqz v3, :cond_16

    .line 615
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v2}, Lfdb;->axG()Lfcl;

    move-result-object v2

    invoke-virtual {v2, v7, v3}, Lfcl;->a(Landroid/accounts/Account;Lfcm;)V

    .line 618
    :cond_16
    iget-object v2, v13, Ljeh;->aeC:Lizn;

    if-eqz v2, :cond_17

    .line 619
    iget-object v2, v13, Ljeh;->aeC:Lizn;

    invoke-virtual {v2}, Lizn;->bdj()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 620
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v2}, Lfdb;->axG()Lfcl;

    move-result-object v2

    invoke-virtual {v2, v7}, Lfcl;->F(Landroid/accounts/Account;)V

    .line 625
    :cond_17
    if-eqz v13, :cond_18

    iget-object v2, v13, Ljeh;->aeC:Lizn;

    if-eqz v2, :cond_18

    invoke-static {v13}, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->d(Ljeh;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 627
    :cond_18
    const-string v2, "EntriesRefreshIntentService"

    const-string v3, "Error sending request to the server"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mEntryProvider:Lfaq;

    invoke-static {v13}, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->d(Ljeh;)Z

    move-result v3

    move/from16 v0, p1

    invoke-virtual {v2, v0, v3}, Lfaq;->D(IZ)V

    .line 631
    const/4 v2, 0x0

    .line 799
    :goto_8
    if-eqz v2, :cond_7

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcqc;->iq(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 517
    :pswitch_0
    const/16 p2, 0x16

    goto/16 :goto_5

    :pswitch_1
    const/16 p2, 0x17

    goto/16 :goto_5

    :pswitch_2
    const/16 p2, 0x13

    goto/16 :goto_5

    :pswitch_3
    const/16 p2, 0x14

    goto/16 :goto_5

    :pswitch_4
    const/16 p2, 0x15

    goto/16 :goto_5

    .line 523
    :cond_19
    const/4 v2, 0x2

    goto/16 :goto_6

    .line 606
    :cond_1a
    const/4 v2, 0x0

    move-object v13, v2

    goto :goto_7

    .line 636
    :cond_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->DR()Lcpx;

    move-result-object v2

    const-string v3, "REFRESH_SUCCESS"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcpx;->Y(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    iget-object v0, v13, Ljeh;->aeC:Lizn;

    move-object/from16 v18, v0

    .line 642
    invoke-static/range {v18 .. v18}, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->c(Lizn;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 643
    const-string v2, "EntriesRefreshIntentService"

    const-string v3, "Partial entry source failure from the server"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mExecutedUserActionStore:Lfcr;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lfcr;->fk(Z)V

    .line 652
    const/4 v8, 0x0

    .line 653
    move-object/from16 v0, v18

    iget-object v2, v0, Lizn;->dUJ:Liyl;

    if-eqz v2, :cond_1e

    .line 654
    move-object/from16 v0, v18

    iget-object v3, v0, Lizn;->dUJ:Liyl;

    .line 655
    iget-object v2, v3, Liyl;->dQB:Ljel;

    if-eqz v2, :cond_20

    const/4 v2, 0x1

    move v8, v2

    .line 660
    :goto_9
    const/4 v2, 0x0

    invoke-interface {v6, v7, v3, v2}, Lcin;->a(Landroid/accounts/Account;Liyl;Lcom/google/android/gms/location/reporting/ReportingState;)Z

    move-result v2

    .line 662
    const/4 v4, 0x0

    invoke-interface {v6, v3, v7, v4}, Lcin;->a(Liyl;Landroid/accounts/Account;Lcom/google/android/gms/location/reporting/ReportingState;)V

    .line 665
    if-eqz v2, :cond_1d

    iget-object v2, v3, Liyl;->dQB:Ljel;

    if-eqz v2, :cond_1e

    iget-object v2, v3, Liyl;->dQB:Ljel;

    invoke-virtual {v2}, Ljel;->biZ()Z

    move-result v2

    if-eqz v2, :cond_1e

    iget-object v2, v3, Liyl;->dQB:Ljel;

    invoke-virtual {v2}, Ljel;->biY()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 668
    :cond_1d
    invoke-interface {v6, v3, v7}, Lcin;->a(Liyl;Landroid/accounts/Account;)Z

    move-result v5

    .line 670
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mAsyncServices:Lema;

    invoke-virtual {v2}, Lema;->aus()Leqo;

    move-result-object v9

    new-instance v2, Lfag;

    const-string v4, "Notify about Google Now disabled"

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lfag;-><init>(Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;Ljava/lang/String;ZLcin;Landroid/accounts/Account;)V

    invoke-interface {v9, v2}, Leqo;->execute(Ljava/lang/Runnable;)V

    :cond_1e
    move v11, v8

    .line 691
    invoke-static/range {p1 .. p1}, Lflk;->iU(I)Z

    move-result v3

    .line 693
    if-eqz v3, :cond_21

    const/4 v2, 0x0

    .line 694
    :goto_a
    if-eqz v2, :cond_1f

    move-object/from16 v0, v18

    iget-object v4, v0, Lizn;->dUI:[Lizo;

    array-length v4, v4

    if-lez v4, :cond_1f

    move-object/from16 v0, v18

    iget-object v4, v0, Lizn;->dUI:[Lizo;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-object v4, v4, Lizo;->dUQ:Lizq;

    if-eqz v4, :cond_1f

    .line 697
    move-object/from16 v0, v18

    iget-object v4, v0, Lizn;->dUI:[Lizo;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    .line 698
    iget-object v5, v4, Lizo;->dUQ:Lizq;

    iget-object v4, v4, Lizo;->dUQ:Lizq;

    iget-object v4, v4, Lizq;->dUW:[Lizq;

    invoke-static {v4, v2}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lizq;

    iput-object v2, v5, Lizq;->dUW:[Lizq;

    .line 705
    :cond_1f
    invoke-static/range {p1 .. p1}, Lflk;->iW(I)Z

    move-result v2

    if-eqz v2, :cond_23

    if-eqz v18, :cond_23

    move-object/from16 v0, v18

    iget-object v2, v0, Lizn;->dUI:[Lizo;

    array-length v2, v2

    if-lez v2, :cond_23

    .line 707
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mEntryProvider:Lfaq;

    move-object/from16 v0, v18

    iget-object v3, v0, Lizn;->dUI:[Lizo;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lfaq;->b(Lizo;)V

    .line 719
    :goto_b
    move-object/from16 v0, v18

    invoke-static {v0, v14}, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->a(Lizn;Leym;)V

    .line 721
    iget-object v2, v13, Ljeh;->eeo:Ljht;

    if-eqz v2, :cond_25

    .line 722
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mTrainingQuestionManager:Lfdn;

    iget-object v3, v13, Ljeh;->eeo:Ljht;

    move-object/from16 v0, v16

    invoke-interface {v2, v3, v0}, Lfdn;->a(Ljht;Ljava/lang/Iterable;)V

    .line 729
    :goto_c
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->b(Lizn;)V

    .line 731
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mAsyncServices:Lema;

    invoke-virtual {v2}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    new-instance v3, Lfah;

    const-string v4, "offlineCacher"

    const/4 v5, 0x0

    new-array v5, v5, [I

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v3, v0, v4, v5, v1}, Lfah;-><init>(Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;Ljava/lang/String;[ILizn;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 734
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v2}, Lfdb;->axH()Lfil;

    move-result-object v3

    .line 736
    invoke-virtual/range {p3 .. p3}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_26

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfin;

    .line 737
    invoke-virtual {v3, v2}, Lfil;->a(Lfin;)V

    goto :goto_d

    .line 655
    :cond_20
    const/4 v2, 0x0

    move v8, v2

    goto/16 :goto_9

    .line 693
    :cond_21
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    iget-object v2, v2, Lfdb;->mLocationDisabledCardHelper:Lfbq;

    invoke-virtual {v2}, Lfbq;->axm()Ljas;

    move-result-object v2

    if-nez v2, :cond_22

    const/4 v2, 0x0

    goto/16 :goto_a

    :cond_22
    new-instance v4, Lizj;

    invoke-direct {v4}, Lizj;-><init>()V

    iput-object v2, v4, Lizj;->dSh:Ljas;

    new-instance v2, Lizq;

    invoke-direct {v2}, Lizq;-><init>()V

    const/4 v5, 0x1

    new-array v5, v5, [Lizj;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    iput-object v5, v2, Lizq;->dUX:[Lizj;

    goto/16 :goto_a

    .line 709
    :cond_23
    if-eqz v3, :cond_24

    .line 710
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mEntryProvider:Lfaq;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0, v15}, Lfaq;->a(Lizn;Landroid/location/Location;)V

    goto/16 :goto_b

    .line 712
    :cond_24
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mEntryProvider:Lfaq;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    move-object/from16 v0, v17

    iget-object v8, v0, Lfcy;->cnq:[B

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mAccessibilityManager:Lelo;

    invoke-virtual {v3}, Lelo;->aum()Z

    move-result v9

    move-object/from16 v3, v18

    move/from16 v4, p1

    move-object v5, v15

    move/from16 v7, p6

    invoke-virtual/range {v2 .. v9}, Lfaq;->a(Lizn;ILandroid/location/Location;Ljava/util/Locale;Z[BZ)V

    goto/16 :goto_b

    .line 725
    :cond_25
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mTrainingQuestionManager:Lfdn;

    move-object/from16 v0, v16

    invoke-interface {v2, v0}, Lfdn;->i(Ljava/lang/Iterable;)V

    goto/16 :goto_c

    .line 741
    :cond_26
    move-object/from16 v0, v18

    iget-object v2, v0, Lizn;->dUI:[Lizo;

    array-length v2, v2

    if-lez v2, :cond_28

    move-object/from16 v0, v18

    iget-object v2, v0, Lizn;->dUI:[Lizo;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget-object v2, v2, Lizo;->dUS:[Lizp;

    array-length v2, v2

    if-lez v2, :cond_28

    .line 748
    new-instance v2, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 750
    const-string v3, "com.google.android.apps.sidekick.notifications.SCHEDULE_REFRESH"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 755
    new-instance v3, Lizo;

    invoke-direct {v3}, Lizo;-><init>()V

    .line 756
    move-object/from16 v0, v18

    iget-object v4, v0, Lizn;->dUI:[Lizo;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    .line 757
    iget-object v4, v4, Lizo;->dUS:[Lizp;

    iput-object v4, v3, Lizo;->dUS:[Lizp;

    .line 758
    const-string v4, "com.google.android.apps.sidekick.notifications.NEXT_REFRESH"

    invoke-static {v3}, Ljsr;->m(Ljsr;)[B

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 760
    move-object/from16 v0, v17

    iget-object v3, v0, Lfcy;->cnq:[B

    if-eqz v3, :cond_27

    .line 761
    const-string v3, "com.google.android.apps.sidekick.notifications.EVENT_ID"

    move-object/from16 v0, v17

    iget-object v4, v0, Lfcy;->cnq:[B

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 764
    :cond_27
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 773
    :cond_28
    if-eqz v11, :cond_2a

    .line 778
    new-instance v2, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 780
    const-string v3, "com.google.android.apps.sidekick.notifications.REFRESH_ALL_NOTIFICATIONS"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 781
    move-object/from16 v0, v17

    iget-object v3, v0, Lfcy;->cnq:[B

    if-eqz v3, :cond_29

    .line 782
    const-string v3, "com.google.android.apps.sidekick.notifications.EVENT_ID"

    move-object/from16 v0, v17

    iget-object v4, v0, Lfcy;->cnq:[B

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 785
    :cond_29
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 789
    :cond_2a
    invoke-static/range {p1 .. p1}, Lflk;->iS(I)Z

    move-result v2

    if-eqz v2, :cond_2b

    invoke-static/range {p1 .. p1}, Lflk;->iV(I)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 791
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mEntriesRefreshScheduler:Lfak;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lfak;->fi(Z)V

    :cond_2b
    move-object v2, v12

    goto/16 :goto_8

    :cond_2c
    move-object v3, v2

    goto/16 :goto_4

    :cond_2d
    move v3, v2

    goto/16 :goto_0

    :cond_2e
    move v10, v2

    goto/16 :goto_2

    .line 517
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method private static a(ILijj;)Ljava/util/Collection;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 967
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 968
    invoke-static {p0}, Lflk;->iW(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 969
    invoke-virtual {p1}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfin;

    .line 970
    iget-object v0, v0, Lfin;->cqs:Ljbj;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 972
    :cond_0
    invoke-static {p0}, Lflk;->iT(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 973
    new-instance v2, Ljbj;

    invoke-direct {v2}, Ljbj;-><init>()V

    .line 974
    invoke-virtual {v2, v0}, Ljbj;->on(I)Ljbj;

    .line 975
    invoke-static {p0}, Lflk;->iU(I)Z

    move-result v3

    if-nez v3, :cond_2

    :goto_1
    invoke-virtual {v2, v0}, Ljbj;->hC(Z)Ljbj;

    .line 977
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 981
    :cond_1
    :goto_2
    return-object v1

    .line 975
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 979
    :cond_3
    new-instance v0, Ljbj;

    invoke-direct {v0}, Ljbj;-><init>()V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private static a(Lizn;Leym;)V
    .locals 4

    .prologue
    .line 1005
    iget-object v1, p0, Lizn;->dUI:[Lizo;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1006
    invoke-interface {p1, v3}, Leym;->a(Lizo;)Z

    .line 1005
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1013
    :cond_0
    return-void
.end method

.method private static c(Lizn;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 989
    iget-object v2, p0, Lizn;->dUI:[Lizo;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 990
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lizo;->hasError()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Lizo;->aZt()I

    move-result v4

    const/16 v5, 0xc

    if-ne v4, v5, :cond_1

    .line 993
    const/4 v0, 0x1

    .line 996
    :cond_0
    return v0

    .line 989
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static d(Ljeh;)Z
    .locals 1

    .prologue
    .line 849
    sget-object v0, Lewi;->ckA:Ljeh;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/Intent;Lfaf;J)I
    .locals 11
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "Wakelock"
        }
    .end annotation

    .prologue
    .line 220
    if-nez p1, :cond_0

    .line 221
    const/4 v0, 0x0

    .line 324
    :goto_0
    return v0

    .line 223
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 225
    const-string v0, "com.google.android.apps.sidekick.REFRESH"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.google.android.apps.sidekick.PARTIAL_REFRESH"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.google.android.apps.sidekick.SCHEDULED_REFRESH"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 229
    const/4 v0, 0x0

    goto :goto_0

    .line 234
    :cond_1
    const-string v0, "com.google.android.apps.sidekick.TYPE"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 237
    const-string v0, "com.google.android.apps.sidekick.TRACE"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 239
    if-nez v2, :cond_2

    const-string v3, "EntriesRefreshIntentService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, "No request trace given for request (type="

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    packed-switch v1, :pswitch_data_0

    const-string v0, "(unknown)"

    :goto_1
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ")"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    :cond_2
    const-string v0, "com.google.android.apps.sidekick.SCHEDULED_REFRESH"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 242
    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mEntriesRefreshScheduler:Lfak;

    iget-object v0, v0, Lfak;->mAlarmHelper:Ldjx;

    const-string v3, "refresh_alarm"

    invoke-virtual {v0, v3}, Ldjx;->jK(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v0, "EntriesRefreshIntentService"

    const-string v3, "postponeRefreshIfTooEarly: alarm trigger time not found"

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_5

    .line 243
    const/4 v0, 0x0

    goto :goto_0

    .line 239
    :pswitch_0
    const-string v0, "INITIAL"

    goto :goto_1

    :pswitch_1
    const-string v0, "SCHEDULED_POLL"

    goto :goto_1

    :pswitch_2
    const-string v0, "USER_REQUESTED"

    goto :goto_1

    :pswitch_3
    const-string v0, "USER_REQUESTED_MORE"

    goto :goto_1

    :pswitch_4
    const-string v0, "USER_REQUESTED_MORE_INCREMENTAL"

    goto :goto_1

    :pswitch_5
    const-string v0, "PARTIAL_UPDATE"

    goto :goto_1

    :pswitch_6
    const-string v0, "USER_INITIATED_PARTIAL_UPDATE"

    goto :goto_1

    :pswitch_7
    const-string v0, "TRIGGERED_REFRESH"

    goto :goto_1

    .line 242
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x1388

    cmp-long v3, v6, v8

    if-lez v3, :cond_3

    const-string v3, "EntriesRefreshIntentService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Rescheduling periodic refresh which ran "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lesi;->aX(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " ahead of alarm time"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mEntriesRefreshScheduler:Lfak;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lfak;->fi(Z)V

    const/4 v0, 0x1

    goto :goto_2

    .line 247
    :cond_5
    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->axH()Lfil;

    move-result-object v0

    invoke-virtual {v0}, Lfil;->aAg()Lijj;

    move-result-object v3

    .line 249
    invoke-static {v1}, Lflk;->iW(I)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v3}, Lijj;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 251
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 254
    :cond_6
    iget-wide v6, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cna:J

    cmp-long v0, p3, v6

    if-gez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cnb:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {v1}, Lflk;->iV(I)Z

    move-result v0

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cnc:I

    invoke-static {v0}, Lflk;->iV(I)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_8

    .line 256
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 254
    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    .line 258
    :cond_8
    iput-object v4, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cnb:Ljava/lang/String;

    .line 259
    iput v1, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cnc:I

    .line 263
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 264
    const/4 v5, 0x1

    const-string v6, "EntriesRefresh_wakelock"

    invoke-virtual {v0, v5, v6}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v8

    .line 270
    :try_start_0
    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 272
    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DF()Lcin;

    move-result-object v0

    invoke-interface {v0}, Lcin;->KG()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_9

    .line 274
    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DC()Lemp;

    move-result-object v0

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cna:J

    .line 321
    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->release()V

    const/4 v0, 0x0

    goto/16 :goto_0

    .line 278
    :cond_9
    :try_start_1
    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mEntriesRefreshScheduler:Lfak;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lfak;->fi(Z)V

    .line 280
    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mEntryProvider:Lfaq;

    invoke-virtual {v0}, Lfaq;->awV()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_a

    .line 283
    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DC()Lemp;

    move-result-object v0

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cna:J

    .line 321
    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->release()V

    const/4 v0, 0x0

    goto/16 :goto_0

    .line 286
    :cond_a
    :try_start_2
    invoke-static {v1}, Lflk;->iS(I)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mEntryProvider:Lfaq;

    invoke-virtual {v0}, Lfaq;->axh()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-eqz v0, :cond_b

    .line 289
    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DC()Lemp;

    move-result-object v0

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cna:J

    .line 321
    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->release()V

    const/4 v0, 0x0

    goto/16 :goto_0

    .line 294
    :cond_b
    const/4 v0, 0x0

    .line 295
    :try_start_3
    const-string v5, "com.google.android.apps.sidekick.PARTIAL_REFRESH"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 296
    const/4 v0, 0x0

    const/4 v4, 0x0

    invoke-static {p0, v0, p1, v4}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 300
    :cond_c
    const-string v4, "reminder_updated"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 307
    iget-object v4, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->DC()Lemp;

    move-result-object v4

    invoke-interface {v4}, Lemp;->elapsedRealtime()J

    move-result-wide v4

    .line 308
    iget-object v7, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v7}, Lfdb;->axF()Lfam;

    move-result-object v7

    invoke-interface {v7, v1, v0, v4, v5}, Lfam;->a(ILandroid/app/PendingIntent;J)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 311
    const-string v0, "com.google.android.apps.sidekick.SAVE_CALL_LOG"

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 313
    const-string v0, "com.google.android.apps.sidekick.TC"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    if-nez v0, :cond_d

    const/4 v7, 0x0

    :goto_4
    move-object v0, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->a(IILijj;Lfaf;ZZLjie;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    .line 320
    iget-object v1, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DC()Lemp;

    move-result-object v1

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cna:J

    .line 321
    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_0

    .line 313
    :cond_d
    :try_start_4
    new-instance v4, Ljie;

    invoke-direct {v4}, Ljie;-><init>()V

    invoke-static {v4, v0}, Leqh;->b(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Ljie;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v7, v0

    goto :goto_4

    .line 320
    :cond_e
    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DC()Lemp;

    move-result-object v0

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cna:J

    .line 321
    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 324
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 320
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DC()Lemp;

    move-result-object v1

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cna:J

    .line 321
    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0

    .line 239
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected final a(Lgql;Lcfo;Lcjs;Lfdb;Lcgj;Livq;Livq;Lfak;Lfdg;Lema;Lfdn;Lfcr;Lfjr;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mVelvetServices:Lgql;

    .line 180
    iput-object p2, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    .line 181
    iput-object p3, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mConfig:Lcjs;

    .line 182
    iput-object p4, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    .line 183
    iput-object p5, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mGmmPrecacher:Lcgj;

    .line 184
    iput-object p6, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mTvDetectorFactory:Livq;

    .line 185
    iput-object p7, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCastTvDetectorFactory:Livq;

    .line 186
    iput-object p8, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mEntriesRefreshScheduler:Lfak;

    .line 187
    iput-object p9, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mInteractionManager:Lfdg;

    .line 188
    iput-object p10, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mAsyncServices:Lema;

    .line 189
    iput-object p11, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mTrainingQuestionManager:Lfdn;

    .line 190
    iput-object p12, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mExecutedUserActionStore:Lfcr;

    .line 191
    iput-object p13, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mContentCacherFactory:Lfjr;

    .line 192
    return-void
.end method

.method protected final b(Lizn;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 854
    iget-object v3, p1, Lizn;->dUq:[Ljda;

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 855
    const/4 v0, 0x0

    .line 856
    iget-object v6, v5, Ljda;->eaV:Ljbp;

    if-eqz v6, :cond_2

    .line 857
    iget-object v0, v5, Ljda;->eaV:Ljbp;

    invoke-static {v0}, Lgay;->d(Ljbp;)Landroid/location/Location;

    move-result-object v0

    invoke-static {v0}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v0

    .line 867
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 868
    iget-object v5, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mGmmPrecacher:Lcgj;

    invoke-virtual {v5, p0, v0}, Lcgj;->a(Landroid/content/Context;Ljava/util/List;)V

    .line 854
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 860
    :cond_2
    iget-object v6, v5, Ljda;->ebh:[Ljbp;

    array-length v6, v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    .line 861
    iget-object v0, v5, Ljda;->ebh:[Ljbp;

    aget-object v0, v0, v2

    invoke-static {v0}, Lgay;->d(Ljbp;)Landroid/location/Location;

    move-result-object v0

    iget-object v5, v5, Ljda;->ebh:[Ljbp;

    const/4 v6, 0x1

    aget-object v5, v5, v6

    invoke-static {v5}, Lgay;->d(Ljbp;)Landroid/location/Location;

    move-result-object v5

    invoke-static {v0, v5}, Lijj;->r(Ljava/lang/Object;Ljava/lang/Object;)Lijj;

    move-result-object v0

    goto :goto_1

    .line 871
    :cond_3
    return-void
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mVelvetServices:Lgql;

    if-nez v0, :cond_0

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mVelvetServices:Lgql;

    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DD()Lcjs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mConfig:Lcjs;

    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    new-instance v0, Lcgj;

    invoke-direct {v0}, Lcgj;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mGmmPrecacher:Lcgj;

    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->ayi()Livq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mTvDetectorFactory:Livq;

    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->ayj()Livq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mCastTvDetectorFactory:Livq;

    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->axZ()Lfak;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mEntriesRefreshScheduler:Lfak;

    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->axY()Lfdg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mInteractionManager:Lfdg;

    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mVelvetServices:Lgql;

    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    iput-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mAsyncServices:Lema;

    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->ayc()Lfdn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mTrainingQuestionManager:Lfdn;

    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->Sh()Lfcr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mExecutedUserActionStore:Lfcr;

    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->getEntryProvider()Lfaq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mEntryProvider:Lfaq;

    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->ayo()Lfjr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mContentCacherFactory:Lfjr;

    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aKb()Lelo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mAccessibilityManager:Lelo;

    .line 152
    :cond_0
    invoke-super {p0}, Lfac;->onCreate()V

    .line 153
    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->aJP()V

    .line 154
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cmY:Lfja;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cmY:Lfja;

    invoke-interface {v0}, Lfja;->wE()V

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cmZ:Lfja;

    if-eqz v0, :cond_1

    .line 162
    iget-object v0, p0, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;->cmZ:Lfja;

    invoke-interface {v0}, Lfja;->wE()V

    .line 164
    :cond_1
    invoke-super {p0}, Lfac;->onDestroy()V

    .line 165
    return-void
.end method
