.class public Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bdr:Ljava/util/Set;

.field private final crq:I

.field public crr:Ljava/util/LinkedList;

.field public crs:Landroid/util/SparseArray;

.field public crt:Landroid/util/SparseArray;

.field public cru:Landroid/util/SparseBooleanArray;

.field private mTrainingQuestionManager:Lfdn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 281
    new-instance v0, Lfhd;

    invoke-direct {v0}, Lfhd;-><init>()V

    sput-object v0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->bdr:Ljava/util/Set;

    .line 79
    iput p1, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crq:I

    .line 80
    invoke-direct {p0}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->azT()V

    .line 81
    return-void
.end method

.method public constructor <init>(ILfdn;Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->bdr:Ljava/util/Set;

    .line 69
    iput p1, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crq:I

    .line 70
    iput-object p2, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->mTrainingQuestionManager:Lfdn;

    .line 72
    invoke-direct {p0}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->azT()V

    .line 73
    invoke-direct {p0, p3}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->j(Ljava/lang/Iterable;)V

    .line 74
    return-void
.end method

.method private azT()V
    .locals 3

    .prologue
    .line 236
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crr:Ljava/util/LinkedList;

    .line 237
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crs:Landroid/util/SparseArray;

    .line 238
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crt:Landroid/util/SparseArray;

    .line 239
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->cru:Landroid/util/SparseBooleanArray;

    .line 240
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crq:I

    if-ge v0, v1, :cond_0

    .line 241
    iget-object v1, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->cru:Landroid/util/SparseBooleanArray;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 240
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 243
    :cond_0
    return-void
.end method

.method private j(Ljava/lang/Iterable;)V
    .locals 4

    .prologue
    .line 250
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 251
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 252
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 254
    :cond_0
    :goto_1
    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 255
    invoke-virtual {v1}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 256
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v2

    .line 257
    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v2

    if-nez v2, :cond_1

    .line 258
    iget-object v2, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crr:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 260
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCz()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 261
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCA()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 262
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 268
    :cond_3
    const/4 v0, 0x0

    :goto_3
    iget v1, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crq:I

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crr:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 270
    iget-object v1, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crt:Landroid/util/SparseArray;

    iget-object v2, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crr:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 271
    iget-object v1, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->cru:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->delete(I)V

    .line 269
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 273
    :cond_4
    invoke-direct {p0}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->notifyObservers()V

    .line 274
    return-void
.end method

.method private notifyObservers()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->bdr:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhe;

    .line 99
    invoke-interface {v0}, Lfhe;->notifyChanged()V

    goto :goto_0

    .line 101
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;Ljdf;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 178
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crt:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfValue(Ljava/lang/Object;)I

    move-result v0

    .line 179
    if-gez v0, :cond_0

    .line 180
    const-string v0, "IcebreakerSectionAdapter"

    const-string v1, "Answered question not currently displayed -> likely already processed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    :goto_0
    return-void

    .line 183
    :cond_0
    iget-object v1, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crt:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    .line 184
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->mTrainingQuestionManager:Lfdn;

    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v1

    invoke-interface {v0, v1, p2, v3}, Lfdn;->a(Ljde;Ljdf;Lizj;)V

    .line 185
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->a(Ljdf;)V

    .line 192
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crs:Landroid/util/SparseArray;

    invoke-virtual {v0, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    .line 194
    if-nez v0, :cond_6

    .line 195
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 196
    iget-object v1, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crs:Landroid/util/SparseArray;

    invoke-virtual {v1, v6, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move-object v1, v0

    .line 199
    :goto_1
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    .line 200
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCz()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v3

    :cond_1
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 201
    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v8

    if-nez v8, :cond_1

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCA()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 202
    if-nez v2, :cond_2

    move-object v2, v0

    .line 203
    goto :goto_2

    .line 205
    :cond_2
    invoke-virtual {v7, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    move v4, v5

    .line 211
    :goto_3
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->cru:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    if-ge v4, v0, :cond_4

    invoke-virtual {v7}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 212
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->cru:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v8

    .line 213
    invoke-virtual {v7}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 214
    iget-object v9, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crt:Landroid/util/SparseArray;

    invoke-virtual {v9, v8, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 215
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crs:Landroid/util/SparseArray;

    invoke-virtual {v0, v8, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 211
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    .line 220
    :cond_4
    invoke-virtual {v1, v5, v7}, Ljava/util/LinkedList;->addAll(ILjava/util/Collection;)Z

    .line 223
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crr:Ljava/util/LinkedList;

    invoke-virtual {v0, v7}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 226
    if-nez v2, :cond_5

    .line 227
    invoke-virtual {p0, v6}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->iN(I)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 232
    :goto_4
    invoke-direct {p0}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->notifyObservers()V

    goto/16 :goto_0

    .line 229
    :cond_5
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crt:Landroid/util/SparseArray;

    invoke-virtual {v0, v6, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_4

    :cond_6
    move-object v1, v0

    goto :goto_1
.end method

.method public final a(Lfdn;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->mTrainingQuestionManager:Lfdn;

    .line 85
    return-void
.end method

.method public final a(Lfhe;)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->bdr:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 90
    return-void
.end method

.method public final azR()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crq:I

    return v0
.end method

.method public final azS()I
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crr:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    return v0
.end method

.method public final b(Lfhe;)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->bdr:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 95
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x0

    return v0
.end method

.method public final iM(I)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 118
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crq:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 119
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crt:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    return-object v0

    .line 118
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iN(I)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 129
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crq:I

    if-ge p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 130
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crs:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    .line 132
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 134
    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 137
    iget-object v1, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crr:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 138
    iget-object v1, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crt:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 168
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 129
    goto :goto_0

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crr:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 144
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crr:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 147
    :goto_2
    iget v1, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crq:I

    if-ge v2, v1, :cond_3

    .line 148
    iget-object v1, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crs:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    .line 149
    if-eqz v1, :cond_2

    .line 150
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 154
    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    .line 156
    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 147
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 160
    :cond_3
    iget-object v1, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crt:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 161
    iget-object v1, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crs:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->delete(I)V

    goto :goto_1

    .line 166
    :cond_4
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crt:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->delete(I)V

    .line 167
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->cru:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 168
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    .line 315
    iget v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crq:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 316
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crr:Ljava/util/LinkedList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 318
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 319
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crq:I

    if-ge v2, v0, :cond_2

    .line 320
    iget-object v0, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crt:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    .line 321
    if-eqz v0, :cond_0

    .line 322
    iget-object v1, p0, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->crs:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    .line 326
    if-nez v1, :cond_1

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 328
    :goto_1
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->push(Ljava/lang/Object;)V

    .line 331
    invoke-interface {v3, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 319
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 326
    :cond_1
    invoke-static {v1}, Lilw;->y(Ljava/lang/Iterable;)Ljava/util/LinkedList;

    move-result-object v1

    goto :goto_1

    .line 334
    :cond_2
    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 335
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 336
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto :goto_2

    .line 338
    :cond_3
    return-void
.end method
