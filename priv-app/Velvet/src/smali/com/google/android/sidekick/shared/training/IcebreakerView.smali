.class public Lcom/google/android/sidekick/shared/training/IcebreakerView;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lfqc;


# instance fields
.field private cuA:Lfkd;

.field private cyQ:I

.field private czd:Lfpu;

.field private mCardContainer:Lfmt;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->cyQ:I

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->cyQ:I

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->cyQ:I

    .line 52
    return-void
.end method

.method private a(Landroid/widget/Button;Ljava/lang/String;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lizj;Ljdf;)V
    .locals 1
    .param p4    # Lizj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 279
    invoke-virtual {p1, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 280
    new-instance v0, Lfpx;

    invoke-direct {v0, p0, p3, p5, p4}, Lfpx;-><init>(Lcom/google/android/sidekick/shared/training/IcebreakerView;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljdf;Lizj;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 286
    return-void
.end method

.method private b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 195
    sparse-switch p2, :sswitch_data_0

    .line 208
    invoke-static {p2}, Lfqe;->jh(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->cuA:Lfkd;

    invoke-interface {v1}, Lfkd;->getEntry()Lizj;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, Lfqe;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;ILandroid/content/Context;Lizj;)V

    .line 216
    :goto_0
    return-void

    .line 197
    :sswitch_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->fx(Z)V

    goto :goto_0

    .line 200
    :sswitch_1
    invoke-direct {p0, v0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->fx(Z)V

    goto :goto_0

    .line 204
    :sswitch_2
    iput p2, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->cyQ:I

    .line 205
    invoke-direct {p0, v0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->fx(Z)V

    goto :goto_0

    .line 212
    :cond_0
    const-string v0, "IcebreakerView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized client action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 195
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x4 -> :sswitch_2
        0x9 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method

.method private fx(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 219
    iget v0, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->cyQ:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 221
    :goto_0
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->czd:Lfpu;

    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->cuA:Lfkd;

    invoke-interface {v0, v1, p1, v5}, Lfpu;->a(Lfkd;ZLepp;)V

    .line 222
    return-void

    .line 219
    :cond_0
    new-instance v0, Lfpz;

    iget v1, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->cyQ:I

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->mCardContainer:Lfmt;

    iget-object v4, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->cuA:Lfkd;

    invoke-direct/range {v0 .. v5}, Lfpz;-><init>(ILandroid/content/Context;Lfmt;Lfkd;Landroid/view/accessibility/AccessibilityEvent;)V

    move-object v5, v0

    goto :goto_0
.end method

.method private static z(Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 169
    const v0, 0x7f1100b0

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 170
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    const v0, 0x7f110257

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 173
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 178
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;I)V
    .locals 0

    .prologue
    .line 239
    invoke-direct {p0, p1, p2}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;I)V

    .line 240
    return-void
.end method

.method public final a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lfoz;)V
    .locals 4

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->aAD()Lfml;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->cuA:Lfkd;

    invoke-interface {v1}, Lfkd;->getEntry()Lizj;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v2

    iget-object v3, p2, Lfoz;->ckR:Liwk;

    invoke-virtual {v0, v1, v2, v3}, Lfml;->b(Lizj;Ljde;Liwk;)V

    .line 246
    iget-object v0, p2, Lfoz;->cyj:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p2, Lfoz;->cyj:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;I)V

    .line 253
    :goto_0
    return-void

    .line 251
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->fx(Z)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljdf;Lizj;)V
    .locals 2
    .param p3    # Lizj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->aAD()Lfml;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Lfml;->b(Ljde;Ljdf;Lizj;)V

    .line 229
    invoke-virtual {p1, p2}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->b(Ljdf;)Ljava/lang/Integer;

    move-result-object v0

    .line 230
    if-nez v0, :cond_0

    .line 231
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->fx(Z)V

    .line 235
    :goto_0
    return-void

    .line 233
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;I)V

    goto :goto_0
.end method

.method public final a(Lfmt;Lfkd;Lfpu;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 73
    iput-object p1, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->mCardContainer:Lfmt;

    .line 74
    iput-object p2, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->cuA:Lfkd;

    .line 75
    iput-object p3, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->czd:Lfpu;

    .line 77
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04019f

    invoke-virtual {v0, v1, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0182

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->z(Landroid/view/View;I)V

    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->cuA:Lfkd;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->mCardContainer:Lfmt;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4, v0}, Lfkd;->b(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lftp;

    move-result-object v3

    const v1, 0x7f110257

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, v3, Lftp;->cBN:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f1100b0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, v3, Lftp;->cBO:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    const v2, 0x7f1100fb

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iget-object v4, v3, Lftp;->cBp:Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v4, Lfpv;

    invoke-direct {v4, p0, v3}, Lfpv;-><init>(Lcom/google/android/sidekick/shared/training/IcebreakerView;Lftp;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f1100fc

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iget-object v4, v3, Lftp;->cBq:Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v4, Lfpw;

    invoke-direct {v4, p0, v3}, Lfpw;-><init>(Lcom/google/android/sidekick/shared/training/IcebreakerView;Lftp;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, v3, Lftp;->cBP:Landroid/view/View;

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v1

    iget-object v2, v3, Lftp;->cBP:Landroid/view/View;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0181

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v2, 0x7f110259

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/view/View;->setPadding(IIII)V

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->addView(Landroid/view/View;)V

    .line 78
    return-void
.end method

.method public final a(Lfmt;Lfkd;Lfqh;Lfpu;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 64
    iput-object p1, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->mCardContainer:Lfmt;

    .line 65
    iput-object p2, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->cuA:Lfkd;

    .line 66
    iput-object p4, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->czd:Lfpu;

    .line 68
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->cuA:Lfkd;

    invoke-interface {v0}, Lfkd;->getEntry()Lizj;

    move-result-object v0

    iget-object v1, v0, Lizj;->dTo:Ljdi;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lizj;->dTo:Ljdi;

    iget-object v0, v0, Ljdi;->eca:Ljdj;

    :goto_0
    if-nez v0, :cond_1

    const-string v0, "IcebreakerView"

    const-string v1, "Icebreaker mode specified, but no icebreaker question"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 69
    :goto_1
    return-void

    .line 68
    :cond_0
    iget-object v0, v0, Lizj;->dSa:Ljdj;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->mCardContainer:Lfmt;

    invoke-interface {v1}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBQ()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v0, v1}, Lfqe;->a(Ljdj;Ljava/util/Collection;)Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    move-result-object v7

    if-nez v7, :cond_2

    const-string v0, "IcebreakerView"

    const-string v1, "Icebreaker data missing from rendering context"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v7}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v0

    invoke-static {v0}, Lfqh;->jj(I)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "IcebreakerView"

    const-string v1, "Icebreaker question type is not supported"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v7}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->getType()I

    move-result v0

    if-ne v0, v8, :cond_4

    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->cuA:Lfkd;

    invoke-interface {v0}, Lfkd;->getEntry()Lizj;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04019f

    invoke-virtual {v0, v1, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v7}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v0

    invoke-static {v6, v0, v4}, Lfqk;->a(Landroid/view/View;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lizj;)V

    const v0, 0x7f1100fb

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0452

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v3

    new-instance v0, Ljdf;

    invoke-direct {v0}, Ljdf;-><init>()V

    invoke-virtual {v0, v5}, Ljdf;->hH(Z)Ljdf;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->a(Landroid/widget/Button;Ljava/lang/String;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lizj;Ljdf;)V

    const v0, 0x7f1100fc

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0451

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v3

    new-instance v0, Ljdf;

    invoke-direct {v0}, Ljdf;-><init>()V

    invoke-virtual {v0, v8}, Ljdf;->hH(Z)Ljdf;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->a(Landroid/widget/Button;Ljava/lang/String;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lizj;Ljdf;)V

    move-object v0, v6

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0182

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->z(Landroid/view/View;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/training/IcebreakerView;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/sidekick/shared/training/IcebreakerView;->cuA:Lfkd;

    invoke-interface {v0}, Lfkd;->getEntry()Lizj;

    move-result-object v0

    invoke-virtual {p3, v7, p0, v0, p0}, Lfqh;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;Landroid/view/ViewGroup;Lizj;Lfqc;)Landroid/view/View;

    move-result-object v0

    goto :goto_2
.end method
