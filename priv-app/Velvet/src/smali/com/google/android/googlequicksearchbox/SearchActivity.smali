.class public Lcom/google/android/googlequicksearchbox/SearchActivity;
.super Landroid/app/Activity;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private k(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 125
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 126
    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 130
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 131
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 134
    :cond_0
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 135
    const-string v1, "ORIGINAL_ACTION"

    const-string v2, "com.google.android.googlequicksearchbox.GOOGLE_ICON"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 140
    :goto_0
    const-string v1, "source"

    invoke-static {p1}, Lcom/google/android/googlequicksearchbox/SearchActivity;->m(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    invoke-virtual {p0, v0}, Lcom/google/android/googlequicksearchbox/SearchActivity;->startActivity(Landroid/content/Intent;)V

    .line 142
    return-void

    .line 138
    :cond_1
    const-string v1, "ORIGINAL_ACTION"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private l(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 146
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 147
    const-string v0, "android.intent.action.MAIN"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    invoke-static {p1}, Lhgn;->ad(Landroid/content/Intent;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 149
    const-string v4, "source"

    invoke-static {p1}, Lcom/google/android/googlequicksearchbox/SearchActivity;->m(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    invoke-virtual {v3}, Landroid/content/Intent;->getFlags()I

    move-result v4

    const v5, -0x800001

    and-int/2addr v4, v5

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 155
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.google.android.googlequicksearchbox.INTERNAL_GOOGLE_SEARCH"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "com.google.android.googlequicksearchbox.TEXT_ASSIST"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 156
    const/high16 v1, 0x4000000

    invoke-virtual {v3, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 158
    :cond_1
    const-string v1, "FIRST_RUN"

    const-string v4, "source"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 159
    const-string v1, "disable-opt-in"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 161
    :cond_2
    invoke-static {p1}, Lhgn;->U(Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {p1}, Lhgn;->W(Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 163
    const-string v1, "commit-query"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 165
    :cond_3
    invoke-static {p1}, Lhgn;->V(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 167
    const-string v1, "com.google.android.googlequicksearchbox"

    const-string v2, "com.google.android.search.queryentry.QueryEntryActivity"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    :goto_1
    const-string v1, "velvet-query"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 179
    invoke-virtual {p0, v3}, Lcom/google/android/googlequicksearchbox/SearchActivity;->startActivity(Landroid/content/Intent;)V

    .line 180
    return-void

    .line 155
    :cond_4
    invoke-static {p1}, Lhgn;->T(Landroid/content/Intent;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-static {p1}, Lhgn;->Y(Landroid/content/Intent;)Z

    move-result v5

    if-eqz v5, :cond_6

    :cond_5
    move v1, v2

    goto :goto_0

    :cond_6
    const-string v5, "android.intent.action.ASSIST"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v2

    goto :goto_0

    .line 171
    :cond_7
    const-string v1, "com.google.android.googlequicksearchbox"

    const-string v2, "com.google.android.velvet.ui.VelvetActivity"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 175
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoY()Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_1
.end method

.method private static m(Landroid/content/Intent;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 205
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 207
    const-string v0, "source"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 208
    if-eqz v0, :cond_0

    .line 246
    :goto_0
    return-object v0

    .line 212
    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 213
    const-string v0, "android.intent.action.ASSIST"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214
    invoke-static {p0}, Lfgb;->K(Landroid/content/Intent;)Lfgb;

    move-result-object v0

    .line 215
    if-eqz v0, :cond_1

    .line 216
    invoke-virtual {v0}, Lfgb;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 218
    :cond_1
    const-string v0, "ASSIST_GESTURE"

    goto :goto_0

    .line 220
    :cond_2
    const-string v0, "android.intent.action.MAIN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "com.google.android.googlequicksearchbox.GOOGLE_ICON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 222
    :cond_3
    const-string v0, "LAUNCHER"

    goto :goto_0

    .line 224
    :cond_4
    const-string v0, "android.intent.action.WEB_SEARCH"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 225
    const-string v0, "SYSTEM_WEB_SEARCH"

    goto :goto_0

    .line 227
    :cond_5
    const-string v0, "android.search.action.GLOBAL_SEARCH"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 228
    const-string v0, "SEARCH_WIDGET"

    goto :goto_0

    .line 230
    :cond_6
    const-string v0, "android.intent.action.SEARCH_LONG_PRESS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 231
    const-string v0, "SEARCH_LONG_PRESS"

    goto :goto_0

    .line 233
    :cond_7
    const-string v0, "android.speech.action.WEB_SEARCH"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 234
    const-string v0, "VOICE_SEARCH"

    goto :goto_0

    .line 236
    :cond_8
    const-string v0, "android.intent.action.VOICE_ASSIST"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 237
    const-string v0, "VOICE_ASSIST"

    goto :goto_0

    .line 239
    :cond_9
    const-string v0, "android.intent.action.SEND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 240
    const-string v0, "SHARE_INTENT"

    goto :goto_0

    .line 242
    :cond_a
    const-string v0, "com.google.android.googlequicksearchbox.GOOGLE_SEARCH"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 243
    const-string v0, "GOOGLE_SEARCH_INTENT"

    goto :goto_0

    .line 246
    :cond_b
    const-string v0, "UNKNOWN"

    goto/16 :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/16 v6, 0x108

    const/4 v1, -0x1

    .line 54
    invoke-virtual {p0}, Lcom/google/android/googlequicksearchbox/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 55
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v1

    .line 56
    :goto_0
    if-eq v0, v1, :cond_f

    .line 57
    invoke-static {v6}, Lege;->hs(I)Litu;

    move-result-object v1

    invoke-virtual {v1, v0}, Litu;->mF(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 66
    :goto_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 68
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 72
    const-string v0, "com.google.android.googlequicksearchbox.VIEW_TERMS_OF_SERVICE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.google.android.googlequicksearchbox.VIEW_PRIVACY_POLICY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 76
    :cond_0
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DD()Lcjs;

    move-result-object v2

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DI()Lcpn;

    move-result-object v3

    const/4 v0, 0x0

    const-string v4, "com.google.android.googlequicksearchbox.VIEW_TERMS_OF_SERVICE"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-virtual {v2}, Lcjs;->Lf()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcpn;->hE(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :cond_1
    :goto_2
    if-eqz v0, :cond_2

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v1}, Lcom/google/android/googlequicksearchbox/SearchActivity;->startActivity(Landroid/content/Intent;)V

    .line 113
    :cond_2
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/googlequicksearchbox/SearchActivity;->finish()V

    .line 114
    return-void

    .line 55
    :cond_3
    const-string v3, "android.intent.action.MAIN"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    const-string v3, "android.intent.action.ASSIST"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    const-string v3, "android.search.action.GLOBAL_SEARCH"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v0, 0x2

    goto :goto_0

    :cond_6
    const-string v3, "android.intent.action.SEARCH_LONG_PRESS"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v0, 0x3

    goto/16 :goto_0

    :cond_7
    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v0, 0x4

    goto/16 :goto_0

    :cond_8
    const-string v3, "android.intent.action.WEB_SEARCH"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    const/4 v0, 0x5

    goto/16 :goto_0

    :cond_9
    const-string v3, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    const/4 v0, 0x6

    goto/16 :goto_0

    :cond_a
    const-string v3, "android.speech.action.WEB_SEARCH"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    const/4 v0, 0x7

    goto/16 :goto_0

    :cond_b
    const-string v3, "com.google.android.googlequicksearchbox.VOICE_SEARCH_RECORDED_AUDIO"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    const/16 v0, 0x8

    goto/16 :goto_0

    :cond_c
    const-string v3, "android.intent.action.VOICE_ASSIST"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    const/16 v0, 0x9

    goto/16 :goto_0

    :cond_d
    const-string v3, "com.google.android.googlequicksearchbox.GOOGLE_ICON"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    const/16 v0, 0xa

    goto/16 :goto_0

    :cond_e
    const-string v3, "SearchActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unrecognized intent action: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto/16 :goto_0

    .line 61
    :cond_f
    invoke-static {v6}, Lege;->ht(I)V

    goto/16 :goto_1

    .line 76
    :cond_10
    const-string v2, "com.google.android.googlequicksearchbox.VIEW_PRIVACY_POLICY"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/googlequicksearchbox/SearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_2

    .line 77
    :cond_11
    const-string v0, "android.speech.action.WEB_SEARCH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-static {}, Ldmh;->YZ()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 80
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 81
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 82
    const-string v1, "com.google.android.googlequicksearchbox"

    const-string v2, "com.google.android.handsfree.HandsFreeOverlayActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 84
    invoke-virtual {p0, v0}, Lcom/google/android/googlequicksearchbox/SearchActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_3

    .line 85
    :cond_12
    const-string v0, "com.google.android.googlequicksearchbox.VOICE_SEARCH_DSP_HOTWORD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 86
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lchk;->FD()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 88
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.search.core.action.HOTWORD_TRIGGERED_ON_DSP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 89
    const-class v1, Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 90
    invoke-virtual {p0, v0}, Lcom/google/android/googlequicksearchbox/SearchActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_3

    .line 92
    :cond_13
    const-string v0, "android.intent.action.ASSIST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-static {v2}, Lhgn;->V(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 94
    invoke-static {p0}, Lhgn;->bI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 95
    invoke-direct {p0, v2}, Lcom/google/android/googlequicksearchbox/SearchActivity;->k(Landroid/content/Intent;)V

    goto/16 :goto_3

    .line 97
    :cond_14
    invoke-direct {p0, v2}, Lcom/google/android/googlequicksearchbox/SearchActivity;->l(Landroid/content/Intent;)V

    goto/16 :goto_3

    .line 99
    :cond_15
    invoke-static {p0}, Lhgn;->bI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_16

    const-string v0, "com.google.android.googlequicksearchbox.MUSIC_SEARCH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    const-string v0, "com.google.android.googlequicksearchbox.GOOGLE_SEARCH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    const-string v0, "com.google.android.googlequicksearchbox.INTERNAL_GOOGLE_SEARCH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    const-string v0, "com.google.android.googlequicksearchbox.LAUNCH_GSA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    const-string v0, "android.intent.action.WEB_SEARCH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    const-string v0, "com.google.android.googlequicksearchbox.VOICE_SEARCH_RECORDED_AUDIO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    invoke-static {v2}, Lhgn;->V(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_16

    invoke-static {v2}, Lhgn;->Z(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 109
    invoke-direct {p0, v2}, Lcom/google/android/googlequicksearchbox/SearchActivity;->k(Landroid/content/Intent;)V

    goto/16 :goto_3

    .line 111
    :cond_16
    invoke-direct {p0, v2}, Lcom/google/android/googlequicksearchbox/SearchActivity;->l(Landroid/content/Intent;)V

    goto/16 :goto_3
.end method
