.class public Lcom/google/android/googlequicksearchbox/VoiceSearchActivity;
.super Landroid/app/Activity;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const v2, -0x800001

    .line 21
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    invoke-static {p0}, Lhgn;->bI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/googlequicksearchbox/VoiceSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 28
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v1

    and-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 30
    const-string v1, "android.speech.action.WEB_SEARCH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 31
    invoke-virtual {p0}, Lcom/google/android/googlequicksearchbox/VoiceSearchActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.launcher.GEL"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 32
    invoke-virtual {p0, v0}, Lcom/google/android/googlequicksearchbox/VoiceSearchActivity;->startActivity(Landroid/content/Intent;)V

    .line 41
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/googlequicksearchbox/VoiceSearchActivity;->finish()V

    .line 42
    return-void

    .line 34
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/googlequicksearchbox/VoiceSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 35
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v1

    and-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 36
    const-string v1, "velvet-query"

    sget-object v2, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aph()Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 37
    const-string v1, "commit-query"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 38
    invoke-virtual {p0}, Lcom/google/android/googlequicksearchbox/VoiceSearchActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.velvet.ui.VelvetActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 39
    invoke-virtual {p0, v0}, Lcom/google/android/googlequicksearchbox/VoiceSearchActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
