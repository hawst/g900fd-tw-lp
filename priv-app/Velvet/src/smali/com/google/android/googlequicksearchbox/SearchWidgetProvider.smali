.class public Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "PG"


# static fields
.field private static aMi:Z

.field private static aMj:Z

.field private static aMk:Ljava/lang/String;

.field private static aMl:I

.field private static aMm:I

.field private static aMn:I

.field private static aMo:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 69
    sput-boolean v0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMi:Z

    .line 70
    sput-boolean v0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMj:Z

    .line 71
    const-string v0, ""

    sput-object v0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMk:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;IILjava/lang/String;)Landroid/widget/RemoteViews;
    .locals 6

    .prologue
    const v5, 0x7f1103d0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 226
    const-string v0, "recents-widget"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 227
    const-string v0, "com.google.android.googlequicksearchbox.TEXT_ASSIST"

    invoke-static {p0, p3, v0, v2}, Lhgn;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 229
    const-string v1, "android.intent.action.VOICE_ASSIST"

    invoke-static {p0, p3, v1, v2}, Lhgn;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    move-object v2, v1

    move-object v1, v0

    .line 237
    :goto_0
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4, p1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 238
    const v4, 0x7f1103cd

    invoke-static {p0, v0, v4, v1}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILandroid/content/Intent;)V

    .line 239
    invoke-static {p0, v0, v5, v2}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILandroid/content/Intent;)V

    .line 240
    sget-boolean v1, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMi:Z

    if-eqz v1, :cond_2

    const v1, 0x7f02018f

    :goto_1
    invoke-virtual {v0, v5, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 243
    const v1, 0x7f1103d1

    move v2, p2

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    .line 244
    sget-boolean v1, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMj:Z

    if-eqz v1, :cond_3

    sget-object v1, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMk:Ljava/lang/String;

    .line 246
    :goto_2
    const v2, 0x7f1103ce

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, 0x8

    :cond_0
    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 248
    return-object v0

    .line 232
    :cond_1
    invoke-static {p0, p3}, Lhgn;->t(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 234
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.speech.action.WEB_SEARCH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-static {v1, p3}, Lhgn;->f(Landroid/content/Intent;Ljava/lang/String;)V

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    .line 240
    :cond_2
    const v1, 0x7f02019d

    goto :goto_1

    .line 244
    :cond_3
    const-string v1, ""

    goto :goto_2
.end method

.method private static a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 11

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 105
    array-length v8, p2

    move v7, v4

    :goto_0
    if-ge v7, v8, :cond_8

    aget v9, p2, v7

    .line 107
    invoke-virtual {p1, v9}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    move-result-object v0

    .line 108
    const-string v1, "appWidgetCategory"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_7

    .line 110
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    iget v0, v10, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_2

    move v6, v2

    :goto_1
    if-eqz v6, :cond_3

    iget v0, v1, Landroid/graphics/Point;->x:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    :goto_2
    sget v5, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMm:I

    if-ne v5, v0, :cond_0

    sget v5, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMl:I

    if-eq v5, v1, :cond_4

    :cond_0
    move v5, v2

    :goto_3
    if-eqz v5, :cond_1

    sput v0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMm:I

    sput v1, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMl:I

    invoke-static {p0, v6}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->c(Landroid/content/Context;Z)V

    new-instance v1, Landroid/content/res/Configuration;

    invoke-direct {v1, v10}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iget v0, v10, Landroid/content/res/Configuration;->screenWidthDp:I

    iput v0, v1, Landroid/content/res/Configuration;->screenHeightDp:I

    iget v0, v10, Landroid/content/res/Configuration;->screenHeightDp:I

    iput v0, v1, Landroid/content/res/Configuration;->screenWidthDp:I

    if-eqz v6, :cond_5

    move v0, v2

    :goto_4
    iput v0, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->createConfigurationContext(Landroid/content/res/Configuration;)Landroid/content/Context;

    move-result-object v1

    if-nez v6, :cond_6

    move v0, v2

    :goto_5
    invoke-static {v1, v0}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->c(Landroid/content/Context;Z)V

    :cond_1
    new-instance v0, Landroid/widget/RemoteViews;

    const v1, 0x7f04015c

    sget v5, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMo:I

    const-string v6, "recents-widget"

    invoke-static {p0, v1, v5, v6}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->a(Landroid/content/Context;IILjava/lang/String;)Landroid/widget/RemoteViews;

    move-result-object v1

    const v5, 0x7f04015d

    sget v6, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMn:I

    const-string v10, "recents-widget"

    invoke-static {p0, v5, v6, v10}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->a(Landroid/content/Context;IILjava/lang/String;)Landroid/widget/RemoteViews;

    move-result-object v5

    invoke-direct {v0, v1, v5}, Landroid/widget/RemoteViews;-><init>(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)V

    .line 114
    :goto_6
    invoke-virtual {p1, v9, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 105
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto/16 :goto_0

    :cond_2
    move v6, v4

    .line 110
    goto :goto_1

    :cond_3
    iget v0, v1, Landroid/graphics/Point;->y:I

    iget v1, v1, Landroid/graphics/Point;->x:I

    goto :goto_2

    :cond_4
    move v5, v4

    goto :goto_3

    :cond_5
    move v0, v3

    goto :goto_4

    :cond_6
    move v0, v4

    goto :goto_5

    .line 112
    :cond_7
    const v0, 0x7f04015b

    const-string v1, "launcher-widget"

    invoke-static {p0, v0, v4, v1}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->a(Landroid/content/Context;IILjava/lang/String;)Landroid/widget/RemoteViews;

    move-result-object v0

    goto :goto_6

    .line 116
    :cond_8
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/widget/RemoteViews;ILandroid/content/Intent;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 268
    invoke-static {p0, v0, p3, v0}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 269
    invoke-virtual {p1, p2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 270
    return-void
.end method

.method public static a(Landroid/content/Context;ZZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 124
    invoke-static {}, Lenu;->auR()V

    .line 126
    sget-boolean v0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMi:Z

    .line 127
    sget-boolean v1, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMj:Z

    .line 128
    sget-object v2, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMk:Ljava/lang/String;

    .line 129
    sput-boolean p1, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMi:Z

    .line 130
    sput-boolean p2, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMj:Z

    .line 131
    sput-object p3, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMk:Ljava/lang/String;

    .line 132
    sget-boolean v3, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMi:Z

    if-ne v3, v0, :cond_0

    sget-boolean v0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMj:Z

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMk:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 135
    :cond_0
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/content/ComponentName;

    invoke-direct {v3, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 137
    :cond_1
    return-void
.end method

.method private static c(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 154
    if-eqz p1, :cond_0

    .line 155
    sget v0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMm:I

    invoke-static {p0, v0, v1}, Leot;->b(Landroid/content/Context;IZ)I

    move-result v0

    sput v0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMo:I

    .line 161
    :goto_0
    return-void

    .line 158
    :cond_0
    sget v0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMl:I

    invoke-static {p0, v0, v1}, Leot;->b(Landroid/content/Context;IZ)I

    move-result v0

    sput v0, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->aMn:I

    goto :goto_0
.end method


# virtual methods
.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 0

    .prologue
    .line 83
    invoke-static {p1, p2, p3}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 84
    return-void
.end method
