.class public final Lcom/google/android/gms/internal/ov;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbnj;


# instance fields
.field public final aGg:Z

.field public final aGh:Z

.field public final aGi:Ljava/lang/String;

.field public final aGj:Z

.field private final auB:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbnj;

    invoke-direct {v0}, Lbnj;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ov;->CREATOR:Lbnj;

    return-void
.end method

.method public constructor <init>(IZZLjava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/ov;->auB:I

    iput-boolean p2, p0, Lcom/google/android/gms/internal/ov;->aGg:Z

    iput-boolean p3, p0, Lcom/google/android/gms/internal/ov;->aGh:Z

    iput-object p4, p0, Lcom/google/android/gms/internal/ov;->aGi:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/google/android/gms/internal/ov;->aGj:Z

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lbjp;->ao(Ljava/lang/Object;)Lbjq;

    move-result-object v0

    const-string v1, "useOfflineDatabase"

    iget-boolean v2, p0, Lcom/google/android/gms/internal/ov;->aGg:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbjq;->a(Ljava/lang/String;Ljava/lang/Object;)Lbjq;

    move-result-object v0

    const-string v1, "useWebData"

    iget-boolean v2, p0, Lcom/google/android/gms/internal/ov;->aGh:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbjq;->a(Ljava/lang/String;Ljava/lang/Object;)Lbjq;

    move-result-object v0

    const-string v1, "endpoint"

    iget-object v2, p0, Lcom/google/android/gms/internal/ov;->aGi:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lbjq;->a(Ljava/lang/String;Ljava/lang/Object;)Lbjq;

    move-result-object v0

    invoke-virtual {v0}, Lbjq;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Lbnj;->a(Lcom/google/android/gms/internal/ov;Landroid/os/Parcel;)V

    return-void
.end method

.method public final xT()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/ov;->auB:I

    return v0
.end method
