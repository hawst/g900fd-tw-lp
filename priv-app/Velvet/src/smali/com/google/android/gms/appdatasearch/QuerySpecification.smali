.class public Lcom/google/android/gms/appdatasearch/QuerySpecification;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbep;


# instance fields
.field public final auB:I

.field public final avW:Z

.field public final avX:Ljava/util/List;

.field public final avY:Ljava/util/List;

.field public final avZ:Z

.field public final avr:I

.field public final avs:I

.field public final avt:I

.field public final awa:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbep;

    invoke-direct {v0}, Lbep;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:Lbep;

    return-void
.end method

.method public constructor <init>(IZLjava/util/List;Ljava/util/List;ZIIZI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->auB:I

    iput-boolean p2, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->avW:Z

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->avX:Ljava/util/List;

    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->avY:Ljava/util/List;

    iput-boolean p5, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->avZ:Z

    iput p6, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->avr:I

    iput p7, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->avs:I

    iput-boolean p8, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->awa:Z

    iput p9, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->avt:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:Lbep;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:Lbep;

    invoke-static {p0, p1}, Lbep;->a(Lcom/google/android/gms/appdatasearch/QuerySpecification;Landroid/os/Parcel;)V

    return-void
.end method
