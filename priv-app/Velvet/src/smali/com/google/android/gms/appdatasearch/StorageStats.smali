.class public Lcom/google/android/gms/appdatasearch/StorageStats;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbcz;


# instance fields
.field public final auB:I

.field public final axd:[Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;

.field public final axe:J

.field public final axf:J

.field public final axg:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbcz;

    invoke-direct {v0}, Lbcz;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/StorageStats;->CREATOR:Lbcz;

    return-void
.end method

.method public constructor <init>(I[Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;JJJ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->auB:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->axd:[Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;

    iput-wide p3, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->axe:J

    iput-wide p5, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->axf:J

    iput-wide p7, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->axg:J

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/StorageStats;->CREATOR:Lbcz;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/StorageStats;->CREATOR:Lbcz;

    invoke-static {p0, p1, p2}, Lbcz;->a(Lcom/google/android/gms/appdatasearch/StorageStats;Landroid/os/Parcel;I)V

    return-void
.end method
