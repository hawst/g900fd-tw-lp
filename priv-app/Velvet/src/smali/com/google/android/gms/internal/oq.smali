.class public final Lcom/google/android/gms/internal/oq;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbnf;


# instance fields
.field private final aGf:Ljava/lang/String;

.field private final auB:I

.field private final aye:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbnf;

    invoke-direct {v0}, Lbnf;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/oq;->CREATOR:Lbnf;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/oq;->auB:I

    iput-object p2, p0, Lcom/google/android/gms/internal/oq;->aye:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/internal/oq;->aGf:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Lbnf;->a(Lcom/google/android/gms/internal/oq;Landroid/os/Parcel;)V

    return-void
.end method

.method public final xT()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/oq;->auB:I

    return v0
.end method

.method public final yM()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/oq;->aye:Ljava/lang/String;

    return-object v0
.end method

.method public final zP()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/oq;->aGf:Ljava/lang/String;

    return-object v0
.end method
