.class public Lcom/google/android/gms/internal/st$b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbpi;


# instance fields
.field public final auB:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbpi;

    invoke-direct {v0}, Lbpi;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/st$b;->CREATOR:Lbpi;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/st$b;->auB:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/st$b;->auB:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/st$b;->CREATOR:Lbpi;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/st$b;->CREATOR:Lbpi;

    invoke-static {p0, p1}, Lbpi;->a(Lcom/google/android/gms/internal/st$b;Landroid/os/Parcel;)V

    return-void
.end method
