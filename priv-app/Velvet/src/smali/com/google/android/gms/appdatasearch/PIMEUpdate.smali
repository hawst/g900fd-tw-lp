.class public Lcom/google/android/gms/appdatasearch/PIMEUpdate;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbds;


# instance fields
.field public final account:Landroid/accounts/Account;

.field public final auB:I

.field public final avF:[B

.field public final avG:[B

.field public final avH:I

.field public final avI:Ljava/lang/String;

.field public final avJ:Ljava/lang/String;

.field public final avK:Z

.field public final avL:Landroid/os/Bundle;

.field public final avM:J

.field public final avN:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbds;

    invoke-direct {v0}, Lbds;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->CREATOR:Lbds;

    return-void
.end method

.method public constructor <init>(I[B[BILjava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;JJLandroid/accounts/Account;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->auB:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->avF:[B

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->avG:[B

    iput p4, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->avH:I

    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->avI:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->avJ:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->avK:Z

    iput-object p8, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->avL:Landroid/os/Bundle;

    iput-wide p9, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->avM:J

    iput-wide p11, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->avN:J

    iput-object p13, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->account:Landroid/accounts/Account;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->CREATOR:Lbds;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->CREATOR:Lbds;

    invoke-static {p0, p1, p2}, Lbds;->a(Lcom/google/android/gms/appdatasearch/PIMEUpdate;Landroid/os/Parcel;I)V

    return-void
.end method
