.class public Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbdv;


# instance fields
.field public final auB:I

.field public final avS:[Lcom/google/android/gms/appdatasearch/CorpusId;

.field public final avT:[I

.field public final mErrorMessage:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbdv;

    invoke-direct {v0}, Lbdv;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->CREATOR:Lbdv;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/CorpusId;[I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->auB:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->mErrorMessage:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->avS:[Lcom/google/android/gms/appdatasearch/CorpusId;

    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->avT:[I

    return-void
.end method


# virtual methods
.method public final ac(II)I
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->avT:[I

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->avS:[Lcom/google/android/gms/appdatasearch/CorpusId;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x0

    add-int/lit8 v1, v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->CREATOR:Lbdv;

    const/4 v0, 0x0

    return v0
.end method

.method public final dy(I)Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->avS:[Lcom/google/android/gms/appdatasearch/CorpusId;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->avS:[Lcom/google/android/gms/appdatasearch/CorpusId;

    array-length v1, v1

    add-int/2addr v1, v0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->avT:[I

    aget v2, v2, v0

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final hasError()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->mErrorMessage:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final wW()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->mErrorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->CREATOR:Lbdv;

    invoke-static {p0, p1, p2}, Lbdv;->a(Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;Landroid/os/Parcel;I)V

    return-void
.end method
