.class public Lcom/google/android/gms/internal/ph;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbnt;


# instance fields
.field private final aGk:I

.field private final aGl:I

.field private final aGm:Z

.field private final auB:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbnt;

    invoke-direct {v0}, Lbnt;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ph;->CREATOR:Lbnt;

    return-void
.end method

.method public constructor <init>(IIIZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/ph;->auB:I

    iput p2, p0, Lcom/google/android/gms/internal/ph;->aGk:I

    iput p3, p0, Lcom/google/android/gms/internal/ph;->aGl:I

    iput-boolean p4, p0, Lcom/google/android/gms/internal/ph;->aGm:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lbjp;->ao(Ljava/lang/Object;)Lbjq;

    move-result-object v0

    const-string v1, "imageSize"

    iget v2, p0, Lcom/google/android/gms/internal/ph;->aGk:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbjq;->a(Ljava/lang/String;Ljava/lang/Object;)Lbjq;

    move-result-object v0

    const-string v1, "avatarOptions"

    iget v2, p0, Lcom/google/android/gms/internal/ph;->aGl:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbjq;->a(Ljava/lang/String;Ljava/lang/Object;)Lbjq;

    move-result-object v0

    const-string v1, "useLargePictureForCp2Images"

    iget-boolean v2, p0, Lcom/google/android/gms/internal/ph;->aGm:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbjq;->a(Ljava/lang/String;Ljava/lang/Object;)Lbjq;

    move-result-object v0

    invoke-virtual {v0}, Lbjq;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Lbnt;->a(Lcom/google/android/gms/internal/ph;Landroid/os/Parcel;)V

    return-void
.end method

.method public final xT()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/ph;->auB:I

    return v0
.end method

.method public final zU()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/ph;->aGk:I

    return v0
.end method

.method public final zV()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/ph;->aGl:I

    return v0
.end method

.method public final zW()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/ph;->aGm:Z

    return v0
.end method
