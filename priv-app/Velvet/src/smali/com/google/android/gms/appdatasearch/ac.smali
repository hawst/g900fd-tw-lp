.class public Lcom/google/android/gms/appdatasearch/ac;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbcw;


# instance fields
.field public final auB:I

.field public final axu:Ljava/lang/String;

.field public final axv:[Lcom/google/android/gms/appdatasearch/DocumentId;

.field public final axw:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbcw;

    invoke-direct {v0}, Lbcw;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/ac;->CREATOR:Lbcw;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/DocumentId;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/ac;->auB:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/ac;->axu:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/ac;->axv:[Lcom/google/android/gms/appdatasearch/DocumentId;

    iput p4, p0, Lcom/google/android/gms/appdatasearch/ac;->axw:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/ac;->CREATOR:Lbcw;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/ac;->CREATOR:Lbcw;

    invoke-static {p0, p1, p2}, Lbcw;->a(Lcom/google/android/gms/appdatasearch/ac;Landroid/os/Parcel;I)V

    return-void
.end method
