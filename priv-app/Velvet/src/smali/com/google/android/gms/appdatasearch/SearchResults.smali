.class public Lcom/google/android/gms/appdatasearch/SearchResults;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Ljava/lang/Iterable;


# static fields
.field public static final CREATOR:Lbcx;


# instance fields
.field public final auB:I

.field public final awI:[I

.field public final awJ:[B

.field public final awK:[Landroid/os/Bundle;

.field public final awL:[Landroid/os/Bundle;

.field public final awM:[Landroid/os/Bundle;

.field public final awN:I

.field public final awO:[I

.field public final awP:[Ljava/lang/String;

.field public final awQ:[B

.field public final awR:[D

.field public final mErrorMessage:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbcx;

    invoke-direct {v0}, Lbcx;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/SearchResults;->CREATOR:Lbcx;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;[I[B[Landroid/os/Bundle;[Landroid/os/Bundle;[Landroid/os/Bundle;I[I[Ljava/lang/String;[B[D)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->auB:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mErrorMessage:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awI:[I

    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awJ:[B

    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awK:[Landroid/os/Bundle;

    iput-object p6, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awL:[Landroid/os/Bundle;

    iput-object p7, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awM:[Landroid/os/Bundle;

    iput p8, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awN:I

    iput-object p9, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awO:[I

    iput-object p10, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awP:[Ljava/lang/String;

    iput-object p11, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awQ:[B

    iput-object p12, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awR:[D

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/SearchResults;->CREATOR:Lbcx;

    const/4 v0, 0x0

    return v0
.end method

.method public final eP(Ljava/lang/String;)Lbci;
    .locals 2

    new-instance v0, Lbcj;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lbcj;-><init>(Lcom/google/android/gms/appdatasearch/SearchResults;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final hasError()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mErrorMessage:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/SearchResults;->xf()Lbci;

    move-result-object v0

    return-object v0
.end method

.method public final m(Ljava/lang/String;Ljava/lang/String;)Lbci;
    .locals 1

    new-instance v0, Lbcj;

    invoke-direct {v0, p0, p1, p2}, Lbcj;-><init>(Lcom/google/android/gms/appdatasearch/SearchResults;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final wW()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mErrorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final wX()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->awN:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/SearchResults;->CREATOR:Lbcx;

    invoke-static {p0, p1, p2}, Lbcx;->a(Lcom/google/android/gms/appdatasearch/SearchResults;Landroid/os/Parcel;I)V

    return-void
.end method

.method public final xf()Lbci;
    .locals 1

    new-instance v0, Lbci;

    invoke-direct {v0, p0}, Lbci;-><init>(Lcom/google/android/gms/appdatasearch/SearchResults;)V

    return-object v0
.end method
