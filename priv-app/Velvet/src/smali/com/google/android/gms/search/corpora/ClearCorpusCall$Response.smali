.class public Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;
.super Ljava/lang/Object;

# interfaces
.implements Lbho;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbuz;


# instance fields
.field public final auB:I

.field public avd:Lcom/google/android/gms/common/api/Status;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbuz;

    invoke-direct {v0}, Lbuz;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->CREATOR:Lbuz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->auB:I

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/common/api/Status;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->auB:I

    iput-object p2, p0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->avd:Lcom/google/android/gms/common/api/Status;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->CREATOR:Lbuz;

    const/4 v0, 0x0

    return v0
.end method

.method public final wZ()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->avd:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->CREATOR:Lbuz;

    invoke-static {p0, p1, p2}, Lbuz;->a(Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;Landroid/os/Parcel;I)V

    return-void
.end method
