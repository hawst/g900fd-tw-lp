.class public final Lcom/google/android/gms/location/places/PlaceType;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbry;


# instance fields
.field public final aIb:Ljava/lang/String;

.field public final auB:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "accounting"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "airport"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "amusement_park"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "aquarium"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "art_gallery"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "atm"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "bakery"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "bank"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "bar"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "beauty_salon"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "bicycle_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "book_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "bowling_alley"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "bus_station"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "cafe"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "campground"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "car_dealer"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "car_rental"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "car_repair"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "car_wash"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "casino"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "cemetery"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "church"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "city_hall"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "clothing_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "convenience_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "courthouse"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "dentist"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "department_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "doctor"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "electrician"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "electronics_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "embassy"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "establishment"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "finance"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "fire_station"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "florist"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "food"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "funeral_home"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "furniture_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "gas_station"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "general_contractor"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "grocery_or_supermarket"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "gym"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "hair_care"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "hardware_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "health"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "hindu_temple"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "home_goods_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "hospital"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "insurance_agency"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "jewelry_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "laundry"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "lawyer"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "library"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "liquor_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "local_government_office"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "locksmith"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "lodging"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "meal_delivery"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "meal_takeaway"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "mosque"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "movie_rental"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "movie_theater"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "moving_company"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "museum"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "night_club"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "painter"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "park"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "parking"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "pet_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "pharmacy"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "physiotherapist"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "place_of_worship"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "plumber"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "police"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "post_office"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "real_estate_agency"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "restaurant"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "roofing_contractor"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "rv_park"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "school"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "shoe_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "shopping_mall"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "spa"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "stadium"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "storage"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "subway_station"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "synagogue"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "taxi_stand"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "train_station"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "travel_agency"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "university"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "veterinary_care"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "zoo"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "administrative_area_level_1"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "administrative_area_level_2"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "administrative_area_level_3"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "colloquial_area"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "country"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "floor"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "geocode"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "intersection"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "locality"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "natural_feature"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "neighborhood"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "political"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "point_of_interest"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "post_box"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "postal_code"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "postal_code_prefix"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "postal_town"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "premise"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "room"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "route"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "street_address"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "sublocality"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "sublocality_level_1"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "sublocality_level_2"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "sublocality_level_3"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "sublocality_level_4"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "sublocality_level_5"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "subpremise"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "transit_station"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "other"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    new-instance v0, Lbry;

    invoke-direct {v0}, Lbry;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->CREATOR:Lbry;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lbjr;->fg(Ljava/lang/String;)Ljava/lang/String;

    iput p1, p0, Lcom/google/android/gms/location/places/PlaceType;->auB:I

    iput-object p2, p0, Lcom/google/android/gms/location/places/PlaceType;->aIb:Ljava/lang/String;

    return-void
.end method

.method private static fG(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;
    .locals 2

    new-instance v0, Lcom/google/android/gms/location/places/PlaceType;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/location/places/PlaceType;->CREATOR:Lbry;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lcom/google/android/gms/location/places/PlaceType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/location/places/PlaceType;->aIb:Ljava/lang/String;

    check-cast p1, Lcom/google/android/gms/location/places/PlaceType;

    iget-object v1, p1, Lcom/google/android/gms/location/places/PlaceType;->aIb:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/PlaceType;->aIb:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/PlaceType;->aIb:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/location/places/PlaceType;->CREATOR:Lbry;

    invoke-static {p0, p1}, Lbry;->a(Lcom/google/android/gms/location/places/PlaceType;Landroid/os/Parcel;)V

    return-void
.end method
