.class public Lcom/google/android/gms/auth/AccountChangeEventsRequest;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbeu;


# instance fields
.field public final ayc:I

.field public aye:Ljava/lang/String;

.field public ayg:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbeu;

    invoke-direct {v0}, Lbeu;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/AccountChangeEventsRequest;->CREATOR:Lbeu;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/auth/AccountChangeEventsRequest;->ayc:I

    return-void
.end method

.method public constructor <init>(IILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/auth/AccountChangeEventsRequest;->ayc:I

    iput p2, p0, Lcom/google/android/gms/auth/AccountChangeEventsRequest;->ayg:I

    iput-object p3, p0, Lcom/google/android/gms/auth/AccountChangeEventsRequest;->aye:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final dB(I)Lcom/google/android/gms/auth/AccountChangeEventsRequest;
    .locals 0

    iput p1, p0, Lcom/google/android/gms/auth/AccountChangeEventsRequest;->ayg:I

    return-object p0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final fa(Ljava/lang/String;)Lcom/google/android/gms/auth/AccountChangeEventsRequest;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/auth/AccountChangeEventsRequest;->aye:Ljava/lang/String;

    return-object p0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Lbeu;->a(Lcom/google/android/gms/auth/AccountChangeEventsRequest;Landroid/os/Parcel;)V

    return-void
.end method
