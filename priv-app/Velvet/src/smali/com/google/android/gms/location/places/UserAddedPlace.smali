.class public Lcom/google/android/gms/location/places/UserAddedPlace;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final aIc:Lcom/google/android/gms/maps/model/LatLng;

.field private final aId:Ljava/lang/String;

.field private final aIe:Ljava/util/List;

.field private final aIf:Ljava/lang/String;

.field private final aIg:Ljava/lang/String;

.field public final auB:I

.field private final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbrz;

    invoke-direct {v0}, Lbrz;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/places/UserAddedPlace;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/location/places/UserAddedPlace;->auB:I

    iput-object p2, p0, Lcom/google/android/gms/location/places/UserAddedPlace;->mName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/location/places/UserAddedPlace;->aIc:Lcom/google/android/gms/maps/model/LatLng;

    iput-object p4, p0, Lcom/google/android/gms/location/places/UserAddedPlace;->aId:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/gms/location/places/UserAddedPlace;->aIe:Ljava/util/List;

    iput-object p6, p0, Lcom/google/android/gms/location/places/UserAddedPlace;->aIf:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/location/places/UserAddedPlace;->aIg:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final Ap()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/UserAddedPlace;->aIc:Lcom/google/android/gms/maps/model/LatLng;

    return-object v0
.end method

.method public final Aq()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/UserAddedPlace;->aIe:Ljava/util/List;

    return-object v0
.end method

.method public final Ar()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/UserAddedPlace;->aIf:Ljava/lang/String;

    return-object v0
.end method

.method public final As()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/UserAddedPlace;->aIg:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/UserAddedPlace;->aId:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/UserAddedPlace;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lbrz;->a(Lcom/google/android/gms/location/places/UserAddedPlace;Landroid/os/Parcel;I)V

    return-void
.end method
