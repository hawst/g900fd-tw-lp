.class public Lcom/google/android/gms/internal/st$a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbph;


# instance fields
.field public aHc:J

.field public aHd:J

.field public final auB:I

.field public awG:Z

.field public packageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbph;

    invoke-direct {v0}, Lbph;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/st$a;->CREATOR:Lbph;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/st$a;->auB:I

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;JZJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/st$a;->auB:I

    iput-object p2, p0, Lcom/google/android/gms/internal/st$a;->packageName:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/gms/internal/st$a;->aHc:J

    iput-boolean p5, p0, Lcom/google/android/gms/internal/st$a;->awG:Z

    iput-wide p6, p0, Lcom/google/android/gms/internal/st$a;->aHd:J

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/st$a;->CREATOR:Lbph;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/st$a;->CREATOR:Lbph;

    invoke-static {p0, p1}, Lbph;->a(Lcom/google/android/gms/internal/st$a;Landroid/os/Parcel;)V

    return-void
.end method
