.class public Lcom/google/android/gms/search/queries/GlobalQueryCall$b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbvy;


# instance fields
.field public aHj:I

.field public aLI:I

.field public aLJ:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

.field public final auB:I

.field public axs:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbvy;

    invoke-direct {v0}, Lbvy;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->CREATOR:Lbvy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->auB:I

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->auB:I

    iput-object p2, p0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->axs:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->aLI:I

    iput p4, p0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->aHj:I

    iput-object p5, p0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->aLJ:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->CREATOR:Lbvy;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->CREATOR:Lbvy;

    invoke-static {p0, p1, p2}, Lbvy;->a(Lcom/google/android/gms/search/queries/GlobalQueryCall$b;Landroid/os/Parcel;I)V

    return-void
.end method
