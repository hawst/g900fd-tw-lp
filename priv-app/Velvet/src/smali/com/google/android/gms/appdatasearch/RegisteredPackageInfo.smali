.class public Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbcu;


# instance fields
.field public final auB:I

.field public final awF:J

.field public final awG:Z

.field public final awH:J

.field public final packageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbcu;

    invoke-direct {v0}, Lbcu;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->CREATOR:Lbcu;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;JZJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->auB:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->packageName:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->awF:J

    iput-boolean p5, p0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->awG:Z

    iput-wide p6, p0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->awH:J

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->CREATOR:Lbcu;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->CREATOR:Lbcu;

    invoke-static {p0, p1}, Lbcu;->a(Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;Landroid/os/Parcel;)V

    return-void
.end method
