.class public Lcom/google/android/gms/location/places/AutocompleteFilter;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbrt;


# instance fields
.field private final aHM:Z

.field private final aHN:Ljava/util/List;

.field public final auB:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbrt;

    invoke-direct {v0}, Lbrt;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/places/AutocompleteFilter;->CREATOR:Lbrt;

    return-void
.end method

.method public constructor <init>(IZLjava/util/List;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/location/places/AutocompleteFilter;->auB:I

    iput-boolean p2, p0, Lcom/google/android/gms/location/places/AutocompleteFilter;->aHM:Z

    iput-object p3, p0, Lcom/google/android/gms/location/places/AutocompleteFilter;->aHN:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final Ah()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/location/places/AutocompleteFilter;->aHM:Z

    return v0
.end method

.method public final Ai()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/AutocompleteFilter;->aHN:Ljava/util/List;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/location/places/AutocompleteFilter;->CREATOR:Lbrt;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/location/places/AutocompleteFilter;->CREATOR:Lbrt;

    invoke-static {p0, p1}, Lbrt;->a(Lcom/google/android/gms/location/places/AutocompleteFilter;Landroid/os/Parcel;)V

    return-void
.end method
