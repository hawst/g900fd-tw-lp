.class public Lcom/google/android/gms/internal/st$c;
.super Ljava/lang/Object;

# interfaces
.implements Lbho;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbpj;


# instance fields
.field public aHe:[Lcom/google/android/gms/internal/st$a;

.field public aHf:J

.field public aHg:J

.field public aHh:J

.field public final auB:I

.field public avd:Lcom/google/android/gms/common/api/Status;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbpj;

    invoke-direct {v0}, Lbpj;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/st$c;->CREATOR:Lbpj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/st$c;->auB:I

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/common/api/Status;[Lcom/google/android/gms/internal/st$a;JJJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/st$c;->auB:I

    iput-object p2, p0, Lcom/google/android/gms/internal/st$c;->avd:Lcom/google/android/gms/common/api/Status;

    iput-object p3, p0, Lcom/google/android/gms/internal/st$c;->aHe:[Lcom/google/android/gms/internal/st$a;

    iput-wide p4, p0, Lcom/google/android/gms/internal/st$c;->aHf:J

    iput-wide p6, p0, Lcom/google/android/gms/internal/st$c;->aHg:J

    iput-wide p8, p0, Lcom/google/android/gms/internal/st$c;->aHh:J

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/st$c;->CREATOR:Lbpj;

    const/4 v0, 0x0

    return v0
.end method

.method public final wZ()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/st$c;->avd:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/st$c;->CREATOR:Lbpj;

    invoke-static {p0, p1, p2}, Lbpj;->a(Lcom/google/android/gms/internal/st$c;Landroid/os/Parcel;I)V

    return-void
.end method
