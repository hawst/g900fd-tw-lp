.class public Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbdn;


# instance fields
.field public final auB:I

.field public final avg:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

.field public final avh:[Lcom/google/android/gms/appdatasearch/k;

.field public final enabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbdn;

    invoke-direct {v0}, Lbdn;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->CREATOR:Lbdn;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;[Lcom/google/android/gms/appdatasearch/k;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->auB:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->avg:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->avh:[Lcom/google/android/gms/appdatasearch/k;

    iput-boolean p4, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->enabled:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;ZLjava/util/Map;)V
    .locals 2

    const/4 v0, 0x1

    invoke-static {p3}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->c(Ljava/util/Map;)[Lcom/google/android/gms/appdatasearch/k;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1, p2}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;-><init>(ILcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;[Lcom/google/android/gms/appdatasearch/k;Z)V

    return-void
.end method

.method private static c(Ljava/util/Map;)[Lcom/google/android/gms/appdatasearch/k;
    .locals 6

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    new-array v3, v0, [Lcom/google/android/gms/appdatasearch/k;

    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v5, Lcom/google/android/gms/appdatasearch/k;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lbjr;->ap(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lbjr;->ap(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appdatasearch/Feature;

    invoke-direct {v5, v1, v0}, Lcom/google/android/gms/appdatasearch/k;-><init>(Ljava/lang/String;[Lcom/google/android/gms/appdatasearch/Feature;)V

    aput-object v5, v3, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v3

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->CREATOR:Lbdn;

    const/4 v0, 0x0

    return v0
.end method

.method public final eJ(Ljava/lang/String;)[Lcom/google/android/gms/appdatasearch/Feature;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->avh:[Lcom/google/android/gms/appdatasearch/k;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->avh:[Lcom/google/android/gms/appdatasearch/k;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->avh:[Lcom/google/android/gms/appdatasearch/k;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/google/android/gms/appdatasearch/k;->auC:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->avh:[Lcom/google/android/gms/appdatasearch/k;

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/k;->avn:[Lcom/google/android/gms/appdatasearch/Feature;

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->CREATOR:Lbdn;

    invoke-static {p0, p1, p2}, Lbdn;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;Landroid/os/Parcel;I)V

    return-void
.end method

.method public final xa()Ljava/util/Set;
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->avh:[Lcom/google/android/gms/appdatasearch/k;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/util/HashSet;

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->avh:[Lcom/google/android/gms/appdatasearch/k;

    array-length v0, v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->avh:[Lcom/google/android/gms/appdatasearch/k;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->avh:[Lcom/google/android/gms/appdatasearch/k;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/android/gms/appdatasearch/k;->auC:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
