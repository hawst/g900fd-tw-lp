.class public Lcom/google/android/handsfree/MessageContents;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private aOs:Ljava/lang/String;

.field private aOt:Z

.field private aOu:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    new-instance v0, Lbxx;

    invoke-direct {v0}, Lbxx;-><init>()V

    sput-object v0, Lcom/google/android/handsfree/MessageContents;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/handsfree/MessageContents;->aOu:Z

    .line 40
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/handsfree/MessageContents;->aOs:Ljava/lang/String;

    .line 45
    iput-boolean p2, p0, Lcom/google/android/handsfree/MessageContents;->aOu:Z

    .line 46
    return-void
.end method


# virtual methods
.method public final BA()V
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/handsfree/MessageContents;->aOu:Z

    .line 102
    return-void
.end method

.method public final Bx()Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 75
    iget-object v1, p0, Lcom/google/android/handsfree/MessageContents;->aOs:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    const-string v1, "MessageContents"

    const-string v2, "Next notification not available. Perhaps it was already retrieved?"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :goto_0
    return-object v0

    .line 80
    :cond_0
    iget-object v1, p0, Lcom/google/android/handsfree/MessageContents;->aOs:Ljava/lang/String;

    .line 81
    iput-object v0, p0, Lcom/google/android/handsfree/MessageContents;->aOs:Ljava/lang/String;

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/handsfree/MessageContents;->aOt:Z

    move-object v0, v1

    .line 83
    goto :goto_0
.end method

.method public final By()Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/google/android/handsfree/MessageContents;->aOt:Z

    return v0
.end method

.method public final Bz()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/google/android/handsfree/MessageContents;->aOu:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    return v0
.end method

.method public final e(Ljava/lang/String;Z)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 49
    iget-object v2, p0, Lcom/google/android/handsfree/MessageContents;->aOs:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 51
    iput-object p1, p0, Lcom/google/android/handsfree/MessageContents;->aOs:Ljava/lang/String;

    .line 53
    if-nez p2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/handsfree/MessageContents;->aOu:Z

    .line 70
    :goto_1
    return p2

    :cond_0
    move v0, v1

    .line 53
    goto :goto_0

    .line 58
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/handsfree/MessageContents;->aOs:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/handsfree/MessageContents;->aOs:Ljava/lang/String;

    .line 60
    iget-boolean v2, p0, Lcom/google/android/handsfree/MessageContents;->aOu:Z

    if-eqz v2, :cond_2

    if-eqz p2, :cond_2

    move v2, v0

    .line 69
    :goto_2
    iget-boolean v3, p0, Lcom/google/android/handsfree/MessageContents;->aOu:Z

    if-nez p2, :cond_3

    :goto_3
    and-int/2addr v0, v3

    iput-boolean v0, p0, Lcom/google/android/handsfree/MessageContents;->aOu:Z

    move p2, v2

    .line 70
    goto :goto_1

    :cond_2
    move v2, v1

    .line 60
    goto :goto_2

    :cond_3
    move v0, v1

    .line 69
    goto :goto_3
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 106
    if-nez p1, :cond_1

    .line 109
    :cond_0
    :goto_0
    return v0

    .line 107
    :cond_1
    instance-of v1, p1, Lcom/google/android/handsfree/MessageContents;

    if-eqz v1, :cond_0

    .line 108
    check-cast p1, Lcom/google/android/handsfree/MessageContents;

    .line 109
    iget-boolean v1, p1, Lcom/google/android/handsfree/MessageContents;->aOu:Z

    iget-boolean v2, p0, Lcom/google/android/handsfree/MessageContents;->aOu:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p1, Lcom/google/android/handsfree/MessageContents;->aOs:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/handsfree/MessageContents;->aOs:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 115
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/handsfree/MessageContents;->aOs:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/handsfree/MessageContents;->aOu:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/handsfree/MessageContents;->aOs:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 126
    iget-boolean v0, p0, Lcom/google/android/handsfree/MessageContents;->aOu:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 127
    return-void

    .line 126
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
