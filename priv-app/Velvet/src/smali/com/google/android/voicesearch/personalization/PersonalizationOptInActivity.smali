.class public Lcom/google/android/voicesearch/personalization/PersonalizationOptInActivity;
.super Landroid/app/Activity;
.source "PG"


# instance fields
.field private duk:Lhxn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    .line 37
    new-instance v1, Lhxn;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    invoke-direct {v1, p0, v0}, Lhxn;-><init>(Landroid/content/Context;Lhym;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/personalization/PersonalizationOptInActivity;->duk:Lhxn;

    .line 38
    return-void
.end method

.method protected onResume()V
    .locals 13

    .prologue
    const v12, 0x104000a

    const/high16 v11, 0x1040000

    const/16 v10, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 42
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 43
    const/4 v0, 0x2

    .line 44
    invoke-virtual {p0}, Lcom/google/android/voicesearch/personalization/PersonalizationOptInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "PERSONALIZATION_OPT_IN_ENABLE"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 45
    invoke-virtual {p0}, Lcom/google/android/voicesearch/personalization/PersonalizationOptInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "PERSONALIZATION_OPT_IN_ENABLE"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 47
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    move v3, v0

    .line 51
    :goto_1
    iget-object v5, p0, Lcom/google/android/voicesearch/personalization/PersonalizationOptInActivity;->duk:Lhxn;

    new-instance v6, Lhxs;

    invoke-direct {v6, p0}, Lhxs;-><init>(Lcom/google/android/voicesearch/personalization/PersonalizationOptInActivity;)V

    new-instance v7, Landroid/view/ContextThemeWrapper;

    iget-object v0, v5, Lhxn;->mContext:Landroid/content/Context;

    const v4, 0x103006f

    invoke-direct {v7, v0, v4}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    new-instance v8, Landroid/app/AlertDialog$Builder;

    invoke-direct {v8, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0a0833

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const-string v0, "layout_inflater"

    invoke-virtual {v7, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    packed-switch v3, :pswitch_data_0

    const-string v0, "PersonalizationOptInActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown dialog "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    .line 68
    :goto_2
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 69
    return-void

    :cond_0
    move v0, v2

    .line 47
    goto :goto_0

    .line 51
    :pswitch_0
    const v1, 0x7f0a0838

    invoke-virtual {v7, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v4, v5, Lhxn;->mSettings:Lhym;

    invoke-virtual {v4}, Lhym;->aER()Ljze;

    move-result-object v4

    iget-object v4, v4, Ljze;->eNl:Ljzl;

    invoke-virtual {v4}, Ljzl;->bxb()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lerr;->aE(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v4, 0x7f0a083b

    new-instance v9, Lhxq;

    invoke-direct {v9, v5, v7, v3}, Lhxq;-><init>(Lhxn;Landroid/content/Context;I)V

    invoke-virtual {v8, v4, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v4, 0x7f0a083c

    new-instance v9, Lhxp;

    invoke-direct {v9, v5, v3}, Lhxp;-><init>(Lhxn;I)V

    invoke-virtual {v8, v4, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-object v3, v1

    move v1, v2

    :goto_3
    const v4, 0x7f0400ed

    const/4 v9, 0x0

    invoke-virtual {v0, v4, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    if-nez v1, :cond_1

    const v0, 0x7f1102ae

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    const v0, 0x7f1101fb

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-static {v1, v5}, Lcom/google/android/voicesearch/ui/URLObservableSpan;->a(Landroid/text/Spanned;Libn;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const v0, 0x7f1102af

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v2, :cond_2

    const v1, 0x7f0a0839

    invoke-virtual {v7, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v5, Lhxn;->mSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aER()Ljze;

    move-result-object v2

    iget-object v2, v2, Ljze;->eNc:Ljzu;

    invoke-virtual {v2}, Ljzu;->bxu()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lerr;->aF(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-static {v1, v5}, Lcom/google/android/voicesearch/ui/URLObservableSpan;->a(Landroid/text/Spanned;Libn;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :goto_4
    invoke-virtual {v8, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto/16 :goto_2

    :pswitch_1
    const v4, 0x7f0a0838

    invoke-virtual {v7, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v9, v5, Lhxn;->mSettings:Lhym;

    invoke-virtual {v9}, Lhym;->aER()Ljze;

    move-result-object v9

    iget-object v9, v9, Ljze;->eNl:Ljzl;

    invoke-virtual {v9}, Ljzl;->bxb()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v9}, Lerr;->aE(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v9, Lhxq;

    invoke-direct {v9, v5, v7, v3}, Lhxq;-><init>(Lhxn;Landroid/content/Context;I)V

    invoke-virtual {v8, v12, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v9, Lhxp;

    invoke-direct {v9, v5, v3}, Lhxp;-><init>(Lhxn;I)V

    invoke-virtual {v8, v11, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-object v3, v4

    goto/16 :goto_3

    :pswitch_2
    const v2, 0x7f0a083a

    invoke-virtual {v7, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v4, v5, Lhxn;->mSettings:Lhym;

    invoke-virtual {v4}, Lhym;->aER()Ljze;

    move-result-object v4

    iget-object v4, v4, Ljze;->eNc:Ljzu;

    invoke-virtual {v4}, Ljzu;->bxt()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lerr;->aE(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Lhxp;

    invoke-direct {v4, v5, v3}, Lhxp;-><init>(Lhxn;I)V

    invoke-virtual {v8, v12, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v4, Lhxq;

    invoke-direct {v4, v5, v7, v3}, Lhxq;-><init>(Lhxn;Landroid/content/Context;I)V

    invoke-virtual {v8, v11, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-object v3, v2

    move v2, v1

    goto/16 :goto_3

    :cond_2
    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    :cond_3
    move v3, v0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
