.class public Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;
.super Landroid/app/Activity;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lhym;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 111
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 112
    new-instance v6, Landroid/content/ComponentName;

    const-class v0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;

    invoke-direct {v6, p0, v0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 113
    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v0

    if-eq v0, v3, :cond_2

    move v0, v1

    .line 115
    :goto_0
    invoke-virtual {p1}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    const-string v7, "en"

    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    :cond_0
    const-string v4, "android.intent.action.VOICE_COMMAND"

    invoke-static {p0, v4}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->y(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "android.speech.action.VOICE_SEARCH_HANDS_FREE"

    invoke-static {p0, v4}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->y(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v4, v2

    .line 116
    :goto_1
    if-eq v0, v4, :cond_1

    .line 117
    if-eqz v4, :cond_4

    move v0, v2

    :goto_2
    invoke-virtual {v5, v6, v0, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 121
    const-string v1, "HandsFreeActivity"

    if-eqz v4, :cond_5

    const-string v0, "Enabling voice dialer"

    :goto_3
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 125
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 113
    goto :goto_0

    :cond_3
    move v4, v1

    .line 115
    goto :goto_1

    :cond_4
    move v0, v3

    .line 117
    goto :goto_2

    .line 121
    :cond_5
    const-string v0, "Disabling voice dialer"

    goto :goto_3
.end method

.method private static y(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 145
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x10000

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 147
    if-nez v0, :cond_0

    move v0, v1

    .line 168
    :goto_0
    return v0

    .line 151
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 152
    if-eqz v0, :cond_1

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v3, :cond_1

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v3, :cond_1

    .line 158
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 164
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 168
    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 64
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 68
    if-eqz v3, :cond_1

    const-string v2, "android.speech.action.VOICE_SEARCH_HANDS_FREE"

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.intent.action.VOICE_COMMAND"

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v1

    .line 71
    :goto_0
    if-nez v2, :cond_2

    .line 73
    const-string v1, "HandsFreeActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "HandsFreeActivity started with an invalid intent: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Leor;->e(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->finish()V

    .line 104
    :goto_1
    return-void

    :cond_1
    move v2, v0

    .line 68
    goto :goto_0

    .line 78
    :cond_2
    sget-object v2, Lcgg;->aVt:Lcgg;

    invoke-virtual {v2}, Lcgg;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 79
    const-string v2, "com.google.android.voicesearch.PICKER_START"

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 83
    const-string v1, "HandsFreeActivity"

    const-string v2, "Onboarding flow started a query. Returning success!"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 84
    const/16 v0, 0x2a

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->setResult(I)V

    .line 85
    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->finish()V

    goto :goto_1

    .line 90
    :cond_3
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    invoke-virtual {v2}, Lgql;->aJq()Lhhq;

    move-result-object v2

    .line 91
    new-instance v4, Ldlu;

    sget-object v5, Lcgg;->aVt:Lcgg;

    invoke-virtual {v5}, Lcgg;->isEnabled()Z

    move-result v5

    if-nez v5, :cond_4

    sget-object v5, Lcgg;->aVv:Lcgg;

    invoke-virtual {v5}, Lcgg;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_4
    move v0, v1

    :cond_5
    invoke-direct {v4, v0}, Ldlu;-><init>(Z)V

    .line 93
    new-instance v0, Ldmh;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v0, v5}, Ldmh;-><init>(Landroid/content/Context;)V

    .line 94
    const-string v5, "android.intent.action.VOICE_COMMAND"

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 97
    :goto_2
    iget-object v2, v2, Lhhq;->mSettings:Lhym;

    invoke-virtual {v0}, Ldmh;->adq()I

    move-result v0

    invoke-virtual {v4, v1, v2, v0}, Ldlu;->a(ILhym;I)Z

    move-result v0

    .line 99
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 100
    const-class v2, Lcom/google/android/voicesearch/handsfree/HeadsetQueryCommitService;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 101
    const-string v2, "com.google.android.voicesearch.handsfree.HandsFreeService.extra.QUERY_ALLOWED"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 102
    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 103
    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivity;->finish()V

    goto :goto_1

    .line 94
    :cond_6
    const/4 v1, 0x2

    goto :goto_2
.end method
