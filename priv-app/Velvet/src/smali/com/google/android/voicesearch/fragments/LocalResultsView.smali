.class public Lcom/google/android/voicesearch/fragments/LocalResultsView;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private djh:Landroid/view/ViewGroup;

.field private dji:Lcom/google/android/search/shared/ui/WebImageView;

.field private djj:Landroid/view/View;

.field public djk:Lcom/google/android/search/shared/ui/TravelModeSpinner;

.field private djl:I

.field private final djm:Ljava/util/List;

.field public djn:Lhkz;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djm:Ljava/util/List;

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djm:Ljava/util/List;

    .line 61
    return-void
.end method

.method private a(Lhkz;)V
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djj:Landroid/view/View;

    new-instance v1, Lhky;

    invoke-direct {v1, p0, p1}, Lhky;-><init>(Lcom/google/android/voicesearch/fragments/LocalResultsView;Lhkz;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 248
    return-void
.end method

.method private static bK(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 258
    const v0, 0x7f110249

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private static bL(Landroid/view/View;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 262
    const v0, 0x7f110248

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private static bM(Landroid/view/View;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 267
    const v0, 0x7f11024d

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public final a(Ljpe;Lhkz;Lesm;Landroid/view/LayoutInflater;Z)V
    .locals 17
    .param p1    # Ljpe;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lhkz;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lesm;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Landroid/view/LayoutInflater;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 89
    invoke-static/range {p1 .. p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    invoke-static/range {p2 .. p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    invoke-static/range {p3 .. p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    invoke-static/range {p4 .. p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djn:Lhkz;

    .line 94
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djk:Lcom/google/android/search/shared/ui/TravelModeSpinner;

    new-instance v3, Lhkt;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v3, v0, v1}, Lhkt;-><init>(Lcom/google/android/voicesearch/fragments/LocalResultsView;Lhkz;)V

    invoke-virtual {v2, v3}, Lcom/google/android/search/shared/ui/TravelModeSpinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 101
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djk:Lcom/google/android/search/shared/ui/TravelModeSpinner;

    new-instance v3, Lhku;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v3, v0, v1}, Lhku;-><init>(Lcom/google/android/voicesearch/fragments/LocalResultsView;Lhkz;)V

    invoke-virtual {v2, v3}, Lcom/google/android/search/shared/ui/TravelModeSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 114
    invoke-virtual/range {p1 .. p1}, Ljpe;->oY()I

    move-result v7

    .line 116
    move-object/from16 v0, p1

    iget-object v2, v0, Ljpe;->exC:[Ljpd;

    array-length v2, v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_6

    const/4 v2, 0x1

    move v3, v2

    .line 117
    :goto_0
    invoke-static {v7}, Ldye;->gU(I)I

    move-result v8

    .line 118
    invoke-static {v7}, Ldye;->gT(I)I

    move-result v9

    .line 120
    const/4 v2, 0x0

    .line 121
    invoke-virtual/range {p1 .. p1}, Ljpe;->brS()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 122
    invoke-virtual/range {p1 .. p1}, Ljpe;->brR()[B

    move-result-object v4

    .line 123
    const/4 v5, 0x0

    array-length v6, v4

    invoke-static {v4, v5, v6}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 124
    if-eqz v4, :cond_0

    .line 125
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->dji:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-virtual {v2, v4}, Lcom/google/android/search/shared/ui/WebImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 126
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/fragments/LocalResultsView;->a(Lhkz;)V

    .line 127
    const/4 v2, 0x1

    .line 130
    :cond_0
    if-nez v2, :cond_1

    .line 131
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->dji:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-virtual/range {p1 .. p1}, Ljpe;->brQ()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v2, v4, v0}, Lcom/google/android/search/shared/ui/WebImageView;->a(Landroid/net/Uri;Lesm;)V

    .line 132
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/fragments/LocalResultsView;->a(Lhkz;)V

    .line 135
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djk:Lcom/google/android/search/shared/ui/TravelModeSpinner;

    invoke-virtual/range {p1 .. p1}, Ljpe;->brT()I

    move-result v4

    invoke-virtual {v2, v7, v4}, Lcom/google/android/search/shared/ui/TravelModeSpinner;->aE(II)V

    .line 137
    invoke-virtual/range {p1 .. p1}, Ljpe;->oY()I

    move-result v2

    const/4 v4, 0x4

    if-ne v2, v4, :cond_7

    const/4 v2, 0x1

    move v4, v2

    .line 139
    :goto_1
    const/4 v2, 0x0

    move v5, v2

    :goto_2
    move-object/from16 v0, p1

    iget-object v2, v0, Ljpe;->exC:[Ljpd;

    array-length v2, v2

    if-ge v5, v2, :cond_d

    .line 140
    move-object/from16 v0, p1

    iget-object v2, v0, Ljpe;->exC:[Ljpd;

    aget-object v10, v2, v5

    .line 141
    if-eqz v4, :cond_8

    invoke-virtual {v10}, Ljpd;->Ar()Ljava/lang/String;

    move-result-object v2

    move-object v6, v2

    .line 142
    :goto_3
    invoke-virtual {v10}, Ljpd;->getTitle()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10}, Ljpd;->getAddress()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Lhkv;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v13, v0, v1, v10}, Lhkv;-><init>(Lcom/google/android/voicesearch/fragments/LocalResultsView;Lhkz;Ljpd;)V

    new-instance v14, Lhkw;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v14, v0, v1, v10}, Lhkw;-><init>(Lcom/google/android/voicesearch/fragments/LocalResultsView;Lhkz;Ljpd;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/fragments/LocalResultsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v10, 0x7f0f0043

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djl:I

    array-length v15, v10

    if-ge v2, v15, :cond_4

    if-eqz p5, :cond_9

    const v2, 0x7f0400b7

    :goto_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djh:Landroid/view/ViewGroup;

    const/16 v16, 0x0

    move-object/from16 v0, p4

    move/from16 v1, v16

    invoke-virtual {v0, v2, v15, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v15

    const v2, 0x7f11024a

    invoke-virtual {v15, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f11024b

    invoke-virtual {v15, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f11024c

    invoke-virtual {v15, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_a

    :cond_2
    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_5
    invoke-static {v15}, Lcom/google/android/voicesearch/fragments/LocalResultsView;->bL(Landroid/view/View;)Landroid/widget/TextView;

    move-result-object v2

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djl:I

    aget-object v6, v10, v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v13}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {v15}, Lcom/google/android/voicesearch/fragments/LocalResultsView;->bM(Landroid/view/View;)Landroid/widget/ImageView;

    move-result-object v6

    if-eqz v6, :cond_3

    if-eqz v3, :cond_b

    const/4 v2, 0x0

    :goto_6
    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    if-eqz v3, :cond_3

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/fragments/LocalResultsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    const/4 v2, 0x4

    if-ne v7, v2, :cond_3

    const v2, 0x7f0c006e

    invoke-static {v6, v2}, Lehd;->r(Landroid/view/View;I)V

    :cond_3
    invoke-static {v15}, Lcom/google/android/voicesearch/fragments/LocalResultsView;->bK(Landroid/view/View;)Landroid/view/View;

    move-result-object v2

    const/4 v6, 0x4

    if-ne v7, v6, :cond_c

    const v6, 0x7f0c00d8

    invoke-static {v2, v6}, Lehd;->r(Landroid/view/View;I)V

    new-instance v6, Lhkx;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v14}, Lhkx;-><init>(Lcom/google/android/voicesearch/fragments/LocalResultsView;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djh:Landroid/view/ViewGroup;

    invoke-virtual {v2, v15}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djm:Ljava/util/List;

    invoke-interface {v2, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djl:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djl:I

    .line 157
    :cond_4
    move-object/from16 v0, p1

    iget-object v2, v0, Ljpe;->exC:[Ljpd;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v5, v2, :cond_5

    .line 158
    const v2, 0x7f0400b5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djh:Landroid/view/ViewGroup;

    const/4 v10, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v6, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djh:Landroid/view/ViewGroup;

    invoke-virtual {v6, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djm:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    :cond_5
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto/16 :goto_2

    .line 116
    :cond_6
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_0

    .line 137
    :cond_7
    const/4 v2, 0x0

    move v4, v2

    goto/16 :goto_1

    .line 141
    :cond_8
    const-string v2, ""

    move-object v6, v2

    goto/16 :goto_3

    .line 142
    :cond_9
    const v2, 0x7f0400b6

    goto/16 :goto_4

    :cond_a
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_b
    const/16 v2, 0x8

    goto/16 :goto_6

    :cond_c
    invoke-virtual {v2, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_7

    .line 161
    :cond_d
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 65
    const v0, 0x7f11024f

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/LocalResultsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->dji:Lcom/google/android/search/shared/ui/WebImageView;

    .line 66
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->dji:Lcom/google/android/search/shared/ui/WebImageView;

    new-instance v1, Lhks;

    invoke-direct {v1, p0}, Lhks;-><init>(Lcom/google/android/voicesearch/fragments/LocalResultsView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/WebImageView;->a(Ledl;)V

    .line 75
    const v0, 0x7f110250

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/LocalResultsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djj:Landroid/view/View;

    .line 76
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djj:Landroid/view/View;

    const v1, 0x7f0c0116

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 78
    const v0, 0x7f11024e

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/LocalResultsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djh:Landroid/view/ViewGroup;

    .line 79
    const v0, 0x7f110251

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/LocalResultsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/TravelModeSpinner;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djk:Lcom/google/android/search/shared/ui/TravelModeSpinner;

    .line 82
    return-void
.end method

.method public final uM()V
    .locals 3

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 222
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djh:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 225
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djl:I

    .line 226
    return-void
.end method

.method public final ux()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 229
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djh:Landroid/view/ViewGroup;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 230
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djj:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 231
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djk:Lcom/google/android/search/shared/ui/TravelModeSpinner;

    invoke-virtual {v0, v4}, Lcom/google/android/search/shared/ui/TravelModeSpinner;->setEnabled(Z)V

    .line 232
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/LocalResultsView;->djm:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 233
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f110247

    if-ne v2, v3, :cond_0

    .line 234
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/LocalResultsView;->bK(Landroid/view/View;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 235
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/LocalResultsView;->bL(Landroid/view/View;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 236
    invoke-static {v0}, Lcom/google/android/voicesearch/fragments/LocalResultsView;->bM(Landroid/view/View;)Landroid/widget/ImageView;

    move-result-object v0

    const v2, -0x333334

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setColorFilter(I)V

    goto :goto_0

    .line 239
    :cond_1
    return-void
.end method
