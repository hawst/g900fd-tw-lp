.class public Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;
.super Lhfa;
.source "PG"


# instance fields
.field private asy:Lfll;

.field private cou:Lfjs;

.field private cri:Lizo;

.field private dnr:Lizq;

.field private dns:Ljava/util/List;

.field private dnt:Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;

.field public dnu:Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;

.field public mActivityHelper:Lfzw;

.field private mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lhfa;-><init>()V

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnr:Lizq;

    return-void
.end method

.method private a(Lizq;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 244
    new-instance v0, Lizo;

    invoke-direct {v0}, Lizo;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->cri:Lizo;

    .line 245
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->cri:Lizo;

    iput-object p1, v0, Lizo;->dUQ:Lizq;

    .line 246
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->asy:Lfll;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->cri:Lizo;

    invoke-virtual {v0, v1}, Lfll;->k(Lizo;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dns:Ljava/util/List;

    .line 247
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnt:Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dns:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, p2, v3}, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Ljava/lang/String;Lizj;)V

    .line 248
    return-void
.end method

.method private b(Landroid/content/Intent;Z)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 203
    const-string v0, "rendering_context"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    const-string v0, "rendering_context"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    .line 207
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0512

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 208
    const-string v1, "title"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 209
    const-string v0, "title"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 212
    :goto_0
    const-string v0, "test_file"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 213
    new-instance v4, Lizq;

    invoke-direct {v4}, Lizq;-><init>()V

    .line 214
    const-string v0, "test_file"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/io/File;

    invoke-virtual {p0, v3}, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    const-string v6, "TestData"

    invoke-direct {v2, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {v4, v2}, Leqh;->a(Ljsr;Ljava/io/InputStream;)Ljsr;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    .line 215
    :goto_1
    invoke-direct {p0, v4, v1}, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->a(Lizq;Ljava/lang/String;)V

    .line 223
    :cond_1
    :goto_2
    const-string v0, "expanded_tree"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 224
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "expanded_tree"

    const-class v2, Lizq;

    invoke-static {v0, v1, v2}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizq;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnr:Lizq;

    .line 226
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "scroll_target"

    const-class v2, Lizj;

    invoke-static {v0, v1, v2}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 228
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnr:Lizq;

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->i(Lizq;)Ljava/util/List;

    move-result-object v1

    .line 230
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnu:Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnr:Lizq;

    iget-object v5, v5, Lizq;->dUZ:Lizj;

    iget-object v5, v5, Lizj;->dUx:Liwn;

    invoke-virtual {v5}, Liwn;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v1, v4, v5, v0}, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Ljava/lang/String;Lizj;)V

    .line 232
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnu:Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 234
    if-eqz p2, :cond_2

    .line 235
    invoke-virtual {v0, v3}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 237
    :cond_2
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 241
    :goto_3
    return-void

    .line 214
    :catch_0
    move-exception v0

    move-object v2, v3

    :goto_4
    :try_start_2
    const-string v5, "PersonalSearchActivity"

    const-string v6, "File not found: "

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v0, "File not found.  Check the logs."

    const/4 v5, 0x0

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v2, v3

    :goto_5
    :try_start_3
    const-string v5, "PersonalSearchActivity"

    const-string v6, "IO Exception: "

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v0, "IO Exception.  Check the logs."

    const/4 v5, 0x0

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    move-object v2, v3

    :goto_6
    invoke-static {v2}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0

    .line 216
    :cond_3
    const-string v0, "full_tree"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 217
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "full_tree"

    const-class v4, Lizq;

    invoke-static {v0, v2, v4}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizq;

    .line 219
    invoke-direct {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->a(Lizq;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 239
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnu:Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_3

    .line 214
    :catchall_1
    move-exception v0

    goto :goto_6

    :catch_2
    move-exception v0

    goto :goto_5

    :catch_3
    move-exception v0

    goto :goto_4

    :cond_5
    move-object v1, v0

    goto/16 :goto_0
.end method

.method private i(Lizq;)Ljava/util/List;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 251
    iget-object v0, p1, Lizq;->dUZ:Lizj;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    iget-object v0, p1, Lizq;->dUZ:Lizj;

    invoke-virtual {v0}, Lizj;->getType()I

    move-result v0

    const/16 v2, 0x89

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 254
    iget-object v0, p1, Lizq;->dUX:[Lizj;

    array-length v0, v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 255
    iget-object v3, p1, Lizq;->dUX:[Lizj;

    array-length v4, v3

    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v0, v3, v1

    .line 256
    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->cou:Lfjs;

    invoke-interface {v5, v0}, Lfjs;->r(Lizj;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkd;

    .line 257
    if-eqz v0, :cond_0

    .line 258
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 252
    goto :goto_0

    .line 261
    :cond_2
    return-object v2
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 94
    invoke-super {p0, p1}, Lhfa;->onCreate(Landroid/os/Bundle;)V

    .line 95
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lfdb;->axW()Lfzw;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->mActivityHelper:Lfzw;

    .line 98
    invoke-virtual {v0}, Lfdb;->axP()Lfjs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->cou:Lfjs;

    .line 99
    new-instance v0, Lfll;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->cou:Lfjs;

    invoke-direct {v0, v1}, Lfll;-><init>(Lfjs;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->asy:Lfll;

    .line 100
    new-instance v0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    invoke-direct {v0}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    .line 102
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f1102ab

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnt:Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;

    .line 104
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnt:Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;

    new-instance v1, Lhpg;

    invoke-direct {v1, p0}, Lhpg;-><init>(Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->f(Landroid/view/View$OnClickListener;)V

    .line 120
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f1102ac

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnu:Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;

    .line 122
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnu:Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;

    new-instance v1, Lhpi;

    invoke-direct {v1, p0}, Lhpi;-><init>(Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->f(Landroid/view/View$OnClickListener;)V

    .line 133
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->b(Landroid/content/Intent;Z)V

    .line 136
    if-eqz p1, :cond_0

    const-string v0, "expanded_tree"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    const-string v0, "expanded_tree"

    const-class v1, Lizq;

    invoke-static {p1, v0, v1}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizq;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnr:Lizq;

    .line 139
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnr:Lizq;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->i(Lizq;)Ljava/util/List;

    move-result-object v0

    .line 141
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnu:Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnr:Lizq;

    iget-object v3, v3, Lizq;->dUZ:Lizj;

    iget-object v3, v3, Lizj;->dUx:Liwn;

    invoke-virtual {v3}, Liwn;->getTitle()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Ljava/lang/String;Lizj;)V

    .line 143
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnu:Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 145
    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 149
    invoke-super {p0, p1}, Lhfa;->onNewIntent(Landroid/content/Intent;)V

    .line 150
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->b(Landroid/content/Intent;Z)V

    .line 151
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 86
    invoke-super {p0, p1}, Lhfa;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnu:Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnr:Lizq;

    if-eqz v0, :cond_0

    .line 88
    const-string v0, "expanded_tree"

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->dnr:Lizq;

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 90
    :cond_0
    return-void
.end method

.method protected final wd()I
    .locals 1

    .prologue
    .line 284
    const v0, 0x7f0400ec

    return v0
.end method

.method protected final we()Lfoh;
    .locals 6

    .prologue
    .line 155
    new-instance v0, Lhpj;

    const-string v3, "now_cards"

    new-instance v1, Lgpk;

    invoke-direct {v1, p0}, Lgpk;-><init>(Landroid/content/Context;)V

    const-string v2, "now_cards"

    invoke-virtual {v1, v2}, Lgpk;->nv(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->getContentView()Landroid/view/View;

    move-result-object v5

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lhpj;-><init>(Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;Landroid/app/Activity;Ljava/lang/String;Landroid/net/Uri;Landroid/view/View;)V

    return-object v0
.end method
