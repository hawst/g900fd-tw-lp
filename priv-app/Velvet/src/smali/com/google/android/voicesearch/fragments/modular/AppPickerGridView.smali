.class public Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;
.super Lcom/google/android/voicesearch/ui/ExpandedGridView;
.source "PG"


# instance fields
.field private cgV:Ljava/util/List;

.field private dle:Liai;

.field private dlf:Ledq;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/ui/ExpandedGridView;-><init>(Landroid/content/Context;)V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->cgV:Ljava/util/List;

    .line 31
    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->ed()V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/ui/ExpandedGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->cgV:Ljava/util/List;

    .line 36
    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->ed()V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/ui/ExpandedGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->cgV:Ljava/util/List;

    .line 41
    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->ed()V

    .line 42
    return-void
.end method

.method public static synthetic a(Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;)Ledq;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->dlf:Ledq;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;)Ljava/util/List;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->cgV:Ljava/util/List;

    return-object v0
.end method

.method private ed()V
    .locals 3

    .prologue
    .line 45
    new-instance v0, Liai;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->cgV:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Liai;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->dle:Liai;

    .line 46
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->dle:Liai;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->setChoiceMode(I)V

    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->setVerticalScrollBarEnabled(Z)V

    .line 50
    new-instance v0, Lhmt;

    invoke-direct {v0, p0}, Lhmt;-><init>(Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 58
    return-void
.end method


# virtual methods
.method public final a(Ledq;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->dlf:Ledq;

    .line 69
    return-void
.end method

.method public final a(Ljava/util/List;Lcom/google/android/shared/util/App;)V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->cgV:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 62
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->cgV:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 63
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->dle:Liai;

    invoke-virtual {v0}, Liai;->notifyDataSetChanged()V

    .line 64
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->cgV:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/modular/AppPickerGridView;->setItemChecked(IZ)V

    .line 65
    return-void
.end method
