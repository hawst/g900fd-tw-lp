.class public Lcom/google/android/voicesearch/logger/EventLoggerService;
.super Landroid/app/IntentService;
.source "PG"


# static fields
.field private static volatile dub:Z


# instance fields
.field private dtQ:Ljava/lang/String;

.field private dtR:Ljava/lang/String;

.field private duc:Ljava/lang/String;

.field private dud:Ljava/io/File;

.field private mActionCardEventLogger:Legs;

.field private mClearcutLogger:Lhwv;

.field private final mClock:Lemp;

.field private mGsaConfigFlags:Lchk;

.field private mHttpHelper:Ldkx;

.field private mLogSender:Lglz;

.field private mLogsBackupManager:Lhxl;

.field private mSearchSettings:Lcke;

.field private mSearchUrlHelper:Lcpn;

.field private mSettings:Lhym;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/voicesearch/logger/EventLoggerService;->dub:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 108
    const-string v0, "EventLoggerService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 109
    new-instance v0, Lere;

    invoke-direct {v0, p0}, Lere;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mClock:Lemp;

    .line 110
    return-void
.end method

.method public constructor <init>(Lcpn;Ldkx;Lhwv;Lchk;Lglz;Lhxl;Lhym;)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;-><init>()V

    .line 120
    iput-object p2, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mHttpHelper:Ldkx;

    .line 121
    iput-object p1, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mSearchUrlHelper:Lcpn;

    .line 122
    iput-object p3, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mClearcutLogger:Lhwv;

    .line 123
    iput-object p4, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mGsaConfigFlags:Lchk;

    .line 124
    iput-object p6, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mLogsBackupManager:Lhxl;

    .line 125
    iput-object p5, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mLogSender:Lglz;

    .line 126
    iput-object p7, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mSettings:Lhym;

    .line 127
    return-void
.end method

.method public static aTp()V
    .locals 1

    .prologue
    .line 418
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/voicesearch/logger/EventLoggerService;->dub:Z

    .line 419
    return-void
.end method

.method public static aTq()V
    .locals 1

    .prologue
    .line 423
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/voicesearch/logger/EventLoggerService;->dub:Z

    .line 424
    return-void
.end method

.method private ax(Ljava/util/List;)V
    .locals 6

    .prologue
    .line 406
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljvi;

    .line 407
    iget-object v2, v0, Ljvi;->eHp:[Litu;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 408
    invoke-virtual {v4}, Litu;->getEventType()I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_2

    .line 409
    iget-object v0, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aUr()V

    .line 414
    :cond_1
    return-void

    .line 407
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static bP(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 427
    sget-boolean v0, Lcom/google/android/voicesearch/logger/EventLoggerService;->dub:Z

    if-nez v0, :cond_0

    .line 429
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 431
    invoke-static {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->bR(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 433
    :cond_0
    return-void
.end method

.method public static bQ(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 436
    sget-boolean v0, Lcom/google/android/voicesearch/logger/EventLoggerService;->dub:Z

    if-nez v0, :cond_0

    .line 438
    invoke-static {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->bR(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 440
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 443
    const/4 v2, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const-wide/16 v6, 0x2710

    add-long/2addr v4, v6

    invoke-virtual {v0, v2, v4, v5, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 446
    :cond_0
    return-void
.end method

.method private static bR(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 449
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/voicesearch/logger/EventLoggerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 450
    const-string v1, "send_events"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 452
    const/4 v1, 0x0

    const/high16 v2, 0x10000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a(Ljyr;Z)V
    .locals 5
    .param p1    # Ljyr;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 319
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Ljyr;->dT(J)Ljyr;

    .line 322
    iget-object v0, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mSearchUrlHelper:Lcpn;

    invoke-virtual {v0, p1}, Lcpn;->a(Ljyr;)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    .line 323
    new-instance v1, Ldlb;

    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->getHeaders()Ljava/util/Map;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ldlb;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    .line 326
    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mHttpHelper:Ldkx;

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Ldkx;->b(Ldlb;I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 333
    :goto_0
    if-eqz p2, :cond_0

    .line 334
    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    .line 335
    iget-object v1, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->dud:Ljava/io/File;

    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    array-length v3, v0

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v2, v0}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 337
    :cond_0
    :goto_1
    return-void

    .line 329
    :catch_0
    move-exception v0

    const/16 v0, 0x11d

    invoke-static {v0}, Lege;->ht(I)V

    .line 330
    const-string v0, "EventLoggerService"

    const-string v1, "Could not log the ACLE batch"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 335
    :catch_1
    move-exception v0

    const-string v1, "EventLoggerService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "File not found: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v1, "EventLoggerService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not write to file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method protected final h(Ljava/util/List;Z)V
    .locals 5

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->JI()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 368
    iget-object v0, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mLogsBackupManager:Lhxl;

    invoke-virtual {v0}, Lhxl;->aTr()Ljava/util/List;

    move-result-object v1

    .line 371
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxm;

    .line 372
    iget-object v0, v0, Lhxm;->duf:Ljvi;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 378
    :cond_0
    if-eqz p2, :cond_1

    .line 379
    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mLogSender:Lglz;

    invoke-interface {v0, v1}, Lglz;->am(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 386
    :cond_1
    :goto_1
    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/logger/EventLoggerService;->ax(Ljava/util/List;)V

    .line 390
    :cond_2
    return-void

    .line 381
    :catch_0
    move-exception v0

    .line 382
    const-string v2, "EventLoggerService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to send logs "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 384
    iget-object v0, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mLogsBackupManager:Lhxl;

    invoke-virtual {v0, v1}, Lhxl;->ay(Ljava/util/List;)V

    goto :goto_1
.end method

.method public onCreate()V
    .locals 6

    .prologue
    .line 132
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 134
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v1

    .line 137
    iget-object v2, v1, Lhhq;->mSettings:Lhym;

    iput-object v2, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mSettings:Lhym;

    .line 138
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BK()Lcke;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mSearchSettings:Lcke;

    .line 139
    new-instance v2, Lgmc;

    iget-object v3, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mSettings:Lhym;

    iget-object v1, v1, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->Ek()Lccz;

    move-result-object v1

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v4

    invoke-virtual {v4}, Lcfo;->DL()Lcrh;

    move-result-object v4

    iget-object v5, v0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v5}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v5

    invoke-direct {v2, v3, v1, v4, v5}, Lgmc;-><init>(Lgdo;Lccz;Lgln;Ljava/util/concurrent/ExecutorService;)V

    iput-object v2, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mLogSender:Lglz;

    .line 144
    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->aJg()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->dtQ:Ljava/lang/String;

    .line 145
    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->aJh()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->dtR:Ljava/lang/String;

    .line 146
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->BK()Lcke;

    move-result-object v1

    invoke-interface {v1}, Lcke;->Ob()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->duc:Ljava/lang/String;

    .line 149
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->BL()Lchk;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mGsaConfigFlags:Lchk;

    .line 150
    invoke-static {}, Legs;->aoH()Legs;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mActionCardEventLogger:Legs;

    .line 151
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DI()Lcpn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mSearchUrlHelper:Lcpn;

    .line 152
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DG()Ldkx;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mHttpHelper:Ldkx;

    .line 153
    new-instance v1, Lhxl;

    invoke-direct {v1, p0}, Lhxl;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mLogsBackupManager:Lhxl;

    .line 155
    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    const/16 v1, 0x14

    iget-object v2, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mGsaConfigFlags:Lchk;

    invoke-virtual {v2}, Lchk;->JN()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mGsaConfigFlags:Lchk;

    invoke-virtual {v3}, Lchk;->JH()Z

    move-result v3

    invoke-static {p0, v0, v1, v2, v3}, Lhwv;->a(Landroid/content/Context;Ljava/util/concurrent/Executor;ILjava/lang/String;Z)Lhwv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mClearcutLogger:Lhwv;

    .line 162
    iget-object v0, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mClearcutLogger:Lhwv;

    invoke-virtual {v0}, Lhwv;->start()V

    .line 164
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mClearcutLogger:Lhwv;

    invoke-virtual {v0}, Lhwv;->stop()V

    .line 176
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 177
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 182
    iget-object v0, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->GR()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mActionCardEventLogger:Legs;

    invoke-virtual {v0}, Legs;->aoJ()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyr;

    invoke-virtual {p0, v0, v12}, Lcom/google/android/voicesearch/logger/EventLoggerService;->a(Ljyr;Z)V

    goto :goto_0

    :cond_0
    invoke-static {v1}, Legs;->J(Ljava/util/List;)V

    :cond_1
    new-instance v0, Lhxa;

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mClock:Lemp;

    iget-object v2, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->dtQ:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->dtR:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mSettings:Lhym;

    invoke-virtual {v4}, Lhym;->aFb()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "input_method"

    invoke-virtual {p0, v6}, Lcom/google/android/voicesearch/logger/EventLoggerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/inputmethod/InputMethodManager;

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v12}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mSettings:Lhym;

    invoke-virtual {v7}, Lhym;->aTH()Z

    move-result v7

    if-eqz v7, :cond_6

    move v7, v12

    :goto_1
    iget-object v8, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mSettings:Lhym;

    invoke-virtual {v8}, Lhym;->aUp()Ljava/util/List;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mGsaConfigFlags:Lchk;

    iget-object v10, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mSearchSettings:Lcke;

    invoke-interface {v10}, Lcke;->ND()Ljava/lang/String;

    move-result-object v10

    invoke-static {}, Lege;->aov()Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {v0 .. v11}, Lhxa;-><init>(Lemp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/util/List;Lchk;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Legj;->aox()Legn;

    move-result-object v1

    invoke-virtual {v1}, Legn;->aoC()Legp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhxa;->a(Legp;)Ljava/util/List;

    move-result-object v0

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Landroid/app/ActivityManager;->isRunningInTestHarness()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const-string v1, "forceEmulatorServerLogs"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    move v12, v13

    :cond_4
    invoke-virtual {p0, v0, v12}, Lcom/google/android/voicesearch/logger/EventLoggerService;->h(Ljava/util/List;Z)V

    iget-object v1, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->JH()Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v1, Lhwu;

    iget-object v2, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->duc:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mGsaConfigFlags:Lchk;

    iget-object v4, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mSearchSettings:Lcke;

    invoke-direct {v1, v2, v3, v4}, Lhwu;-><init>(Ljava/lang/String;Lchk;Lcke;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxm;

    iget-object v3, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mClearcutLogger:Lhwv;

    iget-object v4, v0, Lhxm;->bbH:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lhwv;->os(Ljava/lang/String;)V

    iget-object v3, v0, Lhxm;->duf:Ljvi;

    iget-object v0, v0, Lhxm;->dug:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Lhwu;->a(Ljvi;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyt;

    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/voicesearch/logger/EventLoggerService;->mClearcutLogger:Lhwv;

    invoke-virtual {v4, v0}, Lhwv;->Y([B)V

    goto :goto_2

    :cond_6
    move v7, v13

    goto/16 :goto_1

    .line 184
    :cond_7
    return-void
.end method
