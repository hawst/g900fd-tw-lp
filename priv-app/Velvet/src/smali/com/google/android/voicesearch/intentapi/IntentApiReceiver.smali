.class public Lcom/google/android/voicesearch/intentapi/IntentApiReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method protected static ak(Landroid/content/Intent;)Landroid/os/Bundle;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 47
    const-string v0, "android.speech.action.GET_LANGUAGE_DETAILS"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 48
    const/4 v0, 0x0

    .line 70
    :cond_0
    :goto_0
    return-object v0

    .line 51
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 52
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    invoke-virtual {v1}, Lgql;->aJq()Lhhq;

    move-result-object v1

    iget-object v1, v1, Lhhq;->mSettings:Lhym;

    .line 54
    const-string v2, "android.speech.extra.LANGUAGE_PREFERENCE"

    invoke-virtual {v1}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v2, "android.speech.extra.ONLY_RETURN_LANGUAGE_PREFERENCE"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 61
    const-string v2, "android.speech.extra.SUPPORTED_LANGUAGES"

    invoke-virtual {v1}, Lhym;->aER()Ljze;

    move-result-object v3

    invoke-static {v3}, Lgnq;->c(Ljze;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 65
    const-string v2, "android.speech.extra.SUPPORTED_LANGUAGE_NAMES"

    invoke-virtual {v1}, Lhym;->aER()Ljze;

    move-result-object v1

    invoke-static {v1}, Lgnq;->d(Ljze;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/google/android/voicesearch/intentapi/IntentApiReceiver;->isOrderedBroadcast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 41
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    invoke-static {p2}, Lcom/google/android/voicesearch/intentapi/IntentApiReceiver;->ak(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    .line 38
    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/intentapi/IntentApiReceiver;->setResultExtras(Landroid/os/Bundle;)V

    goto :goto_0
.end method
