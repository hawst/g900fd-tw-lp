.class public Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;
.super Landroid/app/Fragment;
.source "PG"

# interfaces
.implements Lflv;


# instance fields
.field private final anP:Lhpk;

.field private anR:Z

.field private aoA:Landroid/widget/TextView;

.field private asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

.field private asG:Lcom/google/android/shared/ui/CoScrollContainer;

.field private asJ:Lflr;

.field private dnm:Lhpe;

.field private dnn:Landroid/view/View;

.field private dno:I

.field private mActivityHelper:Lfzw;

.field private mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 51
    new-instance v0, Lhpk;

    invoke-direct {v0}, Lhpk;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->anP:Lhpk;

    .line 52
    new-instance v0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    invoke-direct {v0}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    .line 53
    iput-boolean v1, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->anR:Z

    .line 57
    iput v1, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dno:I

    .line 215
    return-void
.end method

.method private aRp()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 195
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dnm:Lhpe;

    if-nez v0, :cond_0

    .line 213
    :goto_0
    return-void

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->aoA:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dnm:Lhpe;

    iget-object v1, v1, Lhpe;->bLI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dnm:Lhpe;

    iget-object v0, v0, Lhpe;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    .line 201
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dnm:Lhpe;

    iget-object v0, v0, Lhpe;->dnq:Lizj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dnm:Lhpe;

    iget-object v0, v0, Lhpe;->dnq:Lizj;

    move-object v7, v0

    .line 204
    :goto_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->asJ:Lflr;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dnm:Lhpe;

    iget-object v1, v1, Lhpe;->dnp:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    new-instance v6, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;

    iget v8, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dno:I

    invoke-direct {v6, v7, v8}, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;-><init>(Lizj;I)V

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dnm:Lhpe;

    iget-object v7, v7, Lhpe;->dnq:Lizj;

    if-eqz v7, :cond_2

    move v7, v3

    :goto_2
    move v8, v4

    invoke-virtual/range {v0 .. v8}, Lflr;->a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;ZZLjava/util/List;Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;ZZ)V

    .line 212
    iput-object v5, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dnm:Lhpe;

    goto :goto_0

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dnm:Lhpe;

    iget-object v0, v0, Lhpe;->dnp:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkd;

    invoke-interface {v0}, Lfkd;->getEntry()Lizj;

    move-result-object v0

    move-object v7, v0

    goto :goto_1

    :cond_2
    move v7, v4

    .line 204
    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Ljava/lang/String;Lizj;)V
    .locals 1
    .param p4    # Lizj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 188
    new-instance v0, Lhpe;

    invoke-direct {v0, p1, p2, p3, p4}, Lhpe;-><init>(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Ljava/lang/String;Lizj;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dnm:Lhpe;

    .line 189
    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->anR:Z

    if-eqz v0, :cond_0

    .line 190
    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->aRp()V

    .line 192
    :cond_0
    return-void
.end method

.method public final aRo()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 176
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->asJ:Lflr;

    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    const/4 v3, 0x1

    move-object v6, v5

    move v7, v4

    move v8, v4

    invoke-virtual/range {v0 .. v8}, Lflr;->a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;ZZLjava/util/List;Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;ZZ)V

    .line 184
    return-void
.end method

.method public final f(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dnn:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 166
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 167
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->anR:Z

    .line 168
    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->aRp()V

    .line 169
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 67
    const v0, 0x7f0400da

    invoke-virtual {p1, v0, p2, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v11

    .line 68
    const v0, 0x7f110161

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/ui/CoScrollContainer;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    .line 69
    const v0, 0x7f11025e

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/ui/SuggestionGridLayout;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    .line 71
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    .line 72
    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DC()Lemp;

    move-result-object v3

    .line 73
    invoke-virtual {v2}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->ayg()Lfml;

    move-result-object v7

    .line 74
    invoke-virtual {v2}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->axW()Lfzw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->mActivityHelper:Lfzw;

    .line 75
    new-instance v0, Lflr;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, v2, Lgql;->mAsyncServices:Lema;

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->asG:Lcom/google/android/shared/ui/CoScrollContainer;

    new-instance v6, Lhpf;

    iget-object v8, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    invoke-direct {v6, v8, v7}, Lhpf;-><init>(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lfml;)V

    new-instance v7, Lgbr;

    invoke-direct {v7, v3}, Lgbr;-><init>(Lemp;)V

    const/4 v8, 0x0

    move-object v3, p0

    invoke-direct/range {v0 .. v8}, Lflr;-><init>(Landroid/app/Activity;Lerp;Lflv;Lcom/google/android/shared/ui/SuggestionGridLayout;Lekf;Lfmt;Lgbr;Lflb;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->asJ:Lflr;

    .line 84
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->asJ:Lflr;

    invoke-virtual {v0, p3}, Lflr;->V(Landroid/os/Bundle;)V

    .line 86
    const v0, 0x7f1101a9

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->aoA:Landroid/widget/TextView;

    .line 87
    const v0, 0x7f110293

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dnn:Landroid/view/View;

    .line 88
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getPaddingTop()I

    move-result v0

    iput v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dno:I

    .line 89
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0xc000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "status_bar_height"

    const-string v2, "dimen"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v0, 0x7f110292

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v2, v10}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f11006e

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v3, v1

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dnn:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v2, v1

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dnn:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dno:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dno:I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v9, :cond_3

    move v0, v9

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz v0, :cond_4

    const-string v0, "navigation_bar_height"

    :goto_1
    const-string v2, "dimen"

    const-string v3, "android"

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getPaddingLeft()I

    move-result v1

    iget v2, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->dno:I

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v3}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v4}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getPaddingBottom()I

    move-result v4

    add-int/2addr v4, v10

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/shared/ui/SuggestionGridLayout;->setPadding(IIII)V

    .line 90
    :cond_2
    return-object v11

    :cond_3
    move v0, v10

    .line 89
    goto :goto_0

    :cond_4
    const-string v0, "navigation_bar_height_landscape"

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->asJ:Lflr;

    invoke-virtual {v0, p1}, Lflr;->U(Landroid/os/Bundle;)V

    .line 63
    return-void
.end method

.method public final wh()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 232
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 233
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    move-object v0, v3

    .line 234
    :goto_1
    instance-of v4, v0, Landroid/view/ViewGroup;

    if-eqz v4, :cond_2

    instance-of v4, v0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    if-eqz v4, :cond_1

    check-cast v0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-object v4, v0

    .line 235
    :goto_2
    if-eqz v4, :cond_0

    .line 236
    invoke-virtual {v4, v2}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->fa(Z)V

    .line 240
    const v0, 0x7f11000a

    invoke-virtual {v3, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkd;

    .line 242
    instance-of v3, v0, Lfup;

    if-eqz v3, :cond_0

    .line 243
    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->anP:Lhpk;

    invoke-interface {v0}, Lfkd;->aAv()Lizq;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/personal/NowCardsFragment;->mActivityHelper:Lfzw;

    invoke-virtual {v3, v4, v0, v5}, Lhpk;->a(Landroid/view/ViewGroup;Lizq;Lfzw;)V

    .line 232
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 234
    :cond_1
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    move-object v4, v0

    goto :goto_2

    .line 249
    :cond_3
    return-void
.end method
