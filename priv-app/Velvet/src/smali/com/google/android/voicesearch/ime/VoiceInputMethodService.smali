.class public Lcom/google/android/voicesearch/ime/VoiceInputMethodService;
.super Landroid/inputmethodservice/InputMethodService;
.source "PG"


# instance fields
.field private dsk:Lhvi;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/inputmethodservice/InputMethodService;-><init>()V

    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 108
    invoke-super {p0, p1, p2, p3}, Landroid/inputmethodservice/InputMethodService;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->dsk:Lhvi;

    new-instance v1, Landroid/util/PrintWriterPrinter;

    invoke-direct {v1, p2}, Landroid/util/PrintWriterPrinter;-><init>(Ljava/io/PrintWriter;)V

    const-string v2, "VoiceIME state :"

    invoke-interface {v1, v2}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  mDictationResultHandler = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lhvi;->drZ:Lhup;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  mImeLoggerHelper="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lhvi;->drY:Lhuy;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  mVoiceInputViewHelper="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lhvi;->drX:Lhvz;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  mInputViewActive="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, v0, Lhvi;->dse:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  mVoiceLanguageSelector="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lhvi;->mVoiceLanguageSelector:Lhvo;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    .line 110
    return-void
.end method

.method public hideWindow()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 67
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->dsk:Lhvi;

    const-string v1, "VoiceInputMethodManager"

    const-string v2, "#handleHideWindow"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lhvi;->aSM()V

    iget-object v1, v0, Lhvi;->drY:Lhuy;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lhvi;->drY:Lhuy;

    iget-wide v2, v0, Lhuy;->drI:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/16 v1, 0x28

    invoke-static {v1}, Lege;->ht(I)V

    iput-wide v4, v0, Lhuy;->drI:J

    .line 69
    :cond_0
    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->hideWindow()V

    .line 70
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->dsk:Lhvi;

    invoke-virtual {v0, p1}, Lhvi;->b(Landroid/content/res/Configuration;)V

    .line 49
    invoke-super {p0, p1}, Landroid/inputmethodservice/InputMethodService;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 50
    return-void
.end method

.method public onCreate()V
    .locals 18

    .prologue
    .line 27
    invoke-super/range {p0 .. p0}, Landroid/inputmethodservice/InputMethodService;->onCreate()V

    .line 29
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    iget-object v8, v1, Lgql;->mAsyncServices:Lema;

    new-instance v9, Lhuy;

    invoke-direct {v9}, Lhuy;-><init>()V

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    invoke-virtual {v1}, Lgql;->aJq()Lhhq;

    move-result-object v5

    iget-object v4, v5, Lhhq;->mSettings:Lhym;

    invoke-virtual {v5}, Lhhq;->aOZ()Lhik;

    move-result-object v11

    invoke-virtual {v5}, Lhhq;->aPf()Lgel;

    move-result-object v3

    invoke-virtual {v5}, Lhhq;->aPl()Lhvf;

    move-result-object v12

    new-instance v13, Lhvz;

    invoke-virtual {v5}, Lhhq;->aAs()Lequ;

    move-result-object v1

    invoke-virtual {v4}, Lhym;->JW()Z

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v1, v2}, Lhvz;-><init>(Landroid/content/Context;Lequ;Z)V

    const-string v1, "power"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/inputmethodservice/InputMethodService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    new-instance v7, Lhva;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lhva;-><init>(Landroid/content/Context;)V

    new-instance v10, Lhvo;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v4}, Lhvo;-><init>(Landroid/content/Context;Lhym;)V

    new-instance v2, Lhvk;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v1, v5}, Lhvk;-><init>(Landroid/inputmethodservice/InputMethodService;Landroid/os/PowerManager;Lhhq;)V

    new-instance v15, Lhvs;

    invoke-virtual {v8}, Lema;->aus()Leqo;

    move-result-object v1

    invoke-direct {v15, v2, v1}, Lhvs;-><init>(Lhve;Ljava/util/concurrent/Executor;)V

    new-instance v1, Lhup;

    new-instance v5, Lhvt;

    invoke-direct {v5}, Lhvt;-><init>()V

    invoke-virtual {v8}, Lema;->aus()Leqo;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lhup;-><init>(Lhve;Lgel;Lhym;Lhvu;Leqo;)V

    new-instance v5, Lhvi;

    invoke-virtual {v8}, Lema;->aus()Leqo;

    move-result-object v8

    new-instance v17, Lhvd;

    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Lhvd;-><init>(Ljava/lang/Object;)V

    move-object v6, v10

    move-object v10, v4

    move-object v14, v2

    move-object/from16 v16, v1

    invoke-direct/range {v5 .. v17}, Lhvi;-><init>(Lhvo;Lhva;Leqo;Lhuy;Lhym;Lhik;Lhvf;Lhvz;Lhve;Lhvs;Lhup;Lhvd;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->dsk:Lhvi;

    .line 30
    return-void
.end method

.method public onCreateInputView()Landroid/view/View;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->dsk:Lhvi;

    invoke-virtual {v0}, Lhvi;->aSJ()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->dsk:Lhvi;

    invoke-virtual {v0}, Lhvi;->aSI()V

    .line 62
    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onDestroy()V

    .line 63
    return-void
.end method

.method public onEvaluateFullscreenMode()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 99
    iget-object v1, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->dsk:Lhvi;

    iget-object v2, v1, Lhvi;->drW:Lhve;

    invoke-interface {v2}, Lhve;->getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;

    move-result-object v2

    if-eqz v2, :cond_2

    iget v3, v2, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const/high16 v4, 0x10000000

    and-int/2addr v3, v4

    if-eqz v3, :cond_1

    move v1, v0

    :goto_0
    if-eqz v1, :cond_0

    .line 100
    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onEvaluateFullscreenMode()Z

    move-result v0

    .line 102
    :cond_0
    return v0

    .line 99
    :cond_1
    iget v2, v2, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const/high16 v3, 0x2000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_2

    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v1, v1, Lhvi;->drW:Lhve;

    invoke-interface {v1}, Lhve;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v2, v2

    const v3, 0x7f0d00db

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    cmpl-float v1, v2, v1

    if-lez v1, :cond_3

    move v1, v0

    goto :goto_0

    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onFinishInput()V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->dsk:Lhvi;

    invoke-virtual {v0}, Lhvi;->aSF()V

    .line 43
    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onFinishInput()V

    .line 44
    return-void
.end method

.method public onFinishInputView(Z)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->dsk:Lhvi;

    invoke-virtual {v0, p1}, Lhvi;->gI(Z)V

    .line 55
    invoke-super {p0, p1}, Landroid/inputmethodservice/InputMethodService;->onFinishInputView(Z)V

    .line 56
    return-void
.end method

.method public onStartInputView(Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0, p1, p2}, Landroid/inputmethodservice/InputMethodService;->onStartInputView(Landroid/view/inputmethod/EditorInfo;Z)V

    .line 35
    invoke-static {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->bP(Landroid/content/Context;)V

    .line 36
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->dsk:Lhvi;

    invoke-virtual {v0}, Lhvi;->aSB()V

    .line 37
    return-void
.end method

.method public onUpdateSelection(IIIIII)V
    .locals 1

    .prologue
    .line 92
    invoke-super/range {p0 .. p6}, Landroid/inputmethodservice/InputMethodService;->onUpdateSelection(IIIIII)V

    .line 94
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->dsk:Lhvi;

    if-eq p3, p4, :cond_0

    invoke-virtual {v0}, Lhvi;->aSK()V

    .line 95
    :cond_0
    return-void
.end method

.method public onViewClicked(Z)V
    .locals 4

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->dsk:Lhvi;

    const-string v1, "VoiceInputMethodManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#handleViewClicked["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    invoke-virtual {v0}, Lhvi;->aSK()V

    .line 87
    :cond_0
    return-void
.end method

.method public showWindow(Z)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/VoiceInputMethodService;->dsk:Lhvi;

    invoke-virtual {v0, p1}, Lhvi;->gJ(Z)V

    .line 76
    invoke-super {p0, p1}, Landroid/inputmethodservice/InputMethodService;->showWindow(Z)V

    .line 77
    return-void
.end method
