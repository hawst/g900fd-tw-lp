.class public Lcom/google/android/voicesearch/fragments/modular/DeviceSettingsArgumentView;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private dlv:Landroid/widget/Switch;

.field public dlw:Lhng;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/modular/DeviceSettingsArgumentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/fragments/modular/DeviceSettingsArgumentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;Lhng;)V
    .locals 2

    .prologue
    .line 52
    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/modular/DeviceSettingsArgumentView;->dlw:Lhng;

    .line 53
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->akb()I

    .line 55
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DeviceSettingsArgumentView;->dlv:Landroid/widget/Switch;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/DeviceSettingsArgument;->akc()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 65
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 40
    const v0, 0x7f11014c

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/DeviceSettingsArgumentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DeviceSettingsArgumentView;->dlv:Landroid/widget/Switch;

    .line 41
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DeviceSettingsArgumentView;->dlv:Landroid/widget/Switch;

    new-instance v1, Lhnf;

    invoke-direct {v1, p0}, Lhnf;-><init>(Lcom/google/android/voicesearch/fragments/modular/DeviceSettingsArgumentView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 49
    return-void
.end method
