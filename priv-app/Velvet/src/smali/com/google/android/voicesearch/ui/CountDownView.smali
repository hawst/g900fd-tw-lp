.class public Lcom/google/android/voicesearch/ui/CountDownView;
.super Landroid/widget/RelativeLayout;
.source "PG"


# instance fields
.field private dmv:Landroid/widget/ProgressBar;

.field private dmy:Landroid/animation/ObjectAnimator;

.field private dvV:Landroid/widget/ImageView;

.field public dws:Lhzx;

.field private dww:Landroid/view/View;

.field private dwx:Landroid/view/ViewGroup;

.field private dwy:Landroid/widget/TextView;

.field private dwz:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/ui/CountDownView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 48
    const v0, 0x7f090132

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/ui/CountDownView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 104
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dvV:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 105
    if-eqz p2, :cond_1

    .line 106
    const/4 v0, 0x1

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/CountDownView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    .line 108
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dvV:Landroid/widget/ImageView;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 113
    :goto_0
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dvV:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dwx:Landroid/view/ViewGroup;

    if-eq v0, v1, :cond_0

    .line 114
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dvV:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/CountDownView;->bR(Landroid/view/View;)V

    .line 116
    :cond_0
    return-void

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dvV:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/ImageView;->setPadding(IIII)V

    goto :goto_0
.end method

.method public final a(Lhzx;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dws:Lhzx;

    .line 183
    return-void
.end method

.method public final aL(J)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 159
    invoke-virtual {p0, v4}, Lcom/google/android/voicesearch/ui/CountDownView;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dmv:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 161
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dwz:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 162
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dmv:Landroid/widget/ProgressBar;

    const-class v1, Landroid/widget/ProgressBar;

    const-class v2, Ljava/lang/Integer;

    const-string v3, "progress"

    invoke-static {v1, v2, v3}, Landroid/util/Property;->of(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Landroid/util/Property;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [I

    aput v4, v2, v4

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dmv:Landroid/widget/ProgressBar;

    invoke-virtual {v4}, Landroid/widget/ProgressBar;->getMax()I

    move-result v4

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dmy:Landroid/animation/ObjectAnimator;

    .line 166
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dmy:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 167
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dmy:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 168
    return-void
.end method

.method public final aUC()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dwx:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 93
    return-void
.end method

.method protected final aUE()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dvV:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dvV:Landroid/widget/ImageView;

    const v1, -0x333334

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 130
    :cond_0
    return-void
.end method

.method public final aUF()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 149
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dww:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    invoke-static {}, Legs;->aoH()Legs;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dww:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Legs;->q(Landroid/view/View;I)V

    .line 154
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final auk()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 171
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dmy:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dmy:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 173
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dmv:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dmv:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 176
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dmv:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dwz:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 179
    :cond_1
    return-void
.end method

.method public final bR(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dwx:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 120
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dwx:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 121
    return-void
.end method

.method public final gv(Z)V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dww:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 187
    return-void
.end method

.method public final lJ(I)Z
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dww:Landroid/view/View;

    invoke-static {v0}, Lehd;->aG(Landroid/view/View;)I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 142
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dww:Landroid/view/View;

    invoke-static {v0, p1}, Lehd;->s(Landroid/view/View;I)V

    .line 143
    const/4 v0, 0x1

    .line 145
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final lc(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 96
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dvV:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/CountDownView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dvV:Landroid/widget/ImageView;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 98
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dvV:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dwx:Landroid/view/ViewGroup;

    if-eq v0, v1, :cond_0

    .line 99
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dvV:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/CountDownView;->bR(Landroid/view/View;)V

    .line 101
    :cond_0
    return-void
.end method

.method public final ld(I)V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dwy:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 138
    return-void
.end method

.method public final oR(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dwy:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 57
    const v0, 0x7f11006d

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/CountDownView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dmv:Landroid/widget/ProgressBar;

    .line 58
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dmv:Landroid/widget/ProgressBar;

    const v1, 0x7f0c0079

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 60
    const v0, 0x7f11013e

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/CountDownView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dwx:Landroid/view/ViewGroup;

    .line 61
    const v0, 0x7f11013f

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/CountDownView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dvV:Landroid/widget/ImageView;

    .line 62
    const v0, 0x7f11013c

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/CountDownView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dww:Landroid/view/View;

    .line 63
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dww:Landroid/view/View;

    const v1, 0x7f0c0076

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 64
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dww:Landroid/view/View;

    new-instance v1, Liak;

    invoke-direct {v1, p0}, Liak;-><init>(Lcom/google/android/voicesearch/ui/CountDownView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    const v0, 0x7f110140

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/CountDownView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dwy:Landroid/widget/TextView;

    .line 76
    const v0, 0x7f11013d

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/CountDownView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dwz:Landroid/view/View;

    .line 77
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dwz:Landroid/view/View;

    const v1, 0x7f0c0078

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 79
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dwz:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/CountDownView;->dwz:Landroid/view/View;

    new-instance v1, Lial;

    invoke-direct {v1, p0}, Lial;-><init>(Lcom/google/android/voicesearch/ui/CountDownView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    :cond_0
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 194
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 195
    const-class v0, Lcom/google/android/voicesearch/ui/CountDownView;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 196
    return-void
.end method
