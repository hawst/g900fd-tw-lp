.class public Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;
.super Landroid/speech/RecognitionService;
.source "PG"


# instance fields
.field private duo:Lhxu;

.field private mSettings:Lhym;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/speech/RecognitionService;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCancel(Landroid/speech/RecognitionService$Callback;)V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->duo:Lhxu;

    if-nez v0, :cond_0

    .line 63
    const-string v0, "GoogleRecognitionService"

    const-string v1, "Cancel is called before startListening"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->duo:Lhxu;

    invoke-virtual {v0}, Lhxu;->cancel()V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 29
    invoke-super {p0}, Landroid/speech/RecognitionService;->onCreate()V

    .line 31
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    .line 32
    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    iput-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->mSettings:Lhym;

    .line 33
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->duo:Lhxu;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->duo:Lhxu;

    invoke-virtual {v0}, Lhxu;->destroy()V

    .line 85
    :cond_0
    invoke-super {p0}, Landroid/speech/RecognitionService;->onDestroy()V

    .line 86
    return-void
.end method

.method protected onStartListening(Landroid/content/Intent;Landroid/speech/RecognitionService$Callback;)V
    .locals 5

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->duo:Lhxu;

    if-nez v0, :cond_0

    .line 53
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v1

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->aus()Leqo;

    move-result-object v0

    new-instance v2, Lhxy;

    invoke-virtual {v1}, Lhhq;->aAs()Lequ;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lhxy;-><init>(Leqo;Lequ;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->JW()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->JX()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    new-instance v3, Lhxu;

    invoke-virtual {v1}, Lhhq;->aOR()Lgdb;

    move-result-object v4

    iget-object v1, v1, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->aus()Leqo;

    move-result-object v1

    invoke-direct {v3, v4, v2, v1, v0}, Lhxu;-><init>(Lgdb;Lhxy;Ljava/util/concurrent/Executor;Z)V

    iput-object v3, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->duo:Lhxu;

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->duo:Lhxu;

    new-instance v1, Lhxt;

    iget-object v2, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->mSettings:Lhym;

    invoke-direct {v1, p1, v2}, Lhxt;-><init>(Landroid/content/Intent;Lhym;)V

    invoke-virtual {v0, v1, p2}, Lhxu;->a(Lhxt;Landroid/speech/RecognitionService$Callback;)V

    .line 57
    return-void

    .line 53
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onStopListening(Landroid/speech/RecognitionService$Callback;)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->duo:Lhxu;

    if-nez v0, :cond_0

    .line 73
    const-string v0, "GoogleRecognitionService"

    const-string v1, "StopListening is called before startListening"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    :goto_0
    return-void

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/serviceapi/GoogleRecognitionService;->duo:Lhxu;

    invoke-virtual {v0}, Lhxu;->stopListening()V

    goto :goto_0
.end method
