.class public Lcom/google/android/voicesearch/ui/ArgumentLabelTextView;
.super Landroid/widget/TextView;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 32
    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-super {p0}, Landroid/widget/TextView;->drawableStateChanged()V

    .line 40
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/ArgumentLabelTextView;->getDrawableState()[I

    move-result-object v2

    .line 41
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget v4, v2, v1

    .line 42
    const v5, 0x7f0100c7

    if-ne v4, v5, :cond_1

    .line 43
    const/4 v1, 0x1

    .line 48
    :goto_1
    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    :cond_0
    invoke-virtual {p0, v2, v0}, Lcom/google/android/voicesearch/ui/ArgumentLabelTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 49
    return-void

    .line 41
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1
.end method
