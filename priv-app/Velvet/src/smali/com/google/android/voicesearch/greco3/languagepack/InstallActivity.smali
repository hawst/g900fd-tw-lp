.class public Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;
.super Landroid/app/Activity;
.source "PG"


# instance fields
.field private doJ:Landroid/support/v4/view/ViewPager;

.field private doK:Lhqz;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 172
    return-void
.end method

.method public static bM(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 285
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    instance-of v1, p0, Landroid/app/Activity;

    if-nez v1, :cond_0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 286
    return-void
.end method

.method private static lr(I)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 74
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 75
    const-string v1, "type"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 76
    return-object v0
.end method

.method public static u(II)Ljava/lang/String;
    .locals 2

    .prologue
    .line 271
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "android:switcher:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 39
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    const v0, 0x7f0400af

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->setContentView(I)V

    .line 42
    const v0, 0x7f110237

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->doJ:Landroid/support/v4/view/ViewPager;

    .line 44
    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 45
    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 46
    const v1, 0x7f0a08ec

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 47
    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 51
    new-instance v1, Lhqz;

    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->doJ:Landroid/support/v4/view/ViewPager;

    invoke-direct {v1, p0, v2}, Lhqz;-><init>(Landroid/app/Activity;Landroid/support/v4/view/ViewPager;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->doK:Lhqz;

    .line 52
    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->doK:Lhqz;

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v2

    const v3, 0x7f0a08f0

    invoke-virtual {v2, v3}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v2

    const-class v3, Lhrz;

    invoke-static {v4}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->lr(I)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lhqz;->a(Landroid/app/ActionBar$Tab;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 55
    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->doK:Lhqz;

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v2

    const v3, 0x7f0a08ef

    invoke-virtual {v2, v3}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v2

    const-class v3, Lhrz;

    const/4 v4, 0x1

    invoke-static {v4}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->lr(I)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lhqz;->a(Landroid/app/ActionBar$Tab;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 58
    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->doK:Lhqz;

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    const v2, 0x7f0a08f1

    invoke-virtual {v0, v2}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v0

    const-class v2, Lhrx;

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v1, v0, v2, v3}, Lhqz;->a(Landroid/app/ActionBar$Tab;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 61
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 65
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->finish()V

    .line 68
    const/4 v0, 0x1

    .line 70
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
