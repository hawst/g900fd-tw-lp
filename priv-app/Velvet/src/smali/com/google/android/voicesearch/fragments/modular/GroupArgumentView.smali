.class public Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Ldua;
.implements Ldub;


# static fields
.field private static final bLM:[I


# instance fields
.field private bLP:Ldub;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bOf:Ldvv;

.field private dlI:Landroid/widget/RadioGroup;

.field private dlJ:Landroid/view/ViewGroup;

.field private dlK:[Landroid/view/ViewGroup;

.field private dlL:Lhnl;

.field private dlM:Lhmu;

.field private dlN:Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

.field private dlO:I

.field private mCallback:Lefk;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f0100c7

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->bLM:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    return-void
.end method

.method public static synthetic a(Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;)Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlN:Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;I)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->ln(I)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;)Lefk;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->mCallback:Lefk;

    return-object v0
.end method

.method private ln(I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 242
    const/4 v0, -0x1

    if-eq p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlK:[Landroid/view/ViewGroup;

    aget-object v0, v0, p1

    if-nez v0, :cond_3

    .line 243
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlN:Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->ako()[Ljqy;

    move-result-object v0

    aget-object v4, v0, p1

    iget-object v5, v4, Ljqy;->ezI:[I

    array-length v6, v5

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_2

    aget v0, v5, v2

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlN:Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    invoke-virtual {v7, v0}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->gN(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v7

    if-nez v7, :cond_1

    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->bOf:Ldvv;

    invoke-virtual {v7, v0}, Ldvv;->gF(I)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlM:Lhmu;

    invoke-virtual {v0, v7}, Lhmu;->i(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v7}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajk()I

    move-result v7

    invoke-static {v0, v7}, Lehd;->s(Landroid/view/View;I)V

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    instance-of v7, v0, Ldua;

    if-eqz v7, :cond_0

    check-cast v0, Ldua;

    invoke-interface {v0, p0}, Ldua;->a(Ldub;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v4}, Ljqy;->bsP()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v4}, Ljqy;->bsO()I

    move-result v0

    invoke-static {v3, v0}, Lehd;->s(Landroid/view/View;I)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlJ:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlK:[Landroid/view/ViewGroup;

    aput-object v3, v0, p1

    :cond_3
    move v0, v1

    .line 246
    :goto_3
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlK:[Landroid/view/ViewGroup;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 247
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlK:[Landroid/view/ViewGroup;

    aget-object v2, v2, v0

    if-eqz v2, :cond_4

    .line 248
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlK:[Landroid/view/ViewGroup;

    aget-object v3, v2, v0

    if-ne v0, p1, :cond_6

    move v2, v1

    :goto_4
    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 246
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 243
    :cond_5
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->bOf:Ldvv;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlN:Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->getId()I

    move-result v2

    invoke-virtual {v0, v2}, Ldvv;->gG(I)V

    goto :goto_2

    .line 248
    :cond_6
    const/16 v2, 0x8

    goto :goto_4

    .line 251
    :cond_7
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Landroid/view/View;
    .locals 4

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlN:Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    if-ne v0, p1, :cond_1

    .line 147
    :cond_0
    :goto_0
    return-object p0

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlK:[Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlN:Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->akn()I

    move-result v1

    aget-object v2, v0, v1

    .line 138
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 139
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 140
    instance-of v3, v0, Ldua;

    if-eqz v3, :cond_2

    .line 141
    check-cast v0, Ldua;

    invoke-interface {v0, p1}, Ldua;->a(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Landroid/view/View;

    move-result-object p0

    .line 142
    if-nez p0, :cond_0

    .line 138
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 147
    :cond_3
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final a(Ldub;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->bLP:Ldub;

    .line 153
    return-void
.end method

.method public final a(Ldvv;Lhmu;Lefk;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->bOf:Ldvv;

    .line 70
    iput-object p2, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlM:Lhmu;

    .line 71
    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->mCallback:Lefk;

    .line 72
    return-void
.end method

.method public final aQW()Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlN:Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    return-object v0
.end method

.method public final bridge synthetic ahf()Lcom/google/android/search/shared/actions/modular/arguments/Argument;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlN:Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    return-object v0
.end method

.method public final ahg()V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->bLP:Ldub;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->bLP:Ldub;

    invoke-interface {v0}, Ldub;->ahg()V

    .line 160
    :cond_0
    return-void
.end method

.method public final b(ILjava/lang/CharSequence;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 174
    iget v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlO:I

    if-eq v0, p1, :cond_0

    .line 175
    iput p1, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlO:I

    .line 176
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->refreshDrawableState()V

    .line 178
    :cond_0
    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlK:[Landroid/view/ViewGroup;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    .line 179
    if-eqz v6, :cond_2

    move v1, v2

    .line 180
    :goto_1
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 183
    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 184
    instance-of v7, v0, Ldua;

    if-eqz v7, :cond_1

    .line 185
    check-cast v0, Ldua;

    .line 186
    invoke-interface {v0, p1, p2}, Ldua;->b(ILjava/lang/CharSequence;)V

    .line 182
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 178
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 190
    :cond_3
    return-void
.end method

.method public final bridge synthetic b(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->b(Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;)V

    return-void
.end method

.method public final b(Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 76
    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlN:Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    .line 77
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->ako()[Ljqy;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlK:[Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    array-length v0, v4

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlK:[Landroid/view/ViewGroup;

    array-length v1, v1

    if-eq v0, v1, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    .line 78
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlJ:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 79
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->ako()[Ljqy;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlK:[Landroid/view/ViewGroup;

    .line 82
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlN:Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->ako()[Ljqy;

    move-result-object v6

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlI:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->removeAllViews()V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlI:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->clearCheck()V

    array-length v7, v6

    move v3, v2

    move v4, v2

    :goto_1
    if-ge v3, v7, :cond_a

    aget-object v8, v6, v3

    const v1, 0x7f040095

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlI:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1, v5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    invoke-virtual {v8}, Ljqy;->getLabel()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    add-int/lit8 v5, v4, 0x1

    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setId(I)V

    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlL:Lhnl;

    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v8}, Ljqy;->bsN()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-virtual {v8}, Ljqy;->bsM()I

    move-result v4

    invoke-static {v1, v4}, Lehd;->s(Landroid/view/View;I)V

    :goto_2
    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlI:Landroid/widget/RadioGroup;

    invoke-virtual {v4, v1}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v4, v5

    goto :goto_1

    :cond_2
    move v1, v2

    .line 77
    :goto_3
    array-length v0, v4

    if-ge v1, v0, :cond_8

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlK:[Landroid/view/ViewGroup;

    aget-object v0, v0, v1

    if-eqz v0, :cond_7

    aget-object v5, v4, v1

    iget-object v0, v5, Ljqy;->ezI:[I

    array-length v0, v0

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlK:[Landroid/view/ViewGroup;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-eq v0, v3, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    move v3, v2

    :goto_4
    aget-object v0, v4, v1

    iget-object v0, v0, Ljqy;->ezI:[I

    array-length v0, v0

    if-ge v3, v0, :cond_7

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlK:[Landroid/view/ViewGroup;

    aget-object v0, v0, v1

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v6, v0, Ldua;

    if-nez v6, :cond_4

    move v0, v2

    goto/16 :goto_0

    :cond_4
    check-cast v0, Ldua;

    invoke-interface {v0}, Ldua;->ahf()Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v6

    iget-object v7, v5, Ljqy;->ezI:[I

    aget v7, v7, v3

    invoke-virtual {p1, v7}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->gN(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v7

    if-eqz v6, :cond_5

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    if-eq v8, v6, :cond_6

    :cond_5
    move v0, v2

    goto/16 :goto_0

    :cond_6
    invoke-interface {v0, v7}, Ldua;->b(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_8
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 82
    :cond_9
    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->bOf:Ldvv;

    iget-object v8, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlN:Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    invoke-virtual {v8}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->getId()I

    move-result v8

    invoke-virtual {v4, v8}, Ldvv;->gG(I)V

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlN:Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->akm()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlI:Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlN:Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->akn()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 83
    :cond_b
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlN:Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->akm()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->akn()I

    move-result v0

    :goto_5
    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->ln(I)V

    .line 85
    return-void

    .line 83
    :cond_c
    const/4 v0, -0x1

    goto :goto_5
.end method

.method public final getPrompt()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 200
    iget v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlO:I

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlN:Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->getId()I

    move-result v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlN:Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/GroupArgument;->Pd()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 203
    :goto_0
    if-eqz v0, :cond_0

    .line 204
    add-int/lit8 p1, p1, 0x1

    .line 207
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onCreateDrawableState(I)[I

    move-result-object v1

    .line 208
    if-eqz v0, :cond_1

    .line 209
    sget-object v0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->bLM:[I

    invoke-static {v1, v0}, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->mergeDrawableStates([I[I)[I

    .line 212
    :cond_1
    return-object v1

    .line 200
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 61
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 62
    const v0, 0x7f1101f2

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlI:Landroid/widget/RadioGroup;

    .line 63
    const v0, 0x7f1101f3

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlJ:Landroid/view/ViewGroup;

    .line 64
    new-instance v0, Lhnl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lhnl;-><init>(Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;B)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlL:Lhnl;

    .line 65
    return-void
.end method

.method public final setEditable(Z)V
    .locals 3

    .prologue
    .line 164
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlJ:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 165
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/GroupArgumentView;->dlJ:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 166
    instance-of v2, v0, Ldua;

    if-eqz v2, :cond_0

    .line 167
    check-cast v0, Ldua;

    invoke-interface {v0, p1}, Ldua;->setEditable(Z)V

    .line 164
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 170
    :cond_1
    return-void
.end method
