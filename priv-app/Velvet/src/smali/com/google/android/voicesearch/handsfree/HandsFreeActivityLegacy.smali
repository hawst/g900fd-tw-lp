.class public Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;
.super Landroid/app/Activity;
.source "PG"


# instance fields
.field public dqd:Z

.field private dqe:Z

.field private dqf:Lhuk;

.field public dqg:Lhtj;

.field private final dqh:Lesk;

.field private final dqi:Ljava/lang/Runnable;

.field private mDeviceCapabilityManager:Lenm;

.field private mGreco3DataManager:Lgix;

.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 58
    new-instance v0, Lhsu;

    const-string v1, "resumeInternal"

    invoke-direct {v0, p0, v1}, Lhsu;-><init>(Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqh:Lesk;

    .line 65
    new-instance v0, Lhsv;

    const-string v1, "Start HandsFreeMainController"

    invoke-direct {v0, p0, v1}, Lhsv;-><init>(Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqi:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public final gH(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 179
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqf:Lhuk;

    iget-object v2, v0, Lhuk;->mGreco3DataManager:Lgix;

    invoke-virtual {v0}, Lhuk;->aHG()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lgix;->mN(Ljava/lang/String;)Z

    move-result v0

    iget-object v2, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->mDeviceCapabilityManager:Lenm;

    invoke-interface {v2}, Lenm;->auH()Z

    move-result v2

    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    .line 181
    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->finish()V

    .line 202
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 179
    goto :goto_0

    .line 185
    :cond_2
    iput-boolean v1, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqe:Z

    .line 186
    iget-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqd:Z

    if-nez v0, :cond_0

    .line 190
    if-eqz p1, :cond_3

    .line 198
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqi:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 200
    :cond_3
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqi:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 17

    .prologue
    .line 81
    const/16 v1, 0x4f

    invoke-static {v1}, Lege;->ht(I)V

    .line 83
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 85
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    .line 86
    invoke-virtual {v1}, Lgql;->aJq()Lhhq;

    move-result-object v7

    .line 88
    iget-object v11, v7, Lhhq;->mSettings:Lhym;

    .line 90
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.speech.action.VOICE_SEARCH_HANDS_FREE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 93
    invoke-virtual {v7}, Lhhq;->aOV()Lgiw;

    move-result-object v2

    invoke-virtual {v2}, Lgiw;->aGh()Lgix;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->mGreco3DataManager:Lgix;

    .line 95
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/high16 v3, 0x280000

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 99
    const v2, 0x7f040096

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->setContentView(I)V

    .line 101
    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DX()Lenm;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->mDeviceCapabilityManager:Lenm;

    .line 103
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    iget-object v1, v1, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->aus()Leqo;

    move-result-object v2

    .line 106
    new-instance v1, Lhuk;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->mGreco3DataManager:Lgix;

    invoke-direct {v1, v3, v11}, Lhuk;-><init>(Lgix;Lhym;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqf:Lhuk;

    .line 108
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->mHandler:Landroid/os/Handler;

    .line 110
    invoke-static {v7}, Lhkd;->a(Lhhq;)Lhkd;

    move-result-object v13

    .line 112
    invoke-virtual {v7}, Lhhq;->aPj()Lice;

    move-result-object v14

    .line 113
    invoke-virtual {v7}, Lhhq;->aOZ()Lhik;

    move-result-object v10

    .line 115
    new-instance v15, Lhsg;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lhsg;-><init>(Landroid/app/Activity;)V

    .line 117
    new-instance v12, Lhul;

    const v1, 0x7f1101f4

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v1}, Lhul;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 120
    new-instance v16, Lhsj;

    invoke-static/range {p0 .. p0}, Lghy;->bG(Landroid/content/Context;)Lghy;

    move-result-object v1

    iget-object v3, v7, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->aus()Leqo;

    move-result-object v3

    iget-object v4, v7, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v4}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-direct {v0, v1, v3, v4}, Lhsj;-><init>(Lghy;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    .line 124
    invoke-virtual {v7}, Lhhq;->aPk()Lgkg;

    move-result-object v8

    .line 126
    new-instance v1, Lhsm;

    iget-object v3, v7, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v3}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    invoke-virtual {v7}, Lhhq;->aPb()Lhhu;

    move-result-object v4

    invoke-virtual {v7}, Lhhq;->aPc()Lhjb;

    move-result-object v5

    invoke-virtual {v7}, Lhhq;->aOZ()Lhik;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lhsm;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;Lhhu;Lhjb;ZLhik;)V

    .line 130
    new-instance v3, Lhsz;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->getResources()Landroid/content/res/Resources;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->mGreco3DataManager:Lgix;

    move-object v4, v1

    move-object v5, v14

    move-object v6, v12

    move-object v9, v2

    invoke-direct/range {v3 .. v11}, Lhsz;-><init>(Lhsm;Lice;Lhul;Lgix;Lgkg;Leqo;Lhik;Lhym;)V

    .line 134
    new-instance v4, Lhud;

    move-object v5, v13

    move-object v6, v14

    move-object v7, v12

    move-object/from16 v8, v16

    move-object v9, v2

    invoke-direct/range {v4 .. v9}, Lhud;-><init>(Lhkd;Lice;Lhul;Lhsj;Leqo;)V

    .line 137
    new-instance v9, Lhts;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v9, v13, v5, v12, v10}, Lhts;-><init>(Lhkd;Landroid/content/res/Resources;Lhul;Lhik;)V

    .line 141
    new-instance v16, Lhtl;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-direct {v0, v13, v5, v12, v10}, Lhtl;-><init>(Lhkd;Landroid/content/res/Resources;Lhul;Lhik;)V

    .line 144
    new-instance v11, Lhsq;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v11, v5, v14, v12}, Lhsq;-><init>(Landroid/content/res/Resources;Lice;Lhul;)V

    .line 147
    new-instance v5, Lhtj;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqf:Lhuk;

    move-object v6, v15

    move-object v7, v3

    move-object v8, v4

    move-object/from16 v10, v16

    move-object v15, v1

    move-object/from16 v16, v2

    invoke-direct/range {v5 .. v16}, Lhtj;-><init>(Lhsg;Lhsz;Lhud;Lhts;Lhtl;Lhsq;Lhuk;Lhkd;Lice;Lhsm;Leqo;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqg:Lhtj;

    .line 152
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqg:Lhtj;

    iget-object v2, v1, Lhtj;->dqy:Lhsz;

    iput-object v1, v2, Lhsz;->dqb:Lhtj;

    iget-object v2, v1, Lhtj;->dqB:Lhud;

    iput-object v1, v2, Lhud;->dqb:Lhtj;

    iget-object v2, v1, Lhtj;->dqC:Lhts;

    invoke-virtual {v2, v1}, Lhts;->a(Lhtj;)V

    iget-object v2, v1, Lhtj;->dqD:Lhtl;

    invoke-virtual {v2, v1}, Lhtl;->a(Lhtj;)V

    iget-object v2, v1, Lhtj;->dqE:Lhsq;

    iput-object v1, v2, Lhsq;->dqb:Lhtj;

    .line 153
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqg:Lhtj;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqg:Lhtj;

    .line 161
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 162
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 208
    iget-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqd:Z

    if-eqz v0, :cond_0

    .line 210
    const/16 v0, 0x4e

    invoke-static {v0}, Lege;->ht(I)V

    .line 212
    invoke-static {}, Legm;->aoz()Legm;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Legm;)V

    .line 213
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqg:Lhtj;

    iget-object v1, v0, Lhtj;->mRecognizerController:Lhkd;

    invoke-virtual {v1}, Lhkd;->cancel()V

    iget-object v1, v0, Lhtj;->mLocalTtsManager:Lice;

    invoke-virtual {v1}, Lice;->aUL()V

    iget-object v0, v0, Lhtj;->dqG:Lhsm;

    invoke-virtual {v0}, Lhsm;->aRP()V

    .line 214
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqe:Z

    .line 215
    invoke-static {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->bQ(Landroid/content/Context;)V

    .line 220
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 221
    return-void

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqi:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 167
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 169
    invoke-static {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->bP(Landroid/content/Context;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->mGreco3DataManager:Lgix;

    invoke-virtual {v0}, Lgix;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->gH(Z)V

    .line 175
    :goto_0
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->mGreco3DataManager:Lgix;

    iget-object v1, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqh:Lesk;

    invoke-virtual {v0, v1}, Lgix;->h(Lesk;)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 226
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 227
    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqe:Z

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->dqg:Lhtj;

    iget-object v0, v0, Lhtj;->dqG:Lhsm;

    invoke-virtual {v0}, Lhsm;->aRP()V

    .line 229
    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;->finish()V

    .line 231
    :cond_0
    return-void
.end method
