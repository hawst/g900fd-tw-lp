.class public Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView;
.super Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    return-void
.end method

.method static synthetic a(Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView;Ljava/lang/String;Ldzb;Ljava/lang/String;)Lcom/google/android/search/shared/contact/PersonDisambiguation;
    .locals 1

    .prologue
    .line 20
    invoke-static {p1, p2, p3}, Lico;->a(Ljava/lang/String;Ldzb;Ljava/lang/String;)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final c(Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;)V
    .locals 4

    .prologue
    .line 35
    new-instance v2, Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView$1;

    new-instance v3, Ljqg;

    invoke-direct {v3}, Ljqg;-><init>()V

    sget-object v0, Ljrh;->eAt:Ljsm;

    new-instance v1, Ljrh;

    invoke-direct {v1}, Ljrh;-><init>()V

    invoke-virtual {v3, v0, v1}, Ljqg;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    instance-of v1, v0, Landroid/text/Spanned;

    if-eqz v1, :cond_2

    check-cast v0, Landroid/text/Spanned;

    invoke-static {v0}, Landroid/text/Html;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljqg;->yh(Ljava/lang/String;)Ljqg;

    :cond_0
    :goto_0
    sget-object v0, Ljrh;->eAt:Ljsm;

    invoke-virtual {v3, v0}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljrh;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alF()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/contact/Contact;

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/Contact;->alP()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljrh;->yw(Ljava/lang/String;)Ljrh;

    invoke-direct {v2, p0, v3, p1}, Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView$1;-><init>(Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView;Ljqg;Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;)V

    invoke-super {p0, v2}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->b(Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;)V

    .line 45
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->akB()Ldzb;

    move-result-object v0

    sget-object v1, Ldzb;->bRq:Ldzb;

    if-ne v0, v1, :cond_3

    .line 46
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView;->setInputType(I)V

    .line 50
    :cond_1
    :goto_1
    return-void

    .line 35
    :cond_2
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljqg;->yh(Ljava/lang/String;)Ljqg;

    goto :goto_0

    .line 47
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->akB()Ldzb;

    move-result-object v0

    sget-object v1, Ldzb;->bRp:Ldzb;

    if-ne v0, v1, :cond_1

    .line 48
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView;->setInputType(I)V

    goto :goto_1
.end method
