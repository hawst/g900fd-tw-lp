.class public Lcom/google/android/voicesearch/ui/ClassicActionView;
.super Lhzw;
.source "PG"


# instance fields
.field private dvP:Landroid/view/ViewGroup;

.field private final dwr:Landroid/graphics/drawable/Drawable;

.field private dws:Lhzx;

.field private dwt:Landroid/view/View;

.field private dwu:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 57
    const v0, 0x7f090132

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lhzw;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 49
    sget-object v1, Lbwe;->aLZ:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 50
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/voicesearch/ui/ClassicActionView;->dwr:Landroid/graphics/drawable/Drawable;

    .line 51
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 52
    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    return-void
.end method

.method private lI(I)Lcom/google/android/voicesearch/ui/CountDownView;
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ClassicActionView;->dwu:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/ui/CountDownView;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;Z)V
    .locals 2

    .prologue
    .line 132
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->lI(I)Lcom/google/android/voicesearch/ui/CountDownView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/voicesearch/ui/CountDownView;->a(Landroid/graphics/drawable/Drawable;Z)V

    .line 133
    return-void
.end method

.method public final a(Lhzx;)V
    .locals 1

    .prologue
    .line 195
    iput-object p1, p0, Lcom/google/android/voicesearch/ui/ClassicActionView;->dws:Lhzx;

    .line 196
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->lI(I)Lcom/google/android/voicesearch/ui/CountDownView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/CountDownView;->a(Lhzx;)V

    .line 197
    return-void
.end method

.method public final aL(J)V
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->lI(I)Lcom/google/android/voicesearch/ui/CountDownView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/voicesearch/ui/CountDownView;->aL(J)V

    .line 202
    return-void
.end method

.method public final aUC()V
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->lI(I)Lcom/google/android/voicesearch/ui/CountDownView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/CountDownView;->aUC()V

    .line 123
    return-void
.end method

.method public final aUD()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ClassicActionView;->dws:Lhzx;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ClassicActionView;->dws:Lhzx;

    invoke-interface {v0}, Lhzx;->aQm()V

    .line 85
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->lI(I)Lcom/google/android/voicesearch/ui/CountDownView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/CountDownView;->auk()V

    .line 90
    invoke-static {}, Legs;->aoH()Legs;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, p0, v1}, Legs;->q(Landroid/view/View;I)V

    .line 91
    return-void
.end method

.method public final auj()V
    .locals 2

    .prologue
    .line 182
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->lI(I)Lcom/google/android/voicesearch/ui/CountDownView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/CountDownView;->aUF()Z

    move-result v0

    if-nez v0, :cond_0

    .line 183
    invoke-static {}, Legs;->aoH()Legs;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Legs;->q(Landroid/view/View;I)V

    .line 186
    :cond_0
    return-void
.end method

.method public final auk()V
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->lI(I)Lcom/google/android/voicesearch/ui/CountDownView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/CountDownView;->auk()V

    .line 207
    return-void
.end method

.method public final bR(Landroid/view/View;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 138
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->lI(I)Lcom/google/android/voicesearch/ui/CountDownView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/CountDownView;->bR(Landroid/view/View;)V

    .line 139
    return-void
.end method

.method public final fc(Z)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 112
    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->lI(I)Lcom/google/android/voicesearch/ui/CountDownView;

    move-result-object v1

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/voicesearch/ui/CountDownView;->setVisibility(I)V

    .line 113
    return-void

    .line 112
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final gR(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 211
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ClassicActionView;->dvP:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClickable(Z)V

    .line 212
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ClassicActionView;->dvP:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 213
    return-void
.end method

.method public final gS(Z)V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ClassicActionView;->dwt:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    .line 218
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ClassicActionView;->dwt:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 219
    return-void
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->lI(I)Lcom/google/android/voicesearch/ui/CountDownView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/CountDownView;->getTag()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final gv(Z)V
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->lI(I)Lcom/google/android/voicesearch/ui/CountDownView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/CountDownView;->gv(Z)V

    .line 224
    return-void
.end method

.method public final lF(I)V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ClassicActionView;->dvP:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/ClassicActionView;->dvP:Landroid/view/ViewGroup;

    invoke-static {v0, p1, v1}, Lcom/google/android/voicesearch/ui/ClassicActionView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 97
    return-void
.end method

.method public final lG(I)Z
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->lI(I)Lcom/google/android/voicesearch/ui/CountDownView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/CountDownView;->lJ(I)Z

    move-result v0

    return v0
.end method

.method public final lH(I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 236
    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/ui/ClassicActionView;->ld(I)V

    .line 239
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->setClickable(Z)V

    .line 240
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->gv(Z)V

    .line 241
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->gS(Z)V

    .line 242
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->gR(Z)V

    .line 245
    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->lI(I)Lcom/google/android/voicesearch/ui/CountDownView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/voicesearch/ui/CountDownView;->aUE()V

    .line 246
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ClassicActionView;->dvP:Landroid/view/ViewGroup;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 247
    return-void
.end method

.method public final lc(I)V
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->lI(I)Lcom/google/android/voicesearch/ui/CountDownView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/CountDownView;->lc(I)V

    .line 128
    return-void
.end method

.method public final ld(I)V
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->lI(I)Lcom/google/android/voicesearch/ui/CountDownView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/CountDownView;->ld(I)V

    .line 149
    return-void
.end method

.method public final le(I)V
    .locals 2

    .prologue
    .line 172
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->lI(I)Lcom/google/android/voicesearch/ui/CountDownView;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/voicesearch/ui/CountDownView;->setTag(Ljava/lang/Object;)V

    .line 173
    return-void
.end method

.method public final oR(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->lI(I)Lcom/google/android/voicesearch/ui/CountDownView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/CountDownView;->oR(Ljava/lang/String;)V

    .line 154
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 66
    const v0, 0x7f110106

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/ClassicActionView;->dwr:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 68
    const v0, 0x7f110107

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ClassicActionView;->dvP:Landroid/view/ViewGroup;

    .line 69
    const v0, 0x7f110109

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ClassicActionView;->dwu:Landroid/widget/LinearLayout;

    .line 71
    const v0, 0x7f110108

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ClassicActionView;->dwt:Landroid/view/View;

    .line 72
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ClassicActionView;->dwt:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 73
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ClassicActionView;->dwt:Landroid/view/View;

    new-instance v1, Liaj;

    invoke-direct {v1, p0}, Liaj;-><init>(Lcom/google/android/voicesearch/ui/ClassicActionView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 309
    invoke-super {p0, p1}, Lhzw;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 310
    const-class v0, Lcom/google/android/voicesearch/ui/ClassicActionView;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 311
    return-void
.end method

.method public final uh()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 258
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->setClickable(Z)V

    .line 259
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->gv(Z)V

    .line 260
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->gS(Z)V

    .line 261
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->gR(Z)V

    .line 264
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ClassicActionView;->fc(Z)V

    .line 265
    return-void
.end method
