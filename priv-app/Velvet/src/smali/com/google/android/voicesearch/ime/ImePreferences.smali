.class public Lcom/google/android/voicesearch/ime/ImePreferences;
.super Landroid/app/Activity;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 59
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 29
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    .line 33
    iget-object v1, v0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->JW()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 34
    invoke-static {p0}, Lhuz;->bO(Landroid/content/Context;)V

    .line 57
    :goto_0
    return-void

    .line 37
    :cond_0
    invoke-virtual {v0}, Lhhq;->aPl()Lhvf;

    move-result-object v1

    .line 46
    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    invoke-virtual {v1, v0, v2}, Lhvf;->a(Ljze;Z)V

    .line 48
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 49
    const-class v1, Lcom/google/android/velvet/ui/settings/SettingsActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 50
    const-string v1, ":android:no_headers"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 51
    const-string v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    const-string v1, ":android:show_fragment_title"

    const v2, 0x7f0a0858

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 54
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ime/ImePreferences;->startActivity(Landroid/content/Intent;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/ImePreferences;->finish()V

    goto :goto_0
.end method
