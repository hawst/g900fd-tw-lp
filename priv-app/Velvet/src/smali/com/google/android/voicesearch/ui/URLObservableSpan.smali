.class public Lcom/google/android/voicesearch/ui/URLObservableSpan;
.super Landroid/text/style/URLSpan;
.source "PG"


# instance fields
.field private dxl:Libn;


# direct methods
.method private constructor <init>(Landroid/text/style/URLSpan;Libn;)V
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    .line 30
    iput-object p2, p0, Lcom/google/android/voicesearch/ui/URLObservableSpan;->dxl:Libn;

    .line 31
    return-void
.end method

.method public static a(Landroid/text/Spanned;Libn;)Landroid/text/Spanned;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 71
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 74
    invoke-interface {p0}, Landroid/text/Spanned;->length()I

    move-result v1

    const-class v2, Ljava/lang/Object;

    invoke-interface {p0, v0, v1, v2}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v4

    .line 76
    array-length v5, v4

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v1, v4, v2

    .line 77
    instance-of v0, v1, Landroid/text/style/URLSpan;

    if-eqz v0, :cond_0

    .line 78
    new-instance v6, Lcom/google/android/voicesearch/ui/URLObservableSpan;

    move-object v0, v1

    check-cast v0, Landroid/text/style/URLSpan;

    invoke-direct {v6, v0, p1}, Lcom/google/android/voicesearch/ui/URLObservableSpan;-><init>(Landroid/text/style/URLSpan;Libn;)V

    .line 79
    invoke-interface {p0, v1}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    invoke-interface {p0, v1}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    invoke-interface {p0, v1}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v3, v6, v0, v7, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 76
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 82
    :cond_0
    invoke-interface {p0, v1}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    invoke-interface {p0, v1}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    invoke-interface {p0, v1}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    move-result v7

    invoke-virtual {v3, v1, v0, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    .line 87
    :cond_1
    return-object v3
.end method

.method private static bT(Landroid/content/Context;)Landroid/inputmethodservice/InputMethodService;
    .locals 2

    .prologue
    .line 57
    instance-of v0, p0, Landroid/inputmethodservice/InputMethodService;

    if-eqz v0, :cond_0

    .line 58
    check-cast p0, Landroid/inputmethodservice/InputMethodService;

    .line 67
    :goto_0
    return-object p0

    :cond_0
    move-object v0, p0

    .line 61
    :cond_1
    instance-of v1, v0, Landroid/content/ContextWrapper;

    if-eqz v1, :cond_2

    .line 62
    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    .line 63
    instance-of v1, v0, Landroid/inputmethodservice/InputMethodService;

    if-eqz v1, :cond_1

    .line 64
    check-cast v0, Landroid/inputmethodservice/InputMethodService;

    move-object p0, v0

    goto :goto_0

    .line 67
    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/URLObservableSpan;->dxl:Libn;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/URLObservableSpan;->getURL()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Libn;->ov(Ljava/lang/String;)V

    .line 39
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/URLObservableSpan;->getURL()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 40
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 42
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 43
    const-string v0, "com.android.browser.application_id"

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 45
    invoke-static {v1}, Lcom/google/android/voicesearch/ui/URLObservableSpan;->bT(Landroid/content/Context;)Landroid/inputmethodservice/InputMethodService;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_0

    .line 47
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 50
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/inputmethodservice/InputMethodService;->requestHideSelf(I)V

    .line 53
    :cond_0
    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 54
    return-void
.end method
