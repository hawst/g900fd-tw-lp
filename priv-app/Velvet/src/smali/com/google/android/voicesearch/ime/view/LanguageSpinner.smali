.class public Lcom/google/android/voicesearch/ime/view/LanguageSpinner;
.super Landroid/widget/Spinner;
.source "PG"


# instance fields
.field public dss:Lhvx;

.field public dst:Ljzh;

.field public dsu:[Ljzh;

.field private dsv:Landroid/app/AlertDialog;

.field private dsw:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;I)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 58
    return-void
.end method

.method public static synthetic a(Lcom/google/android/voicesearch/ime/view/LanguageSpinner;)Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dsw:Z

    return v0
.end method


# virtual methods
.method public final a(Lhvx;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dss:Lhvx;

    .line 84
    return-void
.end method

.method public final a(Ljava/lang/String;[Ljzh;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 61
    array-length v3, p2

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, p2, v1

    invoke-virtual {v0}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_1
    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dst:Ljzh;

    .line 62
    iput-object p2, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dsu:[Ljzh;

    .line 63
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0400a6

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dst:Ljzh;

    invoke-virtual {v5}, Ljzh;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-direct {v0, v1, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 65
    return-void

    .line 61
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    aget-object v0, p2, v2

    goto :goto_1
.end method

.method public final gK(Z)V
    .locals 0

    .prologue
    .line 180
    iput-boolean p1, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dsw:Z

    .line 181
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dsv:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dsv:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->hide()V

    .line 96
    :cond_0
    invoke-super {p0}, Landroid/widget/Spinner;->onDetachedFromWindow()V

    .line 97
    return-void
.end method

.method public final op(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 68
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0400a6

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 70
    return-void
.end method

.method public performClick()Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 101
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dsu:[Ljzh;

    if-nez v0, :cond_0

    .line 110
    const-string v0, "VS.LanguageSpinner"

    const-string v1, "#performClick: missing call to setLanguages"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :goto_0
    return v4

    .line 114
    :cond_0
    const/16 v0, 0x43

    invoke-static {v0}, Lege;->ht(I)V

    .line 117
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dss:Lhvx;

    invoke-interface {v0}, Lhvx;->aSQ()V

    .line 119
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dsu:[Ljzh;

    invoke-static {v0}, Lgnq;->a([Ljzh;)[Ljava/lang/CharSequence;

    move-result-object v1

    .line 120
    array-length v0, v1

    add-int/lit8 v0, v0, 0x1

    new-array v2, v0, [Ljava/lang/CharSequence;

    .line 121
    array-length v0, v1

    invoke-static {v1, v3, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 124
    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dsw:Z

    if-nez v0, :cond_1

    .line 125
    array-length v0, v1

    if-ne v0, v4, :cond_1

    const v0, 0x7f0a08e5

    .line 131
    :goto_1
    array-length v1, v1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 134
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v1, Lhvw;

    invoke-direct {v1, p0}, Lhvw;-><init>(Lcom/google/android/voicesearch/ime/view/LanguageSpinner;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lhvv;

    invoke-direct {v1, p0}, Lhvv;-><init>(Lcom/google/android/voicesearch/ime/view/LanguageSpinner;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 161
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-boolean v0, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dsw:Z

    if-eqz v0, :cond_2

    const v0, 0x7f0a08e4

    :goto_2
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 165
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dsv:Landroid/app/AlertDialog;

    .line 167
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dsv:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 169
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    iput-object v2, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 170
    const/16 v2, 0x3eb

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 171
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 172
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 174
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/view/LanguageSpinner;->dsv:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 125
    :cond_1
    const v0, 0x7f0a08e6

    goto :goto_1

    .line 161
    :cond_2
    const v0, 0x7f0a08e3

    goto :goto_2
.end method
