.class public Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;
.super Lhou;
.source "PG"


# instance fields
.field private ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

.field private final dkY:Lop;

.field private final dkZ:Lelh;

.field private dla:Landroid/widget/TextView;

.field public dlb:Lhnn;

.field private dlc:Loi;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lhou;-><init>(Landroid/content/Context;)V

    .line 39
    new-instance v0, Lhmr;

    invoke-direct {v0, p0}, Lhmr;-><init>(Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dkY:Lop;

    .line 46
    new-instance v0, Lelh;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f004a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    aget-object v1, v1, v2

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lelh;-><init>(Ljava/lang/String;Ljava/lang/Object;Z)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dkZ:Lelh;

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lhou;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    new-instance v0, Lhmr;

    invoke-direct {v0, p0}, Lhmr;-><init>(Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dkY:Lop;

    .line 46
    new-instance v0, Lelh;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f004a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    aget-object v1, v1, v2

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lelh;-><init>(Ljava/lang/String;Ljava/lang/Object;Z)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dkZ:Lelh;

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Lhou;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    new-instance v0, Lhmr;

    invoke-direct {v0, p0}, Lhmr;-><init>(Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dkY:Lop;

    .line 46
    new-instance v0, Lelh;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f004a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    aget-object v1, v1, v2

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lelh;-><init>(Ljava/lang/String;Ljava/lang/Object;Z)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dkZ:Lelh;

    .line 58
    return-void
.end method

.method private N(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dlb:Lhnn;

    invoke-virtual {v0, p1}, Lhnn;->F(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dlb:Lhnn;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dkZ:Lelh;

    invoke-virtual {v1, v2}, Lhnn;->getPosition(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->hY(I)V

    .line 152
    return-void
.end method

.method public static synthetic a(Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;)V
    .locals 0

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->ahi()V

    return-void
.end method

.method private b(Ljrk;)Lelh;
    .locals 3

    .prologue
    .line 169
    new-instance v0, Lelh;

    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->c(Ljrk;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lelh;-><init>(Ljava/lang/String;Ljava/lang/Object;Z)V

    return-object v0
.end method

.method private c(Ljkh;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;

    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->a(Ljkh;)J

    move-result-wide v2

    const/16 v0, 0xa01

    invoke-static {v1, v2, v3, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljrk;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p1}, Ljrk;->rn()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljrk;->getLabel()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Ljrk;->eAF:Ljkh;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->c(Ljkh;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;)V
    .locals 8

    .prologue
    const v7, 0x7f0a0898

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 97
    invoke-super {p0, p1}, Lhou;->b(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dlb:Lhnn;

    invoke-virtual {v0}, Lhnn;->clear()V

    .line 99
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->ajz()Z

    move-result v0

    if-nez v0, :cond_1

    .line 100
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dla:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 102
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->ajH()I

    move-result v0

    .line 103
    packed-switch v0, :pswitch_data_0

    .line 144
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->ajB()Z

    move-result v0

    .line 145
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dla:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setClickable(Z)V

    .line 146
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v1, v0}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setEnabled(Z)V

    .line 147
    return-void

    .line 105
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dla:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->ajx()Ljrk;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->c(Ljrk;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 108
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dla:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->ajx()Ljrk;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->c(Ljrk;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {v2, v7, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 114
    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setVisibility(I)V

    .line 115
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->ajA()[Ljrk;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 116
    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dlb:Lhnn;

    invoke-direct {p0, v4}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->b(Ljrk;)Lelh;

    move-result-object v4

    invoke-virtual {v5, v4}, Lhnn;->add(Ljava/lang/Object;)V

    .line 115
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 118
    :cond_2
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dlb:Lhnn;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dkZ:Lelh;

    invoke-virtual {v0, v2}, Lhnn;->add(Ljava/lang/Object;)V

    .line 119
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->ajx()Ljrk;

    move-result-object v0

    .line 121
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->ajH()I

    move-result v2

    .line 122
    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 124
    :pswitch_2
    invoke-virtual {v0}, Ljrk;->rn()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 125
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dlb:Lhnn;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->b(Ljrk;)Lelh;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhnn;->getPosition(Ljava/lang/Object;)I

    move-result v1

    .line 127
    if-ltz v1, :cond_3

    .line 129
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->hY(I)V

    goto/16 :goto_0

    .line 131
    :cond_3
    invoke-virtual {v0}, Ljrk;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->N(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 134
    :cond_4
    iget-object v0, v0, Ljrk;->eAF:Ljkh;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->c(Ljkh;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->N(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 138
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->ajx()Ljrk;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->c(Ljrk;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-virtual {v0, v7, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->N(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 103
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 122
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljrk;)V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;

    .line 156
    invoke-virtual {v0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->a(Ljrk;)V

    .line 157
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->a(Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;)V

    .line 158
    return-void
.end method

.method public final aQL()V
    .locals 4

    .prologue
    .line 196
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;

    .line 198
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->Pd()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->ajx()Ljrk;

    move-result-object v1

    iget-object v1, v1, Ljrk;->eAF:Ljkh;

    if-eqz v1, :cond_0

    .line 199
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->ajx()Ljrk;

    move-result-object v0

    iget-object v0, v0, Ljrk;->eAF:Ljkh;

    .line 204
    :goto_0
    invoke-virtual {v0}, Ljkh;->getHour()I

    move-result v1

    .line 205
    invoke-virtual {v0}, Ljkh;->getMinute()I

    move-result v0

    .line 206
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dkY:Lop;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v3

    invoke-static {v2, v1, v0, v3}, Loi;->a(Lop;IIZ)Loi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dlc:Loi;

    .line 208
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dlc:Loi;

    const-string v1, "timepicker_tag"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->a(Landroid/app/DialogFragment;Ljava/lang/String;)V

    .line 209
    return-void

    .line 201
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    new-instance v1, Ljkh;

    invoke-direct {v1}, Ljkh;-><init>()V

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljkh;->qt(I)Ljkh;

    move-result-object v1

    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v1, v0}, Ljkh;->qu(I)Ljkh;

    move-result-object v0

    .line 202
    invoke-static {v0}, Lesi;->b(Ljkh;)V

    goto :goto_0
.end method

.method public final ahm()[Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 225
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->ajz()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    new-array v0, v1, [Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    aput-object v1, v0, v2

    .line 228
    :goto_0
    return-object v0

    :cond_0
    new-array v0, v1, [Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dla:Landroid/widget/TextView;

    aput-object v1, v0, v2

    goto :goto_0
.end method

.method public final synthetic b(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V
    .locals 0

    .prologue
    .line 33
    check-cast p1, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->a(Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;)V

    return-void
.end method

.method protected final onClick()V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;

    .line 186
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->ajB()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/AbsoluteTimeArgument;->ajz()Z

    move-result v0

    if-nez v0, :cond_1

    .line 188
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->aQL()V

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->performClick()Z

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    .line 70
    invoke-super {p0}, Lhou;->onFinishInflate()V

    .line 71
    const v0, 0x7f110405

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    .line 72
    const v0, 0x7f1100a9

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dla:Landroid/widget/TextView;

    .line 73
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dla:Landroid/widget/TextView;

    const v1, 0x7f0c014e

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 75
    new-instance v1, Lhnn;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v2}, Ldxw;->z(Ljava/util/List;)[Lelh;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lhnn;-><init>(Landroid/content/Context;[Lelh;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dlb:Lhnn;

    .line 78
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dlb:Lhnn;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    new-instance v1, Lhms;

    invoke-direct {v1, p0}, Lhms;-><init>(Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->a(Lekh;)V

    .line 93
    return-void
.end method

.method public final p(II)V
    .locals 2

    .prologue
    .line 162
    new-instance v0, Ljkh;

    invoke-direct {v0}, Ljkh;-><init>()V

    invoke-virtual {v0, p1}, Ljkh;->qt(I)Ljkh;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljkh;->qu(I)Ljkh;

    move-result-object v0

    .line 163
    new-instance v1, Ljrk;

    invoke-direct {v1}, Ljrk;-><init>()V

    .line 164
    iput-object v0, v1, Ljrk;->eAF:Ljkh;

    .line 165
    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->a(Ljrk;)V

    .line 166
    return-void
.end method

.method public final setEditable(Z)V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v0, p1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setEnabled(Z)V

    .line 220
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/AbsoluteTimeArgumentView;->dla:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 221
    return-void
.end method
