.class public Lcom/google/android/voicesearch/handsfree/HeadsetQueryCommitService;
.super Landroid/app/Service;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 45
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 46
    const-string v1, "com.google.android.voicesearch.handsfree.HandsFreeService.extra.QUERY_ALLOWED"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 47
    if-nez v1, :cond_0

    .line 55
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    invoke-virtual {v0}, Lhhq;->aPj()Lice;

    move-result-object v0

    .line 58
    const v1, 0x7f0a094f

    invoke-virtual {v0, v1, v3, v3}, Lice;->a(ILesk;Ljava/lang/String;)V

    .line 101
    :goto_0
    const/4 v0, 0x2

    return v0

    .line 70
    :cond_0
    sget-object v1, Lcgg;->aVt:Lcgg;

    invoke-virtual {v1}, Lcgg;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcgg;->aVv:Lcgg;

    invoke-virtual {v1}, Lcgg;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 76
    :cond_1
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    new-instance v2, Lhsy;

    invoke-direct {v2, p0, v0}, Lhsy;-><init>(Lcom/google/android/voicesearch/handsfree/HeadsetQueryCommitService;Ljava/lang/String;)V

    const-wide/16 v4, 0x96

    invoke-virtual {v1, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0

    .line 93
    :cond_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 94
    const-class v0, Lcom/google/android/voicesearch/handsfree/HandsFreeActivityLegacy;

    invoke-virtual {v1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 95
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 97
    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/handsfree/HeadsetQueryCommitService;->startActivity(Landroid/content/Intent;)V

    .line 98
    invoke-virtual {p0}, Lcom/google/android/voicesearch/handsfree/HeadsetQueryCommitService;->stopSelf()V

    goto :goto_0
.end method
