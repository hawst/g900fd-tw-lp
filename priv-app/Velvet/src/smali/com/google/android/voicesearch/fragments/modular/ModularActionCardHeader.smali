.class public Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;
.super Landroid/widget/LinearLayout;
.source "PG"


# static fields
.field private static final bLM:[I


# instance fields
.field private UG:Landroid/widget/TextView;

.field private bWI:Landroid/widget/ImageView;

.field private dmK:Landroid/view/View;

.field private dmL:Z

.field private dmM:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f0100c7

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->bLM:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method

.method private a(Landroid/transition/TransitionSet;)V
    .locals 2

    .prologue
    .line 182
    new-instance v0, Lejy;

    invoke-direct {v0}, Lejy;-><init>()V

    .line 183
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->UG:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/transition/Fade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 184
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->bWI:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/transition/Fade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 185
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->dmK:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/transition/Fade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 186
    invoke-virtual {p1, v0}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 187
    return-void
.end method

.method private a(Landroid/transition/TransitionSet;Landroid/transition/TransitionSet;ZZ)V
    .locals 4

    .prologue
    .line 166
    if-eqz p4, :cond_1

    .line 167
    new-instance v1, Libc;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz p3, :cond_0

    sget-object v0, Libb;->dwZ:Libb;

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-direct {v1, v2, v0, v3}, Libc;-><init>(Landroid/content/res/Resources;Libb;I)V

    .line 169
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->UG:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Libc;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 170
    invoke-virtual {p1, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 171
    invoke-virtual {p2, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 179
    :goto_1
    return-void

    .line 167
    :cond_0
    sget-object v0, Libb;->dwY:Libb;

    goto :goto_0

    .line 173
    :cond_1
    new-instance v2, Libg;

    if-eqz p3, :cond_2

    sget-object v0, Libb;->dwZ:Libb;

    move-object v1, v0

    :goto_2
    if-eqz p3, :cond_3

    sget-object v0, Libb;->dwY:Libb;

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-direct {v2, v1, v0, v3}, Libg;-><init>(Libb;Libb;I)V

    .line 176
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->UG:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Libg;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 177
    invoke-virtual {p1, v2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    goto :goto_1

    .line 173
    :cond_2
    sget-object v0, Libb;->dwY:Libb;

    move-object v1, v0

    goto :goto_2

    :cond_3
    sget-object v0, Libb;->dwZ:Libb;

    goto :goto_3
.end method

.method private a(Landroid/transition/TransitionSet;Z)V
    .locals 2

    .prologue
    .line 191
    if-eqz p2, :cond_0

    .line 192
    new-instance v0, Libi;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Libi;-><init>(Landroid/content/res/Resources;)V

    .line 193
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->UG:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Libi;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 194
    invoke-virtual {p1, v0}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 196
    :cond_0
    return-void
.end method

.method private b(ILhop;)V
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 199
    iget-boolean v3, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->dmL:Z

    if-eqz v3, :cond_0

    .line 201
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->UG:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    :goto_0
    return-void

    .line 205
    :cond_0
    invoke-virtual {p2}, Lhop;->aRk()I

    move-result v5

    .line 207
    packed-switch p1, :pswitch_data_0

    .line 228
    invoke-virtual {p2}, Lhop;->aRh()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    move v3, v1

    .line 231
    :goto_1
    if-ne v5, v6, :cond_4

    if-ne p1, v0, :cond_4

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->dmM:Z

    .line 233
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->drawableStateChanged()V

    .line 235
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->UG:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 236
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->UG:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    :cond_1
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->UG:Landroid/widget/TextView;

    if-eqz v3, :cond_5

    const/16 v0, 0x11

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0

    .line 209
    :pswitch_0
    if-ne v5, v6, :cond_2

    .line 210
    invoke-virtual {p2}, Lhop;->aRg()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    move v3, v1

    goto :goto_1

    .line 212
    :cond_2
    invoke-virtual {p2}, Lhop;->aRh()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    move v3, v1

    .line 214
    goto :goto_1

    .line 216
    :pswitch_1
    invoke-virtual {p2}, Lhop;->aLg()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v3

    .line 217
    if-ne v5, v2, :cond_3

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/utils/CardDecision;->ald()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 218
    invoke-virtual {p2}, Lhop;->aLg()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alc()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    move v3, v0

    .line 219
    goto :goto_1

    .line 221
    :cond_3
    invoke-virtual {p2}, Lhop;->aRh()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    move v3, v1

    .line 223
    goto :goto_1

    .line 225
    :pswitch_2
    invoke-virtual {p2}, Lhop;->aRi()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    move v3, v1

    .line 226
    goto :goto_1

    :cond_4
    move v0, v1

    .line 231
    goto :goto_2

    :cond_5
    move v0, v2

    .line 238
    goto :goto_3

    .line 207
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static c(ILhop;)Z
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    .line 254
    invoke-virtual {p1}, Lhop;->aRk()I

    move-result v1

    .line 256
    if-ne p0, v0, :cond_0

    if-eq v1, v2, :cond_1

    :cond_0
    if-ne p0, v2, :cond_2

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/transition/TransitionSet;Landroid/transition/TransitionSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 113
    invoke-direct {p0, p2}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->a(Landroid/transition/TransitionSet;)V

    .line 114
    if-ne p1, v1, :cond_0

    .line 115
    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, v1, v0}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->a(Landroid/transition/TransitionSet;Landroid/transition/TransitionSet;ZZ)V

    .line 117
    :cond_0
    return-void
.end method

.method public final a(ILhop;)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 67
    packed-switch p1, :pswitch_data_0

    .line 102
    :goto_0
    const/4 v0, 0x3

    if-ne p1, v0, :cond_4

    .line 103
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->UG:Landroid/widget/TextView;

    const v1, 0x7f0c00d7

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 104
    const v0, 0x7f0c00d5

    invoke-static {p0, v0}, Lehd;->r(Landroid/view/View;I)V

    .line 109
    :goto_1
    return-void

    .line 69
    :pswitch_0
    invoke-virtual {p0, v2}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->setVisibility(I)V

    goto :goto_0

    .line 72
    :pswitch_1
    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->setVisibility(I)V

    .line 73
    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->b(ILhop;)V

    .line 74
    invoke-virtual {p2}, Lhop;->aRj()Z

    move-result v3

    .line 75
    if-eqz v3, :cond_0

    .line 76
    invoke-virtual {p2}, Lhop;->ave()Lcom/google/android/shared/util/App;

    move-result-object v0

    .line 77
    if-eqz v0, :cond_1

    .line 78
    invoke-virtual {p2}, Lhop;->ave()Lcom/google/android/shared/util/App;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/shared/util/App;->ar(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 79
    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->bWI:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 80
    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->bWI:Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 86
    :cond_0
    :goto_2
    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->bWI:Landroid/widget/ImageView;

    if-eqz v3, :cond_2

    move v0, v1

    :goto_3
    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 87
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->dmK:Landroid/view/View;

    if-eqz v3, :cond_3

    :goto_4
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->bWI:Landroid/widget/ImageView;

    const v4, 0x7f020114

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    :cond_2
    move v0, v2

    .line 86
    goto :goto_3

    :cond_3
    move v1, v2

    .line 87
    goto :goto_4

    .line 90
    :pswitch_2
    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->setVisibility(I)V

    .line 91
    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->b(ILhop;)V

    .line 92
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->bWI:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->dmK:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 96
    :pswitch_3
    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->setVisibility(I)V

    .line 97
    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->b(ILhop;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->bWI:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->dmK:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 106
    :cond_4
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->UG:Landroid/widget/TextView;

    const v1, 0x7f0c0161

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 107
    const v0, 0x7f0c0160

    invoke-static {p0, v0}, Lehd;->r(Landroid/view/View;I)V

    goto/16 :goto_1

    .line 67
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public final a(ILhop;Landroid/transition/TransitionSet;Landroid/transition/TransitionSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 121
    invoke-direct {p0, p3}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->a(Landroid/transition/TransitionSet;)V

    .line 122
    invoke-static {v2, p2}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->c(ILhop;)Z

    move-result v0

    .line 124
    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    .line 125
    const/4 v1, 0x0

    invoke-direct {p0, p3, p4, v1, v0}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->a(Landroid/transition/TransitionSet;Landroid/transition/TransitionSet;ZZ)V

    .line 132
    :goto_0
    return-void

    .line 127
    :cond_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 128
    invoke-direct {p0, p3, p4, v2, v0}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->a(Landroid/transition/TransitionSet;Landroid/transition/TransitionSet;ZZ)V

    goto :goto_0

    .line 130
    :cond_1
    invoke-direct {p0, p4, v0}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->a(Landroid/transition/TransitionSet;Z)V

    goto :goto_0
.end method

.method public final b(ILhop;Landroid/transition/TransitionSet;Landroid/transition/TransitionSet;)V
    .locals 2

    .prologue
    .line 136
    invoke-direct {p0, p3}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->a(Landroid/transition/TransitionSet;)V

    .line 137
    const/4 v0, 0x2

    invoke-static {v0, p2}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->c(ILhop;)Z

    move-result v0

    .line 139
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 140
    const/4 v1, 0x0

    invoke-direct {p0, p3, p4, v1, v0}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->a(Landroid/transition/TransitionSet;Landroid/transition/TransitionSet;ZZ)V

    .line 144
    :goto_0
    return-void

    .line 142
    :cond_0
    invoke-direct {p0, p4, v0}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->a(Landroid/transition/TransitionSet;Z)V

    goto :goto_0
.end method

.method public final gF(Z)V
    .locals 0

    .prologue
    .line 155
    iput-boolean p1, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->dmL:Z

    .line 156
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->dmM:Z

    if-eqz v0, :cond_0

    .line 244
    add-int/lit8 p1, p1, 0x1

    .line 246
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 247
    iget-boolean v1, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->dmM:Z

    if-eqz v1, :cond_1

    .line 248
    sget-object v1, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->bLM:[I

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->mergeDrawableStates([I[I)[I

    .line 250
    :cond_1
    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 58
    const v0, 0x7f11006f

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->UG:Landroid/widget/TextView;

    .line 59
    const v0, 0x7f110070

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->bWI:Landroid/widget/ImageView;

    .line 60
    const v0, 0x7f110071

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->dmK:Landroid/view/View;

    .line 62
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->bWI:Landroid/widget/ImageView;

    const v1, 0x7f0c0132

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 63
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/ModularActionCardHeader;->dmK:Landroid/view/View;

    const v1, 0x7f0c010d

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 64
    return-void
.end method
