.class public Lcom/google/android/voicesearch/intentapi/IntentApiActivity;
.super Landroid/app/Activity;
.source "PG"


# instance fields
.field private dsY:Lhwj;

.field private mController:Lhkj;

.field private mIntentApiParams:Lhwg;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 84
    invoke-virtual {p0, p1, p2}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->setResult(ILandroid/content/Intent;)V

    .line 85
    return-void
.end method

.method public final lw(I)V
    .locals 0

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->setResult(I)V

    .line 77
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 30
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    new-instance v0, Lhwg;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lhwg;-><init>(Landroid/content/Intent;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mIntentApiParams:Lhwg;

    .line 34
    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mIntentApiParams:Lhwg;

    invoke-virtual {v0}, Lhwg;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    const-string v0, "IntentApiActivity"

    const-string v1, "ACTION_RECOGNIZE_SPEECH intent called incorrectly. Maybe you called startActivity, but you should have called startActivityForResult (or otherwise included a pending intent)."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 38
    invoke-virtual {p0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->finish()V

    .line 41
    :cond_0
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v1

    .line 44
    new-instance v0, Lhwj;

    invoke-virtual {v1}, Lhhq;->aAs()Lequ;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lhwj;-><init>(Landroid/app/Activity;Lequ;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->dsY:Lhwj;

    .line 45
    new-instance v5, Lhwh;

    invoke-direct {v5, v1}, Lhwh;-><init>(Lhhq;)V

    .line 47
    new-instance v0, Lhkj;

    iget-object v2, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->dsY:Lhwj;

    iget-object v3, v1, Lhhq;->mSettings:Lhym;

    invoke-virtual {v1}, Lhhq;->aOZ()Lhik;

    move-result-object v4

    iget-object v1, v1, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->aus()Leqo;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lhkj;-><init>(Lcom/google/android/voicesearch/intentapi/IntentApiActivity;Lhkq;Lhym;Lhik;Lhwh;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mController:Lhkj;

    .line 50
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mController:Lhkj;

    invoke-virtual {v0}, Lhkj;->cancel()V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    const/16 v0, 0x3e

    invoke-static {v0}, Lege;->ht(I)V

    .line 66
    invoke-static {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->bQ(Landroid/content/Context;)V

    .line 68
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 69
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 55
    invoke-static {p0}, Lcom/google/android/voicesearch/logger/EventLoggerService;->bP(Landroid/content/Context;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mController:Lhkj;

    iget-object v1, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mIntentApiParams:Lhwg;

    invoke-virtual {v0, v1}, Lhkj;->a(Lhwg;)V

    .line 57
    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/IntentApiActivity;->mController:Lhkj;

    invoke-virtual {v0}, Lhkj;->onResume()V

    .line 58
    return-void
.end method
