.class public Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;
.super Landroid/view/View;
.source "PG"


# instance fields
.field private bFQ:Landroid/animation/TimeAnimator;

.field public dwB:I

.field private dwC:I

.field public dwD:Lequ;

.field private dwE:I

.field private dwF:I

.field private ob:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    sget-object v0, Lbwe;->aLR:[I

    invoke-virtual {p1, p2, v0, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 58
    const-string v1, "#66FFFFFF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v5, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->dwE:I

    .line 61
    invoke-virtual {v0, v6, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->dwF:I

    .line 64
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 65
    const/4 v2, 0x3

    const v3, 0x7f02024b

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->dwC:I

    .line 67
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 71
    new-instance v0, Lequ;

    invoke-direct {v0}, Lequ;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->dwD:Lequ;

    .line 72
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->dwD:Lequ;

    invoke-virtual {v0, v5}, Lequ;->gp(I)V

    .line 74
    new-instance v0, Landroid/animation/TimeAnimator;

    invoke-direct {v0}, Landroid/animation/TimeAnimator;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->bFQ:Landroid/animation/TimeAnimator;

    .line 75
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->bFQ:Landroid/animation/TimeAnimator;

    invoke-virtual {v0, v4}, Landroid/animation/TimeAnimator;->setRepeatCount(I)V

    .line 76
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->bFQ:Landroid/animation/TimeAnimator;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/animation/TimeAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 77
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->bFQ:Landroid/animation/TimeAnimator;

    new-instance v2, Liam;

    invoke-direct {v2, p0}, Liam;-><init>(Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;)V

    invoke-virtual {v0, v2}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    .line 93
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->ob:Landroid/graphics/Paint;

    .line 94
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 95
    invoke-direct {p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->aUG()V

    .line 96
    return-void
.end method

.method private aUG()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->bFQ:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->bFQ:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->start()V

    .line 143
    :cond_0
    return-void
.end method


# virtual methods
.method public final c(Lequ;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->dwD:Lequ;

    .line 162
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 147
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 148
    invoke-direct {p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->aUG()V

    .line 149
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->bFQ:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->cancel()V

    .line 154
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 155
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iget v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->dwE:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 168
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->dwC:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->dwB:I

    mul-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x64

    iget v1, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->dwC:I

    add-int/2addr v0, v1

    .line 169
    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    int-to-float v0, v0

    iget-object v3, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->ob:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 173
    :goto_0
    return-void

    .line 171
    :cond_0
    iget v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->dwF:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 115
    instance-of v0, p1, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$SavedState;

    if-nez v0, :cond_1

    .line 116
    invoke-super {p0, p1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    check-cast p1, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$SavedState;

    .line 121
    invoke-virtual {p1}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 123
    iget-boolean v0, p1, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$SavedState;->dwH:Z

    if-eqz v0, :cond_0

    .line 124
    invoke-direct {p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->aUG()V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 108
    new-instance v0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$SavedState;

    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 109
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->bFQ:Landroid/animation/TimeAnimator;

    invoke-virtual {v1}, Landroid/animation/TimeAnimator;->isStarted()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView$SavedState;->dwH:Z

    .line 110
    return-object v0
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 130
    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 132
    if-eqz p1, :cond_0

    .line 133
    invoke-direct {p0}, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->aUG()V

    .line 137
    :goto_0
    return-void

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/DrawSoundLevelsView;->bFQ:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->cancel()V

    goto :goto_0
.end method
