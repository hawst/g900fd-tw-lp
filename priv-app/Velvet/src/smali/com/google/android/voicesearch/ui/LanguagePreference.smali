.class public Lcom/google/android/voicesearch/ui/LanguagePreference;
.super Landroid/preference/ListPreference;
.source "PG"


# instance fields
.field public drQ:I

.field private dwI:[Ljava/lang/String;

.field public dwJ:Ljava/util/List;

.field public dwK:Ljava/lang/String;

.field private dwL:Liav;

.field public dwM:Liaw;

.field public final mSettings:Lhym;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 78
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/ui/LanguagePreference;-><init>(Landroid/content/Context;Lhym;)V

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    .line 90
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    .line 91
    new-instance v0, Liaw;

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->getHotwordConfigMap()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Liaw;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwM:Liaw;

    .line 92
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->JV()I

    move-result v0

    iput v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->drQ:I

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lhym;)V
    .locals 2

    .prologue
    .line 82
    invoke-direct {p0, p1}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    .line 83
    iput-object p2, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    .line 84
    new-instance v0, Liaw;

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->getHotwordConfigMap()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Liaw;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwM:Liaw;

    .line 85
    invoke-virtual {p2}, Lhym;->JV()I

    move-result v0

    iput v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->drQ:I

    .line 86
    return-void
.end method

.method public static synthetic a(Lcom/google/android/voicesearch/ui/LanguagePreference;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 46
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lgnq;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a085f

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public static b(Landroid/content/Context;Lhym;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 533
    invoke-virtual {p1}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    .line 534
    invoke-virtual {p1}, Lhym;->aTI()Ljava/util/List;

    move-result-object v1

    .line 535
    invoke-virtual {p1}, Lhym;->aER()Ljze;

    move-result-object v2

    invoke-static {v2, v0}, Lgnq;->b(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 537
    invoke-virtual {p1}, Lhym;->aTH()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 538
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 539
    const v1, 0x7f0a0864

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 550
    :goto_0
    return-object v0

    .line 542
    :cond_0
    const v2, 0x7f0a0863

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 546
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 547
    const v1, 0x7f0a0862

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 550
    :cond_2
    const v2, 0x7f0a0861

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Lhym;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 574
    invoke-virtual {p1}, Lhym;->aER()Ljze;

    move-result-object v0

    .line 576
    invoke-virtual {p1}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v1

    .line 578
    invoke-static {v0, v1, p0}, Lgnq;->a(Ljze;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 582
    invoke-virtual {p1}, Lhym;->aTI()Ljava/util/List;

    move-result-object v2

    .line 584
    invoke-static {v0, v2, p0}, Lgnq;->a(Ljze;Ljava/util/List;Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 587
    invoke-virtual {p1}, Lhym;->aTH()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 588
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 589
    const v0, 0x7f0a0866

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v1, v2, v4

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 599
    :goto_0
    return-object v0

    .line 592
    :cond_0
    const v2, 0x7f0a0865

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v4

    const-string v1, ", "

    invoke-static {v1}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v1

    invoke-virtual {v1, v0}, Lifj;->n(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 596
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 597
    const v0, 0x7f0a0868

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v1, v2, v4

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 599
    :cond_2
    const v2, 0x7f0a0867

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v4

    const-string v1, ", "

    invoke-static {v1}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v1

    invoke-virtual {v1, v0}, Lifj;->n(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/app/AlertDialog$Builder;)V
    .locals 5

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwK:Ljava/lang/String;

    .line 336
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aTI()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    .line 337
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwK:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 340
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    invoke-static {v0}, Lgnq;->c(Ljze;)Ljava/util/ArrayList;

    move-result-object v1

    .line 342
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    invoke-static {v0}, Lgnq;->d(Ljze;)Ljava/util/ArrayList;

    move-result-object v0

    .line 345
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 348
    new-instance v2, Liau;

    invoke-direct {v2, p0, v1}, Liau;-><init>(Lcom/google/android/voicesearch/ui/LanguagePreference;Ljava/util/List;)V

    .line 350
    new-instance v1, Liat;

    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, p0, v3}, Liat;-><init>(Lcom/google/android/voicesearch/ui/LanguagePreference;Landroid/content/Context;)V

    .line 353
    const v3, 0x7f0a085c

    invoke-virtual {p1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4, v2}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0a085d

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 356
    return-void
.end method

.method public final a(Landroid/content/Context;Landroid/content/DialogInterface;Ljava/util/List;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 420
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v1

    .line 422
    invoke-interface {p3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsi;

    .line 425
    iget-object v2, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwK:Ljava/lang/String;

    invoke-static {v1, v2}, Lgnq;->b(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 426
    invoke-virtual {v0}, Lcsi;->getLocale()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lgnq;->b(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 431
    iget-object v3, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwK:Ljava/lang/String;

    invoke-static {v2, v3, p1}, Lgnq;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 433
    invoke-virtual {v0}, Lcsi;->getLocale()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, p1}, Lgnq;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 437
    const v3, 0x7f0a0869

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v5

    const/4 v2, 0x1

    aput-object v1, v4, v2

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 441
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 443
    const v3, 0x7f0400d4

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 445
    const v1, 0x7f110280

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 447
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->getHotwordConfigMap()Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwK:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 448
    const v1, 0x7f110281

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 449
    invoke-virtual {v1}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 452
    :cond_0
    invoke-virtual {v0}, Lcsi;->SE()Z

    move-result v1

    if-nez v1, :cond_1

    .line 453
    const v1, 0x7f110282

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 454
    invoke-virtual {v1}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 457
    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a086a

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a086c

    new-instance v3, Lias;

    invoke-direct {v3, p0, v0, p2}, Lias;-><init>(Lcom/google/android/voicesearch/ui/LanguagePreference;Lcsi;Landroid/content/DialogInterface;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a086b

    new-instance v2, Liar;

    invoke-direct {v2, p0, p2}, Liar;-><init>(Lcom/google/android/voicesearch/ui/LanguagePreference;Landroid/content/DialogInterface;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 478
    return-void
.end method

.method public final a(Landroid/content/DialogInterface;)V
    .locals 4

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwK:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 508
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 509
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwK:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aER()Ljze;

    move-result-object v2

    invoke-static {v2, v1}, Lgnq;->g(Ljze;Ljava/lang/String;)Ljzh;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v3}, Lhym;->aER()Ljze;

    move-result-object v3

    invoke-static {v3, v0}, Lgnq;->b(Ljze;[Ljava/lang/String;)[Ljzh;

    move-result-object v0

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    const/16 v0, 0x42

    invoke-static {v0}, Lege;->ht(I)V

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/ui/LanguagePreference;->setValue(Ljava/lang/String;)V

    .line 510
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 511
    return-void
.end method

.method public final a(Liav;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwL:Liav;

    .line 97
    return-void
.end method

.method public final oS(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 105
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    invoke-static {v0, p1}, Lgnq;->j(Ljze;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwI:[Ljava/lang/String;

    .line 110
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwI:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 111
    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/ui/LanguagePreference;->oT(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    :goto_0
    return-void

    .line 115
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->showDialog(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final oT(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    invoke-static {v0, p1}, Lgnq;->k(Ljze;Ljava/lang/String;)Ljzh;

    move-result-object v0

    .line 205
    if-eqz v0, :cond_0

    .line 206
    const/16 v1, 0x42

    invoke-static {v1}, Lege;->ht(I)V

    .line 208
    invoke-virtual {v0}, Ljzh;->bwQ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->setValue(Ljava/lang/String;)V

    .line 209
    const/4 v0, 0x1

    .line 211
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 6

    .prologue
    const v5, 0x7f0400fc

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 248
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->JW()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/ui/LanguagePreference;->a(Landroid/app/AlertDialog$Builder;)V

    .line 253
    :goto_0
    return-void

    .line 251
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwI:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwI:[Ljava/lang/String;

    const v1, 0x7f0a085a

    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v5, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    new-instance v2, Liap;

    invoke-direct {v2, p0, v0}, Liap;-><init>(Lcom/google/android/voicesearch/ui/LanguagePreference;[Ljava/lang/String;)V

    invoke-virtual {p1, v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iput-object v3, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwI:[Ljava/lang/String;

    :goto_1
    invoke-virtual {p1, v3, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    const-string v1, "\u2026"

    invoke-static {v0, v1}, Lgnq;->i(Ljze;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v5, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    new-instance v2, Liaq;

    invoke-direct {v2, p0, v0}, Liaq;-><init>(Lcom/google/android/voicesearch/ui/LanguagePreference;[Ljava/lang/String;)V

    invoke-virtual {p1, v1, v4, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwL:Liav;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 240
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwK:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 242
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwL:Liav;

    invoke-interface {v1, p0, p1, v0}, Liav;->a(Lcom/google/android/voicesearch/ui/LanguagePreference;Ljava/lang/String;Ljava/util/List;)V

    .line 244
    :cond_0
    return-void
.end method

.method protected showDialog(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 120
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->showDialog(Landroid/os/Bundle;)V

    .line 124
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->JW()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0a085e

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 130
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    .line 131
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v3

    .line 134
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aER()Ljze;

    move-result-object v0

    invoke-static {v0}, Lgnq;->c(Ljze;)Ljava/util/ArrayList;

    move-result-object v0

    .line 136
    iget-object v2, p0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    invoke-static {v0, v2}, Lgnq;->c(Ljava/util/List;Ljava/util/List;)[Z

    move-result-object v4

    .line 139
    array-length v5, v4

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_0

    aget-boolean v6, v4, v0

    .line 140
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v3, v1, v6}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 139
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    .line 147
    :cond_0
    new-instance v0, Liao;

    invoke-direct {v0, p0}, Liao;-><init>(Lcom/google/android/voicesearch/ui/LanguagePreference;)V

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 185
    :cond_1
    return-void
.end method
