.class public Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;
.super Landroid/app/Activity;
.source "PG"


# static fields
.field private static final CLIENT_CONFIG:Lcom/google/android/search/shared/service/ClientConfig;

.field private static final cXI:Lcom/google/android/shared/search/SearchBoxStats;


# instance fields
.field private aOO:Lecq;

.field private final anp:Lecr;

.field private dsY:Lhwj;

.field private dtx:Lcom/google/android/shared/search/Query;

.field private dty:Lcom/google/android/search/shared/actions/errors/SearchError;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 61
    const-string v0, "voiceCommandIntent"

    const-string v1, "android-search-app"

    invoke-static {v0, v1}, Lcom/google/android/shared/search/SearchBoxStats;->aA(Ljava/lang/String;Ljava/lang/String;)Lehj;

    move-result-object v0

    invoke-virtual {v0}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v0

    sput-object v0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->cXI:Lcom/google/android/shared/search/SearchBoxStats;

    .line 64
    new-instance v0, Lcom/google/android/search/shared/service/ClientConfig;

    const-wide/32 v2, 0x80020c0

    sget-object v1, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->cXI:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/search/shared/service/ClientConfig;-><init>(JLcom/google/android/shared/search/SearchBoxStats;)V

    sput-object v0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->CLIENT_CONFIG:Lcom/google/android/search/shared/service/ClientConfig;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 72
    new-instance v0, Lhwn;

    invoke-direct {v0, p0}, Lhwn;-><init>(Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->anp:Lecr;

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->dty:Lcom/google/android/search/shared/actions/errors/SearchError;

    .line 218
    return-void
.end method

.method public static synthetic a(Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;Lcom/google/android/search/shared/actions/errors/SearchError;)Lcom/google/android/search/shared/actions/errors/SearchError;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->dty:Lcom/google/android/search/shared/actions/errors/SearchError;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;)V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->aOO:Lecq;

    invoke-virtual {v0}, Lecq;->any()V

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->aOO:Lecq;

    iget-object v1, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->dtx:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lecq;->y(Lcom/google/android/shared/search/Query;)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;)Lhwj;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->dsY:Lhwj;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;)Lecq;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->aOO:Lecq;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;)Lcom/google/android/search/shared/actions/errors/SearchError;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->dty:Lcom/google/android/search/shared/actions/errors/SearchError;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;)Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->dtx:Lcom/google/android/shared/search/Query;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 92
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 96
    new-instance v6, Lhwg;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v6, v0, v1}, Lhwg;-><init>(Landroid/content/Intent;Ljava/lang/String;)V

    .line 99
    invoke-virtual {v6}, Lhwg;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "VoiceCommandActivity"

    const-string v1, "RECOGNIZE_VOICE_COMMAND intent called incorrectly. Missing calling package."

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->finish()V

    .line 104
    :cond_0
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    sget-object v1, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->cXI:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->api()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v6}, Lhwg;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->kW(Ljava/lang/String;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->dtx:Lcom/google/android/shared/search/Query;

    .line 109
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    .line 111
    new-instance v1, Lhwj;

    invoke-virtual {v0}, Lhhq;->aAs()Lequ;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lhwj;-><init>(Landroid/app/Activity;Lequ;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->dsY:Lhwj;

    .line 113
    new-instance v3, Lhwq;

    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->dsY:Lhwj;

    new-instance v1, Lhwp;

    invoke-direct {v1, p0, p0, v6, v7}, Lhwp;-><init>(Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;Landroid/app/Activity;Lhwg;B)V

    invoke-direct {v3, v0, v1}, Lhwq;-><init>(Lhkq;Lhwp;)V

    .line 117
    new-instance v0, Lecq;

    iget-object v2, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->anp:Lecr;

    sget-object v4, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->CLIENT_CONFIG:Lcom/google/android/search/shared/service/ClientConfig;

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lecq;-><init>(Landroid/content/Context;Lecr;Lecm;Lcom/google/android/search/shared/service/ClientConfig;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->aOO:Lecq;

    .line 120
    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->dsY:Lhwj;

    new-instance v1, Lhwo;

    invoke-direct {v1, p0, v7}, Lhwo;-><init>(Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;B)V

    iput-object v1, v0, Lhwj;->dtn:Lhkr;

    .line 121
    invoke-virtual {v6}, Lhwg;->getPrompt()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 122
    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->dsY:Lhwj;

    invoke-virtual {v6}, Lhwg;->getPrompt()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhwj;->ob(Ljava/lang/String;)V

    .line 124
    :cond_1
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 135
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 136
    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->aOO:Lecq;

    invoke-virtual {v0}, Lecq;->stopListening()V

    .line 137
    invoke-virtual {p0}, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->finish()V

    .line 138
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 128
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 129
    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->dsY:Lhwj;

    invoke-virtual {v0}, Lhwj;->aQv()V

    .line 130
    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->aOO:Lecq;

    invoke-virtual {v0}, Lecq;->connect()V

    .line 131
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 142
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 143
    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->aOO:Lecq;

    invoke-virtual {v0}, Lecq;->disconnect()V

    .line 144
    iget-object v0, p0, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->aOO:Lecq;

    invoke-virtual {v0}, Lecq;->stop()V

    .line 145
    return-void
.end method
