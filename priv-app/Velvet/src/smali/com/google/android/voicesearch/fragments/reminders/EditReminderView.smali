.class public Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Lhpq;


# instance fields
.field private apG:Landroid/view/View;

.field private apP:Lcom/google/android/search/shared/actions/RemindersConfigFlags;

.field private ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

.field private final dkY:Lop;

.field private final dlS:Lekh;

.field private dlT:Lhqk;

.field private dlU:Lhqk;

.field private final dlq:Lnk;

.field private dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

.field private dlt:Ldxw;

.field private final dmR:Lajr;

.field private final dnH:Landroid/text/TextWatcher;

.field private final dnI:Landroid/view/View$OnFocusChangeListener;

.field private final dnJ:Landroid/view/View$OnTouchListener;

.field private final dnK:Landroid/view/View$OnTouchListener;

.field private final dnL:Lekh;

.field private final dnM:Lekh;

.field private final dnN:Landroid/view/View$OnClickListener;

.field private final dnO:Landroid/view/View$OnClickListener;

.field public dnP:Landroid/widget/EditText;

.field private dnQ:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

.field private dnR:Landroid/widget/TextView;

.field private dnS:Landroid/widget/TextView;

.field private dnT:Landroid/widget/TextView;

.field public dnU:Lelg;

.field public dnV:Lelg;

.field private dnW:Lelf;

.field private dnX:Lhqi;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private dnY:[Landroid/view/View;

.field private mActionCardEventLogger:Legs;

.field public mLocationTriggerView:Landroid/widget/RadioButton;

.field public mPresenter:Lhpn;

.field public mTimeTriggerView:Landroid/widget/RadioButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 374
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 101
    new-instance v0, Lhpr;

    invoke-direct {v0, p0}, Lhpr;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnH:Landroid/text/TextWatcher;

    .line 120
    new-instance v0, Lhqa;

    invoke-direct {v0, p0}, Lhqa;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnI:Landroid/view/View$OnFocusChangeListener;

    .line 129
    new-instance v0, Lhqb;

    invoke-direct {v0, p0}, Lhqb;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnJ:Landroid/view/View$OnTouchListener;

    .line 142
    new-instance v0, Lhqc;

    invoke-direct {v0, p0}, Lhqc;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnK:Landroid/view/View$OnTouchListener;

    .line 153
    new-instance v0, Lhqd;

    invoke-direct {v0, p0}, Lhqd;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnL:Lekh;

    .line 187
    new-instance v0, Lhqe;

    invoke-direct {v0, p0}, Lhqe;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnM:Lekh;

    .line 205
    new-instance v0, Lhqf;

    invoke-direct {v0, p0}, Lhqf;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlq:Lnk;

    .line 215
    new-instance v0, Lhqg;

    invoke-direct {v0, p0}, Lhqg;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dkY:Lop;

    .line 225
    new-instance v0, Lhqh;

    invoke-direct {v0, p0}, Lhqh;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnN:Landroid/view/View$OnClickListener;

    .line 258
    new-instance v0, Lhps;

    invoke-direct {v0, p0}, Lhps;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnO:Landroid/view/View$OnClickListener;

    .line 266
    new-instance v0, Lhpt;

    invoke-direct {v0, p0}, Lhpt;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dmR:Lajr;

    .line 287
    new-instance v0, Lhpu;

    invoke-direct {v0, p0}, Lhpu;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlS:Lekh;

    .line 305
    new-instance v0, Lhpv;

    invoke-direct {v0, p0}, Lhpv;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlT:Lhqk;

    .line 325
    new-instance v0, Lhpx;

    invoke-direct {v0, p0}, Lhpx;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlU:Lhqk;

    .line 375
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 378
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 101
    new-instance v0, Lhpr;

    invoke-direct {v0, p0}, Lhpr;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnH:Landroid/text/TextWatcher;

    .line 120
    new-instance v0, Lhqa;

    invoke-direct {v0, p0}, Lhqa;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnI:Landroid/view/View$OnFocusChangeListener;

    .line 129
    new-instance v0, Lhqb;

    invoke-direct {v0, p0}, Lhqb;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnJ:Landroid/view/View$OnTouchListener;

    .line 142
    new-instance v0, Lhqc;

    invoke-direct {v0, p0}, Lhqc;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnK:Landroid/view/View$OnTouchListener;

    .line 153
    new-instance v0, Lhqd;

    invoke-direct {v0, p0}, Lhqd;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnL:Lekh;

    .line 187
    new-instance v0, Lhqe;

    invoke-direct {v0, p0}, Lhqe;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnM:Lekh;

    .line 205
    new-instance v0, Lhqf;

    invoke-direct {v0, p0}, Lhqf;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlq:Lnk;

    .line 215
    new-instance v0, Lhqg;

    invoke-direct {v0, p0}, Lhqg;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dkY:Lop;

    .line 225
    new-instance v0, Lhqh;

    invoke-direct {v0, p0}, Lhqh;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnN:Landroid/view/View$OnClickListener;

    .line 258
    new-instance v0, Lhps;

    invoke-direct {v0, p0}, Lhps;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnO:Landroid/view/View$OnClickListener;

    .line 266
    new-instance v0, Lhpt;

    invoke-direct {v0, p0}, Lhpt;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dmR:Lajr;

    .line 287
    new-instance v0, Lhpu;

    invoke-direct {v0, p0}, Lhpu;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlS:Lekh;

    .line 305
    new-instance v0, Lhpv;

    invoke-direct {v0, p0}, Lhpv;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlT:Lhqk;

    .line 325
    new-instance v0, Lhpx;

    invoke-direct {v0, p0}, Lhpx;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlU:Lhqk;

    .line 379
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 382
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 101
    new-instance v0, Lhpr;

    invoke-direct {v0, p0}, Lhpr;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnH:Landroid/text/TextWatcher;

    .line 120
    new-instance v0, Lhqa;

    invoke-direct {v0, p0}, Lhqa;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnI:Landroid/view/View$OnFocusChangeListener;

    .line 129
    new-instance v0, Lhqb;

    invoke-direct {v0, p0}, Lhqb;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnJ:Landroid/view/View$OnTouchListener;

    .line 142
    new-instance v0, Lhqc;

    invoke-direct {v0, p0}, Lhqc;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnK:Landroid/view/View$OnTouchListener;

    .line 153
    new-instance v0, Lhqd;

    invoke-direct {v0, p0}, Lhqd;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnL:Lekh;

    .line 187
    new-instance v0, Lhqe;

    invoke-direct {v0, p0}, Lhqe;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnM:Lekh;

    .line 205
    new-instance v0, Lhqf;

    invoke-direct {v0, p0}, Lhqf;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlq:Lnk;

    .line 215
    new-instance v0, Lhqg;

    invoke-direct {v0, p0}, Lhqg;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dkY:Lop;

    .line 225
    new-instance v0, Lhqh;

    invoke-direct {v0, p0}, Lhqh;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnN:Landroid/view/View$OnClickListener;

    .line 258
    new-instance v0, Lhps;

    invoke-direct {v0, p0}, Lhps;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnO:Landroid/view/View$OnClickListener;

    .line 266
    new-instance v0, Lhpt;

    invoke-direct {v0, p0}, Lhpt;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dmR:Lajr;

    .line 287
    new-instance v0, Lhpu;

    invoke-direct {v0, p0}, Lhpu;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlS:Lekh;

    .line 305
    new-instance v0, Lhpv;

    invoke-direct {v0, p0}, Lhpv;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlT:Lhqk;

    .line 325
    new-instance v0, Lhpx;

    invoke-direct {v0, p0}, Lhpx;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlU:Lhqk;

    .line 383
    return-void
.end method

.method public static synthetic a(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)Lcom/google/android/shared/ui/SpinnerAlwaysCallback;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    return-object v0
.end method

.method private a(Ljava/lang/String;ZLhqk;)Lhql;
    .locals 2

    .prologue
    .line 955
    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lhql;

    .line 957
    if-nez v0, :cond_0

    .line 958
    new-instance v0, Lhql;

    invoke-direct {v0}, Lhql;-><init>()V

    .line 959
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhql;->bL(Landroid/content/Context;)V

    .line 960
    invoke-virtual {v0}, Lhql;->aRv()Lhqm;

    move-result-object v1

    iput-boolean p2, v1, Lhqm;->doj:Z

    .line 961
    invoke-virtual {v0, p3}, Lhql;->a(Lhqk;)V

    .line 962
    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 964
    :cond_0
    return-object v0
.end method

.method private a(Landroid/app/DialogFragment;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 817
    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 819
    if-eqz v0, :cond_0

    .line 820
    invoke-virtual {p1, v0, p2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 822
    :cond_0
    return-void
.end method

.method private a(Lelh;)V
    .locals 2

    .prologue
    .line 681
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnU:Lelg;

    invoke-virtual {v0, p1}, Lelg;->getPosition(Ljava/lang/Object;)I

    move-result v1

    .line 682
    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 683
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->hY(I)V

    .line 684
    return-void

    .line 682
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)Legs;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mActionCardEventLogger:Legs;

    return-object v0
.end method

.method private getFragmentManager()Landroid/app/FragmentManager;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 840
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v2, v0, Landroid/app/Activity;

    if-eqz v2, :cond_0

    check-cast v0, Landroid/app/Activity;

    .line 841
    :goto_0
    if-nez v0, :cond_2

    move-object v0, v1

    .line 844
    :goto_1
    return-object v0

    .line 840
    :cond_0
    instance-of v2, v0, Landroid/view/ContextThemeWrapper;

    if-eqz v2, :cond_1

    check-cast v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {v0}, Landroid/view/ContextThemeWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    goto :goto_0

    :cond_1
    const-string v0, "EditReminderView"

    const-string v2, "Unable to get activity from context"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_0

    .line 844
    :cond_2
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    goto :goto_1
.end method

.method public static lp(I)I
    .locals 1

    .prologue
    .line 715
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final D(J)V
    .locals 3

    .prologue
    .line 645
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnU:Lelg;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0xa12

    invoke-static {v1, p1, p2, v2}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lelg;->F(Ljava/lang/CharSequence;)V

    .line 647
    return-void
.end method

.method public final E(J)V
    .locals 3

    .prologue
    .line 651
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnV:Lelg;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0xa01

    invoke-static {v1, p1, p2, v2}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lelg;->F(Ljava/lang/CharSequence;)V

    .line 653
    return-void
.end method

.method public final F(J)V
    .locals 3

    .prologue
    .line 432
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnU:Lelg;

    invoke-virtual {v0}, Lelg;->clear()V

    .line 433
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnU:Lelg;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlt:Ldxw;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->apP:Lcom/google/android/search/shared/actions/RemindersConfigFlags;

    invoke-virtual {v1, p1, p2, v2}, Ldxw;->a(JLcom/google/android/search/shared/actions/RemindersConfigFlags;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ldxw;->z(Ljava/util/List;)[Lelh;

    move-result-object v1

    invoke-virtual {v0, v1}, Lelg;->addAll([Ljava/lang/Object;)V

    .line 436
    return-void
.end method

.method public final Z(II)V
    .locals 2

    .prologue
    .line 688
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->vj()V

    .line 689
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dkY:Lop;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    invoke-static {v0, p1, p2, v1}, Loi;->a(Lop;IIZ)Loi;

    move-result-object v0

    .line 691
    const-string v1, "timepicker_tag"

    invoke-direct {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->a(Landroid/app/DialogFragment;Ljava/lang/String;)V

    .line 692
    return-void
.end method

.method public final a(Lcom/google/android/search/shared/actions/RemindersConfigFlags;)V
    .locals 0

    .prologue
    .line 386
    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->apP:Lcom/google/android/search/shared/actions/RemindersConfigFlags;

    .line 387
    return-void
.end method

.method public final a(Ldyf;Ljava/util/List;)V
    .locals 2

    .prologue
    .line 851
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setVisibility(I)V

    .line 853
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnV:Lelg;

    invoke-virtual {v0}, Lelg;->clear()V

    .line 855
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlt:Ldxw;

    invoke-virtual {v0, p2}, Ldxw;->y(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 856
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnV:Lelg;

    invoke-virtual {v0, v1}, Lelg;->addAll(Ljava/util/Collection;)V

    .line 857
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnV:Lelg;

    invoke-virtual {v0}, Lelg;->notifyDataSetChanged()V

    .line 859
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlt:Ldxw;

    invoke-virtual {v0}, Ldxw;->akX()Lelh;

    move-result-object v0

    :goto_0
    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 862
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v1, v0}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->hY(I)V

    .line 863
    return-void

    .line 859
    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlt:Ldxw;

    invoke-virtual {v0, p1}, Ldxw;->b(Ldyf;)Lelh;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a(Legs;)V
    .locals 0

    .prologue
    .line 945
    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mActionCardEventLogger:Legs;

    .line 946
    return-void
.end method

.method public final a(Lhpn;)V
    .locals 3

    .prologue
    .line 390
    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    .line 391
    invoke-direct {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 393
    if-eqz v1, :cond_4

    .line 394
    const-string v0, "datepicker_tag"

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lnh;

    .line 397
    if-eqz v0, :cond_0

    .line 398
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlq:Lnk;

    invoke-virtual {v0, v2}, Lnh;->a(Lnk;)V

    .line 401
    :cond_0
    const-string v0, "timepicker_tag"

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Loi;

    .line 404
    if-eqz v0, :cond_1

    .line 405
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dkY:Lop;

    invoke-virtual {v0, v2}, Loi;->a(Lop;)V

    .line 408
    :cond_1
    const-string v0, "recurrencepicker_tag"

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/recurrencepicker/RecurrencePickerDialog;

    .line 410
    if-eqz v0, :cond_2

    .line 411
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dmR:Lajr;

    invoke-virtual {v0, v2}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->a(Lajr;)V

    .line 414
    :cond_2
    const-string v0, "custom_location_tag"

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lhql;

    .line 416
    if-eqz v0, :cond_3

    .line 417
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlT:Lhqk;

    invoke-virtual {v0, v2}, Lhql;->a(Lhqk;)V

    .line 420
    :cond_3
    const-string v0, "edit_place_location_tag"

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lhql;

    .line 422
    if-eqz v0, :cond_4

    .line 423
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlU:Lhqk;

    invoke-virtual {v0, v1}, Lhql;->a(Lhqk;)V

    .line 427
    :cond_4
    return-void
.end method

.method public final a(Lhqi;)V
    .locals 0

    .prologue
    .line 934
    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnX:Lhqi;

    .line 935
    return-void
.end method

.method protected final aRu()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 950
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnN:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public final b(Laiw;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 608
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 611
    if-eqz p1, :cond_2

    .line 612
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, v1, v1}, Laju;->a(Landroid/content/Context;Laiw;ZZ)Ljava/lang/String;

    move-result-object v0

    .line 613
    if-nez v0, :cond_1

    .line 614
    const v0, 0x7f0a08a6

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 615
    const-string v1, "EditReminderView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can\'t generate display string for recurrence: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 616
    const/4 v1, 0x0

    .line 629
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnR:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 630
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnR:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 631
    return-void

    .line 620
    :cond_1
    invoke-static {p1}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->a(Laiw;)Z

    move-result v1

    .line 621
    if-nez v1, :cond_0

    .line 622
    const-string v2, "EditReminderView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UI can\'t handle recurrence:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 626
    :cond_2
    const v0, 0x7f0a0575

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lhqq;Lefk;)V
    .locals 8

    .prologue
    .line 741
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    if-eqz v0, :cond_2

    .line 742
    invoke-virtual {p1}, Lhqq;->aRz()Lixu;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 744
    new-instance v0, Ljpd;

    invoke-direct {v0}, Ljpd;-><init>()V

    .line 745
    invoke-virtual {p1}, Lhqq;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljpd;->xV(Ljava/lang/String;)Ljpd;

    .line 746
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljpd;->iQ(Z)Ljpd;

    .line 747
    invoke-virtual {p1}, Lhqq;->aRz()Lixu;

    move-result-object v1

    .line 748
    iget-object v2, v1, Lixu;->dOG:Lixv;

    .line 749
    new-instance v3, Ljpb;

    invoke-direct {v3}, Ljpb;-><init>()V

    .line 750
    new-instance v4, Ljpc;

    invoke-direct {v4}, Ljpc;-><init>()V

    .line 751
    iput-object v4, v3, Ljpb;->ewY:Ljpc;

    .line 752
    iget-object v5, v2, Lixv;->dMA:Ljbc;

    if-eqz v5, :cond_0

    .line 753
    iget-object v2, v2, Lixv;->dMA:Ljbc;

    .line 755
    new-instance v5, Ljpf;

    invoke-direct {v5}, Ljpf;-><init>()V

    .line 756
    invoke-virtual {v2}, Ljbc;->beI()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v2}, Ljbc;->beK()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 757
    invoke-virtual {v2}, Ljbc;->beH()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljpf;->dE(J)Ljpf;

    .line 758
    invoke-virtual {v2}, Ljbc;->beJ()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljpf;->dF(J)Ljpf;

    .line 759
    iput-object v5, v4, Ljpc;->exb:Ljpf;

    .line 762
    :cond_0
    invoke-virtual {v1}, Lixu;->baV()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 763
    invoke-virtual {v1}, Lixu;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljpb;->xU(Ljava/lang/String;)Ljpb;

    .line 765
    :cond_1
    iput-object v3, v0, Ljpd;->exz:Ljpb;

    .line 766
    invoke-interface {p2, v0}, Lefk;->ar(Ljava/lang/Object;)V

    .line 787
    :cond_2
    :goto_0
    return-void

    .line 767
    :cond_3
    invoke-virtual {p1}, Lhqq;->aRA()Ljbr;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lhqq;->aRA()Ljbr;

    move-result-object v0

    invoke-virtual {v0}, Ljbr;->bfm()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p1}, Lhqq;->aRA()Ljbr;

    move-result-object v0

    invoke-virtual {v0}, Ljbr;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 770
    new-instance v0, Ljpd;

    invoke-direct {v0}, Ljpd;-><init>()V

    .line 771
    invoke-virtual {p1}, Lhqq;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljpd;->xV(Ljava/lang/String;)Ljpd;

    .line 773
    new-instance v1, Ljpa;

    invoke-direct {v1}, Ljpa;-><init>()V

    iput-object v1, v0, Ljpd;->exA:Ljpa;

    .line 774
    iget-object v1, v0, Ljpd;->exA:Ljpa;

    invoke-virtual {p1}, Lhqq;->aRA()Ljbr;

    move-result-object v2

    invoke-virtual {v2}, Ljbr;->bfm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljpa;->xR(Ljava/lang/String;)Ljpa;

    .line 775
    iget-object v1, v0, Ljpd;->exA:Ljpa;

    invoke-virtual {p1}, Lhqq;->aRA()Ljbr;

    move-result-object v2

    invoke-virtual {v2}, Ljbr;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljpa;->xS(Ljava/lang/String;)Ljpa;

    .line 776
    invoke-virtual {p1}, Lhqq;->aRA()Ljbr;

    move-result-object v1

    invoke-virtual {v1}, Ljbr;->bfo()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 778
    iget-object v1, v0, Ljpd;->exA:Ljpa;

    invoke-virtual {p1}, Lhqq;->aRA()Ljbr;

    move-result-object v2

    invoke-virtual {v2}, Ljbr;->bfo()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljpa;->xT(Ljava/lang/String;)Ljpa;

    .line 781
    :cond_4
    invoke-interface {p2, v0}, Lefk;->ar(Ljava/lang/Object;)V

    goto :goto_0

    .line 784
    :cond_5
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    invoke-virtual {v0, p1, p2}, Lhpn;->c(Lhqq;Lefk;)V

    goto :goto_0
.end method

.method public final c(Ljpd;)V
    .locals 3

    .prologue
    .line 791
    invoke-virtual {p1}, Ljpd;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 792
    iget-object v1, p1, Ljpd;->exz:Ljpb;

    if-eqz v1, :cond_1

    .line 794
    iget-object v0, p1, Ljpd;->exz:Ljpb;

    invoke-virtual {v0}, Ljpb;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 795
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0588

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 803
    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 804
    const/4 v1, 0x0

    .line 807
    :cond_0
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnW:Lelf;

    invoke-virtual {v2, v0, v1}, Lelf;->aB(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    return-void

    .line 796
    :cond_1
    iget-object v1, p1, Ljpd;->exA:Ljpa;

    if-eqz v1, :cond_2

    .line 797
    iget-object v0, p1, Ljpd;->exA:Ljpa;

    invoke-virtual {v0}, Ljpa;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 798
    iget-object v1, p1, Ljpd;->exA:Ljpa;

    invoke-virtual {v1}, Ljpa;->bfo()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 800
    :cond_2
    invoke-virtual {p1}, Ljpd;->getAddress()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final cZ(I)V
    .locals 2

    .prologue
    .line 896
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 897
    return-void
.end method

.method public final d(Ljava/lang/String;I)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x0

    .line 915
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnY:[Landroid/view/View;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 916
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 915
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 918
    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->apG:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 920
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnS:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 921
    const-string v0, ""

    .line 922
    if-eqz p2, :cond_1

    .line 923
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 925
    :cond_1
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnT:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 926
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnS:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 927
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnT:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 928
    return-void
.end method

.method public final dj(I)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 561
    .line 566
    const/4 v0, 0x4

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->apP:Lcom/google/android/search/shared/actions/RemindersConfigFlags;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->aii()Z

    move-result v0

    if-eqz v0, :cond_3

    move v5, v6

    .line 568
    :goto_0
    if-eq p1, v6, :cond_0

    if-eqz v5, :cond_4

    .line 570
    :cond_0
    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mTimeTriggerView:Landroid/widget/RadioButton;

    .line 572
    if-nez v5, :cond_7

    move v0, v1

    .line 576
    :goto_1
    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->apP:Lcom/google/android/search/shared/actions/RemindersConfigFlags;

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/RemindersConfigFlags;->aih()Z

    move-result v3

    if-eqz v3, :cond_6

    if-nez v5, :cond_6

    move v3, v1

    .line 579
    :goto_2
    iget-object v7, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnQ:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v7, v2}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setVisibility(I)V

    .line 580
    if-eqz v5, :cond_1

    .line 581
    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlt:Ldxw;

    invoke-virtual {v5}, Ldxw;->ala()Lelh;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->a(Lelh;)V

    .line 589
    :cond_1
    :goto_3
    if-eqz v4, :cond_2

    .line 590
    invoke-virtual {v4, v6}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 592
    :cond_2
    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v4, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setVisibility(I)V

    .line 593
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v1, v0}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setVisibility(I)V

    .line 594
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnR:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 595
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnQ:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v0, v2}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setVisibility(I)V

    .line 596
    return-void

    :cond_3
    move v5, v1

    .line 566
    goto :goto_0

    .line 583
    :cond_4
    const/4 v0, 0x2

    if-ne p1, v0, :cond_5

    .line 584
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mLocationTriggerView:Landroid/widget/RadioButton;

    move v3, v2

    move-object v4, v0

    move v0, v2

    move v8, v2

    move v2, v1

    move v1, v8

    .line 585
    goto :goto_3

    .line 587
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown trigger type"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move v3, v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_1
.end method

.method public final dk(I)V
    .locals 3

    .prologue
    .line 731
    const-string v0, "edit_place_location_tag"

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlU:Lhqk;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->a(Ljava/lang/String;ZLhqk;)Lhql;

    move-result-object v0

    .line 733
    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lhqj;->a(Lhql;Ljava/lang/String;I)Lhqj;

    move-result-object v0

    .line 735
    const-string v1, "locationpicker_tag"

    invoke-direct {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->a(Landroid/app/DialogFragment;Ljava/lang/String;)V

    .line 736
    return-void
.end method

.method public final dl(I)V
    .locals 2

    .prologue
    .line 600
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnQ:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->hY(I)V

    .line 603
    return-void

    .line 600
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final eA(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 887
    return-void
.end method

.method public final em(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnP:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 551
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 552
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnP:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 553
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 554
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnP:Landroid/widget/EditText;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 557
    :cond_0
    return-void
.end method

.method public final ez(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 722
    const-string v0, "custom_location_tag"

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlT:Lhqk;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->a(Ljava/lang/String;ZLhqk;)Lhql;

    move-result-object v0

    .line 724
    const v1, 0x7f0a089c

    invoke-static {v0, p1, v1}, Lhqj;->a(Lhql;Ljava/lang/String;I)Lhqj;

    move-result-object v0

    .line 726
    const-string v1, "locationpicker_tag"

    invoke-direct {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->a(Landroid/app/DialogFragment;Ljava/lang/String;)V

    .line 727
    return-void
.end method

.method public final j(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 868
    if-eqz p1, :cond_0

    .line 869
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnW:Lelf;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0082

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lelf;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 873
    :cond_0
    if-eqz p2, :cond_1

    .line 874
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnW:Lelf;

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0172

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p2}, Lelf;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 877
    :cond_1
    return-void
.end method

.method public final n(III)V
    .locals 2

    .prologue
    .line 662
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->vi()V

    .line 664
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlq:Lnk;

    invoke-static {v0, p1, p2, p3}, Lnh;->a(Lnk;III)Lnh;

    move-result-object v0

    .line 666
    const-string v1, "datepicker_tag"

    invoke-direct {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->a(Landroid/app/DialogFragment;Ljava/lang/String;)V

    .line 667
    return-void
.end method

.method protected onFinishInflate()V
    .locals 11

    .prologue
    const v10, 0x7f0c013c

    const v9, 0x7f0c013a

    const v8, 0x7f04006f

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 440
    new-instance v0, Ldxw;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Ldxw;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlt:Ldxw;

    .line 442
    const v0, 0x7f110198

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnP:Landroid/widget/EditText;

    .line 443
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnP:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnH:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 444
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnP:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnI:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 445
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnP:Landroid/widget/EditText;

    new-instance v1, Lhpz;

    invoke-direct {v1, p0}, Lhpz;-><init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 452
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnP:Landroid/widget/EditText;

    const v1, 0x7f0c0137

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 454
    const v0, 0x7f11019c

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->apG:Landroid/view/View;

    .line 456
    const v0, 0x7f110199

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c013e

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 459
    const v0, 0x7f11019a

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mTimeTriggerView:Landroid/widget/RadioButton;

    .line 460
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mTimeTriggerView:Landroid/widget/RadioButton;

    const v1, 0x7f0c013f

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 462
    const v0, 0x7f11019b

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mLocationTriggerView:Landroid/widget/RadioButton;

    .line 463
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mLocationTriggerView:Landroid/widget/RadioButton;

    const v1, 0x7f0c0140

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 465
    const v0, 0x7f11019d

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c013d

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 468
    const v0, 0x7f11019e

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0139

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 471
    const v0, 0x7f11019f

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    .line 472
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-static {v0, v9}, Lehd;->r(Landroid/view/View;I)V

    .line 474
    const v0, 0x7f11007c

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    .line 475
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-static {v0, v10}, Lehd;->r(Landroid/view/View;I)V

    .line 477
    const v0, 0x7f1101a1

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnQ:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    .line 478
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnQ:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    const v1, 0x7f0c0138

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 480
    const v0, 0x7f1101a2

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnS:Landroid/widget/TextView;

    .line 481
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnS:Landroid/widget/TextView;

    const v1, 0x7f0c0074

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 483
    const v0, 0x7f1101a3

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnT:Landroid/widget/TextView;

    .line 485
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 488
    new-instance v1, Lelg;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlt:Ldxw;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const/4 v3, 0x0

    invoke-virtual {v2, v4, v5, v3}, Ldxw;->a(JLcom/google/android/search/shared/actions/RemindersConfigFlags;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ldxw;->z(Ljava/util/List;)[Lelh;

    move-result-object v2

    invoke-direct {v1, v0, v8, v2}, Lelg;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnU:Lelg;

    .line 495
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnU:Lelg;

    invoke-virtual {v1, v2}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 496
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnL:Lekh;

    invoke-virtual {v1, v2}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->a(Lekh;)V

    .line 497
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnJ:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 498
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-static {v1, v9}, Lehd;->r(Landroid/view/View;I)V

    .line 502
    new-instance v1, Lelg;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlt:Ldxw;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ldxw;->aA(J)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Ldxw;->y(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ldxw;->z(Ljava/util/List;)[Lelh;

    move-result-object v2

    invoke-direct {v1, v0, v8, v2}, Lelg;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnV:Lelg;

    .line 510
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnV:Lelg;

    invoke-virtual {v1, v2}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 511
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnM:Lekh;

    invoke-virtual {v1, v2}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->a(Lekh;)V

    .line 512
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnJ:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 513
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-static {v1, v10}, Lehd;->r(Landroid/view/View;I)V

    .line 515
    new-instance v1, Lelf;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f004c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lelf;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnW:Lelf;

    .line 518
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnQ:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnW:Lelf;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 519
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnQ:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlS:Lekh;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->a(Lekh;)V

    .line 520
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnQ:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnJ:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 522
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mTimeTriggerView:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnN:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 523
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mTimeTriggerView:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnK:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 524
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mLocationTriggerView:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnN:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 525
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mLocationTriggerView:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnK:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 527
    const v0, 0x7f1101a0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnR:Landroid/widget/TextView;

    .line 528
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnR:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnO:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 529
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnR:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 530
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnR:Landroid/widget/TextView;

    const v1, 0x7f0c013b

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 531
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnR:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_1

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawablesRelative()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    aget-object v0, v0, v7

    if-eqz v0, :cond_0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 533
    :cond_0
    const/4 v0, 0x7

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnP:Landroid/widget/EditText;

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mTimeTriggerView:Landroid/widget/RadioButton;

    aput-object v1, v0, v6

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mLocationTriggerView:Landroid/widget/RadioButton;

    aput-object v1, v0, v7

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnQ:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnR:Landroid/widget/TextView;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnY:[Landroid/view/View;

    .line 536
    invoke-static {}, Legs;->aoH()Legs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mActionCardEventLogger:Legs;

    .line 538
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 539
    return-void

    .line 531
    :cond_1
    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public final onUserInteraction()V
    .locals 1

    .prologue
    .line 938
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnX:Lhqi;

    if-eqz v0, :cond_0

    .line 939
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnX:Lhqi;

    invoke-interface {v0}, Lhqi;->onUserInteraction()V

    .line 941
    :cond_0
    return-void
.end method

.method public final ve()V
    .locals 4

    .prologue
    .line 696
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 697
    const-string v0, "bundle_event_start_time"

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    iget-object v2, v2, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/SetReminderAction;->aio()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 699
    const-string v0, "bundle_event_time_zone"

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    iget-object v0, v0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->ais()Laiw;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    iget-object v0, v0, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/SetReminderAction;->ais()Laiw;

    move-result-object v0

    invoke-virtual {v0}, Laiw;->toString()Ljava/lang/String;

    move-result-object v0

    .line 702
    :goto_0
    const-string v2, "bundle_event_rrule"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    new-instance v0, Lcom/android/recurrencepicker/RecurrencePickerDialog;

    invoke-direct {v0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;-><init>()V

    .line 705
    invoke-virtual {v0, v1}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->setArguments(Landroid/os/Bundle;)V

    .line 706
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dmR:Lajr;

    invoke-virtual {v0, v1}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->a(Lajr;)V

    .line 707
    const-string v1, "recurrencepicker_tag"

    invoke-direct {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->a(Landroid/app/DialogFragment;Ljava/lang/String;)V

    .line 708
    return-void

    .line 700
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final vf()V
    .locals 1

    .prologue
    .line 635
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlt:Ldxw;

    invoke-virtual {v0}, Ldxw;->akY()Lelh;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->a(Lelh;)V

    .line 636
    return-void
.end method

.method public final vg()V
    .locals 1

    .prologue
    .line 640
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlt:Ldxw;

    invoke-virtual {v0}, Ldxw;->akZ()Lelh;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->a(Lelh;)V

    .line 641
    return-void
.end method

.method public final vh()V
    .locals 2

    .prologue
    .line 812
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnQ:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnW:Lelf;

    invoke-virtual {v1}, Lelf;->aul()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->hY(I)V

    .line 814
    return-void
.end method

.method public final vi()V
    .locals 1

    .prologue
    .line 657
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlt:Ldxw;

    invoke-virtual {v0}, Ldxw;->alb()Lelh;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->a(Lelh;)V

    .line 658
    return-void
.end method

.method public final vj()V
    .locals 2

    .prologue
    .line 671
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dlt:Ldxw;

    invoke-virtual {v0}, Ldxw;->akX()Lelh;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnV:Lelg;

    invoke-virtual {v1, v0}, Lelg;->getPosition(Ljava/lang/Object;)I

    move-result v1

    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->ctP:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->hY(I)V

    .line 672
    return-void

    .line 671
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final vk()V
    .locals 1

    .prologue
    .line 881
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnQ:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->atZ()V

    .line 882
    return-void
.end method

.method public final vl()V
    .locals 0

    .prologue
    .line 892
    return-void
.end method

.method public final vm()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 901
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnY:[Landroid/view/View;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 902
    invoke-virtual {v4, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 901
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 904
    :cond_0
    return-void
.end method

.method public final vn()V
    .locals 5

    .prologue
    .line 908
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnY:[Landroid/view/View;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 909
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 908
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 911
    :cond_0
    return-void
.end method
