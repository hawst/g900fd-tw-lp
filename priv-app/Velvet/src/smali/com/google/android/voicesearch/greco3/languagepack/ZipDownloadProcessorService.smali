.class public Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;
.super Landroid/app/IntentService;
.source "PG"


# static fields
.field private static dpQ:I


# instance fields
.field private dpR:Lhrk;

.field private mDataManager:Lgix;

.field private mSettings:Lhym;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const v0, 0x522f3

    sput v0, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->dpQ:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/io/File;)I
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 218
    .line 224
    :try_start_0
    new-instance v6, Ljava/util/zip/ZipFile;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-direct {v6, v0, v3}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 225
    :try_start_1
    invoke-virtual {v6}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v8

    move v3, v2

    .line 226
    :goto_0
    invoke-interface {v8}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 227
    invoke-interface {v8}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/zip/ZipEntry;

    .line 228
    new-instance v5, Ljava/io/File;

    invoke-virtual {v0}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 229
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    .line 231
    const-string v7, "metadata"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    move v7, v1

    .line 238
    :goto_1
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, p1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 246
    :try_start_2
    invoke-virtual {v6, v0}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v5

    .line 247
    :try_start_3
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 248
    :try_start_4
    invoke-static {v5, v3}, Liso;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 254
    :try_start_5
    invoke-static {v5}, Lisq;->b(Ljava/io/Closeable;)V

    .line 255
    invoke-static {v3}, Lisq;->b(Ljava/io/Closeable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move v3, v7

    .line 256
    goto :goto_0

    .line 250
    :catch_0
    move-exception v0

    move-object v1, v4

    .line 251
    :goto_2
    :try_start_6
    const-string v3, "VS.DownloadProcessorService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "Error processing download: "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    .line 252
    :try_start_7
    invoke-static {v4}, Lisq;->b(Ljava/io/Closeable;)V

    .line 255
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 260
    invoke-virtual {v6}, Ljava/util/zip/ZipFile;->close()V

    move v0, v2

    :goto_3
    return v0

    .line 254
    :catchall_0
    move-exception v0

    move-object v1, v4

    :goto_4
    :try_start_8
    invoke-static {v4}, Lisq;->b(Ljava/io/Closeable;)V

    .line 255
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 260
    :catchall_1
    move-exception v0

    move-object v4, v6

    :goto_5
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/util/zip/ZipFile;->close()V

    :cond_0
    throw v0

    .line 257
    :cond_1
    if-eqz v3, :cond_2

    const/4 v0, 0x3

    .line 260
    :goto_6
    invoke-virtual {v6}, Ljava/util/zip/ZipFile;->close()V

    goto :goto_3

    :cond_2
    move v0, v1

    .line 257
    goto :goto_6

    .line 260
    :catchall_2
    move-exception v0

    goto :goto_5

    .line 254
    :catchall_3
    move-exception v0

    move-object v1, v4

    move-object v4, v5

    goto :goto_4

    :catchall_4
    move-exception v0

    move-object v1, v3

    move-object v4, v5

    goto :goto_4

    :catchall_5
    move-exception v0

    goto :goto_4

    .line 250
    :catch_1
    move-exception v0

    move-object v1, v4

    move-object v4, v5

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v1, v3

    move-object v4, v5

    goto :goto_2

    :cond_3
    move v7, v3

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljzp;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 181
    invoke-virtual {p2}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v2

    const-string v3, "g3_models"

    invoke-virtual {p0, v3, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 185
    invoke-static {v4}, Lhrb;->t(Ljava/io/File;)Z

    .line 190
    :try_start_0
    invoke-static {p1, v4}, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->a(Ljava/lang/String;Ljava/io/File;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 194
    if-ne v3, v0, :cond_1

    .line 196
    :try_start_1
    new-instance v2, Ljava/io/File;

    const-string v5, "metadata"

    invoke-direct {v2, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v4, "VS.DownloadProcessorService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Writing to: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p2}, Ljsr;->m(Ljsr;)[B

    move-result-object v4

    invoke-static {v4, v2}, Lisr;->a([BLjava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 206
    :cond_0
    :goto_0
    return v0

    .line 200
    :catch_0
    move-exception v2

    move v3, v1

    .line 203
    :goto_1
    const-string v4, "VS.DownloadProcessorService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error processing download: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    :cond_1
    if-nez v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 200
    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method private ai(Landroid/content/Intent;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 136
    .line 138
    const-string v0, "extra_download_id"

    const-wide/16 v4, -0x1

    invoke-virtual {p1, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 139
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-gez v0, :cond_6

    .line 140
    const-string v0, "VS.DownloadProcessorService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Received intent without download ID :"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 144
    :goto_0
    iget-object v3, p0, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->dpR:Lhrk;

    invoke-virtual {v3, v4, v5}, Lhrk;->bJ(J)Lhrq;

    move-result-object v4

    .line 145
    if-nez v4, :cond_5

    move v3, v1

    .line 150
    :goto_1
    if-eqz v3, :cond_1

    .line 153
    :try_start_0
    iget v0, v4, Lhrq;->status:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, v4, Lhrq;->dpx:Ljava/lang/String;

    iget-object v5, v4, Lhrq;->cMz:Ljzp;

    invoke-direct {p0, v0, v5}, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->a(Ljava/lang/String;Ljzp;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 157
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->mDataManager:Lgix;

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Lgix;->fK(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 165
    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->dpR:Lhrk;

    iget-object v5, v4, Lhrq;->cMz:Ljzp;

    invoke-virtual {v5}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lhrk;->mU(Ljava/lang/String;)V

    .line 167
    iget-object v0, v4, Lhrq;->dpx:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, v4, Lhrq;->dpx:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->om(Ljava/lang/String;)V

    .line 174
    :cond_1
    if-eqz v4, :cond_2

    if-nez v3, :cond_2

    .line 175
    iget-object v4, v4, Lhrq;->cMz:Ljzp;

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    new-instance v5, Landroid/app/Notification$Builder;

    invoke-direct {v5, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    invoke-virtual {v5, v2}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    invoke-virtual {v5, v2}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    iget-object v6, p0, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->mSettings:Lhym;

    invoke-virtual {v6}, Lhym;->aER()Ljze;

    move-result-object v6

    invoke-virtual {v4}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Lgnq;->b(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v6, Landroid/content/Intent;

    const-class v7, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;

    invoke-direct {v6, p0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v1, v6, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    const v6, 0x1080078

    invoke-virtual {v5, v6}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    const v6, 0x7f0a08ea

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v4, v2, v1

    invoke-virtual {p0, v6, v2}, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    const v1, 0x7f0a08eb

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {v5}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    sget v2, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->dpQ:I

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 177
    :cond_2
    return v3

    .line 159
    :cond_3
    :try_start_1
    const-string v0, "VS.DownloadProcessorService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "processDownloadedFile() failed for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v4, Lhrq;->dpx:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 164
    :catchall_0
    move-exception v0

    .line 165
    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->dpR:Lhrk;

    iget-object v2, v4, Lhrq;->cMz:Ljzp;

    invoke-virtual {v2}, Ljzp;->bxm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhrk;->mU(Ljava/lang/String;)V

    .line 167
    iget-object v1, v4, Lhrq;->dpx:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 169
    iget-object v1, v4, Lhrq;->dpx:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->om(Ljava/lang/String;)V

    :cond_4
    throw v0

    :cond_5
    move v3, v0

    goto/16 :goto_1

    :cond_6
    move v0, v2

    goto/16 :goto_0
.end method

.method public static k(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 77
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 78
    const/4 v1, 0x2

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->fillIn(Landroid/content/Intent;I)I

    .line 80
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 81
    return-void
.end method

.method private static om(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 265
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 266
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 267
    const-string v0, "VS.DownloadProcessorService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to delete downloaded file:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 2

    .prologue
    .line 102
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 103
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Lhhq;->aOX()Lhrk;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->dpR:Lhrk;

    .line 106
    invoke-virtual {v0}, Lhhq;->aOV()Lgiw;

    move-result-object v1

    invoke-virtual {v1}, Lgiw;->aGh()Lgix;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->mDataManager:Lgix;

    .line 107
    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->mSettings:Lhym;

    .line 108
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 112
    const-string v0, "language_pack_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 113
    const-string v0, "language_pack_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "location_abs"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->mSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aER()Ljze;

    move-result-object v2

    iget-object v2, v2, Ljze;->eNd:[Ljzp;

    invoke-static {v0, v2}, Lgkb;->a(Ljava/lang/String;[Ljzp;)Ljzp;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->a(Ljava/lang/String;Ljzp;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->mDataManager:Lgix;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lgix;->fK(Z)V

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->dpR:Lhrk;

    invoke-virtual {v1, v0}, Lhrk;->mU(Ljava/lang/String;)V

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->ai(Landroid/content/Intent;)Z

    goto :goto_0
.end method
