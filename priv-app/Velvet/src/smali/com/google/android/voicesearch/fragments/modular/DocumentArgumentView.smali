.class public Lcom/google/android/voicesearch/fragments/modular/DocumentArgumentView;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private dlH:Lcom/google/android/search/shared/ui/WebImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/modular/DocumentArgumentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/fragments/modular/DocumentArgumentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;Lesm;)V
    .locals 2

    .prologue
    .line 38
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/DocumentArgumentView;->dlH:Lcom/google/android/search/shared/ui/WebImageView;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/DocumentArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/search/shared/ui/WebImageView;->a(Landroid/net/Uri;Lesm;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DocumentArgumentView;->dlH:Lcom/google/android/search/shared/ui/WebImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/WebImageView;->setVisibility(I)V

    .line 39
    :goto_0
    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DocumentArgumentView;->dlH:Lcom/google/android/search/shared/ui/WebImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/ui/WebImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 33
    const v0, 0x7f11014e

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/DocumentArgumentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DocumentArgumentView;->dlH:Lcom/google/android/search/shared/ui/WebImageView;

    .line 34
    return-void
.end method
