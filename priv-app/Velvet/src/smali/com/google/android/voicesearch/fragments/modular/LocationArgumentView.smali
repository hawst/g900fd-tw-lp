.class public Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;
.super Lhou;
.source "PG"


# instance fields
.field private final dlR:Lell;

.field private final dlS:Lekh;

.field private final dlT:Lhqk;

.field private dlU:Lhqk;

.field private dlV:Lelf;

.field private dlW:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

.field private dlX:Landroid/widget/LinearLayout;

.field private dlY:Landroid/widget/TextView;

.field private dlZ:Landroid/widget/TextView;

.field private dma:Lhnt;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 145
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 141
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 136
    invoke-direct {p0, p1, p2, p3}, Lhou;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    new-instance v0, Lell;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f004c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    aget-object v1, v1, v2

    const/4 v2, 0x1

    invoke-direct {v0, v1, v3, v3, v2}, Lell;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlR:Lell;

    .line 58
    new-instance v0, Lhno;

    invoke-direct {v0, p0}, Lhno;-><init>(Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlS:Lekh;

    .line 74
    new-instance v0, Lhnp;

    invoke-direct {v0, p0}, Lhnp;-><init>(Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlT:Lhqk;

    .line 96
    new-instance v0, Lhnr;

    invoke-direct {v0, p0}, Lhnr;-><init>(Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlU:Lhqk;

    .line 137
    return-void
.end method

.method private a(Ljava/lang/String;ZLhqk;)Lhql;
    .locals 2

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lhql;

    .line 320
    if-nez v0, :cond_0

    .line 321
    new-instance v0, Lhql;

    invoke-direct {v0}, Lhql;-><init>()V

    .line 322
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhql;->bL(Landroid/content/Context;)V

    .line 323
    invoke-virtual {v0}, Lhql;->aRv()Lhqm;

    move-result-object v1

    iput-boolean p2, v1, Lhqm;->doj:Z

    .line 324
    invoke-virtual {v0, p3}, Lhql;->a(Lhqk;)V

    .line 325
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 327
    :cond_0
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;)V
    .locals 0

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->ahi()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;)Lelf;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlV:Lelf;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;)Lcom/google/android/shared/ui/SpinnerAlwaysCallback;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlW:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;)Lhnt;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dma:Lhnt;

    return-object v0
.end method

.method private i(Ljpd;)Landroid/util/Pair;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const v4, 0x7f0f004c

    .line 230
    invoke-virtual {p1}, Ljpd;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 231
    invoke-virtual {p1}, Ljpd;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 232
    iget-object v3, p1, Ljpd;->esI:Ljne;

    if-eqz v3, :cond_0

    .line 233
    iget-object v3, p1, Ljpd;->esI:Ljne;

    invoke-virtual {v3}, Ljne;->bqz()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 244
    :cond_0
    :goto_0
    iget-object v3, p1, Ljpd;->exz:Ljpb;

    if-eqz v3, :cond_3

    .line 245
    iget-object v0, p1, Ljpd;->exz:Ljpb;

    invoke-virtual {v0}, Ljpb;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 246
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f0a0588

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 251
    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v0, v2

    .line 254
    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    move-object v1, v0

    move-object v0, v2

    .line 259
    :cond_2
    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    .line 235
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    aget-object v0, v0, v3

    goto :goto_0

    .line 239
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    aget-object v0, v0, v3

    goto :goto_0

    .line 247
    :cond_3
    iget-object v3, p1, Ljpd;->exA:Ljpa;

    if-eqz v3, :cond_4

    .line 248
    iget-object v0, p1, Ljpd;->exA:Ljpa;

    invoke-virtual {v0}, Ljpa;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 249
    iget-object v0, p1, Ljpd;->exA:Ljpa;

    invoke-virtual {v0}, Ljpa;->bfo()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_1

    .line 233
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static lo(I)Lizj;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 285
    new-instance v0, Lizj;

    invoke-direct {v0}, Lizj;-><init>()V

    invoke-virtual {v0, v1}, Lizj;->nV(I)Lizj;

    move-result-object v0

    .line 287
    new-array v1, v1, [Liwk;

    const/4 v2, 0x0

    new-instance v3, Liwk;

    invoke-direct {v3}, Liwk;-><init>()V

    invoke-virtual {v3, p0}, Liwk;->nb(I)Liwk;

    move-result-object v3

    aput-object v3, v1, v2

    iput-object v1, v0, Lizj;->dUo:[Liwk;

    .line 288
    return-object v0
.end method


# virtual methods
.method public final a(Lhnt;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dma:Lhnt;

    .line 202
    return-void
.end method

.method public final aQX()V
    .locals 4

    .prologue
    .line 264
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->aiz()Ljpd;

    move-result-object v0

    iget-object v0, v0, Ljpd;->esI:Ljne;

    invoke-virtual {v0}, Ljne;->bqz()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 279
    :goto_0
    return-void

    .line 266
    :pswitch_0
    const v0, 0x7f0a089d

    .line 274
    :goto_1
    const-string v1, "edit_place_location_tag"

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlU:Lhqk;

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->a(Ljava/lang/String;ZLhqk;)Lhql;

    move-result-object v1

    .line 276
    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lhqj;->a(Lhql;Ljava/lang/String;I)Lhqj;

    move-result-object v0

    .line 278
    const-string v1, "locationpicker_tag"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->a(Landroid/app/DialogFragment;Ljava/lang/String;)V

    goto :goto_0

    .line 269
    :pswitch_1
    const v0, 0x7f0a089f

    .line 270
    goto :goto_1

    .line 264
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final aQY()V
    .locals 4

    .prologue
    .line 299
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->aiz()Ljpd;

    move-result-object v1

    .line 300
    const/4 v0, 0x0

    .line 301
    if-eqz v1, :cond_0

    .line 302
    iget-object v2, v1, Ljpd;->exz:Ljpb;

    if-eqz v2, :cond_1

    iget-object v2, v1, Ljpd;->exz:Ljpb;

    invoke-virtual {v2}, Ljpb;->baV()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 303
    iget-object v0, v1, Ljpd;->exz:Ljpb;

    invoke-virtual {v0}, Ljpb;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 309
    :cond_0
    :goto_0
    const-string v1, "custom_location_tag"

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlT:Lhqk;

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->a(Ljava/lang/String;ZLhqk;)Lhql;

    move-result-object v1

    .line 311
    const v2, 0x7f0a089c

    invoke-static {v1, v0, v2}, Lhqj;->a(Lhql;Ljava/lang/String;I)Lhqj;

    move-result-object v0

    .line 313
    const-string v1, "locationpicker_tag"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->a(Landroid/app/DialogFragment;Ljava/lang/String;)V

    .line 314
    return-void

    .line 304
    :cond_1
    invoke-virtual {v1}, Ljpd;->pb()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 305
    invoke-virtual {v1}, Ljpd;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final ahm()[Landroid/view/View;
    .locals 3

    .prologue
    .line 212
    const/4 v0, 0x1

    new-array v1, v0, [Landroid/view/View;

    const/4 v2, 0x0

    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->aks()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlW:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    :goto_0
    aput-object v0, v1, v2

    return-object v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlX:Landroid/widget/LinearLayout;

    goto :goto_0
.end method

.method public final bridge synthetic b(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V
    .locals 0

    .prologue
    .line 48
    check-cast p1, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->b(Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;)V

    return-void
.end method

.method public final b(Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 164
    invoke-super {p0, p1}, Lhou;->b(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V

    .line 165
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->aks()Z

    move-result v0

    if-nez v0, :cond_2

    .line 167
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->aiz()Ljpd;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->i(Ljpd;)Landroid/util/Pair;

    move-result-object v1

    .line 169
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlY:Landroid/widget/TextView;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlZ:Landroid/widget/TextView;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    :cond_0
    :goto_1
    return-void

    .line 170
    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 176
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 178
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->akt()Ljpe;

    move-result-object v0

    .line 179
    if-eqz v0, :cond_3

    .line 180
    iget-object v5, v0, Ljpe;->exC:[Ljpd;

    array-length v6, v5

    move v2, v3

    :goto_2
    if-ge v2, v6, :cond_3

    aget-object v7, v5, v2

    .line 181
    invoke-direct {p0, v7}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->i(Ljpd;)Landroid/util/Pair;

    move-result-object v1

    new-instance v8, Lell;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-direct {v8, v0, v1, v7, v3}, Lell;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)V

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 185
    :cond_3
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlR:Lell;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    new-instance v0, Lelf;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Lelf;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlV:Lelf;

    .line 189
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlW:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlV:Lelf;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 191
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->aiz()Ljpd;

    move-result-object v0

    .line 192
    if-eqz v0, :cond_0

    .line 193
    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->i(Ljpd;)Landroid/util/Pair;

    move-result-object v1

    .line 194
    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlV:Lelf;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lelf;->aB(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlW:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlV:Lelf;

    invoke-virtual {v1}, Lelf;->aul()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->hY(I)V

    goto :goto_1
.end method

.method public final b(Lhqq;Lefk;)V
    .locals 8

    .prologue
    .line 332
    invoke-virtual {p1}, Lhqq;->aRz()Lixu;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 334
    new-instance v0, Ljpd;

    invoke-direct {v0}, Ljpd;-><init>()V

    .line 335
    invoke-virtual {p1}, Lhqq;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljpd;->xV(Ljava/lang/String;)Ljpd;

    .line 336
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljpd;->iQ(Z)Ljpd;

    .line 337
    invoke-virtual {p1}, Lhqq;->aRz()Lixu;

    move-result-object v1

    .line 338
    iget-object v2, v1, Lixu;->dOG:Lixv;

    .line 339
    new-instance v3, Ljpb;

    invoke-direct {v3}, Ljpb;-><init>()V

    .line 340
    new-instance v4, Ljpc;

    invoke-direct {v4}, Ljpc;-><init>()V

    .line 341
    iput-object v4, v3, Ljpb;->ewY:Ljpc;

    .line 342
    iget-object v5, v2, Lixv;->dMA:Ljbc;

    if-eqz v5, :cond_0

    .line 343
    iget-object v2, v2, Lixv;->dMA:Ljbc;

    .line 345
    new-instance v5, Ljpf;

    invoke-direct {v5}, Ljpf;-><init>()V

    .line 346
    invoke-virtual {v2}, Ljbc;->beI()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v2}, Ljbc;->beK()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 347
    invoke-virtual {v2}, Ljbc;->beH()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljpf;->dE(J)Ljpf;

    .line 348
    invoke-virtual {v2}, Ljbc;->beJ()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljpf;->dF(J)Ljpf;

    .line 349
    iput-object v5, v4, Ljpc;->exb:Ljpf;

    .line 352
    :cond_0
    invoke-virtual {v1}, Lixu;->baV()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 353
    invoke-virtual {v1}, Lixu;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljpb;->xU(Ljava/lang/String;)Ljpb;

    .line 355
    :cond_1
    iput-object v3, v0, Ljpd;->exz:Ljpb;

    .line 356
    invoke-interface {p2, v0}, Lefk;->ar(Ljava/lang/Object;)V

    .line 375
    :goto_0
    return-void

    .line 357
    :cond_2
    invoke-virtual {p1}, Lhqq;->aRA()Ljbr;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lhqq;->aRA()Ljbr;

    move-result-object v0

    invoke-virtual {v0}, Ljbr;->bfm()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lhqq;->aRA()Ljbr;

    move-result-object v0

    invoke-virtual {v0}, Ljbr;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 360
    new-instance v0, Ljpd;

    invoke-direct {v0}, Ljpd;-><init>()V

    .line 361
    invoke-virtual {p1}, Lhqq;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljpd;->xV(Ljava/lang/String;)Ljpd;

    .line 363
    new-instance v1, Ljpa;

    invoke-direct {v1}, Ljpa;-><init>()V

    iput-object v1, v0, Ljpd;->exA:Ljpa;

    .line 364
    iget-object v1, v0, Ljpd;->exA:Ljpa;

    invoke-virtual {p1}, Lhqq;->aRA()Ljbr;

    move-result-object v2

    invoke-virtual {v2}, Ljbr;->bfm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljpa;->xR(Ljava/lang/String;)Ljpa;

    .line 365
    iget-object v1, v0, Ljpd;->exA:Ljpa;

    invoke-virtual {p1}, Lhqq;->aRA()Ljbr;

    move-result-object v2

    invoke-virtual {v2}, Ljbr;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljpa;->xS(Ljava/lang/String;)Ljpa;

    .line 366
    invoke-virtual {p1}, Lhqq;->aRA()Ljbr;

    move-result-object v1

    invoke-virtual {v1}, Ljbr;->bfo()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 368
    iget-object v1, v0, Ljpd;->exA:Ljpa;

    invoke-virtual {p1}, Lhqq;->aRA()Ljbr;

    move-result-object v2

    invoke-virtual {v2}, Ljbr;->bfo()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljpa;->xT(Ljava/lang/String;)Ljpa;

    .line 371
    :cond_3
    invoke-interface {p2, v0}, Lefk;->ar(Ljava/lang/Object;)V

    goto :goto_0

    .line 373
    :cond_4
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dma:Lhnt;

    invoke-interface {v0, p1, p2}, Lhnt;->a(Lhqq;Lefk;)V

    goto :goto_0
.end method

.method public final cZ(I)V
    .locals 2

    .prologue
    .line 378
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 379
    return-void
.end method

.method protected final onClick()V
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->aks()Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->aQY()V

    .line 296
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    .line 149
    invoke-super {p0}, Lhou;->onFinishInflate()V

    .line 150
    const v0, 0x7f110253

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlX:Landroid/widget/LinearLayout;

    .line 151
    const v0, 0x1020014

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlY:Landroid/widget/TextView;

    .line 152
    const v0, 0x1020015

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlZ:Landroid/widget/TextView;

    .line 153
    const v0, 0x7f110405

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlW:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    .line 155
    new-instance v0, Lelf;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v1, v2}, Lelf;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlV:Lelf;

    .line 157
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlW:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlV:Lelf;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlW:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlS:Lekh;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->a(Lekh;)V

    .line 160
    return-void
.end method

.method public final setEditable(Z)V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlW:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v0, p1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setEnabled(Z)V

    .line 207
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/LocationArgumentView;->dlX:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 208
    return-void
.end method
