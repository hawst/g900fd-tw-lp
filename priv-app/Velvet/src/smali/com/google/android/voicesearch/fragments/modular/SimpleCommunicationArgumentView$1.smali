.class Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView$1;
.super Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;
.source "PG"


# instance fields
.field private synthetic bOh:Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

.field private synthetic dmV:Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView;


# direct methods
.method constructor <init>(Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView;Ljqg;Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView$1;->dmV:Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView;

    iput-object p3, p0, Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView$1;->bOh:Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    invoke-direct {p0, p2}, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;-><init>(Ljqg;)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic setValue(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView$1;->setValue(Ljava/lang/String;)V

    return-void
.end method

.method public final setValue(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    invoke-super {p0, p1}, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;->setValue(Ljava/lang/String;)V

    .line 40
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView$1;->bOh:Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView$1;->dmV:Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView$1;->bOh:Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    iget-object v0, v0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->akB()Ldzb;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView$1;->bOh:Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    iget-object v0, v0, Lcom/google/android/search/shared/actions/modular/arguments/SingleValueArgument;->wi:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Person;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, p1, v3, v0}, Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView;->a(Lcom/google/android/voicesearch/fragments/modular/SimpleCommunicationArgumentView;Ljava/lang/String;Ldzb;Ljava/lang/String;)Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->b(Lcom/google/android/search/shared/actions/utils/Disambiguation;)V

    .line 43
    :cond_0
    return-void
.end method
