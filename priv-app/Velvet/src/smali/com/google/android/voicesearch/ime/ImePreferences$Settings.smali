.class public Lcom/google/android/voicesearch/ime/ImePreferences$Settings;
.super Lcom/android/inputmethodcommon/InputMethodSettingsFragment;
.source "PG"


# instance fields
.field private bmF:Lcxt;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 66
    iget-object v0, p0, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;->xb:Lrn;

    invoke-virtual {v0}, Lrn;->dU()V

    .line 67
    const v0, 0x7f0a0872

    iget-object v1, p0, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;->xb:Lrn;

    iput v0, v1, Lrn;->xd:I

    invoke-virtual {v1}, Lrn;->dU()V

    .line 69
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v1

    .line 71
    new-instance v2, Lcyw;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    invoke-virtual {v1}, Lhhq;->aOV()Lgiw;

    move-result-object v1

    invoke-virtual {v1}, Lgiw;->aGj()Ligi;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lcyw;-><init>(Lcke;Ligi;)V

    iput-object v2, p0, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->bmF:Lcxt;

    .line 77
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-static {v0}, Lchr;->a(Landroid/preference/PreferenceManager;)V

    .line 80
    const v0, 0x7f070017

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->addPreferencesFromResource(I)V

    .line 82
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->bmF:Lcxt;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-interface {v0, v1}, Lcxt;->a(Landroid/preference/Preference;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->bmF:Lcxt;

    invoke-interface {v0, p1}, Lcxt;->i(Landroid/os/Bundle;)V

    .line 84
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->bmF:Lcxt;

    invoke-interface {v0}, Lcxt;->onDestroy()V

    .line 113
    invoke-super {p0}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;->onDestroy()V

    .line 114
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->bmF:Lcxt;

    invoke-interface {v0}, Lcxt;->onPause()V

    .line 101
    invoke-super {p0}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;->onPause()V

    .line 102
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 94
    invoke-super {p0}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;->onResume()V

    .line 95
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->bmF:Lcxt;

    invoke-interface {v0}, Lcxt;->onResume()V

    .line 96
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 118
    invoke-super {p0, p1}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 119
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->bmF:Lcxt;

    invoke-interface {v0, p1}, Lcxt;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 120
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;->onStart()V

    .line 89
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->bmF:Lcxt;

    invoke-interface {v0}, Lcxt;->onStart()V

    .line 90
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/voicesearch/ime/ImePreferences$Settings;->bmF:Lcxt;

    invoke-interface {v0}, Lcxt;->onStop()V

    .line 107
    invoke-super {p0}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;->onStop()V

    .line 108
    return-void
.end method
