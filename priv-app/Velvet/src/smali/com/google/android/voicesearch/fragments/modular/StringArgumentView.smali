.class public Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;
.super Lduc;
.source "PG"


# instance fields
.field private dmY:Landroid/widget/EditText;

.field private dmZ:Lhoz;

.field private dna:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lduc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    new-instance v0, Lhow;

    invoke-direct {v0, p0}, Lhow;-><init>(Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    return-void
.end method

.method public static synthetic a(Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->tP()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;Z)Z
    .locals 0

    .prologue
    .line 21
    iput-boolean p1, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dna:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;)V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dmZ:Lhoz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dmZ:Lhoz;

    iget-boolean v1, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dna:Z

    invoke-interface {v0, v1}, Lhoz;->gB(Z)V

    :cond_0
    return-void
.end method

.method public static synthetic c(Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dmY:Landroid/widget/EditText;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->tP()V

    return-void
.end method


# virtual methods
.method public final a(Lhoz;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dmZ:Lhoz;

    .line 136
    return-void
.end method

.method protected final ahj()Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dna:Z

    return v0
.end method

.method public final ahm()[Landroid/view/View;
    .locals 3

    .prologue
    .line 122
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dmY:Landroid/widget/EditText;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic b(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->b(Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;)V

    return-void
.end method

.method public final b(Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;)V
    .locals 2

    .prologue
    .line 90
    invoke-super {p0, p1}, Lduc;->b(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V

    .line 91
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 94
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dmY:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 95
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dmY:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 98
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/StringArgument;->isMultiLine()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dmY:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getInputType()I

    move-result v0

    const/high16 v1, 0x20000

    or-int/2addr v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->setInputType(I)V

    .line 101
    return-void

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dmY:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getInputType()I

    move-result v0

    const v1, -0x20001

    and-int/2addr v0, v1

    goto :goto_0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dmY:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    add-int/lit8 p1, p1, 0x1

    .line 108
    :cond_0
    invoke-super {p0, p1}, Lduc;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 109
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dmY:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 110
    sget-object v1, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->FOCUSED_STATE_SET:[I

    invoke-static {v0, v1}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->mergeDrawableStates([I[I)[I

    .line 112
    :cond_1
    return-object v0
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0}, Lduc;->onFinishInflate()V

    .line 51
    const v0, 0x7f110414

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dmY:Landroid/widget/EditText;

    .line 52
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dmY:Landroid/widget/EditText;

    new-instance v1, Lhox;

    invoke-direct {v1, p0}, Lhox;-><init>(Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dmY:Landroid/widget/EditText;

    new-instance v1, Lhoy;

    invoke-direct {v1, p0}, Lhoy;-><init>(Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 86
    return-void
.end method

.method public final setEditable(Z)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dmY:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 132
    return-void
.end method

.method protected final setInputType(I)V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/StringArgumentView;->dmY:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setInputType(I)V

    .line 127
    return-void
.end method
