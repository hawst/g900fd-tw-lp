.class public Lcom/google/android/voicesearch/fragments/MessageSearchCardView;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private djs:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/MessageSearchCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public static bK(Landroid/content/Context;)Lcom/google/android/voicesearch/fragments/MessageSearchCardView;
    .locals 2

    .prologue
    .line 27
    const v0, 0x7f0400c9

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/MessageSearchCardView;

    return-object v0
.end method


# virtual methods
.method public final bN(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 39
    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/MessageSearchCardView;->addView(Landroid/view/View;)V

    .line 40
    return-void
.end method

.method public final oc(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/MessageSearchCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0400ca

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f110266

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageSearchCardView;->djs:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageSearchCardView;->djs:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/fragments/MessageSearchCardView;->addView(Landroid/view/View;)V

    .line 35
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/MessageSearchCardView;->djs:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 36
    return-void
.end method
