.class public Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/app/DownloadManager;J)Z
    .locals 2

    .prologue
    .line 44
    invoke-virtual {p1, p2, p3}, Landroid/app/DownloadManager;->getMimeTypeForDownloadedFile(J)Ljava/lang/String;

    move-result-object v0

    .line 45
    const-string v1, "application/zip"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    invoke-static {p0}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->bM(Landroid/content/Context;)V

    .line 47
    const/4 v0, 0x1

    .line 49
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 19
    const-string v0, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 20
    invoke-static {p1, p2}, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadProcessorService;->k(Landroid/content/Context;Landroid/content/Intent;)V

    .line 39
    :cond_0
    :goto_0
    return-void

    .line 21
    :cond_1
    const-string v0, "android.intent.action.DOWNLOAD_NOTIFICATION_CLICKED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22
    const-string v0, "download"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    .line 24
    const-string v1, "extra_click_download_ids"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v2

    .line 26
    if-eqz v2, :cond_2

    .line 27
    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-wide v4, v2, v1

    .line 28
    invoke-static {p1, v0, v4, v5}, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadReceiver;->a(Landroid/content/Context;Landroid/app/DownloadManager;J)Z

    move-result v4

    if-nez v4, :cond_0

    .line 29
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 33
    :cond_2
    const-string v1, "extra_download_id"

    invoke-virtual {p2, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 34
    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 35
    invoke-static {p1, v0, v2, v3}, Lcom/google/android/voicesearch/greco3/languagepack/ZipDownloadReceiver;->a(Landroid/content/Context;Landroid/app/DownloadManager;J)Z

    goto :goto_0
.end method
