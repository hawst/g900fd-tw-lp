.class public Lcom/google/android/voicesearch/ui/ActionView;
.super Lhzw;
.source "PG"


# static fields
.field private static final bLM:[I


# instance fields
.field private dmv:Landroid/widget/ProgressBar;

.field private dmy:Landroid/animation/ObjectAnimator;

.field private final dvO:Ljava/util/List;

.field private dvP:Landroid/view/ViewGroup;

.field private dvQ:Landroid/view/ViewGroup;

.field private dvR:Landroid/view/ViewGroup;

.field private dvS:Landroid/view/ViewGroup;

.field private dvT:Landroid/widget/TextView;

.field private dvU:Landroid/widget/ImageButton;

.field private dvV:Landroid/widget/ImageView;

.field private dvW:Landroid/widget/TextView;

.field private dvX:Landroid/widget/GridView;

.field private dvY:Landroid/view/ViewGroup;

.field private dvZ:Landroid/view/ViewGroup;

.field private dwa:Landroid/widget/TextView;

.field private dwb:Landroid/widget/ImageButton;

.field private dwc:Landroid/widget/ImageButton;

.field private dwd:Landroid/widget/TextView;

.field public dwe:Lhzx;

.field public dwf:Liai;

.field private dwg:Landroid/graphics/Typeface;

.field private dwh:Landroid/transition/Transition;

.field private dwi:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 45
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f0100c7

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/voicesearch/ui/ActionView;->bLM:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/ui/ActionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 86
    const v0, 0x7f090132

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/ui/ActionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 87
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Lhzw;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvO:Ljava/util/List;

    .line 81
    sget-object v0, Lbwe;->aLZ:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 83
    return-void
.end method

.method public static synthetic a(Lcom/google/android/voicesearch/ui/ActionView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwd:Landroid/widget/TextView;

    return-object v0
.end method

.method private static varargs a(Libb;I[Landroid/view/View;)Liay;
    .locals 4

    .prologue
    .line 230
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 231
    new-instance v0, Liay;

    invoke-direct {v0, p0, p1}, Liay;-><init>(Libb;I)V

    .line 232
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Liay;->setDuration(J)Landroid/transition/Transition;

    .line 233
    array-length v2, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p2, v1

    .line 234
    invoke-virtual {v0, v3}, Liay;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 233
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 237
    :cond_0
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Liay;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    .line 240
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static synthetic b(Lcom/google/android/voicesearch/ui/ActionView;)Landroid/animation/ObjectAnimator;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dmy:Landroid/animation/ObjectAnimator;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;Z)V
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwb:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 325
    return-void
.end method

.method public final a(Lhzx;)V
    .locals 0

    .prologue
    .line 395
    iput-object p1, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwe:Lhzx;

    .line 396
    return-void
.end method

.method public final aL(J)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 401
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dmv:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 402
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dmv:Landroid/widget/ProgressBar;

    const-class v1, Landroid/widget/ProgressBar;

    const-class v2, Ljava/lang/Integer;

    const-string v3, "progress"

    invoke-static {v1, v2, v3}, Landroid/util/Property;->of(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Landroid/util/Property;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [I

    aput v4, v2, v4

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/voicesearch/ui/ActionView;->dmv:Landroid/widget/ProgressBar;

    invoke-virtual {v4}, Landroid/widget/ProgressBar;->getMax()I

    move-result v4

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dmy:Landroid/animation/ObjectAnimator;

    .line 406
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dmy:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 407
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dmy:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 408
    return-void
.end method

.method public final aUC()V
    .locals 2

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwb:Landroid/widget/ImageButton;

    const v1, 0x7f02011d

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 312
    return-void
.end method

.method public final auj()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 384
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvQ:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 385
    invoke-static {}, Legs;->aoH()Legs;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvQ:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2}, Legs;->q(Landroid/view/View;I)V

    .line 391
    :goto_0
    return-void

    .line 388
    :cond_0
    invoke-static {}, Legs;->aoH()Legs;

    move-result-object v0

    invoke-virtual {v0, p0, v2}, Legs;->q(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public final auk()V
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dmv:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 414
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dmy:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dmy:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 416
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dmv:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 418
    :cond_0
    return-void
.end method

.method public final bR(Landroid/view/View;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 331
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 3

    .prologue
    .line 503
    invoke-super {p0}, Lhzw;->drawableStateChanged()V

    .line 505
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwg:Landroid/graphics/Typeface;

    if-nez v0, :cond_0

    .line 507
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvT:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwg:Landroid/graphics/Typeface;

    .line 510
    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvT:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwg:Landroid/graphics/Typeface;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 512
    return-void
.end method

.method public final fc(Z)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 293
    iget-object v2, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvQ:Landroid/view/ViewGroup;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 294
    if-nez p1, :cond_0

    .line 297
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwd:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 299
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 293
    goto :goto_0
.end method

.method public final gR(Z)V
    .locals 0

    .prologue
    .line 423
    return-void
.end method

.method public final gS(Z)V
    .locals 0

    .prologue
    .line 428
    return-void
.end method

.method public final gT(Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 244
    iget-boolean v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwi:Z

    if-nez v0, :cond_0

    .line 272
    :goto_0
    return-void

    .line 247
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwi:Z

    .line 249
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 251
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwh:Landroid/transition/Transition;

    invoke-static {p0, v0}, Landroid/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;Landroid/transition/Transition;)V

    .line 252
    iput-boolean v2, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwi:Z

    .line 253
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvZ:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 254
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvX:Landroid/widget/GridView;

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setVisibility(I)V

    .line 255
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvW:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 257
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvY:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 258
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvP:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 259
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvV:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 260
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvT:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 261
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvQ:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 271
    :goto_1
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvU:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_0

    .line 263
    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvY:Landroid/view/ViewGroup;

    invoke-static {v0}, Lelv;->aA(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    .line 268
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvZ:Landroid/view/ViewGroup;

    invoke-static {v0, v3}, Lelv;->p(Landroid/view/View;I)Landroid/view/ViewPropertyAnimator;

    goto :goto_1
.end method

.method public final gv(Z)V
    .locals 0

    .prologue
    .line 433
    return-void
.end method

.method public final k(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 517
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvV:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 518
    return-void
.end method

.method public final lF(I)V
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvP:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 288
    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/ActionView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvP:Landroid/view/ViewGroup;

    invoke-static {v0, p1, v1}, Lcom/google/android/voicesearch/ui/ActionView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 289
    return-void
.end method

.method public final lG(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 370
    const/4 v0, 0x0

    .line 371
    iget-object v2, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvQ:Landroid/view/ViewGroup;

    invoke-static {v2}, Lehd;->aG(Landroid/view/View;)I

    move-result v2

    if-eq v2, p1, :cond_0

    .line 372
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvQ:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, Lehd;->s(Landroid/view/View;I)V

    move v0, v1

    .line 375
    :cond_0
    iget-object v2, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwd:Landroid/widget/TextView;

    invoke-static {v2}, Lehd;->aG(Landroid/view/View;)I

    move-result v2

    if-eq v2, p1, :cond_1

    .line 376
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwd:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lehd;->s(Landroid/view/View;I)V

    .line 379
    :goto_0
    return v1

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method public final lH(I)V
    .locals 2

    .prologue
    .line 443
    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/ui/ActionView;->ld(I)V

    .line 446
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionView;->setClickable(Z)V

    .line 452
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvP:Landroid/view/ViewGroup;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 453
    return-void
.end method

.method public final lc(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 317
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwb:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 318
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwd:Landroid/widget/TextView;

    invoke-virtual {v0, p1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    .line 319
    return-void
.end method

.method public final ld(I)V
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwd:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 345
    return-void
.end method

.method public final le(I)V
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwd:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 366
    return-void
.end method

.method public final oR(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwd:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 350
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 1

    .prologue
    .line 276
    invoke-super {p0, p1}, Lhzw;->onCreateDrawableState(I)[I

    move-result-object v0

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 9

    .prologue
    const-wide/16 v4, 0xfa

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 95
    const v0, 0x7f11010d

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvY:Landroid/view/ViewGroup;

    .line 96
    const v0, 0x7f110115

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvZ:Landroid/view/ViewGroup;

    .line 97
    const v0, 0x7f110107

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvP:Landroid/view/ViewGroup;

    .line 98
    const v0, 0x7f110062

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvQ:Landroid/view/ViewGroup;

    .line 99
    const v0, 0x7f110060

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvR:Landroid/view/ViewGroup;

    .line 100
    const v0, 0x7f110116

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvS:Landroid/view/ViewGroup;

    .line 101
    const v0, 0x7f11010f

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvT:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f11010e

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvV:Landroid/widget/ImageView;

    .line 103
    const v0, 0x7f110117

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvW:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f1100fe

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvU:Landroid/widget/ImageButton;

    .line 105
    const v0, 0x7f110119

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvX:Landroid/widget/GridView;

    .line 106
    const v0, 0x7f11006d

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dmv:Landroid/widget/ProgressBar;

    .line 107
    const v0, 0x7f11011a

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwa:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f11006c

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwb:Landroid/widget/ImageButton;

    .line 110
    const v0, 0x7f110056

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwc:Landroid/widget/ImageButton;

    .line 111
    const v0, 0x7f11006b

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionView;->findViewById(I)Landroid/view/View;

    .line 112
    const v0, 0x7f110110

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/ui/ActionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwd:Landroid/widget/TextView;

    .line 114
    new-instance v0, Libl;

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvT:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/ActionView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Libl;-><init>(Landroid/widget/TextView;Landroid/content/res/Resources;)V

    .line 117
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/transition/TransitionSet;

    invoke-direct {v0}, Landroid/transition/TransitionSet;-><init>()V

    invoke-virtual {v0, v7}, Landroid/transition/TransitionSet;->setOrdering(I)Landroid/transition/TransitionSet;

    new-instance v1, Landroid/transition/TransitionSet;

    invoke-direct {v1}, Landroid/transition/TransitionSet;-><init>()V

    invoke-virtual {v1, v6}, Landroid/transition/TransitionSet;->setOrdering(I)Landroid/transition/TransitionSet;

    new-instance v2, Landroid/transition/Fade;

    invoke-direct {v2, v8}, Landroid/transition/Fade;-><init>(I)V

    invoke-virtual {v2, v4, v5}, Landroid/transition/Fade;->setDuration(J)Landroid/transition/Transition;

    invoke-virtual {v1, v2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    new-instance v2, Landroid/transition/Fade;

    invoke-direct {v2, v6}, Landroid/transition/Fade;-><init>(I)V

    invoke-virtual {v2, v4, v5}, Landroid/transition/Fade;->setDuration(J)Landroid/transition/Transition;

    invoke-virtual {v1, v2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    new-instance v1, Landroid/transition/ChangeBounds;

    invoke-direct {v1}, Landroid/transition/ChangeBounds;-><init>()V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/transition/ChangeBounds;->setDuration(J)Landroid/transition/Transition;

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/ActionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0122

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/ActionView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0123

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sget-object v3, Libb;->dwY:Libb;

    new-array v4, v6, [Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvW:Landroid/widget/TextView;

    aput-object v5, v4, v7

    invoke-static {v3, v1, v4}, Lcom/google/android/voicesearch/ui/ActionView;->a(Libb;I[Landroid/view/View;)Liay;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    sget-object v3, Libb;->dwY:Libb;

    new-array v4, v6, [Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvX:Landroid/widget/GridView;

    aput-object v5, v4, v7

    invoke-static {v3, v2, v4}, Lcom/google/android/voicesearch/ui/ActionView;->a(Libb;I[Landroid/view/View;)Liay;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    sget-object v3, Libb;->dwZ:Libb;

    new-array v4, v8, [Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvT:Landroid/widget/TextView;

    aput-object v5, v4, v7

    iget-object v5, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvV:Landroid/widget/ImageView;

    aput-object v5, v4, v6

    invoke-static {v3, v1, v4}, Lcom/google/android/voicesearch/ui/ActionView;->a(Libb;I[Landroid/view/View;)Liay;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    sget-object v1, Libb;->dwZ:Libb;

    const/4 v3, 0x3

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvP:Landroid/view/ViewGroup;

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwd:Landroid/widget/TextView;

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvQ:Landroid/view/ViewGroup;

    aput-object v4, v3, v8

    invoke-static {v1, v2, v3}, Lcom/google/android/voicesearch/ui/ActionView;->a(Libb;I[Landroid/view/View;)Liay;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwh:Landroid/transition/Transition;

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvZ:Landroid/view/ViewGroup;

    new-instance v1, Lhzy;

    invoke-direct {v1, p0}, Lhzy;-><init>(Lcom/google/android/voicesearch/ui/ActionView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    new-instance v0, Liai;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/ui/ActionView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvO:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Liai;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwf:Liai;

    .line 130
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvX:Landroid/widget/GridView;

    invoke-virtual {v0, v6}, Landroid/widget/GridView;->setChoiceMode(I)V

    .line 131
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvX:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwf:Liai;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 132
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvX:Landroid/widget/GridView;

    invoke-virtual {v0, v7}, Landroid/widget/GridView;->setVerticalScrollBarEnabled(Z)V

    .line 134
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvX:Landroid/widget/GridView;

    new-instance v1, Lhzz;

    invoke-direct {v1, p0}, Lhzz;-><init>(Lcom/google/android/voicesearch/ui/ActionView;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 144
    new-instance v0, Liaa;

    invoke-direct {v0, p0}, Liaa;-><init>(Lcom/google/android/voicesearch/ui/ActionView;)V

    .line 150
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvU:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvR:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    iget-object v1, p0, Lcom/google/android/voicesearch/ui/ActionView;->dvS:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwa:Landroid/widget/TextView;

    new-instance v1, Liab;

    invoke-direct {v1, p0}, Liab;-><init>(Lcom/google/android/voicesearch/ui/ActionView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwb:Landroid/widget/ImageButton;

    new-instance v1, Liac;

    invoke-direct {v1, p0}, Liac;-><init>(Lcom/google/android/voicesearch/ui/ActionView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwd:Landroid/widget/TextView;

    new-instance v1, Liad;

    invoke-direct {v1, p0}, Liad;-><init>(Lcom/google/android/voicesearch/ui/ActionView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    iget-object v0, p0, Lcom/google/android/voicesearch/ui/ActionView;->dwc:Landroid/widget/ImageButton;

    new-instance v1, Liae;

    invoke-direct {v1, p0}, Liae;-><init>(Lcom/google/android/voicesearch/ui/ActionView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    return-void
.end method

.method public final uh()V
    .locals 0

    .prologue
    .line 458
    return-void
.end method
