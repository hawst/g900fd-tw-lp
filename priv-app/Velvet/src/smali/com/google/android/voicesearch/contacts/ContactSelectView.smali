.class public Lcom/google/android/voicesearch/contacts/ContactSelectView;
.super Landroid/widget/RelativeLayout;
.source "PG"


# instance fields
.field private diJ:Lcom/google/android/search/shared/contact/ContactListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 40
    const v0, 0x7f11012f

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/contacts/ContactSelectView;->findViewById(I)Landroid/view/View;

    .line 41
    const v0, 0x7f110131

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/contacts/ContactSelectView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/ContactListView;

    iput-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectView;->diJ:Lcom/google/android/search/shared/contact/ContactListView;

    .line 43
    iget-object v0, p0, Lcom/google/android/voicesearch/contacts/ContactSelectView;->diJ:Lcom/google/android/search/shared/contact/ContactListView;

    new-instance v1, Lhjn;

    invoke-direct {v1, p0}, Lhjn;-><init>(Lcom/google/android/voicesearch/contacts/ContactSelectView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/contact/ContactListView;->a(Ldyz;)V

    .line 51
    return-void
.end method
