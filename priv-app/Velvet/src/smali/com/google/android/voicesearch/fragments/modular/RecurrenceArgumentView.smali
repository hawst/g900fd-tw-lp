.class public Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;
.super Lhou;
.source "PG"


# instance fields
.field private final dmR:Lajr;

.field private dmS:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lhou;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    new-instance v0, Lhos;

    invoke-direct {v0, p0}, Lhos;-><init>(Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;->dmR:Lajr;

    .line 50
    return-void
.end method

.method public static synthetic a(Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;Laiw;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;->c(Laiw;)V

    return-void
.end method

.method private c(Laiw;)V
    .locals 3
    .param p1    # Laiw;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 79
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 81
    if-eqz p1, :cond_1

    .line 82
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, v2, v2}, Laju;->a(Landroid/content/Context;Laiw;ZZ)Ljava/lang/String;

    move-result-object v0

    .line 83
    if-nez v0, :cond_0

    .line 84
    const v0, 0x7f0a08a6

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 89
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;->dmS:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    return-void

    .line 87
    :cond_1
    const v0, 0x7f0a0575

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final ahj()Z
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x1

    return v0
.end method

.method public final ahm()[Landroid/view/View;
    .locals 3

    .prologue
    .line 94
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;->dmS:Landroid/widget/TextView;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic b(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;->b(Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;)V

    return-void
.end method

.method public final b(Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;)V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0, p1}, Lhou;->b(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V

    .line 69
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laiw;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;->c(Laiw;)V

    .line 70
    new-instance v0, Lhot;

    invoke-direct {v0, p0, p1}, Lhot;-><init>(Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;)V

    invoke-virtual {p1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->a(Ldwn;)V

    .line 76
    return-void
.end method

.method protected final onClick()V
    .locals 6

    .prologue
    .line 112
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "bundle_event_start_time"

    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->ajy()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "bundle_event_time_zone"

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/RecurrenceArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laiw;

    invoke-virtual {v0}, Laiw;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v2, "bundle_event_rrule"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/android/recurrencepicker/RecurrencePickerDialog;

    invoke-direct {v0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;-><init>()V

    invoke-virtual {v0, v1}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->setArguments(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;->dmR:Lajr;

    invoke-virtual {v0, v1}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->a(Lajr;)V

    const-string v1, "recurrencepicker_tag"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;->a(Landroid/app/DialogFragment;Ljava/lang/String;)V

    .line 113
    return-void

    .line 112
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Lhou;->onFinishInflate()V

    .line 63
    const v0, 0x7f1100a9

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;->dmS:Landroid/widget/TextView;

    .line 64
    return-void
.end method

.method public final setEditable(Z)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/RecurrenceArgumentView;->dmS:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 100
    return-void
.end method
