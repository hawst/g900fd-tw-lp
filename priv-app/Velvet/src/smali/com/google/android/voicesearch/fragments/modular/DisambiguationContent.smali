.class public Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private aom:Lcom/google/android/search/shared/contact/ContactDisambiguationView;

.field private dlA:Lcom/google/android/search/shared/contact/PersonDisambiguation;

.field private dlB:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

.field private dly:Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;

.field private dlz:Lcom/google/android/voicesearch/fragments/LocalResultsView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    return-void
.end method

.method public static synthetic a(Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;)Lcom/google/android/search/shared/contact/PersonDisambiguation;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->dlA:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    return-object v0
.end method

.method private bP(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 210
    const/4 v0, 0x3

    new-array v3, v0, [Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->aom:Lcom/google/android/search/shared/contact/ContactDisambiguationView;

    aput-object v0, v3, v1

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->dly:Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;

    aput-object v2, v3, v0

    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->dlz:Lcom/google/android/voicesearch/fragments/LocalResultsView;

    aput-object v2, v3, v0

    .line 212
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 213
    if-ne v5, p1, :cond_0

    move v0, v1

    :goto_1
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 212
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 213
    :cond_0
    const/16 v0, 0x8

    goto :goto_1

    .line 215
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;Lhop;Ldvt;)V
    .locals 3

    .prologue
    .line 106
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->ajO()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    :goto_0
    return-void

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->dly:Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->bP(Landroid/view/View;)V

    .line 112
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    .line 113
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->dly:Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;

    invoke-virtual {p2}, Lhop;->anM()Lesm;

    move-result-object v2

    invoke-virtual {v1, v2, p3}, Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;->a(Lesm;Ldvt;)V

    .line 114
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->dly:Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;->a(Lcom/google/android/search/shared/actions/utils/Disambiguation;Ljava/lang/Object;)V

    .line 115
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->dly:Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;

    new-instance v2, Lhnh;

    invoke-direct {v2, p0, v0}, Lhnh;-><init>(Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;Lcom/google/android/search/shared/actions/utils/Disambiguation;)V

    invoke-virtual {v1, v2}, Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;->a(Lecz;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->dly:Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/EntityArgument;->ajk()I

    move-result v1

    invoke-static {v0, v1}, Lehd;->s(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;Lhop;)V
    .locals 6

    .prologue
    .line 166
    new-instance v2, Lhnj;

    invoke-direct {v2, p0, p2, p1}, Lhnj;-><init>(Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;Lhop;Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;)V

    .line 202
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->dlz:Lcom/google/android/voicesearch/fragments/LocalResultsView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/LocalResultsView;->uM()V

    .line 203
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->dlz:Lcom/google/android/voicesearch/fragments/LocalResultsView;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->bP(Landroid/view/View;)V

    .line 204
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->dlz:Lcom/google/android/voicesearch/fragments/LocalResultsView;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/LocationArgument;->ahw()Ljpe;

    move-result-object v1

    invoke-virtual {p2}, Lhop;->anM()Lesm;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/voicesearch/fragments/LocalResultsView;->a(Ljpe;Lhkz;Lesm;Landroid/view/LayoutInflater;Z)V

    .line 207
    return-void
.end method

.method public final a(Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;Lhop;)V
    .locals 3

    .prologue
    .line 139
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alu()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->ajk()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/search/shared/contact/Person;->gX(I)V

    goto :goto_0

    .line 140
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    .line 141
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->aom:Lcom/google/android/search/shared/contact/ContactDisambiguationView;

    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->bP(Landroid/view/View;)V

    .line 143
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->aom:Lcom/google/android/search/shared/contact/ContactDisambiguationView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->ex(Z)V

    .line 144
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->aom:Lcom/google/android/search/shared/contact/ContactDisambiguationView;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->akB()Ldzb;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->a(Lcom/google/android/search/shared/actions/utils/Disambiguation;Ldzb;)V

    .line 145
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->aom:Lcom/google/android/search/shared/contact/ContactDisambiguationView;

    new-instance v2, Lhni;

    invoke-direct {v2, p0, v0, p2}, Lhni;-><init>(Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;Lcom/google/android/search/shared/contact/PersonDisambiguation;Lhop;)V

    invoke-virtual {v1, v2}, Lcom/google/android/search/shared/contact/ContactDisambiguationView;->a(Lecz;)V

    .line 162
    return-void
.end method

.method public final a(Lhop;Ldvt;)V
    .locals 3

    .prologue
    .line 82
    invoke-virtual {p1}, Lhop;->aRe()Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v1

    .line 83
    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->Pd()Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 86
    instance-of v0, v1, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 87
    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->dlA:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    .line 92
    :goto_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->dlB:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    invoke-static {v1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->a(Lcom/google/android/search/shared/actions/modular/arguments/Argument;Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z

    move-result v0

    .line 95
    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajE()Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->dlB:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    .line 97
    if-eqz v0, :cond_1

    .line 102
    :goto_1
    return-void

    .line 89
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->dlA:Lcom/google/android/search/shared/contact/PersonDisambiguation;

    goto :goto_0

    .line 101
    :cond_1
    new-instance v0, Lhnk;

    invoke-direct {v0, p0, p1, p2}, Lhnk;-><init>(Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;Lhop;Ldvt;)V

    invoke-virtual {v1, v0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->a(Ldwu;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 71
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 72
    const v0, 0x7f11007a

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/ContactDisambiguationView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->aom:Lcom/google/android/search/shared/contact/ContactDisambiguationView;

    .line 74
    const v0, 0x7f1101af

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->dly:Lcom/google/android/search/shared/actions/modular/EntityDisambiguationView;

    .line 76
    const v0, 0x7f11024e

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/voicesearch/fragments/LocalResultsView;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DisambiguationContent;->dlz:Lcom/google/android/voicesearch/fragments/LocalResultsView;

    .line 78
    return-void
.end method
