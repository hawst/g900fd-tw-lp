.class public Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;
.super Lhou;
.source "PG"


# instance fields
.field private final dlq:Lnk;

.field private dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

.field public dls:Lhnn;

.field private dlt:Ldxw;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lhou;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    new-instance v0, Lhnd;

    invoke-direct {v0, p0}, Lhnd;-><init>(Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;)V

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dlq:Lnk;

    .line 50
    return-void
.end method

.method private a(Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 104
    invoke-static {p2, p1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->a(Lcom/google/android/search/shared/actions/modular/arguments/Argument;Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z

    move-result v0

    .line 105
    invoke-super {p0, p1}, Lhou;->b(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V

    .line 106
    if-eqz v0, :cond_0

    .line 138
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dls:Lhnn;

    invoke-virtual {v0}, Lhnn;->clear()V

    .line 110
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 111
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->Pd()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 112
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->getCalendar()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 113
    iget-object v4, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dls:Lhnn;

    iget-object v5, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dlt:Ldxw;

    invoke-virtual {v5, v2, v3, v6}, Ldxw;->a(JLcom/google/android/search/shared/actions/RemindersConfigFlags;)Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Ldxw;->z(Ljava/util/List;)[Lelh;

    move-result-object v5

    invoke-virtual {v4, v5}, Lhnn;->addAll([Ljava/lang/Object;)V

    .line 115
    invoke-static {v0, v1}, Lesi;->isToday(J)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {v2, v3}, Ldxw;->az(J)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 117
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dlt:Ldxw;

    invoke-virtual {v0}, Ldxw;->akY()Lelh;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->a(Lelh;)V

    goto :goto_0

    .line 118
    :cond_1
    invoke-static {v0, v1}, Lesi;->aY(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 119
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dlt:Ldxw;

    invoke-virtual {v0}, Ldxw;->akZ()Lelh;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->a(Lelh;)V

    goto :goto_0

    .line 121
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0xa12

    invoke-static {v2, v0, v1, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 122
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->ajH()I

    move-result v1

    .line 123
    packed-switch v1, :pswitch_data_0

    .line 132
    :goto_1
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dlt:Ldxw;

    invoke-virtual {v0}, Ldxw;->alb()Lelh;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->a(Lelh;)V

    goto :goto_0

    .line 125
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dls:Lhnn;

    invoke-virtual {v1, v0}, Lhnn;->F(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 128
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dls:Lhnn;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0898

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Lhnn;->F(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 135
    :cond_3
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dls:Lhnn;

    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dlt:Ldxw;

    invoke-virtual {v1, v2, v3, v6}, Ldxw;->a(JLcom/google/android/search/shared/actions/RemindersConfigFlags;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ldxw;->z(Ljava/util/List;)[Lelh;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhnn;->addAll([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 123
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static synthetic a(Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;)V
    .locals 0

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->ahi()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->a(Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;)V

    return-void
.end method

.method private a(Lelh;)V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dls:Lhnn;

    invoke-virtual {v0, p1}, Lhnn;->getPosition(Ljava/lang/Object;)I

    move-result v1

    .line 163
    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 164
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->hY(I)V

    .line 165
    return-void

    .line 163
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;)V
    .locals 0

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->ahi()V

    return-void
.end method

.method private b(Ljava/util/Calendar;)V
    .locals 4

    .prologue
    .line 157
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    const/4 v1, 0x5

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->r(III)V

    .line 159
    return-void
.end method

.method public static synthetic c(Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;)Lcom/google/android/shared/ui/SpinnerAlwaysCallback;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    return-object v0
.end method


# virtual methods
.method public final aQT()V
    .locals 3

    .prologue
    .line 141
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 142
    new-instance v2, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    invoke-direct {v2, v0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;)V

    .line 143
    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->b(Ljava/util/Calendar;)V

    .line 144
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    invoke-direct {p0, v0, v2}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->a(Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;)V

    .line 145
    return-void
.end method

.method public final aQU()V
    .locals 3

    .prologue
    .line 148
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 149
    const/4 v0, 0x5

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->add(II)V

    .line 150
    new-instance v2, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    invoke-direct {v2, v0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;-><init>(Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;)V

    .line 151
    invoke-direct {p0, v1}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->b(Ljava/util/Calendar;)V

    .line 152
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    invoke-direct {p0, v0, v2}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->a(Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;)V

    .line 153
    return-void
.end method

.method public final aQV()V
    .locals 5

    .prologue
    .line 172
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    .line 174
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->Pd()Z

    move-result v1

    if-nez v1, :cond_0

    .line 175
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 176
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dlq:Lnk;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-static {v1, v2, v3, v0}, Lnh;->a(Lnk;III)Lnh;

    move-result-object v0

    .line 184
    :goto_0
    const-string v1, "datepicker_tag"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->a(Landroid/app/DialogFragment;Ljava/lang/String;)V

    .line 185
    return-void

    .line 181
    :cond_0
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dlq:Lnk;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->getYear()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->getMonth()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;->getDayOfMonth()I

    move-result v0

    invoke-static {v1, v2, v3, v0}, Lnh;->a(Lnk;III)Lnh;

    move-result-object v0

    goto :goto_0
.end method

.method public final ahm()[Landroid/view/View;
    .locals 3

    .prologue
    .line 194
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic b(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->b(Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;)V

    return-void
.end method

.method public final b(Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;

    invoke-direct {p0, p1, v0}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->a(Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;Lcom/google/android/search/shared/actions/modular/arguments/DateArgument;)V

    .line 98
    return-void
.end method

.method protected final onClick()V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->performClick()Z

    .line 200
    return-void
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0}, Lhou;->onFinishInflate()V

    .line 63
    const v0, 0x7f110405

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iput-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    .line 64
    invoke-virtual {p0}, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 65
    new-instance v1, Lhnn;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v2}, Ldxw;->z(Ljava/util/List;)[Lelh;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lhnn;-><init>(Landroid/content/Context;[Lelh;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dls:Lhnn;

    .line 68
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    iget-object v2, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dls:Lhnn;

    invoke-virtual {v1, v2}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 69
    iget-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    new-instance v2, Lhne;

    invoke-direct {v2, p0}, Lhne;-><init>(Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;)V

    invoke-virtual {v1, v2}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->a(Lekh;)V

    .line 92
    new-instance v1, Ldxw;

    invoke-direct {v1, v0}, Ldxw;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dlt:Ldxw;

    .line 93
    return-void
.end method

.method public final setEditable(Z)V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/voicesearch/fragments/modular/DateArgumentView;->dlr:Lcom/google/android/shared/ui/SpinnerAlwaysCallback;

    invoke-virtual {v0, p1}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->setEnabled(Z)V

    .line 190
    return-void
.end method
