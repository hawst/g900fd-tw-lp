.class public Lcom/google/android/voicesearch/SendSmsActivity;
.super Landroid/app/Activity;
.source "PG"


# instance fields
.field private dhc:Lhhk;

.field private dhd:Ljava/lang/String;

.field private dhe:Landroid/os/PowerManager$WakeLock;

.field public mTimeoutWatchdog:Lidv;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->dhe:Landroid/os/PowerManager$WakeLock;

    .line 78
    return-void
.end method

.method private aON()Z
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->dhd:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aOO()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 221
    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->dhc:Lhhk;

    if-eqz v0, :cond_0

    .line 223
    :try_start_0
    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->dhc:Lhhk;

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/SendSmsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    :goto_0
    iput-object v3, p0, Lcom/google/android/voicesearch/SendSmsActivity;->dhc:Lhhk;

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->mTimeoutWatchdog:Lidv;

    if-eqz v0, :cond_1

    .line 230
    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->mTimeoutWatchdog:Lidv;

    invoke-virtual {v0}, Lidv;->stop()V

    .line 231
    iput-object v3, p0, Lcom/google/android/voicesearch/SendSmsActivity;->mTimeoutWatchdog:Lidv;

    .line 233
    :cond_1
    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->dhe:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_3

    .line 234
    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->dhe:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 235
    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->dhe:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 237
    :cond_2
    iput-object v3, p0, Lcom/google/android/voicesearch/SendSmsActivity;->dhe:Landroid/os/PowerManager$WakeLock;

    .line 239
    :cond_3
    return-void

    .line 224
    :catch_0
    move-exception v0

    .line 225
    const-string v1, "SendSmsActivity"

    const-string v2, "StatusReceiver already unregistered. Ignoring error."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 284
    if-nez p1, :cond_0

    .line 285
    const-string v0, "SendSmsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "failure sending sms, status:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    :goto_0
    invoke-direct {p0}, Lcom/google/android/voicesearch/SendSmsActivity;->aON()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 290
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.e100.NOTIFICATION_RECEIVED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 291
    const-string v1, "com.google.android.e100.MESSAGE"

    iget-object v2, p0, Lcom/google/android/voicesearch/SendSmsActivity;->dhd:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 292
    const-string v1, "com.google.android.e100.FORCE_READ"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 293
    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/SendSmsActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 297
    :goto_1
    invoke-direct {p0}, Lcom/google/android/voicesearch/SendSmsActivity;->aOO()V

    .line 298
    invoke-virtual {p0}, Lcom/google/android/voicesearch/SendSmsActivity;->finish()V

    .line 299
    return-void

    .line 287
    :cond_0
    const-string v0, "SendSmsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "failure sending sms, status:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 295
    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/android/voicesearch/SendSmsActivity;->showDialog(I)V

    goto :goto_1
.end method

.method public final aOP()V
    .locals 1

    .prologue
    .line 276
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/SendSmsActivity;->setResult(I)V

    .line 277
    invoke-direct {p0}, Lcom/google/android/voicesearch/SendSmsActivity;->aOO()V

    .line 278
    invoke-virtual {p0}, Lcom/google/android/voicesearch/SendSmsActivity;->finish()V

    .line 279
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 122
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/voicesearch/SendSmsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 125
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 126
    const-string v0, "smsto"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 127
    const-string v0, "SendSmsActivity"

    const-string v1, "unexpected data scheme, requires \'smsto\'"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-virtual {p0}, Lcom/google/android/voicesearch/SendSmsActivity;->finish()V

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    const-string v0, "com.google.android.e100.FAILURE_TTS"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->dhd:Ljava/lang/String;

    .line 133
    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->dhe:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->dhe:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 134
    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->dhe:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 136
    :cond_2
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/SendSmsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 137
    const/4 v3, 0x1

    const-string v4, "SendSmsActivity"

    invoke-virtual {v0, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->dhe:Landroid/os/PowerManager$WakeLock;

    .line 138
    iget-object v0, p0, Lcom/google/android/voicesearch/SendSmsActivity;->dhe:Landroid/os/PowerManager$WakeLock;

    const-wide/32 v4, 0xea60

    invoke-virtual {v0, v4, v5}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 140
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    .line 142
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 144
    :try_start_0
    const-string v3, "UTF-8"

    invoke-static {v2, v3}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 150
    const-string v2, ","

    invoke-virtual {v8, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 152
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 153
    if-nez v1, :cond_5

    const-string v1, ""

    move-object v6, v1

    .line 154
    :goto_1
    invoke-virtual {v0, v6}, Landroid/telephony/SmsManager;->divideMessage(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 155
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 157
    new-instance v1, Lidv;

    const v2, 0xea60

    new-instance v4, Lhhg;

    const-string v5, "Send SMS timeout, background"

    const/4 v7, 0x0

    new-array v7, v7, [I

    invoke-direct {v4, p0, v5, v7}, Lhhg;-><init>(Lcom/google/android/voicesearch/SendSmsActivity;Ljava/lang/String;[I)V

    invoke-direct {v1, v2, v4}, Lidv;-><init>(ILjava/lang/Runnable;)V

    iput-object v1, p0, Lcom/google/android/voicesearch/SendSmsActivity;->mTimeoutWatchdog:Lidv;

    .line 170
    iget-object v1, p0, Lcom/google/android/voicesearch/SendSmsActivity;->mTimeoutWatchdog:Lidv;

    invoke-virtual {v1}, Lidv;->start()V

    .line 177
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.google.android.voicesearch.action.SMS_STATUS"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 178
    new-instance v2, Lhhk;

    array-length v4, v9

    mul-int/2addr v4, v10

    invoke-direct {v2, p0, v4}, Lhhk;-><init>(Lcom/google/android/voicesearch/SendSmsActivity;I)V

    iput-object v2, p0, Lcom/google/android/voicesearch/SendSmsActivity;->dhc:Lhhk;

    .line 179
    iget-object v2, p0, Lcom/google/android/voicesearch/SendSmsActivity;->dhc:Lhhk;

    invoke-virtual {p0, v2, v1}, Lcom/google/android/voicesearch/SendSmsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 181
    invoke-direct {p0}, Lcom/google/android/voicesearch/SendSmsActivity;->aON()Z

    move-result v1

    if-nez v1, :cond_3

    .line 182
    const v1, 0x7f0a0853

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 185
    :cond_3
    array-length v11, v9

    const/4 v1, 0x0

    move v7, v1

    :goto_2
    if-ge v7, v11, :cond_0

    aget-object v1, v9, v7

    .line 192
    new-instance v5, Landroid/content/Intent;

    const-string v2, "com.google.android.voicesearch.action.SMS_STATUS"

    invoke-direct {v5, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 193
    invoke-virtual {p0}, Lcom/google/android/voicesearch/SendSmsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    const-string v2, "com.google.android.voicesearch.extras.SMS_RECIPIENTS"

    invoke-virtual {v5, v2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 195
    const-string v2, "com.google.android.voicesearch.extras.SMS_MESSAGE"

    invoke-virtual {v5, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 200
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v10, :cond_4

    .line 201
    const/4 v12, 0x0

    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {p0, v12, v5, v13}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 200
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 145
    :catch_0
    move-exception v0

    .line 146
    const-string v1, "SendSmsActivity"

    const-string v2, "error decoding recipients"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/SendSmsActivity;->a(Ljava/lang/Exception;I)V

    goto/16 :goto_0

    .line 209
    :cond_4
    const/4 v2, 0x0

    const/4 v5, 0x0

    :try_start_1
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 185
    :goto_4
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_2

    .line 211
    :catch_1
    move-exception v1

    .line 212
    const/4 v2, -0x1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/voicesearch/SendSmsActivity;->a(Ljava/lang/Exception;I)V

    goto :goto_4

    :cond_5
    move-object v6, v1

    goto/16 :goto_1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 245
    packed-switch p1, :pswitch_data_0

    .line 270
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 247
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0a0854

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lhhj;

    invoke-direct {v1, p0}, Lhhj;-><init>(Lcom/google/android/voicesearch/SendSmsActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lhhi;

    invoke-direct {v2, p0}, Lhhi;-><init>(Lcom/google/android/voicesearch/SendSmsActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 245
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
