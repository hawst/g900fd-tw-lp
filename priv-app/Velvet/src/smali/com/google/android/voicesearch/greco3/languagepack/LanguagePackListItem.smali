.class public Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;
.super Landroid/widget/RelativeLayout;
.source "PG"


# instance fields
.field private anD:Ljava/lang/String;

.field public doQ:Lhrk;

.field public doR:Ljzp;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public doS:Ljzp;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private mConfiguration:Ljze;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method

.method private e(Ljzp;)Landroid/app/AlertDialog;
    .locals 6

    .prologue
    .line 199
    new-instance v0, Lhrd;

    invoke-direct {v0, p0, p1}, Lhrd;-><init>(Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;Ljzp;)V

    .line 205
    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0938

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljzp;->getVersion()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->c(Ljzp;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 207
    const v2, 0x7f0a0939

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->a(Ljava/lang/String;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a05ec

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 3

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->mConfiguration:Ljze;

    iget-object v1, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->anD:Ljava/lang/String;

    invoke-static {v0, v1}, Lgnq;->b(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 178
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lhrk;Ljze;Ljzp;Ljzp;)V
    .locals 8
    .param p3    # Ljzp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljzp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 54
    iput-object p2, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->mConfiguration:Ljze;

    .line 55
    iput-object p1, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doQ:Lhrk;

    .line 57
    iput-object p3, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doR:Ljzp;

    .line 58
    iput-object p4, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doS:Ljzp;

    .line 60
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doR:Ljzp;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doR:Ljzp;

    invoke-virtual {v0}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->anD:Ljava/lang/String;

    .line 71
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->aRC()I

    move-result v1

    const v0, 0x7f110239

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->lt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->mConfiguration:Ljze;

    iget-object v3, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->anD:Ljava/lang/String;

    invoke-static {v2, v3}, Lgnq;->b(Ljze;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->b(Landroid/widget/TextView;Ljava/lang/String;)V

    const v0, 0x7f11023a

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->lt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown action "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doS:Ljzp;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doS:Ljzp;

    invoke-virtual {v0}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->anD:Ljava/lang/String;

    goto :goto_0

    .line 65
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "installed and updgrade language packs cannot both be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :pswitch_1
    const v1, 0x7f0a0946

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->b(Landroid/widget/TextView;I)V

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doS:Ljzp;

    new-instance v1, Lhrf;

    invoke-direct {v1, p0, v0}, Lhrf;-><init>(Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;Ljzp;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0938

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljzp;->getVersion()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->c(Ljzp;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0a093d

    invoke-virtual {p0, v0, v2, v1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->a(Ljava/lang/String;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a093c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    :goto_1
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->setClickable(Z)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->setFocusable(Z)V

    new-instance v1, Lhrc;

    invoke-direct {v1, p0, v0}, Lhrc;-><init>(Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;Landroid/app/AlertDialog;)V

    invoke-virtual {p0, v1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    return-void

    .line 71
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0941

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doS:Ljzp;

    invoke-virtual {p0, v5}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->d(Ljzp;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->b(Landroid/widget/TextView;Ljava/lang/String;)V

    new-instance v0, Lhrg;

    invoke-direct {v0, p0}, Lhrg;-><init>(Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;)V

    new-instance v1, Lhrh;

    invoke-direct {v1, p0}, Lhrh;-><init>(Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0937

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doR:Ljzp;

    invoke-virtual {v7}, Ljzp;->getVersion()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doR:Ljzp;

    invoke-virtual {p0, v7}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->c(Ljzp;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a0938

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doS:Ljzp;

    invoke-virtual {v7}, Ljzp;->getVersion()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doS:Ljzp;

    invoke-virtual {p0, v7}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->c(Ljzp;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a0939

    invoke-virtual {p0, v2, v3, v0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->a(Ljava/lang/String;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0a05ec

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0a093b

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0943

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doR:Ljzp;

    invoke-virtual {p0, v5}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->d(Ljzp;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->b(Landroid/widget/TextView;Ljava/lang/String;)V

    new-instance v0, Lhre;

    invoke-direct {v0, p0}, Lhre;-><init>(Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0937

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doR:Ljzp;

    invoke-virtual {v5}, Ljzp;->getVersion()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doR:Ljzp;

    invoke-virtual {p0, v5}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->c(Ljzp;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a093b

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->a(Ljava/lang/String;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a05ec

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0942

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doS:Ljzp;

    invoke-virtual {p0, v5}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->d(Ljzp;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->b(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doS:Ljzp;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->e(Ljzp;)Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_5
    const v1, 0x7f0a0945

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->b(Landroid/widget/TextView;I)V

    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0936

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doR:Ljzp;

    invoke-virtual {v4}, Ljzp;->getVersion()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doR:Ljzp;

    invoke-virtual {p0, v4}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->c(Ljzp;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a093e

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->a(Ljava/lang/String;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_6
    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0944

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doS:Ljzp;

    invoke-virtual {p0, v5}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->d(Ljzp;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->b(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doS:Ljzp;

    invoke-direct {p0, v0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->e(Ljzp;)Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method protected final aRC()I
    .locals 4

    .prologue
    const/4 v0, 0x7

    const/4 v1, 0x5

    .line 92
    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doS:Ljzp;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doQ:Lhrk;

    iget-object v3, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doS:Ljzp;

    invoke-virtual {v2, v3}, Lhrk;->g(Ljzp;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 93
    const/4 v0, 0x1

    .line 117
    :cond_0
    :goto_0
    return v0

    .line 94
    :cond_1
    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doQ:Lhrk;

    iget-object v3, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->anD:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lhrk;->oj(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 95
    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doQ:Lhrk;

    iget-object v3, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->anD:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lhrk;->mO(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 96
    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doQ:Lhrk;

    iget-object v3, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->anD:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lhrk;->mP(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 97
    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doS:Ljzp;

    if-nez v2, :cond_0

    move v0, v1

    .line 100
    goto :goto_0

    .line 103
    :cond_2
    iget-object v0, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doS:Ljzp;

    if-eqz v0, :cond_3

    .line 104
    const/4 v0, 0x6

    goto :goto_0

    .line 106
    :cond_3
    const/4 v0, 0x4

    goto :goto_0

    .line 110
    :cond_4
    iget-object v2, p0, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->doS:Ljzp;

    if-nez v2, :cond_0

    move v0, v1

    .line 113
    goto :goto_0

    .line 117
    :cond_5
    const/4 v0, 0x3

    goto :goto_0
.end method

.method protected b(Landroid/widget/TextView;I)V
    .locals 0

    .prologue
    .line 82
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 83
    return-void
.end method

.method protected b(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 87
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    return-void
.end method

.method protected final c(Ljzp;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Ljzp;->bxl()I

    move-result v1

    mul-int/lit16 v1, v1, 0x400

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final d(Ljzp;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Ljzp;->bxl()I

    move-result v1

    mul-int/lit16 v1, v1, 0x400

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected lt(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Lcom/google/android/voicesearch/greco3/languagepack/LanguagePackListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
