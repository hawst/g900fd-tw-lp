.class public Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;
.super Landroid/app/Service;
.source "PG"


# instance fields
.field private aOW:Lbyn;

.field private aOX:Ljava/io/BufferedWriter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private BY()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 125
    const-string v1, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 127
    const-string v1, "HotwordBenchmarkCtrl"

    const-string v2, "SD card root directory is not available"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 136
    :goto_0
    return v0

    .line 130
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    const-string v2, "benchmark.txt"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 132
    :try_start_0
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, v0}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    iput-object v1, p0, Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;->aOX:Ljava/io/BufferedWriter;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 133
    :catch_0
    move-exception v0

    .line 134
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;->output(Ljava/lang/String;)V

    return-void
.end method

.method private output(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 116
    :try_start_0
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;->aOX:Ljava/io/BufferedWriter;

    invoke-virtual {v0, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;->aOX:Ljava/io/BufferedWriter;

    invoke-virtual {v0}, Ljava/io/BufferedWriter;->newLine()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :goto_0
    return-void

    .line 119
    :catch_0
    move-exception v0

    .line 120
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 63
    new-instance v0, Lbyn;

    invoke-virtual {p0}, Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-direct {v0, v1}, Lbyn;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;->aOW:Lbyn;

    .line 65
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 66
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 68
    const/16 v1, 0x4d2

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;->startForeground(ILandroid/app/Notification;)V

    .line 69
    invoke-direct {p0}, Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;->BY()Z

    .line 70
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;->aOX:Ljava/io/BufferedWriter;

    if-eqz v0, :cond_0

    .line 105
    :try_start_0
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;->aOX:Ljava/io/BufferedWriter;

    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V

    .line 106
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;->aOX:Ljava/io/BufferedWriter;

    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    :cond_0
    :goto_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 112
    return-void

    .line 107
    :catch_0
    move-exception v0

    .line 108
    const-string v1, "HotwordBenchmarkCtrl"

    const-string v2, "Error closing output file: "

    invoke-static {v1, v2, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 75
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 76
    const-string v0, "com.google.android.googlequicksearchbox.LISTEN_HOTWORD"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 77
    const-string v0, "audio_file_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 78
    const-string v1, "enroll"

    invoke-virtual {p1, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 79
    if-eqz v0, :cond_1

    .line 80
    invoke-direct {p0, v0}, Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;->output(Ljava/lang/String;)V

    .line 86
    :goto_0
    new-instance v0, Lbyp;

    invoke-direct {v0, p0}, Lbyp;-><init>(Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;)V

    iget-object v2, p0, Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;->aOW:Lbyn;

    invoke-virtual {v2, v1, v0}, Lbyn;->a(ZLbyy;)V

    .line 92
    :cond_0
    :goto_1
    return v6

    .line 84
    :cond_1
    const-string v0, "Missing audio file name"

    invoke-direct {p0, v0}, Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;->output(Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_2
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/controller/HotwordBenchmarkController;->aOW:Lbyn;

    const-string v1, "com.google.android.googlequicksearchbox.HOTWORD_BENCHMARK_SERVICE"

    const-string v2, "com.google.android.googlequicksearchbox"

    iget-boolean v3, v0, Lbyn;->aOT:Z

    if-nez v3, :cond_0

    iput-boolean v6, v0, Lbyn;->aOT:Z

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, v0, Lbyn;->mContext:Landroid/content/Context;

    iget-object v3, v0, Lbyn;->abz:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v1, v3, v6}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "HotwordBenchmarkServiceClient"

    const-string v2, "Unable to bind to the hotword benchmark service"

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    iput-boolean v5, v0, Lbyn;->aOT:Z

    goto :goto_1
.end method
