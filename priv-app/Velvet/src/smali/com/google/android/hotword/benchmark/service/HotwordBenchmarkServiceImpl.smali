.class public Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;
.super Lbyq;
.source "PG"


# static fields
.field private static final aPb:Lcom/google/android/search/shared/service/ClientConfig;

.field private static final aPc:Lcom/google/android/search/shared/service/ClientConfig;

.field private static final ani:Lcom/google/android/shared/search/SearchBoxStats;


# instance fields
.field private aPd:Lecq;

.field private aPe:Lecq;

.field private aPf:Z

.field private aPg:Z

.field private aPh:Z

.field private aPi:Z

.field public ank:Ljava/util/List;

.field private anl:Larv;

.field private anm:Lecp;

.field private final anp:Lecr;

.field private final anq:Lary;

.field private mAsyncServices:Lema;

.field private mGsaConfigFlags:Lchk;

.field private mVoiceSettings:Lhym;

.field private mVss:Lhhq;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 45
    const-string v0, "hotword-benchmark-service"

    const-string v1, "android-search-app"

    invoke-static {v0, v1}, Lcom/google/android/shared/search/SearchBoxStats;->aA(Ljava/lang/String;Ljava/lang/String;)Lehj;

    move-result-object v0

    invoke-virtual {v0}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v0

    sput-object v0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->ani:Lcom/google/android/shared/search/SearchBoxStats;

    .line 49
    new-instance v0, Lcom/google/android/search/shared/service/ClientConfig;

    const-wide/32 v2, 0x80000

    sget-object v1, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->ani:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/search/shared/service/ClientConfig;-><init>(JLcom/google/android/shared/search/SearchBoxStats;)V

    sput-object v0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPb:Lcom/google/android/search/shared/service/ClientConfig;

    .line 52
    new-instance v0, Lcom/google/android/search/shared/service/ClientConfig;

    const-wide/32 v2, 0x20080000

    sget-object v1, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->ani:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/search/shared/service/ClientConfig;-><init>(JLcom/google/android/shared/search/SearchBoxStats;)V

    sput-object v0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPc:Lcom/google/android/search/shared/service/ClientConfig;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lbyq;-><init>()V

    .line 71
    new-instance v0, Lbys;

    invoke-direct {v0, p0}, Lbys;-><init>(Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;)V

    iput-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->anp:Lecr;

    .line 204
    new-instance v0, Lbyt;

    invoke-direct {v0, p0}, Lbyt;-><init>(Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;)V

    iput-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->anq:Lary;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPh:Z

    return v0
.end method

.method public static synthetic b(Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->ank:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const-string v0, "benchmark-test"

    invoke-static {p0, v0}, Lgep;->p(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final Ca()V
    .locals 3

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->ue()Lcse;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "benchmark-test"

    invoke-virtual {v0, v1, v2}, Lcse;->a([BLjava/lang/String;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->ue()Lcse;

    move-result-object v0

    const-string v1, "benchmark-test"

    invoke-virtual {v0, v1}, Lcse;->iJ(Ljava/lang/String;)V

    .line 175
    return-void
.end method

.method public final bU(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 127
    iget-boolean v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPh:Z

    if-nez v0, :cond_2

    if-ne p1, v3, :cond_2

    .line 132
    iget-boolean v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPi:Z

    if-eqz v0, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->Ca()V

    .line 134
    iput-boolean v4, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPi:Z

    .line 142
    :cond_0
    :goto_0
    iput-boolean p1, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPh:Z

    .line 148
    if-nez p1, :cond_3

    iget-boolean v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPi:Z

    if-eqz v0, :cond_3

    .line 149
    iget-boolean v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPg:Z

    if-nez v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPd:Lecq;

    invoke-virtual {v0}, Lecq;->stop()V

    .line 152
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPe:Lecq;

    invoke-virtual {v0}, Lecq;->start()V

    .line 153
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPe:Lecq;

    invoke-virtual {v0, v3}, Lecq;->bq(Z)V

    .line 154
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPd:Lecq;

    invoke-virtual {v0, v4}, Lecq;->bq(Z)V

    .line 155
    iput-boolean v3, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPg:Z

    .line 156
    iput-boolean v4, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPf:Z

    .line 169
    :cond_1
    :goto_1
    return-void

    .line 136
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPh:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->anl:Larv;

    iget-object v1, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->ank:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->anq:Lary;

    invoke-virtual {v0, v1, v2}, Larv;->a(Ljava/util/List;Lary;)V

    .line 140
    iput-boolean v3, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPi:Z

    goto :goto_0

    .line 159
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPf:Z

    if-nez v0, :cond_1

    .line 161
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPe:Lecq;

    invoke-virtual {v0}, Lecq;->stop()V

    .line 162
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPd:Lecq;

    invoke-virtual {v0}, Lecq;->start()V

    .line 163
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPd:Lecq;

    invoke-virtual {v0, v3}, Lecq;->bq(Z)V

    .line 164
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPe:Lecq;

    invoke-virtual {v0, v4}, Lecq;->bq(Z)V

    .line 165
    iput-boolean v3, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPf:Z

    .line 166
    iput-boolean v4, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPg:Z

    goto :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 85
    invoke-super {p0, p1}, Lbyq;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 15

    .prologue
    const/4 v5, 0x0

    .line 90
    invoke-super {p0}, Lbyq;->onCreate()V

    .line 91
    new-instance v0, Lbyu;

    invoke-direct {v0, p0}, Lbyu;-><init>(Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;)V

    iput-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->anm:Lecp;

    .line 93
    new-instance v0, Lecq;

    iget-object v2, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->anp:Lecr;

    iget-object v3, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->anm:Lecp;

    sget-object v4, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPc:Lcom/google/android/search/shared/service/ClientConfig;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lecq;-><init>(Landroid/content/Context;Lecr;Lecm;Lcom/google/android/search/shared/service/ClientConfig;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPe:Lecq;

    .line 95
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPe:Lecq;

    invoke-virtual {v0}, Lecq;->connect()V

    .line 96
    new-instance v0, Lecq;

    iget-object v2, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->anp:Lecr;

    iget-object v3, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->anm:Lecp;

    sget-object v4, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPb:Lcom/google/android/search/shared/service/ClientConfig;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lecq;-><init>(Landroid/content/Context;Lecr;Lecm;Lcom/google/android/search/shared/service/ClientConfig;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPd:Lecq;

    .line 98
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPd:Lecq;

    invoke-virtual {v0}, Lecq;->connect()V

    .line 99
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->mVss:Lhhq;

    .line 100
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->mVss:Lhhq;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    iput-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->mVoiceSettings:Lhym;

    .line 101
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    iput-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->mAsyncServices:Lema;

    .line 102
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->mGsaConfigFlags:Lchk;

    .line 103
    new-instance v6, Larv;

    iget-object v8, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->mVss:Lhhq;

    iget-object v9, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->mAsyncServices:Lema;

    iget-object v10, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->mGsaConfigFlags:Lchk;

    const-string v11, "benchmark-test"

    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v12

    const/16 v13, 0x3e80

    const/4 v14, 0x1

    move-object v7, p0

    invoke-direct/range {v6 .. v14}, Larv;-><init>(Landroid/content/Context;Lhhq;Lema;Lchk;Ljava/lang/String;Ljava/lang/String;II)V

    iput-object v6, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->anl:Larv;

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->ank:Ljava/util/List;

    .line 110
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->ue()Lcse;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->mVoiceSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Lcse;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 111
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPd:Lecq;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPd:Lecq;

    invoke-virtual {v0}, Lecq;->disconnect()V

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPe:Lecq;

    if-eqz v0, :cond_1

    .line 119
    iget-object v0, p0, Lcom/google/android/hotword/benchmark/service/HotwordBenchmarkServiceImpl;->aPe:Lecq;

    invoke-virtual {v0}, Lecq;->disconnect()V

    .line 121
    :cond_1
    invoke-super {p0}, Lbyq;->onDestroy()V

    .line 122
    return-void
.end method
