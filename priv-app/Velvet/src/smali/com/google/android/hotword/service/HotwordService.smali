.class public Lcom/google/android/hotword/service/HotwordService;
.super Landroid/app/Service;
.source "PG"


# instance fields
.field private final PC:Landroid/os/IBinder;

.field private final aPk:Lcom/google/android/search/shared/service/ClientConfig;

.field public final aPl:Ljava/util/Set;

.field public aPm:Lecq;

.field private anm:Lecp;

.field private final anp:Lecr;

.field public cq:Z

.field public mSettings:Lgdo;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 38
    new-instance v0, Lbzc;

    invoke-direct {v0, p0}, Lbzc;-><init>(Lcom/google/android/hotword/service/HotwordService;)V

    iput-object v0, p0, Lcom/google/android/hotword/service/HotwordService;->PC:Landroid/os/IBinder;

    .line 39
    new-instance v0, Lbzd;

    invoke-direct {v0, p0}, Lbzd;-><init>(Lcom/google/android/hotword/service/HotwordService;)V

    iput-object v0, p0, Lcom/google/android/hotword/service/HotwordService;->anp:Lecr;

    .line 41
    new-instance v0, Lcom/google/android/search/shared/service/ClientConfig;

    const-wide/32 v2, 0x10800001

    const-string v1, "hotwordservice"

    const-string v4, "android-search-app"

    invoke-static {v1, v4}, Lcom/google/android/shared/search/SearchBoxStats;->aA(Ljava/lang/String;Ljava/lang/String;)Lehj;

    move-result-object v1

    invoke-virtual {v1}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/search/shared/service/ClientConfig;-><init>(JLcom/google/android/shared/search/SearchBoxStats;)V

    iput-object v0, p0, Lcom/google/android/hotword/service/HotwordService;->aPk:Lcom/google/android/search/shared/service/ClientConfig;

    .line 50
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/hotword/service/HotwordService;->aPl:Ljava/util/Set;

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/hotword/service/HotwordService;->cq:Z

    .line 166
    return-void
.end method

.method public static Cb()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 190
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.googlequicksearchbox.HOTWORD_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.googlequicksearchbox"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final fQ(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/google/android/hotword/service/HotwordService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    .line 143
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 144
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 145
    return-void

    .line 143
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "doesn\'t belong to the calling UID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/hotword/service/HotwordService;->PC:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    .line 61
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 62
    new-instance v0, Lbzb;

    invoke-direct {v0, p0, p0}, Lbzb;-><init>(Lcom/google/android/hotword/service/HotwordService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/hotword/service/HotwordService;->anm:Lecp;

    .line 63
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJq()Lhhq;

    move-result-object v0

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    iput-object v0, p0, Lcom/google/android/hotword/service/HotwordService;->mSettings:Lgdo;

    .line 64
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    .line 65
    new-instance v0, Lecq;

    iget-object v2, p0, Lcom/google/android/hotword/service/HotwordService;->anp:Lecr;

    iget-object v3, p0, Lcom/google/android/hotword/service/HotwordService;->anm:Lecp;

    iget-object v4, p0, Lcom/google/android/hotword/service/HotwordService;->aPk:Lcom/google/android/search/shared/service/ClientConfig;

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lecq;-><init>(Landroid/content/Context;Lecr;Lecm;Lcom/google/android/search/shared/service/ClientConfig;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/android/hotword/service/HotwordService;->aPm:Lecq;

    .line 67
    iget-object v0, p0, Lcom/google/android/hotword/service/HotwordService;->aPm:Lecq;

    invoke-virtual {v0}, Lecq;->connect()V

    .line 68
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/hotword/service/HotwordService;->aPm:Lecq;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/hotword/service/HotwordService;->aPm:Lecq;

    invoke-virtual {v0}, Lecq;->disconnect()V

    .line 82
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 83
    return-void
.end method
