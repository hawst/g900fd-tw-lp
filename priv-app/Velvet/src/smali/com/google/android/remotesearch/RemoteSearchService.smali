.class public Lcom/google/android/remotesearch/RemoteSearchService;
.super Landroid/app/Service;
.source "PG"

# interfaces
.implements Leoj;


# static fields
.field private static final aPs:Lcom/google/android/shared/search/SearchBoxStats;


# instance fields
.field public aOO:Lecq;

.field private final aPk:Lcom/google/android/search/shared/service/ClientConfig;

.field public aRO:Lhzc;

.field aRT:Lidh;

.field public aRU:Ligi;

.field public aRV:Lefk;

.field public aRW:Lccl;

.field public aRX:Lcom/google/android/search/shared/actions/VoiceAction;

.field private final anp:Lecr;

.field public final mCoreServices:Lcfo;

.field private mGsaConfigFlags:Lchk;

.field private mIntentStarter:Leqp;

.field public mSearchSettings:Lcke;

.field public final mVelvetServices:Lgql;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 73
    const-string v0, "clockwork"

    const-string v1, "com.google.android.search.core.service.SearchService"

    invoke-static {v0, v1}, Lcom/google/android/shared/search/SearchBoxStats;->aA(Ljava/lang/String;Ljava/lang/String;)Lehj;

    move-result-object v0

    invoke-virtual {v0}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v0

    sput-object v0, Lcom/google/android/remotesearch/RemoteSearchService;->aPs:Lcom/google/android/shared/search/SearchBoxStats;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 68
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 77
    new-instance v0, Lcom/google/android/search/shared/service/ClientConfig;

    const-wide/32 v2, 0x75400a0

    sget-object v1, Lcom/google/android/remotesearch/RemoteSearchService;->aPs:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/search/shared/service/ClientConfig;-><init>(JLcom/google/android/shared/search/SearchBoxStats;)V

    iput-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->aPk:Lcom/google/android/search/shared/service/ClientConfig;

    .line 87
    new-instance v0, Lccx;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lccx;-><init>(Lcom/google/android/remotesearch/RemoteSearchService;B)V

    iput-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->anp:Lecr;

    .line 89
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->mVelvetServices:Lgql;

    .line 90
    iget-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->mGsaConfigFlags:Lchk;

    .line 91
    iget-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->mCoreServices:Lcfo;

    .line 95
    new-instance v0, Lccq;

    invoke-direct {v0, p0}, Lccq;-><init>(Lcom/google/android/remotesearch/RemoteSearchService;)V

    invoke-static {v0}, Ligj;->b(Ligi;)Ligi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->aRU:Ligi;

    .line 113
    iget-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->mCoreServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->mSearchSettings:Lcke;

    .line 468
    return-void
.end method

.method private CT()V
    .locals 30

    .prologue
    .line 191
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRO:Lhzc;

    if-nez v2, :cond_0

    .line 192
    new-instance v11, Lccs;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lccs;-><init>(Lcom/google/android/remotesearch/RemoteSearchService;)V

    .line 207
    new-instance v2, Lhzc;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->aJU()Lgpu;

    move-result-object v3

    invoke-virtual {v3}, Lgpu;->aJM()Ldmh;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/remotesearch/RemoteSearchService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, Ldyv;->f(Landroid/content/res/Resources;)Ldyv;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mCoreServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->Em()Lcha;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->aJV()Lcjg;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->aKa()Lciy;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mSearchSettings:Lcke;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mGsaConfigFlags:Lchk;

    invoke-static/range {p0 .. p0}, Lghy;->bG(Landroid/content/Context;)Lghy;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/remotesearch/RemoteSearchService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mCoreServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DX()Lenm;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mCoreServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->Eg()Ligi;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mCoreServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DL()Lcrh;

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    new-instance v21, Licp;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mCoreServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->Es()Libo;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mCoreServices:Lcfo;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcfo;->DL()Lcrh;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v0, v3, v1}, Licp;-><init>(Libo;Lglm;)V

    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->xT()I

    move-result v22

    new-instance v23, Lgru;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mCoreServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DC()Lemp;

    move-result-object v3

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3}, Lgru;-><init>(Landroid/content/Context;Lemp;)V

    new-instance v24, Lgtj;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mCoreServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DC()Lemp;

    move-result-object v3

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3}, Lgtj;-><init>(Landroid/content/Context;Lemp;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mCoreServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DF()Lcin;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->aJZ()Lciu;

    move-result-object v26

    const/16 v27, 0x0

    new-instance v28, Lbye;

    invoke-direct/range {v28 .. v28}, Lbye;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/remotesearch/RemoteSearchService;->mVelvetServices:Lgql;

    invoke-virtual {v3}, Lgql;->aJR()Lcgh;

    move-result-object v3

    invoke-interface {v3}, Lcgh;->ET()Ldgm;

    move-result-object v29

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v29}, Lhzc;-><init>(Landroid/content/Context;Ldmh;Ldyv;Lcha;Lcjg;Lciy;Lcke;Lchk;Ligi;Lghy;Landroid/content/ContentResolver;Lenm;ZLigi;Lcrh;Lbxy;Libv;Lgpc;Licp;ILgru;Lgtj;Lcin;Lciu;Lbwo;Lbye;Ldgm;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRO:Lhzc;

    .line 238
    :cond_0
    return-void
.end method

.method public static synthetic CU()Lcom/google/android/shared/search/SearchBoxStats;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/google/android/remotesearch/RemoteSearchService;->aPs:Lcom/google/android/shared/search/SearchBoxStats;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/remotesearch/RemoteSearchService;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/remotesearch/RemoteSearchService;->CT()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/remotesearch/RemoteSearchService;Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 2

    .prologue
    .line 68
    new-instance v0, Lccu;

    invoke-direct {v0, p0, p1}, Lccu;-><init>(Lcom/google/android/remotesearch/RemoteSearchService;Lcom/google/android/search/shared/actions/VoiceAction;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lccu;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/remotesearch/RemoteSearchService;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 68
    iget-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->aRT:Lidh;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/remotesearch/RemoteSearchService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    :cond_0
    new-instance v1, Lidi;

    iget-object v2, p0, Lcom/google/android/remotesearch/RemoteSearchService;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BL()Lchk;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/remotesearch/RemoteSearchService;->mCoreServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->BK()Lcke;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/remotesearch/RemoteSearchService;->mCoreServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->DL()Lcrh;

    move-result-object v5

    invoke-static {v0}, Ldyv;->f(Landroid/content/res/Resources;)Ldyv;

    move-result-object v0

    invoke-direct {v1, v2, v4, v5, v0}, Lidi;-><init>(Lchk;Lcke;Lcrh;Ldyv;)V

    iput-object v1, p0, Lcom/google/android/remotesearch/RemoteSearchService;->aRT:Lidh;

    :cond_1
    iget-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->aRT:Lidh;

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lidh;->a(Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;Ljku;Ljlf;Ljlm;)Ljkt;

    move-result-object v0

    new-instance v1, Ljyl;

    invoke-direct {v1}, Ljyl;-><init>()V

    invoke-virtual {v1, v6}, Ljyl;->th(I)Ljyl;

    invoke-virtual {v1, v7}, Ljyl;->ti(I)Ljyl;

    new-instance v2, Ljye;

    invoke-direct {v2}, Ljye;-><init>()V

    if-eqz v0, :cond_2

    new-array v3, v7, [Ljkt;

    aput-object v0, v3, v6

    iput-object v3, v2, Ljye;->eBt:[Ljkt;

    :cond_2
    iput-object v2, v1, Ljyl;->eLN:Ljye;

    iget-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->aRW:Lccl;

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-interface {v0, v1}, Lccl;->j([B)V

    return-void
.end method


# virtual methods
.method public final CR()Z
    .locals 1

    .prologue
    .line 501
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/google/android/shared/search/Query;Ljkt;)V
    .locals 2

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/google/android/remotesearch/RemoteSearchService;->CT()V

    .line 161
    new-instance v0, Lccr;

    invoke-direct {v0, p0, p2, p1}, Lccr;-><init>(Lcom/google/android/remotesearch/RemoteSearchService;Ljkt;Lcom/google/android/shared/search/Query;)V

    iput-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->aRV:Lefk;

    .line 187
    iget-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->aRO:Lhzc;

    iget-object v1, p0, Lcom/google/android/remotesearch/RemoteSearchService;->aRV:Lefk;

    invoke-virtual {v0, p2, p1, v1}, Lhzc;->a(Ljkt;Lcom/google/android/shared/search/Query;Lefk;)Z

    .line 188
    return-void
.end method

.method public final a(Landroid/content/Intent;Leol;)Z
    .locals 1

    .prologue
    .line 496
    const/4 v0, 0x0

    return v0
.end method

.method public final varargs b([Landroid/content/Intent;)Z
    .locals 4

    .prologue
    .line 483
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 484
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 483
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 486
    :cond_0
    iget-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->mIntentStarter:Leqp;

    if-nez v0, :cond_1

    new-instance v0, Lenh;

    invoke-direct {v0, p0}, Lenh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->mIntentStarter:Leqp;

    :cond_1
    iget-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->mIntentStarter:Leqp;

    invoke-interface {v0, p1}, Leqp;->b([Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public final varargs c([Landroid/content/Intent;)Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 1

    .prologue
    .line 491
    invoke-virtual {p0}, Lcom/google/android/remotesearch/RemoteSearchService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p1}, Lepd;->a(Landroid/content/pm/PackageManager;[Landroid/content/Intent;)Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 140
    iput-object v5, p0, Lcom/google/android/remotesearch/RemoteSearchService;->aRW:Lccl;

    .line 141
    iput-object v5, p0, Lcom/google/android/remotesearch/RemoteSearchService;->aRV:Lefk;

    .line 142
    new-instance v0, Lecq;

    iget-object v2, p0, Lcom/google/android/remotesearch/RemoteSearchService;->anp:Lecr;

    new-instance v3, Lccy;

    invoke-direct {v3, p0}, Lccy;-><init>(Lcom/google/android/remotesearch/RemoteSearchService;)V

    iget-object v4, p0, Lcom/google/android/remotesearch/RemoteSearchService;->aPk:Lcom/google/android/search/shared/service/ClientConfig;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lecq;-><init>(Landroid/content/Context;Lecr;Lecm;Lcom/google/android/search/shared/service/ClientConfig;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->aOO:Lecq;

    .line 144
    iget-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->aOO:Lecq;

    invoke-virtual {v0}, Lecq;->start()V

    .line 145
    iget-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->aOO:Lecq;

    invoke-virtual {v0}, Lecq;->connect()V

    .line 146
    new-instance v0, Lccv;

    new-instance v1, Ldmf;

    invoke-virtual {p0}, Lcom/google/android/remotesearch/RemoteSearchService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-direct {v1, v2}, Ldmf;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-direct {v0, p0, v1}, Lccv;-><init>(Lcom/google/android/remotesearch/RemoteSearchService;Ldmf;)V

    return-object v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->aRV:Lefk;

    .line 152
    iget-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->aOO:Lecq;

    invoke-virtual {v0}, Lecq;->cancel()V

    .line 153
    iget-object v0, p0, Lcom/google/android/remotesearch/RemoteSearchService;->aOO:Lecq;

    invoke-virtual {v0}, Lecq;->disconnect()V

    .line 154
    const/4 v0, 0x0

    return v0
.end method
