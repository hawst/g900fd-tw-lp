.class public Lcom/google/android/launcher/GEL;
.super Lcom/android/launcher3/Launcher;
.source "PG"

# interfaces
.implements Lepx;


# static fields
.field private static final aPs:Lcom/google/android/shared/search/SearchBoxStats;


# instance fields
.field private aPA:Z

.field private aPB:Landroid/os/Bundle;

.field private aPC:Landroid/os/Bundle;

.field private aPD:Z

.field private aPE:Z

.field private aPF:Z

.field private aPG:Z

.field private aPH:Z

.field private aPI:Z

.field private aPJ:Z

.field public aPK:Z

.field private aPL:Z

.field public aPM:Z

.field private aPN:Z

.field private aPO:Z

.field private aPP:I

.field private aPQ:Lesk;

.field private aPR:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private aPS:Lesk;

.field private final aPt:Lcom/google/android/search/shared/service/ClientConfig;

.field private aPu:Lenz;

.field private aPv:Lcah;

.field public aPw:Ldpx;

.field public aPx:Lfmd;

.field private aPy:Landroid/content/BroadcastReceiver;

.field private aPz:Z

.field private as:Z

.field private mIntentStarter:Lelp;

.field private mNowRemoteClient:Lfml;

.field private mRunner:Lerk;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 89
    const-string v0, "gel"

    const-string v1, "android-search-app"

    invoke-static {v0, v1}, Lcom/google/android/shared/search/SearchBoxStats;->aA(Ljava/lang/String;Ljava/lang/String;)Lehj;

    move-result-object v0

    invoke-virtual {v0}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v0

    sput-object v0, Lcom/google/android/launcher/GEL;->aPs:Lcom/google/android/shared/search/SearchBoxStats;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 79
    invoke-direct {p0}, Lcom/android/launcher3/Launcher;-><init>()V

    .line 100
    new-instance v0, Lcom/google/android/search/shared/service/ClientConfig;

    const-wide v2, 0x198211602L

    sget-object v1, Lcom/google/android/launcher/GEL;->aPs:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/search/shared/service/ClientConfig;-><init>(JLcom/google/android/shared/search/SearchBoxStats;)V

    iput-object v0, p0, Lcom/google/android/launcher/GEL;->aPt:Lcom/google/android/search/shared/service/ClientConfig;

    .line 127
    iput-boolean v4, p0, Lcom/google/android/launcher/GEL;->aPA:Z

    .line 139
    iput-boolean v4, p0, Lcom/google/android/launcher/GEL;->as:Z

    .line 144
    iput-boolean v4, p0, Lcom/google/android/launcher/GEL;->aPD:Z

    .line 146
    iput-boolean v4, p0, Lcom/google/android/launcher/GEL;->aPE:Z

    .line 148
    iput-boolean v4, p0, Lcom/google/android/launcher/GEL;->aPF:Z

    .line 153
    iput-boolean v5, p0, Lcom/google/android/launcher/GEL;->aPH:Z

    .line 160
    iput-boolean v4, p0, Lcom/google/android/launcher/GEL;->aPI:Z

    .line 163
    iput-boolean v4, p0, Lcom/google/android/launcher/GEL;->aPJ:Z

    .line 167
    iput-boolean v4, p0, Lcom/google/android/launcher/GEL;->aPK:Z

    .line 169
    iput-boolean v4, p0, Lcom/google/android/launcher/GEL;->aPL:Z

    .line 171
    iput-boolean v4, p0, Lcom/google/android/launcher/GEL;->aPM:Z

    .line 173
    iput-boolean v4, p0, Lcom/google/android/launcher/GEL;->aPN:Z

    .line 175
    iput-boolean v5, p0, Lcom/google/android/launcher/GEL;->aPO:Z

    .line 177
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/launcher/GEL;->aPP:I

    .line 179
    new-instance v0, Lbzl;

    const-string v1, "Log impression"

    invoke-direct {v0, p0, v1}, Lbzl;-><init>(Lcom/google/android/launcher/GEL;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/launcher/GEL;->aPQ:Lesk;

    .line 187
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/launcher/GEL;->aPR:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 330
    new-instance v0, Lbzq;

    const-string v1, "Wallpaper visibility"

    invoke-direct {v0, p0, v1}, Lbzq;-><init>(Lcom/google/android/launcher/GEL;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/launcher/GEL;->aPS:Lesk;

    .line 1430
    return-void
.end method

.method private Cf()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1011
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPR:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1013
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->mRunner:Lerk;

    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPQ:Lesk;

    invoke-interface {v0, v1}, Lerk;->b(Lesk;)V

    .line 1014
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPR:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1015
    invoke-direct {p0, v2}, Lcom/google/android/launcher/GEL;->eu(I)V

    .line 1019
    :cond_0
    return-void
.end method

.method private Cg()V
    .locals 2

    .prologue
    .line 1032
    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPH:Z

    if-nez v0, :cond_1

    .line 1041
    :cond_0
    :goto_0
    return-void

    .line 1037
    :cond_1
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    if-eqz v0, :cond_0

    .line 1038
    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->as:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPF:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPE:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v1, v0}, Ldpx;->dc(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private Cj()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1245
    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/launcher/GEL;->aPI:Z

    if-eqz v1, :cond_0

    .line 1248
    iput-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPJ:Z

    .line 1254
    :goto_0
    return-void

    .line 1251
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/launcher/GEL;->aPG:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->hI()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1252
    :goto_1
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->hH()V

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v1, v0}, Lcom/android/launcher3/Workspace;->aw(Z)V

    goto :goto_0

    .line 1251
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private Ck()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1261
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPu:Lenz;

    invoke-virtual {v0}, Lenz;->acJ()V

    .line 1262
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPu:Lenz;

    const-string v1, "GEL.GSAPrefs.now_enabled"

    invoke-virtual {v0, v1, v2}, Lenz;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1265
    iget-boolean v1, p0, Lcom/google/android/launcher/GEL;->aPI:Z

    if-eq v0, v1, :cond_0

    .line 1266
    iput-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPI:Z

    .line 1267
    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->he()V

    .line 1268
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    if-eqz v0, :cond_0

    .line 1270
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    invoke-virtual {v0, v2}, Lfmd;->dJ(Z)V

    .line 1271
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    .line 1274
    :cond_0
    return-void
.end method

.method private Cl()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1283
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPu:Lenz;

    invoke-virtual {v0}, Lenz;->acJ()V

    .line 1284
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPu:Lenz;

    const-string v1, "GSAPrefs.hotword_enabled"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lenz;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1287
    iget-boolean v1, p0, Lcom/google/android/launcher/GEL;->aPH:Z

    if-eq v0, v1, :cond_0

    .line 1288
    iput-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPH:Z

    .line 1289
    invoke-direct {p0}, Lcom/google/android/launcher/GEL;->Cg()V

    .line 1291
    :cond_0
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPu:Lenz;

    const-string v1, "GEL.GSAPrefs.log_gel_events"

    invoke-virtual {v0, v1, v5}, Lenz;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1293
    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPu:Lenz;

    const-string v2, "GEL.GSAPrefs.gel_tag"

    const-string v3, "GEL"

    invoke-virtual {v1, v2, v3}, Lenz;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1296
    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/launcher/GEL;->mRunner:Lerk;

    iget-object v4, p0, Lcom/google/android/launcher/GEL;->aPv:Lcah;

    invoke-static {v2, v3, v0, v1, v4}, Lcad;->a(Landroid/content/Context;Lern;ZLjava/lang/String;Lcah;)Z

    move-result v0

    .line 1298
    if-eqz v0, :cond_1

    .line 1300
    invoke-direct {p0, v5}, Lcom/google/android/launcher/GEL;->eu(I)V

    .line 1302
    :cond_1
    return-void
.end method

.method public static synthetic a(Lcom/google/android/launcher/GEL;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPR:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method private a(IILandroid/view/View;)V
    .locals 0

    .prologue
    .line 998
    invoke-direct {p0}, Lcom/google/android/launcher/GEL;->Cf()V

    .line 999
    if-nez p3, :cond_0

    .line 1000
    invoke-static {p1, p2}, Lcad;->af(II)V

    .line 1004
    :goto_0
    return-void

    .line 1002
    :cond_0
    invoke-static {p1, p3, p2}, Lcad;->a(ILandroid/view/View;I)V

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;Z)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1099
    const-string v0, "GEL"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "handleIntent("

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ")"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v7, v0, v1, v5}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1104
    const-string v0, "ORIGINAL_ACTION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1105
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ORIGINAL_ACTION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1111
    :goto_0
    const-string v1, "app_data"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 1112
    if-eqz v1, :cond_4

    const-string v5, "source"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1114
    :goto_1
    if-nez v1, :cond_0

    .line 1115
    const-string v1, "android-search-app"

    .line 1118
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v5

    const-string v6, "android.intent.category.HOME"

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "android.intent.action.MAIN"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1122
    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c00d3

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    invoke-direct {p0, v7, v5, v4}, Lcom/google/android/launcher/GEL;->a(IILandroid/view/View;)V

    .line 1127
    :cond_1
    const-string v5, "com.google.android.googlequicksearchbox.GOOGLE_ICON"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-boolean v5, p0, Lcom/google/android/launcher/GEL;->aPI:Z

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/google/android/launcher/GEL;->aPu:Lenz;

    const-string v6, "GSAPrefs.first_run_screens_shown"

    invoke-virtual {v5, v6, v3}, Lenz;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_5

    .line 1133
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1134
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.google.android.velvet.tg.FirstRunActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1136
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1138
    invoke-virtual {p0, v0}, Lcom/google/android/launcher/GEL;->startActivity(Landroid/content/Intent;)V

    .line 1180
    :cond_2
    :goto_2
    return-void

    .line 1107
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    move-object v1, v4

    .line 1112
    goto :goto_1

    .line 1142
    :cond_5
    const-string v5, "android.intent.action.ASSIST"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1144
    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0145

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    invoke-direct {p0, v5, v6, v4}, Lcom/google/android/launcher/GEL;->a(IILandroid/view/View;)V

    .line 1148
    :cond_6
    const-string v4, "android.intent.action.ASSIST"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    const-string v4, "com.google.android.googlequicksearchbox.GOOGLE_ICON"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1150
    :cond_7
    if-eqz p2, :cond_8

    invoke-virtual {p0, v3}, Lcom/google/android/launcher/GEL;->X(Z)V

    .line 1152
    :cond_8
    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPI:Z

    if-eqz v0, :cond_a

    .line 1156
    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPD:Z

    if-eqz v0, :cond_9

    .line 1157
    iput-boolean v2, p0, Lcom/google/android/launcher/GEL;->aPK:Z

    .line 1158
    iput-boolean v3, p0, Lcom/google/android/launcher/GEL;->aPD:Z

    .line 1160
    :cond_9
    iput-boolean v2, p0, Lcom/google/android/launcher/GEL;->aPM:Z

    .line 1161
    invoke-direct {p0, p1}, Lcom/google/android/launcher/GEL;->p(Landroid/content/Intent;)V

    .line 1162
    invoke-direct {p0}, Lcom/google/android/launcher/GEL;->Cj()V

    .line 1163
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    invoke-interface {v0, v2}, Ldpx;->bW(Z)Z

    goto :goto_2

    .line 1165
    :cond_a
    iget-object v4, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    sget-object v5, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPD:Z

    if-nez v0, :cond_b

    move v0, v2

    :goto_3
    invoke-interface {v4, v5, v0, v1}, Ldpx;->a(Lcom/google/android/shared/search/Query;ZLjava/lang/String;)V

    goto :goto_2

    :cond_b
    move v0, v3

    goto :goto_3

    .line 1167
    :cond_c
    const-string v4, "android.search.action.GLOBAL_SEARCH"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_d

    const-string v4, "FIRST_RUN"

    const-string v5, "source"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 1170
    :cond_d
    if-eqz p2, :cond_e

    invoke-virtual {p0, v3}, Lcom/google/android/launcher/GEL;->X(Z)V

    .line 1171
    :cond_e
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    sget-object v4, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iget-boolean v5, p0, Lcom/google/android/launcher/GEL;->aPD:Z

    if-nez v5, :cond_f

    :goto_4
    invoke-interface {v0, v4, v2, v1}, Ldpx;->a(Lcom/google/android/shared/search/Query;ZLjava/lang/String;)V

    goto/16 :goto_2

    :cond_f
    move v2, v3

    goto :goto_4

    .line 1172
    :cond_10
    const-string v2, "android.speech.action.WEB_SEARCH"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    const-string v2, "android.intent.action.SEARCH_LONG_PRESS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    const-string v2, "android.intent.action.VOICE_ASSIST"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1175
    :cond_11
    if-eqz p2, :cond_12

    invoke-virtual {p0, v3}, Lcom/google/android/launcher/GEL;->X(Z)V

    .line 1176
    :cond_12
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    invoke-interface {v0, v1}, Ldpx;->ke(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public static synthetic a(Lcom/google/android/launcher/GEL;Z)Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPz:Z

    return v0
.end method

.method public static synthetic b(Lcom/google/android/launcher/GEL;)Lerk;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->mRunner:Lerk;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/launcher/GEL;Z)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/google/android/launcher/GEL;->bY(Z)V

    return-void
.end method

.method private bW(Z)Z
    .locals 1

    .prologue
    .line 848
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    if-eqz v0, :cond_0

    .line 849
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    invoke-interface {v0, p1}, Ldpx;->bW(Z)Z

    move-result v0

    .line 851
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bY(Z)V
    .locals 0

    .prologue
    .line 1354
    iput-boolean p1, p0, Lcom/google/android/launcher/GEL;->aPO:Z

    .line 1355
    invoke-virtual {p0, p1}, Lcom/google/android/launcher/GEL;->W(Z)V

    .line 1356
    return-void
.end method

.method public static synthetic c(Lcom/google/android/launcher/GEL;)Lenz;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPu:Lenz;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/launcher/GEL;)Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPz:Z

    return v0
.end method

.method public static synthetic e(Lcom/google/android/launcher/GEL;)Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPO:Z

    return v0
.end method

.method private eu(I)V
    .locals 4

    .prologue
    .line 968
    invoke-static {}, Lcad;->Cw()Z

    move-result v0

    if-nez v0, :cond_1

    .line 989
    :cond_0
    :goto_0
    return-void

    .line 972
    :cond_1
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->mRunner:Lerk;

    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPQ:Lesk;

    invoke-interface {v0, v1}, Lerk;->b(Lesk;)V

    .line 973
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPR:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 974
    sget-object v0, Lcad;->aPW:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 975
    if-lez p1, :cond_2

    .line 980
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->mRunner:Lerk;

    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPQ:Lesk;

    int-to-long v2, p1

    invoke-interface {v0, v1, v2, v3}, Lerk;->a(Lesk;J)V

    .line 981
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPR:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 986
    :cond_2
    const v0, 0x7f11023c

    invoke-virtual {p0, v0}, Lcom/google/android/launcher/GEL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->Kc:Landroid/view/View;

    invoke-static {v0, v1}, Lcad;->b(Landroid/view/View;Landroid/view/View;)V

    goto :goto_0
.end method

.method private p(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 652
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    iget-boolean v0, v0, Lfmd;->Kl:Z

    if-eqz v0, :cond_0

    .line 653
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    iget-boolean v1, p0, Lcom/google/android/launcher/GEL;->aPL:Z

    invoke-virtual {v0, v1, p1}, Lfmd;->a(ZLandroid/content/Intent;)V

    .line 660
    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->hP()I

    move-result v0

    if-nez v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {v1, v0}, Lfmd;->D(F)V

    .line 662
    :cond_0
    return-void

    .line 660
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final Ch()V
    .locals 1

    .prologue
    .line 1053
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/launcher/GEL;->W(Z)V

    .line 1054
    return-void
.end method

.method public final Ci()V
    .locals 0

    .prologue
    .line 1237
    return-void
.end method

.method protected final R(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 889
    invoke-super {p0, p1}, Lcom/android/launcher3/Launcher;->R(Landroid/view/View;)V

    .line 891
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00d4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/launcher/GEL;->a(IILandroid/view/View;)V

    .line 892
    return-void
.end method

.method protected final T(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 896
    invoke-super {p0, p1}, Lcom/android/launcher3/Launcher;->T(Landroid/view/View;)V

    .line 898
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c010b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/launcher/GEL;->a(IILandroid/view/View;)V

    .line 899
    return-void
.end method

.method protected final U(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 865
    invoke-super {p0, p1}, Lcom/android/launcher3/Launcher;->U(Landroid/view/View;)V

    .line 868
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0167

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/launcher/GEL;->a(IILandroid/view/View;)V

    .line 869
    return-void
.end method

.method protected final V(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 873
    invoke-super {p0, p1}, Lcom/android/launcher3/Launcher;->V(Landroid/view/View;)V

    .line 876
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0165

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/launcher/GEL;->a(IILandroid/view/View;)V

    .line 877
    return-void
.end method

.method protected final W(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 484
    invoke-super {p0, p1}, Lcom/android/launcher3/Launcher;->W(Landroid/view/View;)V

    .line 486
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0155

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/launcher/GEL;->a(IILandroid/view/View;)V

    .line 487
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 488
    const-string v1, "com.google.android.googlequicksearchbox"

    const-string v2, "com.google.android.velvet.ui.settings.SettingsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 490
    invoke-virtual {p0, v0}, Lcom/google/android/launcher/GEL;->startActivity(Landroid/content/Intent;)V

    .line 491
    return-void
.end method

.method public final W(Z)V
    .locals 2

    .prologue
    .line 1306
    if-eqz p1, :cond_0

    .line 1308
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->mRunner:Lerk;

    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPS:Lesk;

    invoke-interface {v0, v1}, Lerk;->b(Lesk;)V

    .line 1310
    :cond_0
    invoke-super {p0, p1}, Lcom/android/launcher3/Launcher;->W(Z)V

    .line 1311
    return-void
.end method

.method public final X(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 937
    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->hP()I

    move-result v0

    if-nez v0, :cond_0

    .line 940
    iget-object v0, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Workspace;->av(Z)V

    .line 942
    :cond_0
    invoke-direct {p0}, Lcom/google/android/launcher/GEL;->Cf()V

    .line 943
    const/4 v0, -0x1

    invoke-static {p1, v0}, Lcad;->l(Landroid/view/View;I)V

    .line 944
    return-void
.end method

.method protected final Z(Z)Z
    .locals 1

    .prologue
    .line 461
    const/4 v0, 0x1

    return v0
.end method

.method protected final a(Landroid/graphics/drawable/Drawable$ConstantState;)V
    .locals 0

    .prologue
    .line 456
    return-void
.end method

.method public final a(Ljava/lang/String;ZLandroid/os/Bundle;Landroid/graphics/Rect;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 701
    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    if-eqz v1, :cond_0

    .line 702
    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, p1}, Lcom/google/android/shared/search/Query;->x(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    .line 703
    iget-object v2, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    const-string v3, "android-launcher-search"

    invoke-interface {v2, v1, v0, v3}, Ldpx;->a(Lcom/google/android/shared/search/Query;ZLjava/lang/String;)V

    .line 706
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aa(Z)V
    .locals 1

    .prologue
    .line 722
    const/4 v0, 0x1

    invoke-super {p0, v0}, Lcom/android/launcher3/Launcher;->aa(Z)V

    .line 723
    return-void
.end method

.method public final ac(Z)V
    .locals 2

    .prologue
    .line 919
    invoke-super {p0, p1}, Lcom/android/launcher3/Launcher;->ac(Z)V

    .line 923
    sget-object v0, Lcad;->aPW:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 925
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/launcher/GEL;->eu(I)V

    .line 926
    return-void
.end method

.method protected final b(Landroid/graphics/drawable/Drawable$ConstantState;)V
    .locals 0

    .prologue
    .line 467
    return-void
.end method

.method public final bX(Z)V
    .locals 2

    .prologue
    .line 1324
    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPA:Z

    if-ne v0, p1, :cond_0

    .line 1348
    :goto_0
    return-void

    .line 1327
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/launcher/GEL;->aPA:Z

    .line 1329
    if-eqz p1, :cond_1

    .line 1335
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    invoke-interface {v1}, Ldpx;->aeq()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfmd;->bo(Landroid/view/View;)V

    .line 1336
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    invoke-interface {v0}, Ldpx;->aeq()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1338
    :cond_1
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    invoke-interface {v0}, Ldpx;->aeq()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1341
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->mRunner:Lerk;

    new-instance v1, Lbzw;

    invoke-direct {v1, p0}, Lbzw;-><init>(Lcom/google/android/launcher/GEL;)V

    invoke-interface {v0, v1}, Lerk;->a(Lesk;)V

    goto :goto_0
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1315
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/launcher3/Launcher;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 1316
    new-instance v0, Letj;

    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Letj;-><init>(Landroid/content/res/Resources;)V

    .line 1317
    const-string v1, "GEL"

    invoke-virtual {v0, v1}, Letj;->lt(Ljava/lang/String;)V

    .line 1318
    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    invoke-virtual {v0, v1}, Letj;->b(Leti;)V

    .line 1319
    invoke-virtual {v0, p3, p1}, Letj;->a(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1320
    return-void
.end method

.method protected final hA()V
    .locals 2

    .prologue
    .line 951
    invoke-super {p0}, Lcom/android/launcher3/Launcher;->hA()V

    .line 952
    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->hy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 953
    sget-object v0, Lcad;->aPW:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 959
    :goto_0
    return-void

    .line 955
    :cond_0
    sget-object v0, Lcad;->aPW:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0
.end method

.method protected final hC()Landroid/content/ComponentName;
    .locals 2

    .prologue
    .line 727
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/google/android/launcher/GelWallpaperPickerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public final hD()V
    .locals 2

    .prologue
    .line 712
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    if-eqz v0, :cond_0

    .line 713
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    const-string v1, "android-search-app"

    invoke-interface {v0, v1}, Ldpx;->ke(Ljava/lang/String;)V

    .line 715
    :cond_0
    return-void
.end method

.method protected final hF()V
    .locals 1

    .prologue
    .line 677
    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPE:Z

    if-eqz v0, :cond_0

    .line 679
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPE:Z

    .line 680
    invoke-direct {p0}, Lcom/google/android/launcher/GEL;->Cg()V

    .line 682
    :cond_0
    return-void
.end method

.method protected final hG()V
    .locals 1

    .prologue
    .line 667
    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPE:Z

    if-nez v0, :cond_0

    .line 669
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPE:Z

    .line 670
    invoke-direct {p0}, Lcom/google/android/launcher/GEL;->Cg()V

    .line 672
    :cond_0
    return-void
.end method

.method public final hM()Landroid/view/View;
    .locals 10

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    if-nez v0, :cond_0

    .line 430
    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/shared/search/RestrictedProfileBroadcastReceiver;->ap(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 431
    new-instance v0, Ldpz;

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    iget-object v2, p0, Lcom/google/android/launcher/GEL;->mRunner:Lerk;

    iget-object v3, p0, Lcom/google/android/launcher/GEL;->mIntentStarter:Lelp;

    new-instance v4, Ldqf;

    iget-object v5, p0, Lcom/google/android/launcher/GEL;->mNowRemoteClient:Lfml;

    iget-object v6, p0, Lcom/google/android/launcher/GEL;->aPu:Lenz;

    invoke-direct {v4, v5, v6}, Ldqf;-><init>(Lfml;Lenz;)V

    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v5

    iget-object v5, v5, Lyu;->Mk:Lur;

    invoke-virtual {v5}, Lur;->gl()Ltu;

    move-result-object v5

    invoke-virtual {v5}, Ltu;->fC()Z

    move-result v5

    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v6

    iget-object v6, v6, Lyu;->Mk:Lur;

    invoke-virtual {v6}, Lur;->gl()Ltu;

    move-result-object v7

    iget-boolean v6, v7, Ltu;->Dp:Z

    if-eqz v6, :cond_1

    const/4 v6, 0x0

    :goto_0
    invoke-virtual {v7, v6}, Ltu;->aZ(I)Landroid/graphics/Rect;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/launcher/GEL;->aPC:Landroid/os/Bundle;

    iget-object v8, p0, Lcom/google/android/launcher/GEL;->aPt:Lcom/google/android/search/shared/service/ClientConfig;

    const-string v9, "gel"

    invoke-direct/range {v0 .. v9}, Ldpz;-><init>(Landroid/widget/FrameLayout;Lerp;Leqp;Leax;ZLandroid/graphics/Rect;Landroid/os/Bundle;Lcom/google/android/search/shared/service/ClientConfig;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    .line 441
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/launcher/GEL;->aPC:Landroid/os/Bundle;

    .line 443
    :cond_0
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->hasWindowFocus()Z

    move-result v1

    invoke-interface {v0, v1}, Ldpx;->onWindowFocusChanged(Z)V

    .line 444
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    invoke-interface {v0}, Ldpx;->aeq()Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 431
    :cond_1
    const/4 v6, 0x1

    goto :goto_0

    .line 438
    :cond_2
    new-instance v0, Ldpt;

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->xu:Lcom/android/launcher3/DragLayer;

    invoke-direct {v0, v1}, Ldpt;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    goto :goto_1
.end method

.method protected final hN()Z
    .locals 1

    .prologue
    .line 450
    const/4 v0, 0x1

    return v0
.end method

.method protected final hU()Z
    .locals 1

    .prologue
    .line 732
    invoke-static {p0}, Lesp;->ay(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method protected final hV()Z
    .locals 1

    .prologue
    .line 785
    invoke-static {p0}, Lesp;->ay(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method protected final hW()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 738
    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPI:Z

    if-nez v0, :cond_2

    invoke-static {p0}, Lesp;->ay(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 743
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-static {v3}, Lacv;->a(Landroid/content/pm/PackageManager;)Lacv;

    move-result-object v3

    .line 744
    if-eqz v3, :cond_4

    .line 745
    iget-boolean v4, p0, Lcom/google/android/launcher/GEL;->aPI:Z

    if-nez v4, :cond_3

    invoke-static {p0}, Lesp;->ay(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3}, Lacv;->jU()Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v1

    .line 749
    :goto_1
    if-nez v0, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 738
    goto :goto_0

    :cond_3
    move v3, v2

    .line 745
    goto :goto_1

    :cond_4
    move v3, v2

    goto :goto_1
.end method

.method protected final hX()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 756
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 757
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.google.android.velvet.tg.FirstRunActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 759
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 761
    const-string v1, "gel_onboard_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 762
    return-object v0
.end method

.method protected final hZ()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 772
    invoke-static {p0}, Lesp;->ay(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/launcher3/Launcher;->KH:Landroid/content/SharedPreferences;

    const-string v2, "launcher.first_run_activity_displayed"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 774
    :cond_0
    return v0
.end method

.method public final hb()V
    .locals 1

    .prologue
    .line 930
    invoke-super {p0}, Lcom/android/launcher3/Launcher;->hb()V

    .line 932
    const/16 v0, 0x3e8

    invoke-direct {p0, v0}, Lcom/google/android/launcher/GEL;->eu(I)V

    .line 933
    return-void
.end method

.method public final hc()Z
    .locals 1

    .prologue
    .line 500
    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPI:Z

    return v0
.end method

.method protected final hd()V
    .locals 15

    .prologue
    .line 508
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    if-eqz v0, :cond_1

    .line 510
    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0585

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 511
    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    iget-object v1, v1, Lfmd;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    new-instance v2, Lbzy;

    invoke-direct {v2, p0}, Lbzy;-><init>(Lcom/google/android/launcher/GEL;)V

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/launcher/GEL;->a(Landroid/view/View;Lym;Ljava/lang/String;)Lyp;

    .line 626
    :cond_0
    :goto_0
    return-void

    .line 516
    :cond_1
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPu:Lenz;

    const-string v1, "GSAPrefs.debug_features_level"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lenz;->x(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/launcher/GEL;->aPP:I

    .line 519
    new-instance v9, Lfzw;

    iget-object v0, p0, Lcom/google/android/launcher/GEL;->mRunner:Lerk;

    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPu:Lenz;

    const-string v2, "GSAPrefs.redirect_mfe_requests"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lenz;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/launcher/GEL;->aPu:Lenz;

    const-string v3, "GSAPrefs.redirect_mfe_to_gmm"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lenz;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-direct {v9, v0, v1, v2}, Lfzw;-><init>(Lerk;ZZ)V

    .line 522
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPu:Lenz;

    const-string v1, "quantum_now_cards"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lenz;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    .line 524
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPu:Lenz;

    const-string v1, "traditional_view_time_recording"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lenz;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    .line 526
    new-instance v12, Lbzr;

    invoke-direct {v12, p0}, Lbzr;-><init>(Lcom/google/android/launcher/GEL;)V

    .line 532
    iget-object v1, p0, Lcom/google/android/launcher/GEL;->mNowRemoteClient:Lfml;

    iget-object v6, p0, Lcom/google/android/launcher/GEL;->mRunner:Lerk;

    iget-object v7, p0, Lcom/google/android/launcher/GEL;->mIntentStarter:Lelp;

    new-instance v8, Lbzx;

    invoke-direct {v8, p0}, Lbzx;-><init>(Lcom/google/android/launcher/GEL;)V

    new-instance v13, Lbzz;

    invoke-direct {v13, p0}, Lbzz;-><init>(Lcom/google/android/launcher/GEL;)V

    const-string v14, "now_gel"

    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v2, 0x103006e

    invoke-direct {v0, p0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0400db

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    const v0, 0x7f110296

    invoke-virtual {v2, v0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lflw;

    new-instance v3, Lfnx;

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->aCL()Lfqn;

    move-result-object v0

    new-instance v4, Lfoo;

    invoke-direct {v4, v1}, Lfoo;-><init>(Lfml;)V

    invoke-direct {v3, p0, v0, v2, v4}, Lfnx;-><init>(Landroid/app/Activity;Lfom;Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;Lfon;)V

    const/4 v4, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v14}, Lfmd;->a(Landroid/app/Activity;Lfml;Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;Lfnx;Landroid/view/View;Lflw;Lerk;Leoj;Ligi;Lfzw;ZZLigi;Leko;Ljava/lang/String;)Lfmd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    .line 535
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPB:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lfmd;->onPostCreate(Landroid/os/Bundle;)V

    .line 536
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/launcher/GEL;->aPB:Landroid/os/Bundle;

    .line 537
    iget v0, p0, Lcom/google/android/launcher/GEL;->aPP:I

    invoke-static {v0}, Ledw;->hm(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    const v1, 0x7f0a04ab

    invoke-virtual {p0, v1}, Lcom/google/android/launcher/GEL;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lbzm;

    invoke-direct {v2, p0}, Lbzm;-><init>(Lcom/google/android/launcher/GEL;)V

    invoke-virtual {v0, v1, v2}, Lfmd;->a(Ljava/lang/String;Lfok;)V

    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    const-string v1, "Share launcher.db"

    new-instance v2, Lbzn;

    invoke-direct {v2, p0}, Lbzn;-><init>(Lcom/google/android/launcher/GEL;)V

    invoke-virtual {v0, v1, v2}, Lfmd;->a(Ljava/lang/String;Lfok;)V

    .line 540
    :cond_2
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    iget-object v0, v0, Lfmd;->cvI:Lflw;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lflw;->w(ZZ)V

    .line 542
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    iget-boolean v1, p0, Lcom/google/android/launcher/GEL;->aPL:Z

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lfmd;->a(ZLandroid/content/Intent;)V

    .line 543
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lfmd;->D(F)V

    .line 546
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    iget-object v0, v0, Lfmd;->cvK:Lfnx;

    invoke-virtual {v0}, Lfnx;->aBz()V

    .line 548
    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0585

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 550
    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    iget-object v1, v1, Lfmd;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    new-instance v2, Lbzy;

    invoke-direct {v2, p0}, Lbzy;-><init>(Lcom/google/android/launcher/GEL;)V

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/launcher/GEL;->a(Landroid/view/View;Lym;Ljava/lang/String;)Lyp;

    .line 553
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    new-instance v1, Lbzs;

    invoke-direct {v1, p0}, Lbzs;-><init>(Lcom/google/android/launcher/GEL;)V

    iget-object v2, v0, Lfmd;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    if-eqz v2, :cond_3

    iget-object v0, v0, Lfmd;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->a(Lejr;)V

    .line 573
    :cond_3
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    new-instance v1, Lbzt;

    invoke-direct {v1, p0}, Lbzt;-><init>(Lcom/google/android/launcher/GEL;)V

    iget-object v0, v0, Lfmd;->cvI:Lflw;

    invoke-virtual {v0}, Lflw;->wj()Lekf;

    move-result-object v0

    invoke-interface {v0, v1}, Lekf;->a(Lekg;)V

    .line 611
    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->hP()I

    move-result v0

    if-nez v0, :cond_5

    .line 614
    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPL:Z

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lfmd;->x(ZZ)V

    .line 615
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    if-eqz v0, :cond_4

    .line 616
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, Ldpx;->D(F)V

    .line 618
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPK:Z

    .line 619
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/launcher/GEL;->bY(Z)V

    .line 622
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPJ:Z

    if-eqz v0, :cond_0

    .line 623
    invoke-direct {p0}, Lcom/google/android/launcher/GEL;->Cj()V

    .line 624
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPJ:Z

    goto/16 :goto_0

    .line 614
    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final hi()V
    .locals 0

    .prologue
    .line 496
    return-void
.end method

.method protected final hj()Z
    .locals 1

    .prologue
    .line 479
    const/4 v0, 0x1

    return v0
.end method

.method protected final hv()Z
    .locals 1

    .prologue
    .line 630
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    invoke-interface {v0}, Ldpx;->aeu()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final hw()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 635
    invoke-super {p0}, Lcom/android/launcher3/Launcher;->hw()V

    .line 636
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    invoke-interface {v0, v2}, Ldpx;->bW(Z)Z

    .line 637
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    if-eqz v0, :cond_1

    .line 638
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    iget-object v0, v0, Lfmd;->cvK:Lfnx;

    invoke-virtual {v0}, Lfnx;->aBI()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 639
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPz:Z

    .line 640
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    iget-object v0, v0, Lfmd;->cvK:Lfnx;

    invoke-virtual {v0}, Lfnx;->aBH()V

    .line 642
    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/launcher/GEL;->bX(Z)V

    .line 644
    :cond_1
    return-void
.end method

.method public final i(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 857
    invoke-super {p0, p1, p2}, Lcom/android/launcher3/Launcher;->i(Landroid/view/View;I)V

    .line 859
    invoke-direct {p0}, Lcom/google/android/launcher/GEL;->Cf()V

    .line 860
    invoke-static {p1, p2}, Lcad;->m(Landroid/view/View;I)V

    .line 861
    return-void
.end method

.method protected final ia()Landroid/view/View;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 790
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040088

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f1101d5

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcaa;

    invoke-direct {v1, p0}, Lcaa;-><init>(Lcom/android/launcher3/Launcher;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a02d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f1101d4

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "com.google.android.googlequicksearchbox.VIEW_PRIVACY_POLICY"

    aput-object v5, v4, v2

    const/4 v5, 0x1

    const-string v6, "com.google.android.googlequicksearchbox.VIEW_TERMS_OF_SERVICE"

    aput-object v6, v4, v5

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Landroid/text/Spannable;

    invoke-virtual {v0}, Landroid/widget/TextView;->getUrls()[Landroid/text/style/URLSpan;

    move-result-object v4

    array-length v5, v4

    move v0, v2

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v2, v4, v0

    invoke-interface {v1, v2}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-interface {v1, v2}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    invoke-virtual {v2}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, v2}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    new-instance v2, Lcom/google/android/launcher/GELConsentScreenFactory$IntentSpan;

    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, p0, v9}, Lcom/google/android/launcher/GELConsentScreenFactory$IntentSpan;-><init>(Lcom/android/launcher3/Launcher;Landroid/content/Intent;)V

    const/16 v8, 0x22

    invoke-interface {v1, v2, v6, v7, v8}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method public final k(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 903
    invoke-super {p0, p1}, Lcom/android/launcher3/Launcher;->k(Ljava/util/ArrayList;)V

    .line 905
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {v0}, Lcad;->ev(I)V

    .line 906
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 471
    invoke-super {p0, p1, p2, p3}, Lcom/android/launcher3/Launcher;->onActivityResult(IILandroid/content/Intent;)V

    .line 472
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->mIntentStarter:Lelp;

    if-eqz v0, :cond_0

    .line 473
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->mIntentStarter:Lelp;

    invoke-virtual {v0, p1, p2, p3}, Lelp;->a(IILandroid/content/Intent;)V

    .line 475
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 689
    invoke-direct {p0, v0}, Lcom/google/android/launcher/GEL;->bW(Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 695
    :cond_0
    :goto_0
    return-void

    .line 691
    :cond_1
    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/launcher/GEL;->aPI:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    iget-object v2, v1, Lfmd;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    if-eqz v2, :cond_3

    iget-object v2, v1, Lfmd;->cvJ:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->cN()Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_1
    if-nez v0, :cond_0

    .line 693
    :cond_2
    invoke-super {p0}, Lcom/android/launcher3/Launcher;->onBackPressed()V

    goto :goto_0

    .line 691
    :cond_3
    iget-object v0, v1, Lfmd;->cvI:Lflw;

    invoke-virtual {v0}, Lflw;->cN()Z

    move-result v0

    goto :goto_1
.end method

.method protected final onClickAllAppsButton(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 910
    invoke-super {p0, p1}, Lcom/android/launcher3/Launcher;->onClickAllAppsButton(Landroid/view/View;)V

    .line 912
    invoke-direct {p0}, Lcom/google/android/launcher/GEL;->Cf()V

    .line 913
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00d1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-static {v0, p1, v1}, Lcad;->b(ILandroid/view/View;I)V

    .line 915
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 195
    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcab;->af(Landroid/content/Context;)Lcab;

    move-result-object v0

    .line 196
    iget-object v2, v0, Lcab;->mTaskRunner:Lerk;

    iput-object v2, p0, Lcom/google/android/launcher/GEL;->mRunner:Lerk;

    .line 197
    new-instance v2, Lelp;

    const/16 v3, 0x64

    invoke-direct {v2, p0, v3}, Lelp;-><init>(Landroid/app/Activity;I)V

    iput-object v2, p0, Lcom/google/android/launcher/GEL;->mIntentStarter:Lelp;

    .line 198
    iget-object v2, v0, Lcab;->mNowRemoteClient:Lfml;

    iput-object v2, p0, Lcom/google/android/launcher/GEL;->mNowRemoteClient:Lfml;

    .line 199
    iget-object v0, v0, Lcab;->aPu:Lenz;

    iput-object v0, p0, Lcom/google/android/launcher/GEL;->aPu:Lenz;

    .line 201
    if-eqz p1, :cond_0

    const-string v0, "gel:changing_configurations"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPL:Z

    .line 204
    iput-object p1, p0, Lcom/google/android/launcher/GEL;->aPC:Landroid/os/Bundle;

    .line 206
    invoke-direct {p0}, Lcom/google/android/launcher/GEL;->Ck()V

    .line 207
    invoke-super {p0, p1}, Lcom/android/launcher3/Launcher;->onCreate(Landroid/os/Bundle;)V

    .line 210
    new-instance v0, Lcah;

    iget-object v2, p0, Lcom/android/launcher3/Launcher;->KH:Landroid/content/SharedPreferences;

    invoke-direct {v0, v2}, Lcah;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/launcher/GEL;->aPv:Lcah;

    .line 211
    invoke-direct {p0}, Lcom/google/android/launcher/GEL;->Cl()V

    .line 212
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    new-instance v2, Lbzu;

    invoke-direct {v2, p0}, Lbzu;-><init>(Lcom/google/android/launcher/GEL;)V

    invoke-interface {v0, v2}, Ldpx;->a(Lebj;)V

    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    new-instance v2, Lbzv;

    invoke-direct {v2, p0}, Lbzv;-><init>(Lcom/google/android/launcher/GEL;)V

    invoke-interface {v0, v2}, Ldpx;->a(Ldpy;)V

    .line 213
    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 214
    invoke-direct {p0, v0, v1}, Lcom/google/android/launcher/GEL;->a(Landroid/content/Intent;Z)V

    .line 215
    sget-object v0, Lcad;->aPW:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 219
    new-instance v0, Lbzo;

    invoke-direct {v0, p0}, Lbzo;-><init>(Lcom/google/android/launcher/GEL;)V

    iput-object v0, p0, Lcom/google/android/launcher/GEL;->aPy:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.googlequicksearchbox.action.ACTION_FINISH_GEL_ACTIVITY"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPy:Landroid/content/BroadcastReceiver;

    const-string v2, "com.google.android.googlequicksearchbox.permission.FINISH_GEL_ACTIVITY"

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/launcher/GEL;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 220
    return-void

    :cond_0
    move v0, v1

    .line 201
    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->isChangingConfigurations()Z

    move-result v1

    invoke-virtual {v0, v1}, Lfmd;->dJ(Z)V

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    if-eqz v0, :cond_1

    .line 416
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->isChangingConfigurations()Z

    move-result v1

    invoke-interface {v0, v1}, Ldpx;->dJ(Z)V

    .line 417
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    .line 419
    :cond_1
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPy:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/launcher/GEL;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 421
    invoke-static {}, Lcad;->stop()V

    .line 422
    invoke-super {p0}, Lcom/android/launcher3/Launcher;->onDestroy()V

    .line 423
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 277
    invoke-super {p0, p1}, Lcom/android/launcher3/Launcher;->onNewIntent(Landroid/content/Intent;)V

    .line 279
    invoke-direct {p0}, Lcom/google/android/launcher/GEL;->Ck()V

    .line 280
    invoke-direct {p0}, Lcom/google/android/launcher/GEL;->Cl()V

    .line 281
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/launcher/GEL;->a(Landroid/content/Intent;Z)V

    .line 282
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 374
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/launcher/GEL;->W(Z)V

    .line 375
    invoke-super {p0}, Lcom/android/launcher3/Launcher;->onPause()V

    .line 376
    iput-boolean v1, p0, Lcom/google/android/launcher/GEL;->as:Z

    .line 377
    iput-boolean v1, p0, Lcom/google/android/launcher/GEL;->aPK:Z

    .line 378
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    invoke-virtual {v0}, Lfmd;->onPause()V

    .line 381
    :cond_0
    invoke-static {}, Lepy;->avr()Lepy;

    move-result-object v0

    iget-object v0, v0, Lepy;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 382
    return-void
.end method

.method public onPostCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 262
    invoke-super {p0, p1}, Lcom/android/launcher3/Launcher;->onPostCreate(Landroid/os/Bundle;)V

    .line 263
    iget-object v2, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPL:Z

    if-eqz v0, :cond_1

    move-object v0, p1

    :goto_0
    invoke-interface {v2, v0}, Ldpx;->onPostCreate(Landroid/os/Bundle;)V

    .line 265
    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPL:Z

    if-eqz v0, :cond_2

    :goto_1
    iput-object p1, p0, Lcom/google/android/launcher/GEL;->aPB:Landroid/os/Bundle;

    .line 266
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    iget-object v2, p0, Lcom/google/android/launcher/GEL;->aPB:Landroid/os/Bundle;

    invoke-virtual {v0, v2}, Lfmd;->onPostCreate(Landroid/os/Bundle;)V

    .line 268
    iput-object v1, p0, Lcom/google/android/launcher/GEL;->aPB:Landroid/os/Bundle;

    .line 271
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPL:Z

    .line 272
    return-void

    :cond_1
    move-object v0, v1

    .line 263
    goto :goto_0

    :cond_2
    move-object p1, v1

    .line 265
    goto :goto_1
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 322
    invoke-super {p0, p1}, Lcom/android/launcher3/Launcher;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    .line 323
    iget-object v2, p0, Lcom/android/launcher3/Launcher;->JP:Lcom/android/launcher3/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher3/Workspace;->le()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 327
    :goto_0
    return v0

    .line 326
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/launcher/GEL;->bW(Z)Z

    move v0, v1

    .line 327
    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 290
    invoke-direct {p0}, Lcom/google/android/launcher/GEL;->Ck()V

    .line 291
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/launcher/GEL;->p(Landroid/content/Intent;)V

    .line 293
    invoke-super {p0}, Lcom/android/launcher3/Launcher;->onResume()V

    .line 295
    iput-boolean v1, p0, Lcom/google/android/launcher/GEL;->aPG:Z

    .line 296
    iput-boolean v1, p0, Lcom/google/android/launcher/GEL;->as:Z

    .line 299
    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->hasWindowFocus()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPF:Z

    .line 303
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPD:Z

    .line 305
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    invoke-interface {v0}, Ldpx;->onResume()V

    .line 308
    :cond_0
    invoke-direct {p0}, Lcom/google/android/launcher/GEL;->Cl()V

    .line 309
    invoke-direct {p0}, Lcom/google/android/launcher/GEL;->Cg()V

    .line 310
    invoke-static {}, Lepy;->avr()Lepy;

    move-result-object v0

    iget-object v0, v0, Lepy;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 312
    const/4 v0, -0x1

    invoke-static {v1, v0}, Lcad;->af(II)V

    .line 314
    invoke-static {}, Lcad;->Cv()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 315
    const/16 v0, 0x3e8

    invoke-direct {p0, v0}, Lcom/google/android/launcher/GEL;->eu(I)V

    .line 317
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 250
    invoke-super {p0, p1}, Lcom/android/launcher3/Launcher;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 251
    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->isChangingConfigurations()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    const-string v0, "gel:changing_configurations"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 253
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    invoke-interface {v0, p1}, Ldpx;->w(Landroid/os/Bundle;)V

    .line 254
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    invoke-virtual {v0, p1}, Lfmd;->w(Landroid/os/Bundle;)V

    .line 258
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 388
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPD:Z

    .line 390
    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 391
    invoke-direct {p0, v1}, Lcom/google/android/launcher/GEL;->bW(Z)Z

    .line 394
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/launcher/GEL;->aPG:Z

    .line 397
    invoke-direct {p0}, Lcom/google/android/launcher/GEL;->Cg()V

    .line 401
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/launcher/GEL;->hP()I

    move-result v0

    if-eqz v0, :cond_1

    .line 402
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPx:Lfmd;

    iget-object v0, v0, Lfmd;->cvI:Lflw;

    invoke-virtual {v0}, Lflw;->wj()Lekf;

    move-result-object v0

    invoke-interface {v0, v1}, Lekf;->setScrollY(I)V

    .line 405
    :cond_1
    invoke-super {p0}, Lcom/android/launcher3/Launcher;->onStop()V

    .line 406
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 4

    .prologue
    .line 341
    invoke-super {p0, p1}, Lcom/android/launcher3/Launcher;->onWindowFocusChanged(Z)V

    .line 342
    iput-boolean p1, p0, Lcom/google/android/launcher/GEL;->aPF:Z

    .line 348
    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->as:Z

    if-eqz v0, :cond_0

    .line 349
    invoke-direct {p0}, Lcom/google/android/launcher/GEL;->Cg()V

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->aPw:Ldpx;

    invoke-interface {v0, p1}, Ldpx;->onWindowFocusChanged(Z)V

    .line 354
    if-nez p1, :cond_2

    .line 355
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/launcher/GEL;->W(Z)V

    .line 361
    :cond_1
    :goto_0
    return-void

    .line 357
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/launcher/GEL;->aPO:Z

    if-nez v0, :cond_1

    .line 358
    iget-object v0, p0, Lcom/google/android/launcher/GEL;->mRunner:Lerk;

    iget-object v1, p0, Lcom/google/android/launcher/GEL;->aPS:Lesk;

    const-wide/16 v2, 0x1f4

    invoke-interface {v0, v1, v2, v3}, Lerk;->a(Lesk;J)V

    goto :goto_0
.end method

.method public startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 365
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/launcher/GEL;->W(Z)V

    .line 366
    invoke-super {p0, p1, p2, p3}, Lcom/android/launcher3/Launcher;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 367
    return-void
.end method
