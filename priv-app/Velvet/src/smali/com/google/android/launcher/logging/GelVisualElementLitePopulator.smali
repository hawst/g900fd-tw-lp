.class public Lcom/google/android/launcher/logging/GelVisualElementLitePopulator;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lehe;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(ILjyc;)Ljyc;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p1, p0}, Ljyc;->tf(I)Ljyc;

    .line 37
    const/4 v0, 0x0

    new-array v0, v0, [I

    iput-object v0, p1, Ljyc;->dKU:[I

    .line 38
    invoke-virtual {p1}, Ljyc;->bwe()Ljyc;

    .line 39
    invoke-virtual {p1}, Ljyc;->bwd()Ljyc;

    .line 40
    return-object p1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljyc;)Ljyc;
    .locals 2

    .prologue
    .line 30
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c010c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-static {v0, p2}, Lcom/google/android/launcher/logging/GelVisualElementLitePopulator;->a(ILjyc;)Ljyc;

    .line 31
    return-object p2
.end method

.method public final a(Landroid/view/View;Ljyc;)Ljyc;
    .locals 1

    .prologue
    .line 19
    invoke-static {p1}, Lehd;->aG(Landroid/view/View;)I

    move-result v0

    invoke-static {v0, p2}, Lcom/google/android/launcher/logging/GelVisualElementLitePopulator;->a(ILjyc;)Ljyc;

    .line 21
    new-instance v0, Ljxz;

    invoke-direct {v0}, Ljxz;-><init>()V

    .line 22
    invoke-static {p1, v0}, Lcad;->a(Landroid/view/View;Ljxz;)V

    .line 23
    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    invoke-virtual {p2, v0}, Ljyc;->aF([B)Ljyc;

    .line 24
    return-object p2
.end method
