.class Lcom/google/android/launcher/GELConsentScreenFactory$IntentSpan;
.super Landroid/text/style/URLSpan;
.source "PG"


# instance fields
.field private mIntent:Landroid/content/Intent;

.field private xZ:Lcom/android/launcher3/Launcher;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/Launcher;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 75
    const-string v0, ""

    invoke-direct {p0, v0}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    .line 76
    iput-object p2, p0, Lcom/google/android/launcher/GELConsentScreenFactory$IntentSpan;->mIntent:Landroid/content/Intent;

    .line 77
    iput-object p1, p0, Lcom/google/android/launcher/GELConsentScreenFactory$IntentSpan;->xZ:Lcom/android/launcher3/Launcher;

    .line 78
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/launcher/GELConsentScreenFactory$IntentSpan;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v1, p0, Lcom/google/android/launcher/GELConsentScreenFactory$IntentSpan;->mIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Launcher;->startActivity(Landroid/content/Intent;)V

    .line 83
    return-void
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0, p1}, Landroid/text/style/URLSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 88
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 89
    return-void
.end method
