.class public Lcom/google/android/shared/util/MatchingAppInfo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public cgV:Ljava/util/List;

.field private cgW:I

.field public cgX:Lcom/google/android/shared/util/App;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cgY:Z

.field private cgZ:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 354
    new-instance v0, Lepc;

    invoke-direct {v0}, Lepc;-><init>()V

    sput-object v0, Lcom/google/android/shared/util/MatchingAppInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(ILcom/google/android/shared/util/App;)V
    .locals 1
    .param p2    # Lcom/google/android/shared/util/App;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    if-eqz p2, :cond_0

    invoke-static {p2}, Lijj;->bs(Ljava/lang/Object;)Lijj;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgV:Ljava/util/List;

    .line 64
    iput p1, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgW:I

    .line 65
    iput-object p2, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    .line 66
    return-void

    .line 63
    :cond_0
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 70
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgV:Ljava/util/List;

    .line 71
    iget-object v1, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgV:Ljava/util/List;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgW:I

    .line 73
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/App;

    iput-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    .line 74
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgZ:I

    .line 75
    return-void
.end method

.method public constructor <init>(Lcom/google/android/shared/util/MatchingAppInfo;)V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iget-object v0, p1, Lcom/google/android/shared/util/MatchingAppInfo;->cgV:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgV:Ljava/util/List;

    .line 82
    iget v0, p1, Lcom/google/android/shared/util/MatchingAppInfo;->cgW:I

    iput v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgW:I

    .line 83
    iget-object v0, p1, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    iput-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    .line 84
    iget-boolean v0, p1, Lcom/google/android/shared/util/MatchingAppInfo;->cgY:Z

    iput-boolean v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgY:Z

    .line 85
    iget v0, p1, Lcom/google/android/shared/util/MatchingAppInfo;->cgZ:I

    iput v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgZ:I

    .line 86
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/google/android/shared/util/App;)V
    .locals 1
    .param p2    # Lcom/google/android/shared/util/App;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgV:Ljava/util/List;

    .line 55
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgW:I

    .line 56
    iput-object p2, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    .line 57
    return-void
.end method

.method public static a(ILcom/google/android/shared/util/App;)Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 1
    .param p1    # Lcom/google/android/shared/util/App;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 284
    new-instance v0, Lcom/google/android/shared/util/MatchingAppInfo;

    invoke-direct {v0, p0, p1}, Lcom/google/android/shared/util/MatchingAppInfo;-><init>(ILcom/google/android/shared/util/App;)V

    return-object v0
.end method

.method private static a(Lcom/google/android/shared/util/App;Lcom/google/android/shared/util/App;)Z
    .locals 2
    .param p0    # Lcom/google/android/shared/util/App;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/google/android/shared/util/App;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 329
    if-nez p0, :cond_1

    .line 330
    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 335
    :cond_0
    :goto_0
    return v0

    .line 332
    :cond_1
    if-eqz p1, :cond_0

    .line 335
    invoke-virtual {p0}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static avb()Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 3

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/shared/util/MatchingAppInfo;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/shared/util/MatchingAppInfo;-><init>(ILcom/google/android/shared/util/App;)V

    return-object v0
.end method

.method public static avc()Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 3

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/shared/util/MatchingAppInfo;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/shared/util/MatchingAppInfo;-><init>(ILcom/google/android/shared/util/App;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/shared/util/MatchingAppInfo;Landroid/content/pm/PackageManager;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 234
    iget v1, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgZ:I

    if-nez v1, :cond_0

    .line 275
    :goto_0
    return v0

    .line 237
    :cond_0
    iput v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgZ:I

    .line 238
    invoke-virtual {p0, p1}, Lcom/google/android/shared/util/MatchingAppInfo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 242
    iget v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgW:I

    iget v1, p1, Lcom/google/android/shared/util/MatchingAppInfo;->cgW:I

    if-eq v0, v1, :cond_1

    .line 243
    iget v0, p1, Lcom/google/android/shared/util/MatchingAppInfo;->cgW:I

    iput v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgW:I

    .line 245
    :cond_1
    iget-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    iget-object v1, p1, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    invoke-static {v0, v1}, Lcom/google/android/shared/util/MatchingAppInfo;->a(Lcom/google/android/shared/util/App;Lcom/google/android/shared/util/App;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 250
    invoke-virtual {p0}, Lcom/google/android/shared/util/MatchingAppInfo;->avd()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/shared/util/MatchingAppInfo;->avd()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 255
    iget-object v0, p1, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    iput-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    .line 275
    :cond_2
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 258
    :cond_3
    iget-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    invoke-virtual {v0}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_2

    .line 262
    invoke-virtual {p1}, Lcom/google/android/shared/util/MatchingAppInfo;->avd()Z

    .line 265
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    goto :goto_1
.end method

.method public final ahT()Ljava/util/List;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgV:Ljava/util/List;

    return-object v0
.end method

.method public final avd()Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ave()Lcom/google/android/shared/util/App;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    return-object v0
.end method

.method public final avf()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgY:Z

    return v0
.end method

.method public final avg()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    invoke-virtual {v0}, Lcom/google/android/shared/util/App;->getLabel()Ljava/lang/String;

    move-result-object v0

    .line 168
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final avh()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgW:I

    return v0
.end method

.method public final avi()Z
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgW:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final avj()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 180
    iget v1, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgW:I

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final avk()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 191
    iget v1, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgW:I

    if-le v1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/shared/util/MatchingAppInfo;->avd()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final avl()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 200
    iget v1, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgZ:I

    if-nez v1, :cond_0

    .line 201
    iput v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgZ:I

    .line 204
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final avm()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 213
    iget v1, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgZ:I

    if-ne v1, v0, :cond_0

    .line 214
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgZ:I

    .line 217
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final avn()Z
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgZ:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 344
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 302
    instance-of v0, p1, Lcom/google/android/shared/util/MatchingAppInfo;

    if-eqz v0, :cond_5

    .line 303
    check-cast p1, Lcom/google/android/shared/util/MatchingAppInfo;

    .line 304
    iget v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgW:I

    iget v1, p1, Lcom/google/android/shared/util/MatchingAppInfo;->cgW:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgV:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p1, Lcom/google/android/shared/util/MatchingAppInfo;->cgV:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    iget-object v1, p1, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    invoke-static {v0, v1}, Lcom/google/android/shared/util/MatchingAppInfo;->a(Lcom/google/android/shared/util/App;Lcom/google/android/shared/util/App;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 321
    :goto_0
    return v0

    .line 311
    :cond_1
    iget-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgV:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 312
    iget-object v0, p1, Lcom/google/android/shared/util/MatchingAppInfo;->cgV:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 313
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 314
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/App;

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/shared/util/App;

    invoke-static {v0, v1}, Lcom/google/android/shared/util/MatchingAppInfo;->a(Lcom/google/android/shared/util/App;Lcom/google/android/shared/util/App;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_3
    move v0, v2

    .line 316
    goto :goto_0

    .line 319
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v2

    .line 321
    goto :goto_0
.end method

.method public final l(Lcom/google/android/shared/util/App;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 124
    iput-boolean v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgY:Z

    .line 125
    iget-object v1, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    if-eq p1, v1, :cond_0

    .line 126
    iput-object p1, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    .line 127
    const/4 v0, 0x1

    .line 129
    :cond_0
    return v0
.end method

.method public final lk(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgV:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgY:Z

    .line 146
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 147
    iget-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgV:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/android/shared/util/App;->c(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/shared/util/App;

    move-result-object v0

    .line 149
    if-eqz v0, :cond_2

    .line 151
    iput-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    goto :goto_0

    .line 156
    :cond_2
    iget-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgV:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/App;

    iput-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/google/android/shared/util/MatchingAppInfo;->avj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    const-string v0, "MatchingAppInfo{Internal}"

    .line 294
    :goto_0
    return-object v0

    .line 291
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/shared/util/MatchingAppInfo;->avi()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 292
    const-string v0, "MatchingAppInfo{No Match}"

    goto :goto_0

    .line 294
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MatchingAppInfo{Num Apps = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgW:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Preferred App = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Apps = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgV:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgV:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 350
    iget v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgW:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 351
    iget-object v0, p0, Lcom/google/android/shared/util/MatchingAppInfo;->cgX:Lcom/google/android/shared/util/App;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 352
    return-void
.end method
