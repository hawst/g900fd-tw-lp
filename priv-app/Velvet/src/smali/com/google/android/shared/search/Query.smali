.class public Lcom/google/android/shared/search/Query;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Letz;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final bZk:Ljava/lang/ThreadLocal;

.field public static final bZl:Lcom/google/android/shared/search/Query;

.field public static bZm:Ljava/util/concurrent/atomic/AtomicLong;


# instance fields
.field final amT:J

.field final bKQ:Ljava/lang/CharSequence;

.field final bKR:I

.field final bKS:I

.field final bZA:J

.field final bZB:J

.field final bZn:Ljava/io/Serializable;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final bZo:Lijj;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final bZp:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final bZq:Lehm;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final bZr:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final bZs:I

.field final bZt:Lijm;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field final bZu:J

.field final bZv:Ljava/lang/String;

.field final bZw:Ljava/lang/String;

.field final bZx:Landroid/location/Location;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final bZy:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final bZz:Landroid/net/Uri;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final bsH:I

.field final bwM:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final cF:Landroid/os/Bundle;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final cZ:I

.field final mSearchBoxStats:Lcom/google/android/shared/search/SearchBoxStats;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 274
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lcom/google/android/shared/search/Query;->bZk:Ljava/lang/ThreadLocal;

    .line 276
    new-instance v0, Lcom/google/android/shared/search/Query;

    invoke-direct {v0}, Lcom/google/android/shared/search/Query;-><init>()V

    sput-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    .line 278
    new-instance v0, Lehg;

    invoke-direct {v0}, Lehg;-><init>()V

    sput-object v0, Lcom/google/android/shared/search/Query;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 291
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    sput-object v0, Lcom/google/android/shared/search/Query;->bZm:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method constructor <init>()V
    .locals 29

    .prologue
    .line 467
    const/4 v1, 0x0

    const-string v2, ""

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "web"

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const-wide/16 v22, 0x0

    const-wide/16 v24, 0x0

    const/16 v26, 0x0

    const-wide/16 v27, 0x0

    move-object/from16 v0, p0

    invoke-direct/range {v0 .. v28}, Lcom/google/android/shared/search/Query;-><init>(ILjava/lang/CharSequence;Lijj;Ljava/lang/String;Lehm;Ljava/lang/String;IILjava/lang/String;ILjava/util/Map;JLjava/lang/String;Lcom/google/android/shared/search/SearchBoxStats;Landroid/location/Location;Ljava/lang/String;Ljava/io/Serializable;Landroid/os/Bundle;Ljava/lang/String;Landroid/net/Uri;JJIJ)V

    .line 469
    return-void
.end method

.method private constructor <init>(ILjava/lang/CharSequence;Lijj;Ljava/lang/String;Lehm;Ljava/lang/String;IILjava/lang/String;ILjava/util/Map;JLjava/lang/String;Lcom/google/android/shared/search/SearchBoxStats;Landroid/location/Location;Ljava/lang/String;Ljava/io/Serializable;Landroid/os/Bundle;Ljava/lang/String;Landroid/net/Uri;JJIJ)V
    .locals 4

    .prologue
    .line 432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 433
    iput p1, p0, Lcom/google/android/shared/search/Query;->cZ:I

    .line 434
    if-eqz p2, :cond_0

    :goto_0
    iput-object p2, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    .line 435
    iput-object p3, p0, Lcom/google/android/shared/search/Query;->bZo:Lijj;

    .line 436
    iput-object p4, p0, Lcom/google/android/shared/search/Query;->bZp:Ljava/lang/String;

    .line 437
    iput-object p5, p0, Lcom/google/android/shared/search/Query;->bZq:Lehm;

    .line 438
    iput-object p6, p0, Lcom/google/android/shared/search/Query;->bZr:Ljava/lang/String;

    .line 439
    iput p7, p0, Lcom/google/android/shared/search/Query;->bKR:I

    .line 440
    iput p8, p0, Lcom/google/android/shared/search/Query;->bKS:I

    .line 441
    iput-object p9, p0, Lcom/google/android/shared/search/Query;->bZw:Ljava/lang/String;

    .line 442
    iput p10, p0, Lcom/google/android/shared/search/Query;->bZs:I

    .line 443
    if-nez p11, :cond_1

    .line 444
    invoke-static {}, Lijm;->aWY()Lijm;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/shared/search/Query;->bZt:Lijm;

    .line 450
    :goto_1
    move-wide/from16 v0, p12

    iput-wide v0, p0, Lcom/google/android/shared/search/Query;->bZu:J

    .line 451
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/shared/search/Query;->bZv:Ljava/lang/String;

    .line 452
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/shared/search/Query;->mSearchBoxStats:Lcom/google/android/shared/search/SearchBoxStats;

    .line 453
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/shared/search/Query;->bZx:Landroid/location/Location;

    .line 454
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/shared/search/Query;->bZy:Ljava/lang/String;

    .line 455
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/shared/search/Query;->bZn:Ljava/io/Serializable;

    .line 456
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/shared/search/Query;->cF:Landroid/os/Bundle;

    .line 457
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/shared/search/Query;->bwM:Ljava/lang/String;

    .line 458
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/shared/search/Query;->bZz:Landroid/net/Uri;

    .line 459
    move-wide/from16 v0, p22

    iput-wide v0, p0, Lcom/google/android/shared/search/Query;->bZA:J

    .line 460
    move-wide/from16 v0, p24

    iput-wide v0, p0, Lcom/google/android/shared/search/Query;->amT:J

    .line 461
    move/from16 v0, p26

    iput v0, p0, Lcom/google/android/shared/search/Query;->bsH:I

    .line 462
    move-wide/from16 v0, p27

    iput-wide v0, p0, Lcom/google/android/shared/search/Query;->bZB:J

    .line 463
    return-void

    .line 434
    :cond_0
    const-string p2, ""

    goto :goto_0

    .line 445
    :cond_1
    instance-of v2, p11, Lijm;

    if-eqz v2, :cond_2

    .line 446
    check-cast p11, Lijm;

    iput-object p11, p0, Lcom/google/android/shared/search/Query;->bZt:Lijm;

    goto :goto_1

    .line 448
    :cond_2
    invoke-static {p11}, Lijm;->s(Ljava/util/Map;)Lijm;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/shared/search/Query;->bZt:Lijm;

    goto :goto_1
.end method

.method public synthetic constructor <init>(ILjava/lang/CharSequence;Lijj;Ljava/lang/String;Lehm;Ljava/lang/String;IILjava/lang/String;ILjava/util/Map;JLjava/lang/String;Lcom/google/android/shared/search/SearchBoxStats;Landroid/location/Location;Ljava/lang/String;Ljava/io/Serializable;Landroid/os/Bundle;Ljava/lang/String;Landroid/net/Uri;JJIJB)V
    .locals 0

    .prologue
    .line 61
    invoke-direct/range {p0 .. p28}, Lcom/google/android/shared/search/Query;-><init>(ILjava/lang/CharSequence;Lijj;Ljava/lang/String;Lehm;Ljava/lang/String;IILjava/lang/String;ILjava/util/Map;JLjava/lang/String;Lcom/google/android/shared/search/SearchBoxStats;Landroid/location/Location;Ljava/lang/String;Ljava/io/Serializable;Landroid/os/Bundle;Ljava/lang/String;Landroid/net/Uri;JJIJ)V

    return-void
.end method

.method private apW()I
    .locals 1

    .prologue
    .line 1379
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    and-int/lit16 v0, v0, 0xf0

    return v0
.end method

.method private are()Lehh;
    .locals 4

    .prologue
    .line 1708
    sget-object v0, Lcom/google/android/shared/search/Query;->bZk:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehh;

    .line 1709
    if-nez v0, :cond_0

    .line 1710
    new-instance v0, Lehh;

    invoke-direct {v0}, Lehh;-><init>()V

    .line 1711
    sget-object v1, Lcom/google/android/shared/search/Query;->bZk:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 1713
    :cond_0
    iput-object p0, v0, Lehh;->mQuery:Lcom/google/android/shared/search/Query;

    iget v1, p0, Lcom/google/android/shared/search/Query;->cZ:I

    iput v1, v0, Lehh;->cZ:I

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    iput-object v1, v0, Lehh;->bKQ:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZo:Lijj;

    iput-object v1, v0, Lehh;->bZo:Lijj;

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZp:Ljava/lang/String;

    iput-object v1, v0, Lehh;->bZp:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZq:Lehm;

    iput-object v1, v0, Lehh;->bZq:Lehm;

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZr:Ljava/lang/String;

    iput-object v1, v0, Lehh;->bZr:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/shared/search/Query;->bKR:I

    iput v1, v0, Lehh;->bKR:I

    iget v1, p0, Lcom/google/android/shared/search/Query;->bKS:I

    iput v1, v0, Lehh;->bKS:I

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZw:Ljava/lang/String;

    iput-object v1, v0, Lehh;->bZw:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/shared/search/Query;->bZu:J

    iput-wide v2, v0, Lehh;->bZu:J

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZv:Ljava/lang/String;

    iput-object v1, v0, Lehh;->bZv:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/shared/search/Query;->bZs:I

    iput v1, v0, Lehh;->bZs:I

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZt:Lijm;

    iput-object v1, v0, Lehh;->bft:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->mSearchBoxStats:Lcom/google/android/shared/search/SearchBoxStats;

    iput-object v1, v0, Lehh;->mSearchBoxStats:Lcom/google/android/shared/search/SearchBoxStats;

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZx:Landroid/location/Location;

    iput-object v1, v0, Lehh;->bZx:Landroid/location/Location;

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZy:Ljava/lang/String;

    iput-object v1, v0, Lehh;->bZy:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZn:Ljava/io/Serializable;

    iput-object v1, v0, Lehh;->bZn:Ljava/io/Serializable;

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->cF:Landroid/os/Bundle;

    iput-object v1, v0, Lehh;->cF:Landroid/os/Bundle;

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bwM:Ljava/lang/String;

    iput-object v1, v0, Lehh;->bwM:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZz:Landroid/net/Uri;

    iput-object v1, v0, Lehh;->bZz:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/google/android/shared/search/Query;->bZA:J

    iput-wide v2, v0, Lehh;->bZA:J

    iget-wide v2, p0, Lcom/google/android/shared/search/Query;->amT:J

    iput-wide v2, v0, Lehh;->amT:J

    iget v1, p0, Lcom/google/android/shared/search/Query;->bsH:I

    iput v1, v0, Lehh;->bsH:I

    iget-wide v2, p0, Lcom/google/android/shared/search/Query;->bZB:J

    iput-wide v2, v0, Lehh;->bZB:J

    const/4 v1, 0x0

    iput-boolean v1, v0, Lehh;->bbz:Z

    return-object v0
.end method

.method private arf()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2201
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    .line 2202
    sparse-switch v0, :sswitch_data_0

    .line 2228
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2204
    :sswitch_0
    const-string v0, "user"

    goto :goto_0

    .line 2206
    :sswitch_1
    const-string v0, "intent"

    goto :goto_0

    .line 2208
    :sswitch_2
    const-string v0, "prefetch"

    goto :goto_0

    .line 2210
    :sswitch_3
    const-string v0, "predictive"

    goto :goto_0

    .line 2212
    :sswitch_4
    const-string v0, "webview"

    goto :goto_0

    .line 2214
    :sswitch_5
    const-string v0, "hotword"

    goto :goto_0

    .line 2216
    :sswitch_6
    const-string v0, "bthandsfree"

    goto :goto_0

    .line 2218
    :sswitch_7
    const-string v0, "wiredheadset"

    goto :goto_0

    .line 2220
    :sswitch_8
    const-string v0, "follow-on"

    goto :goto_0

    .line 2222
    :sswitch_9
    const-string v0, "doodle"

    goto :goto_0

    .line 2224
    :sswitch_a
    const-string v0, "voice-command-intent"

    goto :goto_0

    .line 2226
    :sswitch_b
    const-string v0, "hands-free-overlay-tap"

    goto :goto_0

    .line 2202
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x20 -> :sswitch_2
        0x30 -> :sswitch_3
        0x40 -> :sswitch_4
        0x50 -> :sswitch_5
        0x60 -> :sswitch_6
        0x70 -> :sswitch_7
        0x80 -> :sswitch_8
        0xc0 -> :sswitch_9
        0xd0 -> :sswitch_a
        0xe0 -> :sswitch_b
    .end sparse-switch
.end method

.method public static ay(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 500
    invoke-static {p0, p1}, Lesp;->aH(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private c(IZZ)Lcom/google/android/shared/search/Query;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v3, 0x200000

    const/16 v1, 0x4000

    const/4 v2, 0x0

    .line 891
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lehh;->hB(I)Lehh;

    move-result-object v0

    const/16 v4, 0x400

    invoke-virtual {v0, v2, v4}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->hC(I)Lehh;

    move-result-object v0

    const/high16 v4, 0x20000

    invoke-virtual {v0, v4, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    const/16 v4, 0x2000

    invoke-virtual {v0, v4, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    const/16 v4, 0x200

    invoke-virtual {v0, v4, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Lehh;->aK(II)Lehh;

    move-result-object v4

    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v1, v0}, Lehh;->aJ(II)Lehh;

    move-result-object v1

    if-eqz p3, :cond_1

    move v0, v3

    :goto_1
    invoke-virtual {v1, v3, v0}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0, v2}, Lehh;->hE(I)Lehh;

    move-result-object v0

    invoke-virtual {v0, v5}, Lehh;->H(Landroid/net/Uri;)Lehh;

    move-result-object v0

    invoke-virtual {v0, v5}, Lehh;->kY(Ljava/lang/String;)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public static c(Ljava/lang/CharSequence;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2247
    packed-switch p1, :pswitch_data_0

    .line 2252
    if-ltz p1, :cond_0

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-gt p1, v1, :cond_0

    :goto_0
    :pswitch_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2247
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private getTypeString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2178
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    and-int/lit8 v0, v0, 0xf

    .line 2179
    packed-switch v0, :pswitch_data_0

    .line 2197
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2181
    :pswitch_0
    const-string v0, "text"

    goto :goto_0

    .line 2183
    :pswitch_1
    const-string v0, "voice"

    goto :goto_0

    .line 2185
    :pswitch_2
    const-string v0, "music"

    goto :goto_0

    .line 2187
    :pswitch_3
    const-string v0, "tv"

    goto :goto_0

    .line 2189
    :pswitch_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sentinel["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqI()Ljava/io/Serializable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2191
    :pswitch_5
    const-string v0, "externalActivitySentinel"

    goto :goto_0

    .line 2193
    :pswitch_6
    const-string v0, "notificationAnnouncement"

    goto :goto_0

    .line 2195
    :pswitch_7
    const-string v0, "transcription"

    goto :goto_0

    .line 2179
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private static hA(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2232
    packed-switch p0, :pswitch_data_0

    .line 2238
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2234
    :pswitch_0
    const-string v0, "unchanged"

    goto :goto_0

    .line 2236
    :pswitch_1
    const-string v0, "end"

    goto :goto_0

    .line 2232
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static i(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 476
    invoke-static {p0, p1}, Lcom/google/android/shared/search/Query;->j(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/google/android/shared/search/Query;->bZs:I

    iget v3, p1, Lcom/google/android/shared/search/Query;->bZs:I

    if-ne v2, v3, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/shared/search/Query;->bZw:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/shared/search/Query;->bZw:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static j(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 485
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lesp;->aH(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->arc()Z

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->arc()Z

    move-result v3

    if-ne v2, v3, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->ard()Z

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->ard()Z

    move-result v3

    if-ne v2, v3, :cond_1

    move v2, v0

    :goto_1
    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/shared/search/Query;->bZx:Landroid/location/Location;

    iget-object v3, p1, Lcom/google/android/shared/search/Query;->bZx:Landroid/location/Location;

    invoke-static {v2, v3}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqr()Z

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqr()Z

    move-result v3

    if-ne v2, v3, :cond_2

    move v2, v0

    :goto_2
    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/shared/search/Query;->bZy:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/shared/search/Query;->bZy:Ljava/lang/String;

    invoke-static {v2, v3}, Lesp;->aH(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_3
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v2, v1

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_3
.end method

.method public static k(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;)Z
    .locals 2

    .prologue
    .line 504
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lesp;->aH(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final A(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 1178
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1179
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    iget v3, p0, Lcom/google/android/shared/search/Query;->bKR:I

    invoke-interface {v0, v1, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1180
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1181
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    iget v3, p0, Lcom/google/android/shared/search/Query;->bKS:I

    iget-object v4, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-interface {v0, v3, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1183
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_1

    .line 1184
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    .line 1185
    invoke-static {v2}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v4

    .line 1186
    iget v2, p0, Lcom/google/android/shared/search/Query;->bKR:I

    const-class v3, Landroid/text/style/SuggestionSpan;

    move v5, v1

    invoke-static/range {v0 .. v5}, Leqt;->copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V

    .line 1188
    iget v2, p0, Lcom/google/android/shared/search/Query;->bKR:I

    const-class v3, Lcom/google/android/shared/util/CorrectionSpan;

    move v5, v1

    invoke-static/range {v0 .. v5}, Leqt;->copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V

    .line 1190
    iget v2, p0, Lcom/google/android/shared/search/Query;->bKR:I

    const-class v3, Lcom/google/android/shared/util/VoiceCorrectionSpan;

    move v5, v1

    invoke-static/range {v0 .. v5}, Leqt;->copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V

    .line 1192
    instance-of v2, p1, Landroid/text/Spanned;

    if-eqz v2, :cond_0

    move-object v5, p1

    .line 1193
    check-cast v5, Landroid/text/Spanned;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v7

    const-class v8, Landroid/text/style/SuggestionSpan;

    iget v10, p0, Lcom/google/android/shared/search/Query;->bKR:I

    move v6, v1

    move-object v9, v4

    invoke-static/range {v5 .. v10}, Leqt;->copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V

    move-object v5, p1

    .line 1195
    check-cast v5, Landroid/text/Spanned;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v7

    const-class v8, Lcom/google/android/shared/util/CorrectionSpan;

    iget v10, p0, Lcom/google/android/shared/search/Query;->bKR:I

    move v6, v1

    move-object v9, v4

    invoke-static/range {v5 .. v10}, Leqt;->copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V

    move-object v5, p1

    .line 1197
    check-cast v5, Landroid/text/Spanned;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v7

    const-class v8, Lcom/google/android/shared/util/VoiceCorrectionSpan;

    iget v10, p0, Lcom/google/android/shared/search/Query;->bKR:I

    move v6, v1

    move-object v9, v4

    invoke-static/range {v5 .. v10}, Leqt;->copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V

    .line 1200
    :cond_0
    iget v2, p0, Lcom/google/android/shared/search/Query;->bKR:I

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    add-int v10, v2, v3

    .line 1201
    iget v6, p0, Lcom/google/android/shared/search/Query;->bKS:I

    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v7

    const-class v8, Landroid/text/style/SuggestionSpan;

    move-object v5, v0

    move-object v9, v4

    invoke-static/range {v5 .. v10}, Leqt;->copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V

    .line 1203
    iget v6, p0, Lcom/google/android/shared/search/Query;->bKS:I

    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v7

    const-class v8, Lcom/google/android/shared/util/CorrectionSpan;

    move-object v5, v0

    move-object v9, v4

    invoke-static/range {v5 .. v10}, Leqt;->copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V

    .line 1205
    iget v6, p0, Lcom/google/android/shared/search/Query;->bKS:I

    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v7

    const-class v8, Lcom/google/android/shared/util/VoiceCorrectionSpan;

    move-object v5, v0

    move-object v9, v4

    invoke-static/range {v5 .. v10}, Leqt;->copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V

    .line 1211
    :goto_0
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, v4}, Lehh;->D(Ljava/lang/CharSequence;)Lehh;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Lehh;->aK(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0

    .line 1209
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public final AF()J
    .locals 2

    .prologue
    .line 1581
    iget-wide v0, p0, Lcom/google/android/shared/search/Query;->amT:J

    return-wide v0
.end method

.method public final B(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;
    .locals 4

    .prologue
    .line 1221
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->D(Ljava/lang/CharSequence;)Lehh;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Lehh;->hC(I)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->ari()Lehh;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lehh;->aI(J)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final C(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    .line 2307
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lehh;->hB(I)Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->D(Ljava/lang/CharSequence;)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final E(Landroid/os/Bundle;)Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 669
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->G(Landroid/os/Bundle;)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final F(Landroid/net/Uri;)Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 908
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lehh;->hB(I)Lehh;

    move-result-object v0

    const/16 v1, 0x200

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->H(Landroid/net/Uri;)Lehh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lehh;->kY(Ljava/lang/String;)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final F(Landroid/os/Bundle;)Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    .line 700
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lehh;->hB(I)Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->G(Landroid/os/Bundle;)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final G(Landroid/net/Uri;)Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 917
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lehh;->hB(I)Lehh;

    move-result-object v0

    const/16 v1, 0x200

    invoke-virtual {v0, v1, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    const/high16 v1, 0x800000

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    const/high16 v1, 0x1000000

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    const/high16 v1, 0x2000000

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->H(Landroid/net/Uri;)Lehh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lehh;->kY(Ljava/lang/String;)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final YP()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1680
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bwM:Ljava/lang/String;

    return-object v0
.end method

.method public final ZB()Z
    .locals 1

    .prologue
    .line 1652
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqV()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/shared/search/Query;->bZs:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lehm;)Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 648
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->b(Lehm;)Lehh;

    move-result-object v0

    const/high16 v1, 0x8000000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lehm;Ljava/lang/CharSequence;Ljava/lang/String;ILjava/lang/String;JLjava/util/Map;)Lcom/google/android/shared/search/Query;
    .locals 4
    .param p8    # Ljava/util/Map;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 633
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, v2}, Lehh;->hB(I)Lehh;

    move-result-object v0

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Lehh;->hC(I)Lehh;

    move-result-object v0

    invoke-virtual {v0, p2}, Lehh;->D(Ljava/lang/CharSequence;)Lehh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p3, v1}, Lehh;->p(Ljava/lang/String;Z)Lehh;

    move-result-object v0

    invoke-virtual {v0, p4}, Lehh;->hD(I)Lehh;

    move-result-object v0

    invoke-virtual {v0, p5}, Lehh;->la(Ljava/lang/String;)Lehh;

    move-result-object v0

    invoke-virtual {v0, p8}, Lehh;->m(Ljava/util/Map;)Lehh;

    move-result-object v0

    invoke-virtual {v0, p6, p7}, Lehh;->aI(J)Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->b(Lehm;)Lehh;

    move-result-object v0

    const/high16 v1, 0x8000000

    invoke-virtual {v0, v1, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/io/Serializable;Landroid/os/Bundle;)Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 687
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v1

    const/16 v0, 0xf

    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, Lehh;->aJ(II)Lehh;

    iget-boolean v2, v1, Lehh;->bbz:Z

    iget-object v0, v1, Lehh;->bZn:Ljava/io/Serializable;

    if-ne v0, p1, :cond_0

    iget-object v0, v1, Lehh;->cF:Landroid/os/Bundle;

    if-eq v0, p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v2

    iput-boolean v0, v1, Lehh;->bbz:Z

    iput-object p1, v1, Lehh;->bZn:Ljava/io/Serializable;

    iput-object p2, v1, Lehh;->cF:Landroid/os/Bundle;

    invoke-virtual {v1}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;Landroid/location/Location;Ljava/lang/String;)Lcom/google/android/shared/search/Query;
    .locals 4
    .param p2    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1231
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v1

    invoke-virtual {v1, p1}, Lehh;->D(Ljava/lang/CharSequence;)Lehh;

    move-result-object v1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v1, v2, v2}, Lehh;->aK(II)Lehh;

    move-result-object v1

    invoke-virtual {v1, v0}, Lehh;->hB(I)Lehh;

    move-result-object v1

    const/16 v2, 0x30

    invoke-virtual {v1, v2}, Lehh;->hC(I)Lehh;

    move-result-object v1

    iget-boolean v2, v1, Lehh;->bbz:Z

    iget-object v3, v1, Lehh;->bZx:Landroid/location/Location;

    invoke-static {p2, v3}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    or-int/2addr v0, v2

    iput-boolean v0, v1, Lehh;->bbz:Z

    iput-object p2, v1, Lehh;->bZx:Landroid/location/Location;

    invoke-virtual {v1, p3}, Lehh;->la(Ljava/lang/String;)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;Landroid/location/Location;Ljava/lang/String;Z)Lcom/google/android/shared/search/Query;
    .locals 3
    .param p2    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1243
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/shared/search/Query;->a(Ljava/lang/CharSequence;Landroid/location/Location;Ljava/lang/String;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-direct {v0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const-string v1, "web"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lehh;->p(Ljava/lang/String;Z)Lehh;

    move-result-object v1

    if-eqz p4, :cond_0

    const/16 v0, 0xc0

    :goto_0
    invoke-virtual {v1, v0}, Lehh;->hC(I)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0

    :cond_0
    const/16 v0, 0x30

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;Lijj;Z)Lcom/google/android/shared/search/Query;
    .locals 3
    .param p2    # Lijj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1100
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->D(Ljava/lang/CharSequence;)Lehh;

    move-result-object v0

    iget-object v2, v0, Lehh;->bZo:Lijj;

    invoke-static {p2, v2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iput-object p2, v0, Lehh;->bZo:Lijj;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lehh;->bbz:Z

    :cond_0
    invoke-virtual {v0, v1, v1}, Lehh;->aK(II)Lehh;

    move-result-object v2

    if-eqz p3, :cond_1

    const/high16 v0, 0x100000

    :goto_0
    or-int/lit16 v0, v0, 0x800

    invoke-virtual {v2, v1, v0}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->mSearchBoxStats:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-virtual {v0, v1}, Lehh;->d(Lcom/google/android/shared/search/SearchBoxStats;)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;)Lcom/google/android/shared/search/Query;
    .locals 5
    .param p6    # Ljava/util/Map;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 617
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v2

    invoke-virtual {v2, v1}, Lehh;->hB(I)Lehh;

    move-result-object v2

    invoke-virtual {v2, p2}, Lehh;->D(Ljava/lang/CharSequence;)Lehh;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v3, v3}, Lehh;->aK(II)Lehh;

    move-result-object v2

    invoke-virtual {v2, p3, v0}, Lehh;->p(Ljava/lang/String;Z)Lehh;

    move-result-object v2

    invoke-virtual {v2, p4}, Lehh;->hD(I)Lehh;

    move-result-object v2

    invoke-virtual {v2, p5}, Lehh;->la(Ljava/lang/String;)Lehh;

    move-result-object v2

    invoke-virtual {v2, p6}, Lehh;->m(Ljava/util/Map;)Lehh;

    move-result-object v2

    iget-boolean v3, v2, Lehh;->bbz:Z

    iget-object v4, v2, Lehh;->bZp:Ljava/lang/String;

    invoke-static {p1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    or-int/2addr v0, v3

    iput-boolean v0, v2, Lehh;->bbz:Z

    iput-object p1, v2, Lehh;->bZp:Ljava/lang/String;

    invoke-virtual {v2}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final a(Letj;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2322
    const-string v0, "Query"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 2323
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    if-ne p0, v0, :cond_0

    .line 2324
    const-string v0, "[EMPTY]"

    invoke-virtual {p1, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 2368
    :goto_0
    return-void

    .line 2327
    :cond_0
    const-string v0, "type"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->getTypeString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 2328
    const-string v0, "trigger"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->arf()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 2330
    const-string v0, "query chars"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 2331
    const-string v0, "corpus ID"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZw:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 2332
    const-string v0, "result index"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget v1, p0, Lcom/google/android/shared/search/Query;->bZs:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 2333
    const-string v0, "commit ID"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/shared/search/Query;->bZu:J

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 2334
    const-string v0, "resend audio request ID"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZv:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 2335
    const-string v0, "selection start"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget v1, p0, Lcom/google/android/shared/search/Query;->bKR:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 2336
    const-string v0, "selection end"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget v1, p0, Lcom/google/android/shared/search/Query;->bKS:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 2337
    const-string v0, "assist"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqA()Z

    move-result v1

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 2338
    const-string v0, "play tts"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->alf()Z

    move-result v1

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 2339
    const-string v0, "eyes free"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v1

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 2340
    const-string v0, "voice search"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v1

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 2341
    const-string v0, "is full SRP"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqB()Z

    move-result v1

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 2342
    const-string v0, "opaque actions"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqC()Z

    move-result v1

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 2343
    const-string v0, "GPM"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqz()Z

    move-result v1

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 2344
    const-string v0, "resend last recording"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqF()Z

    move-result v1

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 2345
    const-string v0, "rewritten"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->afX()Z

    move-result v1

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 2346
    const-string v0, "from back stack"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apO()Z

    move-result v1

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 2347
    const-string v0, "restored state"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqu()Z

    move-result v1

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 2348
    const-string v0, "client stats"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->mSearchBoxStats:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 2351
    const-string v0, "location override"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZx:Landroid/location/Location;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 2355
    const-string v0, "stick"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZy:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 2356
    const-string v0, "persist CGI parameters"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZt:Lijm;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 2358
    const-string v0, "assist package"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bwM:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 2359
    const-string v0, "original url"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZp:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 2360
    const-string v0, "webapp request"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZq:Lehm;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 2361
    const-string v0, "substate"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZr:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 2363
    const-string v0, "recording URI"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZz:Landroid/net/Uri;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 2365
    const-string v0, "submission time"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/shared/search/Query;->bZA:J

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 2366
    const-string v0, "UI to launch for voice search"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget v1, p0, Lcom/google/android/shared/search/Query;->bsH:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    goto/16 :goto_0
.end method

.method public final aA(Landroid/os/Parcel;)Lcom/google/android/shared/search/Query;
    .locals 31

    .prologue
    .line 2143
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 2144
    invoke-static/range {p1 .. p1}, Leqt;->aC(Landroid/os/Parcel;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 2145
    invoke-static/range {p1 .. p1}, Leqt;->aD(Landroid/os/Parcel;)Lijj;

    move-result-object v5

    .line 2146
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 2147
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 2148
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v9

    .line 2149
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    .line 2150
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v11

    .line 2151
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v2}, Lesp;->H(Landroid/os/Bundle;)Ljava/util/Map;

    move-result-object v2

    invoke-static {v2}, Lijm;->s(Ljava/util/Map;)Lijm;

    move-result-object v13

    .line 2153
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v12

    .line 2154
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v14

    .line 2155
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v16

    .line 2157
    sget-object v2, Lcom/google/android/shared/search/Query;->bZm:Ljava/util/concurrent/atomic/AtomicLong;

    sget-object v7, Lcom/google/android/shared/search/Query;->bZm:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-static {v0, v1, v14, v15}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 2158
    const-class v2, Lcom/google/android/shared/search/SearchBoxStats;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v17

    check-cast v17, Lcom/google/android/shared/search/SearchBoxStats;

    .line 2160
    const-class v2, Landroid/location/Location;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v18

    check-cast v18, Landroid/location/Location;

    .line 2161
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v19

    .line 2162
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v20

    .line 2164
    const-class v2, Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;

    move-result-object v21

    .line 2165
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v22

    .line 2166
    const-class v2, Landroid/net/Uri;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v23

    check-cast v23, Landroid/net/Uri;

    .line 2167
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v24

    .line 2168
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v28

    .line 2169
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v29

    .line 2170
    new-instance v2, Lcom/google/android/shared/search/Query;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/shared/search/Query;->amT:J

    move-wide/from16 v26, v0

    invoke-direct/range {v2 .. v30}, Lcom/google/android/shared/search/Query;-><init>(ILjava/lang/CharSequence;Lijj;Ljava/lang/String;Lehm;Ljava/lang/String;IILjava/lang/String;ILjava/util/Map;JLjava/lang/String;Lcom/google/android/shared/search/SearchBoxStats;Landroid/location/Location;Ljava/lang/String;Ljava/io/Serializable;Landroid/os/Bundle;Ljava/lang/String;Landroid/net/Uri;JJIJ)V

    return-object v2
.end method

.method public final aS(Lcom/google/android/shared/search/Query;)Lcom/google/android/shared/search/Query;
    .locals 4

    .prologue
    .line 1035
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    iget-wide v2, p1, Lcom/google/android/shared/search/Query;->amT:J

    invoke-virtual {v0, v2, v3}, Lehh;->aG(J)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final aT(Lcom/google/android/shared/search/Query;)Lcom/google/android/shared/search/Query;
    .locals 4

    .prologue
    .line 1053
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    iget-wide v2, p1, Lcom/google/android/shared/search/Query;->bZu:J

    invoke-virtual {v0, v2, v3}, Lehh;->aF(J)Lehh;

    move-result-object v0

    iget-wide v2, p1, Lcom/google/android/shared/search/Query;->amT:J

    invoke-virtual {v0, v2, v3}, Lehh;->aG(J)Lehh;

    move-result-object v0

    iget-wide v2, p1, Lcom/google/android/shared/search/Query;->bZB:J

    invoke-virtual {v0, v2, v3}, Lehh;->aH(J)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final aU(Lcom/google/android/shared/search/Query;)Lcom/google/android/shared/search/Query;
    .locals 4

    .prologue
    const/16 v1, 0x4000

    .line 1066
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    iget-wide v2, p1, Lcom/google/android/shared/search/Query;->bZu:J

    invoke-virtual {v0, v2, v3}, Lehh;->aF(J)Lehh;

    move-result-object v0

    iget-wide v2, p1, Lcom/google/android/shared/search/Query;->amT:J

    invoke-virtual {v0, v2, v3}, Lehh;->aG(J)Lehh;

    move-result-object v0

    iget-wide v2, p1, Lcom/google/android/shared/search/Query;->bZB:J

    invoke-virtual {v0, v2, v3}, Lehh;->aH(J)Lehh;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v1, v0}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aV(Lcom/google/android/shared/search/Query;)Lcom/google/android/shared/search/Query;
    .locals 4

    .prologue
    .line 1169
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    iget-wide v2, p1, Lcom/google/android/shared/search/Query;->bZA:J

    invoke-virtual {v0, v2, v3}, Lehh;->aI(J)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final aW(Lcom/google/android/shared/search/Query;)Z
    .locals 4

    .prologue
    .line 1299
    iget-wide v0, p0, Lcom/google/android/shared/search/Query;->bZu:J

    iget-wide v2, p1, Lcom/google/android/shared/search/Query;->bZu:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final afX()Z
    .locals 2

    .prologue
    .line 1668
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aiM()Z
    .locals 1

    .prologue
    .line 1506
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    and-int/lit16 v0, v0, 0x4000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final alf()Z
    .locals 1

    .prologue
    .line 1543
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aoO()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1577
    iget-wide v0, p0, Lcom/google/android/shared/search/Query;->amT:J

    invoke-static {v0, v1}, Leoi;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aoQ()Z
    .locals 2

    .prologue
    .line 1423
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aoY()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 531
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/high16 v1, 0x400000

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final aoZ()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 539
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apA()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 1117
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/high16 v1, 0x10000000

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apB()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 1121
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/high16 v1, 0x10000000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apC()Z
    .locals 2

    .prologue
    const/high16 v1, 0x10000000

    .line 1125
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final apD()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 1129
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/high16 v1, 0x100000

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apE()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 1133
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/high16 v1, 0x100000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apF()Z
    .locals 2

    .prologue
    .line 1137
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final apG()Lcom/google/android/shared/search/Query;
    .locals 4

    .prologue
    .line 1159
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lehh;->aI(J)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apH()Lcom/google/android/shared/search/Query;
    .locals 5

    .prologue
    const/high16 v4, 0x8000000

    const v3, 0x8000

    const/4 v1, 0x0

    .line 1252
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->ard()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Received an auth failure for request without tokens."

    invoke-static {v0, v2}, Lifv;->d(ZLjava/lang/Object;)V

    .line 1255
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->arc()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1257
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, v3, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    const/high16 v2, 0x10000

    invoke-virtual {v0, v1, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0, v1, v4}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->ari()Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1262
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 1252
    goto :goto_0

    .line 1262
    :cond_1
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, v1, v3}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0, v1, v4}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->ari()Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_1
.end method

.method public final apI()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 1272
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->ari()Lehh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lehh;->b(Lehm;)Lehh;

    move-result-object v0

    const/high16 v1, 0x8000000

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Lehh;->hC(I)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apJ()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 1285
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->ari()Lehh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lehh;->b(Lehm;)Lehh;

    move-result-object v0

    const/high16 v1, 0x8000000

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apK()Z
    .locals 1

    .prologue
    .line 1294
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqo()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apY()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final apL()Z
    .locals 1

    .prologue
    .line 1305
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apY()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqU()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final apM()Z
    .locals 2

    .prologue
    .line 1315
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqD()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqz()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    const/high16 v1, 0x8000000

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final apN()Z
    .locals 2

    .prologue
    .line 1322
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    const/high16 v1, 0x20000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZr:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final apO()Z
    .locals 2

    .prologue
    .line 1326
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final apP()Z
    .locals 1

    .prologue
    .line 1331
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apY()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final apQ()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1335
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final apR()Lijj;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1339
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZo:Lijj;

    return-object v0
.end method

.method public final apS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1347
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZr:Ljava/lang/String;

    return-object v0
.end method

.method public final apT()Lehm;
    .locals 1

    .prologue
    .line 1351
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZq:Lehm;

    return-object v0
.end method

.method public final apU()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1363
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZw:Ljava/lang/String;

    return-object v0
.end method

.method public final apV()I
    .locals 1

    .prologue
    .line 1367
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1368
    iget v0, p0, Lcom/google/android/shared/search/Query;->bsH:I

    .line 1370
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final apX()Z
    .locals 1

    .prologue
    .line 1383
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final apY()Z
    .locals 1

    .prologue
    .line 1387
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    and-int/lit8 v0, v0, 0xf

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final apZ()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1391
    iget v1, p0, Lcom/google/android/shared/search/Query;->cZ:I

    and-int/lit8 v1, v1, 0xf

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final apa()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 726
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/high16 v1, 0x2000000

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apb()Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    .line 738
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lehh;->hB(I)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apc()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 743
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lehh;->D(Ljava/lang/CharSequence;)Lehh;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Lehh;->aK(II)Lehh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lehh;->hB(I)Lehh;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apd()Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    .line 755
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lehh;->hB(I)Lehh;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lehh;->hC(I)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final ape()Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    .line 762
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lehh;->hB(I)Lehh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lehh;->hC(I)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apf()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 769
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lehh;->D(Ljava/lang/CharSequence;)Lehh;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Lehh;->aK(II)Lehh;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lehh;->hB(I)Lehh;

    move-result-object v0

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Lehh;->hC(I)Lehh;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apg()Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    .line 779
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lehh;->hB(I)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final aph()Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 783
    invoke-direct {p0, v0, v0, v0}, Lcom/google/android/shared/search/Query;->c(IZZ)Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final api()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 794
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lehh;->hB(I)Lehh;

    move-result-object v0

    const/16 v1, 0xd0

    invoke-virtual {v0, v1}, Lehh;->hC(I)Lehh;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    const/16 v1, 0x200

    invoke-virtual {v0, v1, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Lehh;->aK(II)Lehh;

    move-result-object v0

    invoke-virtual {v0, v2}, Lehh;->hE(I)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apj()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 812
    const/16 v0, 0x80

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqz()Z

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/shared/search/Query;->c(IZZ)Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apk()Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 819
    const/16 v0, 0x50

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/shared/search/Query;->c(IZZ)Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apl()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 826
    const/16 v0, 0x50

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/shared/search/Query;->c(IZZ)Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apm()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 834
    const/16 v0, 0x50

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/shared/search/Query;->c(IZZ)Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apn()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 842
    const/16 v0, 0xe0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/shared/search/Query;->c(IZZ)Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apo()Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 857
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/shared/search/Query;->c(IZZ)Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final app()Lcom/google/android/shared/search/Query;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 960
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arh()Lehh;

    move-result-object v0

    const v1, 0x8000

    invoke-virtual {v0, v1, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    const/high16 v1, 0x10000

    invoke-virtual {v0, v1, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->ari()Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arj()Lehh;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lehh;->aI(J)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apq()Lcom/google/android/shared/search/Query;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 971
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arh()Lehh;

    move-result-object v0

    const v2, 0x8000

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    const/high16 v2, 0x10000

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->ari()Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arj()Lehh;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lehh;->aI(J)Lehh;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/shared/search/Query;->bZq:Lehm;

    invoke-virtual {v0, v2}, Lehh;->b(Lehm;)Lehh;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apM()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v1, v0}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/shared/search/Query;->bZr:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lehh;->kZ(Ljava/lang/String;)Lehh;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apN()Z

    move-result v0

    if-eqz v0, :cond_1

    const/high16 v0, 0x20000000

    :goto_1
    invoke-virtual {v2, v1, v0}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0

    :cond_0
    const/high16 v0, 0x8000000

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final apr()Lcom/google/android/shared/search/Query;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 986
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/high16 v1, 0x20000

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lehh;->d(Lcom/google/android/shared/search/SearchBoxStats;)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->ark()Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arj()Lehh;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lehh;->aI(J)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final aps()Lcom/google/android/shared/search/Query;
    .locals 4

    .prologue
    .line 997
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lehh;->d(Lcom/google/android/shared/search/SearchBoxStats;)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->ark()Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arl()Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arj()Lehh;

    move-result-object v0

    const/16 v1, 0x400

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lehh;->aI(J)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apt()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1008
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, v2}, Lehh;->hC(I)Lehh;

    move-result-object v0

    invoke-virtual {v0, v2}, Lehh;->hB(I)Lehh;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lehh;->D(Ljava/lang/CharSequence;)Lehh;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Lehh;->aK(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apu()Lcom/google/android/shared/search/Query;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1017
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 1018
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqo()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 1019
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    invoke-static {v1}, Lifv;->gY(Z)V

    .line 1020
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lehh;->d(Lcom/google/android/shared/search/SearchBoxStats;)Lehh;

    move-result-object v0

    invoke-virtual {v0, v2}, Lehh;->hB(I)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arj()Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->ari()Lehh;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lehh;->aI(J)Lehh;

    move-result-object v0

    const/high16 v1, 0x8000000

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 1017
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1018
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1019
    goto :goto_2
.end method

.method public final apv()Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 1031
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arj()Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apw()Lcom/google/android/shared/search/Query;
    .locals 4

    .prologue
    .line 1039
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->ari()Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arj()Lehh;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lehh;->aI(J)Lehh;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v1}, Lehh;->aK(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apx()Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    .line 1075
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Lehh;->hC(I)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apy()Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    .line 1087
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/16 v1, 0x90

    invoke-virtual {v0, v1}, Lehh;->hC(I)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final apz()Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 1091
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lehh;->d(Lcom/google/android/shared/search/SearchBoxStats;)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arj()Lehh;

    move-result-object v0

    const/16 v1, 0x2000

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final aqA()Z
    .locals 1

    .prologue
    .line 1522
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->ZB()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqB()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqD()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqf()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqB()Z
    .locals 2

    .prologue
    .line 1527
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    const/high16 v1, 0x400000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqC()Z
    .locals 2

    .prologue
    .line 1531
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    const/high16 v1, 0x40000000    # 2.0f

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqD()Z
    .locals 2

    .prologue
    .line 1535
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    const/high16 v1, 0x800000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqE()Z
    .locals 2

    .prologue
    .line 1539
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    const/high16 v1, 0x1000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqF()Z
    .locals 1

    .prologue
    .line 1547
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqG()Z
    .locals 2

    .prologue
    .line 1551
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    const/high16 v1, 0x2000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqH()Z
    .locals 2

    .prologue
    .line 1555
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    const/high16 v1, 0x4000000

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqI()Ljava/io/Serializable;
    .locals 1

    .prologue
    .line 1563
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 1564
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZn:Ljava/io/Serializable;

    return-object v0
.end method

.method public final aqJ()J
    .locals 2

    .prologue
    .line 1572
    iget-wide v0, p0, Lcom/google/android/shared/search/Query;->bZu:J

    return-wide v0
.end method

.method public final aqK()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1585
    iget-wide v0, p0, Lcom/google/android/shared/search/Query;->bZB:J

    invoke-static {v0, v1}, Leoi;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aqL()J
    .locals 2

    .prologue
    .line 1589
    iget-wide v0, p0, Lcom/google/android/shared/search/Query;->bZB:J

    return-wide v0
.end method

.method public final aqM()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1593
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZv:Ljava/lang/String;

    return-object v0
.end method

.method public final aqN()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1602
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    const/16 v1, 0x20

    invoke-static {v0, v1}, Letd;->d(Ljava/lang/CharSequence;C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final aqO()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1612
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apC()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1613
    const-string v0, ""

    .line 1616
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    const/16 v1, 0x20

    invoke-static {v0, v1}, Letd;->c(Ljava/lang/CharSequence;C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final aqP()Z
    .locals 1

    .prologue
    .line 1623
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqO()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public final aqQ()Lcom/google/android/shared/search/SearchBoxStats;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1627
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->mSearchBoxStats:Lcom/google/android/shared/search/SearchBoxStats;

    return-object v0
.end method

.method public final aqR()I
    .locals 1

    .prologue
    .line 1643
    iget v0, p0, Lcom/google/android/shared/search/Query;->bZs:I

    return v0
.end method

.method public final aqS()Lijm;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1647
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZt:Lijm;

    return-object v0
.end method

.method public final aqT()Z
    .locals 2

    .prologue
    .line 1656
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZw:Ljava/lang/String;

    const-string v1, "web.isch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final aqU()Z
    .locals 2

    .prologue
    .line 1660
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZw:Ljava/lang/String;

    const-string v1, "summons"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final aqV()Z
    .locals 2

    .prologue
    .line 1664
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZw:Ljava/lang/String;

    const-string v1, "web"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final aqW()Landroid/location/Location;
    .locals 1

    .prologue
    .line 1672
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZx:Landroid/location/Location;

    return-object v0
.end method

.method public final aqX()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1676
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZy:Ljava/lang/String;

    return-object v0
.end method

.method public final aqY()Landroid/net/Uri;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1684
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZz:Landroid/net/Uri;

    return-object v0
.end method

.method public final aqZ()Z
    .locals 1

    .prologue
    .line 1688
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZz:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqa()Z
    .locals 1

    .prologue
    .line 1395
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqb()Z
    .locals 2

    .prologue
    .line 1399
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    const/16 v1, 0x60

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqc()Z
    .locals 2

    .prologue
    .line 1403
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    const/16 v1, 0xa0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqd()Z
    .locals 2

    .prologue
    .line 1407
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    const/16 v1, 0x50

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqe()Z
    .locals 2

    .prologue
    .line 1411
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    const/16 v1, 0xd0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqf()Z
    .locals 2

    .prologue
    .line 1415
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqg()Z
    .locals 2

    .prologue
    .line 1419
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    const/16 v1, 0x70

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqh()Z
    .locals 2

    .prologue
    .line 1428
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    const/16 v1, 0xb0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqi()Z
    .locals 2

    .prologue
    .line 1432
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    const/16 v1, 0xe0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqj()Z
    .locals 1

    .prologue
    .line 1436
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZq:Lehm;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apM()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqk()Z
    .locals 2

    .prologue
    .line 1440
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aql()Z
    .locals 2

    .prologue
    .line 1444
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqm()Z
    .locals 1

    .prologue
    .line 1448
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqn()Z
    .locals 2

    .prologue
    .line 1452
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqo()Z
    .locals 2

    .prologue
    .line 1456
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqp()Z
    .locals 2

    .prologue
    .line 1460
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqq()Z
    .locals 2

    .prologue
    .line 1464
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    const/16 v1, 0x30

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    const/16 v1, 0xc0

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqr()Z
    .locals 2

    .prologue
    .line 1468
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    const/16 v1, 0xc0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqs()Z
    .locals 2

    .prologue
    .line 1472
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    const/16 v1, 0x30

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqt()Z
    .locals 2

    .prologue
    .line 1476
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqu()Z
    .locals 1

    .prologue
    .line 1481
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqv()Z
    .locals 1

    .prologue
    .line 1486
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqw()Z
    .locals 2

    .prologue
    .line 1490
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqx()Z
    .locals 2

    .prologue
    .line 1494
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqy()Z
    .locals 2

    .prologue
    .line 1498
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->apW()I

    move-result v0

    const/16 v1, 0x90

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aqz()Z
    .locals 2

    .prologue
    .line 1514
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    const/high16 v1, 0x200000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ara()Z
    .locals 4

    .prologue
    .line 1692
    iget-wide v0, p0, Lcom/google/android/shared/search/Query;->bZA:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final arb()J
    .locals 2

    .prologue
    .line 1696
    iget-wide v0, p0, Lcom/google/android/shared/search/Query;->bZA:J

    return-wide v0
.end method

.method public final arc()Z
    .locals 2

    .prologue
    .line 1700
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    const v1, 0x8000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ard()Z
    .locals 2

    .prologue
    .line 1704
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final az(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/shared/search/Query;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 947
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 948
    const-string v1, "notification-message"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 949
    const-string v1, "notification-sender"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 950
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lehh;->hB(I)Lehh;

    move-result-object v1

    const/16 v2, 0x4000

    invoke-virtual {v1, v3, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v1

    const/high16 v2, 0x40000

    invoke-virtual {v1, v3, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v1

    const/16 v2, 0x400

    invoke-virtual {v1, v3, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v1

    invoke-virtual {v1, v0}, Lehh;->G(Landroid/os/Bundle;)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/net/Uri;Landroid/os/Bundle;)Lcom/google/android/shared/search/Query;
    .locals 3
    .param p1    # Landroid/net/Uri;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 714
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lehh;->hB(I)Lehh;

    move-result-object v0

    const/16 v1, 0x200

    invoke-virtual {v0, v1, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    const/high16 v1, 0x1000000

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1, v2}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->H(Landroid/net/Uri;)Lehh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lehh;->kY(Ljava/lang/String;)Lehh;

    move-result-object v0

    invoke-virtual {v0, p2}, Lehh;->G(Landroid/os/Bundle;)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/CharSequence;I)Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 570
    invoke-virtual {p0, p1, p2, p2}, Lcom/google/android/shared/search/Query;->c(Ljava/lang/CharSequence;II)Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Class;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1634
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    iget-object v2, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-interface {v0, v1, v2, p1}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 663
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->d(Lcom/google/android/shared/search/SearchBoxStats;)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/CharSequence;II)Lcom/google/android/shared/search/Query;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 578
    invoke-static {p1, p2}, Lcom/google/android/shared/search/Query;->c(Ljava/lang/CharSequence;I)Z

    move-result v0

    const-string v1, "selectionStart bad %s %s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Lifv;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 580
    invoke-static {p1, p3}, Lcom/google/android/shared/search/Query;->c(Ljava/lang/CharSequence;I)Z

    move-result v0

    const-string v1, "selectionEnd bad %s %s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Lifv;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 583
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Leqt;->c(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 585
    iget v0, p0, Lcom/google/android/shared/search/Query;->bKR:I

    if-ne p2, v0, :cond_0

    iget v0, p0, Lcom/google/android/shared/search/Query;->bKS:I

    if-ne p3, v0, :cond_0

    .line 593
    :goto_0
    return-object p0

    .line 589
    :cond_0
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, v4}, Lehh;->hC(I)Lehh;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lehh;->aK(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object p0

    goto :goto_0

    .line 593
    :cond_1
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, v4}, Lehh;->hC(I)Lehh;

    move-result-object v0

    invoke-virtual {v0, v4}, Lehh;->hB(I)Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->D(Ljava/lang/CharSequence;)Lehh;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lehh;->aK(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object p0

    goto :goto_0
.end method

.method public final c(Ljava/lang/CharSequence;Z)Lcom/google/android/shared/search/Query;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, -0x2

    .line 691
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->D(Ljava/lang/CharSequence;)Lehh;

    move-result-object v3

    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0, v2}, Lehh;->aK(II)Lehh;

    move-result-object v0

    const/16 v2, 0x10

    invoke-virtual {v0, v2}, Lehh;->hC(I)Lehh;

    move-result-object v0

    invoke-virtual {v0, v1}, Lehh;->hB(I)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 2138
    const/4 v0, 0x0

    return v0
.end method

.method public final eQ(Z)Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    .line 846
    const/16 v0, 0x60

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/shared/search/Query;->c(IZZ)Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final eR(Z)Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    .line 850
    const/16 v0, 0x70

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/shared/search/Query;->c(IZZ)Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1568
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->cF:Landroid/os/Bundle;

    return-object v0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2372
    const-string v0, "Velvet.Query"

    return-object v0
.end method

.method public final getOriginalUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1343
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZp:Ljava/lang/String;

    return-object v0
.end method

.method public final getSelectionEnd()I
    .locals 1

    .prologue
    .line 1359
    iget v0, p0, Lcom/google/android/shared/search/Query;->bKS:I

    return v0
.end method

.method public final getSelectionStart()I
    .locals 1

    .prologue
    .line 1355
    iget v0, p0, Lcom/google/android/shared/search/Query;->bKR:I

    return v0
.end method

.method public final hz(I)Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 677
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 678
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->hE(I)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final kT(Ljava/lang/String;)Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    .line 515
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 516
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lehh;->p(Ljava/lang/String;Z)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final kU(Ljava/lang/String;)Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    .line 520
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lehh;->p(Ljava/lang/String;Z)Lehh;

    move-result-object v0

    const/16 v1, 0xa0

    invoke-virtual {v0, v1}, Lehh;->hC(I)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final kV(Ljava/lang/String;)Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 599
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->D(Ljava/lang/CharSequence;)Lehh;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1, v1}, Lehh;->aK(II)Lehh;

    move-result-object v0

    const/16 v1, 0x1000

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final kW(Ljava/lang/String;)Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 732
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v1

    iget-boolean v2, v1, Lehh;->bbz:Z

    iget-object v0, v1, Lehh;->bwM:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v2

    iput-boolean v0, v1, Lehh;->bbz:Z

    iput-object p1, v1, Lehh;->bwM:Ljava/lang/String;

    invoke-virtual {v1}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final kX(Ljava/lang/String;)Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 931
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lehh;->hB(I)Lehh;

    move-result-object v0

    const/16 v1, 0x200

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lehh;->H(Landroid/net/Uri;)Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->kY(Ljava/lang/String;)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final l(Ljava/util/Map;)Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 2300
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->m(Ljava/util/Map;)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final n(Ljava/lang/String;J)Lcom/google/android/shared/search/Query;
    .locals 4

    .prologue
    .line 655
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->kZ(Ljava/lang/String;)Lehh;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lehh;->aI(J)Lehh;

    move-result-object v0

    const/high16 v1, 0x20000000

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2258
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    if-ne p0, v0, :cond_0

    .line 2259
    const-string v0, "Query[EMPTY]"

    .line 2261
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Query["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->getTypeString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->arf()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\"/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZw:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/shared/search/Query;->bZs:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " CID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/shared/search/Query;->bZu:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mResendAudioRequestId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Query;->bZv:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sel-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/shared/search/Query;->bKR:I

    invoke-static {v1}, Lcom/google/android/shared/search/Query;->hA(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/shared/search/Query;->bKS:I

    invoke-static {v1}, Lcom/google/android/shared/search/Query;->hA(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->alf()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ", play-tts"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ", eyes-free"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, ", voice-search"

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqB()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, ", is-full-srp"

    :goto_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqC()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, ", opaque-actions"

    :goto_5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqz()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, ", gpm"

    :goto_6
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqA()Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, ", assist"

    :goto_7
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqF()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, ", resend-last-recording"

    :goto_8
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->afX()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, ", rewritten"

    :goto_9
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/shared/search/Query;->mSearchBoxStats:Lcom/google/android/shared/search/SearchBoxStats;

    if-eqz v0, :cond_a

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ", client-stats:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/shared/search/Query;->mSearchBoxStats:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_a
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apO()Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, ", from-back-stack"

    :goto_b
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqu()Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, ", restored-state"

    :goto_c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZx:Landroid/location/Location;

    if-eqz v0, :cond_d

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ", location-override:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/shared/search/Query;->bZx:Landroid/location/Location;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZy:Ljava/lang/String;

    if-eqz v0, :cond_e

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ", stick:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/shared/search/Query;->bZy:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_e
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ", persist-cgi-parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/shared/search/Query;->bZt:Lijm;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bwM:Ljava/lang/String;

    if-eqz v0, :cond_f

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ", assist-package:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/shared/search/Query;->bwM:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_f
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZz:Landroid/net/Uri;

    if-eqz v0, :cond_10

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ", recording-uri:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/shared/search/Query;->bZz:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_10
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->ara()Z

    move-result v0

    if-eqz v0, :cond_11

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ", submission-time:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/shared/search/Query;->bZA:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_11
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " uiToLaunchForVoiceSearch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/shared/search/Query;->bsH:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZq:Lehm;

    if-eqz v0, :cond_12

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ", webAppRequest: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/shared/search/Query;->bZq:Lehm;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_12
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZp:Ljava/lang/String;

    if-eqz v0, :cond_13

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ", originalUrl: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/shared/search/Query;->bZp:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_13
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZr:Ljava/lang/String;

    if-eqz v0, :cond_14

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ", substate: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/shared/search/Query;->bZr:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_14
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_1
    const-string v0, ""

    goto/16 :goto_1

    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    :cond_3
    const-string v0, ""

    goto/16 :goto_3

    :cond_4
    const-string v0, ""

    goto/16 :goto_4

    :cond_5
    const-string v0, ""

    goto/16 :goto_5

    :cond_6
    const-string v0, ""

    goto/16 :goto_6

    :cond_7
    const-string v0, ""

    goto/16 :goto_7

    :cond_8
    const-string v0, ""

    goto/16 :goto_8

    :cond_9
    const-string v0, ""

    goto/16 :goto_9

    :cond_a
    const-string v0, ""

    goto/16 :goto_a

    :cond_b
    const-string v0, ""

    goto/16 :goto_b

    :cond_c
    const-string v0, ""

    goto/16 :goto_c

    :cond_d
    const-string v0, ""

    goto/16 :goto_d

    :cond_e
    const-string v0, ""

    goto/16 :goto_e

    :cond_f
    const-string v0, ""

    goto/16 :goto_f

    :cond_10
    const-string v0, ""

    goto/16 :goto_10

    :cond_11
    const-string v0, ""

    goto/16 :goto_11

    :cond_12
    const-string v0, ""

    goto/16 :goto_12

    :cond_13
    const-string v0, ""

    goto :goto_13

    :cond_14
    const-string v0, ""

    goto :goto_14
.end method

.method public final v(Ljava/lang/String;I)Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 606
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, v1}, Lehh;->hB(I)Lehh;

    move-result-object v0

    invoke-virtual {v0, v1}, Lehh;->hC(I)Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->D(Ljava/lang/CharSequence;)Lehh;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1, v1}, Lehh;->aK(II)Lehh;

    move-result-object v0

    invoke-virtual {v0, p2}, Lehh;->hD(I)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final v(ZZ)Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 883
    const/16 v0, 0xb0

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/shared/search/Query;->c(IZZ)Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2112
    iget v0, p0, Lcom/google/android/shared/search/Query;->cZ:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2113
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    invoke-static {v0, p1, p2}, Leqt;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 2114
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZo:Lijj;

    invoke-static {v0, p1, p2}, Leqt;->a(Ljava/util/Collection;Landroid/os/Parcel;I)V

    .line 2115
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZp:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2116
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2117
    iget v0, p0, Lcom/google/android/shared/search/Query;->bKR:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2118
    iget v0, p0, Lcom/google/android/shared/search/Query;->bKS:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2119
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZw:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2120
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZt:Lijm;

    invoke-static {v0}, Lesp;->o(Ljava/util/Map;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 2121
    iget v0, p0, Lcom/google/android/shared/search/Query;->bZs:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2122
    iget-wide v0, p0, Lcom/google/android/shared/search/Query;->bZu:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2123
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZv:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2124
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->mSearchBoxStats:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2125
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZx:Landroid/location/Location;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2126
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZy:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2127
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZn:Ljava/io/Serializable;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2128
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->cF:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 2129
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bwM:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2130
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bZz:Landroid/net/Uri;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2131
    iget-wide v0, p0, Lcom/google/android/shared/search/Query;->bZA:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2132
    iget v0, p0, Lcom/google/android/shared/search/Query;->bsH:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2133
    iget-wide v0, p0, Lcom/google/android/shared/search/Query;->bZB:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2134
    return-void
.end method

.method public final x(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 550
    iget-object v0, p0, Lcom/google/android/shared/search/Query;->bKQ:Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Leqt;->c(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 553
    :goto_0
    return-object p0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, v1}, Lehh;->hC(I)Lehh;

    move-result-object v0

    invoke-virtual {v0, v1}, Lehh;->hB(I)Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->D(Ljava/lang/CharSequence;)Lehh;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {v0, v1, v1}, Lehh;->aK(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object p0

    goto :goto_0
.end method

.method public final y(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1145
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->D(Ljava/lang/CharSequence;)Lehh;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->afX()Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, 0x80000

    :goto_0
    invoke-virtual {v2, v1, v0}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final z(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;
    .locals 3

    .prologue
    .line 1152
    invoke-direct {p0}, Lcom/google/android/shared/search/Query;->are()Lehh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lehh;->D(Ljava/lang/CharSequence;)Lehh;

    move-result-object v0

    const/high16 v1, 0x80000

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lehh;->aJ(II)Lehh;

    move-result-object v0

    invoke-virtual {v0}, Lehh;->arm()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method
