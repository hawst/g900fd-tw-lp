.class public Lcom/google/android/shared/search/Suggestion;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final bZN:[Ljava/lang/String;

.field private static final bZO:Liqa;

.field private static final bZP:Liqa;

.field private static final bZQ:Liqa;


# instance fields
.field private final bZR:I

.field private bZS:Ljava/lang/String;

.field private final bjh:J

.field private final bqt:Lcom/google/android/shared/util/BitFlags;

.field private final buA:Ljava/lang/String;

.field private final buB:Ljava/lang/String;

.field private final buC:J

.field private final buD:Ljava/lang/String;

.field private final buE:Ljava/lang/String;

.field private final buF:Ljava/lang/String;

.field private final buG:Ljava/lang/String;

.field private final buI:Landroid/content/ComponentName;

.field private final buJ:Ljava/lang/String;

.field private final buK:Ljava/lang/String;

.field private final buL:I

.field private final buM:Ljava/util/ArrayList;

.field private final buv:Ljava/lang/String;

.field private final buw:Ljava/lang/String;

.field private final bux:Ljava/lang/CharSequence;

.field private final buy:Ljava/lang/CharSequence;

.field private final buz:Ljava/lang/String;

.field private final bva:Ljava/lang/String;

.field private final bvb:Ljava/lang/String;

.field private final bvc:Ljava/lang/String;

.field private final bvg:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 56
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "www."

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "www1."

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "www2."

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "m."

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/shared/search/Suggestion;->bZN:[Ljava/lang/String;

    .line 57
    invoke-static {}, Liqa;->aYi()Liqa;

    move-result-object v0

    invoke-virtual {v0}, Liqa;->aYg()Liqa;

    move-result-object v0

    sput-object v0, Lcom/google/android/shared/search/Suggestion;->bZO:Liqa;

    .line 58
    invoke-static {}, Liqa;->aYj()Liqa;

    move-result-object v0

    invoke-virtual {v0}, Liqa;->aYg()Liqa;

    move-result-object v0

    sput-object v0, Lcom/google/android/shared/search/Suggestion;->bZP:Liqa;

    .line 60
    invoke-static {}, Liqa;->aYi()Liqa;

    move-result-object v0

    invoke-virtual {v0}, Liqa;->aYk()Liqa;

    move-result-object v0

    invoke-virtual {v0}, Liqa;->aYg()Liqa;

    move-result-object v0

    sput-object v0, Lcom/google/android/shared/search/Suggestion;->bZQ:Liqa;

    .line 689
    new-instance v0, Lehk;

    invoke-direct {v0}, Lehk;-><init>()V

    sput-object v0, Lcom/google/android/shared/search/Suggestion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buv:Ljava/lang/String;

    .line 204
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buw:Ljava/lang/String;

    .line 205
    invoke-static {p1}, Leqt;->aC(Landroid/os/Parcel;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bux:Ljava/lang/CharSequence;

    .line 206
    invoke-static {p1}, Leqt;->aC(Landroid/os/Parcel;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buy:Ljava/lang/CharSequence;

    .line 207
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buz:Ljava/lang/String;

    .line 208
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buA:Ljava/lang/String;

    .line 209
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buB:Ljava/lang/String;

    .line 210
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/shared/search/Suggestion;->buC:J

    .line 211
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buD:Ljava/lang/String;

    .line 212
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buE:Ljava/lang/String;

    .line 213
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buF:Ljava/lang/String;

    .line 214
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buG:Ljava/lang/String;

    .line 215
    const-class v0, Landroid/content/ComponentName;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buI:Landroid/content/ComponentName;

    .line 216
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buJ:Ljava/lang/String;

    .line 217
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buK:Ljava/lang/String;

    .line 218
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/Suggestion;->buL:I

    .line 219
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buM:Ljava/util/ArrayList;

    .line 220
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bva:Ljava/lang/String;

    .line 221
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bvb:Ljava/lang/String;

    .line 222
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bvc:Ljava/lang/String;

    .line 223
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/shared/search/Suggestion;->bjh:J

    .line 224
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/Suggestion;->bvg:I

    .line 225
    const-class v0, Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/BitFlags;

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    .line 226
    invoke-direct {p0}, Lcom/google/android/shared/search/Suggestion;->asH()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/Suggestion;->bZR:I

    .line 227
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ComponentName;Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;ZZZZZZZZZZZZZZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJI)V
    .locals 6

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    if-nez p27, :cond_0

    if-nez p37, :cond_3

    if-nez p38, :cond_3

    :cond_0
    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lifv;->gX(Z)V

    .line 132
    if-nez p39, :cond_1

    if-nez p27, :cond_4

    :cond_1
    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Lifv;->gX(Z)V

    .line 134
    if-ltz p42, :cond_2

    if-eqz p30, :cond_5

    if-nez p39, :cond_5

    :cond_2
    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, Lifv;->gX(Z)V

    .line 135
    iput-object p2, p0, Lcom/google/android/shared/search/Suggestion;->buv:Ljava/lang/String;

    .line 136
    iput-object p1, p0, Lcom/google/android/shared/search/Suggestion;->buw:Ljava/lang/String;

    .line 137
    iput-object p3, p0, Lcom/google/android/shared/search/Suggestion;->buB:Ljava/lang/String;

    .line 138
    iput-object p4, p0, Lcom/google/android/shared/search/Suggestion;->bux:Ljava/lang/CharSequence;

    .line 139
    iput-object p5, p0, Lcom/google/android/shared/search/Suggestion;->buy:Ljava/lang/CharSequence;

    .line 140
    iput-object p6, p0, Lcom/google/android/shared/search/Suggestion;->buz:Ljava/lang/String;

    .line 141
    iput-object p7, p0, Lcom/google/android/shared/search/Suggestion;->buA:Ljava/lang/String;

    .line 142
    iput-wide p8, p0, Lcom/google/android/shared/search/Suggestion;->buC:J

    .line 143
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buD:Ljava/lang/String;

    .line 144
    iget-object v2, p0, Lcom/google/android/shared/search/Suggestion;->buD:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 145
    if-nez p11, :cond_6

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, Lifv;->gX(Z)V

    .line 146
    if-nez p12, :cond_7

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, Lifv;->gX(Z)V

    .line 147
    if-nez p13, :cond_8

    const/4 v2, 0x1

    :goto_5
    invoke-static {v2}, Lifv;->gX(Z)V

    .line 148
    const/4 v2, 0x0

    .line 150
    const/4 v3, 0x0

    :try_start_0
    move-object/from16 v0, p10

    invoke-static {v0, v3}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 154
    :goto_6
    if-eqz v2, :cond_9

    .line 155
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buE:Ljava/lang/String;

    .line 156
    invoke-virtual {v2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/shared/search/Suggestion;->buF:Ljava/lang/String;

    .line 161
    :goto_7
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/shared/search/Suggestion;->buG:Ljava/lang/String;

    .line 167
    :goto_8
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buI:Landroid/content/ComponentName;

    .line 168
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buJ:Ljava/lang/String;

    .line 169
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buK:Ljava/lang/String;

    .line 170
    move/from16 v0, p17

    iput v0, p0, Lcom/google/android/shared/search/Suggestion;->buL:I

    .line 171
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buM:Ljava/util/ArrayList;

    .line 172
    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bva:Ljava/lang/String;

    .line 173
    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bvb:Ljava/lang/String;

    .line 174
    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bvc:Ljava/lang/String;

    .line 175
    move-wide/from16 v0, p40

    iput-wide v0, p0, Lcom/google/android/shared/search/Suggestion;->bjh:J

    .line 176
    move/from16 v0, p42

    iput v0, p0, Lcom/google/android/shared/search/Suggestion;->bvg:I

    .line 177
    new-instance v4, Lcom/google/android/shared/util/BitFlags;

    const-class v5, Lcom/google/android/shared/search/Suggestion;

    if-eqz p19, :cond_b

    const/4 v2, 0x1

    move v3, v2

    :goto_9
    if-eqz p21, :cond_c

    const/4 v2, 0x2

    :goto_a
    or-int/2addr v3, v2

    if-eqz p23, :cond_d

    const/4 v2, 0x4

    :goto_b
    or-int/2addr v3, v2

    if-eqz p24, :cond_e

    const/16 v2, 0x8

    :goto_c
    or-int/2addr v3, v2

    if-eqz p25, :cond_f

    const/16 v2, 0x10

    :goto_d
    or-int/2addr v3, v2

    if-eqz p22, :cond_10

    const/16 v2, 0x20

    :goto_e
    or-int/2addr v3, v2

    if-eqz p20, :cond_11

    const/16 v2, 0x40

    :goto_f
    or-int/2addr v3, v2

    if-eqz p26, :cond_12

    const/16 v2, 0x80

    :goto_10
    or-int/2addr v3, v2

    if-eqz p27, :cond_13

    const/16 v2, 0x100

    :goto_11
    or-int/2addr v3, v2

    if-eqz p28, :cond_14

    const/16 v2, 0x200

    :goto_12
    or-int/2addr v3, v2

    if-eqz p31, :cond_15

    const/16 v2, 0x400

    :goto_13
    or-int/2addr v3, v2

    if-eqz p32, :cond_16

    const/16 v2, 0x800

    :goto_14
    or-int/2addr v3, v2

    if-eqz p33, :cond_17

    const/16 v2, 0x1000

    :goto_15
    or-int/2addr v3, v2

    if-eqz p29, :cond_18

    const/16 v2, 0x2000

    :goto_16
    or-int/2addr v3, v2

    if-eqz p39, :cond_19

    const/16 v2, 0x4000

    :goto_17
    or-int/2addr v3, v2

    if-eqz p30, :cond_1a

    const v2, 0x8000

    :goto_18
    or-int/2addr v3, v2

    if-eqz p34, :cond_1b

    const/high16 v2, 0x10000

    :goto_19
    or-int/2addr v3, v2

    if-eqz p35, :cond_1c

    const/high16 v2, 0x20000

    :goto_1a
    or-int/2addr v2, v3

    int-to-long v2, v2

    invoke-direct {v4, v5, v2, v3}, Lcom/google/android/shared/util/BitFlags;-><init>(Ljava/lang/Class;J)V

    iput-object v4, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    .line 197
    invoke-direct {p0}, Lcom/google/android/shared/search/Suggestion;->asH()I

    move-result v2

    iput v2, p0, Lcom/google/android/shared/search/Suggestion;->bZR:I

    .line 199
    return-void

    .line 131
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 132
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 134
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 145
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 146
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 147
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 151
    :catch_0
    move-exception v3

    .line 152
    const-string v4, "Suggestion"

    const-string v5, "Wrong intent uri."

    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_6

    .line 158
    :cond_9
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/shared/search/Suggestion;->buE:Ljava/lang/String;

    .line 159
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/shared/search/Suggestion;->buF:Ljava/lang/String;

    goto/16 :goto_7

    .line 163
    :cond_a
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buE:Ljava/lang/String;

    .line 164
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buF:Ljava/lang/String;

    .line 165
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buG:Ljava/lang/String;

    goto/16 :goto_8

    .line 177
    :cond_b
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_9

    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_a

    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_b

    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_c

    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_d

    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_e

    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_f

    :cond_12
    const/4 v2, 0x0

    goto/16 :goto_10

    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_11

    :cond_14
    const/4 v2, 0x0

    goto/16 :goto_12

    :cond_15
    const/4 v2, 0x0

    goto/16 :goto_13

    :cond_16
    const/4 v2, 0x0

    goto/16 :goto_14

    :cond_17
    const/4 v2, 0x0

    goto :goto_15

    :cond_18
    const/4 v2, 0x0

    goto :goto_16

    :cond_19
    const/4 v2, 0x0

    goto :goto_17

    :cond_1a
    const/4 v2, 0x0

    goto :goto_18

    :cond_1b
    const/4 v2, 0x0

    goto :goto_19

    :cond_1c
    const/4 v2, 0x0

    goto :goto_1a
.end method

.method private asH()I
    .locals 3

    .prologue
    .line 472
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/shared/search/Suggestion;->buA:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/shared/search/Suggestion;->bux:Ljava/lang/CharSequence;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/shared/search/Suggestion;->buy:Ljava/lang/CharSequence;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/shared/search/Suggestion;->buD:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/shared/search/Suggestion;->buE:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/shared/search/Suggestion;->buF:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/shared/search/Suggestion;->buG:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/shared/search/Suggestion;->buL:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/shared/search/Suggestion;->buM:Ljava/util/ArrayList;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/shared/search/Suggestion;->buw:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/shared/search/Suggestion;->buJ:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private asI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buM:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 581
    const-string v0, ""

    .line 583
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static lb(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 587
    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    return-object p0
.end method

.method protected static lc(Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 604
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 653
    :cond_0
    :goto_0
    return-object p0

    .line 613
    :cond_1
    const-string v0, "://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 615
    const/4 v0, -0x1

    if-ne v1, v0, :cond_4

    .line 617
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 618
    const/4 v1, 0x4

    .line 619
    const/4 v0, 0x7

    move v2, v3

    .line 631
    :goto_1
    const-string v5, "http"

    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "https"

    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 632
    :cond_2
    sget-object v6, Lcom/google/android/shared/search/Suggestion;->bZN:[Ljava/lang/String;

    array-length v7, v6

    move v5, v3

    :goto_2
    if-ge v5, v7, :cond_3

    aget-object v8, v6, v5

    .line 633
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {p0, v0, v8, v3, v9}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 634
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v0, v2

    move v2, v4

    .line 642
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .line 643
    add-int/lit8 v6, v5, -0x1

    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x2f

    if-ne v6, v7, :cond_6

    .line 644
    add-int/lit8 v2, v5, -0x1

    .line 648
    :goto_3
    if-eqz v4, :cond_0

    .line 651
    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 652
    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 653
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 622
    :cond_4
    add-int/lit8 v0, v1, 0x3

    .line 623
    const/4 v2, 0x5

    .line 624
    if-ne v1, v2, :cond_7

    const-string v2, "https"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 625
    add-int/lit8 v1, v1, -0x1

    move v2, v4

    .line 626
    goto :goto_1

    .line 632
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_6
    move v4, v2

    move v2, v5

    goto :goto_3

    :cond_7
    move v2, v3

    goto :goto_1
.end method

.method public static s(Lcom/google/android/shared/search/Suggestion;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 703
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buF:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 704
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buF:Ljava/lang/String;

    .line 709
    :goto_0
    return-object v0

    .line 707
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buD:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 709
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final abO()Z
    .locals 4

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x80

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final acm()I
    .locals 1

    .prologue
    .line 297
    iget v0, p0, Lcom/google/android/shared/search/Suggestion;->buL:I

    return v0
.end method

.method public final arW()Ljava/lang/String;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buw:Ljava/lang/String;

    return-object v0
.end method

.method public final arX()Ljava/lang/String;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buv:Ljava/lang/String;

    return-object v0
.end method

.method public final arY()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bux:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final arZ()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buy:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final asA()Z
    .locals 4

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x1000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final asB()Ljava/lang/String;
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bva:Ljava/lang/String;

    return-object v0
.end method

.method public final asC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bvb:Ljava/lang/String;

    return-object v0
.end method

.method public final asD()Ljava/lang/String;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bvc:Ljava/lang/String;

    return-object v0
.end method

.method public final asE()J
    .locals 2

    .prologue
    .line 403
    iget-wide v0, p0, Lcom/google/android/shared/search/Suggestion;->bjh:J

    return-wide v0
.end method

.method public final asF()I
    .locals 1

    .prologue
    .line 411
    iget v0, p0, Lcom/google/android/shared/search/Suggestion;->bvg:I

    return v0
.end method

.method public final declared-synchronized asG()Ljava/lang/String;
    .locals 6

    .prologue
    .line 419
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bZS:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 420
    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->asx()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "NowPromo"

    :goto_0
    iput-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bZS:Ljava/lang/String;

    .line 422
    :cond_0
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bZS:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 420
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->asq()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->asz()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/shared/search/Suggestion;->bux:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Suggestion;->buy:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Suggestion;->buz:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->isStale()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->asr()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buz:Ljava/lang/String;

    :goto_1
    invoke-static {v0}, Lcom/google/android/shared/search/Suggestion;->lc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/Suggestion;->buE:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/shared/search/Suggestion;->lb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/shared/search/Suggestion;->lb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/shared/search/Suggestion;->buJ:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/shared/search/Suggestion;->lb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v5, 0x23

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0x23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/16 v0, 0x23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->isStale()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buF:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 419
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final asa()Ljava/lang/String;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buz:Ljava/lang/String;

    return-object v0
.end method

.method public final asb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buA:Ljava/lang/String;

    return-object v0
.end method

.method public final asc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buB:Ljava/lang/String;

    return-object v0
.end method

.method public final asd()J
    .locals 2

    .prologue
    .line 265
    iget-wide v0, p0, Lcom/google/android/shared/search/Suggestion;->buC:J

    return-wide v0
.end method

.method public final ase()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buD:Ljava/lang/String;

    return-object v0
.end method

.method public final asf()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buE:Ljava/lang/String;

    return-object v0
.end method

.method public final asg()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buI:Landroid/content/ComponentName;

    return-object v0
.end method

.method public final ash()Ljava/lang/String;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buF:Ljava/lang/String;

    return-object v0
.end method

.method public final asi()Ljava/lang/String;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buG:Ljava/lang/String;

    return-object v0
.end method

.method public final asj()Ljava/lang/String;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buJ:Ljava/lang/String;

    return-object v0
.end method

.method public final ask()Ljava/lang/String;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buK:Ljava/lang/String;

    return-object v0
.end method

.method public final asl()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buM:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final asm()Z
    .locals 4

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x100

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final asn()Z
    .locals 4

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x4000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final aso()Z
    .locals 4

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x400

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final asp()Z
    .locals 4

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x800

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final asq()Z
    .locals 4

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final asr()Z
    .locals 4

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x8

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final ass()Z
    .locals 4

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x10

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final ast()Z
    .locals 4

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x20

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final asu()Z
    .locals 4

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final asv()Z
    .locals 4

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x40

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final asw()Z
    .locals 4

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final asx()Z
    .locals 4

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x200

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final asy()Z
    .locals 4

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x8000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final asz()Z
    .locals 4

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x2000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 50
    check-cast p1, Lcom/google/android/shared/search/Suggestion;

    invoke-virtual {p0, p1}, Lcom/google/android/shared/search/Suggestion;->r(Lcom/google/android/shared/search/Suggestion;)I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 659
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 509
    if-ne p0, p1, :cond_1

    .line 517
    :cond_0
    :goto_0
    return v0

    .line 513
    :cond_1
    instance-of v2, p1, Lcom/google/android/shared/search/Suggestion;

    if-nez v2, :cond_2

    move v0, v1

    .line 514
    goto :goto_0

    .line 516
    :cond_2
    check-cast p1, Lcom/google/android/shared/search/Suggestion;

    .line 517
    invoke-virtual {p0, p1}, Lcom/google/android/shared/search/Suggestion;->r(Lcom/google/android/shared/search/Suggestion;)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 468
    iget v0, p0, Lcom/google/android/shared/search/Suggestion;->bZR:I

    return v0
.end method

.method public final isEnabled()Z
    .locals 4

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x20000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final isStale()Z
    .locals 4

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/32 v2, 0x10000

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final r(Lcom/google/android/shared/search/Suggestion;)I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 483
    if-ne p0, p1, :cond_0

    .line 490
    :goto_0
    return v2

    .line 486
    :cond_0
    if-nez p1, :cond_1

    .line 487
    const/4 v2, -0x1

    goto :goto_0

    .line 490
    :cond_1
    invoke-static {}, Liig;->aWF()Liig;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->bux:Ljava/lang/CharSequence;

    iget-object v4, p1, Lcom/google/android/shared/search/Suggestion;->bux:Ljava/lang/CharSequence;

    sget-object v5, Lcom/google/android/shared/search/Suggestion;->bZP:Liqa;

    invoke-virtual {v0, v3, v4, v5}, Liig;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Liig;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buy:Ljava/lang/CharSequence;

    iget-object v4, p1, Lcom/google/android/shared/search/Suggestion;->buy:Ljava/lang/CharSequence;

    sget-object v5, Lcom/google/android/shared/search/Suggestion;->bZP:Liqa;

    invoke-virtual {v0, v3, v4, v5}, Liig;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Liig;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buA:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/shared/search/Suggestion;->buA:Ljava/lang/String;

    sget-object v5, Lcom/google/android/shared/search/Suggestion;->bZP:Liqa;

    invoke-virtual {v0, v3, v4, v5}, Liig;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Liig;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buD:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/shared/search/Suggestion;->buD:Ljava/lang/String;

    sget-object v5, Lcom/google/android/shared/search/Suggestion;->bZO:Liqa;

    invoke-virtual {v0, v3, v4, v5}, Liig;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Liig;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buE:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/shared/search/Suggestion;->buE:Ljava/lang/String;

    sget-object v5, Lcom/google/android/shared/search/Suggestion;->bZO:Liqa;

    invoke-virtual {v0, v3, v4, v5}, Liig;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Liig;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buF:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/shared/search/Suggestion;->buF:Ljava/lang/String;

    sget-object v5, Lcom/google/android/shared/search/Suggestion;->bZO:Liqa;

    invoke-virtual {v0, v3, v4, v5}, Liig;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Liig;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buG:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/shared/search/Suggestion;->buG:Ljava/lang/String;

    sget-object v5, Lcom/google/android/shared/search/Suggestion;->bZO:Liqa;

    invoke-virtual {v0, v3, v4, v5}, Liig;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Liig;

    move-result-object v0

    iget v3, p0, Lcom/google/android/shared/search/Suggestion;->buL:I

    iget v4, p1, Lcom/google/android/shared/search/Suggestion;->buL:I

    invoke-virtual {v0, v3, v4}, Liig;->bl(II)Liig;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buM:Ljava/util/ArrayList;

    iget-object v4, p1, Lcom/google/android/shared/search/Suggestion;->buM:Ljava/util/ArrayList;

    sget-object v5, Lcom/google/android/shared/search/Suggestion;->bZQ:Liqa;

    invoke-virtual {v0, v3, v4, v5}, Liig;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Liig;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buv:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/shared/search/Suggestion;->buv:Ljava/lang/String;

    sget-object v5, Lcom/google/android/shared/search/Suggestion;->bZO:Liqa;

    invoke-virtual {v0, v3, v4, v5}, Liig;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Liig;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buJ:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/shared/search/Suggestion;->buJ:Ljava/lang/String;

    sget-object v5, Lcom/google/android/shared/search/Suggestion;->bZO:Liqa;

    invoke-virtual {v0, v3, v4, v5}, Liig;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Liig;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buK:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/shared/search/Suggestion;->buK:Ljava/lang/String;

    sget-object v5, Lcom/google/android/shared/search/Suggestion;->bZO:Liqa;

    invoke-virtual {v0, v3, v4, v5}, Liig;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Liig;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->isStale()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->isStale()Z

    move-result v4

    if-nez v4, :cond_3

    :goto_2
    invoke-virtual {v3, v0, v1}, Liig;->L(ZZ)Liig;

    move-result-object v0

    invoke-virtual {v0}, Liig;->aWG()I

    move-result v2

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 526
    invoke-static {p0}, Lifo;->bc(Ljava/lang/Object;)Lifp;

    move-result-object v2

    .line 527
    const-string v0, "flags"

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v3}, Lcom/google/android/shared/util/BitFlags;->auA()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    .line 529
    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->asq()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 530
    const-string v0, "web"

    invoke-virtual {v2, v0}, Lifp;->bd(Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "query"

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buJ:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "logtype"

    iget v3, p0, Lcom/google/android/shared/search/Suggestion;->buL:I

    invoke-virtual {v0, v1, v3}, Lifp;->O(Ljava/lang/String;I)Lifp;

    move-result-object v0

    const-string v1, "mlogsubtypes"

    invoke-direct {p0}, Lcom/google/android/shared/search/Suggestion;->asI()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    .line 574
    :cond_0
    :goto_0
    const-string v0, "stale"

    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->isStale()Z

    move-result v1

    invoke-virtual {v2, v0, v1}, Lifp;->F(Ljava/lang/String;Z)Lifp;

    move-result-object v0

    const-string v1, "score"

    iget-wide v4, p0, Lcom/google/android/shared/search/Suggestion;->bjh:J

    invoke-virtual {v0, v1, v4, v5}, Lifp;->u(Ljava/lang/String;J)Lifp;

    .line 576
    invoke-virtual {v2}, Lifp;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 534
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->asr()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 536
    const-string v0, "nav"

    invoke-virtual {v2, v0}, Lifp;->bd(Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "title"

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->bux:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "url"

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buJ:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "logtype"

    iget v3, p0, Lcom/google/android/shared/search/Suggestion;->buL:I

    invoke-virtual {v0, v1, v3}, Lifp;->O(Ljava/lang/String;I)Lifp;

    move-result-object v0

    const-string v1, "mlogsubtypes"

    invoke-direct {p0}, Lcom/google/android/shared/search/Suggestion;->asI()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    goto :goto_0

    .line 541
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->ass()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 542
    const-string v0, "universal"

    invoke-virtual {v2, v0}, Lifp;->bd(Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "query"

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buJ:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "corpus"

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buK:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "icon"

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buA:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "logtype"

    iget v3, p0, Lcom/google/android/shared/search/Suggestion;->buL:I

    invoke-virtual {v0, v1, v3}, Lifp;->O(Ljava/lang/String;I)Lifp;

    move-result-object v0

    const-string v1, "mlogsubtypes"

    invoke-direct {p0}, Lcom/google/android/shared/search/Suggestion;->asI()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    goto :goto_0

    .line 548
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->asm()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 549
    const-string v0, "icing"

    invoke-virtual {v2, v0}, Lifp;->bd(Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "text1"

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->bux:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "text2"

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buy:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "source"

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buv:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "corpus"

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->bvb:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "uri"

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->bvc:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    goto/16 :goto_0

    .line 555
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/shared/search/Suggestion;->asv()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 556
    const-string v0, "correction"

    invoke-virtual {v2, v0}, Lifp;->bd(Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v3, "text1"

    iget-object v4, p0, Lcom/google/android/shared/search/Suggestion;->bux:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3, v4}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    .line 558
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bux:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_0

    .line 559
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bux:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    .line 560
    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v3

    const-class v4, Lcom/google/android/shared/util/CorrectionSpan;

    invoke-interface {v0, v1, v3, v4}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/shared/util/CorrectionSpan;

    .line 561
    :goto_1
    array-length v3, v0

    if-ge v1, v3, :cond_0

    .line 562
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aget-object v4, v0, v1

    invoke-virtual {v2, v3, v4}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    .line 561
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 566
    :cond_5
    const-string v0, "other"

    invoke-virtual {v2, v0}, Lifp;->bd(Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "source"

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buw:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "text1"

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->bux:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "text2"

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buy:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "intentAction"

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buE:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "intentData"

    iget-object v3, p0, Lcom/google/android/shared/search/Suggestion;->buF:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "lastAccessTime"

    new-instance v3, Ljava/util/Date;

    iget-wide v4, p0, Lcom/google/android/shared/search/Suggestion;->buC:J

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1, v3}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    goto/16 :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 664
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buv:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 665
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buw:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 666
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bux:Ljava/lang/CharSequence;

    invoke-static {v0, p1, p2}, Leqt;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 667
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buy:Ljava/lang/CharSequence;

    invoke-static {v0, p1, p2}, Leqt;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 668
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buz:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 669
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buA:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 670
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buB:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 671
    iget-wide v0, p0, Lcom/google/android/shared/search/Suggestion;->buC:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 672
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buD:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 673
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 674
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buF:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 675
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buG:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 676
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buI:Landroid/content/ComponentName;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 677
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buJ:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 678
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buK:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 679
    iget v0, p0, Lcom/google/android/shared/search/Suggestion;->buL:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 680
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->buM:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 681
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bva:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 682
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bvb:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 683
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bvc:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 684
    iget-wide v0, p0, Lcom/google/android/shared/search/Suggestion;->bjh:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 685
    iget v0, p0, Lcom/google/android/shared/search/Suggestion;->bvg:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 686
    iget-object v0, p0, Lcom/google/android/shared/search/Suggestion;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 687
    return-void
.end method
