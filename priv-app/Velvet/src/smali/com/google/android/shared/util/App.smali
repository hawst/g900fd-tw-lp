.class public Lcom/google/android/shared/util/App;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private Sk:Landroid/graphics/drawable/Drawable;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final Yq:Landroid/content/pm/ActivityInfo;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bMW:Ljava/lang/String;

.field private final ceW:I

.field private final mIntent:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 140
    new-instance v0, Lelz;

    invoke-direct {v0}, Lelz;-><init>()V

    sput-object v0, Lcom/google/android/shared/util/App;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/shared/util/App;-><init>(Landroid/content/Intent;Ljava/lang/String;ILandroid/content/pm/ActivityInfo;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;Ljava/lang/String;ILandroid/content/pm/ActivityInfo;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/google/android/shared/util/App;->mIntent:Landroid/content/Intent;

    .line 53
    iput-object p2, p0, Lcom/google/android/shared/util/App;->bMW:Ljava/lang/String;

    .line 54
    iput p3, p0, Lcom/google/android/shared/util/App;->ceW:I

    .line 55
    iput-object p4, p0, Lcom/google/android/shared/util/App;->Yq:Landroid/content/pm/ActivityInfo;

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/shared/util/App;->Sk:Landroid/graphics/drawable/Drawable;

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;Ljava/lang/String;Landroid/content/pm/ActivityInfo;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/shared/util/App;-><init>(Landroid/content/Intent;Ljava/lang/String;ILandroid/content/pm/ActivityInfo;)V

    .line 49
    return-void
.end method

.method public static a(Landroid/content/pm/ResolveInfo;Landroid/content/Intent;Landroid/content/pm/PackageManager;)Lcom/google/android/shared/util/App;
    .locals 3

    .prologue
    .line 99
    new-instance v0, Lcom/google/android/shared/util/App;

    iget-object v1, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v1, p2}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/shared/util/App;-><init>(Landroid/content/Intent;Ljava/lang/String;Landroid/content/pm/ActivityInfo;)V

    return-object v0
.end method

.method public static a(Landroid/content/pm/PackageManager;Landroid/content/Intent;Ljava/util/List;Ljava/util/List;)V
    .locals 2

    .prologue
    .line 93
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 94
    invoke-static {v0, p1, p0}, Lcom/google/android/shared/util/App;->a(Landroid/content/pm/ResolveInfo;Landroid/content/Intent;Landroid/content/pm/PackageManager;)Lcom/google/android/shared/util/App;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 96
    :cond_0
    return-void
.end method

.method public static c(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/shared/util/App;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 111
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/App;

    .line 115
    invoke-virtual {v0}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 119
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final ar(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/shared/util/App;->Sk:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 65
    iget v0, p0, Lcom/google/android/shared/util/App;->ceW:I

    if-eqz v0, :cond_1

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/google/android/shared/util/App;->ceW:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/util/App;->Sk:Landroid/graphics/drawable/Drawable;

    .line 71
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/shared/util/App;->Sk:Landroid/graphics/drawable/Drawable;

    return-object v0

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/google/android/shared/util/App;->Yq:Landroid/content/pm/ActivityInfo;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/google/android/shared/util/App;->Yq:Landroid/content/pm/ActivityInfo;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/util/App;->Sk:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public final auq()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/shared/util/App;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    return v0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/shared/util/App;->bMW:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.gm"

    invoke-virtual {p0}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    const-string v0, "Gmail"

    .line 79
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/shared/util/App;->bMW:Ljava/lang/String;

    goto :goto_0
.end method

.method public final getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/shared/util/App;->mIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_0

    .line 87
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/shared/util/App;->Yq:Landroid/content/pm/ActivityInfo;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/shared/util/App;->Yq:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Label: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/shared/util/App;->bMW:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Intent: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/util/App;->mIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", PackageName: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/shared/util/App;->mIntent:Landroid/content/Intent;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 135
    iget-object v0, p0, Lcom/google/android/shared/util/App;->bMW:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/google/android/shared/util/App;->Yq:Landroid/content/pm/ActivityInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 137
    iget v0, p0, Lcom/google/android/shared/util/App;->ceW:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 138
    return-void
.end method
