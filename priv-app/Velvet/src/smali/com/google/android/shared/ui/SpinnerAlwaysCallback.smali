.class public Lcom/google/android/shared/ui/SpinnerAlwaysCallback;
.super Landroid/widget/Spinner;
.source "PG"


# instance fields
.field private ccN:I

.field private ccO:Lekh;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    .line 11
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->ccN:I

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 11
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->ccN:I

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 11
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->ccN:I

    .line 29
    return-void
.end method


# virtual methods
.method public final a(Lekh;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->ccO:Lekh;

    .line 46
    return-void
.end method

.method public final atZ()V
    .locals 2

    .prologue
    .line 49
    iget v0, p0, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->ccN:I

    invoke-virtual {p0}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->getSelectedItemPosition()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 50
    iget v0, p0, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->ccN:I

    invoke-super {p0, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 52
    :cond_0
    return-void
.end method

.method public final aua()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->ccN:I

    return v0
.end method

.method public final hY(I)V
    .locals 0

    .prologue
    .line 32
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 33
    return-void
.end method

.method public setSelection(I)V
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->getSelectedItemPosition()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->ccN:I

    .line 38
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 39
    iget-object v0, p0, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->ccO:Lekh;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/google/android/shared/ui/SpinnerAlwaysCallback;->ccO:Lekh;

    invoke-interface {v0, p1}, Lekh;->hZ(I)V

    .line 42
    :cond_0
    return-void
.end method
