.class public Lcom/google/android/shared/speech/Hypothesis;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final bag:Ljava/lang/String;

.field private caf:Lijj;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 217
    new-instance v0, Lehq;

    invoke-direct {v0}, Lehq;-><init>()V

    sput-object v0, Lcom/google/android/shared/speech/Hypothesis;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/speech/Hypothesis;->bag:Ljava/lang/String;

    .line 35
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 36
    sget-object v1, Lcom/google/android/shared/speech/Hypothesis$Span;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 37
    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/speech/Hypothesis;->caf:Lijj;

    .line 38
    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/shared/speech/Hypothesis;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/speech/Hypothesis;->bag:Ljava/lang/String;

    .line 42
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/speech/Hypothesis;->caf:Lijj;

    .line 43
    return-void
.end method

.method private constructor <init>(Ljava/lang/CharSequence;Ljava/lang/Iterable;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/speech/Hypothesis;->bag:Ljava/lang/String;

    .line 66
    invoke-static {p2}, Lijj;->q(Ljava/lang/Iterable;)Lijj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/speech/Hypothesis;->caf:Lijj;

    .line 67
    return-void
.end method

.method private constructor <init>(Ljava/lang/CharSequence;[Ljtm;)V
    .locals 12

    .prologue
    const/4 v6, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/speech/Hypothesis;->bag:Ljava/lang/String;

    .line 47
    invoke-static {}, Lijj;->aWX()Lijk;

    move-result-object v8

    .line 48
    array-length v9, p2

    move v7, v6

    :goto_0
    if-ge v7, v9, :cond_1

    aget-object v4, p2, v7

    .line 49
    invoke-direct {p0}, Lcom/google/android/shared/speech/Hypothesis;->asW()[B

    move-result-object v0

    .line 50
    iget v1, v4, Ljtm;->aLI:I

    invoke-static {v0, v1}, Lcom/google/android/shared/speech/Hypothesis;->d([BI)I

    move-result v1

    .line 51
    iget v2, v4, Ljtm;->aLI:I

    iget v3, v4, Ljtm;->length:I

    add-int/2addr v2, v3

    invoke-static {v0, v2}, Lcom/google/android/shared/speech/Hypothesis;->d([BI)I

    move-result v2

    .line 52
    invoke-static {}, Lijj;->aWX()Lijk;

    move-result-object v5

    .line 53
    iget-object v3, v4, Ljtm;->eDz:[Ljtk;

    array-length v10, v3

    move v0, v6

    :goto_1
    if-ge v0, v10, :cond_0

    aget-object v11, v3, v0

    .line 54
    iget-object v11, v11, Ljtk;->ev:Ljava/lang/String;

    invoke-virtual {v5, v11}, Lijk;->bt(Ljava/lang/Object;)Lijk;

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 57
    :cond_0
    new-instance v0, Lcom/google/android/shared/speech/Hypothesis$Span;

    iget v3, v4, Ljtm;->aLI:I

    iget v4, v4, Ljtm;->length:I

    iget-object v5, v5, Lijk;->IA:Ljava/util/ArrayList;

    invoke-static {v5}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/shared/speech/Hypothesis$Span;-><init>(IIIILijj;)V

    invoke-virtual {v8, v0}, Lijk;->bt(Ljava/lang/Object;)Lijk;

    .line 48
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 61
    :cond_1
    iget-object v0, v8, Lijk;->IA:Ljava/util/ArrayList;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/speech/Hypothesis;->caf:Lijj;

    .line 62
    return-void
.end method

.method public static E(Ljava/lang/CharSequence;)Lcom/google/android/shared/speech/Hypothesis;
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lcom/google/android/shared/speech/Hypothesis;

    invoke-direct {v0, p0}, Lcom/google/android/shared/speech/Hypothesis;-><init>(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public static a(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Lcom/google/android/shared/speech/Hypothesis;
    .locals 1
    .param p1    # Ljava/lang/Iterable;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 87
    if-nez p1, :cond_0

    .line 88
    invoke-static {p0}, Lcom/google/android/shared/speech/Hypothesis;->E(Ljava/lang/CharSequence;)Lcom/google/android/shared/speech/Hypothesis;

    move-result-object v0

    .line 90
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/shared/speech/Hypothesis;

    invoke-direct {v0, p0, p1}, Lcom/google/android/shared/speech/Hypothesis;-><init>(Ljava/lang/CharSequence;Ljava/lang/Iterable;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/CharSequence;[Ljtm;)Lcom/google/android/shared/speech/Hypothesis;
    .locals 1
    .param p1    # [Ljtm;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 75
    if-nez p1, :cond_0

    .line 76
    invoke-static {p0}, Lcom/google/android/shared/speech/Hypothesis;->E(Ljava/lang/CharSequence;)Lcom/google/android/shared/speech/Hypothesis;

    move-result-object v0

    .line 78
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/shared/speech/Hypothesis;

    invoke-direct {v0, p0, p1}, Lcom/google/android/shared/speech/Hypothesis;-><init>(Ljava/lang/CharSequence;[Ljtm;)V

    goto :goto_0
.end method

.method private asW()[B
    .locals 2

    .prologue
    .line 166
    :try_start_0
    iget-object v0, p0, Lcom/google/android/shared/speech/Hypothesis;->bag:Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 167
    :catch_0
    move-exception v0

    .line 168
    invoke-static {v0}, Ligm;->g(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method private static d([BI)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 125
    move v1, v0

    .line 127
    :cond_0
    :goto_0
    if-ge v0, p1, :cond_2

    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 128
    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    .line 130
    ushr-int/lit8 v3, v2, 0x7

    if-nez v3, :cond_1

    .line 132
    add-int/lit8 v1, v1, 0x1

    .line 133
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    :cond_1
    ushr-int/lit8 v3, v2, 0x5

    const/4 v4, 0x6

    if-ne v3, v4, :cond_4

    .line 137
    add-int/lit8 v2, v0, 0x1

    if-lt v2, p1, :cond_3

    .line 161
    :cond_2
    return v1

    .line 140
    :cond_3
    add-int/lit8 v0, v0, 0x2

    .line 141
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 142
    :cond_4
    ushr-int/lit8 v3, v2, 0x4

    const/16 v4, 0xe

    if-ne v3, v4, :cond_5

    .line 145
    add-int/lit8 v2, v0, 0x2

    if-ge v2, p1, :cond_2

    .line 148
    add-int/lit8 v0, v0, 0x3

    .line 149
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 150
    :cond_5
    ushr-int/lit8 v2, v2, 0x3

    const/16 v3, 0x1e

    if-ne v2, v3, :cond_0

    .line 154
    add-int/lit8 v2, v0, 0x3

    if-ge v2, p1, :cond_2

    .line 157
    add-int/lit8 v0, v0, 0x4

    .line 158
    add-int/lit8 v1, v1, 0x2

    goto :goto_0
.end method


# virtual methods
.method public final asX()Lijj;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/shared/speech/Hypothesis;->caf:Lijj;

    return-object v0
.end method

.method public final asY()I
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/shared/speech/Hypothesis;->caf:Lijj;

    invoke-virtual {v0}, Lijj;->size()I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 193
    instance-of v1, p1, Lcom/google/android/shared/speech/Hypothesis;

    if-eqz v1, :cond_0

    .line 194
    check-cast p1, Lcom/google/android/shared/speech/Hypothesis;

    .line 195
    iget-object v1, p0, Lcom/google/android/shared/speech/Hypothesis;->bag:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/shared/speech/Hypothesis;->bag:Ljava/lang/String;

    invoke-static {v1, v2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/shared/speech/Hypothesis;->caf:Lijj;

    iget-object v2, p1, Lcom/google/android/shared/speech/Hypothesis;->caf:Lijj;

    invoke-static {v1, v2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 198
    :cond_0
    return v0
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/shared/speech/Hypothesis;->bag:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 203
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/shared/speech/Hypothesis;->bag:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/shared/speech/Hypothesis;->caf:Lijj;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Hypothesis: ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/shared/speech/Hypothesis;->bag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] with "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/speech/Hypothesis;->caf:Lijj;

    invoke-virtual {v1}, Lijj;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " span(s)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/shared/speech/Hypothesis;->bag:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/google/android/shared/speech/Hypothesis;->caf:Lijj;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 215
    return-void
.end method
