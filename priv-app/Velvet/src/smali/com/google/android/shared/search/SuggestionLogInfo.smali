.class public Lcom/google/android/shared/search/SuggestionLogInfo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static bZT:Lcom/google/android/shared/search/SuggestionLogInfo;


# instance fields
.field private final bZU:Ljava/lang/String;

.field private final bZV:[B


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 9
    new-instance v0, Lcom/google/android/shared/search/SuggestionLogInfo;

    const-string v1, ""

    const/4 v2, 0x0

    new-array v2, v2, [B

    invoke-direct {v0, v1, v2}, Lcom/google/android/shared/search/SuggestionLogInfo;-><init>(Ljava/lang/String;[B)V

    sput-object v0, Lcom/google/android/shared/search/SuggestionLogInfo;->bZT:Lcom/google/android/shared/search/SuggestionLogInfo;

    .line 56
    new-instance v0, Lehl;

    invoke-direct {v0}, Lehl;-><init>()V

    sput-object v0, Lcom/google/android/shared/search/SuggestionLogInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/SuggestionLogInfo;->bZU:Ljava/lang/String;

    .line 23
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 24
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/shared/search/SuggestionLogInfo;->bZV:[B

    .line 25
    iget-object v0, p0, Lcom/google/android/shared/search/SuggestionLogInfo;->bZV:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[B)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/android/shared/search/SuggestionLogInfo;->bZU:Ljava/lang/String;

    .line 18
    iput-object p2, p0, Lcom/google/android/shared/search/SuggestionLogInfo;->bZV:[B

    .line 19
    return-void
.end method


# virtual methods
.method public final asJ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/shared/search/SuggestionLogInfo;->bZU:Ljava/lang/String;

    return-object v0
.end method

.method public final asK()[B
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/shared/search/SuggestionLogInfo;->bZV:[B

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SuggestionLogInfo: \n    SuggestionsEncoding: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/shared/search/SuggestionLogInfo;->bZU:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n    serializedAndroidGsaOmniboxEvent: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/SuggestionLogInfo;->bZV:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/shared/search/SuggestionLogInfo;->bZU:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/shared/search/SuggestionLogInfo;->bZV:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 45
    iget-object v0, p0, Lcom/google/android/shared/search/SuggestionLogInfo;->bZV:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 46
    return-void
.end method
