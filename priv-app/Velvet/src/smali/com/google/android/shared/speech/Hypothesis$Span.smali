.class public Lcom/google/android/shared/speech/Hypothesis$Span;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final cag:I

.field public final cah:I

.field public final cai:I

.field public final caj:I

.field public cak:Lijj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 331
    new-instance v0, Lehr;

    invoke-direct {v0}, Lehr;-><init>()V

    sput-object v0, Lcom/google/android/shared/speech/Hypothesis$Span;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIIILijj;)V
    .locals 0

    .prologue
    .line 263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 264
    iput p1, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cag:I

    .line 265
    iput p2, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cah:I

    .line 266
    iput p3, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cai:I

    .line 267
    iput p4, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->caj:I

    .line 268
    iput-object p5, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cak:Lijj;

    .line 269
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 272
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cag:I

    .line 273
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cah:I

    .line 274
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cai:I

    .line 275
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->caj:I

    .line 276
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 277
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 278
    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cak:Lijj;

    .line 279
    return-void
.end method


# virtual methods
.method public final a(Lijj;)Lcom/google/android/shared/speech/Hypothesis$Span;
    .locals 6

    .prologue
    .line 285
    new-instance v0, Lcom/google/android/shared/speech/Hypothesis$Span;

    iget v1, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cag:I

    iget v2, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cah:I

    iget v3, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cai:I

    iget v4, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->caj:I

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/shared/speech/Hypothesis$Span;-><init>(IIIILijj;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 319
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 301
    instance-of v1, p1, Lcom/google/android/shared/speech/Hypothesis$Span;

    if-eqz v1, :cond_0

    .line 302
    check-cast p1, Lcom/google/android/shared/speech/Hypothesis$Span;

    .line 303
    iget v1, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cag:I

    iget v2, p1, Lcom/google/android/shared/speech/Hypothesis$Span;->cag:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cah:I

    iget v2, p1, Lcom/google/android/shared/speech/Hypothesis$Span;->cah:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cai:I

    iget v2, p1, Lcom/google/android/shared/speech/Hypothesis$Span;->cai:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->caj:I

    iget v2, p1, Lcom/google/android/shared/speech/Hypothesis$Span;->caj:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cak:Lijj;

    iget-object v2, p1, Lcom/google/android/shared/speech/Hypothesis$Span;->cak:Lijj;

    invoke-static {v1, v2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 309
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 314
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cag:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cah:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cai:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->caj:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cak:Lijj;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 290
    invoke-static {p0}, Lifo;->bc(Ljava/lang/Object;)Lifp;

    move-result-object v0

    const-string v1, "mUtf16Start"

    iget v2, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cag:I

    invoke-virtual {v0, v1, v2}, Lifp;->O(Ljava/lang/String;I)Lifp;

    move-result-object v0

    const-string v1, "mUtf16End"

    iget v2, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cah:I

    invoke-virtual {v0, v1, v2}, Lifp;->O(Ljava/lang/String;I)Lifp;

    move-result-object v0

    const-string v1, "mUtf8Start"

    iget v2, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cai:I

    invoke-virtual {v0, v1, v2}, Lifp;->O(Ljava/lang/String;I)Lifp;

    move-result-object v0

    const-string v1, "mUtf8Length"

    iget v2, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->caj:I

    invoke-virtual {v0, v1, v2}, Lifp;->O(Ljava/lang/String;I)Lifp;

    move-result-object v0

    const-string v1, "mAlternates"

    iget-object v2, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cak:Lijj;

    invoke-virtual {v0, v1, v2}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    move-result-object v0

    invoke-virtual {v0}, Lifp;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 324
    iget v0, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cag:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 325
    iget v0, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cah:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 326
    iget v0, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cai:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 327
    iget v0, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->caj:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 328
    iget-object v0, p0, Lcom/google/android/shared/speech/Hypothesis$Span;->cak:Lijj;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 329
    return-void
.end method
