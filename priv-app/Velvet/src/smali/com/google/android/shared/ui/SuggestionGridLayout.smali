.class public Lcom/google/android/shared/ui/SuggestionGridLayout;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field public RJ:Z

.field private Vs:Landroid/animation/LayoutTransition;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ccP:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field private ccQ:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field private ccR:I

.field private ccS:I

.field private ccT:I

.field private ccU:I

.field private ccV:Z

.field private ccW:I

.field private ccX:[I

.field private ccY:I

.field public ccZ:Lekt;

.field public cda:Lekq;

.field public final cdb:Ljava/util/ArrayList;

.field private cdc:Ljava/util/ArrayList;

.field private cdd:I

.field public cde:Landroid/view/View;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cdf:Lekl;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public cdg:Ljava/util/ArrayList;

.field private cdh:Landroid/view/View;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cdi:Z

.field public cdj:I

.field public cdk:Z

.field public cdl:I

.field private cdm:Lejg;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cdn:[I

.field private cdo:[Z

.field public final iA:Landroid/graphics/Rect;

.field public mScrollView:Lekf;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private yk:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 150
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 154
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const/4 v2, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 157
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->iA:Landroid/graphics/Rect;

    .line 84
    iput-boolean v4, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->RJ:Z

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdb:Ljava/util/ArrayList;

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdc:Ljava/util/ArrayList;

    .line 99
    iput v4, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdd:I

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdg:Ljava/util/ArrayList;

    .line 159
    sget-object v0, Lbwe;->aMd:[I

    invoke-virtual {p1, p2, v0, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 161
    const v1, 0x7fffffff

    invoke-virtual {v0, v5, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccP:I

    .line 163
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    .line 164
    iget v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccX:[I

    .line 165
    iget v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdn:[I

    .line 166
    iget v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    new-array v1, v1, [Z

    iput-object v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdo:[Z

    .line 167
    invoke-virtual {v0, v7, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccR:I

    .line 169
    invoke-virtual {v0, v6, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccS:I

    .line 171
    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccT:I

    .line 173
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccU:I

    .line 175
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 177
    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->Vs:Landroid/animation/LayoutTransition;

    .line 178
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->Vs:Landroid/animation/LayoutTransition;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v2}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/LayoutTransition;->setDuration(J)V

    invoke-virtual {v0, v6}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    invoke-virtual {v0, v7}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    new-instance v2, Leix;

    invoke-direct {v2, v5, v1}, Leix;-><init>(ZI)V

    invoke-virtual {v0, v6, v2}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v3, 0x40200000    # 2.5f

    invoke-direct {v2, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v6, v2}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    new-instance v2, Leix;

    invoke-direct {v2, v4, v1}, Leix;-><init>(ZI)V

    invoke-virtual {v0, v7, v2}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x3fc00000    # 1.5f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v7, v1}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v4}, Landroid/animation/LayoutTransition;->setAnimateParentHierarchy(Z)V

    .line 182
    :cond_0
    invoke-virtual {p0, v4}, Lcom/google/android/shared/ui/SuggestionGridLayout;->setClipToPadding(Z)V

    .line 183
    invoke-virtual {p0, v4}, Lcom/google/android/shared/ui/SuggestionGridLayout;->setClipChildren(Z)V

    .line 184
    invoke-virtual {p0, v5}, Lcom/google/android/shared/ui/SuggestionGridLayout;->setChildrenDrawingOrderEnabled(Z)V

    .line 186
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 187
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 188
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v1

    .line 190
    new-instance v2, Lekt;

    new-instance v3, Leks;

    invoke-direct {v3, p0}, Leks;-><init>(Lcom/google/android/shared/ui/SuggestionGridLayout;)V

    int-to-float v1, v1

    invoke-direct {v2, v4, v3, v0, v1}, Lekt;-><init>(ILeky;FF)V

    iput-object v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccZ:Lekt;

    .line 192
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 193
    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/View;Landroid/view/View;II)V
    .locals 4
    .param p2    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 768
    instance-of v0, p1, Lekk;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 769
    check-cast v0, Lekk;

    iget-object v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccZ:Lekt;

    iget-boolean v1, v1, Lekt;->cel:Z

    iget-object v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccZ:Lekt;

    iget-boolean v2, v2, Lekt;->cem:Z

    invoke-interface {v0, v1, v2}, Lekk;->w(ZZ)V

    .line 772
    :cond_0
    invoke-direct {p0, p1, p5}, Lcom/google/android/shared/ui/SuggestionGridLayout;->w(Landroid/view/View;I)Lekm;

    .line 773
    if-eqz p2, :cond_1

    .line 774
    invoke-direct {p0, p2, p5}, Lcom/google/android/shared/ui/SuggestionGridLayout;->w(Landroid/view/View;I)Lekm;

    .line 776
    :cond_1
    if-eqz p3, :cond_2

    .line 777
    invoke-direct {p0, p3, p5}, Lcom/google/android/shared/ui/SuggestionGridLayout;->w(Landroid/view/View;I)Lekm;

    .line 779
    :cond_2
    new-instance v2, Lekl;

    invoke-direct {v2, p1, p2, p3}, Lekl;-><init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    .line 780
    invoke-virtual {p0, v2}, Lcom/google/android/shared/ui/SuggestionGridLayout;->a(Lekl;)V

    .line 781
    if-ltz p4, :cond_3

    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildCount()I

    move-result v0

    if-lt p4, v0, :cond_6

    .line 782
    :cond_3
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdc:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 783
    invoke-direct {p0, p1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aK(Landroid/view/View;)V

    .line 784
    if-eqz p2, :cond_4

    .line 785
    invoke-direct {p0, p2}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aK(Landroid/view/View;)V

    .line 787
    :cond_4
    if-eqz p3, :cond_5

    .line 788
    invoke-direct {p0, p3}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aK(Landroid/view/View;)V

    .line 811
    :cond_5
    :goto_0
    return-void

    .line 792
    :cond_6
    invoke-virtual {p0, p4}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aO(Landroid/view/View;)I

    move-result v0

    move v1, p4

    .line 795
    :goto_1
    if-gez v0, :cond_7

    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_7

    .line 796
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aO(Landroid/view/View;)I

    move-result v0

    goto :goto_1

    .line 798
    :cond_7
    if-ltz v0, :cond_5

    .line 799
    iget-object v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdc:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 802
    if-eqz p3, :cond_8

    .line 803
    invoke-direct {p0, p3, p4}, Lcom/google/android/shared/ui/SuggestionGridLayout;->v(Landroid/view/View;I)V

    .line 805
    :cond_8
    if-eqz p2, :cond_9

    .line 806
    invoke-direct {p0, p2, p4}, Lcom/google/android/shared/ui/SuggestionGridLayout;->v(Landroid/view/View;I)V

    .line 808
    :cond_9
    invoke-direct {p0, p1, p4}, Lcom/google/android/shared/ui/SuggestionGridLayout;->v(Landroid/view/View;I)V

    goto :goto_0
.end method

.method private aK(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 749
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccV:Z

    .line 750
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 752
    iput-boolean v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccV:Z

    .line 753
    return-void

    .line 752
    :catchall_0
    move-exception v0

    iput-boolean v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccV:Z

    throw v0
.end method

.method public static aN(Landroid/view/View;)Ljava/util/List;
    .locals 1

    .prologue
    .line 1103
    invoke-static {p0}, Lekl;->aQ(Landroid/view/View;)Lekl;

    move-result-object v0

    .line 1104
    iget-object v0, v0, Lekl;->cds:Ljava/util/List;

    return-object v0
.end method

.method private aO(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 1108
    invoke-static {p1}, Lekl;->aQ(Landroid/view/View;)Lekl;

    move-result-object v0

    .line 1109
    iget-object v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdc:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public static aP(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1274
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 1275
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1276
    return-void
.end method

.method private auc()I
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 669
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    .line 672
    iget v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 673
    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->auf()I

    move-result v2

    .line 674
    const/16 v1, 0x1e

    .line 679
    :goto_0
    iget-object v4, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->mScrollView:Lekf;

    invoke-interface {v4}, Lekf;->ate()I

    move-result v4

    iget v5, v0, Lekm;->topMargin:I

    add-int/2addr v2, v5

    iget v0, v0, Lekm;->bottomMargin:I

    add-int/2addr v0, v2

    iget v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccU:I

    add-int/2addr v0, v2

    sub-int v0, v4, v0

    .line 681
    iget-object v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->mScrollView:Lekf;

    invoke-interface {v2}, Lekf;->ate()I

    move-result v2

    mul-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x64

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0

    .line 677
    :cond_0
    const/16 v1, 0x19

    move v2, v3

    goto :goto_0
.end method

.method public static c(Lekl;)Lekm;
    .locals 1

    .prologue
    .line 1113
    iget-object v0, p0, Lekl;->cdr:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    return-object v0
.end method

.method private fa(Z)V
    .locals 1

    .prologue
    .line 1047
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccZ:Lekt;

    invoke-virtual {v0, p1}, Lekt;->setEnabled(Z)V

    .line 1048
    return-void
.end method

.method private v(Landroid/view/View;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 758
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccV:Z

    .line 759
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 761
    iput-boolean v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccV:Z

    .line 762
    return-void

    .line 761
    :catchall_0
    move-exception v0

    iput-boolean v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccV:Z

    throw v0
.end method

.method private w(Landroid/view/View;I)Lekm;
    .locals 3

    .prologue
    .line 864
    iget v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    if-lt p2, v0, :cond_0

    .line 865
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Column exceeds column count."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 867
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 868
    if-nez v0, :cond_1

    .line 869
    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 871
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v1

    if-eqz v1, :cond_4

    :goto_0
    check-cast v0, Lekm;

    check-cast v0, Lekm;

    .line 873
    iget v1, v0, Lekm;->column:I

    const/4 v2, -0x2

    if-ne v1, v2, :cond_2

    .line 874
    invoke-static {p0}, Leot;->ay(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 875
    const/4 p2, 0x0

    .line 880
    :cond_2
    :goto_1
    const/4 v1, -0x3

    if-eq p2, v1, :cond_3

    .line 881
    iput p2, v0, Lekm;->column:I

    .line 883
    :cond_3
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 884
    return-object v0

    .line 871
    :cond_4
    invoke-virtual {p0, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    goto :goto_0

    .line 877
    :cond_5
    iget v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    add-int/lit8 p2, v1, -0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/View;Landroid/view/View;I)V
    .locals 6
    .param p2    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 737
    const/4 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/shared/ui/SuggestionGridLayout;->a(Landroid/view/View;Landroid/view/View;Landroid/view/View;II)V

    .line 738
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 6
    .param p3    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 833
    invoke-static {p1}, Lekl;->aQ(Landroid/view/View;)Lekl;

    move-result-object v1

    .line 834
    if-nez v1, :cond_0

    .line 835
    const-string v0, "SuggestionGridLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot replace non-grid item "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    :goto_0
    return-void

    .line 838
    :cond_0
    iget-object v0, v1, Lekl;->cdr:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    .line 839
    iget v5, v0, Lekm;->column:I

    .line 841
    iget-object v0, v1, Lekl;->cdr:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->indexOfChild(Landroid/view/View;)I

    move-result v4

    .line 842
    invoke-virtual {p0, v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->b(Lekl;)V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    .line 844
    invoke-direct/range {v0 .. v5}, Lcom/google/android/shared/ui/SuggestionGridLayout;->a(Landroid/view/View;Landroid/view/View;Landroid/view/View;II)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Landroid/view/View;Lekf;Z)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 905
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 906
    if-eqz p1, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 907
    if-eqz p2, :cond_4

    move v0, v1

    :goto_2
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 908
    invoke-virtual {p0, p2}, Lcom/google/android/shared/ui/SuggestionGridLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_5

    move v0, v1

    :goto_3
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 909
    invoke-interface {p3, p0}, Lekf;->aI(Landroid/view/View;)Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 911
    iput-object p1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    .line 912
    invoke-static {p2}, Lekl;->aQ(Landroid/view/View;)Lekl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    .line 913
    iput-object p3, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->mScrollView:Lekf;

    .line 914
    iput-boolean p4, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdi:Z

    .line 917
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdh:Landroid/view/View;

    .line 918
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdh:Landroid/view/View;

    new-instance v2, Leki;

    invoke-direct {v2, p0}, Leki;-><init>(Lcom/google/android/shared/ui/SuggestionGridLayout;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 924
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdh:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aK(Landroid/view/View;)V

    .line 926
    if-nez p4, :cond_0

    .line 927
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->mScrollView:Lekf;

    invoke-interface {v0, p0}, Lekf;->aH(Landroid/view/View;)I

    move-result v0

    .line 928
    invoke-interface {p3}, Lekf;->getScrollY()I

    move-result v2

    .line 929
    iget-object v3, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    iget-object v3, v3, Lekl;->cdr:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    add-int/2addr v0, v3

    sub-int v0, v2, v0

    iput v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdj:I

    .line 932
    :cond_0
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    .line 933
    if-nez v0, :cond_8

    .line 934
    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    move-object v8, v0

    .line 936
    :goto_4
    iget v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    if-ne v0, v1, :cond_6

    .line 938
    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v4

    .line 939
    new-instance v0, Lejg;

    iget-object v3, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdh:Landroid/view/View;

    const/16 v5, 0xc8

    new-instance v6, Lekp;

    invoke-direct {v6, p0}, Lekp;-><init>(Lcom/google/android/shared/ui/SuggestionGridLayout;)V

    move-object v1, p3

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lejg;-><init>(Lekf;Landroid/view/ViewGroup;Landroid/view/View;IILejh;)V

    iput-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdm:Lejg;

    .line 941
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->mScrollView:Lekf;

    iget-object v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdm:Lejg;

    invoke-interface {v0, v1}, Lekf;->a(Leka;)V

    .line 942
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccZ:Lekt;

    invoke-virtual {v0}, Lekt;->aui()V

    .line 945
    sget-object v0, Lekn;->cdT:Lekn;

    iput-object v0, v8, Lekm;->cdz:Lekn;

    .line 946
    sget-object v0, Lekn;->cdT:Lekn;

    iput-object v0, v8, Lekm;->cdA:Lekn;

    .line 947
    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d017f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, v8, Lekm;->cdE:I

    .line 949
    iput v9, v8, Lekm;->cdF:I

    .line 950
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 956
    :goto_5
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 957
    iget-object v0, v8, Lekm;->cdz:Lekn;

    invoke-static {v0}, Leix;->a(Lekn;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 958
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v9, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 960
    :cond_1
    invoke-direct {p0, v7}, Lcom/google/android/shared/ui/SuggestionGridLayout;->fa(Z)V

    .line 963
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    invoke-direct {p0, v0, v7}, Lcom/google/android/shared/ui/SuggestionGridLayout;->v(Landroid/view/View;I)V

    .line 965
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdg:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leko;

    .line 966
    invoke-interface {v0}, Leko;->Cr()V

    goto :goto_6

    :cond_2
    move v0, v7

    .line 905
    goto/16 :goto_0

    :cond_3
    move v0, v7

    .line 906
    goto/16 :goto_1

    :cond_4
    move v0, v7

    .line 907
    goto/16 :goto_2

    :cond_5
    move v0, v7

    .line 908
    goto/16 :goto_3

    .line 952
    :cond_6
    sget-object v0, Lekn;->cdN:Lekn;

    iput-object v0, v8, Lekm;->cdz:Lekn;

    .line 953
    sget-object v0, Lekn;->cdN:Lekn;

    iput-object v0, v8, Lekm;->cdA:Lekn;

    .line 954
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->mScrollView:Lekf;

    invoke-interface {v0, v7}, Lekf;->eT(Z)V

    goto :goto_5

    .line 968
    :cond_7
    return-void

    :cond_8
    move-object v8, v0

    goto/16 :goto_4
.end method

.method public final a(Lekl;)V
    .locals 3

    .prologue
    .line 245
    invoke-static {p1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->c(Lekl;)Lekm;

    move-result-object v0

    .line 248
    iget-object v0, v0, Lekm;->cdz:Lekn;

    invoke-static {v0}, Leix;->a(Lekn;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lekl;->id(I)V

    .line 251
    :cond_0
    iget-object v0, p1, Lekl;->cds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 252
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    instance-of v2, v2, Lekm;

    if-eqz v2, :cond_1

    .line 253
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    iget v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdd:I

    iput v2, v0, Lekm;->cdy:I

    goto :goto_0

    .line 256
    :cond_2
    iget v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdd:I

    .line 257
    return-void
.end method

.method public final a(Leko;)V
    .locals 1

    .prologue
    .line 1029
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdg:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 1030
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdg:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1031
    return-void

    .line 1029
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lekq;)V
    .locals 0

    .prologue
    .line 1051
    iput-object p1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cda:Lekq;

    .line 1052
    return-void
.end method

.method public final aK(J)V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->Vs:Landroid/animation/LayoutTransition;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, p1, p2}, Landroid/animation/LayoutTransition;->setDuration(J)V

    .line 242
    :cond_0
    return-void
.end method

.method public final aL(Landroid/view/View;)Landroid/view/View;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 819
    invoke-virtual {p0, p1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 820
    invoke-static {p1}, Lekl;->aQ(Landroid/view/View;)Lekl;

    move-result-object v0

    .line 821
    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 819
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 821
    :cond_1
    iget-object v0, v0, Lekl;->bXk:Landroid/view/View;

    goto :goto_1
.end method

.method public final aM(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1069
    invoke-static {p1}, Lekl;->aQ(Landroid/view/View;)Lekl;

    move-result-object v0

    .line 1070
    if-eqz v0, :cond_0

    .line 1071
    invoke-virtual {p0, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->b(Lekl;)V

    .line 1075
    :goto_0
    return-void

    .line 1073
    :cond_0
    const-string v0, "SuggestionGridLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removeGridItem with non-grid item "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public addView(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 711
    iget-boolean v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccV:Z

    if-eqz v0, :cond_0

    .line 712
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 716
    :goto_0
    return-void

    .line 714
    :cond_0
    const/4 v4, -0x1

    const/4 v5, -0x3

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/shared/ui/SuggestionGridLayout;->a(Landroid/view/View;Landroid/view/View;Landroid/view/View;II)V

    goto :goto_0
.end method

.method public addView(Landroid/view/View;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 720
    iget-boolean v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccV:Z

    if-eqz v0, :cond_0

    .line 721
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 725
    :goto_0
    return-void

    .line 723
    :cond_0
    const/4 v5, -0x3

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/shared/ui/SuggestionGridLayout;->a(Landroid/view/View;Landroid/view/View;Landroid/view/View;II)V

    goto :goto_0
.end method

.method public final aub()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 313
    const v1, 0x7fffffff

    move v2, v1

    move v1, v0

    .line 315
    :goto_0
    iget v3, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    if-ge v0, v3, :cond_1

    .line 316
    iget-object v3, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccX:[I

    aget v3, v3, v0

    .line 317
    if-ge v3, v2, :cond_0

    move v1, v0

    move v2, v3

    .line 315
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 322
    :cond_1
    return v1
.end method

.method public final aud()V
    .locals 8

    .prologue
    const-wide/16 v0, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 975
    iget-object v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    if-nez v2, :cond_1

    .line 1026
    :cond_0
    return-void

    .line 977
    :cond_1
    invoke-direct {p0, v7}, Lcom/google/android/shared/ui/SuggestionGridLayout;->fa(Z)V

    .line 979
    iget-object v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->Vs:Landroid/animation/LayoutTransition;

    if-nez v2, :cond_5

    move-wide v2, v0

    .line 981
    :goto_0
    const-wide/16 v4, 0x190

    invoke-virtual {p0, v4, v5}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aK(J)V

    .line 982
    iget v4, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    if-ne v4, v7, :cond_6

    .line 984
    invoke-virtual {p0, v7, v0, v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->e(IJ)V

    .line 986
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdm:Lejg;

    iget-object v1, v0, Lejg;->cbz:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lejg;->cbz:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_2
    invoke-virtual {v0}, Lejg;->reset()V

    .line 987
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->mScrollView:Lekf;

    invoke-interface {v0, v6}, Lekf;->a(Leka;)V

    .line 991
    :goto_1
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    .line 992
    iget-object v0, v0, Lekm;->cdA:Lekn;

    invoke-static {v0}, Leix;->a(Lekn;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 993
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v6}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 995
    :cond_3
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 997
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->Vs:Landroid/animation/LayoutTransition;

    if-nez v0, :cond_7

    .line 998
    invoke-static {v6}, Livg;->bC(Ljava/lang/Object;)Livq;

    move-result-object v0

    move-object v1, v0

    .line 1002
    :goto_2
    iput-object v6, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    .line 1003
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdh:Landroid/view/View;

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1004
    iput-object v6, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdh:Landroid/view/View;

    .line 1005
    iput-object v6, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    .line 1006
    iput-object v6, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->mScrollView:Lekf;

    .line 1007
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdi:Z

    .line 1010
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->Vs:Landroid/animation/LayoutTransition;

    if-eqz v0, :cond_4

    .line 1011
    new-instance v0, Lekj;

    invoke-direct {v0, p0, v2, v3}, Lekj;-><init>(Lcom/google/android/shared/ui/SuggestionGridLayout;J)V

    invoke-static {}, Livu;->aZh()Livs;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Livq;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 1023
    :cond_4
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdg:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leko;

    .line 1024
    invoke-interface {v0, v1}, Leko;->a(Livq;)V

    goto :goto_3

    .line 979
    :cond_5
    iget-object v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {v2, v7}, Landroid/animation/LayoutTransition;->getStartDelay(I)J

    move-result-wide v2

    goto :goto_0

    .line 989
    :cond_6
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->mScrollView:Lekf;

    invoke-interface {v0, v7}, Lekf;->eT(Z)V

    goto :goto_1

    .line 1000
    :cond_7
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->Vs:Landroid/animation/LayoutTransition;

    invoke-static {v0, v6}, Lelv;->a(Landroid/animation/LayoutTransition;Landroid/view/View;)Livq;

    move-result-object v0

    move-object v1, v0

    goto :goto_2
.end method

.method public final aue()Z
    .locals 1

    .prologue
    .line 1039
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final auf()I
    .locals 2

    .prologue
    .line 1043
    iget v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccT:I

    iget v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccW:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    invoke-virtual {v1}, Lekl;->getMeasuredHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public final b(Lekl;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const v2, 0x7f110013

    .line 1078
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    if-ne p1, v0, :cond_0

    .line 1080
    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aud()V

    .line 1083
    :cond_0
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdb:Ljava/util/ArrayList;

    iget-object v1, p1, Lekl;->cdr:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1085
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccZ:Lekt;

    invoke-virtual {v0}, Lekt;->aui()V

    .line 1088
    :cond_1
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdc:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1089
    iget-object v0, p1, Lekl;->cdr:Landroid/view/View;

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v0, p1, Lekl;->bXk:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lekl;->bXk:Landroid/view/View;

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :cond_2
    iget-object v0, p1, Lekl;->bXl:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lekl;->bXl:Landroid/view/View;

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1090
    :cond_3
    iget-object v0, p1, Lekl;->cdr:Landroid/view/View;

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1091
    iget-object v0, p1, Lekl;->bXk:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 1092
    iget-object v0, p1, Lekl;->bXk:Landroid/view/View;

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1094
    :cond_4
    iget-object v0, p1, Lekl;->bXl:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 1095
    iget-object v0, p1, Lekl;->bXl:Landroid/view/View;

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1097
    :cond_5
    return-void
.end method

.method public final b(Leko;)V
    .locals 1

    .prologue
    .line 1034
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdg:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1035
    return-void
.end method

.method public checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 2

    .prologue
    .line 1123
    instance-of v0, p1, Lekm;

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 3

    .prologue
    .line 687
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    .line 688
    iget-object v1, v0, Lekm;->cdJ:Landroid/graphics/Rect;

    if-eqz v1, :cond_0

    .line 689
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 690
    iget-object v0, v0, Lekm;->cdJ:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 691
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    .line 692
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 705
    :goto_0
    return v0

    .line 694
    :cond_0
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdm:Lejg;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    if-eq p2, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdh:Landroid/view/View;

    if-eq p2, v0, :cond_1

    .line 696
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdm:Lejg;

    invoke-virtual {v0, p2}, Lejg;->aJ(Landroid/view/View;)I

    move-result v0

    .line 697
    if-eqz v0, :cond_1

    .line 698
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 699
    const/4 v1, 0x0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 700
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    .line 701
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0

    .line 705
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    goto :goto_0
.end method

.method public final e(IJ)V
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->Vs:Landroid/animation/LayoutTransition;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, p1, p2, p3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 236
    :cond_0
    return-void
.end method

.method public final eZ(Z)V
    .locals 1

    .prologue
    .line 229
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->Vs:Landroid/animation/LayoutTransition;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 230
    return-void

    .line 229
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Landroid/view/View;II)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 732
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/shared/ui/SuggestionGridLayout;->a(Landroid/view/View;Landroid/view/View;Landroid/view/View;II)V

    .line 733
    return-void
.end method

.method public generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 4

    .prologue
    .line 1118
    new-instance v0, Lekm;

    const/4 v1, -0x1

    const/4 v2, -0x2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lekm;-><init>(III)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 1133
    new-instance v0, Lekm;

    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lekm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1128
    new-instance v0, Lekm;

    invoke-direct {v0, p1}, Lekm;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected getChildDrawingOrder(II)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 262
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdb:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    :goto_0
    return p2

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdb:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 266
    sub-int v0, p1, v0

    .line 267
    if-lt p2, v0, :cond_2

    .line 268
    iget-object v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdb:Ljava/util/ArrayList;

    sub-int v0, p2, v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->indexOfChild(Landroid/view/View;)I

    move-result p2

    .line 269
    if-ltz p2, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v1, "Animating view must be in layout"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v0, p2

    .line 273
    :goto_2
    if-gt v1, v0, :cond_4

    .line 274
    invoke-virtual {p0, v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 275
    iget-object v3, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdb:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 276
    add-int/lit8 v0, v0, 0x1

    .line 273
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    move p2, v0

    .line 280
    goto :goto_0
.end method

.method public final getColumnCount()I
    .locals 1

    .prologue
    .line 303
    iget v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    return v0
.end method

.method public final ia(I)V
    .locals 3

    .prologue
    .line 848
    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildCount()I

    move-result v0

    .line 849
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 850
    invoke-virtual {p0, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 851
    invoke-virtual {v1, p1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 852
    invoke-virtual {p0, v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->removeView(Landroid/view/View;)V

    .line 849
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 855
    :cond_1
    return-void
.end method

.method public final ib(I)V
    .locals 0

    .prologue
    .line 888
    iput p1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccW:I

    .line 889
    return-void
.end method

.method public final ic(I)Landroid/view/View;
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1503
    .line 1507
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdc:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v2

    move-object v3, v4

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekl;

    .line 1508
    invoke-static {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->c(Lekl;)Lekm;

    move-result-object v5

    iget v5, v5, Lekm;->column:I

    if-gez v5, :cond_1

    move v5, v6

    :goto_1
    if-eqz v5, :cond_0

    .line 1509
    iget-object v5, v0, Lekl;->cdr:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    .line 1510
    if-gt v5, p1, :cond_5

    .line 1512
    sub-int v1, p1, v5

    move-object v3, v0

    goto :goto_0

    .line 1508
    :cond_1
    invoke-static {p0}, Leot;->ay(Landroid/view/View;)Z

    move-result v8

    if-eqz v8, :cond_3

    iget v8, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    add-int/lit8 v8, v8, -0x1

    if-ne v5, v8, :cond_2

    move v5, v6

    goto :goto_1

    :cond_2
    move v5, v2

    goto :goto_1

    :cond_3
    if-nez v5, :cond_4

    move v5, v6

    goto :goto_1

    :cond_4
    move v5, v2

    goto :goto_1

    .line 1515
    :cond_5
    sub-int v2, v5, p1

    .line 1520
    :goto_2
    if-eqz v3, :cond_7

    if-nez v0, :cond_7

    .line 1521
    iget-object v4, v3, Lekl;->cdr:Landroid/view/View;

    .line 1528
    :cond_6
    :goto_3
    return-object v4

    .line 1522
    :cond_7
    if-nez v3, :cond_8

    if-eqz v0, :cond_8

    .line 1523
    iget-object v4, v0, Lekl;->cdr:Landroid/view/View;

    goto :goto_3

    .line 1524
    :cond_8
    if-eqz v3, :cond_6

    if-eqz v0, :cond_6

    .line 1526
    if-ge v1, v2, :cond_9

    iget-object v4, v3, Lekl;->cdr:Landroid/view/View;

    goto :goto_3

    :cond_9
    iget-object v4, v0, Lekl;->cdr:Landroid/view/View;

    goto :goto_3

    :cond_a
    move-object v0, v4

    goto :goto_2
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 1714
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1715
    const-class v0, Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 1716
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 328
    iget-object v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccZ:Lekt;

    invoke-virtual {v1, p1}, Lekt;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 329
    if-eqz v1, :cond_0

    .line 330
    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 332
    :cond_0
    if-nez v1, :cond_1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 14

    .prologue
    .line 482
    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->yk:I

    sub-int/2addr v0, v1

    div-int/lit8 v8, v0, 0x2

    .line 485
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    if-nez v0, :cond_2

    .line 486
    const/4 v0, 0x0

    .line 498
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdi:Z

    if-eqz v1, :cond_0

    .line 499
    iget-object v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->mScrollView:Lekf;

    invoke-interface {v1}, Lekf;->ate()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 500
    iget v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdl:I

    sub-int v1, v0, v1

    iput v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdj:I

    .line 501
    iget-object v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->mScrollView:Lekf;

    iget-object v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->mScrollView:Lekf;

    invoke-interface {v2, p0}, Lekf;->aH(Landroid/view/View;)I

    move-result v2

    add-int/2addr v0, v2

    invoke-interface {v1, v0}, Lekf;->setScrollY(I)V

    .line 502
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdi:Z

    .line 505
    :cond_0
    const/4 v2, 0x0

    .line 506
    const/4 v0, 0x0

    .line 507
    const/4 v1, 0x0

    .line 508
    iget-object v3, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    if-eqz v3, :cond_11

    .line 512
    iget v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdl:I

    iget v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdj:I

    add-int v3, v0, v1

    .line 513
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->mScrollView:Lekf;

    invoke-interface {v0}, Lekf;->ate()I

    move-result v0

    add-int v2, v3, v0

    .line 514
    iget v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_8

    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->auf()I

    move-result v0

    neg-int v0, v0

    move v1, v0

    .line 516
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getMeasuredWidth()I

    move-result v0

    iget v4, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->yk:I

    sub-int/2addr v0, v4

    div-int/lit8 v4, v0, 0x2

    iget v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    const/4 v5, 0x1

    if-ne v0, v5, :cond_9

    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->auf()I

    move-result v5

    sub-int v5, v2, v5

    iget-object v6, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->auc()I

    move-result v7

    add-int/2addr v7, v3

    iget v9, v0, Lekm;->topMargin:I

    add-int/2addr v7, v9

    iget v9, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->yk:I

    add-int/2addr v9, v4

    iget v10, v0, Lekm;->bottomMargin:I

    sub-int v10, v5, v10

    invoke-virtual {v6, v4, v7, v9, v10}, Landroid/view/View;->layout(IIII)V

    iget-object v6, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdh:Landroid/view/View;

    iget v7, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->yk:I

    add-int/2addr v7, v4

    invoke-virtual {v6, v4, v5, v7, v2}, Landroid/view/View;->layout(IIII)V

    iget-object v6, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    iget v7, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->yk:I

    add-int/2addr v7, v4

    iget-object v9, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    invoke-virtual {v9}, Lekl;->getMeasuredHeight()I

    invoke-virtual {v6, v4, v5, v7}, Lekl;->y(III)V

    iput v3, v0, Lekm;->cdH:I

    sub-int v4, v5, v3

    iput v4, v0, Lekm;->cdI:I

    iget v4, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdl:I

    iget v5, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccR:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    iput v4, v0, Lekm;->cdG:I

    .line 519
    :goto_2
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccX:[I

    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getPaddingTop()I

    move-result v4

    invoke-static {v0, v4}, Ljava/util/Arrays;->fill([II)V

    .line 520
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdc:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 521
    const/4 v0, 0x0

    move v7, v0

    :goto_3
    if-ge v7, v9, :cond_e

    .line 522
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdc:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekl;

    .line 523
    invoke-virtual {v0}, Lekl;->aug()Z

    move-result v4

    if-nez v4, :cond_1

    .line 524
    invoke-static {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->c(Lekl;)Lekm;

    move-result-object v5

    .line 528
    iget v4, v5, Lekm;->column:I

    const/4 v6, -0x1

    if-ne v4, v6, :cond_a

    const/4 v4, 0x0

    .line 529
    :goto_4
    iget v6, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccY:I

    iget v10, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccS:I

    add-int/2addr v6, v10

    mul-int/2addr v6, v4

    add-int v10, v8, v6

    .line 530
    iget-object v6, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccX:[I

    aget v6, v6, v4

    iget v11, v5, Lekm;->topMargin:I

    add-int/2addr v6, v11

    .line 532
    invoke-virtual {v0}, Lekl;->getMeasuredHeight()I

    move-result v11

    add-int/2addr v11, v6

    iget v12, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccR:I

    add-int/2addr v11, v12

    iget v12, v5, Lekm;->bottomMargin:I

    add-int/2addr v11, v12

    .line 533
    iget v12, v5, Lekm;->column:I

    const/4 v13, -0x1

    if-ne v12, v13, :cond_b

    .line 534
    iget-object v5, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccX:[I

    invoke-static {v5, v11}, Ljava/util/Arrays;->fill([II)V

    .line 542
    :goto_5
    iget-object v5, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    if-eqz v5, :cond_10

    .line 543
    iget-object v5, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    if-eq v0, v5, :cond_1

    .line 544
    iget-object v5, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdn:[I

    aget v5, v5, v4

    if-gt v11, v5, :cond_c

    .line 548
    iget-object v5, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdn:[I

    aget v4, v5, v4

    sub-int/2addr v4, v3

    sub-int v4, v6, v4

    .line 560
    :goto_6
    invoke-virtual {v0}, Lekl;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v10

    .line 561
    invoke-virtual {v0}, Lekl;->getMeasuredHeight()I

    .line 562
    invoke-virtual {v0, v10, v4, v5}, Lekl;->y(III)V

    .line 521
    :cond_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_3

    .line 493
    :cond_2
    iget v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdl:I

    iget-object v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    invoke-virtual {v1}, Lekl;->getMeasuredHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int v3, v0, v1

    .line 494
    iget-object v5, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdn:[I

    iget-object v6, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdo:[Z

    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getPaddingTop()I

    move-result v0

    invoke-static {v5, v0}, Ljava/util/Arrays;->fill([II)V

    const/4 v0, 0x0

    invoke-static {v6, v0}, Ljava/util/Arrays;->fill([ZZ)V

    array-length v2, v6

    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdc:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v0, 0x0

    move v4, v0

    :goto_7
    if-ge v4, v7, :cond_12

    if-lez v2, :cond_12

    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdc:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekl;

    invoke-virtual {v0}, Lekl;->aug()Z

    move-result v1

    if-nez v1, :cond_7

    invoke-static {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->c(Lekl;)Lekm;

    move-result-object v9

    iget v1, v9, Lekm;->column:I

    const/4 v10, -0x1

    if-ne v1, v10, :cond_3

    const/4 v1, 0x0

    aget v1, v5, v1

    iget v10, v9, Lekm;->topMargin:I

    add-int/2addr v1, v10

    :goto_8
    invoke-virtual {v0}, Lekl;->getMeasuredHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v10, v1

    if-lt v10, v3, :cond_5

    iget v0, v9, Lekm;->column:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    invoke-static {v6, v0}, Ljava/util/Arrays;->fill([ZZ)V

    const/4 v0, 0x0

    :goto_9
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v2, v0

    goto :goto_7

    :cond_3
    iget v1, v9, Lekm;->column:I

    aget-boolean v1, v6, v1

    if-nez v1, :cond_7

    iget v1, v9, Lekm;->column:I

    aget v1, v5, v1

    iget v10, v9, Lekm;->topMargin:I

    add-int/2addr v1, v10

    goto :goto_8

    :cond_4
    iget v0, v9, Lekm;->column:I

    const/4 v1, 0x1

    aput-boolean v1, v6, v0

    add-int/lit8 v0, v2, -0x1

    goto :goto_9

    :cond_5
    invoke-virtual {v0}, Lekl;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccR:I

    add-int/2addr v0, v1

    iget v1, v9, Lekm;->bottomMargin:I

    add-int/2addr v0, v1

    iget v1, v9, Lekm;->column:I

    const/4 v10, -0x1

    if-ne v1, v10, :cond_6

    invoke-static {v5, v0}, Ljava/util/Arrays;->fill([II)V

    move v0, v2

    goto :goto_9

    :cond_6
    iget v1, v9, Lekm;->column:I

    aput v0, v5, v1

    :cond_7
    move v0, v2

    goto :goto_9

    .line 514
    :cond_8
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    invoke-virtual {v0}, Lekl;->getMeasuredHeight()I

    move-result v0

    neg-int v0, v0

    move v1, v0

    goto/16 :goto_1

    .line 516
    :cond_9
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    invoke-direct {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->auc()I

    move-result v5

    add-int/2addr v5, v3

    iget v0, v0, Lekm;->topMargin:I

    add-int/2addr v0, v5

    iget-object v5, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdh:Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    invoke-virtual {v6}, Lekl;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v4

    iget-object v7, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    invoke-virtual {v7}, Lekl;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v0

    invoke-virtual {v5, v4, v0, v6, v7}, Landroid/view/View;->layout(IIII)V

    iget-object v5, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    iget-object v6, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    invoke-virtual {v6}, Lekl;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v4

    iget-object v7, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    invoke-virtual {v7}, Lekl;->getMeasuredHeight()I

    invoke-virtual {v5, v4, v0, v6}, Lekl;->y(III)V

    iget v5, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccY:I

    iget v6, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccS:I

    add-int/2addr v5, v6

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v4

    iget-object v7, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v0

    invoke-virtual {v5, v4, v0, v6, v7}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_2

    .line 528
    :cond_a
    iget v4, v5, Lekm;->column:I

    goto/16 :goto_4

    .line 536
    :cond_b
    iget-object v12, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccX:[I

    iget v5, v5, Lekm;->column:I

    aput v11, v12, v5

    goto/16 :goto_5

    .line 553
    :cond_c
    iget-object v5, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    invoke-static {v5}, Lcom/google/android/shared/ui/SuggestionGridLayout;->c(Lekl;)Lekm;

    move-result-object v5

    iget v5, v5, Lekm;->column:I

    if-ne v5, v4, :cond_d

    add-int v5, v2, v1

    .line 556
    :goto_a
    iget-object v11, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdn:[I

    aget v4, v11, v4

    sub-int v4, v5, v4

    add-int/2addr v4, v6

    goto/16 :goto_6

    :cond_d
    move v5, v2

    .line 553
    goto :goto_a

    .line 564
    :cond_e
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdd:I

    .line 565
    iget-boolean v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdk:Z

    if-eqz v0, :cond_f

    .line 566
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdk:Z

    .line 567
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->eZ(Z)V

    .line 569
    :cond_f
    return-void

    :cond_10
    move v4, v6

    goto/16 :goto_6

    :cond_11
    move v3, v2

    move v2, v0

    goto/16 :goto_2

    :cond_12
    move v0, v3

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 15

    .prologue
    .line 346
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 347
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v9

    .line 348
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 349
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 351
    const/4 v2, 0x0

    .line 352
    const/4 v0, 0x0

    .line 353
    iget v5, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccS:I

    iget v6, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    add-int/lit8 v6, v6, -0x1

    mul-int/2addr v5, v6

    .line 354
    sparse-switch v3, :sswitch_data_0

    move v1, v2

    .line 373
    :goto_0
    iget v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    mul-int/2addr v2, v0

    add-int/2addr v2, v5

    iput v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->yk:I

    .line 374
    iput v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccY:I

    .line 375
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 377
    iget v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->yk:I

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 380
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccX:[I

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 382
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdc:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 383
    const/4 v2, 0x0

    .line 385
    const/4 v0, 0x0

    move v8, v0

    :goto_1
    if-ge v8, v10, :cond_6

    .line 386
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdc:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekl;

    .line 387
    invoke-virtual {v0}, Lekl;->aug()Z

    move-result v3

    if-nez v3, :cond_3

    .line 388
    invoke-static {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->c(Lekl;)Lekm;

    move-result-object v11

    .line 394
    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    .line 396
    iget v3, v11, Lekm;->column:I

    .line 397
    const/4 v5, -0x1

    if-ne v3, v5, :cond_4

    .line 399
    const/4 v3, 0x0

    move v5, v6

    .line 403
    :goto_2
    const/4 v13, -0x2

    if-ne v3, v13, :cond_0

    .line 404
    invoke-static {p0}, Leot;->ay(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x0

    .line 406
    :cond_0
    :goto_3
    add-int/lit8 v13, v10, -0x1

    if-ne v8, v13, :cond_1

    iget-boolean v13, v11, Lekm;->cdw:Z

    if-eqz v13, :cond_1

    move-object v2, v0

    .line 409
    :cond_1
    iget-object v13, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    if-ne v0, v13, :cond_2

    .line 411
    iget-object v13, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccX:[I

    aget v13, v13, v3

    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getPaddingTop()I

    move-result v14

    add-int/2addr v13, v14

    iput v13, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdl:I

    .line 413
    :cond_2
    invoke-virtual {v0, v5, v12}, Lekl;->measure(II)V

    .line 414
    iget-object v5, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccX:[I

    aget v12, v5, v3

    iget v13, v11, Lekm;->topMargin:I

    invoke-virtual {v0}, Lekl;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v13

    iget v13, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccR:I

    add-int/2addr v0, v13

    iget v13, v11, Lekm;->bottomMargin:I

    add-int/2addr v0, v13

    add-int/2addr v0, v12

    aput v0, v5, v3

    .line 416
    iget v0, v11, Lekm;->column:I

    const/4 v5, -0x1

    if-ne v0, v5, :cond_3

    .line 417
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccX:[I

    iget-object v5, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccX:[I

    aget v3, v5, v3

    invoke-static {v0, v3}, Ljava/util/Arrays;->fill([II)V

    .line 385
    :cond_3
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_1

    .line 357
    :sswitch_0
    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getPaddingLeft()I

    move-result v0

    sub-int v0, v1, v0

    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    .line 358
    iget v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccP:I

    sub-int/2addr v0, v5

    iget v3, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    div-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto/16 :goto_0

    .line 362
    :sswitch_1
    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getPaddingRight()I

    move-result v2

    add-int/2addr v2, v0

    .line 363
    sub-int v0, v1, v2

    .line 364
    iget v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccP:I

    sub-int/2addr v0, v5

    iget v3, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    div-int/2addr v0, v3

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 365
    iget v1, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    mul-int/2addr v1, v0

    add-int/2addr v1, v5

    add-int/2addr v1, v2

    .line 366
    goto/16 :goto_0

    .line 369
    :sswitch_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot measure SuggestionGridLayout with mode UNSPECIFIED"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v5, v7

    .line 401
    goto/16 :goto_2

    .line 404
    :cond_5
    iget v3, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    add-int/lit8 v3, v3, -0x1

    goto/16 :goto_3

    .line 422
    :cond_6
    const/high16 v0, 0x40000000    # 2.0f

    if-eq v9, v0, :cond_e

    .line 423
    const/4 v3, 0x0

    .line 424
    const/4 v0, 0x0

    :goto_4
    iget v4, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    if-ge v0, v4, :cond_8

    .line 425
    iget-object v4, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccX:[I

    aget v4, v4, v0

    iget v5, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccR:I

    sub-int/2addr v4, v5

    .line 426
    if-le v4, v3, :cond_7

    move v3, v4

    .line 424
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 430
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getPaddingTop()I

    move-result v0

    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getPaddingBottom()I

    move-result v3

    add-int/2addr v0, v3

    iget v3, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccW:I

    add-int/2addr v0, v3

    move v3, v0

    .line 433
    :goto_5
    if-eqz v2, :cond_9

    .line 434
    invoke-static {v2}, Lcom/google/android/shared/ui/SuggestionGridLayout;->c(Lekl;)Lekm;

    move-result-object v0

    .line 436
    iget v4, v0, Lekm;->column:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_c

    const/4 v0, 0x0

    .line 437
    :goto_6
    iget-object v4, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccX:[I

    aget v0, v4, v0

    iget v4, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccR:I

    sub-int/2addr v0, v4

    invoke-virtual {v2}, Lekl;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v0, v4

    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getPaddingTop()I

    move-result v4

    add-int/2addr v0, v4

    .line 439
    invoke-virtual {v2}, Lekl;->getMeasuredWidth()I

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    sub-int v0, v3, v0

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v2, v4, v0}, Lekl;->measure(II)V

    .line 445
    :cond_9
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    if-eqz v0, :cond_b

    .line 446
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    iget-object v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->mScrollView:Lekf;

    invoke-interface {v2}, Lekf;->ate()I

    move-result v2

    iget v4, v0, Lekm;->topMargin:I

    iget v0, v0, Lekm;->bottomMargin:I

    add-int/2addr v0, v4

    invoke-direct {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->auc()I

    move-result v4

    add-int/2addr v0, v4

    sub-int v0, v2, v0

    iget v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_a

    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->auf()I

    move-result v2

    sub-int/2addr v0, v2

    :cond_a
    iget v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccQ:I

    const/4 v4, 0x1

    if-le v2, v4, :cond_d

    iget-object v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v7, v4}, Landroid/view/View;->measure(II)V

    iget-object v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    if-gt v2, v0, :cond_d

    iget-object v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdf:Lekl;

    invoke-virtual {v2}, Lekl;->getMeasuredHeight()I

    move-result v2

    iget v4, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccU:I

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    if-ge v2, v0, :cond_b

    iget-object v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v2, v7, v0}, Landroid/view/View;->measure(II)V

    .line 448
    :cond_b
    :goto_7
    invoke-virtual {p0, v1, v3}, Lcom/google/android/shared/ui/SuggestionGridLayout;->setMeasuredDimension(II)V

    .line 449
    return-void

    .line 436
    :cond_c
    iget v0, v0, Lekm;->column:I

    goto/16 :goto_6

    .line 446
    :cond_d
    iget-object v2, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    const/high16 v4, -0x80000000

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v2, v7, v0}, Landroid/view/View;->measure(II)V

    goto :goto_7

    :cond_e
    move v3, v4

    goto/16 :goto_5

    .line 354
    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_2
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccZ:Lekt;

    invoke-virtual {v0, p1}, Lekt;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 339
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 341
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public removeAllViews()V
    .locals 1

    .prologue
    .line 1060
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cde:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1061
    invoke-virtual {p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aud()V

    .line 1063
    :cond_0
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdb:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1064
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->cdc:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1065
    invoke-super {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1066
    return-void
.end method

.method public removeView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 860
    invoke-virtual {p0, p1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aM(Landroid/view/View;)V

    .line 861
    return-void
.end method

.method public final u(Landroid/view/View;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 728
    const/4 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/shared/ui/SuggestionGridLayout;->a(Landroid/view/View;Landroid/view/View;Landroid/view/View;II)V

    .line 729
    return-void
.end method

.method public final w(ZZ)V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccZ:Lekt;

    iput-boolean p1, v0, Lekt;->cel:Z

    .line 203
    iget-object v0, p0, Lcom/google/android/shared/ui/SuggestionGridLayout;->ccZ:Lekt;

    iput-boolean p2, v0, Lekt;->cem:Z

    .line 204
    return-void
.end method
