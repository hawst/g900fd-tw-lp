.class public Lcom/google/android/shared/speech/HotwordResult;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final anB:I

.field private final anC:I

.field private final caa:Z

.field private final cab:F

.field private final cac:F

.field private final cad:F

.field private final cae:F

.field private final mAudio:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lehp;

    invoke-direct {v0}, Lehp;-><init>()V

    sput-object v0, Lcom/google/android/shared/speech/HotwordResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(IZFF[BIFF)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput p1, p0, Lcom/google/android/shared/speech/HotwordResult;->anC:I

    .line 73
    iput-boolean p2, p0, Lcom/google/android/shared/speech/HotwordResult;->caa:Z

    .line 74
    iput p3, p0, Lcom/google/android/shared/speech/HotwordResult;->cab:F

    .line 75
    iput p4, p0, Lcom/google/android/shared/speech/HotwordResult;->cac:F

    .line 76
    iput-object p5, p0, Lcom/google/android/shared/speech/HotwordResult;->mAudio:[B

    .line 77
    iput p6, p0, Lcom/google/android/shared/speech/HotwordResult;->anB:I

    .line 78
    iput p7, p0, Lcom/google/android/shared/speech/HotwordResult;->cae:F

    .line 79
    iput p8, p0, Lcom/google/android/shared/speech/HotwordResult;->cad:F

    .line 80
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/speech/HotwordResult;->anC:I

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/shared/speech/HotwordResult;->caa:Z

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/shared/speech/HotwordResult;->cab:F

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/shared/speech/HotwordResult;->cac:F

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/speech/HotwordResult;->mAudio:[B

    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/speech/HotwordResult;->anB:I

    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/shared/speech/HotwordResult;->cae:F

    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/shared/speech/HotwordResult;->cad:F

    .line 91
    return-void

    .line 84
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/google/android/shared/speech/HotwordResult;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static a(FF[BIFF)Lcom/google/android/shared/speech/HotwordResult;
    .locals 9

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/shared/speech/HotwordResult;

    const/4 v1, 0x2

    const/4 v2, 0x1

    move v3, p0

    move v4, p1

    move-object v5, p2

    move v6, p3

    move v7, p4

    move v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/shared/speech/HotwordResult;-><init>(IZFF[BIFF)V

    return-object v0
.end method

.method public static a(F[BI)Lcom/google/android/shared/speech/HotwordResult;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 42
    new-instance v0, Lcom/google/android/shared/speech/HotwordResult;

    move v2, v1

    move v3, p0

    move-object v5, p1

    move v6, p2

    move v7, v4

    move v8, v4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/shared/speech/HotwordResult;-><init>(IZFF[BIFF)V

    return-object v0
.end method

.method public static b(FF[BIFF)Lcom/google/android/shared/speech/HotwordResult;
    .locals 9

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/shared/speech/HotwordResult;

    const/4 v1, 0x2

    const/4 v2, 0x0

    move v3, p0

    move v4, p1

    move-object v5, p2

    move v6, p3

    move v7, p4

    move v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/shared/speech/HotwordResult;-><init>(IZFF[BIFF)V

    return-object v0
.end method

.method public static b(F[BI)Lcom/google/android/shared/speech/HotwordResult;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 47
    new-instance v0, Lcom/google/android/shared/speech/HotwordResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    move v3, p0

    move-object v5, p1

    move v6, p2

    move v7, v4

    move v8, v4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/shared/speech/HotwordResult;-><init>(IZFF[BIFF)V

    return-object v0
.end method

.method public static c(F[BI)Lcom/google/android/shared/speech/HotwordResult;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 52
    new-instance v0, Lcom/google/android/shared/speech/HotwordResult;

    const/4 v1, 0x3

    const/4 v2, 0x0

    move v3, p0

    move-object v5, p1

    move v6, p2

    move v7, v4

    move v8, v4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/shared/speech/HotwordResult;-><init>(IZFF[BIFF)V

    return-object v0
.end method


# virtual methods
.method public final asQ()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/google/android/shared/speech/HotwordResult;->anC:I

    return v0
.end method

.method public final asR()Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/google/android/shared/speech/HotwordResult;->caa:Z

    return v0
.end method

.method public final asS()F
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/google/android/shared/speech/HotwordResult;->cac:F

    return v0
.end method

.method public final asT()F
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lcom/google/android/shared/speech/HotwordResult;->cad:F

    return v0
.end method

.method public final asU()F
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/google/android/shared/speech/HotwordResult;->cae:F

    return v0
.end method

.method public final asV()[B
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/shared/speech/HotwordResult;->mAudio:[B

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x0

    return v0
.end method

.method public final getHotwordScore()F
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/google/android/shared/speech/HotwordResult;->cab:F

    return v0
.end method

.method public final getSampleRate()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/google/android/shared/speech/HotwordResult;->anB:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[Hotword detected., SpeakerTriggered="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/shared/speech/HotwordResult;->caa:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", HotwordScore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/shared/speech/HotwordResult;->cab:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", SpeakerScore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/shared/speech/HotwordResult;->cac:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Sample rate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/shared/speech/HotwordResult;->anB:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/google/android/shared/speech/HotwordResult;->anC:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 169
    iget-boolean v0, p0, Lcom/google/android/shared/speech/HotwordResult;->caa:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 170
    iget v0, p0, Lcom/google/android/shared/speech/HotwordResult;->cab:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 171
    iget v0, p0, Lcom/google/android/shared/speech/HotwordResult;->cac:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 172
    iget-object v0, p0, Lcom/google/android/shared/speech/HotwordResult;->mAudio:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 173
    iget v0, p0, Lcom/google/android/shared/speech/HotwordResult;->anB:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 174
    iget v0, p0, Lcom/google/android/shared/speech/HotwordResult;->cad:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 175
    iget v0, p0, Lcom/google/android/shared/speech/HotwordResult;->cae:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 176
    return-void

    .line 169
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
