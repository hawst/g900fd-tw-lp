.class public Lcom/google/android/shared/search/SearchBoxStats;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Leti;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final acx:Ljava/lang/String;

.field public final bKO:I

.field public final bKP:I

.field public final bVV:I

.field public final bVW:I

.field public final bVX:Ljava/util/List;

.field public final bVZ:I

.field public final bWa:I

.field public final bWb:Ljava/lang/String;

.field public final bWc:J

.field public final bWd:J

.field public final bWe:J

.field public final bWf:J

.field public final bWg:J

.field public final bWh:J

.field public final bWk:J

.field public final bWl:J

.field public final bWm:I

.field public final bWn:I

.field public final bWo:J

.field public final bWq:I

.field public final bWr:I

.field public final bWs:I

.field public final bZC:I

.field public final bZD:J

.field public final bZE:Lcom/google/android/shared/search/SuggestionLogInfo;

.field public final bZF:I

.field public final bZG:I

.field public final bZH:I

.field public final bZI:J

.field public final bZJ:Z

.field public final beJ:Ljava/lang/String;

.field public final bfO:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 490
    new-instance v0, Lehi;

    invoke-direct {v0}, Lehi;-><init>()V

    sput-object v0, Lcom/google/android/shared/search/SearchBoxStats;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->acx:Ljava/lang/String;

    .line 163
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->beJ:Ljava/lang/String;

    .line 164
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZC:I

    .line 165
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVV:I

    .line 166
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVW:I

    .line 168
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVX:Ljava/util/List;

    .line 169
    iget-object v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVX:Ljava/util/List;

    sget-object v2, Lcom/google/android/shared/search/Suggestion;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 170
    const-class v0, Lcom/google/android/shared/search/SuggestionLogInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/SuggestionLogInfo;

    iput-object v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZE:Lcom/google/android/shared/search/SuggestionLogInfo;

    .line 171
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVZ:I

    .line 172
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWa:I

    .line 173
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWb:Ljava/lang/String;

    .line 175
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWc:J

    .line 176
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWd:J

    .line 177
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWe:J

    .line 178
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWf:J

    .line 179
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWg:J

    .line 180
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWh:J

    .line 181
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZD:J

    .line 182
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWk:J

    .line 183
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWl:J

    .line 185
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWm:I

    .line 186
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWn:I

    .line 187
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWo:J

    .line 189
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZF:I

    .line 190
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bfO:I

    .line 191
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZG:I

    .line 192
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZH:I

    .line 193
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZI:J

    .line 195
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZJ:Z

    .line 196
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWq:I

    .line 197
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWr:I

    .line 198
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWs:I

    .line 199
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bKO:I

    .line 200
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bKP:I

    .line 201
    return-void

    .line 195
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lehj;)V
    .locals 2

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    iget-object v0, p1, Lehj;->acx:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->acx:Ljava/lang/String;

    .line 121
    iget-object v0, p1, Lehj;->beJ:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->beJ:Ljava/lang/String;

    .line 122
    iget v0, p1, Lehj;->bZC:I

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZC:I

    .line 123
    iget v0, p1, Lehj;->bVV:I

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVV:I

    .line 124
    iget v0, p1, Lehj;->bVW:I

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVW:I

    .line 126
    iget-object v0, p1, Lehj;->bVX:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVX:Ljava/util/List;

    .line 127
    iget-object v0, p1, Lehj;->bZE:Lcom/google/android/shared/search/SuggestionLogInfo;

    iput-object v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZE:Lcom/google/android/shared/search/SuggestionLogInfo;

    .line 128
    iget v0, p1, Lehj;->bVZ:I

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVZ:I

    .line 129
    iget v0, p1, Lehj;->bWa:I

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWa:I

    .line 130
    iget-object v0, p1, Lehj;->bWb:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWb:Ljava/lang/String;

    .line 132
    iget-wide v0, p1, Lehj;->bWc:J

    iput-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWc:J

    .line 133
    iget-wide v0, p1, Lehj;->bWd:J

    iput-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWd:J

    .line 134
    iget-wide v0, p1, Lehj;->bWe:J

    iput-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWe:J

    .line 135
    iget-wide v0, p1, Lehj;->bWf:J

    iput-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWf:J

    .line 136
    iget-wide v0, p1, Lehj;->bWg:J

    iput-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWg:J

    .line 137
    iget-wide v0, p1, Lehj;->bWh:J

    iput-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWh:J

    .line 138
    iget-wide v0, p1, Lehj;->bZD:J

    iput-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZD:J

    .line 139
    iget-wide v0, p1, Lehj;->bWk:J

    iput-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWk:J

    .line 140
    iget-wide v0, p1, Lehj;->bWl:J

    iput-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWl:J

    .line 142
    iget v0, p1, Lehj;->bWm:I

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWm:I

    .line 143
    iget v0, p1, Lehj;->bWn:I

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWn:I

    .line 144
    iget-wide v0, p1, Lehj;->bWo:J

    iput-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWo:J

    .line 146
    iget v0, p1, Lehj;->bZF:I

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZF:I

    .line 147
    iget v0, p1, Lehj;->bfO:I

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bfO:I

    .line 148
    iget v0, p1, Lehj;->bZG:I

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZG:I

    .line 149
    iget v0, p1, Lehj;->bZH:I

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZH:I

    .line 150
    iget-wide v0, p1, Lehj;->bZI:J

    iput-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZI:J

    .line 152
    iget-boolean v0, p1, Lehj;->bZJ:Z

    iput-boolean v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZJ:Z

    .line 153
    iget v0, p1, Lehj;->bWq:I

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWq:I

    .line 154
    iget v0, p1, Lehj;->bWr:I

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWr:I

    .line 155
    iget v0, p1, Lehj;->bWs:I

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWs:I

    .line 156
    iget v0, p1, Lehj;->bKO:I

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bKO:I

    .line 157
    iget v0, p1, Lehj;->bKP:I

    iput v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bKP:I

    .line 158
    return-void
.end method

.method public static aA(Ljava/lang/String;Ljava/lang/String;)Lehj;
    .locals 1

    .prologue
    .line 512
    new-instance v0, Lehj;

    invoke-direct {v0, p0, p1}, Lehj;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final QW()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->acx:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Letj;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 777
    const-string v0, "SearchBoxStats"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 778
    const-string v0, "client ID"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->acx:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 779
    const-string v0, "source"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->beJ:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 780
    return-void
.end method

.method public final arA()J
    .locals 2

    .prologue
    .line 312
    iget-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWo:J

    return-wide v0
.end method

.method public final arB()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 319
    iget v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVZ:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final arC()Z
    .locals 2

    .prologue
    .line 326
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVZ:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final arD()Ljava/util/List;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVX:Ljava/util/List;

    return-object v0
.end method

.method public final arE()Lcom/google/android/shared/search/SuggestionLogInfo;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZE:Lcom/google/android/shared/search/SuggestionLogInfo;

    return-object v0
.end method

.method public final arF()I
    .locals 1

    .prologue
    .line 350
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVZ:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 352
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWa:I

    return v0

    .line 350
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final arG()Ljava/lang/String;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWb:Ljava/lang/String;

    return-object v0
.end method

.method public final arH()I
    .locals 1

    .prologue
    .line 365
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVZ:I

    return v0
.end method

.method public final arI()J
    .locals 2

    .prologue
    .line 370
    iget-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWc:J

    return-wide v0
.end method

.method public final arJ()I
    .locals 1

    .prologue
    .line 379
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZF:I

    return v0
.end method

.method public final arK()I
    .locals 1

    .prologue
    .line 387
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bfO:I

    return v0
.end method

.method public final arL()I
    .locals 1

    .prologue
    .line 392
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZG:I

    return v0
.end method

.method public final arM()I
    .locals 1

    .prologue
    .line 397
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZH:I

    return v0
.end method

.method public final arN()J
    .locals 2

    .prologue
    .line 402
    iget-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZI:J

    return-wide v0
.end method

.method public final arO()Z
    .locals 1

    .prologue
    .line 407
    iget-boolean v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZJ:Z

    return v0
.end method

.method public final arP()I
    .locals 1

    .prologue
    .line 412
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWq:I

    return v0
.end method

.method public final arQ()I
    .locals 1

    .prologue
    .line 420
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWr:I

    return v0
.end method

.method public final arR()I
    .locals 1

    .prologue
    .line 425
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWs:I

    return v0
.end method

.method public final arS()I
    .locals 1

    .prologue
    .line 430
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bKO:I

    return v0
.end method

.method public final arT()I
    .locals 1

    .prologue
    .line 435
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bKP:I

    return v0
.end method

.method public final arU()Lehj;
    .locals 1

    .prologue
    .line 517
    new-instance v0, Lehj;

    invoke-direct {v0, p0}, Lehj;-><init>(Lcom/google/android/shared/search/SearchBoxStats;)V

    return-object v0
.end method

.method public final arn()I
    .locals 1

    .prologue
    .line 214
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZC:I

    return v0
.end method

.method public final aro()I
    .locals 1

    .prologue
    .line 219
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVV:I

    return v0
.end method

.method public final arp()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVW:I

    return v0
.end method

.method public final arq()J
    .locals 2

    .prologue
    .line 232
    iget-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWd:J

    return-wide v0
.end method

.method public final arr()J
    .locals 2

    .prologue
    .line 240
    iget-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWe:J

    return-wide v0
.end method

.method public final ars()J
    .locals 2

    .prologue
    .line 248
    iget-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWf:J

    return-wide v0
.end method

.method public final art()J
    .locals 2

    .prologue
    .line 256
    iget-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWg:J

    return-wide v0
.end method

.method public final aru()J
    .locals 2

    .prologue
    .line 264
    iget-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWh:J

    return-wide v0
.end method

.method public final arv()J
    .locals 2

    .prologue
    .line 272
    iget-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZD:J

    return-wide v0
.end method

.method public final arw()J
    .locals 2

    .prologue
    .line 280
    iget-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWk:J

    return-wide v0
.end method

.method public final arx()J
    .locals 2

    .prologue
    .line 288
    iget-wide v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWl:J

    return-wide v0
.end method

.method public final ary()I
    .locals 1

    .prologue
    .line 296
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWm:I

    return v0
.end method

.method public final arz()I
    .locals 1

    .prologue
    .line 305
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWn:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 445
    const/4 v0, 0x0

    return v0
.end method

.method public final getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->beJ:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 440
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SearchBoxStats["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->acx:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->beJ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 450
    iget-object v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->acx:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 451
    iget-object v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->beJ:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 452
    iget v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZC:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 453
    iget v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVV:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 454
    iget v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVW:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 456
    iget-object v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVX:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 457
    iget-object v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZE:Lcom/google/android/shared/search/SuggestionLogInfo;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 458
    iget v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->bVZ:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 459
    iget v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWa:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 460
    iget-object v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWb:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 462
    iget-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWc:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 463
    iget-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWd:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 464
    iget-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWe:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 465
    iget-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWf:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 466
    iget-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWg:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 467
    iget-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWh:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 468
    iget-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZD:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 469
    iget-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWk:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 470
    iget-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWl:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 472
    iget v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWm:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 473
    iget v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWn:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 474
    iget-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWo:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 476
    iget v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZF:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 477
    iget v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->bfO:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 478
    iget v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZG:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 479
    iget v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZH:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 480
    iget-wide v2, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZI:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 482
    iget-boolean v1, p0, Lcom/google/android/shared/search/SearchBoxStats;->bZJ:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 483
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWq:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 484
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWr:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 485
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bWs:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 486
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bKO:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 487
    iget v0, p0, Lcom/google/android/shared/search/SearchBoxStats;->bKP:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 488
    return-void
.end method
