.class public Lcom/google/android/shared/util/BitFlags;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final cfl:Ljava/lang/Class;

.field private final cfm:Ljava/lang/String;

.field private cfn:J

.field private cfo:Landroid/util/LongSparseArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 306
    new-instance v0, Lemh;

    invoke-direct {v0}, Lemh;-><init>()V

    sput-object v0, Lcom/google/android/shared/util/BitFlags;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 50
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;-><init>(Ljava/lang/Class;J)V

    .line 51
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/shared/util/BitFlags;-><init>(Ljava/lang/Class;J)V

    .line 55
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;)V
    .locals 2

    .prologue
    .line 58
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/shared/util/BitFlags;-><init>(Ljava/lang/Class;J)V

    .line 59
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;J)V
    .locals 2

    .prologue
    .line 62
    const-string v0, "FLAG_"

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/shared/util/BitFlags;-><init>(Ljava/lang/Class;Ljava/lang/String;J)V

    .line 63
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-wide p3, p0, Lcom/google/android/shared/util/BitFlags;->cfn:J

    .line 67
    iput-object p1, p0, Lcom/google/android/shared/util/BitFlags;->cfl:Ljava/lang/Class;

    .line 68
    iput-object p2, p0, Lcom/google/android/shared/util/BitFlags;->cfm:Ljava/lang/String;

    .line 69
    return-void
.end method

.method private final aS(J)Ljava/lang/String;
    .locals 9

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/shared/util/BitFlags;->cfo:Landroid/util/LongSparseArray;

    if-nez v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/google/android/shared/util/BitFlags;->cfl:Ljava/lang/Class;

    invoke-direct {p0, v0}, Lcom/google/android/shared/util/BitFlags;->c(Ljava/lang/Class;)Landroid/util/LongSparseArray;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/shared/util/BitFlags;->cfo:Landroid/util/LongSparseArray;

    .line 212
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 215
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/shared/util/BitFlags;->cfo:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 216
    iget-object v0, p0, Lcom/google/android/shared/util/BitFlags;->cfo:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    .line 219
    and-long v6, p1, v4

    cmp-long v0, v6, v4

    if-nez v0, :cond_1

    .line 221
    iget-object v0, p0, Lcom/google/android/shared/util/BitFlags;->cfo:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v4, v5}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 222
    iget-object v3, p0, Lcom/google/android/shared/util/BitFlags;->cfm:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 227
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 228
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 231
    :goto_1
    return-object v0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private c(Ljava/lang/Class;)Landroid/util/LongSparseArray;
    .locals 8

    .prologue
    .line 240
    new-instance v1, Landroid/util/LongSparseArray;

    invoke-direct {v1}, Landroid/util/LongSparseArray;-><init>()V

    .line 242
    if-eqz p1, :cond_1

    .line 243
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 244
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 245
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    .line 248
    iget-object v6, p0, Lcom/google/android/shared/util/BitFlags;->cfm:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v6

    invoke-static {v6}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 250
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {v4, v6}, Ljava/lang/reflect/Field;->getLong(Ljava/lang/Object;)J

    move-result-wide v6

    .line 256
    invoke-virtual {v1, v6, v7, v5}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 265
    :cond_1
    return-object v1

    :catch_0
    move-exception v4

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/shared/util/BitFlags;)Z
    .locals 2

    .prologue
    .line 89
    iget-wide v0, p1, Lcom/google/android/shared/util/BitFlags;->cfn:J

    invoke-virtual {p0, v0, v1}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final aO(J)Z
    .locals 3

    .prologue
    .line 97
    iget-wide v0, p0, Lcom/google/android/shared/util/BitFlags;->cfn:J

    and-long/2addr v0, p1

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aP(J)Z
    .locals 5

    .prologue
    .line 112
    iget-wide v0, p0, Lcom/google/android/shared/util/BitFlags;->cfn:J

    and-long/2addr v0, p1

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aQ(J)Z
    .locals 3

    .prologue
    .line 131
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/shared/util/BitFlags;->e(JJ)Z

    move-result v0

    return v0
.end method

.method public final aR(J)Z
    .locals 3

    .prologue
    .line 150
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/google/android/shared/util/BitFlags;->e(JJ)Z

    move-result v0

    return v0
.end method

.method public final auA()Ljava/lang/String;
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/shared/util/BitFlags;->cfl:Ljava/lang/Class;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    iget-wide v0, p0, Lcom/google/android/shared/util/BitFlags;->cfn:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/shared/util/BitFlags;->aS(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final auz()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 77
    iget-wide v0, p0, Lcom/google/android/shared/util/BitFlags;->cfn:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 78
    iput-wide v2, p0, Lcom/google/android/shared/util/BitFlags;->cfn:J

    .line 79
    invoke-virtual {p0}, Lcom/google/android/shared/util/BitFlags;->onChanged()V

    .line 80
    const/4 v0, 0x1

    .line 82
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/shared/util/BitFlags;)Z
    .locals 2

    .prologue
    .line 104
    iget-wide v0, p1, Lcom/google/android/shared/util/BitFlags;->cfn:J

    invoke-virtual {p0, v0, v1}, Lcom/google/android/shared/util/BitFlags;->aP(J)Z

    move-result v0

    return v0
.end method

.method public final c(Lcom/google/android/shared/util/BitFlags;)Z
    .locals 4

    .prologue
    .line 121
    iget-wide v0, p1, Lcom/google/android/shared/util/BitFlags;->cfn:J

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/shared/util/BitFlags;->e(JJ)Z

    move-result v0

    return v0
.end method

.method public final d(Lcom/google/android/shared/util/BitFlags;)Z
    .locals 4

    .prologue
    .line 140
    iget-wide v0, p1, Lcom/google/android/shared/util/BitFlags;->cfn:J

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v2, v3, v0, v1}, Lcom/google/android/shared/util/BitFlags;->e(JJ)Z

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 297
    const/4 v0, 0x0

    return v0
.end method

.method public final e(JJ)Z
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 165
    cmp-long v0, p1, v4

    if-ltz v0, :cond_0

    cmp-long v0, p3, v4

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 166
    iget-wide v4, p0, Lcom/google/android/shared/util/BitFlags;->cfn:J

    const-wide/16 v6, -0x1

    xor-long/2addr v6, p1

    and-long/2addr v4, v6

    or-long/2addr v4, p3

    .line 173
    iget-wide v6, p0, Lcom/google/android/shared/util/BitFlags;->cfn:J

    cmp-long v0, v6, v4

    if-eqz v0, :cond_1

    .line 178
    iput-wide v4, p0, Lcom/google/android/shared/util/BitFlags;->cfn:J

    .line 179
    invoke-virtual {p0}, Lcom/google/android/shared/util/BitFlags;->onChanged()V

    .line 182
    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 165
    goto :goto_0

    :cond_1
    move v1, v2

    .line 182
    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 288
    instance-of v1, p1, Lcom/google/android/shared/util/BitFlags;

    if-eqz v1, :cond_0

    .line 289
    check-cast p1, Lcom/google/android/shared/util/BitFlags;

    .line 290
    iget-wide v2, p0, Lcom/google/android/shared/util/BitFlags;->cfn:J

    iget-wide v4, p1, Lcom/google/android/shared/util/BitFlags;->cfn:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 292
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 278
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/android/shared/util/BitFlags;->cfn:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected onChanged()V
    .locals 0

    .prologue
    .line 191
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 273
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BitFlags"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/shared/util/BitFlags;->cfn:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aS(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 302
    iget-wide v0, p0, Lcom/google/android/shared/util/BitFlags;->cfn:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 303
    iget-object v0, p0, Lcom/google/android/shared/util/BitFlags;->cfl:Ljava/lang/Class;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 304
    return-void

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/google/android/shared/util/BitFlags;->cfl:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
