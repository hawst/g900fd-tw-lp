.class public Lcom/google/android/shared/ui/CoScrollContainer;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lekf;


# instance fields
.field private final caL:Landroid/view/animation/DecelerateInterpolator;

.field private final caM:Ljava/util/List;

.field private final caN:Z

.field private final caO:I

.field private caP:Z

.field public caQ:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field public caR:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "velvet"
    .end annotation
.end field

.field private caS:Lejd;

.field private caT:Z

.field private caU:Z

.field private caV:I

.field private caW:Landroid/view/View;

.field private caX:I

.field private caY:Landroid/animation/TimeInterpolator;

.field private caZ:I

.field public cba:Z

.field private cbb:Landroid/view/View$OnTouchListener;

.field private final iA:Landroid/graphics/Rect;

.field private mScrollHelper:Lekb;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/shared/ui/CoScrollContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 146
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/shared/ui/CoScrollContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 150
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/16 v3, 0x1e

    const/4 v2, 0x1

    .line 153
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40200000    # 2.5f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caL:Landroid/view/animation/DecelerateInterpolator;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caM:Ljava/util/List;

    .line 78
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->iA:Landroid/graphics/Rect;

    .line 118
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caV:I

    .line 154
    if-eqz p2, :cond_0

    .line 155
    sget-object v0, Lbwe;->aLO:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 156
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caN:Z

    .line 158
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caO:I

    .line 160
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 165
    :goto_0
    return-void

    .line 162
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caN:Z

    .line 163
    iput v3, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caO:I

    goto :goto_0
.end method

.method private a(Landroid/util/AttributeSet;)Lejd;
    .locals 2

    .prologue
    .line 1159
    new-instance v0, Lejd;

    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, Lejd;-><init>(Landroid/content/Context;Lcom/google/android/shared/ui/CoScrollContainer;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method private a(Landroid/view/View;ILandroid/animation/TimeInterpolator;I)V
    .locals 1

    .prologue
    .line 450
    iput-object p1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caW:Landroid/view/View;

    .line 451
    iput p2, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caX:I

    .line 452
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caY:Landroid/animation/TimeInterpolator;

    .line 453
    iput p4, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caZ:I

    .line 454
    return-void
.end method

.method private a(Lejd;II)V
    .locals 10

    .prologue
    const-wide/16 v2, 0x190

    const-wide/16 v4, 0x0

    .line 940
    iget-object v0, p1, Lejd;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    float-to-int v0, v0

    .line 943
    if-eq p2, v0, :cond_6

    .line 944
    if-gez v0, :cond_0

    .line 946
    iget-boolean v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caN:Z

    if-eqz v0, :cond_3

    .line 949
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getScrollY()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 950
    iget-object v1, p1, Lejd;->mView:Landroid/view/View;

    int-to-float v6, v0

    invoke-virtual {v1, v6}, Landroid/view/View;->setTranslationY(F)V

    .line 960
    :cond_0
    iget-wide v6, p1, Lejd;->cbo:J

    cmp-long v1, v6, v4

    if-eqz v1, :cond_1

    iget-wide v6, p1, Lejd;->cbp:J

    int-to-long v8, p2

    cmp-long v1, v6, v8

    if-eqz v1, :cond_2

    .line 961
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 962
    sub-int/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-le v0, v1, :cond_7

    .line 965
    iget-wide v0, p1, Lejd;->cbo:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_4

    if-eqz p3, :cond_4

    .line 968
    iget-wide v0, p1, Lejd;->cbo:J

    sub-long/2addr v0, v6

    .line 975
    :goto_0
    const-wide/16 v2, 0x32

    cmp-long v2, v0, v2

    if-lez v2, :cond_5

    .line 976
    iget-object v2, p1, Lejd;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    int-to-float v3, p2

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caL:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p1, Lejd;->cbq:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 980
    int-to-long v0, p2

    iput-wide v0, p1, Lejd;->cbp:J

    .line 994
    :cond_2
    :goto_1
    return-void

    .line 953
    :cond_3
    invoke-static {p1, p2}, Lcom/google/android/shared/ui/CoScrollContainer;->b(Lejd;I)V

    goto :goto_1

    .line 971
    :cond_4
    add-long v0, v6, v2

    iput-wide v0, p1, Lejd;->cbo:J

    move-wide v0, v2

    goto :goto_0

    .line 983
    :cond_5
    invoke-static {p1, p2}, Lcom/google/android/shared/ui/CoScrollContainer;->b(Lejd;I)V

    goto :goto_1

    .line 991
    :cond_6
    iget-object v0, p1, Lejd;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 992
    iput-wide v4, p1, Lejd;->cbo:J

    goto :goto_1

    :cond_7
    move-wide v0, v4

    goto :goto_0
.end method

.method private static aM(II)I
    .locals 1

    .prologue
    .line 1107
    if-gtz p0, :cond_0

    .line 1109
    const/4 v0, 0x0

    .line 1111
    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method private ati()V
    .locals 4

    .prologue
    .line 509
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getScrollY()I

    move-result v3

    .line 510
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getMaxScrollY()I

    move-result v0

    .line 516
    iget-object v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    invoke-virtual {v1}, Lekb;->atV()Z

    move-result v1

    if-nez v1, :cond_1

    .line 517
    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    .line 524
    :goto_0
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caM:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 525
    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_0

    .line 526
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caM:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekg;

    invoke-interface {v0, v3, v1}, Lekg;->aa(II)V

    .line 525
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    .line 528
    :cond_0
    return-void

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method private atl()Lejd;
    .locals 1

    .prologue
    .line 1133
    new-instance v0, Lejd;

    invoke-direct {v0, p0}, Lejd;-><init>(Lcom/google/android/shared/ui/CoScrollContainer;)V

    return-object v0
.end method

.method private static b(Lejd;I)V
    .locals 2

    .prologue
    .line 934
    iget-object v0, p0, Lejd;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 935
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lejd;->cbo:J

    .line 936
    iget-object v0, p0, Lejd;->mView:Landroid/view/View;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 937
    return-void
.end method

.method private e(ZI)V
    .locals 4

    .prologue
    .line 814
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getChildCount()I

    move-result v2

    .line 815
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 816
    invoke-virtual {p0, v1}, Lcom/google/android/shared/ui/CoScrollContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 817
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lejd;

    .line 818
    iget v3, v0, Lejd;->cbd:I

    if-eqz v3, :cond_1

    .line 819
    if-eqz p1, :cond_0

    .line 820
    invoke-virtual {p0, v0}, Lcom/google/android/shared/ui/CoScrollContainer;->a(Lejd;)V

    .line 823
    :cond_0
    invoke-virtual {p0, v0, p2}, Lcom/google/android/shared/ui/CoScrollContainer;->a(Lejd;I)V

    .line 815
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 826
    :cond_2
    return-void
.end method

.method private hF(I)Z
    .locals 1

    .prologue
    .line 244
    const/16 v0, 0x14

    if-ne p1, v0, :cond_0

    .line 245
    const/16 v0, 0x82

    invoke-direct {p0, v0}, Lcom/google/android/shared/ui/CoScrollContainer;->hH(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    invoke-virtual {v0, p1}, Lekb;->hW(I)Z

    move-result v0

    .line 253
    :goto_0
    return v0

    .line 248
    :cond_0
    const/16 v0, 0x13

    if-ne p1, v0, :cond_1

    .line 249
    const/16 v0, 0x21

    invoke-direct {p0, v0}, Lcom/google/android/shared/ui/CoScrollContainer;->hH(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    invoke-virtual {v0, p1}, Lekb;->hW(I)Z

    move-result v0

    goto :goto_0

    .line 253
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hH(I)Z
    .locals 10

    .prologue
    const/16 v9, 0x82

    const/16 v8, 0x21

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 389
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->findFocus()Landroid/view/View;

    move-result-object v2

    .line 390
    if-nez v2, :cond_1

    .line 445
    :cond_0
    :goto_0
    return v0

    .line 395
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getScrollY()I

    move-result v3

    .line 396
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getHeight()I

    move-result v4

    .line 401
    iget-object v5, p0, Lcom/google/android/shared/ui/CoScrollContainer;->iA:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v6

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v7

    invoke-virtual {v5, v1, v1, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 402
    iget-object v5, p0, Lcom/google/android/shared/ui/CoScrollContainer;->iA:Landroid/graphics/Rect;

    invoke-virtual {p0, v2, v5}, Lcom/google/android/shared/ui/CoScrollContainer;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 406
    if-ne p1, v9, :cond_2

    iget-object v5, p0, Lcom/google/android/shared/ui/CoScrollContainer;->iA:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    if-ge v5, v3, :cond_2

    move v0, v1

    .line 410
    goto :goto_0

    .line 411
    :cond_2
    if-ne p1, v8, :cond_3

    iget-object v5, p0, Lcom/google/android/shared/ui/CoScrollContainer;->iA:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    add-int v6, v3, v4

    if-le v5, v6, :cond_3

    move v0, v1

    .line 413
    goto :goto_0

    .line 416
    :cond_3
    invoke-virtual {v2, p1}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    move-result-object v2

    .line 417
    if-eqz v2, :cond_0

    .line 422
    iget-object v5, p0, Lcom/google/android/shared/ui/CoScrollContainer;->iA:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v6

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v7

    invoke-virtual {v5, v1, v1, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 424
    :try_start_0
    iget-object v5, p0, Lcom/google/android/shared/ui/CoScrollContainer;->iA:Landroid/graphics/Rect;

    invoke-virtual {p0, v2, v5}, Lcom/google/android/shared/ui/CoScrollContainer;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 428
    if-ne p1, v8, :cond_4

    iget-object v2, p0, Lcom/google/android/shared/ui/CoScrollContainer;->iA:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    if-le v2, v3, :cond_4

    move v0, v1

    .line 430
    goto :goto_0

    .line 431
    :cond_4
    if-ne p1, v9, :cond_0

    iget-object v2, p0, Lcom/google/android/shared/ui/CoScrollContainer;->iA:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v3, v4

    if-ge v2, v3, :cond_0

    move v0, v1

    .line 435
    goto :goto_0

    .line 442
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private hI(I)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 786
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getChildCount()I

    move-result v5

    move v4, v0

    move v1, v0

    move v2, v0

    .line 789
    :goto_0
    if-ge v4, v5, :cond_3

    .line 790
    invoke-virtual {p0, v4}, Lcom/google/android/shared/ui/CoScrollContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 791
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lejd;

    .line 792
    iget v3, v0, Lejd;->cbi:I

    if-lez v3, :cond_2

    .line 793
    iget v3, v0, Lejd;->cbi:I

    invoke-static {v3, p1}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 794
    iget v6, v0, Lejd;->cbn:I

    sub-int/2addr v6, v3

    iput v6, v0, Lejd;->cbn:I

    .line 795
    iget v6, v0, Lejd;->cbi:I

    sub-int/2addr v6, v3

    iput v6, v0, Lejd;->cbi:I

    .line 796
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 797
    iget-object v3, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caS:Lejd;

    if-eq v0, v3, :cond_0

    .line 798
    invoke-virtual {p0, v0, p1}, Lcom/google/android/shared/ui/CoScrollContainer;->a(Lejd;I)V

    .line 800
    :cond_0
    iget v3, v0, Lejd;->cbi:I

    if-lez v3, :cond_1

    .line 801
    const/4 v1, 0x1

    .line 805
    :cond_1
    iget v3, v0, Lejd;->cbi:I

    iget v0, v0, Lejd;->cbh:I

    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caM:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_1
    if-ltz v3, :cond_2

    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caM:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekg;

    invoke-interface {v0}, Lekg;->vU()V

    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 789
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v0

    goto :goto_0

    .line 809
    :cond_3
    iput-boolean v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->cba:Z

    .line 810
    sub-int v0, p1, v2

    return v0
.end method

.method private x(III)I
    .locals 0

    .prologue
    .line 1117
    if-gtz p2, :cond_0

    .line 1122
    :goto_0
    return p1

    .line 1119
    :cond_0
    if-ge p2, p3, :cond_1

    .line 1120
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getScrollY()I

    move-result p1

    goto :goto_0

    .line 1122
    :cond_1
    add-int/2addr p1, p3

    goto :goto_0
.end method


# virtual methods
.method public final a(Lejd;)V
    .locals 3

    .prologue
    .line 834
    const/4 v0, 0x0

    .line 835
    iget v1, p1, Lejd;->cbd:I

    packed-switch v1, :pswitch_data_0

    .line 851
    :goto_0
    :pswitch_0
    iget v1, p1, Lejd;->cbj:I

    sub-int/2addr v0, v1

    .line 852
    iget v1, p1, Lejd;->cbi:I

    add-int/2addr v0, v1

    .line 854
    iput v0, p1, Lejd;->cbn:I

    .line 855
    return-void

    .line 837
    :pswitch_1
    iget v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caQ:I

    goto :goto_0

    .line 840
    :pswitch_2
    iget-object v1, p1, Lejd;->cbf:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/shared/ui/CoScrollContainer;->aH(Landroid/view/View;)I

    move-result v1

    .line 841
    if-ltz v1, :cond_0

    .line 842
    iget-object v0, p1, Lejd;->cbf:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v0, v1

    iget v1, p1, Lejd;->cbg:I

    add-int/2addr v0, v1

    goto :goto_0

    .line 844
    :cond_0
    const-string v1, "Velvet.CoScrollContainer"

    const-string v2, "Scroll anchor is not a descendant"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 848
    :pswitch_3
    iget-object v0, p1, Lejd;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_0

    .line 835
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lejd;I)V
    .locals 5

    .prologue
    .line 866
    iget v0, p1, Lejd;->cbd:I

    packed-switch v0, :pswitch_data_0

    .line 910
    :cond_0
    :goto_0
    return-void

    .line 868
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getHeight()I

    move-result v0

    .line 872
    if-lez v0, :cond_0

    .line 873
    neg-int v0, v0

    invoke-static {p1, v0}, Lcom/google/android/shared/ui/CoScrollContainer;->b(Lejd;I)V

    .line 874
    iget-boolean v0, p1, Lejd;->cbl:Z

    if-eqz v0, :cond_0

    .line 875
    iget-object v0, p1, Lejd;->cbm:Lejf;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lejf;->hN(I)V

    goto :goto_0

    .line 881
    :pswitch_1
    iget v0, p1, Lejd;->cbn:I

    .line 882
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getScrollY()I

    move-result v1

    sub-int/2addr v1, v0

    .line 884
    iget-boolean v2, p1, Lejd;->cbl:Z

    if-eqz v2, :cond_1

    .line 885
    iget-object v2, p1, Lejd;->cbm:Lejf;

    .line 886
    invoke-virtual {p1}, Lejd;->ato()I

    move-result v3

    .line 888
    invoke-static {v1, v3}, Lcom/google/android/shared/ui/CoScrollContainer;->aM(II)I

    move-result v4

    .line 889
    invoke-interface {v2, v4}, Lejf;->hN(I)V

    .line 890
    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/shared/ui/CoScrollContainer;->x(III)I

    move-result v0

    .line 891
    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/shared/ui/CoScrollContainer;->a(Lejd;II)V

    goto :goto_0

    .line 893
    :cond_1
    iget v0, p1, Lejd;->cbn:I

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/shared/ui/CoScrollContainer;->a(Lejd;II)V

    goto :goto_0

    .line 897
    :pswitch_2
    iget v0, p1, Lejd;->cbn:I

    invoke-static {p1, v0}, Lcom/google/android/shared/ui/CoScrollContainer;->b(Lejd;I)V

    goto :goto_0

    .line 900
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->computeVerticalScrollRange()I

    move-result v0

    iget v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caR:I

    sub-int/2addr v0, v1

    invoke-static {p1, v0}, Lcom/google/android/shared/ui/CoScrollContainer;->b(Lejd;I)V

    goto :goto_0

    .line 903
    :pswitch_4
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getScrollY()I

    move-result v0

    iget v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caQ:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/shared/ui/CoScrollContainer;->a(Lejd;II)V

    goto :goto_0

    .line 907
    :pswitch_5
    iget-object v0, p1, Lejd;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/shared/ui/CoScrollContainer;->a(Lejd;II)V

    goto :goto_0

    .line 866
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Leka;)V
    .locals 1
    .param p1    # Leka;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1506
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    invoke-virtual {v0, p1}, Lekb;->a(Leka;)V

    .line 1507
    return-void
.end method

.method public final a(Lekg;)V
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caM:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 459
    return-void
.end method

.method public final aH(Landroid/view/View;)I
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 532
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->iA:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 534
    :try_start_0
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->iA:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/shared/ui/CoScrollContainer;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 546
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    :goto_0
    if-eq v0, p0, :cond_1

    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    move-object p1, v0

    move-object v0, v2

    goto :goto_0

    .line 540
    :catch_0
    move-exception v0

    move v0, v1

    .line 559
    :goto_1
    return v0

    .line 546
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Descendant isn\'t our descendant?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 547
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lejd;

    .line 548
    iget v2, v0, Lejd;->cbd:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_2

    move v0, v1

    .line 550
    goto :goto_1

    .line 552
    :cond_2
    iget-object v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->iA:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    .line 553
    iget-boolean v2, v0, Lejd;->cbl:Z

    if-eqz v2, :cond_3

    .line 556
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v2

    add-int/2addr v1, v2

    .line 558
    :cond_3
    iget v0, v0, Lejd;->cbn:I

    add-int/2addr v0, v1

    .line 559
    goto :goto_1
.end method

.method public final aI(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 1494
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1495
    :goto_0
    if-eqz v0, :cond_1

    .line 1496
    if-ne v0, p0, :cond_0

    .line 1497
    const/4 v0, 0x1

    .line 1501
    :goto_1
    return v0

    .line 1499
    :cond_0
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    .line 1501
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final aL(II)V
    .locals 2

    .prologue
    .line 726
    iget v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caQ:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caR:I

    if-eq p2, v0, :cond_1

    .line 727
    :cond_0
    iput p1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caQ:I

    .line 728
    iput p2, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caR:I

    .line 729
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/shared/ui/CoScrollContainer;->e(ZI)V

    .line 731
    :cond_1
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 289
    invoke-super {p0, p1, p2, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 290
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lejd;

    invoke-virtual {v0, p1}, Lejd;->setView(Landroid/view/View;)V

    .line 291
    return-void
.end method

.method public final ate()I
    .locals 1

    .prologue
    .line 468
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getHeight()I

    move-result v0

    return v0
.end method

.method public final atf()Z
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caW:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    invoke-virtual {v0}, Lekb;->atf()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final atg()V
    .locals 2

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caM:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 485
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 486
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caM:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekg;

    invoke-interface {v0}, Lekg;->vS()V

    .line 485
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 488
    :cond_0
    return-void
.end method

.method public final ath()V
    .locals 2

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caM:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 494
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 495
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caM:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekg;

    invoke-interface {v0}, Lekg;->vT()V

    .line 494
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 497
    :cond_0
    return-void
.end method

.method public final atj()V
    .locals 1

    .prologue
    .line 1064
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caT:Z

    .line 1065
    return-void
.end method

.method public final atk()Lejd;
    .locals 2

    .prologue
    .line 1128
    new-instance v0, Lejd;

    const/4 v1, 0x5

    invoke-direct {v0, p0, v1}, Lejd;-><init>(Lcom/google/android/shared/ui/CoScrollContainer;I)V

    return-object v0
.end method

.method public final atm()V
    .locals 2

    .prologue
    .line 1463
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caM:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1464
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 1465
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caM:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekg;

    invoke-interface {v0}, Lekg;->vV()V

    .line 1464
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1467
    :cond_0
    return-void
.end method

.method public final atn()V
    .locals 2

    .prologue
    .line 1472
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caM:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1473
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 1474
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caM:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekg;

    invoke-interface {v0}, Lekg;->vW()V

    .line 1473
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1476
    :cond_0
    return-void
.end method

.method public final b(Lekg;)V
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caM:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 464
    return-void
.end method

.method public final c(Lejd;I)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1002
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caS:Lejd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caS:Lejd;

    if-eq v0, p1, :cond_0

    .line 1003
    const-string v0, "Velvet.CoScrollContainer"

    const-string v1, "Multiple children causing a scroll?"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1004
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caS:Lejd;

    iput v3, v0, Lejd;->cbr:I

    .line 1006
    :cond_0
    iget-object v0, p1, Lejd;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v1

    .line 1007
    iput-object p1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caS:Lejd;

    .line 1008
    iput-boolean v4, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caT:Z

    .line 1010
    iget v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caV:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    .line 1011
    iget v0, p1, Lejd;->cbr:I

    sub-int/2addr p2, v0

    .line 1029
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->cba:Z

    if-eqz v0, :cond_3

    if-lez p2, :cond_3

    .line 1030
    invoke-direct {p0, p2}, Lcom/google/android/shared/ui/CoScrollContainer;->hI(I)I

    move-result v0

    .line 1036
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getScrollY()I

    move-result v2

    add-int/2addr v0, v2

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getMaxScrollY()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1038
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getScrollX()I

    move-result v2

    invoke-super {p0, v2, v0}, Landroid/widget/FrameLayout;->scrollTo(II)V

    .line 1041
    iget v2, p1, Lejd;->cbn:I

    .line 1042
    sub-int/2addr v0, v2

    .line 1043
    invoke-virtual {p1}, Lejd;->ato()I

    move-result v3

    .line 1047
    invoke-direct {p0, v2, v0, v3}, Lcom/google/android/shared/ui/CoScrollContainer;->x(III)I

    move-result v2

    .line 1048
    invoke-static {p1, v2}, Lcom/google/android/shared/ui/CoScrollContainer;->b(Lejd;I)V

    .line 1053
    invoke-static {v0, v3}, Lcom/google/android/shared/ui/CoScrollContainer;->aM(II)I

    move-result v0

    .line 1054
    sub-int/2addr v0, v1

    .line 1057
    sub-int v1, v0, p2

    iput v1, p1, Lejd;->cbr:I

    .line 1059
    return v0

    .line 1020
    :cond_2
    if-gez p2, :cond_1

    iget-object v0, p1, Lejd;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v0

    add-int/2addr v0, p2

    if-gt v0, v4, :cond_1

    .line 1021
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getScrollY()I

    move-result v0

    neg-int p2, v0

    goto :goto_0

    :cond_3
    move v0, p2

    goto :goto_1
.end method

.method public final c(Landroid/view/View$OnTouchListener;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->cbb:Landroid/view/View$OnTouchListener;

    .line 285
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1138
    instance-of v2, p1, Lejd;

    if-eqz v2, :cond_2

    .line 1139
    check-cast p1, Lejd;

    .line 1140
    iget-object v2, p1, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    if-nez v2, :cond_1

    .line 1143
    iput-object p0, p1, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    .line 1149
    :cond_0
    :goto_0
    return v0

    .line 1146
    :cond_1
    iget-object v2, p1, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    if-eq v2, p0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1149
    goto :goto_0
.end method

.method public computeScroll()V
    .locals 6

    .prologue
    .line 610
    iget-object v2, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    invoke-virtual {v2}, Lekb;->atf()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v2, Lekb;->ccs:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v1, v2, Lekb;->ccG:Z

    if-eqz v1, :cond_3

    invoke-virtual {v2}, Lekb;->atV()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v2, Lekb;->mScrollConsumer:Leka;

    iget-object v3, v2, Lekb;->ccs:Landroid/widget/OverScroller;

    invoke-virtual {v3}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v3

    iget v4, v2, Lekb;->ccA:I

    sub-int/2addr v3, v4

    invoke-interface {v1, v3}, Leka;->hP(I)I

    move-result v1

    iget-object v3, v2, Lekb;->ccs:Landroid/widget/OverScroller;

    invoke-virtual {v3}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v3

    iput v3, v2, Lekb;->ccA:I

    iget-object v3, v2, Lekb;->mView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getScrollY()I

    move-result v3

    add-int/2addr v1, v3

    iget-object v3, v2, Lekb;->mScrollConsumer:Leka;

    invoke-interface {v3}, Leka;->att()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v0, v2, Lekb;->ccs:Landroid/widget/OverScroller;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/OverScroller;->forceFinished(Z)V

    const/4 v0, 0x0

    :cond_0
    move v5, v1

    move v1, v0

    move v0, v5

    :goto_0
    iget-object v3, v2, Lekb;->mView:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setScrollY(I)V

    iget-object v0, v2, Lekb;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidateOnAnimation()V

    :goto_1
    if-nez v1, :cond_2

    iget-boolean v0, v2, Lekb;->ccG:Z

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lekb;->atV()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, v2, Lekb;->mView:Landroid/view/View;

    iget-object v1, v2, Lekb;->ccI:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_1
    const/4 v0, -0x1

    iput v0, v2, Lekb;->ccE:I

    iget-object v0, v2, Lekb;->mScrollViewControl:Lekf;

    invoke-interface {v0}, Lekf;->atg()V

    .line 611
    :cond_2
    return-void

    .line 610
    :cond_3
    iget-object v1, v2, Lekb;->ccs:Landroid/widget/OverScroller;

    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v1

    move v5, v1

    move v1, v0

    move v0, v5

    goto :goto_0

    :cond_4
    move v1, v0

    goto :goto_1
.end method

.method public computeVerticalScrollRange()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 696
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getMeasuredHeight()I

    move-result v0

    move v1, v2

    move v3, v0

    .line 697
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 698
    invoke-virtual {p0, v1}, Lcom/google/android/shared/ui/CoScrollContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 699
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v5, 0x8

    if-eq v0, v5, :cond_0

    .line 700
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lejd;

    .line 703
    iget-boolean v5, v0, Lejd;->cbl:Z

    if-eqz v5, :cond_1

    .line 704
    iget v4, v0, Lejd;->cbd:I

    const/4 v5, 0x5

    if-eq v4, v5, :cond_0

    .line 705
    iget-object v4, v0, Lejd;->cbm:Lejf;

    invoke-interface {v4}, Lejf;->ats()I

    move-result v4

    iget v5, v0, Lejd;->cbk:I

    sub-int/2addr v4, v5

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 707
    iget v0, v0, Lejd;->cbn:I

    add-int/2addr v0, v4

    iget v4, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caR:I

    add-int/2addr v0, v4

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 697
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 717
    :cond_1
    iget v0, v0, Lejd;->cbi:I

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v0, v4

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto :goto_1

    .line 720
    :cond_2
    return v3
.end method

.method public final d(Lejd;I)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1070
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getScrollY()I

    move-result v1

    .line 1072
    iget v2, p1, Lejd;->cbd:I

    const/4 v3, 0x5

    if-eq v2, v3, :cond_0

    iget v2, p1, Lejd;->cbn:I

    if-ge v1, v2, :cond_1

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caP:Z

    if-nez v1, :cond_1

    .line 1102
    :goto_0
    return v0

    .line 1085
    :cond_1
    iget v1, p1, Lejd;->cbn:I

    add-int/2addr v1, p2

    .line 1089
    if-nez p2, :cond_2

    .line 1090
    :goto_1
    invoke-static {p1, v1}, Lcom/google/android/shared/ui/CoScrollContainer;->b(Lejd;I)V

    .line 1100
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caP:Z

    .line 1101
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getScrollX()I

    move-result v1

    invoke-super {p0, v1, v0}, Landroid/widget/FrameLayout;->scrollTo(II)V

    move v0, p2

    .line 1102
    goto :goto_0

    :cond_2
    move v0, v1

    .line 1089
    goto :goto_1
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 7

    .prologue
    const/16 v6, 0x14

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 181
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getFocusedChild()Landroid/view/View;

    move-result-object v4

    .line 182
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lejd;

    iget-boolean v0, v0, Lejd;->cbl:Z

    if-eqz v0, :cond_2

    move v0, v1

    .line 183
    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    const/16 v5, 0x3d

    if-eq v3, v5, :cond_0

    const/16 v5, 0x13

    if-eq v3, v5, :cond_0

    if-eq v3, v6, :cond_0

    const/16 v5, 0x15

    if-eq v3, v5, :cond_0

    const/16 v5, 0x16

    if-ne v3, v5, :cond_3

    :cond_0
    move v3, v1

    .line 185
    :goto_1
    if-eqz v0, :cond_4

    if-eqz v3, :cond_4

    move v3, v1

    :goto_2
    iput-boolean v3, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caP:Z

    .line 189
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 191
    if-eqz v0, :cond_1

    .line 192
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lejd;

    iget-boolean v2, v0, Lejd;->cbl:Z

    if-eqz v2, :cond_1

    iget-object v2, v0, Lejd;->cbm:Lejf;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    if-ne v2, v6, :cond_1

    invoke-virtual {v4}, Landroid/view/View;->getScrollY()I

    move-result v2

    invoke-virtual {v0}, Lejd;->ato()I

    move-result v0

    if-lt v2, v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getMaxScrollY()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/shared/ui/CoScrollContainer;->scrollTo(II)V

    .line 204
    :cond_1
    :goto_3
    return v1

    :cond_2
    move v0, v2

    .line 182
    goto :goto_0

    :cond_3
    move v3, v2

    .line 183
    goto :goto_1

    :cond_4
    move v3, v2

    .line 185
    goto :goto_2

    .line 197
    :cond_5
    if-nez v0, :cond_6

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_6

    .line 198
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/shared/ui/CoScrollContainer;->hF(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_6
    move v1, v2

    .line 204
    goto :goto_3
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 736
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 764
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 738
    :pswitch_0
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caV:I

    .line 739
    iput-object v4, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caS:Lejd;

    goto :goto_0

    .line 742
    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caU:Z

    if-eqz v0, :cond_0

    .line 743
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    iget-boolean v1, v0, Lekb;->oN:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lekb;->ccF:Z

    invoke-virtual {v0, p1}, Lekb;->o(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 747
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caU:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caT:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caV:I

    if-eq v0, v5, :cond_2

    .line 749
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    iget v2, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caV:I

    iget-boolean v3, v0, Lekb;->oN:Z

    if-eqz v3, :cond_4

    iget-boolean v3, v0, Lekb;->ccF:Z

    if-eqz v3, :cond_4

    iput-boolean v1, v0, Lekb;->ccF:Z

    invoke-virtual {v0, v2}, Lekb;->hX(I)V

    iget-object v2, v0, Lekb;->fW:Landroid/view/VelocityTracker;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lekb;->fW:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->recycle()V

    iput-object v4, v0, Lekb;->fW:Landroid/view/VelocityTracker;

    :cond_1
    invoke-virtual {v0}, Lekb;->atf()Z

    move-result v0

    :goto_1
    if-nez v0, :cond_2

    .line 750
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->ath()V

    .line 755
    :cond_2
    :pswitch_3
    iput-boolean v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caU:Z

    .line 756
    iput-boolean v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caT:Z

    .line 757
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caS:Lejd;

    if-eqz v0, :cond_3

    .line 758
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caS:Lejd;

    iput v1, v0, Lejd;->cbr:I

    .line 759
    iput-object v4, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caS:Lejd;

    .line 761
    :cond_3
    iput v5, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caV:I

    goto :goto_0

    :cond_4
    move v0, v1

    .line 749
    goto :goto_1

    .line 736
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public final eT(Z)V
    .locals 1

    .prologue
    .line 1489
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    invoke-virtual {v0, p1}, Lekb;->eY(Z)V

    .line 1490
    return-void
.end method

.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->atl()Lejd;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->atl()Lejd;

    move-result-object v0

    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/shared/ui/CoScrollContainer;->a(Landroid/util/AttributeSet;)Lejd;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lejd;

    invoke-direct {v0, p0, p1}, Lejd;-><init>(Lcom/google/android/shared/ui/CoScrollContainer;Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/shared/ui/CoScrollContainer;->a(Landroid/util/AttributeSet;)Lejd;

    move-result-object v0

    return-object v0
.end method

.method public final getMaxScrollY()I
    .locals 2

    .prologue
    .line 478
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->computeVerticalScrollRange()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public final hG(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 305
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->isLayoutRequested()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p0, v0, v3, v2}, Lcom/google/android/shared/ui/CoScrollContainer;->a(Landroid/view/View;ILandroid/animation/TimeInterpolator;I)V

    .line 306
    :goto_0
    return-void

    .line 305
    :cond_0
    iget-object v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    invoke-virtual {v1, v0, v3, v2}, Lekb;->a(ILandroid/animation/TimeInterpolator;I)Z

    goto :goto_0
.end method

.method public final hJ(I)V
    .locals 2

    .prologue
    .line 1481
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caM:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1482
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 1483
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caM:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekg;

    invoke-interface {v0, p1}, Lekg;->dr(I)V

    .line 1482
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1485
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 615
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 616
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    iget-boolean v1, v0, Lekb;->ccB:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lekb;->ccC:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    iget-object v2, v0, Lekb;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getScrollY()I

    move-result v2

    iget-object v3, v0, Lekb;->mView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    iget-object v4, v0, Lekb;->mView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    iget-boolean v5, v0, Lekb;->ccz:Z

    if-eqz v5, :cond_1

    const/high16 v5, 0x43340000    # 180.0f

    invoke-virtual {p1, v5}, Landroid/graphics/Canvas;->rotate(F)V

    neg-int v5, v3

    int-to-float v5, v5

    neg-int v2, v2

    sub-int/2addr v2, v4

    int-to-float v2, v2

    invoke-virtual {p1, v5, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, v0, Lekb;->ccC:Landroid/widget/EdgeEffect;

    iget v4, v0, Lekb;->caO:I

    invoke-virtual {v2, v3, v4}, Landroid/widget/EdgeEffect;->setSize(II)V

    :goto_0
    iget-object v2, v0, Lekb;->ccC:Landroid/widget/EdgeEffect;

    invoke-virtual {v2, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v0, v0, Lekb;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidateOnAnimation()V

    :goto_1
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 617
    :cond_0
    return-void

    .line 616
    :cond_1
    const/4 v4, 0x0

    int-to-float v2, v2

    invoke-virtual {p1, v4, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, v0, Lekb;->ccC:Landroid/widget/EdgeEffect;

    iget v4, v0, Lekb;->caO:I

    invoke-virtual {v2, v3, v4}, Landroid/widget/EdgeEffect;->setSize(II)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, v0, Lekb;->ccB:Z

    goto :goto_1
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 169
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 170
    new-instance v0, Lekb;

    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caO:I

    invoke-direct {v0, v1, p0, p0, v2}, Lekb;-><init>(Landroid/content/Context;Lekf;Landroid/view/View;I)V

    iput-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    .line 172
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/shared/ui/CoScrollContainer;->setWillNotDraw(Z)V

    .line 173
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 601
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    invoke-virtual {v0, p1}, Lekb;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602
    const/4 v0, 0x1

    .line 604
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 1435
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1436
    const-class v0, Lcom/google/android/shared/ui/CoScrollContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 1437
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 575
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    invoke-virtual {v0, p1}, Lekb;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 577
    const/4 v0, 0x1

    .line 581
    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->cbb:Landroid/view/View$OnTouchListener;

    if-eqz v1, :cond_0

    .line 582
    iget-object v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->cbb:Landroid/view/View$OnTouchListener;

    invoke-interface {v1, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 584
    :cond_0
    return v0

    .line 579
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 233
    invoke-direct {p0, p1}, Lcom/google/android/shared/ui/CoScrollContainer;->hF(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    const/4 v0, 0x1

    .line 237
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v4, 0x0

    .line 655
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 657
    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/shared/ui/CoScrollContainer;->e(ZI)V

    .line 660
    iget-object v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caW:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 662
    iget-object v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caW:Landroid/view/View;

    if-ne v1, p0, :cond_3

    .line 663
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caY:Landroid/animation/TimeInterpolator;

    if-nez v0, :cond_2

    .line 664
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    iget v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caX:I

    invoke-virtual {v0, v1, v4, v2}, Lekb;->a(ILandroid/animation/TimeInterpolator;I)Z

    move-result v0

    .line 681
    :cond_0
    :goto_0
    iput-object v4, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caW:Landroid/view/View;

    .line 682
    if-nez v0, :cond_1

    .line 683
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->atg()V

    .line 688
    :cond_1
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    invoke-virtual {v0}, Lekb;->atU()V

    .line 691
    invoke-direct {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->ati()V

    .line 692
    return-void

    .line 666
    :cond_2
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    iget v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caX:I

    iget-object v2, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caY:Landroid/animation/TimeInterpolator;

    iget v3, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caZ:I

    invoke-virtual {v0, v1, v2, v3}, Lekb;->a(ILandroid/animation/TimeInterpolator;I)Z

    move-result v0

    goto :goto_0

    .line 670
    :cond_3
    iget-object v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caW:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/shared/ui/CoScrollContainer;->aH(Landroid/view/View;)I

    move-result v1

    .line 671
    if-ltz v1, :cond_0

    .line 672
    iget v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caX:I

    sub-int v0, v1, v0

    .line 673
    iget-object v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caY:Landroid/animation/TimeInterpolator;

    if-nez v1, :cond_4

    .line 674
    iget-object v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    invoke-virtual {v1, v0, v4, v2}, Lekb;->a(ILandroid/animation/TimeInterpolator;I)Z

    move-result v0

    goto :goto_0

    .line 676
    :cond_4
    iget-object v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    iget-object v2, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caY:Landroid/animation/TimeInterpolator;

    iget v3, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caZ:I

    invoke-virtual {v1, v0, v2, v3}, Lekb;->a(ILandroid/animation/TimeInterpolator;I)Z

    move-result v0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 621
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-ne v0, v7, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 622
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-ne v0, v7, :cond_2

    :goto_1
    invoke-static {v1}, Lifv;->gY(Z)V

    .line 623
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 624
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 625
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 626
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 627
    invoke-virtual {p0, v2}, Lcom/google/android/shared/ui/CoScrollContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 628
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lejd;

    .line 629
    iget-boolean v6, v0, Lejd;->cbl:Z

    if-eqz v6, :cond_3

    .line 632
    invoke-virtual {v5, p1, p2}, Landroid/view/View;->measure(II)V

    .line 626
    :cond_0
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    move v0, v2

    .line 621
    goto :goto_0

    :cond_2
    move v1, v2

    .line 622
    goto :goto_1

    .line 637
    :cond_3
    invoke-virtual {v5, p1, v4}, Landroid/view/View;->measure(II)V

    .line 638
    add-int/lit8 v6, v3, 0x0

    .line 639
    iget-boolean v0, v0, Lejd;->cbe:Z

    if-eqz v0, :cond_0

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    if-ge v0, v6, :cond_0

    .line 641
    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v5, p1, v0}, Landroid/view/View;->measure(II)V

    goto :goto_3

    .line 646
    :cond_4
    invoke-virtual {p0, v1, v3}, Lcom/google/android/shared/ui/CoScrollContainer;->setMeasuredDimension(II)V

    .line 647
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 0

    .prologue
    .line 567
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onScrollChanged(IIII)V

    .line 568
    invoke-direct {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->ati()V

    .line 569
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 590
    iget-object v0, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    invoke-virtual {v0, p1}, Lekb;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592
    const/4 v0, 0x1

    .line 595
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public removeView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 295
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lejd;

    .line 296
    iget-object v1, v0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    if-ne v1, p0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lifv;->gY(Z)V

    .line 297
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 298
    const/4 v1, 0x0

    iput-object v1, v0, Lejd;->cbc:Lcom/google/android/shared/ui/CoScrollContainer;

    .line 299
    return-void

    .line 296
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 0

    .prologue
    .line 770
    iput-boolean p1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->caU:Z

    .line 771
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->requestDisallowInterceptTouchEvent(Z)V

    .line 772
    return-void
.end method

.method public scrollTo(II)V
    .locals 3

    .prologue
    .line 776
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->getScrollY()I

    move-result v0

    .line 777
    sub-int v1, p2, v0

    .line 778
    if-lez v1, :cond_0

    iget-boolean v2, p0, Lcom/google/android/shared/ui/CoScrollContainer;->cba:Z

    if-eqz v2, :cond_0

    .line 779
    invoke-direct {p0, v1}, Lcom/google/android/shared/ui/CoScrollContainer;->hI(I)I

    move-result v2

    add-int p2, v0, v2

    .line 781
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->scrollTo(II)V

    .line 782
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/shared/ui/CoScrollContainer;->e(ZI)V

    .line 783
    return-void
.end method

.method public final t(Landroid/view/View;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 359
    invoke-virtual {p0}, Lcom/google/android/shared/ui/CoScrollContainer;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 362
    invoke-direct {p0, p1, p2, v3, v2}, Lcom/google/android/shared/ui/CoScrollContainer;->a(Landroid/view/View;ILandroid/animation/TimeInterpolator;I)V

    .line 370
    :cond_0
    :goto_0
    return-void

    .line 364
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/shared/ui/CoScrollContainer;->aH(Landroid/view/View;)I

    move-result v0

    .line 365
    if-ltz v0, :cond_0

    .line 367
    iget-object v1, p0, Lcom/google/android/shared/ui/CoScrollContainer;->mScrollHelper:Lekb;

    sub-int/2addr v0, p2

    invoke-virtual {v1, v0, v3, v2}, Lekb;->a(ILandroid/animation/TimeInterpolator;I)Z

    goto :goto_0
.end method
