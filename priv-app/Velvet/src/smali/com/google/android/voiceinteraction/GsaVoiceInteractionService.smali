.class public Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;
.super Landroid/service/voice/VoiceInteractionService;
.source "PG"


# instance fields
.field private final KU:Landroid/content/BroadcastReceiver;

.field private anD:Ljava/lang/String;

.field private final dK:Ljava/lang/Object;

.field private dgF:Landroid/service/voice/AlwaysOnHotwordDetector;

.field private volatile dgG:I

.field private dgH:Lhgv;

.field private dgI:Z

.field private dgJ:Lbze;

.field private dgK:Z

.field private dgL:Z

.field private final dgM:Landroid/service/voice/AlwaysOnHotwordDetector$Callback;

.field private dgN:Lhgw;

.field private final dgO:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Landroid/service/voice/VoiceInteractionService;-><init>()V

    .line 87
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dK:Ljava/lang/Object;

    .line 101
    new-instance v0, Lhgr;

    invoke-direct {v0, p0}, Lhgr;-><init>(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)V

    iput-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgM:Landroid/service/voice/AlwaysOnHotwordDetector$Callback;

    .line 178
    new-instance v0, Lhgs;

    invoke-direct {v0, p0}, Lhgs;-><init>(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)V

    iput-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgN:Lhgw;

    .line 286
    new-instance v0, Lhgt;

    invoke-direct {v0, p0}, Lhgt;-><init>(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)V

    iput-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgO:Landroid/content/ServiceConnection;

    .line 307
    new-instance v0, Lhgu;

    invoke-direct {v0, p0}, Lhgu;-><init>(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)V

    iput-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->KU:Landroid/content/BroadcastReceiver;

    .line 353
    return-void
.end method

.method private static a(Landroid/service/voice/AlwaysOnHotwordDetector$EventPayload;)I
    .locals 6

    .prologue
    const/4 v1, -0x1

    const/4 v5, 0x0

    .line 539
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "getCaptureSession"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 540
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 541
    if-eqz v0, :cond_0

    .line 542
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 547
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 544
    goto :goto_0

    .line 545
    :catch_0
    move-exception v0

    .line 546
    const-string v2, "GsaVoiceInteractionSrv"

    const-string v3, "Failed to get captureSession"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v0, v1

    .line 547
    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;I)I
    .locals 0

    .prologue
    .line 69
    iput p1, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgG:I

    return p1
.end method

.method public static synthetic a(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;Landroid/service/voice/AlwaysOnHotwordDetector$EventPayload;)I
    .locals 1

    .prologue
    .line 69
    invoke-static {p1}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->a(Landroid/service/voice/AlwaysOnHotwordDetector$EventPayload;)I

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;Lbze;)Lbze;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgJ:Lbze;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;J)V
    .locals 3

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgH:Lhgv;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1, p2}, Lhgv;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method public static synthetic a(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgH:Lhgv;

    invoke-virtual {v0}, Lhgv;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgH:Lhgv;

    invoke-virtual {v1, v0}, Lhgv;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public static synthetic a(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Z
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgF:Landroid/service/voice/AlwaysOnHotwordDetector;

    invoke-virtual {v0}, Landroid/service/voice/AlwaysOnHotwordDetector;->getSupportedRecognitionModes()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;Z)Z
    .locals 0

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgL:Z

    return p1
.end method

.method private aOG()Z
    .locals 2

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgF:Landroid/service/voice/AlwaysOnHotwordDetector;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/service/voice/AlwaysOnHotwordDetector;->startRecognition(I)Z

    move-result v0

    .line 496
    return v0
.end method

.method public static synthetic b(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgG:I

    return v0
.end method

.method public static synthetic b(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;Landroid/service/voice/AlwaysOnHotwordDetector$EventPayload;)I
    .locals 2

    .prologue
    .line 69
    invoke-virtual {p1}, Landroid/service/voice/AlwaysOnHotwordDetector$EventPayload;->getCaptureAudioFormat()Landroid/media/AudioFormat;

    move-result-object v1

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/media/AudioFormat;->getSampleRate()I

    move-result v0

    :cond_0
    if-nez v0, :cond_1

    const/16 v0, 0x3e80

    :cond_1
    return v0
.end method

.method public static synthetic b(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 69
    iget-object v1, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->anD:Ljava/lang/String;

    const-string v0, "Ok Google"

    invoke-static {p1}, Ljava/util/Locale;->forLanguageTag(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgM:Landroid/service/voice/AlwaysOnHotwordDetector$Callback;

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->createAlwaysOnHotwordDetector(Ljava/lang/String;Ljava/util/Locale;Landroid/service/voice/AlwaysOnHotwordDetector$Callback;)Landroid/service/voice/AlwaysOnHotwordDetector;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgF:Landroid/service/voice/AlwaysOnHotwordDetector;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static synthetic b(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;Z)Z
    .locals 0

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgI:Z

    return p1
.end method

.method public static bJ(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 509
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.voiceinteraction.GsaVoiceInteractionService"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 69
    iget v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgG:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgI:Z

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgJ:Lbze;

    iget v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgG:I

    const/4 v3, -0x2

    if-eq v0, v3, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v2, v0}, Lbze;->bV(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_2
    iget-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgO:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->unbindService(Landroid/content/ServiceConnection;)V

    iput-boolean v1, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgI:Z

    :cond_1
    return-void

    :pswitch_1
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgI:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgJ:Lbze;

    invoke-interface {v0}, Lbze;->Cd()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgL:Z

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgL:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->aOG()Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "GsaVoiceInteractionSrv"

    const-string v3, "RemoteException %s"

    invoke-static {v2, v3, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v2, "GsaVoiceInteractionSrv"

    const-string v3, "RemoteException %s"

    invoke-static {v2, v3, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static synthetic d(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgK:Z

    return v0
.end method

.method public static synthetic e(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->anD:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dK:Ljava/lang/Object;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Z
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->aOG()Z

    move-result v0

    return v0
.end method

.method public static synthetic h(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Landroid/service/voice/AlwaysOnHotwordDetector;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgF:Landroid/service/voice/AlwaysOnHotwordDetector;

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Z
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgF:Landroid/service/voice/AlwaysOnHotwordDetector;

    invoke-virtual {v0}, Landroid/service/voice/AlwaysOnHotwordDetector;->stopRecognition()Z

    move-result v0

    return v0
.end method

.method public static synthetic j(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Lbze;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgJ:Lbze;

    return-object v0
.end method

.method public static synthetic k(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;)Z
    .locals 2

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgK:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgL:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgG:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 561
    invoke-super {p0, p1, p2, p3}, Landroid/service/voice/VoiceInteractionService;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 562
    invoke-static {p0}, Lifo;->bc(Ljava/lang/Object;)Lifp;

    move-result-object v0

    .line 563
    const-string v1, "Locale"

    iget-object v2, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->anD:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lifp;->e(Ljava/lang/String;Ljava/lang/Object;)Lifp;

    .line 564
    const-string v1, "Availability"

    iget v2, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgG:I

    invoke-virtual {v0, v1, v2}, Lifp;->O(Ljava/lang/String;I)Lifp;

    .line 565
    const-string v1, "Connected To HotwordService"

    iget-boolean v2, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgI:Z

    invoke-virtual {v0, v1, v2}, Lifp;->F(Ljava/lang/String;Z)Lifp;

    .line 566
    invoke-virtual {v0}, Lifp;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 567
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 387
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.voiceinteraction.GsaVoiceInteractionService"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388
    iget-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgN:Lhgw;

    invoke-interface {v0}, Lhgw;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 390
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/service/voice/VoiceInteractionService;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 420
    invoke-super {p0}, Landroid/service/voice/VoiceInteractionService;->onCreate()V

    .line 421
    new-instance v0, Lhgv;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lhgv;-><init>(Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;B)V

    iput-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgH:Lhgv;

    .line 422
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 432
    invoke-super {p0}, Landroid/service/voice/VoiceInteractionService;->onDestroy()V

    .line 434
    return-void
.end method

.method public onReady()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 395
    invoke-super {p0}, Landroid/service/voice/VoiceInteractionService;->onReady()V

    .line 397
    iput-boolean v4, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgK:Z

    .line 398
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.google.android.googlequicksearchbox.CHANGE_VOICESEARCH_LANGUAGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.google.android.googlequicksearchbox.interactor.RESTART_RECOGNITION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->KU:Landroid/content/BroadcastReceiver;

    const-string v2, "android.permission.MANAGE_VOICE_KEYPHRASES"

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 399
    invoke-static {}, Lcom/google/android/hotword/service/HotwordService;->Cb()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgO:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v1, v4}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 400
    return-void
.end method

.method public onShutdown()V
    .locals 1

    .prologue
    .line 554
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->dgK:Z

    .line 555
    iget-object v0, p0, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->KU:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 556
    invoke-virtual {p0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->stopSelf()V

    .line 557
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    .prologue
    .line 438
    invoke-super {p0, p1, p2, p3}, Landroid/service/voice/VoiceInteractionService;->onStartCommand(Landroid/content/Intent;II)I

    .line 440
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.voiceinteraction.START_VOICE_INTERACTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/voiceinteraction/GsaVoiceInteractionService;->startSession(Landroid/os/Bundle;)V

    .line 444
    :cond_0
    const/4 v0, 0x2

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 427
    invoke-super {p0, p1}, Landroid/service/voice/VoiceInteractionService;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
