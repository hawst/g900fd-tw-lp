.class public Lcom/android/datetimepicker/time/RadialPickerLayout;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final fF:I

.field private mHandler:Landroid/os/Handler;

.field private mJ:Lnd;

.field private oA:Z

.field private oB:I

.field private oC:I

.field private oD:Z

.field private oE:I

.field private oF:Loa;

.field private oG:Lnz;

.field private oH:Log;

.field private oI:Log;

.field private oJ:Loe;

.field private oK:Loe;

.field private oL:Landroid/view/View;

.field private oM:[I

.field private oN:Z

.field private oO:I

.field private oP:Z

.field private oQ:Z

.field private oR:I

.field private oS:F

.field private oT:F

.field private oU:Landroid/view/accessibility/AccessibilityManager;

.field private oV:Landroid/animation/AnimatorSet;

.field private os:Z

.field private final ox:I

.field private oy:I

.field private oz:Lod;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 101
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 85
    iput v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oO:I

    .line 94
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->mHandler:Landroid/os/Handler;

    .line 103
    invoke-virtual {p0, p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 104
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->ox:I

    .line 106
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->fF:I

    .line 107
    iput-boolean v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oP:Z

    .line 109
    new-instance v0, Loa;

    invoke-direct {v0, p1}, Loa;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oF:Loa;

    .line 110
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oF:Loa;

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->addView(Landroid/view/View;)V

    .line 112
    new-instance v0, Lnz;

    invoke-direct {v0, p1}, Lnz;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oG:Lnz;

    .line 113
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oG:Lnz;

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->addView(Landroid/view/View;)V

    .line 115
    new-instance v0, Log;

    invoke-direct {v0, p1}, Log;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oH:Log;

    .line 116
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oH:Log;

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->addView(Landroid/view/View;)V

    .line 117
    new-instance v0, Log;

    invoke-direct {v0, p1}, Log;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oI:Log;

    .line 118
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oI:Log;

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->addView(Landroid/view/View;)V

    .line 120
    new-instance v0, Loe;

    invoke-direct {v0, p1}, Loe;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oJ:Loe;

    .line 121
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oJ:Loe;

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->addView(Landroid/view/View;)V

    .line 122
    new-instance v0, Loe;

    invoke-direct {v0, p1}, Loe;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oK:Loe;

    .line 123
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oK:Loe;

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->addView(Landroid/view/View;)V

    .line 126
    invoke-direct {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->cB()V

    .line 128
    iput v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oy:I

    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oN:Z

    .line 131
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oL:Landroid/view/View;

    .line 132
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oL:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 134
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oL:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 135
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oL:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oL:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->addView(Landroid/view/View;)V

    .line 138
    const-string v0, "accessibility"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oU:Landroid/view/accessibility/AccessibilityManager;

    .line 140
    iput-boolean v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oA:Z

    .line 141
    return-void
.end method

.method private a(FFZ[Ljava/lang/Boolean;)I
    .locals 2

    .prologue
    .line 495
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->cC()I

    move-result v0

    .line 496
    if-nez v0, :cond_0

    .line 497
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oJ:Loe;

    invoke-virtual {v0, p1, p2, p3, p4}, Loe;->a(FFZ[Ljava/lang/Boolean;)I

    move-result v0

    .line 503
    :goto_0
    return v0

    .line 499
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 500
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oK:Loe;

    invoke-virtual {v0, p1, p2, p3, p4}, Loe;->a(FFZ[Ljava/lang/Boolean;)I

    move-result v0

    goto :goto_0

    .line 503
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private a(IZZZ)I
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v0, -0x1

    const/16 v2, 0x168

    const/4 v3, 0x0

    .line 435
    if-ne p1, v0, :cond_0

    .line 478
    :goto_0
    return v0

    .line 438
    :cond_0
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->cC()I

    move-result v6

    .line 441
    if-nez p3, :cond_1

    if-ne v6, v5, :cond_1

    move v1, v5

    .line 442
    :goto_1
    if-eqz v1, :cond_3

    .line 443
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oM:[I

    if-nez v1, :cond_2

    :goto_2
    move v4, v0

    .line 449
    :goto_3
    if-nez v6, :cond_4

    .line 450
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oJ:Loe;

    .line 451
    const/16 v1, 0x1e

    .line 456
    :goto_4
    invoke-virtual {v0, v4, p2, p4}, Loe;->b(IZZ)V

    .line 457
    invoke-virtual {v0}, Loe;->invalidate()V

    .line 460
    if-nez v6, :cond_7

    .line 461
    iget-boolean v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->os:Z

    if-eqz v0, :cond_6

    .line 462
    if-nez v4, :cond_5

    if-eqz p2, :cond_5

    move v0, v2

    .line 474
    :goto_5
    div-int v1, v0, v1

    .line 475
    if-nez v6, :cond_8

    iget-boolean v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->os:Z

    if-eqz v2, :cond_8

    if-nez p2, :cond_8

    if-eqz v0, :cond_8

    .line 476
    add-int/lit8 v0, v1, 0xc

    goto :goto_0

    :cond_1
    move v1, v3

    .line 441
    goto :goto_1

    .line 443
    :cond_2
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oM:[I

    aget v0, v0, p1

    goto :goto_2

    .line 445
    :cond_3
    invoke-static {p1, v3}, Lcom/android/datetimepicker/time/RadialPickerLayout;->s(II)I

    move-result v4

    goto :goto_3

    .line 453
    :cond_4
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oK:Loe;

    .line 454
    const/4 v1, 0x6

    goto :goto_4

    .line 464
    :cond_5
    if-ne v4, v2, :cond_9

    if-nez p2, :cond_9

    move v0, v3

    .line 465
    goto :goto_5

    .line 467
    :cond_6
    if-nez v4, :cond_9

    move v0, v2

    .line 468
    goto :goto_5

    .line 470
    :cond_7
    if-ne v4, v2, :cond_9

    if-ne v6, v5, :cond_9

    move v0, v3

    .line 471
    goto :goto_5

    :cond_8
    move v0, v1

    goto :goto_0

    :cond_9
    move v0, v4

    goto :goto_5
.end method

.method public static synthetic a(Lcom/android/datetimepicker/time/RadialPickerLayout;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oO:I

    return v0
.end method

.method public static synthetic a(Lcom/android/datetimepicker/time/RadialPickerLayout;I)I
    .locals 0

    .prologue
    .line 48
    iput p1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oy:I

    return p1
.end method

.method public static synthetic a(Lcom/android/datetimepicker/time/RadialPickerLayout;IZZZ)I
    .locals 2

    .prologue
    .line 48
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(IZZZ)I

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/android/datetimepicker/time/RadialPickerLayout;Z)Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oP:Z

    return v0
.end method

.method private ak(I)Z
    .locals 1

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->os:Z

    if-eqz v0, :cond_0

    const/16 v0, 0xc

    if-gt p1, v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic b(Lcom/android/datetimepicker/time/RadialPickerLayout;)Lnz;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oG:Lnz;

    return-object v0
.end method

.method public static synthetic c(Lcom/android/datetimepicker/time/RadialPickerLayout;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oR:I

    return v0
.end method

.method private cB()V
    .locals 7

    .prologue
    const/16 v6, 0x169

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 344
    new-array v1, v6, [I

    iput-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oM:[I

    .line 353
    const/16 v1, 0x8

    move v4, v0

    move v3, v0

    move v0, v1

    move v1, v2

    .line 355
    :goto_0
    if-ge v4, v6, :cond_3

    .line 357
    iget-object v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oM:[I

    aput v3, v5, v4

    .line 360
    if-ne v1, v0, :cond_2

    .line 361
    add-int/lit8 v1, v3, 0x6

    .line 362
    const/16 v0, 0x168

    if-ne v1, v0, :cond_0

    .line 363
    const/4 v0, 0x7

    :goto_1
    move v3, v1

    move v1, v2

    .line 355
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 364
    :cond_0
    rem-int/lit8 v0, v1, 0x1e

    if-nez v0, :cond_1

    .line 365
    const/16 v0, 0xe

    goto :goto_1

    .line 367
    :cond_1
    const/4 v0, 0x4

    goto :goto_1

    .line 371
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 374
    :cond_3
    return-void
.end method

.method public static synthetic d(Lcom/android/datetimepicker/time/RadialPickerLayout;)Lod;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oz:Lod;

    return-object v0
.end method

.method private q(II)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 239
    if-nez p1, :cond_1

    .line 240
    invoke-direct {p0, v3, p2}, Lcom/android/datetimepicker/time/RadialPickerLayout;->r(II)V

    .line 241
    rem-int/lit8 v0, p2, 0xc

    mul-int/lit8 v0, v0, 0x1e

    .line 242
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oJ:Loe;

    invoke-direct {p0, p2}, Lcom/android/datetimepicker/time/RadialPickerLayout;->ak(I)Z

    move-result v2

    invoke-virtual {v1, v0, v2, v3}, Loe;->b(IZZ)V

    .line 243
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oJ:Loe;

    invoke-virtual {v0}, Loe;->invalidate()V

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 244
    :cond_1
    if-ne p1, v0, :cond_0

    .line 245
    invoke-direct {p0, v0, p2}, Lcom/android/datetimepicker/time/RadialPickerLayout;->r(II)V

    .line 246
    mul-int/lit8 v0, p2, 0x6

    .line 247
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oK:Loe;

    invoke-virtual {v1, v0, v3, v3}, Loe;->b(IZZ)V

    .line 248
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oK:Loe;

    invoke-virtual {v0}, Loe;->invalidate()V

    goto :goto_0
.end method

.method private r(II)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 297
    if-nez p1, :cond_1

    .line 298
    iput p2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oB:I

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    if-ne p1, v1, :cond_2

    .line 300
    iput p2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oC:I

    goto :goto_0

    .line 301
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 302
    if-nez p2, :cond_3

    .line 303
    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oB:I

    rem-int/lit8 v0, v0, 0xc

    iput v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oB:I

    goto :goto_0

    .line 304
    :cond_3
    if-ne p2, v1, :cond_0

    .line 305
    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oB:I

    rem-int/lit8 v0, v0, 0xc

    add-int/lit8 v0, v0, 0xc

    iput v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oB:I

    goto :goto_0
.end method

.method private static s(II)I
    .locals 4

    .prologue
    .line 400
    div-int/lit8 v0, p0, 0x1e

    mul-int/lit8 v0, v0, 0x1e

    .line 402
    add-int/lit8 v1, v0, 0x1e

    .line 403
    const/4 v2, 0x1

    if-eq p1, v2, :cond_2

    .line 404
    const/4 v2, -0x1

    if-ne p1, v2, :cond_1

    .line 406
    if-ne p0, v0, :cond_0

    .line 407
    add-int/lit8 v0, v0, -0x1e

    .line 417
    :cond_0
    :goto_0
    return v0

    .line 411
    :cond_1
    sub-int v2, p0, v0

    sub-int v3, v1, p0

    if-lt v2, v3, :cond_0

    :cond_2
    move v0, v1

    .line 414
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lnd;IIZ)V
    .locals 13

    .prologue
    .line 171
    iget-boolean v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oA:Z

    if-eqz v1, :cond_0

    .line 172
    const-string v1, "RadialPickerLayout"

    const-string v2, "Time has already been initialized."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :goto_0
    return-void

    .line 176
    :cond_0
    iput-object p2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->mJ:Lnd;

    .line 177
    move/from16 v0, p5

    iput-boolean v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->os:Z

    .line 178
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oU:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oD:Z

    .line 181
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oF:Loa;

    iget-boolean v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oD:Z

    iget-boolean v3, v1, Loa;->ok:Z

    if-eqz v3, :cond_3

    const-string v1, "CircleView"

    const-string v2, "CircleView may only be initialized once."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    :goto_2
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oF:Loa;

    invoke-virtual {v1}, Loa;->invalidate()V

    .line 183
    iget-boolean v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oD:Z

    if-nez v1, :cond_1

    .line 184
    iget-object v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oG:Lnz;

    const/16 v1, 0xc

    move/from16 v0, p3

    if-ge v0, v1, :cond_5

    const/4 v1, 0x0

    :goto_3
    iget-boolean v3, v2, Lnz;->ok:Z

    if-eqz v3, :cond_6

    const-string v1, "AmPmCirclesView"

    const-string v2, "AmPmCirclesView may only be initialized once."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    :goto_4
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oG:Lnz;

    invoke-virtual {v1}, Lnz;->invalidate()V

    .line 189
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 190
    const/16 v1, 0xc

    new-array v6, v1, [I

    fill-array-data v6, :array_0

    .line 191
    const/16 v1, 0xc

    new-array v7, v1, [I

    fill-array-data v7, :array_1

    .line 192
    const/16 v1, 0xc

    new-array v8, v1, [I

    fill-array-data v8, :array_2

    .line 193
    const/16 v1, 0xc

    new-array v3, v1, [Ljava/lang/String;

    .line 194
    const/16 v1, 0xc

    new-array v4, v1, [Ljava/lang/String;

    .line 195
    const/16 v1, 0xc

    new-array v9, v1, [Ljava/lang/String;

    .line 196
    const/4 v1, 0x0

    move v5, v1

    :goto_5
    const/16 v1, 0xc

    if-ge v5, v1, :cond_8

    .line 197
    if-eqz p5, :cond_7

    const-string v1, "%02d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aget v12, v7, v5

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v1, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_6
    aput-object v1, v3, v5

    .line 199
    const-string v1, "%d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aget v12, v6, v5

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v1, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    .line 200
    const-string v1, "%02d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aget v12, v8, v5

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v1, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v9, v5

    .line 196
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_5

    .line 178
    :cond_2
    iget-boolean v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->os:Z

    goto/16 :goto_1

    .line 181
    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iput-boolean v2, v1, Loa;->os:Z

    if-eqz v2, :cond_4

    const v2, 0x7f0a0052

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    iput v2, v1, Loa;->og:F

    :goto_7
    const/4 v2, 0x1

    iput-boolean v2, v1, Loa;->ok:Z

    goto/16 :goto_2

    :cond_4
    const v2, 0x7f0a0051

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    iput v2, v1, Loa;->og:F

    const v2, 0x7f0a0054

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    iput v2, v1, Loa;->oh:F

    goto :goto_7

    .line 184
    :cond_5
    const/4 v1, 0x1

    goto/16 :goto_3

    :cond_6
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0028

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iput v4, v2, Lnz;->od:I

    const v4, 0x7f0b0036

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iput v4, v2, Lnz;->of:I

    const v4, 0x7f0b0031

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iput v4, v2, Lnz;->oe:I

    const/16 v4, 0x33

    iput v4, v2, Lnz;->oc:I

    const v4, 0x7f0a0069

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    iget-object v5, v2, Lnz;->ob:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v4, v2, Lnz;->ob:Landroid/graphics/Paint;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v4, v2, Lnz;->ob:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    const v4, 0x7f0a0051

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    iput v4, v2, Lnz;->og:F

    const v4, 0x7f0a0054

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    iput v3, v2, Lnz;->oh:F

    new-instance v3, Ljava/text/DateFormatSymbols;

    invoke-direct {v3}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v3}, Ljava/text/DateFormatSymbols;->getAmPmStrings()[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v4, v3, v4

    iput-object v4, v2, Lnz;->oi:Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    iput-object v3, v2, Lnz;->oj:Ljava/lang/String;

    iput v1, v2, Lnz;->oq:I

    const/4 v1, -0x1

    iput v1, v2, Lnz;->or:I

    const/4 v1, 0x1

    iput-boolean v1, v2, Lnz;->ok:Z

    goto/16 :goto_4

    .line 197
    :cond_7
    const-string v1, "%d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aget v12, v6, v5

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v1, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_6

    .line 202
    :cond_8
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oH:Log;

    if-eqz p5, :cond_9

    :goto_8
    iget-boolean v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oD:Z

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Log;->a(Landroid/content/res/Resources;[Ljava/lang/String;[Ljava/lang/String;ZZ)V

    .line 204
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oH:Log;

    invoke-virtual {v1}, Log;->invalidate()V

    .line 205
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oI:Log;

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oD:Z

    const/4 v6, 0x0

    move-object v3, v9

    invoke-virtual/range {v1 .. v6}, Log;->a(Landroid/content/res/Resources;[Ljava/lang/String;[Ljava/lang/String;ZZ)V

    .line 206
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oI:Log;

    invoke-virtual {v1}, Log;->invalidate()V

    .line 209
    const/4 v1, 0x0

    move/from16 v0, p3

    invoke-direct {p0, v1, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->r(II)V

    .line 210
    const/4 v1, 0x1

    move/from16 v0, p4

    invoke-direct {p0, v1, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->r(II)V

    .line 211
    rem-int/lit8 v1, p3, 0xc

    mul-int/lit8 v6, v1, 0x1e

    .line 212
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oJ:Loe;

    iget-boolean v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oD:Z

    const/4 v5, 0x1

    move/from16 v0, p3

    invoke-direct {p0, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->ak(I)Z

    move-result v7

    move-object v2, p1

    move/from16 v4, p5

    invoke-virtual/range {v1 .. v7}, Loe;->a(Landroid/content/Context;ZZZIZ)V

    .line 214
    mul-int/lit8 v6, p4, 0x6

    .line 215
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oK:Loe;

    iget-boolean v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oD:Z

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v2, p1

    invoke-virtual/range {v1 .. v7}, Loe;->a(Landroid/content/Context;ZZZIZ)V

    .line 218
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oA:Z

    goto/16 :goto_0

    .line 202
    :cond_9
    const/4 v4, 0x0

    goto :goto_8

    .line 190
    :array_0
    .array-data 4
        0xc
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
        0xa
        0xb
    .end array-data

    .line 191
    :array_1
    .array-data 4
        0x0
        0xd
        0xe
        0xf
        0x10
        0x11
        0x12
        0x13
        0x14
        0x15
        0x16
        0x17
    .end array-data

    .line 192
    :array_2
    .array-data 4
        0x0
        0x5
        0xa
        0xf
        0x14
        0x19
        0x1e
        0x23
        0x28
        0x2d
        0x32
        0x37
    .end array-data
.end method

.method public final a(Landroid/content/Context;Z)V
    .locals 5

    .prologue
    const v4, 0x7f0b0042

    const v3, 0x7f0b0028

    .line 222
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oF:Loa;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p2, :cond_0

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v0, Loa;->nS:I

    const v2, 0x7f0b0041

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Loa;->ot:I

    .line 223
    :goto_0
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oG:Lnz;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p2, :cond_1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v0, Lnz;->od:I

    const v2, 0x7f0b003f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v0, Lnz;->of:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Lnz;->oe:I

    const/16 v1, 0x66

    iput v1, v0, Lnz;->oc:I

    .line 224
    :goto_1
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oH:Log;

    invoke-virtual {v0, p1, p2}, Log;->a(Landroid/content/Context;Z)V

    .line 225
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oI:Log;

    invoke-virtual {v0, p1, p2}, Log;->a(Landroid/content/Context;Z)V

    .line 226
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oJ:Loe;

    invoke-virtual {v0, p1, p2}, Loe;->a(Landroid/content/Context;Z)V

    .line 227
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oK:Loe;

    invoke-virtual {v0, p1, p2}, Loe;->a(Landroid/content/Context;Z)V

    .line 228
    return-void

    .line 222
    :cond_0
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v0, Loa;->nS:I

    const v2, 0x7f0b0034

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Loa;->ot:I

    goto :goto_0

    .line 223
    :cond_1
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v0, Lnz;->od:I

    const v2, 0x7f0b0036

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v0, Lnz;->of:I

    const v2, 0x7f0b0031

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Lnz;->oe:I

    const/16 v1, 0x33

    iput v1, v0, Lnz;->oc:I

    goto :goto_1
.end method

.method public final a(Lod;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oz:Lod;

    .line 160
    return-void
.end method

.method public final al(I)V
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oG:Lnz;

    iput p1, v0, Lnz;->oq:I

    .line 316
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oG:Lnz;

    invoke-virtual {v0}, Lnz;->invalidate()V

    .line 317
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lcom/android/datetimepicker/time/RadialPickerLayout;->r(II)V

    .line 318
    return-void
.end method

.method public final cA()I
    .locals 2

    .prologue
    .line 285
    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oB:I

    const/16 v1, 0xc

    if-ge v0, v1, :cond_0

    .line 286
    const/4 v0, 0x0

    .line 290
    :goto_0
    return v0

    .line 287
    :cond_0
    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oB:I

    const/16 v1, 0x18

    if-ge v0, v1, :cond_1

    .line 288
    const/4 v0, 0x1

    goto :goto_0

    .line 290
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final cC()I
    .locals 3

    .prologue
    .line 511
    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oE:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oE:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 512
    const-string v0, "RadialPickerLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Current item showing was unfortunately set to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oE:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    const/4 v0, -0x1

    .line 515
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oE:I

    goto :goto_0
.end method

.method public final d(IZ)V
    .locals 6

    .prologue
    const/16 v0, 0xff

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 523
    if-eqz p1, :cond_0

    if-eq p1, v3, :cond_0

    .line 524
    const-string v0, "RadialPickerLayout"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TimePicker does not support view at index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    :goto_0
    return-void

    .line 528
    :cond_0
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->cC()I

    move-result v2

    .line 529
    iput p1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oE:I

    .line 531
    if-eqz p2, :cond_4

    if-eq p1, v2, :cond_4

    .line 532
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/animation/ObjectAnimator;

    .line 533
    if-ne p1, v3, :cond_3

    .line 534
    iget-object v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oH:Log;

    invoke-virtual {v2}, Log;->cD()Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v0, v1

    .line 535
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oJ:Loe;

    invoke-virtual {v1}, Loe;->cD()Landroid/animation/ObjectAnimator;

    move-result-object v1

    aput-object v1, v0, v3

    .line 536
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oI:Log;

    invoke-virtual {v1}, Log;->cE()Landroid/animation/ObjectAnimator;

    move-result-object v1

    aput-object v1, v0, v4

    .line 537
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oK:Loe;

    invoke-virtual {v1}, Loe;->cE()Landroid/animation/ObjectAnimator;

    move-result-object v1

    aput-object v1, v0, v5

    .line 545
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oV:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oV:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 546
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oV:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->end()V

    .line 548
    :cond_2
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oV:Landroid/animation/AnimatorSet;

    .line 549
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oV:Landroid/animation/AnimatorSet;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 550
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oV:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 538
    :cond_3
    if-nez p1, :cond_1

    .line 539
    iget-object v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oH:Log;

    invoke-virtual {v2}, Log;->cE()Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v0, v1

    .line 540
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oJ:Loe;

    invoke-virtual {v1}, Loe;->cE()Landroid/animation/ObjectAnimator;

    move-result-object v1

    aput-object v1, v0, v3

    .line 541
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oI:Log;

    invoke-virtual {v1}, Log;->cD()Landroid/animation/ObjectAnimator;

    move-result-object v1

    aput-object v1, v0, v4

    .line 542
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oK:Loe;

    invoke-virtual {v1}, Loe;->cD()Landroid/animation/ObjectAnimator;

    move-result-object v1

    aput-object v1, v0, v5

    goto :goto_1

    .line 552
    :cond_4
    if-nez p1, :cond_5

    move v2, v0

    .line 553
    :goto_2
    if-ne p1, v3, :cond_6

    .line 554
    :goto_3
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oH:Log;

    int-to-float v3, v2

    invoke-virtual {v1, v3}, Log;->setAlpha(F)V

    .line 555
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oJ:Loe;

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Loe;->setAlpha(F)V

    .line 556
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oI:Log;

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Log;->setAlpha(F)V

    .line 557
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oK:Loe;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Loe;->setAlpha(F)V

    goto/16 :goto_0

    :cond_5
    move v2, v1

    .line 552
    goto :goto_2

    :cond_6
    move v0, v1

    .line 553
    goto :goto_3
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 754
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v2, 0x20

    if-ne v0, v2, :cond_0

    .line 756
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 757
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 758
    iget v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oB:I

    iput v2, v0, Landroid/text/format/Time;->hour:I

    .line 759
    iget v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oC:I

    iput v2, v0, Landroid/text/format/Time;->minute:I

    .line 760
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 762
    iget-boolean v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->os:Z

    if-eqz v0, :cond_1

    .line 763
    const/16 v0, 0x81

    .line 765
    :goto_0
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2, v3, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 766
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 769
    :goto_1
    return v1

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v1

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final getHours()I
    .locals 1

    .prologue
    .line 262
    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oB:I

    return v0
.end method

.method public final getMinutes()I
    .locals 1

    .prologue
    .line 266
    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oC:I

    return v0
.end method

.method public final n(Z)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 728
    iget-boolean v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oQ:Z

    if-eqz v1, :cond_0

    if-nez p1, :cond_0

    .line 735
    :goto_0
    return v0

    .line 733
    :cond_0
    iput-boolean p1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oN:Z

    .line 734
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oL:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x4

    :cond_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 735
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 744
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 745
    const/16 v0, 0x1000

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 746
    const/16 v0, 0x2000

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 747
    return-void
.end method

.method public onMeasure(II)V
    .locals 4

    .prologue
    .line 148
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 149
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 150
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 151
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 152
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 154
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v1, v0}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 156
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v6, 0x2

    const/4 v9, 0x0

    const/4 v8, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 564
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 565
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 568
    new-array v4, v1, [Ljava/lang/Boolean;

    .line 569
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v2

    .line 571
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :cond_0
    :goto_0
    move v1, v2

    .line 721
    :cond_1
    :goto_1
    return v1

    .line 573
    :pswitch_0
    iget-boolean v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oN:Z

    if-eqz v5, :cond_1

    .line 577
    iput v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oS:F

    .line 578
    iput v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oT:F

    .line 580
    iput v8, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oy:I

    .line 581
    iput-boolean v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oP:Z

    .line 582
    iput-boolean v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oQ:Z

    .line 584
    iget-boolean v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oD:Z

    if-nez v2, :cond_3

    .line 585
    iget-object v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oG:Lnz;

    invoke-virtual {v2, v0, v3}, Lnz;->h(FF)I

    move-result v2

    iput v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oO:I

    .line 589
    :goto_2
    iget v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oO:I

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oO:I

    if-ne v2, v1, :cond_4

    .line 592
    :cond_2
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->mJ:Lnd;

    invoke-virtual {v0}, Lnd;->ci()V

    .line 593
    iput v8, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oR:I

    .line 594
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->mHandler:Landroid/os/Handler;

    new-instance v2, Lob;

    invoke-direct {v2, p0}, Lob;-><init>(Lcom/android/datetimepicker/time/RadialPickerLayout;)V

    iget v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->fF:I

    int-to-long v4, v3

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 587
    :cond_3
    iput v8, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oO:I

    goto :goto_2

    .line 604
    :cond_4
    iget-object v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oU:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v2

    .line 606
    invoke-direct {p0, v0, v3, v2, v4}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(FFZ[Ljava/lang/Boolean;)I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oR:I

    .line 607
    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oR:I

    if-eq v0, v8, :cond_1

    .line 610
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->mJ:Lnd;

    invoke-virtual {v0}, Lnd;->ci()V

    .line 611
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->mHandler:Landroid/os/Handler;

    new-instance v2, Loc;

    invoke-direct {v2, p0, v4}, Loc;-><init>(Lcom/android/datetimepicker/time/RadialPickerLayout;[Ljava/lang/Boolean;)V

    iget v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->fF:I

    int-to-long v4, v3

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 625
    :pswitch_1
    iget-boolean v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oN:Z

    if-nez v5, :cond_5

    .line 627
    const-string v0, "RadialPickerLayout"

    const-string v2, "Input was disabled, but received ACTION_MOVE."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 631
    :cond_5
    iget v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oT:F

    sub-float v5, v3, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 632
    iget v6, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oS:F

    sub-float v6, v0, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    .line 634
    iget-boolean v7, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oP:Z

    if-nez v7, :cond_6

    iget v7, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->ox:I

    int-to-float v7, v7

    cmpg-float v6, v6, v7

    if-gtz v6, :cond_6

    iget v6, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->ox:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-lez v5, :cond_0

    .line 636
    :cond_6
    iget v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oO:I

    if-eqz v5, :cond_7

    iget v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oO:I

    if-ne v5, v1, :cond_8

    .line 643
    :cond_7
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v9}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 644
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oG:Lnz;

    invoke-virtual {v1, v0, v3}, Lnz;->h(FF)I

    move-result v0

    .line 645
    iget v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oO:I

    if-eq v0, v1, :cond_0

    .line 646
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oG:Lnz;

    iput v8, v0, Lnz;->or:I

    .line 647
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oG:Lnz;

    invoke-virtual {v0}, Lnz;->invalidate()V

    .line 648
    iput v8, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oO:I

    goto/16 :goto_0

    .line 653
    :cond_8
    iget v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oR:I

    if-eq v5, v8, :cond_0

    .line 655
    iput-boolean v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oP:Z

    .line 660
    iget-object v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v9}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 661
    invoke-direct {p0, v0, v3, v1, v4}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(FFZ[Ljava/lang/Boolean;)I

    move-result v0

    .line 662
    if-eq v0, v8, :cond_1

    .line 663
    aget-object v3, v4, v2

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-direct {p0, v0, v3, v2, v1}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(IZZZ)I

    move-result v0

    .line 664
    iget v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oy:I

    if-eq v0, v3, :cond_1

    .line 665
    iget-object v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->mJ:Lnd;

    invoke-virtual {v3}, Lnd;->ci()V

    .line 666
    iput v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oy:I

    .line 667
    iget-object v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oz:Lod;

    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->cC()I

    move-result v4

    invoke-interface {v3, v4, v0, v2}, Lod;->a(IIZ)V

    goto/16 :goto_1

    .line 672
    :pswitch_2
    iget-boolean v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oN:Z

    if-nez v5, :cond_9

    .line 674
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oz:Lod;

    const/4 v3, 0x3

    invoke-interface {v0, v3, v1, v2}, Lod;->a(IIZ)V

    goto/16 :goto_1

    .line 679
    :cond_9
    iget-object v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v9}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 680
    iput-boolean v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oQ:Z

    .line 683
    iget v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oO:I

    if-eqz v5, :cond_a

    iget v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oO:I

    if-ne v5, v1, :cond_c

    .line 684
    :cond_a
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oG:Lnz;

    invoke-virtual {v1, v0, v3}, Lnz;->h(FF)I

    move-result v0

    .line 685
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oG:Lnz;

    iput v8, v1, Lnz;->or:I

    .line 686
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oG:Lnz;

    invoke-virtual {v1}, Lnz;->invalidate()V

    .line 688
    iget v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oO:I

    if-ne v0, v1, :cond_b

    .line 689
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oG:Lnz;

    iput v0, v1, Lnz;->oq:I

    .line 690
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->cA()I

    move-result v1

    if-eq v1, v0, :cond_b

    .line 691
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oz:Lod;

    iget v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oO:I

    invoke-interface {v1, v6, v3, v2}, Lod;->a(IIZ)V

    .line 692
    invoke-direct {p0, v6, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->r(II)V

    .line 695
    :cond_b
    iput v8, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oO:I

    goto/16 :goto_0

    .line 700
    :cond_c
    iget v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oR:I

    if-eq v5, v8, :cond_e

    .line 701
    iget-boolean v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oP:Z

    invoke-direct {p0, v0, v3, v5, v4}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(FFZ[Ljava/lang/Boolean;)I

    move-result v3

    .line 702
    if-eq v3, v8, :cond_e

    .line 703
    aget-object v0, v4, v2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iget-boolean v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oP:Z

    if-nez v0, :cond_f

    move v0, v1

    :goto_3
    invoke-direct {p0, v3, v4, v0, v2}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(IZZZ)I

    move-result v0

    .line 704
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->cC()I

    move-result v3

    if-nez v3, :cond_d

    iget-boolean v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->os:Z

    if-nez v3, :cond_d

    .line 705
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->cA()I

    move-result v3

    .line 706
    if-nez v3, :cond_10

    const/16 v4, 0xc

    if-ne v0, v4, :cond_10

    move v0, v2

    .line 712
    :cond_d
    :goto_4
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->cC()I

    move-result v3

    invoke-direct {p0, v3, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->r(II)V

    .line 713
    iget-object v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oz:Lod;

    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->cC()I

    move-result v4

    invoke-interface {v3, v4, v0, v1}, Lod;->a(IIZ)V

    .line 716
    :cond_e
    iput-boolean v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oP:Z

    goto/16 :goto_1

    :cond_f
    move v0, v2

    .line 703
    goto :goto_3

    .line 708
    :cond_10
    if-ne v3, v1, :cond_d

    const/16 v3, 0xc

    if-eq v0, v3, :cond_d

    .line 709
    add-int/lit8 v0, v0, 0xc

    goto :goto_4

    .line 571
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final p(II)V
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/android/datetimepicker/time/RadialPickerLayout;->q(II)V

    .line 232
    const/4 v0, 0x1

    invoke-direct {p0, v0, p2}, Lcom/android/datetimepicker/time/RadialPickerLayout;->q(II)V

    .line 233
    return-void
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 779
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v2, v4

    .line 827
    :cond_0
    :goto_0
    return v2

    .line 784
    :cond_1
    const/16 v1, 0x1000

    if-ne p1, v1, :cond_3

    move v3, v4

    .line 789
    :goto_1
    if-eqz v3, :cond_0

    .line 790
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->cC()I

    move-result v1

    if-nez v1, :cond_4

    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oB:I

    .line 792
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->cC()I

    move-result v5

    .line 793
    if-nez v5, :cond_5

    .line 794
    const/16 v1, 0x1e

    .line 795
    rem-int/lit8 v0, v0, 0xc

    move v6, v1

    move v1, v0

    move v0, v6

    .line 800
    :goto_3
    mul-int/2addr v1, v0

    .line 801
    invoke-static {v1, v3}, Lcom/android/datetimepicker/time/RadialPickerLayout;->s(II)I

    move-result v1

    .line 802
    div-int v3, v1, v0

    .line 805
    if-nez v5, :cond_7

    .line 806
    iget-boolean v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->os:Z

    if-eqz v0, :cond_6

    .line 807
    const/16 v1, 0x17

    move v0, v2

    .line 815
    :goto_4
    if-le v3, v1, :cond_8

    .line 822
    :goto_5
    invoke-direct {p0, v5, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->q(II)V

    .line 823
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oz:Lod;

    invoke-interface {v1, v5, v0, v2}, Lod;->a(IIZ)V

    move v2, v4

    .line 824
    goto :goto_0

    .line 786
    :cond_3
    const/16 v1, 0x2000

    if-ne p1, v1, :cond_b

    move v3, v0

    .line 787
    goto :goto_1

    .line 790
    :cond_4
    if-ne v1, v4, :cond_2

    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->oC:I

    goto :goto_2

    .line 796
    :cond_5
    if-ne v5, v4, :cond_a

    .line 797
    const/4 v1, 0x6

    move v6, v1

    move v1, v0

    move v0, v6

    goto :goto_3

    .line 809
    :cond_6
    const/16 v1, 0xc

    move v0, v4

    .line 810
    goto :goto_4

    .line 813
    :cond_7
    const/16 v1, 0x37

    move v0, v2

    goto :goto_4

    .line 818
    :cond_8
    if-ge v3, v0, :cond_9

    move v0, v1

    .line 820
    goto :goto_5

    :cond_9
    move v0, v3

    goto :goto_5

    :cond_a
    move v1, v0

    move v0, v2

    goto :goto_3

    :cond_b
    move v3, v2

    goto :goto_1
.end method
