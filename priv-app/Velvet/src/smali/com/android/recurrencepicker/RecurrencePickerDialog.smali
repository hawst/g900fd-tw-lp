.class public Lcom/android/recurrencepicker/RecurrencePickerDialog;
.super Landroid/app/DialogFragment;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;
.implements Lnk;


# static fields
.field private static TAG:Ljava/lang/String;

.field private static final aaB:[I


# instance fields
.field private aaA:Landroid/widget/Spinner;

.field private aaC:Landroid/widget/Switch;

.field private aaD:Landroid/widget/EditText;

.field private aaE:Landroid/widget/TextView;

.field private aaF:Landroid/widget/TextView;

.field private aaG:I

.field private aaH:Landroid/widget/Spinner;

.field private aaI:Landroid/widget/TextView;

.field private aaJ:Landroid/widget/EditText;

.field private aaK:Landroid/widget/TextView;

.field private aaL:Z

.field private aaM:Ljava/util/ArrayList;

.field private aaN:Lajq;

.field private aaO:Ljava/lang/String;

.field private aaP:Ljava/lang/String;

.field private aaQ:Ljava/lang/String;

.field private aaR:Landroid/widget/LinearLayout;

.field private aaS:Landroid/widget/LinearLayout;

.field private aaT:[Landroid/widget/ToggleButton;

.field private aaU:[[Ljava/lang/String;

.field private aaV:Landroid/widget/LinearLayout;

.field private aaW:Landroid/widget/RadioGroup;

.field private aaX:Landroid/widget/RadioButton;

.field private aaY:Landroid/widget/RadioButton;

.field private aaZ:Landroid/widget/RadioButton;

.field private aau:Lnh;

.field private aav:Laiw;

.field private aaw:Landroid/text/format/Time;

.field private aax:I

.field private aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

.field private final aaz:[I

.field private aba:Ljava/lang/String;

.field private abb:Landroid/widget/Button;

.field private abc:Lajr;

.field private mResources:Landroid/content/res/Resources;

.field private mView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-class v0, Lcom/android/recurrencepicker/RecurrencePickerDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->TAG:Ljava/lang/String;

    .line 320
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaB:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x4
        0x5
        0x6
        0x7
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x7

    .line 377
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 296
    new-instance v0, Laiw;

    invoke-direct {v0}, Laiw;-><init>()V

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aav:Laiw;

    .line 297
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    .line 299
    new-instance v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    invoke-direct {v0}, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;-><init>()V

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    .line 302
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaz:[I

    .line 342
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaG:I

    .line 350
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaM:Ljava/util/ArrayList;

    .line 360
    new-array v0, v2, [Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaT:[Landroid/widget/ToggleButton;

    .line 378
    return-void

    .line 302
    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
    .end array-data
.end method

.method private static a(Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;Laiw;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 546
    iget v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abj:I

    if-nez v0, :cond_0

    .line 547
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There\'s no recurrence"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 551
    :cond_0
    sget-object v0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaB:[I

    iget v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZU:I

    aget v0, v0, v1

    iput v0, p1, Laiw;->ZU:I

    .line 554
    iget v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZW:I

    if-gt v0, v3, :cond_3

    .line 555
    iput v2, p1, Laiw;->ZW:I

    .line 561
    :goto_0
    iget v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abk:I

    packed-switch v0, :pswitch_data_0

    .line 580
    iput v2, p1, Laiw;->count:I

    .line 581
    iput-object v4, p1, Laiw;->ZV:Ljava/lang/String;

    .line 586
    :cond_1
    :goto_1
    iput v2, p1, Laiw;->aag:I

    .line 587
    iput v2, p1, Laiw;->aai:I

    .line 589
    iget v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZU:I

    packed-switch v0, :pswitch_data_1

    .line 637
    :cond_2
    :goto_2
    invoke-static {p1}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->a(Laiw;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 638
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UI generated recurrence that it can\'t handle. ER:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Laiw;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Model: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 557
    :cond_3
    iget v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZW:I

    iput v0, p1, Laiw;->ZW:I

    goto :goto_0

    .line 563
    :pswitch_0
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    if-eqz v0, :cond_4

    .line 564
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 565
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 566
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Laiw;->ZV:Ljava/lang/String;

    .line 567
    iput v2, p1, Laiw;->count:I

    goto :goto_1

    .line 569
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "end = END_BY_DATE but endDate is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 573
    :pswitch_1
    iget v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abm:I

    iput v0, p1, Laiw;->count:I

    .line 574
    iput-object v4, p1, Laiw;->ZV:Ljava/lang/String;

    .line 575
    iget v0, p1, Laiw;->count:I

    if-gtz v0, :cond_1

    .line 576
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "count is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Laiw;->count:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 591
    :pswitch_2
    iget v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abo:I

    if-nez v0, :cond_8

    .line 592
    iget v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abp:I

    if-gtz v0, :cond_5

    iget v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abp:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 593
    :cond_5
    iget-object v0, p1, Laiw;->aah:[I

    if-eqz v0, :cond_6

    iget v0, p1, Laiw;->aai:I

    if-gtz v0, :cond_7

    .line 594
    :cond_6
    new-array v0, v3, [I

    iput-object v0, p1, Laiw;->aah:[I

    .line 596
    :cond_7
    iget-object v0, p1, Laiw;->aah:[I

    iget v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abp:I

    aput v1, v0, v2

    .line 597
    iput v3, p1, Laiw;->aai:I

    goto/16 :goto_2

    .line 599
    :cond_8
    iget v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abo:I

    if-ne v0, v3, :cond_2

    .line 600
    iget v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abr:I

    invoke-static {v0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->bW(I)Z

    move-result v0

    if-nez v0, :cond_9

    .line 601
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "month repeat by nth week but n is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abr:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 604
    :cond_9
    iget v0, p1, Laiw;->aag:I

    if-lez v0, :cond_a

    iget-object v0, p1, Laiw;->aae:[I

    if-eqz v0, :cond_a

    iget-object v0, p1, Laiw;->aaf:[I

    if-nez v0, :cond_b

    .line 606
    :cond_a
    new-array v0, v3, [I

    iput-object v0, p1, Laiw;->aae:[I

    .line 607
    new-array v0, v3, [I

    iput-object v0, p1, Laiw;->aaf:[I

    .line 609
    :cond_b
    iput v3, p1, Laiw;->aag:I

    .line 610
    iget-object v0, p1, Laiw;->aae:[I

    iget v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abq:I

    invoke-static {v1}, Laiw;->bT(I)I

    move-result v1

    aput v1, v0, v2

    .line 611
    iget-object v0, p1, Laiw;->aaf:[I

    iget v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abr:I

    aput v1, v0, v2

    goto/16 :goto_2

    :pswitch_3
    move v1, v2

    move v0, v2

    .line 616
    :goto_3
    const/4 v3, 0x7

    if-ge v1, v3, :cond_d

    .line 617
    iget-object v3, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abn:[Z

    aget-boolean v3, v3, v1

    if-eqz v3, :cond_c

    .line 618
    add-int/lit8 v0, v0, 0x1

    .line 616
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 622
    :cond_d
    iget v1, p1, Laiw;->aag:I

    if-lt v1, v0, :cond_e

    iget-object v1, p1, Laiw;->aae:[I

    if-eqz v1, :cond_e

    iget-object v1, p1, Laiw;->aaf:[I

    if-nez v1, :cond_f

    .line 623
    :cond_e
    new-array v1, v0, [I

    iput-object v1, p1, Laiw;->aae:[I

    .line 624
    new-array v1, v0, [I

    iput-object v1, p1, Laiw;->aaf:[I

    .line 626
    :cond_f
    iput v0, p1, Laiw;->aag:I

    .line 628
    const/4 v1, 0x6

    :goto_4
    if-ltz v1, :cond_2

    .line 629
    iget-object v3, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abn:[Z

    aget-boolean v3, v3, v1

    if-eqz v3, :cond_10

    .line 630
    iget-object v3, p1, Laiw;->aaf:[I

    add-int/lit8 v0, v0, -0x1

    aput v2, v3, v0

    .line 631
    iget-object v3, p1, Laiw;->aae:[I

    invoke-static {v1}, Laiw;->bT(I)I

    move-result v4

    aput v4, v3, v0

    .line 628
    :cond_10
    add-int/lit8 v1, v1, -0x1

    goto :goto_4

    .line 641
    :cond_11
    return-void

    .line 561
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 589
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static synthetic a(Lcom/android/recurrencepicker/RecurrencePickerDialog;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mg()V

    return-void
.end method

.method public static a(Laiw;)Z
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 387
    iget v0, p0, Laiw;->ZU:I

    packed-switch v0, :pswitch_data_0

    .line 439
    :cond_0
    :goto_0
    return v1

    .line 397
    :pswitch_0
    iget v0, p0, Laiw;->count:I

    if-lez v0, :cond_1

    iget-object v0, p0, Laiw;->ZV:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    move v0, v1

    move v2, v1

    .line 410
    :goto_1
    iget v4, p0, Laiw;->aag:I

    if-ge v0, v4, :cond_3

    .line 411
    iget-object v4, p0, Laiw;->aaf:[I

    aget v4, v4, v0

    invoke-static {v4}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->bW(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 412
    add-int/lit8 v2, v2, 0x1

    .line 410
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 416
    :cond_3
    if-gt v2, v3, :cond_0

    .line 420
    if-lez v2, :cond_4

    iget v0, p0, Laiw;->ZU:I

    if-ne v0, v5, :cond_0

    .line 426
    :cond_4
    iget v0, p0, Laiw;->aai:I

    if-gt v0, v3, :cond_0

    .line 430
    iget v0, p0, Laiw;->ZU:I

    if-ne v0, v5, :cond_5

    .line 431
    iget v0, p0, Laiw;->aag:I

    if-gt v0, v3, :cond_0

    .line 434
    iget v0, p0, Laiw;->aag:I

    if-lez v0, :cond_5

    iget v0, p0, Laiw;->aai:I

    if-gtz v0, :cond_0

    :cond_5
    move v1, v3

    .line 439
    goto :goto_0

    .line 387
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static synthetic a(Lcom/android/recurrencepicker/RecurrencePickerDialog;Z)Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaL:Z

    return v0
.end method

.method public static synthetic b(Lcom/android/recurrencepicker/RecurrencePickerDialog;)Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    return-object v0
.end method

.method private static bW(I)Z
    .locals 1

    .prologue
    .line 383
    if-lez p0, :cond_0

    const/4 v0, 0x5

    if-le p0, v0, :cond_1

    :cond_0
    const/4 v0, -0x1

    if-ne p0, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic c(Lcom/android/recurrencepicker/RecurrencePickerDialog;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mf()V

    return-void
.end method

.method public static synthetic d(Lcom/android/recurrencepicker/RecurrencePickerDialog;)I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaG:I

    return v0
.end method

.method public static synthetic e(Lcom/android/recurrencepicker/RecurrencePickerDialog;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaD:Landroid/widget/EditText;

    return-object v0
.end method

.method public static synthetic f(Lcom/android/recurrencepicker/RecurrencePickerDialog;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mj()V

    return-void
.end method

.method public static synthetic g(Lcom/android/recurrencepicker/RecurrencePickerDialog;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mk()V

    return-void
.end method

.method public static synthetic h(Lcom/android/recurrencepicker/RecurrencePickerDialog;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaJ:Landroid/widget/EditText;

    return-object v0
.end method

.method public static synthetic i(Lcom/android/recurrencepicker/RecurrencePickerDialog;)Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaH:Landroid/widget/Spinner;

    return-object v0
.end method

.method public static synthetic j(Lcom/android/recurrencepicker/RecurrencePickerDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaP:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic k(Lcom/android/recurrencepicker/RecurrencePickerDialog;)Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method public static synthetic l(Lcom/android/recurrencepicker/RecurrencePickerDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaQ:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic m(Lcom/android/recurrencepicker/RecurrencePickerDialog;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaK:Landroid/widget/TextView;

    return-object v0
.end method

.method private mf()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 878
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v1, v1, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abj:I

    if-nez v1, :cond_0

    .line 879
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaA:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 880
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaH:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 881
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaE:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 882
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaD:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 883
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaF:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 884
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaW:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->setEnabled(Z)V

    .line 885
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaJ:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 886
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaK:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 887
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaI:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 888
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaX:Landroid/widget/RadioButton;

    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 889
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaY:Landroid/widget/RadioButton;

    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 890
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaZ:Landroid/widget/RadioButton;

    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 891
    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaT:[Landroid/widget/ToggleButton;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 892
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 891
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 895
    :cond_0
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    const v2, 0x7f110394

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 896
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaA:Landroid/widget/Spinner;

    invoke-virtual {v1, v4}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 897
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaH:Landroid/widget/Spinner;

    invoke-virtual {v1, v4}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 898
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaE:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 899
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaD:Landroid/widget/EditText;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 900
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaF:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 901
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaW:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v4}, Landroid/widget/RadioGroup;->setEnabled(Z)V

    .line 902
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaJ:Landroid/widget/EditText;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 903
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaK:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 904
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaI:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 905
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaX:Landroid/widget/RadioButton;

    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 906
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaY:Landroid/widget/RadioButton;

    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 907
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaZ:Landroid/widget/RadioButton;

    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 908
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaT:[Landroid/widget/ToggleButton;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 909
    invoke-virtual {v3, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 908
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 912
    :cond_1
    invoke-direct {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mg()V

    .line 913
    return-void
.end method

.method private mg()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 916
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abj:I

    if-nez v0, :cond_0

    .line 917
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->abb:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 944
    :goto_0
    return-void

    .line 921
    :cond_0
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaD:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 922
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->abb:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 926
    :cond_1
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaJ:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaJ:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 928
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->abb:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 932
    :cond_2
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZU:I

    if-ne v0, v5, :cond_5

    .line 933
    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaT:[Landroid/widget/ToggleButton;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_4

    aget-object v4, v2, v0

    .line 934
    invoke-virtual {v4}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 935
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->abb:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 933
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 939
    :cond_4
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->abb:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 943
    :cond_5
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->abb:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method private mh()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v2, 0x8

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 959
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZW:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 960
    iget-object v3, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaD:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 961
    iget-object v3, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaD:Landroid/widget/EditText;

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 964
    :cond_0
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaA:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v3, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZU:I

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 965
    iget-object v3, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaR:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZU:I

    if-ne v0, v4, :cond_4

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 966
    iget-object v3, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaS:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZU:I

    if-ne v0, v4, :cond_5

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 967
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaV:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v3, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZU:I

    if-ne v3, v5, :cond_1

    move v2, v1

    :cond_1
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 969
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZU:I

    packed-switch v0, :pswitch_data_0

    .line 1015
    :cond_2
    :goto_2
    invoke-direct {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mj()V

    .line 1016
    invoke-direct {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mg()V

    .line 1018
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaH:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v2, v2, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abk:I

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1019
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abk:I

    if-ne v0, v4, :cond_b

    .line 1020
    invoke-virtual {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v2, v2, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    invoke-virtual {v2, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    const/high16 v1, 0x20000

    invoke-static {v0, v2, v3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 1022
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaI:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1034
    :cond_3
    :goto_3
    return-void

    :cond_4
    move v0, v2

    .line 965
    goto :goto_0

    :cond_5
    move v0, v2

    .line 966
    goto :goto_1

    .line 971
    :pswitch_0
    const v0, 0x7f100001

    iput v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaG:I

    goto :goto_2

    .line 975
    :pswitch_1
    const v0, 0x7f100002

    iput v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaG:I

    move v0, v1

    .line 976
    :goto_4
    const/4 v2, 0x7

    if-ge v0, v2, :cond_2

    .line 977
    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaT:[Landroid/widget/ToggleButton;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v3, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abn:[Z

    aget-boolean v3, v3, v0

    invoke-virtual {v2, v3}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 976
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 982
    :pswitch_2
    const v0, 0x7f100003

    iput v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaG:I

    .line 984
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abo:I

    if-nez v0, :cond_9

    .line 985
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abp:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_8

    .line 986
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaW:Landroid/widget/RadioGroup;

    const v2, 0x7f11039e

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 994
    :cond_6
    :goto_5
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aba:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 995
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abr:I

    if-nez v0, :cond_7

    .line 996
    invoke-direct {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mi()V

    .line 999
    :cond_7
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaU:[[Ljava/lang/String;

    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v2, v2, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abq:I

    aget-object v2, v0, v2

    .line 1003
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abr:I

    if-gez v0, :cond_a

    const/4 v0, 0x5

    .line 1005
    :goto_6
    add-int/lit8 v0, v0, -0x1

    aget-object v0, v2, v0

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aba:Ljava/lang/String;

    .line 1007
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaX:Landroid/widget/RadioButton;

    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aba:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 988
    :cond_8
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaW:Landroid/widget/RadioGroup;

    const v2, 0x7f11039c

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_5

    .line 990
    :cond_9
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abo:I

    if-ne v0, v4, :cond_6

    .line 991
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaW:Landroid/widget/RadioGroup;

    const v2, 0x7f11039d

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_5

    .line 1003
    :cond_a
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abr:I

    goto :goto_6

    .line 1012
    :pswitch_3
    const v0, 0x7f100004

    iput v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaG:I

    goto/16 :goto_2

    .line 1024
    :cond_b
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abk:I

    if-ne v0, v5, :cond_3

    .line 1028
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abm:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 1029
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaJ:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1030
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaJ:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 969
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private mi()V
    .locals 2

    .prologue
    .line 1037
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, 0x6

    div-int/lit8 v1, v1, 0x7

    iput v1, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abr:I

    .line 1039
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x7

    iget v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aax:I

    if-le v0, v1, :cond_0

    .line 1040
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    const/4 v1, -0x1

    iput v1, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abr:I

    .line 1042
    :cond_0
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->weekDay:I

    iput v1, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abq:I

    .line 1043
    return-void
.end method

.method private mj()V
    .locals 5

    .prologue
    const/4 v3, -0x1

    .line 1055
    iget v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaG:I

    if-ne v0, v3, :cond_1

    .line 1069
    :cond_0
    :goto_0
    return-void

    .line 1059
    :cond_1
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mResources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaG:I

    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v2, v2, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZW:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    .line 1061
    const-string v1, "%d"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 1063
    if-eq v1, v3, :cond_0

    .line 1064
    add-int/lit8 v2, v1, 0x2

    .line 1065
    iget-object v3, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaF:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1067
    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaE:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private mk()V
    .locals 4

    .prologue
    .line 1076
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mResources:Landroid/content/res/Resources;

    const/high16 v1, 0x7f100000

    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v2, v2, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abm:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    .line 1079
    const-string v1, "%d"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 1081
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 1082
    if-nez v1, :cond_1

    .line 1083
    sget-object v0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->TAG:Ljava/lang/String;

    const-string v1, "No text to put in to recurrence\'s end spinner."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1090
    :cond_0
    :goto_0
    return-void

    .line 1085
    :cond_1
    add-int/lit8 v1, v1, 0x2

    .line 1086
    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaK:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lajr;)V
    .locals 0

    .prologue
    .line 1223
    iput-object p1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->abc:Lajr;

    .line 1224
    return-void
.end method

.method public final d(III)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1137
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    if-nez v0, :cond_0

    .line 1138
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    iget-object v2, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    .line 1139
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v1, v1, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v2, v2, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    iput v3, v2, Landroid/text/format/Time;->second:I

    iput v3, v1, Landroid/text/format/Time;->minute:I

    iput v3, v0, Landroid/text/format/Time;->hour:I

    .line 1141
    :cond_0
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    iput p1, v0, Landroid/text/format/Time;->year:I

    .line 1142
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    iput p2, v0, Landroid/text/format/Time;->month:I

    .line 1143
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    iput p3, v0, Landroid/text/format/Time;->monthDay:I

    .line 1144
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->normalize(Z)J

    .line 1145
    invoke-direct {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mh()V

    .line 1146
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1210
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1211
    invoke-virtual {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "tag_date_picker_frag"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lnh;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aau:Lnh;

    .line 1213
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aau:Lnh;

    if-eqz v0, :cond_0

    .line 1214
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aau:Lnh;

    invoke-virtual {v0, p0}, Lnh;->a(Lnk;)V

    .line 1216
    :cond_0
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 1152
    .line 1153
    const/4 v1, 0x0

    move v0, v2

    :goto_0
    const/4 v3, 0x7

    if-ge v1, v3, :cond_1

    .line 1154
    if-ne v0, v2, :cond_0

    iget-object v3, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaT:[Landroid/widget/ToggleButton;

    aget-object v3, v3, v1

    if-ne p1, v3, :cond_0

    .line 1156
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abn:[Z

    aput-boolean p2, v0, v1

    move v0, v1

    .line 1153
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1159
    :cond_1
    invoke-direct {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mh()V

    .line 1160
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1166
    const v0, 0x7f11039c

    if-ne p2, v0, :cond_1

    .line 1167
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iput v1, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abo:I

    .line 1168
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    iput v1, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abp:I

    .line 1175
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mh()V

    .line 1176
    return-void

    .line 1169
    :cond_1
    const v0, 0x7f11039e

    if-ne p2, v0, :cond_2

    .line 1170
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iput v1, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abo:I

    .line 1171
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    const/4 v1, -0x1

    iput v1, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abp:I

    goto :goto_0

    .line 1172
    :cond_2
    const v0, 0x7f11039d

    if-ne p2, v0, :cond_0

    .line 1173
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    const/4 v1, 0x1

    iput v1, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abo:I

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1183
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaI:Landroid/widget/TextView;

    if-ne v0, p1, :cond_2

    .line 1184
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aau:Lnh;

    if-eqz v0, :cond_0

    .line 1185
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aau:Lnh;

    invoke-virtual {v0}, Lnh;->dismiss()V

    .line 1187
    :cond_0
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->year:I

    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v1, v1, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->month:I

    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v2, v2, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->monthDay:I

    invoke-static {p0, v0, v1, v2}, Lnh;->a(Lnk;III)Lnh;

    move-result-object v0

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aau:Lnh;

    .line 1189
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aau:Lnh;

    invoke-virtual {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->getActivity()Landroid/app/Activity;

    const/4 v0, 0x0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument must be between Time.SUNDAY and Time.SATURDAY"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v0, 0x2

    :goto_0
    invoke-virtual {v1, v0}, Lnh;->setFirstDayOfWeek(I)V

    .line 1190
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aau:Lnh;

    const/16 v1, 0x7b2

    const/16 v2, 0x7f4

    invoke-virtual {v0, v1, v2}, Lnh;->n(II)V

    .line 1191
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1192
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->year:I

    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->month:I

    iget-object v3, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    .line 1193
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aau:Lnh;

    invoke-virtual {v1, v0}, Lnh;->a(Ljava/util/Calendar;)V

    .line 1194
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aau:Lnh;

    invoke-virtual {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "tag_date_picker_frag"

    invoke-virtual {v0, v1, v2}, Lnh;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1206
    :cond_1
    :goto_1
    return-void

    .line 1189
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x5

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x6

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x7

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x1

    goto :goto_0

    .line 1195
    :cond_2
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->abb:Landroid/widget/Button;

    if-ne v0, p1, :cond_1

    .line 1197
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abj:I

    if-nez v0, :cond_3

    .line 1198
    const/4 v0, 0x0

    .line 1203
    :goto_2
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->abc:Lajr;

    invoke-interface {v1, v0}, Lajr;->z(Ljava/lang/String;)V

    .line 1204
    invoke-virtual {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->dismiss()V

    goto :goto_1

    .line 1200
    :cond_3
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aav:Laiw;

    invoke-static {v0, v1}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->a(Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;Laiw;)V

    .line 1201
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aav:Laiw;

    invoke-virtual {v0}, Laiw;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1189
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v9, 0x7

    const/4 v10, 0x3

    const/4 v11, 0x2

    const/4 v6, 0x0

    const/4 v8, 0x1

    .line 646
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aav:Laiw;

    invoke-virtual {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->getActivity()Landroid/app/Activity;

    invoke-static {v6}, Laiw;->bT(I)I

    move-result v1

    iput v1, v0, Laiw;->ZX:I

    .line 649
    invoke-virtual {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/Window;->requestFeature(I)Z

    .line 652
    if-eqz p3, :cond_3

    .line 653
    const-string v0, "bundle_model"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    .line 654
    if-eqz v0, :cond_0

    .line 655
    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    .line 657
    :cond_0
    const-string v0, "bundle_end_count_has_focus"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    move v7, v0

    .line 697
    :goto_0
    invoke-virtual {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mResources:Landroid/content/res/Resources;

    .line 698
    const v0, 0x7f04014e

    invoke-virtual {p1, v0, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    .line 700
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    const v1, 0x7f110393

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaC:Landroid/widget/Switch;

    .line 701
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaC:Landroid/widget/Switch;

    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abj:I

    if-ne v0, v8, :cond_14

    move v0, v8

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/Switch;->setChecked(Z)V

    .line 702
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaC:Landroid/widget/Switch;

    new-instance v1, Lajn;

    invoke-direct {v1, p0}, Lajn;-><init>(Lcom/android/recurrencepicker/RecurrencePickerDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 712
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    const v1, 0x7f110392

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaA:Landroid/widget/Spinner;

    .line 713
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaA:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 714
    invoke-virtual {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/high16 v1, 0x7f0f0000

    const v2, 0x7f040150

    invoke-static {v0, v1, v2}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    .line 716
    const v1, 0x7f040150

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 717
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaA:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 719
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    const v1, 0x7f110397

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaD:Landroid/widget/EditText;

    .line 720
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaD:Landroid/widget/EditText;

    new-instance v1, Lajo;

    const/16 v2, 0x63

    invoke-direct {v1, p0, v8, v8, v2}, Lajo;-><init>(Lcom/android/recurrencepicker/RecurrencePickerDialog;III)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 730
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    const v1, 0x7f110396

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaE:Landroid/widget/TextView;

    .line 731
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    const v1, 0x7f110398

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaF:Landroid/widget/TextView;

    .line 733
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaO:Ljava/lang/String;

    .line 734
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a0049

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaP:Ljava/lang/String;

    .line 735
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a004b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaQ:Ljava/lang/String;

    .line 737
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaM:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaO:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 738
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaM:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaP:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 739
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaM:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaQ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 740
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    const v1, 0x7f1103a0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaH:Landroid/widget/Spinner;

    .line 741
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaH:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 742
    new-instance v0, Lajq;

    invoke-virtual {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaM:Ljava/util/ArrayList;

    const v4, 0x7f040150

    const v5, 0x7f04014f

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lajq;-><init>(Lcom/android/recurrencepicker/RecurrencePickerDialog;Landroid/content/Context;Ljava/util/ArrayList;II)V

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaN:Lajq;

    .line 744
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaN:Lajq;

    const v1, 0x7f040150

    invoke-virtual {v0, v1}, Lajq;->setDropDownViewResource(I)V

    .line 745
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaH:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaN:Lajq;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 747
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    const v1, 0x7f1103a1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaJ:Landroid/widget/EditText;

    .line 748
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaJ:Landroid/widget/EditText;

    new-instance v1, Lajp;

    const/4 v2, 0x5

    const/16 v3, 0x2da

    invoke-direct {v1, p0, v8, v2, v3}, Lajp;-><init>(Lcom/android/recurrencepicker/RecurrencePickerDialog;III)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 758
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    const v1, 0x7f1103a2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaK:Landroid/widget/TextView;

    .line 760
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    const v1, 0x7f1103a3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaI:Landroid/widget/TextView;

    .line 761
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaI:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 762
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    if-nez v0, :cond_1

    .line 763
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iput-object v1, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    .line 764
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZU:I

    packed-switch v0, :pswitch_data_0

    .line 776
    :goto_2
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    invoke-virtual {v0, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 779
    :cond_1
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    const v1, 0x7f110399

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaR:Landroid/widget/LinearLayout;

    .line 780
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    const v1, 0x7f11039a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaS:Landroid/widget/LinearLayout;

    .line 783
    new-instance v0, Ljava/text/DateFormatSymbols;

    invoke-direct {v0}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v0}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    .line 785
    new-array v0, v9, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaU:[[Ljava/lang/String;

    .line 787
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaU:[[Ljava/lang/String;

    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0f0002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    .line 788
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaU:[[Ljava/lang/String;

    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0f0003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v8

    .line 789
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaU:[[Ljava/lang/String;

    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0f0004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v11

    .line 790
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaU:[[Ljava/lang/String;

    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0f0005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v10

    .line 791
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaU:[[Ljava/lang/String;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0f0006

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 792
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaU:[[Ljava/lang/String;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0f0007

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 793
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaU:[[Ljava/lang/String;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0f0008

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 796
    invoke-virtual {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->getActivity()Landroid/app/Activity;

    .line 799
    new-instance v0, Ljava/text/DateFormatSymbols;

    invoke-direct {v0}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v0}, Ljava/text/DateFormatSymbols;->getShortWeekdays()[Ljava/lang/String;

    move-result-object v5

    .line 804
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenWidthDp:I

    const/16 v1, 0x1c2

    if-le v0, v1, :cond_15

    .line 807
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaS:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 808
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    move v1, v9

    move v2, v6

    :goto_3
    move v3, v6

    move v4, v6

    .line 820
    :goto_4
    if-ge v4, v9, :cond_17

    .line 821
    if-lt v4, v1, :cond_16

    .line 822
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaR:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/16 v8, 0x8

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    move v0, v3

    .line 820
    :cond_2
    :goto_5
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v0

    goto :goto_4

    .line 659
    :cond_3
    invoke-virtual {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 660
    if-eqz v0, :cond_13

    .line 661
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    const-string v2, "bundle_event_start_time"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 663
    const-string v1, "bundle_event_time_zone"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 664
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 665
    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    iput-object v1, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 667
    :cond_4
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    invoke-virtual {v1, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 669
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 670
    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    invoke-virtual {v1, v8, v2}, Ljava/util/Calendar;->set(II)V

    .line 671
    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->month:I

    invoke-virtual {v1, v11, v2}, Ljava/util/Calendar;->set(II)V

    .line 672
    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v1

    iput v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aax:I

    .line 675
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v1, v1, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abn:[Z

    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->weekDay:I

    aput-boolean v8, v1, v2

    .line 676
    const-string v1, "bundle_event_rrule"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 677
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 678
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iput v8, v1, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abj:I

    .line 679
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aav:Laiw;

    invoke-virtual {v1, v0}, Laiw;->parse(Ljava/lang/String;)V

    .line 680
    iget-object v2, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aav:Laiw;

    iget-object v3, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v2, Laiw;->ZU:I

    packed-switch v0, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "freq="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v2, Laiw;->ZU:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iput v6, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZU:I

    :goto_6
    iget v0, v2, Laiw;->ZW:I

    if-lez v0, :cond_5

    iget v0, v2, Laiw;->ZW:I

    iput v0, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZW:I

    :cond_5
    iget v0, v2, Laiw;->count:I

    iput v0, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abm:I

    iget v0, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abm:I

    if-lez v0, :cond_6

    iput v11, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abk:I

    :cond_6
    iget-object v0, v2, Laiw;->ZV:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    if-nez v0, :cond_7

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    :cond_7
    :try_start_0
    iget-object v0, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    iget-object v1, v2, Laiw;->ZV:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/util/TimeFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_7
    iget v0, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abk:I

    if-ne v0, v11, :cond_8

    iget-object v0, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    if-eqz v0, :cond_8

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "freq="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v2, Laiw;->ZU:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    iput v11, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZU:I

    goto :goto_6

    :pswitch_2
    iput v10, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZU:I

    goto :goto_6

    :pswitch_3
    iput v8, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZU:I

    goto :goto_6

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    goto :goto_7

    :cond_8
    iput v8, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abk:I

    :cond_9
    iget-object v0, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abn:[Z

    invoke-static {v0, v6}, Ljava/util/Arrays;->fill([ZZ)V

    iget v0, v2, Laiw;->aag:I

    if-lez v0, :cond_d

    move v0, v6

    move v1, v6

    :goto_8
    iget v4, v2, Laiw;->aag:I

    if-ge v0, v4, :cond_b

    iget-object v4, v2, Laiw;->aae:[I

    aget v4, v4, v0

    invoke-static {v4}, Laiw;->bU(I)I

    move-result v4

    iget-object v5, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abn:[Z

    aput-boolean v8, v5, v4

    iget v5, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZU:I

    if-ne v5, v11, :cond_a

    iget-object v5, v2, Laiw;->aaf:[I

    aget v5, v5, v0

    invoke-static {v5}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->bW(I)Z

    move-result v5

    if-eqz v5, :cond_a

    iput v4, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abq:I

    iget-object v4, v2, Laiw;->aaf:[I

    aget v4, v4, v0

    iput v4, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abr:I

    iput v8, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abo:I

    add-int/lit8 v1, v1, 0x1

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_b
    iget v0, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZU:I

    if-ne v0, v11, :cond_d

    iget v0, v2, Laiw;->aag:I

    if-eq v0, v8, :cond_c

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can handle only 1 byDayOfWeek in monthly"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    if-eq v1, v8, :cond_d

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Didn\'t specify which nth day of week to repeat for a monthly"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    iget v0, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZU:I

    if-ne v0, v11, :cond_f

    iget v0, v2, Laiw;->aai:I

    if-ne v0, v8, :cond_10

    iget v0, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abo:I

    if-ne v0, v8, :cond_e

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can handle only by monthday or by nth day of week, not both"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    iget-object v0, v2, Laiw;->aah:[I

    aget v0, v0, v6

    iput v0, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abp:I

    iput v6, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abo:I

    .line 682
    :cond_f
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aav:Laiw;

    iget v0, v0, Laiw;->aag:I

    if-nez v0, :cond_11

    .line 683
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abn:[Z

    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->weekDay:I

    aput-boolean v8, v0, v1

    move v7, v6

    goto/16 :goto_0

    .line 680
    :cond_10
    iget v0, v2, Laiw;->aao:I

    if-le v0, v8, :cond_f

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can handle only one bymonthday"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 684
    :cond_11
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aav:Laiw;

    iget v0, v0, Laiw;->aag:I

    if-ne v0, v8, :cond_12

    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aav:Laiw;

    iget v0, v0, Laiw;->ZU:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_12

    .line 688
    invoke-direct {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mi()V

    :cond_12
    move v7, v6

    .line 692
    goto/16 :goto_0

    .line 693
    :cond_13
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    move v7, v6

    goto/16 :goto_0

    :cond_14
    move v0, v6

    .line 701
    goto/16 :goto_1

    .line 767
    :pswitch_4
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->month:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/text/format/Time;->month:I

    goto/16 :goto_2

    .line 770
    :pswitch_5
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->month:I

    add-int/lit8 v1, v1, 0x3

    iput v1, v0, Landroid/text/format/Time;->month:I

    goto/16 :goto_2

    .line 773
    :pswitch_6
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget-object v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abl:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->year:I

    add-int/lit8 v1, v1, 0x3

    iput v1, v0, Landroid/text/format/Time;->year:I

    goto/16 :goto_2

    .line 810
    :cond_15
    const/4 v0, 0x4

    .line 813
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaS:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 816
    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaS:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    move v1, v0

    move v2, v10

    goto/16 :goto_3

    .line 826
    :cond_16
    iget-object v8, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaT:[Landroid/widget/ToggleButton;

    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaR:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    aput-object v0, v8, v3

    .line 827
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaT:[Landroid/widget/ToggleButton;

    aget-object v0, v0, v3

    iget-object v8, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaz:[I

    aget v8, v8, v3

    aget-object v8, v5, v8

    invoke-virtual {v0, v8}, Landroid/widget/ToggleButton;->setTextOff(Ljava/lang/CharSequence;)V

    .line 828
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaT:[Landroid/widget/ToggleButton;

    aget-object v0, v0, v3

    iget-object v8, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaz:[I

    aget v8, v8, v3

    aget-object v8, v5, v8

    invoke-virtual {v0, v8}, Landroid/widget/ToggleButton;->setTextOn(Ljava/lang/CharSequence;)V

    .line 829
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaT:[Landroid/widget/ToggleButton;

    aget-object v0, v0, v3

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 831
    add-int/lit8 v0, v3, 0x1

    if-lt v0, v9, :cond_2

    move v0, v6

    .line 832
    goto/16 :goto_5

    :cond_17
    move v1, v3

    move v3, v6

    .line 837
    :goto_9
    if-ge v3, v10, :cond_1a

    .line 838
    if-lt v3, v2, :cond_19

    .line 839
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    move v0, v1

    .line 837
    :cond_18
    :goto_a
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_9

    .line 842
    :cond_19
    iget-object v4, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaT:[Landroid/widget/ToggleButton;

    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    aput-object v0, v4, v1

    .line 843
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaT:[Landroid/widget/ToggleButton;

    aget-object v0, v0, v1

    iget-object v4, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaz:[I

    aget v4, v4, v1

    aget-object v4, v5, v4

    invoke-virtual {v0, v4}, Landroid/widget/ToggleButton;->setTextOff(Ljava/lang/CharSequence;)V

    .line 844
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaT:[Landroid/widget/ToggleButton;

    aget-object v0, v0, v1

    iget-object v4, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaz:[I

    aget v4, v4, v1

    aget-object v4, v5, v4

    invoke-virtual {v0, v4}, Landroid/widget/ToggleButton;->setTextOn(Ljava/lang/CharSequence;)V

    .line 845
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaT:[Landroid/widget/ToggleButton;

    aget-object v0, v0, v1

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 847
    add-int/lit8 v0, v1, 0x1

    if-lt v0, v9, :cond_18

    move v0, v6

    .line 848
    goto :goto_a

    .line 852
    :cond_1a
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    const v1, 0x7f11039b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaV:Landroid/widget/LinearLayout;

    .line 853
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    const v1, 0x7f11039b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaW:Landroid/widget/RadioGroup;

    .line 854
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaW:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 855
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    const v1, 0x7f11039d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaX:Landroid/widget/RadioButton;

    .line 857
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    const v1, 0x7f11039c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaY:Landroid/widget/RadioButton;

    .line 859
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    const v1, 0x7f11039e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaZ:Landroid/widget/RadioButton;

    .line 862
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaw:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    iget v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aax:I

    if-eq v0, v1, :cond_1b

    .line 863
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaZ:Landroid/widget/RadioButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 866
    :cond_1b
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    const v1, 0x7f110142

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->abb:Landroid/widget/Button;

    .line 867
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->abb:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 869
    invoke-direct {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mf()V

    .line 870
    invoke-direct {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mh()V

    .line 871
    if-eqz v7, :cond_1c

    .line 872
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaJ:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 874
    :cond_1c
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mView:Landroid/view/View;

    return-object v0

    .line 764
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 680
    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    const/16 v3, 0x2da

    const/16 v2, 0x8

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1097
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaA:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_1

    .line 1098
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iput p3, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->ZU:I

    .line 1127
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mh()V

    .line 1128
    return-void

    .line 1099
    :cond_1
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaH:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_0

    .line 1100
    packed-switch p3, :pswitch_data_0

    .line 1118
    :goto_1
    iget-object v3, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaJ:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abk:I

    if-ne v0, v5, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1120
    iget-object v3, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaI:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abk:I

    if-ne v0, v4, :cond_5

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1122
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaK:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v3, v3, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abk:I

    if-ne v3, v5, :cond_6

    iget-boolean v3, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaL:Z

    if-nez v3, :cond_6

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 1102
    :pswitch_0
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iput v1, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abk:I

    goto :goto_1

    .line 1105
    :pswitch_1
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iput v4, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abk:I

    goto :goto_1

    .line 1108
    :pswitch_2
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iput v5, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abk:I

    .line 1110
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abm:I

    if-gt v0, v4, :cond_3

    .line 1111
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iput v4, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abm:I

    .line 1115
    :cond_2
    :goto_5
    invoke-direct {p0}, Lcom/android/recurrencepicker/RecurrencePickerDialog;->mk()V

    goto :goto_1

    .line 1112
    :cond_3
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iget v0, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abm:I

    if-le v0, v3, :cond_2

    .line 1113
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    iput v3, v0, Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;->abm:I

    goto :goto_5

    :cond_4
    move v0, v2

    .line 1118
    goto :goto_2

    :cond_5
    move v0, v2

    .line 1120
    goto :goto_3

    :cond_6
    move v1, v2

    .line 1122
    goto :goto_4

    .line 1100
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 1133
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 948
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 949
    const-string v0, "bundle_model"

    iget-object v1, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aay:Lcom/android/recurrencepicker/RecurrencePickerDialog$RecurrenceModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 950
    iget-object v0, p0, Lcom/android/recurrencepicker/RecurrencePickerDialog;->aaJ:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 951
    const-string v0, "bundle_end_count_has_focus"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 953
    :cond_0
    return-void
.end method
