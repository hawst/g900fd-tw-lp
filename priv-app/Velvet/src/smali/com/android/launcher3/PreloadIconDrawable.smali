.class public Lcom/android/launcher3/PreloadIconDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "PG"


# static fields
.field private static final IK:Landroid/graphics/Rect;


# instance fields
.field private final Si:Landroid/graphics/RectF;

.field private Sj:Z

.field final Sk:Landroid/graphics/drawable/Drawable;

.field private Sl:Landroid/graphics/drawable/Drawable;

.field private Sm:I

.field private Sn:I

.field private So:I

.field private Sp:F

.field private Sq:Landroid/animation/ObjectAnimator;

.field private final ob:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/android/launcher3/PreloadIconDrawable;->IK:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;Landroid/content/res/Resources$Theme;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 29
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Si:Landroid/graphics/RectF;

    .line 38
    iput v2, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sn:I

    .line 44
    iput v2, p0, Lcom/android/launcher3/PreloadIconDrawable;->So:I

    .line 46
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sp:F

    .line 50
    iput-object p1, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sk:Landroid/graphics/drawable/Drawable;

    .line 52
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->ob:Landroid/graphics/Paint;

    .line 53
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->ob:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 54
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->ob:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 56
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PreloadIconDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 57
    invoke-virtual {p0, p2}, Lcom/android/launcher3/PreloadIconDrawable;->applyTheme(Landroid/content/res/Resources$Theme;)V

    .line 58
    invoke-virtual {p0, v2}, Lcom/android/launcher3/PreloadIconDrawable;->onLevelChange(I)Z

    .line 59
    return-void
.end method


# virtual methods
.method public applyTheme(Landroid/content/res/Resources$Theme;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 63
    sget-object v0, Ladb;->Sy:[I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 64
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sl:Landroid/graphics/drawable/Drawable;

    .line 65
    iget-object v1, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v5}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 66
    iget-object v1, p0, Lcom/android/launcher3/PreloadIconDrawable;->ob:Landroid/graphics/Paint;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 67
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sm:I

    .line 68
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 69
    invoke-virtual {p0}, Lcom/android/launcher3/PreloadIconDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PreloadIconDrawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 70
    invoke-virtual {p0}, Lcom/android/launcher3/PreloadIconDrawable;->invalidateSelf()V

    .line 71
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const/16 v12, 0xff

    const/4 v4, 0x0

    const/high16 v0, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f000000    # 0.5f

    .line 113
    new-instance v7, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/android/launcher3/PreloadIconDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-direct {v7, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 114
    sget-object v1, Lcom/android/launcher3/PreloadIconDrawable;->IK:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/launcher3/PreloadIconDrawable;->IK:Landroid/graphics/Rect;

    invoke-static {v1, v7}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 150
    :goto_0
    return-void

    .line 118
    :cond_0
    iget-boolean v1, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sj:Z

    if-eqz v1, :cond_1

    .line 119
    iget-object v1, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    sget-object v3, Lcom/android/launcher3/PreloadIconDrawable;->IK:Landroid/graphics/Rect;

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v3, v5

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    int-to-float v1, v1

    div-float v1, v5, v1

    iget-object v5, p0, Lcom/android/launcher3/PreloadIconDrawable;->Si:Landroid/graphics/RectF;

    iget v8, v2, Landroid/graphics/Rect;->left:I

    int-to-float v8, v8

    sget-object v9, Lcom/android/launcher3/PreloadIconDrawable;->IK:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    int-to-float v9, v9

    mul-float/2addr v9, v3

    add-float/2addr v8, v9

    iget v9, v2, Landroid/graphics/Rect;->top:I

    int-to-float v9, v9

    sget-object v10, Lcom/android/launcher3/PreloadIconDrawable;->IK:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    int-to-float v10, v10

    mul-float/2addr v10, v1

    add-float/2addr v9, v10

    iget v10, v2, Landroid/graphics/Rect;->right:I

    int-to-float v10, v10

    sget-object v11, Lcom/android/launcher3/PreloadIconDrawable;->IK:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    int-to-float v11, v11

    mul-float/2addr v3, v11

    sub-float v3, v10, v3

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    sget-object v10, Lcom/android/launcher3/PreloadIconDrawable;->IK:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v10, v10

    mul-float/2addr v1, v10

    sub-float v1, v2, v1

    invoke-virtual {v5, v8, v9, v3, v1}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v1, p0, Lcom/android/launcher3/PreloadIconDrawable;->ob:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/android/launcher3/PreloadIconDrawable;->Si:Landroid/graphics/RectF;

    invoke-virtual {v2, v1, v1}, Landroid/graphics/RectF;->inset(FF)V

    iput-boolean v4, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sj:Z

    .line 123
    :cond_1
    iget v1, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sp:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_3

    iget v1, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sp:F

    cmpg-float v1, v1, v0

    if-gez v1, :cond_3

    .line 125
    iget-object v1, p0, Lcom/android/launcher3/PreloadIconDrawable;->ob:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sp:F

    sub-float/2addr v0, v2

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 126
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sl:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/launcher3/PreloadIconDrawable;->ob:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getAlpha()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 127
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 128
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Si:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/android/launcher3/PreloadIconDrawable;->ob:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 130
    iget v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sp:F

    mul-float/2addr v0, v6

    add-float/2addr v0, v6

    .line 146
    :cond_2
    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 147
    invoke-virtual {v7}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v1

    invoke-virtual {v7}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    invoke-virtual {p1, v0, v0, v1, v2}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 148
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sk:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 149
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0

    .line 131
    :cond_3
    iget v1, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sp:F

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_2

    .line 132
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, v12}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 134
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v12}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 135
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 137
    iget v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->So:I

    const/16 v1, 0x64

    if-lt v0, v1, :cond_4

    .line 138
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Si:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/android/launcher3/PreloadIconDrawable;->ob:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    move v0, v6

    goto :goto_1

    .line 139
    :cond_4
    iget v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->So:I

    if-lez v0, :cond_5

    .line 140
    iget-object v1, p0, Lcom/android/launcher3/PreloadIconDrawable;->Si:Landroid/graphics/RectF;

    const/high16 v2, -0x3d4c0000    # -90.0f

    iget v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->So:I

    int-to-float v0, v0

    const v3, 0x40666666    # 3.6f

    mul-float/2addr v3, v0

    iget-object v5, p0, Lcom/android/launcher3/PreloadIconDrawable;->ob:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    move v0, v6

    goto :goto_1

    :cond_5
    move v0, v6

    goto :goto_1
.end method

.method public getAnimationProgress()F
    .locals 1

    .prologue
    .line 214
    iget v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sp:F

    return v0
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sk:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sk:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 154
    const/4 v0, -0x3

    return v0
.end method

.method public final jX()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sm:I

    return v0
.end method

.method public final jY()V
    .locals 2

    .prologue
    .line 194
    iget v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sp:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 204
    :goto_0
    return-void

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sq:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sq:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 200
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/PreloadIconDrawable;->setAnimationProgress(F)V

    .line 201
    const-string v0, "animationProgress"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sq:Landroid/animation/ObjectAnimator;

    .line 203
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sq:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    .line 201
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sk:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 76
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sl:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 77
    sget-object v0, Lcom/android/launcher3/PreloadIconDrawable;->IK:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 78
    sget-object v0, Lcom/android/launcher3/PreloadIconDrawable;->IK:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sm:I

    neg-int v1, v1

    iget v2, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sm:I

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->inset(II)V

    .line 79
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sl:Landroid/graphics/drawable/Drawable;

    sget-object v1, Lcom/android/launcher3/PreloadIconDrawable;->IK:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 81
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sj:Z

    .line 82
    return-void
.end method

.method protected onLevelChange(I)Z
    .locals 7

    .prologue
    const/4 v6, 0x2

    const v5, -0xff6978

    const/4 v2, 0x1

    .line 169
    iput p1, p0, Lcom/android/launcher3/PreloadIconDrawable;->So:I

    .line 172
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sq:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sq:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sq:Landroid/animation/ObjectAnimator;

    .line 176
    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sp:F

    .line 177
    if-lez p1, :cond_1

    .line 180
    iget-object v1, p0, Lcom/android/launcher3/PreloadIconDrawable;->ob:Landroid/graphics/Paint;

    iget v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sn:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sn:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 182
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sk:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Lus;

    if-eqz v0, :cond_2

    .line 183
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sk:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lus;

    if-gtz p1, :cond_6

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Lus;->M(Z)V

    .line 186
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher3/PreloadIconDrawable;->invalidateSelf()V

    .line 187
    return v2

    .line 180
    :cond_3
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sk:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Lus;

    if-nez v0, :cond_4

    iput v5, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sn:I

    iget v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sn:I

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sk:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lus;

    invoke-virtual {v0}, Lus;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    const/16 v3, 0x14

    invoke-static {v0, v3}, Ladp;->c(Landroid/graphics/Bitmap;I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sn:I

    const/4 v0, 0x3

    new-array v0, v0, [F

    iget v3, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sn:I

    invoke-static {v3, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    aget v3, v0, v2

    const v4, 0x3e4ccccd    # 0.2f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_5

    iput v5, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sn:I

    iget v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sn:I

    goto :goto_0

    :cond_5
    const v3, 0x3f19999a    # 0.6f

    aget v4, v0, v6

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    aput v3, v0, v6

    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sn:I

    iget v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sn:I

    goto :goto_0

    .line 183
    :cond_6
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public setAlpha(I)V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sk:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 160
    return-void
.end method

.method public setAnimationProgress(F)V
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sp:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 208
    iput p1, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sp:F

    .line 209
    invoke-virtual {p0}, Lcom/android/launcher3/PreloadIconDrawable;->invalidateSelf()V

    .line 211
    :cond_0
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/android/launcher3/PreloadIconDrawable;->Sk:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 165
    return-void
.end method
