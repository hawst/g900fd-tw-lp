.class public Lcom/android/launcher3/WallpaperRootView;
.super Landroid/widget/RelativeLayout;
.source "PG"


# instance fields
.field private final UC:Ladz;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    check-cast p1, Ladz;

    iput-object p1, p0, Lcom/android/launcher3/WallpaperRootView;->UC:Ladz;

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    check-cast p1, Ladz;

    iput-object p1, p0, Lcom/android/launcher3/WallpaperRootView;->UC:Ladz;

    .line 33
    return-void
.end method


# virtual methods
.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/android/launcher3/WallpaperRootView;->UC:Ladz;

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Ladz;->v(F)V

    .line 37
    const/4 v0, 0x1

    return v0
.end method
