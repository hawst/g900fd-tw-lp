.class public Lcom/android/launcher3/CropView;
.super Lair;
.source "PG"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# instance fields
.field private Ca:Landroid/view/ScaleGestureDetector;

.field private Cb:J

.field private Cc:F

.field private Cd:F

.field private Ce:F

.field private Cf:F

.field private Cg:Z

.field private Ch:Landroid/graphics/RectF;

.field private Ci:[F

.field private Cj:[F

.field private Ck:[F

.field private Cl:[F

.field private Cm:[F

.field private Cn:Lti;

.field private Co:Landroid/graphics/Matrix;

.field private Cp:Landroid/graphics/Matrix;

.field private sJ:F

.field private sK:F

.field private ss:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher3/CropView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 65
    invoke-direct {p0, p1, p2}, Lair;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/CropView;->Cg:Z

    .line 44
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/CropView;->Ch:Landroid/graphics/RectF;

    .line 45
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/launcher3/CropView;->Ci:[F

    .line 46
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/android/launcher3/CropView;->Cj:[F

    .line 47
    new-array v0, v1, [F

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/android/launcher3/CropView;->Ck:[F

    .line 48
    new-array v0, v1, [F

    fill-array-data v0, :array_3

    iput-object v0, p0, Lcom/android/launcher3/CropView;->Cl:[F

    .line 49
    new-array v0, v1, [F

    fill-array-data v0, :array_4

    iput-object v0, p0, Lcom/android/launcher3/CropView;->Cm:[F

    .line 66
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-direct {v0, p1, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/android/launcher3/CropView;->Ca:Landroid/view/ScaleGestureDetector;

    .line 67
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/CropView;->Co:Landroid/graphics/Matrix;

    .line 68
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/CropView;->Cp:Landroid/graphics/Matrix;

    .line 69
    return-void

    .line 45
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 46
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 47
    :array_2
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 48
    :array_3
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 49
    :array_4
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method private a(IILaiq;Z)V
    .locals 4

    .prologue
    .line 159
    iget-object v1, p0, Lcom/android/launcher3/CropView;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 160
    if-eqz p4, :cond_0

    .line 161
    :try_start_0
    iget-object v0, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v0, Laiu;->Ix:F

    .line 163
    :cond_0
    if-eqz p3, :cond_1

    .line 164
    invoke-direct {p0}, Lcom/android/launcher3/CropView;->fp()[F

    move-result-object v0

    .line 165
    const/4 v2, 0x0

    aget v2, v0, v2

    .line 166
    const/4 v3, 0x1

    aget v0, v0, v3

    .line 167
    int-to-float v3, p1

    div-float v2, v3, v2

    int-to-float v3, p2

    div-float v0, v3, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/android/launcher3/CropView;->ss:F

    .line 168
    iget-object v2, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget v3, p0, Lcom/android/launcher3/CropView;->ss:F

    if-eqz p4, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, v2, Laiu;->Ix:F

    .line 171
    :cond_1
    monitor-exit v1

    return-void

    .line 168
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget v0, v0, Laiu;->Ix:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 171
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Landroid/graphics/RectF;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    .line 84
    invoke-virtual {p0}, Lcom/android/launcher3/CropView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 85
    invoke-virtual {p0}, Lcom/android/launcher3/CropView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 86
    invoke-direct {p0}, Lcom/android/launcher3/CropView;->fp()[F

    move-result-object v2

    .line 87
    aget v3, v2, v9

    .line 88
    aget v2, v2, v10

    .line 90
    iget-object v4, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget-object v4, v4, Laiu;->ZQ:Laiq;

    invoke-interface {v4}, Laiq;->gi()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v8

    .line 91
    iget-object v5, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget-object v5, v5, Laiu;->ZQ:Laiq;

    invoke-interface {v5}, Laiq;->gj()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v8

    .line 93
    iget-object v6, p0, Lcom/android/launcher3/CropView;->Cm:[F

    .line 94
    iget v7, p0, Lcom/android/launcher3/CropView;->sJ:F

    sub-float v4, v7, v4

    aput v4, v6, v9

    .line 95
    iget v4, p0, Lcom/android/launcher3/CropView;->sK:F

    sub-float/2addr v4, v5

    aput v4, v6, v10

    .line 96
    iget-object v4, p0, Lcom/android/launcher3/CropView;->Co:Landroid/graphics/Matrix;

    invoke-virtual {v4, v6}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 97
    aget v4, v6, v9

    div-float v5, v3, v8

    add-float/2addr v4, v5

    aput v4, v6, v9

    .line 98
    aget v4, v6, v10

    div-float v5, v2, v8

    add-float/2addr v4, v5

    aput v4, v6, v10

    .line 100
    iget-object v4, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget v4, v4, Laiu;->Ix:F

    .line 101
    div-float v5, v0, v8

    aget v7, v6, v9

    sub-float/2addr v5, v7

    sub-float v7, v3, v0

    div-float/2addr v7, v8

    add-float/2addr v5, v7

    mul-float/2addr v5, v4

    div-float/2addr v0, v8

    add-float/2addr v0, v5

    .line 103
    div-float v5, v1, v8

    aget v6, v6, v10

    sub-float/2addr v5, v6

    sub-float v6, v2, v1

    div-float/2addr v6, v8

    add-float/2addr v5, v6

    mul-float/2addr v5, v4

    div-float/2addr v1, v8

    add-float/2addr v1, v5

    .line 105
    div-float v5, v3, v8

    mul-float/2addr v5, v4

    sub-float v5, v0, v5

    .line 106
    div-float/2addr v3, v8

    mul-float/2addr v3, v4

    add-float/2addr v0, v3

    .line 107
    div-float v3, v2, v8

    mul-float/2addr v3, v4

    sub-float v3, v1, v3

    .line 108
    div-float/2addr v2, v8

    mul-float/2addr v2, v4

    add-float/2addr v1, v2

    .line 110
    iput v5, p1, Landroid/graphics/RectF;->left:F

    .line 111
    iput v0, p1, Landroid/graphics/RectF;->right:F

    .line 112
    iput v3, p1, Landroid/graphics/RectF;->top:F

    .line 113
    iput v1, p1, Landroid/graphics/RectF;->bottom:F

    .line 114
    return-void
.end method

.method private fp()[F
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 72
    iget-object v0, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget-object v0, v0, Laiu;->ZQ:Laiq;

    invoke-interface {v0}, Laiq;->gi()I

    move-result v0

    int-to-float v0, v0

    .line 73
    iget-object v1, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget-object v1, v1, Laiu;->ZQ:Laiq;

    invoke-interface {v1}, Laiq;->gj()I

    move-result v1

    int-to-float v1, v1

    .line 74
    iget-object v2, p0, Lcom/android/launcher3/CropView;->Cl:[F

    .line 75
    aput v0, v2, v3

    .line 76
    aput v1, v2, v4

    .line 77
    iget-object v0, p0, Lcom/android/launcher3/CropView;->Co:Landroid/graphics/Matrix;

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 78
    aget v0, v2, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    aput v0, v2, v3

    .line 79
    aget v0, v2, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    aput v0, v2, v4

    .line 80
    return-object v2
.end method

.method private fu()V
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget v1, p0, Lcom/android/launcher3/CropView;->sJ:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v0, Laiu;->centerX:I

    .line 212
    iget-object v0, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget v1, p0, Lcom/android/launcher3/CropView;->sK:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v0, Laiu;->centerY:I

    .line 213
    return-void
.end method


# virtual methods
.method public final K(Z)V
    .locals 0

    .prologue
    .line 216
    iput-boolean p1, p0, Lcom/android/launcher3/CropView;->Cg:Z

    .line 217
    return-void
.end method

.method public final a(Laiq;Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 138
    invoke-super {p0, p1, p2}, Lair;->a(Laiq;Ljava/lang/Runnable;)V

    .line 139
    iget-object v0, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget v0, v0, Laiu;->centerX:I

    int-to-float v0, v0

    iput v0, p0, Lcom/android/launcher3/CropView;->sJ:F

    .line 140
    iget-object v0, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget v0, v0, Laiu;->centerY:I

    int-to-float v0, v0

    iput v0, p0, Lcom/android/launcher3/CropView;->sK:F

    .line 141
    iget-object v0, p0, Lcom/android/launcher3/CropView;->Co:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 142
    iget-object v0, p0, Lcom/android/launcher3/CropView;->Co:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget v1, v1, Laiu;->rotation:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 143
    iget-object v0, p0, Lcom/android/launcher3/CropView;->Cp:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 144
    iget-object v0, p0, Lcom/android/launcher3/CropView;->Cp:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget v1, v1, Laiu;->rotation:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 145
    invoke-virtual {p0}, Lcom/android/launcher3/CropView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/CropView;->getHeight()I

    move-result v1

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/android/launcher3/CropView;->a(IILaiq;Z)V

    .line 146
    return-void
.end method

.method public final a(Lti;)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lcom/android/launcher3/CropView;->Cn:Lti;

    .line 221
    return-void
.end method

.method public final fq()I
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget v0, v0, Laiu;->rotation:I

    return v0
.end method

.method public final fr()Landroid/graphics/RectF;
    .locals 5

    .prologue
    .line 121
    iget-object v0, p0, Lcom/android/launcher3/CropView;->Ch:Landroid/graphics/RectF;

    .line 122
    invoke-direct {p0, v0}, Lcom/android/launcher3/CropView;->a(Landroid/graphics/RectF;)V

    .line 123
    iget-object v1, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget v1, v1, Laiu;->Ix:F

    .line 125
    iget v2, v0, Landroid/graphics/RectF;->left:F

    neg-float v2, v2

    div-float/2addr v2, v1

    .line 126
    iget v0, v0, Landroid/graphics/RectF;->top:F

    neg-float v0, v0

    div-float/2addr v0, v1

    .line 127
    invoke-virtual {p0}, Lcom/android/launcher3/CropView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v1

    add-float/2addr v3, v2

    .line 128
    invoke-virtual {p0}, Lcom/android/launcher3/CropView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float v1, v4, v1

    add-float/2addr v1, v0

    .line 130
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v2, v0, v3, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v4
.end method

.method public final fs()Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 134
    new-instance v0, Landroid/graphics/Point;

    iget-object v1, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget-object v1, v1, Laiu;->ZQ:Laiq;

    invoke-interface {v1}, Laiq;->gi()I

    move-result v1

    iget-object v2, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget-object v2, v2, Laiu;->ZQ:Laiq;

    invoke-interface {v2}, Laiq;->gj()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method

.method public final ft()V
    .locals 4

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/android/launcher3/CropView;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/CropView;->getHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 195
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/CropView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 196
    new-instance v1, Lth;

    invoke-direct {v1, p0}, Lth;-><init>(Lcom/android/launcher3/CropView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 203
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/CropView;->Ch:Landroid/graphics/RectF;

    .line 204
    invoke-direct {p0, v0}, Lcom/android/launcher3/CropView;->a(Landroid/graphics/RectF;)V

    .line 205
    iget-object v1, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget v1, v1, Laiu;->Ix:F

    .line 206
    iget v2, p0, Lcom/android/launcher3/CropView;->sJ:F

    float-to-double v2, v2

    iget v0, v0, Landroid/graphics/RectF;->left:F

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    add-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/android/launcher3/CropView;->sJ:F

    .line 207
    invoke-direct {p0}, Lcom/android/launcher3/CropView;->fu()V

    .line 208
    return-void
.end method

.method public final k(F)V
    .locals 2

    .prologue
    .line 153
    iget-object v1, p0, Lcom/android/launcher3/CropView;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 154
    :try_start_0
    iget-object v0, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iput p1, v0, Laiu;->Ix:F

    .line 155
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 3

    .prologue
    .line 183
    iget-object v0, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget v1, v0, Laiu;->Ix:F

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v2

    mul-float/2addr v1, v2

    iput v1, v0, Laiu;->Ix:F

    .line 184
    iget-object v0, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget v1, p0, Lcom/android/launcher3/CropView;->ss:F

    iget-object v2, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget v2, v2, Laiu;->Ix:F

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, v0, Laiu;->Ix:F

    .line 185
    invoke-virtual {p0}, Lcom/android/launcher3/CropView;->invalidate()V

    .line 186
    const/4 v0, 0x1

    return v0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x1

    return v0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 0

    .prologue
    .line 191
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget-object v0, v0, Laiu;->ZQ:Laiq;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/launcher3/CropView;->a(IILaiq;Z)V

    .line 150
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v8, 0x1

    .line 225
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v9

    .line 226
    const/4 v0, 0x6

    if-ne v9, v0, :cond_1

    move v7, v8

    .line 227
    :goto_0
    if-eqz v7, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 231
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    move v5, v3

    move v1, v6

    move v2, v6

    .line 232
    :goto_2
    if-ge v5, v4, :cond_3

    .line 233
    if-eq v0, v5, :cond_0

    .line 234
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v10

    add-float/2addr v2, v10

    .line 236
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v10

    add-float/2addr v1, v10

    .line 232
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_1
    move v7, v3

    .line 226
    goto :goto_0

    .line 227
    :cond_2
    const/4 v0, -0x1

    goto :goto_1

    .line 238
    :cond_3
    if-eqz v7, :cond_5

    add-int/lit8 v0, v4, -0x1

    .line 239
    :goto_3
    int-to-float v4, v0

    div-float/2addr v2, v4

    .line 240
    int-to-float v0, v0

    div-float/2addr v1, v0

    .line 242
    if-nez v9, :cond_6

    .line 243
    iput v2, p0, Lcom/android/launcher3/CropView;->Cc:F

    .line 244
    iput v1, p0, Lcom/android/launcher3/CropView;->Cd:F

    .line 245
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/launcher3/CropView;->Cb:J

    .line 246
    iget-object v0, p0, Lcom/android/launcher3/CropView;->Cn:Lti;

    if-eqz v0, :cond_4

    .line 247
    iget-object v0, p0, Lcom/android/launcher3/CropView;->Cn:Lti;

    invoke-interface {v0}, Lti;->fv()V

    .line 265
    :cond_4
    :goto_4
    iget-boolean v0, p0, Lcom/android/launcher3/CropView;->Cg:Z

    if-nez v0, :cond_8

    .line 320
    :goto_5
    return v8

    :cond_5
    move v0, v4

    .line 238
    goto :goto_3

    .line 249
    :cond_6
    if-ne v9, v8, :cond_4

    .line 250
    invoke-virtual {p0}, Lcom/android/launcher3/CropView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 252
    iget v4, p0, Lcom/android/launcher3/CropView;->Cc:F

    sub-float/2addr v4, v2

    iget v5, p0, Lcom/android/launcher3/CropView;->Cc:F

    sub-float/2addr v5, v2

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/android/launcher3/CropView;->Cd:F

    sub-float/2addr v5, v1

    iget v7, p0, Lcom/android/launcher3/CropView;->Cd:F

    sub-float/2addr v7, v1

    mul-float/2addr v5, v7

    add-float/2addr v4, v5

    .line 253
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    mul-int/2addr v0, v5

    int-to-float v0, v0

    .line 254
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 255
    iget-object v5, p0, Lcom/android/launcher3/CropView;->Cn:Lti;

    if-eqz v5, :cond_4

    .line 257
    cmpg-float v0, v4, v0

    if-gez v0, :cond_7

    iget-wide v4, p0, Lcom/android/launcher3/CropView;->Cb:J

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    int-to-long v12, v0

    add-long/2addr v4, v12

    cmp-long v0, v10, v4

    if-gez v0, :cond_7

    .line 259
    iget-object v0, p0, Lcom/android/launcher3/CropView;->Cn:Lti;

    invoke-interface {v0}, Lti;->fw()V

    .line 261
    :cond_7
    iget-object v0, p0, Lcom/android/launcher3/CropView;->Cn:Lti;

    invoke-interface {v0}, Lti;->dZ()V

    goto :goto_4

    .line 269
    :cond_8
    iget-object v4, p0, Lcom/android/launcher3/CropView;->dK:Ljava/lang/Object;

    monitor-enter v4

    .line 270
    :try_start_0
    iget-object v0, p0, Lcom/android/launcher3/CropView;->Ca:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 271
    packed-switch v9, :pswitch_data_0

    .line 283
    :goto_6
    iget-object v0, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget-object v0, v0, Laiu;->ZQ:Laiq;

    if-eqz v0, :cond_f

    .line 286
    iget-object v0, p0, Lcom/android/launcher3/CropView;->Ch:Landroid/graphics/RectF;

    .line 287
    invoke-direct {p0, v0}, Lcom/android/launcher3/CropView;->a(Landroid/graphics/RectF;)V

    .line 288
    iget-object v5, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget v5, v5, Laiu;->Ix:F

    .line 290
    iget-object v7, p0, Lcom/android/launcher3/CropView;->Cj:[F

    .line 291
    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    aput v10, v7, v9

    .line 292
    const/4 v9, 0x1

    const/high16 v10, 0x3f800000    # 1.0f

    aput v10, v7, v9

    .line 293
    iget-object v9, p0, Lcom/android/launcher3/CropView;->Co:Landroid/graphics/Matrix;

    invoke-virtual {v9, v7}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 294
    iget-object v9, p0, Lcom/android/launcher3/CropView;->Ck:[F

    .line 295
    iget-object v10, p0, Lcom/android/launcher3/CropView;->Ck:[F

    const/4 v11, 0x0

    const/4 v12, 0x0

    aput v12, v10, v11

    .line 296
    iget-object v10, p0, Lcom/android/launcher3/CropView;->Ck:[F

    const/4 v11, 0x1

    const/4 v12, 0x0

    aput v12, v10, v11

    .line 297
    iget v10, v0, Landroid/graphics/RectF;->left:F

    cmpl-float v10, v10, v6

    if-lez v10, :cond_c

    .line 298
    const/4 v10, 0x0

    iget v11, v0, Landroid/graphics/RectF;->left:F

    div-float/2addr v11, v5

    aput v11, v9, v10

    .line 302
    :cond_9
    :goto_7
    iget v10, v0, Landroid/graphics/RectF;->top:F

    cmpl-float v10, v10, v6

    if-lez v10, :cond_d

    .line 303
    const/4 v10, 0x1

    iget v0, v0, Landroid/graphics/RectF;->top:F

    div-float/2addr v0, v5

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    aput v0, v9, v10

    :cond_a
    :goto_8
    move v0, v3

    .line 307
    :goto_9
    if-gt v0, v8, :cond_e

    .line 308
    aget v3, v7, v0

    cmpl-float v3, v3, v6

    if-lez v3, :cond_b

    aget v3, v9, v0

    invoke-static {v3}, Landroid/util/FloatMath;->ceil(F)F

    move-result v3

    aput v3, v9, v0

    .line 307
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 273
    :pswitch_0
    iget-object v0, p0, Lcom/android/launcher3/CropView;->Ci:[F

    .line 274
    const/4 v5, 0x0

    iget v7, p0, Lcom/android/launcher3/CropView;->Ce:F

    sub-float/2addr v7, v2

    iget-object v9, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget v9, v9, Laiu;->Ix:F

    div-float/2addr v7, v9

    aput v7, v0, v5

    .line 275
    const/4 v5, 0x1

    iget v7, p0, Lcom/android/launcher3/CropView;->Cf:F

    sub-float/2addr v7, v1

    iget-object v9, p0, Lcom/android/launcher3/CropView;->ZN:Laiu;

    iget v9, v9, Laiu;->Ix:F

    div-float/2addr v7, v9

    aput v7, v0, v5

    .line 276
    iget-object v5, p0, Lcom/android/launcher3/CropView;->Cp:Landroid/graphics/Matrix;

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 277
    iget v5, p0, Lcom/android/launcher3/CropView;->sJ:F

    const/4 v7, 0x0

    aget v7, v0, v7

    add-float/2addr v5, v7

    iput v5, p0, Lcom/android/launcher3/CropView;->sJ:F

    .line 278
    iget v5, p0, Lcom/android/launcher3/CropView;->sK:F

    const/4 v7, 0x1

    aget v0, v0, v7

    add-float/2addr v0, v5

    iput v0, p0, Lcom/android/launcher3/CropView;->sK:F

    .line 279
    invoke-direct {p0}, Lcom/android/launcher3/CropView;->fu()V

    .line 280
    invoke-virtual {p0}, Lcom/android/launcher3/CropView;->invalidate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_6

    .line 316
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 299
    :cond_c
    :try_start_1
    iget v10, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {p0}, Lcom/android/launcher3/CropView;->getWidth()I

    move-result v11

    int-to-float v11, v11

    cmpg-float v10, v10, v11

    if-gez v10, :cond_9

    .line 300
    const/4 v10, 0x0

    iget v11, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {p0}, Lcom/android/launcher3/CropView;->getWidth()I

    move-result v12

    int-to-float v12, v12

    sub-float/2addr v11, v12

    div-float/2addr v11, v5

    aput v11, v9, v10

    goto :goto_7

    .line 304
    :cond_d
    iget v10, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p0}, Lcom/android/launcher3/CropView;->getHeight()I

    move-result v11

    int-to-float v11, v11

    cmpg-float v10, v10, v11

    if-gez v10, :cond_a

    .line 305
    const/4 v10, 0x1

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p0}, Lcom/android/launcher3/CropView;->getHeight()I

    move-result v11

    int-to-float v11, v11

    sub-float/2addr v0, v11

    div-float/2addr v0, v5

    aput v0, v9, v10

    goto :goto_8

    .line 311
    :cond_e
    iget-object v0, p0, Lcom/android/launcher3/CropView;->Cp:Landroid/graphics/Matrix;

    invoke-virtual {v0, v9}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 312
    iget v0, p0, Lcom/android/launcher3/CropView;->sJ:F

    const/4 v3, 0x0

    aget v3, v9, v3

    add-float/2addr v0, v3

    iput v0, p0, Lcom/android/launcher3/CropView;->sJ:F

    .line 313
    iget v0, p0, Lcom/android/launcher3/CropView;->sK:F

    const/4 v3, 0x1

    aget v3, v9, v3

    add-float/2addr v0, v3

    iput v0, p0, Lcom/android/launcher3/CropView;->sK:F

    .line 314
    invoke-direct {p0}, Lcom/android/launcher3/CropView;->fu()V

    .line 316
    :cond_f
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 318
    iput v2, p0, Lcom/android/launcher3/CropView;->Ce:F

    .line 319
    iput v1, p0, Lcom/android/launcher3/CropView;->Cf:F

    goto/16 :goto_5

    .line 271
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
