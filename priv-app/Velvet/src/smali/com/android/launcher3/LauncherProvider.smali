.class public Lcom/android/launcher3/LauncherProvider;
.super Landroid/content/ContentProvider;
.source "PG"


# static fields
.field public static final Og:Landroid/net/Uri;


# instance fields
.field private Of:Labi;

.field private Oh:Labd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    const-string v0, "content://com.google.android.launcher.settings/appWidgetReset"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/launcher3/LauncherProvider;->Og:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 2325
    return-void
.end method

.method public static synthetic a(Landroid/content/res/XmlResourceParser;Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 72
    const/4 v1, 0x0

    const-string v0, "http://schemas.android.com/apk/res-auto/com.android.launcher3"

    invoke-interface {p0, v0, p1, v1}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-interface {p0, v0, p1, v1}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    :cond_0
    return v0
.end method

.method private static a(Labd;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    .locals 2

    .prologue
    .line 155
    if-nez p4, :cond_0

    .line 156
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Error: attempting to insert null values"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 158
    :cond_0
    const-string v0, "_id"

    invoke-virtual {p4, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 159
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Error: attempting to add item without specifying an id"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 161
    :cond_1
    invoke-virtual {p0, p2, p4}, Labd;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 162
    invoke-virtual {p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static synthetic a(Ljava/lang/String;[I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 72
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    if-lez v0, :cond_0

    const-string v2, " OR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 269
    const-string v0, "modified"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 270
    return-void
.end method

.method public static synthetic a(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 72
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Labn;->a(JZ)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Labg;

    invoke-direct {v1, v0, v2, v2}, Labg;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    iget-object v0, v1, Labg;->On:Ljava/lang/String;

    iget-object v2, v1, Labg;->Oo:Ljava/lang/String;

    iget-object v1, v1, Labg;->Op:[Ljava/lang/String;

    invoke-virtual {p0, v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public static synthetic ag(Z)Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    return v0
.end method

.method public static synthetic b(Labd;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    .locals 2

    .prologue
    .line 72
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0, p4}, Lcom/android/launcher3/LauncherProvider;->a(Labd;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static synthetic b(Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0, p2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-void
.end method

.method public static synthetic c(Landroid/content/res/XmlResourceParser;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    const-string v0, "http://schemas.android.com/apk/res-auto/com.android.launcher3"

    invoke-interface {p0, v0, p1}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {p0, v0, p1}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private c(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 256
    const-string v0, "notify"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 257
    if-eqz v0, :cond_0

    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 258
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/LauncherProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 262
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher3/LauncherProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/launcher3/LauncherBackupAgentHelper;->o(Landroid/content/Context;)V

    .line 263
    iget-object v0, p0, Lcom/android/launcher3/LauncherProvider;->Of:Labi;

    if-eqz v0, :cond_2

    .line 264
    iget-object v0, p0, Lcom/android/launcher3/LauncherProvider;->Of:Labi;

    invoke-interface {v0}, Labi;->hb()V

    .line 266
    :cond_2
    return-void
.end method

.method public static synthetic v(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 72
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public final a(Labi;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/android/launcher3/LauncherProvider;->Of:Labi;

    .line 126
    return-void
.end method

.method public applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    invoke-virtual {v0}, Labd;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 222
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 224
    :try_start_0
    invoke-super {p0, p1}, Landroid/content/ContentProvider;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v0

    .line 225
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 197
    new-instance v2, Labg;

    invoke-direct {v2, p1}, Labg;-><init>(Landroid/net/Uri;)V

    .line 199
    iget-object v1, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    invoke-virtual {v1}, Labd;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 200
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 202
    :try_start_0
    array-length v4, p2

    move v1, v0

    .line 203
    :goto_0
    if-ge v1, v4, :cond_1

    .line 204
    aget-object v5, p2, v1

    invoke-static {v5}, Lcom/android/launcher3/LauncherProvider;->a(Landroid/content/ContentValues;)V

    .line 205
    iget-object v5, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    iget-object v6, v2, Labg;->On:Ljava/lang/String;

    const/4 v7, 0x0

    aget-object v8, p2, v1

    invoke-static {v5, v3, v6, v7, v8}, Lcom/android/launcher3/LauncherProvider;->a(Labd;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-gez v5, :cond_0

    .line 206
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 215
    :goto_1
    return v0

    .line 203
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 209
    :cond_1
    :try_start_1
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 214
    invoke-direct {p0, p1}, Lcom/android/launcher3/LauncherProvider;->c(Landroid/net/Uri;)V

    .line 215
    array-length v0, p2

    goto :goto_1

    .line 211
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 234
    new-instance v0, Labg;

    invoke-direct {v0, p1, p2, p3}, Labg;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 236
    iget-object v1, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    invoke-virtual {v1}, Labd;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 237
    iget-object v2, v0, Labg;->On:Ljava/lang/String;

    iget-object v3, v0, Labg;->Oo:Ljava/lang/String;

    iget-object v0, v0, Labg;->Op:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 238
    if-lez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/launcher3/LauncherProvider;->c(Landroid/net/Uri;)V

    .line 240
    :cond_0
    return v0
.end method

.method public final eE()J
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    invoke-virtual {v0}, Labd;->eE()J

    move-result-wide v0

    return-wide v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 130
    new-instance v0, Labg;

    invoke-direct {v0, p1, v1, v1}, Labg;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 131
    iget-object v1, v0, Labg;->Oo:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "vnd.android.cursor.dir/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Labg;->On:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 134
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "vnd.android.cursor.item/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Labg;->On:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final iL()J
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    invoke-virtual {v0}, Labd;->iL()J

    move-result-wide v0

    return-wide v0
.end method

.method public final declared-synchronized iM()V
    .locals 2

    .prologue
    .line 314
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    iget-object v1, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    invoke-virtual {v1}, Labd;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v0, v1}, Labd;->a(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 315
    monitor-exit p0

    return-void

    .line 314
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized iN()V
    .locals 6

    .prologue
    .line 324
    monitor-enter p0

    :try_start_0
    const-string v0, "com.android.launcher3.prefs"

    .line 325
    invoke-virtual {p0}, Lcom/android/launcher3/LauncherProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 327
    const-string v0, "EMPTY_DATABASE_CREATED"

    const/4 v1, 0x0

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 328
    invoke-virtual {p0}, Lcom/android/launcher3/LauncherProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    invoke-static {v1}, Labd;->a(Labd;)Landroid/appwidget/AppWidgetHost;

    move-result-object v1

    iget-object v3, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    invoke-static {v0, v1, v3}, Lsi;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetHost;Lsn;)Lsi;

    move-result-object v0

    .line 333
    if-nez v0, :cond_0

    .line 334
    invoke-virtual {p0}, Lcom/android/launcher3/LauncherProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v1}, Lacv;->a(Landroid/content/pm/PackageManager;)Lacv;

    move-result-object v1

    .line 335
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lacv;->jR()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 336
    invoke-virtual {v1}, Lacv;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 337
    const-string v4, "partner_default_layout"

    const-string v5, "xml"

    invoke-virtual {v1}, Lacv;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v5, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 339
    if-eqz v1, :cond_0

    .line 340
    new-instance v0, Labf;

    iget-object v4, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    invoke-direct {v0, v4, v3, v1}, Labf;-><init>(Labd;Landroid/content/res/Resources;I)V

    .line 345
    :cond_0
    if-nez v0, :cond_1

    .line 346
    new-instance v0, Labf;

    iget-object v3, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    invoke-virtual {p0}, Lcom/android/launcher3/LauncherProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v1

    iget-object v1, v1, Lyu;->Mk:Lur;

    invoke-virtual {v1}, Lur;->gl()Ltu;

    move-result-object v1

    invoke-static {}, Lyu;->im()Z

    move-result v5

    if-eqz v5, :cond_3

    iget v1, v1, Ltu;->Do:I

    :goto_0
    invoke-direct {v0, v3, v4, v1}, Labf;-><init>(Labd;Landroid/content/res/Resources;I)V

    .line 351
    :cond_1
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "EMPTY_DATABASE_CREATED"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 352
    iget-object v2, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    iget-object v3, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    invoke-virtual {v3}, Labd;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    invoke-static {v2, v3, v0}, Labd;->a(Labd;Landroid/database/sqlite/SQLiteDatabase;Labh;)I

    .line 353
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 355
    :cond_2
    monitor-exit p0

    return-void

    .line 346
    :cond_3
    :try_start_1
    iget v1, v1, Ltu;->Dn:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 324
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final iO()V
    .locals 4

    .prologue
    .line 358
    iget-object v0, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    iget-object v1, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    invoke-virtual {v1}, Labd;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/launcher3/LauncherProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a007e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v1, v2}, Labd;->a(Labd;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;)V

    .line 360
    return-void
.end method

.method public final iP()V
    .locals 2

    .prologue
    .line 385
    iget-object v0, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    invoke-virtual {v0}, Labd;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 386
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 387
    iget-object v0, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    invoke-virtual {v0}, Labd;->close()V

    .line 388
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389
    invoke-static {v1}, Landroid/database/sqlite/SQLiteDatabase;->deleteDatabase(Ljava/io/File;)Z

    .line 391
    :cond_0
    new-instance v0, Labd;

    invoke-virtual {p0}, Lcom/android/launcher3/LauncherProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Labd;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    .line 392
    return-void
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 173
    new-instance v1, Labg;

    invoke-direct {v1, p1}, Labg;-><init>(Landroid/net/Uri;)V

    .line 176
    const-string v2, "isExternalAdd"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 177
    if-eqz v2, :cond_1

    const-string v3, "true"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 178
    iget-object v2, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    invoke-static {v2, p2}, Labd;->a(Labd;Landroid/content/ContentValues;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 191
    :cond_0
    :goto_0
    return-object v0

    .line 183
    :cond_1
    iget-object v2, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    invoke-virtual {v2}, Labd;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 184
    invoke-static {p2}, Lcom/android/launcher3/LauncherProvider;->a(Landroid/content/ContentValues;)V

    .line 185
    iget-object v3, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    iget-object v1, v1, Labg;->On:Ljava/lang/String;

    invoke-static {v3, v2, v1, v0, p2}, Lcom/android/launcher3/LauncherProvider;->a(Labd;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 186
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 188
    invoke-static {p1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 189
    invoke-direct {p0, v0}, Lcom/android/launcher3/LauncherProvider;->c(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/android/launcher3/LauncherProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 115
    new-instance v1, Labd;

    invoke-direct {v1, v0}, Labd;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    .line 116
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lyu;->Mh:Ljava/lang/ref/WeakReference;

    .line 117
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 142
    new-instance v2, Labg;

    invoke-direct {v2, p1, p3, p4}, Labg;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 143
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 144
    iget-object v1, v2, Labg;->On:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 146
    iget-object v1, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    invoke-virtual {v1}, Labd;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 147
    iget-object v3, v2, Labg;->Oo:Ljava/lang/String;

    iget-object v4, v2, Labg;->Op:[Ljava/lang/String;

    move-object v2, p2

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 148
    invoke-virtual {p0}, Lcom/android/launcher3/LauncherProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 150
    return-object v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 245
    new-instance v0, Labg;

    invoke-direct {v0, p1, p3, p4}, Labg;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 247
    invoke-static {p2}, Lcom/android/launcher3/LauncherProvider;->a(Landroid/content/ContentValues;)V

    .line 248
    iget-object v1, p0, Lcom/android/launcher3/LauncherProvider;->Oh:Labd;

    invoke-virtual {v1}, Labd;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 249
    iget-object v2, v0, Labg;->On:Ljava/lang/String;

    iget-object v3, v0, Labg;->Oo:Ljava/lang/String;

    iget-object v0, v0, Labg;->Op:[Ljava/lang/String;

    invoke-virtual {v1, v2, p2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 250
    if-lez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/launcher3/LauncherProvider;->c(Landroid/net/Uri;)V

    .line 252
    :cond_0
    return v0
.end method
