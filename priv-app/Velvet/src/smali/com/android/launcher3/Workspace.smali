.class public Lcom/android/launcher3/Workspace;
.super Ladi;
.source "PG"

# interfaces
.implements Labp;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;
.implements Ltz;
.implements Luh;
.implements Lui;
.implements Luo;
.implements Lwm;


# static fields
.field private static final IK:Landroid/graphics/Rect;

.field private static VE:Landroid/graphics/Rect;

.field private static VF:Landroid/graphics/Rect;

.field private static Vw:Z


# instance fields
.field private AL:Lup;

.field private Cb:J

.field private EC:Landroid/os/IBinder;

.field private Fg:Z

.field private He:[I

.field private final Hh:Lro;

.field private Hw:Ljava/lang/Runnable;

.field private Hx:Z

.field private Hy:Z

.field private final IL:Landroid/graphics/Canvas;

.field private QE:[I

.field private final SQ:Landroid/app/WallpaperManager;

.field private VA:Z

.field private VB:Lsy;

.field private VC:I

.field private VD:I

.field private VG:Lym;

.field private VH:Z

.field private VI:F

.field private VJ:Ljava/lang/String;

.field private VK:Lcom/android/launcher3/CellLayout;

.field private VL:Lcom/android/launcher3/CellLayout;

.field private VM:Lcom/android/launcher3/CellLayout;

.field private VN:[I

.field private VO:[I

.field private VP:[F

.field private VQ:[F

.field private VR:Landroid/graphics/Matrix;

.field private VS:Ladk;

.field private VT:F

.field private VU:F

.field private VV:Lagv;

.field private VW:Z

.field public VX:Z

.field public VY:Z

.field private VZ:Z

.field private Vn:Landroid/animation/ObjectAnimator;

.field private Vo:Landroid/animation/ObjectAnimator;

.field private Vp:F

.field private Vq:Landroid/animation/ValueAnimator;

.field private Vr:J

.field private Vs:Landroid/animation/LayoutTransition;

.field private Vt:I

.field private Vu:I

.field private Vv:Ladg;

.field private Vx:Ljava/util/HashMap;

.field private Vy:Ljava/util/ArrayList;

.field private Vz:Ljava/lang/Runnable;

.field private WA:F

.field private WB:F

.field private WC:F

.field private WD:F

.field private WE:[F

.field private WF:[F

.field private WG:[F

.field private WH:[F

.field private WI:I

.field private WJ:F

.field private WK:F

.field private final WL:Ljava/lang/Runnable;

.field private final WM:Lagy;

.field private Wa:Z

.field private Wb:Landroid/graphics/Bitmap;

.field private final Wc:[I

.field private Wd:Z

.field private We:Z

.field public Wf:Lagw;

.field private Wg:Z

.field private Wh:I

.field private Wi:F

.field private Wj:Ljava/lang/Runnable;

.field private Wk:Ljava/lang/Runnable;

.field private Wl:Landroid/graphics/Point;

.field private final Wm:Lro;

.field private Wn:Lvs;

.field private Wo:Lcom/android/launcher3/FolderIcon;

.field private Wp:Z

.field private Wq:Z

.field private Wr:F

.field private Ws:F

.field private Wt:F

.field private Wu:I

.field private Wv:I

.field private Ww:I

.field private Wx:Landroid/util/SparseArray;

.field private final Wy:Ljava/util/ArrayList;

.field private Wz:I

.field private xZ:Lcom/android/launcher3/Launcher;

.field private xn:Lwi;

.field private yg:Lty;

.field private zr:Lwf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 158
    sput-object v0, Lcom/android/launcher3/Workspace;->VE:Landroid/graphics/Rect;

    .line 159
    sput-object v0, Lcom/android/launcher3/Workspace;->VF:Landroid/graphics/Rect;

    .line 215
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/android/launcher3/Workspace;->IK:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 306
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher3/Workspace;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 307
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10

    .prologue
    const/4 v6, -0x1

    const/4 v3, 0x0

    const/4 v9, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 317
    invoke-direct {p0, p1, p2, p3}, Ladi;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 116
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher3/Workspace;->Vp:F

    .line 123
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/android/launcher3/Workspace;->Cb:J

    .line 124
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/android/launcher3/Workspace;->Vr:J

    .line 140
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    .line 141
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    .line 144
    iput-boolean v1, p0, Lcom/android/launcher3/Workspace;->VA:Z

    .line 154
    new-array v0, v9, [I

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->He:[I

    .line 155
    iput v6, p0, Lcom/android/launcher3/Workspace;->VC:I

    .line 156
    iput v6, p0, Lcom/android/launcher3/Workspace;->VD:I

    .line 163
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/android/launcher3/Workspace;->VI:F

    .line 164
    const-string v0, ""

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->VJ:Ljava/lang/String;

    .line 169
    iput-object v3, p0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    .line 173
    iput-object v3, p0, Lcom/android/launcher3/Workspace;->VL:Lcom/android/launcher3/CellLayout;

    .line 178
    iput-object v3, p0, Lcom/android/launcher3/Workspace;->VM:Lcom/android/launcher3/CellLayout;

    .line 186
    new-array v0, v9, [I

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->VN:[I

    .line 187
    new-array v0, v9, [I

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->VO:[I

    .line 188
    new-array v0, v9, [F

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->VP:[F

    .line 190
    new-array v0, v9, [F

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->VQ:[F

    .line 191
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->VR:Landroid/graphics/Matrix;

    .line 201
    sget-object v0, Lagv;->Xv:Lagv;

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->VV:Lagv;

    .line 202
    iput-boolean v1, p0, Lcom/android/launcher3/Workspace;->VW:Z

    .line 204
    iput-boolean v1, p0, Lcom/android/launcher3/Workspace;->VX:Z

    .line 205
    iput-boolean v1, p0, Lcom/android/launcher3/Workspace;->VY:Z

    .line 206
    iput-boolean v2, p0, Lcom/android/launcher3/Workspace;->VZ:Z

    .line 208
    iput-boolean v1, p0, Lcom/android/launcher3/Workspace;->Wa:Z

    .line 211
    iput-boolean v1, p0, Lcom/android/launcher3/Workspace;->Fg:Z

    .line 214
    iput-object v3, p0, Lcom/android/launcher3/Workspace;->Wb:Landroid/graphics/Bitmap;

    .line 216
    new-array v0, v9, [I

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Wc:[I

    .line 217
    new-array v0, v9, [I

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->QE:[I

    .line 225
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher3/Workspace;->Wi:F

    .line 229
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Wl:Landroid/graphics/Point;

    .line 235
    new-instance v0, Lro;

    invoke-direct {v0}, Lro;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Wm:Lro;

    .line 236
    new-instance v0, Lro;

    invoke-direct {v0}, Lro;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Hh:Lro;

    .line 237
    iput-object v3, p0, Lcom/android/launcher3/Workspace;->Wn:Lvs;

    .line 238
    iput-object v3, p0, Lcom/android/launcher3/Workspace;->Wo:Lcom/android/launcher3/FolderIcon;

    .line 239
    iput-boolean v1, p0, Lcom/android/launcher3/Workspace;->Wp:Z

    .line 240
    iput-boolean v1, p0, Lcom/android/launcher3/Workspace;->Wq:Z

    .line 244
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    .line 265
    iput v1, p0, Lcom/android/launcher3/Workspace;->Wu:I

    .line 266
    iput v6, p0, Lcom/android/launcher3/Workspace;->Wv:I

    .line 267
    iput v6, p0, Lcom/android/launcher3/Workspace;->Ww:I

    .line 270
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Wy:Ljava/util/ArrayList;

    .line 283
    iput v6, p0, Lcom/android/launcher3/Workspace;->WI:I

    .line 286
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher3/Workspace;->WK:F

    .line 292
    new-instance v0, Lafn;

    invoke-direct {v0, p0}, Lafn;-><init>(Lcom/android/launcher3/Workspace;)V

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->WL:Ljava/lang/Runnable;

    .line 1975
    new-instance v0, Lagy;

    invoke-direct {v0}, Lagy;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->WM:Lagy;

    .line 318
    iput-boolean v1, p0, Lcom/android/launcher3/Workspace;->QK:Z

    .line 320
    invoke-static {p1}, Lwf;->k(Landroid/content/Context;)Lwf;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->zr:Lwf;

    .line 322
    new-instance v0, Lup;

    invoke-direct {v0, p1}, Lup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->AL:Lup;

    .line 324
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jd()V

    move-object v0, p1

    .line 326
    check-cast v0, Lcom/android/launcher3/Launcher;

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    .line 327
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 328
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    invoke-virtual {v0}, Ltu;->fC()Z

    move-result v4

    if-nez v4, :cond_0

    iget-boolean v0, v0, Ltu;->Dr:Z

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/android/launcher3/Workspace;->We:Z

    .line 330
    iput-boolean v1, p0, Lcom/android/launcher3/Workspace;->QL:Z

    .line 331
    invoke-static {p1}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->SQ:Landroid/app/WallpaperManager;

    .line 333
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 334
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    .line 335
    sget-object v4, Ladb;->Sz:[I

    invoke-virtual {p1, p2, v4, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 337
    const v5, 0x7f0c000f

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x42c80000    # 100.0f

    div-float/2addr v5, v6

    iput v5, p0, Lcom/android/launcher3/Workspace;->VT:F

    .line 339
    invoke-virtual {v0}, Ltu;->fA()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v0}, Ltu;->fB()Landroid/graphics/Rect;

    move-result-object v6

    iget v7, v0, Ltu;->DA:I

    iget v8, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v8

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    sub-int v5, v7, v5

    iget v0, v0, Ltu;->DH:F

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    sub-int v6, v5, v6

    int-to-float v6, v6

    mul-float/2addr v0, v6

    int-to-float v5, v5

    div-float/2addr v0, v5

    iput v0, p0, Lcom/android/launcher3/Workspace;->VU:F

    .line 340
    const v0, 0x7f0c0028

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    .line 341
    invoke-virtual {v4, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/Workspace;->Vu:I

    iput v0, p0, Lcom/android/launcher3/Workspace;->Vt:I

    .line 342
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 344
    invoke-virtual {p0, p0}, Lcom/android/launcher3/Workspace;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 345
    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->setHapticFeedbackEnabled(Z)V

    .line 347
    iget v0, p0, Lcom/android/launcher3/Workspace;->Vu:I

    iput v0, p0, Lcom/android/launcher3/Workspace;->Qc:I

    iget v0, p0, Lcom/android/launcher3/Workspace;->Qc:I

    invoke-static {v0}, Lcom/android/launcher3/Launcher;->bk(I)V

    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    iget-object v3, v0, Lyu;->Mk:Lur;

    invoke-virtual {v3}, Lur;->gl()Ltu;

    move-result-object v3

    iget-object v0, v0, Lyu;->xn:Lwi;

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->xn:Lwi;

    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->setWillNotDraw(Z)V

    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->setClipChildren(Z)V

    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->setClipToPadding(Z)V

    invoke-virtual {p0, v2}, Lcom/android/launcher3/Workspace;->setChildrenDrawnWithCacheEnabled(Z)V

    iget v0, p0, Lcom/android/launcher3/Workspace;->VU:F

    iput v0, p0, Lcom/android/launcher3/PagedView;->ss:F

    iput-boolean v2, p0, Lcom/android/launcher3/PagedView;->QZ:Z

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->requestLayout()V

    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Vs:Landroid/animation/LayoutTransition;

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vs:Landroid/animation/LayoutTransition;

    const/4 v4, 0x3

    invoke-virtual {v0, v4}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v2}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v9}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    new-instance v0, Lagw;

    invoke-direct {v0, p0}, Lagw;-><init>(Lcom/android/launcher3/Workspace;)V

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Wf:Lagw;

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Wl:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    const v0, 0x3f0ccccd    # 0.55f

    iget v1, v3, Ltu;->DI:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/android/launcher3/Workspace;->Wr:F

    const/high16 v0, 0x43fa0000    # 500.0f

    iget v1, p0, Lcom/android/launcher3/Workspace;->PY:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/launcher3/Workspace;->PV:I

    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->kZ()V

    .line 350
    invoke-virtual {p0, v2}, Lcom/android/launcher3/Workspace;->setMotionEventSplittingEnabled(Z)V

    .line 351
    invoke-virtual {p0, v2}, Lcom/android/launcher3/Workspace;->setImportantForAccessibility(I)V

    .line 352
    return-void

    :cond_1
    move v0, v1

    .line 328
    goto/16 :goto_0
.end method

.method private S(II)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 1817
    if-le p1, p2, :cond_1

    .line 1823
    :goto_0
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v1

    .line 1825
    const/4 v0, 0x0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1826
    add-int/lit8 v1, v1, -0x1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v1, v0

    .line 1828
    :goto_1
    if-gt v1, v2, :cond_0

    .line 1829
    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 1830
    invoke-virtual {v0, v3}, Lcom/android/launcher3/CellLayout;->setChildrenDrawnWithCacheEnabled(Z)V

    .line 1831
    invoke-virtual {v0, v3}, Lcom/android/launcher3/CellLayout;->setChildrenDrawingCacheEnabled(Z)V

    .line 1828
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1833
    :cond_0
    return-void

    :cond_1
    move v4, p2

    move p2, p1

    move p1, v4

    goto :goto_0
.end method

.method private T(II)V
    .locals 1

    .prologue
    .line 3337
    iget v0, p0, Lcom/android/launcher3/Workspace;->VC:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/android/launcher3/Workspace;->VD:I

    if-eq p2, v0, :cond_1

    .line 3338
    :cond_0
    iput p1, p0, Lcom/android/launcher3/Workspace;->VC:I

    .line 3339
    iput p2, p0, Lcom/android/launcher3/Workspace;->VD:I

    .line 3340
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->bP(I)V

    .line 3342
    :cond_1
    return-void
.end method

.method public static synthetic a(Lcom/android/launcher3/Workspace;F)F
    .locals 0

    .prologue
    .line 88
    iput p1, p0, Lcom/android/launcher3/Workspace;->Wi:F

    return p1
.end method

.method public static synthetic a(Lcom/android/launcher3/Workspace;I)I
    .locals 0

    .prologue
    .line 88
    iput p1, p0, Lcom/android/launcher3/Workspace;->Wh:I

    return p1
.end method

.method private a(JI)J
    .locals 3

    .prologue
    .line 567
    const-string v0, "Launcher.Workspace"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "11683562 - insertNewWorkspaceScreen(): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 570
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 571
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Screen id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " already exists!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 574
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401cd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 577
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Qx:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CellLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 578
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CellLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 579
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CellLayout;->setSoundEffectsEnabled(Z)V

    .line 580
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 581
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, p3, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 582
    invoke-virtual {p0, v0, p3}, Lcom/android/launcher3/Workspace;->addView(Landroid/view/View;I)V

    .line 583
    return-wide p1
.end method

.method private a(Lagv;ZIILjava/util/ArrayList;)Landroid/animation/Animator;
    .locals 22

    .prologue
    .line 2194
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VV:Lagv;

    move-object/from16 v0, p1

    if-ne v2, v0, :cond_0

    .line 2195
    const/4 v3, 0x0

    .line 2420
    :goto_0
    return-object v3

    .line 2199
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/launcher3/Workspace;->WI:I

    if-eq v3, v2, :cond_1

    new-array v3, v2, [F

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher3/Workspace;->WE:[F

    new-array v3, v2, [F

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher3/Workspace;->WF:[F

    new-array v3, v2, [F

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/launcher3/Workspace;->WG:[F

    new-array v2, v2, [F

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/launcher3/Workspace;->WH:[F

    .line 2201
    :cond_1
    if-eqz p2, :cond_d

    invoke-static {}, Lyr;->ii()Landroid/animation/AnimatorSet;

    move-result-object v2

    move-object v3, v2

    .line 2203
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->VV:Lagv;

    .line 2204
    sget-object v2, Lagv;->Xv:Lagv;

    if-ne v4, v2, :cond_e

    const/4 v2, 0x1

    .line 2205
    :goto_2
    sget-object v5, Lagv;->Xx:Lagv;

    .line 2206
    sget-object v5, Lagv;->Xw:Lagv;

    .line 2207
    sget-object v5, Lagv;->Xz:Lagv;

    .line 2208
    sget-object v5, Lagv;->Xy:Lagv;

    if-ne v4, v5, :cond_f

    const/4 v4, 0x1

    .line 2209
    :goto_3
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/launcher3/Workspace;->VV:Lagv;

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->ll()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->VV:Lagv;

    sget-object v6, Lagv;->Xv:Lagv;

    if-ne v5, v6, :cond_10

    const/4 v5, 0x1

    :goto_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/launcher3/Workspace;->setImportantForAccessibility(I)V

    .line 2210
    sget-object v5, Lagv;->Xv:Lagv;

    move-object/from16 v0, p1

    if-ne v0, v5, :cond_11

    const/4 v5, 0x1

    .line 2211
    :goto_5
    sget-object v6, Lagv;->Xx:Lagv;

    move-object/from16 v0, p1

    if-ne v0, v6, :cond_12

    const/4 v6, 0x1

    .line 2212
    :goto_6
    sget-object v7, Lagv;->Xw:Lagv;

    move-object/from16 v0, p1

    if-ne v0, v7, :cond_13

    const/4 v7, 0x1

    .line 2213
    :goto_7
    sget-object v8, Lagv;->Xz:Lagv;

    move-object/from16 v0, p1

    if-ne v0, v8, :cond_14

    const/4 v8, 0x1

    .line 2214
    :goto_8
    sget-object v9, Lagv;->Xy:Lagv;

    move-object/from16 v0, p1

    if-ne v0, v9, :cond_15

    const/4 v9, 0x1

    move v11, v9

    .line 2215
    :goto_9
    if-nez v6, :cond_2

    if-eqz v11, :cond_16

    :cond_2
    const/high16 v9, 0x3f800000    # 1.0f

    move/from16 v20, v9

    .line 2216
    :goto_a
    if-nez v5, :cond_3

    if-eqz v6, :cond_17

    :cond_3
    const/high16 v9, 0x3f800000    # 1.0f

    move/from16 v19, v9

    .line 2217
    :goto_b
    if-eqz v11, :cond_18

    const/high16 v9, 0x3f800000    # 1.0f

    move/from16 v18, v9

    .line 2218
    :goto_c
    if-nez v5, :cond_19

    const/4 v9, 0x0

    move/from16 v17, v9

    .line 2219
    :goto_d
    if-nez v11, :cond_4

    if-eqz v8, :cond_1a

    :cond_4
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v9

    iget-object v9, v9, Lyu;->Mk:Lur;

    invoke-virtual {v9}, Lur;->gl()Ltu;

    move-result-object v9

    invoke-virtual {v9}, Ltu;->fB()Landroid/graphics/Rect;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v10

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/launcher3/Workspace;->VU:F

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/launcher3/PagedView;->Qb:I

    int-to-float v13, v13

    mul-float/2addr v12, v13

    float-to-int v12, v12

    sub-int v13, v10, v12

    div-int/lit8 v13, v13, 0x2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/launcher3/Workspace;->yW:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->top:I

    sub-int/2addr v10, v14

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    sub-int v9, v10, v9

    sub-int/2addr v9, v12

    div-int/lit8 v9, v9, 0x2

    neg-int v10, v13

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher3/Workspace;->yW:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->top:I

    add-int/2addr v10, v12

    add-int/2addr v9, v10

    int-to-float v9, v9

    move/from16 v16, v9

    .line 2222
    :goto_e
    if-eqz v2, :cond_1b

    if-eqz v7, :cond_1b

    const/4 v9, 0x1

    move v15, v9

    .line 2223
    :goto_f
    if-eqz v4, :cond_1c

    if-eqz v8, :cond_1c

    const/4 v9, 0x1

    move v10, v9

    .line 2224
    :goto_10
    if-eqz v7, :cond_1d

    if-eqz v5, :cond_1d

    const/4 v9, 0x1

    move v14, v9

    .line 2225
    :goto_11
    if-eqz v2, :cond_1e

    if-eqz v11, :cond_1e

    const/4 v2, 0x1

    move v13, v2

    .line 2226
    :goto_12
    if-eqz v4, :cond_1f

    if-eqz v5, :cond_1f

    const/4 v2, 0x1

    move v9, v2

    .line 2228
    :goto_13
    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/launcher3/Workspace;->WD:F

    .line 2230
    if-eqz v4, :cond_20

    .line 2231
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->jw()V

    .line 2236
    :cond_5
    :goto_14
    sget-object v2, Lagv;->Xv:Lagv;

    move-object/from16 v0, p1

    if-eq v0, v2, :cond_6

    .line 2237
    if-eqz v6, :cond_21

    .line 2238
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher3/Workspace;->VT:F

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/launcher3/Workspace;->WD:F

    .line 2245
    :cond_6
    :goto_15
    if-nez v15, :cond_7

    if-eqz v10, :cond_23

    .line 2246
    :cond_7
    const/16 v2, 0x64

    move v12, v2

    .line 2253
    :goto_16
    const/4 v2, -0x1

    move/from16 v0, p4

    if-ne v0, v2, :cond_8

    .line 2254
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->jC()I

    move-result p4

    .line 2256
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->WM:Lagy;

    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-virtual {v0, v1, v12, v2}, Lcom/android/launcher3/Workspace;->a(IILandroid/animation/TimeInterpolator;)V

    .line 2258
    const/4 v2, 0x0

    move v10, v2

    :goto_17
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v2

    if-ge v10, v2, :cond_2d

    .line 2259
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/launcher3/CellLayout;

    .line 2260
    move/from16 v0, p4

    if-ne v10, v0, :cond_26

    const/4 v4, 0x1

    move v11, v4

    .line 2261
    :goto_18
    invoke-virtual {v2}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v4

    invoke-virtual {v4}, Ladg;->getAlpha()F

    move-result v6

    .line 2263
    if-nez v7, :cond_9

    if-eqz v8, :cond_27

    .line 2264
    :cond_9
    const/4 v4, 0x0

    .line 2273
    :goto_19
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/launcher3/Workspace;->VW:Z

    move/from16 v21, v0

    if-nez v21, :cond_c

    .line 2274
    if-nez v15, :cond_a

    if-eqz v14, :cond_c

    .line 2275
    :cond_a
    if-eqz v14, :cond_2b

    if-eqz v11, :cond_2b

    .line 2276
    const/4 v6, 0x0

    .line 2280
    :cond_b
    :goto_1a
    invoke-virtual {v2, v6}, Lcom/android/launcher3/CellLayout;->j(F)V

    .line 2284
    :cond_c
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/launcher3/Workspace;->WF:[F

    aput v6, v11, v10

    .line 2285
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Workspace;->WH:[F

    aput v4, v6, v10

    .line 2286
    if-eqz p2, :cond_2c

    .line 2287
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->WE:[F

    invoke-virtual {v2}, Lcom/android/launcher3/CellLayout;->getBackgroundAlpha()F

    move-result v2

    aput v2, v4, v10

    .line 2288
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->WG:[F

    aput v20, v2, v10

    .line 2258
    :goto_1b
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_17

    .line 2201
    :cond_d
    const/4 v2, 0x0

    move-object v3, v2

    goto/16 :goto_1

    .line 2204
    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 2208
    :cond_f
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 2209
    :cond_10
    const/4 v5, 0x4

    goto/16 :goto_4

    .line 2210
    :cond_11
    const/4 v5, 0x0

    goto/16 :goto_5

    .line 2211
    :cond_12
    const/4 v6, 0x0

    goto/16 :goto_6

    .line 2212
    :cond_13
    const/4 v7, 0x0

    goto/16 :goto_7

    .line 2213
    :cond_14
    const/4 v8, 0x0

    goto/16 :goto_8

    .line 2214
    :cond_15
    const/4 v9, 0x0

    move v11, v9

    goto/16 :goto_9

    .line 2215
    :cond_16
    const/4 v9, 0x0

    move/from16 v20, v9

    goto/16 :goto_a

    .line 2216
    :cond_17
    const/4 v9, 0x0

    move/from16 v19, v9

    goto/16 :goto_b

    .line 2217
    :cond_18
    const/4 v9, 0x0

    move/from16 v18, v9

    goto/16 :goto_c

    .line 2218
    :cond_19
    const/high16 v9, 0x3f800000    # 1.0f

    move/from16 v17, v9

    goto/16 :goto_d

    .line 2219
    :cond_1a
    const/4 v9, 0x0

    move/from16 v16, v9

    goto/16 :goto_e

    .line 2222
    :cond_1b
    const/4 v9, 0x0

    move v15, v9

    goto/16 :goto_f

    .line 2223
    :cond_1c
    const/4 v9, 0x0

    move v10, v9

    goto/16 :goto_10

    .line 2224
    :cond_1d
    const/4 v9, 0x0

    move v14, v9

    goto/16 :goto_11

    .line 2225
    :cond_1e
    const/4 v2, 0x0

    move v13, v2

    goto/16 :goto_12

    .line 2226
    :cond_1f
    const/4 v2, 0x0

    move v9, v2

    goto/16 :goto_13

    .line 2232
    :cond_20
    if-eqz v11, :cond_5

    .line 2233
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->jv()V

    goto/16 :goto_14

    .line 2239
    :cond_21
    if-nez v11, :cond_22

    if-eqz v8, :cond_6

    .line 2240
    :cond_22
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher3/Workspace;->VU:F

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/launcher3/Workspace;->WD:F

    goto/16 :goto_15

    .line 2247
    :cond_23
    if-nez v13, :cond_24

    if-eqz v9, :cond_25

    .line 2248
    :cond_24
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0c000e

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    move v12, v2

    goto/16 :goto_16

    .line 2250
    :cond_25
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0c0016

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    move v12, v2

    goto/16 :goto_16

    .line 2260
    :cond_26
    const/4 v4, 0x0

    move v11, v4

    goto/16 :goto_18

    .line 2265
    :cond_27
    if-eqz v5, :cond_2a

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/launcher3/Workspace;->We:Z

    if-eqz v4, :cond_2a

    .line 2266
    move/from16 v0, p4

    if-eq v10, v0, :cond_28

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->ld()I

    move-result v4

    if-ge v10, v4, :cond_29

    :cond_28
    const/high16 v4, 0x3f800000    # 1.0f

    goto/16 :goto_19

    :cond_29
    const/4 v4, 0x0

    goto/16 :goto_19

    .line 2268
    :cond_2a
    const/high16 v4, 0x3f800000    # 1.0f

    goto/16 :goto_19

    .line 2277
    :cond_2b
    if-nez v11, :cond_b

    .line 2278
    const/4 v4, 0x0

    move v6, v4

    goto/16 :goto_1a

    .line 2290
    :cond_2c
    move/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/android/launcher3/CellLayout;->setBackgroundAlpha(F)V

    .line 2291
    invoke-virtual {v2, v4}, Lcom/android/launcher3/CellLayout;->j(F)V

    goto/16 :goto_1b

    .line 2295
    :cond_2d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher3/Launcher;->hM()Landroid/view/View;

    move-result-object v6

    .line 2296
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher3/Launcher;->hq()Landroid/view/ViewGroup;

    move-result-object v7

    .line 2297
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v8

    .line 2298
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    .line 2299
    if-eqz p2, :cond_39

    .line 2300
    new-instance v2, Labq;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Labq;-><init>(Landroid/view/View;)V

    .line 2301
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/launcher3/Workspace;->WD:F

    invoke-virtual {v2, v4}, Labq;->r(F)Labq;

    move-result-object v4

    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/launcher3/Workspace;->WD:F

    invoke-virtual {v4, v11}, Labq;->s(F)Labq;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Labq;->q(F)Labq;

    move-result-object v4

    int-to-long v14, v12

    invoke-virtual {v4, v14, v15}, Labq;->setDuration(J)Landroid/animation/Animator;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/launcher3/Workspace;->WM:Lagy;

    invoke-virtual {v4, v11}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2306
    invoke-virtual {v3, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 2307
    const/4 v2, 0x0

    move v4, v2

    :goto_1c
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v2

    if-ge v4, v2, :cond_34

    .line 2309
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/launcher3/CellLayout;

    .line 2310
    invoke-virtual {v2}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v11

    invoke-virtual {v11}, Ladg;->getAlpha()F

    move-result v11

    .line 2311
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/launcher3/Workspace;->WF:[F

    aget v14, v14, v4

    const/4 v15, 0x0

    cmpl-float v14, v14, v15

    if-nez v14, :cond_2f

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/launcher3/Workspace;->WH:[F

    aget v14, v14, v4

    const/4 v15, 0x0

    cmpl-float v14, v14, v15

    if-nez v14, :cond_2f

    .line 2312
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/launcher3/Workspace;->WG:[F

    aget v11, v11, v4

    invoke-virtual {v2, v11}, Lcom/android/launcher3/CellLayout;->setBackgroundAlpha(F)V

    .line 2313
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/launcher3/Workspace;->WH:[F

    aget v11, v11, v4

    invoke-virtual {v2, v11}, Lcom/android/launcher3/CellLayout;->j(F)V

    .line 2307
    :cond_2e
    :goto_1d
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1c

    .line 2315
    :cond_2f
    if-eqz p5, :cond_30

    .line 2316
    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2318
    :cond_30
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/launcher3/Workspace;->WF:[F

    aget v14, v14, v4

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/launcher3/Workspace;->WH:[F

    aget v15, v15, v4

    cmpl-float v14, v14, v15

    if-nez v14, :cond_31

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/launcher3/Workspace;->WH:[F

    aget v14, v14, v4

    cmpl-float v11, v11, v14

    if-eqz v11, :cond_32

    .line 2319
    :cond_31
    new-instance v11, Labq;

    invoke-virtual {v2}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v14

    invoke-direct {v11, v14}, Labq;-><init>(Landroid/view/View;)V

    .line 2321
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/launcher3/Workspace;->WH:[F

    aget v14, v14, v4

    invoke-virtual {v11, v14}, Labq;->t(F)Labq;

    move-result-object v14

    int-to-long v0, v12

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v14, v0, v1}, Labq;->setDuration(J)Landroid/animation/Animator;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/launcher3/Workspace;->WM:Lagy;

    invoke-virtual {v14, v15}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2324
    invoke-virtual {v3, v11}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 2326
    :cond_32
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/launcher3/Workspace;->WE:[F

    aget v11, v11, v4

    const/4 v14, 0x0

    cmpl-float v11, v11, v14

    if-nez v11, :cond_33

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/launcher3/Workspace;->WG:[F

    aget v11, v11, v4

    const/4 v14, 0x0

    cmpl-float v11, v11, v14

    if-eqz v11, :cond_2e

    .line 2328
    :cond_33
    const/4 v11, 0x2

    new-array v11, v11, [F

    fill-array-data v11, :array_0

    invoke-static {v11}, Lyr;->c([F)Landroid/animation/ValueAnimator;

    move-result-object v11

    .line 2330
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/launcher3/Workspace;->WM:Lagy;

    invoke-virtual {v11, v14}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2331
    int-to-long v14, v12

    invoke-virtual {v11, v14, v15}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2332
    new-instance v14, Lafo;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v2, v4}, Lafo;-><init>(Lcom/android/launcher3/Workspace;Lcom/android/launcher3/CellLayout;I)V

    invoke-virtual {v11, v14}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2339
    invoke-virtual {v3, v11}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_1d

    .line 2343
    :cond_34
    if-eqz v10, :cond_37

    .line 2345
    new-instance v2, Labq;

    invoke-direct {v2, v10}, Labq;-><init>(Landroid/view/View;)V

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Labq;->t(F)Labq;

    move-result-object v2

    invoke-virtual {v2}, Labq;->iS()Labq;

    move-result-object v2

    .line 2347
    new-instance v4, Lagp;

    invoke-direct {v4, v10}, Lagp;-><init>(Landroid/view/View;)V

    invoke-virtual {v2, v4}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2353
    :goto_1e
    new-instance v4, Labq;

    invoke-direct {v4, v8}, Labq;-><init>(Landroid/view/View;)V

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Labq;->t(F)Labq;

    move-result-object v4

    invoke-virtual {v4}, Labq;->iS()Labq;

    move-result-object v4

    .line 2355
    new-instance v10, Lagp;

    invoke-direct {v10, v8}, Lagp;-><init>(Landroid/view/View;)V

    invoke-virtual {v4, v10}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2357
    new-instance v10, Labq;

    invoke-direct {v10, v6}, Labq;-><init>(Landroid/view/View;)V

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Labq;->t(F)Labq;

    move-result-object v10

    invoke-virtual {v10}, Labq;->iS()Labq;

    move-result-object v10

    .line 2359
    new-instance v11, Lagp;

    invoke-direct {v11, v6}, Lagp;-><init>(Landroid/view/View;)V

    invoke-virtual {v10, v11}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2361
    new-instance v11, Labq;

    invoke-direct {v11, v7}, Labq;-><init>(Landroid/view/View;)V

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Labq;->t(F)Labq;

    move-result-object v11

    invoke-virtual {v11}, Labq;->iS()Labq;

    move-result-object v11

    .line 2363
    new-instance v14, Lagp;

    invoke-direct {v14, v7}, Lagp;-><init>(Landroid/view/View;)V

    invoke-virtual {v11, v14}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2367
    const/4 v14, 0x2

    const/4 v15, 0x0

    invoke-virtual {v8, v14, v15}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 2368
    const/4 v14, 0x2

    const/4 v15, 0x0

    invoke-virtual {v6, v14, v15}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 2369
    const/4 v14, 0x2

    const/4 v15, 0x0

    invoke-virtual {v7, v14, v15}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 2370
    if-eqz p5, :cond_35

    .line 2371
    move-object/from16 v0, p5

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2372
    move-object/from16 v0, p5

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2373
    move-object/from16 v0, p5

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2376
    :cond_35
    if-eqz v13, :cond_38

    .line 2377
    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v7, 0x40000000    # 2.0f

    invoke-direct {v6, v7}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v2, v6}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2378
    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v7, 0x40000000    # 2.0f

    invoke-direct {v6, v7}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v4, v6}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2379
    const/4 v6, 0x0

    invoke-virtual {v11, v6}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2386
    :cond_36
    :goto_1f
    int-to-long v6, v12

    invoke-virtual {v11, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 2387
    int-to-long v6, v12

    invoke-virtual {v2, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 2388
    int-to-long v6, v12

    invoke-virtual {v4, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 2389
    int-to-long v6, v12

    invoke-virtual {v10, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 2391
    invoke-virtual {v3, v11}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 2392
    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 2393
    invoke-virtual {v3, v10}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 2394
    invoke-virtual {v3, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 2395
    move/from16 v0, p3

    int-to-long v6, v0

    invoke-virtual {v3, v6, v7}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 2412
    :goto_20
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/android/launcher3/Launcher;->aa(Z)V

    .line 2414
    if-eqz v5, :cond_3d

    .line 2415
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v2, v1}, Lcom/android/launcher3/Workspace;->b(FZ)V

    goto/16 :goto_0

    .line 2350
    :cond_37
    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_1

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v2

    goto/16 :goto_1e

    .line 2380
    :cond_38
    if-eqz v9, :cond_36

    .line 2381
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2382
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2383
    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v7, 0x40000000    # 2.0f

    invoke-direct {v6, v7}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v11, v6}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    goto :goto_1f

    .line 2397
    :cond_39
    move/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2398
    invoke-static {v7}, Lagp;->ah(Landroid/view/View;)V

    .line 2399
    move/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2400
    invoke-static {v8}, Lagp;->ah(Landroid/view/View;)V

    .line 2401
    if-eqz v10, :cond_3a

    .line 2402
    move/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2403
    invoke-static {v10}, Lagp;->ah(Landroid/view/View;)V

    .line 2405
    :cond_3a
    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2406
    invoke-static {v6}, Lagp;->ah(Landroid/view/View;)V

    .line 2407
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VV:Lagv;

    sget-object v4, Lagv;->Xv:Lagv;

    if-ne v2, v4, :cond_3c

    const/4 v2, 0x0

    move v4, v2

    :goto_21
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->lc()Z

    move-result v2

    if-eqz v2, :cond_3b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    const-wide/16 v6, -0x12d

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/launcher3/CellLayout;

    invoke-virtual {v2, v4}, Lcom/android/launcher3/CellLayout;->setVisibility(I)V

    .line 2408
    :cond_3b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher3/Workspace;->WD:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Workspace;->setScaleX(F)V

    .line 2409
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher3/Workspace;->WD:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Workspace;->setScaleY(F)V

    .line 2410
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Workspace;->setTranslationY(F)V

    goto/16 :goto_20

    .line 2407
    :cond_3c
    const/4 v2, 0x4

    move v4, v2

    goto :goto_21

    .line 2417
    :cond_3d
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0c000c

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v2, v4

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v2, v1}, Lcom/android/launcher3/Workspace;->b(FZ)V

    goto/16 :goto_0

    .line 2328
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 2350
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method private a(Landroid/graphics/Bitmap;IIIZ)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2620
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0057

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 2621
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p3, p4, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2622
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2624
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-direct {v0, v6, v6, v2, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2625
    add-int/lit8 v2, p3, -0x2

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v2, v4

    add-int/lit8 v4, p4, -0x2

    int-to-float v4, v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 2627
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v2

    float-to-int v4, v4

    .line 2628
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v2, v5

    float-to-int v2, v2

    .line 2629
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v6, v6, v4, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2632
    sub-int v4, p3, v4

    div-int/lit8 v4, v4, 0x2

    sub-int v2, p4, v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v5, v4, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 2634
    iget-object v2, p0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    invoke-virtual {v2, p1, v0, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2635
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->zr:Lwf;

    iget-object v2, p0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    move v4, v3

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lwf;->a(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;IIZ)V

    .line 2637
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    invoke-virtual {v0, v7}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2639
    return-object v1
.end method

.method private a(Landroid/view/View;Ljava/util/concurrent/atomic/AtomicInteger;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 2579
    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    .line 2580
    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 2581
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v2, 0x1

    aget-object v0, v0, v2

    .line 2582
    invoke-static {v0}, Lcom/android/launcher3/Workspace;->e(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;

    move-result-object v2

    .line 2583
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr v3, v1

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2585
    iget v3, v2, Landroid/graphics/Rect;->left:I

    sub-int v3, v1, v3

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int v2, v3, v2

    invoke-virtual {p2, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 2591
    :goto_0
    iget-object v2, p0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2592
    iget-object v2, p0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    invoke-static {p1, v2, v1}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;Landroid/graphics/Canvas;I)V

    .line 2593
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2595
    return-object v0

    .line 2587
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v2, v1

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/android/launcher3/CellLayout;IIII)Landroid/graphics/Rect;
    .locals 6

    .prologue
    .line 392
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    .line 393
    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher3/CellLayout;->b(IIIILandroid/graphics/Rect;)V

    .line 394
    return-object v5
.end method

.method public static a(Lcom/android/launcher3/Launcher;I)Landroid/graphics/Rect;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 3240
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 3241
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    .line 3243
    invoke-virtual {p0}, Lcom/android/launcher3/Launcher;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 3244
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 3245
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 3246
    invoke-virtual {v1, v2, v3}, Landroid/view/Display;->getCurrentSizeRange(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 3247
    iget v1, v0, Ltu;->Dh:F

    float-to-int v1, v1

    .line 3248
    iget v4, v0, Ltu;->Dg:F

    float-to-int v4, v4

    .line 3249
    if-nez p1, :cond_1

    .line 3250
    sget-object v5, Lcom/android/launcher3/Workspace;->VE:Landroid/graphics/Rect;

    if-nez v5, :cond_0

    .line 3251
    invoke-virtual {v0, v6}, Ltu;->ba(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 3252
    iget v3, v3, Landroid/graphics/Point;->x:I

    iget v5, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v5

    iget v5, v0, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v5

    .line 3253
    iget v2, v2, Landroid/graphics/Point;->y:I

    iget v5, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v5

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v2, v0

    .line 3254
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 3255
    sput-object v2, Lcom/android/launcher3/Workspace;->VE:Landroid/graphics/Rect;

    div-int v1, v3, v1

    div-int/2addr v0, v4

    invoke-virtual {v2, v1, v0, v6, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 3259
    :cond_0
    sget-object v0, Lcom/android/launcher3/Workspace;->VE:Landroid/graphics/Rect;

    .line 3272
    :goto_0
    return-object v0

    .line 3260
    :cond_1
    if-ne p1, v7, :cond_3

    .line 3261
    sget-object v5, Lcom/android/launcher3/Workspace;->VF:Landroid/graphics/Rect;

    if-nez v5, :cond_2

    .line 3262
    invoke-virtual {v0, v7}, Ltu;->ba(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 3263
    iget v2, v2, Landroid/graphics/Point;->x:I

    iget v5, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v5

    iget v5, v0, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v5

    .line 3264
    iget v3, v3, Landroid/graphics/Point;->y:I

    iget v5, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v5

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v3, v0

    .line 3265
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 3266
    sput-object v3, Lcom/android/launcher3/Workspace;->VF:Landroid/graphics/Rect;

    div-int v1, v2, v1

    div-int/2addr v0, v4

    invoke-virtual {v3, v1, v0, v6, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 3270
    :cond_2
    sget-object v0, Lcom/android/launcher3/Workspace;->VF:Landroid/graphics/Rect;

    goto :goto_0

    .line 3272
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lagt;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4605
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    .line 4606
    new-instance v1, Laga;

    invoke-direct {v1, p0, p1, v0}, Laga;-><init>(Lcom/android/launcher3/Workspace;Lagt;[Landroid/view/View;)V

    invoke-direct {p0, v2, v1}, Lcom/android/launcher3/Workspace;->a(ZLagt;)V

    .line 4616
    aget-object v0, v0, v2

    return-object v0
.end method

.method private a(FFZ)Lcom/android/launcher3/CellLayout;
    .locals 11

    .prologue
    .line 3463
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v5

    .line 3464
    const/4 v3, 0x0

    .line 3465
    const v2, 0x7f7fffff    # Float.MAX_VALUE

    .line 3467
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v5, :cond_1

    .line 3469
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v6, -0x12d

    cmp-long v0, v0, v6

    if-eqz v0, :cond_2

    .line 3470
    invoke-virtual {p0, v4}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 3475
    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v6, 0x0

    aput p1, v1, v6

    const/4 v6, 0x1

    aput p2, v1, v6

    .line 3478
    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v6

    iget-object v7, p0, Lcom/android/launcher3/Workspace;->VR:Landroid/graphics/Matrix;

    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 3479
    iget-object v6, p0, Lcom/android/launcher3/Workspace;->VR:Landroid/graphics/Matrix;

    invoke-static {v0, v1}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;[F)V

    .line 3481
    const/4 v6, 0x0

    aget v6, v1, v6

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_0

    const/4 v6, 0x0

    aget v6, v1, v6

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->getWidth()I

    move-result v7

    int-to-float v7, v7

    cmpg-float v6, v6, v7

    if-gtz v6, :cond_0

    const/4 v6, 0x1

    aget v6, v1, v6

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_0

    const/4 v6, 0x1

    aget v6, v1, v6

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->getHeight()I

    move-result v7

    int-to-float v7, v7

    cmpg-float v6, v6, v7

    if-gtz v6, :cond_0

    .line 3506
    :goto_1
    return-object v0

    .line 3486
    :cond_0
    iget-object v6, p0, Lcom/android/launcher3/Workspace;->VQ:[F

    .line 3489
    const/4 v7, 0x0

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->getWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    aput v8, v6, v7

    .line 3490
    const/4 v7, 0x1

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->getHeight()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    aput v8, v6, v7

    .line 3491
    const/4 v7, 0x0

    aget v8, v6, v7

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v9

    int-to-float v9, v9

    add-float/2addr v8, v9

    aput v8, v6, v7

    const/4 v7, 0x1

    aget v8, v6, v7

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v9

    int-to-float v9, v9

    add-float/2addr v8, v9

    aput v8, v6, v7

    .line 3493
    const/4 v7, 0x0

    aput p1, v1, v7

    .line 3494
    const/4 v7, 0x1

    aput p2, v1, v7

    .line 3498
    const/4 v7, 0x0

    aget v1, v1, v7

    const/4 v7, 0x0

    aget v7, v6, v7

    sub-float/2addr v1, v7

    const/4 v7, 0x1

    aget v7, v6, v7

    const/4 v8, 0x1

    aget v6, v6, v8

    sub-float v6, v7, v6

    mul-float/2addr v1, v1

    mul-float/2addr v6, v6

    add-float/2addr v1, v6

    .line 3500
    cmpg-float v6, v1, v2

    if-gez v6, :cond_2

    move v10, v1

    move-object v1, v0

    move v0, v10

    .line 3467
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    move v2, v0

    goto/16 :goto_0

    :cond_1
    move-object v0, v3

    .line 3506
    goto :goto_1

    :cond_2
    move v0, v2

    move-object v1, v3

    goto :goto_2
.end method

.method public static synthetic a(Lcom/android/launcher3/Workspace;)Lcom/android/launcher3/Launcher;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    return-object v0
.end method

.method public static synthetic a(Lcom/android/launcher3/Workspace;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/android/launcher3/Workspace;->Wj:Ljava/lang/Runnable;

    return-object p1
.end method

.method public static synthetic a(Lcom/android/launcher3/Workspace;Lvs;)Lvs;
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/android/launcher3/Workspace;->Wn:Lvs;

    return-object p1
.end method

.method private a(IILjava/lang/Runnable;Z)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 791
    const-string v0, "Launcher.Workspace"

    const-string v1, "11683562 - fadeAndRemoveEmptyScreen()"

    invoke-static {v0, v1, v6}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 792
    const-string v0, "alpha"

    new-array v1, v6, [F

    aput v3, v1, v7

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 793
    const-string v0, "backgroundAlpha"

    new-array v2, v6, [F

    aput v3, v2, v7

    invoke-static {v0, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 795
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    const-wide/16 v4, -0xc9

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 797
    new-instance v3, Lagj;

    invoke-direct {v3, p0, v0, p4}, Lagj;-><init>(Lcom/android/launcher3/Workspace;Lcom/android/launcher3/CellLayout;Z)V

    iput-object v3, p0, Lcom/android/launcher3/Workspace;->Vz:Ljava/lang/Runnable;

    .line 811
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/PropertyValuesHolder;

    aput-object v1, v3, v7

    aput-object v2, v3, v6

    invoke-static {v0, v3}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 812
    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 813
    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 814
    new-instance v1, Lagk;

    invoke-direct {v1, p0, p3}, Lagk;-><init>(Lcom/android/launcher3/Workspace;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 825
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 826
    return-void
.end method

.method private a(JLjava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 1311
    invoke-virtual {p0, p1, p2}, Lcom/android/launcher3/Workspace;->k(J)I

    move-result v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/launcher3/Workspace;->Wk:Ljava/lang/Runnable;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/launcher3/Workspace;->Wk:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    :cond_0
    iput-object v1, p0, Lcom/android/launcher3/Workspace;->Wk:Ljava/lang/Runnable;

    const/16 v1, 0x3b6

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/Workspace;->P(II)V

    .line 1312
    return-void
.end method

.method private a(Landroid/view/View;JJIIIIZZ)V
    .locals 6

    .prologue
    .line 989
    const-wide/16 v0, -0x64

    cmp-long v0, p2, v0

    if-nez v0, :cond_1

    .line 990
    invoke-virtual {p0, p4, p5}, Lcom/android/launcher3/Workspace;->j(J)Lcom/android/launcher3/CellLayout;

    move-result-object v0

    if-nez v0, :cond_1

    .line 991
    const-string v0, "Launcher.Workspace"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Skipping child, screenId "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 993
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 1062
    :cond_0
    :goto_0
    return-void

    .line 997
    :cond_1
    const-wide/16 v0, -0xc9

    cmp-long v0, p4, v0

    if-nez v0, :cond_2

    .line 999
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Screen id should not be EXTRA_EMPTY_SCREEN_ID"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1003
    :cond_2
    const-wide/16 v0, -0x65

    cmp-long v0, p2, v0

    if-nez v0, :cond_9

    .line 1004
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/Hotseat;->gW()Lcom/android/launcher3/CellLayout;

    move-result-object v1

    .line 1005
    new-instance v0, Lwh;

    invoke-direct {v0}, Lwh;-><init>()V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1008
    instance-of v0, p1, Lcom/android/launcher3/FolderIcon;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 1009
    check-cast v0, Lcom/android/launcher3/FolderIcon;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/android/launcher3/FolderIcon;->Q(Z)V

    .line 1012
    :cond_3
    if-eqz p11, :cond_8

    .line 1013
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v0

    long-to-int v2, p4

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Hotseat;->bg(I)I

    move-result p6

    .line 1014
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v0

    long-to-int v2, p4

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Hotseat;->bh(I)I

    move-result p7

    move-object v0, v1

    .line 1027
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1029
    if-eqz v1, :cond_4

    instance-of v2, v1, Lcom/android/launcher3/CellLayout$LayoutParams;

    if-nez v2, :cond_b

    .line 1030
    :cond_4
    new-instance v4, Lcom/android/launcher3/CellLayout$LayoutParams;

    invoke-direct {v4, p6, p7, p8, p9}, Lcom/android/launcher3/CellLayout$LayoutParams;-><init>(IIII)V

    .line 1039
    :goto_2
    if-gez p8, :cond_5

    if-gez p9, :cond_5

    .line 1040
    const/4 v1, 0x0

    iput-boolean v1, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bs:Z

    .line 1044
    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lwq;

    .line 1045
    iget-object v2, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2, v1}, Lcom/android/launcher3/Launcher;->d(Lwq;)I

    move-result v3

    .line 1047
    instance-of v1, p1, Lcom/android/launcher3/Folder;

    if-nez v1, :cond_c

    const/4 v5, 0x1

    .line 1048
    :goto_3
    if-eqz p10, :cond_d

    const/4 v2, 0x0

    :goto_4
    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher3/CellLayout;->a(Landroid/view/View;IILcom/android/launcher3/CellLayout$LayoutParams;Z)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1052
    const-string v0, "Launcher.Workspace"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to add to item at ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") to CellLayout"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1055
    :cond_6
    instance-of v0, p1, Lcom/android/launcher3/Folder;

    if-nez v0, :cond_7

    .line 1056
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setHapticFeedbackEnabled(Z)V

    .line 1057
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Qx:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1059
    :cond_7
    instance-of v0, p1, Luo;

    if-eqz v0, :cond_0

    .line 1060
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->yg:Lty;

    check-cast p1, Luo;

    invoke-virtual {v0, p1}, Lty;->b(Luo;)V

    goto/16 :goto_0

    .line 1016
    :cond_8
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v0

    invoke-virtual {v0, p6, p7}, Lcom/android/launcher3/Hotseat;->K(II)I

    move-object v0, v1

    goto :goto_1

    .line 1020
    :cond_9
    instance-of v0, p1, Lcom/android/launcher3/FolderIcon;

    if-eqz v0, :cond_a

    move-object v0, p1

    .line 1021
    check-cast v0, Lcom/android/launcher3/FolderIcon;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/FolderIcon;->Q(Z)V

    .line 1023
    :cond_a
    invoke-virtual {p0, p4, p5}, Lcom/android/launcher3/Workspace;->j(J)Lcom/android/launcher3/CellLayout;

    move-result-object v0

    .line 1024
    new-instance v1, Lwl;

    invoke-direct {v1}, Lwl;-><init>()V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto/16 :goto_1

    .line 1032
    :cond_b
    check-cast v1, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 1033
    iput p6, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    .line 1034
    iput p7, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    .line 1035
    iput p8, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    .line 1036
    iput p9, v1, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    move-object v4, v1

    goto/16 :goto_2

    .line 1047
    :cond_c
    const/4 v5, 0x0

    goto :goto_3

    .line 1048
    :cond_d
    const/4 v2, -0x1

    goto :goto_4
.end method

.method private static a(Landroid/view/View;Landroid/graphics/Canvas;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2537
    sget-object v3, Lcom/android/launcher3/Workspace;->IK:Landroid/graphics/Rect;

    .line 2538
    invoke-virtual {p0, v3}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2542
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2543
    instance-of v0, p0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 2544
    check-cast p0, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    .line 2545
    invoke-static {v0}, Lcom/android/launcher3/Workspace;->e(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;

    move-result-object v1

    .line 2546
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/2addr v4, p2

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v5

    add-int/2addr v5, p2

    invoke-virtual {v3, v2, v2, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 2547
    div-int/lit8 v2, p2, 0x2

    iget v3, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-int/lit8 v3, p2, 0x2

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v3, v1

    int-to-float v1, v1

    invoke-virtual {p1, v2, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2548
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2567
    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2568
    return-void

    .line 2550
    :cond_1
    instance-of v0, p0, Lcom/android/launcher3/FolderIcon;

    if-eqz v0, :cond_2

    move-object v0, p0

    .line 2553
    check-cast v0, Lcom/android/launcher3/FolderIcon;

    invoke-virtual {v0}, Lcom/android/launcher3/FolderIcon;->gO()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p0

    .line 2554
    check-cast v0, Lcom/android/launcher3/FolderIcon;

    invoke-virtual {v0, v2}, Lcom/android/launcher3/FolderIcon;->Q(Z)V

    move v0, v1

    .line 2558
    :goto_1
    invoke-virtual {p0}, Landroid/view/View;->getScrollX()I

    move-result v2

    neg-int v2, v2

    div-int/lit8 v4, p2, 0x2

    add-int/2addr v2, v4

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getScrollY()I

    move-result v4

    neg-int v4, v4

    div-int/lit8 v5, p2, 0x2

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2559
    sget-object v2, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v3, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 2560
    invoke-virtual {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 2563
    if-eqz v0, :cond_0

    .line 2564
    check-cast p0, Lcom/android/launcher3/FolderIcon;

    invoke-virtual {p0, v1}, Lcom/android/launcher3/FolderIcon;->Q(Z)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method private static a(Landroid/view/View;[F)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3402
    aget v0, p1, v2

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    aput v0, p1, v2

    .line 3403
    aget v0, p1, v3

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    aput v0, p1, v3

    .line 3404
    return-void
.end method

.method private a(Lcom/android/launcher3/CellLayout;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 4265
    invoke-virtual {p1}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v0

    invoke-virtual {v0}, Ladg;->getChildCount()I

    move-result v4

    .line 4267
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v0, v2

    .line 4268
    :goto_0
    if-ge v0, v4, :cond_0

    .line 4269
    invoke-virtual {p1}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v1

    invoke-virtual {v1, v0}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 4270
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4268
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v3, v2

    .line 4273
    :goto_1
    if-ge v3, v4, :cond_8

    .line 4274
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 4275
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwq;

    .line 4277
    instance-of v6, v0, Ladh;

    if-eqz v6, :cond_2

    .line 4278
    check-cast v0, Ladh;

    .line 4279
    iget-object v6, v0, Ladh;->intent:Landroid/content/Intent;

    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v6

    .line 4281
    iget-object v0, v0, Ladh;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 4285
    if-eqz v0, :cond_1

    sget-object v7, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v0, v7}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4286
    :cond_1
    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 4290
    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4301
    :cond_2
    :goto_2
    instance-of v0, v1, Lcom/android/launcher3/FolderIcon;

    if-eqz v0, :cond_7

    move-object v0, v1

    .line 4302
    check-cast v0, Lcom/android/launcher3/FolderIcon;

    .line 4303
    invoke-virtual {v0}, Lcom/android/launcher3/FolderIcon;->gL()Lcom/android/launcher3/Folder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/Folder;->gJ()Ljava/util/ArrayList;

    move-result-object v6

    move v1, v2

    .line 4304
    :goto_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 4305
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ladh;

    if-eqz v0, :cond_4

    .line 4306
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladh;

    .line 4307
    iget-object v7, v0, Ladh;->intent:Landroid/content/Intent;

    invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v7

    .line 4309
    iget-object v0, v0, Ladh;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 4313
    if-eqz v0, :cond_3

    sget-object v8, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v0, v8}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4314
    :cond_3
    invoke-virtual {p2, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 4318
    invoke-virtual {p2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4304
    :cond_4
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 4292
    :cond_5
    if-eqz p3, :cond_2

    .line 4297
    invoke-virtual {p3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 4320
    :cond_6
    if-eqz p3, :cond_4

    .line 4325
    invoke-virtual {p3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 4273
    :cond_7
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_1

    .line 4332
    :cond_8
    return-void
.end method

.method private a(Lcom/android/launcher3/Hotseat;[F)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 3424
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VO:[I

    aget v1, p2, v4

    float-to-int v1, v1

    aput v1, v0, v4

    .line 3425
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VO:[I

    aget v1, p2, v3

    float-to-int v1, v1

    aput v1, v0, v3

    .line 3426
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher3/Workspace;->VO:[I

    invoke-virtual {v0, p0, v1, v3}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/View;[IZ)F

    .line 3427
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v0

    invoke-virtual {p1}, Lcom/android/launcher3/Hotseat;->gW()Lcom/android/launcher3/CellLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/android/launcher3/Workspace;->VO:[I

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher3/DragLayer;->c(Landroid/view/View;[I)F

    .line 3429
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VO:[I

    aget v0, v0, v4

    int-to-float v0, v0

    aput v0, p2, v4

    .line 3430
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VO:[I

    aget v0, v0, v3

    int-to-float v0, v0

    aput v0, p2, v3

    .line 3431
    return-void
.end method

.method private a(Ljava/util/HashSet;Lahz;)V
    .locals 10

    .prologue
    .line 4685
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->lx()Ljava/util/ArrayList;

    move-result-object v0

    .line 4686
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/android/launcher3/CellLayout;

    .line 4687
    invoke-virtual {v7}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v9

    .line 4689
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 4690
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v9}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 4691
    invoke-virtual {v9, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 4692
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwq;

    invoke-virtual {v6, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4690
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 4695
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 4696
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 4698
    new-instance v0, Lagd;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lagd;-><init>(Lcom/android/launcher3/Workspace;Ljava/util/HashSet;Lahz;Ljava/util/HashMap;Ljava/util/ArrayList;Ljava/util/HashMap;)V

    .line 4724
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {v1, v0}, Lzi;->a(Ljava/util/Collection;Laag;)Ljava/util/ArrayList;

    .line 4727
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lvy;

    .line 4728
    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 4729
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladh;

    .line 4730
    invoke-virtual {v1, v0}, Lvy;->k(Ladh;)V

    goto :goto_2

    .line 4735
    :cond_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 4738
    invoke-virtual {v7, v0}, Lcom/android/launcher3/CellLayout;->removeViewInLayout(Landroid/view/View;)V

    .line 4739
    instance-of v2, v0, Luo;

    if-eqz v2, :cond_4

    .line 4740
    iget-object v2, p0, Lcom/android/launcher3/Workspace;->yg:Lty;

    check-cast v0, Luo;

    invoke-virtual {v2, v0}, Lty;->c(Luo;)V

    goto :goto_3

    .line 4744
    :cond_5
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 4745
    invoke-virtual {v9}, Landroid/view/ViewGroup;->requestLayout()V

    .line 4746
    invoke-virtual {v9}, Landroid/view/ViewGroup;->invalidate()V

    goto/16 :goto_0

    .line 4751
    :cond_6
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->kU()V

    .line 4752
    return-void
.end method

.method private a(Ljava/util/Set;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 5000
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 5003
    new-instance v1, Lagh;

    invoke-direct {v1, p0, p1, v0}, Lagh;-><init>(Lcom/android/launcher3/Workspace;Ljava/util/Set;Ljava/util/ArrayList;)V

    invoke-direct {p0, v3, v1}, Lcom/android/launcher3/Workspace;->a(ZLagt;)V

    .line 5023
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5024
    new-instance v1, Lagq;

    iget-object v2, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher3/Launcher;->hr()Lyw;

    move-result-object v2

    invoke-direct {v1, p0, v0, v2}, Lagq;-><init>(Lcom/android/launcher3/Workspace;Ljava/util/ArrayList;Lyw;)V

    .line 5026
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyy;

    iget-object v0, v0, Lyy;->Mp:Landroid/content/ComponentName;

    invoke-static {v2, v0}, Lzi;->a(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 5029
    invoke-virtual {v1}, Lagq;->run()V

    .line 5034
    :cond_0
    return-void
.end method

.method private a(ZIZ)V
    .locals 6

    .prologue
    .line 2119
    sget-object v1, Lagv;->Xy:Lagv;

    .line 2120
    if-nez p1, :cond_0

    .line 2121
    sget-object v1, Lagv;->Xv:Lagv;

    .line 2124
    :cond_0
    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v2, p3

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher3/Workspace;->a(Lagv;ZIILjava/util/ArrayList;)Landroid/animation/Animator;

    move-result-object v0

    .line 2125
    if-eqz v0, :cond_1

    .line 2126
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->ln()V

    .line 2127
    new-instance v1, Lago;

    invoke-direct {v1, p0}, Lago;-><init>(Lcom/android/launcher3/Workspace;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2133
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 2135
    :cond_1
    return-void
.end method

.method private a(ZLagt;)V
    .locals 13

    .prologue
    const/4 v5, 0x0

    .line 4773
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v2

    move v1, v5

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/Hotseat;->gW()Lcom/android/launcher3/CellLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4774
    :cond_1
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v4, v5

    .line 4775
    :goto_1
    if-ge v4, v9, :cond_2

    .line 4776
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ladg;

    .line 4778
    invoke-virtual {v3}, Ladg;->getChildCount()I

    move-result v10

    move v7, v5

    .line 4779
    :goto_2
    if-ge v7, v10, :cond_6

    .line 4780
    invoke-virtual {v3, v7}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 4781
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwq;

    .line 4782
    if-eqz p1, :cond_4

    instance-of v2, v0, Lvy;

    if-eqz v2, :cond_4

    instance-of v2, v1, Lcom/android/launcher3/FolderIcon;

    if-eqz v2, :cond_4

    move-object v0, v1

    .line 4783
    check-cast v0, Lcom/android/launcher3/FolderIcon;

    .line 4784
    invoke-virtual {v0}, Lcom/android/launcher3/FolderIcon;->gL()Lcom/android/launcher3/Folder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher3/Folder;->gJ()Ljava/util/ArrayList;

    move-result-object v11

    .line 4786
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v12

    move v6, v5

    .line 4787
    :goto_3
    if-ge v6, v12, :cond_5

    .line 4788
    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 4789
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lwq;

    .line 4790
    invoke-interface {p2, v2, v1, v0}, Lagt;->a(Lwq;Landroid/view/View;Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4801
    :cond_2
    return-void

    .line 4787
    :cond_3
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_3

    .line 4795
    :cond_4
    const/4 v2, 0x0

    invoke-interface {p2, v0, v1, v2}, Lagt;->a(Lwq;Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 4779
    :cond_5
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_2

    .line 4775
    :cond_6
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1
.end method

.method private a(IILandroid/graphics/Rect;)Z
    .locals 7

    .prologue
    const v6, 0x7fffffff

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3407
    if-nez p3, :cond_0

    .line 3408
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 3410
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VO:[I

    aput p1, v0, v2

    .line 3411
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VO:[I

    aput p2, v0, v1

    .line 3412
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v0

    iget-object v3, p0, Lcom/android/launcher3/Workspace;->VO:[I

    invoke-virtual {v0, p0, v3, v1}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/View;[IZ)F

    .line 3414
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 3415
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v3

    .line 3416
    invoke-virtual {v3}, Ltu;->fC()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/graphics/Rect;

    iget v4, v3, Ltu;->Dz:I

    iget v5, v3, Ltu;->DX:I

    sub-int/2addr v4, v5

    iget v3, v3, Ltu;->DA:I

    invoke-direct {v0, v4, v2, v6, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3417
    :goto_0
    iget-object v3, p0, Lcom/android/launcher3/Workspace;->VO:[I

    aget v3, v3, v2

    iget-object v4, p0, Lcom/android/launcher3/Workspace;->VO:[I

    aget v4, v4, v1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 3420
    :goto_1
    return v0

    .line 3416
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    iget v4, v3, Ltu;->DA:I

    iget v5, v3, Ltu;->DX:I

    sub-int/2addr v4, v5

    iget v3, v3, Ltu;->Dz:I

    invoke-direct {v0, v2, v4, v3, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 3420
    goto :goto_1
.end method

.method public static synthetic a(Lcom/android/launcher3/Workspace;Z)Z
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/Workspace;->VA:Z

    return v0
.end method

.method private a(Ljava/lang/Object;Lcom/android/launcher3/CellLayout;[IF)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2906
    iget v0, p0, Lcom/android/launcher3/Workspace;->Wr:F

    cmpl-float v0, p4, v0

    if-lez v0, :cond_0

    move v0, v2

    .line 2922
    :goto_0
    return v0

    .line 2907
    :cond_0
    aget v0, p3, v2

    aget v1, p3, v3

    invoke-virtual {p2, v0, v1}, Lcom/android/launcher3/CellLayout;->C(II)Landroid/view/View;

    move-result-object v1

    .line 2909
    if-eqz v1, :cond_2

    .line 2910
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 2911
    iget-boolean v4, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bp:Z

    if-eqz v4, :cond_2

    iget v4, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bn:I

    iget v5, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    if-ne v4, v5, :cond_1

    iget v4, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bo:I

    iget v0, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bo:I

    if-eq v4, v0, :cond_2

    :cond_1
    move v0, v2

    .line 2912
    goto :goto_0

    .line 2916
    :cond_2
    instance-of v0, v1, Lcom/android/launcher3/FolderIcon;

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 2917
    check-cast v0, Lcom/android/launcher3/FolderIcon;

    .line 2918
    invoke-virtual {v0, p1}, Lcom/android/launcher3/FolderIcon;->aa(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    .line 2919
    goto :goto_0

    :cond_3
    move v0, v2

    .line 2922
    goto :goto_0
.end method

.method private a(Lwq;Lcom/android/launcher3/CellLayout;[IFZ)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2877
    iget v0, p0, Lcom/android/launcher3/Workspace;->Wr:F

    cmpl-float v0, p4, v0

    if-lez v0, :cond_1

    .line 2901
    :cond_0
    :goto_0
    return v2

    .line 2878
    :cond_1
    aget v0, p3, v2

    aget v3, p3, v1

    invoke-virtual {p2, v0, v3}, Lcom/android/launcher3/CellLayout;->C(II)Landroid/view/View;

    move-result-object v3

    .line 2880
    if-eqz v3, :cond_2

    .line 2881
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 2882
    iget-boolean v4, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bp:Z

    if-eqz v4, :cond_2

    iget v4, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bn:I

    iget v5, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    if-ne v4, v5, :cond_0

    iget v4, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bo:I

    iget v0, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bo:I

    if-ne v4, v0, :cond_0

    .line 2888
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    if-eqz v0, :cond_7

    .line 2889
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget-object v0, v0, Lsy;->Ba:Landroid/view/View;

    if-ne v3, v0, :cond_5

    move v0, v1

    .line 2892
    :goto_1
    if-eqz v3, :cond_0

    if-nez v0, :cond_0

    if-eqz p5, :cond_3

    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->Wp:Z

    if-eqz v0, :cond_0

    .line 2896
    :cond_3
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v3, v0, Ladh;

    .line 2897
    iget v0, p1, Lwq;->Jz:I

    if-eqz v0, :cond_4

    iget v0, p1, Lwq;->Jz:I

    if-ne v0, v1, :cond_6

    :cond_4
    move v0, v1

    .line 2901
    :goto_2
    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    move v2, v1

    goto :goto_0

    :cond_5
    move v0, v2

    .line 2889
    goto :goto_1

    :cond_6
    move v0, v2

    .line 2897
    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_1
.end method

.method private a(IIIILuj;[F)[F
    .locals 4

    .prologue
    .line 3516
    if-nez p6, :cond_0

    .line 3517
    const/4 v0, 0x2

    new-array p6, v0, [F

    .line 3524
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    add-int/2addr v0, p1

    .line 3525
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v1, p2

    .line 3531
    sub-int/2addr v0, p3

    .line 3532
    sub-int/2addr v1, p4

    .line 3535
    const/4 v2, 0x0

    invoke-virtual {p5}, Luj;->fZ()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    int-to-float v0, v0

    aput v0, p6, v2

    .line 3536
    const/4 v0, 0x1

    invoke-virtual {p5}, Luj;->fZ()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    aput v1, p6, v0

    .line 3538
    return-object p6
.end method

.method private static a(IIIILcom/android/launcher3/CellLayout;[I)[I
    .locals 6

    .prologue
    .line 4151
    move-object v0, p4

    move v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher3/CellLayout;->c(IIII[I)[I

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/android/launcher3/Workspace;IIIILcom/android/launcher3/CellLayout;[I)[I
    .locals 1

    .prologue
    .line 88
    invoke-static/range {p1 .. p6}, Lcom/android/launcher3/Workspace;->a(IIIILcom/android/launcher3/CellLayout;[I)[I

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/android/launcher3/Workspace;[I)[I
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/android/launcher3/Workspace;->He:[I

    return-object p1
.end method

.method private as(Z)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1848
    iget-object v2, p0, Lcom/android/launcher3/Workspace;->VV:Lagv;

    sget-object v3, Lagv;->Xy:Lagv;

    if-eq v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/android/launcher3/Workspace;->VW:Z

    if-eqz v2, :cond_3

    :cond_0
    move v2, v0

    .line 1849
    :goto_0
    if-nez p1, :cond_1

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/android/launcher3/Workspace;->VX:Z

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jp()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1851
    :cond_1
    :goto_1
    iget-boolean v2, p0, Lcom/android/launcher3/Workspace;->VZ:Z

    if-eq v0, v2, :cond_2

    .line 1852
    iput-boolean v0, p0, Lcom/android/launcher3/Workspace;->VZ:Z

    .line 1853
    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->VZ:Z

    if-eqz v0, :cond_5

    .line 1854
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->et()V

    .line 1862
    :cond_2
    return-void

    :cond_3
    move v2, v1

    .line 1848
    goto :goto_0

    :cond_4
    move v0, v1

    .line 1849
    goto :goto_1

    :cond_5
    move v2, v1

    .line 1856
    :goto_2
    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1857
    invoke-virtual {p0, v2}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 1858
    invoke-virtual {v0, v1}, Lcom/android/launcher3/CellLayout;->E(Z)V

    .line 1856
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2
.end method

.method private au(Z)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 3384
    if-eqz p1, :cond_0

    .line 3385
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Hh:Lro;

    invoke-virtual {v0}, Lro;->dV()V

    .line 3387
    :cond_0
    iput v1, p0, Lcom/android/launcher3/Workspace;->Wv:I

    .line 3388
    iput v1, p0, Lcom/android/launcher3/Workspace;->Ww:I

    .line 3389
    return-void
.end method

.method public static synthetic b(Lcom/android/launcher3/Workspace;I)I
    .locals 0

    .prologue
    .line 88
    iput p1, p0, Lcom/android/launcher3/Workspace;->Wv:I

    return p1
.end method

.method public static synthetic b(Lcom/android/launcher3/Workspace;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Hw:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic b(Lcom/android/launcher3/Workspace;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    return-object v0
.end method

.method private b(FZ)V
    .locals 4

    .prologue
    .line 1542
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v0

    .line 1544
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Vq:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    .line 1549
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Vq:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1550
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/launcher3/Workspace;->Vq:Landroid/animation/ValueAnimator;

    .line 1552
    :cond_0
    invoke-virtual {v0}, Lcom/android/launcher3/DragLayer;->getBackgroundAlpha()F

    move-result v1

    .line 1553
    cmpl-float v2, p1, v1

    if-eqz v2, :cond_1

    .line 1554
    if-eqz p2, :cond_2

    .line 1555
    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v1, v2, v3

    const/4 v1, 0x1

    aput p1, v2, v1

    invoke-static {v2}, Lyr;->c([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher3/Workspace;->Vq:Landroid/animation/ValueAnimator;

    .line 1557
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Vq:Landroid/animation/ValueAnimator;

    new-instance v2, Lagm;

    invoke-direct {v2, p0, v0}, Lagm;-><init>(Lcom/android/launcher3/Workspace;Lcom/android/launcher3/DragLayer;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1563
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vq:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x3fc00000    # 1.5f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1564
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vq:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1565
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vq:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1570
    :cond_1
    :goto_0
    return-void

    .line 1567
    :cond_2
    invoke-virtual {v0, p1}, Lcom/android/launcher3/DragLayer;->setBackgroundAlpha(F)V

    goto :goto_0
.end method

.method public static synthetic b(Lcom/android/launcher3/Workspace;Z)V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/launcher3/Workspace;->as(Z)V

    return-void
.end method

.method public static synthetic c(Lcom/android/launcher3/Workspace;I)I
    .locals 0

    .prologue
    .line 88
    iput p1, p0, Lcom/android/launcher3/Workspace;->Ww:I

    return p1
.end method

.method public static synthetic c(Lcom/android/launcher3/Workspace;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    return-object v0
.end method

.method private c(Ljava/util/ArrayList;Lahz;)V
    .locals 7

    .prologue
    .line 4824
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 4825
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 4826
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrr;

    .line 4827
    iget-object v3, v0, Lrr;->xr:Landroid/content/ComponentName;

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4828
    iget-object v0, v0, Lrr;->xr:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 4830
    :cond_0
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 4832
    const/4 v6, 0x1

    new-instance v0, Lage;

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lage;-><init>(Lcom/android/launcher3/Workspace;Ljava/util/HashMap;Lahz;Ljava/util/HashSet;Ljava/util/HashSet;)V

    invoke-direct {p0, v6, v0}, Lcom/android/launcher3/Workspace;->a(ZLagt;)V

    .line 4909
    invoke-virtual {v5}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4910
    invoke-direct {p0, v5, p2}, Lcom/android/launcher3/Workspace;->a(Ljava/util/HashSet;Lahz;)V

    .line 4912
    :cond_1
    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v0

    invoke-virtual {p2, v0}, Lahz;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4913
    invoke-direct {p0, v4}, Lcom/android/launcher3/Workspace;->a(Ljava/util/Set;)V

    .line 4915
    :cond_2
    return-void
.end method

.method public static synthetic d(Lcom/android/launcher3/Workspace;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vz:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic e(Lcom/android/launcher3/Workspace;)Landroid/app/WallpaperManager;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->SQ:Landroid/app/WallpaperManager;

    return-object v0
.end method

.method private static e(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1987
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1988
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->copyBounds(Landroid/graphics/Rect;)V

    .line 1989
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-nez v1, :cond_2

    .line 1990
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 1994
    :goto_0
    instance-of v1, p0, Lcom/android/launcher3/PreloadIconDrawable;

    if-eqz v1, :cond_1

    .line 1995
    check-cast p0, Lcom/android/launcher3/PreloadIconDrawable;

    invoke-virtual {p0}, Lcom/android/launcher3/PreloadIconDrawable;->jX()I

    move-result v1

    neg-int v1, v1

    .line 1996
    invoke-virtual {v0, v1, v1}, Landroid/graphics/Rect;->inset(II)V

    .line 1998
    :cond_1
    return-object v0

    .line 1992
    :cond_2
    invoke-virtual {v0, v3, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    goto :goto_0
.end method

.method private et()V
    .locals 12

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1865
    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->VZ:Z

    if-eqz v0, :cond_2

    .line 1866
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v8

    .line 1867
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->QE:[I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->f([I)V

    .line 1868
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->QE:[I

    aget v1, v0, v6

    .line 1869
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->QE:[I

    aget v0, v0, v5

    .line 1870
    if-ne v1, v0, :cond_3

    .line 1872
    add-int/lit8 v2, v8, -0x1

    if-ge v0, v2, :cond_0

    .line 1873
    add-int/lit8 v0, v0, 0x1

    move v2, v0

    move v3, v1

    .line 1879
    :goto_0
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    const-wide/16 v10, -0x12d

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    move v7, v6

    .line 1880
    :goto_1
    if-ge v7, v8, :cond_2

    .line 1881
    invoke-virtual {p0, v7}, Lcom/android/launcher3/Workspace;->aS(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/CellLayout;

    .line 1885
    if-eq v1, v0, :cond_1

    if-gt v3, v7, :cond_1

    if-gt v7, v2, :cond_1

    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->aa(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v5

    .line 1887
    :goto_2
    invoke-virtual {v1, v4}, Lcom/android/launcher3/CellLayout;->E(Z)V

    .line 1880
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_1

    .line 1874
    :cond_0
    if-lez v1, :cond_3

    .line 1875
    add-int/lit8 v1, v1, -0x1

    move v2, v0

    move v3, v1

    goto :goto_0

    :cond_1
    move v4, v6

    .line 1885
    goto :goto_2

    .line 1890
    :cond_2
    return-void

    :cond_3
    move v2, v0

    move v3, v1

    goto :goto_0
.end method

.method public static synthetic f(Lcom/android/launcher3/Workspace;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->EC:Landroid/os/IBinder;

    return-object v0
.end method

.method public static synthetic g(Lcom/android/launcher3/Workspace;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->Wg:Z

    return v0
.end method

.method public static synthetic h(Lcom/android/launcher3/Workspace;)I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/android/launcher3/Workspace;->Wh:I

    return v0
.end method

.method public static synthetic i(Lcom/android/launcher3/Workspace;)F
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/android/launcher3/Workspace;->Wi:F

    return v0
.end method

.method private i(J)J
    .locals 3

    .prologue
    .line 562
    const-wide/16 v0, -0xc9

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/android/launcher3/Workspace;->a(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method private i(Lcom/android/launcher3/CellLayout;)J
    .locals 5

    .prologue
    .line 869
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 870
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 871
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 872
    iget-object v3, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-ne v3, p1, :cond_0

    .line 876
    :goto_0
    return-wide v0

    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private j(Lcom/android/launcher3/CellLayout;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 3312
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    if-eqz v0, :cond_0

    .line 3313
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->fd()V

    .line 3314
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->ff()V

    .line 3316
    :cond_0
    iput-object p1, p0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    .line 3317
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    if-eqz v0, :cond_1

    .line 3318
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->fe()V

    .line 3320
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/launcher3/Workspace;->au(Z)V

    .line 3321
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->lq()V

    .line 3322
    invoke-direct {p0, v1, v1}, Lcom/android/launcher3/Workspace;->T(II)V

    .line 3323
    return-void
.end method

.method public static synthetic j(Lcom/android/launcher3/Workspace;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->lo()V

    return-void
.end method

.method private k(Lcom/android/launcher3/CellLayout;)V
    .locals 2

    .prologue
    .line 3326
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VL:Lcom/android/launcher3/CellLayout;

    if-eqz v0, :cond_0

    .line 3327
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VL:Lcom/android/launcher3/CellLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CellLayout;->H(Z)V

    .line 3329
    :cond_0
    iput-object p1, p0, Lcom/android/launcher3/Workspace;->VL:Lcom/android/launcher3/CellLayout;

    .line 3330
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VL:Lcom/android/launcher3/CellLayout;

    if-eqz v0, :cond_1

    .line 3331
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VL:Lcom/android/launcher3/CellLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CellLayout;->H(Z)V

    .line 3333
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->invalidate()V

    .line 3334
    return-void
.end method

.method private static k(Luq;)Z
    .locals 1

    .prologue
    .line 3542
    iget-object v0, p0, Luq;->Ge:Ljava/lang/Object;

    instance-of v0, v0, Lyy;

    if-nez v0, :cond_0

    iget-object v0, p0, Luq;->Ge:Ljava/lang/Object;

    instance-of v0, v0, Lacy;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic k(Lcom/android/launcher3/Workspace;)[F
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->WE:[F

    return-object v0
.end method

.method private kJ()V
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 479
    return-void
.end method

.method private kZ()V
    .locals 5

    .prologue
    .line 1281
    new-instance v0, Lagl;

    invoke-direct {v0, p0}, Lagl;-><init>(Lcom/android/launcher3/Workspace;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Void;

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lagl;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1292
    return-void
.end method

.method public static synthetic l(Lcom/android/launcher3/Workspace;)[F
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->WG:[F

    return-object v0
.end method

.method private la()V
    .locals 4

    .prologue
    .line 1513
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lf()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->VW:Z

    if-nez v0, :cond_2

    .line 1514
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vn:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vn:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 1515
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vo:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vo:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 1516
    :cond_1
    const-string v0, "childrenOutlineAlpha"

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Vo:Landroid/animation/ObjectAnimator;

    .line 1517
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vo:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x177

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1518
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vo:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 1519
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vo:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1521
    :cond_2
    return-void
.end method

.method private ln()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2486
    iput-boolean v0, p0, Lcom/android/launcher3/Workspace;->VW:Z

    .line 2489
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->invalidate()V

    .line 2491
    invoke-direct {p0, v1}, Lcom/android/launcher3/Workspace;->as(Z)V

    .line 2492
    iget-object v2, p0, Lcom/android/launcher3/Workspace;->VV:Lagv;

    sget-object v3, Lagv;->Xv:Lagv;

    if-eq v2, v3, :cond_1

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lc()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    const-wide/16 v2, -0x12d

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CellLayout;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->kJ()V

    .line 2493
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 2492
    goto :goto_0
.end method

.method private lo()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2519
    iput-boolean v1, p0, Lcom/android/launcher3/Workspace;->VW:Z

    .line 2520
    invoke-direct {p0, v1}, Lcom/android/launcher3/Workspace;->as(Z)V

    .line 2521
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VV:Lagv;

    sget-object v2, Lagv;->Xv:Lagv;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lc()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    const-wide/16 v2, -0x12d

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CellLayout;->setVisibility(I)V

    .line 2522
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 2521
    goto :goto_0
.end method

.method private lp()Z
    .locals 2

    .prologue
    .line 2782
    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->VW:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/launcher3/Workspace;->WJ:F

    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VV:Lagv;

    sget-object v1, Lagv;->Xv:Lagv;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VV:Lagv;

    sget-object v1, Lagv;->Xx:Lagv;

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private lq()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3367
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Wn:Lvs;

    if-eqz v0, :cond_0

    .line 3368
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Wn:Lvs;

    invoke-virtual {v0}, Lvs;->gR()V

    .line 3369
    iput-object v1, p0, Lcom/android/launcher3/Workspace;->Wn:Lvs;

    .line 3371
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Wm:Lro;

    invoke-virtual {v0, v1}, Lro;->a(Lace;)V

    .line 3372
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Wm:Lro;

    invoke-virtual {v0}, Lro;->dV()V

    .line 3373
    return-void
.end method

.method private lr()V
    .locals 1

    .prologue
    .line 3376
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Wo:Lcom/android/launcher3/FolderIcon;

    if-eqz v0, :cond_0

    .line 3377
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Wo:Lcom/android/launcher3/FolderIcon;

    invoke-virtual {v0}, Lcom/android/launcher3/FolderIcon;->gN()V

    .line 3378
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Wo:Lcom/android/launcher3/FolderIcon;

    .line 3380
    :cond_0
    return-void
.end method

.method private lu()Lcom/android/launcher3/CellLayout;
    .locals 1

    .prologue
    .line 4127
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jg()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    return-object v0
.end method

.method private lx()Ljava/util/ArrayList;
    .locals 4

    .prologue
    .line 4543
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 4544
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v3

    .line 4545
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 4546
    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4545
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 4548
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 4549
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/Hotseat;->gW()Lcom/android/launcher3/CellLayout;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4551
    :cond_1
    return-object v2
.end method

.method public static synthetic lz()Z
    .locals 1

    .prologue
    .line 88
    sget-boolean v0, Lcom/android/launcher3/Workspace;->Vw:Z

    return v0
.end method

.method public static synthetic m(Lcom/android/launcher3/Workspace;)Lvs;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Wn:Lvs;

    return-object v0
.end method

.method public static synthetic n(Lcom/android/launcher3/Workspace;)[F
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VP:[F

    return-object v0
.end method

.method public static synthetic o(Lcom/android/launcher3/Workspace;)Lcom/android/launcher3/CellLayout;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    return-object v0
.end method

.method public static synthetic p(Lcom/android/launcher3/Workspace;)[I
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->He:[I

    return-object v0
.end method

.method public static synthetic q(Lcom/android/launcher3/Workspace;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Wb:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static synthetic r(Lcom/android/launcher3/Workspace;)Lty;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->yg:Lty;

    return-object v0
.end method

.method public static synthetic s(Lcom/android/launcher3/Workspace;)Lwi;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xn:Lwi;

    return-object v0
.end method


# virtual methods
.method public final P(Z)V
    .locals 1

    .prologue
    .line 4222
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/Workspace;->Hx:Z

    .line 4223
    iput-boolean p1, p0, Lcom/android/launcher3/Workspace;->Hy:Z

    .line 4224
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Hw:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 4225
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Hw:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 4227
    :cond_0
    return-void
.end method

.method final a(Lagv;ZLjava/util/ArrayList;)Landroid/animation/Animator;
    .locals 6

    .prologue
    .line 2053
    const/4 v3, 0x0

    const/4 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher3/Workspace;->a(Lagv;ZIILjava/util/ArrayList;)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lacw;Landroid/graphics/Bitmap;Z)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2030
    iget v0, p1, Lacw;->AY:I

    iget v1, p1, Lacw;->AZ:I

    invoke-virtual {p0, v0, v1, p1, v3}, Lcom/android/launcher3/Workspace;->a(IILwq;Z)[I

    move-result-object v0

    .line 2033
    const/4 v2, 0x2

    aget v3, v0, v3

    const/4 v1, 0x1

    aget v4, v0, v1

    move-object v0, p0

    move-object v1, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher3/Workspace;->a(Landroid/graphics/Bitmap;IIIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Wb:Landroid/graphics/Bitmap;

    .line 2034
    return-void
.end method

.method public final a(Landroid/view/View;JJIIII)V
    .locals 12

    .prologue
    .line 963
    const/4 v10, 0x0

    const/4 v11, 0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    invoke-direct/range {v0 .. v11}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;JJIIIIZZ)V

    .line 964
    return-void
.end method

.method final a(Landroid/view/View;JJIIIIZ)V
    .locals 12

    .prologue
    .line 969
    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;JJIIIIZZ)V

    .line 970
    return-void
.end method

.method public final a(Landroid/view/View;Lui;)V
    .locals 12

    .prologue
    const/4 v5, 0x2

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 2659
    invoke-virtual {p1}, Landroid/view/View;->clearFocus()V

    .line 2660
    invoke-virtual {p1, v6}, Landroid/view/View;->setPressed(Z)V

    .line 2663
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0057

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iget-object v3, p0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    invoke-virtual {v3, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v3, p0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    invoke-static {p1, v3, v5}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;Landroid/graphics/Canvas;I)V

    iget-object v3, p0, Lcom/android/launcher3/Workspace;->zr:Lwf;

    iget-object v4, p0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    invoke-virtual {v3, v2, v4, v1, v1}, Lwf;->a(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    iget-object v1, p0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    invoke-virtual {v1, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/android/launcher3/Workspace;->Wb:Landroid/graphics/Bitmap;

    .line 2665
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1, p1}, Lcom/android/launcher3/Launcher;->X(Landroid/view/View;)V

    .line 2667
    new-instance v4, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v4, v5}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 2668
    invoke-direct {p0, p1, v4}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;Ljava/util/concurrent/atomic/AtomicInteger;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2670
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 2671
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 2673
    iget-object v2, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v2

    iget-object v7, p0, Lcom/android/launcher3/Workspace;->Wc:[I

    invoke-virtual {v2, p1, v7}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/View;[I)F

    move-result v9

    .line 2674
    iget-object v2, p0, Lcom/android/launcher3/Workspace;->Wc:[I

    aget v2, v2, v6

    int-to-float v2, v2

    int-to-float v7, v5

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    div-float/2addr v7, v10

    sub-float/2addr v2, v7

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 2675
    iget-object v7, p0, Lcom/android/launcher3/Workspace;->Wc:[I

    const/4 v8, 0x1

    aget v7, v7, v8

    int-to-float v7, v7

    int-to-float v8, v3

    int-to-float v3, v3

    mul-float/2addr v3, v9

    sub-float v3, v8, v3

    div-float/2addr v3, v10

    sub-float v3, v7, v3

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    sub-float/2addr v3, v7

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 2678
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v7

    .line 2679
    iget-object v7, v7, Lyu;->Mk:Lur;

    invoke-virtual {v7}, Lur;->gl()Ltu;

    move-result-object v7

    .line 2682
    instance-of v8, p1, Lcom/android/launcher3/BubbleTextView;

    if-eqz v8, :cond_2

    .line 2683
    iget v0, v7, Ltu;->DI:I

    .line 2684
    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v7

    .line 2685
    sub-int/2addr v5, v0

    div-int/lit8 v5, v5, 0x2

    .line 2686
    add-int v10, v5, v0

    .line 2687
    add-int v11, v7, v0

    .line 2688
    add-int/2addr v3, v7

    .line 2691
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v8

    neg-int v8, v8

    div-int/lit8 v8, v8, 0x2

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    invoke-direct {v0, v8, v4}, Landroid/graphics/Point;-><init>(II)V

    .line 2692
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8, v5, v7, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v7, v0

    .line 2699
    :goto_0
    instance-of v0, p1, Lcom/android/launcher3/BubbleTextView;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 2700
    check-cast v0, Lcom/android/launcher3/BubbleTextView;

    .line 2701
    invoke-virtual {v0}, Lcom/android/launcher3/BubbleTextView;->eG()V

    .line 2704
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lwq;

    if-nez v0, :cond_3

    .line 2705
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Drag started with a view that has no tag set. This will cause a crash (issue 11627249) down the line. View: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  tag: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2708
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2693
    :cond_2
    instance-of v4, p1, Lcom/android/launcher3/FolderIcon;

    if-eqz v4, :cond_5

    .line 2694
    iget v4, v7, Ltu;->DS:I

    .line 2695
    new-instance v8, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v5

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v7

    invoke-direct {v8, v6, v5, v7, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v7, v0

    goto :goto_0

    .line 2711
    :cond_3
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->yg:Lty;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    move-object v4, p2

    invoke-virtual/range {v0 .. v9}, Lty;->a(Landroid/graphics/Bitmap;IILui;Ljava/lang/Object;ILandroid/graphics/Point;Landroid/graphics/Rect;F)Luj;

    move-result-object v0

    .line 2713
    invoke-interface {p2}, Lui;->eo()F

    move-result v2

    invoke-virtual {v0, v2}, Luj;->l(F)V

    .line 2715
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Ladg;

    if-eqz v0, :cond_4

    .line 2716
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Ladg;

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Vv:Ladg;

    .line 2719
    :cond_4
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 2720
    return-void

    :cond_5
    move-object v8, v0

    move-object v7, v0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Luq;ZZ)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 4169
    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->Hx:Z

    if-eqz v0, :cond_0

    .line 4170
    new-instance v0, Lafv;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lafv;-><init>(Lcom/android/launcher3/Workspace;Landroid/view/View;Luq;ZZ)V

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Hw:Ljava/lang/Runnable;

    .line 4214
    :goto_0
    return-void

    .line 4179
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Hw:Ljava/lang/Runnable;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    move v1, v0

    .line 4181
    :goto_1
    if-eqz p4, :cond_8

    if-eqz v1, :cond_1

    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->Hy:Z

    if-eqz v0, :cond_8

    .line 4182
    :cond_1
    if-eq p1, p0, :cond_3

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    if-eqz v0, :cond_3

    .line 4183
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget-object v0, v0, Lsy;->Ba:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->ag(Landroid/view/View;)Lcom/android/launcher3/CellLayout;

    move-result-object v0

    .line 4184
    if-eqz v0, :cond_7

    .line 4185
    iget-object v3, p0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget-object v3, v3, Lsy;->Ba:Landroid/view/View;

    invoke-virtual {v0, v3}, Lcom/android/launcher3/CellLayout;->removeView(Landroid/view/View;)V

    .line 4189
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget-object v0, v0, Lsy;->Ba:Landroid/view/View;

    instance-of v0, v0, Luo;

    if-eqz v0, :cond_3

    .line 4190
    iget-object v3, p0, Lcom/android/launcher3/Workspace;->yg:Lty;

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget-object v0, v0, Lsy;->Ba:Landroid/view/View;

    check-cast v0, Luo;

    invoke-virtual {v3, v0}, Lty;->c(Luo;)V

    .line 4208
    :cond_3
    :goto_2
    iget-boolean v0, p2, Luq;->AX:Z

    if-nez v0, :cond_4

    if-eqz v1, :cond_5

    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->Hy:Z

    if-nez v0, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget-object v0, v0, Lsy;->Ba:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 4210
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget-object v0, v0, Lsy;->Ba:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 4212
    :cond_5
    iput-object v6, p0, Lcom/android/launcher3/Workspace;->Wb:Landroid/graphics/Bitmap;

    .line 4213
    iput-object v6, p0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    goto :goto_0

    :cond_6
    move v1, v2

    .line 4179
    goto :goto_1

    .line 4186
    :cond_7
    invoke-static {}, Lyu;->isDogfoodBuild()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4187
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "mDragInfo.cell has null parent"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4193
    :cond_8
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    if-eqz v0, :cond_3

    .line 4195
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0, p1}, Lcom/android/launcher3/Launcher;->Y(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 4196
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/Hotseat;->gW()Lcom/android/launcher3/CellLayout;

    move-result-object v0

    .line 4200
    :goto_3
    if-nez v0, :cond_a

    invoke-static {}, Lyu;->isDogfoodBuild()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 4201
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid state: cellLayout == null in Workspace#onDropCompleted. Please file a bug. "

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4198
    :cond_9
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget-wide v4, v0, Lsy;->Bd:J

    invoke-virtual {p0, v4, v5}, Lcom/android/launcher3/Workspace;->j(J)Lcom/android/launcher3/CellLayout;

    move-result-object v0

    goto :goto_3

    .line 4204
    :cond_a
    if-eqz v0, :cond_3

    .line 4205
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget-object v0, v0, Lsy;->Ba:Landroid/view/View;

    invoke-static {v0}, Lcom/android/launcher3/CellLayout;->I(Landroid/view/View;)V

    goto :goto_2
.end method

.method public final a(Lcom/android/launcher3/Launcher;F)V
    .locals 0

    .prologue
    .line 2477
    iput p2, p0, Lcom/android/launcher3/Workspace;->WJ:F

    .line 2478
    return-void
.end method

.method public final a(Lcom/android/launcher3/Launcher;ZZ)V
    .locals 0

    .prologue
    .line 2468
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->ln()V

    .line 2469
    return-void
.end method

.method final a(Ljava/util/ArrayList;Lahz;)V
    .locals 8

    .prologue
    .line 4636
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 4637
    invoke-virtual {v2, p1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 4640
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 4641
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 4642
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->lx()Ljava/util/ArrayList;

    move-result-object v0

    .line 4643
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 4644
    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v6

    .line 4645
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v7

    .line 4646
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v7, :cond_0

    .line 4647
    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 4648
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwq;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 4646
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 4651
    :cond_1
    new-instance v0, Lagc;

    invoke-direct {v0, p0, v2, p2, v4}, Lagc;-><init>(Lcom/android/launcher3/Workspace;Ljava/util/HashSet;Lahz;Ljava/util/HashSet;)V

    .line 4663
    invoke-static {v3, v0}, Lzi;->a(Ljava/util/Collection;Laag;)Ljava/util/ArrayList;

    .line 4666
    invoke-direct {p0, v4, p2}, Lcom/android/launcher3/Workspace;->a(Ljava/util/HashSet;Lahz;)V

    .line 4667
    return-void
.end method

.method final a(Lsy;)V
    .locals 2

    .prologue
    .line 2643
    iget-object v1, p1, Lsy;->Ba:Landroid/view/View;

    .line 2646
    invoke-virtual {v1}, Landroid/view/View;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2656
    :goto_0
    return-void

    .line 2650
    :cond_0
    iput-object p1, p0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    .line 2651
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2652
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 2653
    invoke-virtual {v0, v1}, Lcom/android/launcher3/CellLayout;->H(Landroid/view/View;)V

    .line 2655
    invoke-virtual {p0, v1, p0}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;Lui;)V

    goto :goto_0
.end method

.method public final a(Lui;Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 398
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/launcher3/Workspace;->VY:Z

    .line 399
    invoke-direct {p0, v0}, Lcom/android/launcher3/Workspace;->as(Z)V

    .line 400
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->hT()V

    .line 401
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->hG()V

    move v1, v0

    .line 402
    :goto_0
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Lcom/android/launcher3/CellLayout;->i(F)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 404
    :cond_0
    invoke-static {}, Lcom/android/launcher3/InstallShortcutReceiver;->ha()V

    .line 405
    invoke-static {}, Lcom/android/launcher3/UninstallShortcutReceiver;->kl()V

    .line 406
    new-instance v0, Lafy;

    invoke-direct {v0, p0}, Lafy;-><init>(Lcom/android/launcher3/Workspace;)V

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->post(Ljava/lang/Runnable;)Z

    .line 415
    return-void
.end method

.method public final a(Luq;Landroid/graphics/PointF;)V
    .locals 0

    .prologue
    .line 4399
    return-void
.end method

.method public final a(Lwq;Lcom/android/launcher3/CellLayout;Luj;Ljava/lang/Runnable;ILandroid/view/View;Z)V
    .locals 28

    .prologue
    .line 4054
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 4055
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v4}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0, v7}, Lcom/android/launcher3/DragLayer;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 4057
    const/4 v4, 0x2

    new-array v6, v4, [I

    .line 4058
    const/4 v4, 0x2

    new-array v8, v4, [F

    .line 4059
    move-object/from16 v0, p1

    instance-of v4, v0, Lacx;

    if-nez v4, :cond_3

    const/4 v4, 0x1

    .line 4060
    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->He:[I

    move-object/from16 v0, p1

    iget v9, v0, Lwq;->AY:I

    move-object/from16 v0, p1

    iget v10, v0, Lwq;->AZ:I

    const/4 v11, 0x0

    aget v11, v5, v11

    const/4 v12, 0x1

    aget v5, v5, v12

    move-object/from16 v0, p2

    invoke-static {v0, v11, v5, v9, v10}, Lcom/android/launcher3/Workspace;->a(Lcom/android/launcher3/CellLayout;IIII)Landroid/graphics/Rect;

    move-result-object v9

    const/4 v5, 0x0

    iget v10, v9, Landroid/graphics/Rect;->left:I

    aput v10, v6, v5

    const/4 v5, 0x1

    iget v10, v9, Landroid/graphics/Rect;->top:I

    aput v10, v6, v5

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->ls()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v5}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v5

    const/4 v10, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v5, v0, v6, v10}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/View;[IZ)F

    move-result v10

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->lt()V

    if-eqz v4, :cond_4

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    invoke-virtual/range {p3 .. p3}, Luj;->getMeasuredWidth()I

    move-result v5

    int-to-float v5, v5

    div-float v5, v4, v5

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v11

    int-to-float v11, v11

    mul-float/2addr v4, v11

    invoke-virtual/range {p3 .. p3}, Luj;->getMeasuredHeight()I

    move-result v11

    int-to-float v11, v11

    div-float/2addr v4, v11

    :goto_1
    const/4 v11, 0x0

    aget v12, v6, v11

    int-to-float v12, v12

    invoke-virtual/range {p3 .. p3}, Luj;->getMeasuredWidth()I

    move-result v13

    int-to-float v13, v13

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v14

    int-to-float v14, v14

    mul-float/2addr v14, v10

    sub-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    sub-float/2addr v12, v13

    float-to-int v12, v12

    aput v12, v6, v11

    const/4 v11, 0x1

    aget v12, v6, v11

    int-to-float v12, v12

    invoke-virtual/range {p3 .. p3}, Luj;->getMeasuredHeight()I

    move-result v13

    int-to-float v13, v13

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v9, v10

    sub-float v9, v13, v9

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v9, v13

    sub-float v9, v12, v9

    float-to-int v9, v9

    aput v9, v6, v11

    const/4 v9, 0x0

    mul-float/2addr v5, v10

    aput v5, v8, v9

    const/4 v5, 0x1

    mul-float/2addr v4, v10

    aput v4, v8, v5

    .line 4063
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v4}, Lcom/android/launcher3/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 4064
    const v5, 0x7f0c0021

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    add-int/lit16 v12, v4, -0xc8

    .line 4067
    move-object/from16 v0, p6

    instance-of v4, v0, Landroid/appwidget/AppWidgetHostView;

    if-eqz v4, :cond_0

    if-eqz p7, :cond_0

    .line 4068
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v4}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-virtual {v4, v0}, Lcom/android/launcher3/DragLayer;->removeView(Landroid/view/View;)V

    .line 4071
    :cond_0
    const/4 v4, 0x2

    move/from16 v0, p5

    if-eq v0, v4, :cond_1

    if-eqz p7, :cond_5

    :cond_1
    if-eqz p6, :cond_5

    .line 4072
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v4}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v4

    move-object/from16 v0, p1

    iget v5, v0, Lwq;->AY:I

    move-object/from16 v0, p1

    iget v9, v0, Lwq;->AZ:I

    const/4 v10, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v4, v5, v9, v0, v10}, Lcom/android/launcher3/Workspace;->a(IILwq;Z)[I

    move-result-object v4

    invoke-virtual/range {p6 .. p6}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/4 v9, 0x0

    move-object/from16 v0, p6

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    const/4 v9, 0x0

    aget v9, v4, v9

    const/high16 v10, 0x40000000    # 2.0f

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    const/4 v10, 0x1

    aget v10, v4, v10

    const/high16 v11, 0x40000000    # 2.0f

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    const/4 v11, 0x0

    aget v11, v4, v11

    const/4 v13, 0x1

    aget v13, v4, v13

    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v13, v14}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    invoke-virtual {v13, v11}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v9, v10}, Landroid/view/View;->measure(II)V

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v13, 0x0

    aget v13, v4, v13

    const/4 v14, 0x1

    aget v4, v4, v14

    move-object/from16 v0, p6

    invoke-virtual {v0, v9, v10, v13, v4}, Landroid/view/View;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    const/4 v9, 0x0

    invoke-virtual {v4, v9}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 4073
    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Luj;->e(Landroid/graphics/Bitmap;)V

    .line 4074
    int-to-float v4, v12

    const v5, 0x3f4ccccd    # 0.8f

    mul-float/2addr v4, v5

    float-to-int v4, v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Luj;->bb(I)V

    .line 4079
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v4}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v13

    .line 4080
    const/4 v4, 0x4

    move/from16 v0, p5

    if-ne v0, v4, :cond_6

    .line 4081
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v4}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v4

    const/4 v7, 0x0

    const v8, 0x3dcccccd    # 0.1f

    const v9, 0x3dcccccd    # 0.1f

    const/4 v10, 0x0

    move-object/from16 v5, p3

    move-object/from16 v11, p4

    invoke-virtual/range {v4 .. v12}, Lcom/android/launcher3/DragLayer;->a(Luj;[IFFFILjava/lang/Runnable;I)V

    .line 4106
    :goto_3
    return-void

    .line 4059
    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 4060
    :cond_4
    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    goto/16 :goto_1

    .line 4075
    :cond_5
    move-object/from16 v0, p1

    iget v4, v0, Lwq;->Jz:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_2

    if-eqz p7, :cond_2

    .line 4076
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v9, 0x0

    aget v9, v8, v9

    const/4 v10, 0x1

    aget v10, v8, v10

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    aput v9, v8, v5

    aput v9, v8, v4

    goto :goto_2

    .line 4085
    :cond_6
    const/4 v4, 0x1

    move/from16 v0, p5

    if-ne v0, v4, :cond_7

    .line 4086
    const/16 v25, 0x2

    .line 4091
    :goto_4
    new-instance v24, Lafu;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    move-object/from16 v2, p6

    move-object/from16 v3, p4

    invoke-direct {v0, v1, v2, v3}, Lafu;-><init>(Lcom/android/launcher3/Workspace;Landroid/view/View;Ljava/lang/Runnable;)V

    .line 4102
    iget v15, v7, Landroid/graphics/Rect;->left:I

    iget v0, v7, Landroid/graphics/Rect;->top:I

    move/from16 v16, v0

    const/4 v4, 0x0

    aget v17, v6, v4

    const/4 v4, 0x1

    aget v18, v6, v4

    const/high16 v19, 0x3f800000    # 1.0f

    const/high16 v20, 0x3f800000    # 1.0f

    const/high16 v21, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    aget v22, v8, v4

    const/4 v4, 0x1

    aget v23, v8, v4

    move-object/from16 v14, p3

    move/from16 v26, v12

    move-object/from16 v27, p0

    invoke-virtual/range {v13 .. v27}, Lcom/android/launcher3/DragLayer;->a(Luj;IIIIFFFFFLjava/lang/Runnable;IILandroid/view/View;)V

    goto :goto_3

    .line 4088
    :cond_7
    const/16 v25, 0x0

    goto :goto_4
.end method

.method public final a(ZLjava/lang/Runnable;IZ)V
    .locals 10

    .prologue
    const/16 v9, 0x190

    const/16 v8, 0x96

    const-wide/16 v6, -0xc9

    const/4 v5, 0x1

    .line 747
    const-string v0, "Launcher.Workspace"

    const-string v1, "11683562 - removeExtraEmptyScreen()"

    invoke-static {v0, v1, v5}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 748
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hz()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 750
    const-string v0, "Launcher.Workspace"

    const-string v1, "    - workspace loading, skip"

    invoke-static {v0, v1, v5}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 785
    :cond_0
    :goto_0
    return-void

    .line 754
    :cond_1
    if-lez p3, :cond_2

    .line 755
    new-instance v0, Lagi;

    invoke-direct {v0, p0, p1, p2, p4}, Lagi;-><init>(Lcom/android/launcher3/Workspace;ZLjava/lang/Runnable;Z)V

    int-to-long v2, p3

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/launcher3/Workspace;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 764
    :cond_2
    const-string v0, "Launcher.Workspace"

    const-string v1, "11683562 - convertFinalScreenToEmptyScreenIfNecessary()"

    invoke-static {v0, v1, v5}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hz()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "Launcher.Workspace"

    const-string v1, "    - workspace loading, skip"

    invoke-static {v0, v1, v5}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 765
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->kR()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 766
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 767
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jg()I

    move-result v1

    if-ne v1, v0, :cond_5

    .line 768
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jg()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0, v9}, Lcom/android/launcher3/Workspace;->P(II)V

    .line 769
    invoke-direct {p0, v9, v8, p2, p4}, Lcom/android/launcher3/Workspace;->a(IILjava/lang/Runnable;Z)V

    goto :goto_0

    .line 764
    :cond_4
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->kR()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v0, -0x12d

    cmp-long v0, v2, v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v1

    invoke-virtual {v1}, Ladg;->getChildCount()I

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eN()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hs()Lzi;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v4, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v4}, Lzi;->d(Landroid/content/Context;Ljava/util/ArrayList;)V

    const-string v0, "Launcher.Workspace"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "11683562 -   extra empty screen: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 772
    :cond_5
    const/4 v0, 0x0

    invoke-direct {p0, v0, v8, p2, p4}, Lcom/android/launcher3/Workspace;->a(IILjava/lang/Runnable;Z)V

    goto/16 :goto_0

    .line 776
    :cond_6
    if-eqz p4, :cond_7

    .line 779
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->kU()V

    .line 782
    :cond_7
    if-eqz p2, :cond_0

    .line 783
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_0
.end method

.method final a(Landroid/view/View;JLcom/android/launcher3/CellLayout;[IFZLuj;Ljava/lang/Runnable;)Z
    .locals 16

    .prologue
    .line 2928
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher3/Workspace;->Wr:F

    cmpl-float v2, p6, v2

    if-lez v2, :cond_0

    const/4 v2, 0x0

    .line 2975
    :goto_0
    return v2

    .line 2929
    :cond_0
    const/4 v2, 0x0

    aget v2, p5, v2

    const/4 v3, 0x1

    aget v3, p5, v3

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Lcom/android/launcher3/CellLayout;->C(II)Landroid/view/View;

    move-result-object v12

    .line 2931
    const/4 v2, 0x0

    .line 2932
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    if-eqz v3, :cond_1

    .line 2933
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget-object v2, v2, Lsy;->Ba:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Workspace;->ag(Landroid/view/View;)Lcom/android/launcher3/CellLayout;

    move-result-object v2

    .line 2934
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget v3, v3, Lsy;->Bb:I

    const/4 v4, 0x0

    aget v4, p5, v4

    if-ne v3, v4, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget v3, v3, Lsy;->Bc:I

    const/4 v4, 0x1

    aget v4, p5, v4

    if-ne v3, v4, :cond_3

    move-object/from16 v0, p4

    if-ne v2, v0, :cond_3

    const/4 v2, 0x1

    .line 2938
    :cond_1
    :goto_1
    if-eqz v12, :cond_2

    if-nez v2, :cond_2

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/launcher3/Workspace;->Wp:Z

    if-nez v2, :cond_4

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 2934
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 2939
    :cond_4
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/launcher3/Workspace;->Wp:Z

    .line 2940
    if-nez p5, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget-wide v6, v2, Lsy;->Bd:J

    .line 2942
    :goto_2
    invoke-virtual {v12}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Ladh;

    .line 2943
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Ladh;

    .line 2945
    if-eqz v2, :cond_9

    if-eqz v3, :cond_9

    .line 2946
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Ladh;

    .line 2947
    invoke-virtual {v12}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    move-object v11, v2

    check-cast v11, Ladh;

    .line 2949
    if-nez p7, :cond_5

    .line 2950
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget-object v2, v2, Lsy;->Ba:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Workspace;->ag(Landroid/view/View;)Lcom/android/launcher3/CellLayout;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget-object v3, v3, Lsy;->Ba:Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/android/launcher3/CellLayout;->removeView(Landroid/view/View;)V

    .line 2953
    :cond_5
    new-instance v13, Landroid/graphics/Rect;

    invoke-direct {v13}, Landroid/graphics/Rect;-><init>()V

    .line 2954
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    move-result v14

    .line 2955
    move-object/from16 v0, p4

    invoke-virtual {v0, v12}, Lcom/android/launcher3/CellLayout;->removeView(Landroid/view/View;)V

    .line 2957
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    const/4 v3, 0x0

    aget v8, p5, v3

    const/4 v3, 0x1

    aget v9, p5, v3

    move-object/from16 v3, p4

    move-wide/from16 v4, p2

    invoke-virtual/range {v2 .. v9}, Lcom/android/launcher3/Launcher;->a(Lcom/android/launcher3/CellLayout;JJII)Lcom/android/launcher3/FolderIcon;

    move-result-object v2

    .line 2959
    const/4 v3, -0x1

    iput v3, v11, Ladh;->Bb:I

    .line 2960
    const/4 v3, -0x1

    iput v3, v11, Ladh;->Bc:I

    .line 2961
    const/4 v3, -0x1

    iput v3, v10, Ladh;->Bb:I

    .line 2962
    const/4 v3, -0x1

    iput v3, v10, Ladh;->Bc:I

    .line 2965
    if-eqz p8, :cond_7

    const/4 v3, 0x1

    .line 2966
    :goto_3
    if-eqz v3, :cond_8

    move-object v3, v11

    move-object v4, v12

    move-object v5, v10

    move-object/from16 v6, p8

    move-object v7, v13

    move v8, v14

    move-object/from16 v9, p9

    .line 2967
    invoke-virtual/range {v2 .. v9}, Lcom/android/launcher3/FolderIcon;->a(Ladh;Landroid/view/View;Ladh;Luj;Landroid/graphics/Rect;FLjava/lang/Runnable;)V

    .line 2973
    :goto_4
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2940
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v1}, Lcom/android/launcher3/Workspace;->i(Lcom/android/launcher3/CellLayout;)J

    move-result-wide v6

    goto/16 :goto_2

    .line 2965
    :cond_7
    const/4 v3, 0x0

    goto :goto_3

    .line 2970
    :cond_8
    invoke-virtual {v2, v11}, Lcom/android/launcher3/FolderIcon;->i(Ladh;)V

    .line 2971
    invoke-virtual {v2, v10}, Lcom/android/launcher3/FolderIcon;->i(Ladh;)V

    goto :goto_4

    .line 2975
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method final a(Lcom/android/launcher3/CellLayout;[IFLuq;Z)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2980
    iget v0, p0, Lcom/android/launcher3/Workspace;->Wr:F

    cmpl-float v0, p3, v0

    if-lez v0, :cond_0

    move v0, v1

    .line 2998
    :goto_0
    return v0

    .line 2982
    :cond_0
    aget v0, p2, v1

    aget v3, p2, v2

    invoke-virtual {p1, v0, v3}, Lcom/android/launcher3/CellLayout;->C(II)Landroid/view/View;

    move-result-object v0

    .line 2983
    iget-boolean v3, p0, Lcom/android/launcher3/Workspace;->Wq:Z

    if-nez v3, :cond_1

    move v0, v1

    goto :goto_0

    .line 2984
    :cond_1
    iput-boolean v1, p0, Lcom/android/launcher3/Workspace;->Wq:Z

    .line 2986
    instance-of v3, v0, Lcom/android/launcher3/FolderIcon;

    if-eqz v3, :cond_3

    .line 2987
    check-cast v0, Lcom/android/launcher3/FolderIcon;

    .line 2988
    iget-object v3, p4, Luq;->Ge:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Lcom/android/launcher3/FolderIcon;->aa(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2989
    invoke-virtual {v0, p4}, Lcom/android/launcher3/FolderIcon;->b(Luq;)V

    .line 2992
    if-nez p5, :cond_2

    .line 2993
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget-object v0, v0, Lsy;->Ba:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->ag(Landroid/view/View;)Lcom/android/launcher3/CellLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget-object v1, v1, Lsy;->Ba:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CellLayout;->removeView(Landroid/view/View;)V

    :cond_2
    move v0, v2

    .line 2995
    goto :goto_0

    :cond_3
    move v0, v1

    .line 2998
    goto :goto_0
.end method

.method public final a(Luq;)Z
    .locals 19

    .prologue
    .line 2791
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher3/Workspace;->VM:Lcom/android/launcher3/CellLayout;

    move-object/from16 v18, v0

    .line 2792
    move-object/from16 v0, p1

    iget-object v2, v0, Luq;->Gf:Lui;

    move-object/from16 v0, p0

    if-eq v2, v0, :cond_8

    .line 2794
    if-nez v18, :cond_0

    .line 2795
    const/4 v2, 0x0

    .line 2872
    :goto_0
    return v2

    .line 2797
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->lp()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    .line 2799
    :cond_1
    move-object/from16 v0, p1

    iget v3, v0, Luq;->x:I

    move-object/from16 v0, p1

    iget v4, v0, Luq;->y:I

    move-object/from16 v0, p1

    iget v5, v0, Luq;->Ga:I

    move-object/from16 v0, p1

    iget v6, v0, Luq;->Gb:I

    move-object/from16 v0, p1

    iget-object v7, v0, Luq;->Gd:Luj;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher3/Workspace;->VP:[F

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/android/launcher3/Workspace;->a(IIIILuj;[F)[F

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/launcher3/Workspace;->VP:[F

    .line 2803
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/android/launcher3/Launcher;->Y(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2804
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->VP:[F

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/launcher3/Workspace;->a(Lcom/android/launcher3/Hotseat;[F)V

    .line 2809
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    if-eqz v2, :cond_3

    .line 2812
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    .line 2813
    iget v2, v3, Lsy;->AY:I

    .line 2814
    iget v12, v3, Lsy;->AZ:I

    move/from16 v17, v2

    .line 2823
    :goto_2
    move-object/from16 v0, p1

    iget-object v2, v0, Luq;->Ge:Ljava/lang/Object;

    instance-of v2, v2, Lacy;

    if-eqz v2, :cond_a

    .line 2824
    move-object/from16 v0, p1

    iget-object v2, v0, Luq;->Ge:Ljava/lang/Object;

    check-cast v2, Lacy;

    iget v4, v2, Lacy;->JB:I

    .line 2825
    move-object/from16 v0, p1

    iget-object v2, v0, Luq;->Ge:Ljava/lang/Object;

    check-cast v2, Lacy;

    iget v5, v2, Lacy;->JC:I

    .line 2828
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    float-to-int v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v6, 0x1

    aget v3, v3, v6

    float-to-int v3, v3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher3/Workspace;->He:[I

    move-object/from16 v6, v18

    invoke-static/range {v2 .. v7}, Lcom/android/launcher3/Workspace;->a(IIIILcom/android/launcher3/CellLayout;[I)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/launcher3/Workspace;->He:[I

    .line 2831
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v6, 0x1

    aget v3, v3, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Workspace;->He:[I

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v3, v6}, Lcom/android/launcher3/CellLayout;->a(FF[I)F

    move-result v10

    .line 2833
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/launcher3/Workspace;->Wp:Z

    if-eqz v2, :cond_4

    move-object/from16 v0, p1

    iget-object v7, v0, Luq;->Ge:Ljava/lang/Object;

    check-cast v7, Lwq;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v11, 0x1

    move-object/from16 v6, p0

    move-object/from16 v8, v18

    invoke-direct/range {v6 .. v11}, Lcom/android/launcher3/Workspace;->a(Lwq;Lcom/android/launcher3/CellLayout;[IFZ)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2835
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2806
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VP:[F

    move-object/from16 v0, v18

    invoke-static {v0, v2}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;[F)V

    goto/16 :goto_1

    .line 2816
    :cond_3
    move-object/from16 v0, p1

    iget-object v2, v0, Luq;->Ge:Ljava/lang/Object;

    check-cast v2, Lwq;

    .line 2817
    iget v3, v2, Lwq;->AY:I

    .line 2818
    iget v12, v2, Lwq;->AZ:I

    move/from16 v17, v3

    goto/16 :goto_2

    .line 2838
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/launcher3/Workspace;->Wq:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p1

    iget-object v2, v0, Luq;->Ge:Ljava/lang/Object;

    check-cast v2, Lwq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->He:[I

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v2, v1, v3, v10}, Lcom/android/launcher3/Workspace;->a(Ljava/lang/Object;Lcom/android/launcher3/CellLayout;[IF)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2840
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2843
    :cond_5
    const/4 v2, 0x2

    new-array v15, v2, [I

    .line 2844
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    float-to-int v7, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    float-to-int v8, v2

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/16 v16, 0x4

    move-object/from16 v6, v18

    move v9, v4

    move v10, v5

    move/from16 v11, v17

    invoke-virtual/range {v6 .. v16}, Lcom/android/launcher3/CellLayout;->a(IIIIIILandroid/view/View;[I[II)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/launcher3/Workspace;->He:[I

    .line 2847
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    if-ltz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    if-ltz v2, :cond_6

    const/4 v2, 0x1

    .line 2850
    :goto_4
    if-nez v2, :cond_8

    .line 2853
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/android/launcher3/Launcher;->Y(Landroid/view/View;)Z

    move-result v2

    .line 2854
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->He:[I

    if-eqz v3, :cond_7

    if-eqz v2, :cond_7

    .line 2855
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v3

    .line 2856
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/android/launcher3/Hotseat;->K(II)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/android/launcher3/Hotseat;->bi(I)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2858
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2847
    :cond_6
    const/4 v2, 0x0

    goto :goto_4

    .line 2862
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v3, v2}, Lcom/android/launcher3/Launcher;->S(Z)V

    .line 2863
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2867
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/launcher3/Workspace;->i(Lcom/android/launcher3/CellLayout;)J

    move-result-wide v2

    .line 2868
    const-wide/16 v4, -0xc9

    cmp-long v2, v2, v4

    if-nez v2, :cond_9

    .line 2869
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->kS()J

    .line 2872
    :cond_9
    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_a
    move v5, v12

    move/from16 v4, v17

    goto/16 :goto_3
.end method

.method public final a(IILwq;Z)[I
    .locals 5

    .prologue
    const v2, 0x7fffffff

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 371
    const/4 v0, 0x2

    new-array v1, v0, [I

    .line 372
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 374
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->ld()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 375
    invoke-static {v0, v3, v3, p1, p2}, Lcom/android/launcher3/Workspace;->a(Lcom/android/launcher3/CellLayout;IIII)Landroid/graphics/Rect;

    move-result-object v0

    .line 376
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    aput v2, v1, v3

    .line 377
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    aput v0, v1, v4

    .line 378
    if-eqz p4, :cond_0

    .line 379
    aget v0, v1, v3

    int-to-float v0, v0

    iget v2, p0, Lcom/android/launcher3/Workspace;->VT:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    aput v0, v1, v3

    .line 380
    aget v0, v1, v4

    int-to-float v0, v0

    iget v2, p0, Lcom/android/launcher3/Workspace;->VT:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    aput v0, v1, v4

    :cond_0
    move-object v0, v1

    .line 386
    :goto_0
    return-object v0

    .line 384
    :cond_1
    aput v2, v1, v3

    .line 385
    aput v2, v1, v4

    move-object v0, v1

    .line 386
    goto :goto_0
.end method

.method protected final aU(I)V
    .locals 14

    .prologue
    const-wide/16 v12, -0x12d

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 1683
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jc()Z

    move-result v6

    .line 1684
    invoke-super {p0, p1}, Ladi;->aU(I)V

    .line 1686
    iget v0, p0, Lcom/android/launcher3/Workspace;->QH:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/android/launcher3/Workspace;->QH:I

    iget v3, p0, Lcom/android/launcher3/Workspace;->Qg:I

    if-le v0, v3, :cond_6

    :cond_0
    move v0, v2

    :goto_0
    iget-boolean v3, p0, Lcom/android/launcher3/Workspace;->We:Z

    if-eqz v3, :cond_8

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lf()Z

    move-result v3

    if-nez v3, :cond_8

    iget-boolean v3, p0, Lcom/android/launcher3/Workspace;->VW:Z

    if-nez v3, :cond_8

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->ld()I

    move-result v0

    move v4, v0

    :goto_1
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v0

    if-ge v4, v0, :cond_8

    invoke-virtual {p0, v4}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {p0, v4}, Lcom/android/launcher3/PagedView;->bz(I)I

    move-result v7

    add-int/2addr v3, v7

    sub-int v7, p1, v3

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->getChildCount()I

    move-result v8

    add-int/lit8 v3, v4, 0x1

    if-gez v7, :cond_1

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jc()Z

    move-result v9

    if-eqz v9, :cond_2

    :cond_1
    if-lez v7, :cond_3

    invoke-virtual {p0}, Lcom/android/launcher3/PagedView;->jc()Z

    move-result v9

    if-eqz v9, :cond_3

    :cond_2
    add-int/lit8 v3, v4, -0x1

    :cond_3
    if-ltz v3, :cond_4

    add-int/lit8 v8, v8, -0x1

    if-le v3, v8, :cond_7

    :cond_4
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iget v8, p0, Lcom/android/launcher3/PagedView;->Qj:I

    add-int/2addr v3, v8

    :goto_2
    int-to-float v7, v7

    int-to-float v3, v3

    mul-float/2addr v3, v10

    div-float v3, v7, v3

    invoke-static {v3, v10}, Ljava/lang/Math;->min(FF)F

    move-result v3

    const/high16 v7, -0x40800000    # -1.0f

    invoke-static {v3, v7}, Ljava/lang/Math;->max(FF)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    sub-float v3, v10, v3

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v0

    invoke-virtual {v0, v3}, Ladg;->setAlpha(F)V

    :cond_5
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    invoke-virtual {p0, v3}, Lcom/android/launcher3/PagedView;->bz(I)I

    move-result v3

    invoke-virtual {p0, v4}, Lcom/android/launcher3/PagedView;->bz(I)I

    move-result v8

    sub-int/2addr v3, v8

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    goto :goto_2

    .line 1687
    :cond_8
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lc()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getScrollX()I

    move-result v3

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->bz(I)I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->bA(I)I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, v4}, Lcom/android/launcher3/Workspace;->bz(I)I

    move-result v4

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->bz(I)I

    move-result v0

    sub-int v0, v4, v0

    int-to-float v0, v0

    int-to-float v3, v3

    sub-float v3, v0, v3

    div-float v4, v3, v0

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jc()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-static {v5, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    :goto_3
    invoke-static {v5, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    move v4, v0

    :goto_4
    iget v0, p0, Lcom/android/launcher3/Workspace;->VI:F

    invoke-static {v3, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    cmpl-float v7, v3, v5

    if-lez v7, :cond_9

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->getVisibility()I

    move-result v7

    if-eqz v7, :cond_9

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lf()Z

    move-result v7

    if-nez v7, :cond_9

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CellLayout;->setVisibility(I)V

    :cond_9
    iput v3, p0, Lcom/android/launcher3/Workspace;->VI:F

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v0

    const v7, 0x3f4ccccd    # 0.8f

    mul-float/2addr v7, v3

    invoke-virtual {v0, v7}, Lcom/android/launcher3/DragLayer;->setBackgroundAlpha(F)V

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/android/launcher3/Hotseat;->setTranslationX(F)V

    :cond_a
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    invoke-virtual {v0, v4}, Lcom/android/launcher3/PageIndicator;->setTranslationX(F)V

    :cond_b
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VG:Lym;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VG:Lym;

    invoke-interface {v0, v3}, Lym;->m(F)V

    .line 1688
    :cond_c
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->et()V

    .line 1690
    iget v0, p0, Lcom/android/launcher3/Workspace;->QH:I

    if-ltz v0, :cond_d

    iget v0, p0, Lcom/android/launcher3/Workspace;->QH:I

    iget v3, p0, Lcom/android/launcher3/Workspace;->Qg:I

    if-le v0, v3, :cond_12

    :cond_d
    move v0, v2

    .line 1692
    :goto_5
    if-eqz v0, :cond_15

    .line 1693
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1697
    iget v3, p0, Lcom/android/launcher3/Workspace;->QH:I

    if-gez v3, :cond_13

    move v3, v2

    .line 1698
    :goto_6
    if-nez v6, :cond_e

    if-nez v3, :cond_f

    :cond_e
    if-eqz v6, :cond_14

    if-nez v3, :cond_14

    .line 1700
    :cond_f
    :goto_7
    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 1701
    iget v1, p0, Lcom/android/launcher3/Workspace;->WK:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 1702
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-virtual {v0, v1, v3}, Lcom/android/launcher3/CellLayout;->a(FZ)V

    .line 1704
    iput-boolean v2, p0, Lcom/android/launcher3/Workspace;->Wd:Z

    .line 1712
    :cond_10
    :goto_8
    return-void

    .line 1687
    :cond_11
    invoke-static {v5, v3}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto/16 :goto_3

    :cond_12
    move v0, v1

    .line 1690
    goto :goto_5

    :cond_13
    move v3, v1

    .line 1697
    goto :goto_6

    :cond_14
    move v1, v0

    .line 1698
    goto :goto_7

    .line 1706
    :cond_15
    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->Wd:Z

    if-eqz v0, :cond_10

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v0

    if-lez v0, :cond_10

    .line 1707
    iput-boolean v1, p0, Lcom/android/launcher3/Workspace;->Wd:Z

    .line 1708
    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0, v5, v1}, Lcom/android/launcher3/CellLayout;->a(FZ)V

    .line 1709
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0, v5, v1}, Lcom/android/launcher3/CellLayout;->a(FZ)V

    goto :goto_8

    :cond_16
    move v3, v5

    move v4, v5

    goto/16 :goto_4
.end method

.method protected final aa(Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 502
    move-object v0, p1

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 503
    invoke-super {p0, p1}, Ladi;->aa(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/android/launcher3/Workspace;->VW:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v1

    invoke-virtual {v1}, Ladg;->getAlpha()F

    move-result v1

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->getBackgroundAlpha()F

    move-result v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 1

    .prologue
    .line 1802
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hI()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1803
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->kK()Lcom/android/launcher3/Folder;

    move-result-object v0

    .line 1804
    if-eqz v0, :cond_1

    .line 1805
    invoke-virtual {v0, p1, p2}, Lcom/android/launcher3/Folder;->addFocusables(Ljava/util/ArrayList;I)V

    .line 1810
    :cond_0
    :goto_0
    return-void

    .line 1807
    :cond_1
    invoke-super {p0, p1, p2, p3}, Ladi;->addFocusables(Ljava/util/ArrayList;II)V

    goto :goto_0
.end method

.method public final ae(Ljava/lang/Object;)Lcom/android/launcher3/Folder;
    .locals 1

    .prologue
    .line 4573
    new-instance v0, Lafw;

    invoke-direct {v0, p0, p1}, Lafw;-><init>(Lcom/android/launcher3/Workspace;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/android/launcher3/Workspace;->a(Lagt;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/Folder;

    return-object v0
.end method

.method public final af(Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 4584
    new-instance v0, Lafx;

    invoke-direct {v0, p0, p1}, Lafx;-><init>(Lcom/android/launcher3/Workspace;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/android/launcher3/Workspace;->a(Lagt;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final af(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2003
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 2004
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    .line 2005
    iget v3, v0, Ltu;->DI:I

    .line 2006
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 2007
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 2010
    instance-of v2, p1, Landroid/widget/TextView;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 2011
    check-cast v0, Landroid/widget/TextView;

    .line 2012
    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v5

    .line 2013
    invoke-static {v0}, Lcom/android/launcher3/Workspace;->e(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;

    move-result-object v0

    .line 2014
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 2015
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 2019
    :cond_0
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v0, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2021
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2022
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;Landroid/graphics/Canvas;I)V

    .line 2023
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2026
    const/4 v2, 0x2

    move-object v0, p0

    move v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher3/Workspace;->a(Landroid/graphics/Bitmap;IIIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Wb:Landroid/graphics/Bitmap;

    .line 2027
    return-void
.end method

.method final ag(Landroid/view/View;)Lcom/android/launcher3/CellLayout;
    .locals 3

    .prologue
    .line 4530
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->lx()Ljava/util/ArrayList;

    move-result-object v0

    .line 4531
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 4532
    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v2

    invoke-virtual {v2, p1}, Ladg;->indexOfChild(Landroid/view/View;)I

    move-result v2

    if-ltz v2, :cond_0

    .line 4536
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public announceForAccessibility(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1497
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hI()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1498
    invoke-super {p0, p1}, Ladi;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 1500
    :cond_0
    return-void
.end method

.method public final at(Z)V
    .locals 2

    .prologue
    .line 2111
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0, p1}, Lcom/android/launcher3/Workspace;->a(ZIZ)V

    .line 2112
    return-void
.end method

.method public final av(Z)V
    .locals 2

    .prologue
    .line 5051
    iget v0, p0, Lcom/android/launcher3/Workspace;->Vu:I

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lf()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p1, :cond_2

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->bB(I)V

    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 5052
    :cond_1
    return-void

    .line 5051
    :cond_2
    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->bu(I)V

    goto :goto_0
.end method

.method public final aw(Z)V
    .locals 2

    .prologue
    .line 5055
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lc()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5056
    const-wide/16 v0, -0x12d

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/Workspace;->k(J)I

    move-result v0

    .line 5057
    if-eqz p1, :cond_1

    .line 5058
    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->bB(I)V

    .line 5062
    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 5063
    if-eqz v0, :cond_0

    .line 5064
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 5067
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lh()V

    .line 5068
    return-void

    .line 5060
    :cond_1
    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->bu(I)V

    goto :goto_0
.end method

.method public final b(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 356
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->yW:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 358
    const-wide/16 v0, -0x12d

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher3/Workspace;->j(J)Lcom/android/launcher3/CellLayout;

    move-result-object v0

    .line 359
    if-eqz v0, :cond_0

    .line 360
    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 361
    instance-of v1, v0, Lwm;

    if-eqz v1, :cond_0

    .line 362
    check-cast v0, Lwm;

    iget-object v1, p0, Lcom/android/launcher3/Workspace;->yW:Landroid/graphics/Rect;

    invoke-interface {v0, v1}, Lwm;->b(Landroid/graphics/Rect;)V

    .line 365
    :cond_0
    return-void
.end method

.method public final b(Landroid/view/View;Lui;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v6, 0x0

    .line 2723
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 2724
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    .line 2725
    iget v0, v0, Ltu;->DI:I

    .line 2728
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1, p1}, Lcom/android/launcher3/Launcher;->X(Landroid/view/View;)V

    .line 2731
    new-instance v4, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x2

    invoke-direct {v4, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 2732
    invoke-direct {p0, p1, v4}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;Ljava/util/concurrent/atomic/AtomicInteger;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 2733
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2734
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 2735
    invoke-virtual {v2, v11}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 2736
    iget-object v3, p0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    invoke-virtual {v3, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2737
    iget-object v3, p0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    new-instance v5, Landroid/graphics/Rect;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-direct {v5, v6, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7, v6, v6, v0, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v10, v5, v7, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2739
    iget-object v2, p0, Lcom/android/launcher3/Workspace;->IL:Landroid/graphics/Canvas;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2742
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 2743
    int-to-float v3, v2

    int-to-float v5, v0

    div-float/2addr v3, v5

    .line 2744
    iget-object v5, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v5}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v5

    iget-object v7, p0, Lcom/android/launcher3/Workspace;->Wc:[I

    invoke-virtual {v5, p1, v7}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/View;[I)F

    move-result v5

    mul-float v9, v5, v3

    .line 2745
    iget-object v3, p0, Lcom/android/launcher3/Workspace;->Wc:[I

    aget v3, v3, v6

    int-to-float v3, v3

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v9

    sub-float/2addr v2, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v2, v5

    sub-float v2, v3, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 2746
    iget-object v3, p0, Lcom/android/launcher3/Workspace;->Wc:[I

    aget v3, v3, v11

    int-to-float v3, v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 2750
    new-instance v7, Landroid/graphics/Point;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v5

    neg-int v5, v5

    div-int/lit8 v5, v5, 0x2

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    invoke-direct {v7, v5, v4}, Landroid/graphics/Point;-><init>(II)V

    .line 2751
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8, v6, v6, v0, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2753
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lwq;

    if-nez v0, :cond_1

    .line 2754
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Drag started with a view that has no tag set. This will cause a crash (issue 11627249) down the line. View: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  tag: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2757
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2761
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->yg:Lty;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    move-object v4, p2

    invoke-virtual/range {v0 .. v9}, Lty;->a(Landroid/graphics/Bitmap;IILui;Ljava/lang/Object;ILandroid/graphics/Point;Landroid/graphics/Rect;F)Luj;

    move-result-object v0

    .line 2763
    invoke-interface {p2}, Lui;->eo()F

    move-result v1

    invoke-virtual {v0, v1}, Luj;->l(F)V

    .line 2766
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->recycle()V

    .line 2767
    return-void
.end method

.method public final b(Landroid/view/View;Lym;Ljava/lang/String;)V
    .locals 8

    .prologue
    const-wide/16 v6, -0x12d

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 641
    invoke-virtual {p0, v6, v7}, Lcom/android/launcher3/Workspace;->k(J)I

    move-result v0

    if-gez v0, :cond_0

    .line 642
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expected custom content screen to exist"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 646
    :cond_0
    invoke-virtual {p0, v6, v7}, Lcom/android/launcher3/Workspace;->j(J)Lcom/android/launcher3/CellLayout;

    move-result-object v0

    .line 647
    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eT()I

    move-result v1

    .line 648
    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eU()I

    move-result v3

    .line 649
    new-instance v4, Lcom/android/launcher3/CellLayout$LayoutParams;

    invoke-direct {v4, v2, v2, v1, v3}, Lcom/android/launcher3/CellLayout$LayoutParams;-><init>(IIII)V

    .line 650
    iput-boolean v2, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bu:Z

    .line 651
    iput-boolean v5, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bt:Z

    .line 652
    instance-of v1, p1, Lwm;

    if-eqz v1, :cond_1

    move-object v1, p1

    .line 653
    check-cast v1, Lwm;

    iget-object v3, p0, Lcom/android/launcher3/Workspace;->yW:Landroid/graphics/Rect;

    invoke-interface {v1, v3}, Lwm;->b(Landroid/graphics/Rect;)V

    .line 657
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_2

    .line 658
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 659
    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 661
    :cond_2
    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->removeAllViews()V

    move-object v1, p1

    move v3, v2

    .line 662
    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher3/CellLayout;->a(Landroid/view/View;IILcom/android/launcher3/CellLayout$LayoutParams;Z)Z

    .line 663
    iput-object p3, p0, Lcom/android/launcher3/Workspace;->VJ:Ljava/lang/String;

    .line 665
    iput-object p2, p0, Lcom/android/launcher3/Workspace;->VG:Lym;

    .line 666
    return-void
.end method

.method public final b(Lcom/android/launcher3/Launcher;ZZ)V
    .locals 0

    .prologue
    .line 2473
    return-void
.end method

.method final b(Ljava/util/ArrayList;Lahz;)V
    .locals 3

    .prologue
    .line 4674
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 4675
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrr;

    .line 4676
    iget-object v0, v0, Lrr;->xr:Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 4680
    :cond_0
    invoke-direct {p0, v1, p2}, Lcom/android/launcher3/Workspace;->a(Ljava/util/HashSet;Lahz;)V

    .line 4681
    return-void
.end method

.method public final b(Luq;)V
    .locals 32

    .prologue
    .line 3002
    move-object/from16 v0, p1

    iget v5, v0, Luq;->x:I

    move-object/from16 v0, p1

    iget v6, v0, Luq;->y:I

    move-object/from16 v0, p1

    iget v7, v0, Luq;->Ga:I

    move-object/from16 v0, p1

    iget v8, v0, Luq;->Gb:I

    move-object/from16 v0, p1

    iget-object v9, v0, Luq;->Gd:Luj;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/launcher3/Workspace;->VP:[F

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v10}, Lcom/android/launcher3/Workspace;->a(IIIILuj;[F)[F

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/launcher3/Workspace;->VP:[F

    .line 3005
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher3/Workspace;->VM:Lcom/android/launcher3/CellLayout;

    .line 3008
    if-eqz v8, :cond_0

    .line 3009
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v4, v8}, Lcom/android/launcher3/Launcher;->Y(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 3010
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v4}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->VP:[F

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/android/launcher3/Workspace;->a(Lcom/android/launcher3/Hotseat;[F)V

    .line 3016
    :cond_0
    :goto_0
    const/16 v30, -0x1

    .line 3017
    const/16 v28, 0x0

    .line 3018
    move-object/from16 v0, p1

    iget-object v4, v0, Luq;->Gf:Lui;

    move-object/from16 v0, p0

    if-eq v4, v0, :cond_10

    .line 3019
    const/4 v4, 0x2

    new-array v14, v4, [I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    float-to-int v5, v5

    aput v5, v14, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    float-to-int v5, v5

    aput v5, v14, v4

    .line 3021
    move-object/from16 v0, p1

    iget-object v4, v0, Luq;->Ge:Ljava/lang/Object;

    new-instance v24, Lafs;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lafs;-><init>(Lcom/android/launcher3/Workspace;)V

    move-object/from16 v19, v4

    check-cast v19, Lwq;

    move-object/from16 v0, v19

    iget v6, v0, Lwq;->AY:I

    move-object/from16 v0, v19

    iget v7, v0, Lwq;->AZ:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    if-eqz v5, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget v6, v5, Lsy;->AY:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget v7, v5, Lsy;->AZ:I

    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v5, v8}, Lcom/android/launcher3/Launcher;->Y(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_b

    const-wide/16 v10, -0x65

    move-wide/from16 v22, v10

    :goto_1
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/launcher3/Workspace;->i(Lcom/android/launcher3/CellLayout;)J

    move-result-wide v26

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v5, v8}, Lcom/android/launcher3/Launcher;->Y(Landroid/view/View;)Z

    move-result v5

    if-nez v5, :cond_2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/launcher3/Workspace;->Qc:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/launcher3/Workspace;->bM(I)J

    move-result-wide v10

    cmp-long v5, v26, v10

    if-eqz v5, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->VV:Lagv;

    sget-object v9, Lagv;->Xx:Lagv;

    if-eq v5, v9, :cond_2

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, v26

    invoke-direct {v0, v1, v2, v5}, Lcom/android/launcher3/Workspace;->a(JLjava/lang/Runnable;)V

    :cond_2
    move-object/from16 v0, v19

    instance-of v5, v0, Lacw;

    if-eqz v5, :cond_d

    move-object/from16 v20, v4

    check-cast v20, Lacw;

    const/4 v12, 0x1

    move-object/from16 v0, v20

    iget v4, v0, Lacw;->Jz:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_28

    const/4 v4, 0x0

    aget v4, v14, v4

    const/4 v5, 0x1

    aget v5, v14, v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher3/Workspace;->He:[I

    invoke-static/range {v4 .. v9}, Lcom/android/launcher3/Workspace;->a(IIIILcom/android/launcher3/CellLayout;[I)[I

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/launcher3/Workspace;->He:[I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Workspace;->He:[I

    invoke-virtual {v8, v4, v5, v6}, Lcom/android/launcher3/CellLayout;->a(FF[I)F

    move-result v10

    move-object/from16 v0, p1

    iget-object v7, v0, Luq;->Ge:Ljava/lang/Object;

    check-cast v7, Lwq;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v11, 0x1

    move-object/from16 v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/android/launcher3/Workspace;->a(Lwq;Lcom/android/launcher3/CellLayout;[IFZ)Z

    move-result v4

    if-nez v4, :cond_3

    move-object/from16 v0, p1

    iget-object v4, v0, Luq;->Ge:Ljava/lang/Object;

    check-cast v4, Lwq;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->He:[I

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v8, v5, v10}, Lcom/android/launcher3/Workspace;->a(Ljava/lang/Object;Lcom/android/launcher3/CellLayout;[IF)Z

    move-result v4

    if-eqz v4, :cond_28

    :cond_3
    const/4 v4, 0x0

    move v5, v4

    :goto_2
    move-object/from16 v0, p1

    iget-object v4, v0, Luq;->Ge:Ljava/lang/Object;

    check-cast v4, Lwq;

    const/4 v6, 0x0

    if-eqz v5, :cond_6

    iget v11, v4, Lwq;->AY:I

    iget v12, v4, Lwq;->AZ:I

    iget v5, v4, Lwq;->JB:I

    if-lez v5, :cond_4

    iget v5, v4, Lwq;->JC:I

    if-lez v5, :cond_4

    iget v11, v4, Lwq;->JB:I

    iget v12, v4, Lwq;->JC:I

    :cond_4
    const/4 v5, 0x2

    new-array v0, v5, [I

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v7, 0x0

    aget v5, v5, v7

    float-to-int v9, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v7, 0x1

    aget v5, v5, v7

    float-to-int v10, v5

    move-object/from16 v0, v19

    iget v13, v0, Lwq;->AY:I

    move-object/from16 v0, v19

    iget v14, v0, Lwq;->AZ:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher3/Workspace;->He:[I

    move-object/from16 v16, v0

    const/16 v18, 0x3

    invoke-virtual/range {v8 .. v18}, Lcom/android/launcher3/CellLayout;->a(IIIIIILandroid/view/View;[I[II)[I

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v5, 0x0

    aget v5, v17, v5

    iget v7, v4, Lwq;->AY:I

    if-ne v5, v7, :cond_5

    const/4 v5, 0x1

    aget v5, v17, v5

    iget v7, v4, Lwq;->AZ:I

    if-eq v5, v7, :cond_27

    :cond_5
    const/4 v5, 0x1

    :goto_3
    const/4 v6, 0x0

    aget v6, v17, v6

    iput v6, v4, Lwq;->AY:I

    const/4 v6, 0x1

    aget v6, v17, v6

    iput v6, v4, Lwq;->AZ:I

    move v6, v5

    :cond_6
    new-instance v10, Laft;

    move-object/from16 v11, p0

    move-object/from16 v12, v20

    move-object v13, v4

    move-wide/from16 v14, v22

    move-wide/from16 v16, v26

    invoke-direct/range {v10 .. v17}, Laft;-><init>(Lcom/android/launcher3/Workspace;Lacw;Lwq;JJ)V

    move-object/from16 v0, v20

    iget v5, v0, Lacw;->Jz:I

    const/4 v7, 0x4

    if-ne v5, v7, :cond_c

    move-object/from16 v5, v20

    check-cast v5, Lacy;

    iget-object v12, v5, Lacy;->RR:Landroid/appwidget/AppWidgetHostView;

    :goto_4
    instance-of v5, v12, Landroid/appwidget/AppWidgetHostView;

    if-eqz v5, :cond_7

    if-eqz v6, :cond_7

    move-object v5, v12

    check-cast v5, Landroid/appwidget/AppWidgetHostView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    iget v7, v4, Lwq;->AY:I

    iget v4, v4, Lwq;->AZ:I

    invoke-static {v5, v6, v7, v4}, Lrs;->a(Landroid/appwidget/AppWidgetHostView;Lcom/android/launcher3/Launcher;II)V

    :cond_7
    const/4 v11, 0x0

    move-object/from16 v0, v20

    iget v4, v0, Lacw;->Jz:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_8

    check-cast v20, Lacy;

    move-object/from16 v0, v20

    iget-object v4, v0, Lacy;->RQ:Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v4, v4, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    if-eqz v4, :cond_8

    const/4 v11, 0x1

    :cond_8
    move-object/from16 v0, p1

    iget-object v9, v0, Luq;->Gd:Luj;

    const/4 v13, 0x1

    move-object/from16 v6, p0

    move-object/from16 v7, v19

    invoke-virtual/range {v6 .. v13}, Lcom/android/launcher3/Workspace;->a(Lwq;Lcom/android/launcher3/CellLayout;Luj;Ljava/lang/Runnable;ILandroid/view/View;Z)V

    .line 3185
    :cond_9
    :goto_5
    return-void

    .line 3012
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->VP:[F

    invoke-static {v8, v4}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;[F)V

    goto/16 :goto_0

    .line 3021
    :cond_b
    const-wide/16 v10, -0x64

    move-wide/from16 v22, v10

    goto/16 :goto_1

    :cond_c
    const/4 v12, 0x0

    goto :goto_4

    :cond_d
    move-object/from16 v0, v19

    iget v4, v0, Lwq;->Jz:I

    packed-switch v4, :pswitch_data_0

    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unknown item type: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    iget v6, v0, Lwq;->Jz:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    :pswitch_0
    move-object/from16 v0, v19

    iget-wide v4, v0, Lwq;->JA:J

    const-wide/16 v10, -0x1

    cmp-long v4, v4, v10

    if-nez v4, :cond_26

    move-object/from16 v0, v19

    instance-of v4, v0, Lrr;

    if-eqz v4, :cond_26

    new-instance v5, Ladh;

    check-cast v19, Lrr;

    move-object/from16 v0, v19

    invoke-direct {v5, v0}, Ladh;-><init>(Lrr;)V

    :goto_6
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    const v10, 0x7f04001c

    move-object v4, v5

    check-cast v4, Ladh;

    invoke-virtual {v9, v10, v8, v4}, Lcom/android/launcher3/Launcher;->a(ILandroid/view/ViewGroup;Ladh;)Landroid/view/View;

    move-result-object v4

    move-object/from16 v19, v5

    move-object/from16 v21, v4

    :goto_7
    if-eqz v14, :cond_e

    const/4 v4, 0x0

    aget v4, v14, v4

    const/4 v5, 0x1

    aget v5, v14, v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher3/Workspace;->He:[I

    invoke-static/range {v4 .. v9}, Lcom/android/launcher3/Workspace;->a(IIIILcom/android/launcher3/CellLayout;[I)[I

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/launcher3/Workspace;->He:[I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Workspace;->He:[I

    invoke-virtual {v8, v4, v5, v6}, Lcom/android/launcher3/CellLayout;->a(FF[I)F

    move-result v10

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    iput-object v0, v1, Luq;->Gg:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v11, 0x1

    move-object/from16 v0, p1

    iget-object v12, v0, Luq;->Gd:Luj;

    move-object/from16 v0, p1

    iget-object v13, v0, Luq;->Gg:Ljava/lang/Runnable;

    move-object/from16 v4, p0

    move-object/from16 v5, v21

    move-wide/from16 v6, v22

    invoke-virtual/range {v4 .. v13}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;JLcom/android/launcher3/CellLayout;[IFZLuj;Ljava/lang/Runnable;)Z

    move-result v4

    if-nez v4, :cond_9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v12, 0x1

    move-object/from16 v7, p0

    move-object/from16 v11, p1

    invoke-virtual/range {v7 .. v12}, Lcom/android/launcher3/Workspace;->a(Lcom/android/launcher3/CellLayout;[IFLuq;Z)Z

    move-result v4

    if-nez v4, :cond_9

    :cond_e
    if-eqz v14, :cond_f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    float-to-int v9, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    float-to-int v10, v4

    const/4 v11, 0x1

    const/4 v12, 0x1

    const/4 v13, 0x1

    const/4 v14, 0x1

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher3/Workspace;->He:[I

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x3

    invoke-virtual/range {v8 .. v18}, Lcom/android/launcher3/CellLayout;->a(IIIIIILandroid/view/View;[I[II)[I

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/launcher3/Workspace;->He:[I

    :goto_8
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v5, 0x0

    aget v16, v4, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v5, 0x1

    aget v17, v4, v5

    move-object/from16 v11, v19

    move-wide/from16 v12, v22

    move-wide/from16 v14, v26

    invoke-static/range {v10 .. v17}, Lzi;->a(Landroid/content/Context;Lwq;JJII)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v5, 0x0

    aget v16, v4, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v5, 0x1

    aget v17, v4, v5

    move-object/from16 v0, v19

    iget v0, v0, Lwq;->AY:I

    move/from16 v18, v0

    move-object/from16 v0, v19

    iget v0, v0, Lwq;->AZ:I

    move/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v10, p0

    move-object/from16 v11, v21

    move-wide/from16 v12, v22

    move-wide/from16 v14, v26

    invoke-virtual/range {v10 .. v20}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;JJIIIIZ)V

    invoke-static/range {v21 .. v21}, Lcom/android/launcher3/CellLayout;->I(Landroid/view/View;)V

    invoke-virtual {v8}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ladg;->ad(Landroid/view/View;)V

    move-object/from16 v0, p1

    iget-object v4, v0, Luq;->Gd:Luj;

    if-eqz v4, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->ls()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v4}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Luq;->Gd:Luj;

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    move-object/from16 v2, p0

    invoke-virtual {v4, v5, v0, v1, v2}, Lcom/android/launcher3/DragLayer;->a(Luj;Landroid/view/View;Ljava/lang/Runnable;Landroid/view/View;)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->lt()V

    goto/16 :goto_5

    :pswitch_1
    const v5, 0x7f04008d

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    move-object/from16 v4, v19

    check-cast v4, Lvy;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/launcher3/Workspace;->xn:Lwi;

    invoke-static {v5, v9, v8, v4}, Lcom/android/launcher3/FolderIcon;->a(ILcom/android/launcher3/Launcher;Landroid/view/ViewGroup;Lvy;)Lcom/android/launcher3/FolderIcon;

    move-result-object v4

    move-object/from16 v21, v4

    goto/16 :goto_7

    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-virtual {v8, v4, v5, v6}, Lcom/android/launcher3/CellLayout;->b([III)Z

    goto/16 :goto_8

    .line 3022
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    if-eqz v4, :cond_9

    .line 3023
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget-object v11, v4, Lsy;->Ba:Landroid/view/View;

    .line 3025
    const/16 v29, 0x0

    .line 3026
    if-eqz v8, :cond_25

    move-object/from16 v0, p1

    iget-boolean v4, v0, Luq;->AX:Z

    if-nez v4, :cond_25

    .line 3028
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/android/launcher3/Workspace;->ag(Landroid/view/View;)Lcom/android/launcher3/CellLayout;

    move-result-object v4

    if-eq v4, v8, :cond_17

    const/4 v4, 0x1

    move/from16 v25, v4

    .line 3029
    :goto_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v4, v8}, Lcom/android/launcher3/Launcher;->Y(Landroid/view/View;)Z

    move-result v31

    .line 3030
    if-eqz v31, :cond_18

    const-wide/16 v12, -0x65

    .line 3033
    :goto_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    if-gez v4, :cond_19

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget-wide v4, v4, Lsy;->Bd:J

    move-wide/from16 v26, v4

    .line 3035
    :goto_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    if-eqz v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget v6, v4, Lsy;->AY:I

    .line 3036
    :goto_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    if-eqz v4, :cond_1b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget v7, v4, Lsy;->AZ:I

    .line 3040
    :goto_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    float-to-int v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v9, 0x1

    aget v5, v5, v9

    float-to-int v5, v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher3/Workspace;->He:[I

    invoke-static/range {v4 .. v9}, Lcom/android/launcher3/Workspace;->a(IIIILcom/android/launcher3/CellLayout;[I)[I

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/launcher3/Workspace;->He:[I

    .line 3042
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v9, 0x1

    aget v5, v5, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher3/Workspace;->He:[I

    invoke-virtual {v8, v4, v5, v9}, Lcom/android/launcher3/CellLayout;->a(FF[I)F

    move-result v16

    .line 3047
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/launcher3/Workspace;->Fg:Z

    if-nez v4, :cond_11

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/16 v17, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Luq;->Gd:Luj;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v10, p0

    move-object v14, v8

    invoke-virtual/range {v10 .. v19}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;JLcom/android/launcher3/CellLayout;[IFZLuj;Ljava/lang/Runnable;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 3052
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher3/Workspace;->He:[I

    move-object/from16 v19, v0

    const/16 v22, 0x0

    move-object/from16 v17, p0

    move-object/from16 v18, v8

    move/from16 v20, v16

    move-object/from16 v21, p1

    invoke-virtual/range {v17 .. v22}, Lcom/android/launcher3/Workspace;->a(Lcom/android/launcher3/CellLayout;[IFLuq;Z)Z

    move-result v4

    if-nez v4, :cond_9

    .line 3059
    move-object/from16 v0, p1

    iget-object v4, v0, Luq;->Ge:Ljava/lang/Object;

    check-cast v4, Lwq;

    .line 3060
    iget v0, v4, Lwq;->AY:I

    move/from16 v17, v0

    .line 3061
    iget v0, v4, Lwq;->AZ:I

    move/from16 v18, v0

    .line 3062
    iget v5, v4, Lwq;->JB:I

    if-lez v5, :cond_12

    iget v5, v4, Lwq;->JC:I

    if-lez v5, :cond_12

    .line 3063
    iget v0, v4, Lwq;->JB:I

    move/from16 v17, v0

    .line 3064
    iget v0, v4, Lwq;->JC:I

    move/from16 v18, v0

    .line 3067
    :cond_12
    const/4 v5, 0x2

    new-array v0, v5, [I

    move-object/from16 v23, v0

    .line 3068
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v9, 0x0

    aget v5, v5, v9

    float-to-int v15, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v9, 0x1

    aget v5, v5, v9

    float-to-int v0, v5

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher3/Workspace;->He:[I

    move-object/from16 v22, v0

    const/16 v24, 0x2

    move-object v14, v8

    move/from16 v19, v6

    move/from16 v20, v7

    move-object/from16 v21, v11

    invoke-virtual/range {v14 .. v24}, Lcom/android/launcher3/CellLayout;->a(IIIIIILandroid/view/View;[I[II)[I

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/launcher3/Workspace;->He:[I

    .line 3072
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    if-ltz v5, :cond_1c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    if-ltz v5, :cond_1c

    const/4 v5, 0x1

    move v7, v5

    .line 3075
    :goto_e
    if-eqz v7, :cond_24

    instance-of v5, v11, Landroid/appwidget/AppWidgetHostView;

    if-eqz v5, :cond_24

    const/4 v5, 0x0

    aget v5, v23, v5

    iget v6, v4, Lwq;->AY:I

    if-ne v5, v6, :cond_13

    const/4 v5, 0x1

    aget v5, v23, v5

    iget v6, v4, Lwq;->AZ:I

    if-eq v5, v6, :cond_24

    .line 3077
    :cond_13
    const/4 v6, 0x1

    .line 3078
    const/4 v5, 0x0

    aget v5, v23, v5

    iput v5, v4, Lwq;->AY:I

    .line 3079
    const/4 v5, 0x1

    aget v5, v23, v5

    iput v5, v4, Lwq;->AZ:I

    move-object v5, v11

    .line 3080
    check-cast v5, Landroid/appwidget/AppWidgetHostView;

    .line 3081
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    const/4 v10, 0x0

    aget v10, v23, v10

    const/4 v14, 0x1

    aget v14, v23, v14

    invoke-static {v5, v9, v10, v14}, Lrs;->a(Landroid/appwidget/AppWidgetHostView;Lcom/android/launcher3/Launcher;II)V

    move v9, v6

    .line 3085
    :goto_f
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/launcher3/Workspace;->Qc:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/launcher3/Workspace;->bM(I)J

    move-result-wide v14

    cmp-long v5, v14, v26

    if-eqz v5, :cond_14

    if-nez v31, :cond_14

    .line 3086
    move-object/from16 v0, p0

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher3/Workspace;->k(J)I

    move-result v30

    .line 3087
    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Workspace;->bB(I)V

    :cond_14
    move/from16 v24, v30

    .line 3090
    if-eqz v7, :cond_1e

    .line 3091
    invoke-virtual {v11}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lwq;

    .line 3092
    if-eqz v25, :cond_16

    .line 3094
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/android/launcher3/Workspace;->ag(Landroid/view/View;)Lcom/android/launcher3/CellLayout;

    move-result-object v6

    .line 3095
    if-eqz v6, :cond_1d

    .line 3096
    invoke-virtual {v6, v11}, Lcom/android/launcher3/CellLayout;->removeView(Landroid/view/View;)V

    .line 3100
    :cond_15
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v7, 0x0

    aget v16, v6, v7

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v7, 0x1

    aget v17, v6, v7

    iget v0, v5, Lwq;->AY:I

    move/from16 v18, v0

    iget v0, v5, Lwq;->AZ:I

    move/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v10, p0

    move-wide/from16 v14, v26

    invoke-direct/range {v10 .. v21}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;JJIIIIZZ)V

    .line 3105
    :cond_16
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 3106
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v10, 0x0

    aget v7, v7, v10

    iput v7, v6, Lcom/android/launcher3/CellLayout$LayoutParams;->Bn:I

    iput v7, v6, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    .line 3107
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v10, 0x1

    aget v7, v7, v10

    iput v7, v6, Lcom/android/launcher3/CellLayout$LayoutParams;->Bo:I

    iput v7, v6, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    .line 3108
    iget v7, v4, Lwq;->AY:I

    iput v7, v6, Lcom/android/launcher3/CellLayout$LayoutParams;->Bq:I

    .line 3109
    iget v7, v4, Lwq;->AZ:I

    iput v7, v6, Lcom/android/launcher3/CellLayout$LayoutParams;->Br:I

    .line 3110
    const/4 v7, 0x1

    iput-boolean v7, v6, Lcom/android/launcher3/CellLayout$LayoutParams;->Bs:Z

    .line 3112
    const-wide/16 v14, -0x65

    cmp-long v7, v12, v14

    if-eqz v7, :cond_23

    instance-of v7, v11, Lyx;

    if-eqz v7, :cond_23

    move-object v7, v11

    .line 3118
    check-cast v7, Lyx;

    .line 3119
    invoke-virtual {v7}, Lyx;->getAppWidgetInfo()Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v10

    .line 3120
    if-eqz v10, :cond_23

    iget v10, v10, Landroid/appwidget/AppWidgetProviderInfo;->resizeMode:I

    if-eqz v10, :cond_23

    .line 3122
    new-instance v10, Lafp;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v5, v7, v8}, Lafp;-><init>(Lcom/android/launcher3/Workspace;Lwq;Lyx;Lcom/android/launcher3/CellLayout;)V

    .line 3128
    new-instance v7, Lafq;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v10}, Lafq;-><init>(Lcom/android/launcher3/Workspace;Ljava/lang/Runnable;)V

    .line 3140
    :goto_10
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    iget v0, v6, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    move/from16 v20, v0

    iget v0, v6, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    move/from16 v21, v0

    iget v0, v4, Lwq;->AY:I

    move/from16 v22, v0

    iget v0, v4, Lwq;->AZ:I

    move/from16 v23, v0

    move-object v15, v5

    move-wide/from16 v16, v12

    move-wide/from16 v18, v26

    invoke-static/range {v14 .. v23}, Lzi;->a(Landroid/content/Context;Lwq;JJIIII)V

    move-object/from16 v29, v7

    move/from16 v28, v9

    move/from16 v4, v24

    .line 3152
    :goto_11
    invoke-virtual {v11}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    check-cast v7, Lcom/android/launcher3/CellLayout;

    .line 3156
    new-instance v9, Lafr;

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v9, v0, v1}, Lafr;-><init>(Lcom/android/launcher3/Workspace;Ljava/lang/Runnable;)V

    .line 3166
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/android/launcher3/Workspace;->VX:Z

    .line 3167
    move-object/from16 v0, p1

    iget-object v5, v0, Luq;->Gd:Luj;

    invoke-virtual {v5}, Luj;->gd()Z

    move-result v5

    if-eqz v5, :cond_22

    .line 3168
    invoke-virtual {v11}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lwq;

    .line 3169
    iget v5, v6, Lwq;->Jz:I

    const/4 v8, 0x4

    if-ne v5, v8, :cond_20

    .line 3170
    if-eqz v28, :cond_1f

    const/4 v10, 0x2

    .line 3172
    :goto_12
    move-object/from16 v0, p1

    iget-object v8, v0, Luq;->Gd:Luj;

    const/4 v12, 0x0

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v12}, Lcom/android/launcher3/Workspace;->a(Lwq;Lcom/android/launcher3/CellLayout;Luj;Ljava/lang/Runnable;ILandroid/view/View;Z)V

    .line 3183
    :goto_13
    invoke-static {v11}, Lcom/android/launcher3/CellLayout;->I(Landroid/view/View;)V

    goto/16 :goto_5

    .line 3028
    :cond_17
    const/4 v4, 0x0

    move/from16 v25, v4

    goto/16 :goto_9

    .line 3030
    :cond_18
    const-wide/16 v12, -0x64

    goto/16 :goto_a

    .line 3033
    :cond_19
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/launcher3/Workspace;->i(Lcom/android/launcher3/CellLayout;)J

    move-result-wide v4

    move-wide/from16 v26, v4

    goto/16 :goto_b

    .line 3035
    :cond_1a
    const/4 v6, 0x1

    goto/16 :goto_c

    .line 3036
    :cond_1b
    const/4 v7, 0x1

    goto/16 :goto_d

    .line 3072
    :cond_1c
    const/4 v5, 0x0

    move v7, v5

    goto/16 :goto_e

    .line 3097
    :cond_1d
    invoke-static {}, Lyu;->isDogfoodBuild()Z

    move-result v6

    if-eqz v6, :cond_15

    .line 3098
    new-instance v4, Ljava/lang/NullPointerException;

    const-string v5, "mDragInfo.cell has null parent"

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 3144
    :cond_1e
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 3145
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v6, 0x0

    iget v7, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    aput v7, v5, v6

    .line 3146
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v6, 0x1

    iget v4, v4, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    aput v4, v5, v6

    .line 3147
    invoke-virtual {v11}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Lcom/android/launcher3/CellLayout;

    .line 3148
    invoke-virtual {v4, v11}, Lcom/android/launcher3/CellLayout;->J(Landroid/view/View;)V

    move/from16 v28, v9

    move/from16 v4, v24

    goto/16 :goto_11

    .line 3170
    :cond_1f
    const/4 v10, 0x0

    goto :goto_12

    .line 3175
    :cond_20
    if-gez v4, :cond_21

    const/4 v8, -0x1

    .line 3176
    :goto_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v4}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v5

    move-object/from16 v0, p1

    iget-object v6, v0, Luq;->Gd:Luj;

    move-object v7, v11

    move-object/from16 v10, p0

    invoke-virtual/range {v5 .. v10}, Lcom/android/launcher3/DragLayer;->a(Luj;Landroid/view/View;ILjava/lang/Runnable;Landroid/view/View;)V

    goto :goto_13

    .line 3175
    :cond_21
    const/16 v8, 0x12c

    goto :goto_14

    .line 3180
    :cond_22
    const/4 v4, 0x0

    move-object/from16 v0, p1

    iput-boolean v4, v0, Luq;->Gh:Z

    .line 3181
    const/4 v4, 0x0

    invoke-virtual {v11, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_13

    :cond_23
    move-object/from16 v7, v29

    goto/16 :goto_10

    :cond_24
    move/from16 v9, v28

    goto/16 :goto_f

    :cond_25
    move/from16 v4, v30

    goto/16 :goto_11

    :cond_26
    move-object/from16 v5, v19

    goto/16 :goto_6

    :cond_27
    move v5, v6

    goto/16 :goto_3

    :cond_28
    move v5, v12

    goto/16 :goto_2

    .line 3021
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final bM(I)J
    .locals 2

    .prologue
    .line 884
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 885
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 887
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public final bN(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3188
    invoke-virtual {p0, p1}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 3189
    if-eqz v0, :cond_0

    .line 3190
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getScrollX()I

    move-result v1

    iput v1, p0, Lcom/android/launcher3/Workspace;->Wz:I

    .line 3191
    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->getTranslationX()F

    move-result v1

    iput v1, p0, Lcom/android/launcher3/Workspace;->WB:F

    .line 3192
    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->getRotationY()F

    move-result v1

    iput v1, p0, Lcom/android/launcher3/Workspace;->WA:F

    .line 3193
    invoke-virtual {p0, p1}, Lcom/android/launcher3/Workspace;->bz(I)I

    move-result v1

    .line 3194
    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->setScrollX(I)V

    .line 3195
    invoke-virtual {v0, v2}, Lcom/android/launcher3/CellLayout;->setTranslationX(F)V

    .line 3196
    invoke-virtual {v0, v2}, Lcom/android/launcher3/CellLayout;->setRotationY(F)V

    .line 3198
    :cond_0
    return-void
.end method

.method public final bO(I)V
    .locals 2

    .prologue
    .line 3201
    if-ltz p1, :cond_0

    .line 3202
    invoke-virtual {p0, p1}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 3203
    iget v1, p0, Lcom/android/launcher3/Workspace;->Wz:I

    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->setScrollX(I)V

    .line 3204
    iget v1, p0, Lcom/android/launcher3/Workspace;->WB:F

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CellLayout;->setTranslationX(F)V

    .line 3205
    iget v1, p0, Lcom/android/launcher3/Workspace;->WA:F

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CellLayout;->setRotationY(F)V

    .line 3207
    :cond_0
    return-void
.end method

.method public final bP(I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 3345
    iget v0, p0, Lcom/android/launcher3/Workspace;->Wu:I

    if-eq p1, v0, :cond_1

    .line 3346
    if-nez p1, :cond_2

    .line 3347
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->lr()V

    .line 3350
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/launcher3/Workspace;->au(Z)V

    .line 3351
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->lq()V

    .line 3362
    :cond_0
    :goto_0
    iput p1, p0, Lcom/android/launcher3/Workspace;->Wu:I

    .line 3364
    :cond_1
    return-void

    .line 3352
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    .line 3353
    invoke-direct {p0, v1}, Lcom/android/launcher3/Workspace;->au(Z)V

    .line 3354
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->lq()V

    goto :goto_0

    .line 3355
    :cond_3
    if-ne p1, v1, :cond_4

    .line 3356
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->lr()V

    .line 3357
    invoke-direct {p0, v1}, Lcom/android/launcher3/Workspace;->au(Z)V

    goto :goto_0

    .line 3358
    :cond_4
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 3359
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->lr()V

    .line 3360
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->lq()V

    goto :goto_0
.end method

.method public final bQ(I)V
    .locals 2

    .prologue
    .line 4425
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Wx:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    .line 4426
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Wy:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4427
    invoke-virtual {p0, p1}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 4428
    if-eqz v0, :cond_0

    .line 4429
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Wx:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CellLayout;->a(Landroid/util/SparseArray;)V

    .line 4432
    :cond_0
    return-void
.end method

.method public final bR(I)Lyx;
    .locals 1

    .prologue
    .line 4594
    new-instance v0, Lafz;

    invoke-direct {v0, p0, p1}, Lafz;-><init>(Lcom/android/launcher3/Workspace;I)V

    invoke-direct {p0, v0}, Lcom/android/launcher3/Workspace;->a(Lagt;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lyx;

    return-object v0
.end method

.method protected final bs(I)Lacg;
    .locals 4

    .prologue
    .line 5072
    invoke-virtual {p0, p1}, Lcom/android/launcher3/Workspace;->bM(I)J

    move-result-wide v0

    .line 5073
    const-wide/16 v2, -0xc9

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 5074
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->ld()I

    move-result v1

    sub-int/2addr v0, v1

    .line 5075
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 5076
    new-instance v0, Lacg;

    const v1, 0x7f0201b0

    const v2, 0x7f0201af

    invoke-direct {v0, v1, v2}, Lacg;-><init>(II)V

    .line 5081
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Ladi;->bs(I)Lacg;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 3787
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    .line 3788
    return-void
.end method

.method public final c(Lcom/android/launcher3/Launcher;ZZ)V
    .locals 0

    .prologue
    .line 2482
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->lo()V

    .line 2483
    return-void
.end method

.method public final c(Luq;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3223
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->AL:Lup;

    invoke-virtual {v0}, Lup;->fe()V

    .line 3224
    iput-boolean v1, p0, Lcom/android/launcher3/Workspace;->Wp:Z

    .line 3225
    iput-boolean v1, p0, Lcom/android/launcher3/Workspace;->Wq:Z

    .line 3227
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->VM:Lcom/android/launcher3/CellLayout;

    .line 3228
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->lu()Lcom/android/launcher3/CellLayout;

    move-result-object v0

    .line 3229
    invoke-direct {p0, v0}, Lcom/android/launcher3/Workspace;->j(Lcom/android/launcher3/CellLayout;)V

    .line 3230
    invoke-direct {p0, v0}, Lcom/android/launcher3/Workspace;->k(Lcom/android/launcher3/CellLayout;)V

    .line 3232
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lf()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3233
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/DragLayer;->fR()V

    .line 3235
    :cond_0
    return-void
.end method

.method public computeScroll()V
    .locals 1

    .prologue
    .line 1490
    invoke-super {p0}, Ladi;->computeScroll()V

    .line 1491
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Wf:Lagw;

    invoke-virtual {v0}, Lagw;->lC()V

    .line 1492
    return-void
.end method

.method public final d(Luq;)V
    .locals 24

    .prologue
    .line 3551
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/launcher3/Workspace;->Fg:Z

    if-nez v2, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->lp()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3676
    :cond_0
    :goto_0
    return-void

    .line 3553
    :cond_1
    new-instance v10, Landroid/graphics/Rect;

    invoke-direct {v10}, Landroid/graphics/Rect;-><init>()V

    .line 3554
    const/4 v9, 0x0

    .line 3555
    move-object/from16 v0, p1

    iget-object v2, v0, Luq;->Ge:Ljava/lang/Object;

    move-object/from16 v17, v2

    check-cast v17, Lwq;

    .line 3556
    if-nez v17, :cond_2

    .line 3557
    invoke-static {}, Lyu;->isDogfoodBuild()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3558
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "DragObject has null info"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 3564
    :cond_2
    move-object/from16 v0, v17

    iget v2, v0, Lwq;->AY:I

    if-ltz v2, :cond_3

    move-object/from16 v0, v17

    iget v2, v0, Lwq;->AZ:I

    if-gez v2, :cond_4

    :cond_3
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Improper spans found"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 3565
    :cond_4
    move-object/from16 v0, p1

    iget v3, v0, Luq;->x:I

    move-object/from16 v0, p1

    iget v4, v0, Luq;->y:I

    move-object/from16 v0, p1

    iget v5, v0, Luq;->Ga:I

    move-object/from16 v0, p1

    iget v6, v0, Luq;->Gb:I

    move-object/from16 v0, p1

    iget-object v7, v0, Luq;->Gd:Luj;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher3/Workspace;->VP:[F

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/android/launcher3/Workspace;->a(IIIILuj;[F)[F

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/launcher3/Workspace;->VP:[F

    .line 3568
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    if-nez v2, :cond_b

    const/4 v13, 0x0

    .line 3570
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->lf()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 3571
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v2

    if-eqz v2, :cond_1b

    move-object/from16 v0, p1

    iget-object v2, v0, Luq;->Gf:Lui;

    move-object/from16 v0, p0

    if-eq v2, v0, :cond_c

    invoke-static/range {p1 .. p1}, Lcom/android/launcher3/Workspace;->k(Luq;)Z

    move-result v2

    if-eqz v2, :cond_c

    const/4 v2, 0x1

    :goto_2
    if-nez v2, :cond_1b

    .line 3572
    move-object/from16 v0, p1

    iget v2, v0, Luq;->x:I

    move-object/from16 v0, p1

    iget v3, v0, Luq;->y:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v10}, Lcom/android/launcher3/Workspace;->a(IILandroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 3573
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/launcher3/Hotseat;->gW()Lcom/android/launcher3/CellLayout;

    move-result-object v9

    move-object v2, v9

    .line 3576
    :goto_3
    if-nez v2, :cond_5

    .line 3577
    move-object/from16 v0, p1

    iget-object v2, v0, Luq;->Gd:Luj;

    move-object/from16 v0, p1

    iget v2, v0, Luq;->x:I

    int-to-float v2, v2

    move-object/from16 v0, p1

    iget v3, v0, Luq;->y:I

    int-to-float v3, v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/android/launcher3/Workspace;->a(FFZ)Lcom/android/launcher3/CellLayout;

    move-result-object v2

    .line 3579
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    if-eq v2, v3, :cond_6

    .line 3580
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/launcher3/Workspace;->j(Lcom/android/launcher3/CellLayout;)V

    .line 3581
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/launcher3/Workspace;->k(Lcom/android/launcher3/CellLayout;)V

    .line 3583
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->VV:Lagv;

    sget-object v4, Lagv;->Xx:Lagv;

    if-ne v3, v4, :cond_d

    const/4 v3, 0x1

    .line 3584
    :goto_4
    if-eqz v3, :cond_6

    .line 3585
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v3, v2}, Lcom/android/launcher3/Launcher;->Y(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 3586
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VS:Ladk;

    iget-object v2, v2, Ladk;->Te:Lro;

    invoke-virtual {v2}, Lro;->dV()V

    .line 3609
    :cond_6
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    if-eqz v2, :cond_0

    .line 3611
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v2, v3}, Lcom/android/launcher3/Launcher;->Y(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 3612
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->VP:[F

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/launcher3/Workspace;->a(Lcom/android/launcher3/Hotseat;[F)V

    .line 3617
    :goto_6
    move-object/from16 v0, p1

    iget-object v2, v0, Luq;->Ge:Ljava/lang/Object;

    move-object v12, v2

    check-cast v12, Lwq;

    .line 3619
    move-object/from16 v0, v17

    iget v4, v0, Lwq;->AY:I

    .line 3620
    move-object/from16 v0, v17

    iget v5, v0, Lwq;->AZ:I

    .line 3621
    move-object/from16 v0, v17

    iget v2, v0, Lwq;->JB:I

    if-lez v2, :cond_7

    move-object/from16 v0, v17

    iget v2, v0, Lwq;->JC:I

    if-lez v2, :cond_7

    .line 3622
    move-object/from16 v0, v17

    iget v4, v0, Lwq;->JB:I

    .line 3623
    move-object/from16 v0, v17

    iget v5, v0, Lwq;->JC:I

    .line 3626
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    float-to-int v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v6, 0x1

    aget v3, v3, v6

    float-to-int v3, v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher3/Workspace;->He:[I

    invoke-static/range {v2 .. v7}, Lcom/android/launcher3/Workspace;->a(IIIILcom/android/launcher3/CellLayout;[I)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/launcher3/Workspace;->He:[I

    .line 3629
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v3, 0x0

    aget v3, v2, v3

    .line 3630
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v6, 0x1

    aget v14, v2, v6

    .line 3632
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v6, 0x0

    aget v2, v2, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v7, 0x1

    aget v6, v6, v7

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v6}, Lcom/android/launcher3/Workspace;->T(II)V

    .line 3634
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher3/Workspace;->He:[I

    invoke-virtual {v2, v6, v7, v8}, Lcom/android/launcher3/CellLayout;->a(FF[I)F

    move-result v10

    .line 3637
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v8, 0x1

    aget v7, v7, v8

    invoke-virtual {v2, v6, v7}, Lcom/android/launcher3/CellLayout;->C(II)Landroid/view/View;

    move-result-object v2

    .line 3640
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v11, 0x0

    move-object/from16 v6, p0

    move-object v7, v12

    invoke-direct/range {v6 .. v11}, Lcom/android/launcher3/Workspace;->a(Lwq;Lcom/android/launcher3/CellLayout;[IFZ)Z

    move-result v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/launcher3/Workspace;->Wu:I

    if-nez v7, :cond_13

    if-eqz v6, :cond_13

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher3/Workspace;->Wm:Lro;

    invoke-virtual {v7}, Lro;->dW()Z

    move-result v7

    if-nez v7, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->Wm:Lro;

    new-instance v6, Lagr;

    const/4 v7, 0x0

    aget v7, v9, v7

    const/4 v10, 0x1

    aget v9, v9, v10

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v8, v7, v9}, Lagr;-><init>(Lcom/android/launcher3/Workspace;Lcom/android/launcher3/CellLayout;II)V

    invoke-virtual {v2, v6}, Lro;->a(Lace;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->Wm:Lro;

    const-wide/16 v6, 0x0

    invoke-virtual {v2, v6, v7}, Lro;->e(J)V

    .line 3643
    :cond_8
    :goto_7
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v7, 0x0

    aget v2, v2, v7

    float-to-int v7, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v8, 0x1

    aget v2, v2, v8

    float-to-int v8, v2

    move-object/from16 v0, v17

    iget v9, v0, Lwq;->AY:I

    move-object/from16 v0, v17

    iget v10, v0, Lwq;->AZ:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/launcher3/Workspace;->He:[I

    move-object v11, v13

    invoke-virtual/range {v6 .. v12}, Lcom/android/launcher3/CellLayout;->a(IIIILandroid/view/View;[I)Z

    move-result v23

    .line 3647
    if-nez v23, :cond_17

    .line 3648
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->Wb:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->VP:[F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->VP:[F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v5, 0x0

    aget v5, v3, v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/4 v6, 0x1

    aget v6, v3, v6

    move-object/from16 v0, v17

    iget v7, v0, Lwq;->AY:I

    move-object/from16 v0, v17

    iget v8, v0, Lwq;->AZ:I

    const/4 v9, 0x0

    move-object/from16 v0, p1

    iget-object v3, v0, Luq;->Gd:Luj;

    invoke-virtual {v3}, Luj;->fY()Landroid/graphics/Point;

    move-result-object v10

    move-object/from16 v0, p1

    iget-object v3, v0, Luq;->Gd:Luj;

    invoke-virtual {v3}, Luj;->fZ()Landroid/graphics/Rect;

    move-result-object v11

    move-object v3, v13

    invoke-virtual/range {v2 .. v11}, Lcom/android/launcher3/CellLayout;->a(Landroid/view/View;Landroid/graphics/Bitmap;IIIIZLandroid/graphics/Point;Landroid/graphics/Rect;)V

    .line 3669
    :cond_9
    :goto_8
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher3/Workspace;->Wu:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_a

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher3/Workspace;->Wu:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_a

    if-nez v23, :cond_0

    .line 3671
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    if-eqz v2, :cond_0

    .line 3672
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    invoke-virtual {v2}, Lcom/android/launcher3/CellLayout;->fd()V

    goto/16 :goto_0

    .line 3568
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VB:Lsy;

    iget-object v13, v2, Lsy;->Ba:Landroid/view/View;

    goto/16 :goto_1

    .line 3571
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 3583
    :cond_d
    const/4 v3, 0x0

    goto/16 :goto_4

    .line 3588
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher3/Workspace;->VS:Ladk;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    iget-object v2, v4, Ladk;->Te:Lro;

    invoke-virtual {v2}, Lro;->dV()V

    iget-object v6, v4, Ladk;->Te:Lro;

    if-nez v5, :cond_f

    const-wide/16 v2, 0x3b6

    :goto_9
    invoke-virtual {v6, v2, v3}, Lro;->e(J)V

    iput-object v5, v4, Ladk;->Tf:Lcom/android/launcher3/CellLayout;

    goto/16 :goto_5

    :cond_f
    const-wide/16 v2, 0x1f4

    goto :goto_9

    .line 3594
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v2

    if-eqz v2, :cond_1a

    invoke-static/range {p1 .. p1}, Lcom/android/launcher3/Workspace;->k(Luq;)Z

    move-result v2

    if-nez v2, :cond_1a

    .line 3595
    move-object/from16 v0, p1

    iget v2, v0, Luq;->x:I

    move-object/from16 v0, p1

    iget v3, v0, Luq;->y:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v10}, Lcom/android/launcher3/Workspace;->a(IILandroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 3596
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/launcher3/Hotseat;->gW()Lcom/android/launcher3/CellLayout;

    move-result-object v2

    .line 3599
    :goto_a
    if-nez v2, :cond_11

    .line 3600
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher3/Workspace;->lu()Lcom/android/launcher3/CellLayout;

    move-result-object v2

    .line 3602
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    if-eq v2, v3, :cond_6

    .line 3603
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/launcher3/Workspace;->j(Lcom/android/launcher3/CellLayout;)V

    .line 3604
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/launcher3/Workspace;->k(Lcom/android/launcher3/CellLayout;)V

    goto/16 :goto_5

    .line 3614
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/Workspace;->VP:[F

    invoke-static {v2, v3}, Lcom/android/launcher3/Workspace;->a(Landroid/view/View;[F)V

    goto/16 :goto_6

    .line 3640
    :cond_13
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v8, v9, v10}, Lcom/android/launcher3/Workspace;->a(Ljava/lang/Object;Lcom/android/launcher3/CellLayout;[IF)Z

    move-result v7

    if-eqz v7, :cond_15

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/launcher3/Workspace;->Wu:I

    if-nez v9, :cond_15

    check-cast v2, Lcom/android/launcher3/FolderIcon;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/launcher3/Workspace;->Wo:Lcom/android/launcher3/FolderIcon;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->Wo:Lcom/android/launcher3/FolderIcon;

    invoke-virtual {v2, v12}, Lcom/android/launcher3/FolderIcon;->ab(Ljava/lang/Object;)V

    if-eqz v8, :cond_14

    invoke-virtual {v8}, Lcom/android/launcher3/CellLayout;->fa()V

    :cond_14
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Workspace;->bP(I)V

    goto/16 :goto_7

    :cond_15
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher3/Workspace;->Wu:I

    const/4 v8, 0x2

    if-ne v2, v8, :cond_16

    if-nez v7, :cond_16

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Workspace;->bP(I)V

    :cond_16
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher3/Workspace;->Wu:I

    const/4 v7, 0x1

    if-ne v2, v7, :cond_8

    if-nez v6, :cond_8

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Workspace;->bP(I)V

    goto/16 :goto_7

    .line 3652
    :cond_17
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher3/Workspace;->Wu:I

    if-eqz v2, :cond_18

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher3/Workspace;->Wu:I

    const/4 v6, 0x3

    if-ne v2, v6, :cond_9

    :cond_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->Hh:Lro;

    invoke-virtual {v2}, Lro;->dW()Z

    move-result v2

    if-nez v2, :cond_9

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher3/Workspace;->Wv:I

    if-ne v2, v3, :cond_19

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher3/Workspace;->Ww:I

    if-eq v2, v14, :cond_9

    .line 3656
    :cond_19
    const/4 v2, 0x2

    new-array v15, v2, [I

    .line 3657
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    float-to-int v7, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->VP:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    float-to-int v8, v2

    move-object/from16 v0, v17

    iget v11, v0, Lwq;->AY:I

    move-object/from16 v0, v17

    iget v12, v0, Lwq;->AZ:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/launcher3/Workspace;->He:[I

    const/16 v16, 0x0

    move v9, v4

    move v10, v5

    invoke-virtual/range {v6 .. v16}, Lcom/android/launcher3/CellLayout;->a(IIIIIILandroid/view/View;[I[II)[I

    .line 3663
    new-instance v14, Lagu;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/launcher3/Workspace;->VP:[F

    move-object/from16 v16, v0

    move-object/from16 v0, v17

    iget v0, v0, Lwq;->AY:I

    move/from16 v19, v0

    move-object/from16 v0, v17

    iget v0, v0, Lwq;->AZ:I

    move/from16 v20, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Luq;->Gd:Luj;

    move-object/from16 v21, v0

    move-object/from16 v15, p0

    move/from16 v17, v4

    move/from16 v18, v5

    move-object/from16 v22, v13

    invoke-direct/range {v14 .. v22}, Lagu;-><init>(Lcom/android/launcher3/Workspace;[FIIIILuj;Landroid/view/View;)V

    .line 3665
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->Hh:Lro;

    invoke-virtual {v2, v14}, Lro;->a(Lace;)V

    .line 3666
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/Workspace;->Hh:Lro;

    const-wide/16 v4, 0x15e

    invoke-virtual {v2, v4, v5}, Lro;->e(J)V

    goto/16 :goto_8

    :cond_1a
    move-object v2, v9

    goto/16 :goto_a

    :cond_1b
    move-object v2, v9

    goto/16 :goto_3
.end method

.method public final d(ZZ)V
    .locals 2

    .prologue
    .line 741
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/android/launcher3/Workspace;->a(ZLjava/lang/Runnable;IZ)V

    .line 742
    return-void
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0

    .prologue
    .line 4421
    iput-object p1, p0, Lcom/android/launcher3/Workspace;->Wx:Landroid/util/SparseArray;

    .line 4422
    return-void
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 1091
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lf()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->kW()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1093
    :cond_0
    const/4 v0, 0x0

    .line 1095
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Ladi;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    goto :goto_0
.end method

.method final e(Lty;)V
    .locals 2

    .prologue
    .line 4156
    new-instance v0, Ladk;

    iget-object v1, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-direct {v0, v1}, Ladk;-><init>(Lcom/android/launcher3/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->VS:Ladk;

    .line 4157
    iput-object p1, p0, Lcom/android/launcher3/Workspace;->yg:Lty;

    .line 4161
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/launcher3/Workspace;->as(Z)V

    .line 4162
    return-void
.end method

.method public final e(Luq;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 3276
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->AL:Lup;

    invoke-virtual {v0}, Lup;->ff()V

    .line 3280
    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->Fg:Z

    if-eqz v0, :cond_3

    .line 3281
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jp()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3284
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jg()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->aS(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->VM:Lcom/android/launcher3/CellLayout;

    .line 3292
    :goto_0
    iget v0, p0, Lcom/android/launcher3/Workspace;->Wu:I

    if-ne v0, v2, :cond_4

    .line 3293
    iput-boolean v2, p0, Lcom/android/launcher3/Workspace;->Wp:Z

    .line 3299
    :cond_0
    :goto_1
    invoke-direct {p0, v3}, Lcom/android/launcher3/Workspace;->k(Lcom/android/launcher3/CellLayout;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/Workspace;->Fg:Z

    .line 3300
    invoke-direct {p0, v3}, Lcom/android/launcher3/Workspace;->j(Lcom/android/launcher3/CellLayout;)V

    .line 3301
    invoke-direct {p0, v3}, Lcom/android/launcher3/Workspace;->k(Lcom/android/launcher3/CellLayout;)V

    .line 3303
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VS:Ladk;

    iget-object v0, v0, Ladk;->Te:Lro;

    invoke-virtual {v0}, Lro;->dV()V

    .line 3305
    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->QP:Z

    if-nez v0, :cond_1

    .line 3306
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->la()V

    .line 3308
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/DragLayer;->fS()V

    .line 3309
    return-void

    .line 3286
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VL:Lcom/android/launcher3/CellLayout;

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->VM:Lcom/android/launcher3/CellLayout;

    goto :goto_0

    .line 3289
    :cond_3
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VK:Lcom/android/launcher3/CellLayout;

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->VM:Lcom/android/launcher3/CellLayout;

    goto :goto_0

    .line 3294
    :cond_4
    iget v0, p0, Lcom/android/launcher3/Workspace;->Wu:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 3295
    iput-boolean v2, p0, Lcom/android/launcher3/Workspace;->Wq:Z

    goto :goto_1
.end method

.method protected final e([I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2058
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->ld()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    aput v0, p1, v3

    const/4 v0, 0x1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    aput v1, p1, v0

    .line 2059
    return-void
.end method

.method public final eJ()Z
    .locals 1

    .prologue
    .line 4407
    const/4 v0, 0x1

    return v0
.end method

.method public final eK()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 423
    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->VA:Z

    if-nez v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vv:Ladg;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/android/launcher3/Workspace;->d(ZZ)V

    .line 427
    :cond_0
    iput-boolean v2, p0, Lcom/android/launcher3/Workspace;->VY:Z

    .line 428
    invoke-direct {p0, v2}, Lcom/android/launcher3/Workspace;->as(Z)V

    .line 429
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Launcher;->ad(Z)V

    .line 432
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/launcher3/InstallShortcutReceiver;->l(Landroid/content/Context;)V

    .line 433
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/launcher3/UninstallShortcutReceiver;->x(Landroid/content/Context;)V

    .line 435
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Vv:Ladg;

    .line 436
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hF()V

    .line 437
    return-void

    :cond_1
    move v0, v2

    .line 424
    goto :goto_0
.end method

.method public final ek()V
    .locals 0

    .prologue
    .line 4404
    return-void
.end method

.method public final el()Z
    .locals 1

    .prologue
    .line 4383
    const/4 v0, 0x1

    return v0
.end method

.method public final em()Z
    .locals 1

    .prologue
    .line 4388
    const/4 v0, 0x0

    return v0
.end method

.method public final en()Z
    .locals 1

    .prologue
    .line 4393
    const/4 v0, 0x1

    return v0
.end method

.method public final eo()F
    .locals 1

    .prologue
    .line 4378
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public final es()V
    .locals 0

    .prologue
    .line 5086
    return-void
.end method

.method protected final eu()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 1223
    invoke-super {p0}, Ladi;->eu()V

    .line 1225
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1226
    invoke-direct {p0, v2}, Lcom/android/launcher3/Workspace;->as(Z)V

    .line 1231
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->yg:Lty;

    invoke-virtual {v0}, Lty;->fE()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1232
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lf()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1235
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->yg:Lty;

    invoke-virtual {v0}, Lty;->fJ()V

    .line 1239
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Wj:Ljava/lang/Runnable;

    if-eqz v0, :cond_2

    .line 1240
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Wj:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1241
    iput-object v5, p0, Lcom/android/launcher3/Workspace;->Wj:Ljava/lang/Runnable;

    .line 1244
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Wk:Ljava/lang/Runnable;

    if-eqz v0, :cond_3

    .line 1245
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Wk:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1246
    iput-object v5, p0, Lcom/android/launcher3/Workspace;->Wk:Ljava/lang/Runnable;

    .line 1248
    :cond_3
    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->Wa:Z

    if-eqz v0, :cond_4

    .line 1249
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->kU()V

    .line 1250
    iput-boolean v2, p0, Lcom/android/launcher3/Workspace;->Wa:Z

    .line 1252
    :cond_4
    return-void

    .line 1228
    :cond_5
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    invoke-virtual {v0, v2}, Lcom/android/launcher3/CellLayout;->setChildrenDrawnWithCacheEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->isHardwareAccelerated()Z

    move-result v4

    if-nez v4, :cond_6

    invoke-virtual {v0, v2}, Lcom/android/launcher3/CellLayout;->setChildrenDrawingCacheEnabled(Z)V

    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method protected final ex()Ljava/lang/String;
    .locals 5

    .prologue
    .line 5098
    iget v0, p0, Lcom/android/launcher3/Workspace;->Qf:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/launcher3/Workspace;->Qf:I

    .line 5099
    :goto_0
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->ld()I

    move-result v1

    .line 5100
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lc()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jg()I

    move-result v2

    if-nez v2, :cond_1

    .line 5101
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VJ:Ljava/lang/String;

    .line 5103
    :goto_1
    return-object v0

    .line 5098
    :cond_0
    iget v0, p0, Lcom/android/launcher3/Workspace;->Qc:I

    goto :goto_0

    .line 5103
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a00c1

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    add-int/lit8 v0, v0, 0x1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v4

    sub-int v1, v4, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final f(IZ)V
    .locals 0

    .prologue
    .line 5090
    return-void
.end method

.method public final fU()V
    .locals 1

    .prologue
    .line 4447
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lf()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->VW:Z

    if-nez v0, :cond_0

    .line 4448
    invoke-super {p0}, Ladi;->fU()V

    .line 4450
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->kK()Lcom/android/launcher3/Folder;

    move-result-object v0

    .line 4451
    if-eqz v0, :cond_1

    .line 4452
    invoke-virtual {v0}, Lcom/android/launcher3/Folder;->gv()V

    .line 4454
    :cond_1
    return-void
.end method

.method public final fV()V
    .locals 1

    .prologue
    .line 4458
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lf()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->VW:Z

    if-nez v0, :cond_0

    .line 4459
    invoke-super {p0}, Ladi;->fV()V

    .line 4461
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->kK()Lcom/android/launcher3/Folder;

    move-result-object v0

    .line 4462
    if-eqz v0, :cond_1

    .line 4463
    invoke-virtual {v0}, Lcom/android/launcher3/Folder;->gv()V

    .line 4465
    :cond_1
    return-void
.end method

.method public final fW()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4508
    .line 4509
    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->Fg:Z

    if-eqz v0, :cond_0

    .line 4510
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->invalidate()V

    .line 4511
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->lu()Lcom/android/launcher3/CellLayout;

    move-result-object v0

    .line 4512
    invoke-direct {p0, v0}, Lcom/android/launcher3/Workspace;->j(Lcom/android/launcher3/CellLayout;)V

    .line 4513
    invoke-direct {p0, v0}, Lcom/android/launcher3/Workspace;->k(Lcom/android/launcher3/CellLayout;)V

    .line 4515
    const/4 v0, 0x1

    .line 4516
    iput-boolean v1, p0, Lcom/android/launcher3/Workspace;->Fg:Z

    .line 4518
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;Lahz;)V
    .locals 2

    .prologue
    .line 4918
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 4919
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4920
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v1, p1, p2}, Lzi;->a(Landroid/content/Context;Ljava/lang/String;Lahz;)V

    .line 4921
    invoke-virtual {p0, v0, p2}, Lcom/android/launcher3/Workspace;->a(Ljava/util/ArrayList;Lahz;)V

    .line 4922
    return-void
.end method

.method public getChildrenOutlineAlpha()F
    .locals 1

    .prologue
    .line 1538
    iget v0, p0, Lcom/android/launcher3/Workspace;->Vp:F

    return v0
.end method

.method public getDescendantFocusability()I
    .locals 1

    .prologue
    .line 1794
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lf()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1795
    const/high16 v0, 0x60000

    .line 1797
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Ladi;->getDescendantFocusability()I

    move-result v0

    goto :goto_0
.end method

.method public final gw()V
    .locals 1

    .prologue
    .line 4217
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/Workspace;->Hx:Z

    .line 4218
    return-void
.end method

.method public final h(J)J
    .locals 5

    .prologue
    .line 554
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    const-wide/16 v2, -0xc9

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 555
    if-gez v0, :cond_0

    .line 556
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 558
    :cond_0
    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher3/Workspace;->a(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method protected final h(F)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1716
    cmpg-float v0, p1, v1

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lc()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jc()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    cmpl-float v0, p1, v1

    if-lez v0, :cond_3

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lc()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jc()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    .line 1718
    :goto_0
    if-eqz v0, :cond_5

    .line 1719
    invoke-virtual {p0, p1}, Lcom/android/launcher3/Workspace;->u(F)V

    .line 1720
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QV:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    int-to-float v0, v0

    div-float v0, p1, v0

    mul-float/2addr v0, v2

    cmpl-float v2, v0, v1

    if-nez v2, :cond_4

    move v0, v1

    :cond_2
    :goto_1
    iput v0, p0, Lcom/android/launcher3/Workspace;->WK:F

    .line 1724
    :goto_2
    return-void

    .line 1716
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1720
    :cond_4
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v0, v1

    goto :goto_1

    .line 1722
    :cond_5
    iput v1, p0, Lcom/android/launcher3/Workspace;->WK:F

    goto :goto_2
.end method

.method public final h(Ljava/lang/String;Lahz;)V
    .locals 2

    .prologue
    .line 4925
    const/4 v0, 0x1

    new-instance v1, Lagf;

    invoke-direct {v1, p0, p2, p1}, Lagf;-><init>(Lcom/android/launcher3/Workspace;Lahz;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/Workspace;->a(ZLagt;)V

    .line 4954
    return-void
.end method

.method public final h(III)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 4470
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    move v0, v1

    .line 4471
    :goto_1
    iget-object v3, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v3

    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    .line 4472
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 4473
    iget-object v3, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/android/launcher3/Hotseat;->getHitRect(Landroid/graphics/Rect;)V

    .line 4474
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4503
    :cond_0
    :goto_2
    return v2

    :cond_1
    move v0, v2

    .line 4470
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 4480
    :cond_3
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lf()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->VW:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->kK()Lcom/android/launcher3/Folder;

    move-result-object v0

    if-nez v0, :cond_0

    .line 4481
    iput-boolean v1, p0, Lcom/android/launcher3/Workspace;->Fg:Z

    .line 4483
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jg()I

    move-result v3

    if-nez p3, :cond_4

    const/4 v0, -0x1

    :goto_3
    add-int/2addr v0, v3

    .line 4486
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/android/launcher3/Workspace;->j(Lcom/android/launcher3/CellLayout;)V

    .line 4488
    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 4490
    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->bM(I)J

    move-result-wide v4

    const-wide/16 v6, -0x12d

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 4494
    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 4495
    invoke-direct {p0, v0}, Lcom/android/launcher3/Workspace;->k(Lcom/android/launcher3/CellLayout;)V

    .line 4499
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->invalidate()V

    move v2, v1

    .line 4500
    goto :goto_2

    :cond_4
    move v0, v1

    .line 4483
    goto :goto_3
.end method

.method protected final iY()Ljava/lang/String;
    .locals 3

    .prologue
    .line 5093
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00df

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 5094
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->ex()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final iZ()Landroid/view/View$OnClickListener;
    .locals 2

    .prologue
    .line 1667
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 1669
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1670
    const/4 v0, 0x0

    .line 1678
    :goto_0
    return-object v0

    .line 1672
    :cond_0
    new-instance v0, Lagn;

    invoke-direct {v0, p0}, Lagn;-><init>(Lcom/android/launcher3/Workspace;)V

    goto :goto_0
.end method

.method public final j(J)Lcom/android/launcher3/CellLayout;
    .locals 3

    .prologue
    .line 864
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 865
    return-object v0
.end method

.method protected final jI()V
    .locals 4

    .prologue
    .line 2070
    invoke-super {p0}, Ladi;->jI()V

    .line 2071
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lf()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->VW:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vo:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vo:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vn:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vn:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_1
    const-string v0, "childrenOutlineAlpha"

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Lyr;->a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Vn:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vn:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vn:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 2073
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 2074
    return-void
.end method

.method protected final jK()V
    .locals 6

    .prologue
    .line 2077
    invoke-super {p0}, Ladi;->jK()V

    .line 2079
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hz()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2096
    :goto_0
    return-void

    .line 2084
    :cond_0
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->la()V

    .line 2085
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2086
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v2

    .line 2087
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_1

    .line 2088
    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 2089
    iget-object v3, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/android/launcher3/Workspace;->i(Lcom/android/launcher3/CellLayout;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2087
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2092
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hs()Lzi;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v2, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lzi;->d(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 2095
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->kJ()V

    goto :goto_0
.end method

.method protected final jl()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1256
    invoke-super {p0}, Ladi;->jl()V

    .line 1257
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jg()I

    move-result v0

    invoke-static {v0}, Lcom/android/launcher3/Launcher;->bk(I)V

    .line 1259
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lc()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jg()I

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->VH:Z

    if-nez v0, :cond_1

    .line 1260
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/Workspace;->VH:Z

    .line 1261
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VG:Lym;

    if-eqz v0, :cond_0

    .line 1262
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VG:Lym;

    invoke-interface {v0, v2}, Lym;->ae(Z)V

    .line 1263
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/launcher3/Workspace;->Vr:J

    .line 1264
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Launcher;->aa(Z)V

    .line 1274
    :cond_0
    :goto_0
    return-void

    .line 1266
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lc()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jg()I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->VH:Z

    if-eqz v0, :cond_0

    .line 1267
    iput-boolean v2, p0, Lcom/android/launcher3/Workspace;->VH:Z

    .line 1268
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VG:Lym;

    if-eqz v0, :cond_0

    .line 1269
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VG:Lym;

    invoke-interface {v0}, Lym;->if()V

    .line 1270
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hi()V

    .line 1271
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0, v2}, Lcom/android/launcher3/Launcher;->aa(Z)V

    goto :goto_0
.end method

.method protected final jq()V
    .locals 2

    .prologue
    .line 1206
    invoke-super {p0}, Ladi;->jq()V

    .line 1208
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1209
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/launcher3/Workspace;->as(Z)V

    .line 1220
    :goto_0
    return-void

    .line 1211
    :cond_0
    iget v0, p0, Lcom/android/launcher3/Workspace;->Qf:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 1213
    iget v0, p0, Lcom/android/launcher3/Workspace;->Qc:I

    iget v1, p0, Lcom/android/launcher3/Workspace;->Qf:I

    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/Workspace;->S(II)V

    goto :goto_0

    .line 1217
    :cond_1
    iget v0, p0, Lcom/android/launcher3/Workspace;->Qc:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/android/launcher3/Workspace;->Qc:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/Workspace;->S(II)V

    goto :goto_0
.end method

.method public final k(J)I
    .locals 3

    .prologue
    .line 880
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method protected final k(Landroid/view/MotionEvent;)V
    .locals 12

    .prologue
    const v11, 0x3f060a92

    const/4 v10, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1154
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->kW()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1203
    :cond_0
    :goto_0
    return-void

    .line 1156
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v3, p0, Lcom/android/launcher3/Workspace;->Ws:F

    sub-float v3, v0, v3

    .line 1157
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 1158
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iget v5, p0, Lcom/android/launcher3/Workspace;->Wt:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 1160
    invoke-static {v0, v10}, Ljava/lang/Float;->compare(FF)I

    move-result v5

    if-eqz v5, :cond_0

    .line 1162
    div-float v5, v4, v0

    .line 1163
    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->atan(D)D

    move-result-wide v6

    double-to-float v5, v6

    .line 1165
    iget v6, p0, Lcom/android/launcher3/Workspace;->mTouchSlop:I

    int-to-float v6, v6

    cmpl-float v0, v0, v6

    if-gtz v0, :cond_2

    iget v0, p0, Lcom/android/launcher3/Workspace;->mTouchSlop:I

    int-to-float v0, v0

    cmpl-float v0, v4, v0

    if-lez v0, :cond_3

    .line 1166
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->ju()V

    .line 1169
    :cond_3
    iget-wide v6, p0, Lcom/android/launcher3/Workspace;->Cb:J

    iget-wide v8, p0, Lcom/android/launcher3/Workspace;->Vr:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0xc8

    cmp-long v0, v6, v8

    if-lez v0, :cond_6

    move v0, v1

    .line 1172
    :goto_1
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jc()Z

    move-result v4

    if-eqz v4, :cond_8

    cmpg-float v3, v3, v10

    if-gez v3, :cond_7

    move v3, v1

    .line 1173
    :goto_2
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jf()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/android/launcher3/Workspace;->bM(I)J

    move-result-wide v6

    const-wide/16 v8, -0x12d

    cmp-long v4, v6, v8

    if-nez v4, :cond_a

    .line 1175
    :goto_3
    if-eqz v3, :cond_4

    if-eqz v1, :cond_4

    if-nez v0, :cond_0

    .line 1180
    :cond_4
    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VG:Lym;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VG:Lym;

    invoke-interface {v0}, Lym;->ig()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1187
    :cond_5
    const v0, 0x3f860a92

    cmpl-float v0, v5, v0

    if-gtz v0, :cond_0

    .line 1190
    cmpl-float v0, v5, v11

    if-lez v0, :cond_b

    .line 1195
    sub-float v0, v5, v11

    .line 1196
    div-float/2addr v0, v11

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 1198
    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x40800000    # 4.0f

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    invoke-super {p0, p1, v0}, Ladi;->a(Landroid/view/MotionEvent;F)V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 1169
    goto :goto_1

    :cond_7
    move v3, v2

    .line 1172
    goto :goto_2

    :cond_8
    cmpl-float v3, v3, v10

    if-lez v3, :cond_9

    move v3, v1

    goto :goto_2

    :cond_9
    move v3, v2

    goto :goto_2

    :cond_a
    move v1, v2

    .line 1173
    goto :goto_3

    .line 1201
    :cond_b
    invoke-super {p0, p1}, Ladi;->k(Landroid/view/MotionEvent;)V

    goto/16 :goto_0
.end method

.method public final kI()V
    .locals 1

    .prologue
    .line 419
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/Workspace;->VA:Z

    .line 420
    return-void
.end method

.method final kK()Lcom/android/launcher3/Folder;
    .locals 5

    .prologue
    .line 513
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v2

    .line 514
    invoke-virtual {v2}, Lcom/android/launcher3/DragLayer;->getChildCount()I

    move-result v3

    .line 515
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 516
    invoke-virtual {v2, v1}, Lcom/android/launcher3/DragLayer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 517
    instance-of v4, v0, Lcom/android/launcher3/Folder;

    if-eqz v4, :cond_0

    .line 518
    check-cast v0, Lcom/android/launcher3/Folder;

    .line 519
    invoke-virtual {v0}, Lcom/android/launcher3/Folder;->gt()Lvy;

    move-result-object v4

    iget-boolean v4, v4, Lvy;->Iz:Z

    if-eqz v4, :cond_0

    .line 523
    :goto_1
    return-object v0

    .line 515
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 523
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method final kL()Z
    .locals 1

    .prologue
    .line 527
    iget v0, p0, Lcom/android/launcher3/Workspace;->Qv:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final kM()V
    .locals 1

    .prologue
    .line 534
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 538
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lc()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 539
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->kO()V

    .line 543
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->removeAllViews()V

    .line 544
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 545
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 548
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->kJ()V

    .line 549
    return-void
.end method

.method public final kN()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x12d

    const/4 v3, 0x0

    .line 587
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401cd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 589
    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eO()V

    .line 590
    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eP()V

    .line 592
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 593
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 596
    invoke-virtual {v0, v3, v3, v3, v3}, Lcom/android/launcher3/CellLayout;->setPadding(IIII)V

    .line 598
    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->Z(Landroid/view/View;)V

    .line 601
    iget v0, p0, Lcom/android/launcher3/Workspace;->Vt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/launcher3/Workspace;->Vu:I

    .line 604
    iget v0, p0, Lcom/android/launcher3/Workspace;->Qd:I

    const/16 v1, -0x3e9

    if-eq v0, v1, :cond_0

    .line 605
    iget v0, p0, Lcom/android/launcher3/Workspace;->Qd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/launcher3/Workspace;->Qd:I

    .line 609
    :goto_0
    return-void

    .line 607
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jf()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->bu(I)V

    goto :goto_0
.end method

.method public final kO()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x12d

    .line 612
    invoke-virtual {p0, v4, v5}, Lcom/android/launcher3/Workspace;->j(J)Lcom/android/launcher3/CellLayout;

    move-result-object v0

    .line 613
    if-nez v0, :cond_0

    .line 614
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Expected custom content screen to exist"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 617
    :cond_0
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 618
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 619
    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->removeView(Landroid/view/View;)V

    .line 621
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VG:Lym;

    if-eqz v0, :cond_1

    .line 622
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VG:Lym;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lym;->m(F)V

    .line 623
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VG:Lym;

    invoke-interface {v0}, Lym;->if()V

    .line 626
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->VG:Lym;

    .line 629
    iget v0, p0, Lcom/android/launcher3/Workspace;->Vt:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/launcher3/Workspace;->Vu:I

    .line 632
    iget v0, p0, Lcom/android/launcher3/Workspace;->Qd:I

    const/16 v1, -0x3e9

    if-eq v0, v1, :cond_2

    .line 633
    iget v0, p0, Lcom/android/launcher3/Workspace;->Qd:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/launcher3/Workspace;->Qd:I

    .line 637
    :goto_0
    return-void

    .line 635
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jf()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->bu(I)V

    goto :goto_0
.end method

.method public final kP()V
    .locals 8

    .prologue
    const-wide/16 v6, -0xc9

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 670
    const-string v0, "Launcher.Workspace"

    const-string v1, "11683562 - addExtraEmptyScreenOnDrag()"

    invoke-static {v0, v1, v2}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 676
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Vz:Ljava/lang/Runnable;

    .line 678
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vv:Ladg;

    if-eqz v0, :cond_4

    .line 679
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vv:Ladg;

    invoke-virtual {v0}, Ladg;->getChildCount()I

    move-result v0

    if-ne v0, v2, :cond_3

    move v1, v2

    .line 682
    :goto_0
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vv:Ladg;

    invoke-virtual {v0}, Ladg;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 683
    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v0, v4, :cond_0

    move v3, v2

    .line 689
    :cond_0
    :goto_1
    if-eqz v1, :cond_2

    if-eqz v3, :cond_2

    .line 695
    :cond_1
    :goto_2
    return-void

    .line 692
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 693
    invoke-direct {p0, v6, v7}, Lcom/android/launcher3/Workspace;->i(J)J

    goto :goto_2

    :cond_3
    move v1, v3

    goto :goto_0

    :cond_4
    move v1, v3

    goto :goto_1
.end method

.method public final kQ()Z
    .locals 6

    .prologue
    const-wide/16 v4, -0xc9

    const/4 v0, 0x1

    .line 699
    const-string v1, "Launcher.Workspace"

    const-string v2, "11683562 - addExtraEmptyScreen()"

    invoke-static {v1, v2, v0}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 701
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 702
    invoke-direct {p0, v4, v5}, Lcom/android/launcher3/Workspace;->i(J)J

    .line 705
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final kR()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 829
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v1

    .line 830
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->ld()I

    move-result v2

    sub-int/2addr v1, v2

    .line 831
    iget-object v2, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    const-wide/16 v4, -0xc9

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final kS()J
    .locals 6

    .prologue
    const/4 v2, 0x1

    const-wide/16 v4, -0xc9

    .line 836
    const-string v0, "Launcher.Workspace"

    const-string v1, "11683562 - commitExtraEmptyScreen()"

    invoke-static {v0, v1, v2}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 837
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hz()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 839
    const-string v0, "Launcher.Workspace"

    const-string v1, "    - workspace loading, skip"

    invoke-static {v0, v1, v2}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 840
    const-wide/16 v0, -0x1

    .line 860
    :goto_0
    return-wide v0

    .line 843
    :cond_0
    invoke-virtual {p0, v4, v5}, Lcom/android/launcher3/Workspace;->k(J)I

    move-result v1

    .line 844
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 845
    iget-object v2, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 846
    iget-object v2, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 848
    invoke-static {}, Lyu;->il()Lcom/android/launcher3/LauncherProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/launcher3/LauncherProvider;->iL()J

    move-result-wide v2

    .line 849
    iget-object v4, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 850
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 853
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    if-eqz v0, :cond_1

    .line 854
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->bs(I)Lacg;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Lcom/android/launcher3/PageIndicator;->a(ILacg;)V

    .line 858
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hs()Lzi;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v4, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v4}, Lzi;->d(Landroid/content/Context;Ljava/util/ArrayList;)V

    move-wide v0, v2

    .line 860
    goto :goto_0
.end method

.method final kT()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 891
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final kU()V
    .locals 14

    .prologue
    const-wide/16 v12, -0xc9

    const/4 v10, 0x1

    .line 896
    const-string v0, "Launcher.Workspace"

    const-string v1, "11683562 - stripEmptyScreens()"

    invoke-static {v0, v1, v10}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 898
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hz()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 901
    const-string v0, "Launcher.Workspace"

    const-string v1, "    - workspace loading, skip"

    invoke-static {v0, v1, v10}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 951
    :cond_0
    :goto_0
    return-void

    .line 905
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jp()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 906
    iput-boolean v10, p0, Lcom/android/launcher3/Workspace;->Wa:Z

    goto :goto_0

    .line 910
    :cond_2
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jg()I

    move-result v3

    .line 911
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 912
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 913
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/CellLayout;

    .line 914
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-ltz v5, :cond_3

    invoke-virtual {v1}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v1

    invoke-virtual {v1}, Ladg;->getChildCount()I

    move-result v1

    if-nez v1, :cond_3

    .line 915
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 921
    :cond_4
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->ld()I

    move-result v0

    add-int/lit8 v5, v0, 0x1

    .line 923
    const/4 v0, 0x0

    .line 924
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v0

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 925
    const-string v1, "Launcher.Workspace"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "11683562 -   removing id: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7, v10}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 926
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/CellLayout;

    .line 927
    iget-object v7, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-virtual {v7, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 928
    iget-object v7, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 930
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v0

    if-le v0, v5, :cond_5

    .line 931
    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-ge v0, v3, :cond_8

    .line 932
    add-int/lit8 v0, v2, 0x1

    .line 934
    :goto_3
    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->removeView(Landroid/view/View;)V

    move v2, v0

    goto :goto_2

    .line 937
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Vz:Ljava/lang/Runnable;

    .line 938
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vx:Ljava/util/HashMap;

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v0, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 939
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 943
    :cond_6
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 945
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hs()Lzi;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v4, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v4}, Lzi;->d(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 948
    :cond_7
    if-ltz v2, :cond_0

    .line 949
    sub-int v0, v3, v2

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->bu(I)V

    goto/16 :goto_0

    :cond_8
    move v0, v2

    goto :goto_3
.end method

.method public final kV()Z
    .locals 1

    .prologue
    .line 1076
    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->VW:Z

    return v0
.end method

.method public final kW()Z
    .locals 2

    .prologue
    .line 1082
    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->VW:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/launcher3/Workspace;->WJ:F

    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final kX()V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 1130
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v6

    move v5, v4

    .line 1131
    :goto_0
    if-ge v5, v6, :cond_2

    .line 1132
    invoke-virtual {p0, v5}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 1133
    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v7

    .line 1134
    invoke-virtual {v7}, Ladg;->getChildCount()I

    move-result v8

    move v3, v4

    .line 1135
    :goto_1
    if-ge v3, v8, :cond_1

    .line 1136
    invoke-virtual {v7, v3}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1138
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lyy;

    if-eqz v2, :cond_0

    .line 1139
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lyy;

    .line 1140
    iget-object v2, v1, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    check-cast v2, Lyx;

    .line 1141
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lyx;->in()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1142
    iget-object v9, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v9, v1}, Lcom/android/launcher3/Launcher;->a(Lyy;)V

    .line 1144
    invoke-virtual {v0, v2}, Lcom/android/launcher3/CellLayout;->removeView(Landroid/view/View;)V

    .line 1145
    iget-object v2, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2, v1}, Lcom/android/launcher3/Launcher;->b(Lyy;)V

    .line 1135
    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 1131
    :cond_1
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 1150
    :cond_2
    return-void
.end method

.method protected final kY()Lym;
    .locals 1

    .prologue
    .line 1277
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VG:Lym;

    return-object v0
.end method

.method public final l(IZ)V
    .locals 1

    .prologue
    .line 2115
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/android/launcher3/Workspace;->a(ZIZ)V

    .line 2116
    return-void
.end method

.method public final l(J)V
    .locals 1

    .prologue
    .line 1307
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher3/Workspace;->a(JLjava/lang/Runnable;)V

    .line 1308
    return-void
.end method

.method final l(Lcom/android/launcher3/CellLayout;)V
    .locals 14

    .prologue
    const/4 v12, 0x0

    .line 4230
    invoke-virtual {p1}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v0

    invoke-virtual {v0}, Ladg;->getChildCount()I

    move-result v13

    .line 4232
    invoke-direct {p0, p1}, Lcom/android/launcher3/Workspace;->i(Lcom/android/launcher3/CellLayout;)J

    move-result-wide v4

    .line 4233
    const/16 v0, -0x64

    .line 4235
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1, p1}, Lcom/android/launcher3/Launcher;->Y(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4236
    const-wide/16 v4, -0x1

    .line 4237
    const/16 v0, -0x65

    move v10, v0

    :goto_0
    move v11, v12

    .line 4240
    :goto_1
    if-ge v11, v13, :cond_1

    .line 4241
    invoke-virtual {p1}, Lcom/android/launcher3/CellLayout;->eZ()Ladg;

    move-result-object v0

    invoke-virtual {v0, v11}, Ladg;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 4242
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lwq;

    .line 4244
    if-eqz v1, :cond_0

    iget-boolean v0, v1, Lwq;->JD:Z

    if-eqz v0, :cond_0

    .line 4245
    iput-boolean v12, v1, Lwq;->JD:Z

    .line 4246
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    int-to-long v2, v10

    iget v6, v1, Lwq;->Bb:I

    iget v7, v1, Lwq;->Bc:I

    iget v8, v1, Lwq;->AY:I

    iget v9, v1, Lwq;->AZ:I

    invoke-static/range {v0 .. v9}, Lzi;->a(Landroid/content/Context;Lwq;JJIIII)V

    .line 4240
    :cond_0
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    goto :goto_1

    .line 4250
    :cond_1
    return-void

    :cond_2
    move v10, v0

    goto :goto_0
.end method

.method public final lb()V
    .locals 1

    .prologue
    .line 1524
    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->QP:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->kL()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1525
    iget v0, p0, Lcom/android/launcher3/Workspace;->Qc:I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->bB(I)V

    .line 1527
    :cond_0
    return-void
.end method

.method public final lc()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1610
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Vy:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, -0x12d

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final ld()I
    .locals 1

    .prologue
    .line 1614
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lc()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final le()Z
    .locals 1

    .prologue
    .line 1618
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lc()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jg()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final lf()Z
    .locals 2

    .prologue
    .line 1813
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VV:Lagv;

    sget-object v1, Lagv;->Xv:Lagv;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final lg()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1894
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/launcher3/Workspace;->as(Z)V

    .line 1895
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1896
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v3

    move v1, v2

    .line 1897
    :goto_0
    if-ge v1, v3, :cond_0

    .line 1898
    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 1899
    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->eL()V

    .line 1897
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1902
    :cond_0
    invoke-direct {p0, v2}, Lcom/android/launcher3/Workspace;->as(Z)V

    .line 1903
    return-void
.end method

.method public final lh()V
    .locals 1

    .prologue
    .line 2037
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v0

    .line 2038
    invoke-virtual {v0}, Lcom/android/launcher3/DragLayer;->fL()V

    .line 2039
    return-void
.end method

.method public final li()Z
    .locals 2

    .prologue
    .line 2099
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VV:Lagv;

    sget-object v1, Lagv;->Xy:Lagv;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final lj()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2103
    iget v1, p0, Lcom/android/launcher3/Workspace;->Qv:I

    if-eqz v1, :cond_0

    .line 2104
    const/4 v0, 0x0

    .line 2107
    :goto_0
    return v0

    .line 2106
    :cond_0
    const/4 v1, -0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/android/launcher3/Workspace;->a(ZIZ)V

    goto :goto_0
.end method

.method final lk()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2152
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->le()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2158
    :cond_0
    :goto_0
    return v0

    .line 2155
    :cond_1
    iget-object v1, p0, Lcom/android/launcher3/Workspace;->VV:Lagv;

    sget-object v2, Lagv;->Xv:Lagv;

    if-ne v1, v2, :cond_0

    .line 2158
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ll()V
    .locals 2

    .prologue
    .line 2162
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VV:Lagv;

    sget-object v1, Lagv;->Xv:Lagv;

    if-eq v0, v1, :cond_0

    .line 2163
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hG()V

    .line 2167
    :goto_0
    return-void

    .line 2165
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hF()V

    goto :goto_0
.end method

.method final lm()Lagv;
    .locals 1

    .prologue
    .line 2176
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VV:Lagv;

    return-object v0
.end method

.method public final ls()V
    .locals 1

    .prologue
    .line 4109
    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->VW:Z

    if-eqz v0, :cond_0

    .line 4110
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getScaleX()F

    move-result v0

    iput v0, p0, Lcom/android/launcher3/Workspace;->WC:F

    .line 4111
    iget v0, p0, Lcom/android/launcher3/Workspace;->WD:F

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->setScaleX(F)V

    .line 4112
    iget v0, p0, Lcom/android/launcher3/Workspace;->WD:F

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->setScaleY(F)V

    .line 4114
    :cond_0
    return-void
.end method

.method public final lt()V
    .locals 1

    .prologue
    .line 4116
    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->VW:Z

    if-eqz v0, :cond_0

    .line 4117
    iget v0, p0, Lcom/android/launcher3/Workspace;->WC:F

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->setScaleX(F)V

    .line 4118
    iget v0, p0, Lcom/android/launcher3/Workspace;->WC:F

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->setScaleY(F)V

    .line 4120
    :cond_0
    return-void
.end method

.method public final lv()I
    .locals 2

    .prologue
    .line 4141
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jg()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->ld()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public final lw()V
    .locals 4

    .prologue
    .line 4435
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v1

    .line 4436
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 4437
    iget-object v2, p0, Lcom/android/launcher3/Workspace;->Wy:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4438
    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->bQ(I)V

    .line 4436
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4441
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Wy:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4442
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->Wx:Landroid/util/SparseArray;

    .line 4443
    return-void
.end method

.method final ly()V
    .locals 2

    .prologue
    .line 4620
    const/4 v0, 0x0

    new-instance v1, Lagb;

    invoke-direct {v1, p0}, Lagb;-><init>(Lcom/android/launcher3/Workspace;)V

    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/Workspace;->a(ZLagt;)V

    .line 4630
    return-void
.end method

.method public final n(Ljava/util/ArrayList;)V
    .locals 5

    .prologue
    .line 4957
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 4959
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahv;

    .line 4960
    const/4 v3, 0x1

    new-instance v4, Lagg;

    invoke-direct {v4, p0, v0}, Lagg;-><init>(Lcom/android/launcher3/Workspace;Lahv;)V

    invoke-direct {p0, v3, v4}, Lcom/android/launcher3/Workspace;->a(ZLagt;)V

    .line 4988
    iget v3, v0, Lahv;->state:I

    if-nez v3, :cond_0

    .line 4989
    iget-object v0, v0, Lahv;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 4994
    :cond_1
    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 4995
    invoke-direct {p0, v1}, Lcom/android/launcher3/Workspace;->a(Ljava/util/Set;)V

    .line 4997
    :cond_2
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 1727
    invoke-super {p0}, Ladi;->onAttachedToWindow()V

    .line 1728
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->EC:Landroid/os/IBinder;

    .line 1729
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->computeScroll()V

    .line 1730
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->yg:Lty;

    iget-object v1, p0, Lcom/android/launcher3/Workspace;->EC:Landroid/os/IBinder;

    invoke-virtual {v0, v1}, Lty;->a(Landroid/os/IBinder;)V

    .line 1731
    return-void
.end method

.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 491
    instance-of v0, p2, Lcom/android/launcher3/CellLayout;

    if-nez v0, :cond_0

    .line 492
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A Workspace can only have CellLayout children."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p2

    .line 494
    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 495
    invoke-virtual {v0, p0}, Lcom/android/launcher3/CellLayout;->a(Landroid/view/View$OnTouchListener;)V

    .line 496
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CellLayout;->setClickable(Z)V

    .line 497
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/launcher3/CellLayout;->setImportantForAccessibility(I)V

    .line 498
    invoke-super {p0, p1, p2}, Ladi;->onChildViewAdded(Landroid/view/View;Landroid/view/View;)V

    .line 499
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 1734
    invoke-super {p0}, Ladi;->onDetachedFromWindow()V

    .line 1735
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/Workspace;->EC:Landroid/os/IBinder;

    .line 1736
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1773
    invoke-super {p0, p1}, Ladi;->onDraw(Landroid/graphics/Canvas;)V

    .line 1776
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->WL:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->post(Ljava/lang/Runnable;)Z

    .line 1777
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 1121
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->jf()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->bM(I)J

    move-result-wide v0

    const-wide/16 v2, -0x12d

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VG:Lym;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->VG:Lym;

    invoke-interface {v0}, Lym;->ig()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1124
    const/4 v0, 0x0

    .line 1126
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Ladi;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1100
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    sparse-switch v0, :sswitch_data_0

    .line 1115
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Ladi;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 1102
    :sswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/android/launcher3/Workspace;->Ws:F

    .line 1103
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/android/launcher3/Workspace;->Wt:F

    .line 1104
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/launcher3/Workspace;->Cb:J

    goto :goto_0

    .line 1108
    :sswitch_1
    iget v0, p0, Lcom/android/launcher3/Workspace;->Qv:I

    if-nez v0, :cond_0

    .line 1109
    iget v0, p0, Lcom/android/launcher3/Workspace;->Qc:I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 1110
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->fj()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1111
    iget-object v4, p0, Lcom/android/launcher3/Workspace;->VN:[I

    invoke-virtual {p0, v4}, Lcom/android/launcher3/Workspace;->getLocationOnScreen([I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    aget v1, v4, v5

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v1, v2

    aput v1, v4, v5

    aget v1, v4, v6

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    float-to-int v0, v0

    add-int/2addr v0, v1

    aput v0, v4, v6

    iget-object v0, p0, Lcom/android/launcher3/Workspace;->SQ:Landroid/app/WallpaperManager;

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v6, :cond_1

    const-string v2, "android.wallpaper.tap"

    :goto_1
    aget v3, v4, v5

    aget v4, v4, v6

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/app/WallpaperManager;->sendWallpaperCommand(Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;)V

    goto :goto_0

    :cond_1
    const-string v2, "android.wallpaper.secondaryTap"

    goto :goto_1

    .line 1100
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x6 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 1764
    iget-boolean v0, p0, Lcom/android/launcher3/Workspace;->gT:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/launcher3/Workspace;->Qc:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/android/launcher3/Workspace;->Qc:I

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1765
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Wf:Lagw;

    invoke-virtual {v0}, Lagw;->lC()V

    .line 1766
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->Wf:Lagw;

    invoke-virtual {v0}, Lagw;->lE()V

    .line 1768
    :cond_0
    invoke-super/range {p0 .. p5}, Ladi;->onLayout(ZIIII)V

    .line 1769
    return-void
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 1

    .prologue
    .line 1781
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hI()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1782
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->kK()Lcom/android/launcher3/Folder;

    move-result-object v0

    .line 1783
    if-eqz v0, :cond_0

    .line 1784
    invoke-virtual {v0, p1, p2}, Lcom/android/launcher3/Folder;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    .line 1789
    :goto_0
    return v0

    .line 1786
    :cond_0
    invoke-super {p0, p1, p2}, Ladi;->onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z

    move-result v0

    goto :goto_0

    .line 1789
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 4412
    invoke-super {p0, p1}, Ladi;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 4413
    iget v0, p0, Lcom/android/launcher3/Workspace;->Qc:I

    invoke-static {v0}, Lcom/android/launcher3/Launcher;->bk(I)V

    .line 4414
    return-void
.end method

.method protected final onResume()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1739
    iget-object v0, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    if-eqz v0, :cond_0

    .line 1742
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->iZ()Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 1743
    if-eqz v0, :cond_0

    .line 1744
    iget-object v2, p0, Lcom/android/launcher3/PagedView;->QT:Lcom/android/launcher3/PageIndicator;

    invoke-virtual {v2, v0}, Lcom/android/launcher3/PageIndicator;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1747
    :cond_0
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "accessibility"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 1749
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    sput-boolean v0, Lcom/android/launcher3/Workspace;->Vw:Z

    .line 1753
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    iget-boolean v2, v0, Lyu;->Mg:Z

    iput-boolean v1, v0, Lyu;->Mg:Z

    if-eqz v2, :cond_1

    .line 1754
    invoke-direct {p0}, Lcom/android/launcher3/Workspace;->kZ()V

    .line 1756
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->SQ:Landroid/app/WallpaperManager;

    invoke-virtual {v0}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/android/launcher3/Workspace;->Wg:Z

    .line 1759
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher3/Workspace;->Wi:F

    .line 1760
    return-void

    :cond_2
    move v0, v1

    .line 1756
    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 1071
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lf()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->kW()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->lf()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/android/launcher3/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v0

    iget v1, p0, Lcom/android/launcher3/Workspace;->Qc:I

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1

    .prologue
    .line 1086
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0, p1}, Lcom/android/launcher3/Launcher;->onWindowVisibilityChanged(I)V

    .line 1087
    return-void
.end method

.method final r(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 4253
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 4254
    iget-object v0, p0, Lcom/android/launcher3/Workspace;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hp()Lcom/android/launcher3/Hotseat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/Hotseat;->gW()Lcom/android/launcher3/CellLayout;

    move-result-object v0

    invoke-direct {p0, v0, v3, v5, v2}, Lcom/android/launcher3/Workspace;->a(Lcom/android/launcher3/CellLayout;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    .line 4255
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v4

    move v1, v2

    .line 4256
    :goto_0
    if-ge v1, v4, :cond_0

    .line 4257
    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 4258
    invoke-direct {p0, v0, v3, v5, v2}, Lcom/android/launcher3/Workspace;->a(Lcom/android/launcher3/CellLayout;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    .line 4256
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 4260
    :cond_0
    return-object v3
.end method

.method final s(Ljava/util/ArrayList;)V
    .locals 5

    .prologue
    .line 4805
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 4807
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrr;

    .line 4808
    iget-object v1, v0, Lrr;->Jl:Lahz;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 4809
    if-nez v1, :cond_0

    .line 4810
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 4811
    iget-object v4, v0, Lrr;->Jl:Lahz;

    invoke-virtual {v2, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4813
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 4816
    :cond_1
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 4817
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahz;

    invoke-direct {p0, v1, v0}, Lcom/android/launcher3/Workspace;->c(Ljava/util/ArrayList;Lahz;)V

    goto :goto_1

    .line 4819
    :cond_2
    return-void
.end method

.method public setChildrenOutlineAlpha(F)V
    .locals 2

    .prologue
    .line 1530
    iput p1, p0, Lcom/android/launcher3/Workspace;->Vp:F

    .line 1531
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/android/launcher3/Workspace;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1532
    invoke-virtual {p0, v1}, Lcom/android/launcher3/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout;

    .line 1533
    invoke-virtual {v0, p1}, Lcom/android/launcher3/CellLayout;->setBackgroundAlpha(F)V

    .line 1531
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1535
    :cond_0
    return-void
.end method
