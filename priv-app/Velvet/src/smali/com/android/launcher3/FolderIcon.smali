.class public Lcom/android/launcher3/FolderIcon;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lvz;


# static fields
.field private static HJ:Z

.field public static HK:Landroid/graphics/drawable/Drawable;


# instance fields
.field private GM:Lvy;

.field private HI:Lcom/android/launcher3/Folder;

.field private HL:Landroid/widget/ImageView;

.field private HM:Lcom/android/launcher3/BubbleTextView;

.field private HN:Lvs;

.field private HO:I

.field private HP:F

.field private HQ:I

.field private HR:I

.field private HS:I

.field private HT:I

.field private HU:I

.field private HV:F

.field private HW:Landroid/graphics/Rect;

.field private HX:Lvx;

.field private HY:Lvx;

.field private HZ:Ljava/util/ArrayList;

.field private Ia:Lro;

.field private Ib:Lwq;

.field private Ic:Lace;

.field public hN:Z

.field private xZ:Lcom/android/launcher3/Launcher;

.field private zA:Ltf;

.field private zt:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/launcher3/FolderIcon;->HJ:Z

    .line 89
    const/4 v0, 0x0

    sput-object v0, Lcom/android/launcher3/FolderIcon;->HK:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 124
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/FolderIcon;->HN:Lvs;

    .line 102
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher3/FolderIcon;->HS:I

    .line 106
    iput-boolean v5, p0, Lcom/android/launcher3/FolderIcon;->hN:Z

    .line 107
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/FolderIcon;->HW:Landroid/graphics/Rect;

    .line 111
    new-instance v0, Lvx;

    move-object v1, p0

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lvx;-><init>(Lcom/android/launcher3/FolderIcon;FFFI)V

    iput-object v0, p0, Lcom/android/launcher3/FolderIcon;->HX:Lvx;

    .line 112
    new-instance v0, Lvx;

    move-object v1, p0

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lvx;-><init>(Lcom/android/launcher3/FolderIcon;FFFI)V

    iput-object v0, p0, Lcom/android/launcher3/FolderIcon;->HY:Lvx;

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/FolderIcon;->HZ:Ljava/util/ArrayList;

    .line 115
    new-instance v0, Lro;

    invoke-direct {v0}, Lro;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/FolderIcon;->Ia:Lro;

    .line 362
    new-instance v0, Lvo;

    invoke-direct {v0, p0}, Lvo;-><init>(Lcom/android/launcher3/FolderIcon;)V

    iput-object v0, p0, Lcom/android/launcher3/FolderIcon;->Ic:Lace;

    .line 125
    invoke-direct {p0}, Lcom/android/launcher3/FolderIcon;->ed()V

    .line 126
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 119
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher3/FolderIcon;->HN:Lvs;

    .line 102
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher3/FolderIcon;->HS:I

    .line 106
    iput-boolean v5, p0, Lcom/android/launcher3/FolderIcon;->hN:Z

    .line 107
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/FolderIcon;->HW:Landroid/graphics/Rect;

    .line 111
    new-instance v0, Lvx;

    move-object v1, p0

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lvx;-><init>(Lcom/android/launcher3/FolderIcon;FFFI)V

    iput-object v0, p0, Lcom/android/launcher3/FolderIcon;->HX:Lvx;

    .line 112
    new-instance v0, Lvx;

    move-object v1, p0

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lvx;-><init>(Lcom/android/launcher3/FolderIcon;FFFI)V

    iput-object v0, p0, Lcom/android/launcher3/FolderIcon;->HY:Lvx;

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/FolderIcon;->HZ:Ljava/util/ArrayList;

    .line 115
    new-instance v0, Lro;

    invoke-direct {v0}, Lro;-><init>()V

    iput-object v0, p0, Lcom/android/launcher3/FolderIcon;->Ia:Lro;

    .line 362
    new-instance v0, Lvo;

    invoke-direct {v0, p0}, Lvo;-><init>(Lcom/android/launcher3/FolderIcon;)V

    iput-object v0, p0, Lcom/android/launcher3/FolderIcon;->Ic:Lace;

    .line 120
    invoke-direct {p0}, Lcom/android/launcher3/FolderIcon;->ed()V

    .line 121
    return-void
.end method

.method private I(II)V
    .locals 5

    .prologue
    .line 490
    iget v0, p0, Lcom/android/launcher3/FolderIcon;->HO:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/android/launcher3/FolderIcon;->HS:I

    if-eq v0, p2, :cond_1

    .line 491
    :cond_0
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 492
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v0

    .line 494
    iput p1, p0, Lcom/android/launcher3/FolderIcon;->HO:I

    .line 495
    iput p2, p0, Lcom/android/launcher3/FolderIcon;->HS:I

    .line 497
    iget-object v1, p0, Lcom/android/launcher3/FolderIcon;->HL:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 498
    sget v2, Lvs;->Iq:I

    .line 500
    mul-int/lit8 v3, v2, 0x2

    sub-int/2addr v1, v3

    iput v1, p0, Lcom/android/launcher3/FolderIcon;->HR:I

    .line 502
    iget v1, p0, Lcom/android/launcher3/FolderIcon;->HR:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    const v3, 0x3fe66666    # 1.8f

    mul-float/2addr v1, v3

    float-to-int v1, v1

    .line 504
    iget v3, p0, Lcom/android/launcher3/FolderIcon;->HO:I

    int-to-float v3, v3

    const v4, 0x3f970a3e    # 1.1800001f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 506
    const/high16 v4, 0x3f800000    # 1.0f

    int-to-float v1, v1

    mul-float/2addr v1, v4

    int-to-float v3, v3

    div-float/2addr v1, v3

    iput v1, p0, Lcom/android/launcher3/FolderIcon;->HP:F

    .line 508
    iget v1, p0, Lcom/android/launcher3/FolderIcon;->HO:I

    int-to-float v1, v1

    iget v3, p0, Lcom/android/launcher3/FolderIcon;->HP:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Lcom/android/launcher3/FolderIcon;->HQ:I

    .line 509
    iget v1, p0, Lcom/android/launcher3/FolderIcon;->HQ:I

    int-to-float v1, v1

    const v3, 0x3e3851ec    # 0.18f

    mul-float/2addr v1, v3

    iput v1, p0, Lcom/android/launcher3/FolderIcon;->HV:F

    .line 511
    iget v1, p0, Lcom/android/launcher3/FolderIcon;->HS:I

    iget v3, p0, Lcom/android/launcher3/FolderIcon;->HR:I

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/android/launcher3/FolderIcon;->HT:I

    .line 512
    iget v0, v0, Ltu;->DR:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/android/launcher3/FolderIcon;->HU:I

    .line 514
    :cond_1
    return-void
.end method

.method public static synthetic R(Z)Z
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/launcher3/FolderIcon;->HJ:Z

    return v0
.end method

.method static a(ILcom/android/launcher3/Launcher;Landroid/view/ViewGroup;Lvy;)Lcom/android/launcher3/FolderIcon;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 142
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 149
    iget-object v0, v0, Lyu;->Mk:Lur;

    invoke-virtual {v0}, Lur;->gl()Ltu;

    move-result-object v2

    .line 151
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, p0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/FolderIcon;

    .line 152
    invoke-virtual {v0, v5}, Lcom/android/launcher3/FolderIcon;->setClipToPadding(Z)V

    .line 153
    const v1, 0x7f1101e2

    invoke-virtual {v0, v1}, Lcom/android/launcher3/FolderIcon;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/BubbleTextView;

    iput-object v1, v0, Lcom/android/launcher3/FolderIcon;->HM:Lcom/android/launcher3/BubbleTextView;

    .line 154
    iget-object v1, v0, Lcom/android/launcher3/FolderIcon;->HM:Lcom/android/launcher3/BubbleTextView;

    iget-object v3, p3, Lvy;->title:Ljava/lang/CharSequence;

    invoke-virtual {v1, v3}, Lcom/android/launcher3/BubbleTextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    iget-object v1, v0, Lcom/android/launcher3/FolderIcon;->HM:Lcom/android/launcher3/BubbleTextView;

    invoke-virtual {v1, v5}, Lcom/android/launcher3/BubbleTextView;->setCompoundDrawablePadding(I)V

    .line 156
    iget-object v1, v0, Lcom/android/launcher3/FolderIcon;->HM:Lcom/android/launcher3/BubbleTextView;

    invoke-virtual {v1}, Lcom/android/launcher3/BubbleTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 157
    iget v3, v2, Ltu;->DI:I

    iget v4, v2, Ltu;->DK:I

    add-int/2addr v3, v4

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 160
    const v1, 0x7f1101e1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/FolderIcon;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/android/launcher3/FolderIcon;->HL:Landroid/widget/ImageView;

    .line 161
    iget-object v1, v0, Lcom/android/launcher3/FolderIcon;->HL:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 162
    iget v3, v2, Ltu;->DR:I

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 163
    iget v3, v2, Ltu;->DS:I

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 164
    iget v2, v2, Ltu;->DS:I

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 166
    invoke-virtual {v0, p3}, Lcom/android/launcher3/FolderIcon;->setTag(Ljava/lang/Object;)V

    .line 167
    invoke-virtual {v0, p1}, Lcom/android/launcher3/FolderIcon;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    iput-object p3, v0, Lcom/android/launcher3/FolderIcon;->GM:Lvy;

    .line 169
    iput-object p1, v0, Lcom/android/launcher3/FolderIcon;->xZ:Lcom/android/launcher3/Launcher;

    .line 170
    const v1, 0x7f0a00db

    invoke-virtual {p1, v1}, Lcom/android/launcher3/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p3, Lvy;->title:Ljava/lang/CharSequence;

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/FolderIcon;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 172
    invoke-static {p1}, Lcom/android/launcher3/Folder;->j(Landroid/content/Context;)Lcom/android/launcher3/Folder;

    move-result-object v1

    .line 173
    invoke-virtual {p1}, Lcom/android/launcher3/Launcher;->hx()Lty;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/launcher3/Folder;->d(Lty;)V

    .line 174
    invoke-virtual {v1, v0}, Lcom/android/launcher3/Folder;->a(Lcom/android/launcher3/FolderIcon;)V

    .line 175
    invoke-virtual {v1, p3}, Lcom/android/launcher3/Folder;->a(Lvy;)V

    .line 176
    iput-object v1, v0, Lcom/android/launcher3/FolderIcon;->HI:Lcom/android/launcher3/Folder;

    .line 178
    new-instance v1, Lvs;

    invoke-direct {v1, p1, v0}, Lvs;-><init>(Lcom/android/launcher3/Launcher;Lcom/android/launcher3/FolderIcon;)V

    iput-object v1, v0, Lcom/android/launcher3/FolderIcon;->HN:Lvs;

    .line 179
    invoke-virtual {p3, v0}, Lvy;->a(Lvz;)V

    .line 181
    iget-object v1, p1, Lcom/android/launcher3/Launcher;->KS:Lcom/android/launcher3/FocusIndicatorView;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/FolderIcon;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 182
    return-object v0
.end method

.method private a(ILvx;)Lvx;
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v6, 0x3f800000    # 1.0f

    .line 549
    rsub-int/lit8 v0, p1, 0x3

    add-int/lit8 v0, v0, -0x1

    .line 550
    int-to-float v0, v0

    mul-float/2addr v0, v6

    div-float/2addr v0, v7

    .line 551
    const v1, 0x3eb33333    # 0.35f

    sub-float v2, v6, v0

    mul-float/2addr v1, v2

    sub-float v1, v6, v1

    .line 553
    sub-float v2, v6, v0

    iget v3, p0, Lcom/android/launcher3/FolderIcon;->HV:F

    mul-float/2addr v2, v3

    .line 554
    iget v3, p0, Lcom/android/launcher3/FolderIcon;->HQ:I

    int-to-float v3, v3

    mul-float v4, v1, v3

    .line 555
    sub-float v3, v6, v1

    iget v5, p0, Lcom/android/launcher3/FolderIcon;->HQ:I

    int-to-float v5, v5

    mul-float/2addr v3, v5

    .line 559
    iget v5, p0, Lcom/android/launcher3/FolderIcon;->HR:I

    int-to-float v5, v5

    add-float/2addr v2, v4

    add-float/2addr v2, v3

    sub-float v2, v5, v2

    invoke-virtual {p0}, Lcom/android/launcher3/FolderIcon;->getPaddingTop()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v3, v2

    .line 560
    iget v2, p0, Lcom/android/launcher3/FolderIcon;->HR:I

    int-to-float v2, v2

    sub-float/2addr v2, v4

    div-float/2addr v2, v7

    .line 561
    iget v4, p0, Lcom/android/launcher3/FolderIcon;->HP:F

    mul-float/2addr v4, v1

    .line 562
    const/high16 v1, 0x42a00000    # 80.0f

    sub-float v0, v6, v0

    mul-float/2addr v0, v1

    float-to-int v5, v0

    .line 564
    if-nez p2, :cond_0

    .line 565
    new-instance v0, Lvx;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lvx;-><init>(Lcom/android/launcher3/FolderIcon;FFFI)V

    .line 572
    :goto_0
    return-object v0

    .line 567
    :cond_0
    iput v2, p2, Lvx;->Iv:F

    .line 568
    iput v3, p2, Lvx;->Iw:F

    .line 569
    iput v4, p2, Lvx;->Ix:F

    .line 570
    iput v5, p2, Lvx;->Iy:I

    move-object v0, p2

    goto :goto_0
.end method

.method private a(Ladh;Luj;Landroid/graphics/Rect;FILjava/lang/Runnable;)V
    .locals 17

    .prologue
    .line 420
    const/4 v2, -0x1

    move-object/from16 v0, p1

    iput v2, v0, Ladh;->Bb:I

    .line 421
    const/4 v2, -0x1

    move-object/from16 v0, p1

    iput v2, v0, Ladh;->Bc:I

    .line 426
    if-eqz p2, :cond_1

    .line 427
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/FolderIcon;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v2

    .line 428
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 429
    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v4}, Lcom/android/launcher3/DragLayer;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 431
    if-nez p3, :cond_2

    .line 432
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 433
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/FolderIcon;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v3

    .line 435
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/FolderIcon;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    invoke-virtual {v3}, Lcom/android/launcher3/Workspace;->ls()V

    .line 436
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/FolderIcon;->getScaleX()F

    move-result v6

    .line 437
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/FolderIcon;->getScaleY()F

    move-result v7

    .line 438
    const/high16 v8, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/android/launcher3/FolderIcon;->setScaleX(F)V

    .line 439
    const/high16 v8, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/android/launcher3/FolderIcon;->setScaleY(F)V

    .line 440
    move-object/from16 v0, p0

    invoke-virtual {v2, v0, v5}, Lcom/android/launcher3/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    move-result p4

    .line 442
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/launcher3/FolderIcon;->setScaleX(F)V

    .line 443
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/android/launcher3/FolderIcon;->setScaleY(F)V

    .line 444
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher3/FolderIcon;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    invoke-virtual {v3}, Lcom/android/launcher3/Workspace;->lt()V

    .line 447
    :goto_0
    const/4 v3, 0x2

    new-array v3, v3, [I

    .line 448
    const/4 v6, 0x3

    move/from16 v0, p5

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher3/FolderIcon;->HX:Lvx;

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/android/launcher3/FolderIcon;->a(ILvx;)Lvx;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/android/launcher3/FolderIcon;->HX:Lvx;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/FolderIcon;->HX:Lvx;

    iget v7, v6, Lvx;->Iv:F

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/launcher3/FolderIcon;->HT:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iput v7, v6, Lvx;->Iv:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/FolderIcon;->HX:Lvx;

    iget v7, v6, Lvx;->Iw:F

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/launcher3/FolderIcon;->HU:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iput v7, v6, Lvx;->Iw:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/FolderIcon;->HX:Lvx;

    iget v6, v6, Lvx;->Iv:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher3/FolderIcon;->HX:Lvx;

    iget v7, v7, Lvx;->Ix:F

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/launcher3/FolderIcon;->HO:I

    int-to-float v8, v8

    mul-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    add-float/2addr v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher3/FolderIcon;->HX:Lvx;

    iget v7, v7, Lvx;->Iw:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher3/FolderIcon;->HX:Lvx;

    iget v8, v8, Lvx;->Ix:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/launcher3/FolderIcon;->HO:I

    int-to-float v9, v9

    mul-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    const/4 v8, 0x0

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    aput v6, v3, v8

    const/4 v6, 0x1

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    aput v7, v3, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher3/FolderIcon;->HX:Lvx;

    iget v7, v6, Lvx;->Ix:F

    .line 449
    const/4 v6, 0x0

    const/4 v8, 0x0

    aget v8, v3, v8

    int-to-float v8, v8

    mul-float v8, v8, p4

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    aput v8, v3, v6

    .line 450
    const/4 v6, 0x1

    const/4 v8, 0x1

    aget v8, v3, v8

    int-to-float v8, v8

    mul-float v8, v8, p4

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    aput v8, v3, v6

    .line 452
    const/4 v6, 0x0

    aget v6, v3, v6

    invoke-virtual/range {p2 .. p2}, Luj;->getMeasuredWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v6, v8

    const/4 v8, 0x1

    aget v3, v3, v8

    invoke-virtual/range {p2 .. p2}, Luj;->getMeasuredHeight()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v3, v8

    invoke-virtual {v5, v6, v3}, Landroid/graphics/Rect;->offset(II)V

    .line 455
    const/4 v3, 0x3

    move/from16 v0, p5

    if-ge v0, v3, :cond_0

    const/high16 v6, 0x3f000000    # 0.5f

    .line 457
    :goto_1
    mul-float v9, v7, p4

    .line 458
    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v8, 0x3f800000    # 1.0f

    const/16 v11, 0x190

    new-instance v12, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-direct {v12, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    new-instance v13, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-direct {v13, v3}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v3, p2

    move v10, v9

    move-object/from16 v14, p6

    invoke-virtual/range {v2 .. v16}, Lcom/android/launcher3/DragLayer;->a(Luj;Landroid/graphics/Rect;Landroid/graphics/Rect;FFFFFILandroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;Ljava/lang/Runnable;ILandroid/view/View;)V

    .line 462
    invoke-virtual/range {p0 .. p1}, Lcom/android/launcher3/FolderIcon;->i(Ladh;)V

    .line 463
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/FolderIcon;->HZ:Ljava/util/ArrayList;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 464
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/FolderIcon;->HI:Lcom/android/launcher3/Folder;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/android/launcher3/Folder;->d(Ladh;)V

    .line 465
    new-instance v2, Lvp;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v2, v0, v1}, Lvp;-><init>(Lcom/android/launcher3/FolderIcon;Ladh;)V

    const-wide/16 v4, 0x190

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4, v5}, Lcom/android/launcher3/FolderIcon;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 475
    :goto_2
    return-void

    .line 455
    :cond_0
    const/4 v6, 0x0

    goto :goto_1

    .line 473
    :cond_1
    invoke-virtual/range {p0 .. p1}, Lcom/android/launcher3/FolderIcon;->i(Ladh;)V

    goto :goto_2

    :cond_2
    move-object/from16 v5, p3

    goto/16 :goto_0
.end method

.method private a(Landroid/graphics/Canvas;Lvx;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0xff

    .line 576
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 577
    iget v0, p2, Lvx;->Iv:F

    iget v1, p0, Lcom/android/launcher3/FolderIcon;->HT:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget v1, p2, Lvx;->Iw:F

    iget v2, p0, Lcom/android/launcher3/FolderIcon;->HU:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 578
    iget v0, p2, Lvx;->Ix:F

    iget v1, p2, Lvx;->Ix:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 579
    iget-object v1, p2, Lvx;->rJ:Landroid/graphics/drawable/Drawable;

    .line 581
    if-eqz v1, :cond_0

    .line 582
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HW:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 583
    iget v0, p0, Lcom/android/launcher3/FolderIcon;->HO:I

    iget v2, p0, Lcom/android/launcher3/FolderIcon;->HO:I

    invoke-virtual {v1, v4, v4, v0, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 584
    instance-of v0, v1, Lus;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 585
    check-cast v0, Lus;

    .line 586
    invoke-virtual {v0}, Lus;->gm()I

    move-result v2

    .line 587
    iget v3, p2, Lvx;->Iy:I

    invoke-virtual {v0, v3}, Lus;->bc(I)V

    .line 588
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 589
    invoke-virtual {v0, v2}, Lus;->bc(I)V

    .line 596
    :goto_0
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HW:Landroid/graphics/Rect;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 598
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 599
    return-void

    .line 591
    :cond_1
    iget v0, p2, Lvx;->Iy:I

    invoke-static {v0, v3, v3, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 593
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 594
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    goto :goto_0
.end method

.method private a(Landroid/graphics/drawable/Drawable;IZLjava/lang/Runnable;)V
    .locals 7

    .prologue
    .line 644
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/FolderIcon;->a(ILvx;)Lvx;

    move-result-object v4

    .line 646
    iget v0, p0, Lcom/android/launcher3/FolderIcon;->HR:I

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v3, v0

    .line 648
    iget v0, p0, Lcom/android/launcher3/FolderIcon;->HR:I

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, Lcom/android/launcher3/FolderIcon;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    int-to-float v5, v0

    .line 649
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HY:Lvx;

    iput-object p1, v0, Lvx;->rJ:Landroid/graphics/drawable/Drawable;

    .line 651
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Lyr;->c([F)Landroid/animation/ValueAnimator;

    move-result-object v6

    .line 652
    new-instance v0, Lvq;

    move-object v1, p0

    move v2, p3

    invoke-direct/range {v0 .. v5}, Lvq;-><init>(Lcom/android/launcher3/FolderIcon;ZFLvx;F)V

    invoke-virtual {v6, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 666
    new-instance v0, Lvr;

    invoke-direct {v0, p0, p4}, Lvr;-><init>(Lcom/android/launcher3/FolderIcon;Ljava/lang/Runnable;)V

    invoke-virtual {v6, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 679
    int-to-long v0, p2

    invoke-virtual {v6, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 680
    invoke-virtual {v6}, Landroid/animation/ValueAnimator;->start()V

    .line 681
    return-void

    .line 651
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static synthetic b(Lcom/android/launcher3/FolderIcon;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HL:Landroid/widget/ImageView;

    return-object v0
.end method

.method private b(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 517
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher3/FolderIcon;->getMeasuredWidth()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/launcher3/FolderIcon;->I(II)V

    .line 518
    return-void
.end method

.method private b(Lwq;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 324
    iget v1, p1, Lwq;->Jz:I

    .line 325
    if-eqz v1, :cond_0

    if-ne v1, v0, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/android/launcher3/FolderIcon;->HI:Lcom/android/launcher3/Folder;

    invoke-virtual {v1}, Lcom/android/launcher3/Folder;->isFull()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/launcher3/FolderIcon;->GM:Lvy;

    if-eq p1, v1, :cond_1

    iget-object v1, p0, Lcom/android/launcher3/FolderIcon;->GM:Lvy;

    iget-boolean v1, v1, Lvy;->Iz:Z

    if-nez v1, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Landroid/widget/TextView;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 638
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    .line 639
    instance-of v1, v0, Lcom/android/launcher3/PreloadIconDrawable;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/launcher3/PreloadIconDrawable;

    iget-object v0, v0, Lcom/android/launcher3/PreloadIconDrawable;->Sk:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-object v0
.end method

.method public static synthetic c(Lcom/android/launcher3/FolderIcon;)Lwq;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->Ib:Lwq;

    return-object v0
.end method

.method public static synthetic d(Lcom/android/launcher3/FolderIcon;)Lcom/android/launcher3/Folder;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HI:Lcom/android/launcher3/Folder;

    return-object v0
.end method

.method public static synthetic e(Lcom/android/launcher3/FolderIcon;)Lcom/android/launcher3/Launcher;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->xZ:Lcom/android/launcher3/Launcher;

    return-object v0
.end method

.method private ed()V
    .locals 1

    .prologue
    .line 129
    new-instance v0, Ltf;

    invoke-direct {v0, p0}, Ltf;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/android/launcher3/FolderIcon;->zA:Ltf;

    .line 130
    return-void
.end method

.method public static synthetic f(Lcom/android/launcher3/FolderIcon;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HZ:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static synthetic g(Lcom/android/launcher3/FolderIcon;)Lvx;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HY:Lvx;

    return-object v0
.end method

.method public static synthetic gP()Z
    .locals 1

    .prologue
    .line 52
    sget-boolean v0, Lcom/android/launcher3/FolderIcon;->HJ:Z

    return v0
.end method


# virtual methods
.method public final Q(Z)V
    .locals 2

    .prologue
    .line 684
    if-eqz p1, :cond_0

    .line 685
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HM:Lcom/android/launcher3/BubbleTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher3/BubbleTextView;->setVisibility(I)V

    .line 689
    :goto_0
    return-void

    .line 687
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HM:Lcom/android/launcher3/BubbleTextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/launcher3/BubbleTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Ladh;Landroid/view/View;Ladh;Luj;Landroid/graphics/Rect;FLjava/lang/Runnable;)V
    .locals 7

    .prologue
    .line 384
    move-object v0, p2

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/launcher3/FolderIcon;->c(Landroid/widget/TextView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 385
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/android/launcher3/FolderIcon;->I(II)V

    .line 390
    const/16 v1, 0x15e

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/android/launcher3/FolderIcon;->a(Landroid/graphics/drawable/Drawable;IZLjava/lang/Runnable;)V

    .line 391
    invoke-virtual {p0, p1}, Lcom/android/launcher3/FolderIcon;->i(Ladh;)V

    .line 394
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move v4, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher3/FolderIcon;->a(Ladh;Luj;Landroid/graphics/Rect;FILjava/lang/Runnable;)V

    .line 395
    return-void
.end method

.method public final aa(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 331
    check-cast p1, Lwq;

    .line 332
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HI:Lcom/android/launcher3/Folder;

    invoke-virtual {v0}, Lcom/android/launcher3/Folder;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/launcher3/FolderIcon;->b(Lwq;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ab(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 340
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HI:Lcom/android/launcher3/Folder;

    invoke-virtual {v0}, Lcom/android/launcher3/Folder;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p1

    check-cast v0, Lwq;

    invoke-direct {p0, v0}, Lcom/android/launcher3/FolderIcon;->b(Lwq;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 357
    :cond_0
    :goto_0
    return-void

    .line 341
    :cond_1
    invoke-virtual {p0}, Lcom/android/launcher3/FolderIcon;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher3/CellLayout$LayoutParams;

    .line 342
    invoke-virtual {p0}, Lcom/android/launcher3/FolderIcon;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/CellLayout;

    .line 343
    iget-object v2, p0, Lcom/android/launcher3/FolderIcon;->HN:Lvs;

    iget v3, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bb:I

    iget v0, v0, Lcom/android/launcher3/CellLayout$LayoutParams;->Bc:I

    invoke-virtual {v2, v3, v0}, Lvs;->J(II)V

    .line 344
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HN:Lvs;

    invoke-virtual {v0, v1}, Lvs;->h(Lcom/android/launcher3/CellLayout;)V

    .line 345
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HN:Lvs;

    invoke-virtual {v0}, Lvs;->gQ()V

    .line 346
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HN:Lvs;

    invoke-virtual {v1, v0}, Lcom/android/launcher3/CellLayout;->a(Lvs;)V

    .line 347
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->Ia:Lro;

    iget-object v1, p0, Lcom/android/launcher3/FolderIcon;->Ic:Lace;

    invoke-virtual {v0, v1}, Lro;->a(Lace;)V

    .line 348
    instance-of v0, p1, Lrr;

    if-nez v0, :cond_2

    instance-of v0, p1, Ladh;

    if-eqz v0, :cond_3

    .line 354
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->Ia:Lro;

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v2, v3}, Lro;->e(J)V

    .line 356
    :cond_3
    check-cast p1, Lwq;

    iput-object p1, p0, Lcom/android/launcher3/FolderIcon;->Ib:Lwq;

    goto :goto_0
.end method

.method public final b(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 398
    move-object v0, p1

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/android/launcher3/FolderIcon;->c(Landroid/widget/TextView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 399
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/android/launcher3/FolderIcon;->I(II)V

    .line 404
    const/16 v1, 0xc8

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2, p2}, Lcom/android/launcher3/FolderIcon;->a(Landroid/graphics/drawable/Drawable;IZLjava/lang/Runnable;)V

    .line 406
    return-void
.end method

.method public final b(Luq;)V
    .locals 7

    .prologue
    .line 479
    iget-object v0, p1, Luq;->Ge:Ljava/lang/Object;

    instance-of v0, v0, Lrr;

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p1, Luq;->Ge:Ljava/lang/Object;

    check-cast v0, Lrr;

    invoke-virtual {v0}, Lrr;->dX()Ladh;

    move-result-object v1

    .line 485
    :goto_0
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HI:Lcom/android/launcher3/Folder;

    invoke-virtual {v0}, Lcom/android/launcher3/Folder;->gz()V

    .line 486
    iget-object v2, p1, Luq;->Gd:Luj;

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->GM:Lvy;

    iget-object v0, v0, Lvy;->IA:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    iget-object v6, p1, Luq;->Gg:Ljava/lang/Runnable;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher3/FolderIcon;->a(Ladh;Luj;Landroid/graphics/Rect;FILjava/lang/Runnable;)V

    .line 487
    return-void

    .line 483
    :cond_0
    iget-object v0, p1, Luq;->Ge:Ljava/lang/Object;

    check-cast v0, Ladh;

    move-object v1, v0

    goto :goto_0
.end method

.method public cancelLongPress()V
    .locals 1

    .prologue
    .line 747
    invoke-super {p0}, Landroid/widget/FrameLayout;->cancelLongPress()V

    .line 749
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->zA:Ltf;

    invoke-virtual {v0}, Ltf;->cancelLongPress()V

    .line 750
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 603
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 605
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HI:Lcom/android/launcher3/Folder;

    if-nez v0, :cond_1

    .line 635
    :cond_0
    :goto_0
    return-void

    .line 606
    :cond_1
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HI:Lcom/android/launcher3/Folder;

    invoke-virtual {v0}, Lcom/android/launcher3/Folder;->getItemCount()I

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/launcher3/FolderIcon;->hN:Z

    if-eqz v0, :cond_0

    .line 608
    :cond_2
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HI:Lcom/android/launcher3/Folder;

    invoke-virtual {v0}, Lcom/android/launcher3/Folder;->gJ()Ljava/util/ArrayList;

    move-result-object v2

    .line 613
    iget-boolean v0, p0, Lcom/android/launcher3/FolderIcon;->hN:Z

    if-eqz v0, :cond_4

    .line 614
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HY:Lvx;

    iget-object v0, v0, Lvx;->rJ:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0}, Lcom/android/launcher3/FolderIcon;->b(Landroid/graphics/drawable/Drawable;)V

    .line 621
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 622
    iget-boolean v1, p0, Lcom/android/launcher3/FolderIcon;->hN:Z

    if-nez v1, :cond_5

    .line 623
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_0

    .line 624
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 625
    iget-object v3, p0, Lcom/android/launcher3/FolderIcon;->HZ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 626
    invoke-static {v0}, Lcom/android/launcher3/FolderIcon;->c(Landroid/widget/TextView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 627
    iget-object v3, p0, Lcom/android/launcher3/FolderIcon;->HX:Lvx;

    invoke-direct {p0, v1, v3}, Lcom/android/launcher3/FolderIcon;->a(ILvx;)Lvx;

    move-result-object v3

    iput-object v3, p0, Lcom/android/launcher3/FolderIcon;->HX:Lvx;

    .line 628
    iget-object v3, p0, Lcom/android/launcher3/FolderIcon;->HX:Lvx;

    iput-object v0, v3, Lvx;->rJ:Landroid/graphics/drawable/Drawable;

    .line 629
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HX:Lvx;

    invoke-direct {p0, p1, v0}, Lcom/android/launcher3/FolderIcon;->a(Landroid/graphics/Canvas;Lvx;)V

    .line 623
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 616
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 617
    invoke-static {v0}, Lcom/android/launcher3/FolderIcon;->c(Landroid/widget/TextView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 618
    invoke-direct {p0, v0}, Lcom/android/launcher3/FolderIcon;->b(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 633
    :cond_5
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HY:Lvx;

    invoke-direct {p0, p1, v0}, Lcom/android/launcher3/FolderIcon;->a(Landroid/graphics/Canvas;Lvx;)V

    goto :goto_0
.end method

.method public final f(Ladh;)V
    .locals 0

    .prologue
    .line 701
    invoke-virtual {p0}, Lcom/android/launcher3/FolderIcon;->invalidate()V

    .line 702
    invoke-virtual {p0}, Lcom/android/launcher3/FolderIcon;->requestLayout()V

    .line 703
    return-void
.end method

.method public final g(Ladh;)V
    .locals 0

    .prologue
    .line 706
    invoke-virtual {p0}, Lcom/android/launcher3/FolderIcon;->invalidate()V

    .line 707
    invoke-virtual {p0}, Lcom/android/launcher3/FolderIcon;->requestLayout()V

    .line 708
    return-void
.end method

.method public final gI()V
    .locals 0

    .prologue
    .line 696
    invoke-virtual {p0}, Lcom/android/launcher3/FolderIcon;->invalidate()V

    .line 697
    invoke-virtual {p0}, Lcom/android/launcher3/FolderIcon;->requestLayout()V

    .line 698
    return-void
.end method

.method public final gL()Lcom/android/launcher3/Folder;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HI:Lcom/android/launcher3/Folder;

    return-object v0
.end method

.method final gM()Lvy;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->GM:Lvy;

    return-object v0
.end method

.method public final gN()V
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HN:Lvs;

    invoke-virtual {v0}, Lvs;->gR()V

    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->Ia:Lro;

    invoke-virtual {v0}, Lro;->dV()V

    .line 410
    return-void
.end method

.method public final gO()Z
    .locals 1

    .prologue
    .line 692
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HM:Lcom/android/launcher3/BubbleTextView;

    invoke-virtual {v0}, Lcom/android/launcher3/BubbleTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 711
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->HM:Lcom/android/launcher3/BubbleTextView;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/BubbleTextView;->setText(Ljava/lang/CharSequence;)V

    .line 712
    invoke-virtual {p0}, Lcom/android/launcher3/FolderIcon;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a00db

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/launcher3/FolderIcon;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 714
    return-void
.end method

.method public final i(Ladh;)V
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/android/launcher3/FolderIcon;->GM:Lvy;

    invoke-virtual {v0, p1}, Lvy;->j(Ladh;)V

    .line 337
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 741
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 742
    invoke-virtual {p0}, Lcom/android/launcher3/FolderIcon;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/android/launcher3/FolderIcon;->zt:F

    .line 743
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/launcher3/FolderIcon;->HJ:Z

    .line 188
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 720
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 722
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 736
    :cond_0
    :goto_0
    return v0

    .line 724
    :pswitch_0
    iget-object v1, p0, Lcom/android/launcher3/FolderIcon;->zA:Ltf;

    invoke-virtual {v1}, Ltf;->fo()V

    goto :goto_0

    .line 728
    :pswitch_1
    iget-object v1, p0, Lcom/android/launcher3/FolderIcon;->zA:Ltf;

    invoke-virtual {v1}, Ltf;->cancelLongPress()V

    goto :goto_0

    .line 731
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v3, p0, Lcom/android/launcher3/FolderIcon;->zt:F

    invoke-static {p0, v1, v2, v3}, Ladp;->a(Landroid/view/View;FFF)Z

    move-result v1

    if-nez v1, :cond_0

    .line 732
    iget-object v1, p0, Lcom/android/launcher3/FolderIcon;->zA:Ltf;

    invoke-virtual {v1}, Ltf;->cancelLongPress()V

    goto :goto_0

    .line 722
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
