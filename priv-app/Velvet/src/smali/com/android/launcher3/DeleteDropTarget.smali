.class public Lcom/android/launcher3/DeleteDropTarget;
.super Lss;
.source "PG"


# static fields
.field private static Cv:I

.field private static Cw:I

.field private static Cx:F

.field private static Cy:I

.field private static Cz:I


# instance fields
.field private final CA:I

.field private CB:Landroid/content/res/ColorStateList;

.field private CC:Landroid/graphics/drawable/TransitionDrawable;

.field private CD:Landroid/graphics/drawable/TransitionDrawable;

.field private CE:Landroid/graphics/drawable/TransitionDrawable;

.field private CF:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const/16 v0, 0x11d

    sput v0, Lcom/android/launcher3/DeleteDropTarget;->Cv:I

    .line 52
    const/16 v0, 0x15e

    sput v0, Lcom/android/launcher3/DeleteDropTarget;->Cw:I

    .line 53
    const v0, 0x3d0f5c29    # 0.035f

    sput v0, Lcom/android/launcher3/DeleteDropTarget;->Cx:F

    .line 54
    const/4 v0, 0x0

    sput v0, Lcom/android/launcher3/DeleteDropTarget;->Cy:I

    .line 55
    const/4 v0, 0x1

    sput v0, Lcom/android/launcher3/DeleteDropTarget;->Cz:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher3/DeleteDropTarget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p3}, Lss;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    sget v0, Lcom/android/launcher3/DeleteDropTarget;->Cz:I

    iput v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CA:I

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CF:Z

    .line 72
    return-void
.end method

.method public static Z(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 152
    instance-of v0, p0, Lwq;

    if-eqz v0, :cond_7

    move-object v0, p0

    .line 153
    check-cast v0, Lwq;

    .line 154
    iget v3, v0, Lwq;->Jz:I

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    iget v3, v0, Lwq;->Jz:I

    if-ne v3, v1, :cond_1

    :cond_0
    move v0, v1

    .line 181
    :goto_0
    return v0

    .line 159
    :cond_1
    invoke-static {}, Lyu;->im()Z

    move-result v3

    if-nez v3, :cond_2

    iget v3, v0, Lwq;->Jz:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    move v0, v1

    .line 161
    goto :goto_0

    .line 164
    :cond_2
    invoke-static {}, Lyu;->im()Z

    move-result v3

    if-nez v3, :cond_4

    iget v3, v0, Lwq;->Jz:I

    if-nez v3, :cond_4

    instance-of v3, v0, Lrr;

    if-eqz v3, :cond_4

    .line 167
    check-cast p0, Lrr;

    .line 168
    iget v0, p0, Lrr;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    .line 171
    :cond_4
    iget v3, v0, Lwq;->Jz:I

    if-nez v3, :cond_7

    instance-of v0, v0, Ladh;

    if-eqz v0, :cond_7

    .line 173
    invoke-static {}, Lyu;->im()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 174
    check-cast p0, Ladh;

    .line 175
    iget v0, p0, Ladh;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0

    :cond_6
    move v0, v1

    .line 177
    goto :goto_0

    :cond_7
    move v0, v2

    .line 181
    goto :goto_0
.end method

.method public static synthetic a(Lcom/android/launcher3/DeleteDropTarget;Luq;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 50
    iget-object v0, p1, Luq;->Ge:Ljava/lang/Object;

    check-cast v0, Lwq;

    iget-boolean v4, p0, Lcom/android/launcher3/DeleteDropTarget;->CF:Z

    iput-boolean v3, p0, Lcom/android/launcher3/DeleteDropTarget;->CF:Z

    iget-object v1, p1, Luq;->Gf:Lui;

    invoke-static {v1, v0}, Lcom/android/launcher3/DeleteDropTarget;->b(Lui;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    check-cast v0, Lrr;

    iget-object v1, p0, Lcom/android/launcher3/DeleteDropTarget;->xZ:Lcom/android/launcher3/Launcher;

    iget-object v2, v0, Lrr;->xr:Landroid/content/ComponentName;

    iget v5, v0, Lrr;->flags:I

    iget-object v0, v0, Lrr;->Jl:Lahz;

    invoke-virtual {v1, v2, v5, v0}, Lcom/android/launcher3/Launcher;->a(Landroid/content/ComponentName;ILahz;)Z

    :cond_0
    :goto_0
    if-eqz v4, :cond_1

    iget-boolean v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CF:Z

    if-nez v0, :cond_1

    iget-object v0, p1, Luq;->Gf:Lui;

    instance-of v0, v0, Lcom/android/launcher3/Folder;

    if-eqz v0, :cond_9

    iget-object v0, p1, Luq;->Gf:Lui;

    check-cast v0, Lcom/android/launcher3/Folder;

    invoke-virtual {v0, v3}, Lcom/android/launcher3/Folder;->P(Z)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-direct {p0, p1}, Lcom/android/launcher3/DeleteDropTarget;->i(Luq;)Z

    move-result v1

    if-eqz v1, :cond_3

    check-cast v0, Ladh;

    iget-object v1, v0, Ladh;->intent:Landroid/content/Intent;

    if-eqz v1, :cond_0

    iget-object v1, v0, Ladh;->intent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Ladh;->intent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    iget-object v2, p1, Luq;->Gf:Lui;

    iget-object v5, v0, Ladh;->Jl:Lahz;

    iget-object v6, p0, Lcom/android/launcher3/DeleteDropTarget;->xZ:Lcom/android/launcher3/Launcher;

    iget v0, v0, Ladh;->flags:I

    invoke-virtual {v6, v1, v0, v5}, Lcom/android/launcher3/Launcher;->a(Landroid/content/ComponentName;ILahz;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CF:Z

    iget-boolean v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CF:Z

    if-eqz v0, :cond_0

    new-instance v0, Ltn;

    invoke-direct {v0, p0, v1, v5, v2}, Ltn;-><init>(Lcom/android/launcher3/DeleteDropTarget;Landroid/content/ComponentName;Lahz;Lui;)V

    iget-object v1, p0, Lcom/android/launcher3/DeleteDropTarget;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1, v0}, Lcom/android/launcher3/Launcher;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1}, Lcom/android/launcher3/DeleteDropTarget;->g(Luq;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/launcher3/DeleteDropTarget;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v1, v0}, Lzi;->b(Landroid/content/Context;Lwq;)V

    goto :goto_0

    :cond_4
    iget-object v1, p1, Luq;->Gf:Lui;

    instance-of v1, v1, Lcom/android/launcher3/Workspace;

    if-eqz v1, :cond_5

    iget-object v1, p1, Luq;->Ge:Ljava/lang/Object;

    instance-of v1, v1, Lvy;

    if-eqz v1, :cond_5

    move v1, v2

    :goto_2
    if-eqz v1, :cond_6

    check-cast v0, Lvy;

    iget-object v1, p0, Lcom/android/launcher3/DeleteDropTarget;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v0}, Lcom/android/launcher3/Launcher;->b(Lvy;)V

    iget-object v1, p0, Lcom/android/launcher3/DeleteDropTarget;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v1, v0}, Lzi;->a(Landroid/content/Context;Lvy;)V

    goto :goto_0

    :cond_5
    move v1, v3

    goto :goto_2

    :cond_6
    invoke-static {p1}, Lcom/android/launcher3/DeleteDropTarget;->f(Luq;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p1, Luq;->Ge:Ljava/lang/Object;

    instance-of v1, v1, Lyy;

    if-eqz v1, :cond_7

    move v1, v2

    :goto_3
    if-eqz v1, :cond_0

    iget-object v5, p0, Lcom/android/launcher3/DeleteDropTarget;->xZ:Lcom/android/launcher3/Launcher;

    move-object v1, v0

    check-cast v1, Lyy;

    invoke-virtual {v5, v1}, Lcom/android/launcher3/Launcher;->a(Lyy;)V

    iget-object v1, p0, Lcom/android/launcher3/DeleteDropTarget;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v1, v0}, Lzi;->b(Landroid/content/Context;Lwq;)V

    check-cast v0, Lyy;

    iget-object v1, p0, Lcom/android/launcher3/DeleteDropTarget;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->hr()Lyw;

    move-result-object v5

    if-eqz v5, :cond_0

    iget v1, v0, Lyy;->Mq:I

    and-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_8

    move v1, v2

    :goto_4
    if-eqz v1, :cond_0

    new-instance v1, Lto;

    invoke-direct {v1, p0, v5, v0}, Lto;-><init>(Lcom/android/launcher3/DeleteDropTarget;Lyw;Lyy;)V

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    const/4 v5, 0x0

    aput-object v5, v2, v3

    invoke-virtual {v1, v0, v2}, Lto;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    :cond_7
    move v1, v3

    goto :goto_3

    :cond_8
    move v1, v3

    goto :goto_4

    :cond_9
    iget-object v0, p1, Luq;->Gf:Lui;

    instance-of v0, v0, Lcom/android/launcher3/Workspace;

    if-eqz v0, :cond_1

    iget-object v0, p1, Luq;->Gf:Lui;

    check-cast v0, Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v3}, Lcom/android/launcher3/Workspace;->P(Z)V

    goto/16 :goto_1
.end method

.method public static synthetic a(Lcom/android/launcher3/DeleteDropTarget;Z)Z
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CF:Z

    return v0
.end method

.method private static b(Lui;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 105
    invoke-interface {p0}, Lui;->em()Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lrr;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static f(Luq;)Z
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Luq;->Gf:Lui;

    instance-of v0, v0, Lcom/android/launcher3/Workspace;

    if-nez v0, :cond_0

    iget-object v0, p0, Luq;->Gf:Lui;

    instance-of v0, v0, Lcom/android/launcher3/Folder;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private fy()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CE:Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CE:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CB:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lcom/android/launcher3/DeleteDropTarget;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 144
    return-void
.end method

.method private g(Luq;)Z
    .locals 1

    .prologue
    .line 124
    invoke-static {p1}, Lcom/android/launcher3/DeleteDropTarget;->f(Luq;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Luq;->Ge:Ljava/lang/Object;

    instance-of v0, v0, Ladh;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h(Luq;)V
    .locals 1

    .prologue
    .line 279
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CF:Z

    .line 280
    invoke-direct {p0, p1}, Lcom/android/launcher3/DeleteDropTarget;->i(Luq;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281
    iget-object v0, p1, Luq;->Gf:Lui;

    instance-of v0, v0, Lcom/android/launcher3/Folder;

    if-eqz v0, :cond_2

    .line 282
    iget-object v0, p1, Luq;->Gf:Lui;

    check-cast v0, Lcom/android/launcher3/Folder;

    invoke-virtual {v0}, Lcom/android/launcher3/Folder;->gw()V

    .line 286
    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CF:Z

    .line 288
    :cond_1
    return-void

    .line 283
    :cond_2
    iget-object v0, p1, Luq;->Gf:Lui;

    instance-of v0, v0, Lcom/android/launcher3/Workspace;

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p1, Luq;->Gf:Lui;

    check-cast v0, Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->gw()V

    goto :goto_0
.end method

.method private i(Luq;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 291
    invoke-static {}, Lyu;->im()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/launcher3/DeleteDropTarget;->g(Luq;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 292
    iget-object v0, p1, Luq;->Ge:Ljava/lang/Object;

    check-cast v0, Ladh;

    .line 294
    iget-object v0, v0, Ladh;->intent:Landroid/content/Intent;

    invoke-static {v0}, Lcom/android/launcher3/InstallShortcutReceiver;->c(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 296
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 294
    goto :goto_0

    :cond_1
    move v0, v1

    .line 296
    goto :goto_0
.end method


# virtual methods
.method public final a(Lui;Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 186
    .line 187
    invoke-static {}, Lyu;->im()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {p1, p2}, Lcom/android/launcher3/DeleteDropTarget;->b(Lui;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v4, v0

    .line 189
    :goto_0
    if-nez v4, :cond_5

    invoke-interface {p1}, Lui;->en()Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v0

    .line 194
    :goto_1
    invoke-static {p2}, Lcom/android/launcher3/DeleteDropTarget;->Z(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    instance-of v3, p1, Lcom/android/launcher3/AppsCustomizePagedView;

    if-eqz v3, :cond_0

    instance-of v3, p2, Lacw;

    if-eqz v3, :cond_0

    check-cast p2, Lacw;

    iget v3, p2, Lacw;->Jz:I

    packed-switch v3, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v3, v2

    :goto_2
    if-eqz v3, :cond_b

    :cond_1
    move v3, v2

    .line 197
    :goto_3
    if-eqz v4, :cond_a

    .line 198
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-lt v0, v5, :cond_a

    .line 199
    invoke-virtual {p0}, Lcom/android/launcher3/DeleteDropTarget;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v5, "user"

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 201
    invoke-virtual {v0}, Landroid/os/UserManager;->getUserRestrictions()Landroid/os/Bundle;

    move-result-object v0

    .line 202
    const-string v5, "no_control_apps"

    invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "no_uninstall_apps"

    invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_2
    move v0, v2

    .line 209
    :goto_4
    if-eqz v4, :cond_6

    .line 210
    iget-object v1, p0, Lcom/android/launcher3/DeleteDropTarget;->CC:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {p0, v1, v6, v6, v6}, Lcom/android/launcher3/DeleteDropTarget;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move v1, v0

    .line 216
    :goto_5
    invoke-virtual {p0}, Lcom/android/launcher3/DeleteDropTarget;->eI()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    iput-object v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CE:Landroid/graphics/drawable/TransitionDrawable;

    .line 218
    iput-boolean v1, p0, Lcom/android/launcher3/DeleteDropTarget;->zE:Z

    .line 219
    invoke-direct {p0}, Lcom/android/launcher3/DeleteDropTarget;->fy()V

    .line 220
    invoke-virtual {p0}, Lcom/android/launcher3/DeleteDropTarget;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_8

    :goto_6
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 221
    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/android/launcher3/DeleteDropTarget;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 222
    if-eqz v4, :cond_9

    const v0, 0x7f0a00a6

    :goto_7
    invoke-virtual {p0, v0}, Lcom/android/launcher3/DeleteDropTarget;->setText(I)V

    .line 225
    :cond_3
    return-void

    :cond_4
    move v4, v2

    .line 187
    goto/16 :goto_0

    :cond_5
    move v1, v2

    .line 189
    goto :goto_1

    :pswitch_1
    move v3, v0

    .line 194
    goto :goto_2

    .line 211
    :cond_6
    if-eqz v1, :cond_7

    .line 212
    iget-object v1, p0, Lcom/android/launcher3/DeleteDropTarget;->CD:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {p0, v1, v6, v6, v6}, Lcom/android/launcher3/DeleteDropTarget;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move v1, v0

    goto :goto_5

    :cond_7
    move v1, v2

    .line 214
    goto :goto_5

    .line 220
    :cond_8
    const/16 v2, 0x8

    goto :goto_6

    .line 222
    :cond_9
    const v0, 0x7f0a00a5

    goto :goto_7

    :cond_a
    move v0, v3

    goto :goto_4

    :cond_b
    move v3, v0

    goto :goto_3

    .line 194
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Luq;Landroid/graphics/PointF;)V
    .locals 14

    .prologue
    .line 494
    iget-object v1, p1, Luq;->Gf:Lui;

    instance-of v11, v1, Lcom/android/launcher3/AppsCustomizePagedView;

    .line 497
    iget-object v1, p1, Luq;->Gd:Luj;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Luj;->setColor(I)V

    .line 498
    iget-object v1, p1, Luq;->Gd:Luj;

    invoke-virtual {v1}, Luj;->gb()V

    .line 500
    if-eqz v11, :cond_0

    .line 501
    invoke-direct {p0}, Lcom/android/launcher3/DeleteDropTarget;->fy()V

    .line 504
    :cond_0
    iget v1, p0, Lcom/android/launcher3/DeleteDropTarget;->CA:I

    if-nez v1, :cond_1

    .line 506
    iget-object v1, p0, Lcom/android/launcher3/DeleteDropTarget;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    invoke-virtual {v1}, Lcom/android/launcher3/SearchDropTargetBar;->kb()V

    .line 507
    iget-object v1, p0, Lcom/android/launcher3/DeleteDropTarget;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    invoke-virtual {v1}, Lcom/android/launcher3/SearchDropTargetBar;->ka()V

    .line 510
    :cond_1
    iget-object v1, p0, Lcom/android/launcher3/DeleteDropTarget;->xZ:Lcom/android/launcher3/Launcher;

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    .line 511
    iget-object v1, p0, Lcom/android/launcher3/DeleteDropTarget;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v3

    .line 512
    sget v12, Lcom/android/launcher3/DeleteDropTarget;->Cw:I

    .line 513
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v6

    .line 520
    new-instance v13, Ltr;

    invoke-direct {v13, p0, v6, v7, v12}, Ltr;-><init>(Lcom/android/launcher3/DeleteDropTarget;JI)V

    .line 536
    const/4 v5, 0x0

    .line 537
    iget v1, p0, Lcom/android/launcher3/DeleteDropTarget;->CA:I

    if-nez v1, :cond_5

    .line 538
    iget-object v1, p0, Lcom/android/launcher3/DeleteDropTarget;->CE:Landroid/graphics/drawable/TransitionDrawable;

    if-nez v1, :cond_3

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/launcher3/DeleteDropTarget;->CE:Landroid/graphics/drawable/TransitionDrawable;

    if-nez v2, :cond_4

    const/4 v2, 0x0

    :goto_1
    iget-object v5, p1, Luq;->Gd:Luj;

    invoke-virtual {v5}, Luj;->getMeasuredWidth()I

    move-result v5

    iget-object v6, p1, Luq;->Gd:Luj;

    invoke-virtual {v6}, Luj;->getMeasuredHeight()I

    move-result v6

    invoke-virtual {p0, v5, v6, v1, v2}, Lcom/android/launcher3/DeleteDropTarget;->c(IIII)Landroid/graphics/Rect;

    move-result-object v1

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iget-object v5, p1, Luq;->Gd:Luj;

    invoke-virtual {v3, v5, v2}, Lcom/android/launcher3/DragLayer;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/PointF;->length()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v4

    int-to-float v4, v4

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v4, v7

    div-float v4, v5, v4

    invoke-static {v6, v4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    iget v5, v2, Landroid/graphics/Rect;->top:I

    neg-int v5, v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    float-to-int v4, v4

    int-to-float v5, v4

    move-object/from16 v0, p2

    iget v6, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p2

    iget v7, v0, Landroid/graphics/PointF;->x:F

    div-float/2addr v6, v7

    div-float/2addr v5, v6

    float-to-int v5, v5

    iget v6, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v6

    int-to-float v9, v4

    iget v4, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v5

    int-to-float v6, v4

    iget v4, v2, Landroid/graphics/Rect;->left:I

    int-to-float v5, v4

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v8, v2

    iget v2, v1, Landroid/graphics/Rect;->left:I

    int-to-float v7, v2

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v10, v1

    new-instance v4, Ltp;

    invoke-direct {v4, p0}, Ltp;-><init>(Lcom/android/launcher3/DeleteDropTarget;)V

    new-instance v1, Ltq;

    move-object v2, p0

    invoke-direct/range {v1 .. v10}, Ltq;-><init>(Lcom/android/launcher3/DeleteDropTarget;Lcom/android/launcher3/DragLayer;Landroid/animation/TimeInterpolator;FFFFFF)V

    move-object v5, v1

    .line 543
    :cond_2
    :goto_2
    invoke-direct {p0, p1}, Lcom/android/launcher3/DeleteDropTarget;->h(Luq;)V

    .line 545
    new-instance v8, Lts;

    invoke-direct {v8, p0, v11, p1}, Lts;-><init>(Lcom/android/launcher3/DeleteDropTarget;ZLuq;)V

    .line 557
    iget-object v4, p1, Luq;->Gd:Luj;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move v6, v12

    move-object v7, v13

    invoke-virtual/range {v3 .. v10}, Lcom/android/launcher3/DragLayer;->a(Luj;Landroid/animation/ValueAnimator$AnimatorUpdateListener;ILandroid/animation/TimeInterpolator;Ljava/lang/Runnable;ILandroid/view/View;)V

    .line 559
    return-void

    .line 538
    :cond_3
    iget-object v1, p0, Lcom/android/launcher3/DeleteDropTarget;->CE:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/TransitionDrawable;->getIntrinsicWidth()I

    move-result v1

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lcom/android/launcher3/DeleteDropTarget;->CE:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/TransitionDrawable;->getIntrinsicHeight()I

    move-result v2

    goto/16 :goto_1

    .line 539
    :cond_5
    iget v1, p0, Lcom/android/launcher3/DeleteDropTarget;->CA:I

    sget v2, Lcom/android/launcher3/DeleteDropTarget;->Cz:I

    if-ne v1, v2, :cond_2

    .line 540
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    iget-object v1, p1, Luq;->Gd:Luj;

    invoke-virtual {v3, v1, v5}, Lcom/android/launcher3/DragLayer;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    new-instance v2, Ltt;

    sget v8, Lcom/android/launcher3/DeleteDropTarget;->Cx:F

    move-object/from16 v4, p2

    invoke-direct/range {v2 .. v8}, Ltt;-><init>(Lcom/android/launcher3/DragLayer;Landroid/graphics/PointF;Landroid/graphics/Rect;JF)V

    move-object v5, v2

    goto :goto_2
.end method

.method public final a(Luq;)Z
    .locals 1

    .prologue
    .line 148
    iget-object v0, p1, Luq;->Ge:Ljava/lang/Object;

    invoke-static {v0}, Lcom/android/launcher3/DeleteDropTarget;->Z(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Luq;)V
    .locals 17

    .prologue
    .line 371
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher3/DeleteDropTarget;->xZ:Lcom/android/launcher3/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v2

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p1

    iget-object v3, v0, Luq;->Gd:Luj;

    invoke-virtual {v2, v3, v4}, Lcom/android/launcher3/DragLayer;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/DeleteDropTarget;->CE:Landroid/graphics/drawable/TransitionDrawable;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/DeleteDropTarget;->CE:Landroid/graphics/drawable/TransitionDrawable;

    if-nez v5, :cond_1

    const/4 v5, 0x0

    :goto_1
    move-object/from16 v0, p1

    iget-object v6, v0, Luq;->Gd:Luj;

    invoke-virtual {v6}, Luj;->getMeasuredWidth()I

    move-result v6

    move-object/from16 v0, p1

    iget-object v7, v0, Luq;->Gd:Luj;

    invoke-virtual {v7}, Luj;->getMeasuredHeight()I

    move-result v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7, v3, v5}, Lcom/android/launcher3/DeleteDropTarget;->c(IIII)Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    div-float v6, v3, v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/DeleteDropTarget;->zD:Lcom/android/launcher3/SearchDropTargetBar;

    invoke-virtual {v3}, Lcom/android/launcher3/SearchDropTargetBar;->kb()V

    invoke-direct/range {p0 .. p1}, Lcom/android/launcher3/DeleteDropTarget;->h(Luq;)V

    new-instance v14, Ltm;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v14, v0, v1}, Ltm;-><init>(Lcom/android/launcher3/DeleteDropTarget;Luq;)V

    move-object/from16 v0, p1

    iget-object v3, v0, Luq;->Gd:Luj;

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v8, 0x3f800000    # 1.0f

    const v9, 0x3dcccccd    # 0.1f

    const v10, 0x3dcccccd    # 0.1f

    sget v11, Lcom/android/launcher3/DeleteDropTarget;->Cv:I

    new-instance v12, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v13, 0x40000000    # 2.0f

    invoke-direct {v12, v13}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    new-instance v13, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v13}, Landroid/view/animation/LinearInterpolator;-><init>()V

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v2 .. v16}, Lcom/android/launcher3/DragLayer;->a(Luj;Landroid/graphics/Rect;Landroid/graphics/Rect;FFFFFILandroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;Ljava/lang/Runnable;ILandroid/view/View;)V

    .line 372
    return-void

    .line 371
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher3/DeleteDropTarget;->CE:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/TransitionDrawable;->getIntrinsicWidth()I

    move-result v3

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher3/DeleteDropTarget;->CE:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/TransitionDrawable;->getIntrinsicHeight()I

    move-result v5

    goto :goto_1
.end method

.method public final c(Luq;)V
    .locals 2

    .prologue
    .line 234
    invoke-super {p0, p1}, Lss;->c(Luq;)V

    .line 236
    iget-object v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CE:Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CE:Landroid/graphics/drawable/TransitionDrawable;

    iget v1, p0, Lcom/android/launcher3/DeleteDropTarget;->zB:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    :cond_0
    iget v0, p0, Lcom/android/launcher3/DeleteDropTarget;->zF:I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/DeleteDropTarget;->setTextColor(I)V

    .line 237
    return-void
.end method

.method public final e(Luq;)V
    .locals 2

    .prologue
    .line 240
    invoke-super {p0, p1}, Lss;->e(Luq;)V

    .line 242
    iget-boolean v0, p1, Luq;->Gc:Z

    if-nez v0, :cond_0

    .line 243
    invoke-direct {p0}, Lcom/android/launcher3/DeleteDropTarget;->fy()V

    .line 248
    :goto_0
    return-void

    .line 246
    :cond_0
    iget-object v0, p1, Luq;->Gd:Luj;

    iget v1, p0, Lcom/android/launcher3/DeleteDropTarget;->zF:I

    invoke-virtual {v0, v1}, Luj;->setColor(I)V

    goto :goto_0
.end method

.method public final eK()V
    .locals 1

    .prologue
    .line 229
    invoke-super {p0}, Lss;->eK()V

    .line 230
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher3/DeleteDropTarget;->zE:Z

    .line 231
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 76
    invoke-super {p0}, Lss;->onFinishInflate()V

    .line 79
    invoke-virtual {p0}, Lcom/android/launcher3/DeleteDropTarget;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CB:Landroid/content/res/ColorStateList;

    .line 82
    invoke-virtual {p0}, Lcom/android/launcher3/DeleteDropTarget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 83
    const v0, 0x7f0b004c

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher3/DeleteDropTarget;->zF:I

    .line 84
    const v0, 0x7f020301

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    iput-object v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CC:Landroid/graphics/drawable/TransitionDrawable;

    .line 86
    const v0, 0x7f0202a3

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    iput-object v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CD:Landroid/graphics/drawable/TransitionDrawable;

    .line 88
    iget-object v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CD:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 89
    iget-object v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CC:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 93
    invoke-virtual {p0}, Lcom/android/launcher3/DeleteDropTarget;->eI()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    iput-object v0, p0, Lcom/android/launcher3/DeleteDropTarget;->CE:Landroid/graphics/drawable/TransitionDrawable;

    .line 96
    invoke-virtual {p0}, Lcom/android/launcher3/DeleteDropTarget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 97
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 98
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    iget-boolean v0, v0, Lyu;->Md:Z

    if-nez v0, :cond_0

    .line 99
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/android/launcher3/DeleteDropTarget;->setText(Ljava/lang/CharSequence;)V

    .line 102
    :cond_0
    return-void
.end method
