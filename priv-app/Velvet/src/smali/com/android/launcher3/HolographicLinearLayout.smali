.class public Lcom/android/launcher3/HolographicLinearLayout;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private final IC:Lwg;

.field private ID:Z

.field private IE:Z

.field private IF:Z

.field private IH:Landroid/widget/ImageView;

.field private II:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher3/HolographicLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher3/HolographicLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    sget-object v0, Ladb;->Sv:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 52
    const/4 v1, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher3/HolographicLinearLayout;->II:I

    .line 53
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/launcher3/HolographicLinearLayout;->ID:Z

    .line 54
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 57
    invoke-virtual {p0, v2}, Lcom/android/launcher3/HolographicLinearLayout;->setWillNotDraw(Z)V

    .line 58
    new-instance v0, Lwg;

    invoke-direct {v0, p1}, Lwg;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher3/HolographicLinearLayout;->IC:Lwg;

    .line 60
    new-instance v0, Lwd;

    invoke-direct {v0, p0}, Lwd;-><init>(Lcom/android/launcher3/HolographicLinearLayout;)V

    invoke-virtual {p0, v0}, Lcom/android/launcher3/HolographicLinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 71
    new-instance v0, Lwe;

    invoke-direct {v0, p0}, Lwe;-><init>(Lcom/android/launcher3/HolographicLinearLayout;)V

    invoke-virtual {p0, v0}, Lcom/android/launcher3/HolographicLinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 80
    return-void
.end method

.method public static synthetic a(Lcom/android/launcher3/HolographicLinearLayout;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/android/launcher3/HolographicLinearLayout;->IE:Z

    return v0
.end method

.method public static synthetic a(Lcom/android/launcher3/HolographicLinearLayout;Z)Z
    .locals 0

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/android/launcher3/HolographicLinearLayout;->IE:Z

    return p1
.end method

.method public static synthetic b(Lcom/android/launcher3/HolographicLinearLayout;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/android/launcher3/HolographicLinearLayout;->IF:Z

    return v0
.end method

.method public static synthetic b(Lcom/android/launcher3/HolographicLinearLayout;Z)Z
    .locals 0

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/android/launcher3/HolographicLinearLayout;->IF:Z

    return p1
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 84
    invoke-super {p0}, Landroid/widget/LinearLayout;->drawableStateChanged()V

    .line 86
    iget-object v0, p0, Lcom/android/launcher3/HolographicLinearLayout;->IH:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/android/launcher3/HolographicLinearLayout;->IC:Lwg;

    iget-object v1, p0, Lcom/android/launcher3/HolographicLinearLayout;->IH:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lwg;->a(Landroid/widget/ImageView;)V

    .line 88
    iget-object v0, p0, Lcom/android/launcher3/HolographicLinearLayout;->IH:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 89
    instance-of v1, v0, Landroid/graphics/drawable/StateListDrawable;

    if-eqz v1, :cond_0

    .line 90
    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    .line 91
    invoke-virtual {p0}, Lcom/android/launcher3/HolographicLinearLayout;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    .line 92
    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->invalidateSelf()V

    .line 95
    :cond_0
    return-void
.end method

.method final gV()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/launcher3/HolographicLinearLayout;->IC:Lwg;

    iget-object v1, p0, Lcom/android/launcher3/HolographicLinearLayout;->IH:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lwg;->b(Landroid/widget/ImageView;)V

    .line 99
    invoke-virtual {p0}, Lcom/android/launcher3/HolographicLinearLayout;->invalidate()V

    .line 100
    return-void
.end method

.method public onCreateDrawableState(I)[I
    .locals 4

    .prologue
    .line 128
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 129
    iget-boolean v1, p0, Lcom/android/launcher3/HolographicLinearLayout;->ID:Z

    if-eqz v1, :cond_0

    .line 130
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x7f01005d

    aput v3, v1, v2

    invoke-static {v0, v1}, Lcom/android/launcher3/HolographicLinearLayout;->mergeDrawableStates([I[I)[I

    .line 132
    :cond_0
    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 108
    iget-object v0, p0, Lcom/android/launcher3/HolographicLinearLayout;->IH:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 109
    iget v0, p0, Lcom/android/launcher3/HolographicLinearLayout;->II:I

    invoke-virtual {p0, v0}, Lcom/android/launcher3/HolographicLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/launcher3/HolographicLinearLayout;->IH:Landroid/widget/ImageView;

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/android/launcher3/HolographicLinearLayout;->IC:Lwg;

    iget-object v1, p0, Lcom/android/launcher3/HolographicLinearLayout;->IH:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lwg;->a(Landroid/widget/ImageView;)V

    .line 112
    return-void
.end method
